﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Payroll_wPg_PayslipReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    'Sohail (12 Oct 2021) -- Start
    'NMB Issue :  : Performance issue on payslip report in new ui.
    'Private mdtEmployee As DataTable
    'Private mdtPeriod As DataTable
    'Sohail (12 Oct 2021) -- End
    'Sohail (18 May 2013) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPaySlip"
    'Anjan [04 June 2014] -- End
    Private mstrAdvanceFilter As String = "" 'Varsha (29-Sep-2017)
    'S.SANDEEP |22-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
    Private mblnFromDashBoard As Boolean = False
    'S.SANDEEP |22-JUN-2021| -- END
#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dtTable As DataTable
        'Sohail (16 Jul 2012) -- End
        Dim objExRate As New clsExchangeRate 'Sohail (11 Nov 2014)
        Dim dsCombo As DataSet
        Try
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Moved to drpFinancialYear_SelectedIndexChanged
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, 2)
            'With drpPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsCombo = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True)
                With cboMembership
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With

                dsCombo = (New clsleavetype_master).getListForCombo(, True, 1, Session("Database_Name").ToString)
                With cboLeaveType
                    .DataValueField = "leavetypeunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
            Else
                'Sohail (18 May 2013) -- End
                dsCombo = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List")
                dtTable = New DataView(dsCombo.Tables("List"), "", "end_date DESC ", DataViewRowState.CurrentRows).ToTable
                With drpFinancialYear
                    .DataValueField = "yearunkid"
                    .DataTextField = "financialyear_name"
                    .DataSource = dtTable
                    .DataBind()
                    If .Items.Count > 0 Then
                        .SelectedIndex = 0
                        Call drpFinancialYear_SelectedIndexChanged(Me, New System.EventArgs())
                    End If
                End With
                'Sohail (16 Jul 2012) -- End
            End If 'Sohail (18 May 2013)

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            dsCombo = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("ExRate")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (11 Nov 2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
        Finally
            objCompany = Nothing
            objExRate = Nothing
            'Sohail (11 Nov 2014) -- End
        End Try
    End Sub

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT

    'S.SANDEEP |22-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
    'Private Sub CreateEmpListTable()
    '    Try
    '        mdtEmployee = New DataTable
    '        With mdtEmployee
    '            .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            .Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            .Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
    '        End With

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |22-JUN-2021| -- END

    'Sohail (12 Oct 2021) -- Start
    'NMB Issue :  : Performance issue on payslip report in new ui.
    'Private Sub CreatePeriodListTable()
    '    Try
    '        mdtPeriod = New DataTable
    '        With mdtPeriod
    '            .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            .Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            .Columns.Add("period_name", System.Type.GetType("System.String")).DefaultValue = ""
    '            .Columns.Add("start_date", System.Type.GetType("System.String")).DefaultValue = ""
    '            .Columns.Add("end_date", System.Type.GetType("System.String")).DefaultValue = ""
    '        End With

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Sohail (12 Oct 2021) -- End

    Public Sub FillEmpList()
        Dim objEmployee As New clsEmployee_Master
        'Sohail (12 Oct 2021) -- Start
        'NMB Issue :  : Performance issue on payslip report in new ui.
        Dim mdtEmployee As DataTable
        Dim dsList As DataSet = Nothing
        'Sohail (12 Oct 2021) -- End
        Try
            'S.SANDEEP |22-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
            'mdtEmployee.Rows.Clear()
            'S.SANDEEP |22-JUN-2021| -- END


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objEmployee.GetEmployeeList("Employee", False, Not chkInactiveemp.Checked, , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            Dim blnIncludeInactiveEmp As Boolean = chkInactiveemp.Checked

            'Varsha Rana (29-Sept-2017) -- Start
            'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
            'Dim dsList As DataSet = objEmployee.GetEmployeeList(Session("Database_Name").ToString, _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    Session("UserAccessModeSetting").ToString, True, _
            '                                    blnIncludeInactiveEmp, "Employee", False)


            'S.SANDEEP |22-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
            'Dim dsList As DataSet = objEmployee.GetEmployeeList(Session("Database_Name").ToString, _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    Session("UserAccessModeSetting").ToString, True, _
            '                                    blnIncludeInactiveEmp, "Employee", False, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True)
            ''Varsha Rana (29-Sept-2017) -- End
            ''Shani(24-Aug-2015) -- End
            'Dim dtTable As DataTable = New DataView(dsList.Tables("Employee"), "", "employeename", DataViewRowState.CurrentRows).ToTable

            'Dim dRow As DataRow

            'For Each dtRow As DataRow In dtTable.Rows
            '    dRow = mdtEmployee.NewRow

            '    dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
            '    dRow.Item("employeename") = dtRow.Item("employeename").ToString

            '    mdtEmployee.Rows.Add(dRow)
            'Next
            'Dim dsList As New DataSet 'Sohail (12 Oct 2021)
            Dim intEmpId As Integer = 0
            If mblnFromDashBoard Then
                If CInt(Session("E_Employeeunkid")) > 0 Then intEmpId = CInt(Session("E_Employeeunkid"))
            End If
            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                Session("UserAccessModeSetting").ToString, True, _
                                                blnIncludeInactiveEmp, "Employee", False, intEmpId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True)

            Dim dtTable As DataTable = New DataView(dsList.Tables("Employee"), "", "employeename", DataViewRowState.CurrentRows).ToTable

            Dim iCol As New DataColumn
            With iCol
                .ColumnName = "IsChecked"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dtTable.Columns.Add(iCol)
            mdtEmployee = dtTable.DefaultView.ToTable(False, "IsChecked", "employeeunkid", "employeename")
            'S.SANDEEP |22-JUN-2021| -- END

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            Dim blnRecordExist As Boolean = True
            If mdtEmployee.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = mdtEmployee.NewRow
                mdtEmployee.Rows.Add(r)
            End If

            dgvEmployee.DataSource = mdtEmployee
            dgvEmployee.DataBind()

            If blnRecordExist = False Then
                dgvEmployee.Rows(0).Visible = False
            End If
            'Sohail (12 Oct 2021) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillEmpList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objEmployee = Nothing

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'If mdtEmployee.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtEmployee.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(1) = "0"

            '    mdtEmployee.Rows.Add(r)
            'End If

            ''Nilay (23-Aug-2016) -- Start
            ''ENHANCEMENT : Search option on payslip in MSS
            ''gvEmployee.DataSource = mdtEmployee
            ''gvEmployee.DataBind()
            'dgvEmployee.DataSource = mdtEmployee
            'dgvEmployee.DataBind()
            ''Nilay (23-Aug-2016) -- END
            'Sohail (12 Oct 2021) -- End

        End Try
    End Sub

    Public Sub FillPeriodList()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing 'Sohail (12 Oct 2021)
        Try
            'mdtPeriod.Rows.Clear() 'Sohail (12 Oct 2021)


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objPeriod.GetList("Period", enModuleReference.Payroll, True, , True, , CInt(Session("Fin_year")))
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'Dim dsList As DataSet = objPeriod.GetList("Period", enModuleReference.Payroll, CInt(Session("Fin_year")), CDate(Session("fin_startdate")), True, , True)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, CInt(Session("Fin_year")), CDate(Session("fin_startdate")), True, , True)
            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            Dim blnRecordExist As Boolean = True
            If dsList.Tables(0).Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dsList.Tables(0).NewRow
                dsList.Tables(0).Rows.Add(r)
            End If

            gvPeriod.DataSource = dsList.Tables(0)
            gvPeriod.DataBind()

            If blnRecordExist = False Then
                gvPeriod.Rows(0).Visible = False
            End If
            'Sohail (12 Oct 2021) -- End
            'Shani(20-Nov-2015) -- End

            'Dim dRow As DataRow

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'For Each dtRow As DataRow In dsList.Tables("Period").Rows
            '    dRow = mdtPeriod.NewRow

            '    dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
            '    dRow.Item("period_name") = dtRow.Item("period_name").ToString

            '    dRow.Item("start_date") = dtRow.Item("start_date").ToString
            '    dRow.Item("end_date") = dtRow.Item("end_date").ToString

            '    mdtPeriod.Rows.Add(dRow)
            'Next
            'Sohail (12 Oct 2021) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillPeriodList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPeriod = Nothing

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'If mdtPeriod.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtPeriod.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(1) = "0"

            '    mdtPeriod.Rows.Add(r)
            'End If

            'gvPeriod.DataSource = mdtPeriod
            'gvPeriod.DataBind()
            'Sohail (12 Oct 2021) -- End
        End Try
    End Sub

    'Nilay (23-Aug-2016) -- Start
    'ENHANCEMENT : Search option on payslip in MSS
    'Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
    '    Try
    '        Dim cb As CheckBox
    '        For Each gvr As GridViewRow In gvEmployee.Rows
    '            If CInt(mdtEmployee.Rows(gvr.RowIndex).Item("employeeunkid")) > 0 AndAlso mdtEmployee.Rows(gvr.RowIndex).Item("employeename").ToString <> "" Then
    '                cb = CType(gvEmployee.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
    '                cb.Checked = blnCheckAll

    '                mdtEmployee.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
    '            End If
    '        Next

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
    '        If chkSelectAll Is Nothing Then Exit Try

    '        Call CheckAllEmployee(chkSelectAll.Checked)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
    '        If chkSelect Is Nothing Then Exit Try

    '        Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            mdtEmployee.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (23-Aug-2016) -- END


    'Sohail (12 Oct 2021) -- Start
    'NMB Issue :  : Performance issue on payslip report in new ui.
    'Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
    '    Try
    '        Dim cb As CheckBox
    '        For Each gvr As GridViewRow In gvPeriod.Rows
    '            If CInt(mdtPeriod.Rows(gvr.RowIndex).Item("periodunkid")) > 0 AndAlso mdtPeriod.Rows(gvr.RowIndex).Item("period_name").ToString <> "" Then
    '                cb = CType(gvPeriod.Rows(gvr.RowIndex).FindControl("chkSelectPeriod"), CheckBox)
    '                cb.Checked = blnCheckAll

    '                mdtPeriod.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
    '            End If
    '        Next

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelectAllPeriod_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try

    '        Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
    '        If chkSelectAll Is Nothing Then Exit Try

    '        Call CheckAllPeriod(chkSelectAll.Checked)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelectPeriod_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
    '        If chkSelect Is Nothing Then Exit Try

    '        Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            mdtPeriod.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Sohail (12 Oct 2021) -- End
    'Sohail (18 May 2013) -- End

    Private Function IsValidate() As Boolean
        'Sohail (11 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objPeriod As New clscommom_period_Tran
        Dim objPayment As New clsPayment_tran
        'Sohail (11 Sep 2012) -- End
        Try
            If drpPeriod.SelectedValue = "" OrElse CInt(drpPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Period."), Me)
                Return False
            End If

            'Sohail (11 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedItem.Value)
            If objPeriod._Statusid <> 2 Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim ds As DataSet = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(drpPeriod.SelectedItem.Value), clsPayment_tran.enPayTypeId.PAYMENT, CInt(Session("Employeeunkid")), " AND 1 = 1")

                'Nilay (03-Feb-2016) -- Start
                'Accessfilter paramter problem
                'Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name"), _
                '                                               Session("UserId"), _
                '                                               Session("Fin_year"), _
                '                                               Session("CompanyUnkId"), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               Session("UserAccessModeSetting"), True, _
                '                                               Session("IsIncludeInactiveEmp"), "Payment", _
                '                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                               CInt(drpPeriod.SelectedItem.Value), _
                '                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                               CInt(Session("Employeeunkid")), True, " AND 1 = 1")

                Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               chkInactiveemp.Checked, "Payment", _
                                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                               CInt(drpPeriod.SelectedItem.Value), _
                                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                                                               CInt(Session("Employeeunkid")).ToString, False)
                'Nilay (03-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If ds.Tables("Payment").Rows.Count <= 0 Then
                    'Sohail (15 May 2020) -- Start
                    'NMB Enhancement # : Option Don't Show Payslip On ESS If Payment is Not Done on aruti configuration.
                    If CBool(Session("DontShowPayslipOnESSIfPaymentNotDone")) = True Then
                        'Sohail (15 May 2020) -- End
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry! Payment is not done for the selected period."), Me)
                        Return False
                    End If 'Sohail (15 May 2020)
                    'Sohail (29 Mar 2017) -- Start
                    'CCK Enhancement - 65.1 - Payslip to be accessible on ess only after payment authorisation. Provide setting on configuration as earlier agreed. 
                ElseIf CBool(Session("DontShowPayslipOnESSIfPaymentNotAuthorized")) = True AndAlso CBool(ds.Tables("Payment").Rows(0).Item("isauthorized")) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry! Payment is not Authorized for the selected period."), Me)
                    Return False
                    'Sohail (29 Mar 2017) -- End
                End If
            End If

            'Sohail (11 Sep 2012) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private Function IsValidMSS() As Boolean
        Try
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            Dim gRowPeriod As IEnumerable(Of GridViewRow) = Nothing

            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            gRowPeriod = gvPeriod.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelectPeriod"), CheckBox).Checked = True)
            'Sohail (12 Oct 2021) -- End

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'If mdtEmployee.Select("IsChecked = 1").Length <= 0 Then
            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                'Sohail (12 Oct 2021) -- End

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select atleast one Employee."), Me)
                Return False
                'Sohail (12 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on payslip report in new ui.
                'ElseIf mdtPeriod.Select("IsChecked = 1").Length <= 0 Then
            ElseIf gRowPeriod Is Nothing OrElse gRowPeriod.Count <= 0 Then
                'Sohail (12 Oct 2021) -- End
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select atleast one Period."), Me)
                Return False
            End If


            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidMSS:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
    'Sohail (18 May 2013) -- End

    'Anjan [01 September 2016] -- Start
    'ENHANCEMENT : Including Payslip settings on configuration.
    Private Sub ResetValue()
        Try
            chkShowBankAccountNo.Checked = CBool(Session("ShowBankAccountNoOnPayslip"))
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [01 Sepetember 2016] -- End



#End Region

#Region " Page's Event "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'ViewState.Add("mdtEmployee", mdtEmployee)
            'ViewState.Add("mdtPeriod", mdtPeriod)
            'Sohail (12 Oct 2021) -- End

            'Varsha Rana (29-Sept-2017) -- Start
            'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            'Varsha Rana (29-Sept-2017) -- End

            'S.SANDEEP |22-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
            Me.ViewState("mblnFromDashBoard") = mblnFromDashBoard
            'S.SANDEEP |22-JUN-2021| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            If Me.IsPostBack = False Then


                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                SetLanguage()
                'Anjan [04 June 2014] -- End

                'S.SANDEEP |22-JUN-2021| -- START
                'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
                If Session("IsFromDashBoard") IsNot Nothing Then
                    mblnFromDashBoard = CBool(Session("IsFromDashBoard"))
                End If
                If mblnFromDashBoard Then
                    btnSearch.Visible = False
                End If
                'S.SANDEEP |22-JUN-2021| -- END

                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    pnlESS.Visible = False
                    pnlMSS.Visible = True
                Else
                    pnlESS.Visible = True
                    pnlMSS.Visible = False
                End If
                'Sohail (18 May 2013) -- End

                'Sohail (19 Sep 2013) -- Start
                'TRA - ENHANCEMENT
                Select Case CInt(Session("PayslipTemplate"))
                    Case enPayslipTemplate.TAMA_6, enPayslipTemplate.GHANA_9, enPayslipTemplate.IBA_16, enPayslipTemplate.INDONESIA_11, enPayslipTemplate.SIC_18
                        'Sohail (15 Jan 2019) - [GHANA_9, IBA_16, INDONESIA_11, SIC_18]
                        chkShowTwoPayslipPerPage.Visible = True
                    Case enPayslipTemplate.VOLTAMP_7
                        chkShowTwoPayslipPerPage.Visible = True
                        chkShowTwoPayslipPerPage.Checked = True
                        chkShowEachPayslipOnNewPage.Checked = False
                    Case Else
                        chkShowTwoPayslipPerPage.Visible = False
                        chkShowTwoPayslipPerPage.Checked = False
                End Select
                'Sohail (19 Sep 2013) -- End

                'Sohail (11 Nov 2014) -- Start
                'AUMSG Enhancement - Currency option on Payslip Report.
                If CInt(Session("PayslipTemplate")) = enPayslipTemplate.FINCA_4 OrElse CInt(Session("PayslipTemplate")) = enPayslipTemplate.FINCA_DRC_10 OrElse CInt(Session("PayslipTemplate")) = enPayslipTemplate.TRA_2 Then
                    lblCurrency.Visible = False
                    cboCurrency.Visible = False
                Else
                    lblCurrency.Visible = True
                    cboCurrency.Visible = True
                End If
                'Sohail (11 Nov 2014) -- End

                'Nilay (03-Oct-2016) -- Start
                'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
                'Sohail (15 Jan 2019) -- Start
                'SIC Enhancement - 76.1 - New Payslip Template 18 same as Template 11 with Calibri fonts and Size 11 and 2 payslip per page option.
                'If CInt(Session("PayslipTemplate")) = enPayslipTemplate.IBA_16 Then
                '    chkShowTwoPayslipPerPage.Visible = True
                'End If
                'Sohail (15 Jan 2019) -- End
                'Nilay (03-Oct-2016) -- End

                Call FillCombo()
                'S.SANDEEP |22-JUN-2021| -- START
                'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
                'Call CreateEmpListTable()
                'S.SANDEEP |22-JUN-2021| -- END
                Call FillEmpList()
                'Call CreatePeriodListTable() 'Sohail (12 Oct 2021)
                Call FillPeriodList()
                Call ResetValue()
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT

            Else
                'Sohail (12 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on payslip report in new ui.
                'mdtEmployee = CType(ViewState("mdtEmployee"), DataTable)
                'mdtPeriod = CType(ViewState("mdtPeriod"), DataTable)
                'Sohail (12 Oct 2021) -- End
                'Sohail (18 May 2013) -- End

                'Varsha Rana (29-Sept-2017) -- Start
                'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                'Varsha Rana (29-Sept-2017) -- End

                'S.SANDEEP |22-JUN-2021| -- START
                'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
                If Me.ViewState("mblnFromDashBoard") IsNot Nothing Then
                    mblnFromDashBoard = CBool(Me.ViewState("mblnFromDashBoard"))
                End If
                'S.SANDEEP |22-JUN-2021| -- END

            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'S.SANDEEP |22-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
    'Sandeep |09-DEC-2021| -- START
    'Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
    '    Try
    '        If Session("IsFromDashBoard") IsNot Nothing Then
    '            Session.Remove("IsFromDashBoard")
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Sandeep |09-DEC-2021| -- END

    'S.SANDEEP |22-JUN-2021| -- END

#End Region

    'Sohail (16 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Protected Sub drpFinancialYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpFinancialYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData 'Sohail (11 Sep 2012)
        Dim dsCombo As DataSet
        Try
            Dim strDatabaseName As String = ""
            Dim objCompany As New clsCompany_Master
            dsCombo = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List", CInt(drpFinancialYear.SelectedItem.Value))
            If dsCombo.Tables("List").Rows.Count > 0 Then
                strDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
            End If

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(drpFinancialYear.SelectedItem.Value), "Period", True, 2, , , strDatabaseName)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(drpFinancialYear.SelectedItem.Value), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "Period", True, 2)
            'Shani(20-Nov-2015) -- End

            'Sohail (11 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(drpFinancialYear.SelectedItem.Value), 1)
            If intFirstOpenPeriod > 0 Then
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("periodunkid = " & intFirstOpenPeriod & " ")
                If drRow.Length <= 0 Then
                    Dim dRow As DataRow = dsCombo.Tables(0).NewRow

                    objPeriod._Periodunkid(Session("Database_Name").ToString) = intFirstOpenPeriod

                    dRow.Item("periodunkid") = intFirstOpenPeriod
                    dRow.Item("name") = objPeriod._Period_Name
                    dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                    dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)

                    dsCombo.Tables(0).Rows.Add(dRow)
                End If
            End If
            'Sohail (11 Sep 2012) -- End
            With drpPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                'Sohail (25 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = intFirstOpenPeriod.ToString
                'Sohail (25 Mar 2013) -- End
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpFinancialYear_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Sohail (11 Sep 2012) -- Start
            'TRA - ENHANCEMENT
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            'Sohail (11 Sep 2012) -- End
        End Try
    End Sub
#End Region
    'Sohail (16 Jul 2012) -- End

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click, Closebutton2.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click,Closebutton1.CloseButton_click
    'Shani [ 24 DEC 2014 ] -- END
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            If IsValidate() = False Then
                Exit Sub
            End If


            'Pinkal (12-Mar-2013) -- Start
            'Enhancement : TRA Changes

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)

            If CBool(Session("AllowToviewPaysliponEss")) And objPeriod._Statusid = 1 And objPeriod._End_Date.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then

                If DateDiff(DateInterval.Day, ConfigParameter._Object._CurrentDateAndTime.Date, objPeriod._End_Date.Date) > CInt(Session("ViewPayslipDaysBefore")) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "You can not see your payslip now, Reason: As per the Company Policy, you will able to see your payslip on") & " " & objPeriod._End_Date.AddDays(CInt(Session("ViewPayslipDaysBefore")) * -1).Date & ".", Me)
                    Exit Sub
                End If

            End If

            'Pinkal (12-Mar-2013) -- End


            Dim objEmpPaySlip As New clsPaySlip(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            objEmpPaySlip.SetDefaultValue() 'Sohail (07 Nov 2014)

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            Dim strDatabaseName As String = ""
            'Sohail (07 Dec 2016) -- Start
            'Enhancement - 64.1 - ESS Payslip was not coming for previous years.
            Dim intFinYearId As Integer = 0
            Dim strDatabaseStartDate As String = ""
            Dim strDatabaseEndDate As String = ""
            'Sohail (07 Dec 2016) -- End
            'Sohail (15 Jan 2019) -- Start
            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
            Dim blnIsClosed As Boolean = False
            'Sohail (15 Jan 2019) -- End
            Dim objCompany As New clsCompany_Master
            Dim dsCombo As DataSet = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List", CInt(drpFinancialYear.SelectedItem.Value))
            If dsCombo.Tables("List").Rows.Count > 0 Then
                strDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
                'Sohail (07 Dec 2016) -- Start
                'Enhancement - 64.1 - ESS Payslip was not coming for previous years.
                intFinYearId = CInt(dsCombo.Tables("List").Rows(0).Item("yearunkid").ToString)
                strDatabaseStartDate = dsCombo.Tables("List").Rows(0).Item("start_date").ToString
                strDatabaseEndDate = dsCombo.Tables("List").Rows(0).Item("end_date").ToString
                'Sohail (07 Dec 2016) -- End
                'Sohail (15 Jan 2019) -- Start
                'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
                blnIsClosed = CBool(dsCombo.Tables("List").Rows(0).Item("isclosed"))
                'Sohail (15 Jan 2019) -- End
            End If
            objEmpPaySlip._TranDatabaseName = strDatabaseName
            'Sohail (16 Jul 2012) -- End

            objEmpPaySlip._EmployeeIds = CInt(Session("Employeeunkid")).ToString
            objEmpPaySlip._PeriodIds = drpPeriod.SelectedValue.ToString
            objEmpPaySlip._AnalysisRefId = enAnalysisRef.Employee
            objEmpPaySlip._InternalUseMessage = "For Internal Use Only"

            objEmpPaySlip._IgnoreZeroValueHead = CBool(Session("IgnoreZeroValueHeadsOnPayslip"))
            objEmpPaySlip._ShowLoanBalance = CBool(Session("ShowLoanBalanceOnPayslip"))
            objEmpPaySlip._ShowSavingBalance = CBool(Session("ShowSavingBalanceOnPayslip"))
            objEmpPaySlip._ShowEmployerContribution = CBool(Session("ShowEmployerContributionOnPayslip"))
            objEmpPaySlip._ShowAllHeads = CBool(Session("ShowAllHeadsOnPayslip"))
            objEmpPaySlip._LeaveTypeId = CInt(Session("LeaveTypeOnPayslip"))
            objEmpPaySlip._ShowCumulativeAccrual = CBool(Session("ShowCumulativeAccrual"))
            objEmpPaySlip._ShowCategory = CBool(Session("ShowCategoryOnPayslip"))  'Sohail (31 Aug 2013)
            objEmpPaySlip._ShowInformationalHeads = CBool(Session("ShowInformationalHeadsOnPayslip")) 'Sohail (19 Sep 2013)
            'Sohail (23 Dec 2013) -- Start
            'Enhancement - Oman
            objEmpPaySlip._ShowBirthDate = CBool(Session("ShowBirthDateOnPayslip"))
            objEmpPaySlip._ShowAge = CBool(Session("ShowAgeOnPayslip"))
            objEmpPaySlip._ShowNo_of_Dependents = CBool(Session("ShowNoOfDependantsOnPayslip"))
            objEmpPaySlip._ShowMonthlySalary = CBool(Session("ShowMonthlySalaryOnPayslip"))
            'Sohail (23 Dec 2013) -- End


            'Anjan [01 September 2016] -- Start
            'ENHANCEMENT : Including Payslip settings on configuration.
            objEmpPaySlip._ShowBankAccountNo = CBool(Session("ShowBankAccountNoOnPayslip"))
            objEmpPaySlip.setDefaultOrderBy(0)
            'Anjan [01 Sepetember 2016] -- End

            'Hemant (23 Dec 2020) -- Start
            'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
            objEmpPaySlip._ShowRemainingNoOfLoanInstallments = CBool(Session("ShowRemainingNoOfLoanInstallmentsOnPayslip"))
            'Hemant (23 Dec 2020) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            objEmpPaySlip._OT1HourHeadId = CInt(Session("OT1HourHeadID"))
            objEmpPaySlip._OT1HourHeadName = Session("OT1HourHeadName").ToString

            objEmpPaySlip._OT1AmountHeadId = CInt(Session("OT1AMOUNTHEADID"))
            objEmpPaySlip._OT1AmountHeadName = Session("OT1AmountHeadName").ToString

            objEmpPaySlip._OT2HourHeadId = CInt(Session("OT2HourHeadID"))
            objEmpPaySlip._OT2HourHeadName = Session("OT2HourHeadName").ToString

            objEmpPaySlip._OT2AmountHeadId = CInt(Session("OT2AMOUNTHEADID"))
            objEmpPaySlip._OT2AmountHeadName = Session("OT2AmountHeadName").ToString

            objEmpPaySlip._OT3HourHeadId = CInt(Session("OT3HourHeadID"))
            objEmpPaySlip._OT3HourHeadName = Session("OT3HourHeadName").ToString

            objEmpPaySlip._OT3AmountHeadId = CInt(Session("OT3AMOUNTHEADID"))
            objEmpPaySlip._OT3AmountHeadName = Session("OT3AmountHeadName").ToString

            objEmpPaySlip._OT4HourHeadId = CInt(Session("OT4HourHeadID"))
            objEmpPaySlip._OT4HourHeadName = Session("OT4HourHeadName").ToString

            objEmpPaySlip._OT4AmountHeadId = CInt(Session("OT4AMOUNTHEADID"))
            objEmpPaySlip._OT4AmountHeadName = Session("OT4AmountHeadName").ToString
            'Sohail (09 Jan 2014) -- End
            objEmpPaySlip._ShowSalaryOnHold = CBool(Session("ShowSalaryOnHoldOnPayslip")) 'Sohail (24 Feb 2014)

            'Pinkal (12-Mar-2013) -- Start
            'Enhancement : TRA Changes

            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid = CInt(drpPeriod.SelectedValue)

            'Pinkal (12-Mar-2013) -- End

            objEmpPaySlip._PeriodEnd = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (09 Feb 2016)

            'Sohail (21 May 2015) -- Start
            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
            Dim dicPeriod As New Dictionary(Of Integer, String)
            dicPeriod.Add(CInt(drpPeriod.SelectedValue.ToString), eZeeDate.convertDate(objPeriod._End_Date))
            objEmpPaySlip._Dic_Period = dicPeriod
            'Sohail (21 May 2015) -- End

            Select Case CInt(Session("PayslipTemplate"))
                Case 2 'TRA Format
                    objEmpPaySlip._IsLogoCompanyInfo = False
                    objEmpPaySlip._IsOnlyCompanyInfo = False
                    objEmpPaySlip._IsOnlyLogo = True
                Case Else
                    Select Case CInt(Session("LogoOnPayslip"))
                        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                            objEmpPaySlip._IsLogoCompanyInfo = True
                            objEmpPaySlip._IsOnlyCompanyInfo = False
                            objEmpPaySlip._IsOnlyLogo = False
                        Case enLogoOnPayslip.COMPANY_DETAILS
                            objEmpPaySlip._IsLogoCompanyInfo = False
                            objEmpPaySlip._IsOnlyCompanyInfo = True
                            objEmpPaySlip._IsOnlyLogo = False
                        Case enLogoOnPayslip.LOGO
                            objEmpPaySlip._IsLogoCompanyInfo = False
                            objEmpPaySlip._IsOnlyCompanyInfo = False
                            objEmpPaySlip._IsOnlyLogo = True
                    End Select
            End Select

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            objEmpPaySlip._EmpSortById = enAllocation.DEPARTMENT
            objEmpPaySlip._EmpSortByDESC = False
            'Sohail (14 Jun 2013) -- End

            objEmpPaySlip._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmpPaySlip._UserUnkId = CInt(Session("UserId"))
            'Sohail (07 Dec 2016) -- Start
            'Enhancement - 64.1 - ESS Payslip was not coming for previous years.
            'objEmpPaySlip._DatabaseStartDate = CDate(Session("fin_startdate"))
            'objEmpPaySlip._DatabaseEndDate = CDate(Session("fin_enddate"))
            'objEmpPaySlip._YearUnkId = CInt(Session("Fin_year")) 'Sohail (03 Dec 2013)
            objEmpPaySlip._DatabaseStartDate = eZeeDate.convertDate(strDatabaseStartDate)
            objEmpPaySlip._DatabaseEndDate = eZeeDate.convertDate(strDatabaseEndDate)
            objEmpPaySlip._YearUnkId = intFinYearId
            'Sohail (07 Dec 2016) -- End

            GUI.fmtCurrency = Session("fmtCurrency").ToString

            'Pinkal (09-Nov-2013) -- Start
            'Enhancement : Oman Changes
            objEmpPaySlip._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            'Sohail (15 Jan 2019) -- Start
            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
            'objEmpPaySlip._IsFin_Close = CInt(Int(CBool(Session("isclosed"))))
            objEmpPaySlip._IsFin_Close = CInt(Int(blnIsClosed))
            'Sohail (15 Jan 2019) -- End
            'Pinkal (09-Nov-2013) -- End

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            objEmpPaySlip._CurrencyCountryId = CInt(cboCurrency.SelectedValue)
            objEmpPaySlip._CurrencyName = cboCurrency.SelectedItem.Text
            'Sohail (11 Nov 2014) -- End

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 58.1 changes in Web.
            objEmpPaySlip._ShowBankAccountNo = chkShowBankAccountNo.Checked
            'Sohail (21 Jan 2016) -- End

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            objEmpPaySlip._ApplyUserAccessFilter = False
            'Sohail ((13 Feb 2016) -- End

            'Nilay (23 Jan 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show Statutory Message on ESS Payslip.
            objEmpPaySlip._InternalUseMessage = CStr(Session("StatutoryMessageOnPayslipOnESS"))
            'Nilay (23 Jan 2017) -- End

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Pinkal (01-Dec-2016) -- Start
            'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
            If CInt(Session("Employeeunkid")) > 0 Then
                objEmpPaySlip._UserName = Session("DisplayName").ToString()
            End If
            'Pinkal (01-Dec-2016) -- End

            'Pinkal (23-Apr-2018) -- Start
            'Bug - forget to pass Leave Parameters to view the Leave Info in Payslip.
            objEmpPaySlip._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
            objEmpPaySlip._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            'Pinkal (23-Apr-2018) -- End

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'objEmpPaySlip.generateReport(0, enPrintAction.None, enExportAction.None)
            'Sohail (07 Dec 2016) -- Start
            'Enhancement - 64.1 - ESS Payslip was not coming for previous years.
            'objEmpPaySlip.generateReportNew(Session("Database_Name").ToString, _
            '                                           CInt(Session("UserId")), _
            '                                           CInt(Session("Fin_year")), _
            '                                           CInt(Session("CompanyUnkId")), _
            '                                           objPeriod._Start_Date, _
            '                                           objPeriod._End_Date, _
            '                                           Session("UserAccessModeSetting").ToString, True, _
            '                                           Session("ExportReportPath").ToString, _
            '                                           CBool(Session("OpenAfterExport")), _
            '                                           0, enPrintAction.None, enExportAction.None, _
            '                                           CInt(Session("Base_CurrencyId")))
            objEmpPaySlip.generateReportNew(strDatabaseName, _
                                                       CInt(Session("UserId")), _
                                                       intFinYearId, _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       Session("UserAccessModeSetting").ToString, True, _
                                                       Session("ExportReportPath").ToString, _
                                                       CBool(Session("OpenAfterExport")), _
                                                       0, enPrintAction.None, enExportAction.None, _
                                                       CInt(Session("Base_CurrencyId")))
            'Sohail (07 Dec 2016) -- End            
            'Sohail (09 Feb 2016) -- End
            Session("objRpt") = objEmpPaySlip._Rpt

            objPeriod = Nothing 'Sohail (09 Feb 2016)

            'Sohail (07 Nov 2014) -- Start
            'Enhancement - Message handling on payslip.
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            If objEmpPaySlip._Message <> "" Then
                DisplayMessage.DisplayMessage(objEmpPaySlip._Message, Me)
            End If

            If objEmpPaySlip._Rpt IsNot Nothing Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                'Shani(24-Aug-2015) -- End
            End If
            'Sohail (07 Nov 2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnPreview_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    Protected Sub btnMSSClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMSSClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Feb-2015) -- End

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Protected Sub btnMSSReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMSSReport.Click
        Dim strEmployeeId As String = ""
        Dim strPeriodId As String = ""
        Dim StartDate As Date = Nothing
        Dim EndDate As Date = Nothing

        Dim objEmpPaySlip As New clsPaySlip(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If IsValidMSS() = False Then Exit Try

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            Dim gRowPeriod As IEnumerable(Of GridViewRow) = Nothing

            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            gRowPeriod = gvPeriod.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelectPeriod"), CheckBox).Checked = True)
            'Sohail (12 Oct 2021) -- End

            objEmpPaySlip.SetDefaultValue()

            Dim strDatabaseName As String = ""
            Dim objCompany As New clsCompany_Master
            Dim dsCombo As DataSet = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List", CInt(Session("Fin_year")))
            If dsCombo.Tables("List").Rows.Count > 0 Then
                strDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
            End If
            objEmpPaySlip._TranDatabaseName = strDatabaseName

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            'For Each dtRow As DataRow In mdtEmployee.Select("IsChecked = 1 ")
            '    strEmployeeId &= "" & dtRow.Item("employeeunkid").ToString & ","
            'Next
            'strEmployeeId = strEmployeeId.Substring(0, strEmployeeId.Length - 1)
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'Dim allEmp As List(Of String) = (From p In mdtEmployee.Select("IsChecked = 1 ") Select (p.Item("employeeunkid").ToString)).ToList
            Dim allEmp As List(Of String) = (From p In gRow Select (dgvEmployee.DataKeys(p.DataItemIndex)("employeeunkid").ToString())).ToList().Distinct.ToList
            'Sohail (12 Oct 2021) -- End
            strEmployeeId = String.Join(",", allEmp.ToArray)
            'Sohail (11 Nov 2014) -- End

            'Sohail (21 May 2015) -- Start
            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
            Dim dicPeriod As New Dictionary(Of Integer, String)
            'Sohail (21 May 2015) -- End

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on payslip report in new ui.
            'For Each dtRow As DataRow In mdtPeriod.Select("IsChecked = 1 ")
            '    strPeriodId &= "" & dtRow.Item("periodunkid").ToString & ","
            '    If StartDate = Nothing Then StartDate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
            '    EndDate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
            '    dicPeriod.Add(CInt(dtRow.Item("periodunkid")), dtRow.Item("end_date").ToString) 'Sohail (21 May 2015)
            'Next
            'strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
            StartDate = (From p In gRowPeriod Select (eZeeDate.convertDate(gvPeriod.DataKeys(p.DataItemIndex)("start_date").ToString()))).FirstOrDefault
            EndDate = (From p In gRowPeriod Select (eZeeDate.convertDate(gvPeriod.DataKeys(p.DataItemIndex)("end_date").ToString()))).LastOrDefault
            dicPeriod = (From p In gRowPeriod Select (p)).ToDictionary(Function(x) CInt(gvPeriod.DataKeys(x.DataItemIndex)("periodunkid")), Function(y) gvPeriod.DataKeys(y.DataItemIndex)("end_date").ToString)
            strPeriodId = String.Join(",", (From p In gRowPeriod Select (gvPeriod.DataKeys(p.DataItemIndex)("periodunkid").ToString())).ToList().Distinct.ToArray)
            'Sohail (12 Oct 2021) -- End

            'Sohail (21 May 2015) -- Start
            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
            objEmpPaySlip._Dic_Period = dicPeriod
            'Sohail (21 May 2015) -- End

            objEmpPaySlip._EmployeeIds = strEmployeeId
            objEmpPaySlip._PeriodIds = strPeriodId

            objEmpPaySlip._PeriodStart = StartDate
            objEmpPaySlip._PeriodEnd = EndDate

            objEmpPaySlip._MembershipId = CInt(cboMembership.SelectedValue)
            objEmpPaySlip._MembershipName = cboMembership.SelectedItem.Text

            objEmpPaySlip._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objEmpPaySlip._LeaveTypeName = cboLeaveType.SelectedItem.Text

            objEmpPaySlip._AnalysisRefId = enAnalysisRef.Employee 'Default Employee Message

            objEmpPaySlip._IgnoreZeroValueHead = CBool(Session("IgnoreZeroValueHeadsOnPayslip"))
            objEmpPaySlip._ShowLoanBalance = CBool(Session("ShowLoanBalanceOnPayslip"))
            objEmpPaySlip._ShowSavingBalance = CBool(Session("ShowSavingBalanceOnPayslip"))
            objEmpPaySlip._ShowEmployerContribution = CBool(Session("ShowEmployerContributionOnPayslip"))
            objEmpPaySlip._ShowAllHeads = CBool(Session("ShowAllHeadsOnPayslip"))
            objEmpPaySlip._ShowCumulativeAccrual = CBool(Session("ShowCumulativeAccrual"))
            'Sohail (19 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            objEmpPaySlip._ShowCategory = CBool(Session("ShowCategoryOnPayslip"))
            objEmpPaySlip._ShowInformationalHeads = CBool(Session("ShowInformationalHeadsOnPayslip"))
            'Sohail (19 Sep 2013) -- End
            'Sohail (23 Dec 2013) -- Start
            'Enhancement - Oman
            objEmpPaySlip._ShowBirthDate = CBool(Session("ShowBirthDateOnPayslip"))
            objEmpPaySlip._ShowAge = CBool(Session("ShowAgeOnPayslip"))
            objEmpPaySlip._ShowNo_of_Dependents = CBool(Session("ShowNoOfDependantsOnPayslip"))
            objEmpPaySlip._ShowMonthlySalary = CBool(Session("ShowMonthlySalaryOnPayslip"))
            'Sohail (23 Dec 2013) -- End


            'Anjan [01 September 2016] -- Start
            'ENHANCEMENT : Including Payslip settings on configuration.
            objEmpPaySlip._ShowBankAccountNo = CBool(Session("ShowBankAccountNoOnPayslip"))
            objEmpPaySlip.setDefaultOrderBy(0)
            'Anjan [01 Sepetember 2016] -- End  

            'Hemant (23 Dec 2020) -- Start
            'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
            objEmpPaySlip._ShowRemainingNoOfLoanInstallments = CBool(Session("ShowRemainingNoOfLoanInstallmentsOnPayslip"))
            'Hemant (23 Dec 2020) -- End

            'Sohail (09 Jan 2014) -- Start
            'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
            objEmpPaySlip._OT1HourHeadId = CInt(Session("OT1HourHeadID"))
            objEmpPaySlip._OT1HourHeadName = Session("OT1HourHeadName").ToString

            objEmpPaySlip._OT1AmountHeadId = CInt(Session("OT1AMOUNTHEADID"))
            objEmpPaySlip._OT1AmountHeadName = Session("OT1AmountHeadName").ToString

            objEmpPaySlip._OT2HourHeadId = CInt(Session("OT2HourHeadID"))
            objEmpPaySlip._OT2HourHeadName = Session("OT2HourHeadName").ToString

            objEmpPaySlip._OT2AmountHeadId = CInt(Session("OT2AMOUNTHEADID"))
            objEmpPaySlip._OT2AmountHeadName = Session("OT2AmountHeadName").ToString

            objEmpPaySlip._OT3HourHeadId = CInt(Session("OT3HourHeadID"))
            objEmpPaySlip._OT3HourHeadName = Session("OT3HourHeadName").ToString

            objEmpPaySlip._OT3AmountHeadId = CInt(Session("OT3AMOUNTHEADID"))
            objEmpPaySlip._OT3AmountHeadName = Session("OT3AmountHeadName").ToString

            objEmpPaySlip._OT4HourHeadId = CInt(Session("OT4HourHeadID"))
            objEmpPaySlip._OT4HourHeadName = Session("OT4HourHeadName").ToString

            objEmpPaySlip._OT4AmountHeadId = CInt(Session("OT4AMOUNTHEADID"))
            objEmpPaySlip._OT4AmountHeadName = Session("OT4AmountHeadName").ToString
            'Sohail (09 Jan 2014) -- End
            objEmpPaySlip._ShowSalaryOnHold = CBool(Session("ShowSalaryOnHoldOnPayslip")) 'Sohail (24 Feb 2014)

            objEmpPaySlip._IsActive = chkInactiveemp.Checked

            objEmpPaySlip._ShowEachPayslipOnNewPage = chkShowEachPayslipOnNewPage.Checked


            Select Case CInt(Session("PayslipTemplate"))
                Case 2 'TRA Format
                    objEmpPaySlip._IsLogoCompanyInfo = False
                    objEmpPaySlip._IsOnlyCompanyInfo = False
                    objEmpPaySlip._IsOnlyLogo = True
                Case Else
                    Select Case CInt(Session("LogoOnPayslip"))
                        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                            objEmpPaySlip._IsLogoCompanyInfo = True
                            objEmpPaySlip._IsOnlyCompanyInfo = False
                            objEmpPaySlip._IsOnlyLogo = False
                        Case enLogoOnPayslip.COMPANY_DETAILS
                            objEmpPaySlip._IsLogoCompanyInfo = False
                            objEmpPaySlip._IsOnlyCompanyInfo = True
                            objEmpPaySlip._IsOnlyLogo = False
                        Case enLogoOnPayslip.LOGO
                            objEmpPaySlip._IsLogoCompanyInfo = False
                            objEmpPaySlip._IsOnlyCompanyInfo = False
                            objEmpPaySlip._IsOnlyLogo = True
                    End Select
            End Select
            '--------------------------------------------

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            objEmpPaySlip._EmpSortById = enAllocation.DEPARTMENT
            objEmpPaySlip._EmpSortByDESC = False
            'Sohail (14 Jun 2013) -- End

            'Pinkal (09-Nov-2013) -- Start
            'Enhancement : Oman Changes
            objEmpPaySlip._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            objEmpPaySlip._IsFin_Close = CInt(Int(CBool(Session("isclosed"))))
            'Pinkal (09-Nov-2013) -- End

            'Sohail (11 Nov 2014) -- Start
            'AUMSG Enhancement - Currency option on Payslip Report.
            objEmpPaySlip._CurrencyCountryId = CInt(cboCurrency.SelectedValue)
            objEmpPaySlip._CurrencyName = cboCurrency.SelectedItem.Text
            'Sohail (11 Nov 2014) -- End

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 58.1 changes in Web.
            objEmpPaySlip._ShowBankAccountNo = chkShowBankAccountNo.Checked
            'Sohail (21 Jan 2016) -- End

            'Sohail (03 Oct 2017) -- Start
            'Enhancement - 70.1 - Show loan received on payslip as information, When loan is received (paid cash or by deposited) the amount received is not reflected on Payslip. - ((RefNo: 43))
            objEmpPaySlip._ShowLoanReceived = chkShowLoanReceived.Checked
            'Sohail (03 Oct 2017) -- End

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            objEmpPaySlip._ApplyUserAccessFilter = True
            'Sohail ((13 Feb 2016) -- End

            objEmpPaySlip._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmpPaySlip._UserUnkId = CInt(Session("UserId"))
            objEmpPaySlip._DatabaseStartDate = CDate(Session("fin_startdate"))
            objEmpPaySlip._DatabaseEndDate = CDate(Session("fin_enddate"))
            objEmpPaySlip._YearUnkId = CInt(Session("Fin_year")) 'Sohail (03 Dec 2013)

            GUI.fmtCurrency = Session("fmtCurrency").ToString

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Sohail (15 Jan 2019) -- Start
            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
            objEmpPaySlip._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
            objEmpPaySlip._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            'Sohail (15 Jan 2019) -- End

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'objEmpPaySlip.generateReport(0, enPrintAction.None, enExportAction.None)
            objEmpPaySlip.generateReportNew(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            StartDate, _
                                            EndDate, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            Session("ExportReportPath").ToString, _
                                            CBool(Session("OpenAfterExport")), _
                                            0, enPrintAction.None, enExportAction.None, _
                                            CInt(Session("Base_CurrencyId")))
            'Sohail (09 Feb 2016) -- End
            Session("objRpt") = objEmpPaySlip._Rpt

            'Sohail (07 Nov 2014) -- Start
            'Enhancement - Message handling on payslip.
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            If objEmpPaySlip._Message <> "" Then
                DisplayMessage.DisplayMessage(objEmpPaySlip._Message, Me)
            End If

            If objEmpPaySlip._Rpt IsNot Nothing Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                'Shani(24-Aug-2015) -- End
            End If
            'Sohail (07 Nov 2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnMSSReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (18 May 2013) -- End


    'Varsha Rana (29-Sept-2017) -- Start
    'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillEmpList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            mstrAdvanceFilter = ""
            Call FillEmpList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Varsha Rana (29-Sept-2017) -- End
#End Region

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
#Region " GridView Events "

    'Nilay (23-Aug-2016) -- Start
    'ENHANCEMENT : Search option on payslip in MSS
    'Protected Sub gvEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmployee.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(1).Text = "" Then
    '                e.Row.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("gvEmployee_RowDataBound:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (23-Aug-2016) -- END

    Protected Sub gvPeriod_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPeriod.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Sohail (12 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on payslip report in new ui.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(1).Text = "" Then
                '    e.Row.Visible = False
                'End If
                'Sohail (12 Oct 2021) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("gvPeriod_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    'Nilay (23-Aug-2016) -- Start
    'ENHANCEMENT : Search option on payslip in MSS
#Region " DataGrid Events "
    'Sohail (12 Oct 2021) -- Start
    'NMB Issue :  : Performance issue on payslip report in new ui.
    'Protected Sub dgvEmployee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmployee.ItemDataBound
    '    Try
    '        If e.Item.ItemIndex > -1 Then
    '            If e.Item.ItemType = ListItemType.Item Then
    '                If e.Item.Cells(2).Text = "None" AndAlso e.Item.Cells(1).Text = "" Then
    '                    e.Item.Visible = False
    '                End If
    '            End If
    '            If mdtEmployee IsNot Nothing AndAlso mdtEmployee.Rows.Count > 0 Then
    '                Dim dRow() As DataRow = CType(mdtEmployee, DataTable).Select("employeeunkid = " & e.Item.Cells(1).Text & " ")
    '                If dRow.Length > 0 Then
    '                    If CBool(dRow(0).Item("IsChecked")) = True Then
    '                        CType(e.Item.Cells(0).FindControl("chkSelect"), CheckBox).Checked = CBool(dRow(0).Item("IsChecked"))
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("dgvEmployee_ItemDataBound:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    Protected Sub dgvEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvEmployee.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(dgvEmployee.DataKeys(e.Row.RowIndex).Item(0)) = False Then

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (12 Oct 2021) -- End
#End Region
    'Nilay (23-Aug-2016) -- END


    'Sohail (18 May 2013) -- End

    'Sohail (19 Sep 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Checkbox's Events "

    Protected Sub chkShowEachPayslipOnNewPage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowEachPayslipOnNewPage.CheckedChanged
        Try
            If chkShowEachPayslipOnNewPage.Checked = True Then chkShowTwoPayslipPerPage.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkShowEachPayslipOnNewPage_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkShowTwoPayslipPerPage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowTwoPayslipPerPage.CheckedChanged
        Try
            If chkShowTwoPayslipPerPage.Checked = True Then chkShowEachPayslipOnNewPage.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkShowTwoPayslipPerPage_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Protected Sub chkInactiveemp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkInactiveemp.CheckedChanged
        Try
            Call FillEmpList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    'Nilay (23-Aug-2016) -- Start
    'ENHANCEMENT : Search option on payslip in MSS
    'Sohail (12 Oct 2021) -- Start
    'NMB Issue :  : Performance issue on payslip report in new ui.
    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

    '        Dim dvEmployee As DataView = mdtEmployee.DefaultView

    '        dvEmployee.RowFilter = "employeename like '%" & txtSearchEmployee.Text.Trim & "%' "

    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvRow As DataGridItem = dgvEmployee.Items(i)
    '            CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '            Dim dRow() As DataRow = mdtEmployee.Select("employeeunkid = '" & gvRow.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("IsChecked") = chkSelectAll.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '        Next
    '        mdtEmployee = dvEmployee.Table

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

    '        Dim drRow() As DataRow = mdtEmployee.Select("employeeunkid = " & item.Cells(1).Text)
    '        If drRow.Length > 0 Then
    '            drRow(0).Item("IsChecked") = chkSelect.Checked
    '        End If
    '        mdtEmployee.AcceptChanges()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Sohail (12 Oct 2021) -- End
    'Nilay (23-Aug-2016) -- END

#End Region
    'Sohail (19 Sep 2013) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew


    'Nilay (23-Aug-2016) -- Start
    'ENHANCEMENT : Search option on payslip in MSS
#Region " TextBox Events "
    'Sohail (12 Oct 2021) -- Start
    'NMB Issue :  : Performance issue on payslip report in new ui.
    'Protected Sub txtSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmployee.TextChanged
    '    Try
    '        If mdtEmployee IsNot Nothing Then
    '            Dim dvEmployeeSearch As DataView = mdtEmployee.DefaultView
    '            If dvEmployeeSearch.Table.Rows.Count > 0 Then
    '                If txtSearchEmployee.Text.Trim.Length > 0 Then
    '                    dvEmployeeSearch.RowFilter = "employeename like '%" & txtSearchEmployee.Text.Trim & "%' "
    '                End If
    '                dgvEmployee.DataSource = dvEmployeeSearch.ToTable
    '                dgvEmployee.DataBind()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtSearchEmployee_TextChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    'Sohail (12 Oct 2021) -- End
#End Region
    'Nilay (23-Aug-2016) -- END

    'Varsha Rana (29-Sept-2017) -- Start
    'Enhancement - 70.001 - add analysis by option on salary slip reports [Allan Wesonga] - (RefNo: 66)
#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Varsha Rana (29-Sept-2017) -- End


    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebutton1.PageHeading)
            'Me.Closebutton2.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebutton2.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblMembership.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMembership.ID, Me.lblMembership.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.chkShowEachPayslipOnNewPage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEachPayslipOnNewPage.ID, Me.chkShowEachPayslipOnNewPage.Text)
            Me.chkShowTwoPayslipPerPage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowTwoPayslipPerPage.ID, Me.chkShowTwoPayslipPerPage.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.dgvEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmployee.Columns(2).FooterText, Me.dgvEmployee.Columns(2).HeaderText)
            Me.gvPeriod.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPeriod.Columns(2).FooterText, Me.gvPeriod.Columns(2).HeaderText)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.chkShowBankAccountNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowBankAccountNo.ID, Me.chkShowBankAccountNo.Text)
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End






End Class
