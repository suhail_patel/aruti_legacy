﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Payroll_wPg_BatchPostingList
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Dim objBatchPosting As clsBatchPosting
    'Dim objBatchPostingTran As clsBatchPostingTran
    'Hemant (04 Sep 2020) -- End   

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmBatchPostingList"
    Private ReadOnly mstrModuleName1 As String = "frmBatchPosting_AddEdit"
    'Anjan [04 June 2014] -- End

    'Nilay (10-Feb-2016) -- Start
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End
    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Private mdtPostingTranList As DataTable
    Private mdtPostingTran As DataTable
    Private mblnIsPosted As Boolean = False
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (24 Feb 2016) -- End
#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        'Hemant (04 Sep 2020) -- End
        Try


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True, 1, , , Session("Database_Name"))
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1, , True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True, 1)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                .SelectedValue = mintFirstOpenPeriod.ToString
                Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs)
                'Sohail (24 Feb 2016) -- End
            End With

            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period").Copy
                .DataBind()
            End With

            dsCombo = objMaster.getComboListEDBatchPostingStatus("Status", True, "" & enEDBatchPostingStatus.Posted_to_ED & ", " & enEDBatchPostingStatus.Not_Posted & "")
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Status")
                .DataBind()
                .SelectedValue = "2"
            End With

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            dsCombo = objBatchPosting.getComboList("Batch", True)
            With cboBatch
                .DataValueField = "empbatchpostingunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Batch")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, , enCalcType.FlatRate_Others, , , True, "typeof_id <> " & enTypeOf.Salary & "")
            With cboTranhead
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("TranHead")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Finally
            objPeriod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
            'Sohail (24 Feb 2016) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            objBatchPosting = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub SetAddEditVisibility()
        Try
            If CInt(Session("BatchPostingNotype")) = 1 Then
                txtBatchPostingNo.Enabled = False
            Else
                txtBatchPostingNo.Enabled = True
            End If
            btnAdd.Visible = CBool(Session("AllowToAddBatchPosting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetListVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToAddBatchPosting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Dim objEmp As New clsEmployee_Master
        Try
            If CInt(Session("BatchPostingNotype")) = 0 AndAlso txtBatchPostingNo.Text.Trim = "" Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 9, "Sorry! Batch Posting No. cannot be blank. Batch Posting No. is mandatory information."), Me)
                'Anjan [04 June 2014] -- End

                txtBatchPostingNo.Focus()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Please select period."), Me)
                'Anjan [04 June 2014] -- End

                cboPeriod.Focus()
                Return False
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'ElseIf txtEmployeeCode.Text.Trim = "" Then
                '    'Anjan [04 June 2014] -- Start
                '    'ENHANCEMENT : Implementing Language,requested by Andrew
                '    'Language.setLanguage(mstrModuleName1)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Please enter employee code."), Me)
                '    'Anjan [04 June 2014] -- End
                '    txtEmployeeCode.Focus()
                '    Return False
                'ElseIf txtTranHeadCode.Text.Trim = "" Then
                '    'Anjan [04 June 2014] -- Start
                '    'ENHANCEMENT : Implementing Language,requested by Andrew
                '    'Language.setLanguage(mstrModuleName1)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 5, "Please enter transaction head code."), Me)
                '    'Anjan [04 June 2014] -- End
                '    txtTranHeadCode.Focus()
                '    Return False
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
                'ElseIf txtBatchCode.Text.Trim = "" Then
                '    'Language.setLanguage(mstrModuleName1)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Please enter Batch code."), Me)
                '    txtBatchCode.Focus()
                '    Return False
                'Sohail (14 Nov 2017) -- End
            ElseIf txtBatchName.Text.Trim = "" Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Please enter Batch name."), Me)
                txtBatchName.Focus()
                Return False
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please select employee."), Me)
                cboEmployee.Focus()
                Return False
            ElseIf CInt(cboTranhead.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select transaction head."), Me)
                cboTranhead.Focus()
                Return False
                'Sohail (24 Feb 2016) -- End
            ElseIf txtAmount.Text = "0" Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Please enter amount."), Me)
                'Anjan [04 June 2014] -- End
                txtAmount.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidData:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub GetValue(ByVal objBatchPosting As clsBatchPosting)
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPostingTran As New clsBatchPostingTran
        'Hemant (04 Sep 2020) -- End
        Try
            txtBatchPostingNo.Text = objBatchPosting._Batchno
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            txtBatchCode.Text = objBatchPosting._BatchCode
            txtBatchName.Text = objBatchPosting._BatchName
            txtDescription.Text = objBatchPosting._Description
            'Sohail (24 Feb 2016) -- End
            If Me.ViewState("BatchPostingUnkid") Is Nothing Then
                objBatchPosting._Empbatchpostingunkid = 0
            Else
                objBatchPosting._Empbatchpostingunkid = CInt(Me.ViewState("BatchPostingUnkid"))
            End If
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'objBatchPostingTran._Empbatchpostingunkid = objBatchPosting._Empbatchpostingunkid
            'Me.ViewState.Add("PostingTran", objBatchPostingTran._DataTable)
            objBatchPostingTran.GetBatchPosting_Tran(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), objBatchPosting._Empbatchpostingunkid, True, "")
            mdtPostingTran = objBatchPostingTran._DataTable
            'Sohail (24 Feb 2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPostingTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub SetValue(ByVal objBatchPosting As clsBatchPosting)
        Try
            objBatchPosting._Empbatchpostingunkid = CInt(Me.ViewState("BatchPostingUnkid"))
            objBatchPosting._Batchno = txtBatchPostingNo.Text.Trim
            objBatchPosting._Isposted = False
            objBatchPosting._Userunkid = CInt(Session("UserId"))
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
            'objBatchPosting._BatchCode = txtBatchCode.Text.Trim
            objBatchPosting._BatchCode = txtBatchPostingNo.Text.Trim
            'Sohail (14 Nov 2017) -- End
            objBatchPosting._BatchName = txtBatchName.Text.Trim
            objBatchPosting._Description = txtDescription.Text.Trim
            'Sohail (24 Feb 2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillBatchPostingAddEdit()
        Try
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim mdtTran As DataTable = CType(Me.ViewState("PostingTran"), DataTable)

            'If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count <= 0 Then
            '    Dim drRow As DataRow = mdtTran.NewRow
            If mdtPostingTran IsNot Nothing AndAlso mdtPostingTran.Rows.Count <= 0 Then
                Dim drRow As DataRow = mdtPostingTran.NewRow
                'Sohail (24 Feb 2016) -- End
                drRow("periodname") = ""
                drRow("employeecode") = ""
                drRow("trnheadcode") = ""
                drRow("amount") = CDec(0).ToString(Session("fmtcurrency").ToString)
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'mdtTran.Rows.Add(drRow)
                mdtPostingTran.Rows.Add(drRow)
                'Sohail (24 Feb 2016) -- End
            End If

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim dvTranView As DataView = New DataView(mdtTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows)
            Dim dvTranView As DataView = New DataView(mdtPostingTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows)
            'Sohail (24 Feb 2016) -- End

            If dvTranView.Count > 1 Then
                txtBatchPostingNo.Enabled = False
                cboPeriod.Enabled = False
            Else
                txtBatchPostingNo.Enabled = True
                cboPeriod.Enabled = True
            End If

            If Me.ViewState("BatchPostingUnkid") IsNot Nothing AndAlso CInt(Me.ViewState("BatchPostingUnkid")) > 0 Then
                txtBatchPostingNo.Enabled = False
                cboPeriod.Enabled = False
            End If

            SetAddEditVisibility()

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'dgBatchPosting.DataSource = mdtTran
            dgBatchPosting.DataSource = mdtPostingTran
            'Sohail (24 Feb 2016) -- End
            dgBatchPosting.DataBind()
            ResetAddEditValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetAddEditValue()
        Try
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.Text = ""
            'txtTranHeadCode.Text = ""
            'Sohail (24 Feb 2016) -- End
            txtAmount.Text = ""
            btnAdd.Enabled = True
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.Enabled = True
            'txtTranHeadCode.Enabled = True
            cboEmployee.Enabled = True
            cboTranhead.Enabled = True
            'Sohail (24 Feb 2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        'Hemant (04 Sep 2020) -- End
        Try

            If CBool(Session("AllowToViewBatchPostingList")) = False Then Exit Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objBatchPosting.GetBatchList("BatchPosting", txtlstBatchNo.Text.Trim, CInt(cboPayPeriod.SelectedValue), CInt(cboStatus.SelectedValue), "", "premployee_batchposting_master.batchno")

            'Nilay (10-Feb-2016) -- Start
            'dsList = objBatchPosting.GetBatchList(Session("Database_Name"), _
            '                                     Session("UserId"), _
            '                                     Session("Fin_year"), _
            '                                     Session("CompanyUnkId"), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                     Session("UserAccessModeSetting"), True, _
            '                                     Session("IsIncludeInactiveEmp"), _
            '                                     "BatchPosting", True, _
            '                                     txtlstBatchNo.Text.Trim, _
            '                                     CInt(cboPayPeriod.SelectedValue), _
            '                                     CInt(cboStatus.SelectedValue), "", _
            '                                     "premployee_batchposting_master.batchno")
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'dsList = objBatchPosting.GetBatchList(CStr(Session("Database_Name")), _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), _
            '                                      mdtPayPeriodStartDate, _
            '                                      mdtPayPeriodEndDate, _
            '                                      CStr(Session("UserAccessModeSetting")), True, _
            '                                      CBool(Session("IsIncludeInactiveEmp")), _
            '                                      "BatchPosting", True, _
            '                                      txtlstBatchNo.Text.Trim, _
            '                                      CInt(cboPayPeriod.SelectedValue), _
            '                                      CInt(cboStatus.SelectedValue), "", _
            '                                      "premployee_batchposting_master.batchno")
            dsList = objBatchPosting.GetBatchList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  mdtPayPeriodStartDate, _
                                                  mdtPayPeriodEndDate, _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  CBool(Session("IsIncludeInactiveEmp")), _
                                                  "BatchPosting", True, _
                                                  CInt(cboBatch.SelectedValue), _
                                                  CInt(cboPayPeriod.SelectedValue), _
                                                  CInt(cboStatus.SelectedValue), "", _
                                                  "ISNULL(premployee_batchposting_master.batchcode, '')")
            'Sohail (24 Feb 2016) -- End
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            Dim mdtTran As DataTable = dsList.Tables(0)

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count <= 0 Then
                Dim drRow As DataRow = mdtTran.NewRow
                drRow("batchno") = ""
                drRow("PeriodName") = ""
                drRow("IsPosted") = False
                drRow("TotalAmount") = CDec(0).ToString(Session("fmtcurrency").ToString)
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                drRow("batchcode") = ""
                drRow("batchname") = ""
                drRow("user_name") = ""
                drRow("description") = ""
                'Sohail (24 Feb 2016) -- End
                mdtTran.Rows.Add(drRow)
            End If

            dgBatchPostingList.DataSource = mdtTran
            dgBatchPostingList.DataBind()

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Me.ViewState.Add("PostingTranList", dsList.Tables(0))
            mdtPostingTranList = dsList.Tables(0)
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            If IsNothing(dtTable) = False Then
                dtTable.Clear()
                dtTable = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        Dim objBatchPostingTran As New clsBatchPostingTran
        'Hemant (04 Sep 2020) -- End
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            objBatchPosting = New clsBatchPosting
            objBatchPostingTran = New clsBatchPostingTran

            If Not IsPostBack Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End


                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call FillCombo()
                GetValue(objBatchPosting)
                FillList()

                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    btnNew.Visible = CBool(Session("AllowToAddBatchPosting"))
                    btnAdd.Visible = CBool(Session("AllowToAddBatchPosting"))
                    dgBatchPostingList.Columns(0).Visible = CBool(Session("AllowToEditBatchPosting"))
                    dgBatchPostingList.Columns(1).Visible = CBool(Session("AllowToDeleteBatchPosting"))
                    dgBatchPostingList.Columns(2).Visible = CBool(Session("AllowToPostBatchPostingToED"))
                    dgBatchPostingList.Columns(3).Visible = CBool(Session("AllowToVoidBatchPostingToED")) 'Sohail (24 Feb 2016) 
                    dgBatchPosting.Columns(0).Visible = CBool(Session("AllowToEditBatchPosting"))
                    dgBatchPosting.Columns(1).Visible = CBool(Session("AllowToDeleteBatchPosting"))

                End If
                'Pinkal (12-Feb-2015) -- End

                'Nilay (10-Feb-2016) -- Start
            Else
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                mdtPayPeriodStartDate = CDate(Me.ViewState("PayPeriodStartDate"))
                mdtPayPeriodEndDate = CDate(Me.ViewState("PayPeriodEndDate"))
                'Nilay (10-Feb-2016) -- End
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                mdtPostingTranList = CType(Me.ViewState("mdtPostingTranList"), DataTable)
                mdtPostingTran = CType(Me.ViewState("mdtPostingTran"), DataTable)
                mblnIsPosted = CBool(Me.ViewState("mblnIsPosted"))
                mintFirstOpenPeriod = CInt(Me.ViewState("mintFirstOpenPeriod"))
                'Sohail (24 Feb 2016) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            objBatchPostingTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Nilay (10-Feb-2016) -- Start
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("PayPeriodStartDate") = mdtPayPeriodStartDate
            Me.ViewState("PayPeriodEndDate") = mdtPayPeriodEndDate
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            Me.ViewState("mdtPostingTranList") = mdtPostingTranList
            Me.ViewState("mdtPostingTran") = mdtPostingTran
            Me.ViewState("mblnIsPosted") = mblnIsPosted
            Me.ViewState("mintFirstOpenPeriod") = mintFirstOpenPeriod
            'Sohail (24 Feb 2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Nilay (10-Feb-2016) -- End

#End Region

#Region "Combobox Event"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (24 Feb 2016) -- Start
        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Dim objEmp As New clsEmployee_Master
        Dim dsCombo As DataSet
        'Sohail (24 Feb 2016) -- End
        Try
            'Nilay (10-Feb-2016) -- Start
            'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            'Me.ViewState.Add("PeriodStartDate", objPeriod._Start_Date.Date)
            'Me.ViewState.Add("PeriodEndDate", objPeriod._End_Date.Date)
            'popupAddEdit.Show()
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mdtPeriodStartDate = objPeriod._Start_Date.Date
                mdtPeriodEndDate = objPeriod._End_Date.Date
            Else
                mdtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            End If
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'popupAddEdit.Show()
            cboEmployee.DataSource = Nothing
            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                              CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              mdtPeriodStartDate, _
                                              mdtPeriodEndDate, _
                                              Session("UserAccessModeSetting").ToString, _
                                              True, False, "Emp", True)
            With cboEmployee
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataValueField = "employeeunkid"
                .DataSource = dsCombo.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With
            If Me.IsPostBack = True Then popupAddEdit.Show()
            'Sohail (24 Feb 2016) -- End
            objPeriod = Nothing
            'Nilay (10-Feb-2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Finally
            objPeriod = Nothing
            objEmp = Nothing
            'Sohail (24 Feb 2016) -- End
        End Try
    End Sub

    'Nilay (10-Feb-2016) -- Start
    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mdtPayPeriodStartDate = objPeriod._Start_Date.Date
                mdtPayPeriodEndDate = objPeriod._End_Date.Date
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPayPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Nilay (10-Feb-2016) -- End

#End Region

#Region "CheckBox Event"

    Protected Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
            popupPostED.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            txtBatchPostingNo.Text = ""
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.Text = ""
            cboEmployee.SelectedValue = "0"
            'Sohail (24 Feb 2016) -- End
            txtAmount.Text = "0"
            cboPeriod.SelectedValue = "0"
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtTranHeadCode.Text = ""
            cboTranhead.SelectedValue = "0"
            txtBatchCode.Text = ""
            txtBatchName.Text = ""
            txtDescription.Text = ""
            'Sohail (24 Feb 2016) -- End
            Me.ViewState("HeadId") = Nothing
            Me.ViewState("EmployeeId") = Nothing
            Me.ViewState("GUID") = Nothing
            Me.ViewState("BatchPostingUnkid") = Nothing
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'CType(Me.ViewState("PostingTran"), DataTable).Rows.Clear()
            mdtPostingTran.Rows.Clear()
            'Sohail (24 Feb 2016) -- End
            Me.ViewState("empbatchpostingtranunkid") = Nothing
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If Me.ViewState("PostingTran") IsNot Nothing Then
            '    CType(Me.ViewState("PostingTran"), DataTable).Rows.Clear()
            'End If
            If mdtPostingTran IsNot Nothing Then
                mdtPostingTran.Rows.Clear()
            End If
            'Sohail (24 Feb 2016) -- End
            FillBatchPostingAddEdit()

            btnEdit.Enabled = False
            mblnIsPosted = False 'Sohail (24 Feb 2016) 

            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If IsValidData() = False Then Exit Try
            Dim dsCombo As DataSet = Nothing

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim objEmployee As New clsEmployee_Master

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("employee", False, , , , , , , , , , , , , CDate(Me.ViewState("PeriodStartDate")).Date, CDate(Me.ViewState("PeriodEndDate")).Date, , , , , "employeecode = '" & txtEmployeeCode.Text & "'", True)
            'Dim strFilter As String = "hremployee_master.employeecode = '" & txtEmployeeCode.Text & "'"
            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    CDate(Me.ViewState("PeriodStartDate")).Date, _
            '                                    CDate(Me.ViewState("PeriodEndDate")).Date, _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "employee", False, , , , , , , , , , , , , , , strFilter)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), _
            '                                      mdtPeriodStartDate, _
            '                                      mdtPeriodEndDate, _
            '                                      CStr(Session("UserAccessModeSetting")), True, _
            '                                      False, "employee", False, , , , , , , , , , , , , , , strFilter)
            'Sohail (24 Feb 2016) -- End
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End

            'If dsCombo.Tables("employee").Rows.Count <= 0 Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    'Language.setLanguage(mstrModuleName1)
            '    DisplayMessage.DisplayError(ex, Me)
            '    'Anjan [04 June 2014] -- End
            '    txtEmployeeCode.Focus()
            '    Exit Try
            'Else
            '    Me.ViewState.Add("EmployeeId", CInt(dsCombo.Tables("employee").Rows(0).Item("employeeunkid")))
            'End If
            Me.ViewState.Add("EmployeeId", CInt(cboEmployee.SelectedValue))
            'Sohail (24 Feb 2016) -- End

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'dsCombo = Nothing
            'Dim objTranHead As New clsTransactionHead
            ''Sohail (03 Feb 2016) -- Start
            ''Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            ''dsCombo = objTranHead.getComboList("TranHead", False, , enCalcType.FlatRate_Others, , , True)
            'dsCombo = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", False, , enCalcType.FlatRate_Others, , , True)
            ''Sohail (03 Feb 2016) -- End
            'Dim dtTable As DataTable = New DataView(dsCombo.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & " AND code = '" & txtTranHeadCode.Text & "'", "", DataViewRowState.CurrentRows).ToTable

            'If dtTable.Rows.Count <= 0 Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    'Language.setLanguage(mstrModuleName1)
            '    DisplayMessage.DisplayError(ex, Me)
            '    'Anjan [04 June 2014] -- End
            '    txtTranHeadCode.Focus()
            '    Exit Try
            'Else
            '    Me.ViewState.Add("HeadId", CInt(dtTable.Rows(0).Item("tranheadunkid")))
            'End If
            Me.ViewState.Add("HeadId", CInt(cboTranhead.SelectedValue))
            'Sohail (24 Feb 2016) -- End

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim mdtTran As DataTable = CType(Me.ViewState("PostingTran"), DataTable)

            'Dim dtRow As DataRow() = mdtTran.Select("periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND employeeunkid = " & CInt(Me.ViewState("EmployeeId")) & _
            '                                                    " AND tranheadunkid = " & CInt(Me.ViewState("HeadId")) & " AND (AUD <> 'D' OR AUD IS NULL) ")
            Dim dtRow As DataRow() = mdtPostingTran.Select("periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & _
                                                    " AND tranheadunkid = " & CInt(cboTranhead.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")
            'Sohail (24 Feb 2016) -- End

            If dtRow.Length > 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 9, "Sorry! Selected Transaction Head for selected Employee is already added to the list for selected period."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            Dim dtEBRow As DataRow
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'dtEBRow = mdtTran.NewRow
            dtEBRow = mdtPostingTran.NewRow
            'Sohail (24 Feb 2016) -- End
            With dtEBRow

                .Item("empbatchpostingtranunkid") = -1
                .Item("empbatchpostingunkid") = -1
                .Item("batchno") = txtBatchPostingNo.Text.Trim
                .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                .Item("periodname") = cboPeriod.SelectedItem.Text
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                '.Item("employeeunkid") = CInt(Me.ViewState("EmployeeId"))
                '.Item("employeecode") = txtEmployeeCode.Text
                '.Item("tranheadunkid") = CInt(Me.ViewState("HeadId"))
                '.Item("trnheadcode") = txtTranHeadCode.Text
                .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                .Item("employeecode") = ""
                .Item("EmpName") = cboEmployee.SelectedItem.Text
                .Item("tranheadunkid") = CInt(cboTranhead.SelectedValue)
                .Item("trnheadcode") = ""
                .Item("TranHeadName") = cboTranhead.SelectedItem.Text
                'Sohail (24 Feb 2016) -- End
                .Item("amount") = CDec(txtAmount.Text).ToString(Session("fmtCurrency").ToString)
                .Item("isposted") = False
                .Item("enddate") = Me.ViewState("PeriodEndDate")
                .Item("userunkid") = CInt(Session("UserId"))
                dtEBRow.Item("AUD") = "A"
                dtEBRow.Item("GUID") = Guid.NewGuid().ToString
            End With

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'mdtTran.Rows.Add(dtEBRow)
            'Me.ViewState("PostingTran") = mdtTran
            mdtPostingTran.Rows.Add(dtEBRow)
            'Sohail (24 Feb 2016) -- End
            FillBatchPostingAddEdit()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If dgBatchPosting.Rows.Count > 0 Then
                Dim drTemp As DataRow() = Nothing

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'Dim mdtTran As DataTable = CType(Me.ViewState("PostingTran"), DataTable)
                'Sohail (24 Feb 2016) -- End

                If (Me.ViewState("empbatchpostingtranunkid") Is Nothing OrElse CInt(Me.ViewState("empbatchpostingtranunkid")) <= 0) AndAlso Me.ViewState("GUID") IsNot Nothing Then
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'drTemp = mdtTran.Select("GUID = '" & Me.ViewState("GUID").ToString() & "'")
                    drTemp = mdtPostingTran.Select("GUID = '" & Me.ViewState("GUID").ToString() & "'")
                    'Sohail (24 Feb 2016) -- End
                Else
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'drTemp = mdtTran.Select("empbatchpostingtranunkid = " & CInt(Me.ViewState("empbatchpostingtranunkid")))
                    drTemp = mdtPostingTran.Select("empbatchpostingtranunkid = " & CInt(Me.ViewState("empbatchpostingtranunkid")))
                    'Sohail (24 Feb 2016) -- End
                End If

                If drTemp.Length > 0 Then

                    If IsValidData() = False Then Exit Sub

                    With drTemp(0)
                        .Item("empbatchpostingtranunkid") = CInt(drTemp(0)("empbatchpostingtranunkid"))
                        .Item("empbatchpostingunkid") = CInt(Me.ViewState("BatchPostingUnkid"))
                        .Item("batchno") = txtBatchPostingNo.Text.Trim
                        .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        .Item("periodname") = cboPeriod.SelectedItem.Text
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        '.Item("employeeunkid") = CInt(Me.ViewState("EmployeeId"))
                        '.Item("employeecode") = txtEmployeeCode.Text
                        '.Item("tranheadunkid") = CInt(Me.ViewState("HeadId"))
                        '.Item("trnheadcode") = txtTranHeadCode.Text
                        .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                        .Item("employeecode") = ""
                        .Item("EmpName") = cboEmployee.SelectedItem.Text
                        .Item("tranheadunkid") = CInt(cboTranhead.SelectedValue)
                        .Item("trnheadcode") = ""
                        .Item("TranHeadName") = cboTranhead.SelectedItem.Text
                        'Sohail (24 Feb 2016) -- End
                        .Item("amount") = CDec(Format(txtAmount.Text, GUI.fmtCurrency))
                        .Item("isposted") = False
                        .Item("enddate") = Me.ViewState("PeriodEndDate")
                        .Item("userunkid") = CInt(Session("UserId"))

                        If IsDBNull(.Item("AUD")) OrElse CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .Item("GUID") = Guid.NewGuid().ToString
                        .AcceptChanges()
                    End With
                    Call FillBatchPostingAddEdit()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            btnEdit.Enabled = False
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            FillBatchPostingAddEdit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try

    End Sub

    Protected Sub btnlstReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlstReset.Click
        Try
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtlstBatchNo.Text = ""
            cboBatch.SelectedValue = "0"
            'Sohail (24 Feb 2016) -- End
            cboPayPeriod.SelectedValue = mintFirstOpenPeriod.ToString
            cboStatus.SelectedValue = "2"
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        'Hemant (04 Sep 2020) -- End
        Try

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
            'If txtBatchCode.Text.Trim = "" Then
            '    'Language.setLanguage(mstrModuleName1)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Please enter Batch code."), Me)
            '    popupAddEdit.Show()
            '    txtBatchCode.Focus()
            '    Exit Sub
            'Sohail (14 Nov 2017) -- End
            If txtBatchName.Text.Trim = "" Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Please enter Batch name."), Me)
                popupAddEdit.Show()
                txtBatchName.Focus()
                Exit Sub
            End If
            'Sohail (24 Feb 2016) -- End

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim drRow() As DataRow = CType(Me.ViewState("PostingTran"), DataTable).Select("periodname <> '' AND employeecode <> '' AND trnheadcode <> '' AND AUD <> 'D' ")
            Dim drRow() As DataRow = mdtPostingTran.Select("periodname <> '' AND EmpName <> '' AND TranHeadName <> '' AND AUD <> 'D' ")
            'Sohail (24 Feb 2016) -- End

            If drRow.Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Please add atleast one employee batch posting transaction."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                popupAddEdit.Show()
                Exit Sub
            End If

            SetValue(objBatchPosting)

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            Dim dtFinal As DataTable = New DataView(mdtPostingTran, "periodname <> '' AND EmpName <> '' AND TranHeadName <> '' ", "", DataViewRowState.CurrentRows).ToTable

            'Sohail (24 Feb 2016) -- End

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmBatchPosting_AddEdit"
            StrModuleName2 = "mnuPayroll"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Me.ViewState("BatchPostingUnkid") Is Nothing Then
            '    blnFlag = objBatchPosting.Insert(Me.ViewState("PostingTran"))
            'Else
            '    blnFlag = objBatchPosting.Update(Me.ViewState("PostingTran"))
            'End If

            If Me.ViewState("BatchPostingUnkid") Is Nothing Then

                'Nilay (10-Feb-2016) -- Start
                'blnFlag = objBatchPosting.Insert(Session("Database_Name"), _
                '              Session("UserId"), _
                '              Session("Fin_year"), _
                '              Session("CompanyUnkId"), _
                '              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '              Session("UserAccessModeSetting"), True, _
                '              Session("IsIncludeInactiveEmp"), _
                '              Me.ViewState("PostingTran"), _
                '              Session("BatchPostingNotype"), _
                '              Session("BatchPostingPrifix"), "List", )
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'blnFlag = objBatchPosting.Insert(CStr(Session("Database_Name")), _
                '                                 CInt(Session("UserId")), _
                '                                 CInt(Session("Fin_year")), _
                '                                 CInt(Session("CompanyUnkId")), _
                '                                 mdtPeriodStartDate, _
                '                                 mdtPeriodEndDate, _
                '                                 CStr(Session("UserAccessModeSetting")), True, _
                '                                 CBool(Session("IsIncludeInactiveEmp")), _
                '                                 CType(Me.ViewState("PostingTran"), DataTable), _
                '                                 CInt(Session("BatchPostingNotype")), _
                '                                 CStr(Session("BatchPostingPrifix")), "List", True)
                blnFlag = objBatchPosting.Insert(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                mdtPeriodStartDate, _
                                                mdtPeriodEndDate, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), _
                                                dtFinal, _
                                                CInt(Session("BatchPostingNotype")), _
                                                CStr(Session("BatchPostingPrifix")), "List", True)
                'Sohail (24 Feb 2016) -- End
                'Nilay (10-Feb-2016) -- End

            Else
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                If mblnIsPosted = True Then
                    'Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, you cannot Update this Batch transaction. Reason: This Batch is already Posted to Payroll."), Me)
                    Exit Sub
                End If
                'Sohail (24 Feb 2016) -- End
                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objBatchPosting.Update(Me.ViewState("PostingTran"))

                'Nilay (10-Feb-2016) -- Start
                'blnFlag = objBatchPosting.Update(Session("Database_Name"), _
                '                                 Session("UserId"), _
                '                                 Session("Fin_year"), _
                '                                 Session("CompanyUnkId"), _
                '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                 Session("UserAccessModeSetting"), True, _
                '                                 Session("IsIncludeInactiveEmp"), _
                '                                 Me.ViewState("PostingTran"), "List")
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'blnFlag = objBatchPosting.Update(CStr(Session("Database_Name")), _
                '                                 CInt(Session("UserId")), _
                '                                 CInt(Session("Fin_year")), _
                '                                 CInt(Session("CompanyUnkId")), _
                '                                 mdtPeriodStartDate, _
                '                                 mdtPeriodEndDate, _
                '                                 CStr(Session("UserAccessModeSetting")), True, _
                '                                 CBool(Session("IsIncludeInactiveEmp")), _
                '                                 CType(Me.ViewState("PostingTran"), DataTable), "List", True, "")
                blnFlag = objBatchPosting.Update(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                mdtPeriodStartDate, _
                                                mdtPeriodEndDate, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), _
                                                dtFinal, "List", True, "")
                'Sohail (24 Feb 2016) -- End
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

            End If
            'Shani(20-Nov-2015) -- End

            If blnFlag = False And objBatchPosting._Message <> "" Then
                DisplayMessage.DisplayMessage(objBatchPosting._Message, Me)
            End If
            Me.ViewState("BatchPostingUnkid") = Nothing
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'CType(Me.ViewState("PostingTran"), DataTable).Rows.Clear()
            mdtPostingTran.Rows.Clear()
            'Sohail (24 Feb 2016) -- End
            Me.ViewState("empbatchpostingtranunkid") = Nothing
            popupAddEdit.Hide()
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("BatchPostingUnkid") = Nothing
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'CType(Me.ViewState("PostingTran"), DataTable).Rows.Clear()
            mdtPostingTran.Rows.Clear()
            'Sohail (24 Feb 2016) -- End
            Me.ViewState("empbatchpostingtranunkid") = Nothing
            popupAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try
            If chkCopyPreviousEDSlab.Checked Then
                popupCopyPrevious.Show()
                Exit Try
            Else
                popupPost.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        'Hemant (04 Sep 2020) -- End
        Try
            'Hemant (20 June 2018) -- Start
            'popupDelete.Reason = ""
            'Hemant (20 June 2018) -- End
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmBatchPostingList"
            StrModuleName2 = "mnuPayroll"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim dtList As DataTable = CType(Me.ViewState("PostingTranList"), DataTable)
            'Sohail (24 Feb 2016) -- End
            Dim SrNo As Integer = CInt(Me.ViewState("SrNo"))
            objBatchPosting._Isvoid = True
            objBatchPosting._Voiduserunkid = CInt(Session("UserId"))
            objBatchPosting._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If objBatchPosting.Void(CInt(dtList.Rows(SrNo)("empbatchpostingunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason.ToString(), True, True) = True Then
            If objBatchPosting.Void(CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason.ToString(), True, True) = True Then
                'Sohail (24 Feb 2016) -- End
                FillList()
            ElseIf objBatchPosting._Message <> "" Then
                DisplayMessage.DisplayMessage(objBatchPosting._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub popupCopyPrevious_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupCopyPrevious.buttonYes_Click
        Try
            popupPost.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupPost_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupPost.buttonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        Dim objBatchPostingTran As New clsBatchPostingTran
        'Hemant (04 Sep 2020) -- End
        Try
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim dtList As DataTable = CType(Me.ViewState("PostingTranList"), DataTable)
            'Sohail (24 Feb 2016) -- End
            Dim SrNo As Integer = CInt(Me.ViewState("SrNo"))

            Dim objPeriod As New clscommom_period_Tran
            Dim objED As New clsEarningDeduction
            Dim dtTable As DataTable = Nothing

            Dim mdtED As DataTable = objED._DataSource
            Dim dRow As DataRow
            mdtED.Rows.Clear()

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'objBatchPostingTran._Empbatchpostingunkid = CInt(dtList.Rows(SrNo)("empbatchpostingunkid"))
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(mdtPostingTranList.Rows(SrNo)("periodunkid"))
            objBatchPostingTran.GetBatchPosting_Tran(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), False, "")
            'Sohail (24 Feb 2016) -- End
            dtTable = objBatchPostingTran._DataTable

            For Each dtRow As DataRow In dtTable.Rows
                dRow = mdtED.NewRow
                dRow.Item("edunkid") = -1
                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                'dRow.Item("trnheadname") = ""
                dRow.Item("trnheadname") = dtRow.Item("TranHeadName").ToString
                dRow.Item("calctype_id") = CInt(dtRow.Item("calctype_id"))
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("employeename") = dtRow.Item("EmpName").ToString
                'Sohail (31 Oct 2019) -- End
                dRow.Item("batchtransactionunkid") = -1
                dRow.Item("amount") = CDec(dtRow.Item("amount"))
                dRow.Item("currencyid") = 0
                dRow.Item("vendorid") = 0
                dRow.Item("userunkid") = Session("UserId")
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("membership_categoryunkid") = 0
                If CBool(Session("AllowToApproveEarningDeduction")) = True Then
                    dRow.Item("isapproved") = True
                    dRow.Item("approveruserunkid") = Session("UserId")
                Else
                    dRow.Item("isapproved") = False
                    dRow.Item("approveruserunkid") = -1
                End If

                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtRow.Item("periodunkid"))
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
                dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)

                dRow.Item("costcenterunkid") = 0
                dRow.Item("medicalrefno") = ""

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                dRow.Item("membershiptranunkid") = 0
                dRow.Item("disciplinefileunkid") = 0
                dRow.Item("cumulative_startdate") = DBNull.Value
                dRow.Item("stop_date") = DBNull.Value
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                dRow.Item("edstart_date") = DBNull.Value
                'Sohail (24 Aug 2019) -- End
                dRow.Item("empbatchpostingunkid") = CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")) 'Sohail (24 Feb 2016)

                dRow.Item("AUD") = "A"

                mdtED.Rows.Add(dRow)
            Next


            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmBatchPostingList"
            StrModuleName2 = "mnuPayroll"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))


            If mdtED.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False

                Blank_ModuleName()
                objED._WebFormName = "frmBatchPostingList"
                StrModuleName2 = "mnuPayroll"
                objED._WebClientIP = CStr(Session("IP_ADD"))
                objED._WebHostName = CStr(Session("HOST_NAME"))


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objED.InsertAllByDataTable(mdtED, chkOverwriteIfExist.Checked, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, CInt(Session("UserId")))

                'Nilay (10-Feb-2016) -- Start
                'blnFlag = objED.InsertAllByDataTable(Session("Database_Name"), _
                '                                    Session("Fin_year"), _
                '                                    Session("CompanyUnkId"), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                    Session("UserAccessModeSetting"), True, _
                '                                    Session("IsIncludeInactiveEmp"), "List", _
                '                                    mdtED, chkOverwriteIfExist.Checked, _
                '                                    Session("UserId"), True, _
                '                                    ConfigParameter._Object._CurrentDateAndTime, _
                '                                    chkCopyPreviousEDSlab.Checked, _
                '                                    chkOverwritePrevEDSlabHeads.Checked)
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'blnFlag = objED.InsertAllByDataTable(Session("Database_Name").ToString, _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    mdtPayPeriodStartDate, _
                '                                    mdtPayPeriodEndDate, _
                '                                    Session("UserAccessModeSetting").ToString, True, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "List", _
                '                                    mdtED, chkOverwriteIfExist.Checked, _
                '                                    CInt(Session("UserId")), True, _
                '                                    ConfigParameter._Object._CurrentDateAndTime, True, _
                '                                     chkCopyPreviousEDSlab.Checked, _
                '                                    chkOverwritePrevEDSlabHeads.Checked, _
                '                                     True, "")
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Gloab Assign ED screen in 75.1.
                'blnFlag = objED.InsertAllByDataTable(Session("Database_Name").ToString, _
                '                                     CInt(Session("Fin_year")), _
                '                                     CInt(Session("CompanyUnkId")), _
                '                                     objPeriod._Start_Date, _
                '                                     objPeriod._End_Date, _
                '                                     Session("UserAccessModeSetting").ToString, True, _
                '                                     CBool(Session("IsIncludeInactiveEmp")), _
                '                                     mdtED, chkOverwriteIfExist.Checked, _
                '                                     CInt(Session("UserId")), CBool(Session("AllowToApproveEarningDeduction")), _
                '                                     ConfigParameter._Object._CurrentDateAndTime, True, _
                '                                     chkCopyPreviousEDSlab.Checked, _
                '                                     chkOverwritePrevEDSlabHeads.Checked, _
                '                                     True, "")
                blnFlag = objED.InsertAllByDataTable(Session("Database_Name").ToString, _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     objPeriod._Start_Date, _
                                                     objPeriod._End_Date, _
                                                     Session("UserAccessModeSetting").ToString, True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                     mdtED, chkOverwriteIfExist.Checked, _
                                                     CInt(Session("UserId")), CBool(Session("AllowToApproveEarningDeduction")), _
                                                     ConfigParameter._Object._CurrentDateAndTime, Nothing, True, _
                                                     chkCopyPreviousEDSlab.Checked, _
                                                     chkOverwritePrevEDSlabHeads.Checked, _
                                                     True, "", True)
                'Sohail (25 Jun 2020) - [blnSkipInsertIfExist]
                'Sohail (15 Dec 2018) -- End
                'Sohail (24 Feb 2016) -- End
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If blnFlag = False And objED._Message <> "" Then
                    DisplayMessage.DisplayMessage(objED._Message, Me)
                    Exit Try
                End If

                If blnFlag Then
                    blnFlag = False
                    objBatchPosting._Userunkid = CInt(Session("UserId"))
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'blnFlag = objBatchPosting.UpdateStatus(CInt(dtList.Rows(SrNo)("empbatchpostingunkid")), True, True, True)
                    blnFlag = objBatchPosting.UpdateStatus(CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), True, True, True)
                    'Sohail (24 Feb 2016) -- End

                    If blnFlag = False And objBatchPosting._Message <> "" Then
                        DisplayMessage.DisplayMessage(objBatchPosting._Message, Me)
                        Exit Try
                    End If

                    'Sohail (31 Oct 2019) -- Start
                    'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                    If blnFlag = True Then
                        If CBool(Session("AllowToApproveEarningDeduction")) = False AndAlso chkOverwriteIfExist.Checked = True Then
                            If mdtED.Columns.Contains("trnheadname") = True Then
                                mdtED.Columns("trnheadname").ColumnName = "tranheadname"
                            End If
                            Call objED.SendMailToApprover(Session("Database_Name").ToString, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), CInt(Session("UserId")), objPeriod._Start_Date, objPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, False, CBool(Session("AllowToApproveEarningDeduction")), mdtED, CInt(mdtPostingTranList.Rows(SrNo)("periodunkid")), Session("fmtcurrency").ToString, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                        End If
                    End If
                    'Sohail (31 Oct 2019) -- End

                    If blnFlag Then
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName)
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Posting to ED Process completed successfully."), Me)
                        'Sohail (23 Mar 2019) -- End
                        'Anjan [04 June 2014] -- End
                        Call FillList()
                    End If

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            objBatchPostingTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Protected Sub popupVoidBatchConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupVoidBatchConfirm.buttonYes_Click
        Try
            popupVoidBatchPosting.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupVoidBatchPosting_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupVoidBatchPosting.buttonDelReasonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        'Hemant (04 Sep 2020) -- End
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("SrNo"))

            Dim objED As New clsEarningDeduction

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmBatchPostingList"
            StrModuleName2 = "mnuPayroll"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))


            Dim blnFlag As Boolean = False

            Blank_ModuleName()
            objED._WebFormName = "frmBatchPostingList"
            StrModuleName2 = "mnuPayroll"
            objED._WebClientIP = CStr(Session("IP_ADD"))
            objED._WebHostName = CStr(Session("HOST_NAME"))

            blnFlag = objED.VoidEDbyBatchPostingUnkID(Session("Database_Name").ToString, CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), CInt(mdtPostingTranList.Rows(SrNo)("periodunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupVoidBatchPosting.Reason.ToString, CStr(Session("IP_ADD")), CStr(Session("HOST_NAME")), -1, "frmBatchPostingList", "", "mnuPayroll", "", "", "")

            If blnFlag = False And objED._Message <> "" Then
                DisplayMessage.DisplayMessage(objED._Message, Me)
                Exit Try
            End If

            If blnFlag Then
                blnFlag = False
                objBatchPosting._Userunkid = CInt(Session("UserId"))
                blnFlag = objBatchPosting.UpdateStatus(CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), False, False, True, True)

                If blnFlag = False And objBatchPosting._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBatchPosting._Message, Me)
                    Exit Try
                End If

                If blnFlag Then
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Void Batch Posting to ED Process completed successfully."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Call FillList()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub
    'Sohail (24 Feb 2016) -- End
#End Region

#Region "DataGrid Event"

#Region "   Add/Edit Page GridView"

    Protected Sub dgBatchPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgBatchPosting.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim mdtTran As DataTable = CType(Me.ViewState("PostingTran"), DataTable)
            'Sohail (24 Feb 2016) -- End

            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(2).Text = "&nbsp;" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                    e.Row.Visible = False
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'ElseIf mdtTran IsNot Nothing AndAlso mdtTran.Rows(e.Row.RowIndex)("AUD").ToString = "D" Then
                ElseIf mdtPostingTran IsNot Nothing AndAlso mdtPostingTran.Rows(e.Row.RowIndex)("AUD").ToString = "D" Then
                    'Sohail (24 Feb 2016) -- End
                    e.Row.Visible = False
                Else
                    e.Row.Cells(5).Text = CDec(e.Row.Cells(5).Text).ToString(Session("fmtcurrency").ToString)
                End If
                dgBatchPosting.Columns(0).Visible = CBool(Session("AllowToEditBatchPosting"))
                dgBatchPosting.Columns(1).Visible = CBool(Session("AllowToDeleteBatchPosting"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgBatchPosting_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgBatchPosting.PageIndexChanging
        Try
            dgBatchPosting.PageIndex = e.NewPageIndex
            FillBatchPostingAddEdit()
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgBatchPosting_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgBatchPosting.RowCommand
        Try

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim mdtTran As DataTable = CType(Me.ViewState("PostingTran"), DataTable)
            'Sohail (24 Feb 2016) -- End
            Dim SrNo As Integer = CInt(e.CommandArgument)

            If e.CommandName = "Change" Then

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'If mdtTran.Rows.Count > 0 Then
                If mdtPostingTran.Rows.Count > 0 Then
                    'Sohail (24 Feb 2016) -- End

                    btnEdit.Enabled = True

                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'cboPeriod.SelectedValue = CStr(mdtTran.Rows(SrNo)("periodunkid"))
                    'txtEmployeeCode.Text = mdtTran.Rows(SrNo)("employeecode").ToString()
                    'txtTranHeadCode.Text = mdtTran.Rows(SrNo)("trnheadcode").ToString()
                    'txtAmount.Text = mdtTran.Rows(SrNo)("amount").ToString()
                    cboPeriod.SelectedValue = CStr(mdtPostingTran.Rows(SrNo)("periodunkid"))
                    cboEmployee.SelectedValue = mdtPostingTran.Rows(SrNo).Item("employeeunkid").ToString
                    cboTranhead.SelectedValue = mdtPostingTran.Rows(SrNo).Item("tranheadunkid").ToString
                    txtAmount.Text = mdtPostingTran.Rows(SrNo)("amount").ToString()
                    'Sohail (24 Feb 2016) -- End

                    If Me.ViewState("GUID") Is Nothing Then
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        'Me.ViewState.Add("GUID", mdtTran.Rows(SrNo)("GUID").ToString())
                        'Me.ViewState.Add("empbatchpostingtranunkid", mdtTran.Rows(SrNo)("empbatchpostingtranunkid").ToString())
                        Me.ViewState.Add("GUID", mdtPostingTran.Rows(SrNo)("GUID").ToString())
                        Me.ViewState.Add("empbatchpostingtranunkid", mdtPostingTran.Rows(SrNo)("empbatchpostingtranunkid").ToString())
                        'Sohail (24 Feb 2016) -- End
                    Else
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        'Me.ViewState("GUID") = mdtTran.Rows(SrNo)("GUID").ToString()
                        'Me.ViewState("empbatchpostingtranunkid") = mdtTran.Rows(SrNo)("empbatchpostingtranunkid").ToString()
                        Me.ViewState("GUID") = mdtPostingTran.Rows(SrNo)("GUID").ToString()
                        Me.ViewState("empbatchpostingtranunkid") = mdtPostingTran.Rows(SrNo)("empbatchpostingtranunkid").ToString()
                        'Sohail (24 Feb 2016) -- End
                    End If

                    btnAdd.Enabled = False
                    cboPeriod.Enabled = False
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'txtEmployeeCode.Enabled = False
                    'txtTranHeadCode.Enabled = False
                    cboEmployee.Enabled = False
                    cboTranhead.Enabled = False
                    'Sohail (24 Feb 2016) -- End

                End If

            ElseIf e.CommandName = "Remove" Then
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'If mdtTran.Rows.Count > 0 Then
                If mdtPostingTran.Rows.Count > 0 Then
                    'Sohail (24 Feb 2016) -- End

                    Dim drTemp As DataRow() = Nothing
                    If Me.ViewState("BatchPostingUnkid") Is Nothing Then
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        'drTemp = mdtTran.Select("GUID = '" & mdtTran.Rows(SrNo)("GUID").ToString() & "'")
                        drTemp = mdtPostingTran.Select("GUID = '" & mdtPostingTran.Rows(SrNo)("GUID").ToString() & "'")
                        'Sohail (24 Feb 2016) -- End
                    Else
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        'drTemp = mdtTran.Select("empbatchpostingtranunkid = " & CInt(mdtTran.Rows(SrNo)("empbatchpostingtranunkid")))
                        drTemp = mdtPostingTran.Select("empbatchpostingtranunkid = " & CInt(mdtPostingTran.Rows(SrNo)("empbatchpostingtranunkid")))
                        'Sohail (24 Feb 2016) -- End
                    End If

                    If drTemp.Length > 0 Then
                        drTemp(0)("AUD") = "D"
                        drTemp(0).AcceptChanges()
                        FillBatchPostingAddEdit()
                    End If
                End If
            End If
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "   List Page GridView"

    Protected Sub dgBatchPostingList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgBatchPostingList.RowDataBound
        Try
            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
            e.Row.Cells(5).Visible = False
            'Sohail (14 Nov 2017) -- End

            If e.Row.RowIndex < 0 Then Exit Sub

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim mdtTran As DataTable = CType(Me.ViewState("PostingTranList"), DataTable)
            'Sohail (24 Feb 2016) -- End

            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(4).Text = "&nbsp;" AndAlso e.Row.Cells(7).Text = "&nbsp;" Then
                    e.Row.Visible = False
                Else
                    e.Row.Cells(8).Text = CDec(e.Row.Cells(8).Text).ToString(Session("fmtcurrency").ToString)
                End If
                dgBatchPostingList.Columns(0).Visible = CBool(Session("AllowToEditBatchPosting"))
                dgBatchPostingList.Columns(1).Visible = CBool(Session("AllowToDeleteBatchPosting"))
                dgBatchPostingList.Columns(2).Visible = CBool(Session("AllowToPostBatchPostingToED"))
                dgBatchPostingList.Columns(3).Visible = CBool(Session("AllowToVoidBatchPostingToED")) 'Sohail (24 Feb 2016) 

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                If e.Row.Cells(2).HasControls Then
                    Dim lnkPostedToED As LinkButton = DirectCast(e.Row.Cells(2).FindControl("lnkPostedToED"), LinkButton)
                    lnkPostedToED.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuPostToED", lnkPostedToED.Text).Replace("&", "")
                    lnkPostedToED.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuPostToED", lnkPostedToED.Text).Replace("&", "")
                End If
                'Anjan [04 June 2014] -- End

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                If e.Row.Cells(3).HasControls Then
                    Dim lnkPostedToED As LinkButton = DirectCast(e.Row.Cells(2).FindControl("lnkPostedToED"), LinkButton)
                    lnkPostedToED.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuPostToED", lnkPostedToED.Text).Replace("&", "")
                    lnkPostedToED.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuPostToED", lnkPostedToED.Text).Replace("&", "")
                End If
                'Sohail (24 Feb 2016) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgBatchPostingList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgBatchPostingList.PageIndexChanging
        Try
            dgBatchPostingList.PageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgBatchPostingList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgBatchPostingList.RowCommand
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objBatchPosting As New clsBatchPosting
        Dim objBatchPostingTran As New clsBatchPostingTran
        'Hemant (04 Sep 2020) -- End
        Try

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim dtList As DataTable = CType(Me.ViewState("PostingTranList"), DataTable)
            'Sohail (24 Feb 2016) -- End
            Dim SrNo As Integer = CInt(e.CommandArgument)
            Me.ViewState.Add("SrNo", SrNo)

            If e.CommandName = "Change" Then

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'If CBool(dtList.Rows(SrNo)("isposted")) = True Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                ''Language.setLanguage(mstrModuleName)
                'DisplayMessage.DisplayError(ex, Me)
                'Exit Sub
                'Anjan [04 June 2014] -- End
                'Else
                'Sohail (24 Feb 2016) -- End
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtList.Rows(SrNo)("periodunkid"))
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(mdtPostingTranList.Rows(SrNo)("periodunkid"))
                'Sohail (24 Feb 2016) -- End
                If objPeriod._Statusid = 2 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry! You can not edit the selected transaction. Reason: Period is closed."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Sub
                End If
                'End If 'Sohail (24 Feb 2016) 

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'If dtList.Rows.Count > 0 Then
                If mdtPostingTranList.Rows.Count > 0 Then
                    'Sohail (24 Feb 2016) -- End

                    btnEdit.Enabled = False

                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    mblnIsPosted = CBool(mdtPostingTranList.Rows(SrNo)("isposted"))
                    'Sohail (24 Feb 2016) -- End

                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'Me.ViewState.Add("BatchPostingUnkid", dtList.Rows(SrNo)("empbatchpostingunkid"))
                    Me.ViewState.Add("BatchPostingUnkid", mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid"))
                    'Sohail (24 Feb 2016) -- End
                    objBatchPosting._Empbatchpostingunkid = CInt(Me.ViewState("BatchPostingUnkid"))
                    GetValue(objBatchPosting)
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'cboPeriod.SelectedValue = CStr(dtList.Rows(SrNo)("periodunkid"))
                    cboPeriod.SelectedValue = CStr(mdtPostingTranList.Rows(SrNo)("periodunkid"))
                    'Sohail (24 Feb 2016) -- End
                    FillBatchPostingAddEdit()
                    popupAddEdit.Show()
                End If

            ElseIf e.CommandName = "Remove" Then

                Dim objPeriod As New clscommom_period_Tran
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtList.Rows(SrNo)("periodunkid"))
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(mdtPostingTranList.Rows(SrNo)("periodunkid"))
                'Sohail (24 Feb 2016) -- End
                If objPeriod._Statusid = 2 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry! You can not delete the selected transactions. Reason: Period is closed for selected transaction."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Try
                End If

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'If CBool(dtList.Rows(SrNo)("isposted")) = True Then
                If CBool(mdtPostingTranList.Rows(SrNo)("isposted")) = True Then
                    'Sohail (24 Feb 2016) -- End
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry! You can not delete the selected transactions. Reason: selected batch is already posted to ED."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Try
                End If

                popupDelete.Show()

            ElseIf e.CommandName = "PostED" Then

                Dim objPeriod As New clscommom_period_Tran
                Dim objTnA As New clsTnALeaveTran
                Dim dtTable As DataTable = Nothing

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtList.Rows(SrNo)("periodunkid"))
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(mdtPostingTranList.Rows(SrNo)("periodunkid"))
                'Sohail (24 Feb 2016) -- End
                If objPeriod._Statusid = 2 Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry! You can not post the selected batch. Reason: Period is closed for selected batch."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End


                    Exit Try
                End If

                If CBool(dgBatchPostingList.Rows(SrNo).Cells(9).Text) = True Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry! You can not post the selected batch to ED. Reason: selected batch is already posted to ED."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Try
                End If

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'objBatchPostingTran._Empbatchpostingunkid = CInt(dtList.Rows(SrNo)("empbatchpostingunkid"))
                objBatchPostingTran.GetBatchPosting_Tran(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), False, "")
                'Sohail (24 Feb 2016) -- End
                dtTable = objBatchPostingTran._DataTable
                For Each dtRow As DataRow In dtTable.Rows
                    If objTnA.IsPayrollProcessDone(objPeriod._Periodunkid(CStr(Session("Database_Name"))), dtRow.Item("employeeunkid").ToString, objPeriod._End_Date) = True Then

                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName)
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to post earning deduction."), Me)
                        'Sohail (23 Mar 2019) -- End
                        'Anjan [04 June 2014] -- End
                        Exit Try
                    End If
                Next

                chkCopyPreviousEDSlab.Checked = False
                chkOverwriteIfExist.Checked = False
                chkOverwritePrevEDSlabHeads.Checked = False
                chkCopyPreviousEDSlab_CheckedChanged(New Object, New System.EventArgs())

                popupPostED.Show()

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            ElseIf e.CommandName = "VoidBatchPosting" Then

                Dim objPeriod As New clscommom_period_Tran
                Dim objTnA As New clsTnALeaveTran
                Dim dtTable As DataTable = Nothing

                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(mdtPostingTranList.Rows(SrNo)("periodunkid"))
                If objPeriod._Statusid = 2 Then
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry! You can not void posting of the selected batch to ED. Reason: Period is closed for selected batch."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Try
                End If

                If CBool(dgBatchPostingList.Rows(SrNo).Cells(9).Text) = False Then
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry! You cannot void posting of the selected batch to ED. Reason: selected batch is not posted to ED."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Try
                End If

                objBatchPostingTran.GetBatchPosting_Tran(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), CInt(mdtPostingTranList.Rows(SrNo)("empbatchpostingunkid")), False, "")
                dtTable = objBatchPostingTran._DataTable
                For Each dtRow As DataRow In dtTable.Rows
                    If objTnA.IsPayrollProcessDone(objPeriod._Periodunkid(CStr(Session("Database_Name"))), dtRow.Item("employeeunkid").ToString, objPeriod._End_Date) = True Then

                        'Language.setLanguage(mstrModuleName)
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to post earning deduction."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Try
                    End If
                Next

                popupVoidBatchConfirm.Show()
                'Sohail (24 Feb 2016) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objBatchPosting = Nothing
            objBatchPostingTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub


#End Region

#End Region

#Region "ImageButton Events"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Me.ViewState("BatchPostingUnkid") = Nothing
    '        Me.ViewState("PostingTran") = Nothing
    '        Me.ViewState("PostingTranList") = Nothing
    '        Me.ViewState("PeriodStartDate") = Nothing
    '        Me.ViewState("PeriodEndDate") = Nothing
    '        Me.ViewState("HeadId") = Nothing
    '        Me.ViewState("EmployeeId") = Nothing
    '        Me.ViewState("GUID") = Nothing
    '        Me.ViewState("empbatchpostingtranunkid") = Nothing
    '        Me.ViewState("SrNo") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 

            Me.lblBatchPostingNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBatchPostingNo.ID, Me.lblBatchPostingNo.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.chkOverwritePrevEDSlabHeads.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkOverwritePrevEDSlabHeads.ID, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.chkCopyPreviousEDSlab.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkCopyPreviousEDSlab.ID, Me.chkCopyPreviousEDSlab.Text)
            Me.chkOverwriteIfExist.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkOverwriteIfExist.ID, Me.chkOverwriteIfExist.Text)


            Me.dgBatchPostingList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(0).FooterText, Me.dgBatchPostingList.Columns(0).HeaderText).Replace("&", "")
            Me.dgBatchPostingList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(1).FooterText, Me.dgBatchPostingList.Columns(1).HeaderText).Replace("&", "")
            Me.dgBatchPostingList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(4).FooterText, Me.dgBatchPostingList.Columns(4).HeaderText)
            Me.dgBatchPostingList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(5).FooterText, Me.dgBatchPostingList.Columns(5).HeaderText)
            Me.dgBatchPostingList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(6).FooterText, Me.dgBatchPostingList.Columns(6).HeaderText)
            Me.dgBatchPostingList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(7).FooterText, Me.dgBatchPostingList.Columns(7).HeaderText)
            Me.dgBatchPostingList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(8).FooterText, Me.dgBatchPostingList.Columns(8).HeaderText)
            Me.dgBatchPostingList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(9).FooterText, Me.dgBatchPostingList.Columns(9).HeaderText)
            Me.dgBatchPostingList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(10).FooterText, Me.dgBatchPostingList.Columns(10).HeaderText)
            Me.dgBatchPostingList.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPostingList.Columns(11).FooterText, Me.dgBatchPostingList.Columns(11).HeaderText)



            'Language.setLanguage(mstrModuleName1)

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.gbBatchPosting.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gbBatchPosting.ID, Me.gbBatchPosting.Text)
            'Hemant (17 Sep 2020) -- End
            Me.lblTranHead.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblTranHead.ID, Me.lblTranHead.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPayPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)

            Me.btnDelete.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")
            Me.btnEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
            Me.btnAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")


            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblBatchCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblBatchCode.ID, Me.lblBatchCode.Text)
            Me.lblBatchName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblBatchName.ID, Me.lblBatchName.Text)
            Me.lblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblDescription.ID, Me.lblDescription.Text)

            Me.dgBatchPosting.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPosting.Columns(0).FooterText, Me.dgBatchPosting.Columns(0).HeaderText).Replace("&", "")
            Me.dgBatchPosting.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPosting.Columns(1).FooterText, Me.dgBatchPosting.Columns(1).HeaderText).Replace("&", "")
            Me.dgBatchPosting.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPosting.Columns(2).FooterText, Me.dgBatchPosting.Columns(2).HeaderText)
            Me.dgBatchPosting.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPosting.Columns(3).FooterText, Me.dgBatchPosting.Columns(3).HeaderText)
            Me.dgBatchPosting.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPosting.Columns(4).FooterText, Me.dgBatchPosting.Columns(4).HeaderText)
            Me.dgBatchPosting.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgBatchPosting.Columns(5).FooterText, Me.dgBatchPosting.Columns(5).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
