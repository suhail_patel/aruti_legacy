﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Payroll_Rpt_PayeDeductionsFormP9Report
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayeDeductionsFormP9Report"
    Dim DisplayMessage As New CommonCodes
    Dim objPayeP9 As clsPayeDeductionsFormP9Report

    Private mdtPeriod As DataTable
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintYearUnkId As Integer = Nothing
    Private mstrDatabaseName As String = ""
    Private mdtDatabaseStartDate As DateTime = Nothing
    Private mdtDatabaseEndDate As DateTime = Nothing

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Relation = 1
        Paye = 2
        GrossBasic = 3
        HouseAllowance = 4
        FuelLight = 5
        EduAllowance = 6
        OtherBenefit = 7
        LeavePassage = 8
        Gratuity = 9
        Pension = 10
        TaxFreeHouseAllowance = 11
    End Enum
#End Region

#Region " Page's Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPayeP9 = New clsPayeDeductionsFormP9Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            If Not IsPostBack Then

                SetLanguage()
                Call FillCombo()
                Call ResetValue()

            Else
                mdtPeriodEndDate = CDate(Me.ViewState("mdtPeriodEndDate"))
                mintYearUnkId = CInt(Me.ViewState("mintYearUnkId"))
                mstrDatabaseName = Me.ViewState("mstrDatabaseName").ToString
                mdtDatabaseStartDate = CDate(Me.ViewState("mdtDatabaseStartDate"))
                mdtDatabaseEndDate = CDate(Me.ViewState("mdtDatabaseEndDate"))
                mdtPeriod = CType(Me.ViewState("PeriodTable"), DataTable)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtPeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("mintYearUnkId") = mintYearUnkId
            Me.ViewState("mstrDatabaseName") = mstrDatabaseName
            Me.ViewState("mdtDatabaseStartDate") = mdtDatabaseStartDate
            Me.ViewState("mdtDatabaseEndDate") = mdtDatabaseEndDate
            Me.ViewState("PeriodTable") = mdtPeriod
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany_Master
        Dim dsCombos As New DataSet
        Try

            dsCombos = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), -1, "List")
            Dim dtTable As DataTable = New DataView(dsCombos.Tables("List"), "isclosed = 1 ", "end_date DESC ", DataViewRowState.CurrentRows).ToTable
            With cboFYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dtTable
                .DataBind()
            End With
            Call cboFYear_SelectedIndexChanged(cboFYear, New System.EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCompany = Nothing
        End Try
    End Sub

    Public Sub FillList()
        Dim objEmp As New clsEmployee_Master
        Dim objCompany As New clsCompany_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim dsCombos As DataSet = Nothing
        Try
            objCompany._YearUnkid = CInt(cboFYear.SelectedValue)
            mintYearUnkId = CInt(cboFYear.SelectedValue)
            mstrDatabaseName = objCompany._DatabaseName
            mdtDatabaseStartDate = objCompany._Database_Start_Date
            mdtDatabaseEndDate = objCompany._Database_End_Date

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsCombos = objEmp.GetEmployeeList(mstrDatabaseName, CInt(Session("UserId")), mintYearUnkId, CInt(Session("CompanyUnkId")), _
                                                  mdtDatabaseStartDate, mdtDatabaseStartDate, _
                                                  Session("UserAccessModeSetting").ToString, True, True, "Employee", False, CInt(Session("Employeeunkid")), _
                                                  , , , , , , , , , , , , , , , False, True)
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsCombos = objEmp.GetEmployeeList(mstrDatabaseName, CInt(Session("UserId")), mintYearUnkId, CInt(Session("CompanyUnkId")), _
                                             mdtDatabaseStartDate, mdtDatabaseStartDate, _
                                             Session("UserAccessModeSetting").ToString, True, True, "Employee", True)
            End If
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Employee")
                .DataBind()
            End With


            dsList = objPeriod.GetList("Period", CInt(enModuleReference.Payroll), CInt(cboFYear.SelectedValue), mdtDatabaseStartDate, True, 0, False)
            mdtPeriod = New DataView(dsList.Tables(0)).ToTable

            If mdtPeriod.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                mdtPeriod.Columns.Add(dtCol)
            End If


            'Dim dRow As DataRow

            'For Each dtRow As DataRow In dsList.Tables("Period").Rows
            '    dRow = mdtPeriod.NewRow

            '    dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
            '    dRow.Item("period_name") = dtRow.Item("period_name").ToString

            '    dRow.Item("start_date") = dtRow.Item("start_date").ToString
            '    dRow.Item("end_date") = dtRow.Item("end_date").ToString

            '    mdtPeriod.Rows.Add(dRow)
            'Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing

            If mdtPeriod.Rows.Count <= 0 Then
                Dim r As DataRow = mdtPeriod.NewRow

                r.Item(2) = "None" ' To Hide the row and display only Row Header
                r.Item(1) = "0"

                mdtPeriod.Rows.Add(r)
            End If

            gvPeriod.DataSource = mdtPeriod
            gvPeriod.DataBind()
        End Try
    End Sub

    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In gvPeriod.Rows
                If CInt(mdtPeriod.Rows(gvr.RowIndex).Item("periodunkid")) > 0 AndAlso mdtPeriod.Rows(gvr.RowIndex).Item("period_name").ToString <> "" Then
                    cb = CType(gvPeriod.Rows(gvr.RowIndex).FindControl("chkSelectPeriod"), CheckBox)
                    cb.Checked = blnCheckAll

                    mdtPeriod.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = "0"


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            objPayeP9.SetDefaultValue()
            'If CInt(cboPAYE.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head."), enMsgBoxStyle.Information)
            '    cboPAYE.Focus()
            '    Return False
            'ElseIf CInt(cboRelation.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Relation is mandatory information. Please select Relation."), enMsgBoxStyle.Information)
            '    cboRelation.Focus()
            '    Return False
            If cboFYear.Items.Count <= 0 OrElse CInt(cboFYear.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, There is no closed financial year selected."), Me)
                Return False
            ElseIf mdtPeriod IsNot Nothing AndAlso mdtPeriod.Select("IsChecked = 1").Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select atleast one Period."), Me)
                Return False
            End If


            objPayeP9._EmployeeID = CInt(cboEmployee.SelectedValue)
            objPayeP9._EmployeeName = cboEmployee.SelectedItem.Text

            dsList = objUserDefRMode.GetList("List", enArutiReport.PAYE_DEDUCTION_FORM_P9_Report)
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case CInt(enHeadTypeId.Relation)
                            objPayeP9._RelationID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.Paye)
                            objPayeP9._PayeHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.GrossBasic)
                            objPayeP9._GrossBasicHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.HouseAllowance)
                            objPayeP9._HouseAllowanceHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.FuelLight)
                            objPayeP9._FuelLightHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.EduAllowance)
                            objPayeP9._EduAllowanceHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.OtherBenefit)
                            objPayeP9._OtherBenefitHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.LeavePassage)
                            objPayeP9._LeavePassageHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.Gratuity)
                            objPayeP9._GratuityHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.Pension)
                            objPayeP9._PensionHeadID = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.TaxFreeHouseAllowance)
                            objPayeP9._TaxFreeHouseAllowanceHeadID = CInt(dsRow.Item("transactionheadid"))


                    End Select
                Next
            End If

            objPayeP9._FirstNamethenSurname = CBool(Session("FirstNamethenSurname"))
            objPayeP9._FmtCurrency = Session("fmtCurrency").ToString

            Dim strIDs As String = String.Join(",", (From p In mdtPeriod Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("periodunkid").ToString)).ToArray)
            If strIDs.Trim.Length <= 0 Then
                strIDs = " 0 "
            End If
            objPayeP9._PeriodIDs = strIDs
            objPayeP9._PeriodNames = String.Join(",", (From p In mdtPeriod Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("period_name").ToString)).ToArray)
            objPayeP9._PeriodStartDate = eZeeDate.convertDate((From p In mdtPeriod Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("start_date").ToString)).FirstOrDefault)
            objPayeP9._PeriodEndDate = eZeeDate.convertDate((From p In mdtPeriod Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("end_date").ToString)).LastOrDefault)
            objPayeP9._DatabaseEndDate = mdtDatabaseEndDate

            If (CInt(Session("LoginBy")) = CInt(Global.User.en_loginby.Employee)) Then
                objPayeP9._ApplyUserAccessFilter = False
            End If
            'objPayeP9._ViewByIds = mstrStringIds
            'objPayeP9._ViewIndex = mintViewIdx
            'objPayeP9._ViewByName = mstrStringName
            'objPayeP9._Analysis_Fields = mstrAnalysis_Fields
            'objPayeP9._Analysis_Join = mstrAnalysis_Join
            'objPayeP9._Analysis_OrderBy = mstrAnalysis_OrderBy_GName

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Buttons "
    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If Not SetFilter() Then Exit Sub

            objPayeP9.generateReportNew(mstrDatabaseName, _
                                          CInt(Session("UserId")), _
                                         mintYearUnkId, _
                                         CInt(Session("CompanyUnkId")), _
                                         mdtDatabaseStartDate, _
                                         mdtDatabaseEndDate, _
                                         Session("UserAccessModeSetting").ToString, _
                                         True, Session("ExportReportPath").ToString, _
                                         CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objPayeP9._Rpt

            If objPayeP9._Rpt IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboFYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFYear.SelectedIndexChanged
        Try
            If cboFYear.Items.Count > 0 AndAlso CInt(cboFYear.SelectedValue) > 0 Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Protected Sub chkSelectAllPeriod_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllPeriod(chkSelectAll.Checked)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectPeriod_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtPeriod.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblFYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFYear.ID, Me.lblFYear.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.gvPeriod.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPeriod.Columns(2).FooterText, Me.gvPeriod.Columns(2).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

End Class
