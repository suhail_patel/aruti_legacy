﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_BatchPostingList.aspx.vb"
    Inherits="Payroll_wPg_BatchPostingList" Title="Batch Posting List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="Delete" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">
//    function pageLoad(sender, args) {
//        $("select").searchable();

//        alert('hi');
//        $('#imgclose').click(function(evt) {
//            $find('<%= popupAddEdit.ClientID %>').hide();
//        });
//    }

    function closePopup() {
        $('#imgclose').click(function(evt) {
            $find('<%= popupAddEdit.ClientID %>').hide();        
        });
    }

    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
    
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupAddEdit" runat="server" TargetControlID="btnHidden"
                    PopupControlID="pnlAddEdit" BackgroundCssClass="modal-backdrop" DropShadow="false"
                    CancelControlID="btnClose">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlAddEdit" runat="server" Style="display: none;" CssClass="card modal-dialog modal-lg">
                    <%--<div class="block-header">
                        <h2>
                            <asp:Label ID="gbBatchPosting" runat="server" Text="Batch Posting Information" CssClass="form-label"></asp:Label>
                        </h2>
                    </div>--%>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="Label2" runat="server" Text="Batch Posting Information Add/Edit" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height:550px;">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBatchPostingNo" runat="server" Text="Batch Posting #" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtBatchPostingNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBatchCode" runat="server" Text="Batch Code" Visible="false" CssClass="form-label"></asp:Label>
                                        <asp:Label ID="lblBatchName" runat="server" Text="Batch Name" CssClass="form-label"></asp:Label><div
                                            class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtBatchCode" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtBatchCode" ErrorMessage="Please enter Batch Code. " CssClass="error"
                                                    Style="z-index: 1000" ValidationGroup="AddEdit" SetFocusOnError="True"
                                                    Visible="False"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtBatchName" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtBatchName" ErrorMessage="Please enter Batch Name. " CssClass="error"
                                                     Style="z-index: 1000" ValidationGroup="AddEdit" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <asp:UpdatePanel ID="UpdateEmployeeCode" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTranHead" runat="server" Text="Tran. Head" CssClass="form-label"></asp:Label>
                                        <asp:UpdatePanel ID="UpdateTranhead" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboTranhead" runat="server" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:UpdatePanel ID="UpdateAmount" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtAmount" ErrorMessage="Please enter Amount. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="AddEdit" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" ValidationGroup="AddEdit" />
                                <asp:Button ID="btnEdit" runat="server" Text="Update" CssClass="btn btn-default"
                                    ValidationGroup="AddEdit" />
                                <asp:Button ID="btnDelete" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:HiddenField ID="HiddenField1" runat="Server" />
                            </div>
                                <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body_">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 135px;">
                                            <asp:UpdatePanel ID="UpdateBatchPosting" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnl_dgBatchPosting" runat="server" ScrollBars="Auto">
                                                        <asp:GridView ID="dgBatchPosting" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                            Width="99%" CssClass="table table-hover table-bordered">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText=""
                                                                    HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Edit" CommandName="Change"
                                                                                CommandArgument="<%# Container.DataItemIndex %>" CausesValidation="false">
                                                                                    <i class="fas fa-pencil-alt"></i>
                                                                            </asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText=""
                                                                    HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Remove"
                                                                                CommandArgument="<%# Container.DataItemIndex %>" CausesValidation="false">
                                                                                    <i class="fas fa-trash text-danger"></i> 
                                                                            </asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="periodname" HeaderText="Period" ReadOnly="True" FooterText="colhPeriod" />
                                                                <%--<asp:BoundField DataField="employeecode" HeaderText="Employee Code" ReadOnly="True" FooterText="colhEmpCode" />--%>
                                                                <asp:BoundField DataField="EmpName" HeaderText="Employee Name" ReadOnly="True" FooterText="colhEmpName" />
                                                                <%--<asp:BoundField DataField="trnheadcode" HeaderText="Head Code" ReadOnly="True" FooterText="colhTranHeadCode" />--%>
                                                                <asp:BoundField DataField="TranHeadName" HeaderText="Transaction Head Name" ReadOnly="True"
                                                                    FooterText="colhTranHeadName" />
                                                                <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="btnEdit" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupPostED" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnCancel" PopupControlID="pnlPostED" TargetControlID="HiddenField2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlPostED" runat="server" Style="display: none;" DefaultButton="btnPost"
                    CssClass="card modal-dialog">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Post To ED" CssClass="form-label" />
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="UpdateOverWriteIfExist" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkOverwriteIfExist" runat="server" Text="Overwrite if selected head exist"
                                                    CssClass="filled-in" />
                                                <br />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkCopyPreviousEDSlab" EventName="CheckedChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous ED Slab"
                                            AutoPostBack="true" CssClass="filled-in" /><br />
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkOverwritePrevEDSlabHeads" runat="server" Text="Overwrite Previous ED Slab heads"
                                                    CssClass="filled-in" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkCopyPreviousEDSlab" EventName="CheckedChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnPost" runat="server" Text="Post" CssClass="btn btn-primary" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                                <asp:HiddenField ID="HiddenField2" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc4:Delete ID="popupDelete" runat="server" Title="Are you sure you want to delete selected transactions ?" />
                <uc5:Confirmation ID="popupCopyPrevious" runat="server" Message="You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.Do you want to continue?"
                    Title="Confirmation" />
                <uc5:Confirmation ID="popupPost" runat="server" Message="Are you sure you want to Post selected transactions to earning deduction?"
                    Title="Confirmation" />
                <uc5:Confirmation ID="popupVoidBatchConfirm" runat="server" Message="Are you sure you want to Void Posting of selected batch to ED?"
                    Title="Confirmation" />
                <uc4:Delete ID="popupVoidBatchPosting" runat="server" Title="Are you sure you want to Void Posting of selected batch to ED?"
                    ValidationGroup="vbp" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Batch Posting List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboBatch" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Pay Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPayPeriod" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnlstReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive"style="max-height: 400px;">
                                            <asp:Panel ID="pbl_dgBatchPostingList" runat="server" ScrollBars="Auto" >
                                                <asp:GridView ID="dgBatchPostingList" runat="server" AutoGenerateColumns="false"
                                                    CssClass="table table-hover table-bordered" AllowPaging="false" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                                        CommandName="Change" CommandArgument="<%# Container.DataItemIndex %>">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                                <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                        CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>">
                                                                        <i class="fas fa-trash text-danger"></i> 
                                                                    </asp:LinkButton>
                                                                </span>
                                                                <%--  <asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkPostedToED" runat="server" Text="Post To ED" Font-Underline="false"
                                                                    CommandName="PostED" CommandArgument="<%# Container.DataItemIndex %>" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkVoidBatchPosting" runat="server" Text="Void Batch Posting"
                                                                    Font-Underline="false" CommandName="VoidBatchPosting" CommandArgument="<%# Container.DataItemIndex %>" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="batchno" HeaderText="Batch No" ReadOnly="True" FooterText="colhBatchNo" />
                                                        <asp:BoundField DataField="batchcode" HeaderText="Batch Code" ReadOnly="True" FooterText="colhBatchCode" />
                                                        <asp:BoundField DataField="batchname" HeaderText="Batch Name" ReadOnly="True" FooterText="colhBatchName" />
                                                        <asp:BoundField DataField="PeriodName" HeaderText="Pay Period" ReadOnly="True" FooterText="colhPeriod" />
                                                        <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" ReadOnly="True"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                                        <asp:BoundField DataField="IsPosted" HeaderText="Is Posted" ReadOnly="True" FooterText="colhPosted" />
                                                        <asp:BoundField DataField="user_name" HeaderText="User Name" ReadOnly="True" FooterText="colhUser" />
                                                        <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                                            FooterText="colhDesc" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
