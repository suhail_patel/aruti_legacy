﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Payroll_wPg_DetailedSalaryBreakdownReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmDetailedSalaryBreakdownReport"
    Private marrDetailedSalaryBreakdownReportHeadsIds As New ArrayList
    Dim objPayroll As clsDetailedSalaryBreakdownReport

    Private mintFirstOpenPeriod As Integer = 0
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0

    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    Private mstrCurrency_Rate As String = String.Empty
    Private mstrFromDatabaseName As String

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = String.Empty
    Private mstrAnalysis_Join As String = String.Empty
    Private mstrAnalysis_OrderBy As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objCCenter As New clscostcenter_master
        Dim objDept As New clsDepartment
        Dim objBranch As New clsStation
        Dim objExRate As New clsExchangeRate
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Try
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = CStr(mintFirstOpenPeriod)
            End With
            Call cboCurrency_SelectedIndexChanged(cboCurrency, Nothing)
            If ViewState("dsPeriod") Is Nothing Then
                ViewState("dsPeriod") = dsList.Tables(0).Copy
            End If

            dsList = objCCenter.getComboList("List", True)
            With cboCCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objDept.getComboList("List", True)
            With cboDept
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If

            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .DataBind()
                .SelectedValue = "0"
            End With


            dsList = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("HeadType")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboTrnHeadType_SelectedIndexChanged(cboTrnHeadType, Nothing)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = CStr(mintFirstOpenPeriod)
            cboCCenter.SelectedValue = "0"
            cboDept.SelectedValue = "0"
            cboBranch.SelectedValue = "0"
            chkInactiveemp.Checked = False

            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            mstrCurrency_Rate = ""
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillCustomHeads()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            Dim dtTable As New DataTable
            Dim xCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            Dim xCol1 As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            dtTable.Columns.Add(xCol)
            If marrDetailedSalaryBreakdownReportHeadsIds.Count > 0 Then

                Dim xFilter As String = String.Empty
                dtTable.Columns("IsChecked").DefaultValue = True
                For Each xVal As Integer In marrDetailedSalaryBreakdownReportHeadsIds
                    xFilter = "" : xFilter = "prtranhead_master.tranheadunkid = '" & xVal & "' "
                    'Sohail (03 Feb 2016) -- Start
                    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                    'dsList = objTranHead.getComboList("Heads", False, , , , , , xFilter)
                    dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, , , , , , xFilter)
                    'Sohail (03 Feb 2016) -- End
                    dtTable.Merge(dsList.Tables(0), True)
                Next

                xFilter = "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", CType(marrDetailedSalaryBreakdownReportHeadsIds.ToArray(Type.GetType("System.String")), String())) & ")"
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , xFilter)
                dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , xFilter)
                'Sohail (03 Feb 2016) -- End
                dsList.Tables(0).Columns.Add(xCol1)

                For Each xrow As DataRow In dsList.Tables(0).Rows
                    xrow("IsChecked") = False
                    xrow.AcceptChanges()
                Next

                dtTable.Merge(dsList.Tables(0), True)
            Else
                dtTable.Columns("IsChecked").DefaultValue = False
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue))
                dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, CInt(cboTrnHeadType.SelectedValue))
                'Sohail (03 Feb 2016) -- End
                dtTable.Merge(dsList.Tables(0), True)
            End If

            Me.ViewState.Add("dtTranHead", dtTable)

            With lvCustomTranHead
                .DataSource = dtTable
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChangeTranHeadLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim dtTtanHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            Dim xRow As DataRow = dtTtanHead.Rows(rowIndex)
            Dim xNewRow As DataRow = dtTtanHead.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvCustomTranHead.Rows.Count - 1 Then Exit Sub
                dtTtanHead.Rows.RemoveAt(rowIndex)
                dtTtanHead.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtTtanHead.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtTtanHead.Rows.Remove(xRow)
                dtTtanHead.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtTtanHead.Rows.IndexOf(xNewRow)
            End If
            With lvCustomTranHead
                .DataSource = dtTtanHead
                .DataBind()
            End With
            dtTtanHead.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CDbl(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), Me)
                cboPeriod.Focus()
                Return False
            ElseIf (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("tranheadunkid").ToString)).ToList().Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Please select atleast one transaction head in order to generate report."), Me)
                Return False
            ElseIf mdecEx_Rate <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Nilay (10-Feb-2016) -- Start
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            objPayroll = New clsDetailedSalaryBreakdownReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                marrDetailedSalaryBreakdownReportHeadsIds.Clear()
                If CStr(Session("DetailedSalaryBreakdownReportHeadsIds")).Trim <> "" Then
                    marrDetailedSalaryBreakdownReportHeadsIds.AddRange(CStr(Session("DetailedSalaryBreakdownReportHeadsIds")).Split(CChar(",")))
                End If

                Call FillCombo()
                Call ResetValue()
            Else

                mintFirstOpenPeriod = CInt(Me.ViewState("mintFirstOpenPeriod"))
                mstrBaseCurrSign = CStr(Me.ViewState("mstrBaseCurrSign"))
                mintBaseCurrId = CInt(Me.ViewState("mintBaseCurrId"))
                mdecEx_Rate = CDec(Me.ViewState("mdecEx_Rate"))
                mstrCurr_Sign = CStr(Me.ViewState("mstrCurr_Sign"))
                mstrCurrency_Rate = CStr(Me.ViewState("mstrCurrency_Rate"))
                mstrFromDatabaseName = CStr(Me.ViewState("mstrFromDatabaseName"))
                marrDetailedSalaryBreakdownReportHeadsIds = CType(Me.ViewState("marrDetailedSalaryBreakdownReportHeadsIds"), ArrayList)
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintFirstOpenPeriod") = mintFirstOpenPeriod
            Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("mintBaseCurrId") = mintBaseCurrId
            Me.ViewState("mdecEx_Rate") = mdecEx_Rate
            Me.ViewState("mstrCurr_Sign") = mstrCurr_Sign
            Me.ViewState("mstrCurrency_Rate") = mstrCurrency_Rate
            Me.ViewState("mstrFromDatabaseName") = mstrFromDatabaseName
            Me.ViewState("marrDetailedSalaryBreakdownReportHeadsIds") = marrDetailedSalaryBreakdownReportHeadsIds
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Combobox Events "

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            If cboCurrency.Visible = True Then

                If cboCurrency.Items.Count > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate

                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = CStr(dsList.Tables("ExRate").Rows(0)("currency_sign"))
                        lblExRate.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (16 Mar 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = ""
                End If
            End If

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            'Nilay (10-Feb-2016) -- Start
            'dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            'Nilay (10-Feb-2016) -- End

            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Call FillCustomHeads()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Protected Sub chkHeadSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkHeadSelectAll As CheckBox = TryCast(sender, CheckBox)
            Dim dtTranHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            If chkHeadSelectAll Is Nothing Then Exit Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In lvCustomTranHead.Rows
                cb = CType(lvCustomTranHead.Rows(gvr.RowIndex).FindControl("chkHeadSelect"), CheckBox)
                cb.Checked = chkHeadSelectAll.Checked
            Next
            For Each gvr As DataRow In dtTranHead.Rows
                gvr("IsChecked") = chkHeadSelectAll.Checked
            Next
            dtTranHead.AcceptChanges()

            marrDetailedSalaryBreakdownReportHeadsIds.Clear()
            marrDetailedSalaryBreakdownReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeadSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = TryCast(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = CType(ViewState("dtTranHead"), DataTable).Select("tranheadunkid = '" & CInt(lvCustomTranHead.DataKeys(gvRow.RowIndex).Value) & "'")
                If drRow.Length > 0 Then
                    drRow(0)("IsChecked") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If

            marrDetailedSalaryBreakdownReportHeadsIds.Clear()
            marrDetailedSalaryBreakdownReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Other Control Event(S)"
    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Try
            If IsValidate() = False Then Exit Sub
            Dim xStr As String = ""
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = CInt(Session("Companyunkid"))

            Dim selectedTags As List(Of String) = (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("tranheadunkid").ToString)).ToList
            xStr = String.Join(",", selectedTags.ToArray())
            If xStr.Trim.Length > 0 Then
                objConfig._DetailedSalaryBreakdownReportHeadsIds = xStr
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Please select atleast one transaction head in order to generate report."), Me)
                Exit Sub
            End If
            objConfig.updateParam()
            Session("DetailedSalaryBreakdownReportHeadsIds") = objConfig._DetailedSalaryBreakdownReportHeadsIds
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Settings saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            popupAnalysisBy._EffectiveDate = objPeriod._End_Date
            'Sohail (23 May 2017) -- End
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub txtSearchTranHeads_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTranHeads.TextChanged
        Try
            Dim dtTranHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            If dtTranHead.Rows.Count <= 0 Then Exit Sub
            Dim dtView As DataView = dtTranHead.DefaultView
            dtView.RowFilter = "Name LIKE '%" & txtSearchTranHeads.Text & "%' OR Code LIKE '%" & txtSearchTranHeads.Text & "%' "
            dtTranHead = dtView.ToTable
            With lvCustomTranHead
                .DataSource = dtTranHead
                .DataBind()
            End With
            If txtSearchTranHeads.Text.Trim.Length > 0 Then
                lvCustomTranHead.Columns(4).Visible = False
            Else
                lvCustomTranHead.Columns(4).Visible = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim arrDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        Try
            If IsValidate() = False Then Exit Sub

            objPayroll.SetDefaultValue()

            Dim strList As String = cboPeriod.SelectedValue.ToString
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", CInt(CType(ViewState("dsPeriod"), DataTable).Select("periodunkid = " & CInt(cboPeriod.SelectedValue) & " ")(0).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                intPrevYearID = CInt(CType(ViewState("dsPeriod"), DataTable).Select("periodunkid = " & CInt(cboPeriod.SelectedValue) & " ")(0).Item("yearunkid"))
            End If


            objPayroll._PeriodIdList = strList
            objPayroll._Arr_DatabaseName = arrDatabaseName
            objPayroll._PeriodId = CInt(cboPeriod.SelectedValue)
            objPayroll._PeriodName = cboPeriod.SelectedItem.Text
            objPayroll._IsActive = chkInactiveemp.Checked
            objPayroll._CCenterId = CInt(cboCCenter.SelectedValue)
            objPayroll._CCenter_Name = cboCCenter.SelectedItem.Text
            objPayroll._DeptId = CInt(cboDept.SelectedValue)
            objPayroll._Dept_Name = cboDept.SelectedItem.Text
            objPayroll._BranchId = CInt(cboBranch.SelectedValue)
            objPayroll._Branch_Name = cboBranch.SelectedItem.Text
            objPayroll._ViewByIds = mstrStringIds
            objPayroll._ViewIndex = mintViewIdx
            objPayroll._ViewByName = mstrStringName
            objPayroll._Analysis_Fields = mstrAnalysis_Fields
            objPayroll._Analysis_Join = mstrAnalysis_Join

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objPayroll._PeriodStartDate = objPeriod._Start_Date
            objPayroll._PeriodEndDate = objPeriod._End_Date

            objPayroll._Ex_Rate = mdecEx_Rate
            objPayroll._Currency_Sign = mstrCurr_Sign
            objPayroll._Currency_Rate = mstrCurrency_Rate

            Dim xStr As String = ""
            Dim selectedTags As List(Of String) = (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("tranheadunkid").ToString)).ToList
            xStr = String.Join(",", selectedTags.ToArray())
            If xStr.Trim.Length > 0 Then
                objPayroll._DetailedSalaryBreakdownReportHeadsIds = xStr
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Please tick atleast one transaction head in order to save."), Me)
                Exit Sub
            End If

            Dim xAllocVal As New Dictionary(Of Integer, String)
            xAllocVal = (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (CBool(p.Item("IsChecked")) = True) Select (p)).ToDictionary(Function(x) CInt(x.Item("tranheadunkid")), Function(y) y.Item("name").ToString)

            objPayroll._DetailedSalaryBreakdown = xAllocVal

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objPayroll._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objPayroll._fmtCurrency = CStr(Session("fmtCurrency"))
            objPayroll._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objPayroll._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            objPayroll._OpenAfterExport = False

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPayroll.Generate_DetailedSalaryBreakdownReport()
            objPayroll.Generate_DetailedSalaryBreakdownReport(CStr(Session("Database_Name")), _
                                                              CInt(Session("UserId")), _
                                                              CInt(Session("Fin_year")), _
                                                              CInt(Session("CompanyUnkId")), _
                                                              CStr(Session("UserAccessModeSetting")), True)
            'Shani(20-Nov-2015) -- End

            If objPayroll._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objPayroll._FileNameAfterExported
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnPreview_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region
End Class
