﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Loan_Advance_Saving
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    Private mdtLoanList As DataTable
    Private mdtSavingList As DataTable

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceSaving"
    'Anjan [04 June 2014 ] -- End

    'Nilay (28-Aug-2015) -- Start
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = -1
    'Nilay (28-Aug-2015) -- End

#End Region

#Region " Page's Event "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mdtLoanList", mdtLoanList)
            ViewState.Add("mdtSavingList", mdtSavingList)

            'Nilay (28-Aug-2015) -- Start
            Me.ViewState("BaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("BaseCurrId") = mintBaseCurrId
            'Nilay (28-Aug-2015) -- End

            'Sohail (15 May 2020) -- Start
            'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report.
            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            'Sohail (15 May 2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Nilay (07-Feb-2016) -- Start
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (07-Feb-2016) -- End

            If Not IsPostBack Then


                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                SetLanguage()
                'Anjan [04 June 2014 ] -- End


                Dim objLoan As New clsLoanAdvanceSaving(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))


                objLoan.SetDefaultValue()

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Closebutton1.PageHeading = objLoan._ReportName

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objLoan._ReportName)
                'Nilay (01-Feb-2015) -- End
                'Anjan [04 June 2014] -- End

                objLoan = Nothing

                Call CreateLoanListTable()
                Call CreateSavingListTable()
                Call FillCombo()
            Else
                mdtLoanList = CType(ViewState("mdtLoanList"), DataTable)
                mdtSavingList = CType(ViewState("mdtSavingList"), DataTable)
                'Nilay (28-Aug-2015) -- Start
                mstrBaseCurrSign = CStr(Me.ViewState("BaseCurrSign"))
                'Nilay (10-Feb-2016) -- Start
                'mintBaseCurrId = CInt(Me.ViewState("intBaseCurrId"))
                mintBaseCurrId = CInt(Me.ViewState("BaseCurrId"))
                'Nilay (10-Feb-2016) -- End
                'Nilay (28-Aug-2015) -- End
                'Sohail (15 May 2020) -- Start
                'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report.
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter"))
                'Sohail (15 May 2020) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            'Nilay (18-Jan-2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            Dim objMaster As New clsMasterData
            'Nilay (18-Jan-2016) -- End

            Dim objperiod As New clscommom_period_Tran
            Dim objEmp As New clsEmployee_Master
            Dim objBranch As New clsStation
            Dim objCurrency As New clsExchangeRate
            Dim dsList As New DataSet

            dsList = objperiod.getListForCombo(enModuleReference.Payroll, CInt(Session("fin_year")), Session("Database_Name").ToString, _
                                               CDate(Session("fin_startdate").ToString), "Period", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objperiod = Nothing
            dsList = Nothing

            'Nilay (28-Aug-2015) -- Start
            dsList = objCurrency.getComboList("Currency")
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dtTable As DataTable = New DataView(dsList.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If
            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If
            'Nilay (28-Aug-2015) -- End

            'Sohail (21 Dec 2013) -- Start
            'Enhancement - Loan Register on ESS
            'dsList = objEmp.GetEmployeeList("Employee", True, False, , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'Nilay (15-Dec-2015) -- Start
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, False, , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", False, False, CInt(Session("Employeeunkid")), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                'Nilay (10-Feb-2016) -- Start
                'dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                             Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), _
                '                             "Employee", True)

                dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             Session("UserAccessModeSetting").ToString, True, True, "Employee", True)
            Else
                'Nilay (21-Apr-2016) -- Start
                'dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                             Session("UserAccessModeSetting").ToString, True, True, "Employee", True, , , , , , , , , , , , , , , , , , False)

                dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             Session("UserAccessModeSetting").ToString, True, True, "Employee", False, CInt(Session("Employeeunkid")), _
                                             , , , , , , , , , , , , , , , False, True) 'Nilay (21-Oct-2016)
                'Nilay (21-Apr-2016) -- End


                'Nilay (10-Feb-2016) -- End

            End If
            'Nilay (15-Dec-2015) -- End

            'Sohail (21 Dec 2013) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing

            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            objBranch = Nothing

            dsList = Nothing
            'Nilay (18-Jan-2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            dsList = objMaster.GetLoan_Saving_Status("Status", , True)
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Nilay (18-Jan-2016) -- End

            With cboReportType
                .Items.Clear()
                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                '.Items.Add("Loan Report")
                '.Items.Add("Advance Report")
                '.Items.Add("Saving Report")
                '.Items.Add("Loan Register Report") 'Sohail (26 Aug 2013)
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    'Language.setLanguage(mstrModuleName)
                    .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Report"), "0"))
                    .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Advance Report"), "1"))
                    .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Saving Report"), "2"))
                End If
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Loan Register Report"), "3"))
                'Sohail (21 Dec 2013) -- End

                'Nilay (28-Aug-2015) -- Start
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Loan Scheme With Bank Account Report"), "4"))
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Loan History Report"), "5"))
                'Nilay (28-Aug-2015) -- End

                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Loan Byproduct"), "6"))
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Saving Byproduct"), "7"))
                'Sohail (04 Mar 2016) -- End

                'Sohail (13 Dec 2016) -- Start
                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional).
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Loan Repayment Schedule Report"), "8"))
                'Sohail (13 Dec 2016) -- End

                'Nilay (08 Apr 2017) -- Start
                'Enhancements: New Loan Tracking Report for PACRA
                .Items.Add(New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Loan Tracking Report"), "9"))
                'Nilay (08 Apr 2017) -- End

                Call cboReportType_SelectedIndexChanged(cboReportType, New System.EventArgs())

            End With

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            'Nilay (28-Aug-2015) -- Start
            '.Items.Add(New ListItem("Loan Scheme With Bank Account Report", "4"))
            'Nilay (28-Aug-2015) -- End
            Dim objBank As New clspayrollgroup_master
            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboBankName
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            cboBankName_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (12-Jun-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub CreateLoanListTable()
        Try
            mdtLoanList = New DataTable
            With mdtLoanList
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CreateSavingListTable()
        Try
            mdtSavingList = New DataTable
            With mdtSavingList
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillLoanScheme()
        Dim objLoanScheme As New clsLoan_Scheme
        Dim dsList As DataSet
        Try
            mdtLoanList.Rows.Clear()

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(False, "Loan")
            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(False, "Loan", -1, "", mblnSchemeShowOnEss)
            'Pinkal (07-Dec-2017) -- End


            Dim dRow As DataRow

            For Each dsRow As DataRow In dsList.Tables("Loan").Rows
                dRow = mdtLoanList.NewRow

                dRow.Item("id") = CInt(dsRow.Item("loanschemeunkid"))
                dRow.Item("code") = dsRow.Item("Code").ToString
                dRow.Item("name") = dsRow.Item("name").ToString

                mdtLoanList.Rows.Add(dRow)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanScheme = Nothing

            If mdtLoanList.Rows.Count <= 0 Then
                Dim r As DataRow = mdtLoanList.NewRow

                r.Item(2) = "None" ' To Hide the row and display only Row Header
                r.Item(3) = ""

                mdtLoanList.Rows.Add(r)
            End If

            gvScheme.DataSource = mdtLoanList
            gvScheme.DataBind()
        End Try
    End Sub

    Public Sub FillSavingScheme()
        Dim objSavingScheme As New clsSavingScheme
        Dim dsList As DataSet
        Try
            mdtSavingList.Rows.Clear()
            dsList = objSavingScheme.getComboList(False, "Loan")

            Dim dRow As DataRow

            For Each dsRow As DataRow In dsList.Tables("Loan").Rows
                dRow = mdtSavingList.NewRow

                dRow.Item("id") = CInt(dsRow.Item("savingschemeunkid"))
                dRow.Item("code") = dsRow.Item("Code").ToString
                dRow.Item("name") = dsRow.Item("name").ToString

                mdtSavingList.Rows.Add(dRow)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSavingScheme = Nothing

            If mdtSavingList.Rows.Count <= 0 Then
                Dim r As DataRow = mdtSavingList.NewRow

                r.Item(2) = "None" ' To Hide the row and display only Row Header
                r.Item(3) = ""

                mdtSavingList.Rows.Add(r)
            End If

            gvScheme.DataSource = mdtSavingList
            gvScheme.DataBind()
        End Try
    End Sub

    Private Sub CheckAllScheme(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In gvScheme.Rows
                'Sohail (26 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'If cboReportType.SelectedIndex = 0 Then 'Loan
                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'If cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3 Then 'Loan or Loan Register

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport Then 'Loan or Loan Register 
                If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanTrackingReport Then 'Loan or Loan Register 
                    'Varsha Rana (06-Oct-2017) -- End


                    'Sohail (21 Dec 2013) -- End
                    'Sohail (26 Aug 2013) -- End
                    If CInt(mdtLoanList.Rows(gvr.RowIndex).Item("id")) > 0 AndAlso mdtLoanList.Rows(gvr.RowIndex).Item("name").ToString <> "" Then
                        cb = CType(gvScheme.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                        cb.Checked = blnCheckAll

                        mdtLoanList.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                    End If
                    'Sohail (21 Dec 2013) -- Start
                    'Enhancement - Loan Register on ESS
                    'ElseIf cboReportType.SelectedIndex = 2 Then 'Saving
                ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingReport Then 'Saving
                    'Sohail (21 Dec 2013) -- End
                    If CInt(mdtSavingList.Rows(gvr.RowIndex).Item("id")) > 0 AndAlso mdtSavingList.Rows(gvr.RowIndex).Item("name").ToString <> "" Then
                        cb = CType(gvScheme.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                        cb.Checked = blnCheckAll

                        mdtSavingList.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                    End If
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllScheme(chkSelectAll.Checked)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                'Sohail (26 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'If cboReportType.SelectedIndex = 0 Then 'Loan
                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'If cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3 Then 'Loan or Loan Register
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'If CInt(cboReportType.SelectedValue) = 0 OrElse CInt(cboReportType.SelectedValue) = 3 Then 'Loan or Loan Register
                If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport Then 'Loan or Loan Register
                    'Sohail (04 Mar 2016) -- End
                    'Sohail (21 Dec 2013) -- End
                    'Sohail (26 Aug 2013) -- End
                    mdtLoanList.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
                    'Sohail (21 Dec 2013) -- Start
                    'Enhancement - Loan Register on ESS
                    'ElseIf cboReportType.SelectedIndex = 2 Then 'Saving

                    'Nilay (28-Aug-2015) -- Start
                    mdtLoanList.AcceptChanges()
                    'Nilay (28-Aug-2015) -- End

                ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingReport Then 'Saving
                    'Sohail (21 Dec 2013) -- End
                    mdtSavingList.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
                    'Nilay (28-Aug-2015) -- Start
                    mdtSavingList.AcceptChanges()
                    'Nilay (28-Aug-2015) -- End

                    'Pinkal (12-Jun-2014) -- Start
                    'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                    'Sohail (04 Mar 2016) -- Start
                    'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                    'ElseIf CInt(cboReportType.SelectedValue) = 4 Then  'Loan Scheme With Bank Account Report
                ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct Then  'Loan Scheme With Bank Account Report
                    'Sohail (04 Mar 2016) -- End

                    If CType(gvScheme.Rows(gvRow.RowIndex).FindControl("chkSelect"), CheckBox).Checked = True Then

                        For i As Integer = 0 To gvScheme.Rows.Count - 1
                            If i <> gvRow.RowIndex Then
                                mdtLoanList.Rows(i).Item("IsChecked") = False
                                CType(gvScheme.Rows(i).FindControl("chkSelect"), CheckBox).Checked = False
                            Else
                                mdtLoanList.Rows(i).Item("IsChecked") = True
                                CType(gvScheme.Rows(i).FindControl("chkSelect"), CheckBox).Checked = True
                            End If
                        Next
                        mdtLoanList.AcceptChanges()
                    End If
                    'Pinkal (12-Jun-2014) -- End

                    'Sohail (04 Mar 2016) -- Start
                    'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct Then

                    If CType(gvScheme.Rows(gvRow.RowIndex).FindControl("chkSelect"), CheckBox).Checked = True Then

                        For i As Integer = 0 To gvScheme.Rows.Count - 1
                            If i <> gvRow.RowIndex Then
                                mdtSavingList.Rows(i).Item("IsChecked") = False
                                CType(gvScheme.Rows(i).FindControl("chkSelect"), CheckBox).Checked = False
                            Else
                                mdtSavingList.Rows(i).Item("IsChecked") = True
                                CType(gvScheme.Rows(i).FindControl("chkSelect"), CheckBox).Checked = True
                            End If
                        Next
                        mdtSavingList.AcceptChanges()
                    End If

                    'Sohail (04 Mar 2016) -- End

                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0

            'Nilay (21-Apr-2016) -- Start
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                cboEmployee.SelectedValue = "0"
            End If
            'Nilay (21-Apr-2016) -- End

            cboPeriod.SelectedValue = "0"

            'objLoan.setDefaultOrderBy(cboReportType.SelectedIndex)
            'txtOrderBy.Text = objLoan.OrderByDisplay


            chkInactiveemp.Checked = False

            cboBranch.SelectedValue = "0"

            chkShowInterestInfo.Checked = False

            chkIgnoreZeroSavingDeduction.Checked = False

            chkShowLoanReceipt.Checked = False

            'mstrAdvanceFilter = ""
            'Sohail (15 May 2020) -- Start
            'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report.
            mstrAdvanceFilter = ""
            'Sohail (15 May 2020) -- End

            'SHANI (29 JUN 2015) -- Start
            ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
            chkShowOnHold.Checked = False
            'SHANI (29 JUN 2015) -- End 

            'Sohail (17 Feb 2017) -- Start
            'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
            chkShowPrincipleBFCF.Checked = False
            'Sohail (17 Feb 2017) -- End

            'Nilay (28-Aug-2015) -- Start
            cboBankName.SelectedValue = "0"
            cboBankBranch.SelectedValue = "0"

            'For Each gvRow As GridViewRow In gvScheme.Rows
            '    Dim chkSelectAll As CheckBox = CType(gvRow.FindControl("chkSelectAll"), CheckBox)
            '    chkSelectAll.Checked = False
            '    Dim chkSelect As CheckBox = CType(gvRow.FindControl("chkSelect"), CheckBox)
            '    chkSelect.Checked = False
            'Next
            'Nilay (28-Aug-2015) -- End

            'Nilay (18-Jan-2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            cboStatus.SelectedValue = "0"
            'Nilay (18-Jan-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (04 Mar 2016) -- Start
    'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
    Public Function Valiadation() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Period is compulsory information.Please Select Period."), Me)
                Return False
            ElseIf CInt(cboBankName.SelectedValue) > 0 AndAlso CInt(cboBankBranch.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Bank Branch is compulsory information.Please Select Bank Branch."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Valiadation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
    'Sohail (04 Mar 2016) -- End

#End Region

#Region " Combobox's Events "
    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try

            'Nilay (18-Jan-2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            cboStatus.SelectedValue = "0"
            cboStatus.Visible = False
            lblStatus.Visible = False
            'Nilay (18-Jan-2016) -- End

            'Sohail (13 Dec 2016) -- Start
            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
            cboPeriod.Enabled = True
            'Sohail (13 Dec 2016) -- End

            'Sohail (17 Feb 2017) -- Start
            'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
            chkShowPrincipleBFCF.Checked = False
            chkShowPrincipleBFCF.Visible = False
            'Sohail (17 Feb 2017) -- End

            'Sohail (21 Dec 2013) -- Start
            'Enhancement - Loan Register on ESS
            'If cboReportType.SelectedIndex = 0 Then 'Loan 
            If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport Then 'Loan 
                'Sohail (21 Dec 2013) -- End
                cboReportType.DataSource = Nothing

                pnlScheme.Visible = True
                'lblSchemes.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                'objbtnReportTypeSearch.Visible = False
                'mblnIsLoanReportType = True

                pnlLoanReceipt.Visible = True
                pnlInterestInfo.Visible = True

                'SHANI (29 JUN 2015) -- Start
                ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                pnlShowOnHold.Visible = True
                chkShowOnHold.Checked = False
                'SHANI (29 JUN 2015) -- End 

                chkIgnoreZeroSavingDeduction.Checked = False
                pnlSaving.Visible = False

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End

                'Nilay (28-Aug-2015) -- Start
                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Nilay (28-Aug-2015) -- End

                'Nilay (18-Jan-2016) -- Start
                'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
                cboStatus.Visible = True
                lblStatus.Visible = True
                'Nilay (18-Jan-2016) -- End
                cboStatus.SelectedValue = CInt(enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED).ToString 'Sohail (24 Feb 2016) 

                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'ElseIf cboReportType.SelectedIndex = 1 Then 'Advance
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.AdvanceReport Then 'Advance
                'Sohail (21 Dec 2013) -- End
                'cboScheme.DataSource = Nothing
                'lblSchemes.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " ")

                'objbtnReportTypeSearch.Visible = False

                chkShowInterestInfo.Checked = False
                chkShowLoanReceipt.Checked = False
                pnlLoanReceipt.Visible = False
                pnlInterestInfo.Visible = False

                chkIgnoreZeroSavingDeduction.Checked = False
                pnlSaving.Visible = False
                pnlScheme.Visible = False

                'SHANI (29 JUN 2015) -- Start
                ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                pnlShowOnHold.Visible = False
                chkShowOnHold.Checked = False
                'SHANI (29 JUN 2015) -- End 

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankName.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End

                'Nilay (28-Aug-2015) -- Start
                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Nilay (28-Aug-2015) -- End

                'txtSearchScheme.Text = ""
                'lvSchemes.Items.Clear()

                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'ElseIf cboReportType.SelectedIndex = 2 Then 'Saving
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingReport Then 'Saving
                'Sohail (21 Dec 2013) -- End

                pnlScheme.Visible = True
                'lblSchemes.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Saving Scheme")
                Call FillSavingScheme()

                'objbtnReportTypeSearch.Visible = False
                'mblnIsLoanReportType = False

                pnlInterestInfo.Visible = True
                chkShowLoanReceipt.Checked = False
                pnlLoanReceipt.Visible = False
                'Nilay (28-Aug-2015) -- Start
                'chkIgnoreZeroSavingDeduction.Visible = True
                chkIgnoreZeroSavingDeduction.Checked = False
                pnlSaving.Visible = True
                'Nilay (28-Aug-2015) -- End


                'SHANI (29 JUN 2015) -- Start
                ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                pnlShowOnHold.Visible = False
                chkShowOnHold.Checked = False
                'SHANI (29 JUN 2015) -- End 

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankBranch.SelectedIndex = 0
                cboBankName.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End

                'Sohail (26 Aug 2013) -- Start
                'TRA - ENHANCEMENT

                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'ElseIf cboReportType.SelectedIndex = 3 Then 'Loan Register
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport Then 'Loan Register
                'Sohail (21 Dec 2013) -- End
                cboReportType.DataSource = Nothing

                pnlScheme.Visible = True
                Call FillLoanScheme()

                chkShowInterestInfo.Checked = False
                chkShowLoanReceipt.Checked = False
                pnlLoanReceipt.Visible = False
                pnlInterestInfo.Visible = False

                chkIgnoreZeroSavingDeduction.Checked = False
                pnlSaving.Visible = False
                'Sohail (26 Aug 2013) -- End

                'SHANI (29 JUN 2015) -- Start
                ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.

                'Nilay (28-Aug-2015) -- Start
                'pnlShowOnHold.Visible = False
                'chkShowOnHold.Checked = False
                pnlShowOnHold.Visible = True
                chkShowOnHold.Checked = False

                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Nilay (28-Aug-2015) -- End

                'SHANI (29 JUN 2015) -- End 

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                'Nilay (18-Feb-2016) -- Start
                lblStatus.Visible = True
                cboStatus.Visible = True
                'Nilay (18-Feb-2016) -- End

                'Nilay (21-Apr-2016) -- Start
                'ElseIf cboReportType.SelectedIndex = 4 Then 'Loan Scheme With Bank Account Report
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport Then 'Loan Scheme With Bank Account Report
                'Nilay (21-Apr-2016) -- End

                Call FillLoanScheme()
                lblBankName.Visible = True
                cboBankName.Visible = True
                LblBankBranch.Visible = True
                cboBankBranch.Visible = True
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End
                'SHANI (29 JUN 2015) -- Start
                ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                pnlShowOnHold.Visible = False
                chkShowOnHold.Checked = False
                'SHANI (29 JUN 2015) -- End 

                'Nilay (28-Aug-2015) -- Start
                pnlInterestInfo.Visible = True
                chkShowInterestInfo.Checked = False
                pnlLoanReceipt.Visible = True
                chkShowLoanReceipt.Checked = False
                pnlSaving.Visible = False
                'Nilay (28-Aug-2015) -- End

                'Nilay (18-Feb-2016) -- Start
                lblStatus.Visible = True
                cboStatus.Visible = True
                'Nilay (18-Feb-2016) -- End

                'Nilay (28-Aug-2015) -- Start

                'Nilay (21-Apr-2016) -- Start
                'ElseIf cboReportType.SelectedIndex = 5 Then 'Loan History
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport Then 'Loan History
                'Nilay (21-Apr-2016) -- End

                pnlScheme.Visible = True
                Call FillLoanScheme()

                pnlInterestInfo.Visible = False
                pnlLoanReceipt.Visible = False
                pnlSaving.Visible = False
                pnlShowOnHold.Visible = False

                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Nilay (28-Aug-2015) -- End

                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                'Nilay (21-Apr-2016) -- Start
                'ElseIf cboReportType.SelectedIndex = 6 Then 'Loan Byproduct
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct Then 'Loan Byproduct
                'Nilay (21-Apr-2016) -- End

                Call FillLoanScheme()

                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0


                chkShowOnHold.Checked = False
                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                cboStatus.Visible = True
                lblStatus.Visible = True
                cboStatus.SelectedValue = CInt(enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED).ToString

                pnlInterestInfo.Visible = False
                pnlLoanReceipt.Visible = False
                pnlShowOnHold.Visible = False
                chkIgnoreZeroSavingDeduction.Checked = False
                pnlSaving.Visible = False

                'Nilay (21-Apr-2016) -- Start
                'ElseIf cboReportType.SelectedIndex = 7 Then 'Saving Byproduct
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct Then 'Saving Byproduct
                'Nilay (21-Apr-2016) -- End

                Call FillSavingScheme()

                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                pnlInterestInfo.Visible = False
                pnlLoanReceipt.Visible = False
                pnlShowOnHold.Visible = False
                chkIgnoreZeroSavingDeduction.Checked = True
                pnlSaving.Visible = True
                'Sohail (04 Mar 2016) -- End

                'Sohail (13 Dec 2016) -- Start
                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport Then 'Loan Repayment Schedule Report

                Call FillLoanScheme()

                lblBankName.Visible = False
                cboBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0


                chkShowOnHold.Checked = False
                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True
                lblCurrency.Visible = True

                pnlInterestInfo.Visible = False
                pnlLoanReceipt.Visible = False
                pnlShowOnHold.Visible = False
                chkIgnoreZeroSavingDeduction.Checked = False
                pnlSaving.Visible = False

                cboPeriod.Enabled = False
                cboPeriod.SelectedValue = "0"
                'Sohail (13 Dec 2016) - End
                'Sohail (17 Feb 2017) -- Start
                'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
                chkShowPrincipleBFCF.Visible = True
                'Sohail (17 Feb 2017) -- End

                'Nilay (08 Apr 2017) -- Start
                'Enhancements: New Loan Tracking Report for PACRA
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanTrackingReport Then

                Call FillLoanScheme()

                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False
                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False

                chkShowOnHold.Visible = True
                chkShowOnHold.Checked = False

                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False

                lblBankName.Visible = False
                cboBankName.Visible = False

                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                lblCurrency.Visible = True
                cboCurrency.SelectedValue = CStr(mintBaseCurrId)
                cboCurrency.Visible = True

                lblStatus.Visible = True
                cboStatus.Visible = True
                cboStatus.SelectedValue = CStr(enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED)

                cboPeriod.Enabled = True
                cboPeriod.SelectedValue = "0"

                chkShowPrincipleBFCF.Visible = False
                'Nilay (08 Apr 2017) -- End
            End If

            'objLoan.setDefaultOrderBy(cboReportType.SelectedIndex)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboReportType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (12-Jun-2014) -- Start
    'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

    Protected Sub cboBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        Try
            Dim objBankBranch As New clsbankbranch_master
            Dim dsList As DataSet = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
            With cboBankBranch
                .DataValueField = "branchunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboBankName_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (12-Jun-2014) -- End

    'Nilay (08 Apr 2017) -- Start
    'Enhancements: New Loan Tracking Report for PACRA
    Protected Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) >= 0 Then
                If CInt(cboStatus.SelectedValue) > 0 Then
                    Select Case CInt(cboReportType.SelectedIndex)
                        Case enLoanAdvanceSavindReportType.LoanReport, enLoanAdvanceSavindReportType.LoanTrackingReport
                            chkShowOnHold.Checked = True
                    End Select
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStatus_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (08 Apr 2017) -- End


#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim objLoan As New clsLoanAdvanceSaving(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            'Sohail (26 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'If (cboReportType.SelectedIndex = 0) AndAlso mdtLoanList.Select("IsChecked = 1").Length <= 0 Then '0=Loan or 2=Saving
            'Sohail (21 Dec 2013) -- Start
            'Enhancement - Loan Register on ESS
            'If (cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3) AndAlso mdtLoanList.Select("IsChecked = 1").Length <= 0 Then '0=Loan or 2=Saving or Loan Register

            'Nilay (28-Aug-2015) -- Start
            'If (CInt(cboReportType.SelectedValue) = 0 OrElse CInt(cboReportType.SelectedValue) = 3 OrElse CInt(cboReportType.SelectedValue) = 4) AndAlso mdtLoanList.Select("IsChecked = 1").Length <= 0 Then '0=Loan or 2=Saving or Loan Register
            'Sohail (04 Mar 2016) -- Start
            'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
            'If (CInt(cboReportType.SelectedValue) = 0 OrElse CInt(cboReportType.SelectedValue) = 2 OrElse CInt(cboReportType.SelectedValue) = 3 OrElse _
            '                                                  CInt(cboReportType.SelectedValue) = 4 OrElse CInt(cboReportType.SelectedValue) = 5) _
            '                                                  AndAlso (mdtLoanList.Select("IsChecked = True").Length <= 0 _
            '                                                           AndAlso mdtSavingList.Select("IsChecked = True").Length <= 0) Then
            If (CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingReport OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport OrElse _
                CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanTrackingReport) AndAlso _
                (mdtLoanList.Select("IsChecked = True").Length <= 0 AndAlso mdtSavingList.Select("IsChecked = True").Length <= 0) Then
                'Nilay (08 Apr 2017) -- [CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanTrackingReport]

                'Sohail (04 Mar 2016) -- End
                'If (mdtLoanList.Select("IsChecked = True").Length <= 0 AndAlso mdtSavingList.Select("IsChecked = True").Length <= 0) Then


                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]  [OrElse CInt(cboReportType.SelectedValue) = 4]

                'Sohail (21 Dec 2013) -- End
                'Sohail (26 Aug 2013) -- End

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please select atleast One Scheme from the list."), Me)
                Exit Try
                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'ElseIf (cboReportType.SelectedIndex = 2) AndAlso mdtSavingList.Select("IsChecked = 1").Length <= 0 Then '0=Loan or 2=Saving
                'End If
                'Nilay (28-Aug-2015) -- End

                'Nilay (28-Aug-2015) -- Start
                'ElseIf (CInt(cboReportType.SelectedValue) = 2) AndAlso mdtSavingList.Select("IsChecked = 1").Length <= 0 Then '0=Loan or 2=Saving
                '    'Sohail (21 Dec 2013) -- End
                '    'Anjan [04 June 2014] -- Start
                '    'ENHANCEMENT : Implementing Language,requested by Andrew
                '    'Language.setLanguage(mstrModuleName)
                '    'Anjan [04 June 2014 ] -- End

                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Please select atleast One Scheme from the list."), Me)
                '    Exit Try
                'Nilay (28-Aug-2015) -- End

            End If


            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

            'Nilay (21-Apr-2016) -- Start
            'If CInt(cboReportType.SelectedIndex) = 4 Then
            If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport Then
                'Nilay (21-Apr-2016) -- End

                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'If CInt(cboPeriod.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage("Period is compulsory information.Please Select Period.", Me)
                '    Exit Try
                'ElseIf CInt(cboBankName.SelectedValue) > 0 AndAlso CInt(cboBankBranch.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage("Bank Branch is compulsory information.Please Select Bank Branch.", Me)
                '    Exit Try
                'End If
                If Valiadation() = False Then Exit Try

                'Nilay (21-Apr-2016) -- Start
                'ElseIf CInt(cboReportType.SelectedIndex) = 6 OrElse CInt(cboReportType.SelectedIndex) = 7 Then
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct Then
                'Nilay (21-Apr-2016) -- End

                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Period is compulsory information.Please Select Period."), Me)
                    cboPeriod.Focus()
                    Exit Sub
                End If
                'Sohail (04 Mar 2016) -- End
            End If
            'Pinkal (12-Jun-2014) -- End


            objLoan.SetDefaultValue()
            'Sohail (21 Dec 2013) -- Start
            'Enhancement - Loan Register on ESS
            'objLoan.setDefaultOrderBy(cboReportType.SelectedIndex)
            objLoan.setDefaultOrderBy(CInt(cboReportType.SelectedValue))
            'Sohail (21 Dec 2013) -- End

            objLoan._PeriodId = CInt(cboPeriod.SelectedValue)
            objLoan._PeriodName = cboPeriod.SelectedItem.Text

            'objLoan._SchemeId = cboScheme.SelectedValue
            'objLoan._SchemeName = cboScheme.Text

            objLoan._EmployeeId = CInt(cboEmployee.SelectedValue)
            objLoan._EmployeeName = cboEmployee.SelectedItem.Text

            'Sohail (21 Dec 2013) -- Start
            'Enhancement - Loan Register on ESS
            'objLoan._ReportId = cboReportType.SelectedIndex
            objLoan._ReportId = CInt(cboReportType.SelectedValue)
            'Sohail (21 Dec 2013) -- End
            objLoan._ReportTypeName = cboReportType.SelectedItem.Text


            objLoan._IsActive = chkInactiveemp.Checked

            objLoan._BranchId = CInt(cboBranch.SelectedValue)
            objLoan._Branch_Name = cboBranch.Text

            objLoan._ShowInterestInformation = chkShowInterestInfo.Checked
            objLoan._ShowLoanReceipt = chkShowLoanReceipt.Checked


            'SHANI (29 JUN 2015) -- Start
            ''Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
            objLoan._ShowOnHold = chkShowOnHold.Checked
            'SHANI (29 JUN 2015) -- End 

            'Sohail (17 Feb 2017) -- Start
            'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
            objLoan._ShowPrincipleBFCF = chkShowPrincipleBFCF.Checked
            'Sohail (17 Feb 2017) -- End

            'Nilay (28-Aug-2015) -- Start
            objLoan._BaseCurrencySign = mstrBaseCurrSign
            objLoan._CurrencyId = CInt(cboCurrency.SelectedValue)
            objLoan._CurrencyName = cboCurrency.Text
            'Nilay (28-Aug-2015) -- End

            objLoan._IgnoreZeroSavingDeduction = chkIgnoreZeroSavingDeduction.Checked
            Dim strIDs As String = ""
            Dim strName As String = "" 'Sohail (26 Aug 2013)
            'Sohail (26 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'If cboReportType.SelectedIndex = 0 Then 'Loan 
            'Sohail (21 Dec 2013) -- Start
            'Enhancement - Loan Register on ESS
            'If cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3 Then 'Loan 
            'Sohail (04 Mar 2016) -- Start
            'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
            'If CInt(cboReportType.SelectedValue) = 0 OrElse CInt(cboReportType.SelectedValue) = 3 OrElse CInt(cboReportType.SelectedValue) = 4 Then 'Loan 
            If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport Then 'Loan 
                'Sohail (04 Mar 2016) -- End
                'Pinkal (12-Jun-2014) -- 'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes] [ OrElse CInt(cboReportType.SelectedValue) = 4]

                'Sohail (21 Dec 2013) -- End
                'Sohail (26 Aug 2013) -- End
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'For Each dsRow As DataRow In mdtLoanList.Select("IsChecked = 1")
                '    If strIDs = "" Then
                '        strIDs = dsRow.Item("id").ToString
                '        strName = dsRow.Item("name").ToString 'Sohail (26 Aug 2013)
                '    Else
                '        strIDs &= ", " & dsRow.Item("id").ToString
                '        strName &= ", " & dsRow.Item("name").ToString 'Sohail (26 Aug 2013)
                '    End If
                'Next
                strIDs = String.Join(",", (From p In mdtLoanList.Select("IsChecked = 1") Select (p.Item("id").ToString)).ToArray)
                strName = String.Join(",", (From p In mdtLoanList.Select("IsChecked = 1") Select (p.Item("name").ToString)).ToArray)
                'Sohail (04 Mar 2016) -- End
                'Sohail (21 Dec 2013) -- Start
                'Enhancement - Loan Register on ESS
                'ElseIf cboReportType.SelectedIndex = 2 Then 'Saving
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'ElseIf CInt(cboReportType.SelectedValue) = 2 Then 'Saving
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct Then 'Saving
                'Sohail (04 Mar 2016) -- End
                'Sohail (21 Dec 2013) -- End
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'For Each dsRow As DataRow In mdtSavingList.Select("IsChecked = 1")
                '    If strIDs = "" Then
                '        strIDs = dsRow.Item("id").ToString
                '        strName = dsRow.Item("name").ToString 'Sohail (26 Aug 2013)
                '    Else
                '        strIDs &= ", " & dsRow.Item("id").ToString
                '        strName &= ", " & dsRow.Item("name").ToString 'Sohail (26 Aug 2013)
                '    End If
                'Next
                strIDs = String.Join(",", (From p In mdtSavingList.Select("IsChecked = 1") Select (p.Item("id").ToString)).ToArray)
                strName = String.Join(",", (From p In mdtSavingList.Select("IsChecked = 1") Select (p.Item("name").ToString)).ToArray)
                'Sohail (04 Mar 2016) -- End
            End If
            objLoan._SchemeListIDs = strIDs
            objLoan._SchemeName = strName 'Sohail (26 Aug 2013)

            'Nilay (10-Feb-2016) -- Start
            Dim dtPeriodStartDate As Date
            Dim dtPeriodEndDate As Date
            'Nilay (10-Feb-2016) -- End

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                objLoan._PeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                objLoan._PeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (10-Feb-2016) -- Start
                dtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                dtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (10-Feb-2016) -- End
            Else
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue) 'Nilay (15-Dec-2015) -- objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objLoan._PeriodStartDate = objPeriod._Start_Date
                objLoan._PeriodEndDate = objPeriod._End_Date
                'Nilay (10-Feb-2016) -- Start
                dtPeriodStartDate = objPeriod._Start_Date
                dtPeriodEndDate = objPeriod._End_Date
                'Nilay (10-Feb-2016) -- End
                objPeriod = Nothing
            End If

            'objLoan._Advance_Filter = mstrAdvanceFilter

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            objLoan._BankGroupId = CInt(cboBankName.SelectedValue)
            objLoan._BankGroup = cboBankName.SelectedItem.Text
            objLoan._BankBranchId = CInt(cboBankBranch.SelectedValue)
            objLoan._BankBranch = cboBankBranch.SelectedItem.Text
            'Pinkal (12-Jun-2014) -- End

            objLoan._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objLoan._UserUnkId = CInt(Session("UserId"))
            objLoan._YearUnkId = CInt(Session("Fin_year"))
            objLoan._DatabaseStartDate = CDate(Session("fin_startdate"))
            objLoan._DatabaseEndDate = CDate(Session("fin_enddate"))
            objLoan._UserAccessLevel = CStr(Session("UserAccessLevel"))
            objLoan._AccessLevelFilterString = CStr(Session("AccessLevelFilterString"))

            'Nilay (18-Jan-2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            objLoan._LoanStatusId = CInt(cboStatus.SelectedValue)
            'Sohail (07 Mar 2016) -- Start
            'Enhancement - Show Report Name on the reports when Logo only option ticked.
            'objLoan._LoanStatusName = cboStatus.Text
            objLoan._LoanStatusName = cboStatus.SelectedItem.Text
            'Sohail (07 Mar 2016) -- End
            'Nilay (18-Jan-2016) -- End

            'Sohail (15 May 2020) -- Start
            'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report in self service.
            objLoan._Advance_Filter = mstrAdvanceFilter
            'Sohail (15 May 2020) -- End

            'Nilay (21-Apr-2016) -- Start
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                objLoan._ApplyUserAccessFilter = False
            End If
            'Nilay (21-Apr-2016) -- End

            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'objLoan.generateReport(0, enPrintAction.None, enExportAction.None)

            'Nilay (10-Feb-2016) -- Start
            'objLoan.generateReportNew(Session("Database_Name"), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), _
            '                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                         Session("UserAccessModeSetting"), True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
            '                         0, enPrintAction.None, enExportAction.None)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Pinkal (01-Dec-2016) -- Start
            'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
            If CInt(Session("Employeeunkid")) > 0 Then
                objLoan._ESSName = Session("DisplayName").ToString()
            End If
            'Pinkal (01-Dec-2016) -- End


            objLoan.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                     dtPeriodStartDate, dtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, _
                                     Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                     0, enPrintAction.None, enExportAction.None)
            'Nilay (10-Feb-2016) -- End

            'Session("objRpt") = objLoan._Rpt
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            Session("objRpt") = objLoan._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Nilay (15-Dec-2015) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (15 May 2020) -- Start
    'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report.
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (15 May 2020) -- End

#End Region

    'Sohail (15 May 2020) -- Start
    'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report.
#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Sohail (15 May 2020) -- End

#Region "TextBox's Events"

    Protected Sub txtSearchScheme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchScheme.TextChanged
        Try
            'Sohail (04 Mar 2016) -- Start
            'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
            'If CInt(cboReportType.SelectedValue) = 0 Then
            If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport Then
                'Sohail (04 Mar 2016) -- End
                If mdtLoanList IsNot Nothing Then
                    Dim dvLoanList As DataView = mdtLoanList.DefaultView
                    If dvLoanList.Table.Rows.Count > 0 Then
                        dvLoanList.RowFilter = "Code like '%" & txtSearchScheme.Text.Trim & "%' OR name like '%" & txtSearchScheme.Text.Trim & "%' "
                        gvScheme.DataSource = dvLoanList.ToTable
                        gvScheme.DataBind()
                    End If
                End If
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'ElseIf CInt(cboReportType.SelectedValue) = 2 Then
            ElseIf CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct Then
                'Sohail (04 Mar 2016) -- End
                If mdtSavingList IsNot Nothing Then
                    Dim dvSavingList As DataView = mdtSavingList.DefaultView
                    If dvSavingList.Table.Rows.Count > 0 Then
                        dvSavingList.RowFilter = "Code like '%" & txtSearchScheme.Text.Trim & "%' OR name like '%" & txtSearchScheme.Text.Trim & "%' "
                        gvScheme.DataSource = dvSavingList.ToTable
                        gvScheme.DataBind()
                    End If
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearchScheme_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Sohail (21 Dec 2013) -- Start
    'Enhancement - Loan Register on ESS
#Region " GridView Event "
    Protected Sub gvScheme_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvScheme.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                    e.Row.Visible = False
                Else
                    'e.Row.Cells(2).Text = CDec(e.Row.Cells(2).Text).ToString(Session("fmtcurrency"))
                End If
            End If

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            'Sohail (04 Mar 2016) -- Start
            'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
            'If CInt(cboReportType.SelectedValue) = 4 Then
            If CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedValue) = enLoanAdvanceSavindReportType.SavingByproduct Then
                'Sohail (04 Mar 2016) -- End
                CType(gvScheme.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
                CType(gvScheme.HeaderRow.FindControl("chkSelectAll"), CheckBox).Enabled = False
            Else
                CType(gvScheme.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
                CType(gvScheme.HeaderRow.FindControl("chkSelectAll"), CheckBox).Enabled = True
            End If
            'Pinkal (12-Jun-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("gvScheme_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (12-Jun-2014) -- End

#End Region
    'Sohail (21 Dec 2013) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReportType.ID, Me.lblReportType.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.lblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBranch.ID, Me.lblBranch.Text)
            Me.chkShowInterestInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowInterestInfo.ID, Me.chkShowInterestInfo.Text)
            Me.chkIgnoreZeroSavingDeduction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIgnoreZeroSavingDeduction.ID, Me.chkIgnoreZeroSavingDeduction.Text)
            Me.chkShowLoanReceipt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowLoanReceipt.ID, Me.chkShowLoanReceipt.Text)
            Me.gvScheme.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvScheme.Columns(3).FooterText, Me.gvScheme.Columns(3).HeaderText)
            Me.gvScheme.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvScheme.Columns(2).FooterText, Me.gvScheme.Columns(2).HeaderText)
            Me.chkShowPrincipleBFCF.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowPrincipleBFCF.ID, Me.chkShowPrincipleBFCF.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014 ] -- End



End Class
