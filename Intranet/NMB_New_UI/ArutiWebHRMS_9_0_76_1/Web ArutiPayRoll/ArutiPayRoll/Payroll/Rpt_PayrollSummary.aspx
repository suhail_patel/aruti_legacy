﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_PayrollSummary.aspx.vb"
    Inherits="Payroll_rpt_PayrollSummary" Title="Payroll Summary Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>
--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Summary Report" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <asp:Label ID="lblExRate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkInactiveemp" Text="Include Inactive Employee" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkIgnorezeroHead" Text="Ignore Zero Value Heads" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowEmployersContribution" Text="Show Employers Contribution"
                                                            runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowInformational" Text="Show Informational Heads" runat="server"
                                                            AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowEachAnalysisOnNewPage" Text="Show Analysis On New Page"
                                                            runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                                <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblInfoHeadHeader" runat="server" Text="Informational Heads" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkSave" runat="server" Text="Save Settings" ToolTip="Save Settings"
                                                            Visible="false">
                                                    <i class="far fa-save"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <asp:Panel ID="pnlInfoheads" runat="server" class="panel-default">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true" placeholder="Search Heads"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="table-responsive" style="max-height: 300px;">
                                                                <asp:GridView ID="dgHeads" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="tranheadunkid"
                                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged"
                                                                                    Text=" " />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                    OnCheckedChanged="chkHeadSelect_OnCheckedChanged" Text=" " />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                            FooterText="dgColhHeadCode" />
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                            FooterText="dgColhHeadName" />
                                                                        <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                                        <%--<asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                                        Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                                                    <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                                        Text="&#x25BC;" OnClick="ChangeTranHeadLocation" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
