﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Payroll_wPg_PayslipGlobalpayment
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mintPaidCurrId As Integer
    Private mdecPaidExRate As Decimal
    Private mdecTotAmt As Decimal = 0

    Private dtPayment As DataTable
    Private mdtTable As DataTable
    Private mdtDenomTable As DataTable

    Private mstrDenomeFormat As String = "#####################.##"
    Private mstrCashEmpIDs As String = ""

    Private mintBankPaid As Integer = 0
    Private mintCashPaid As Integer = 0
    Private decBankPaid As Decimal = 0
    Private decCashPaid As Decimal = 0

    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date

    Private mstrAdvanceFilter As String = "" 'Sohail (04 Jan 2014)

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPayslipGlobalPayment"
    Private ReadOnly mstrModuleName1 As String = "frmCashDenomination"
    'Anjan [04 June 2014] -- End

    'Nilay (28-Aug-2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mintBaseCountryId As Integer = 0
    Private mdtPaymentDatePeriodStartDate As DateTime
    Private mdtPaymentDatePeriodEndDate As DateTime
    'Nilay (28-Aug-2015) -- End
    Private dtActAdj As New DataTable 'Sohail (23 May 2017)
#End Region

#Region " Private Functions & Methods "

    Private Sub SetVisibility()
        Try
            btnProcess.Enabled = CBool(Session("AllowGlobalPayment"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPayPoint As New clspaypoint_master
        Dim objCommon As New clsCommon_Master
        'Sohail (04 Jan 2014) -- Start
        'Enhancement - Oman
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objAccess As New clsAccess
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objService As New clsServices
        'Dim objJob As New clsJobs
        'Dim objUnit As New clsUnits
        Dim objBank As New clspayrollgroup_master
        'Sohail (04 Jan 2014) -- End
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombo As DataSet
        Dim objExRate As New clsExchangeRate
        'Nilay (28-Aug-2015) -- Start
        Dim objPeriod As New clscommom_period_Tran
        'Nilay (28-Aug-2015) -- End
        Dim intFirstPeriodID As Integer = 0 'Sohail (24 Apr 2019)
        Try

            'Sohail (24 Apr 2019) -- Start
            'Enhancement - 76.1 - Set first open period in period selection on global payslip payment screen.
            intFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN)
            'Sohail (24 Apr 2019) -- End

            'Nilay (28-Aug-2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.

            'Nilay (15-Dec-2015) -- Start
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, 1, False)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "PayPeriod", True, 1, False)
            'Nilay (15-Dec-2015) -- End

            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                .DataBind()
                'Sohail (24 Apr 2019) -- Start
                'NPK Issue - Support Issue Id # 0003771 - 76.1 - Not Able to delete the advance after voiding payroll payment of March (due to transaction was not getting voided from loan_balance_tran on voiding payment).
                'If .Items.Count > 0 Then .SelectedValue = "0"
                If .Items.Count > 0 Then .SelectedValue = intFirstPeriodID.ToString
                'Sohail (24 Apr 2019) -- End
            End With

            With cboPaymentDatePeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayPeriod").Copy
                .DataBind()
                'Sohail (24 Apr 2019) -- Start
                'NPK Issue - Support Issue Id # 0003771 - 76.1 - Not Able to delete the advance after voiding payroll payment of March (due to transaction was not getting voided from loan_balance_tran on voiding payment).
                'If .Items.Count > 0 Then .SelectedValue = "0"
                If .Items.Count > 0 Then .SelectedValue = intFirstPeriodID.ToString
                Call cboPaymentDatePeriod_SelectedIndexChanged(cboPaymentDatePeriod, New System.EventArgs)
                'Sohail (24 Apr 2019) -- End
            End With
            'Nilay (28-Aug-2015) -- End

            dsCombo = objPayPoint.getListForCombo("PayPointList", True)
            With cboPayPoint
                .DataValueField = "paypointunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("PayPointList")
                .DataBind()
            End With

            'Sohail (11 Jan 2014) -- Start
            'Enhancement - PayPoint, PayType filter on Global Payment
            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            With cboPayType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("PayType")
                .DataBind()
            End With
            'Sohail (11 Jan 2014) -- End

            'Sohail (04 Jan 2014) -- Start
            'Enhancement - Oman
            'dsCombo = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .DataValueField = "departmentunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("DepartmentList")
            '    .DataBind()
            'End With

            'dsCombo = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .DataValueField = "gradeunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("GradeList")
            '    .DataBind()
            'End With

            'dsCombo = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .DataValueField = "sectionunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("SectionList")
            '    .DataBind()
            'End With

            'dsCombo = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .DataValueField = "classesunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("ClassList")
            '    .DataBind()
            'End With

            'dsCombo = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .DataValueField = "CostCenterunkid"
            '    .DataTextField = "CostCentername"
            '    .DataSource = dsCombo.Tables("CostCenterList")
            '    .DataBind()
            'End With

            'dsCombo = objJob.getComboList("JobList", True, , , , , , , Session("UserAccessModeSetting"))
            'With cboJob
            '    .DataValueField = "jobunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("JobList")
            '    .DataBind()
            'End With

            'dsCombo = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .DataValueField = "unitunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("UnitList")
            '    .DataBind()
            'End With
            ''Sohail (04 Jan 2014) -- End

            dsCombo = objMaster.GetPaymentMode("PayMode", True)
            With cboPaymentMode
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayMode")
                .DataBind()
            End With

            dsCombo = objCompanyBank.GetComboList(CInt(Session("Companyunkid")), 1, "BankGrp")
            With cboBankGroup
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("BankGrp")
                .DataBind()
                If .Items.Count > 0 Then
                    Call cboBankGroup_SelectedIndexChanged(Me, New System.EventArgs())
                End If
            End With

            dsCombo = objExRate.getComboList("ExRate", False)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    'Nilay (28-Aug-2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                Else
                    mintBaseCountryId = 0
                    'Nilay (28-Aug-2015) -- End
                End If

                With cboCurrency
                    .DataValueField = "countryunkid"
                    .DataTextField = "currency_sign"
                    .DataSource = dsCombo.Tables("ExRate")
                    .DataBind()
                    'Nilay (28-Aug-2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    If mintBaseCountryId > 0 Then
                        .SelectedValue = CStr(mintBaseCountryId)
                    Else
                        .SelectedIndex = 0
                    End If
                    'Nilay (28-Aug-2015) -- End
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - Authorize payment total not matching with global payment total.
                    If .Items.Count > 0 Then
                        Call cboCurrency_SelectedIndexChanged(Me, New System.EventArgs())
                    End If
                    'Sohail (26 May 2017) -- End
                End With
            End If

            'Nilay (15-Dec-2015) -- Start
            'dsCombo = objMaster.getComboListPAYYEAR("PayYear", True, CInt(Session("Fin_year")), Session("FinancialYear_Name"), CInt(Session("Companyunkid")))
            dsCombo = objMaster.getComboListPAYYEAR(CInt(Session("Fin_year")), CStr(Session("FinancialYear_Name")), CInt(Session("Companyunkid")), "PayYear", True)
            'Nilay (15-Dec-2015) -- End

            With cboPayYear
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayYear")
                .DataBind()
                If .Items.Count > 0 Then
                    Call cboPayYear_SelectedIndexChanged(Me, New System.EventArgs())
                End If
            End With

            'Sohail (04 Jan 2014) -- Start
            'Enhancement - Oman
            dsCombo = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboEmpBank
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                If .Items.Count > 0 Then
                    Call cboEmpBank_SelectedIndexChanged(Me, New System.EventArgs())
                End If
            End With
            'Sohail (04 Jan 2014) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMaster = Nothing
            objPayPoint = Nothing
            objCommon = Nothing
            'Sohail (04 Jan 2014) -- Start
            'Enhancement - Oman
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objAccess = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objService = Nothing
            'objJob = Nothing
            'objUnit = Nothing
            objBank = Nothing
            'Sohail (04 Jan 2014) -- End
            objExRate = Nothing
            objPeriod = Nothing 'Nilay (28-Aug-2015) 
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , , Session("AccessLevelFilterString"))

            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Employee", True)

            dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                mdtPayPeriodStartDate, _
                                                mdtPayPeriodEndDate, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                False, "Employee", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables("Employee")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try

    End Sub

    Private Sub CreateListTable()
        Try
            dtPayment = New DataTable
            With dtPayment
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("amount_tag", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("voucherno", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("tnaleavetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("baseamount", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("roundingadjustment", System.Type.GetType("System.Decimal")).DefaultValue = 0 'Sohail (21 Mar 2014)
                .Columns.Add(New DataColumn("default_costcenterunkid", Type.GetType("System.Int32"))) 'Sohail (30 Oct 2018)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim objEmployee As New clsEmployee_Master
        Dim objPayment As New clsPayment_tran
        Dim objTnALeave As New clsTnALeaveTran
        Dim objEmpBank As New clsEmployeeBanks
        Dim dsEmployee As New DataSet
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim dtTemp As DataTable
        Dim intCount As Integer = 0
        Dim strEmployeeList As String = ""
        Dim strEmpListWithBank As String = ""
        Dim strFilter As String = ""
        Dim decAmt As Decimal = 0
        Dim strVoucherNo As String
        Dim intTnALeaveUnkID As Integer
        Dim strTmp As String = ""
        Dim decTotAmt As Decimal
        Dim decBaseAmt As Decimal
        Dim decRondingMultile As Decimal = 0 'Sohail (03 Dec 2013)
        Dim decTotPaymentAmt As Decimal = 0 'Sohail (04 Jan 2014)
        Dim strEmpFilter As String = "" 'Sohail (11 Jan 2014)

        Try
            dtPayment.Rows.Clear()
            txtTotalPaymentAmount.Text = ""
            If CInt(cboPayPeriod.SelectedValue) <= 0 OrElse CInt(cboPaymentMode.SelectedValue) <= 0 Then Exit Try
            If mdecBaseExRate = 0 AndAlso mdecPaidExRate = 0 Then Exit Try ' [No exchange rate found for current currency for current payment date.]

            'Sohail (11 Jan 2014) -- Start
            'Enhancement - PayPoint, PayType filter on Global Payment
            strEmpFilter = mstrAdvanceFilter
            If CInt(cboPayType.SelectedValue) > 0 Then
                If strEmpFilter.Trim <> "" Then
                    strEmpFilter &= " AND hremployee_master.paytypeunkid = " & CInt(cboPayType.SelectedValue) & " "
                Else
                    strEmpFilter = " hremployee_master.paytypeunkid = " & CInt(cboPayType.SelectedValue) & " "
                End If
            End If
            'Sohail (11 Jan 2014) -- End

            'Sohail (12 Jan 2015) -- Start
            'Enhancement - Allow to employee bank period wise.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If strEmpFilter.Trim <> "" Then
                    strEmpFilter &= " AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                Else
                    strEmpFilter = " hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If
            End If
            'Sohail (12 Jan 2015) -- End

            'Sohail (04 Jan 2014) -- Start
            'Enhancement - Oman
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate _
            '                                          , , , Session("AccessLevelFilterString") _
            '                                        )

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate _
            '                                          , , , Session("AccessLevelFilterString"), , strEmpFilter _
            '                                        )

            'Nilay (10-Feb-2016) -- Start
            'dsEmployee = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    mdtPayPeriodStartDate, _
            '                                    mdtPayPeriodEndDate, _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , _
            '                                    CInt(cboPayPoint.SelectedValue), , , , strEmpFilter)

            dsEmployee = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                mdtPayPeriodStartDate, _
                                                mdtPayPeriodEndDate, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , _
                                                CInt(cboPayPoint.SelectedValue), , , , strEmpFilter)
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End

            'Sohail (04 Jan 2014) -- End

            '*** Get list of Employee from Employee Master
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                'Sohail (04 Jan 2014) -- Start
                'Enhancement - Oman
                'For Each dsRow As DataRow In dsEmployee.Tables("Employee").Rows
                '    With dsRow
                '        If strEmployeeList.Length <= 0 Then
                '            strEmployeeList = .Item("employeeunkid").ToString
                '        Else
                '            strEmployeeList &= "," & .Item("employeeunkid").ToString
                '        End If
                '    End With
                'Next
                Dim allEmp As List(Of String) = (From p In dsEmployee.Tables("Employee") Select (p.Item("employeeunkid").ToString)).ToList
                strEmployeeList = String.Join(",", allEmp.ToArray)
                'Sohail (04 Jan 2014) -- End
            End If


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTnALeave.Get_Balance_List("Balance", , , CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, hremployee_master.firstname", Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"))

            'Nilay (10-Feb-2016) -- Start
            'dsList = objTnALeave.Get_Balance_List(Session("Database_Name"), _
            '                                      Session("UserId"), _
            '                                      Session("Fin_year"), _
            '                                      Session("CompanyUnkId"), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      Session("UserAccessModeSetting"), True, _
            '                                      Session("IsIncludeInactiveEmp"), True, "", _
            '                                      "Balance", "", CInt(cboPayPeriod.SelectedValue), _
            '                                      "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'dsList = objTnALeave.Get_Balance_List(CStr(Session("Database_Name")), _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), _
            '                                      mdtPayPeriodStartDate, _
            '                                      mdtPayPeriodEndDate, _
            '                                      CStr(Session("UserAccessModeSetting")), True, _
            '                                      False, True, "", _
            '                                      "Balance", strEmployeeList, CInt(cboPayPeriod.SelectedValue), _
            '                                      "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")
            dsList = objTnALeave.Get_Balance_List(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  mdtPayPeriodStartDate, _
                                                  mdtPayPeriodEndDate, _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  False, True, "", _
                                                  "Balance", strEmployeeList, CInt(cboPayPeriod.SelectedValue), _
                                                  "prtnaleave_tran.payperiodunkid, emp.employeename")
            'Sohail (30 Oct 2018) -- End
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            '*** Get list of Employee who has bank account
            'Sohail (04 Jan 2014) -- Start
            'Enhancement - Oman
            'strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks
            'Sohail (12 Jan 2015) -- Start
            'Enhancement - Allow to employee bank period wise.
            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks(CInt(cboEmpBank.SelectedValue), CInt(cboEmpBranch.SelectedValue), mdtPayPeriodEndDate)
            Else
                strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks(, , mdtPayPeriodEndDate)
            End If
            'Sohail (12 Jan 2015) -- End
            'Sohail (04 Jan 2014) -- End
            If strEmpListWithBank.Trim.Length > 0 Then
                strTmp = "0,"
            ElseIf strEmpListWithBank.Trim.Length <= 0 Then
                strTmp = "0"
            End If
            strEmpListWithBank = strTmp & strEmpListWithBank
            If strEmployeeList.Trim.Length > 0 Then
                strFilter = "employeeunkid IN (" & strEmployeeList & ")"
                'Sohail (11 Jan 2014) -- Start
                'Enhancement - PayPoint, PayType filter on Global Payment
            Else
                strFilter = " 1 = 2 "
                'Sohail (11 Jan 2014) -- End
            End If
            If CInt(cboPaymentMode.SelectedValue) > 0 Then
                If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH Then
                    If strEmployeeList.Trim.Length > 0 Then
                        strFilter &= " AND employeeunkid NOT IN (" & strEmpListWithBank & ")"
                    ElseIf strEmployeeList.Trim.Length <= 0 Then
                        strFilter = " employeeunkid NOT IN (" & strEmpListWithBank & ")"
                    End If
                ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                    If strEmployeeList.Trim.Length > 0 Then
                        strFilter &= " AND employeeunkid IN (" & strEmpListWithBank & ")"
                    Else
                        strFilter = " employeeunkid IN (" & strEmpListWithBank & ")"
                    End If
                ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
                    'List of Both Bank A/C Holder as well as Non Bank A/C Holder Employee
                End If
            End If

            If strFilter.Trim <> "" Then
                strFilter &= " AND total_amount + openingbalance > 0 "
            Else
                strFilter = " total_amount + openingbalance > 0 "
            End If

            dtTable = New DataView(dsList.Tables("Balance"), strFilter, "employeename", DataViewRowState.CurrentRows).ToTable

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - Oman
            Select Case CInt(Session("PaymentRoundingMultiple"))
                Case enPaymentRoundingMultiple._1
                    decRondingMultile = 1
                Case enPaymentRoundingMultiple._5
                    decRondingMultile = 5
                Case enPaymentRoundingMultiple._10
                    decRondingMultile = 10
                Case enPaymentRoundingMultiple._50
                    decRondingMultile = 50
                Case enPaymentRoundingMultiple._100
                    decRondingMultile = 100
                    'Sohail (08 Apr 2016) -- Start
                    'Issue - 58.1 - Allow Rounding to 200 for Payslip Payment Rounding Multiple option for Mabibo.
                Case enPaymentRoundingMultiple._200
                    decRondingMultile = 200
                    'Sohail (08 Apr 2016) -- End
                Case enPaymentRoundingMultiple._500
                    decRondingMultile = 500
                Case enPaymentRoundingMultiple._1000
                    decRondingMultile = 1000
            End Select
            'Sohail (03 Dec 2013) -- End

            Dim dRow As DataRow

            For Each dtRow As DataRow In dtTable.Rows
                dRow = dtPayment.NewRow

                With dRow
                    .Item("employeeunkid") = dtRow.Item("employeeunkid").ToString
                    .Item("employeecode") = dtRow.Item("employeecode").ToString
                    .Item("employeename") = dtRow.Item("employeename").ToString


                    decAmt = 0
                    decTotAmt = 0
                    strVoucherNo = ""
                    intTnALeaveUnkID = 0
                    If CInt(cboPayPeriod.SelectedValue) > 0 Then

                        dtTemp = New DataView(dtTable, "employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & "", "", DataViewRowState.CurrentRows).ToTable

                        If dtTemp.Rows.Count > 0 Then
                            decAmt = CDec(dtTemp.Rows(0).Item("balanceamount"))
                            decTotAmt = CDec(dtTemp.Rows(0).Item("total_amount"))
                            strVoucherNo = dtTemp.Rows(0).Item("voucherno").ToString
                            intTnALeaveUnkID = CInt(dtTemp.Rows(0).Item("tnaleavetranunkid"))

                            'Sohail (03 Dec 2013) -- Start
                            'Enhancement - Oman
                            'Sohail (17 Mar 2020) -- Start
                            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                            'Select Case CInt(Session("PaymentRoundingType"))
                            '    Case enPaymentRoundingType.NONE
                            '        decAmt = CDec(dtTemp.Rows(0).Item("balanceamount"))
                            '    Case enPaymentRoundingType.AUTOMATIC
                            '        If decRondingMultile / 2 > CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile Then
                            '            decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile)
                            '        Else
                            '            If CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) <> 0 Then
                            '                decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) + decRondingMultile
                            '            End If
                            '        End If
                            '    Case enPaymentRoundingType.UP
                            '        If CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) <> 0 Then
                            '            decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) + decRondingMultile
                            '        End If
                            '    Case enPaymentRoundingType.DOWN
                            '        decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile)
                            'End Select
                            ''Sohail (03 Dec 2013) -- End

                            ''Sohail (21 Mar 2014) -- Start
                            ''Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            'If CDec(Session("CFRoundingAbove")) > 0 AndAlso Math.Abs((CDec(dtTemp.Rows(0).Item("balanceamount")) - decAmt)) <= CDec(Session("CFRoundingAbove")) Then
                            '    .Item("roundingadjustment") = CDec(dtTemp.Rows(0).Item("balanceamount")) - decAmt '*** Rounding Adjustment
                            'Else
                            '    .Item("roundingadjustment") = 0 '*** Rounding Adjustment
                            'End If
                            ''Sohail (21 Mar 2014) -- End

                            'decBaseAmt = decAmt
                            'If mdecBaseExRate <> 0 Then
                            '    decAmt = decAmt * mdecPaidExRate / mdecBaseExRate
                            'Else
                            '    decAmt = decAmt * mdecPaidExRate
                            'End If
                            If mdecBaseExRate <> 0 Then
                                decAmt = decAmt * mdecPaidExRate / mdecBaseExRate
                            Else
                                decAmt = decAmt * mdecPaidExRate
                            End If

                            Dim dicBalanceAmt As Decimal = decAmt
                            Select Case CInt(Session("PaymentRoundingType"))
                                Case enPaymentRoundingType.NONE
                                    decAmt = dicBalanceAmt
                                Case enPaymentRoundingType.AUTOMATIC
                                    If decRondingMultile / 2 > dicBalanceAmt Mod decRondingMultile Then
                                        decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                                    Else
                                        If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                                            decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                        End If
                                    End If
                                Case enPaymentRoundingType.UP
                                    If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                                        decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                    End If
                                Case enPaymentRoundingType.DOWN
                                    decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                            End Select

                            If mdecBaseExRate <> 0 Then
                                decBaseAmt = decAmt * mdecBaseExRate / mdecPaidExRate
                            Else
                                decBaseAmt = decAmt * mdecBaseExRate
                            End If

                            If CDec(Session("CFRoundingAbove")) > 0 AndAlso Math.Abs((CDec(dtTemp.Rows(0).Item("balanceamount")) - decBaseAmt)) <= CDec(Session("CFRoundingAbove")) Then
                                .Item("roundingadjustment") = CDec(dtTemp.Rows(0).Item("balanceamount")) - decBaseAmt '*** Rounding Adjustment
                            Else
                                .Item("roundingadjustment") = 0 '*** Rounding Adjustment
                            End If
                            'Sohail (17 Mar 2020) -- End

                        End If
                    End If
                    .Item("amount") = Format(decAmt, CStr(Session("fmtCurrency")))
                    .Item("amount_tag") = decAmt
                    .Item("voucherno") = strVoucherNo
                    'Sohail (30 Oct 2018) -- Start
                    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                    .Item("default_costcenterunkid") = dtRow.Item("costcenterunkid").ToString
                    'Sohail (30 Oct 2018) -- End
                    .Item("tnaleavetranunkid") = intTnALeaveUnkID.ToString
                    .Item("baseamount") = decBaseAmt.ToString


                    '*** Show those Employee only whose Balance Amount > 0
                    If decAmt > 0 Then
                        'Sohail (04 Jan 2014) -- Start
                        'Enhancement - Oman
                        decTotPaymentAmt += decAmt
                        Dim decCutOff As Decimal
                        Decimal.TryParse(txtCutOffAmount.Text, decCutOff)
                        If decCutOff > 0 AndAlso decCutOff < decTotPaymentAmt Then
                            Exit For
                        End If
                        'Sohail (04 Jan 2014) -- End
                        dtPayment.Rows.Add(dRow)
                    End If

                End With
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dtPayment.Rows.Count <= 0 Then
                Dim r As DataRow = dtPayment.NewRow

                r.Item(3) = "None" ' To Hide the row and display only Row Header
                r.Item(4) = "0"

                dtPayment.Rows.Add(r)
            End If

            GvGlobalPayment.DataSource = dtPayment
            GvGlobalPayment.DataBind()
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In GvGlobalPayment.Rows
                If CInt(dtPayment.Rows(gvr.RowIndex).Item("employeeunkid")) > 0 AndAlso dtPayment.Rows(gvr.RowIndex).Item("employeecode").ToString <> "" Then
                    cb = CType(GvGlobalPayment.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                    cb.Checked = blnCheckAll

                    dtPayment.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Dim dtTable As DataTable
        Dim objEmpBank As New clsEmployeeBanks
        Dim ds As DataSet

        Try
            Dim decCashPerc As Decimal
            Decimal.TryParse(txtCashPerc.Text, decCashPerc)

            'Nilay (28-Aug-2015) -- Start
            'If CInt(cboPayYear.SelectedValue) <= 0 Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select Pay Year. Pay Year is mandatory information."), Me)
            '    'Anjan [04 June 2014] -- End
            '    cboPayYear.Focus()
            '    Return False
            'ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                'Nilay (28-Aug-2015) -- End
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please select Pay Period. Pay Period is mandatory information."), Me)
                'Anjan [04 June 2014] -- End
                cboPayPeriod.Focus()
                Return False

                'Nilay (28-Aug-2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            ElseIf CInt(cboPaymentDatePeriod.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Please select Payment Date Period. Payment Date Period is mandatory information."), Me)
                cboPaymentDatePeriod.Focus()
                Return False
            ElseIf dtpPaymentDate.GetDate.Date < mdtPaymentDatePeriodStartDate OrElse dtpPaymentDate.GetDate.Date > mdtPaymentDatePeriodEndDate Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Payment Date should be in between ") & mdtPaymentDatePeriodStartDate.ToShortDateString & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, " And ") & mdtPaymentDatePeriodEndDate.ToShortDateString & ".", Me)
                dtpPaymentDate.Focus()
                Return False
                'Nilay (28-Aug-2015) -- End

            ElseIf dtpPaymentDate.IsNull = True Then
                DisplayMessage.DisplayMessage("Please enter Payment Date.", Me)
                dtpPaymentDate.Focus()
                Return False
                'Nilay (28-Aug-2015) -- Start
                'ElseIf dtpPaymentDate.GetDate < mdtPayPeriodStartDate OrElse dtpPaymentDate.GetDate > CDate(Session("fin_enddate")).Date Then
                '    'Anjan [04 June 2014] -- Start
                '    'ENHANCEMENT : Implementing Language,requested by Andrew
                '    'Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Payment Date should be in between ") & mdtPayPeriodStartDate.ToShortDateString & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, " And ") & CDate(Session("fin_enddate")).Date.ToShortDateString & ".", Me)
                '    'Anjan [04 June 2014] -- End
                '    dtpPaymentDate.Focus()
                '    Return False
                'Nilay (28-Aug-2015) -- End

            ElseIf CInt(Session("PaymentVocNoType")) = 0 AndAlso txtVoucher.Text.Trim = "" Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please enter Payment Voucher. Payment Voucher is mandatory information."), Me)
                'Anjan [04 June 2014] -- End
                txtVoucher.Focus()
                Return False
            ElseIf CInt(cboPaymentMode.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select Payment Mode. Payment Mode is mandatory information."), Me)
                'Anjan [04 June 2014] -- End
                cboPaymentMode.Focus()
                Return False
                'Sohail (21 Apr 2020) -- Start
                'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                'ElseIf (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE) AndAlso (decCashPerc <= 0 Or decCashPerc > 100) Then
            ElseIf (decCashPerc <= 0 Or decCashPerc > 100) Then
                'Sohail (21 Apr 2020) -- End
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, Percentage should be in between 1 to 100."), Me)
                'Anjan [04 June 2014] -- End
                txtCashPerc.Focus()
                Return False
            ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                If CInt(cboBankGroup.SelectedValue) <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please select Bank Group. Bank Group is mandatory information."), Me)
                    'Anjan [04 June 2014] -- End
                    cboBankGroup.Focus()
                    Return False
                ElseIf CInt(cboBranch.SelectedValue) <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please select Bank Branch. Bank Branch is mandatory information."), Me)
                    'Anjan [04 June 2014] -- End
                    cboBranch.Focus()
                    Return False
                ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Please select Bank Account. Bank Account is mandatory information."), Me)
                    'Anjan [04 June 2014] -- End
                    cboAccountNo.Focus()
                    Return False
                ElseIf txtChequeNo.Text.Trim = "" Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Please enter Cheque No., Cheque No. is mandatory information."), Me)
                    txtChequeNo.Focus()
                    Return False
                End If
            End If

            If dtPayment.Select("IsChecked = 1").Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Please check atleast one employee to make Payment."), Me)
                'Anjan [04 June 2014] -- End
                txtChequeNo.Focus()
                Return False
            End If

            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                Dim strEmpIDs As String = ""
                For Each dsRow As DataRow In dtPayment.Rows
                    If CBool(dsRow.Item("IsChecked")) = True Then
                        If strEmpIDs = "" Then
                            strEmpIDs = dsRow.Item("employeeunkid").ToString
                        Else
                            strEmpIDs += "," & dsRow.Item("employeeunkid").ToString
                        End If
                    End If
                Next

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'ds = objEmpBank.GetEmployeeWithOverDistribution("List", strEmpIDs)

                'Nilay (10-Feb-2016) -- Start
                'ds = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name"), _
                '                                                Session("UserId"), _
                '                                                Session("Fin_year"), _
                '                                                Session("CompanyUnkId"), _
                '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                Session("UserAccessModeSetting"), True, _
                '                                                Session("IsIncludeInactiveEmp"), True, "", _
                '                                                "List", strEmpIDs)

                ds = objEmpBank.GetEmployeeWithOverDistribution(CStr(Session("Database_Name")), _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                mdtPayPeriodStartDate, _
                                                                mdtPayPeriodEndDate, _
                                                                CStr(Session("UserAccessModeSetting")), True, _
                                                                True, False, "", _
                                                                "List", strEmpIDs)
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If ds.Tables("List").Rows.Count > 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry! Some of the selected employees are having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                    'Anjan [04 June 2014] -- End
                    cboPaymentMode.Focus()
                    Exit Function
                End If
            End If

            If CBool(Session("DoNotAllowOverDeductionForPayment")) = True Then
                dtTable = (New clsTnALeaveTran).GetOverDeductionList("List", CInt(cboPayPeriod.SelectedValue), False)
                If dtTable.Rows.Count > 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sorry! You can not do Payment now. Reason : Some of the employees having Over Deduction in this month."), Me)
                    'Anjan [04 June 2014] -- End
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpBank = Nothing
        End Try
    End Function

    Private Function CreateDatatable() As Boolean
        Try
            mdtTable = New DataTable
            With mdtTable

                .Columns.Add(New DataColumn("voucherno", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("periodunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("paymentdate", Type.GetType("System.DateTime")))
                .Columns.Add(New DataColumn("employeeunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Paymentrefid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Paymentmodeid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Branchunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Chequeno", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Paymentbyid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Percentage", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("Amount", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("Voucherref", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Payreftranunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Userunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Isvoid", Type.GetType("System.Boolean")))
                .Columns.Add(New DataColumn("Voiduserunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Voiddatetime", Type.GetType("System.DateTime")))
                .Columns.Add(New DataColumn("PaymentTypeId", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("IsReceipt", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Voidreason", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Isglobalpayment", Type.GetType("System.Boolean")))
                .Columns.Add(New DataColumn("basecurrencyid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("baseexchangerate", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("paidcurrencyid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("expaidrate", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("expaidamt", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("Accountno", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("countryunkid", Type.GetType("System.Int32")))
                .Columns.Add("isapproved", Type.GetType("System.Boolean")).DefaultValue = False
                'Sohail (03 Dec 2013) -- Start
                'Enhancement - TBC
                .Columns.Add("roundingtypeid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("roundingmultipleid", Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (03 Dec 2013) -- End
                .Columns.Add("roundingadjustment", Type.GetType("System.Decimal")).DefaultValue = 0 'Sohail (21 Mar 2014)
                .Columns.Add("remarks", Type.GetType("System.String")).DefaultValue = "" 'Sohail (24 Dec 2014)
                'Nilay (28-Aug-2015) -- Start
                .Columns.Add(New DataColumn("paymentdate_periodunkid", Type.GetType("System.Int32")))
                'Nilay (28-Aug-2015) -- End
                .Columns.Add(New DataColumn("default_costcenterunkid", Type.GetType("System.Int32"))) 'Sohail (30 Oct 2018)
            End With

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim decTotAmt As Decimal = 0
        Dim decCashPerc As Decimal

        Try
            Decimal.TryParse(txtCashPerc.Text, decCashPerc)

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

            For Each dsRow As DataRow In dtPayment.Rows
                If CBool(dsRow.Item("IsChecked")) = True Then
                    'Sohail (21 Feb 2017) -- Start
                    'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                    'decTotAmt += CDec(dsRow.Item("amount"))
                    decTotAmt += CDec(dsRow.Item("amount_tag"))
                    'Sohail (21 Feb 2017) -- End
                End If
            Next
            mdecTotAmt = decTotAmt

            'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then 'Sohail (21 Apr 2020)
            decTotAmt = decTotAmt * decCashPerc / 100
            'End If 'Sohail (21 Apr 2020)
            txtTotalPaymentAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim decTotAmt As Decimal
        Dim decCashPerc As Decimal
        Try
            Decimal.TryParse(txtCashPerc.Text, decCashPerc)

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                dtPayment.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

            decTotAmt = 0
            Dim dRow() As DataRow = dtPayment.Select("IsChecked")
            If dRow.Length > 0 Then
                For Each drRow As DataRow In dRow
                    'Sohail (21 Feb 2017) -- Start
                    'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                    'decTotAmt += CDec(drRow.Item("amount"))
                    decTotAmt += CDec(drRow.Item("amount_tag"))
                    'Sohail (21 Feb 2017) -- End
                Next
            End If
            mdecTotAmt = decTotAmt

            'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then 'Sohail (21 Apr 2020)
            decTotAmt = decTotAmt * decCashPerc / 100
            'End If 'Sohail (21 Apr 2020)
            txtTotalPaymentAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (28-Aug-2015) -- Start

    'Private Sub CreateDenomDataTable(ByVal dsDataset As DataSet)
    '    mdtDenomTable = New DataTable("DENOM")
    '    Try
    '        Dim dCol As DataColumn

    '        dCol = New DataColumn("employeename")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dCol.Caption = "Employee"
    '        mdtDenomTable.Columns.Add(dCol)

    '        dCol = New DataColumn("amount")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        dCol.Caption = "Amount"
    '        dCol.DefaultValue = 0
    '        mdtDenomTable.Columns.Add(dCol)

    '        dCol = New DataColumn("total")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        dCol.Caption = "Total"
    '        dCol.DefaultValue = 0
    '        mdtDenomTable.Columns.Add(dCol)

    '        For Each dtRow As DataRow In dsDataset.Tables(0).Rows
    '            dCol = New DataColumn("Column" & dtRow.Item("denomunkid").ToString)
    '            dCol.Caption = Format(dtRow.Item("denomination"), mstrDenomeFormat) & "'s"
    '            dCol.DataType = System.Type.GetType("System.Decimal")
    '            dCol.DefaultValue = 0
    '            dCol.ExtendedProperties.Add("dvalue", CInt(dtRow.Item("denomination")))
    '            mdtDenomTable.Columns.Add(dCol)
    '        Next

    '        dCol = New DataColumn("employeeunkid")
    '        dCol.DataType = System.Type.GetType("System.Int32")
    '        dCol.Caption = "employeeunkid"
    '        mdtDenomTable.Columns.Add(dCol)

    '        dCol = New DataColumn("exchagerateunkid")
    '        dCol.DataType = System.Type.GetType("System.Int32")
    '        dCol.Caption = "exchagerateunkid"
    '        mdtDenomTable.Columns.Add(dCol)

    '        dCol = New DataColumn("currency_sign")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dCol.Caption = "currency_sign"
    '        mdtDenomTable.Columns.Add(dCol)

    '        dCol = New DataColumn("countryunkid")
    '        dCol.DataType = System.Type.GetType("System.Int32")
    '        dCol.Caption = "countryunkid"
    '        mdtDenomTable.Columns.Add(dCol)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Private Sub FillDenomDataTable(ByVal dsDataSet As DataTable)
    '    Try
    '        Dim drRow As DataRow
    '        Dim blnIsAdded As Boolean = False
    '        Dim decBalAmt As Decimal
    '        'If menAction = enAction.ADD_ONE Then
    '        For Each dtRow As DataRow In dsDataSet.Rows
    '            drRow = mdtDenomTable.NewRow

    '            drRow.Item("employeename") = dtRow.Item("employeename")
    '            decBalAmt = CDec(dtRow.Item("balanceamount")) * CDec(dtRow.Item("Percentage")) / 100
    '            If mdecBaseExRate <> 0 Then
    '                drRow.Item("amount") = Format(decBalAmt * mdecPaidExRate / mdecBaseExRate, Session("fmtCurrency"))
    '            Else
    '                drRow.Item("amount") = Format(decBalAmt * mdecPaidExRate, Session("fmtCurrency"))
    '            End If
    '            drRow.Item("employeeunkid") = dtRow.Item("employeeunkid")

    '            drRow.Item("exchagerateunkid") = mintPaidCurrId
    '            drRow.Item("countryunkid") = cboCurrency.SelectedValue

    '            drRow.Item("currency_sign") = cboCurrency.SelectedItem.Text

    '            mdtDenomTable.Rows.Add(drRow)
    'Next
    '        'ElseIf menAction = enAction.EDIT_ONE Then
    '        'drRow = mdtTran.NewRow
    '        'For Each dtRow As DataRow In dsDataSet.Rows
    '        '    If blnIsAdded = False Then
    '        '        drRow.Item("employeename") = dtRow.Item("employeename")
    '        '        drRow.Item("amount") = dtRow.Item("amount")
    '        '        drRow.Item("employeeunkid") = dtRow.Item("employeeunkid")
    '        '        drRow.Item("exchagerateunkid") = dtRow.Item("exchagerateunkid")
    '        '        drRow.Item("currency_sign") = dtRow.Item("currency_sign")
    '        '        drRow.Item("total") = dtRow.Item("amount")
    '        '        'Sohail (03 Sep 2012) -- Start
    '        '        'TRA - ENHANCEMENT
    '        '        drRow.Item("countryunkid") = dtRow.Item("countryunkid")
    '        '        'Sohail (03 Sep 2012) -- End

    '        '        blnIsAdded = True
    '        '    End If
    '        'drRow.Item("Column" & dtRow.Item("denominatonunkid").ToString) = CInt(dtRow.Item("qualtity"))
    '        'Next
    '        'mdtTran.Rows.Add(drRow)
    '        'blnIsAdded = False
    '        'End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Nilay (28-Aug-2015) -- End

    Private Function SaveData(ByRef objPaymentTran As clsPayment_tran, ByVal mstrCashEmpIDs As String) As Boolean
        Dim blnFlag As Boolean = False
        Try
            popupCashDenom.Message = ""
            objPaymentTran._Voucherno = txtVoucher.Text.Trim
            objPaymentTran._PaymentVocNoType = CInt(Session("PaymentVocNoType"))
            objPaymentTran._Companyunkid = CInt(Session("Companyunkid"))
            objPaymentTran._Userunkid = CInt(Session("Userid"))
            objPaymentTran._YearUnkid = CInt(Session("Fin_year"))
            objPaymentTran._AccessLevelFilterString = CStr(Session("AccessLevelFilterString"))
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            objPaymentTran._Paymentrefid = CInt(clsPayment_tran.enPaymentRefId.PAYSLIP)
            objPaymentTran._PaymentTypeId = CInt(clsPayment_tran.enPayTypeId.PAYMENT)
            objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
            objPaymentTran._Periodunkid = CInt(cboPayPeriod.SelectedValue)  'Sohail (29 Nov 2018)
            'Sohail (30 Oct 2018) -- End

            Blank_ModuleName()
            objPaymentTran._WebFormName = "frmPayslipGlobalPayment"
            StrModuleName2 = "mnuPayroll"
            objPaymentTran._WebHostName = CStr(Session("HOST_NAME"))
            objPaymentTran._WebIP = CStr(Session("IP_ADD"))

            clsCommonATLog._WebFormName = "frmPayslipGlobalPayment"
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))


            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            Dim strEmployeeIDs As String = String.Join(",", (From p In mdtTable Select (p.Item("employeeunkid").ToString)).ToArray)
            'Sohail (30 Oct 2018) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'blnFlag = objPaymentTran.InsertAll(mdtTable)

            'Nilay (10-Feb-2016) -- Start
            'blnFlag = objPaymentTran.InsertAll(mdtTable, _
            '                                  Session("PaymentVocNoType"), _
            '                                  Session("Database_Name"), _
            '                                  Session("CompanyUnkId"), _
            '                                  Session("Fin_year"), _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                  Session("UserAccessModeSetting"), True, _
            '                                  Session("IsIncludeInactiveEmp"), _
            '                                  Session("UserId"), _
            '                                  ConfigParameter._Object._CurrentDateAndTime, True, _
            '                                  "List", "")
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'blnFlag = objPaymentTran.InsertAll(mdtTable, _
            '                                  CInt(Session("PaymentVocNoType")), _
            '                                  CStr(Session("Database_Name")), _
            '                                  CInt(Session("CompanyUnkId")), _
            '                                  CInt(Session("Fin_year")), _
            '                                  mdtPayPeriodStartDate, _
            '                                  mdtPayPeriodEndDate, _
            '                                  CStr(Session("UserAccessModeSetting")), _
            '                                  True, False, _
            '                                  CInt(Session("UserId")), _
            '                                  ConfigParameter._Object._CurrentDateAndTime, True, _
            '                                  "List", "")
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'blnFlag = objPaymentTran.InsertAll(mdtTable, _
            '                                  CInt(Session("PaymentVocNoType")), _
            '                                  CStr(Session("Database_Name")), _
            '                                  CInt(Session("CompanyUnkId")), _
            '                                  CInt(Session("Fin_year")), _
            '                                  mdtPayPeriodStartDate, _
            '                                  mdtPayPeriodEndDate, _
            '                                  CStr(Session("UserAccessModeSetting")), _
            '                                  True, False, _
            '                                  CInt(Session("UserId")), _
            '                                  ConfigParameter._Object._CurrentDateAndTime, True, _
            '                                  "List", "", dtActAdj)
            blnFlag = objPaymentTran.InsertAll(mdtTable, _
                                              CInt(Session("PaymentVocNoType")), _
                                              CStr(Session("Database_Name")), _
                                              CInt(Session("CompanyUnkId")), _
                                              CInt(Session("Fin_year")), _
                                              mdtPayPeriodStartDate, _
                                              mdtPayPeriodEndDate, _
                                              CStr(Session("UserAccessModeSetting")), _
                                              True, False, _
                                              CInt(Session("UserId")), _
                                              ConfigParameter._Object._CurrentDateAndTime, True, _
                                              "List", "", dtActAdj, strEmployeeIDs, , CStr(Session("fmtCurrency")))
            'Sohail (30 Oct 2018) -- End
            'Sohail (23 May 2017) -- End
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            If blnFlag = False And objPaymentTran._Message <> "" Then
                If CBool(Session("IsDenominationCompulsory")) = True Then
                    popupCashDenom.Message = objPaymentTran._Message
                    'Nilay (28-Aug-2015) -- Start
                    'popupCashDenom.Show()
                    'Nilay (28-Aug-2015) -- End
                Else
                    DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
                End If

            Else
                If CBool(Session("SetPayslipPaymentApproval")) = True Then
                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'objPaymentTran.SendMailToApprover(True, CInt(Session("Userid")), mstrCashEmpIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, "", Session("fmtCurrency"))
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objPaymentTran.SendMailToApprover(True, CInt(Session("Userid")), mstrCashEmpIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, "", Session("fmtCurrency"), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), mdtTable)
                    'Sohail (24 Dec 2014) -- Start
                    'Enhancement - Provide Remark option on Payslip Payment and Global Payslip Payment.
                    'objPaymentTran.SendMailToApprover(True, CInt(Session("Userid")), mstrCashEmpIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, "", Session("fmtCurrency"), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), mdtTable, , , CInt(cboCurrency.SelectedValue), Session("ArutiSelfServiceURL"))
                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'objPaymentTran.SendMailToApprover(True, CInt(Session("Userid")), mstrCashEmpIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, CStr(Session("fmtCurrency")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")), mdtTable, , , CInt(cboCurrency.SelectedValue), CStr(Session("ArutiSelfServiceURL")))
                    objPaymentTran.SendMailToApprover(True, CInt(Session("Userid")), mstrCashEmpIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, CStr(Session("fmtCurrency")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, mdtTable, , , CInt(cboCurrency.SelectedValue), CStr(Session("ArutiSelfServiceURL")))
                    'Sohail (16 Nov 2016) -- End
                    'Sohail (24 Dec 2014) -- End
                    'Sohail (17 Dec 2014) -- End
                    'Sohail (18 Feb 2014) -- End
                End If

            End If

            If blnFlag Then
                'mblnCancel = False
                'If menAction = enAction.ADD_CONTINUE Then
                objPaymentTran = Nothing
                objPaymentTran = New clsPayment_tran
                cboPayYear.SelectedValue = "0"
                txtVoucher.Text = ""
                cboPaymentMode.SelectedValue = "0"
                Call cboPayYear_SelectedIndexChanged(cboPayYear, New System.EventArgs())
                cboBankGroup.SelectedValue = "0"
                cboBranch.SelectedValue = "0"
                cboAccountNo.SelectedValue = "0"
                txtChequeNo.Text = ""
                txtTotalPaymentAmount.Text = Format(0, CStr(Session("fmtCurrency")))
                txtRemarks.Text = "" 'Sohail (24 Dec 2014) 
                cboPayYear.Focus()
                DisplayMessage.DisplayMessage("Payment Process completed succesfully.", Me)
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Function
#End Region

#Region " Page's Event "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrBaseCurrSign", mstrBaseCurrSign)
            Me.ViewState.Add("mintBaseCurrId", mintBaseCurrId)
            Me.ViewState.Add("mdecBaseExRate", mdecBaseExRate)
            Me.ViewState.Add("mintPaidCurrId", mintPaidCurrId)
            Me.ViewState.Add("mdecPaidExRate", mdecPaidExRate)
            Me.ViewState.Add("mdecTotAmt", mdecTotAmt)

            Me.ViewState.Add("dtPayment", dtPayment)
            Me.ViewState.Add("mdtTable", mdtTable)
            Me.ViewState.Add("mdtDenomTable", mdtDenomTable)

            Me.ViewState.Add("mstrDenomeFormat", mstrDenomeFormat)
            Me.ViewState.Add("mstrCashEmpIDs", mstrCashEmpIDs)

            Me.ViewState.Add("mintBankPaid", mintBankPaid)
            Me.ViewState.Add("mintCashPaid", mintCashPaid)
            Me.ViewState.Add("decBankPaid", decBankPaid)
            Me.ViewState.Add("decCashPaid", decCashPaid)

            Me.ViewState.Add("mdtPayPeriodStartDate", mdtPayPeriodStartDate)
            Me.ViewState.Add("mdtPayPeriodEndDate", mdtPayPeriodEndDate)

            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter) 'Sohail (04 Jan 2014)

            'Nilay (28-Aug-2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Me.ViewState.Add("BaseCountryId", mintBaseCountryId)
            Me.ViewState.Add("PaymentDatePeriodStartDate", mdtPaymentDatePeriodStartDate)
            Me.ViewState.Add("PaymentDatePeriodEndDate", mdtPaymentDatePeriodEndDate)
            'Nilay (28-Aug-2015) -- End
            Me.ViewState("dtActAdj") = dtActAdj 'Sohail (23 May 2017)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

      

            If Me.IsPostBack = False Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End

                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call SetVisibility()
                Call CreateListTable()
                Call FillCombo()

                'Nilay (28-Aug-2015) -- Start
                Call FillEmployeeCombo()
                'Nilay (28-Aug-2015) -- End

                If CInt(Session("PaymentVocNoType")) = 1 Then
                    txtVoucher.Enabled = False
                Else
                    txtVoucher.Enabled = True
                End If
            Else
                mstrBaseCurrSign = CStr(ViewState("mstrBaseCurrSign"))
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mdecBaseExRate = CDec(ViewState("mdecBaseExRate"))
                mintPaidCurrId = CInt(ViewState("mintPaidCurrId"))
                mdecPaidExRate = CDec(ViewState("mdecPaidExRate"))
                mdecTotAmt = CDec(ViewState("mdecTotAmt"))

                dtPayment = CType(ViewState("dtPayment"), DataTable)
                mdtTable = CType(ViewState("mdtTable"), DataTable)
                mdtDenomTable = CType(ViewState("mdtDenomTable"), DataTable)

                mstrDenomeFormat = CStr(ViewState("mstrDenomeFormat"))
                mstrCashEmpIDs = CStr(ViewState("mstrCashEmpIDs"))

                mintBankPaid = CInt(ViewState("mintBankPaid"))
                mintCashPaid = CInt(ViewState("mintCashPaid"))
                decBankPaid = CDec(ViewState("decBankPaid"))
                decCashPaid = CDec(ViewState("decCashPaid"))

                mdtPayPeriodStartDate = CDate(ViewState("mdtPayPeriodStartDate"))
                mdtPayPeriodEndDate = CDate(ViewState("mdtPayPeriodEndDate"))

                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter")) 'Sohail (04 Jan 2014)

                'Nilay (28-Aug-2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mdtPaymentDatePeriodStartDate = CDate(Me.ViewState("PaymentDatePeriodStartDate"))
                mdtPaymentDatePeriodEndDate = CDate(Me.ViewState("PaymentDatePeriodEndDate"))
                'Nilay (28-Aug-2015) -- End
                dtActAdj = CType(ViewState("dtActAdj"), DataTable) 'Sohail (23 May 2017)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect("~\UserHome.aspx", False)
            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            'Response.Redirect(Session("rootpath").ToString & "UserHome.aspx")
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            'Sohail (01 Aug 2019) -- End
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnFlag As Boolean = False
        Dim dtRow As DataRow
        Dim strEmployeeIDs As String = ""
        Dim decCashPerc As Decimal
        Dim objPaymentTran As New clsPayment_tran
        'Sohail (23 May 2017) -- Start
        'Enhancement - 67.1 - Link budget with Payroll.
        Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
        'Sohail (23 May 2017) -- End

        mstrCashEmpIDs = ""
        mintBankPaid = 0
        mintCashPaid = 0
        decBankPaid = 0
        decCashPaid = 0

        Try
            If IsValidData() = False Then Exit Sub

            If CreateDatatable() = False Then Exit Sub

            Decimal.TryParse(txtCashPerc.Text, decCashPerc)

            Dim dRow() As DataRow = dtPayment.Select("IsChecked")

            For Each drRow As DataRow In dRow
                With mdtTable
                    dtRow = .NewRow()
                    dtRow.Item("voucherno") = txtVoucher.Text.Trim
                    dtRow.Item("periodunkid") = CInt(cboPayPeriod.SelectedValue)
                    'Sohail (14 Dec 2018) -- Start
                    'Twaweza Issue : Payment Time not coming on payroll report prepared by. 
                    'dtRow.Item("paymentdate") = dtpPaymentDate.GetDate
                    dtRow.Item("paymentdate") = dtpPaymentDate.GetDateTime
                    'Sohail (14 Dec 2018) -- End
                    dtRow.Item("employeeunkid") = CInt(drRow.Item("employeeunkid"))
                    dtRow.Item("Paymentrefid") = clsPayment_tran.enPaymentRefId.PAYSLIP
                    dtRow.Item("Branchunkid") = CInt(cboBranch.SelectedValue)
                    dtRow.Item("Chequeno") = txtChequeNo.Text.Trim
                    If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
                        dtRow.Item("Paymentmodeid") = enPaymentMode.CASH
                        dtRow.Item("Paymentbyid") = enPaymentBy.Percentage
                        dtRow.Item("Percentage") = decCashPerc
                        dtRow.Item("Amount") = CDec(drRow.Item("baseamount")) * decCashPerc / 100


                        If CDec(dtRow.Item("Amount")) > 0 Then
                            If mstrCashEmpIDs.Trim.Length = 0 Then
                                mstrCashEmpIDs = drRow.Item("employeeunkid").ToString
                            Else
                                mstrCashEmpIDs &= "," & drRow.Item("employeeunkid").ToString
                            End If
                        End If

                        mintCashPaid += 1
                        'Sohail (21 Feb 2017) -- Start
                        'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                        'decCashPaid += CDec(drRow.Item("amount"))
                        decCashPaid += CDec(drRow.Item("amount_tag"))
                        'Sohail (21 Feb 2017) -- End
                    Else
                        dtRow.Item("Paymentmodeid") = CInt(cboPaymentMode.SelectedValue)
                        dtRow.Item("Paymentbyid") = enPaymentBy.Value
                        dtRow.Item("Percentage") = 0
                        'Sohail (21 Apr 2020) -- Start
                        'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                        'dtRow.Item("Amount") = CDec(drRow.Item("baseamount"))
                        dtRow.Item("Amount") = CDec(drRow.Item("baseamount")) * decCashPerc / 100
                        'Sohail (21 Apr 2020) -- End

                        mintBankPaid += 1
                        'Sohail (21 Feb 2017) -- Start
                        'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                        'decBankPaid += CDec(drRow.Item("amount"))
                        decBankPaid += CDec(drRow.Item("amount_tag"))
                        'Sohail (21 Feb 2017) -- End
                    End If
                    dtRow.Item("Voucherref") = drRow.Item("voucherno").ToString
                    dtRow.Item("Payreftranunkid") = CInt(drRow.Item("tnaleavetranunkid"))
                    dtRow.Item("Userunkid") = CInt(Session("UserId"))
                    dtRow.Item("Isvoid") = False
                    dtRow.Item("Voiduserunkid") = 0
                    dtRow.Item("Voiddatetime") = DBNull.Value
                    dtRow.Item("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT
                    dtRow.Item("IsReceipt") = False
                    dtRow.Item("Voidreason") = ""
                    dtRow.Item("Isglobalpayment") = True
                    dtRow.Item("basecurrencyid") = mintBaseCurrId
                    dtRow.Item("baseexchangerate") = mdecBaseExRate
                    dtRow.Item("paidcurrencyid") = mintPaidCurrId
                    dtRow.Item("expaidrate") = mdecPaidExRate
                    'Sohail (21 Feb 2017) -- Start
                    'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                    'dtRow.Item("expaidamt") = CDec(drRow.Item("amount")) * decCashPerc / 100
                    dtRow.Item("expaidamt") = CDec(drRow.Item("amount_tag")) * decCashPerc / 100
                    'Sohail (21 Feb 2017) -- End
                    If CInt(cboAccountNo.SelectedValue) > 0 Then
                        dtRow.Item("Accountno") = cboAccountNo.SelectedItem.Text
                    Else
                        dtRow.Item("Accountno") = ""
                    End If
                    dtRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                    dtRow.Item("isapproved") = False
                    'Sohail (21 Mar 2014) -- Start
                    dtRow.Item("roundingtypeid") = Session("PaymentRoundingType")
                    dtRow.Item("roundingmultipleid") = Session("PaymentRoundingMultiple")
                    dtRow.Item("roundingadjustment") = CDec(drRow.Item("roundingadjustment"))
                    'Sohail (21 Mar 2014) -- End
                    dtRow.Item("remarks") = txtRemarks.Text.Trim 'Sohail (24 Dec 2014)
                    'Nilay (28-Aug-2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    dtRow.Item("paymentdate_periodunkid") = CInt(cboPaymentDatePeriod.SelectedValue)
                    'Nilay (28-Aug-2015) -- End

                    'Sohail (30 Oct 2018) -- Start
                    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                    dtRow.Item("default_costcenterunkid") = CInt(drRow.Item("default_costcenterunkid"))
                    'Sohail (30 Oct 2018) -- End

                    If strEmployeeIDs.Trim = "" Then
                        strEmployeeIDs = drRow.Item("employeeunkid").ToString
                    Else
                        strEmployeeIDs &= "," & drRow.Item("employeeunkid").ToString
                    End If

                    .Rows.Add(dtRow)
                End With
            Next

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If ConfigParameter._Object._IsArutiDemo = True Then
                blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriod.SelectedValue), strEmployeeIDs)
                If blnFlag = False Then
                    DisplayMessage.DisplayMessage("Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current pay period. In order to process Payment, please either Approve or Reject pending operations for selected pay period.", Me)
                    Exit Sub
                End If
            Else
                If CBool(ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management)) = True Then
                    blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriod.SelectedValue), strEmployeeIDs)
                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage("Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current pay period. In order to process Payment, please either Approve or Reject pending operations for selected pay period.", Me)
                        Exit Sub
                    End If
                End If
            End If

            Dim objBudget As New clsBudget_MasterNew
            Dim intBudgetId As Integer = 0
            Dim dsList As DataSet
            dtActAdj = New DataTable
            dsList = objBudget.GetComboList("List", False, True, )
            If dsList.Tables(0).Rows.Count > 0 Then
                intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If
            If intBudgetId > 0 Then
                Dim mstrAnalysis_Fields As String = ""
                Dim mstrAnalysis_Join As String = ""
                Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                Dim mstrReport_GroupName As String = ""
                Dim mstrAnalysis_TableName As String = ""
                Dim mstrAnalysis_CodeField As String = ""
                objBudget._Budgetunkid = intBudgetId
                Dim strEmpList As String = ""
                Dim allEmp As List(Of String) = (From p In mdtTable Select (p.Item("employeeunkid").ToString)).ToList
                strEmpList = String.Join(",", allEmp.ToArray())
                Dim objBudgetCodes As New clsBudgetcodes_master
                Dim dsEmp As DataSet = Nothing
                If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objBudget_Tran As New clsBudget_TranNew
                    Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                    Controls_AnalysisBy.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                    dsEmp = objEmp.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                Else
                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, strEmpList, 1)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                    dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                    dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                    Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                    Dim mdicActivityCode As New Dictionary(Of Integer, String)
                    Dim objActivity As New clsfundactivity_Tran
                    Dim dsAct As DataSet = objActivity.GetList("Activity")
                    Dim objActAdjust As New clsFundActivityAdjustment_Tran
                    Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPayPeriodEndDate)
                    mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                    mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                    'Dim dsBalance As DataSet = objTnALeave.Get_Balance_List(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, True, "prtnaleave_tran.balanceamount > 0", "list", strEmpList, CInt(cboPayPeriod.SelectedValue), "")
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                        mdtTable.PrimaryKey = New DataColumn() {mdtTable.Columns("employeeunkid")}
                        mdtTable.Merge(dsEmp.Tables(0))
                    End If
                    Dim intActivityId As Integer
                    Dim decTotal As Decimal = 0
                    Dim decActBal As Decimal = 0
                    Dim blnShowValidationList As Boolean = False
                    Dim blnIsExeed As Boolean = False
                    For Each pair In mdicActivityCode
                        intActivityId = pair.Key
                        decTotal = 0
                        decActBal = 0
                        blnIsExeed = False

                        If mdicActivity.ContainsKey(intActivityId) = True Then
                            decActBal = mdicActivity.Item(intActivityId)
                        End If

                        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                        If lstRow.Count > 0 Then
                            For Each d_Row As DataRow In lstRow
                                Dim intAllocUnkId As Integer = CInt(d_Row.Item("Employeeunkid"))
                                Dim decNetPay As Decimal = (From p In mdtTable Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("Amount")) = False) Select (CDec(p.Item("Amount")))).Sum
                                decTotal += decNetPay * CDec(d_Row.Item("percentage")) / 100
                            Next

                            If decTotal > decActBal Then
                                blnShowValidationList = True
                                blnIsExeed = True
                            End If
                            Dim dr As DataRow = dtActAdj.NewRow
                            dr.Item("fundactivityunkid") = intActivityId
                            dr.Item("transactiondate") = eZeeDate.convertDate(mdtPayPeriodEndDate).ToString()
                            dr.Item("remark") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Salary payments for the period of") & " " & cboPayPeriod.Text
                            dr.Item("isexeed") = blnIsExeed
                            dr.Item("Activity Code") = pair.Value
                            dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                            dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                            dtActAdj.Rows.Add(dr)
                        End If
                    Next

                    If blnShowValidationList = True Then
                        Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                        For Each r In dr
                            dtActAdj.Rows.Remove(r)
                        Next
                        dtActAdj.AcceptChanges()
                        dtActAdj.Columns.Remove("fundactivityunkid")
                        dtActAdj.Columns.Remove("transactiondate")
                        dtActAdj.Columns.Remove("remark")
                        dtActAdj.Columns.Remove("isexeed")
                        popupValidationList.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")
                        popupValidationList.Data_Table = dtActAdj
                        popupValidationList.Show()
                        Exit Try
                    End If
                End If
            End If
            'Sohail (23 May 2017) -- End

            If CBool(Session("IsDenominationCompulsory")) = True Then
                If mstrCashEmpIDs.Trim.Length > 0 AndAlso (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE) Then
                    Dim objDenome As New clsDenomination
                    'Dim dsList As DataSet 'Sohail (23 May 2017)
                    dsList = objDenome.GetList("List", True, True, CInt(cboCurrency.SelectedValue))
                    If dsList.Tables("List").Rows.Count <= 0 Then
                        DisplayMessage.DisplayMessage("Denomination's are not defined for this currency. Please define denomination to perform this operation.", Me)
                        Exit Try
                    End If

                    'Nilay (28-Aug-2015) -- Start
                    'Call CreateDenomDataTable(dsList)
                    'Nilay (28-Aug-2015) -- End

                    objDenome = Nothing

                    Dim objTnALeave As New clsTnALeaveTran
                    Dim dsTnAData As New DataSet
                    Dim dtTable As DataTable = Nothing

                    'If menAction = enAction.ADD_ONE Then

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsTnAData = objTnALeave.Get_Balance_List("Data", , Session("Fin_year"), CInt(cboPayPeriod.SelectedValue))

                    'Nilay (10-Feb-2016) -- Start
                    'dsTnAData = objTnALeave.Get_Balance_List(Session("Database_Name"), _
                    '                                         Session("UserId"), _
                    '                                         Session("Fin_year"), _
                    '                                         Session("CompanyUnkId"), _
                    '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                         Session("UserAccessModeSetting"), True, _
                    '                                         Session("IsIncludeInactiveEmp"), True, "", _
                    '                                         "Data", , CInt(cboPayPeriod.SelectedValue))

                    dsTnAData = objTnALeave.Get_Balance_List(CStr(Session("Database_Name")), _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             mdtPayPeriodStartDate, _
                                                             mdtPayPeriodEndDate, _
                                                             CStr(Session("UserAccessModeSetting")), True, _
                                                             False, True, "", _
                                                             "Data", , CInt(cboPayPeriod.SelectedValue))
                    'Nilay (10-Feb-2016) -- End

                    'Shani(20-Nov-2015) -- End

                    dtTable = New DataView(dsTnAData.Tables("Data"), "employeeunkid IN (" & mstrCashEmpIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                    dtTable.Columns.Add(New DataColumn("Percentage", System.Type.GetType("System.Decimal")))
                    For Each dsRow As DataRow In dtTable.Rows
                        dsRow.Item("Percentage") = decCashPerc
                    Next
                    'ElseIf menAction = enAction.EDIT_ONE Then
                    '    dtTable = objCashDenome.GetList(mintCashDenominationUnkid, "List")
                    'End If

                    If dtTable.Rows.Count <= 0 Then
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "No Employee(s) present for the given period."), Me)
                        'Anjan [04 June 2014] -- End
                        Exit Try
                    End If

                    'Nilay (28-Aug-2015) -- Start
                    'Call FillDenomDataTable(dtTable)
                    'Nilay (28-Aug-2015) -- End

                    dsTnAData.Dispose()
                    objTnALeave = Nothing

                    'Nilay (28-Aug-2015) -- Start
                    'popupCashDenom.DataSource = mdtDenomTable
                    'popupCashDenom.DataBind()
                    'popupCashDenom.Show()
                    popupCashDenom.Countryunkid = CInt(cboCurrency.SelectedValue)
                    popupCashDenom.ExchangeTranunkid = mintPaidCurrId
                    popupCashDenom.Message = ""
                    popupCashDenom.Show(dtTable)
                    'Nilay (28-Aug-2015) -- End

                    Exit Try
                    'If menAction = enAction.ADD_CONTINUE Then
                    'Dim frm As New frmCashDenomination
                    'If frm.displayDialog(-1, CInt(cboPayPeriod.SelectedValue), strEmpIDs, txtCashPerc.Decimal, enAction.ADD_ONE, mintPaidCurrId, mdecBaseExRate, mdecPaidExRate, CInt(cboCurrency.SelectedValue)) = False Then
                    '    Exit Sub
                    'End If
                    'mdtDenomeTable = frm._CashDataSource
                    'objPaymentTran._Denom_Table = mdtDenomeTable
                    'End If
                End If
            End If

            If SaveData(objPaymentTran, mstrCashEmpIDs) = False Then

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = "0"
            If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = "0"
            If cboPayType.Items.Count > 0 Then cboPayType.SelectedValue = "0"
            'Sohail (04 Jan 2014) -- Start
            'Enhancement - Oman
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            mstrAdvanceFilter = ""
            'Sohail (04 Jan 2014) -- End

            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (04 Jan 2014) -- Start
    'Enhancement - Oman
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Jan 2014) -- End

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " Combobox's Events "

    Protected Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Try

            'Nilay (10-Feb-2016) -- Start
            'Nilay (28-Aug-2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.

            ''Sohail (11 Jan 2014) -- Start
            ''Enhancement - PayPoint, PayType filter on Global Payment
            ''dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, 1, True, , Session("Database_Name"))
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, 1, False, , Session("Database_Name"))
            ''Sohail (11 Jan 2014) -- End
            'With cboPayPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombo.Tables("PayPeriod")
            '    .DataBind()
            '    If .Items.Count > 0 Then
            '        Call cboPayPeriod_SelectedIndexChanged(Me, New System.EventArgs())
            '    End If
            'End With
            'Nilay (28-Aug-2015) -- End

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "PayPeriod", True, 1, False)

            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                .DataBind()
                If .Items.Count > 0 Then
                    Call cboPayPeriod_SelectedIndexChanged(Me, New System.EventArgs())
                End If
            End With

            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPayPeriod.SelectedValue) 'Nilay (15-Dec-2015) -- objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            'Nilay (10-Feb-2016) -- Start
            'If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            'Nilay (28-Aug-2015) -- Start
            'Call FillEmployeeCombo()
            'Nilay (28-Aug-2015) -- End
            If GvGlobalPayment.HeaderRow IsNot Nothing Then
                Dim chkSelectAll As CheckBox = CType(GvGlobalPayment.HeaderRow.Cells(0).FindControl("chkSelectAll"), CheckBox)
                If chkSelectAll.Checked = True Then chkSelectAll.Checked = False
            End If
            Call FillEmployeeCombo()
            'Nilay (10-Feb-2016) -- End

            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    'Nilay (28-Aug-2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Protected Sub cboPaymentDatePeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentDatePeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPaymentDatePeriod.SelectedValue) 'Nilay (28-Aug-2015) -- objPeriod._Periodunkid = CInt(cboPaymentDatePeriod.SelectedValue)
            mdtPaymentDatePeriodStartDate = objPeriod._Start_Date
            mdtPaymentDatePeriodEndDate = objPeriod._End_Date

            If CInt(cboPaymentDatePeriod.SelectedValue) > 0 Then
                'Sohail (14 Dec 2018) -- Start
                'Twaweza Issue : Payment Time not coming on payroll report prepared by.  
                'dtpPaymentDate.SetDate = objPeriod._End_Date
                'Hemant (19 jan 2019) -- Start
                'Issue - 74.1 - On Payroll Report, Prepared By Date (Payment date) is coming as Future date (31st Jan 2019) even if payment is done on before period end date (20th  Jan 2019)
                'dtpPaymentDate.SetDate = objPeriod._End_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                If eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) < eZeeDate.convertDate(objPeriod._Start_Date) Then
                    dtpPaymentDate.SetDate = objPeriod._Start_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                ElseIf eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) > eZeeDate.convertDate(objPeriod._End_Date) Then
                    dtpPaymentDate.SetDate = objPeriod._End_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                ElseIf eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) >= eZeeDate.convertDate(objPeriod._Start_Date) AndAlso eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) <= eZeeDate.convertDate(objPeriod._End_Date) Then
                    dtpPaymentDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                Else
                    dtpPaymentDate.SetDate = objPeriod._End_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                End If
                'Hemant (19 jan 2019) -- End
                'Sohail (14 Dec 2018) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Nilay (28-Aug-2015) -- End


    Protected Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsCombos As DataSet
        Dim objCompanyBank As New clsCompany_Bank_tran
        Try

            dsCombos = objCompanyBank.GetComboList(CInt(Session("companyunkid")), 2, "Branch", " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
            With cboBranch
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Branch")
                .DataBind()
                If .Items.Count > 0 Then
                    Call cboBranch_SelectedIndexChanged(Me, New System.EventArgs())
                End If
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", CInt(Session("companyunkid")), True, CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
            With cboAccountNo
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Account")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub

    Protected Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, dtpPaymentDate.TextChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            lblTotalPaymentAmount.Text = "Total Payment Amt."
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.GetDate, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, CStr(Session("fmtCurrency"))) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                lblTotalPaymentAmount.Text &= " (" & dtTable.Rows(0).Item("currency_sign").ToString & ")"
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged
        Try

            Select Case CInt(cboPaymentMode.SelectedValue)
                Case enPaymentMode.CASH, enPaymentMode.CASH_AND_CHEQUE
                    cboBankGroup.SelectedValue = "0"
                    cboBankGroup.Enabled = False
                    cboBranch.SelectedValue = "0"
                    cboBranch.Enabled = False
                    txtChequeNo.Text = ""
                    txtChequeNo.Enabled = False
                    txtCashPerc.Text = "100.00"

                    lblCashDescr.Visible = True
                    txtCashPerc.Enabled = True
                    Select Case CInt(cboPaymentMode.SelectedValue)
                        Case enPaymentMode.CASH
                            lblACHolder.Text = "To Non Bank A/C Holder Only"
                        Case enPaymentMode.CASH_AND_CHEQUE
                            lblACHolder.Text = "Cash Payment To All Employee"
                    End Select
                    cboAccountNo.SelectedValue = "0"
                    cboAccountNo.Enabled = False
                Case Else
                    cboBankGroup.Enabled = True
                    cboBranch.Enabled = True
                    txtChequeNo.Enabled = True

                    'Sohail (21 Apr 2020) -- Start
                    'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                    'lblCashDescr.Visible = False
                    lblCashDescr.Visible = True
                    'Sohail (21 Apr 2020) -- End
                    txtCashPerc.Text = "100.00"
                    'Sohail (21 Apr 2020) -- Start
                    'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                    'txtCashPerc.Enabled = False
                    txtCashPerc.Enabled = True
                    'Sohail (21 Apr 2020) -- End
                    Select Case CInt(cboPaymentMode.SelectedValue)
                        Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER
                            lblACHolder.Text = "To Bank A/C Holder Only"
                        Case Else
                            lblACHolder.Text = ""
                    End Select
                    cboAccountNo.Enabled = True
            End Select
            'If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (04 Jan 2014) -- Start
    'Enhancement - Oman
    Protected Sub cboEmpBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpBank.SelectedIndexChanged
        Dim objBankBranch As New clsbankbranch_master
        Dim dsList As New DataSet
        Try
            dsList = objBankBranch.getListForCombo("List", True, CInt(cboEmpBank.SelectedValue))
            With cboEmpBranch
                .DataValueField = "branchunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Jan 2014) -- End

#End Region

#Region " Textbox's Events "
    Protected Sub txtCashPerc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCashPerc.TextChanged
        Dim decTotAmt As Decimal = mdecTotAmt
        Dim decCashPerc As Decimal
        Try
            Decimal.TryParse(txtCashPerc.Text, decCashPerc)

            txtCashPerc.Text = Format(decCashPerc, "00.00")
            If dtPayment.Select("IsChecked").Length > 0 Then
                decTotAmt = decTotAmt * decCashPerc / 100 'Sohail (11 May 2011)
            End If
            txtTotalPaymentAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtCutOffAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCutOffAmount.TextChanged
        Dim decTotAmt As Decimal = mdecTotAmt
        Dim decCutOff As Decimal
        Try
            Decimal.TryParse(txtCutOffAmount.Text, decCutOff)

            txtCutOffAmount.Text = Format(decCutOff, "00.00")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Cash Denomination Popup Events "

    'Nilay (28-Aug-2015) -- Start
    'Protected Sub popupCashDenom_buttonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupCashDenom.buttonCancel_Click
    '    Try
    '        popupCashDenom.DataSource = mdtDenomTable
    '        popupCashDenom.EditIndex = -1
    '        popupCashDenom.DataBind()
    '        popupCashDenom.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub popupCashDenom_gridViewCashDenom_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles popupCashDenom.gridViewCashDenom_RowEditing
    '    Try
    '        popupCashDenom.DataSource = mdtDenomTable
    '        popupCashDenom.EditIndex = e.NewEditIndex
    '        popupCashDenom.DataBind()
    '        popupCashDenom.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub popupCashDenom_gridViewCashDenom_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles popupCashDenom.gridViewCashDenom_RowUpdating
    '    Dim gv As GridView = TryCast(sender, GridView)
    '    Dim decTotal As Decimal = 0

    '    Try
    '        popupCashDenom.Message = ""
    '        For i = 0 To gv.Columns.Count - 1
    '            If TypeOf (gv.Columns(i)) Is CommandField Then Continue For

    '            If DirectCast(DirectCast(gv.Columns(i), System.Web.UI.WebControls.DataControlField), System.Web.UI.WebControls.BoundField).ReadOnly = False Then

    '                Dim cell As DataControlFieldCell = gv.Rows(e.RowIndex).Cells(i)

    '                gv.Columns(i).ExtractValuesFromCell(e.NewValues, cell, DataControlRowState.Edit, True)

    '                decTotal = 0

    '                For Each key As String In e.NewValues.Keys
    '                    Dim str As String = e.NewValues(key)

    '                    If mdtDenomTable.Columns(key).DataType Is System.Type.GetType("System.Decimal") Then
    '                        Dim decTemp As Decimal = 0
    '                        If Decimal.TryParse(str, decTemp) = True Then
    '                            mdtDenomTable.Rows(e.RowIndex).Item(key) = decTemp
    '                        Else
    '                            popupCashDenom.Message = "Please enter proper amount."
    '                            popupCashDenom.DataSource = mdtDenomTable
    '                            popupCashDenom.DataBind()
    '                            popupCashDenom.Show()
    '                            e.Cancel = True
    '                            Exit Sub
    '                        End If

    '                    Else
    '                        mdtDenomTable.Rows(e.RowIndex).Item(key) = str
    '                    End If



    '                    If mdtDenomTable.Columns(key).DataType Is System.Type.GetType("System.Decimal") Then
    '                        If mdtDenomTable.Columns(key).ExtendedProperties.Count > 0 AndAlso mdtDenomTable.Columns(key).ExtendedProperties("dvalue") IsNot Nothing Then
    '                            decTotal += CDec(str) * CInt(mdtDenomTable.Columns(key).ExtendedProperties("dvalue"))
    '                        Else
    '                            decTotal += CDec(str)
    '                        End If

    '                    End If
    '                Next
    '            End If
    '        Next


    '        If e.NewValues.Count > 0 Then
    '            mdtDenomTable.Rows(e.RowIndex).Item("total") = Format(decTotal, Session("fmtCurrency"))
    '            popupCashDenom.DataSource = mdtDenomTable

    '            If CDec(mdtDenomTable.Rows(e.RowIndex).Item("total")) <> CDec(mdtDenomTable.Rows(e.RowIndex).Item("amount")) Then
    '                popupCashDenom.Message = "Sorry, Amount and Total Cash Denomination Amount must be same for All employees."
    '                popupCashDenom.DataBind()
    '                popupCashDenom.Show()
    '                e.Cancel = True
    '                Exit Sub
    '            End If

    '            popupCashDenom.EditIndex = -1
    '            popupCashDenom.DataBind()
    '        End If
    '        popupCashDenom.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub

    'Nilay (28-Aug-2015) -- End

    Protected Sub popupCashDenom_buttonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupCashDenom.buttonSave_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objPaymentTran As New clsPayment_tran
        'Hemant (04 Sep 2020) -- End
        Try
            popupCashDenom.Message = ""
            mdtDenomTable = popupCashDenom.CashDataSource

            If mdtDenomTable IsNot Nothing Then

                'Nilay (28-Aug-2015) -- Start
                'For Each dtRow As DataRow In mdtDenomTable.Rows
                '    If CDec(dtRow.Item("amount")) <> CDec(dtRow.Item("total")) Then
                '        popupCashDenom.Message = "Sorry, Amount and Total Cash Denomination Amount must be same for All employees."
                '        popupCashDenom.Show()
                '        Exit Try
                '    End If
                'Next
                'Nilay (28-Aug-2015) -- End


                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                'Dim objPaymentTran As New clsPayment_tran
                'Hemant (04 Sep 2020) -- End
                objPaymentTran._Denom_Table = mdtDenomTable

                If SaveData(objPaymentTran, mstrCashEmpIDs) = False Then

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region " GridView's Events "
    Protected Sub GvGlobalPayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvGlobalPayment.RowDataBound
        Try
            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If e.Row.RowType = DataControlRowType.Header Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'e.Row.Cells(1).Text = "Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (" & dtPayment.Select("employeename <> 'None' AND amount <> 0").Length & ")"
                e.Row.Cells(1).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.GvGlobalPayment.Columns(1).FooterText, Me.GvGlobalPayment.Columns(1).HeaderText) & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (" & dtPayment.Select("employeename <> 'None' AND amount <> 0").Length & ")"
                'Anjan [04 June 2014] -- End
            End If
            'Sohail (18 Feb 2014) -- End
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(1).Text = "None" AndAlso e.Row.Cells(2).Text = "0" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Sohail (04 Jan 2014) -- Start
    'Enhancement - Oman
#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Sohail (04 Jan 2014) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 
            Me.lblPayPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblPayYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayYear.ID, Me.lblPayYear.Text)
            Me.btnProcess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnProcess.ID, Me.btnProcess.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPaymentDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPaymentDate.ID, Me.lblPaymentDate.Text)
            Me.lblPaymentMode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPaymentMode.ID, Me.lblPaymentMode.Text)
            Me.lblCheque.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCheque.ID, Me.lblCheque.Text)
            Me.lblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblBankGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBankGroup.ID, Me.lblBankGroup.Text)
            Me.gbAdvanceAmountInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbAdvanceAmountInfo.ID, Me.gbAdvanceAmountInfo.Text)
            Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbFilterCriteria.ID, Me.gbFilterCriteria.Text)
            Me.gbPaymentInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbPaymentInfo.ID, Me.gbPaymentInfo.Text)
            Me.lblTotalPaymentAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTotalPaymentAmount.ID, Me.lblTotalPaymentAmount.Text)
            Me.lblAgainstVoucher.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAgainstVoucher.ID, Me.lblAgainstVoucher.Text)
            Me.lblCashDescr.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCashDescr.ID, Me.lblCashDescr.Text)
            Me.lblCashPerc.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCashPerc.ID, Me.lblCashPerc.Text)
            Me.lblACHolder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblACHolder.ID, Me.lblACHolder.Text)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblAccountNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAccountNo.ID, Me.lblAccountNo.Text)
            Me.lnkAdvanceFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Me.lblEmpBank.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpBank.ID, Me.lblEmpBank.Text)
            Me.lblEmpBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpBranch.ID, Me.lblEmpBranch.Text)
            Me.lblCutOffAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCutOffAmount.ID, Me.lblCutOffAmount.Text)
            Me.lblPayPoint.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayPoint.ID, Me.lblPayPoint.Text)
            Me.lblPayType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayType.ID, Me.lblPayType.Text)
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemarks.ID, Me.lblRemarks.Text)

            Me.GvGlobalPayment.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvGlobalPayment.Columns(1).FooterText, Me.GvGlobalPayment.Columns(1).HeaderText)
            Me.GvGlobalPayment.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvGlobalPayment.Columns(2).FooterText, Me.GvGlobalPayment.Columns(2).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
