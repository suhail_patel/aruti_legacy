﻿<%@ Page Title="Paye Deductions FormP9 Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_PayeDeductionsFormP9Report.aspx.vb" Inherits="Payroll_Rpt_PayeDeductionsFormP9Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Paye Deductions FormP9 Report"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <asp:UpdatePanel ID="updt_pnl" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblFYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboFYear" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="max-height: 270px;">
                                                    <asp:GridView ID="gvPeriod" runat="server"  AutoGenerateColumns="False"
                                                        AllowPaging="False" Width="99%"  CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAllPeriod" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAllPeriod_OnCheckedChanged"
                                                                        Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelectPeriod" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                        OnCheckedChanged="chkSelectPeriod_OnCheckedChanged" Text=" " />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="periodunkid" HeaderText="periodunkid" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="period_name" HeaderText="Pay Period" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                FooterText="lblPeriods" />
                                                            <asp:BoundField DataField="start_date" HeaderText="start_date" ReadOnly="true" Visible="false"
                                                                HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="end_date" HeaderText="end_date" ReadOnly="true" Visible="false"
                                                                HeaderStyle-HorizontalAlign="Left" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
