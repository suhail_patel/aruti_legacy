﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Payroll_wPg_PaymentApproval
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    Private mintTotalEmployeeCount As Integer = 0
    Private mintTotalPaidEmployee As Integer = 0
    Private mintCurrLevelPriority As Integer = -1

    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime

    Private mdtApproval As DataTable


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmApproveDisapprovePaymentList"
    'Anjan [04 June 2014] -- End

    'Shani(18-Aug-2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    Private objPaymentApproval As New clsPayment_approval_tran
    'Shani(18-Aug-2015) -- End

#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim objApproverMap As New clspayment_approver_mapping
        Dim objPeriod As New clscommom_period_Tran
        'Dim objEmp As New clsEmployee_Master
        'Dim objPayment As New clsPayment_tran
        'Sohail (05 Mar 2014) -- Start
        'Enhancement - 
        Dim objMaster As New clsMasterData
        Dim intCurrPeriodId As Integer = 0
        'Sohail (05 Mar 2014) -- End
        Dim dsCombo As DataSet
        Try

            dsCombo = objApproverMap.GetList("ApproverLevel", CInt(Session("UserId")))
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
            End If

            intCurrPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1) 'Sohail (05 Mar 2014)

            'Shani(18-Aug-2015) -- Start
            'Enhancement - List all approvals with their status on Payment Approval List screen.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", False, 0, , , Session("Database_Name"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", False, 1, , , Session("Database_Name"))
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", False, 1)
            'Shani(20-Nov-2015) -- End

            'Shani(18-Aug-2015) -- End
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()
                'Sohail (05 Mar 2014) -- Start
                'Enhancement - 
                If intCurrPeriodId > 0 Then
                    .SelectedValue = CStr(intCurrPeriodId)
                Else
                    .SelectedIndex = 0
                End If
                'Sohail (05 Mar 2014) -- End
                If .Items.Count > 0 Then
                    Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs)
                End If

            End With

            'Anjan [ 09 Sep 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            'mintTotalEmployeeCount = objEmp.GetEmployeeCount(True, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate, , , Session("AccessLevelFilterString"))

            'dsCombo = objPayment.GetListByPeriod("PaidEmp", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT)
            'mintTotalPaidEmployee = dsCombo.Tables("PaidEmp").Rows.Count
            'Anjan [ 09 Sep 2013 ] -- End




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMap = Nothing
            objPeriod = Nothing
            'objEmp = Nothing
            'objPayment = Nothing
        End Try
    End Sub

    Private Sub CreateListTable()
        Try
            mdtApproval = New DataTable
            With mdtApproval
                .Columns.Add("levelname", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("auditusername", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("priority", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("totempcount", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("approval_date", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("totapproved", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("totdisapproved", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("totpending", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("remarks", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani(18-Aug-2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    '    Private Sub FillList()
    '        Dim objPaymentApproval As New clsPayment_approval_tran
    '        Dim dsList As DataSet
    '        Dim dtTable As DataTable
    '        Dim intCurrPayUnkID As Integer = 0
    '        Dim intPrevPayUnkID As Integer = 0
    '        Dim intCurrPriority As Integer = 0
    '        Dim intPrevPriority As Integer = 0
    '        Dim intCurrUserID As Integer = 0
    '        Dim intPrevUserID As Integer = 0
    '        Try

    '            mdtApproval.Rows.Clear()

    '            dsList = objPaymentApproval.GetApproverStatus("Approval", CInt(cboPeriod.SelectedValue), mintCurrLevelPriority)
    '            dtTable = New DataView(dsList.Tables("Approval"), "1 = 2", "", DataViewRowState.CurrentRows).ToTable

    '            For Each dsRow As DataRow In dsList.Tables("Approval").Rows
    '                intCurrPayUnkID = CInt(dsRow.Item("paymenttranunkid"))
    '                intCurrPriority = CInt(dsRow.Item("priority"))

    '                If intPrevPayUnkID = intCurrPayUnkID AndAlso intPrevPriority < intCurrPriority Then
    '                    GoTo ContinueFor
    '                End If


    '                dtTable.ImportRow(dsRow)


    'ContinueFor:
    '                intPrevPayUnkID = CInt(dsRow.Item("paymenttranunkid"))
    '                intPrevPriority = CInt(dsRow.Item("priority"))
    '            Next

    '            Dim strFilter As String = ""

    '            'Sohail (12 Jun 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'dtTable = New DataView(dtTable, strFilter, "priority, approval_date, auditusername", DataViewRowState.CurrentRows).ToTable
    '            dtTable = New DataView(dtTable, strFilter, "levelname, priority, approval_date, auditusername", DataViewRowState.CurrentRows).ToTable
    '            'Sohail (12 Jun 2013) -- End

    '            Dim intCount As Integer = 0
    '            Dim intTotApproved As Integer
    '            Dim intTotDisapproved As Integer
    '            Dim dtCurrApprovalDate As DateTime = Nothing
    '            Dim dtPrevApprovalDate As DateTime = Nothing
    '            Dim drRow() As DataRow
    '            intCurrPriority = 0
    '            intPrevPriority = 0

    '            Dim dRow As DataRow

    '            For Each dtRow As DataRow In dtTable.Rows
    '                intCurrPriority = CInt(dtRow.Item("priority"))
    '                intCurrUserID = CInt(dtRow.Item("audituserunkid"))
    '                dtCurrApprovalDate = CDate(dtRow.Item("approval_date"))

    '                If intPrevPriority <> intCurrPriority OrElse intPrevUserID <> intCurrUserID OrElse dtPrevApprovalDate <> dtCurrApprovalDate Then

    '                    dRow = mdtApproval.NewRow

    '                    dRow.Item("levelname") = dtRow.Item("levelname").ToString

    '                    dRow.Item("auditusername") = dtRow.Item("auditusername").ToString
    '                    dRow.Item("priority") = dtRow.Item("priority").ToString

    '                    dRow.Item("totempcount") = mintTotalEmployeeCount.ToString

    '                    dRow.Item("approval_date") = CDate(dtRow.Item("approval_date"))

    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND approval_date = '" & dtCurrApprovalDate & "' AND audituserunkid = " & intCurrUserID & " AND statusunkid = 1 ")
    '                    dRow.Item("totapproved") = drRow.Length.ToString

    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND approval_date = '" & dtCurrApprovalDate & "' AND audituserunkid = " & intCurrUserID & " AND statusunkid = 2 ")
    '                    dRow.Item("totdisapproved") = drRow.Length.ToString

    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND statusunkid = 1 ")
    '                    intTotApproved = drRow.Length

    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND statusunkid = 2 ")
    '                    intTotDisapproved = drRow.Length

    '                    dRow.Item("totpending") = (mintTotalPaidEmployee - (intTotApproved + intTotDisapproved)).ToString

    '                    dRow.Item("remarks") = dtRow.Item("remarks").ToString

    '                    mdtApproval.Rows.Add(dRow)
    '                Else

    '                    GoTo ContinueNext
    '                End If


    'ContinueNext:
    '                intPrevPriority = intCurrPriority
    '                intPrevUserID = intCurrUserID
    '                dtPrevApprovalDate = dtCurrApprovalDate
    '            Next

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objPaymentApproval = Nothing

    '            If mdtApproval.Rows.Count <= 0 Then
    '                Dim r As DataRow = mdtApproval.NewRow

    '                r.Item(1) = "None" ' To Hide the row and display only Row Header
    '                r.Item(2) = ""

    '                mdtApproval.Rows.Add(r)
    '            End If

    '            gvApproval.DataSource = mdtApproval
    '            gvApproval.DataBind()
    '        End Try
    '    End Sub
    Private Sub FillList()
        Dim dsList As DataSet
        Try
            dsList = objPaymentApproval.GetApprovalStatus("Approval", CInt(cboPeriod.SelectedValue), mintCurrLevelPriority, , CInt(cboEmployee.SelectedValue))
            Dim dtTable As DataTable = dsList.Tables("Approval").Clone
            dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
            Dim strEmpName As String = ""
            Dim dtRow As DataRow = Nothing
            For Each dRow As DataRow In dsList.Tables("Approval").Rows
                If strEmpName.Trim <> dRow("employeename").ToString.Trim Then
                    dtRow = dtTable.NewRow
                    strEmpName = dRow("employeename").ToString.Trim
                    dtRow("employeename") = strEmpName
                    dtRow("IsGrp") = True
                    dtTable.Rows.Add(dtRow)
                End If
                dtRow = dtTable.NewRow
                For Each dtCol As DataColumn In dsList.Tables("Approval").Columns
                    dtRow(dtCol.ColumnName) = dRow(dtCol.ColumnName)
                Next
                dtRow("IsGrp") = False
                dtTable.Rows.Add(dtRow)
            Next
            gvApproval.DataSource = dtTable
            gvApproval.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate)
            'Else
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'End If
            Dim dtStartDate As Date = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            Dim dtEndDate As Date = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                dtStartDate = mdtPeriodStartDate
                dtEndDate = mdtPeriodEndDate
            End If

            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnksId"), _
            '                                    dtStartDate, _
            '                                    dtEndDate, _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Employee", True)

            dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                dtStartDate, _
                                                dtEndDate, _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  False, "Employee", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Shani(18-Aug-2015) -- End

    Private Sub SetVisibility()
        Try
            btnApprove.Enabled = CBool(Session("AllowToApprovePayment"))
            btnVoidApproved.Enabled = CBool(Session("AllowToVoidApprovedPayment"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Page's Event "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mintTotalEmployeeCount", mintTotalEmployeeCount)
        Me.ViewState.Add("mintTotalPaidEmployee", mintTotalPaidEmployee)
        Me.ViewState.Add("mintCurrLevelPriority", mintCurrLevelPriority)

        Me.ViewState.Add("mdtPeriodStartDate", mdtPeriodStartDate)
        Me.ViewState.Add("mdtPeriodEndDate", mdtPeriodEndDate)

        Me.ViewState.Add("mdtApproval", mdtApproval)
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If


            

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            If Me.IsPostBack = False Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End

                Call SetVisibility()
                Call CreateListTable()
                Call FillCombo()
                'Shani(18-Aug-2015) -- Start
                'Enhancement - List all approvals with their status on Payment Approval List screen.
                Call FillEmployeeCombo()
                'Shani(18-Aug-2015) -- End
                Call FillList()

            Else
                mintTotalEmployeeCount = CInt(ViewState("mintTotalEmployeeCount"))
                mintTotalPaidEmployee = CInt(ViewState("mintTotalPaidEmployee"))
                mintCurrLevelPriority = CInt(ViewState("mintCurrLevelPriority"))

                mdtPeriodStartDate = CDate(ViewState("mdtPeriodStartDate"))
                mdtPeriodEndDate = CDate(ViewState("mdtPeriodEndDate"))

                mdtApproval = CType(ViewState("mdtApproval"), DataTable)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (12 Jun 2013) -- Start
    'TRA - ENHANCEMENTc

    'Shani(18-Aug-2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    'Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
    '    Dim strPreviousGroup As String = ""
    '    Dim intRowIdx As Integer = 0
    '    Try

    '        If gvApproval.Controls.Count > 0 AndAlso gvApproval.Controls(0) IsNot Nothing Then
    '            Dim gvTable As Table = DirectCast(gvApproval.Controls(0), Table)

    '            For Each row As GridViewRow In gvApproval.Items

    '                If row.Visible = True AndAlso strPreviousGroup <> mdtApproval.Rows(row.RowIndex).Item("levelname").ToString Then
    '                    intRowIdx = gvTable.Rows.GetRowIndex(row)

    '                    Dim gvRow As New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)

    '                    Dim cell As New TableCell
    '                    cell.Text = "Level : " & mdtApproval.Rows(row.RowIndex).Item("levelname").ToString
    '                    cell.ColumnSpan = gvApproval.Columns.Count
    '                    cell.CssClass = "GroupHeaderStyle"
    '                    gvRow.Cells.Add(cell)

    '                    gvTable.Controls.AddAt(intRowIdx, gvRow)

    '                End If

    '                strPreviousGroup = mdtApproval.Rows(row.RowIndex).Item("levelname").ToString
    '            Next
    '        End If

    '        MyBase.Render(writer)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Shani(18-Aug-2015) -- End

    'Sohail (12 Jun 2013) -- End

#End Region

#Region " Button's Event "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Session("mblnFromApprove") = True
            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect("~/Payroll/wPg_Paymentapprovedisapprove.aspx")
            Response.Redirect(Session("rootpath").ToString & "Payroll/wPg_Paymentapprovedisapprove.aspx")
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnVoidApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoidApproved.Click
        Try
            Session("mblnFromApprove") = False
            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect("~/Payroll/wPg_Paymentapprovedisapprove.aspx")
            Response.Redirect(Session("rootpath").ToString & "Payroll/wPg_Paymentapprovedisapprove.aspx")
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboPeriod.SelectedIndex = 0
            'Shani(18-Aug-2015) -- Start
            'Enhancement - List all approvals with their status on Payment Approval List screen.
            cboEmployee.SelectedValue = "0"
            'Shani(18-Aug-2015) -- End
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Combobox's Events "
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Dim objEmp As New clsEmployee_Master
        Dim objPayment As New clsPayment_tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date

                'Anjan [ 09 Sep 2013 ] -- Start
                'ENHANCEMENT : Recruitment TRA changes requested by Andrew

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'mintTotalEmployeeCount = objEmp.GetEmployeeCount(True, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate, , , Session("AccessLevelFilterString"), True)

                'Nilay (10-Feb-2016) -- Start
                'mintTotalEmployeeCount = objEmp.GetEmployeeCount(Session("Database_Name"), _
                '                                                Session("UserId"), _
                '                                                Session("Fin_year"), _
                '                                                Session("CompanyUnkId"), _
                '                                                mdtPeriodStartDate, _
                '                                                mdtPeriodEndDate, _
                '                                                Session("UserAccessModeSetting"), True, False)
                'Nilay (10-Feb-2016) -- End

                'Shani(24-Aug-2015) -- End

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objPayment.GetListByPeriod("PaidEmp", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT)

                'Nilay (10-Feb-2016) -- Start
                'dsList = objPayment.GetListByPeriod(Session("Database_Name"), _
                '                                  Session("UserId"), _
                '                                  Session("Fin_year"), _
                '                                  Session("CompanyUnkId"), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  Session("UserAccessModeSetting"), True, _
                '                                  Session("IsIncludeInactiveEmp"), "PaidEmp", _
                '                                  clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                  CInt(cboPeriod.SelectedValue), _
                '                                  clsPayment_tran.enPayTypeId.PAYMENT)

                dsList = objPayment.GetListByPeriod(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    mdtPeriodStartDate, _
                                                    mdtPeriodEndDate, _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "PaidEmp", _
                                                    clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                    CInt(cboPeriod.SelectedValue), _
                                                    clsPayment_tran.enPayTypeId.PAYMENT, , True)
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                mintTotalPaidEmployee = dsList.Tables("PaidEmp").Rows.Count
                'Anjan [ 09 Sep 2013 ] -- End
            End If

            'Shani(18-Aug-2015) -- Start
            'Enhancement - List all approvals with their status on Payment Approval List screen.
            Call FillEmployeeCombo()
            'Shani(18-Aug-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            dsList = Nothing
            objEmp = Nothing
            objPayment = Nothing
        End Try
    End Sub
#End Region

#Region " GridView Events "
    'Shani(18-Aug-2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    'Protected Sub gvApproval_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproval.RowDataBound
    '    Try
    '        'If e.Row.RowType = DataControlRowType.DataRow Then
    '        '    If e.Row.Cells(0).Text = "None" AndAlso e.Row.Cells(1).Text = "&nbsp;" Then
    '        '        e.Row.Visible = False
    '        '        'Sohail (12 Jun 2013) -- Start
    '        '        'TRA - ENHANCEMENT
    '        '    Else
    '        '        e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#ddd'")
    '        '        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=''")
    '        '        'Sohail (12 Jun 2013) -- End
    '        '    End If
    '        'End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    Protected Sub gvApproval_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvApproval.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                e.Item.Cells(0).Text = e.Item.Cells(1).Text
                e.Item.Cells(0).ColumnSpan = 2
                e.Item.Cells(1).Visible = False
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If CBool(e.Item.Cells(10).Text) = True Then
                    e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                    e.Item.Cells(0).Style.Add("text-align", "left")
                    e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    For i = 1 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else
                    e.Item.Cells(0).Attributes.Add("EmpName", e.Item.Cells(0).Text)
                    e.Item.Cells(0).Text = e.Item.Cells(1).Text
                    e.Item.Cells(0).ColumnSpan = 2
                    e.Item.Cells(1).Visible = False

                    If CDate(e.Item.Cells(7).Text).Date = CDate("08-Aug-8888") Then
                        e.Item.Cells(7).Text = ""
                    Else
                        e.Item.Cells(7).Text = Format(CDate(e.Item.Cells(7).Text), "dd-MMM-yyyy HH:mm:ss")
                    End If

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani(18-Aug-2015) -- End

#End Region

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.btnVoidApproved.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVoidApproved.ID, Me.btnVoidApproved.Text).Replace("&", "")
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")

            Me.gvApproval.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(0).FooterText, Me.gvApproval.Columns(0).HeaderText)
            Me.gvApproval.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(1).FooterText, Me.gvApproval.Columns(1).HeaderText)
            Me.gvApproval.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(2).FooterText, Me.gvApproval.Columns(2).HeaderText)
            Me.gvApproval.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(3).FooterText, Me.gvApproval.Columns(3).HeaderText)
            Me.gvApproval.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(4).FooterText, Me.gvApproval.Columns(4).HeaderText)
            Me.gvApproval.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(5).FooterText, Me.gvApproval.Columns(5).HeaderText)
            Me.gvApproval.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvApproval.Columns(6).FooterText, Me.gvApproval.Columns(6).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End




End Class
