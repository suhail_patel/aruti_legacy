﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_PayslipReport.aspx.vb"
    Inherits="Payroll_wPg_PayslipReport" Title="Salary Slip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>
    <asp:Panel ID="pnlESS" runat="server">
        <asp:UpdatePanel ID="uppnl_mianESS" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Salary Slip" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="lblFinancialYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="drpFinancialYear" runat="server" Width="95%" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label></label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="drpPeriod" runat="server" Width="95%">
                                                    </asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="drpFinancialYear" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btn btn-primary"
                                    ValidationGroup="Payslip" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default"
                                    ValidationGroup="Payslip" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Panel ID="pnlMSS" runat="server">
        <asp:UpdatePanel ID="uppnl_mianMSS" runat="server">
            <ContentTemplate>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="Label1" runat="server" Text="Salary Slip" CssClass="form-label"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                    <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembership" runat="server" Text="Membership" CssClass="form-label"></asp:Label></label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboMembership" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLeaveType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCurrency" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkShowEachPayslipOnNewPage" runat="server" Text="Show Each Payslip On New Page"
                                            Checked="true" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowTwoPayslipPerPage" runat="server" Text="Show Two Payslip Per Page"
                                            AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowBankAccountNo" runat="server" Text="Show Bank Account No."
                                            AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowLoanReceived" runat="server" Text="Show Loan Received" Checked="true"
                                            Visible="false" AutoPostBack="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix  d--f jc--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <%--<asp:TextBox ID="txtSearchEmployee" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>--%>
                                                    <input type="text" id="txtSearchEmployee" name="txtSearchEmployee"
                                                           placeholder="Search Employee" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchEmployee', '<%= dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'chkSelect');" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                <asp:CheckBox ID="chkEmpOnlyTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                    CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchEmployee', '<% dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'chkSelect');" />
                                            </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 315px;">
                                                <asp:GridView ID="dgvEmployee" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="employeeunkid">                                                    
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-CssClass="headerstyle" ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" onclick="CheckAll(this, 'chkSelect');"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" 
                                                                    Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                            Visible="false"></asp:BoundField>
                                                        <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                            FooterText="lblEmployees">
                                                        </asp:BoundField>
                                                    </Columns>                                                    
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 370px;">
                                                <asp:GridView ID="gvPeriod" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                    AllowPaging="False" Width="99%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="periodunkid, start_date, end_date">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-CssClass="headerstyle" ItemStyle-CssClass="itemstyle"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAllPeriod" runat="server" AutoPostBack="false" onclick="CheckAll(this, 'chkSelectPeriod');"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelectPeriod" runat="server" AutoPostBack="false" 
                                                                     Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="periodunkid" HeaderText="periodunkid" ReadOnly="true"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="period_name" HeaderText="Pay Period" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                            FooterText="lblPeriods" />
                                                        <asp:BoundField DataField="start_date" HeaderText="start_date" ReadOnly="true" Visible="false"
                                                            HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="end_date" HeaderText="end_date" ReadOnly="true" Visible="false"
                                                            HeaderStyle-HorizontalAlign="Left" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnMSSReport" runat="server" CssClass="btn btn-primary" Text="Report"
                                    ValidationGroup="PayslipMSS" />
                                <asp:Button ID="btnMSSClose" runat="server" Text="Close" CssClass="btn btn-default"
                                    ValidationGroup="PayslipMSS" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
    var scroll = {
        Y: '#<%= hfScrollPosition.ClientID %>'
    };
    var scroll1 = {
        Y: '#<%= hfScrollPosition1.ClientID %>'
    };
    function pageLoad(sender, args) {
        $("select").searchable();

    }

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
    }
    }

    function CheckAll(chkbox, chkgvSelect) {
        var grid = $(chkbox).closest(".table-responsive");
        //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
        $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));        
    }

    function FromSearching(txtID, gridClientID, chkID, chkGvID) {
        if (gridClientID.toString().includes('%') == true)
            gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

        if ($('#' + txtID).val().length > 0) {
            $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
            $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
            //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
            }

        }
        else if ($('#' + txtID).val().length == 0) {
            resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
        }
        if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
            $('.norecords').remove();
        }

        if (event.keyCode == 27) {
            resetFromSearchValue(txtID, gridClientID, chkID);
        }
    }

    function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {

        $('#' + txtID).val('');
        //$('#' + gridClientID + ' tr').show();
        //$('.norecords').remove();
        if (chkID != null && $$(chkID).is(':checked') == true) {
            $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
            $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
            $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
        }
        else {
            $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
            $('.norecords').remove();
        }
        $('#' + txtID).focus();
    }
    
    </script>

</asp:Content>
