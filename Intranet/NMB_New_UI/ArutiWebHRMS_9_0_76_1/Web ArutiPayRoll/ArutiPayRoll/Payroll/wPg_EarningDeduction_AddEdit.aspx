﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EarningDeduction_AddEdit.aspx.vb"
    Inherits="Payroll_wPg_EarningDeduction_AddEdit" Title="Add / Edit Employee Earning and Deduction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>--%>

    <script type="text/javascript">

     function onlyNumbers(txtBox, e) {
         //        var e = event || evt; // for trans-browser compatibility
         //        var charCode = e.which || e.keyCode;
         if (window.event)
             var charCode = window.event.keyCode;       // IE
         else
             var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

         if (charCode == 13)
             return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc9:ConfirmYesNo ID="popupFormulaAssign" runat="server" Title="" />
                <uc9:ConfirmYesNo ID="popupPreviousED" runat="server" Title="" />
                <div class="row clearfix  d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%--<asp:Label ID="lblDetialHeader" runat="server" Text="Add / Edit Employee Earning and Deduction"
                                        CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Employee Earning and Deduction"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Effective Period" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" data-live-search="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" data-live-search="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrnHeadType" runat="server" Text="Transaction Head Type" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true" data-live-search="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTypeOf" runat="server" Text="Transaction Type Of" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTypeOf" runat="server" AutoPostBack="true" data-live-search="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrnHead" runat="server" Text="Transaction Head" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTrnHead" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAmount" runat="server" Text="0" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCostCenter" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMedicalRefNo" runat="server" Text="Reference No." CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtMedicalRefNo" runat="server" Text="" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCumulativeStartDate" runat="server" Text="Cumulative Start Date"
                                            CssClass="form-label" />
                                        <uc2:DateCtrl ID="dtpCumulativeStartDate" runat="server"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEDStartDate" runat="server" Text="E. D. Start Date" CssClass="form-label" />
                                        <uc2:DateCtrl ID="dtpEDStartDate" runat="server"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStopDate" runat="server" Text="Stop Date" CssClass="form-label" />
                                        <uc2:DateCtrl ID="dtpStopDate" runat="server"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous ED Slab"
                                            CssClass="filled-in" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkOverwritePrevEDSlabHeads" runat="server" Text="Overwrite Previous ED Slab heads"
                                            CssClass="filled-in" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDefCC" runat="server" Text="" CssClass="form-label" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSaveEDHistory" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
