﻿Option Strict On 'Nilay (10-Feb-2016)
#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Payroll_wPg_GlobalVoidPayment
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    Dim mstrVoidIds As String = ""
    Dim mdtPayment As DataTable

    Private mstrAdvanceFilter As String = "" 'Sohail (03 Jul 2014)


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmGlobalVoidPayment"
    'Anjan [04 June 2014] -- End

    'Nilay (10-Feb-2016) -- Start
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End
    Private dtActAdj As New DataTable 'Sohail (23 May 2017)
#End Region

#Region " Private Functions & Methods "

    Private Sub SetVisibility()
        Try
            btnVoid.Enabled = CBool(Session("DeletePayment"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                   CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombos.Tables("Emp")
                .DataBind()
            End With


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", False, 1, , , Session("Database_Name"))
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", False, 1)
            'Shani(20-Nov-2015) -- End

            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Period")
                .DataBind()
                Call cboPayPeriod_SelectedIndexChanged(cboPayPeriod, New System.EventArgs)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objPeriod = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsCombos) = False Then
                dsCombos.Clear()
                dsCombos = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub CreateListTable()
        Try
            mdtPayment = New DataTable
            With mdtPayment
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False 'Sohail (12 Jun 2013)
                .Columns.Add("paymenttranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("PaymentDate", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("VoucherNo", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("EmpName", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("PeriodName", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("expaidamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("paidcurrency", System.Type.GetType("System.String")).DefaultValue = ""
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                .Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (23 May 2017) -- End
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsPayment As DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty

        Dim objPaymentTran As New clsPayment_tran
        Dim objExrate As clsExchangeRate

        Dim decPaidAmount As Decimal
        Dim decPaidAmountTo As Decimal

        Try
            mdtPayment.Rows.Clear()
            If CBool(Session("AllowToViewGlobalVoidPaymentList")) = False Then Exit Try

            'lvPayment.Items.Clear()

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsPayment = objPaymentTran.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, cboEmployee.SelectedValue.ToString, Session("AccessLevelFilterString"), mstrAdvanceFilter)
            'Nilay (03-Feb-2016) -- Start
            'Accessfilter parameter problem

            'Nilay (10-Feb-2016) -- Start
            'dsPayment = objPaymentTran.GetListByPeriod(Session("Database_Name"), _
            '                                          Session("UserId"), _
            '                                          Session("Fin_year"), _
            '                                          Session("CompanyUnkId"), _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                          Session("UserAccessModeSetting"), True, _
            '                                          Session("IsIncludeInactiveEmp"), "Payment", _
            '                                          clsPayment_tran.enPaymentRefId.PAYSLIP, _
            '                                          CInt(cboPayPeriod.SelectedValue), _
            '                                          clsPayment_tran.enPayTypeId.PAYMENT, _
            '                                          cboEmployee.SelectedValue.ToString, _
            '                                          True, mstrAdvanceFilter)

            dsPayment = objPaymentTran.GetListByPeriod(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       mdtPeriodStartDate, _
                                                       mdtPeriodEndDate, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "Payment", _
                                                       clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                       CInt(cboPayPeriod.SelectedValue), _
                                                       clsPayment_tran.enPayTypeId.PAYMENT, _
                                                       cboEmployee.SelectedValue.ToString, _
                                                   True, mstrAdvanceFilter)
            'Nilay (10-Feb-2016) -- End

            'Nilay (03-Feb-2016) -- End
            'Shani(20-Nov-2015) -- End

            'Sohail (03 Jul 2014) - [AccessLevelFilterString]

            Decimal.TryParse(txtPaidAmount.Text, decPaidAmount)
            Decimal.TryParse(txtPaidAmountTo.Text, decPaidAmountTo)

            If decPaidAmount <> 0 AndAlso decPaidAmountTo <> 0 Then
                StrSearching &= "AND Amount >= " & decPaidAmount & " AND Amount <= " & decPaidAmountTo & " "
            End If

            'Sohail (03 Jul 2014) -- Start
            'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
            'If txtVoucherNo.Text.Trim <> "" Then
            '    StrSearching &= "AND voucherno LIKE '%" & CStr(txtVoucherNo.Text) & "%'" & " "
            'End If
            If CInt(cboPmtVoucher.SelectedIndex) > 0 Then
                StrSearching &= "AND voucherno = '" & cboPmtVoucher.SelectedItem.Text & "'" & " "
            End If
            'Sohail (03 Jul 2014) -- End

            If dtpPaymentDate.IsNull = False Then
                StrSearching &= "AND PaymentDate = '" & eZeeDate.convertDate(dtpPaymentDate.GetDate) & "'" & "  "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                'Sohail (12 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'dtTable = New DataView(dsPayment.Tables("Payment"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(dsPayment.Tables("Payment"), StrSearching, "voucherno", DataViewRowState.CurrentRows).ToTable
                'Sohail (12 Jun 2013) -- End
            Else
                'Sohail (12 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'dtTable = dsPayment.Tables("Payment")
                dtTable = New DataView(dsPayment.Tables("Payment"), "", "voucherno", DataViewRowState.CurrentRows).ToTable
                'Sohail (12 Jun 2013) -- End
            End If

            Dim dRow As DataRow
            Dim strPrevVoucher As String = "" 'Sohail (12 Jun 2013)

            For Each dtRow As DataRow In dtTable.Rows

                'Sohail (12 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                If strPrevVoucher <> dtRow.Item("VoucherNo").ToString Then
                    dRow = mdtPayment.NewRow

                    dRow.Item("IsGrp") = True
                    dRow.Item("PaymentDate") = dtRow.Item("VoucherNo").ToString

                    mdtPayment.Rows.Add(dRow)
                End If
                'Sohail (12 Jun 2013) -- End

                dRow = mdtPayment.NewRow

                dRow.Item("IsGrp") = False
                dRow.Item("paymenttranunkid") = CInt(dtRow.Item("paymenttranunkid"))
                dRow.Item("PaymentDate") = eZeeDate.convertDate(dtRow.Item("PaymentDate").ToString).ToString(Session("DateFormat").ToString)
                dRow.Item("VoucherNo") = dtRow.Item("VoucherNo").ToString
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("EmpName") = dtRow.Item("EmpName").ToString
                dRow.Item("PeriodName") = dtRow.Item("PeriodName").ToString
                dRow.Item("expaidamt") = Format(CDec(dtRow.Item("expaidamt")), Session("fmtCurrency").ToString)

                objExrate = New clsExchangeRate
                objExrate._ExchangeRateunkid = CInt(dtRow.Item("paidcurrencyid"))

                dRow.Item("paidcurrency") = objExrate._Currency_Sign

                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                'Sohail (23 May 2017) -- End

                mdtPayment.Rows.Add(dRow)

                strPrevVoucher = dtRow.Item("VoucherNo").ToString 'Sohail (12 Jun 2013)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentTran = Nothing
            objExrate = Nothing

            If mdtPayment.Rows.Count <= 0 Then
                Dim r As DataRow = mdtPayment.NewRow

                r.Item(3) = "None" ' To Hide the row and display only Row Header
                r.Item(4) = ""

                mdtPayment.Rows.Add(r)
            End If

            GvVoidPayment.DataSource = mdtPayment
            GvVoidPayment.DataBind()
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsPayment) = False Then
                dsPayment.Clear()
                dsPayment = Nothing
            End If
            If IsNothing(dtTable) = False Then
                dtTable.Clear()
                dtTable = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In GvVoidPayment.Rows
                If CBool(mdtPayment.Rows(gvr.RowIndex).Item("IsGrp")) = True OrElse (CInt(mdtPayment.Rows(gvr.RowIndex).Item("paymenttranunkid")) > 0 AndAlso mdtPayment.Rows(gvr.RowIndex).Item("employeecode").ToString <> "") Then
                    cb = CType(GvVoidPayment.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                    cb.Checked = blnCheckAll

                    mdtPayment.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtPayment.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked

                'Sohail (12 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                If CBool(mdtPayment.Rows(gvRow.RowIndex).Item("IsGrp")) = True Then
                    Dim drRow() As DataRow = mdtPayment.Select("voucherno = '" & mdtPayment.Rows(gvRow.RowIndex).Item("paymentdate").ToString & "' AND IsGrp = 0")

                    If drRow.Length > 0 Then
                        For i As Integer = 0 To drRow.Length - 1
                            drRow(i).Item("IsChecked") = chkSelect.Checked
                            Dim gRow As GridViewRow = GvVoidPayment.Rows(gvRow.RowIndex + i + 1)
                            CType(gRow.FindControl("chkSelect"), CheckBox).Checked = chkSelect.Checked
                        Next
                    End If
                Else
                    Dim drTotGrpRow() As DataRow = mdtPayment.Select("voucherno = '" & mdtPayment.Rows(gvRow.RowIndex).Item("voucherno").ToString & "' AND IsGrp = 0")
                    Dim drTotGrpCheckedRow() As DataRow = mdtPayment.Select("voucherno = '" & mdtPayment.Rows(gvRow.RowIndex).Item("voucherno").ToString & "' AND IsGrp = 0 AND IsChecked = 1 ")

                    Dim drGrpRow() As DataRow = mdtPayment.Select("paymentdate = '" & mdtPayment.Rows(gvRow.RowIndex).Item("voucherno").ToString & "' AND IsGrp = 1")
                    Dim intRowIndex As Integer = mdtPayment.Rows.IndexOf(drGrpRow(0))
                    Dim gParentRow As GridViewRow = GvVoidPayment.Rows(intRowIndex)

                    If drTotGrpRow.Length = drTotGrpCheckedRow.Length Then
                        CType(gParentRow.FindControl("chkSelect"), CheckBox).Checked = True
                    Else
                        CType(gParentRow.FindControl("chkSelect"), CheckBox).Checked = False
                    End If

                End If
                'Sohail (12 Jun 2013) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Page's Event "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mstrVoidIds", mstrVoidIds)
        Me.ViewState.Add("mdtPayment", mdtPayment)
        Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter) 'Sohail (03 Jul 2014)
        'Nilay (10-Feb-2016) -- Start
        Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
        Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
        'Nilay (10-Feb-2016) -- End
        Me.ViewState("dtActAdj") = dtActAdj 'Sohail (23 May 2017)
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

        

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            If Me.IsPostBack = False Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End

                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call SetVisibility()
                Call CreateListTable()
                Call FillCombo()

                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                btnVoid.Visible = CBool(Session("DeletePayment"))
                'Pinkal (12-Feb-2015) -- End

            Else
                'Nilay (10-Feb-2016) -- Start
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                'Nilay (10-Feb-2016) -- End
                mstrVoidIds = CStr(ViewState("mstrVoidIds"))
                mdtPayment = CType(ViewState("mdtPayment"), DataTable)
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter")) 'Sohail (03 Jul 2014)
                dtActAdj = CType(ViewState("dtActAdj"), DataTable) 'Sohail (23 May 2017)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (12 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    'Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
    '    Dim strPreviousGroup As String = ""
    '    Dim intRowIdx As Integer = 0
    '    Try

    '        If GvVoidPayment.Controls.Count > 0 AndAlso GvVoidPayment.Controls(0) IsNot Nothing Then
    '            Dim gvTable As Table = DirectCast(GvVoidPayment.Controls(0), Table)

    '            For Each row As GridViewRow In GvVoidPayment.Rows

    '                If row.Visible = True AndAlso strPreviousGroup <> mdtPayment.Rows(row.RowIndex).Item("VoucherNo").ToString Then
    '                    intRowIdx = gvTable.Rows.GetRowIndex(row)

    '                    Dim gvRow As New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)

    '                    Dim cel As New TableCell
    '                    Dim cb As New CheckBox
    '                    cb.Attributes.Add("onclick", "javascript:checkall(this, " & intRowIdx & ") ")
    '                    cel.Text = ""
    '                    cel.Controls.Add(cb)
    '                    'cel.ColumnSpan = GvVoidPayment.Columns.Count
    '                    cel.CssClass = "GroupHeaderStyle" 'GroupHeaderStyleCheckBox
    '                    cel.HorizontalAlign = HorizontalAlign.Center
    '                    cel.BorderWidth = 0
    '                    gvRow.Cells.Add(cel)

    '                    Dim cell As New TableCell
    '                    cell.Text = "Voucher No : " & mdtPayment.Rows(row.RowIndex).Item("VoucherNo").ToString
    '                    cell.ColumnSpan = GvVoidPayment.Columns.Count - 1
    '                    cell.CssClass = "GroupHeaderStyle"
    '                    gvRow.Cells.Add(cell)

    '                    gvTable.Controls.AddAt(intRowIdx, gvRow)

    '                End If

    '                strPreviousGroup = mdtPayment.Rows(row.RowIndex).Item("VoucherNo").ToString
    '            Next
    '        End If

    '        MyBase.Render(writer)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Sohail (12 Jun 2013) -- End
#End Region

#Region " Button's Event "

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            If cboPayPeriod.Items.Count > 0 Then cboPayPeriod.SelectedIndex = 0
            txtPaidAmount.Text = ""
            txtPaidAmountTo.Text = ""
            'Sohail (03 Jul 2014) -- Start
            'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
            'txtVoucherNo.Text = ""
            cboPmtVoucher.SelectedIndex = 0
            mstrAdvanceFilter = ""
            'Sohail (03 Jul 2014) -- End
            dtpPaymentDate.SetDate = Nothing
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnVoid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Dim objPaymentTran As New clsPayment_tran

        Try
            mstrVoidIds = ""
            popupDelPayment.Reason = ""

            'Sohail (12 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'If mdtPayment.Select("IsChecked = 1 ").Length <= 0 Then
            '    DisplayMessage.DisplayMessage("Please select Payment from the list to perform further operation on it.", Me)
            '    Exit Try
            'End If

            'For Each dtRow As DataRow In mdtPayment.Select("IsChecked = 1 ")
            '    mstrVoidIds &= ", " & dtRow.Item("paymenttranunkid").ToString
            'Next
            If mdtPayment.Select("IsChecked = 1 AND IsGrp = 0 ").Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please select Payment from the list to perform further operation on it."), Me)
                'Anjan [04 June 2014] -- End
                Exit Try
            End If

            For Each dtRow As DataRow In mdtPayment.Select("IsChecked = 1 AND IsGrp = 0 ")
                mstrVoidIds &= ", " & dtRow.Item("paymenttranunkid").ToString
            Next
            'Sohail (12 Jun 2013) -- End
            mstrVoidIds = mstrVoidIds.Substring(2)

            strFilter = " prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " AND isauthorized = 1 AND prpayment_tran.paymenttranunkid IN (" & mstrVoidIds & ") "

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPaymentTran.GetList("Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter, Session("AccessLevelFilterString"))

            'Nilay (10-Feb-2016) -- Start
            'dsList = objPaymentTran.GetList(Session("Database_Name"), _
            '                              Session("UserId"), _
            '                              Session("Fin_year"), _
            '                              Session("CompanyUnkId"), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                              Session("UserAccessModeSetting"), True, _
            '                              Session("IsIncludeInactiveEmp"), _
            '                              "Authorized", _
            '                              clsPayment_tran.enPaymentRefId.PAYSLIP, 0, _
            '                              clsPayment_tran.enPayTypeId.PAYMENT, _
            '                              Session("AccessLevelFilterString"), strFilter)

            dsList = objPaymentTran.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            mdtPeriodStartDate, _
                                            mdtPeriodEndDate, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), _
                                            "Authorized", _
                                            clsPayment_tran.enPaymentRefId.PAYSLIP, 0, _
                                            clsPayment_tran.enPayTypeId.PAYMENT, _
                                            True, strFilter)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            If dsList.Tables("Authorized").Rows.Count > 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry! Some of the payments are already Authorized. Please Void Authorized payment for selected employees."), Me)
                'Anjan [04 June 2014] -- End
                Exit Try
            End If

            If CBool(Session("SetPayslipPaymentApproval")) = True Then
                strFilter = " prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " AND prpayment_tran.isapproved = 1 AND prpayment_tran.paymenttranunkid IN (" & mstrVoidIds & ") "

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objPaymentTran.GetList("Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter, Session("AccessLevelFilterString"))

                'Nilay (10-Feb-2016) -- Start
                'dsList = objPaymentTran.GetList(Session("Database_Name"), _
                '                                Session("UserId"), _
                '                                Session("Fin_year"), _
                '                                Session("CompanyUnkId"), _
                '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                Session("UserAccessModeSetting"), True, _
                '                                Session("IsIncludeInactiveEmp"), "Authorized", _
                '                                clsPayment_tran.enPaymentRefId.PAYSLIP, 0, _
                '                                clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                Session("AccessLevelFilterString"), strFilter)

                dsList = objPaymentTran.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                mdtPeriodStartDate, _
                                                mdtPeriodEndDate, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "Authorized", _
                                                clsPayment_tran.enPaymentRefId.PAYSLIP, 0, _
                                                clsPayment_tran.enPayTypeId.PAYMENT, _
                                                True, strFilter)

                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If dsList.Tables("Authorized").Rows.Count > 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Some of the payments are Final Approved."), Me)
                    'Anjan [04 June 2014] -- End
                    Exit Try
                End If

                Dim objPaymentApproval As New clsPayment_approval_tran
                dsList = objPaymentApproval.GetList("Approved", True, mstrVoidIds, , , "levelunkid <> -1 AND priority <> -1 ")
                If dsList.Tables("Approved").Rows.Count > 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, Some of the Payments are Approved by some Approvers."), Me)
                    'Anjan [04 June 2014] -- End
                    Exit Sub
                End If
                objPaymentApproval = Nothing
            End If

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim objBudget As New clsBudget_MasterNew
            Dim intBudgetId As Integer = 0
            dtActAdj = New DataTable
            dsList = objBudget.GetComboList("List", False, True, )
            If dsList.Tables(0).Rows.Count > 0 Then
                intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If
            If intBudgetId > 0 Then
                Dim mstrAnalysis_Fields As String = ""
                Dim mstrAnalysis_Join As String = ""
                Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                Dim mstrReport_GroupName As String = ""
                Dim mstrAnalysis_TableName As String = ""
                Dim mstrAnalysis_CodeField As String = ""
                objBudget._Budgetunkid = intBudgetId
                Dim strEmpList As String = ""
                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                'Dim allEmp As List(Of String) = (From p In mdtPayment Where (CBool(p.Item("IsChecked")) = True OrElse CBool(p.Item("IsGrp")) = False) Select (p.Item("employeeunkid").ToString)).ToList
                Dim allEmp As List(Of String) = (From p In mdtPayment Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("employeeunkid").ToString)).ToList
                'Sohail (30 Oct 2018) -- End
                strEmpList = String.Join(",", allEmp.ToArray())
                Dim objBudgetCodes As New clsBudgetcodes_master
                Dim dsEmp As DataSet = Nothing
                If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objBudget_Tran As New clsBudget_TranNew
                    Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                    Controls_AnalysisBy.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                    dsEmp = objEmp.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                Else
                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, strEmpList, 1)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                    dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                    dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                    Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                    Dim mdicActivityCode As New Dictionary(Of Integer, String)
                    Dim objActivity As New clsfundactivity_Tran
                    Dim dsAct As DataSet = objActivity.GetList("Activity")
                    Dim objActAdjust As New clsFundActivityAdjustment_Tran
                    Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPeriodEndDate)
                    mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                    mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                    Dim objPayment As New clsPayment_tran
                    Dim dsBalance As DataSet = objPayment.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, " prpayment_tran.paymenttranunkid IN (" & mstrVoidIds & ") ", Nothing)
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                        dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid")}
                        dsBalance.Tables(0).Merge(dsEmp.Tables(0))
                    End If
                    Dim intActivityId As Integer
                    Dim decTotal As Decimal = 0
                    Dim decActBal As Decimal = 0
                    Dim blnShowValidationList As Boolean = False
                    Dim blnIsExeed As Boolean = False
                    For Each pair In mdicActivityCode
                        intActivityId = pair.Key
                        decTotal = 0
                        decActBal = 0
                        blnIsExeed = False

                        If mdicActivity.ContainsKey(intActivityId) = True Then
                            decActBal = mdicActivity.Item(intActivityId)
                        End If

                        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                        If lstRow.Count > 0 Then
                            For Each dRow As DataRow In lstRow
                                Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("Amount")) = False) Select (CDec(p.Item("Amount")))).Sum
                                decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                            Next

                            'If decTotal > decActBal Then
                            '    blnShowValidationList = True
                            '    blnIsExeed = True
                            'End If
                            Dim dr As DataRow = dtActAdj.NewRow
                            dr.Item("fundactivityunkid") = intActivityId
                            dr.Item("transactiondate") = eZeeDate.convertDate(mdtPeriodEndDate).ToString()
                            dr.Item("remark") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Salary payments Voided for the period of") & " " & cboPayPeriod.Text
                            dr.Item("isexeed") = blnIsExeed
                            dr.Item("Activity Code") = pair.Value
                            dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                            dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                            dtActAdj.Rows.Add(dr)
                        End If
                    Next

                    If blnShowValidationList = True Then
                        Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                        For Each r In dr
                            dtActAdj.Rows.Remove(r)
                        Next
                        dtActAdj.AcceptChanges()
                        dtActAdj.Columns.Remove("fundactivityunkid")
                        dtActAdj.Columns.Remove("transactiondate")
                        dtActAdj.Columns.Remove("remark")
                        dtActAdj.Columns.Remove("isexeed")

                        popupValidationList.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")
                        popupValidationList.Data_Table = dtActAdj
                        popupValidationList.Show()
                        Exit Try
                    End If
                End If
            End If
            'Sohail (23 May 2017) -- End

            popupDelPayment.Show()

            Exit Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub popupDelPayment_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelPayment.buttonDelReasonYes_Click
        Dim objPaymentTran As New clsPayment_tran
        Try
            Blank_ModuleName()
            objPaymentTran._WebFormName = "frmGlobalVoidPayment"
            StrModuleName2 = "mnuPayroll"
            objPaymentTran._WebHostName = CStr(Session("HOST_NAME"))
            objPaymentTran._WebIP = CStr(Session("IP_ADD"))
            objPaymentTran._AccessLevelFilterString = CStr(Session("AccessLevelFilterString"))

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            Dim strEmpIDs As String = String.Join(",", (From p In mdtPayment Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("employeeunkid").ToString)).ToArray)
            'Sohail (30 Oct 2018) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objPaymentTran.VoidAll(mstrVoidIds, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelPayment.Reason) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objPaymentTran.VoidAll(Session("Database_Name"), _
            '                          Session("Fin_year"), _
            '                          Session("CompanyUnkId"), _
            '                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                          Session("UserAccessModeSetting"), True, _
            '                          Session("IsIncludeInactiveEmp"), mstrVoidIds, _
            '                          CInt(Session("UserId")), _
            '                          ConfigParameter._Object._CurrentDateAndTime, _
            '                          popupDelPayment.Reason) = True Then
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'If objPaymentTran.VoidAll(CStr(Session("Database_Name")), _
            '                          CInt(Session("Fin_year")), _
            '                          CInt(Session("CompanyUnkId")), _
            '                          mdtPeriodStartDate, _
            '                          mdtPeriodEndDate, _
            '                          CStr(Session("UserAccessModeSetting")), True, _
            '                          CBool(Session("IsIncludeInactiveEmp")), mstrVoidIds, _
            '                          CInt(Session("UserId")), _
            '                          ConfigParameter._Object._CurrentDateAndTime, _
            '                          popupDelPayment.Reason, True) = True Then

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'If objPaymentTran.VoidAll(CStr(Session("Database_Name")), _
            '                          CInt(Session("Fin_year")), _
            '                          CInt(Session("CompanyUnkId")), _
            '                          mdtPeriodStartDate, _
            '                          mdtPeriodEndDate, _
            '                          CStr(Session("UserAccessModeSetting")), True, _
            '                          CBool(Session("IsIncludeInactiveEmp")), mstrVoidIds, _
            '                          CInt(Session("UserId")), _
            '                          ConfigParameter._Object._CurrentDateAndTime, _
            '                         popupDelPayment.Reason, True, , , dtActAdj) = True Then
            If objPaymentTran.VoidAll(CStr(Session("Database_Name")), _
                                      CInt(Session("Fin_year")), _
                                      CInt(Session("CompanyUnkId")), _
                                      mdtPeriodStartDate, _
                                      mdtPeriodEndDate, _
                                      CStr(Session("UserAccessModeSetting")), True, _
                                      CBool(Session("IsIncludeInactiveEmp")), mstrVoidIds, _
                                      CInt(Session("UserId")), _
                                      ConfigParameter._Object._CurrentDateAndTime, _
                                     popupDelPayment.Reason, CInt(clsPayment_tran.enPaymentRefId.PAYSLIP), CInt(clsPayment_tran.enPayTypeId.PAYMENT), True, , , dtActAdj, strEmpIDs, CStr(Session("fmtCurrency"))) = True Then
                'Sohail (30 Oct 2018) -- End
                'Sohail (23 May 2017) -- End
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End
                Call FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentTran = Nothing
        End Try
    End Sub

    'Sohail (03 Jul 2014) -- Start
    'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (03 Jul 2014) -- End

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    Protected Sub Closebotton1_CloseButton_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'SHANI [01 FEB 2015]--END
        Try
            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect(Session("servername").ToString & "~\UserHome.aspx")
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " Combobox's Events "
    'Sohail (03 Jul 2014) -- Start
    'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPaymentTran As New clsPayment_tran
        Dim dsCombo As DataSet
        Try
            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objPaymentTran.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPayPeriod.SelectedValue), Session("mdbname"), mstrAdvanceFilter)
            dsCombo = objPaymentTran.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, Session("Database_Name").ToString, CInt(cboPayPeriod.SelectedValue))
            'Nilay (10-Feb-2016) -- End
            With cboPmtVoucher
                .DataValueField = "voucherno"
                .DataTextField = "voucherno"
                .DataSource = dsCombo.Tables("Voucher")
                .DataBind()
            End With

            'Nilay (10-Feb-2016) -- Start
            Dim objPeriod As New clscommom_period_Tran

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPayPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            Else
                mdtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            End If
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub
    'Sohail (03 Jul 2014) -- End
#End Region

#Region " GridView's Events "

    Protected Sub GvVoidPayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvVoidPayment.RowDataBound

        Try
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            If e.Row.RowType = DataControlRowType.Header OrElse e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(6).Visible = CBool(Session("AllowToViewPaidAmount"))
            End If
            'Sohail (16 Oct 2019) -- End

            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                    e.Row.Visible = False

                Else
                    If CBool(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        For i = 3 To GvVoidPayment.Columns.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                        e.Row.Cells(2).Text = "Voucher No. : " & e.Row.Cells(2).Text
                        e.Row.Cells(2).ColumnSpan = GvVoidPayment.Columns.Count - 2
                        e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Row.Cells(2).CssClass = "GroupHeaderStyleBorderRight"
                    Else
                        'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#ddd'")
                        'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=''")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Sohail (03 Jul 2014) -- Start
    'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Sohail (03 Jul 2014) -- End



    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 

            Me.btnVoid.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVoid.ID, Me.btnVoid.Text).Replace("&", "")
            Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbFilterCriteria.ID, Me.gbFilterCriteria.Text)
            Me.lblTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTo.ID, Me.lblTo.Text)
            Me.lblPaidAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPaidAmount.ID, Me.lblPaidAmount.Text)
            Me.lblPaymentDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPaymentDate.ID, Me.lblPaymentDate.Text)
            Me.lblVoucherno.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVoucherno.ID, Me.lblVoucherno.Text)
            Me.lblPayPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lnkAdvanceFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)

            Me.GvVoidPayment.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvVoidPayment.Columns(2).FooterText, Me.GvVoidPayment.Columns(2).HeaderText)
            Me.GvVoidPayment.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvVoidPayment.Columns(3).FooterText, Me.GvVoidPayment.Columns(3).HeaderText)
            Me.GvVoidPayment.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvVoidPayment.Columns(4).FooterText, Me.GvVoidPayment.Columns(4).HeaderText)
            Me.GvVoidPayment.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvVoidPayment.Columns(5).FooterText, Me.GvVoidPayment.Columns(5).HeaderText)
            Me.GvVoidPayment.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvVoidPayment.Columns(6).FooterText, Me.GvVoidPayment.Columns(6).HeaderText)
            Me.GvVoidPayment.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvVoidPayment.Columns(7).FooterText, Me.GvVoidPayment.Columns(7).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
