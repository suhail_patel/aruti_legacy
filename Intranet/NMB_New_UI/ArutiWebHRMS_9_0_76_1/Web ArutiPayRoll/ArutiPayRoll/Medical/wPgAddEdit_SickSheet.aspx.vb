﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Medical_wPgAddEdit_SickSheet
    Inherits Basepage


#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim objSicksheet As clsmedical_sicksheet


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmSicksheet_AddEdit"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try



            'Pinkal (19-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'Dim objprovider As New clsinstitute_master
            'dsFill = objprovider.getListForCombo(True, "Provider", True, 1, True, CInt(Session("UserId")))

            Dim objUserMapping As New clsprovider_mapping
            dsFill = objUserMapping.GetUserProvider("Provider", , CInt(Session("UserId")), True)
            Dim drRow As DataRow = dsFill.Tables(0).NewRow
            drRow("providerunkid") = 0
            drRow("name") = "Select"
            dsFill.Tables(0).Rows.InsertAt(drRow, 0)
            drpProvider.DataTextField = "name"
            drpProvider.DataValueField = "providerunkid"
            drpProvider.DataSource = dsFill.Tables("Provider")
            drpProvider.DataBind()

            'Pinkal (19-Nov-2012) -- End

            'dsFill = Nothing
            'Dim objjob As New clsJobs
            'dsFill = objjob.getComboList("Job", True)
            'drpJob.DataTextField = "name"
            'drpJob.DataValueField = "jobunkid"
            'drpJob.DataSource = dsFill.Tables("Job")
            'drpJob.DataBind()

            dsFill = Nothing
            Dim objMaster As New clsmedical_master
            dsFill = objMaster.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
            drpMedicalCover.DataTextField = "name"
            drpMedicalCover.DataValueField = "mdmasterunkid"
            drpMedicalCover.DataSource = dsFill.Tables(0)
            drpMedicalCover.DataBind()

            dsFill = Nothing
            Dim objEmployee As New clsEmployee_Master


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsFill = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                False, "Employee", True)

            'Shani(24-Aug-2015) -- End

            'Pinkal (5-MAY-2012) -- End

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'drpEmployeeName.DataTextField = "employeename"
            drpEmployeeName.DataTextField = "EmpCodeName"
            'Nilay (09-Aug-2016) -- End
            drpEmployeeName.DataValueField = "employeeunkid"
            drpEmployeeName.DataSource = dsFill.Tables(0)
            drpEmployeeName.DataBind()

            Session.Add("EmployeeList", dsFill.Tables(0))


            'Shani [ 17 SEP 2014 ] -- START
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            If CInt(Session("SickSheetAllocationId")) > 0 Then

                Select Case CInt(Session("SickSheetAllocationId"))

                    Case enAllocation.BRANCH

                        Dim objBranch As New clsStation
                        dsFill = objBranch.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "stationunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objBranch = Nothing

                    Case enAllocation.DEPARTMENT_GROUP

                        Dim objDeptGroup As New clsDepartmentGroup
                        dsFill = objDeptGroup.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "deptgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objDeptGroup = Nothing

                    Case enAllocation.DEPARTMENT

                        Dim objDepartment As New clsDepartment
                        dsFill = objDepartment.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "departmentunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objDepartment = Nothing

                    Case enAllocation.SECTION_GROUP

                        Dim objSectionGrp As New clsSectionGroup
                        dsFill = objSectionGrp.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "sectiongroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objSectionGrp = Nothing

                    Case enAllocation.SECTION

                        Dim objSection As New clsSections
                        dsFill = objSection.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "sectionunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objSection = Nothing

                    Case enAllocation.UNIT_GROUP

                        Dim objUnitGrp As New clsUnitGroup
                        dsFill = objUnitGrp.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "unitgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objUnitGrp = Nothing

                    Case enAllocation.UNIT

                        Dim objUnit As New clsUnits
                        dsFill = objUnit.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "unitunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objUnit = Nothing

                    Case enAllocation.TEAM

                        Dim objTeam As New clsTeams
                        dsFill = objTeam.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "teamunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objTeam = Nothing

                    Case enAllocation.JOB_GROUP

                        Dim objJobGrp As New clsJobGroup
                        dsFill = objJobGrp.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "jobgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objJobGrp = Nothing

                    Case enAllocation.JOBS

                        Dim objJob As New clsJobs
                        dsFill = objJob.getComboList("List", True, , , , , , , "1=1")
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "jobunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objJob = Nothing

                    Case enAllocation.CLASS_GROUP

                        Dim objClassGrp As New clsClassGroup
                        dsFill = objClassGrp.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "classgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objClassGrp = Nothing

                    Case enAllocation.CLASSES

                        Dim objClass As New clsClass
                        dsFill = objClass.getComboList("List", True)
                        cboSickSheetAllocation.DataTextField = "name"
                        cboSickSheetAllocation.DataValueField = "classesunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        cboSickSheetAllocation.DataBind()
                        objClass = Nothing

                End Select

                cboSickSheetAllocation.SelectedValue = "0"

            End If
            'Shani [ 17 SEP 2014 ] -- END


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objSicksheet._Sicksheetno = txtSickSheetNo.Text.Trim

            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            If dtSheetdate.IsNull = True Then
                objSicksheet._Sicksheetdate = Nothing
            Else
                objSicksheet._Sicksheetdate = dtSheetdate.GetDate.Date
            End If
            'Pinkal (12-Nov-2012) -- End


            objSicksheet._Instituteunkid = CInt(drpProvider.SelectedValue)
            objSicksheet._Employeeunkid = CInt(drpEmployeeName.SelectedValue)
            'objSicksheet._Jobunkid = CInt(drpJob.SelectedValue)

            If Me.ViewState("Jobunkid") IsNot Nothing Then
                objSicksheet._Jobunkid = CInt(Me.ViewState("Jobunkid"))
            Else
                objSicksheet._Jobunkid = 0
            End If

            objSicksheet._Medicalcoverunkid = CInt(drpMedicalCover.SelectedValue)

            If Session("depedants") IsNot Nothing Then
                Dim dtTable As DataTable = CType(Session("depedants"), DataTable)
                Dim strDepedants As String = ""

                For i As Integer = 0 To gvdependants.Items.Count - 1
                    Dim chk As CheckBox = CType(gvdependants.Items(i).FindControl("chkDependant"), CheckBox)
                    If chk.Checked Then
                        strDepedants &= gvdependants.Items(i).Cells(6).Text & ","
                    End If
                Next

                If strDepedants.Trim.Length > 0 Then
                    objSicksheet._Dependantunkid = strDepedants.Substring(0, strDepedants.Length - 1)
                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.
                Else
                    objSicksheet._Dependantunkid = strDepedants
                    'Pinkal (06-Jan-2016) -- End
                End If

            End If

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                objSicksheet._Userunkid = CInt(Session("UserId"))
            End If
            'Sohail (23 Apr 2012) -- End

            'Shani [ 17 SEP 2014 ] -- START
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            objSicksheet._AllocationtypeId = CInt(Session("SickSheetAllocationId"))
            If objSicksheet._AllocationtypeId > 0 Then
                objSicksheet._Allocationtranunkid = CInt(cboSickSheetAllocation.SelectedValue)
            Else
                objSicksheet._Allocationtranunkid = 0
            End If
            'Shani [ 17 SEP 2014 ] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtSickSheetNo.Text = objSicksheet._Sicksheetno
            dtSheetdate.SetDate = objSicksheet._Sicksheetdate.Date

            Dim objemployee As New clsEmployee_Master
            objemployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objSicksheet._Employeeunkid
            txtEmployeeCode.Text = objemployee._Employeecode
            drpEmployeeName.SelectedValue = objemployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date).ToString()
            drpProvider.SelectedValue = objSicksheet._Instituteunkid.ToString()

            If Me.ViewState("Jobunkid") Is Nothing Then
                Dim objJob As New clsJobs
                objJob._Jobunkid = objSicksheet._Jobunkid
                txtJob.Text = objJob._Job_Name
                Me.ViewState.Add("Jobunkid", objJob._Jobunkid)
            End If
            drpMedicalCover.SelectedValue = objSicksheet._Medicalcoverunkid.ToString()

            'Shani [ 17 SEP 2014 ] -- START
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            If CInt(Session("SickSheetAllocationId")) > 0 Then
                cboSickSheetAllocation.SelectedValue = objSicksheet._Allocationtranunkid.ToString()
            End If
            'Shani [ 17 SEP 2014 ] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub BlankObjects()
        Try
            txtSickSheetNo.Text = ""
            dtSheetdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            txtEmployeeCode.Text = ""
            If drpEmployeeName.Items.Count > 0 Then drpEmployeeName.SelectedIndex = 0
            If drpProvider.Items.Count > 0 Then drpProvider.SelectedIndex = 0
            'If drpJob.Items.Count > 0 Then drpJob.SelectedIndex = 0
            txtJob.Text = ""
            If drpMedicalCover.Items.Count > 0 Then drpMedicalCover.SelectedIndex = 0
            gvdependants.DataSource = Nothing
            gvdependants.DataBind()
            If cboSickSheetAllocation.Items.Count > 0 Then cboSickSheetAllocation.SelectedIndex = 0
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BlankObjects :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillDependants()
        Try
            'Dim objEmployee As New clsEmployee_Master
            'Dim dsList As DataSet = objEmployee.GetEmployee_Dependents(CInt(drpEmployeeName.SelectedValue), False)

            Dim objDependant As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dsList As DataSet = objDependant.GetQualifiedDepedant(CInt(drpEmployeeName.SelectedValue), True, False, dtSheetdate.GetDate.Date)
            Dim dsList As DataSet = objDependant.GetQualifiedDepedant(CInt(drpEmployeeName.SelectedValue), True, False, dtSheetdate.GetDate.Date, dtAsOnDate:=dtSheetdate.GetDate.Date)
            'Sohail (18 May 2019) -- End

            If (Not dsList Is Nothing) Then
                gvdependants.DataSource = dsList
                gvdependants.DataKeyField = "dependent_Id"
                gvdependants.DataBind()
            Else
                DisplayMessage.DisplayMessage("GetData :-  ", Me)
                Exit Sub
            End If

            If Session("Depedants") Is Nothing Then
                Session.Add("Depedants", dsList.Tables(0))
            Else
                Session("Depedants") = dsList.Tables(0)
            End If


            'Shani [ 17 SEP 2014 ] -- START
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            If Session("SickSheetId") IsNot Nothing Then
                Dim mstrDependantIds As String = ""
                mstrDependantIds = objSicksheet._Dependantunkid
                If mstrDependantIds.Trim.Length > 0 Then
                    Dim arDependent() As String = mstrDependantIds.Trim.Split(CChar(","))
                    For i As Integer = 0 To arDependent.Length - 1
                        For j As Integer = 0 To gvdependants.Items.Count - 1
                            Dim chk As CheckBox = CType(gvdependants.Items(j).FindControl("chkDependant"), CheckBox)
                            If CInt(arDependent(i)) = CInt(gvdependants.Items(j).Cells(6).Text) Then
                                chk.Checked = True
                            End If
                        Next
                    Next
                End If
                If gvdependants.Items.Count > 0 Then
                    Dim chkedcount As Integer = 0
                    For i As Integer = 0 To gvdependants.Items.Count - 1
                        Dim chk As CheckBox = CType(gvdependants.Items(i).FindControl("chkDependant"), CheckBox)
                        If chk.Checked Then
                            chkedcount += 1
                        End If
                    Next
                    If chkedcount = gvdependants.Items.Count Then
                        Dim GvDependanceTable As Table = CType(gvdependants.Controls(0), Table)
                        Dim chk As CheckBox = CType(GvDependanceTable.Rows(1).FindControl("chkAllDependant"), CheckBox)
                        If chk IsNot Nothing Then
                            chk.Checked = True
                        End If
                    End If
                End If
            End If
            'Shani [ 17 SEP 2014 ] -- END


        Catch ex As Exception

            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                gvdependants.CurrentPageIndex = 0
                gvdependants.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If

        End Try
    End Sub

    'Shani [ 17 SEP 2014 ] -- START
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private Sub GetDependanceageRecordNo()
        Try
            If gvdependants.CurrentPageIndex > 0 Then
                Me.ViewState("FirstRecordNo") = (((gvdependants.CurrentPageIndex + 1) * gvdependants.Items.Count) - gvdependants.Items.Count)
            Else
                If gvdependants.Items.Count < gvdependants.PageSize Then
                    Me.ViewState("FirstRecordNo") = ((1 * gvdependants.Items.Count) - gvdependants.Items.Count)
                Else
                    Me.ViewState("FirstRecordNo") = ((1 * gvdependants.PageSize) - gvdependants.Items.Count)
                End If

            End If
            Me.ViewState("LastRecordNo") = ((gvdependants.CurrentPageIndex + 1) * gvdependants.Items.Count)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetDependanceageRecordNo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Is_Vaid() As Boolean
        Try
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(Session("SickSheetNotype")) = 0 And txtSickSheetNo.Text.Trim.Length = 0 Then
                'If ConfigParameter._Object._SickSheetNotype = 0 And txtSickSheetNo.Text.Trim.Length = 0 Then
                'Sohail (23 Apr 2012) -- End

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Sick Sheet No cannot be blank.Sick Sheet No is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sick Sheet No cannot be blank.Sick Sheet No is required information."), Me)
                'Pinkal (06-May-2014) -- End
                txtSickSheetNo.Focus()
                Return False

            ElseIf CInt(drpProvider.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Provider is compulsory information.Please Select Provider.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Provider is compulsory information.Please Select Provider."), Me)
                'Pinkal (06-May-2014) -- End


                drpProvider.Focus()
                Return False
            ElseIf txtEmployeeCode.Text.Trim.Length = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Employee Code is compulsory information.Please Select Employee Code.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Employee Code is compulsory information.Please Select Employee Code."), Me)
                'Pinkal (06-May-2014) -- End
                txtEmployeeCode.Focus()
                Return False

            ElseIf CInt(drpEmployeeName.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                '  DisplayMessage.DisplayMessage("Employee Name is compulsory information.Please Select Employee Name.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee Name is compulsory information.Please Select Employee Name."), Me)
                'Pinkal (06-May-2014) -- End


                drpEmployeeName.Focus()
                Return False

                'ElseIf CInt(drpJob.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage("Job / Position is compulsory information.Please Select Job / Position.", Me)
                '    drpJob.Focus()
                '    Exit Sub

            ElseIf CInt(drpMedicalCover.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Medical Cover is compulsory information.Please Select Medical Cover.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Medical Cover is compulsory information.Please Select Medical Cover."), Me)
                'Pinkal (06-May-2014) -- End
                drpMedicalCover.Focus()
                Return False

                'Shani [ 17 SEP 2014 ] -- START
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            ElseIf cboSickSheetAllocation.Visible AndAlso CInt(cboSickSheetAllocation.SelectedValue) = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(LblSickSheetAllocation.Text.Trim & Space(1) & Language.getMessage(mstrModuleName, 9, " is compulsory information.Please select ") & Space(1) & LblSickSheetAllocation.Text & ".", Me)
                cboSickSheetAllocation.Focus()
                Return False
            ElseIf gvdependants.Items.Count > 0 Then
                Dim chkItemCount As Integer = 0
                For i As Integer = 0 To gvdependants.Items.Count - 1
                    Dim chk As CheckBox = CType(gvdependants.Items(i).FindControl("chkDependant"), CheckBox)
                    If chk IsNot Nothing Then
                        If chk.Checked Then
                            chkItemCount += 1
                        End If
                    End If
                Next
                If chkItemCount < gvdependants.Items.Count Then
                    popYesNo.Title = Me.Title
                    popYesNo.Message = Language.getMessage(mstrModuleName, 8, "Some of the depedants are not ticked.They will not been included in this sick sheet.Are you sure you want to save this sick sheet ?")
                    popYesNo.Show()
                    Return False
                End If
                'Shani [ 17 SEP 2014 ] -- END
            End If
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Is_Vaid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
    'Shani [ 17 SEP 2014 ] -- END

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmSicksheet_AddEdit"
            StrModuleName2 = "mnuMedical"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            objSicksheet = New clsmedical_sicksheet

            If CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                objSicksheet._Userunkid = CInt(Session("UserId"))
            End If


            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'Nilay (01-Feb-2015) -- End
            If Not IsPostBack Then
                FillCombo()
                If Session("SickSheetId") IsNot Nothing Then
                    objSicksheet._Sicksheetunkid = CInt(Session("SickSheetId"))
                    GetValue()

                    'Pinkal (12-Sep-2014) -- Start
                    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                    'FillDependants
                    'Pinkal (12-Sep-2014) -- End

                    txtSickSheetNo.ReadOnly = True
                    txtEmployeeCode.ReadOnly = True
                    drpEmployeeName.Enabled = False
                Else
                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    If Session("SickSheet_Filter") IsNot Nothing Then
                        drpEmployeeName.SelectedValue = CStr(IIf(CStr(Session("SickSheet_Filter")).Split(CChar("-"))(0).Trim = "", 0, CStr(Session("SickSheet_Filter")).Split(CChar("-"))(0).Trim))
                    End If
                    'SHANI [09 Mar 2015]--END
                    dtSheetdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If

                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                FillDependants()
                'Pinkal (12-Sep-2014) -- End
            End If


            'Shani [ 17 SEP 2014 ] -- START
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If CInt(Session("SickSheetAllocationId")) <= 0 Then
                LblSickSheetAllocation.Visible = False
                cboSickSheetAllocation.Visible = False
            ElseIf CInt(Session("SickSheetAllocationId")) > 0 Then
                Dim objMst As New clsMasterData
                Dim dsList As DataSet = objMst.GetEAllocation_Notification("List", Session("SickSheetAllocationId").ToString(), True)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    LblSickSheetAllocation.Text = dsList.Tables(0).Rows(0)("Name").ToString()
                End If
            End If

            Me.ViewState.Add("FirstRecordNo", 0)
            Me.ViewState.Add("LastRecordNo", 0)
            'Shani [ 17 SEP 2014 ] -- END

            If CInt(Session("SickSheetNotype")) = 1 Then
                txtSickSheetNo.Enabled = False
            Else
                txtSickSheetNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleImageSaprator1(False)
    '    ToolbarEntry1.VisibleImageSaprator2(False)
    '    ToolbarEntry1.VisibleImageSaprator3(False)
    '    ToolbarEntry1.VisibleImageSaprator4(False)
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Vaid() = False Then Exit Sub
            Call popYesNo_buttonYes_Click(New Button, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        'Shani [ 24 DEC 2014 ] -- END
        Try
            BlankObjects()
            Session("SickSheetId") = Nothing
            Session("EmployeeList") = Nothing
            Response.Redirect(Session("rootpath").ToString() & "Medical/wPg_SickSheetList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Shani [ 17 SEP 2014 ] -- START
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Protected Sub popYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popYesNo.buttonYes_Click
        Try
            If Session("SickSheetId") Is Nothing Then
                SetValue()
                objSicksheet.Insert(CInt(Session("CompanyUnkId")))

                If objSicksheet._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & objSicksheet._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                    BlankObjects()
                End If

            ElseIf Session("SickSheetId") IsNot Nothing AndAlso CInt(Session("SickSheetId")) > 0 Then
                objSicksheet._Sicksheetunkid = CInt(Session("SickSheetId"))
                SetValue()
                objSicksheet.Update()
                If objSicksheet._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage("Entry Not Updated. Reason : " & objSicksheet._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Updated Successfully !!!!!", Me)
                    Response.Redirect("~/Medical/wPg_SickSheetList.aspx", False)
                    BlankObjects()
                End If
                objSicksheet._Sicksheetunkid = -1
                Session("SickSheetId") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani [ 17 SEP 2014 ] -- END

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        BlankObjects()
    '        Session("SickSheetId") = Nothing
    '        Session("EmployeeList") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End



#End Region

#Region "Dropdown Event(s)"

    Protected Sub drpEmployeeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployeeName.SelectedIndexChanged
        Try
            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If CInt(drpEmployeeName.SelectedValue) > 0 Then

                Dim objEmployee As New clsEmployee_Master
                Dim ds As DataSet

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    ds = objEmployee.GetList("List", False, , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), CInt(drpEmployeeName.SelectedValue), Session("AccessLevelFilterString"))
                'Else
                '    ds = objEmployee.GetList("List", False, , , , CInt(drpEmployeeName.SelectedValue), Session("AccessLevelFilterString"))
                'End If
                ds = objEmployee.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            False, "EmployeeList", CBool(Session("ShowFirstAppointmentDate")), _
                                            CInt(drpEmployeeName.SelectedValue), , , , Not CBool(Session("RevokeUserAccessOnSicksheet")))

                'Shani(24-Aug-2015) -- End
                If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    RemoveHandler txtEmployeeCode.TextChanged, AddressOf txtEmployeeCode_TextChanged
                    txtEmployeeCode.Text = ds.Tables(0).Rows(0)("employeecode").ToString()
                    AddHandler txtEmployeeCode.TextChanged, AddressOf txtEmployeeCode_TextChanged

                    Dim objJob As New clsJobs
                    objJob._Jobunkid = CInt(ds.Tables(0).Rows(0)("jobunkid").ToString())
                    txtJob.Text = objJob._Job_Name
                    Me.ViewState.Add("Jobunkid", objJob._Jobunkid)
                End If
            Else
                txtEmployeeCode.Text = ""
                txtJob.Text = ""
            End If
            FillDependants()

            'Pinkal (12-Sep-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpEmployeeName_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Event(s)"

    Protected Sub txtEmployeeCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmployeeCode.TextChanged
        Try
            If txtEmployeeCode.Text.Trim.Length > 0 Then
                If CType(Session("EmployeeList"), DataTable) IsNot Nothing Then
                    Dim dtTable As DataTable = CType(Session("EmployeeList"), DataTable)
                    Dim drRow() As DataRow = dtTable.Select("employeecode = '" & txtEmployeeCode.Text.Trim & "'")

                    If drRow.Length > 0 Then
                        drpEmployeeName.SelectedValue = drRow(0)("employeeunkid").ToString()
                        'Pinkal (12-Sep-2014) -- Start
                        'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                        drpEmployeeName_SelectedIndexChanged(New Object(), New EventArgs())
                        'Pinkal (12-Sep-2014) -- End
                        FillDependants()
                    Else
                        DisplayMessage.DisplayMessage("Invalid Employee Code.Please Enter Valid Employee Code.", Me)
                    End If

                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtEmployeeCode_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ToolBar Event(s)"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Medical/wPg_SickSheetList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "GridView Event"


    'Shani [ 17 SEP 2014 ] -- START
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Protected Sub gvdependants_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvdependants.ItemDataBound
        Try
            If Session("SickSheetId") Is Nothing Then
                If e.Item.ItemType = ListItemType.Header Then
                    Dim chkHeader As CheckBox = CType(e.Item.FindControl("chkAllDependant"), CheckBox)
                    If chkHeader IsNot Nothing Then
                        chkHeader.Checked = True
                    End If
                ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim chkItem As CheckBox = CType(e.Item.FindControl("chkDependant"), CheckBox)
                    If chkItem IsNot Nothing Then
                        chkItem.Checked = True
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage(" :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani [ 17 SEP 2014 ] -- END

    Protected Sub gvdependants_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvdependants.PageIndexChanged
        Try
            gvdependants.CurrentPageIndex = e.NewPageIndex
            FillDependants()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("gvdependants_PageIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox"

    'Shani [ 17 SEP 2014 ] -- START
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Protected Sub chkAllDependance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvdependants.Items.Count <= 0 Then Exit Sub
            GetDependanceageRecordNo()
            If ViewState("FirstRecordNo") IsNot Nothing AndAlso ViewState("LastRecordNo") IsNot Nothing Then
                Dim MintFirstRecordNo As Integer = CInt(Me.ViewState("FirstRecordNo"))
                Dim MintLastRecordNO As Integer = CInt(Me.ViewState("LastRecordNo"))
                Dim mintRowIndex As Integer = 0
                For i As Integer = MintFirstRecordNo To MintLastRecordNO - 1
                    CType(gvdependants.Items(mintRowIndex).FindControl("chkDependant"), CheckBox).Checked = cb.Checked
                    mintRowIndex += 1
                Next
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani [ 17 SEP 2014 ] -- END

#End Region

    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    Protected Sub dtSheetdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtSheetdate.TextChanged
        Try
            If CInt(drpEmployeeName.SelectedValue) > 0 Then
                FillDependants()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtSheetdate_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (21-Jul-2014) -- End

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblSheetNo.Text = Language._Object.getCaption(Me.lblSheetNo.ID, Me.lblSheetNo.Text)
        Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.ID, Me.lblProvider.Text)
        Me.lblEmployeecode.Text = Language._Object.getCaption(Me.lblEmployeecode.ID, Me.lblEmployeecode.Text)
        Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
        Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
        Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.ID, Me.lblProvider.Text)
        Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
        Me.lblCover.Text = Language._Object.getCaption(Me.lblCover.ID, Me.lblCover.Text)
        Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)

        gvdependants.Columns(0).HeaderText = Language._Object.getCaption(gvdependants.Columns(0).FooterText, gvdependants.Columns(0).HeaderText).Replace("&", "")
        gvdependants.Columns(1).HeaderText = Language._Object.getCaption(gvdependants.Columns(1).FooterText, gvdependants.Columns(1).HeaderText).Replace("&", "")
        gvdependants.Columns(2).HeaderText = Language._Object.getCaption(gvdependants.Columns(2).FooterText, gvdependants.Columns(2).HeaderText).Replace("&", "")
        gvdependants.Columns(3).HeaderText = Language._Object.getCaption(gvdependants.Columns(3).FooterText, gvdependants.Columns(3).HeaderText).Replace("&", "")
        gvdependants.Columns(4).HeaderText = Language._Object.getCaption(gvdependants.Columns(4).FooterText, gvdependants.Columns(4).HeaderText).Replace("&", "")
        gvdependants.Columns(4).HeaderText = Language._Object.getCaption(gvdependants.Columns(5).FooterText, gvdependants.Columns(5).HeaderText).Replace("&", "")

        Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub



End Class
