﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_Medicalclaim.aspx.vb"
    Inherits="Medical_wPg_Medicalclaim" MasterPageFile="~/Home1.master" Title="Medical Claim" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function onlyNumbers(txtBox, e) {        //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Medical Claim"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblInstutution" runat="server" Text="Service Provider" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpProvider" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Pay Period" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpPeriod" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblSickSheetNo" runat="server" Text="Sick Sheet No" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSickSheetNo" runat="server" AutoPostBack="true" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDependants" runat="server" Text="Dependants" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpdepedants" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-35">
                                                        <asp:CheckBox ID="chkEmpexempted" runat="server" Text="Exempt From Deduction" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblClaimdate" runat="server" Text="Claim Date" CssClass="form-label" />
                                                        <uc2:DateCtrl ID="dtClaimdate" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblClaimno" runat="server" Text="Claim No" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtClaimno" runat="server" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblClaimAmount" runat="server" Text="Claim Amount" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtclaimAmount" AutoPostBack="true" runat="server" Style="text-align: right;"
                                                                    Text="0" onKeypress="return onlyNumbers(this, event);" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblInvoiceDate" runat="server" Text="Invoice Date" CssClass="form-label" />
                                                        <uc2:DateCtrl ID="dtpInvoiceDate" runat="server" AutoPostBack="true" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblInvoiceno" runat="server" Text="Invoice No" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTotInvoiceAmt" runat="server" Text="Total Invoice Amount" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTotInvoiceAmt" AutoPostBack="true" runat="server" Style="text-align: right;"
                                                                    Text="0" onKeypress="return onlyNumbers(this, event); " CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnImportData" runat="server" CssClass="btn btn-primary" Text="Import" />
                                                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add" />
                                                <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary" Text="Edit" />
                                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-primary" Text="Delete" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 250px">
                                                            <asp:GridView ID="GvMedicalClaim" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" SelectImageUrl="~/images/edit.png"
                                                                        ShowSelectButton="True" />
                                                                    <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee" />
                                                                    <asp:BoundField DataField="dependentsname" HeaderText="Depedant" ReadOnly="True"
                                                                        FooterText="colhDependants" />
                                                                    <asp:BoundField DataField="sicksheetno" HeaderText="Sick Sheet No" ReadOnly="True"
                                                                        FooterText="colhSickSheetNo" />
                                                                    <asp:BoundField DataField="claimdate" HeaderText="Claim Date" ReadOnly="True" FooterText="colhClaimDate" />
                                                                    <asp:BoundField DataField="claimno" HeaderText="Claim No" ReadOnly="True" FooterText="colhClaimNo" />
                                                                    <asp:BoundField DataField="amount" HeaderText="Claim Amount" HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-HorizontalAlign="Right" ReadOnly="True" FooterText="colhAmount" />
                                                                    <asp:BoundField DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                    <asp:BoundField DataField="isempexempted" HeaderText="Isempexempted" ReadOnly="True"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-9 col-md-9  col-sm-12 col-xs-12">
                                                    </div>
                                                    <div class="col-lg-3 col-md-3  col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTotAmt" runat="server" Style="margin-right: 5px" Text="Total Amount"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTotalAmount" runat="server" ReadOnly="true" Style="text-align: right;"
                                                                    Text="0" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnFinalSave" runat="server" CssClass="btn btn-primary" Text="Final Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblImportEmployeeCode" runat="server" PopupControlID="pnlpopup"
                     CancelControlID="btnImportCancel" />
                <asp:Panel ID="pnlpopup" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblText" runat="server" Text="To Import Medical Claim :"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 475px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblFile" runat="server" Text="Choose File " CssClass="form-label" />
                                            <asp:FileUpload ID="FlUpload" runat="server" />
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Button ID="BtnGet" Text="Ok" CssClass="btn btn-primary" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="BtnGet" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportEmployeeCode" runat="server" Text="Employee Code " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportEmployeeCode" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportEmployee" runat="server" Text="Employee " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportEmployee" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportDepedant" runat="server" Text="Depedant " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportDependant" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportSicksheet" runat="server" Text="Sick Sheet " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportSickSheet" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportClaimNo" runat="server" Text="Claim No " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportClaimNo" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportClaimDate" runat="server" Text="Claim Date " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportClaimDate" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImportClaimAmount" runat="server" Text="Claim Amount " CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportClaimAmount" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblEmpExempted" runat="server" Text="Exempt From Deduction" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drpImportExemptFromDeduction" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnImportOk" Text="Import" CssClass="btn btn-primary" runat="server" />
                        <asp:Button ID="btnImportCancel" Text="Cancel" CssClass="btn btn-default" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
