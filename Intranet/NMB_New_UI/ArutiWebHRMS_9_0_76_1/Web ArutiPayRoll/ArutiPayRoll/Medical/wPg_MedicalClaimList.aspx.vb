﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Medical_wPg_MedicalClaimList
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Private objmedicalclaim As clsmedical_claim_master


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmMedical_Claim_List"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            dsFill = Nothing
            Dim objInstitute As New clsinstitute_master
            dsFill = objInstitute.getListForCombo(True, "Institute", True, 1, False, CInt(Session("UserId")))
            drpProvider.DataTextField = "name"
            drpProvider.DataValueField = "instituteunkid"
            drpProvider.DataSource = dsFill.Tables("Institute")
            drpProvider.DataBind()

            'FOR YEAR
            dsFill = Nothing
            Dim objPeriod As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, , , , Session("Database_Name"))
            dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "Period", True)
            'Shani(20-Nov-2015) -- End

            drpPeriod.DataSource = dsFill.Tables("Period")
            drpPeriod.DataTextField = "name"
            drpPeriod.DataValueField = "periodunkid"
            drpPeriod.DataBind()

            'FOR Status
            dsFill = Nothing
            Dim objStatus As New clsMasterData
            dsFill = objStatus.getMedicalStatusList("Status", True)
            drpStatus.DataSource = dsFill.Tables("Status")
            drpStatus.DataTextField = "Name"
            drpStatus.DataValueField = "statusunkid"
            drpStatus.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User AndAlso CBool(Session("AllowToViewMedicalClaimList")) = False Then Exit Sub


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.


            'If txtTotInvoiceFromAmt.Text.Trim = "0" Then
            '    txtTotInvoiceFromAmt.Text = "0"

            'ElseIf txtTotInvoiceToAmt.Text.Trim = "0" Then
            '    txtTotInvoiceToAmt.Text = "0"
            'End If

            'If txtInvoiceNo.Text.Trim.Length > 0 Then
            '    StrSearching &= "AND invoiceno like '%" & txtInvoiceNo.Text.Trim & "%'" & " "
            'End If

            'If CInt(drpPeriod.SelectedValue) > 0 Then
            '    StrSearching &= "AND periodunkid =" & CInt(drpPeriod.SelectedValue) & " "
            'End If

            'If CInt(drpProvider.SelectedValue) > 0 Then
            '    StrSearching &= "AND instituteunkid=" & CInt(drpProvider.SelectedValue) & " "
            'End If

            'If CDec(txtTotInvoiceFromAmt.Text.Trim) <> 0 Then
            '    StrSearching &= "AND invoice_amt >=" & CDec(txtTotInvoiceFromAmt.Text.Trim) & " "
            'End If

            'If CDec(txtTotInvoiceToAmt.Text.Trim) <> 0 Then
            '    StrSearching &= "AND invoice_amt <=" & CDec(txtTotInvoiceToAmt.Text.Trim) & " "
            'End If

            'If CInt(drpStatus.SelectedValue) > 0 Then
            '    StrSearching &= "AND statusunkid =" & CInt(drpStatus.SelectedValue) & " "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = dsList.Tables(0)
            'End If

            If txtTotInvoiceFromAmt.Text.Trim = "0" Then
                txtTotInvoiceFromAmt.Text = "0"

            ElseIf txtTotInvoiceToAmt.Text.Trim = "0" Then
                txtTotInvoiceToAmt.Text = "0"
            End If

            If txtInvoiceNo.Text.Trim.Length > 0 Then
                StrSearching &= "AND mdmedical_claim_master.invoiceno like '%" & txtInvoiceNo.Text.Trim & "%'" & " "
            End If

            If CInt(drpPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND mdmedical_claim_master.periodunkid =" & CInt(drpPeriod.SelectedValue) & " "
            End If

            If CInt(drpProvider.SelectedValue) > 0 Then
                StrSearching &= "AND mdmedical_claim_master.instituteunkid=" & CInt(drpProvider.SelectedValue) & " "
            End If

            If CDec(txtTotInvoiceFromAmt.Text.Trim) <> 0 Then
                StrSearching &= "AND mdmedical_claim_master.invoice_amt >=" & CDec(txtTotInvoiceFromAmt.Text.Trim) & " "
            End If

            If CDec(txtTotInvoiceToAmt.Text.Trim) <> 0 Then
                StrSearching &= "AND mdmedical_claim_master.invoice_amt <=" & CDec(txtTotInvoiceToAmt.Text.Trim) & " "
            End If

            If CInt(drpStatus.SelectedValue) > 0 Then
                StrSearching &= "AND mdmedical_claim_master.statusunkid =" & CInt(drpStatus.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsList = objmedicalclaim.GetList("List", True, CInt(Session("UserId")), chkMyInvoiceOnly.Checked, StrSearching)

            dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

            'Pinkal (06-Jan-2016) -- End

            If (Not dtTable Is Nothing) Then
                gvMedicalClaim.DataSource = dtTable
                gvMedicalClaim.DataKeyField = "claimunkid"
                gvMedicalClaim.DataBind()
            Else
                DisplayMessage.DisplayMessage("GetData :-  ", Me)
                Exit Sub
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objmedicalclaim = New clsmedical_claim_master
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
            'Nilay (01-Feb-2015) -- End

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmMedical_Claim_List"
            StrModuleName2 = "mnuMedical"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            If Not IsPostBack Then
                BtnNew.Visible = CBool(Session("AddMedicalClaim"))
                FillCombo()
            End If

            SetLanguage()


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleImageSaprator1(False)
    '    ToolbarEntry1.VisibleImageSaprator2(False)
    '    ToolbarEntry1.VisibleImageSaprator3(False)
    '    ToolbarEntry1.VisibleImageSaprator4(False)
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            Session("Claimunkid") = Nothing
            Session("ClaimList") = Nothing
            Session("ImportList") = Nothing
            Response.Redirect("~/Medical/wPg_Medicalclaim.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            If drpProvider.Items.Count > 0 Then drpProvider.SelectedIndex = 0
            txtInvoiceNo.Text = ""
            txtTotInvoiceFromAmt.Text = Format(0, Session("fmtCurrency").ToString())
            txtTotInvoiceToAmt.Text = Format(0, Session("fmtCurrency").ToString())
            drpPeriod.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            chkMyInvoiceOnly.Checked = True
            gvMedicalClaim.DataSource = Nothing
            gvMedicalClaim.DataBind()
            gvMedicalClaim.CurrentPageIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Nilay (01-Feb-2015) -- End
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'Nilay (01-Feb-2015) -- txtreasondel.Text = ""
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If

            Dim objmedical As New clsmedical_claim_master

            If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                objmedical._Userunkid = CInt(Session("UserId"))
            End If


            With objmedical
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text

                If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                End If

                .Delete(CInt(Me.ViewState("Claimunkid")))

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("Claimunkid") = Nothing
                End If
                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'popup1.Dispose()
                'txtreasondel.Text = ""
                popup_DeleteReason.Dispose()
                popup_DeleteReason.Reason = ""
                'Nilay (01-Feb-2015) -- End


                FillList()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End

#End Region

#Region " GridView's Event(s) "

    Protected Sub gvMedicalClaim_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvMedicalClaim.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)
                gvMedicalClaim.Columns(0).Visible = CBool(Session("EditMedicalClaim"))
                gvMedicalClaim.Columns(1).Visible = CBool(Session("DeleteMedicalClaim"))
                gvMedicalClaim.Columns(2).Visible = CBool(Session("AllowToExportMedicalClaim"))
                gvMedicalClaim.Columns(3).Visible = CBool(Session("AllowToCancelExportedMedicalClaim"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("gvMedicalClaim_ItemDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub gvMedicalClaim_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvMedicalClaim.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            If e.CommandName = "Select" Then

                If CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = True Then

                    If CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 1 Then  'Pending
                        DisplayMessage.DisplayMessage("You cannot Edit this Medical claim.Reason : This Bill has been already finalized.", Me)
                        Exit Sub

                    ElseIf CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 2 Then  'Exported
                        DisplayMessage.DisplayMessage("You cannot Edit this Medical claim.Reason : This Bill has been already exported.", Me)
                        Exit Sub

                    ElseIf CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 3 Then  'Payroll Affected
                        DisplayMessage.DisplayMessage("You cannot Edit this Medical claim.Reason : This Bill has been already payroll Affected.", Me)
                        Exit Sub

                    End If
                Else
                    Session.Add("Claimunkid", gvMedicalClaim.DataKeys(e.Item.ItemIndex))
                    Response.Redirect("~/Medical/wPg_Medicalclaim.aspx", False)
                End If

            ElseIf e.CommandName = "Export" Then

                If CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = True AndAlso CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 1 Then
                    objmedicalclaim._Claimunkid = CInt(gvMedicalClaim.DataKeys(e.Item.ItemIndex))
                    objmedicalclaim._Statusunkid = 2 ' FOR EXPORT STATUS


                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.

                    Dim dsTranData As DataSet = (New clsmedical_claim_Tran).GetClaimTran(Session("Database_Name").ToString(), _
                                                                               CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                               Session("UserAccessModeSetting").ToString(), True, True, _
                                                                               objmedicalclaim._Claimunkid)


                    'If objmedicalclaim.Update(objmedicalclaim._DsTranData.Tables(0)) = False Then
                    If objmedicalclaim.Update(dsTranData.Tables(0)) = False Then
                        'Pinkal (06-Jan-2016) -- End
                        DisplayMessage.DisplayMessage("Medical Claim cannot be Exported Due To " & objmedicalclaim._Message, Me)
                    Else
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Medical Claim bill Exported Successfully."), Me)
                        FillList()
                    End If

                ElseIf CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = False AndAlso CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 1 Then
                    DisplayMessage.DisplayMessage("This bill has not been finalized yet,so you can not  export this bill.", Me)
                    Exit Sub
                End If

            ElseIf e.CommandName = "Cancel Export" Then

                If CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = True AndAlso CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 2 Then

                    objmedicalclaim._Claimunkid = CInt(gvMedicalClaim.DataKeys(e.Item.ItemIndex))
                    objmedicalclaim._IsFinal = False
                    objmedicalclaim._Statusunkid = 1 ' FOR PENDING STATUS


                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.
                    'If objmedicalclaim.Update(objmedicalclaim._DsTranData.Tables(0)) = False Then

                    Dim dsTranData As DataSet = (New clsmedical_claim_Tran).GetClaimTran(Session("Database_Name").ToString(), _
                                                                          CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                           CInt(Session("CompanyUnkId")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                          Session("UserAccessModeSetting").ToString(), True, True, _
                                                                          objmedicalclaim._Claimunkid)

                    If objmedicalclaim.Update(dsTranData.Tables(0)) = False Then
                        'Pinkal (06-Jan-2016) -- End
                        DisplayMessage.DisplayMessage("Medical Claim cannot be Exported Due To " & objmedicalclaim._Message, Me)
                    Else
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Medical Claim bill Cancel Exported Successfully."), Me)
                        FillList()
                    End If

                ElseIf CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = True AndAlso CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 1 Then
                    DisplayMessage.DisplayMessage("This bill has not been exported yet, so you can not perform cancel export operation.", Me)
                    Exit Sub

                ElseIf CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = False AndAlso CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 1 Then
                    DisplayMessage.DisplayMessage("This bill has not been finalized yet,so you can not cancel export this bill.", Me)
                    Exit Sub

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvMedicalClaim_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvMedicalClaim.DeleteCommand
        Try

            If CBool(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(10).Text) = True Then

                If CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 1 Then  'Pending
                    DisplayMessage.DisplayMessage("You cannot Delete this Medical claim.Reason : This Bill has been finalized already.", Me)
                    Exit Sub
                ElseIf CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 2 Then  'Exported
                    DisplayMessage.DisplayMessage("You cannot Delete this Medical claim.Reason : This Bill has been already exported.", Me)
                    Exit Sub
                ElseIf CInt(gvMedicalClaim.Items(e.Item.ItemIndex).Cells(9).Text) = 3 Then  'Payroll Affected
                    DisplayMessage.DisplayMessage("You cannot Delete this Medical claim.Reason : This Bill has been already payroll Affected.", Me)
                    Exit Sub
                End If
            Else
                Me.ViewState.Add("Claimunkid", CInt(gvMedicalClaim.DataKeys(e.Item.ItemIndex)))
                popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popup1.Show()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvMedicalClaim_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvMedicalClaim.PageIndexChanged
        Try
            gvMedicalClaim.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ToolBar Event(s)"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Medical/wPg_MedicalClaim.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End

            Me.lblInvoiceNo.Text = Language._Object.getCaption(Me.lblInvoiceNo.ID, Me.lblInvoiceNo.Text)
            Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.ID, Me.lblProvider.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblFromTotInvoiceAmt.Text = Language._Object.getCaption(Me.lblFromTotInvoiceAmt.ID, Me.lblFromTotInvoiceAmt.Text)
            Me.lblToTotInvoiceAmt.Text = Language._Object.getCaption(Me.lblToTotInvoiceAmt.ID, Me.lblToTotInvoiceAmt.Text)
            Me.chkMyInvoiceOnly.Text = Language._Object.getCaption(Me.chkMyInvoiceOnly.ID, Me.chkMyInvoiceOnly.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.gvMedicalClaim.Columns(0).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(0).FooterText, Me.gvMedicalClaim.Columns(0).HeaderText).Replace("&", "")
            'Me.gvMedicalClaim.Columns(1).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(1).FooterText, Me.gvMedicalClaim.Columns(1).HeaderText).Replace("&", "")
            'Me.gvMedicalClaim.Columns(2).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(2).FooterText, Me.gvMedicalClaim.Columns(2).HeaderText).Replace("&", "")
            'Me.gvMedicalClaim.Columns(3).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(3).FooterText, Me.gvMedicalClaim.Columns(3).HeaderText).Replace("&", "")
            'Gajanan [17-Sep-2020] -- End
            
            Me.gvMedicalClaim.Columns(4).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(4).FooterText, Me.gvMedicalClaim.Columns(4).HeaderText)
            Me.gvMedicalClaim.Columns(5).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(5).FooterText, Me.gvMedicalClaim.Columns(5).HeaderText)
            Me.gvMedicalClaim.Columns(6).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(6).FooterText, Me.gvMedicalClaim.Columns(6).HeaderText)
            Me.gvMedicalClaim.Columns(7).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(7).FooterText, Me.gvMedicalClaim.Columns(7).HeaderText)
            Me.gvMedicalClaim.Columns(8).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(8).FooterText, Me.gvMedicalClaim.Columns(8).HeaderText)
            Me.gvMedicalClaim.Columns(9).HeaderText = Language._Object.getCaption(Me.gvMedicalClaim.Columns(9).FooterText, Me.gvMedicalClaim.Columns(9).HeaderText)

            Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.BtnDelete.Text = Language._Object.getCaption(Me.BtnDelete.ID, Me.BtnDelete.Text).Replace("&", "")
            'Nilay (01-Feb-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try



    End Sub


End Class
