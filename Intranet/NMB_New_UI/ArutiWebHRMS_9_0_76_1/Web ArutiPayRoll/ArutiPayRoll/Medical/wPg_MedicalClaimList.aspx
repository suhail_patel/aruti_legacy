﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_MedicalClaimList.aspx.vb"
    Inherits="Medical_wPg_MedicalClaimList" MasterPageFile="~/Home1.master" Title="Medical Claim List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function onlyNumbers(evt, isInvoiceFromamt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (isInvoiceFromamt == true) {
                var totInvoiceFromAmtval = document.getElementById('<%=txtTotInvoiceFromAmt.ClientID%>').value;
                if (totInvoiceFromAmtval.length > 0) {
                    if (charCode == 46)
                        if (totInvoiceFromAmtval.indexOf(".") > 0)
                        return false;
                }
            }
            else if (isInvoiceFromamt == false) {
                var totInvoiceToAmt = document.getElementById('<%=txtTotInvoiceToAmt.ClientID%>').value;

                if (totInvoiceToAmt.length > 0) {
                    if (charCode == 46)
                        if (totInvoiceToAmt.indexOf(".") > 0)
                        return false;
                }
            }

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }    
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Medical Claim List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblProvider" runat="server" Text="Service Provider" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpProvider" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpPeriod" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFromTotInvoiceAmt" runat="server" Text="Total Invoice From Amount"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtTotInvoiceFromAmt" runat="server" Style="text-align: right" Text="0"
                                                    onKeypress="return onlyNumbers(true);" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToTotInvoiceAmt" runat="server" Text="Total Invoice To Amount"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtTotInvoiceToAmt" runat="server" Style="text-align: right;" Text="0"
                                                    onKeypress="return onlyNumbers(false);" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkMyInvoiceOnly" runat="server" Text="My Invoice Only" Checked="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" CssClass="btn btn-primary" runat="server" Text="New" />
                                <asp:Button ID="BtnSearch" CssClass="btn btn-default" runat="server" Text="Search" />
                                <asp:Button ID="BtnReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="gvMedicalClaim" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Edit" CommandName="Select"
                                                                    CssClass="gridedit">
                                                                                   <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                    CssClass="griddelete">
                                                                                    <i class="fas fa-trash text-danger"></i>    
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn  HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnExportStatus">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc" style="padding: 2px">
                                                                <asp:ImageButton ID="ImgExport" runat="server" ImageUrl="~/images/common-export.png"
                                                                    ToolTip="Export" CommandName="Export" />
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn  HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnCancelExport">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc" style="padding: 2px">
                                                                <asp:ImageButton ID="ImgCancelExport" runat="server" ImageUrl="~/images/CancelExport.png"
                                                                    ToolTip="Cancel Export" CommandName="Cancel Export" />
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="invoiceno" HeaderText="Invoice No" ReadOnly="True" FooterText="colhInvoiceNo" />
                                                    <asp:BoundColumn DataField="period_name" HeaderText="Pay Period" ReadOnly="True"
                                                        FooterText="colhPeriod" />
                                                    <asp:BoundColumn DataField="institute_name" HeaderText="Provider" ReadOnly="True"
                                                        FooterText="colhInstitute" />
                                                    <asp:BoundColumn DataField="invoice_amt" HeaderText="Invoice Amount" ReadOnly="True"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="colhInvoiceAmt" />
                                                    <asp:BoundColumn DataField="status_name" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="True"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="isfinal" HeaderText="isfinal" ReadOnly="True" Visible="false" />
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="panel-primary">
                        <div class="panel-heading">
                          
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                             
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 30%">
                                                
                                            </td>
                                            <td style="width: 20%">
                                               
                                            </td>
                                            <td style="width: 30%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 30%">
                                                
                                            </td>
                                            <td style="width: 20%">
                                                
                                            </td>
                                            <td style="width: 30%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                               
                                            </td>
                                            <td style="width: 30%">
                                               
                                            </td>
                                            <td style="width: 20%">
                                                
                                            </td>
                                            <td style="width: 30%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="4">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top:5px">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_gvMedicalClaim" ScrollBars="Auto" style="margin-top:5px; margin-bottom:10px" Height="400px" runat="server">
                                               
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>--%>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To delete?:" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
