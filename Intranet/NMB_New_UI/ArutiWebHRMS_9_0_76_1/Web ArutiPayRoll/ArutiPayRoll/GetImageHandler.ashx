﻿<%@ WebHandler Language="VB" Class="GetImageHandler" %>
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Web

'S.SANDEEP |24-AUG-2020| -- START
'ISSUE/ENHANCEMENT : Implementing Try Catch
'Public Class GetImageHandler : Implements IHttpHandler, System.Web.SessionState.IRequiresSessionState

'    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
'        Dim mintTranID As Int32 = 0
'        Dim intModeID As Integer = -1

'        Dim arr() As String = context.Request.QueryString.ToString.Trim.Split("&")

'        If arr.Length > 0 Then
'            If Not arr(0) Is Nothing Then
'                Dim arId() As String = arr(0).ToString().Trim.Split("=")
'                If arId.Length > 0 Then
'                    mintTranID = Convert.ToInt32(arId(1))
'                End If
'            Else
'                Throw New ArgumentException("No parameter specified")
'            End If

'            If Not arr(1) Is Nothing Then
'                Dim arModeID() As String = arr(1).ToString().Trim.Split("=")
'                If arModeID.Length > 0 Then
'                    intModeID = Convert.ToInt32(arModeID(1))
'                End If
'            Else
'                Throw New ArgumentException("No parameter specified")
'            End If
'        End If

'        context.Response.ContentType = "image/jpeg"
'        Dim strm As System.IO.Stream = ShowImage(context, mintTranID, intModeID)

'        If strm IsNot Nothing Then
'            Dim buffer As Byte() = New Byte(strm.Length) {}
'            Dim byteSeq As Integer = strm.Read(buffer, 0, strm.Length)

'            Do While byteSeq > 0
'                context.Response.OutputStream.Write(buffer, 0, byteSeq)
'                byteSeq = strm.Read(buffer, 0, strm.Length)
'            Loop
'        End If

'    End Sub

'    Public Function ShowImage(ByVal context As HttpContext, ByVal intTranID As Integer, ByVal intModeID As Integer) As System.IO.Stream

'        If intModeID = 1 Then    'Employee Master

'            Dim objEmployee As New clsEmployee_Master
'            objEmployee._Companyunkid = context.Session("CompanyUnkId")
'            objEmployee._blnImgInDb = context.Session("IsImgInDataBase")

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objEmployee._Employeeunkid = intTranID
'            objEmployee._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate"))) = intTranID
'            'Shani(20-Nov-2015) -- End

'            Try
'                If objEmployee._Photo.Length > 0 Then
'                    Return New System.IO.MemoryStream(CType(objEmployee._Photo, Byte()))
'                Else
'                    Return Nothing
'                End If
'            Catch
'                Return Nothing
'            End Try

'        ElseIf intModeID = 2 Then    'Dependant Master

'            Dim objdependant As New clsDependants_Beneficiary_tran
'            objdependant._CompanyId = context.Session("CompanyUnkId")
'            objdependant._blnImgInDb = context.Session("IsImgInDataBase")
'            objdependant._Dpndtbeneficetranunkid = intTranID
'            Try
'                If objdependant._Photo.Length > 0 Then
'                    Return New System.IO.MemoryStream(CType(objdependant._Photo, Byte()))
'                Else
'                    Return Nothing
'                End If
'            Catch
'                Return Nothing
'            End Try

'            'S.SANDEEP [16-Jan-2018] -- START
'            'ISSUE/ENHANCEMENT : REF-ID # 118
'        ElseIf intModeID = 3 Then

'            Dim objEmployee As New clsEmployee_Master
'            objEmployee._Companyunkid = context.Session("CompanyUnkId")
'            objEmployee._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate"))) = intTranID
'            Try
'                If objEmployee._EmpSignature.Length > 0 Then
'                    Return New System.IO.MemoryStream(CType(objEmployee._EmpSignature, Byte()))
'                Else
'                    Return Nothing
'                End If
'            Catch
'                Return Nothing
'            End Try
'            'S.SANDEEP [16-Jan-2018] -- END

'        End If
'        Return Nothing
'    End Function

'    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
'        Get
'            Return False
'        End Get
'    End Property

'End Class

Public Class GetImageHandler : Implements IHttpHandler, System.Web.SessionState.IRequiresSessionState
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mintTranID As Int32 = 0
        Dim intModeID As Integer = -1
        Try
        Dim arr() As String = context.Request.QueryString.ToString.Trim.Split("&")
        
        If arr.Length > 0 Then
            If Not arr(0) Is Nothing Then
                Dim arId() As String = arr(0).ToString().Trim.Split("=")
                If arId.Length > 0 Then
                    mintTranID = Convert.ToInt32(arId(1))
                End If
            Else
                Throw New ArgumentException("No parameter specified")
            End If
        
            If Not arr(1) Is Nothing Then
                Dim arModeID() As String = arr(1).ToString().Trim.Split("=")
                If arModeID.Length > 0 Then
                    intModeID = Convert.ToInt32(arModeID(1))
                End If
        Else
            Throw New ArgumentException("No parameter specified")
        End If
        End If
        
        context.Response.ContentType = "image/jpeg"
        Dim strm As System.IO.Stream = ShowImage(context, mintTranID, intModeID)
        
        If strm IsNot Nothing Then
            Dim buffer As Byte() = New Byte(strm.Length) {}
            Dim byteSeq As Integer = strm.Read(buffer, 0, strm.Length)
 
            Do While byteSeq > 0
                context.Response.OutputStream.Write(buffer, 0, byteSeq)
                byteSeq = strm.Read(buffer, 0, strm.Length)
            Loop
        End If
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub
    
    Public Function ShowImage(ByVal context As HttpContext, ByVal intTranID As Integer, ByVal intModeID As Integer) As System.IO.Stream
        Try
        If intModeID = 1 Then    'Employee Master
       
        Dim objEmployee As New clsEmployee_Master
        objEmployee._Companyunkid = context.Session("CompanyUnkId")
        objEmployee._blnImgInDb = context.Session("IsImgInDataBase")
            
            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = intTranID
                'objEmployee._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")).ToString()) = intTranID
                objEmployee._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString())) = intTranID
            'Shani(20-Nov-2015) -- End

        Try
            If objEmployee._Photo.Length > 0 Then
                Return New System.IO.MemoryStream(CType(objEmployee._Photo, Byte()))
            Else
                Return Nothing
            End If
        Catch
            Return Nothing
        End Try
            
        ElseIf intModeID = 2 Then    'Dependant Master
            
            Dim objdependant As New clsDependants_Beneficiary_tran
            objdependant._CompanyId = context.Session("CompanyUnkId")
            objdependant._blnImgInDb = context.Session("IsImgInDataBase")
            objdependant._Dpndtbeneficetranunkid = intTranID
            Try
                If objdependant._Photo.Length > 0 Then
                    Return New System.IO.MemoryStream(CType(objdependant._Photo, Byte()))
                Else
                    Return Nothing
                End If
            Catch
                Return Nothing
            End Try
           
        ElseIf intModeID = 3 Then
            
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Companyunkid = context.Session("CompanyUnkId")
                'objEmployee._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")).ToString()) = intTranID
                objEmployee._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString())) = intTranID
            Try
                If objEmployee._EmpSignature.Length > 0 Then
                    Return New System.IO.MemoryStream(CType(objEmployee._EmpSignature, Byte()))
                Else
                    Return Nothing
                End If
            Catch
                Return Nothing
            End Try
                   
        End If
        Return Nothing
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
            Return Nothing
        End Try
    End Function
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
'S.SANDEEP |24-AUG-2020| -- END

