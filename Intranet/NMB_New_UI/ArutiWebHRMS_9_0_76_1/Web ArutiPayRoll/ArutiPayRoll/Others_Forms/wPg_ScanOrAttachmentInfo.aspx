﻿<%@ Page Title="Scan / Attachment Info" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"   CodeFile="wPg_ScanOrAttachmentInfo.aspx.vb" Inherits="Others_Forms_wPg_ScanOrAttachmentInfo"  %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 
    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            //s$(".ajax-upload-dragdrop").css("width","auto");
            $('#<%= lblError.ClientID %>').css("position","static");
        }
        
    </script>
    
   <script type="text/javascript">
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>_drpCombo');
            var cboEmp = $('#<%= cboEmployee.ClientID %>_drpCombo');
            var cboDocument = $('#<%= cboDocument.ClientID %>_drpCombo');
           
            var lblError = $('#<%= lblError.ClientID %>');
            
             if (parseInt(cboEmp.value) <= 0) {
                lblError.html("Employee is compulsory information. Please select Employee to continue.");
                cboEmp.focus();
                return false;
            }
            if (parseInt(cbodoctype.value) <= 0) {
                lblError.html("Document Type is mandatory information. Please select Document Type to continue.");
                cbodoctype.focus();
                return false;
            }
            if (parseInt(cboDocument.value) <= 0) {
                lblError.html("Document is mandatory information. Please select Document to continue.");
                cboDocument.focus();
                return false;
            }
            
            if (document.referrer.indexOf("wPg_LeaveFormAddEdit.aspx") <= 0){
            var cboAssocateVal = $('#<%= cboAssocatedValue.ClientID %>_drpCombo');
            if (typeof cboAssocateVal.value != "undefined") {
                if (parseInt(cboAssocateVal.value) <= 0 && parseInt(cbodoctype.value) != 3) {
                    lblError.html("is mandatory information. Please select Assocate Level.");
                    cboAssocateVal.focus();
                    return false;
                }
            }
            }
            return true;
        }    
        
    </script>

        <asp:Panel ID="MainPan" runat="server" >
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                  <div class="block-header">
                        <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Scan / Attachment Info"></asp:Label>
                        </h2>
                   </div>
                   <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                       <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </h2>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="objlblCaption" runat="server" Text="#Value"  CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True" />
                                                    </div>
                                             </div> 
                                             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblDocType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                             <asp:DropDownList ID="cboDocumentType" runat="server" AutoPostBack="True" />
                                                    </div>
                                             </div> 
                                             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                     <asp:Label ID="lblUploadDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                      <uc2:DateCtrl ID="dtpUploadDate" runat="server" AutoPostBack="false" />
                                             </div>   
                                    </div>
                                    <div class="row clearfix">
                                           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                   <asp:Label ID="lblDocument" runat="server" Text="Document" CssClass="form-label"></asp:Label> 
                                                   <div class="form-group">
                                                            <asp:DropDownList ID="cboDocument" runat="server" />
                                                   </div> 
                                           </div>
                                           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblSelection" runat="server" Text="#Value" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                             <asp:DropDownList ID="cboAssocatedValue" runat="server" />
                                                    </div>
                                           </div>     
                                    </div>
                                     <div class="row clearfix">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblError" runat="server" Text="" CssClass="ErrorControl form-label" Style="text-align: right; position: relative; float: left;"></asp:Label>
                                             </div>
                                     </div>
                                    
                                </div>
                                <div class="footer d--f">
                                        <asp:Button ID="btnScan" runat="server" Text="Scan" CssClass="btn btn-primary" ValidationGroup="ScanAttach"  Visible="false" />    
                                            <div id="fileuploader">
                                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Add" onclick="return IsValidAttach()"  validationgroup="ScanAttach" />
                                             </div>
                                        <asp:Button ID="btnAdd" runat="server" Text="" Style="display: none"   OnClick="btnAdd_Click"  CssClass="btn btn-primary" ValidationGroup="ScanAttach" />
                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary"  ValidationGroup="ScanAttach" />
                                </div>
                            </div>
                         </div>
                    </div>      
                    
                   <div class="row clearfix">
                         <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 350px">   
                                                
                                                <asp:GridView ID="gvScanAttachment" runat="server" AutoGenerateColumns="False" 
                                                            AllowPaging="false" DataKeyNames="GUID,scanattachtranunkid,orgfilepath"  CssClass="table table-hover table-bordered">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width = "50px" HeaderStyle-Width = "50px">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="btnView1" runat="server"  CausesValidation="false" ToolTip= "Delete" 
                                                                                CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                </asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width = "50px" HeaderStyle-Width = "50px">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="btnDownload" runat="server" CausesValidation="false" ToolTip = "Download" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                CommandName="Download">
                                                                                <i class="fa fa-download"></i>
                                                                            </asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" FooterText="colhCode">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="names" HeaderText="Employee/Applicant" ReadOnly="True"
                                                                    FooterText="colhName">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="document" HeaderText="Document" ReadOnly="True" FooterText="colhDocument">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="filename" HeaderText="File Name" ReadOnly="True" FooterText="colhFileName">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="valuename" HeaderText="Associated Value" ReadOnly="True"
                                                                    FooterText="colhValue">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="employeeunkid" HeaderText="empid" ReadOnly="True" Visible="false"
                                                                    FooterText="colhemployeeunkid">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="applicantid" HeaderText="applicantid" ReadOnly="True"
                                                                    Visible="false" FooterText="colhapplicantid">
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>          
                                                </div>
                                           </div>
                                     </div>
                                </div>
                                <div class="footer">
                                         <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" ValidationGroup="Save" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" ValidationGroup="Save" />
                                </div>
                            </div>
                        </div>
                     </div>                           
                   
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="gvScanAttachment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>

 <script type="text/javascript">
			$(document).ready(function()
			{
				ImageLoad();
				//$(".ajax-upload-dragdrop").css("width","auto");
				$('#<%= lblError.ClientID %>').css("position","static");
			});
			function ImageLoad(){
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPg_ScanOrAttachmentInfo.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAdd.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			$("body").on("click", "input[type=file]", function() {
			    return IsValidAttach();
			});
    </script>

</asp:Content>
