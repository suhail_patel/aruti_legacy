﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing


Partial Class Others_Forms_wpg_DashboardSettings
    Inherits Basepage

#Region " Private Variable "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmDashboardSettings"
#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then
                Dim objDashBoardData As New clsUser_dashboard
                Dim dsList As DataSet = objDashBoardData.GetList("List", CInt(Session("CompanyUnkId")), CInt(IIf(CInt(Session("UserId")) > 0, CInt(Session("UserId")), 0)), CInt(IIf(CInt(Session("Employeeunkid")) > 0, CInt(Session("Employeeunkid")), 0)))
                dlDashboardItems.DataSource = dsList.Tables(0)
                dlDashboardItems.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Method "

#End Region

#Region "Button Events"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim dRow As List(Of DataListItem) = Nothing
            dRow = dlDashboardItems.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).ToList()

            If dRow Is Nothing OrElse dRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select atleast one dashboard item."), Me)
                Exit Sub
            End If

            Dim dctDashboardItems As New Dictionary(Of Integer, Boolean)
            For i As Integer = 0 To dlDashboardItems.Items.Count - 1
                Dim chkBox As CheckBox = CType(dlDashboardItems.Items(i).FindControl("chkSelect"), CheckBox)
                If chkBox IsNot Nothing AndAlso dctDashboardItems.ContainsKey(CInt(dlDashboardItems.DataKeys(i))) = False Then
                    dctDashboardItems.Add(CInt(dlDashboardItems.DataKeys(i)), chkBox.Checked)
                End If
            Next


            Dim objUserDashboard As New clsUser_dashboard

            objUserDashboard._Companyunkid = CInt(Session("CompanyUnkId"))
            objUserDashboard._Userunkid = CInt(Session("UserId"))
            objUserDashboard._Employeeunkid = CInt(Session("Employeeunkid"))
            objUserDashboard._FormName = mstrModuleName
            objUserDashboard._ClientIP = Session("IP_ADD").ToString()
            objUserDashboard._HostName = Session("HOST_NAME").ToString()
            objUserDashboard._IsWeb = True

            If objUserDashboard.Insert(dctDashboardItems) = False Then
                DisplayMessage.DisplayMessage(objUserDashboard._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Dashboard settings saved successfully."), Me, Session("rootpath").ToString() & "UserHome.aspx")
            End If

            objUserDashboard = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
   
    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.
   
#Region "DataList Events"

    Protected Sub dlDashboardItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlDashboardItems.ItemDataBound
        Try
            If e.Item.ItemIndex < 0 Then Exit Sub

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            If dlDashboardItems Is Nothing OrElse dlDashboardItems.DataKeys.Count <= 0 Then Exit Sub
            'Pinkal (25-Jan-2022) -- End

            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                If CInt(dlDashboardItems.DataKeys(e.Item.ItemIndex)) = enDashboardItems.Crd_StaffTurnOver OrElse CInt(dlDashboardItems.DataKeys(e.Item.ItemIndex)) = enDashboardItems.Crd_LeaveAnalysis _
                   OrElse CInt(dlDashboardItems.DataKeys(e.Item.ItemIndex)) = enDashboardItems.Crd_NewlyHired Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'CType(dlDashboardItems.Controls(e.Item.ItemIndex).FindControl("dvpanel"), Control).Visible = False
                    Dim ctrl As Control = CType(dlDashboardItems.Controls(e.Item.ItemIndex).FindControl("dvpanel"), Control)
                    If ctrl IsNot Nothing Then
                        ctrl.Visible = False
                    End If
                    'Pinkal (25-Jan-2022) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (09-Aug-2021) -- End
    
    
End Class
