﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Home1.master" CodeFile="wpg_DashboardSettings.aspx.vb"
    Inherits="Others_Forms_wpg_DashboardSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix">
                <%--  <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">--%>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Dashboard Items Settings"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:DataList ID="dlDashboardItems" runat="server" RepeatDirection="Horizontal" ShowHeader="true"
                                        DataKeyField="ItemTypeId" ShowFooter="true" RepeatLayout="Table" RepeatColumns="3">
                                        <ItemTemplate>
                                            <div class="body">
                                                <div class="info-box-2 col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="width: 307px">
                                                    <div class="text col-lg-9 col-md-9 col-sm-12 col-xs-12 m-t-30 ">
                                                        <asp:Label ID="LblItemName" runat="server" Text='<%#Eval("name")%>' />
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-30 ">
                                                        <div class="switch">
                                                            <label>
                                                                <asp:CheckBox ID="chkSelect" runat="server" Checked='<%#Eval("ischeck")%>' />
                                                                <span class="lever"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
