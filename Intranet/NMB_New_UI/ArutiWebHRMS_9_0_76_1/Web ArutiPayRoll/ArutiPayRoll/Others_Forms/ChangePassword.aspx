﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb"
    Inherits="ChangePassword" Title="Change Password" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Employee Without Gender Report"></asp:Label>
                                    <asp:Label ID="lblResetTitle" runat="server" Text="Reset Password" Visible="false"></asp:Label>
                                </h2>
                        </div>
                            <div class="body">
                                <asp:Panel ID="oldPwd" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblOldPwd" runat="server" Text="Old Password"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                    <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" CssClass="form-control"
                                                        Placeholder="Old Password" ValidationGroup="password"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtOldPassword" ErrorMessage="Old Password is compulsory information. Please enter Old Password to continue."
                                            CssClass="error" ValidationGroup="password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblNewPwd" runat="server" Text="New Password"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="form-control"
                                                    ValidationGroup="password"  Placeholder="New Password"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtNewPassword" ErrorMessage="New Password is compulsory information. Please enter New Password to continue."
                                            CssClass="error" ValidationGroup="password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblConfirmPwd" runat="server" Text="Confirm Password"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="form-control"
                                                    ValidationGroup="password"  Placeholder="Confirm Password"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtConfirmPassword" ErrorMessage="Confirmation Password is compulsory information. Please enter Confirmation Password to continue."
                                            CssClass="error" ValidationGroup="password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                                <asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtConfirmPassword"
                                                    ControlToCompare="txtNewPassword" Type="String" Operator="Equal" SetFocusOnError="true"
                                                    ErrorMessage="Password does not match. Please enter correct password." runat="Server"
                                                    Display="None" ForeColor="White" CssClass="ErrorControl" ValidationGroup="password" />
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" ValidationGroup="password"
                                            CausesValidation="true" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                            </div>
                                    </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
