﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" MaintainScrollPositionOnPostback="true"
    CodeFile="UserHome.aspx.vb" Inherits="UserHome" Title="Aruti Human Resource And Payroll Managment System" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnlData" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card welcome-card">
                            <div class="body">
                                <ul class="creative-dots">
                                    <li class="bg-primary big-dot"></li>
                                    <li class="bg-secondary semi-big-dot"></li>
                                    <li class="bg-warning medium-dot"></li>
                                    <li class="bg-info semi-medium-dot"></li>
                                    <li class="bg-secondary semi-small-dot"></li>
                                    <li class="bg-primary small-dot"></li>
                                </ul>
                                <h2 class="welcome-title">
                                    <asp:Label ID="lblWelcomeTitle" runat="server" Text="Welcome, "></asp:Label>
                                    <asp:Label ID="lblWelcomeEmployee" CssClass="welcome-user" runat="server" Text="abc"></asp:Label>
                                </h2>
                                <p class="summary-title">
                                    <asp:Label ID="lblSummaryTitle" runat="server" Text="Your summary for"></asp:Label>
                                    <asp:Label ID="lblSummaryDate" runat="server" Text="Dashboard"></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <asp:LinkButton ID="lnkQApplyLeave" runat="server" CssClass="col-lg-4 col-md-4 col-sm-6 col-xs-12 quick-links">
                        <div class="info-box-4 hover-expand-effect">
                            <div class="content">
                                <div class="text">Apply Leave</div>
                            </div>
                             <div class="icon">
                                <i class="fas fa-calendar-day col-purple"></i>
                            </div>
                        </div>
                    </asp:LinkButton>
                    <asp:LinkButton ID="lnkQApplyOT" runat="server" CssClass="col-lg-4 col-md-4 col-sm-6 col-xs-12 quick-links">
                        <div class="info-box-4 hover-expand-effect">
                            <div class="content">
                                <div class="text">Apply Overtime</div>
                            </div>
                             <div class="icon">
                                <i class="fas fa-clock col-pink"></i>
                            </div>
                        </div>
                    </asp:LinkButton>
                    <asp:LinkButton ID="lnkQMyPayslip" runat="server" CssClass="col-lg-4 col-md-4 col-sm-6 col-xs-12 quick-links">
                        <div class="info-box-4 hover-expand-effect">
                            <div class="content">
                                <div class="text">View Payslip</div>
                            </div>
                             <div class="icon">
                                <i class="fas fa-receipt col-green"></i>
                            </div>
                        </div>
                    </asp:LinkButton>
                </div>
                <div class="row clearfix">
                    <div id="dvBirthDayDetalls" class="col-lg-4 col-md-4 col-sm-6 col-xs-12" runat="server">
                        <div class="card dashboard">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblBirthdayTitle" runat="server" Text="Today's Birthday"></asp:Label></div>
                                    <div class="number count-to">
                                        <asp:Label ID="lblBirthdayCount" runat="server" Text="Today's Birthday"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="af aruti-icon-gift col-pink"></i>
                                </div>
                            </div>
                            <div class="body">
                                <asp:DataList ID="dlBirthday" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                                    ShowFooter="true">
                                    <ItemTemplate>
                                        <div class="comment-box">
                                            <div class="media-left">
                                                <asp:Image ID="imgBdayEmployee" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                </asp:Image>
                                            </div>
                                            <div class="media-body">
                                                <div class="title">
                                                    <asp:Label ID="lblStageTitle" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                                <div class="sub-title">
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                                </div>
                                                <div class="sub-title">
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnShowAllBirthday" runat="server" CssClass="btn btn-link col-pink"
                                    Text="Show More" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlPendingTask" runat="server" CssClass="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card dashboard no-footer">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="Label5" runat="server" Text="Pending Task"></asp:Label>
                                    </div>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-list-alt col-pink"></i>
                                </div>
                            </div>
                            <div class="body">
                                <div class="list-group">
                                    <asp:LinkButton ID="lnkSetScoreCard" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblSetScoreCard" runat="server" Text="SetScoreCard"></asp:Label>
                                        <asp:Label ID="objlblSetScoreCardCount" runat="server" Text="0" CssClass="badge bg-color-1"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveScoreCard" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblApproveScoreCard" runat="server" Text="Approve ScoreCard"></asp:Label>
                                        <asp:Label ID="objlblApproveScoreCardCount" runat="server" Text="0" CssClass="badge bg-color-2"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkSubmitScoreCard" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblSubmitScoreCard" runat="server" Text="Submit Score Card"></asp:Label>
                                        <asp:Label ID="objlblSubmitScoreCardCount" runat="server" Text="0" CssClass="badge bg-color-3"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveUpdateProgress" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveUpdateProgress" runat="server" Text="Approve Update Progress"></asp:Label>
                                        <asp:Label ID="objlblApproveUpdateProgressCount" runat="server" Text="0" CssClass="badge bg-color-4"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkMyAssessment" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblMyAssessment" runat="server" Text="My Assessment"></asp:Label>
                                        <asp:Label ID="objlblMyAssessmentCount" runat="server" Text="0" CssClass="badge bg-color-5"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkAssessEmployee" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblAssessEmployee" runat="server" Text="Assess Employee"></asp:Label>
                                        <asp:Label ID="objlblAssessEmployeeCount" runat="server" Text="0" CssClass="badge bg-color-6"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkMyCompetenceAssessment" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblMyCompetenceAssessment" runat="server" Text="My Competence Assessment"></asp:Label>
                                        <asp:Label ID="objlblMyCompetenceAssessmentCount" runat="server" Text="0" CssClass="badge bg-color-8"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkAssessEmployeeCompetence" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblAssessEmployeeCompetence" runat="server" Text="Assess Employee Competence"></asp:Label>
                                        <asp:Label ID="objlblAssessEmployeeCompetenceCount" runat="server" Text="0" CssClass="badge bg-color-1"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkReviewEmployeeAssessment" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblReviewEmployeeAssessment" runat="server" Text="Review Employee Assessment"></asp:Label>
                                        <asp:Label ID="objlblReviewEmployeeAssessmentCount" runat="server" Text="0" CssClass="badge bg-color-1"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkReviewEmployeeCompetence" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblReviewEmployeeCompetence" runat="server" Text="Review Employee Competence"></asp:Label>
                                        <asp:Label ID="objlblReviewEmployeeCompetenceCount" runat="server" Text="0" CssClass="badge bg-color-2"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveLeave" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblApproveLeave" runat="server" Text="Approve Leave"></asp:Label>
                                        <asp:Label ID="objlblApproveLeaveCount" runat="server" Text="0" CssClass="badge bg-color-3"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveOTApplication" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveOTApplication" runat="server" Text="Approve OT Application"></asp:Label>
                                        <asp:Label ID="objlblApproveOTApplicationCount" runat="server" Text="0" CssClass="badge bg-color-4"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveClaimExpense" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveClaimExpense" runat="server" Text="Approve Claim Expense"></asp:Label>
                                        <asp:Label ID="objlblApproveClaimExpenseCount" runat="server" Text="0" CssClass="badge bg-color-1"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkAssetDeclaration" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblAssetDeclaration" runat="server" Text="Asset Declaration"></asp:Label>
                                        <asp:Label ID="objlblAssetDeclarationCount" runat="server" Text="0" CssClass="badge bg-color-5"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkNonDisclosureDeclaration" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblNonDisclosureDeclaration" runat="server" Text="Non Disclosure Declaration"></asp:Label>
                                        <asp:Label ID="objlblNonDisclosureDeclarationCount" runat="server" Text="0" CssClass="badge bg-color-6"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveSalaryChangeMSS" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveSalaryChangeMSS" runat="server" Text="Approve Salary Change"></asp:Label>
                                        <asp:Label ID="objlblApproveSalaryChangeMSSCount" runat="server" Text="0" CssClass="badge bg-color-7"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApprovePayslipPaymentMSS" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApprovePayslipPaymentMSS" runat="server" Text="Approve Payslip Payment"></asp:Label>
                                        <asp:Label ID="objlblApprovePayslipPaymentMSSCount" runat="server" Text="0" CssClass="badge bg-color-8"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveLoanApplicationMSS" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveLoanApplicationMSS" runat="server" Text="Approve Loan Application"></asp:Label>
                                        <asp:Label ID="objlblApproveLoanApplicationMSSCount" runat="server" Text="0" CssClass="badge bg-color-1"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveCalibrationMSS" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveCalibrationMSS" runat="server" Text="Approve Calibration"></asp:Label>
                                        <asp:Label ID="objlblApproveCalibrationMSSCount" runat="server" Text="0" CssClass="badge bg-color-2"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkApproveStaffRequisitionMSS" runat="server" class="list-group-item"
                                        Visible="false">
                                        <asp:Label ID="lblApproveStaffRequisitionMSS" runat="server" Text="Approve Staff Requisition"></asp:Label>
                                        <asp:Label ID="objlblApproveStaffRequisitionMSSCount" runat="server" Text="0" CssClass="badge bg-color-3"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkMyTrainingFeedback" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblMyTrainingFeedback" runat="server" Text="My Training Feedback"></asp:Label>
                                        <asp:Label ID="objlblMyTrainingFeedbackCount" runat="server" Text="0" CssClass="badge bg-color-8"></asp:Label>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkEvaluateEmployeeTraining" runat="server" class="list-group-item" Visible="false">
                                        <asp:Label ID="lblEvaluateEmployeeTraining" runat="server" Text="Evaluate Employee Training"></asp:Label>
                                        <asp:Label ID="objlblEvaluateEmployeeTrainingCount" runat="server" Text="0" CssClass="badge bg-color-9"></asp:Label>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div id="dvEmpOnLeave" class="col-lg-4 col-md-4 col-sm-6 col-xs-12" runat="server">
                        <div class="card dashboard">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblEmployeeOnLeaveTitle" runat="server" Text="Employee On Leave"></asp:Label></div>
                                    <div class="number count-to">
                                        <asp:Label ID="lblEmployeeOnLeaveCount" runat="server" Text="Today's Birthday"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-calendar-check col-pink"></i>
                                </div>
                            </div>
                            <div class="body">
                                <asp:DataList ID="dlEmployeeOnLeaveCount" runat="server" RepeatDirection="Vertical"
                                    ShowHeader="true" ShowFooter="true">
                                    <ItemTemplate>
                                        <div class="comment-box">
                                            <div class="media-left">
                                                <asp:Image ID="imgEmployeeOnLeaveEmployee" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                </asp:Image>
                                            </div>
                                            <div class="media-body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <div class="title">
                                                            <asp:Label ID="lblEmployeeOnLeaveName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label>
                                                        </div>
                                                        <div class="sub-title">
                                                            <asp:Label ID="lblEmployeeOnLeaveDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                                        </div>
                                                        <div class="sub-title">
                                                            <asp:Label ID="lblEmployeeOnLeavejob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <div class="title">
                                                            <asp:Label ID="Label3" runat="server" Text='<%# String.Format("{0:d/M/yyyy}", Eval("leavedateupto"))  %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnEmployeeOnLeaveShowAll" runat="server" CssClass="btn btn-link col-pink"
                                    Text="Show More" />
                            </div>
                        </div>
                    </div>
                    <div id="dvEmpWorkAnniversary" class="col-lg-4 col-md-4 col-sm-6 col-xs-12" runat="server">
                        <div class="card dashboard">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblWorkAnniversaryTitle" runat="server" Text="Today's Work Anniversary"></asp:Label></div>
                                    <div class="number count-to">
                                        <asp:Label ID="lblWorkAnniversaryCount" runat="server" Text="Today's Birthday"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-award col-purple"></i>
                                </div>
                            </div>
                            <div class="body">
                                <asp:DataList ID="dlWorkAnniversary" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                                    ShowFooter="true">
                                    <ItemTemplate>
                                        <div class="comment-box">
                                            <div class="media-left">
                                                <asp:Image ID="imgWorkAnniversary" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                </asp:Image>
                                            </div>
                                            <div class="media-body">
                                                <div class="title">
                                                    <asp:Label ID="lblWorkAnniversaryEmpName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblWorkAnniversaryDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                                </div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblWorkAnniversaryJob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnShowAllWorkAnniversary" runat="server" CssClass="btn btn-link col-purple"
                                    Text="Show More" />
                            </div>
                        </div>
                    </div>
                    <div id="dvUpcomingHolidays" class="col-lg-4 col-md-4 col-sm-6 col-xs-12" runat="server">
                        <div class="card dashboard">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblUpcomingHolidaysTitle" runat="server" Text="Upcoming Holidays"></asp:Label></div>
                                    <div class="number count-to">
                                        <asp:Label ID="lblUpcomingHolidaysCount" runat="server" Text="0"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="af aruti-icon-calendar col-indigo"></i>
                                </div>
                            </div>
                            <div class="body">
                                <asp:DataList ID="dlUpcomingHolidays" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                                    ShowFooter="true">
                                    <ItemTemplate>
                                        <div class="comment-box">
                                            <%-- <div class="media-left">
                                                    <asp:Image ID="imgBdayEmployee" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                    </asp:Image>
                                            </div>--%>
                                            <div class="media-body">
                                                <div class="title">
                                                    <asp:Label ID="lblUpcomingHolidaysName" runat="server" Text='<%# Eval("holidayname") %>'></asp:Label></div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblUpcomingHolidaysDate" runat="server" Text='<%# String.Format("{0:d/M/yyyy}", Eval("holidaydate")) %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnShowAllUpcomingHolidays" runat="server" CssClass="btn btn-link col-indigo"
                                    Text="Show More" />
                            </div>
                        </div>
                    </div>
                    <div id="dvTeamMembers" class="col-lg-4 col-md-4 col-sm-6 col-xs-12" runat="server">
                        <div class="card dashboard">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblTeamMembersTitle" runat="server" Text="Team Members"></asp:Label></div>
                                    <div class="number count-to">
                                        <asp:Label ID="lblTeamMembersCount" runat="server" Text="0"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-users col-deep-orange"></i>
                                </div>
                            </div>
                            <div class="body">
                                <asp:DataList ID="dlTeamMembers" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                                    ShowFooter="true">
                                    <ItemTemplate>
                                        <div class="comment-box">
                                            <div class="media-left">
                                                <asp:Image ID="imgTeamMembersName" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                </asp:Image>
                                            </div>
                                            <div class="media-body">
                                                <div class="title">
                                                    <asp:Label ID="lblTeamMembersEmpName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblTeamMembersDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                                </div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblTeamMembersJob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnShowAllTeamMembers" runat="server" CssClass="btn btn-link col-deep-orange"
                                    Text="Show More" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <asp:Panel runat="server" ID="pnlNewlyHired" CssClass="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="card dashboard">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblNewlyHired" runat="server" Text="Newly Hired"></asp:Label></div>
                                    <div class="number count-to">
                                        <asp:Label ID="lblNewlyHiredCount" runat="server" Text="0"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="far fa-user col-teal"></i>
                                </div>
                            </div>
                            <div class="body">
                                <asp:DataList ID="dlNewlyHired" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                                    ShowFooter="true">
                                    <ItemTemplate>
                                        <div class="comment-box">
                                            <div class="media-left">
                                                <asp:Image ID="imgNewlyHired" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                </asp:Image>
                                            </div>
                                            <div class="media-body">
                                                <div class="title">
                                                    <asp:Label ID="lblNewlyHiredEmpName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblNewlyHiredDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                                </div>
                                                <div class="sub-title">
                                                    <asp:Label ID="NewlyHiredJob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnShowallNewlyHired" runat="server" CssClass="btn btn-link col-teal"
                                    Text="Show More" />
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlStaffTurnOverChart" runat="server" CssClass="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card dashboard chart">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="lblStaffTurnoverTitle" runat="server" Text="Staff Turnover"></asp:Label></div>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-chart-bar col-indigo"></i>
                                </div>
                            </div>
                            <div class="body">
                                <div class="highcharts-figure">
                                    <div id="staff-turnover-container" class="chart-container">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="row clearfix">
                    <asp:Panel ID="pnlLeaveAnalysisChart" runat="server" CssClass="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card dashboard chart">
                            <div class="header info-box-4">
                                <div class="content">
                                    <div class="text">
                                        <asp:Label ID="Label4" runat="server" Text="Leave Analysis"></asp:Label>
                                    </div>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-calendar-alt col-light-blue"></i>
                                </div>
                            </div>
                            <div class="body">
                                <div class="highcharts-figure">
                                    <div id="leave-analysis-container" class="chart-container">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <ajaxtoolkit:ModalPopupExtender ID="popupShowAll" runat="server" PopupControlID="pnlShowAll"
                    TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlShowAll"
                    BackgroundCssClass="modal-backdrop">
                </ajaxtoolkit:ModalPopupExtender>
                <asp:Panel ID="pnlShowAll" runat="server" CssClass="modal-dialog card dashboard"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblShowAllTitle" runat="server" Text="All Details" />
                        </h2>
                    </div>
                    <div class="body" style="height: 300px">
                        <asp:DataList ID="dlPopupBirthday" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                            ShowFooter="true">
                            <ItemTemplate>
                                <div class="comment-box">
                                    <asp:Panel ID="pnlEmpImg" runat="server" CssClass="media-left">
                                        <asp:Image ID="imgBdayEmployee" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                        </asp:Image>
                                    </asp:Panel>
                                    <div class="media-body">
                                        <div class="title">
                                            <asp:Label ID="lblStageTitle" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                        <div class="sub-title">
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                        </div>
                                        <div class="sub-title">
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlPopupWorkAnniversary" runat="server" RepeatDirection="Vertical"
                            ShowHeader="true" ShowFooter="true">
                            <ItemTemplate>
                                <div class="comment-box">
                                    <div class="media-left">
                                        <asp:Image ID="imgWorkAnniversary" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                        </asp:Image>
                                    </div>
                                    <div class="media-body">
                                        <div class="title">
                                            <asp:Label ID="lblWorkAnniversaryEmpName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                        <div class="sub-title">
                                            <asp:Label ID="lblWorkAnniversaryDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                        </div>
                                        <div class="sub-title">
                                            <asp:Label ID="lblWorkAnniversaryJob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlPopupNewlyHired" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                            ShowFooter="true">
                            <ItemTemplate>
                                <div class="comment-box">
                                    <div class="media-left">
                                        <asp:Image ID="imgNewlyHired" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                        </asp:Image>
                                    </div>
                                    <div class="media-body">
                                        <div class="title">
                                            <asp:Label ID="lblNewlyHiredEmpName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                        <div class="sub-title">
                                            <asp:Label ID="lblNewlyHiredDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                        </div>
                                        <div class="sub-title">
                                            <asp:Label ID="NewlyHiredJob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlPopupUpcomingHolidays" runat="server" RepeatDirection="Vertical"
                            ShowHeader="true" ShowFooter="true">
                            <ItemTemplate>
                                <div class="comment-box">
                                    <%-- <div class="media-left">
                                                    <asp:Image ID="imgBdayEmployee" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                                    </asp:Image>
                                            </div>--%>
                                    <div class="media-body">
                                        <div class="title">
                                            <asp:Label ID="lblUpcomingHolidaysName" runat="server" Text='<%# Eval("holidayname") %>'></asp:Label></div>
                                        <div class="sub-title">
                                            <asp:Label ID="lblUpcomingHolidaysDate" runat="server" Text='<%# String.Format("{0:d/M/yyyy}", Eval("holidaydate")) %>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlPopupTeamMembers" runat="server" RepeatDirection="Vertical" ShowHeader="true"
                            ShowFooter="true">
                            <ItemTemplate>
                                <div class="comment-box">
                                    <div class="media-left">
                                        <asp:Image ID="imgTeamMembersName" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                        </asp:Image>
                                    </div>
                                    <div class="media-body">
                                        <div class="title">
                                            <asp:Label ID="lblTeamMembersEmpName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label></div>
                                        <div class="sub-title">
                                            <asp:Label ID="lblTeamMembersDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                        </div>
                                        <div class="sub-title">
                                            <asp:Label ID="lblTeamMembersJob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlPopupEmployeeOnLeave" runat="server" RepeatDirection="Vertical"
                            ShowHeader="true" ShowFooter="true">
                            <ItemTemplate>
                                <div class="comment-box">
                                    <div class="media-left">
                                        <asp:Image ID="imgEmployeeOnLeaveEmployee" runat="server" ImageUrl='<%# Eval("imagename") %>'>
                                        </asp:Image>
                                    </div>
                                    <div class="media-body">
                                        <div class="row clearfix">
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <div class="title">
                                                    <asp:Label ID="lblEmployeeOnLeaveName" runat="server" Text='<%# Eval("employeename") %>'></asp:Label>
                                                </div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblEmployeeOnLeaveDept" runat="server" Text='<%# Eval("deptname") %>'></asp:Label>
                                                </div>
                                                <div class="sub-title">
                                                    <asp:Label ID="lblEmployeeOnLeavejob" runat="server" Text='<%# Eval("jobname") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="title">
                                                    <asp:Label ID="Label3" runat="server" Text='<%# String.Format("{0:d/M/yyyy}", Eval("leavedateupto"))  %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnCloseShowAll" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

	function getStaffTurnover(ele,xcategories,yAxisTitle,xdata)
    {
	    Highcharts.chart(ele, {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: xcategories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text:  yAxisTitle
                    }
                },
                series: xdata
            });
            		
	}
	
    
	function getLeaveData(ele,xcategories,yAxisTitle,xdata)
    {
	    Highcharts.chart(ele, {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: xcategories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text:  yAxisTitle
                    }
                },
				plotOptions: {
					column: {
						stacking: 'normal',
					}
				},
    
                series: xdata
            });
            		
	}
    </script>

</asp:Content>
