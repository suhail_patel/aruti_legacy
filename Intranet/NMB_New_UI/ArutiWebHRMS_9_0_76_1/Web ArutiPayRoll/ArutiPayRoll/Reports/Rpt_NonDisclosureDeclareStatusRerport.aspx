﻿<%@ Page Title="Confidentiality Acknowledgement Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_NonDisclosureDeclareStatusRerport.aspx.vb"
    Inherits="Reports_Rpt_NonDisclosureDeclareStatusRerport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

      <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc7:AnalysisBy ID="popupAnalysisBy" runat="server" />
                <uc9:Export runat="server" ID="Export" />
               
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%-- <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Confidentiality Acknowledgement Report"
                                        CssClass="form-label"></asp:Label>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                    <i class="fas fa-sliders-h"></i> 
                                            </asp:LinkButton>
                                            <div style="text-align: right; display: none;">
                                                <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" ToolTip="Analysis By">
                                                <i class="fas fa-filter"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </li>
                                    </ul>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFinalcialYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFinalcialYear" runat="server" AutoPostBack="False">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDate" runat="server" Text="As On Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAsOnDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdvanceFilter" runat="server" CssClass="btn btn-default" Text="Advance Filter"
                                    Visible="false" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
