﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_PDPDevelopmentAreasReport.aspx.vb" Inherits="Reports_PDP_Reports_Rpt_PDPDevelopmentAreasReport" title="PDP Development Areas Report" %>


<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="PDP Development Areas Report"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                   <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                    <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblViewType" runat="server" Text="Employee"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboViewType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            
                             <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee"/>
                                </div>
                            </div>
                              <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkOtherTraining" runat="server" Text="View Other Training"/>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-default" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc7:AnalysisBy ID="popupAnalysisBy" runat="server" />
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            <uc9:Export runat="server" ID="Export" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>




