﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports

Partial Class Reports_PDP_Reports_Rpt_PDPDevelopmentAreasReport
    Inherits Basepage

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPDPDevelopmentAreasReport = New clsPDPDevelopmentAreasReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call FillCombo()
                'Call ResetValue()
                Me.ViewState.Add("StringIds", "")
                Me.ViewState.Add("StringName", "")
                Me.ViewState.Add("ViewIdx", -1)
                Me.ViewState.Add("Analysis_Fields", "")
                Me.ViewState.Add("Analysis_Join", "")
                Me.ViewState.Add("Analysis_OrderBy", "")
                Me.ViewState.Add("Report_GroupName", "")
            Else

                If IsNothing(ViewState("mstrAdvanceFilter")) = False Then
                    mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objPDPDevelopmentAreasReport As clsPDPDevelopmentAreasReport
    Private ReadOnly mstrModuleName As String = "frmPDPDevelopmentAreasReport"

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim ObjEmp As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Try

            dsCombo = ObjEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                             CStr(Session("UserAccessModeSetting")), True, _
                                             CBool(Session("IsIncludeInactiveEmp")), "Emp", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = "0"
                .DataBind()
            End With


            dsCombo = objMasterData.GetPDPGoalViewTypeList(True, "GoalViewType")

            With cboViewType
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("GoalViewType")
                .SelectedValue = "0"
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMasterData = Nothing
            ObjEmp = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            chkOtherTraining.Checked = False
            cboViewType.SelectedIndex = 0

            chkIncludeInactiveEmp.Checked = False
            Me.ViewState("StringIds") = ""
            Me.ViewState("ViewIdx") = -1
            Me.ViewState("StringName") = ""
            Me.ViewState("Analysis_Fields") = ""
            Me.ViewState("Analysis_Join") = ""
            Me.ViewState("Analysis_OrderBy") = ""
            Me.ViewState("Report_GroupName") = ""
            Me.ViewState("mstrAdvanceFilter") = ""
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objCycle As New clstlcycle_master
        Try
            objPDPDevelopmentAreasReport.SetDefaultValue()
            objPDPDevelopmentAreasReport._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objPDPDevelopmentAreasReport._EmployeeName = cboEmployee.Text
            objPDPDevelopmentAreasReport._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objPDPDevelopmentAreasReport._PDPGoalViewType = CInt(cboViewType.SelectedValue)
            objPDPDevelopmentAreasReport._AllowToViewOtherTraining = chkOtherTraining.Checked

            objPDPDevelopmentAreasReport._ViewByIds = mstrStringIds
            objPDPDevelopmentAreasReport._ViewIndex = mintViewIdx
            objPDPDevelopmentAreasReport._ViewByName = mstrStringName
            objPDPDevelopmentAreasReport._Analysis_Fields = mstrAnalysis_Fields
            objPDPDevelopmentAreasReport._Analysis_Join = mstrAnalysis_Join
            objPDPDevelopmentAreasReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPDPDevelopmentAreasReport._Report_GroupName = mstrReport_GroupName
            objPDPDevelopmentAreasReport._Advance_Filter = mstrAdvanceFilter
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            objCycle = Nothing
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objPDPDevelopmentAreasReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objPDPDevelopmentAreasReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                Session("UserAccessModeSetting").ToString(), True, ExportReportPath, _
                                                                OpenAfterExport)


            If objPDPDevelopmentAreasReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objPDPDevelopmentAreasReport._FileNameAfterExported
                Export.Show()
            End If

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Combobox Events"

#End Region

#Region "Control Event"

    'Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
    '    Try
    '        popupAnalysisBy.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("lnkAnalysisBy_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            Me.ViewState("StringIds") = popupAnalysisBy._ReportBy_Ids
            Me.ViewState("StringName") = popupAnalysisBy._ReportBy_Name
            Me.ViewState("ViewIdx") = popupAnalysisBy._ViewIndex
            Me.ViewState("Analysis_Fields") = popupAnalysisBy._Analysis_Fields
            Me.ViewState("Analysis_Join") = popupAnalysisBy._Analysis_Join
            Me.ViewState("Analysis_OrderBy") = popupAnalysisBy._Analysis_OrderBy
            Me.ViewState("Report_GroupName") = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :-" & ex.Message, Me)
        End Try
    End Sub



#End Region

#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
End Class

