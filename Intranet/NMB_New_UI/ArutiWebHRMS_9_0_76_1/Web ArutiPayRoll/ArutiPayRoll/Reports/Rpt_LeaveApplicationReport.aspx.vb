﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
#End Region

Partial Class Reports_Rpt_LeaveApplicationReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmLeaveApplication_Report"

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeaveApplication As clsLeaveApplication_Report
    'Pinkal (11-Sep-2020) -- End


    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty

#End Region

#Region " Page's Events "
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If




            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeaveApplication = New clsLeaveApplication_Report
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                dtpApplicationFrom.SetDate = Nothing
                dtpApplicationTo.SetDate = Nothing
                dtpStartDate.SetDate = Nothing
                dtpEndDate.SetDate = Nothing
            Else
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objEmp As New clsEmployee_Master
            Dim objLeave As New clsleavetype_master
            Dim objMaster As New clsMasterData

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "Employee", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, True, "", "", "")
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With
            objEmp = Nothing


            dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            With cboLeave
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Leave")
                .DataBind()
                .SelectedValue = "0"
            End With
            objLeave = Nothing



            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsList = objMaster.getLeaveStatusList("Status", True)
            dsList = objMaster.getLeaveStatusList("Status", Session("ApplicableLeaveStatus").ToString(), True)
            'Pinkal (03-Jan-2020) -- End


            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With
            objMaster = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpApplicationFrom.SetDate = Nothing
            dtpApplicationTo.SetDate = Nothing
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            cboEmployee.SelectedValue = "0"
            cboLeave.SelectedValue = "0"
            cboStatus.SelectedValue = "0"
            mstrViewByIds = ""
            mintViewIndex = 0
            mstrViewByName = ""
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLeaveApplication As clsLeaveApplication_Report) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objLeaveApplication.SetDefaultValue()
            objLeaveApplication.setDefaultOrderBy(0)

            If dtpApplicationFrom.IsNull = False Then
                objLeaveApplication._ApplicationFrom = dtpApplicationFrom.GetDate.Date
            End If

            If dtpApplicationTo.IsNull = False Then
                objLeaveApplication._ApplicationTo = dtpApplicationTo.GetDate.Date
            End If

            If dtpStartDate.IsNull = False Then
                objLeaveApplication._StartDate = dtpStartDate.GetDate.Date
            End If

            If dtpEndDate.IsNull = False Then
                objLeaveApplication._EndDate = dtpEndDate.GetDate.Date
            End If

            objLeaveApplication._EmployeeId = CInt(cboEmployee.SelectedValue)
            objLeaveApplication._Employee = cboEmployee.SelectedItem.Text
            objLeaveApplication._LeaveId = CInt(cboLeave.SelectedValue)
            objLeaveApplication._LeaveName = cboLeave.SelectedItem.Text
            objLeaveApplication._statusId = CInt(cboStatus.SelectedValue)
            objLeaveApplication._Status = cboStatus.SelectedItem.Text
            objLeaveApplication._ViewByIds = mstrViewByIds
            objLeaveApplication._ViewIndex = mintViewIndex
            objLeaveApplication._ViewByName = mstrViewByName
            objLeaveApplication._Analysis_Fields = mstrAnalysisFields
            objLeaveApplication._Analysis_Join = mstrAnalysisJoin
            objLeaveApplication._Analysis_OrderBy = mstrAnalysisOrderBy
            objLeaveApplication._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objLeaveApplication._Report_GroupName = mstrReportGroupName
            objLeaveApplication._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objLeaveApplication._OpenAfterExport = False
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApplication As New clsLeaveApplication_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objLeaveApplication) = False Then Exit Sub
            ''Pinkal (11-Sep-2020) -- End


            SetDateFormat()
            objLeaveApplication.Generate_DetailReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString, _
                                                    CStr(Session("UserAccessModeSetting")), True)

            If objLeaveApplication._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objLeaveApplication._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveApplication = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysisFields = popAnalysisby._Analysis_Fields
            mstrAnalysisJoin = popAnalysisby._Analysis_Join
            mstrAnalysisOrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysisOrderByGName = popAnalysisby._Analysis_OrderBy_GName
            mstrReportGroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " LinkButton Event "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.LblApplicationFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApplicationFrom.ID, Me.LblApplicationFrom.Text)
            Me.LblApplicationTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApplicationTo.ID, Me.LblApplicationTo.Text)
            Me.LblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblStartDate.ID, Me.LblStartDate.Text)
            Me.LblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEndDate.ID, Me.LblEndDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblLeaveName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveName.ID, Me.lblLeaveName.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblStatus.ID, Me.LblStatus.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.lnkAnalysisBy.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
