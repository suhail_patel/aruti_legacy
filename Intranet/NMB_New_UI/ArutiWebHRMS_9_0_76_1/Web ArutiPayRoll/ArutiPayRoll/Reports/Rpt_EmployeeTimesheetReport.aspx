﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_EmployeeTimesheetReport.aspx.vb"
    Inherits="Reports_Rpt_EmployeeTimesheetReport" Title="Employee Timesheet Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Time Sheet"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" ToolTip="Analysis By">
                                                      <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblShift" runat="server" Text="Shift" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboShift" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblAttCode" runat="server" Text="AttCode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAttCode" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:CheckBox ID="chkShowHoliday" runat="server" Text="Show Holiday" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:CheckBox ID="chkShowEachEmpOnNewPage" runat="server" Text="Show Each Employee On New Page"
                                            Checked="true" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:CheckBox ID="chkBreaktime" runat="server" Text="Show Break Time" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlShowTiming" runat="server">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <asp:CheckBox ID="chkShowTimingsIn24Hrs" runat="server" Text="Show Timing in 24 HRS Format" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <asp:CheckBox ID="chkShowLeaveType" runat="server" Text="Show Leave Type" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlShift" runat="server">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <asp:CheckBox ID="chkShift" runat="server" Text="Show Shift" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <asp:CheckBox ID="chkShowBaseHrs" runat="server" Text="Show Base Hrs" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlShowTotalAbsentDays" runat="server">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <asp:CheckBox ID="chkShowTotalAbsentDays" runat="server" Text="Show Total Absent" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlIncludeBaseHrsinTotalBaseHrs" runat="server">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkIncludeOFFHrsInBaseHrs" runat="server" Checked="true" Text="Include OFF Hours in Total Base Hours" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <%--'Pinkal (13-Sep-2021)-- Start
                                     'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.--%>
                                     <div class="row clearfix">
                                    <asp:Panel ID="pnlShowAbsentAfterEmpTerminated" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowAbsentAfterEmpTerminated" runat="server" Checked="true" Text="Show Absent Days On Timesheet After Employee Terminated" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlShowShortHrsOnBaseHrs" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowShortHrsOnBaseHrs" runat="server" Checked="false" Text="Show Short Hrs in Place of Base Hrs"
                                                AutoPostBack="true" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                     <asp:Panel ID="pnlShowLateComingInShortHrs" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowLateComingInShortHrs" runat="server" Checked="false" Text="Show Late Coming/ Early Leaving Consider As Short Hrs" />
                                        </div>
                                    </asp:Panel>
                                     </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlDisplayShortHrsWhenEmpAbsent" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkDisplayShortHrsWhenEmpAbsent" runat="server" Checked="false"
                                                Text="Display Short Hrs When Employee is Absent" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlShowActualTimingAfterRoundOff" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowActualTimingAfterRoundOff" runat="server" Checked="false"
                                                Text="Show Actual Time After Gracing Time" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                   <%--'Pinkal (13-Sep-2021) -- End--%>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSave" runat="server" Text="Save Selection" CssClass="lnkhover pull-right"
                                            Font-Bold="true"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc9:AnalysisBy ID="popAnalysisby" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
