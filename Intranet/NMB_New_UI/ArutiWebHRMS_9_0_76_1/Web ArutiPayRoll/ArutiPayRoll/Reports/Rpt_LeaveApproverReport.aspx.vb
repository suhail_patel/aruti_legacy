﻿Option Strict On 'Shani(11 Feb 2016)

#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
#End Region

Partial Class Reports_Rpt_LeaveApproverReport
    Inherits Basepage

#Region "Private Variables"
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmLeaveApproverReport"

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeaveApprover As New clsLeaveApproverReport
    'Pinkal (11-Sep-2020) -- End

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End




            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                Call ResetValue()
                Me.ViewState.Add("mstrStringIds", "")
                Me.ViewState.Add("mstrStringName", "")
                Me.ViewState.Add("mintViewIdx", 0)
                Me.ViewState.Add("mstrAnalysis_Fields", "")
                Me.ViewState.Add("mstrAnalysis_Join", "")
                Me.ViewState.Add("mstrAnalysis_OrderBy", "")
                Me.ViewState.Add("mstrReport_GroupName", "")
                Me.ViewState.Add("mstrAdvanceFilter", "")
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"
    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objapprover As New clsleaveapprover_master
            Dim objLevel As New clsapproverlevel_master
            Dim objLeaveType As New clsleavetype_master
            Dim dsList As New DataSet



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objapprover.GetList("Approver", True)
            dsList = objapprover.GetList("Approver", _
                                         Session("Database_Name").ToString(), _
                                         CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                         Session("EmployeeAsOnDate").ToString, _
                                         Session("UserAccessModeSetting").ToString, True, _
                                         CBool(Session("IsIncludeInactiveEmp")), True, False, _
                                         -1, Nothing, "", "")
            'Shani(20-Nov-2015) -- End

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("leaveapproverunkid") = 0
            drRow("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name")
            With cboApprover
                .DataValueField = "leaveapproverunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With
            objapprover = Nothing

            dsList = objLevel.getListForCombo("Level", True)
            With cboLevel
                .DataValueField = "levelunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Level")
                .DataBind()
                .SelectedValue = "0"
            End With
            objLevel = Nothing

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            'Shani(24-Aug-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With
            objEmp = Nothing

            dsList = objLeaveType.getListForCombo("LeaveType", True)
            With cboLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LeaveType")
                .DataBind()
                .SelectedValue = "0"
            End With
            objLeaveType = Nothing
            cboReportType.Items.Clear()
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Leave Approver Report"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Employee List with Their Leave Approver Report "))
            If CBool(Session("LeaveApproverForLeaveType")) Then
                cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Employee Wise Leave Approver Report"))
            End If
            cboReportType.SelectedIndex = 0
            Call cboReportType_SelectedIndexChanged(cboReportType, Nothing)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            'Pinkal (11-Sep-2020) -- End
            dsList = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLeaveApprover As clsLeaveApproverReport) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objLeaveApprover.SetDefaultValue()
            objLeaveApprover._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objLeaveApprover._ReportName = cboReportType.SelectedItem.Text
            objLeaveApprover._ApproverID = CInt(cboApprover.SelectedValue)
            objLeaveApprover._ApproverName = cboApprover.SelectedItem.Text
            objLeaveApprover._LevelID = CInt(cboLevel.SelectedValue)
            objLeaveApprover._LevelName = cboLevel.SelectedItem.Text
            objLeaveApprover._EmployeeID = CInt(cboEmployee.SelectedValue)
            objLeaveApprover._EmployeeName = cboEmployee.SelectedItem.Text
            objLeaveApprover._LeaveTypeID = CInt(cboLeaveType.SelectedValue)
            objLeaveApprover._LeaveTypeName = cboLeaveType.SelectedItem.Text
            objLeaveApprover._ViewByIds = Me.ViewState("mstrStringIds").ToString()
            objLeaveApprover._ViewIndex = CInt(Me.ViewState("mintViewIdx"))
            objLeaveApprover._ViewByName = Me.ViewState("mstrStringName").ToString()
            objLeaveApprover._Analysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
            objLeaveApprover._Analysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
            objLeaveApprover._Analysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
            objLeaveApprover._Report_GroupName = Me.ViewState("mstrReport_GroupName").ToString()
            objLeaveApprover._Advance_Filter = Me.ViewState("mstrAdvanceFilter").ToString()
            objLeaveApprover._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objLeaveApprover._UserUnkId = CInt(Session("UserId"))
            objLeaveApprover._IsLeaveApprover_ForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            objLeaveApprover._UserAccessFilter = Session("AccessLevelFilterString").ToString()
            objLeaveApprover.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeaveApprover.setDefaultOrderBy(0)
            'Pinkal (11-Sep-2020) -- End

            cboApprover.SelectedIndex = 0
            cboLevel.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0
            Me.ViewState("mstrStringIds") = ""
            Me.ViewState("mstrStringName") = ""
            Me.ViewState("mintViewIdx") = -1
            Me.ViewState("mstrAnalysis_Fields") = ""
            Me.ViewState("mstrAnalysis_Join") = ""
            Me.ViewState("mstrAnalysis_OrderBy") = ""
            Me.ViewState("mstrReport_GroupName") = ""
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsLeaveApproverReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try
            If CInt(cboReportType.SelectedIndex) <= 0 Then
                If CInt(cboApprover.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Leave Approver is mandatory information.Please Select Leave Approver."), Me)
                    Exit Sub
                End If
            ElseIf CInt(cboReportType.SelectedIndex) > 1 Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Employee is mandatory information.Please Select Employee."), Me)
                    Exit Sub
                End If
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objLeaveApprover) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objLeaveApprover.generateReport(0, enPrintAction.None, enExportAction.None)
            objLeaveApprover.generateReportNew(Session("Database_Name").ToString(), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                               Session("UserAccessModeSetting").ToString(), True, _
                                               Session("ExportReportPath").ToString, _
                                               CBool(Session("OpenAfterExport")), _
                                               0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Shani(20-Nov-2015) -- End

            Session("objRpt") = objLeaveApprover._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "Control Event"

    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex <= 1 Then
                cboApprover.SelectedIndex = 0
                cboApprover.Enabled = True
                cboLevel.SelectedIndex = 0
                cboLevel.Enabled = True
                cboLeaveType.Enabled = False
            Else
                cboLeaveType.Enabled = True
                cboEmployee.SelectedIndex = 0
                cboEmployee.Enabled = True
            End If

            If cboReportType.SelectedIndex = 1 Then
                lnkAnalysisBy.Visible = True
            Else
                lnkAnalysisBy.Visible = False
            End If
            cboLeaveType.SelectedIndex = 0

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeaveApprover.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))
            'Pinkal (11-Sep-2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboReportType_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            popAnalysisby._Hr_EmployeeTable_Alias = "hremployee_master"
            'Shani(11-Feb-2016) -- End
            popAnalysisby.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAnalysisBy_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            Me.ViewState("mstrStringIds") = popAnalysisby._ReportBy_Ids
            Me.ViewState("mstrStringName") = popAnalysisby._ReportBy_Name
            Me.ViewState("mintViewIdx") = popAnalysisby._ViewIndex
            Me.ViewState("mstrAnalysis_Fields") = popAnalysisby._Analysis_Fields
            Me.ViewState("mstrAnalysis_Join") = popAnalysisby._Analysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = popAnalysisby._Analysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = popAnalysisby._Report_GroupName
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub clsBtnLeaveApprover_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("clsBtnLeaveApprover_CloseButton_click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

#End Region

    Public Sub SetLanguage()
        'Language.setLanguage(mstrModuleName)


        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, objLeaveApprover._ReportName)
        'Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, objLeaveApprover._ReportName)
        Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
        'Pinkal (11-Sep-2020) -- End



        Me.LblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReportType.ID, Me.LblReportType.Text)
        Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
        Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
        Me.LblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLevel.ID, Me.LblLevel.Text)
        Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.LblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLeaveType.ID, Me.LblLeaveType.Text)
        Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)

        Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
        Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
    End Sub

End Class
