﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Competencies_Reports_Rpt_Staff_Cmpts_Param_Report
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmStaffCompetenciesReport"
    Private objStaffCmpts As clsStaffCompetenciesReport
    Private mstrAdvanceFilter As String = String.Empty
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objCMst As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, , , True, True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objCMst.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCmptCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCompetencies()
        Dim objCmpt As New clsassess_competencies_master
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim ds As New DataSet
                ds = objCmpt.GetList("List", True)
                Dim dt As DataTable = Nothing
                Dim strfilter As String = ""

                strfilter = "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                If CInt(cboCmptCategory.SelectedValue) > 0 Then
                    strfilter &= " AND competence_categoryunkid = '" & CInt(cboCmptCategory.SelectedValue) & "' "
                End If
                dt = New DataView(ds.Tables(0), strfilter, "", DataViewRowState.CurrentRows).ToTable

                lvCmpts.DataSource = dt
                lvCmpts.DataBind()
            Else
                lvCmpts.DataSource = Nothing
                lvCmpts.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboCmptCategory.SelectedValue = 0
            chkShowwithNocmpts.Checked = False
            lvCmpts.DataSource = Nothing
            lvCmpts.DataBind()
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter(ByRef objStaffCmpts As clsStaffCompetenciesReport) As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                Return False
            End If
            objStaffCmpts.SetDefaultValue()

            objStaffCmpts._AdvanceFilter = mstrAdvanceFilter
            objStaffCmpts._CompetenceGroup = cboCmptCategory.SelectedItem.Text
            objStaffCmpts._CompetenceGroupId = cboCmptCategory.SelectedValue
            If lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True).Count > 0 Then
                Dim strValues As String = ""
                strValues = String.Join(",", lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True).Select(Function(x) lvCmpts.DataKeys(x.RowIndex)("competenciesunkid").ToString).ToArray())
                objStaffCmpts._CompetenciesIds = strValues

                strValues = ""
                strValues = String.Join(",", lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True).Select(Function(x) lvCmpts.DataKeys(x.RowIndex)("name").ToString).ToArray())
                objStaffCmpts._CompetenciesNames = strValues
            End If
            objStaffCmpts._EmployeeId = cboEmployee.SelectedValue
            objStaffCmpts._EmployeeName = cboEmployee.SelectedItem.Text
            objStaffCmpts._FirstNamethenSurname = Session("FirstNamethenSurname")
            objStaffCmpts._PeriodId = cboPeriod.SelectedValue
            objStaffCmpts._PeriodName = cboPeriod.SelectedItem.Text
            objStaffCmpts._ShowEmployeeWithNoCompetencies = chkShowwithNocmpts.Checked


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
            End If
            If IsNothing(ViewState("mstrAdvanceFilter")) = False Then
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        objStaffCmpts = New clsStaffCompetenciesReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(objStaffCmpts) = False Then Exit Sub
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objStaffCmpts.ExportStaffCompetence_Report(Session("Database_Name").ToString, _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                   CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                   CStr(Session("UserAccessModeSetting")), True, strFilePath, False)

            If objStaffCmpts._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objStaffCmpts._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCmptCategory.SelectedIndexChanged
        Try
            FillCompetencies()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Other Event(s) "

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCompetencyCategory.ID, Me.lblCompetencyCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPeriod.ID, Me.lblPeriod.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lvCmpts.Columns(1).FooterText, lvCmpts.Columns(1).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblCompetencyCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCompetencyCategory.ID, Me.lblCompetencyCategory.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            lvCmpts.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lvCmpts.Columns(1).FooterText, lvCmpts.Columns(1).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
