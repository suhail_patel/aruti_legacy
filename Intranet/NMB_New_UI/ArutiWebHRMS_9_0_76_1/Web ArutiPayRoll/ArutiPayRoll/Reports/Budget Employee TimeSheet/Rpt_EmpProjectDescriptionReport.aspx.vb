﻿
#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Budget_Employee_TimeSheet_Rpt_EmpProjectDescriptionReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmEmpProjectDescriptionReport"
    Dim objEmpProjectDescription As clsEmpProjectDescriptionReport
    Private mdtTable As DataTable = Nothing
#End Region

#Region "Constructor"
    Public Sub New()
        'objEmpProjectDescription = New clsEmpProjectDescriptionReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'objEmpProjectDescription.SetDefaultValue()
    End Sub
#End Region

#Region " Page's Events "
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objEmpProjectDescription = New clsEmpProjectDescriptionReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                ResetValue()
            Else
                mdtTable = CType(Me.ViewState("Employee"), DataTable)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("Employee") = mdtTable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
            objPeriod = Nothing

            Dim objStatus As New clsMasterData
            dsList = objStatus.getLeaveStatusList("Status", Session("ApplicableLeaveStatus").ToString(), False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid in (0,1,2,3)", "", DataViewRowState.CurrentRows).ToTable()
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = 0
            End With
            objStatus = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillEmployee(ByVal mdtStartDate As Date, ByVal mdtEndDate As Date)
        Try
            Dim objEmp As New clsEmployee_Master

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = -1
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            Dim dsList As DataSet = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            mdtStartDate, mdtEndDate, _
                                             CStr(Session("UserAccessModeSetting")), _
                                            True, CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpUnkId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, blnApplyAccess)



            mdtTable = dsList.Tables(0)
            objEmp = Nothing

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = mdtTable
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is compulsory information.Please Select period."), Me)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Return False
            End If

            objEmpProjectDescription.SetDefaultValue()

            objEmpProjectDescription._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpProjectDescription._PeriodName = cboPeriod.SelectedItem.Text
            objEmpProjectDescription._StatusId = CInt(cboStatus.SelectedValue)
            objEmpProjectDescription._StatusName = cboStatus.SelectedItem.Text


            If CInt(cboEmployee.SelectedValue) > 0 Then
                objEmpProjectDescription._dtEmployee = mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue)).ToList().CopyToDataTable()
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
            objEmpProjectDescription._StartDate = objPeriod._TnA_StartDate.Date
            objEmpProjectDescription._EndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing

            objEmpProjectDescription._ShowDonort = chkShowDonor.Checked
            objEmpProjectDescription._ShowProject = chkShowProject.Checked
            objEmpProjectDescription._ShowStatus = chkShowStatus.Checked
            objEmpProjectDescription._ViewReportInHTML = chkShowReportInHtml.Checked
            objEmpProjectDescription._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objEmpProjectDescription._OpenAfterExport = False


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Project_Description_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CType(dsRow.Item("headtypeid"), clsEmpProjectDescriptionReport.EnColumn)

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_Donor
                            chkShowDonor.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_Project
                            chkShowProject.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_Status
                            chkShowStatus.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_HTML
                            chkShowReportInHtml.Checked = CBool(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            FillCombo()
            cboPeriod.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            GetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            Call SetDateFormat()

   
            objEmpProjectDescription.Generate_DetailReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString, _
                                                  CStr(Session("UserAccessModeSetting")), True, False)


            If objEmpProjectDescription._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmpProjectDescription._FileNameAfterExported
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()
                'Gajanan [17-Sep-2020] -- End
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try
            objUserDefRMode._Reportunkid = enArutiReport.Employee_Project_Description_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            For i As Integer = 1 To 4

                Select Case CType(i, clsEmpProjectDescriptionReport.EnColumn)

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_Donor
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_Donor
                        objUserDefRMode._EarningTranHeadIds = chkShowDonor.Checked

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_Project
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_Project
                        objUserDefRMode._EarningTranHeadIds = chkShowProject.Checked

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_Status
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_Status
                        objUserDefRMode._EarningTranHeadIds = chkShowStatus.Checked

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_HTML
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_HTML
                        objUserDefRMode._EarningTranHeadIds = chkShowReportInHtml.Checked

                End Select

                intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Project_Description_Report, 0, 0, objUserDefRMode._Headtypeid)
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    mblnFlag = objUserDefRMode.Insert()
                Else
                    mblnFlag = objUserDefRMode.Update()
                End If

            Next

            If mblnFlag Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Selection Saved Successfully."), Me)
            Else
                DisplayMessage.DisplayMessage(objUserDefRMode._Message, Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim mdtStartDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
            Dim mdtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPeriod._TnA_StartDate.Date
                mdtEndDate = objPeriod._TnA_EndDate.Date
                objPeriod = Nothing
            End If
            FillEmployee(mdtStartDate, mdtEndDate)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)

            Me.LblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblStatus.ID, Me.LblStatus.Text)

            Me.chkShowDonor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowDonor.ID, Me.chkShowDonor.Text)
            Me.chkShowStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowStatus.ID, Me.chkShowStatus.Text)
            Me.chkShowProject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowProject.ID, Me.chkShowProject.Text)
            Me.chkShowReportInHtml.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowReportInHtml.ID, Me.chkShowReportInHtml.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Period is compulsory information.Please Select period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Selection Saved Successfully.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
