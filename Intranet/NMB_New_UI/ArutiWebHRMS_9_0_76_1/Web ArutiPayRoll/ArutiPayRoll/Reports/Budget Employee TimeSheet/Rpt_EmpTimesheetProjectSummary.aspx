﻿<%@ Page Title="Employee Timesheet Project Summary Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_EmpTimesheetProjectSummary.aspx.vb" Inherits="Reports_Budget_Employee_TimeSheet_Rpt_EmpTimesheetProjectSummary" %>

<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Timesheet Project Summary Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                                 <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblProjectCode" runat="server" Text="Project Code" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboProjectCode" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblTransactionHead" runat="server" Text="Transaction Head" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTransactionHeadFilter" AutoPostBack="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 200px">
                                                            <asp:DataGrid ID="dgTransactionHead" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                        FooterText="btnEdit">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chktranheadAll" runat="server" Enabled="true" AutoPostBack="true"
                                                                                OnCheckedChanged="chkSelectAll_Checkchanged" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chktranhead" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkSelect_Checkchanged"
                                                                                CssClass="filled-in" Text=" " />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="code" HeaderText="Code" FooterText="dgcolhTranheadcode" />
                                                                    <asp:BoundColumn DataField="name" HeaderText="Transaction Head" FooterText="dgcolhTransactionHead" />
                                                                    <asp:BoundColumn DataField="tranheadunkid" HeaderText="TranHeadID" FooterText="objdgcolhTranHeadID"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowReportInHtml" Text="Show Report In HTML" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEmpIdentifyNo" Text="Show Employee Identify No " runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowLeaveTypeHrs" Text="Show Leave Type Hours" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowHolidayHrs" Text="Show Holiday Hours" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowExtraHrs" Text="Show Extra Hours" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkIgnoreZeroLeaveTypehrs" Text="Ignore Zero hours Leave Type"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSave" Text="Save Settings" runat="server" CssClass="lnkhover pull-right"
                                            Font-Bold="true"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--
                    <div class="panel-primary">
                        <div class="panel-heading">
                           
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                               
                                            </td>
                                            <td style="width: 35%">
                                               
                                            </td>
                                            <td style="width: 10%">
                                               
                                            </td>
                                            <td style="width: 35%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                
                                            </td>
                                            <td colspan="3">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                
                                            </td>
                                            <td colspan="3">
                                              
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="4">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="4">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%; vertical-align: top" colspan="4">
                                                <asp:Panel ID="pnlTranHead" runat="server" Width="100%" ScrollBars="Auto" Height="200px">
                                                   
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" colspan="2">
                                               
                                            </td>
                                            <td align="left" colspan="2">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" colspan="2">
                                              
                                            </td>
                                            <td align="left" colspan="2">
                                               
                                            </td>
                                        </tr>
                                        
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                               
                                            </td>
                                            <td align="left" colspan="2">
                                               
                                            </td>
                                         </tr>   
                                         
                                         <tr style="width: 100%">
                                             <td style="text-align: right; font-weight: bold" colspan="4">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                <uc1:AnalysisBy ID="popupAnalysisBy" runat="server" />
                <uc2:Export runat="server" ID="Export" />
            </ContentTemplate>
              <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
