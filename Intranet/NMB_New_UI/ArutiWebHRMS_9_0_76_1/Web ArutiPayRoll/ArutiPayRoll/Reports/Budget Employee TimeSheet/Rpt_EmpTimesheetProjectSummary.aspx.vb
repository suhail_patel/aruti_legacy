﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Budget_Employee_TimeSheet_Rpt_EmpTimesheetProjectSummary
    Inherits Basepage


#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmEmpTimesheetProjectSummaryReport"
    Dim objEmpTimesheet As clsEmpBudgetTimesheetSummaryReport
    Dim dvTrandhead As DataView = Nothing
    Dim mdtStartDate As DateTime = Nothing
    Dim mdtEndDate As DateTime = Nothing
    Dim mstrTranHeadIDs As String = ""
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""

#End Region

#Region " Page's Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objEmpTimesheet = New clsEmpBudgetTimesheetSummaryReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                ResetValue()
            Else
                mdtStartDate = CDate(Me.ViewState("StartDate"))
                mdtEndDate = CDate(Me.ViewState("EndDate"))
                dvTrandhead = CType(Me.Session("TranHeadView"), DataView)
                mstrViewByIds = ViewState("mstrViewByIds").ToString
                mstrViewByName = ViewState("mstrViewByName").ToString
                mintViewIndex = CInt(ViewState("mintViewIndex"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAnalysis_OrderBy_GName = ViewState("mstrAnalysis_OrderBy_GName").ToString
            End If

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            SetVisibility(True)
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("StartDate") = mdtStartDate
            Me.ViewState("EndDate") = mdtEndDate
            Me.Session("TranHeadView") = dvTrandhead
            Me.ViewState.Add("mstrViewByIds", mstrViewByIds)
            Me.ViewState.Add("mstrViewByName", mstrViewByName)
            Me.ViewState.Add("mintViewIndex", mintViewIndex)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            Me.ViewState.Add("mstrAnalysis_OrderBy_GName", mstrAnalysis_OrderBy_GName)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "By Hours"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "By Percentage"))
            cboReportType.SelectedIndex = 0

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objPeriod = Nothing

            dsList = Nothing

            Dim objEmployee As New clsEmployee_Master
            Dim intEmployeeID As Integer = 0
            Dim blnSelect As Boolean = True
            Dim blnIncludeAccessFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                intEmployeeID = CInt(Session("Employeeunkid"))
                blnIncludeAccessFilter = False
                blnSelect = False
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "EmpList", blnSelect, intEmployeeID, , , , , , , , , , , , , , , , blnIncludeAccessFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    .SelectedValue = CStr(Session("Employeeunkid"))
                Else
                    .SelectedValue = "0"
                End If
            End With
            objEmployee = Nothing

            dsList = Nothing
            Dim objFundProject As New clsFundProjectCode
            dsList = objFundProject.GetComboList("Project", True, 0)
            With cboProjectCode
                .DataValueField = "fundprojectcodeunkid"
                .DataTextField = "fundprojectname"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objFundProject = Nothing

            dsList = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillTransactionHead()
        Dim dsList As DataSet = Nothing
        Try
            Dim objTransaction As New clsTransactionHead
            Dim mstrFilter As String = "trnheadtype_id in (" & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & ")"
            dsList = objTransaction.getComboList(CStr(Session("Database_Name")), "List", False, 0, 0, -1, False, False, mstrFilter)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dcColumn As New DataColumn("ischeck")
                dcColumn.DataType = GetType(System.Boolean)
                dcColumn.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcColumn)
            End If
            dvTrandhead = dsList.Tables(0).DefaultView
            FillGrid()
            GetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            If dvTrandhead IsNot Nothing Then
                dgTransactionHead.AutoGenerateColumns = False
                dgTransactionHead.DataSource = dvTrandhead
                dgTransactionHead.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dvTrandhead Is Nothing OrElse (dvTrandhead IsNot Nothing AndAlso dvTrandhead.Table.Rows.Count <= 0) Then
                dgTransactionHead.DataSource = New List(Of String)
                dgTransactionHead.DataBind()
            End If
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0
            cboProjectCode.SelectedIndex = 0
            chkShowReportInHtml.Checked = False
            txtTransactionHeadFilter.Text = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
            FillTransactionHead()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is compulsory information.Please Select period."), Me)
                cboPeriod.Focus()
                Return False
            End If

            objEmpTimesheet.SetDefaultValue()


            objEmpTimesheet._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objEmpTimesheet._ReportTypeName = cboReportType.SelectedItem.Text

            objEmpTimesheet._EmployeeID = CInt(cboEmployee.SelectedValue)
            objEmpTimesheet._EmployeeName = cboEmployee.SelectedItem.Text

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            objEmpTimesheet._EmployeeCode = objEmployee._Employeecode

            If CInt(Session("Employeeunkid")) > 0 Then
                objEmpTimesheet._UserName = objEmployee._Displayname
            End If

            objEmployee = Nothing

            Dim lstID As List(Of String) = (From p In dvTrandhead.Table Where CBool(p("ischeck")) = True Select CStr(p("tranheadunkid"))).ToList()
            If lstID.Count > 0 Then objEmpTimesheet._TransactionHeadIDs = String.Join(",", lstID.ToArray)

            objEmpTimesheet._ProjectCodeId = CInt(cboProjectCode.SelectedValue)
            objEmpTimesheet._ProjectCode = cboProjectCode.SelectedItem.Text

            objEmpTimesheet._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpTimesheet._PeriodName = cboPeriod.SelectedItem.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'objEmpTimesheet._StartDate = objPeriod._Start_Date.Date
            'objEmpTimesheet._EndDate = objPeriod._End_Date.Date
            'mdtStartDate = objPeriod._Start_Date.Date
            'mdtEndDate = objPeriod._End_Date.Date

            objEmpTimesheet._StartDate = objPeriod._TnA_StartDate.Date
            objEmpTimesheet._EndDate = objPeriod._TnA_EndDate.Date
            mdtStartDate = objPeriod._TnA_StartDate.Date
            mdtEndDate = objPeriod._TnA_EndDate.Date

            objPeriod = Nothing

            If CInt(Session("Employeeunkid")) > 0 Then
                objEmpTimesheet._IncludeAccessFilterQry = False
            End If


            objEmpTimesheet._ViewReportInHTML = chkShowReportInHtml.Checked
            objEmpTimesheet._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objEmpTimesheet._OpenAfterExport = False

            objEmpTimesheet._ViewByIds = mstrViewByIds
            objEmpTimesheet._ViewIndex = mintViewIndex
            objEmpTimesheet._ViewByName = mstrViewByName
            objEmpTimesheet._Analysis_Fields = mstrAnalysis_Fields
            objEmpTimesheet._Analysis_Join = mstrAnalysis_Join
            objEmpTimesheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpTimesheet._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objEmpTimesheet._Report_GroupName = mstrReport_GroupName

            objEmpTimesheet._UserUnkId = CInt(Session("UserId"))
            GUI.fmtCurrency = Session("fmtCurrency")

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            objEmpTimesheet._ShowEmpIdentifyNo = chkShowEmpIdentifyNo.Checked
            objEmpTimesheet._ShowLeaveTypeHours = chkShowLeaveTypeHrs.Checked
            objEmpTimesheet._ShowHolidayHours = chkShowHolidayHrs.Checked
            If chkShowExtraHrs.Visible Then
                objEmpTimesheet._ShowExtraHours = chkShowExtraHrs.Checked
            Else
                objEmpTimesheet._ShowExtraHours = False
            End If
            'Pinkal (28-Jul-2018) -- End

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            objEmpTimesheet._IgnoreZeroLeaveTypeHrs = chkIgnoreZeroLeaveTypehrs.Checked
            'Pinkal (13-Aug-2018) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet

        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Timesheet_Project_Summary_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                If dsList.Tables(0).Rows(0)("transactionheadid").ToString().Trim.Length > 0 Then
                    If dsList.Tables(0).Rows(0)("transactionheadid").ToString().Contains("|") Then
                        mstrTranHeadIDs = dsList.Tables(0).Rows(0)("transactionheadid").ToString().Substring(0, dsList.Tables(0).Rows(0)("transactionheadid").ToString().IndexOf("|"))
                        Dim ar() As String = dsList.Tables(0).Rows(0)("transactionheadid").ToString().Trim.Substring(dsList.Tables(0).Rows(0)("transactionheadid").ToString().IndexOf("|") + 1).Split("|")
                        If ar.Length > 0 Then
                            chkShowReportInHtml.Checked = CBool(ar(0))
                            chkShowEmpIdentifyNo.Checked = CBool(ar(1))
                            chkShowLeaveTypeHrs.Checked = CBool(ar(2))
                            chkShowHolidayHrs.Checked = CBool(ar(3))
                            chkShowExtraHrs.Checked = CBool(ar(4))
                            'Pinkal (13-Aug-2018) -- Start
                            'Enhancement - Changes For PACT [Ref #249,252]
                            chkIgnoreZeroLeaveTypehrs.Checked = CBool(ar(5))
                            'Pinkal (13-Aug-2018) -- End
                        End If
                    Else
                        mstrTranHeadIDs = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                    End If

                    If mstrTranHeadIDs.Trim.Length > 0 Then
                        Dim drRow() As DataRow = dvTrandhead.Table.Select("tranheadunkid in (" & mstrTranHeadIDs & ")")
                        If drRow.Length > 0 Then
                            For Each dr As DataRow In drRow
                                dr("ischeck") = True
                                dr.AcceptChanges()

                                'Pinkal (08-Feb-2022) -- Start
                                'Enhancement NMB  - Language Change Issue for All Modules.	
                                'CType(dgTransactionHead.Items(dvTrandhead.Table.Rows.IndexOf(dr)).Cells(0).FindControl("chkSelect"), CheckBox).Checked = True
                                CType(dgTransactionHead.Items(dvTrandhead.Table.Rows.IndexOf(dr)).Cells(0).FindControl("chktranhead"), CheckBox).Checked = True
                                'Pinkal (08-Feb-2022) -- End


                            Next
                            dvTrandhead.Table.AcceptChanges()
                        End If
                    End If
                End If
                'Pinkal (28-Jul-2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private Sub SetVisibility(ByVal blnFlag As Boolean)
        Try
            chkShowHolidayHrs.Visible = blnFlag
            chkShowLeaveTypeHrs.Visible = blnFlag

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            chkIgnoreZeroLeaveTypehrs.Visible = blnFlag
            'Pinkal (13-Aug-2018) -- End

            chkShowExtraHrs.Visible = blnFlag
            If blnFlag AndAlso CBool(Session("AllowOverTimeToEmpTimesheet")) Then
                chkShowExtraHrs.Visible = blnFlag
            Else
                chkShowExtraHrs.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (28-Jul-2018) -- End

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_Checkchanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgTransactionHead.Items.Count <= 0 Then Exit Sub
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelectAll.NamingContainer, DataGridItem)
            Dim Query = From Row In dvTrandhead.Table.AsEnumerable() Select Row
            For Each rows In Query
                rows.Item("ischeck") = chkSelectAll.Checked
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'CType(dgTransactionHead.Items(dvTrandhead.Table.Rows.IndexOf(rows)).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                CType(dgTransactionHead.Items(dvTrandhead.Table.Rows.IndexOf(rows)).FindControl("chktranhead"), CheckBox).Checked = chkSelectAll.Checked
                'Gajanan [17-Sep-2020] -- End
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_Checkchanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)
            Dim dRow() As DataRow = dvTrandhead.Table.Select("tranheadunkid =" & CInt(item.Cells(3).Text))
            If dRow.Length > 0 Then
                For Each row In dRow
                    row.Item("IsCheck") = chkSelect.Checked
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'CType(dgTransactionHead.Items(item.ItemIndex).Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkSelect.Checked
                    CType(dgTransactionHead.Items(item.ItemIndex).Cells(0).FindControl("chktranhead"), CheckBox).Checked = chkSelect.Checked
                    'Gajanan [17-Sep-2020] -- End
                Next
                dvTrandhead.Table.AcceptChanges()
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            Call SetDateFormat()

            objEmpTimesheet.Generate_EmployeeSummaryReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True)


            If objEmpTimesheet._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmpTimesheet._FileNameAfterExported
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()
                'Gajanan [17-Sep-2020] -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link button Events"

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            Dim lstID As List(Of String) = (From p In dvTrandhead.Table Where CBool(p("ischeck")) = True Select CStr(p("tranheadunkid"))).ToList()

            If lstID.Count > 0 Then
                mstrTranHeadIDs = String.Join(",", lstID.ToArray)
            End If

            objUserDefRMode._Reportunkid = enArutiReport.Employee_Timesheet_Project_Summary_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            'objUserDefRMode._EarningTranHeadIds = mstrTranHeadIDs & "|" & IIf(chkShowReportInHtml.Checked, 1, 0) & "|" & IIf(chkShowEmpIdentifyNo.Checked, 1, 0) & "|" & IIf(chkShowLeaveTypeHrs.Checked, 1, 0) & "|" & IIf(chkShowHolidayHrs.Checked, 1, 0) & "|" & IIf(chkShowExtraHrs.Checked, 1, 0)
            objUserDefRMode._EarningTranHeadIds = mstrTranHeadIDs & "|" & IIf(chkShowReportInHtml.Checked, 1, 0) & "|" & IIf(chkShowEmpIdentifyNo.Checked, 1, 0) & "|" & IIf(chkShowLeaveTypeHrs.Checked, 1, 0) & "|" & IIf(chkShowHolidayHrs.Checked, 1, 0) & "|" & IIf(chkShowExtraHrs.Checked, 1, 0) & "|" & IIf(chkIgnoreZeroLeaveTypehrs.Checked, 1, 0)
            'Pinkal (13-Aug-2018) -- End
            'Pinkal (28-Jul-2018) -- End

            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Timesheet_Project_Summary_Report, 0, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            If mblnFlag Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Selection Saved Successfully"), Me)
                'Sohail (23 Mar 2019) -- End
            Else
                DisplayMessage.DisplayMessage(objUserDefRMode._Message, Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysis_OrderBy_GName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Private Sub txtTransactionHeadFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTransactionHeadFilter.TextChanged
        Try
            If dvTrandhead Is Nothing Then Exit Sub
            dvTrandhead.RowFilter = "code  LIKE  '%" & txtTransactionHeadFilter.Text.Trim & "%' OR name LIKE  '%" & txtTransactionHeadFilter.Text.Trim & "%'"
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Protected Sub dgTransactionHead_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTransactionHead.ItemDataBound
        Try
            If e.Item.ItemIndex < 0 Then Exit Sub
            Dim dRow() As DataRow = dvTrandhead.Table.Select("tranheadunkid = " & CInt(e.Item.Cells(3).Text))
            If dRow.Length > 0 Then
                If IsDBNull(dRow(0).Item("ischeck")) Then dRow(0).Item("ischeck") = False
                If CBool(dRow(0).Item("ischeck")) = True Then
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'CType(e.Item.Cells(0).FindControl("chkSelect"), CheckBox).Checked = CBool(dRow(0).Item("ischeck"))
                    CType(e.Item.Cells(0).FindControl("chktranhead"), CheckBox).Checked = CBool(dRow(0).Item("ischeck"))
                    'Gajanan [17-Sep-2020] -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

#Region "Combobox Events"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                SetVisibility(True)
            Else
                SetVisibility(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (28-Jul-2018) -- End

#Region " Language "
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)

            Me.LblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text)
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnExport.ID, Me.btnExport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text)
            Me.chkShowReportInHtml.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowReportInHtml.ID, Me.chkShowReportInHtml.Text)
            Me.LblProjectCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblProjectCode.ID, Me.LblProjectCode.Text)
            Me.LblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblReportType.ID, Me.LblReportType.Text)
            Me.LblTransactionHead.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblTransactionHead.ID, Me.LblTransactionHead.Text)
            Me.lnkSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkSave.ID, Me.lnkSave.Text)
            Me.dgTransactionHead.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgTransactionHead.Columns(1).FooterText, Me.dgTransactionHead.Columns(1).HeaderText)
            Me.dgTransactionHead.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgTransactionHead.Columns(2).FooterText, Me.dgTransactionHead.Columns(2).HeaderText)
            Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region


End Class
