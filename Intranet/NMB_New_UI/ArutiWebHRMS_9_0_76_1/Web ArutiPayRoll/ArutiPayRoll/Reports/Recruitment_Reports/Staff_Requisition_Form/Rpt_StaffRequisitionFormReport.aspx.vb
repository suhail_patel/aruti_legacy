﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.Net.Dns
Imports ArutiReports

#End Region

Partial Class Reports_Recruitment_Reports_Staff_Requisition_Form_Rpt_StaffRequisitionFormReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmStaffRequisitionFormReport"
    Private objRequisitionForm As clsStaffRequisitionFormReport
    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
    Private mintStaffRequisitionTranunkid As Integer = 0
    'Hemant (01 Nov 2021) -- End
#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objRequisitionForm = New clsStaffRequisitionFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
            Call SetLanguage()
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
                If Session("intstaffrequisitiontranunkid") IsNot Nothing Then
                    ViewState("intstaffrequisitiontranunkid") = CInt(Session("intstaffrequisitiontranunkid"))
                    Session.Remove("intstaffrequisitiontranunkid")
                End If
                mintStaffRequisitionTranunkid = CInt(ViewState("intstaffrequisitiontranunkid"))
                'Hemant (01 Nov 2021) -- End
                Call FillCombo()
            Else
                mintStaffRequisitionTranunkid = CInt(ViewState("intstaffrequisitiontranunkid"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("intstaffrequisitiontranunkid") = mintStaffRequisitionTranunkid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objJob As New clsJobs
        Dim objRequisition As New clsStaffrequisition_Tran
        Dim dsList As New DataSet
        Try
            dsList = objMaster.GetEAllocation_Notification("List")
            Dim dr As DataRow = dsList.Tables(0).NewRow()
            dr("id") = 0 : dr("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(dr, 0)
            With cboRequistionBy
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objMaster.GetListForStaffRequisitionStatus(True, "Status")
            With cboRequisitionType
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Job")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            dsList = objMaster.getApprovalStatus("Job", True, True, True, True, True, True, True)
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Job")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Hemant (01 Nov 2021) -- End

            Call cboRequistionBy_SelectedIndexChanged(cboRequistionBy, New System.EventArgs)
            Call cboJob_SelectedIndexChanged(cboJob, New System.EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose() : objMaster = Nothing : objJob = Nothing : objRequisition = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboJob.SelectedValue = "0"
            cboRequisitionType.SelectedValue = "0"
            cboRequistionBy.SelectedValue = "0"
            dtpWStartDateFrom.SetDate = Nothing
            dtpWStartDateTo.SetDate = Nothing
            nudPositionFrom.Decimal_ = 0
            nudPositionTo.Decimal_ = 0
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            cboStatus.SelectedValue = "0"
            'Hemant (01 Nov 2021) -- End
            Call cboRequistionBy_SelectedIndexChanged(cboRequistionBy, New System.EventArgs)
            Call cboJob_SelectedIndexChanged(cboJob, New System.EventArgs)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objRequisitionForm.SetDefaultValue()
            
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvFormNo.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("objdgcolhFCheck"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, Please check atleast one staff requisition form number in order to generate report."), Me)
                Return False
            End If

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objRequisitionForm._RequisitionFormIds = String.Join(",", (From p In gRow Select (dgvFormNo.DataKeys(p.DataItemIndex)("staffrequisitiontranunkid").ToString())).ToList().Distinct.ToArray)
                objRequisitionForm._RequisitionFormNos = String.Join(",", (From p In gRow Select (dgvFormNo.DataKeys(p.DataItemIndex)("formno").ToString())).ToList().Distinct.ToArray)
            End If

            gRow = Nothing
            Dim lstIDs As List(Of String) = Nothing
            gRow = dgvAllocation.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("objdgcolhACheck"), CheckBox).Checked = True)
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objRequisitionForm._AllocationIds = String.Join(",", (From p In gRow Select (dgvAllocation.DataKeys(p.DataItemIndex)(0).ToString())).ToList().Distinct.ToArray)
                objRequisitionForm._AllocationNames = String.Join(",", (From p In gRow Select (dgvAllocation.DataKeys(p.DataItemIndex)(1).ToString())).ToList().Distinct.ToArray)
            End If

            objRequisitionForm._JobId = CInt(cboJob.SelectedValue)
            objRequisitionForm._JobName = cboJob.SelectedItem.Text
            objRequisitionForm._PositionFrom = CInt(nudPositionFrom.Decimal_)
            objRequisitionForm._PositionTo = CInt(nudPositionTo.Decimal_)
            objRequisitionForm._RequisitionById = CInt(cboRequistionBy.SelectedValue)
            objRequisitionForm._RequisitionByName = cboRequistionBy.SelectedItem.Text
            objRequisitionForm._RequisitionTypeId = CInt(cboRequisitionType.SelectedValue)
            objRequisitionForm._RequisitionTypeName = cboRequisitionType.SelectedItem.Text
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            objRequisitionForm._StatusId = CInt(cboStatus.SelectedValue)
            objRequisitionForm._StatusName = cboStatus.SelectedItem.Text
            'Hemant (01 Nov 2021) -- End
            If dtpWStartDateFrom.IsNull = False Then
                objRequisitionForm._WorkFromDate = dtpWStartDateFrom.GetDate.Date
            End If
            If dtpWStartDateTo.IsNull = False Then
                objRequisitionForm._WorkToDate = dtpWStartDateTo.GetDate.Date
            End If
            'Sohail (14 Sep 2021) -- Start
            'NMB Issue :  : Staff requisition report showing wrong signature (printed by).
            If CStr(Session("Firstname")).Trim <> "" Then
                objRequisitionForm._User_Name = CStr(Session("Firstname")) & " " & CStr(Session("Surname"))
            Else
                objRequisitionForm._User_Name = CStr(Session("UserName"))
            End If
            'Sohail (14 Sep 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboRequistionBy.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
                Case 0
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List", True, "  AND 1 = 2")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Dim mdtAllocation As DataTable
        Try
            Dim dcol As New DataColumn
            With dcol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dTable.Columns.Add(dcol)
            mdtAllocation = dTable.Copy()
            
            Dim blnNoRecord As Boolean = False
            If mdtAllocation.Rows.Count <= 0 Then
                mdtAllocation.Rows.Add()
                blnNoRecord = True
            End If
            DirectCast(dgvAllocation.Columns(1), BoundField).DataField = StrDisColName
            dgvAllocation.DataKeyNames = New String() {StrIdColName, StrDisColName}
            dgvAllocation.DataSource = mdtAllocation
            dgvAllocation.DataBind()
            If blnNoRecord = True Then
                dgvAllocation.Rows(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_FormNo()
        Dim objRequisition As New clsStaffrequisition_Tran
        Dim dsList As DataSet
        Dim mdtRequisitionForm As DataTable
        Dim strFilter As String = ""
        Try
            If CInt(cboRequisitionType.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_tran.staffrequisitiontypeid = " & CInt(cboRequisitionType.SelectedValue) & " "
            End If

            If CInt(cboRequistionBy.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_tran.staffrequisitionbyid = " & CInt(cboRequistionBy.SelectedValue) & " "
            End If

            If CInt(cboJob.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_tran.jobunkid = " & CInt(cboJob.SelectedValue) & " "
            End If

            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            If CInt(cboStatus.SelectedValue) > 0 Then
                strFilter &= "AND rcstaffrequisition_tran.form_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If
            'Hemant (01 Nov 2021) -- End

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(4)

            dsList = objRequisition.GetList("List", strFilter, "")
            mdtRequisitionForm = dsList.Tables("List").Copy
            Dim dcol As New DataColumn
            With dcol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtRequisitionForm.Columns.Add(dcol)
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
            If mintStaffRequisitionTranunkid > 0 Then
                For Each drRow As DataRow In mdtRequisitionForm.Select("staffrequisitiontranunkid =" & mintStaffRequisitionTranunkid)
                    drRow("ischeck") = True
                Next
                mdtRequisitionForm.AcceptChanges()
            End If
            'Hemant (01 Nov 2021) -- End
            Dim blnNoRecord As Boolean = False
            If mdtRequisitionForm.Rows.Count <= 0 Then
                mdtRequisitionForm.Rows.Add()
                blnNoRecord = True
            End If
            dgvFormNo.DataSource = mdtRequisitionForm
            dgvFormNo.DataBind()
            If blnNoRecord = True Then
                dgvFormNo.Rows(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequisition = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            Call SetDateFormat()

            objRequisitionForm.generateReportNew(CStr(Session("Database_Name")), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             CStr(Session("UserAccessModeSetting")), True, CStr(Session("ExportReportPath")), False, 0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objRequisitionForm._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
            Else
                'Hemant (01 Nov 2021) -- End
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If 'Hemant (01 Nov 2021)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboRequistionBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRequistionBy.SelectedIndexChanged
        Try
            Call Fill_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRequisitionType.SelectedIndexChanged, cboRequistionBy.SelectedIndexChanged, cboJob.SelectedIndexChanged, _
                                                                                                         cboStatus.SelectedIndexChanged

        'Hemant (01 Nov 2021) -- [cboStatus.SelectedIndexChanged]
        Try
            Call Fill_FormNo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)

            Me.lblWorkStartDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblWorkStartDateTo.ID, Me.lblWorkStartDateTo.Text)
            Me.lblWorkStartDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblWorkStartDateFrom.ID, Me.lblWorkStartDateFrom.Text)
            Me.lblPositionTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPositionTo.ID, Me.lblPositionTo.Text)
            Me.lblPositionFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPositionFrom.ID, Me.lblPositionFrom.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblJob.ID, Me.lblJob.Text)
            Me.lblRequisitionBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRequisitionBy.ID, Me.lblRequisitionBy.Text)
            Me.lblRequisitiontype.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRequisitiontype.ID, Me.lblRequisitiontype.Text)

            Me.dgvFormNo.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvFormNo.Columns(1).FooterText, Me.dgvFormNo.Columns(1).HeaderText).Replace("&", "")
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            'Hemant (01 Nov 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
