﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_PlannedLeave_SummaryReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmPlannedLeave_SummaryReport"
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet


            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = -1
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing

            Dim objLeave As New clsleavetype_master
            dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            With cboLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeave = Nothing

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpStartdate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter(ByRef objPlannedLeaveSummary As clsPlannedLeave_SummaryReport) As Boolean
        Try
            objPlannedLeaveSummary.SetDefaultValue()

            If Not dtpStartdate.IsNull Then
                objPlannedLeaveSummary._FromDate = dtpStartdate.GetDate.Date
            Else
                objPlannedLeaveSummary._FromDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            If Not dtpToDate.IsNull Then
                objPlannedLeaveSummary._ToDate = dtpToDate.GetDate.Date
            Else
                objPlannedLeaveSummary._ToDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
            objPlannedLeaveSummary._EmployeeID = CInt(cboEmployee.SelectedValue)
            objPlannedLeaveSummary._EmployeeName = cboEmployee.SelectedItem.Text
            objPlannedLeaveSummary._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objPlannedLeaveSummary._LeaveTypeName = cboLeaveType.SelectedItem.Text
            objPlannedLeaveSummary._ViewByIds = mstrViewByIds
            objPlannedLeaveSummary._ViewIndex = mintViewIndex
            objPlannedLeaveSummary._ViewByName = mstrViewByName
            objPlannedLeaveSummary._Analysis_Fields = mstrAnalysisFields
            objPlannedLeaveSummary._Analysis_Join = mstrAnalysisJoin
            objPlannedLeaveSummary._Analysis_OrderBy = mstrAnalysisOrderBy
            objPlannedLeaveSummary._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objPlannedLeaveSummary._Report_GroupName = mstrReportGroupName
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If Not IsPostBack Then
                SetLanguage()
                Call FillCombo()
                dtpStartdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            Else
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        Dim objPlannedLeaveSummary As New clsPlannedLeave_SummaryReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If SetFilter(objPlannedLeaveSummary) = False Then Exit Sub

            objPlannedLeaveSummary._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objPlannedLeaveSummary._OpenAfterExport = False
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            objPlannedLeaveSummary._CompanyUnkId = CInt(Session("CompanyUnkId"))

            objPlannedLeaveSummary._UserUnkId = CInt(Session("UserId"))
            objPlannedLeaveSummary._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            objPlannedLeaveSummary.setDefaultOrderBy(0)
            SetDateFormat()
            If CInt(Session("Employeeunkid")) > 0 Then
                objPlannedLeaveSummary._UserName = Session("DisplayName").ToString()
            End If
            objPlannedLeaveSummary.Generate_DetailReport(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString, _
                                                  Session("UserAccessModeSetting").ToString, True)

            If objPlannedLeaveSummary._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objPlannedLeaveSummary._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPlannedLeaveSummary = Nothing
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysisFields = popAnalysisby._Analysis_Fields
            mstrAnalysisJoin = popAnalysisby._Analysis_Join
            mstrAnalysisOrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysisOrderByGName = popAnalysisby._Analysis_OrderBy_GName
            mstrReportGroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " LinkButton Event "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
