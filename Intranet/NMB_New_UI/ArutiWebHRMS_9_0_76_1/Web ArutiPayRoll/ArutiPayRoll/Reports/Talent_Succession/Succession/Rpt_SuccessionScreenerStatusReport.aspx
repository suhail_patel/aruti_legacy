﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_SuccessionScreenerStatusReport.aspx.vb"
    Inherits="Reports_Talent_Succession_Rpt_SuccessionScreenerStatusReport" Title="Succession Screener Status Report" %>

<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">


          
            $("body").on("click", "[id*=chkSelectAll]", function() {    
                var chkHeader = $(this);
                var grid = $(this).closest(".body");
                $("[id*=chkSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=chkSelect]", function() {   
                var grid = $(this).closest(".body");
                var chkHeader = $("[id*=chkAllSelect]", grid);
                debugger;
                if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearchEmployee').val().length > 0) {
                    $('#<%= dgvEmployee.ClientID %> tbody tr').hide();
                    $('#<%= dgvEmployee.ClientID %> tbody tr:first').show();
                    $('#<%= dgvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmployee').val() + '\')').parent().show();
                }
                else if ($('#txtSearchEmployee').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= dgvEmployee.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearchEmployee').val('');
                $('#<%= dgvEmployee.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchEmployee').focus();
            }


    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            <uc9:Export runat="server" ID="Export" />
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="Label1" runat="server" Text="Succession Screener Status Report" CssClass="form-label"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                    <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label></label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="txtSearchEmployee" name="txtSearch" placeholder="Type To Search Text"
                                                maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 315px;">
                                        <asp:GridView ID="dgvEmployee" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered"
                                            RowStyle-Wrap="false" DataKeyNames="employeeunkid">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" Text=" " CssClass="chk-sm" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                    Visible="false"></asp:BoundField>
                                                <asp:BoundField DataField="employeecode" HeaderText="Emp. Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhEmpCode"></asp:BoundField>
                                                <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhEmployee">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle Font-Bold="False" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">
    var scroll = {
        Y: '#<%= hfScrollPosition.ClientID %>'
    };
   
    function pageLoad(sender, args) {
        $("select").searchable();

    }

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            
    }
    }
    
    </script>

</asp:Content>
