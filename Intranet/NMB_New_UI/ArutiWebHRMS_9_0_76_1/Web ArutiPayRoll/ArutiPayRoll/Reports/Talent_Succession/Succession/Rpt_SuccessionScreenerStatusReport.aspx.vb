﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Talent_Succession_Rpt_SuccessionScreenerStatusReport
    Inherits Basepage
#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmSuccessionScreenerStatusReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        Dim objJob As New clsJobs
        Dim dsCombo As DataSet
        Try

            dsCombo = objJob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With cboJob
                .DataValueField = "Jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("keyJob")
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objJob = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboJob.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillEmpList()
        Dim dtTable As DataTable
        Dim objsucpipeline_master As New clssucpipeline_master
        Dim objJob As New clsJobs
        Dim objSuccessionScreenerStatusReport As New clsSuccessionScreenerStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Dim isblank As Boolean
        Try

            Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL
            objSuccessionScreenerStatusReport._JobUnkid = CInt(cboJob.SelectedValue)
            objSuccessionScreenerStatusReport._Advance_Filter = mstrAdvanceFilter
            objsucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime
            Dim dsList As DataSet = objsucpipeline_master.GetAllPipeLineEmployeeList(Session("Database_Name").ToString, _
                                                                             CInt(Session("UserId")), _
                                                                             CInt(Session("Fin_year")), _
                                                                             CInt(Session("CompanyUnkId")), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             Session("UserAccessModeSetting").ToString, _
                                                                             True, CBool(Session("IsIncludeInactiveEmp")), CInt(cboJob.SelectedValue), _
                                                                             "", -1, FilterType, "Emp", _
                                                                             False, "", True)
            Dim dtmain As New DataTable
            Dim strcolname As String() = New String() {"employeecode", "employeename", "employeeunkid", "empcodename", "job_name", "department"}
            dtmain = dsList.Tables(0).DefaultView.ToTable(True, strcolname)

            dtTable = New DataView(dtmain, "", "employeename", DataViewRowState.CurrentRows).ToTable


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmpList", mstrModuleName)
        Finally
            objJob = Nothing
            objSuccessionScreenerStatusReport = Nothing

            If dtTable.Rows.Count <= 0 Then

                Dim r As DataRow = dtTable.NewRow

                r.Item(1) = "None" ' To Hide the row and display only Row Header
                r.Item(2) = "0"

                dtTable.Rows.Add(r)
                isblank = True
            End If

            dgvEmployee.DataSource = dtTable
            dgvEmployee.DataBind()
            If isblank = True Then
                dgvEmployee.Rows(0).Visible = False
            End If
        End Try
    End Sub

    Public Function SetFilter(ByVal objSuccessionScreenerStatusReport As clsSuccessionScreenerStatusReport) As Boolean
        Try
            objSuccessionScreenerStatusReport.SetDefaultValue()

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelect"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objSuccessionScreenerStatusReport._EmployeeIds = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
            End If

            objSuccessionScreenerStatusReport._JobUnkid = CInt(cboJob.SelectedValue)
            objSuccessionScreenerStatusReport._JobName = cboJob.SelectedItem.Text

            objSuccessionScreenerStatusReport._ViewByIds = mstrStringIds
            objSuccessionScreenerStatusReport._ViewIndex = mintViewIdx
            objSuccessionScreenerStatusReport._ViewByName = mstrStringName
            objSuccessionScreenerStatusReport._Analysis_Fields = mstrAnalysis_Fields
            objSuccessionScreenerStatusReport._Analysis_Join = mstrAnalysis_Join
            objSuccessionScreenerStatusReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objSuccessionScreenerStatusReport._Report_GroupName = mstrReport_GroupName

            objSuccessionScreenerStatusReport._Advance_Filter = mstrAdvanceFilter
            objSuccessionScreenerStatusReport._DateAsOn = ConfigParameter._Object._CurrentDateAndTime
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Combobox's Events "
    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Try
            If CInt(cboJob.SelectedValue) > 0 Then
                Call FillEmpList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objSuccessionScreenerStatusReport As New clsSuccessionScreenerStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If SetFilter(objSuccessionScreenerStatusReport) = False Then Exit Sub



            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            objSuccessionScreenerStatusReport.Generate_DetailReport(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                Session("UserAccessModeSetting").ToString, _
                                                                True, False, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                False, 0, enExportAction.None)

            If objSuccessionScreenerStatusReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objSuccessionScreenerStatusReport._FileNameAfterExported

                Export.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
End Class
