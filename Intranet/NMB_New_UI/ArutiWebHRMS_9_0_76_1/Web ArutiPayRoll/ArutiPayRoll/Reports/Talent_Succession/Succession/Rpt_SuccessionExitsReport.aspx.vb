﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports

Partial Class Reports_Talent_Succession_Succession_Rpt_SuccessionExitsReport
    Inherits Basepage

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objSuccessionExitsReport = New clsSuccessionExitsReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call FillCombo()
                'Call ResetValue()
                Me.ViewState.Add("StringIds", "")
                Me.ViewState.Add("StringName", "")
                Me.ViewState.Add("ViewIdx", -1)
                Me.ViewState.Add("Analysis_Fields", "")
                Me.ViewState.Add("Analysis_Join", "")
                Me.ViewState.Add("Analysis_OrderBy", "")
                Me.ViewState.Add("Report_GroupName", "")
            Else
                If IsNothing(ViewState("mstrAdvanceFilter")) = False Then
                    mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objSuccessionExitsReport As clsSuccessionExitsReport
    Private ReadOnly mstrModuleName As String = "frmSuccessionExitsReport"

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""


#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objJob As New clsJobs

        Try

            dsCombo = objJob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With cboJob
                .DataValueField = "Jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("keyJob")
                .SelectedValue = "0"
                .DataBind()
            End With


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboJob.SelectedIndex = 0
            If CInt(cboJob.SelectedValue) > 0 Then
                cboEmployee.SelectedValue = "0"
            End If
            dtFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

            Me.ViewState("StringIds") = ""
            Me.ViewState("ViewIdx") = -1
            Me.ViewState("StringName") = ""
            Me.ViewState("Analysis_Fields") = ""
            Me.ViewState("Analysis_Join") = ""
            Me.ViewState("Analysis_OrderBy") = ""
            Me.ViewState("Report_GroupName") = ""

            Me.ViewState("mstrAdvanceFilter") = ""
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            objSuccessionExitsReport.SetDefaultValue()

            If dtFromDate.IsNull = False AndAlso dtToDate.IsNull = False Then
                objSuccessionExitsReport._ExitDateFrom = dtFromDate.GetDate.Date
                objSuccessionExitsReport._ExitDateTo = dtToDate.GetDate.Date
            End If

            objSuccessionExitsReport._JobUnkid = CInt(cboJob.SelectedValue)
            objSuccessionExitsReport._JobName = cboJob.Text

            If CInt(cboJob.SelectedValue) > 0 Then
            objSuccessionExitsReport._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objSuccessionExitsReport._EmployeeName = cboEmployee.Text
            End If


            objSuccessionExitsReport._ViewByIds = mstrStringIds
            objSuccessionExitsReport._ViewIndex = mintViewIdx
            objSuccessionExitsReport._ViewByName = mstrStringName
            objSuccessionExitsReport._Analysis_Fields = mstrAnalysis_Fields
            objSuccessionExitsReport._Analysis_Join = mstrAnalysis_Join
            objSuccessionExitsReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objSuccessionExitsReport._Report_GroupName = mstrReport_GroupName
            objSuccessionExitsReport._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objSuccessionExitsReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objSuccessionExitsReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                Session("UserAccessModeSetting").ToString(), True, ExportReportPath, _
                                                                OpenAfterExport)


            If objSuccessionExitsReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objSuccessionExitsReport._FileNameAfterExported
                Export.Show()
            End If


            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Control Event"

    'Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
    '    Try
    '        popupAnalysisBy.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("lnkAnalysisBy_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            Me.ViewState("StringIds") = popupAnalysisBy._ReportBy_Ids
            Me.ViewState("StringName") = popupAnalysisBy._ReportBy_Name
            Me.ViewState("ViewIdx") = popupAnalysisBy._ViewIndex
            Me.ViewState("Analysis_Fields") = popupAnalysisBy._Analysis_Fields
            Me.ViewState("Analysis_Join") = popupAnalysisBy._Analysis_Join
            Me.ViewState("Analysis_OrderBy") = popupAnalysisBy._Analysis_OrderBy
            Me.ViewState("Report_GroupName") = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Dim objsucpipeline_master As New clssucpipeline_master
        Dim objStage As New clssucstages_master
        Dim dsList As New DataSet
        Dim dsCombo As New DataSet
        Try
            dsList = objStage.GetList("list")
            Dim intmaxStage As Integer = -1
            Dim intminStage As Integer = -1
            Dim intmaxToLastStage As Integer = -1
            Dim mintStageUnkid As Integer = -1
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                objStage.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)
                Dim drRow As DataRow = dsList.Tables(0).Select("floworder = " & intmaxStage & "")(0)
                mintStageUnkid = CInt(drRow.Item("stageunkid"))
            End If

            Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL

            dsCombo = objsucpipeline_master.GetOtherStagesDetailList(CStr(Session("Database_Name")), _
                          CInt(Session("UserId")), _
                          CInt(Session("Fin_year")), _
                          CInt(Session("CompanyUnkId")), _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          CStr(Session("UserAccessModeSetting")), True, _
                          CBool(Session("IsIncludeInactiveEmp")), CInt(cboJob.SelectedValue), _
                          mintStageUnkid, "", intmaxStage, -1, FilterType, "Emp", False, mstrAdvanceFilter, True)


            Dim RemoveColumns As String() = {"isapproved", "age", "exyr", "TotalScreener", "stageunkid", "processmstunkid", "empImage", _
                                             "empbaseimage", "isdisapproved", "IsDone", "nominatedjobunkid", "nominatedjob", "color", _
                                             "ismatch", "ismanual", "potentialSuccessiontranunkid"}

            For Each colName As String In RemoveColumns
                dsCombo.Tables(0).Columns.Remove(colName)
            Next

            Dim dt As DataTable = dsCombo.Tables(0).DefaultView.ToTable(True, dsCombo.Tables(0).Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray())

            Dim r As DataRow = dt.NewRow

            r.Item(1) = "Select" ' 
            r.Item(2) = "0"

            dt.Rows.InsertAt(r, 0)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dt
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

 
End Class
