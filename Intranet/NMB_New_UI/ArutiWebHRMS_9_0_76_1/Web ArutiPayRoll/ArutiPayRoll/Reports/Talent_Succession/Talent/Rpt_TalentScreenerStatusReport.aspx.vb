﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports ArutiReports

Partial Class Reports_Talent_Succession_Rpt_TalentScreenerStatusReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTalentScreenerStatusReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        Dim obCycle As New clstlcycle_master
        Dim dsCombo As DataSet
        Try

            dsCombo = obCycle.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)
            With cboCycle
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            obCycle = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCycle.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            dgvEmployee.DataSource = Nothing
            dgvEmployee.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillEmpList()
        Dim dtTable As DataTable
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim objCycle As New clstlcycle_master
        Dim objTalentScreenerStatusReport As New clsTalentScreenerStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Dim isblank As Boolean
        Try

            Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL
            objTalentScreenerStatusReport._CycleUnkid = CInt(cboCycle.SelectedValue)
            objTalentScreenerStatusReport._Advance_Filter = mstrAdvanceFilter

            objCycle._Cycleunkid = CInt(cboCycle.SelectedValue)
            objtlpipeline_master._DateAsOn = CDate(objCycle._Start_Date)
            Dim dsList As DataSet = objtlpipeline_master.GetAllPipeLineEmployeeList(Session("Database_Name").ToString, _
                                                                             CInt(Session("UserId")), _
                                                                             CInt(Session("Fin_year")), _
                                                                             CInt(Session("CompanyUnkId")), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             Session("UserAccessModeSetting").ToString, _
                                                                             True, CBool(Session("IsIncludeInactiveEmp")), CInt(cboCycle.SelectedValue), _
                                                                             "", -1, FilterType, "Emp", _
                                                                             False, "", True)

            dtTable = New DataView(dsList.Tables("Emp"), "", "employeename", DataViewRowState.CurrentRows).ToTable


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmpList", mstrModuleName)
        Finally
            objCycle = Nothing
            objTalentScreenerStatusReport = Nothing
            objtlpipeline_master = Nothing
            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item(1) = "None" ' To Hide the row and display only Row Header
                r.Item(2) = "0"

                dtTable.Rows.Add(r)
                isblank = True
            End If

            dgvEmployee.DataSource = dtTable
            dgvEmployee.DataBind()
            If isblank = True Then
                dgvEmployee.Rows(0).Visible = False
            End If
        End Try
    End Sub

    Public Function SetFilter(ByVal objTalentScreenerStatusReport As clsTalentScreenerStatusReport) As Boolean
        Dim objCycle As New clstlcycle_master
        Try
            If CInt(cboCycle.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Select Cycle."), Me)
                cboCycle.Focus()
                Exit Function
            End If

            objTalentScreenerStatusReport.SetDefaultValue()

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelect"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objTalentScreenerStatusReport._EmployeeIds = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
            End If

            objTalentScreenerStatusReport._CycleUnkid = CInt(cboCycle.SelectedValue)
            objTalentScreenerStatusReport._CycleName = cboCycle.SelectedItem.Text

            objTalentScreenerStatusReport._ViewByIds = mstrStringIds
            objTalentScreenerStatusReport._ViewIndex = mintViewIdx
            objTalentScreenerStatusReport._ViewByName = mstrStringName
            objTalentScreenerStatusReport._Analysis_Fields = mstrAnalysis_Fields
            objTalentScreenerStatusReport._Analysis_Join = mstrAnalysis_Join
            objTalentScreenerStatusReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTalentScreenerStatusReport._Report_GroupName = mstrReport_GroupName

            objCycle._Cycleunkid = CInt(cboCycle.SelectedValue)
            objTalentScreenerStatusReport._DateAsOn = CDate(objCycle._Start_Date)

            objTalentScreenerStatusReport._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
            objCycle = Nothing
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Combobox's Events "
    Private Sub cboCycle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCycle.SelectedIndexChanged
        Try
            If CInt(cboCycle.SelectedValue) > 0 Then
                Call FillEmpList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objTalentScreenerStatusReport As New clsTalentScreenerStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(objTalentScreenerStatusReport) = False Then Exit Sub

            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            objTalentScreenerStatusReport.Generate_DetailReport(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                Session("UserAccessModeSetting").ToString, _
                                                                True, False, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                False, 0, enExportAction.None)

            If objTalentScreenerStatusReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objTalentScreenerStatusReport._FileNameAfterExported

                Export.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
End Class
