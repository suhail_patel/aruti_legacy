﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Rpt_Employee_Listing
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmpList As clsEmployee_Listing


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployee_Listing"
    'Pinkal (06-May-2014) -- End

    'Hemant (28 Apr 2020) -- Start
    'Enhancement - Analysis By on Employee Listing Report
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Hemant (28 Apr 2020) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objEmpType As New clsCommon_Master
            Dim dsList As New DataSet

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            Dim oPeriod As New clscommom_period_Tran
            'S.SANDEEP |15-JUL-2019| -- END


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)
            'Shani(24-Aug-2015) -- End

            With drpemployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With
            objEmp = Nothing

            dsList = objEmpType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            With drpEmployeementType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            objEmpType = Nothing
            dsList = Nothing

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            dsList = oPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate").ToString).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            cboPeriod.Enabled = False
            'S.SANDEEP |15-JUL-2019| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpList.SetDefaultValue()

            objEmpList._EmployeeId = drpemployee.SelectedValue
            objEmpList._EmployeeName = drpemployee.SelectedItem.Text

            objEmpList._EmployeementTypeId = drpEmployeementType.SelectedValue
            objEmpList._EmployeementType = drpEmployeementType.SelectedItem.Text

            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpList._UserAccessFilter = Session("AccessLevelFilterString")
            'Shani(24-Aug-2015) -- End
            'S.SANDEEP [ 12 NOV 2012 ] -- END

            objEmpList._IsActive = chkInactiveemp.Checked


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If Not dtpBirthdate.IsNull Then
                objEmpList._BirthDateFrom = dtpBirthdate.GetDate.Date
            End If

            If Not dtpBirthdateTo.IsNull Then
                objEmpList._BirthDateTo = dtpBirthdateTo.GetDate.Date
            End If

            If Not dtpAnniversaryDateFrom.IsNull Then
                objEmpList._AnniversaryDateFrom = dtpAnniversaryDateFrom.GetDate.Date
            End If

            If Not dtpAnniversaryDateTo.IsNull Then
                objEmpList._AnniversaryDateTo = dtpAnniversaryDateTo.GetDate.Date
            End If

            If ConfigParameter._Object._ShowFirstAppointmentDate Then

                If Not dtpFAppointDateFrom.IsNull Then
                    objEmpList._FirstAppointedDateFrom = dtpFAppointDateFrom.GetDate.Date
                End If

                If Not dtpFAppointDateTo.IsNull Then
                    objEmpList._FirstAppointedDateTo = dtpFAppointDateTo.GetDate.Date
                End If

            End If

            If Not dtpAppointDateFrom.IsNull Then
                objEmpList._AppointedDateFrom = dtpAppointDateFrom.GetDate.Date
            End If

            If Not dtpAppointDateTo.IsNull Then
                objEmpList._AppointedDateTo = dtpAppointDateTo.GetDate.Date
            End If

            If Not dtpConfirmationDateFrom.IsNull Then
                objEmpList._ConfirmationDateFrom = dtpConfirmationDateFrom.GetDate.Date
            End If

            If Not dtpConfirmationDateTo.IsNull Then
                objEmpList._ConfirmationDateTo = dtpConfirmationDateTo.GetDate.Date
            End If

            If Not dtpProbationStartDateFrom.IsNull Then
                objEmpList._ProbationStartDateFrom = dtpProbationStartDateFrom.GetDate.Date
            End If

            If Not dtpProbationStartDateTo.IsNull Then
                objEmpList._ProbationStartDateTo = dtpProbationStartDateTo.GetDate.Date
            End If

            If Not dtpProbationEndDateFrom.IsNull Then
                objEmpList._ProbationEndDateFrom = dtpProbationEndDateFrom.GetDate.Date
            End If

            If Not dtpProbationEndDateTo.IsNull Then
                objEmpList._ProbationEndDateTo = dtpProbationEndDateTo.GetDate.Date
            End If

            If Not dtpSuspendedStartDateFrom.IsNull Then
                objEmpList._SuspendedStartDateFrom = dtpSuspendedStartDateFrom.GetDate.Date
            End If

            If Not dtpSuspendedStartDateTo.IsNull Then
                objEmpList._SuspendedStartDateTo = dtpSuspendedStartDateTo.GetDate.Date
            End If

            If Not dtpSuspendedEndDateFrom.IsNull Then
                objEmpList._SuspendedEndDateFrom = dtpSuspendedEndDateFrom.GetDate.Date
            End If

            If Not dtpSuspendedEndDateTo.IsNull Then
                objEmpList._SuspendedEndDateTo = dtpSuspendedEndDateTo.GetDate.Date
            End If

            If Not dtpReinstatementFrom.IsNull Then
                objEmpList._ReinstatementDateFrom = dtpReinstatementFrom.GetDate.Date
            End If

            If Not dtpReinstatementTo.IsNull Then
                objEmpList._ReinstatementDateTo = dtpReinstatementTo.GetDate.Date
            End If

            If Not dtpEOCDateFrom.IsNull Then
                objEmpList._EOCDateFrom = dtpEOCDateFrom.GetDate.Date
            End If

            If Not dtpEOCDateTo.IsNull Then
                objEmpList._EOCDateTo = dtpEOCDateTo.GetDate.Date
            End If

            objEmpList._ShowEmployeeScale = chkShowEmpScale.Checked

            'Pinkal (24-Apr-2013) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmpList._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            objEmpList._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpList._PeriodName = cboPeriod.SelectedItem.Text
            'S.SANDEEP |15-JUL-2019| -- END

            'Hemant (28 Apr 2020) -- Start
            'Enhancement - Analysis By on Employee Listing Report
            objEmpList._ViewByIds = mstrStringIds
            objEmpList._ViewIndex = mintViewIdx
            objEmpList._ViewByName = mstrStringName
            objEmpList._Analysis_Fields = mstrAnalysis_Fields
            objEmpList._Analysis_Join = mstrAnalysis_Join
            objEmpList._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpList._Report_GroupName = mstrReport_GroupName
            'Hemant (28 Apr 2020) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            drpemployee.SelectedValue = 0
            drpEmployeementType.SelectedValue = 0
            chkInactiveemp.Checked = False


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes

            dtpBirthdate.SetDate = Nothing
            dtpBirthdateTo.SetDate = Nothing
            dtpAnniversaryDateFrom.SetDate = Nothing
            dtpAnniversaryDateTo.SetDate = Nothing
            dtpFAppointDateFrom.SetDate = Nothing
            dtpFAppointDateTo.SetDate = Nothing
            dtpAppointDateFrom.SetDate = Nothing
            dtpAppointDateTo.SetDate = Nothing
            dtpConfirmationDateFrom.SetDate = Nothing
            dtpConfirmationDateTo.SetDate = Nothing
            dtpProbationStartDateFrom.SetDate = Nothing
            dtpProbationStartDateTo.SetDate = Nothing
            dtpProbationEndDateFrom.SetDate = Nothing
            dtpProbationEndDateTo.SetDate = Nothing
            dtpSuspendedStartDateFrom.SetDate = Nothing
            dtpSuspendedStartDateTo.SetDate = Nothing
            dtpSuspendedEndDateFrom.SetDate = Nothing
            dtpSuspendedEndDateTo.SetDate = Nothing
            dtpReinstatementFrom.SetDate = Nothing
            dtpReinstatementTo.SetDate = Nothing
            dtpEOCDateFrom.SetDate = Nothing
            dtpEOCDateTo.SetDate = Nothing
            chkShowEmpScale.Checked = False

            'Pinkal (24-Apr-2013) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder ' 
        Dim blnFlag As Boolean = False
        Try

            ConfigParameter._Object._Companyunkid = Session("CompanyUnkId")
            Company._Object._Companyunkid = Session("CompanyUnkId")
            Aruti.Data.User._Object._Userunkid = Session("UserId")

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'strBuilder.Append(" <TITLE> " & Closebotton1.PageHeading & " " & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <TITLE> " & lblPageHeader.Text & " " & "</TITLE> " & vbCrLf)
            'Nilay (01-Feb-2015) -- End
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(enArutiReport.Employee_Listing))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & "Prepared By :" & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'strBuilder.Append(Aruti.Data.User._Object._Username & vbCrLf)
            strBuilder.Append(Session("UserName") & vbCrLf)
            'Shani(24-Aug-2015) -- End
            strBuilder.Append(" </FONT></TD> " & vbCrLf)

            strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 4 & " align='center' > " & vbCrLf)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Session("CompName") & " </B></FONT> " & vbCrLf)
            'Shani(24-Aug-2015) -- End
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & "Date :" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)

            strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 4 & "  align='center' > " & vbCrLf)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'strBuilder.Append(" <FONT SIZE=3><b> " & Closebotton1.PageHeading & " " & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & lblPageHeader.Text & " " & "</B></FONT> " & vbCrLf)
            'Nilay (01-Feb-2015) -- End
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            'strBuilder.Append(" <B> " & objEmpList._FilterStr & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            For j As Integer = 0 To objDataReader.Columns.Count - 1

                'Pinkal (24-Apr-2013) -- Start
                'Enhancement : TRA Changes
                If objDataReader.Columns(j).ColumnName = "employeeunkid" Or objDataReader.Columns(j).ColumnName = "Id" Or objDataReader.Columns(j).ColumnName = "GName" Then Continue For
                'Pinkal (24-Apr-2013) -- End

                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)


                'Pinkal (24-Apr-2013) -- Start
                'Enhancement : TRA Changes

                'For k As Integer = 0 To objDataReader.Columns.Count - 2
                '    If CBool(objDataReader.Rows(i)("IsGrp")) = True Then
                '        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B> &nbsp;" & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                '    Else
                '        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                '    End If
                'Next

                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If objDataReader.Columns(k).ColumnName = "employeeunkid" Or objDataReader.Columns(k).ColumnName = "Id" Or objDataReader.Columns(k).ColumnName = "GName" Then Continue For
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                Next

                'Pinkal (24-Apr-2013) -- End


                strBuilder.Append(" </TR> " & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            'Pinkal (10-Sep-2012) -- Start
            'Enhancement : TRA Changes

            'If SaveExcelfile(Server.MapPath("~/images/" & flFileName), strBuilder) Then
            '    Session("ExFileName") = Server.MapPath("~/images/" & flFileName)

            Dim StrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            If StrPath.Trim.Length > 0 Then
                SaveExcelfile(StrPath.Trim & "\" & flFileName, strBuilder)
                Session("ExFileName") = StrPath.Trim & "\" & flFileName

                'Shani [ 18 NOV 2015 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

                'Shani [ 18 NOV 2014 ] -- END
                Return True
            Else
                Return False
            End If

            'Pinkal (10-Sep-2012) -- End

            Return blnFlag

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Export_to_Excel:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SaveExcelfile:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objEmpList = New clsEmployee_Listing(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                Call FillCombo()


                'Pinkal (24-Apr-2013) -- Start
                'Enhancement : TRA Changes

                LblFirstAppointDateFrom.Enabled = CBool(Session("ShowFirstAppointmentDate"))
                LblFirstAppointDateTo.Enabled = CBool(Session("ShowFirstAppointmentDate"))
                dtpFAppointDateFrom.Enabled = CBool(Session("ShowFirstAppointmentDate"))
                dtpFAppointDateTo.Enabled = CBool(Session("ShowFirstAppointmentDate"))

                'Pinkal (24-Apr-2013) -- End


                'Pinkal (30-Apr-2013) -- Start
                'Enhancement : TRA Changes
                chkShowEmpScale.Visible = Session("AllowToViewScale")
                'Pinkal (30-Apr-2013) -- End

                'Hemant (28 Apr 2020) -- Start
                'Enhancement - Analysis By on Employee Listing Report
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                'Hemant (28 Apr 2020) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Hemant (28 Apr 2020) -- Start
    'Enhancement - Analysis By on Employee Listing Report
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mstrStringIds", mstrStringIds)
        Me.ViewState.Add("mstrStringName", mstrStringName)
        Me.ViewState.Add("mintViewIdx", mintViewIdx)
        Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
        Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
        Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
        Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
    End Sub
    'Hemant (28 Apr 2020) -- End


#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpList.Generate_DetailReport(True)


            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            'objEmpList.Generate_DetailReport(Session("Database_Name"), _
            '                                 Session("UserId"), _
            '                                 Session("Fin_year"), _
            '                                 Session("CompanyUnkId"), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 Session("UserAccessModeSetting"), True, _
            '                                 IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
            '                                 Session("OpenAfterExport"), False, _
            '                                 Session("ShowFirstAppointmentDate"))

            Dim mdtStartDate As Date = Nothing
            Dim mdtEndDate As Date = Nothing

            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date.Date
                mdtEndDate = objPrd._End_Date.Date
                objPrd = Nothing
            Else
                mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If


            objEmpList.Generate_DetailReport(Session("Database_Name"), _
                                             Session("UserId"), _
                                             Session("Fin_year"), _
                                             Session("CompanyUnkId"), _
                                             mdtStartDate, _
                                             mdtEndDate, _
                                             Session("UserAccessModeSetting"), True, _
                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                             False, False, _
                                             Session("ShowFirstAppointmentDate"))
            'S.SANDEEP |15-JUL-2019| -- END

            'Shani(24-Aug-2015) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'Dim dtTable As DataTable = objEmpList._dtData
            'Dim strFilePath As String = "Employee_Listing_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".xls"
            'Export_to_Excel(strFilePath, Session("ExportReportPath"), dtTable)

            If objEmpList._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmpList._FileNameAfterExported
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnExport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region

    'Hemant (28 Apr 2020) -- Start
    'Enhancement - Analysis By on Employee Listing Report
#Region " Other Controls Events "
    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (28 Apr 2020) -- End

    Protected Sub drpemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpemployee.DataBound
        Try
            If drpemployee.Items.Count > 0 Then
                For Each lstItem As ListItem In drpemployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                drpemployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |15-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
    Protected Sub chkGeneratePeriod_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If chkGeneratePeriod.Checked = True Then
                cboPeriod.Enabled = True
            Else
                cboPeriod.SelectedValue = 0
                cboPeriod.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |15-JUL-2019| -- END

    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End

            'Hemant (28 Apr 2020) -- Start
            'Enhancement - Analysis By on Employee Listing Report
            Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            'Hemant (28 Apr 2020) -- End

            Me.lblEmployeementType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployeementType.ID, Me.lblEmployeementType.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.LblBirthdateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblBirthdateFrom.ID, Me.LblBirthdateFrom.Text)
            Me.LblBirthdateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblBirthdateTo.ID, Me.LblBirthdateTo.Text)
            Me.LblAnniversaryFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAnniversaryFrom.ID, Me.LblAnniversaryFrom.Text)
            Me.LblAnniversaryDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAnniversaryDateTo.ID, Me.LblAnniversaryDateTo.Text)
            Me.LblConfirmationDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblConfirmationDateTo.ID, Me.LblConfirmationDateTo.Text)
            Me.LblConfirmationDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblConfirmationDateFrom.ID, Me.LblConfirmationDateFrom.Text)
            Me.LblSuspendedStartDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSuspendedStartDateTo.ID, Me.LblSuspendedStartDateTo.Text)
            Me.LblSuspendedStartDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSuspendedStartDateFrom.ID, Me.LblSuspendedStartDateFrom.Text)
            Me.LblProbationEndDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblProbationEndDateTo.ID, Me.LblProbationEndDateTo.Text)
            Me.LblProbationEndDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblProbationEndDateFrom.ID, Me.LblProbationEndDateFrom.Text)
            Me.LblProbationStartDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblProbationStartDateTo.ID, Me.LblProbationStartDateTo.Text)
            Me.LblProbationStartDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblProbationStartDateFrom.ID, Me.LblProbationStartDateFrom.Text)
            Me.LblAppointDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAppointDateTo.ID, Me.LblAppointDateTo.Text)
            Me.LblAppointDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAppointDateFrom.ID, Me.LblAppointDateFrom.Text)
            Me.LblSuspendedEndDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSuspendedEndDateTo.ID, Me.LblSuspendedEndDateTo.Text)
            Me.LblSuspendedEndDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSuspendedEndDateFrom.ID, Me.LblSuspendedEndDateFrom.Text)
            Me.LblEOCDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEOCDateTo.ID, Me.LblEOCDateTo.Text)
            Me.LblEOCDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEOCDateFrom.ID, Me.LblEOCDateFrom.Text)
            Me.LblReinstatementDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReinstatementDateTo.ID, Me.LblReinstatementDateTo.Text)
            Me.LblReinstatementDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReinstatementDateFrom.ID, Me.LblReinstatementDateFrom.Text)
            Me.LblFirstAppointDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFirstAppointDateTo.ID, Me.LblFirstAppointDateTo.Text)
            Me.LblFirstAppointDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFirstAppointDateFrom.ID, Me.LblFirstAppointDateFrom.Text)
            Me.chkShowEmpScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEmpScale.ID, Me.chkShowEmpScale.Text)

            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnExport.ID, Me.BtnExport.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


End Class
