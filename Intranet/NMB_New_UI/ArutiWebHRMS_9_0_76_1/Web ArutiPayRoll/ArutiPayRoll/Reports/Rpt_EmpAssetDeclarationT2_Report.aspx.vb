﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmpAssetDeclarationT2_Report
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes

    'Hemant (24 Dec 2018) -- Start
    'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
    'Private Shared ReadOnly mstrModuleName As String = "frmEmployeeAssetDeclarationT2"
    Private Shared ReadOnly mstrModuleName As String = "frmEmployeeAssetDeclarationReportT2"
    'Hemant (24 Dec 2018) -- End

    Private objclsAssetDeclarationT2 As clsAssetDeclarationT2
    Dim objAssetDeclare As New clsAssetdeclaration_masterT2
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
    'Hemant (03 May 2021) -- Start
    'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
    Private mdtFinStartDate As DateTime
    Private mdtFinEndDate As DateTime
    'Hemant (03 May 2021) -- End
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objAssetDeclare = New clsAssetdeclaration_masterT2

            If Not IsPostBack Then
                SetLanguage()
                'Hemant (24 Dec 2018) -- Start
                'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
                'Call Language._Object.SaveValue()
                Call SetControlCaptions()
                'Hemant (24 Dec 2018) -- End
                FillCombo()
                ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                'Hemant (03 May 2021) -- Start
                'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
                mdtFinStartDate = Me.ViewState("mdtFinStartDate")
                mdtFinEndDate = Me.ViewState("mdtFinEndDate")
                'Hemant (03 May 2021) -- End               
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            Me.ViewState.Add("mdtFinStartDate", mdtFinStartDate)
            Me.ViewState.Add("mdtFinEndDate", mdtFinEndDate)
            'Hemant (03 May 2021) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        'Hemant (03 May 2021) -- Start
        'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
        Dim objclsMasterData As New clsMasterData
        Dim dtFinYearList As DataTable
        'Hemant (03 May 2021) -- End

        Try


            objEmp = New clsEmployee_Master

            'Hemant (15 Nov 2019) -- Start
            'Dim blnIncludeInactiveEmp As Boolean = False
            Dim blnIncludeInactiveEmp As Boolean = True
            'Hemant (15 Nov 2019) -- End

            Dim dsList As DataSet = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               blnIncludeInactiveEmp, "Employee", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            Dim intCurrentADYear As Integer = 0
            If Session("AssetDeclarationFromDate").ToString = "" Then
                dsCombo = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))

            ElseIf eZeeDate.convertDate(Session("AssetDeclarationFromDate")) IsNot Nothing Then

                dsCombo = objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True)
                Dim StrYear As String
                Dim intYear As Integer
                If eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    intYear = eZeeDate.convertDate(Session("AssetDeclarationFromDate")).Year - CDate(Session("fin_startdate")).Year
                Else
                    intYear = -1
                End If
                StrYear = CDate(Session("fin_startdate")).AddYears(intYear).Year & "-" & CDate(Session("fin_enddate")).AddYears(intYear).Year
                Dim i As Integer = 0

                Do While i <= dsCombo.Tables(0).Rows.Count - 1
                    If dsCombo.Tables(0).Rows(i)("financialyear_name").ToString() = StrYear Then
                        intCurrentADYear = CInt(dsCombo.Tables(0).Rows(i)("yearunkid"))
                        Exit Do
                    Else
                        intCurrentADYear = 0
                    End If
                    i += 1
                Loop

            Else
                dsCombo = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
            End If

            dtFinYearList = dsCombo.Tables("Year").Copy

            With drpFinalcialYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dtFinYearList
                .DataBind()
                .SelectedValue = intCurrentADYear
            End With
            drpFinalcialYear_SelectedIndexChanged(Nothing, Nothing)
            'Hemant (03 May 2021) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            objclsMasterData = Nothing
            'Hemant (03 May 2021) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            FillCombo()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Employee to Continue"), Me)
                Exit Sub
            End If

            Dim objMaster As New clsMasterData
            Dim blnApplyAccessFilter As Boolean = True
            Dim dsAssetList As DataSet
            Dim dsEmplist As DataSet

            Dim dicDB As New Dictionary(Of String, String)


            Dim dicDBDate As New Dictionary(Of String, String)
            Dim dicDBYearId As New Dictionary(Of String, Integer)


            Dim ds As DataSet = objMaster.Get_Database_Year_List("List", True, CInt(Session("CompanyUnkId")))
            dicDB = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearName = p.Item("financialyear_name").ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.YearName)


            dicDBDate = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .Date = (p.Item("start_date").ToString & "|" & p.Item("end_date").ToString).ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.Date)
            dicDBYearId = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearId = CInt(p.Item("yearunkid"))}).ToDictionary(Function(x) x.DBName, Function(y) y.YearId)


            'dicDB.Add(Session("Database_Name").ToString, Session("FinancialYear_Name").ToString)
            'dicDBDate.Add(Session("Database_Name").ToString, (eZeeDate.convertDate(Session("fin_startdate").ToString) & "|" & eZeeDate.convertDate(Session("fin_enddate").ToString)))
            'dicDBYearId.Add(Session("Database_Name").ToString, CInt( Session("Fin_year")))

            dsAssetList = clsAssetdeclaration_masterT2.GetList(Session("UserId"), _
                                                            Session("CompanyUnkId"), _
                                                            Session("UserAccessModeSetting"), True, _
                                                            True, "List", _
                                                            dicDB, dicDBDate, dicDBYearId, , _
                                                            CInt(cboEmployee.SelectedValue), , , , , , "A.employeename", blnApplyAccessFilter)
            'Hemant (15 Nov 2019) -- [Session("IsIncludeInactiveEmp") --> True]

            'Gajanan (23 Nov 2018) -- Start
            If dsAssetList.Tables(0).Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Declaration has not been submited by this employee."), Me)
                Exit Sub
            End If
            'Gajanan (23 Nov 2018) -- End

            Dim objRptAssetDeclare As New ArutiReports.clsAssetDeclarationT2

            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            'dsEmplist = objAssetDeclare.GetAssetDeclarationidFromEmployee(cboEmployee.SelectedValue)
            dsEmplist = objAssetDeclare.GetAssetDeclarationidFromEmployee(cboEmployee.SelectedValue, mdtFinStartDate, mdtFinEndDate)

            If dsEmplist.Tables(0).Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Declaration has not been submited by this employee."), Me)
                Exit Sub
            End If
            'Hemant (03 May 2021) -- End
            objRptAssetDeclare._AssetDeclarationUnkid = CInt(dsEmplist.Tables(0).Rows(0).Item("assetdeclarationt2unkid"))
            objRptAssetDeclare._EmployeeUnkid = CInt(dsAssetList.Tables(0).Rows(0).Item("employeeunkid"))
            objRptAssetDeclare._EmployeeCode = dsAssetList.Tables(0).Rows(0).Item("noofemployment").ToString
            objRptAssetDeclare._EmployeeName = dsAssetList.Tables(0).Rows(0).Item("employeename").ToString
            objRptAssetDeclare._WorkStation = dsAssetList.Tables(0).Rows(0).Item("workstation").ToString
            objRptAssetDeclare._Title = dsAssetList.Tables(0).Rows(0).Item("position").ToString
            objRptAssetDeclare._CompanyUnkId = CInt(Session("CompanyUnkId"))


            objRptAssetDeclare._EmployeeDepartment = dsAssetList.Tables(0).Rows(0).Item("position").ToString
            objRptAssetDeclare._EmployeeDesignation = dsAssetList.Tables(0).Rows(0).Item("centerforwork").ToString
            objRptAssetDeclare._EmployeeTINno = dsAssetList.Tables(0).Rows(0).Item("Tin_no").ToString
            objRptAssetDeclare._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objRptAssetDeclare._FinancialYear = dsAssetList.Tables(0).Rows(0).Item("finyear_start").ToString


            objRptAssetDeclare._DatabaseName = dsAssetList.Tables(0).Rows(0).Item("databasename").ToString
            objAssetDeclare._DatabaseName = dsAssetList.Tables(0).Rows(0).Item("databasename").ToString

            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            'objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(dsEmplist.Tables(0).Rows(0).Item("employeeunkid"))
            objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(dsEmplist.Tables(0).Rows(0).Item("assetdeclarationt2unkid"))
            'Hemant (03 May 2021) -- End

            If objAssetDeclare._Isfinalsaved = True Then
                objRptAssetDeclare._NonOfficialDeclaration = ""
            Else
                objRptAssetDeclare._NonOfficialDeclaration = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Non Official Declaration")
            End If

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                objRptAssetDeclare.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
            Else
                objRptAssetDeclare.generateReportNew(Session("Database_Name").ToString(), 1, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
            End If
            Session("objRpt") = objRptAssetDeclare._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
    '    Try
    '        popupAnalysisBy.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("lnkSetAnalysis_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
    '    Try
    '        mstrStringIds = popupAnalysisBy._ReportBy_Ids
    '        mstrStringName = popupAnalysisBy._ReportBy_Name
    '        mintViewIdx = popupAnalysisBy._ViewIndex
    '        mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
    '        mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
    '        mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
    '        mstrReport_GroupName = popupAnalysisBy._Report_GroupName

    '        FillCombo()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("popupAnalysisBy_buttonApply_Click :- " & ex.Message, Me)
    '    End Try

    'End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
    '    Try
    '        mstrStringIds = ""
    '        mstrStringName = ""
    '        mintViewIdx = 0
    '        mstrAnalysis_Fields = ""
    '        mstrAnalysis_Join = ""
    '        mstrAnalysis_OrderBy = ""
    '        mstrReport_GroupName = ""
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("popupAnalysisBy_buttonClose_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

    'Hemant (03 May 2021) -- Start
    'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
#Region "Combox Events"
    Protected Sub drpFinalcialYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpFinalcialYear.SelectedIndexChanged
        Try
            If CInt(drpFinalcialYear.SelectedValue) = -99 Then
                mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(1)
                mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(1)

            ElseIf CInt(drpFinalcialYear.SelectedValue) = -98 Then
                mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
            ElseIf CInt(drpFinalcialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                Dim objclsMasterData As New clsMasterData
                For Each drRow As DataRow In objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True).Tables(0).Rows
                    If CInt(drpFinalcialYear.SelectedValue) = CInt(drRow.Item("yearunkid")) Then
                        mdtFinStartDate = CDate(eZeeDate.convertDate(drRow.Item("start_date"))).Date
                        mdtFinEndDate = CDate(eZeeDate.convertDate(drRow.Item("end_date"))).Date
                        Exit For
                    End If
                Next

            Else
                mdtFinStartDate = Session("Fin_StartDate")
                mdtFinEndDate = Session("Fin_EndDate")
            End If
        Catch ex As Exception

            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub
#End Region
    'Hemant (03 May 2021) -- End

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            'Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            'Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.gbFilterCriteria.Text)

            'Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lnkAdvanceFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Language._Object.setCaption("gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Hemant (17 Sep 2020) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReport.ID, Me.btnReport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Employee to Continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Declaration has not been submited by this employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Non Official Declaration")
            'Hemant (24 Dec 2018) -- End


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Hemant (17 Sep 2020) -- End
            Me.lnkAdvanceFilter.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetControlCaptions", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
End Class
