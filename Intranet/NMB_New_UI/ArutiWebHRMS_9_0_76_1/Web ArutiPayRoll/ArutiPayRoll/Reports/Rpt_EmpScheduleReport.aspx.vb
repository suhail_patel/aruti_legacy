﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.Globalization

#End Region

Partial Class Reports_Rpt_EmpScheduleReport
    Inherits Basepage


#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmpScheduleReport"
    Private objSchedule As clsEmpScheduleReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = ObjEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                            True, CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With


            Dim objDepartment As New clsDepartment
            dsList = objDepartment.getComboList("List", True)
            With cboDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objDepartment = Nothing


            Dim objSection As New clsSections
            dsList = objSection.getComboList("List", True)
            With cboSection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objSection = Nothing


            Dim objPrd As New clscommom_period_Tran
            dsList = objPrd.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            If CBool(Session("PolicyManagementTNA")) Then
                dsList = objPolicy.getListForCombo("List", True)
                With cboPolicy
                    .DataValueField = "policyunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                cboPolicy.Enabled = False
            End If

            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Shift Schedule Period Wise"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Shift Schedule Date Wise"))
                If CBool(Session("PolicyManagementTNA")) Then
                    .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Policy Schedule Period Wise"))
                    .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Policy Schedule Date Wise"))
                End If
                .SelectedIndex = 0
                cboReportType_SelectedIndexChanged(New Object(), New EventArgs())
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        Finally
            ObjEmp = Nothing : objShift = Nothing
            objPolicy = Nothing : objMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = "0"
            cboPeriod.SelectedValue = "0"
            cboPolicy.SelectedValue = "0"
            cboReportType.SelectedIndex = 0
            cboShift.SelectedValue = "0"
            mstrAdvanceFilter = ""
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            cboDepartment.SelectedValue = "0"
            cboSection.SelectedValue = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSchedule.SetDefaultValue()
            If CInt(cboReportType.SelectedIndex) = 0 OrElse CInt(cboReportType.SelectedIndex) = 2 Then  'SHIFT & POLICY PERIOD WISE
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                    cboPeriod.Focus()
                    Return False
                End If
            ElseIf CInt(cboReportType.SelectedIndex) = 1 OrElse CInt(cboReportType.SelectedIndex) = 3 Then   'SHIFT & POLICY DATE WISE

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003221 {PAPAYE}.
                'If CInt(cboDepartment.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Department is mandatory information. Please select department to continue."), Me)
                '    cboDepartment.Focus()
                '    Return False

                'ElseIf CInt(cboSection.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, Section is mandatory information. Please select section to continue."), Me)
                '    cboSection.Focus()
                '    Return False
                'End If
                If HttpContext.Current.Session("CompanyGroupName").ToString().ToUpper() <> "PAPAYE FAST FOODS LTD" Then
                    If CInt(cboDepartment.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Department is mandatory information. Please select department to continue."), Me)
                        cboDepartment.Focus()
                        Return False

                    ElseIf CInt(cboSection.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, Section is mandatory information. Please select section to continue."), Me)
                        cboSection.Focus()
                        Return False
                    End If
                End If
                objSchedule._CompanyGroupName = HttpContext.Current.Session("CompanyGroupName").ToString().ToUpper()
                'S.SANDEEP |29-MAR-2019| -- END

            End If

            objSchedule._Advance_Filter = mstrAdvanceFilter
            objSchedule._ViewByIds = mstrStringIds
            objSchedule._ViewIndex = mintViewIdx
            objSchedule._ViewByName = mstrStringName
            objSchedule._Analysis_Fields = mstrAnalysis_Fields
            objSchedule._Analysis_Join = mstrAnalysis_Join
            objSchedule._Report_GroupName = mstrReport_GroupName
            objSchedule._EmployeeId = CInt(cboEmployee.SelectedValue)
            objSchedule._EmployeeName = cboEmployee.SelectedItem.Text
            objSchedule._IncludeInactive = chkInactiveemp.Checked
            objSchedule._PeriodId = CInt(cboPeriod.SelectedValue)
            objSchedule._PeriodName = cboPeriod.SelectedItem.Text

            If CBool(Session("PolicyManagementTNA")) Then
                objSchedule._PolicyId = CInt(cboPolicy.SelectedValue)
                objSchedule._PolicyName = cboPolicy.SelectedItem.Text
            End If
            objSchedule._ReportTypeId = cboReportType.SelectedIndex
            objSchedule._ReportTypeName = cboReportType.SelectedItem.Text
            objSchedule._ShiftId = CInt(cboShift.SelectedValue)
            objSchedule._ShiftName = cboShift.SelectedItem.Text
            If CInt(cboReportType.SelectedIndex) = 1 OrElse CInt(cboReportType.SelectedIndex) = 3 Then
                objSchedule._FromDate = dtpFromDate.GetDate.Date
                objSchedule._ToDate = dtpToDate.GetDate.Date
            End If
            objSchedule._DepartmentId = CInt(cboDepartment.SelectedValue)
            objSchedule._Department = cboDepartment.SelectedItem.Text
            objSchedule._SectionId = CInt(cboSection.SelectedValue)
            objSchedule._Section = cboSection.SelectedItem.Text

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub EnableDisablePeriod(ByVal blnEnable As Boolean)
        Try
            Dim mdtDate As DateTime = ConfigParameter._Object._CurrentDateAndTime.Date
            cboPeriod.SelectedIndex = 0 : cboPeriod.Enabled = blnEnable
            dtpFromDate.SetDate = mdtDate : dtpToDate.SetDate = mdtDate
            dtpFromDate.Enabled = Not blnEnable : dtpToDate.Enabled = Not blnEnable
            cboDepartment.Enabled = Not blnEnable : cboDepartment.SelectedIndex = 0
            cboSection.Enabled = Not blnEnable : cboSection.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Page Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            objSchedule = New clsEmpScheduleReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call SetLanguage()
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName "))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button Event(S) "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003221 {PAPAYE}.
            objSchedule._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'S.SANDEEP |29-MAR-2019| -- END
            objSchedule.Generate_DetailReport(CStr(Session("Database_Name")), _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                CStr(Session("UserAccessModeSetting")), True)

            If objSchedule._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objSchedule._FileNameAfterExported
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                ' ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()
                'Gajanan [17-Sep-2020] -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrStringIds = popAnalysisby._ReportBy_Ids
            mstrStringName = popAnalysisby._ReportBy_Name
            mintViewIdx = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " ComboBox Events "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'SHIFT SCHEDULE PERIOD WISE
                    cboPolicy.Enabled = False : cboPolicy.SelectedValue = "0"
                    cboShift.Enabled = True : cboShift.SelectedValue = "0"
                    EnableDisablePeriod(True)

                Case 1  'SHIFT SCHEDULE DATE WISE
                    cboPolicy.Enabled = False : cboPolicy.SelectedValue = "0"
                    cboShift.Enabled = True : cboShift.SelectedValue = "0"
                    EnableDisablePeriod(False)

                Case 2  'POLICY SCHEDULE PERIOD WISE
                    cboPolicy.Enabled = True : cboPolicy.SelectedValue = "0"
                    cboShift.Enabled = False : cboShift.SelectedValue = "0"
                    EnableDisablePeriod(True)

                Case 3  'POLICY SCHEDULE DATE WISE
                    cboPolicy.Enabled = True : cboPolicy.SelectedValue = "0"
                    cboShift.Enabled = False : cboShift.SelectedValue = "0"
                    EnableDisablePeriod(False)

            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Datepicker Events"

    Private Sub dtpFromDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged
        Try
            dtpToDate.SetDate = dtpFromDate.GetDate.Date
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Language"

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lblPolicy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPolicy.ID, Me.lblPolicy.Text)
            Me.lblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblShift.ID, Me.lblShift.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblRType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRType.ID, Me.lblRType.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
            Me.LblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSection.ID, Me.LblSection.Text)
            Me.LblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDepartment.ID, Me.LblDepartment.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


End Class
