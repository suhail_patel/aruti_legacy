﻿<%@ Page Title="Appraisal Summary Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_AppraisalSummaryReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_AppraisalSummaryReport" %>

<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="uc" TagPrefix="numerictext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Appraisal Summary Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDepartment" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCommendableletters" runat="server" Text="Commendable Letters" CssClass="form-label"></asp:Label>
                                        <numerictext:uc ID="txtClFrom" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <numerictext:uc ID="txtClto" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblImprovment" runat="server" Text="Letter Of Improvement" CssClass="form-label"></asp:Label>
                                        <numerictext:uc ID="txtImpFrom" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblImpTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <numerictext:uc ID="txtImpTo" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblWarning" runat="server" Text="Warning" CssClass="form-label"></asp:Label>
                                        <numerictext:uc ID="txtWarningFrom" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblWarningTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <numerictext:uc ID="txtWarningTo" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
