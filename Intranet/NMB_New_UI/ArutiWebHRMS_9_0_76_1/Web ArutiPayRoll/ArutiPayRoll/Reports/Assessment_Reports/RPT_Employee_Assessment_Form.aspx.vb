﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_RPT_Employee_Assessment_Form
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentFormReport"
    Private DisplayMessage As New CommonCodes
    Private ObjAssesss_Form As clsAssessment_Form
    Private mDicPeriods As Dictionary(Of Integer, String)

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Emp")
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .DataBind()
                .SelectedValue = 0
            End With
            With cboFromPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .DataBind()
                .SelectedValue = 0
            End With

            With cboToPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombos = objYear.getComboListPAYYEAR(CInt(Session("Fin_year")), Session("FinancialYear_Name").ToString(), CInt(Session("CompanyUnkId")), "Year", True, True)
            With cboFromYear
                .DataTextField = "name"
                .DataValueField = "Id"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = CInt(Session("Fin_year"))
                .DataBind()
            End With
            With cboToYear
                .DataTextField = "name"
                .DataValueField = "Id"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = CInt(Session("Fin_year"))
                .DataBind()
            End With

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If Session("AssessmentReportTemplateId") <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, Default Assessment Form template is not set in Aruti Configuration. Please go to following path to set default template. " & _
                                              "Aruti Configuration -> Option -> Performance Management Tab", Me)
                Return False
            End If
            ObjAssesss_Form.SetDefaultValue()
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Employee is compulsory information. Please select employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If pnlNotHTMLTemp.Visible = True Then
                If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Period is compulsory information. Please select period to continue."), Me)
                    cboPeriod.Focus()
                    Return False
                End If
            End If

            If pnlPeriodSelection.Visible = True Then
                If cboFromPeriod.SelectedValue <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "From Period is compulsory information. Please select period to continue."), Me)
                    cboFromPeriod.Focus()
                    Return False
                End If

                If cboToPeriod.SelectedValue <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "To Period is compulsory information. Please select period to continue."), Me)
                    cboToPeriod.Focus()
                    Return False
                End If

                If cboToPeriod.SelectedIndex < cboFromPeriod.SelectedIndex Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, To Period cannot be less than From Period"), Me)
                    cboToPeriod.Focus()
                    Exit Function
                End If

                mDicPeriods = New Dictionary(Of Integer, String)
                Dim objPrd As New clscommom_period_Tran : Dim dsList As New DataSet
                'S.SANDEEP [06-NOV-2017] -- START
                'dsList = objPrd.GetList("List", enModuleReference.Assessment, CInt(Session("Fin_year")), Session("fin_startdate"), True, 1, , True)
                dsList = objPrd.GetList("List", enModuleReference.Assessment, CInt(Session("Fin_year")), Session("fin_startdate"), True, , , True)
                'S.SANDEEP [06-NOV-2017] -- END

                Dim dview As DataView = Nothing : Dim dRow() As DataRow = Nothing
                If cboFromPeriod.SelectedIndex = cboToPeriod.SelectedIndex Then
                    dview = New DataView(dsList.Tables(0), "periodunkid = '" & CInt(IIf(cboFromPeriod.SelectedValue = "", 0, cboFromPeriod.SelectedValue)) & "'", "end_date", DataViewRowState.CurrentRows)
                Else
                    dRow = dsList.Tables(0).Select("periodunkid = '" & CInt(IIf(cboFromPeriod.SelectedValue = "", 0, cboFromPeriod.SelectedValue)) & "'", "end_date")
                    If dRow.Length > 0 Then
                        dview = New DataView(dsList.Tables(0), "start_date >= '" & dRow(0).Item("start_date") & "' AND end_date <= '" & dRow(0).Item("end_date") & "'", "end_date", DataViewRowState.CurrentRows)
                    End If
                End If
                mDicPeriods = dview.ToTable.AsEnumerable().ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("periodunkid"), Function(row) row.Field(Of String)("period_name"))
                objPrd = Nothing : dsList.Dispose()

                ObjAssesss_Form._PeriodId = cboFromPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboFromPeriod.SelectedItem.Text
            Else
                ObjAssesss_Form._PeriodId = cboPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboPeriod.SelectedItem.Text
            End If
            ObjAssesss_Form._IsSelfAssignCompetencies = Session("Self_Assign_Competencies")
            ObjAssesss_Form._EmployeeId = cboEmployee.SelectedValue
            ObjAssesss_Form._EmployeeName = cboEmployee.SelectedItem.Text
            ObjAssesss_Form._IsPreviewDisplay = False
            ObjAssesss_Form._SelectedTemplateId = Session("AssessmentReportTemplateId")
            ObjAssesss_Form._ScoringOptionId = Session("ScoringOptionId")
            ObjAssesss_Form._EmployeeAsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            ObjAssesss_Form._IsProbationTemplate = chkProbationTemplate.Checked
            ObjAssesss_Form._IsUsedAgreedScore = Session("IsUseAgreedScore")

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            ObjAssesss_Form._IsCalibrationSettingActive = Session("IsCalibrationSettingActive")
            ObjAssesss_Form._DisplayScoreName = cboScoreOption.Text
            ObjAssesss_Form._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            ObjAssesss_Form = New clsAssessment_Form(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        If Not IsPostBack Then
            Call FillCombo()
        End If
        If Session("AssessmentReportTemplateId") <= 0 Then
            pnlNotHTMLTemp.Visible = True
            pnlPeriodSelection.Visible = False
            chkProbationTemplate.Visible = False
            Exit Sub
        End If
        If Session("AssessmentReportTemplateId") <> enAssessmentReportTemplate.ART_TEMPLATE_5 Then
            pnlPeriodSelection.Visible = False
            pnlNotHTMLTemp.Visible = True
            If Session("AssessmentReportTemplateId") = enAssessmentReportTemplate.ART_TEMPLATE_13 Then
                chkProbationTemplate.Visible = True
            Else
                chkProbationTemplate.Visible = False
            End If
        Else
            pnlNotHTMLTemp.Visible = False
            pnlPeriodSelection.Visible = True
            chkProbationTemplate.Visible = False
        End If

        'S.SANDEEP |27-MAY-2019| -- START
        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
        If CBool(Session("IsCalibrationSettingActive")) Then
            pnlCalibrationScore.Visible = True
        Else
            pnlCalibrationScore.Visible = False
        End If
        'S.SANDEEP |27-MAY-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()

            If Session("AssessmentReportTemplateId") = enAssessmentReportTemplate.ART_TEMPLATE_5 Then
                Dim strBuilder As System.Text.StringBuilder
                If mDicPeriods.Keys.Count > 0 Then
                    strBuilder = ObjAssesss_Form.Generate_MultiPeriod_DetailReport(cboEmployee.SelectedValue, mDicPeriods, cboFromYear.SelectedItem.Text, Session("ConsiderItemWeightAsNumber"), Session("ConsiderItemWeightAsNumber"), False, Session("Database_Name").ToString, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                    divhtml.InnerHtml = strBuilder.ToString
                    popupEmpAssessForm.Show()
                End If
            Else
                ObjAssesss_Form.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CStr(Session("ExportReportPath")), _
                                                CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))

                Session("objRpt") = ObjAssesss_Form._Rpt
                If Session("objRpt") IsNot Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
