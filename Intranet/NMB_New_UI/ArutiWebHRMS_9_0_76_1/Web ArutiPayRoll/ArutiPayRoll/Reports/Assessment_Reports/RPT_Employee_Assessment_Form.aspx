﻿<%@ Page  Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" Title = "Employee Assessemt Report"
    CodeFile="RPT_Employee_Assessment_Form.aspx.vb" Inherits="Reports_RPT_Employee_Assessment_Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Assessemt Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlMandatroy" runat="server">
                                        <div class="row clearfix   ">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 m-l-10">
                                                <asp:Label ID="gbFilterCriteria" runat="server" Text="Mandatory Info" Font-Bold="true"
                                                    CssClass="form-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 m-l-20">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlNotHTMLTemp" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12  m-l-20">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12  m-l-20">
                                                <asp:CheckBox ID="chkProbationTemplate" runat="server" Text="Probation Template" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlCalibrationScore" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 m-l-20">
                                                <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboScoreOption" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlPeriodSelection" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 m-l-20">
                                                <asp:Label ID="gbMultiperiod" runat="server" Text="Period Selection" Font-Bold="true"
                                                    CssClass="form-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12  m-l-20">
                                                <asp:Label ID="lblFromPeriod" runat="server" Text="From Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboFromPeriod" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ">
                                                <asp:Label ID="lblToPeriod" runat="server" Text="To Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboToPeriod" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlYearSelection" runat="server" Visible="false">
                                        <div class="row clearfix">
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12  m-l-20">
                                                <asp:Label ID="gbYearlyBasis" runat="server" Text="Year Selection" Font-Bold="true"
                                                    CssClass="form-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12  m-l-20">
                                                <asp:Label ID="lblYear" runat="server" Text="From Year" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboFromYear" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblToYear" runat="server" Text="To Year" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboToYear" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupEmpAssessForm" runat="server" CancelControlID="btnEmpReportingClose"
                    BackgroundCssClass="modal-backdrop" PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2"
                    Drag="false">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="width: 90%; height: 588px; display: none;" DefaultButton="btnEmpReportingClose"
                    ScrollBars="Auto">
                    <div class="body">
                        <div class="row clearfix">
                            <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
