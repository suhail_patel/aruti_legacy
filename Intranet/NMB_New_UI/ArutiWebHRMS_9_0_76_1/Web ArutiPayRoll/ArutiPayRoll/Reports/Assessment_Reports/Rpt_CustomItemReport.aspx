﻿<%@ Page Title="Custom Item Value Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_CustomItemReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_CustomItemReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script type="text/javascript">

    $("body").on("click", "[id*=chkHeder1]", function() {
        var chkHeader = $(this);
        debugger;
        var grid = $(this).closest("table");
        $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
    });


    $("body").on("click", "[id*=chkbox1]", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeder1]", grid);
        debugger;
        if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
            chkHeader.prop("checked", true);
        }
        else {
            chkHeader.prop("checked", false);
        }
    });
        
    </script>


    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Custom Item Value Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblItemGroup" runat="server" Text="Custom Header" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCustomHeader" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="Panel2" runat="server" Height="180px" ScrollBars="Auto">
                                            <asp:DataGrid ID="lvCustomItem" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyField = "Id">
                                                <Columns>
                                                    <asp:TemplateColumn ItemStyle-Width="25">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" Text = " "  />  <%--AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged"--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" Text = " " />  <%--AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" --%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Name" HeaderText="" />
                                                    <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                 <uc1:Export runat="server" ID="Export" />
            </ContentTemplate>
              <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
              </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
