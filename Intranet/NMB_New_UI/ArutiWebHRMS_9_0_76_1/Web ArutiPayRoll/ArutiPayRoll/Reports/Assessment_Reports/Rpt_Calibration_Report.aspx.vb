﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_Calibration_Report
    Inherits Basepage
    Private ReadOnly mstrModuleName As String = "frmAssessmentCalibrationReport"
    Private objCalibrationReport As clsAssessmentCalibrationReport

#Region " Private Variables "

    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes
    Private mdtDisplayCol As DataTable

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objScApprl As New clsScoreCalibrationApproval
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                              Session("UserAccessModeSetting").ToString(), True, _
                                              CBool(Session("IsIncludeInactiveEmp")), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name").ToString(), CDate(Session("fin_startdate")), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Not Calibrated Employee"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Calibrated Employee"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Calibration Status"))
                'S.SANDEEP |01-MAY-2020| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Approver Wise Calibration Status"))
                'S.SANDEEP |01-MAY-2020| -- END
                .SelectedIndex = 0
            End With
            Call cboReportType_SelectedIndexChanged(New Object, New EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                mdtDisplayCol = dsList.Tables(0).Copy()

                Dim dCol As New DataColumn
                dCol.ColumnName = "ischeck"
                dCol.DataType = GetType(System.Boolean)
                dCol.DefaultValue = False
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhSelectCol"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhJoin"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhDisplay"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                For Each dtRow As DataRow In mdtDisplayCol.Rows
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrstation_master AS esm ON T.stationunkid = esm.stationunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edg.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrdepartment_group_master AS edg ON T.deptgroupunkid = edg.deptgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edp.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrdepartment_master AS edp ON T.departmentunkid = edp.departmentunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esg.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsectiongroup_master AS esg ON T.sectiongroupunkid = esg.sectiongroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esc.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsection_master AS esc ON T.sectionunkid = esc.sectionunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eug.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunitgroup_master AS eug ON T.unitgroupunkid = eug.unitgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eum.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunit_master AS eum ON T.unitunkid = eum.unitunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.TEAM
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(etm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrteam_master AS etm ON T.teamunkid = etm.teamunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOB_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejg.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrjobgroup_master AS ejg ON J.jobgroupunkid = ejg.jobgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOBS
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejb.job_name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrjob_master AS ejb ON J.jobunkid = ejb.jobunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASS_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecg.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclassgroup_master AS ecg ON T.classgroupunkid = ecg.classgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASSES
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecl.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclasses_master AS ecl ON T.classunkid = ecl.classesunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 900
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egg.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrgradegroup_master AS egg ON G.gradegroupunkid = egg.gradegroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 901
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egd.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrgrade_master AS egd ON G.gradeunkid = egd.gradeunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 902
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egl.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrgradelevel_master AS egl ON G.gradelevelunkid = egl.gradelevelunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.COST_CENTER
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecc.costcentername,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN prcostcenter_master AS ecc ON C.costcenterunkid = ecc.costcenterunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                    End Select
                Next

                lvDisplayCol.DataSource = mdtDisplayCol
                lvDisplayCol.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboPeriod.SelectedValue = "0"
            cboEmployee.SelectedValue = "0"
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            'S.SANDEEP |12-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            If lnkAnalysisBy.Visible = False Then lnkAnalysisBy.Visible = True
            Call cboReportType_SelectedIndexChanged(cboReportType, New EventArgs())
            If lvDisplayCol.Rows.Count > 0 Then
                Dim xDataGridItem As IEnumerable(Of GridViewRow) = Nothing
                Dim chkCheckBox As CheckBox = Nothing
                Dim xCtrl As Control = lvDisplayCol.HeaderRow.FindControl("chkHeder1")
                If xCtrl IsNot Nothing Then
                    chkCheckBox = CType(xCtrl, CheckBox) : chkCheckBox.Checked = False
                End If
                chkCheckBox = Nothing
                xDataGridItem = lvDisplayCol.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.RowType = DataControlRowType.DataRow).Select(Function(x) x)
                If xDataGridItem IsNot Nothing AndAlso xDataGridItem.Count > 0 Then
                    For Each xItem In xDataGridItem
                        chkCheckBox = CType(xItem.FindControl("chkbox1"), CheckBox)
                        If chkCheckBox IsNot Nothing Then
                            chkCheckBox.Checked = False
                        End If
                    Next
                End If
            End If
            'S.SANDEEP |12-NOV-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objCalibrationReport.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                Return False
            End If

            Dim row As IEnumerable(Of GridViewRow) = Nothing
            row = lvDisplayCol.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

            If row Is Nothing Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Please check atleast one column to display in report."), Me)
                Return False
            ElseIf row IsNot Nothing AndAlso row.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Please check atleast one column to display in report."), Me)
                Return False
            End If

            objCalibrationReport._ApplyUserAccessFilter = True
            objCalibrationReport._CalibrationStatusId = 0
            objCalibrationReport._CalibrationStatusName = ""
            objCalibrationReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCalibrationReport._EmplyeeName = cboEmployee.SelectedItem.Text
            objCalibrationReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objCalibrationReport._PeriodName = cboPeriod.SelectedItem.Text
            objCalibrationReport._ReportViewTypeId = cboReportType.SelectedIndex
            objCalibrationReport._ReportViewName = cboReportType.SelectedItem.Text
            objCalibrationReport._ViewByIds = mstrStringIds
            objCalibrationReport._ViewIndex = mintViewIdx
            objCalibrationReport._ViewByName = mstrStringName
            objCalibrationReport._Analysis_Fields = mstrAnalysis_Fields
            objCalibrationReport._Analysis_Join = mstrAnalysis_Join
            objCalibrationReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCalibrationReport._Report_GroupName = mstrReport_GroupName
            objCalibrationReport._Advance_Filter = mstrAdvanceFilter

            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            iCols = String.Join(vbCrLf & " ", row.AsEnumerable().Select(Function(x) lvDisplayCol.DataKeys(x.RowIndex)("objcolhSelectCol").ToString()).ToArray())
            iJoins = String.Join(vbCrLf & " ", row.AsEnumerable().Select(Function(x) lvDisplayCol.DataKeys(x.RowIndex)("objcolhJoin").ToString()).ToArray())
            iDisplay = String.Join(vbCrLf & " ", row.AsEnumerable().Select(Function(x) lvDisplayCol.DataKeys(x.RowIndex)("objcolhDisplay").ToString()).ToArray())
            objCalibrationReport._SelectedCols = iCols
            objCalibrationReport._SelectedJoin = iJoins & vbCrLf
            objCalibrationReport._DisplayCols = iDisplay & vbCrLf

            Dim grow As IEnumerable(Of GridViewRow) = Nothing
            Select Case CInt(cboReportType.SelectedIndex)
                Case 0, 1
                    grow = dgvRating.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkabox1"), CheckBox).Checked = True)
                    If grow IsNot Nothing AndAlso grow.Count > 0 Then
                        objCalibrationReport._mDictRatingFrom = grow.AsEnumerable().ToDictionary(Function(x) CInt(dgvRating.DataKeys(x.RowIndex)("id")), Function(y) CDec(dgvRating.DataKeys(y.RowIndex)("scrf")))
                        objCalibrationReport._mDictRatingTo = grow.AsEnumerable().ToDictionary(Function(x) CInt(dgvRating.DataKeys(x.RowIndex)("id")), Function(y) CDec(dgvRating.DataKeys(y.RowIndex)("scrt")))
                    End If
                Case 2, 3
                    grow = dgvRating.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkabox1"), CheckBox).Checked = True)
                    If grow IsNot Nothing AndAlso grow.Count > 0 Then
                        objCalibrationReport._CalibrationUnkids = String.Join(",", grow.AsEnumerable().Select(Function(x) dgvRating.DataKeys(x.RowIndex)("calibratnounkid").ToString()).ToArray())
                    End If
            End Select

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillGrid(ByVal intReportType As Integer)
        Try
            Dim dsCombos As New DataSet
            dgvRating.DataSource = Nothing : dgvRating.DataBind()
            Select Case intReportType
                Case 0, 1
                    Dim objRating As New clsAppraisal_Rating
                    dsCombos = objRating.getComboList("List", False, Session("Database_Name").ToString())
                    Dim icol As New DataColumn
                    With icol
                        .ColumnName = "calibration_no"
                        .DataType = GetType(System.String)
                        .DefaultValue = ""
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)

                    icol = New DataColumn
                    With icol
                        .ColumnName = "calibratnounkid"
                        .DataType = GetType(System.Int32)
                        .DefaultValue = 0
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)
                Case 2, 3
                    Dim objCSMaster As New clsComputeScore_master
                    dsCombos = objCSMaster.GetCalibrationNoList(CInt(cboPeriod.SelectedValue), "List", Nothing)
                    objCSMaster = Nothing
                    Dim icol As New DataColumn
                    With icol
                        .ColumnName = "name"
                        .DataType = GetType(System.String)
                        .DefaultValue = ""
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)
                    icol = New DataColumn
                    With icol
                        .ColumnName = "scrf"
                        .DataType = GetType(System.Decimal)
                        .DefaultValue = 0
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)
                    icol = New DataColumn
                    With icol
                        .ColumnName = "scrt"
                        .DataType = GetType(System.Decimal)
                        .DefaultValue = 0
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)
                    icol = New DataColumn
                    With icol
                        .ColumnName = "id"
                        .DataType = GetType(System.Int32)
                        .DefaultValue = 0
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)
            End Select
            dgvRating.DataSource = dsCombos.Tables(0).Copy
            dgvRating.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objCalibrationReport = New clsAssessmentCalibrationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call FillCombo()
                Call Fill_Column_List()
                Call ResetValue()
                'S.SANDEEP |12-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
            Else
                mstrStringIds = Me.ViewState("mstrStringIds").ToString()
                mstrStringName = Me.ViewState("mstrStringName").ToString()
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName").ToString()
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
                'S.SANDEEP |12-NOV-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'S.SANDEEP |12-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |12-NOV-2019| -- END

#End Region

#Region " Button's Event(s) "

    Protected Sub btnAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "AEM"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat() : Dim strFileName As String = String.Empty
            Select Case CInt(cboReportType.SelectedIndex)
                Case 0, 1
                    objCalibrationReport.Export_Calibratrion_Report(Session("Database_Name").ToString, _
                                                                    CInt(Session("UserId")), _
                                                                    CInt(Session("Fin_year")), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                    CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                                    IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                    False)
                Case 2
                    objCalibrationReport.Export_CalibrationStatus_Report(Session("Database_Name").ToString, _
                                                                         CInt(Session("CompanyUnkId")), _
                                                                         CInt(Session("Fin_year")), _
                                                                         CStr(Session("UserAccessModeSetting")), _
                                                                         enUserPriviledge.AllowToApproveRejectCalibratedScore, _
                                                                         Session("EmployeeAsOnDate").ToString(), _
                                                                         CInt(Session("UserId")), _
                                                                         False, _
                                                                         IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                         False)
                    'S.SANDEEP |01-MAY-2020| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                Case 3
                    objCalibrationReport.GetNextEmployeeApproversNew(CInt(cboPeriod.SelectedValue), _
                                                                     Session("Database_Name").ToString, _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CStr(Session("UserAccessModeSetting")), _
                                                                     Session("EmployeeAsOnDate").ToString(), _
                                                                     CInt(Session("UserId")), _
                                                                     IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                     False)
                    'S.SANDEEP |01-MAY-2020| -- END
            End Select
            If strFileName.Trim.Length <= 0 Then strFileName = objCalibrationReport._FileNameAfterExported.Trim

            If strFileName.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
                'Pinkal (27-May-2021) -- Start
                'Enhancement NMB - Working on converting Assessment Report in New UI.
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()
                'Pinkal (27-May-2021) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            Dim objPeriod As New clscommom_period_Tran
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0
                    lblCalibrationRating.Visible = False : lblProvisionalRating.Visible = True : lblCalibrationNo.Visible = False
                    dgvRating.Columns(2).Visible = False : dgvRating.Columns(1).Visible = True
                    lnkAnalysisBy.Visible = True
                Case 1
                    lblCalibrationRating.Visible = True : lblProvisionalRating.Visible = False : lblCalibrationNo.Visible = False
                    dgvRating.Columns(2).Visible = False : dgvRating.Columns(1).Visible = True
                    lnkAnalysisBy.Visible = True
                Case 2, 3
                    lblCalibrationNo.Visible = True : lblCalibrationRating.Visible = False : lblProvisionalRating.Visible = False
                    dgvRating.Columns(2).Visible = True : dgvRating.Columns(1).Visible = False
                    lnkAnalysisBy.Visible = False
                    mstrStringIds = "" : mstrStringName = "" : mintViewIdx = 0 : mstrAnalysis_Fields = "" : mstrAnalysis_Join = "" : mstrAnalysis_OrderBy = "" : mstrReport_GroupName = ""
            End Select
            FillGrid(cboReportType.SelectedIndex)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class