﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_CustomItemReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentSoreRatingReport"
    Private objCustomItemValue As clsCustomItemValueReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes
    Private mdtCItems As DataTable

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = (New clsassess_custom_header).getComboList(0, True, "List")
            With cboCustomHeader
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            'S.SANDEEP |09-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Custom Item Header & Period Wise"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Custom Item Employee & Period Wise"))
                .SelectedIndex = 0
            End With
            cboReportType_SelectedIndexChanged(New Object(), New EventArgs())
            'S.SANDEEP |09-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboCustomHeader.SelectedValue = 0
            mstrAdvanceFilter = ""
            mstrAnalysis_Join = ""
            mstrStringName = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_Fields = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objCustomItemValue.SetDefaultValue()

            If cboPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If cboCustomHeader.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Custom Header is mandatory information. Please select Custom Header to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If


            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'If CType(Me.ViewState("mdtCItems"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please select atleast one custom item to export report."), Me)
            '    Return False
            'End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = lvCustomItem.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please select atleast one custom item to export report."), Me)
                Return False
            End If
            'Pinkal (27-May-2021) -- End

            objCustomItemValue._Report_Name = lblPageHeader.Text
            'S.SANDEEP |09-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
            'objCustomItemValue._EmployeeId = cboEmployee.SelectedValue
            'objCustomItemValue._EmployeeName = cboEmployee.Text
            'objCustomItemValue._PeriodId = cboPeriod.SelectedValue
            'objCustomItemValue._PeriodName = cboPeriod.Text
            'objCustomItemValue._CustomHeaderText = cboCustomHeader.Text

            objCustomItemValue._EmployeeId = cboEmployee.SelectedValue
            objCustomItemValue._EmployeeName = cboEmployee.SelectedItem.Text
            objCustomItemValue._PeriodId = cboPeriod.SelectedValue
            objCustomItemValue._PeriodName = cboPeriod.SelectedItem.Text
            objCustomItemValue._CustomHeaderText = cboCustomHeader.SelectedItem.Text
            'S.SANDEEP |09-FEB-2019| -- END


            objCustomItemValue._Analysis_Join = mstrAnalysis_Join
            objCustomItemValue._ViewByName = mstrStringName
            objCustomItemValue._Report_GroupName = mstrReport_GroupName
            objCustomItemValue._Advance_Filter = mstrAdvanceFilter
            objCustomItemValue._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCustomItemValue._Analysis_Fields = mstrAnalysis_Fields

            Dim dRow() As DataRow


            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'Dim strItemIds As String = String.Join(",", CType(Me.ViewState("mdtCItems"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("Id").ToString).ToArray())
            Dim strItemIds As String = String.Join(",", gRow.Select(Function(y) lvCustomItem.DataKeys(y.ItemIndex).ToString()).ToArray())
            'Pinkal (27-May-2021) -- End

            Dim dList As DataSet = (New clsassess_custom_items).GetList("List")
            dRow = dList.Tables(0).Select("customitemunkid IN (" & strItemIds & ")")
            objCustomItemValue._Item_Rows = dRow
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub Fill_List()
        Dim dList As DataSet
        Try
            dList = (New clsassess_custom_items).getComboList(CInt(cboPeriod.SelectedValue), CInt(cboCustomHeader.SelectedValue), False, "List")
            If dList Is Nothing Then Exit Sub
            mdtCItems = dList.Tables(0).Copy()
            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtCItems.Columns.Add(dCol)

            lvCustomItem.DataSource = mdtCItems
            lvCustomItem.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            objCustomItemValue = New clsCustomItemValueReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        If Not IsPostBack Then
            Call FillCombo()
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim StrFileName As String = String.Empty
            objCustomItemValue.Export_Custom_Item_Value_Report(eZeeDate.convertDate(Session("EmployeeAsOnDate")), IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), StrFileName)

            If StrFileName.Trim <> "" Then
                'S.SANDEEP |09-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
                'Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & StrFileName
                If StrFileName.Contains(".") Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & StrFileName
                Else
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & StrFileName & ".xls"
                End If
                'S.SANDEEP |09-FEB-2019| -- END

                'Pinkal (27-May-2021) -- Start
                'Enhancement NMB - Working on converting Assessment Report in New UI.
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()
                'Pinkal (27-May-2021) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |09-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            objCustomItemValue.SetDefaultValue()

            If cboPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            objCustomItemValue._Report_Name = cboReportType.SelectedItem.Text
            objCustomItemValue._EmployeeId = cboEmployee.SelectedValue
            objCustomItemValue._EmployeeName = cboEmployee.SelectedItem.Text
            objCustomItemValue._PeriodId = cboPeriod.SelectedValue
            objCustomItemValue._PeriodName = cboPeriod.SelectedItem.Text

            objCustomItemValue.generateReportNew(Session("Database_Name").ToString, _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                 CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                 CStr(Session("ExportReportPath")), _
                                                 CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                 enExportAction.None, _
                                                 CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objCustomItemValue._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |09-FEB-2019| -- END


#End Region

#Region " Combobox Event(s) "

    Private Sub cboCustomHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomHeader.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCustomHeader.SelectedValue) > 0 Then
                Call Fill_List()

                'Pinkal (27-May-2021) -- Start
                'Enhancement NMB - Working on converting Assessment Report in New UI.
                'If Me.ViewState("mdtCItems") IsNot Nothing Then
                '    Me.ViewState("mdtCItems") = mdtCItems
                'Else
                '    Me.ViewState.Add("mdtCItems", mdtCItems)
                'End If
                'Pinkal (27-May-2021) -- End
            Else
                lvCustomItem.DataSource = Nothing
                lvCustomItem.DataBind()
                'Pinkal (27-May-2021) -- Start
                'Enhancement NMB - Working on converting Assessment Report in New UI.
                'Me.ViewState("mdtCItems") = Nothing
                'Pinkal (27-May-2021) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |09-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                cboCustomHeader.SelectedValue = 0
                cboCustomHeader.Enabled = True
                btnExport.Visible = True
                btnReport.Visible = False
            Else
                cboCustomHeader.SelectedValue = 0
                cboCustomHeader.Enabled = False
                btnExport.Visible = False
                btnReport.Visible = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |09-FEB-2019| -- END

#End Region

#Region " Checkbox Event(s) "


    'Pinkal (27-May-2021) -- Start
    'Enhancement NMB - Working on converting Assessment Report in New UI.

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If lvCustomItem.Items.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim chkValue As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = CType(Me.ViewState("mdtCItems"), DataTable).DefaultView
    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvItem As DataGridItem = lvCustomItem.Items(j)
    '            CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtCItems"), DataTable).Select("Id = '" & gvItem.Cells(2).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '            j += 1
    '        Next
    '        Me.ViewState("mdtCItems") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        If gvItem.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtCItems"), DataTable).Select("Id = '" & gvItem.Cells(2).Text & "'")
    '            dRow(0).Item("ischeck") = cb.Checked
    '            CType(Me.ViewState("mdtCItems"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (27-May-2021) -- End

#End Region

End Class
