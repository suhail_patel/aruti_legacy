﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_EmpCalibrator_Approver_Report
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmpCalibratorApprover As clsEmp_Calibrator_Approver_Report
    Private ReadOnly mstrModuleName As String = "frmEmp_Calibrator_Approver_Report"
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty


#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try

            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            chkInactiveemp.Checked = False
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpCalibratorApprover.SetDefaultValue()
            objEmpCalibratorApprover._EmployeeUnkid = cboEmployee.SelectedValue
            objEmpCalibratorApprover._EmployeeName = cboEmployee.Text
            objEmpCalibratorApprover._EmployeeAsonDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            objEmpCalibratorApprover._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpCalibratorApprover._ViewByIds = mstrViewByIds
            objEmpCalibratorApprover._ViewIndex = mintViewIndex
            objEmpCalibratorApprover._ViewByName = mstrViewByName
            objEmpCalibratorApprover._Analysis_Fields = mstrAnalysisFields
            objEmpCalibratorApprover._Analysis_Join = mstrAnalysisJoin
            objEmpCalibratorApprover._Analysis_OrderBy = mstrAnalysisOrderBy
            objEmpCalibratorApprover._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objEmpCalibratorApprover._Report_GroupName = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objEmpCalibratorApprover = New clsEmp_Calibrator_Approver_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
            Call SetLanguage()
                Call FillCombo()
            Else
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmpCalibratorApprover.setDefaultOrderBy(0)
            objEmpCalibratorApprover._CompanyUnkId = Session("CompanyUnkId")
            objEmpCalibratorApprover._UserUnkId = Session("UserId")
            Call SetDateFormat()

            objEmpCalibratorApprover.generateReportNew(Session("Database_Name"), _
                                          Session("UserId"), _
                                          Session("Fin_year"), _
                                          Session("CompanyUnkId"), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                          Session("UserAccessModeSetting"), True, _
                                          Session("ExportReportPath"), _
                                          Session("OpenAfterExport"), _
                                          0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))


            Session("objRpt") = objEmpCalibratorApprover._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysisFields = popupAnalysisBy._Analysis_Fields
            mstrAnalysisJoin = popupAnalysisBy._Analysis_Join
            mstrAnalysisOrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReportGroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysisOrderByGName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.BtnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

End Class
