﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
Imports System.Web.UI.DataVisualization.Charting

#End Region

Partial Class Reports_Assessment_Reports_Rpt_PerformanceEvaluationReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPerformanceEvaluationReport"
    Private objPerformance As clsPerformanceEvaluationReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private DisplayMessage As New CommonCodes
    Private mdtAllocation As DataTable

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objMData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = 1
            End With
            Call cboAllocations_SelectedIndexChanged(New Object(), New EventArgs())

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboAllocations.SelectedValue = 1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        objPerformance.SetDefaultValue()
        Try
            If cboPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If


            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'If CType(Me.ViewState("mdtAllocation"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please select atleast one allocation to export report."), Me)
            '    lvAllocation.Focus()
            '    Return False
            'End If
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = lvAllocation.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please select atleast one allocation to export report."), Me)
                lvAllocation.Focus()
                Return False
            End If
            'Pinkal (27-May-2021) -- End

            objPerformance._PeriodId = CInt(cboPeriod.SelectedValue)
            objPerformance._PeriodName = cboPeriod.SelectedItem.Text
            objPerformance._StartDate = mdtStartDate
            objPerformance._EndDate = mdtEndDate

            Dim StrStringIds As String = ""

            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'StrStringIds = String.Join(",", CType(Me.ViewState("mdtAllocation"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("Id").ToString()).ToArray())
            StrStringIds = String.Join(",", gRow.Select(Function(y) lvAllocation.DataKeys(y.ItemIndex).ToString()).ToArray())
            'Pinkal (27-May-2021) -- End


            objPerformance._Allocation = cboAllocations.SelectedItem.Text

            If StrStringIds.Length > 0 Then
                Select Case cboAllocations.SelectedValue
                    Case enAllocation.BRANCH
                        StrStringIds = "hrstation_master.stationunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrstation_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "stationunkid"
                    Case enAllocation.DEPARTMENT_GROUP
                        StrStringIds = "hrdepartment_group_master.deptgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrdepartment_group_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "deptgroupunkid"
                    Case enAllocation.DEPARTMENT
                        StrStringIds = "hrdepartment_master.departmentunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrdepartment_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "departmentunkid"
                    Case enAllocation.SECTION_GROUP
                        StrStringIds = "hrsectiongroup_master.sectiongroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrsectiongroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "sectiongroupunkid"
                    Case enAllocation.SECTION
                        StrStringIds = "hrsection_master.sectionunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrsection_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "sectionunkid"
                    Case enAllocation.UNIT_GROUP
                        StrStringIds = "hrunitgroup_master.unitgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrunitgroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "unitgroupunkid"
                    Case enAllocation.UNIT
                        StrStringIds = "hrunit_master.unitunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrunit_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "unitunkid"
                    Case enAllocation.TEAM
                        StrStringIds = "hrteam_master.teamunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrteam_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "teamunkid"
                    Case enAllocation.JOB_GROUP
                        StrStringIds = "hrjobgroup_master.jobgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrjobgroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "jobgroupunkid"
                    Case enAllocation.JOBS
                        StrStringIds = "hrjob_master.jobunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrjob_master"
                        objPerformance._iSelectColName = "job_name"
                        objPerformance._iUnkidColName = "jobunkid"
                    Case enAllocation.CLASS_GROUP
                        StrStringIds = "hrclassgroup_master.classgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrclassgroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "classgroupunkid"
                    Case enAllocation.CLASSES
                        StrStringIds = "hrclasses_master.classesunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrclasses_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "classesunkid"
                End Select
            End If
            objPerformance._AllocationIds = StrStringIds
            objPerformance._ScoringOptionId = Session("ScoringOptionId")
            objPerformance._SelfAssignCompetencies = Session("Self_Assign_Competencies")

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            objPerformance._IsCalibrationSettingActive = Session("IsCalibrationSettingActive")
            objPerformance._DisplayScoreName = cboScoreOption.Text
            objPerformance._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub Fill_Data(Optional ByVal onlyCall As Boolean = False)
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name", onlyCall)
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name", onlyCall)
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name", onlyCall)
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name", onlyCall)
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name", onlyCall)
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name", onlyCall)
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name", onlyCall)
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name", onlyCall)
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name", onlyCall)
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName", onlyCall)
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name", onlyCall)
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name", onlyCall)
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String, Optional ByVal onlyCall As Boolean = False)
        Try
            mdtAllocation = New DataTable("List")
            Dim dCol As New DataColumn
            dCol.ColumnName = "Id"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtAllocation.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Name"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtAllocation.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ischeck"
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtAllocation.Columns.Add(dCol)

            For Each dtRow As DataRow In dTable.Rows
                Dim drow As DataRow = mdtAllocation.NewRow()
                drow.Item("Id") = dtRow.Item(StrIdColName)
                drow.Item("Name") = dtRow.Item(StrDisColName)
                mdtAllocation.Rows.Add(drow)
            Next

            If onlyCall = False Then
            lvAllocation.DataSource = mdtAllocation
            lvAllocation.DataBind()
            lvAllocation.Columns(2).HeaderText = cboAllocations.SelectedItem.Text
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            objPerformance = New clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        If Not IsPostBack Then
            Call FillCombo()
        End If
        'S.SANDEEP |27-MAY-2019| -- START
        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
        If CBool(Session("IsCalibrationSettingActive")) Then
            lblDisplayScore.Visible = True : cboScoreOption.Visible = True
        Else
            lblDisplayScore.Visible = False : cboScoreOption.Visible = False
        End If
        'S.SANDEEP |27-MAY-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            'objPerformance._UserUnkId = CInt(Session("UserId"))
            If objPerformance.Export_Evaluation_Report(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                       CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False) = False Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period."), Me)
                'Sohail (23 Mar 2019) -- End
            Else
                If objPerformance._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objPerformance._FileNameAfterExported
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                    Export.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            Call Fill_Data()

            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'If Me.ViewState("mdtAllocation") IsNot Nothing Then
            '    Me.ViewState("mdtAllocation") = mdtAllocation
            'Else
            '    Me.ViewState.Add("mdtAllocation", mdtAllocation)
            'End If
            'Pinkal (27-May-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Protected Sub txtFrmSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim dView As DataView = CType(Me.ViewState("mdtAllocation"), DataTable).DefaultView
            Dim StrSearch As String = String.Empty
            If txtSearch.Text.Trim.Length > 0 Then
                StrSearch = "Name LIKE '%" & txtSearch.Text & "%' "
            End If
            dView.RowFilter = StrSearch
            lvAllocation.DataSource = dView
            lvAllocation.DataBind()
            lvAllocation.Focus()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure txtFrmSearch_TextChanged : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            Dim j As Integer = 0
            Dim chkValue As CheckBox = CType(sender, CheckBox)
            Dim dvEmployee As DataView = CType(Me.ViewState("mdtAllocation"), DataTable).DefaultView
            For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
                Dim gvItem As DataGridItem = lvAllocation.Items(j)
                CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
                Dim dRow() As DataRow = CType(Me.ViewState("mdtAllocation"), DataTable).Select("Id = '" & gvItem.Cells(2).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked
                End If
                dvEmployee.Table.AcceptChanges()
                j += 1
            Next
            Me.ViewState("mdtAllocation") = dvEmployee.ToTable
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
            If gvItem.Cells.Count > 0 Then
                Dim dRow() As DataRow = CType(Me.ViewState("mdtAllocation"), DataTable).Select("Id = '" & gvItem.Cells(2).Text & "'")
                dRow(0).Item("ischeck") = cb.Checked
                CType(Me.ViewState("mdtAllocation"), DataTable).AcceptChanges()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
#Region " Link Event(s) "

    Protected Sub lnkDisplayChart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDisplayChart.Click
        Try
            If SetFilter() = False Then Exit Sub

            Dim StrStringIds As String = ""
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = lvAllocation.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            StrStringIds = String.Join(",", gRow.Select(Function(y) lvAllocation.DataKeys(y.ItemIndex).ToString()).ToArray())
            Dim dt As New DataTable
            If StrStringIds Then
                Call Fill_Data(True)
                If mdtAllocation IsNot Nothing Then
                    dt = mdtAllocation.Select("Id IN (" & StrStringIds & ")").CopyToDataTable()
                End If
            End If

            Dim dsDataSet As New DataSet

            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            dsDataSet = objPerformance.Show_Analysis_Chart(Session("Database_Name").ToString, _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        objPrd._Start_Date, _
                                                        objPrd._End_Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")))
            objPrd = Nothing
            If dsDataSet.Tables.Count > 0 Then
                lvDisplayAllocation.DataSource = dt
                lvDisplayAllocation.DataBind()
                lvDisplayAllocation.Columns(0).HeaderText = cboAllocations.SelectedItem.Text

                chAnalysis_Chart.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "xAxis", dsDataSet.Tables(0).DefaultView, "yAxis1")
                chAnalysis_Chart.Series(1).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "xAxis", dsDataSet.Tables(0).DefaultView, "yAxis2")

                chAnalysis_Chart.ChartAreas(0).AxisX.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Scores")
                chAnalysis_Chart.ChartAreas(0).AxisY.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Number Of Staff Assessed")
                chAnalysis_Chart.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chAnalysis_Chart.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chAnalysis_Chart.ChartAreas(0).AxisY2.LabelStyle.Format = "{0.##}%"
                chAnalysis_Chart.Titles(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Performance Evaluation Status") & vbCrLf & cboPeriod.SelectedItem.Text
                chAnalysis_Chart.Titles(0).Font = New Font(chAnalysis_Chart.Font.Name, 11, FontStyle.Bold)
                chAnalysis_Chart.Series(0).IsValueShownAsLabel = True
                chAnalysis_Chart.Series(1).IsValueShownAsLabel = True
                chAnalysis_Chart.Series(1).LabelFormat = "{0.##}%"

                chAnalysis_Chart.Series(0).IsVisibleInLegend = False
                chAnalysis_Chart.Series(1).IsVisibleInLegend = False

                chAnalysis_Chart.Series(0).Color = Color.SeaGreen
                chAnalysis_Chart.Series(1).Color = Color.Tomato


                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "# Of Emplyoee")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chAnalysis_Chart.Series(0).Color
                chAnalysis_Chart.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "%")
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chAnalysis_Chart.Series(1).Color
                chAnalysis_Chart.Legends(0).CustomItems.Add(legendItem2)

                chAnalysis_Chart.Series(0).ToolTip = chAnalysis_Chart.Legends(0).CustomItems(0).Name & " : is #VAL "
                chAnalysis_Chart.Series(1).ToolTip = chAnalysis_Chart.Legends(0).CustomItems(1).Name & " : is #VAL "


                For i As Integer = 0 To chAnalysis_Chart.Series.Count - 1
                    For Each dp As DataPoint In chAnalysis_Chart.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next

                popupAnalysisChart.Show()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period."), Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDisplayHPOCurve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDisplayHPOCurve.Click
        Try
            If SetFilter() = False Then Exit Sub
            Dim dsDataSet As New DataSet
            'S.SANDEEP |22-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'dsDataSet = objPerformance.GenerateChartData(CInt(cboPeriod.SelectedValue), _
            '                                             Session("Database_Name").ToString, _
            '                                             CInt(Session("UserId")), _
            '                                             CInt(Session("Fin_year")), _
            '                                             CInt(Session("CompanyUnkId")), _
            '                                             CStr(Session("UserAccessModeSetting")), True, False, "List", , , , , IIf(cboScoreOption.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cboScoreOption.SelectedValue))
            dsDataSet = objPerformance.GenerateChartData(CInt(cboPeriod.SelectedValue), _
                                                         Session("Database_Name").ToString, _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         CStr(Session("UserAccessModeSetting")), True, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         False, 0, "List", , , , , IIf(cboScoreOption.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cboScoreOption.SelectedValue))
            'S.SANDEEP |22-OCT-2019| -- END

            If dsDataSet.Tables.Count > 0 Then
                dgvCalibData.DataSource = dsDataSet.Tables(0)
                dgvCalibData.DataBind()
                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                If dgvCalibData.Items.Count > 0 Then
                    Dim gvr As DataGridItem = Nothing
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(5).Text = "N").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Blue
                    End If
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(5).Text = "A").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Tomato
                    End If
                    Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(5))
                    If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                        For Each item In xItems
                            item.Visible = False
                        Next
                    End If
                End If
                'S.SANDEEP |26-AUG-2019| -- END

                chHpoCurve.ChartAreas(0).AxisX.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 100, "Ratings")
                chHpoCurve.ChartAreas(0).AxisY.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "%")
                chHpoCurve.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Format = "{0.##}%"

                chHpoCurve.Series(0).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "np")
                chHpoCurve.Series(1).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "ap")

                chHpoCurve.Titles(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 200, "Performance HPO Curve") & vbCrLf & _
                                      cboPeriod.SelectedItem.Text

                chHpoCurve.Titles(0).Font = New Font(chHpoCurve.Font.Name, 11, FontStyle.Bold)

                chHpoCurve.Series(0).LabelFormat = "{0.##}%"
                chHpoCurve.Series(1).LabelFormat = "{0.##}%"

                chHpoCurve.Series(0).IsVisibleInLegend = False
                chHpoCurve.Series(1).IsVisibleInLegend = False

                chHpoCurve.Series(0).Color = Color.Blue
                chHpoCurve.Series(1).Color = Color.Tomato

                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Normal Distribution")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chHpoCurve.Series(0).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                If dsDataSet.Tables("Chart").Rows.Count > 0 Then
                    legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Actual Performance") & dsDataSet.Tables("Chart").Rows(0)("litem").ToString()
                Else
                    legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Actual Performance")
                End If
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chHpoCurve.Series(1).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem2)

                chHpoCurve.Series(0).ToolTip = chHpoCurve.Legends(0).CustomItems(0).Name & " : is #VAL "
                chHpoCurve.Series(1).ToolTip = chHpoCurve.Legends(0).CustomItems(1).Name & " : is #VAL "

                For i As Integer = 0 To chHpoCurve.Series.Count - 1
                    For Each dp As DataPoint In chHpoCurve.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next

                popupHPOChart.Show()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |27-JUL-2019| -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Please select atleast one allocation to export report.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Performance Evaluation Status")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "# Of Emplyoee")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "%")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "Scores")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 8, "Number Of Staff Assessed")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 100, "Ratings")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 101, "%")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 102, "Normal Distribution")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 103, "Actual Performance")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 200, "Performance HPO Curve")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
