﻿<%@ Page Title="Performance Evaluation Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_PerformanceEvaluationReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_PerformanceEvaluationReport" %>

<%--'S.SANDEEP |27-JUL-2019| -- START--%>
<%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--'S.SANDEEP |27-JUL-2019| -- END--%>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script type="text/javascript">

    $("body").on("click", "[id*=chkHeder1]", function() {
        var chkHeader = $(this);
        debugger;
        var grid = $(this).closest("table");
        $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
    });


    $("body").on("click", "[id*=chkbox1]", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeder1]", grid);
        debugger;
        if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
            chkHeader.prop("checked", true);
        }
        else {
            chkHeader.prop("checked", false);
        }
    });
        
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Performance Evaluation Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAllocation" runat="server" Text="Allocation" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAllocations" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboScoreOption" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 200px">
                                            <asp:DataGrid ID="lvAllocation" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyField="Id">
                                                <Columns>
                                                    <asp:TemplateColumn ItemStyle-Width="25">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" Text=" " />
                                                            <%--  AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" --%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" Text=" " />
                                                            <%-- AutoPostBack="true"  OnCheckedChanged="chkbox1_CheckedChanged" --%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Name" HeaderText="" />
                                                    <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:LinkButton ID="lnkDisplayChart" Text="Display Analysis Chart" runat="server"
                                            CssClass="form-label"></asp:LinkButton>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:LinkButton ID="lnkDisplayHPOCurve" runat="server" Text="Display HPO Curve" CssClass="form-label"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                <cc1:ModalPopupExtender ID="popupAnalysisChart" BackgroundCssClass="modal-backdrop"
                    TargetControlID="Panel3" runat="server" PopupControlID="pnlAnalysisChart" CancelControlID="btnClAnalysisChart" />
                <asp:Panel ID="pnlAnalysisChart" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none; width: 900px; top: 30px;" Height="575px">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Panel ID="Panel3" runat="server" Height="475px" ScrollBars="Auto">
                                    <asp:DataGrid ID="lvDisplayAllocation" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-hover table-bordered" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:BoundColumn DataField="Name" HeaderText="" />
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Panel ID="achart" runat="server" Height="100%" Width="100%" ScrollBars="Auto">
                                    <asp:Chart ID="chAnalysis_Chart" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                        BorderlineWidth="2" AntiAliasing="All" Width="720px" Height="480px">
                                        <%--Height="475px" Style="width: 100%"--%>
                                        <Titles>
                                            <asp:Title Name="Title1">
                                            </asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column">
                                            </asp:Series>
                                            <asp:Series BorderWidth="3" ChartArea="ChartArea1" ChartType="Spline" LabelBorderWidth="3"
                                                MarkerSize="10" MarkerStyle="Diamond" Name="Series2">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1" Area3DStyle-WallWidth="5">
                                                <AxisY2 Enabled="True">
                                                </AxisY2>
                                                <Area3DStyle WallWidth="5" />
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnClAnalysisChart" runat="server" Text="Close" CssClass="btn btn-primary" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modal-backdrop" TargetControlID="Panel4"
                    runat="server" PopupControlID="pnlHPOChart" CancelControlID="btnClHpoClose" />
                <asp:Panel ID="pnlHPOChart" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none; width: 900px; top: 30px;" Height="575px">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Panel ID="Panel4" runat="server" Height="165px" ScrollBars="Auto">
                                    <asp:DataGrid ID="dgvCalibData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                        ShowHeader="false">
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                    BorderlineWidth="2" Width="890px" Height="300px" AntiAliasing="All">
                                    <Titles>
                                        <asp:Title Name="Title1">
                                        </asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                            MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                        </asp:Series>
                                        <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                            MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    <Legends>
                                        <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                        </asp:Legend>
                                    </Legends>
                                </asp:Chart>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnClHpoClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                    </div>
                </asp:Panel>
                <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                <uc1:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
