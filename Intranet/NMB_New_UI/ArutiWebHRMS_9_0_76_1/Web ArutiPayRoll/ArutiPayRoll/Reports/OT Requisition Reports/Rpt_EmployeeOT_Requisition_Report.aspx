﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" Title="Employee OT Requisition Report"
    CodeFile="Rpt_EmployeeOT_Requisition_Report.aspx.vb" Inherits="Reports_OT_Requisition_Reports_Rpt_EmployeeOT_Requisition_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee OT Requisition Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                                  <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEachEmpOnNewPage" runat="server" Checked="true" Text="Show Each Employee On New Page" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc1:AnalysisBy ID="popupAnalysisBy" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
