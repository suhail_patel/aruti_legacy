﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_OT_Requisition_Reports_Rpt_EmployeeOT_Requisition_Report
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Dim objEmpOTRequisition As New clsEmpOTRequisition_Report
    'Pinkal (03-Sep-2020) -- End
    Private mstrModuleName As String = "frmEmpOTRequisition_Report"
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty


#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            Dim objEmp As New clsEmployee_Master

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 dtpFromDate.GetDate.Date, _
                                                 dtpToDate.GetDate.Date, _
                                                 Session("UserAccessModeSetting").ToString, True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
            End With
            objEmp = Nothing

      
            Dim objStatus As New clsMasterData
            dsList = objStatus.getLeaveStatusList("Status", "", False, False, False)

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement OT Requisition  - OT Requisition Enhancement given by NMB .
            'Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid < 4", "", DataViewRowState.CurrentRows).ToTable()
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid in (0,1,2,3,6)", "", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (09-Mar-2020) -- End

            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With
            objStatus = Nothing


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            chkShowEachEmpOnNewPage.Checked = True
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByVal objEmpOTRequisition As clsEmpOTRequisition_Report) As Boolean
        'Pinkal (03-Sep-2020) -- End
        Try

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtpFromDate.GetDate = Nothing OrElse dtpToDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End

            objEmpOTRequisition.SetDefaultValue()

            objEmpOTRequisition._EmpId = CInt(cboEmployee.SelectedValue)
            objEmpOTRequisition._EmpName = cboEmployee.SelectedItem.Text
            objEmpOTRequisition._StatusId = CInt(cboStatus.SelectedValue)
            objEmpOTRequisition._Status = cboStatus.SelectedItem.Text
            objEmpOTRequisition._FromDate = dtpFromDate.GetDate.Date
            objEmpOTRequisition._ToDate = dtpToDate.GetDate.Date
            objEmpOTRequisition._ViewByIds = mstrViewByIds
            objEmpOTRequisition._ViewIndex = mintViewIndex
            objEmpOTRequisition._ViewByName = mstrViewByName
            objEmpOTRequisition._Analysis_Fields = mstrAnalysisFields
            objEmpOTRequisition._Analysis_Join = mstrAnalysisJoin
            objEmpOTRequisition._Analysis_OrderBy = mstrAnalysisOrderBy
            objEmpOTRequisition._Report_GroupName = mstrReportGroupName
            objEmpOTRequisition._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked

            GUI.fmtCurrency = Session("fmtCurrency").ToString()
            objEmpOTRequisition._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmpOTRequisition._UserUnkId = CInt(Session("UserId"))

            If Session("Employeeunkid") Is Nothing OrElse CInt(Session("Employeeunkid")) <= 0 Then
                objEmpOTRequisition._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            End If

            If CInt(Session("Employeeunkid")) > 0 Then
                objEmpOTRequisition._UserName = Session("DisplayName").ToString()
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objEmpOTRequisition._IncludeAccessFilterQry = False

            objEmpOTRequisition.setDefaultOrderBy(0)

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            If Not IsPostBack Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End
                SetLanguage()
                Call FillCombo()
                ResetValue()
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then lnkAnalysisBy.Visible = False
            Else
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objEmpOTRequisition As New clsEmpOTRequisition_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (03-Sep-2020) -- End
        Try

            SetDateFormat()


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objEmpOTRequisition) = False Then Exit Sub
            'Pinkal (03-Sep-2020) -- End



            objEmpOTRequisition.generateReportNew(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    dtpFromDate.GetDate.Date, _
                                                    dtpToDate.GetDate.Date, _
                                                    Session("UserAccessModeSetting").ToString, True, _
                                                    Session("ExportReportPath").ToString, _
                                                    CBool(Session("OpenAfterExport")), _
                                                    0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objEmpOTRequisition._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objEmpOTRequisition = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysisFields = popupAnalysisBy._Analysis_Fields
            mstrAnalysisJoin = popupAnalysisBy._Analysis_Join
            mstrAnalysisOrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReportGroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysisOrderByGName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblDetialHeader.Text)
            'Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.lnkAnalysisBy.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblStatus.ID, Me.LblStatus.Text)
            Me.chkShowEachEmpOnNewPage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowEachEmpOnNewPage.ID, Me.chkShowEachEmpOnNewPage.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
