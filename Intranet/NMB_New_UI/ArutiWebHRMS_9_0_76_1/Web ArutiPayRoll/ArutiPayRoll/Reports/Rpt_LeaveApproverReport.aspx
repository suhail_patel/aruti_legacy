﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_LeaveApproverReport.aspx.vb" Inherits="Reports_Rpt_LeaveApproverReport"
    Title="Leave Approver Report" %>

<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:Panel ID="Panel1" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                   <div class="row clearfix d--f fd--c ai--c">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                         <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approver Report"></asp:Label>
                                    </h2>
                                     <ul class="header-dropdown m-r--5">
                                             <li class="dropdown">
                                               <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                                     <i class="fas fa-filter"></i>
                                                </asp:LinkButton>
                                          </li>
                                     </ul>
                                </div>
                                <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="LblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                                     <div class="form-group">
                                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                                     </div>
                                            </div>
                                        </div>    
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblApprover" runat="server" Text="Leave Approver" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                             <asp:DropDownList ID="cboApprover" runat="server" />
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
                                                     <asp:Label ID="LblLevel" runat="server" Text="Approver Level" CssClass="form-label"></asp:Label>
                                                     <div class="form-group">
                                                             <asp:DropDownList ID="cboLevel" runat="server" />
                                                     </div>
                                            </div>
                                       </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                     <div class="form-group">
                                                             <asp:DropDownList ID="cboEmployee" runat="server" />
                                                    </div>
                                            </div>
                                       </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                                    <asp:Label ID="LblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                     <div class="form-group">
                                                            <asp:DropDownList ID="cboLeaveType" runat="server" />
                                                     </div>
                                            </div>
                                      </div>             
                                </div>
                                <div class="footer">
                                       <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                       <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                       <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                </div>
                             </div>
                         </div>
                    </div>            
                
                    <%--<div class="panel-primary">
                        <div class="panel-heading">
                           
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               
                                            </td>
                                            <td style="width: 75%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <uc6:AnalysisBy ID="popAnalysisby" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    
</asp:Content>

