﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" Title="Employee Claim Expense Balance Listing Report"
    AutoEventWireup="false" CodeFile="Rpt_EmpClaimBalanceList_Report.aspx.vb" Inherits="Reports_Claim_Request_Reports_Rpt_EmpClaimBalanceList_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <asp:Panel ID="Panel1" runat="server" >
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
            
               <div class="row clearfix d--f fd--c ai--c">
                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                         <h2>
                                             <asp:Label ID="lblPageHeader" runat="server" Text="Employee Claim Expense Balance Listing Report"></asp:Label>
                                     </h2>
                                      <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                      <asp:LinkButton ID="lnkSetAnalysis" runat="server" ToolTip="Analysis By">
                                                            <i class="fas fa-filter"></i>
                                                    </asp:LinkButton>
                                                </li>
                                        </ul>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                  <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>  
                                                   <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                        </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                         </div>
                                    </div>
                                    <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblExpenseCategory" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                  <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true" />
                                                        </div>
                                                </div>
                                    </div>
                                    <div class="row clearfix">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="false" />
                                                        </div>
                                             </div>
                                    </div>
                                    <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                         <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="false" />
                                                </div>
                                           </div>   
                                    </div>
                                    <div class="row clearfix">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <asp:Label ID="LblPayableAmount" runat="server" Text="Payable Amount" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                 <asp:DropDownList ID="cboFromCondition" runat="server" AutoPostBack="false" />
                                                        </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-t-20">
                                                        <div class="form-group">
                                                                    <div class="form-line">
                                                                            <asp:TextBox ID="txtFromExpenseAmt" runat="server" Text="0" Style="text-align: right" onKeypress="return onlyNumbers(this, event)" class="form-control"></asp:TextBox>     
                                                                     </div>
                                                         </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-t-20">
                                                        <div class="form-group">
                                                              <asp:DropDownList ID="cboToCondition" runat="server" AutoPostBack="false" />
                                                         </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-t-20">
                                                        <div class="form-group">
                                                              <div class="form-line">
                                                                     <asp:TextBox ID="txtToExpenseAmt" runat="server" Text="0" Style="text-align: right"   onKeypress="return onlyNumbers(this, event)" class="form-control"></asp:TextBox>
                                                              </div>
                                                         </div>     
                                                </div>
                                      </div>
                                 </div>     
                                <div class="footer">   
                                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                    <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                </div>
                             </div>
                         </div>
                 </div>           
            
            
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <%--<asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                <div style="text-align: right">
                                    
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table width="100%">
                                    <tr style="width: 100%">
                                        <td style="width: 25%">
                                            
                                        </td>
                                        <td style="width: 30%">
                                           
                                        </td>
                                        <td style="width: 15%; text-align: left">
                                            
                                        </td>
                                        <td style="width: 30%">
                                            
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            
                                        </td>
                                        <td colspan="3">
                                          
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            
                                        </td>
                                        <td colspan="3">
                                            
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                           
                                        </td>
                                        <td colspan="3">
                                           
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr style="width: 100%">
                                        <td style="width: 25%">
                                            
                                        </td>
                                        <td style="width: 20%">
                                           
                                        </td>
                                        <td style="width: 18%">
                                            
                                        </td>
                                        <td style="width: 20%">
                                          
                                        </td>
                                        <td style="width: 22%">
                                           
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                  
                                </div>
                            </div>
                        </div>
                       
                    </div>--%>
                     <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
