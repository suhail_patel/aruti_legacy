﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_EmployeeClaimForm.aspx.vb"
    Inherits="Reports_Claim_Request_Reports_Rpt_EmployeeClaimForm" Title="Employee Claim Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Claim Form"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="true" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblToDate" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblExpenseCategory" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row cl   earfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblClaimform" runat="server" Text="Claim Form" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboClaimForm" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlSpecialAllownce" runat="server">
                                            <asp:Label ID="LblSpecialAllowance" runat="server" Text="Select Special Allowance(s)"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtFilterAllowances" runat="server" class="form-control"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <asp:Panel ID="pnlTranHead" runat="server">
                                        <div class="card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 200px">
                                                                <asp:DataGrid ID="dgTranHeads" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                    CssClass="table table-hover table-bordered">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                                    CssClass="filled-in" Text=" " />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"
                                                                                    CssClass="filled-in" Text=" " />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                            <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="code" HeaderText="code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                            FooterText="dgcolhTranHeadCode">
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="name" HeaderText="Tran. Head" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                            FooterText="dgcolhTranHead">
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="tranheadunkid" HeaderText="tranheadunkid" ReadOnly="true"
                                                                            Visible="false"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
