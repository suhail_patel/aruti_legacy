﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Claim_Request_Reports_Rpt_EmpClaimBalanceList_Report
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmployeeClaimBalanceList"

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objEmpClaimBalanceReport As clsEmployeeClaimBalanceReport
    'Pinkal (05-Sep-2020) -- End

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objEmpClaimBalanceReport = New clsEmployeeClaimBalanceReport
            'Pinkal (05-Sep-2020) -- End



            If Not IsPostBack Then
                SetLanguage()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End
                FillCombo()
                ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master

        Try

            objEmp = New clsEmployee_Master
            dsCombo = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmp = Nothing
            'Pinkal (05-Sep-2020) -- End


            Dim objMaster As New clsMasterData
            dsCombo = objMaster.GetCondition(True, True, True, False, False)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


            cboFromCondition.DataTextField = "Name"
            cboFromCondition.DataValueField = "id"
            cboFromCondition.DataSource = dsCombo.Tables(0)
            cboFromCondition.DataBind()

            cboToCondition.DataTextField = "Name"
            cboToCondition.DataValueField = "id"
            cboToCondition.DataSource = dsCombo.Tables(0).Copy
            cboToCondition.DataBind()


            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            cboExpenseCategory.DataValueField = "Id"
            cboExpenseCategory.DataTextField = "Name"
            cboExpenseCategory.DataSource = dsCombo.Tables(0)
            cboExpenseCategory.DataBind()
            cboExpenseCategory.SelectedValue = "0"

            cboExpenseCategory_SelectedIndexChanged(New Object(), New EventArgs())


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.SetDate = DateTime.Now
            dtpToDate.SetDate = DateTime.Now
            cboEmployee.SelectedValue = "0"
            cboExpenseCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboFromCondition.SelectedIndex = 0
            txtFromExpenseAmt.Text = "0"
            cboToCondition.SelectedIndex = 0
            txtToExpenseAmt.Text = "0"
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objEmpClaimBalanceReport.setDefaultOrderBy(cboExpenseCategory.SelectedIndex)
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Function SetFilter() As Boolean
    Private Function SetFilter(ByRef objEmpClaimBalanceReport As clsEmployeeClaimBalanceReport) As Boolean
        'Pinkal (05-Sep-2020) -- End

        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Return False
            End If

            objEmpClaimBalanceReport.SetDefaultValue()

            objEmpClaimBalanceReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEmpClaimBalanceReport._EmployeeName = cboEmployee.SelectedItem.Text
            objEmpClaimBalanceReport._ExpenseCategoryId = CInt(cboExpenseCategory.SelectedValue)
            objEmpClaimBalanceReport._ExpenseCategory = cboExpenseCategory.SelectedItem.Text
            objEmpClaimBalanceReport._ExpenseId = CInt(cboExpense.SelectedValue)
            objEmpClaimBalanceReport._Expense = cboExpense.SelectedItem.Text
            objEmpClaimBalanceReport._FromConditionID = CInt(cboFromCondition.SelectedValue)
            objEmpClaimBalanceReport._FromCondition = cboFromCondition.SelectedItem.Text
            objEmpClaimBalanceReport._FromAmount = CDec(IIf(txtFromExpenseAmt.Text.Trim.Length <= 0, 0, txtFromExpenseAmt.Text.Trim))
            objEmpClaimBalanceReport._ToConditionID = CInt(cboToCondition.SelectedValue)
            objEmpClaimBalanceReport._ToCondition = cboToCondition.SelectedItem.Text
            objEmpClaimBalanceReport._ToAmount = CDec(IIf(txtToExpenseAmt.Text.Trim.Length <= 0, 0, txtToExpenseAmt.Text.Trim))
            objEmpClaimBalanceReport._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            objEmpClaimBalanceReport._ReportId = cboExpenseCategory.SelectedIndex
            objEmpClaimBalanceReport._ReportTypeName = cboExpenseCategory.SelectedItem.Text
            objEmpClaimBalanceReport._FromDate = dtpFromDate.GetDate.Date
            objEmpClaimBalanceReport._ToDate = dtpToDate.GetDate.Date
            objEmpClaimBalanceReport._ViewByIds = mstrStringIds
            objEmpClaimBalanceReport._ViewIndex = mintViewIdx
            objEmpClaimBalanceReport._ViewByName = mstrStringName
            objEmpClaimBalanceReport._Analysis_Fields = mstrAnalysis_Fields
            objEmpClaimBalanceReport._Analysis_Join = mstrAnalysis_Join
            objEmpClaimBalanceReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpClaimBalanceReport._Report_GroupName = mstrReport_GroupName

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Function

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objEmpClaimBalanceReport As New clsEmployeeClaimBalanceReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (05-Sep-2020) -- End
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objEmpClaimBalanceReport) = False Then Exit Sub
            'Pinkal (05-Sep-2020) -- End



            objEmpClaimBalanceReport._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmpClaimBalanceReport._UserUnkId = CInt(Session("UserId"))
            objEmpClaimBalanceReport._UserAccessFilter = Session("AccessLevelFilterString").ToString()
            FinancialYear._Object._YearUnkid = CInt(Session("Fin_year"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString()
            objEmpClaimBalanceReport.setDefaultOrderBy(0)


            SetDateFormat()

            objEmpClaimBalanceReport.generateReportNew(Session("Database_Name").ToString(), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        dtpFromDate.GetDate.Date, _
                                                        dtpToDate.GetDate.Date, _
                                                        Session("UserAccessModeSetting").ToString(), True, _
                                                        Session("ExportReportPath").ToString(), _
                                                        CBool(Session("OpenAfterExport")), _
                                                        0, enPrintAction.None, enExportAction.None, _
                                                        CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objEmpClaimBalanceReport._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmpClaimBalanceReport = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Gajanan [17-Sep-2020] -- Start
    'New UI Change
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [17-Sep-2020] -- End

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkSetAnalysis_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAnalysisBy_buttonApply_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAnalysisBy_buttonClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim objExpMst As New clsExpense_Master
            Dim dsCombo As DataSet = objExpMst.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", CInt(cboEmployee.SelectedValue), False)
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "Id <=0 OR isaccrue = 1", "", DataViewRowState.CurrentRows).ToTable
            cboExpense.DataTextField = "Name"
            cboExpense.DataValueField = "Id"
            cboExpense.DataSource = dtTable
            cboExpense.DataBind()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            objExpMst = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lnkSetAnalysis.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.ToolTip)
            'Gajanan [17-Sep-2020] -- End


            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFromDate.ID, Me.lblFromDate.Text)

            Me.LblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpense.ID, Me.LblExpense.Text)
            Me.LblPayableAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblPayableAmount.ID, Me.LblPayableAmount.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblToDate.ID, Me.lblToDate.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub




End Class
