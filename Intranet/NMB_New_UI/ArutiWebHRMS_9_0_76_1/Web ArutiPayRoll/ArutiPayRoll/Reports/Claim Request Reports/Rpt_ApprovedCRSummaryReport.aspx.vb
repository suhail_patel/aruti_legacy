﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Claim_Request_Reports_Rpt_ApprovedCRSummaryReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmApprovedCRSummaryReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintCountryunkid As Integer = 0
#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try

            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), _
                                                True, False, "Emp", True)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With
            ObjEmp = Nothing


            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            cboExpenseCategory_SelectedIndexChanged(Nothing, Nothing)

            dsCombos = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With

        
            dsCombos = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombos = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With

            Dim drCurrencySign = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("isbasecurrency") = 1).Select(Function(x) x.Field(Of Integer)("countryunkid"))

            If drCurrencySign.Count > 0 Then
                mintCountryunkid = CInt(drCurrencySign(0))
                cboCurrency.SelectedValue = mintCountryunkid
            End If

            objExchange = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboExpenseCategory.SelectedValue = 0
            dtpApprovedFromDate.SetDate = Nothing
            dtpApprovedToDate.SetDate = Nothing
            cboExpense.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboUOM.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = mintCountryunkid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter(ByRef ObjApprovedCRSummary As clsApprovedCRSummaryReport) As Boolean
        Try
            ObjApprovedCRSummary.SetDefaultValue()

            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Expense Category is compulsory information.please select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Return False

            ElseIf CInt(cboUOM.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "UOM is compulsory information.please select UOM."), Me)
                cboUOM.Focus()
                Return False

            End If

            If Not dtpApprovedFromDate.IsNull Then
                ObjApprovedCRSummary._ApprovedFromDate = dtpApprovedFromDate.GetDate.Date
            Else
                ObjApprovedCRSummary._ApprovedFromDate = Nothing
            End If
            If Not dtpApprovedToDate.IsNull Then
                ObjApprovedCRSummary._ApprovedToDate = dtpApprovedToDate.GetDate.Date
            Else
                ObjApprovedCRSummary._ApprovedToDate = Nothing
            End If

            ObjApprovedCRSummary._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            ObjApprovedCRSummary._ExpCateName = cboExpenseCategory.SelectedItem.Text.ToString()
            ObjApprovedCRSummary._EmpUnkId = CInt(cboEmployee.SelectedValue)
            ObjApprovedCRSummary._EmpName = cboEmployee.SelectedItem.Text.ToString
            ObjApprovedCRSummary._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            ObjApprovedCRSummary._UserUnkid = CInt(Session("UserId"))
            ObjApprovedCRSummary._CompanyUnkId = Company._Object._Companyunkid
            ObjApprovedCRSummary._UOMId = CInt(cboUOM.SelectedValue)
            ObjApprovedCRSummary._UOM = cboUOM.SelectedItem.Text
            ObjApprovedCRSummary._ExpenseID = CInt(cboExpense.SelectedValue)
            ObjApprovedCRSummary._Expense = cboExpense.SelectedItem.Text
            ObjApprovedCRSummary._ViewByIds = mstrStringIds
            ObjApprovedCRSummary._ViewIndex = mintViewIdx
            ObjApprovedCRSummary._ViewByName = mstrStringName
            ObjApprovedCRSummary._Analysis_Fields = mstrAnalysis_Fields
            ObjApprovedCRSummary._Analysis_Join = mstrAnalysis_Join
            ObjApprovedCRSummary._Analysis_OrderBy = mstrAnalysis_OrderBy
            ObjApprovedCRSummary._Report_GroupName = mstrReport_GroupName
            GUI.fmtCurrency = Session("fmtCurrency").ToString()
            ObjApprovedCRSummary._CurrencyId = CInt(cboCurrency.SelectedValue)

            If CInt(cboUOM.SelectedValue) = enExpUoM.UOM_AMOUNT Then
                If CInt(cboCurrency.SelectedValue) <= 0 Then
                    ObjApprovedCRSummary._Currency = ""
                Else
                    ObjApprovedCRSummary._Currency = cboCurrency.SelectedItem.Text
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
                SetLanguage()
                GC.Collect()
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mintCountryunkid = ViewState("BaseCurrencyId").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            Me.ViewState.Add("BaseCurrencyId", mintCountryunkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim ObjApprovedCRSummary As New clsApprovedCRSummaryReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If SetFilter(ObjApprovedCRSummary) = False Then Exit Sub

            ObjApprovedCRSummary.setDefaultOrderBy(0)

            Call SetDateFormat()

            ObjApprovedCRSummary.generateReportNew(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           Session("ExportReportPath").ToString, _
                                           CBool(Session("OpenAfterExport")), _
                                           0, Aruti.Data.enPrintAction.None, enExportAction.None)

            Session("objRpt") = ObjApprovedCRSummary._Rpt

            If ObjApprovedCRSummary._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            ObjApprovedCRSummary = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim objExpMst As New clsExpense_Master
            Dim dsCombo As DataSet = objExpMst.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            cboExpense.DataTextField = "Name"
            cboExpense.DataValueField = "Id"
            cboExpense.DataSource = dsCombo.Tables(0)
            cboExpense.DataBind()
            cboExpense.SelectedValue = 0

            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objExpMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboUOM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUOM.SelectedIndexChanged
        Try
            If CInt(cboUOM.SelectedValue) = enExpUoM.UOM_QTY Then
                cboCurrency.SelectedValue = 0
                cboCurrency.Enabled = False
            ElseIf CInt(cboUOM.SelectedValue) = enExpUoM.UOM_AMOUNT Then
                cboCurrency.Enabled = True
                cboCurrency.SelectedValue = mintCountryunkid
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Other Controls Events "

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.ToolTip)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblApprovedFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprovedFromDate.ID, Me.lblApprovedFromDate.Text)
            Me.lblApprovedToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprovedToDate.ID, Me.lblApprovedToDate.Text)
            Me.LblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpense.ID, Me.LblExpense.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblUOM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblUOM.ID, Me.LblUOM.Text)
            Me.LblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblCurrency.ID, Me.LblCurrency.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Expense Category is compulsory information.please select Expense Category.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "UOM is compulsory information.please select UOM.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
