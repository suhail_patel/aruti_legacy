﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region
Partial Class Reports_Claim_Request_Reports_Rpt_EmployeeClaimForm
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimForm As New clsEmployeeClaimForm
    'Pinkal (05-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmEmployeeClaimForm"
    Private dtTranHead As DataTable = Nothing
#End Region

#Region " Private Function "
    Public Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet

            Dim intEmployeeID As Integer = 0
            Dim blnSelect As Boolean = True
            Dim blnIncludeAccessFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                intEmployeeID = CInt(Session("Employeeunkid"))
                blnIncludeAccessFilter = False
                blnSelect = False
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                          Session("UserAccessModeSetting").ToString(), _
                                          True, CBool(Session("IsIncludeInactiveEmp")), "Emp", blnSelect, intEmployeeID, , , , , , , , , , , , , , , , blnIncludeAccessFilter)

            cboEmployee.DataValueField = "employeeunkid"
            cboEmployee.DataTextField = "EmpCodeName"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.DataBind()
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                cboEmployee.SelectedValue = CStr(Session("Employeeunkid"))
            Else
                cboEmployee.SelectedValue = "0"
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmployee = Nothing
            'Pinkal (05-Sep-2020) -- End


            dsList = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            cboExpenseCategory.DataSource = dsList.Tables(0)
            cboExpenseCategory.DataValueField = "Id"
            cboExpenseCategory.DataTextField = "Name"
            cboExpenseCategory.SelectedValue = "0"
            cboExpenseCategory.DataBind()

            cboExpenseCategory_SelectedValueChanged(Nothing, Nothing)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            'Pinkal (18-Jun-2020) -- End
            cboEmployee.SelectedIndex = 0
            cboExpenseCategory.SelectedIndex = 0
            cboClaimForm.SelectedIndex = 0
            txtFilterAllowances.Text = ""
            chkShowEmpScale.Checked = False
            FillTranHead()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function Validation() As Boolean

        Dim mblnFlag As Boolean = False
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Expese Category is compulsory information.Please Select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Return False


            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), Me)
                cboExpenseCategory.Focus()
                Return False

            ElseIf CInt(cboClaimForm.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Claim Form is compulsory information.Please Select Claim Form."), Me)
                cboClaimForm.Focus()
                Return False

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objClaimForm As clsEmployeeClaimForm) As Boolean
        'Pinkal (05-Sep-2020) -- End
        Try
            objClaimForm.SetDefaultValue()

            objClaimForm._EmployeeId = CInt(cboEmployee.SelectedValue)
            objClaimForm._EmployeeName = cboEmployee.SelectedItem.Text
            objClaimForm._ExpenseCategoryId = CInt(cboExpenseCategory.SelectedValue)
            objClaimForm._ExpenseCategory = cboExpenseCategory.SelectedItem.Text
            objClaimForm._ClaimFormId = CInt(cboClaimForm.SelectedValue)
            objClaimForm._ClaimFormName = cboClaimForm.SelectedItem.Text

            objClaimForm._YearId = CInt(Session("Fin_year"))
            objClaimForm._Fin_StartDate = FinancialYear._Object._Database_Start_Date.Date
            objClaimForm._Fin_Enddate = FinancialYear._Object._Database_End_Date.Date

            Dim objClaimRequest As New clsclaim_request_master
            objClaimRequest._Crmasterunkid = CInt(cboClaimForm.SelectedValue)


            'Pinkal (19-Jul-2021)-- Start
            'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
            If Session("CompName").ToString().ToUpper() = "KADCO" Then
                If objClaimRequest._Statusunkid = 1 AndAlso objClaimRequest._IsPrinted = False Then
                    objClaimForm._IsPrinted = True
                    objClaimForm._PrintedDateTime = ConfigParameter._Object._CurrentDateAndTime
                    objClaimForm._PrintUserId = CInt(Session("UserId"))
                    objClaimForm._PrintedIp = CStr(Session("IP_ADD"))
                    objClaimForm._PrintedHost = CStr(Session("HOST_NAME"))
                    objClaimForm._IsPrintFromWeb = True
                Else
                    objClaimForm._IsPrinted = False
                    objClaimForm._PrintedDateTime = Nothing
                    objClaimForm._PrintUserId = -1
                    objClaimForm._PrintedIp = ""
                    objClaimForm._PrintedHost = ""
                    objClaimForm._IsPrintFromWeb = False
                End If
            End If
            'Pinkal (19-Jul-2021)-- End


            Dim objMasterData As New clsMasterData
            Dim mintPeriodID As Integer = objMasterData.getCurrentTnAPeriodID(enModuleReference.Payroll, objClaimRequest._Transactiondate.Date)
            objMasterData = Nothing

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = mintPeriodID
            objClaimForm._PeiordID = mintPeriodID
            objClaimForm._PeriodStartDate = objPeriod._TnA_StartDate.Date
            objClaimForm._PeriodEndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing
            objClaimRequest = Nothing

            If CInt(cboExpenseCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                objClaimForm._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                objClaimForm._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
                objClaimForm._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            End If

            If dtTranHead IsNot Nothing AndAlso dtTranHead.Rows.Count > 0 Then
                objClaimForm._SpecialAllowanceIds = String.Join(",", dtTranHead.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).Select(Function(y) y.Field(Of Integer)("tranheadunkid").ToString()).ToArray())
            End If

            objClaimForm._ShowEmployeeScale = chkShowEmpScale.Checked

            objClaimForm._UserUnkId = CInt(Session("UserId"))
            objClaimForm._CompanyUnkId = CInt(Session("Companyunkid"))
            objClaimForm._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString()


            'Pinkal (28-Jan-2021)-- Start
            'Bug  -  Bug Resolved from Kadco.
            If CInt(Session("Employeeunkid")) > 0 Then
                objClaimForm._UserName = Session("DisplayName").ToString()
            End If
            'Pinkal (28-Jan-2021) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillTranHead()
        Dim objTranHead As clsTransactionHead = Nothing
        Try
            objTranHead = New clsTransactionHead
            Dim dsList As DataSet = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, False, False, "typeof_id <> " & enTypeOf.Salary, False, False, False)

            Dim dcColumn As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
            dcColumn.DefaultValue = False
            dsList.Tables(0).Columns.Add(dcColumn)



            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dtTranHead = dsList.Tables(0)
            dtTranHead = dsList.Tables(0).Copy()
            'Pinkal (05-Sep-2020) -- End

            dgTranHeads.DataSource = dtTranHead
            dgTranHeads.DataBind()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTranHead = Nothing
        End Try
    End Sub
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If



            If Not IsPostBack Then
                SetLanguage()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                Call FillCombo()
                Call ResetValue()
                Call FillTranHead()

                If Session("CompName").ToString.Trim().ToUpper() = "ABOOD GROUP OF COMPANIES." OrElse _
               Session("CompName").ToString.Trim().ToUpper() = "GOOD NEIGHBORS TANZANIA" Then

                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    pnlSpecialAllownce.Visible = True
                    'Gajanan [17-Sep-2020] -- End

                    LblSpecialAllowance.Visible = True
                    txtFilterAllowances.Visible = True
                    txtFilterAllowances.Enabled = True
                    dgTranHeads.Visible = True
                    dgTranHeads.Enabled = True
                    chkShowEmpScale.Visible = CBool(Session("AllowToViewScale"))
                    chkShowEmpScale.Enabled = CBool(Session("AllowToViewScale"))
                Else
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    pnlSpecialAllownce.Visible = False
                    'Gajanan [17-Sep-2020] -- End
                    pnlTranHead.Visible = False
                    LblSpecialAllowance.Visible = False
                    txtFilterAllowances.Visible = False
                    txtFilterAllowances.Enabled = False
                    dgTranHeads.Visible = False
                    dgTranHeads.Enabled = False
                    chkShowEmpScale.Visible = False
                    chkShowEmpScale.Enabled = False
                End If
            Else
                dtTranHead = CType(ViewState("dtTranHead"), DataTable)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Try
            ViewState.Add("dtTranHead", dtTranHead)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Pinkal (05-Sep-2020) -- End
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim objClaimForm As New clsEmployeeClaimForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If Validation() = False Then Exit Sub


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            ' If SetFilter() = False Then Exit Sub
            If SetFilter(objClaimForm) = False Then Exit Sub
            'Pinkal (05-Sep-2020) -- End



            Call SetDateFormat()


            objClaimForm.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                              , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CStr(Session("UserAccessModeSetting")) _
                                                              , True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), 0, Aruti.Data.enPrintAction.None, enExportAction.None, 0)

            Session("objRpt") = objClaimForm._Rpt

            If objClaimForm._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objClaimForm = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox"

    Private Sub cboExpenseCategory_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objClaimRequest As New clsclaim_request_master
            Dim dtList As DataTable
            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                Dim mstrSearch = "CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date).ToString() & "' AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date).ToString() & "'"
                dtList = objClaimRequest.GetEmployeeClaimForm(CInt(cboEmployee.SelectedValue), CInt(cboExpenseCategory.SelectedValue), True, mstrSearch)
            Else
                dtList = objClaimRequest.GetEmployeeClaimForm(CInt(cboEmployee.SelectedValue), CInt(cboExpenseCategory.SelectedValue), True)
            End If
            'Pinkal (30-May-2020) -- end
            cboClaimForm.DataSource = dtList
            cboClaimForm.DataTextField = "claimrequestno"
            cboClaimForm.DataValueField = "crmasterunkid"
            cboClaimForm.SelectedValue = "0"
            cboClaimForm.DataBind()
            objClaimRequest = Nothing

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtList IsNot Nothing Then dtList.Clear()
            dtList = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"
    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            Dim dvTranHead As DataView = dtTranHead.DefaultView

            dvTranHead.RowFilter = "code like '%" & txtFilterAllowances.Text.Trim & "%' OR name like '%" & txtFilterAllowances.Text.Trim & "%' "

            For i As Integer = 0 To dvTranHead.ToTable.Rows.Count - 1
                Dim gvRow As DataGridItem = dgTranHeads.Items(i)
                CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                Dim dRow() As DataRow = dtTranHead.Select("tranheadunkid = '" & gvRow.Cells(3).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("IsChecked") = chkSelectAll.Checked
                End If
                dvTranHead.Table.AcceptChanges()
            Next
            dtTranHead = dvTranHead.Table

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

            Dim drRow() As DataRow = dtTranHead.Select("tranheadunkid = " & item.Cells(3).Text)
            If drRow.Length > 0 Then
                drRow(0).Item("IsChecked") = chkSelect.Checked
            End If
            dtTranHead.AcceptChanges()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " TextBox Events "
    Protected Sub txtFilterAllowances_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilterAllowances.TextChanged
        Try
            If dtTranHead IsNot Nothing Then
                Dim dvFilterAllowancesSearch As DataView = dtTranHead.DefaultView
                If dvFilterAllowancesSearch.Table.Rows.Count > 0 Then
                    If txtFilterAllowances.Text.Trim.Length > 0 Then
                        dvFilterAllowancesSearch.RowFilter = "code like '%" & txtFilterAllowances.Text.Trim & "%' OR name like '%" & txtFilterAllowances.Text.Trim & "%' "
                    End If
                    dgTranHeads.DataSource = dvFilterAllowancesSearch.ToTable
                    dgTranHeads.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub
#End Region

    'Pinkal (30-May-2020) -- Start
    'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.

#Region "DatePicker Events"

    Private Sub dtpFromDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged, dtpToDate.TextChanged
        Try
            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                cboExpenseCategory_SelectedValueChanged(cboExpenseCategory, New EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (30-May-2020) -- End


    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblClaimform.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblClaimform.ID, Me.LblClaimform.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.LblSpecialAllowance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblSpecialAllowance.ID, Me.LblSpecialAllowance.Text)
            Me.chkShowEmpScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowEmpScale.ID, Me.chkShowEmpScale.Text)

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblToDate.ID, Me.LblToDate.Text)
            'Pinkal (30-May-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
End Class
