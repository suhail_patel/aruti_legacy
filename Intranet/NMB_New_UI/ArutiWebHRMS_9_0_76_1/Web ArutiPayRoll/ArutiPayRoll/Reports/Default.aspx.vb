﻿
Partial Class Reports_Default
    Inherits System.Web.UI.Page
    Dim objError As New CommonCodes
    Dim blnFlag As Boolean = True

    Protected Sub btnOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        If Session("ExFileName") <> Nothing Then
            Dim file As New IO.FileInfo(Session("ExFileName"))
            'write it to the browser
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/x-msexcel"
            Response.WriteFile(Session("ExFileName"))
            Response.Flush()
            System.IO.File.Delete(Session("ExFileName"))
            Session("ExFileName") = Nothing
            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            ApplicationInstance.CompleteRequest()
            Response.End()
            'Pinkal (11-Feb-2022) -- End
        Else
            objError.DisplayMessage("Please Export again.", Me)

        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("clsuser") Is Nothing Then

            Exit Sub
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Page.Theme = Session("Theme")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'If Session("ExFileName") <> Nothing Then
        '    If IO.File.Exists(Session("ExFileName")) Then
        '        IO.File.Delete(Session("ExFileName"))
        '    End If
        '    Session("ExFileName") = Nothing
        'End If
    End Sub
End Class
