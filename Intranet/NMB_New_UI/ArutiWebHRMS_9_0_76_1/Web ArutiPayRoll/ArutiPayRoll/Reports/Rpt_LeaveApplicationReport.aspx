﻿<%@ Page Title="Leave Application Report" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_LeaveApplicationReport.aspx.vb" Inherits="Reports_Rpt_LeaveApplicationReport" %>

<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx"  TagName="Export" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:Panel ID="MainPan" runat="server" >
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                      <div class="row clearfix d--f fd--c ai--c">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Application Report"></asp:Label>
                                    </h2>
                                     <ul class="header-dropdown m-r--5">
                                             <li class="dropdown">
                                                 <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip ="Analysis By">
                                                     <i class="fas fa-filter"></i>
                                                </asp:LinkButton>
                                          </li>
                                     </ul>
                                </div>
                                 <div class="body">
                                         <div class="row clearfix">
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                      <asp:Label ID="LblApplicationFrom" runat="server" Text="Application From" CssClass="form-label"></asp:Label>
                                                      <uc2:DateCtrl ID="dtpApplicationFrom" runat="server"/>
                                               </div>
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="LblApplicationTo" runat="server" Text="Application To" CssClass="form-label"></asp:Label>
                                                     <uc2:DateCtrl ID="dtpApplicationTo" runat="server"/>
                                               </div>
                                         </div>   
                                         <div class="row clearfix">
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpStartDate" runat="server"/>
                                               </div>
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                      <asp:Label ID="LblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                                       <uc2:DateCtrl ID="dtpEndDate" runat="server"/>
                                               </div>
                                         </div> 
                                         <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"  CssClass="form-label"></asp:Label>
                                                         <div class="form-group">
                                                                <asp:DropDownList ID="cboEmployee" runat="server" />
                                                         </div>
                                                </div>
                                         </div>
                                         <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                         <asp:Label ID="lblLeaveName" runat="server" Text="Leave" CssClass="form-label"></asp:Label>
                                                         <div class="form-group">
                                                                  <asp:DropDownList ID="cboLeave" runat="server" />
                                                         </div>
                                                </div>
                                          </div>       
                                         <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                          <asp:Label ID="LblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                          <div class="form-group">
                                                                <asp:DropDownList ID="cboStatus" runat="server" />
                                                          </div>
                                                </div>
                                          </div> 
                                 </div>
                                  <div class="footer">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                  </div>
                             </div>
                        </div>
                    </div>            
                
                   <%-- <div class="panel-primary">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                       
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                      <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 25%">
                                              
                                            </td>
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 25%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 25%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td  colspan = "3">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               
                                            </td>
                                             <td  colspan = "3">
                                              
                                            </td>
                                        </tr>
                                       <tr style="width: 100%">
                                            <td style="width: 25%">
                                              
                                            </td>
                                             <td  colspan = "3">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <uc1:AnalysisBy ID="popAnalysisby" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                 <Triggers>
                         <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
