﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Claim_Retirement_Reports_Rpt_ImprestBalanceReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmImprestBalanceReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Imprest Retired Balance Report"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Imprest UnRetired Balance Report"))
            cboReportType.SelectedIndex = 0

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         Session("UserAccessModeSetting").ToString, True, _
                                                         CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                         blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)



            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Employee")
                .DataBind()
            End With
            ObjEmp = Nothing

            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            cboExpenseCategory_SelectedIndexChanged(Nothing, Nothing)

            FillStatus()

            Dim objPeriod As New clscommom_period_Tran
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "Period", True)

            With cboFromPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            With cboToPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
                .DataBind()
            End With
            objPeriod = Nothing

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            dsCombos = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombos = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            objExchange = Nothing
            'Pinkal (30-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub FillStatus()
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMasterData.getLeaveStatusList("List", "")

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable

            Dim dRow As DataRow = dtab.NewRow
            dRow("statusunkid") = 999
            dRow("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Not Retired")
            dtab.Rows.InsertAt(dRow, dtab.Rows.Count)

            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtab
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMasterData = Nothing
            dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboReportType_SelectedIndexChanged(cboReportType, New EventArgs())
            dtpTranFromDate.SetDate = Nothing
            dtpTranToDate.SetDate = Nothing
            cboEmployee.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            chkShowRetirementStatus.Checked = True
            cboFromPeriod.SelectedValue = 0
            cboToPeriod.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            cboExpense.SelectedValue = 0
            dtpTranFromDate.Enabled = True
            dtpTranToDate.Enabled = True
            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            cboCurrency.SelectedValue = 0
            'Pinkal (30-Mar-2021) -- End

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            chkShowCRetirementNo.Checked = True
            'S.SANDEEP |21-FEB-2022| -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter(ByRef ObjImprest As clsImprestBalanceReport) As Boolean
        Try
            ObjImprest.SetDefaultValue()

            If (Not dtpTranFromDate.IsNull AndAlso dtpTranToDate.IsNull) OrElse (dtpTranFromDate.IsNull AndAlso Not dtpTranToDate.IsNull) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue."), Me)
                Return False


            ElseIf CInt(cboFromPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select From Period."), Me)
                cboFromPeriod.Focus()
                Exit Function

            ElseIf CInt(cboFromPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please Select To Period."), Me)
                cboToPeriod.Focus()
                Exit Function

            ElseIf cboToPeriod.SelectedIndex < cboFromPeriod.SelectedIndex Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " To Period cannot be less than From Period."), Me)
                cboToPeriod.Focus()
                Exit Function
            End If

            ObjImprest._ReportTypeId = CInt(cboReportType.SelectedIndex)
            ObjImprest._ReportType = cboReportType.SelectedItem.Text

            If Not dtpTranFromDate.IsNull Then
                ObjImprest._FromDate = dtpTranFromDate.GetDate.Date
            Else
                ObjImprest._FromDate = Nothing
            End If
            If Not dtpTranToDate.IsNull Then
                ObjImprest._ToDate = dtpTranToDate.GetDate.Date
            Else
                ObjImprest._ToDate = Nothing
            End If
            ObjImprest._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            ObjImprest._ExpCateName = cboExpenseCategory.SelectedItem.Text.ToString()
            ObjImprest._EmpUnkId = CInt(cboEmployee.SelectedValue)
            ObjImprest._EmpName = cboEmployee.SelectedItem.Text.ToString

            If CInt(cboFromPeriod.SelectedValue) > 0 AndAlso CInt(cboToPeriod.SelectedValue) > 0 Then
                ObjImprest._StatusId = cboStatus.SelectedIndex
            ElseIf cboStatus.SelectedValue IsNot Nothing Then
                ObjImprest._StatusId = CInt(cboStatus.SelectedValue)
            End If

            ObjImprest._StatusName = cboStatus.SelectedItem.Text.ToString
            ObjImprest._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            ObjImprest._UserUnkid = CInt(Session("UserId"))
            ObjImprest._CompanyUnkId = Company._Object._Companyunkid


            ObjImprest._ShowRetirememtFormStatus = chkShowRetirementStatus.Checked


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            ObjImprest._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
            ObjImprest._FromPeriod = cboFromPeriod.SelectedItem.Text
            ObjImprest._PeriodStartDate = objPeriod._Start_Date.Date

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            ObjImprest._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            ObjImprest._ToPeriod = cboToPeriod.SelectedItem.Text
            ObjImprest._PeriodEndDate = objPeriod._End_Date.Date
            objPeriod = Nothing

            ObjImprest._ExpenseID = CInt(cboExpense.SelectedValue)
            ObjImprest._Expense = cboExpense.SelectedItem.Text

            ObjImprest._ViewByIds = mstrStringIds
            ObjImprest._ViewIndex = mintViewIdx
            ObjImprest._ViewByName = mstrStringName
            ObjImprest._Analysis_Fields = mstrAnalysis_Fields
            ObjImprest._Analysis_Join = mstrAnalysis_Join
            ObjImprest._Analysis_OrderBy = mstrAnalysis_OrderBy
            ObjImprest._Report_GroupName = mstrReport_GroupName

            If CInt(Session("Employeeunkid")) > 0 Then
                ObjImprest._UserName = Session("DisplayName").ToString()
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            ObjImprest._CurrencyId = CInt(cboCurrency.SelectedValue)
            ObjImprest._Currency = cboCurrency.SelectedItem.Text
            'Pinkal (30-Mar-2021) -- End

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            ObjImprest._ShowClaimRetirementNumber = chkShowCRetirementNo.Checked
            'S.SANDEEP |21-FEB-2022| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
                SetLanguage()
                GC.Collect()
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim ObjImprest As New clsImprestBalanceReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(ObjImprest) = False Then Exit Sub

            ObjImprest.setDefaultOrderBy(0)

            Call SetDateFormat()

            ObjImprest.generateReportNew(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           Session("ExportReportPath").ToString, _
                                           CBool(Session("OpenAfterExport")), _
                                           0, Aruti.Data.enPrintAction.None, enExportAction.None)

            Session("objRpt") = ObjImprest._Rpt

            If ObjImprest._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            ObjImprest = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) = 0 Then
                cboStatus.Enabled = True
            Else
                cboStatus.Enabled = False
            End If
            cboFromPeriod_SelectedIndexChanged(cboFromPeriod, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim objExpMst As New clsExpense_Master
            Dim dsCombo As DataSet = objExpMst.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            cboExpense.DataTextField = "Name"
            cboExpense.DataValueField = "Id"
            cboExpense.DataSource = dsCombo.Tables(0)
            cboExpense.DataBind()
            cboExpense.SelectedValue = 0
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objExpMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboFromPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromPeriod.SelectedIndexChanged, cboToPeriod.SelectedIndexChanged
        Try
            If cboFromPeriod.SelectedValue > 0 AndAlso cboToPeriod.SelectedValue > 0 Then
                dtpTranFromDate.SetDate = Nothing
                dtpTranToDate.SetDate = Nothing
                dtpTranFromDate.Enabled = False
                dtpTranToDate.Enabled = False
                If cboStatus.DataSource IsNot Nothing Then cboStatus.DataSource = Nothing
                cboStatus.Items.Clear()
                cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Select"))
                cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Posted"))
                cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Not Posted"))
                cboStatus.SelectedIndex = 0
                cboStatus.Enabled = True
            Else
                dtpTranFromDate.Enabled = True
                dtpTranToDate.Enabled = True
                If cboStatus.DataSource Is Nothing AndAlso cboStatus.Items.Count > 0 Then cboStatus.Items.Clear()
                FillStatus()
                If CInt(cboReportType.SelectedIndex) = 0 Then
                    cboStatus.Enabled = True
                Else
                    cboStatus.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Other Controls Events "

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)

            Me.LblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblReportType.ID, Me.LblReportType.Text)
            Me.lblTranFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblTranFromDate.ID, Me.lblTranFromDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblTranToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblTranToDate.ID, Me.lblTranToDate.Text)
            Me.chkShowRetirementStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkShowRetirementStatus.ID, Me.chkShowRetirementStatus.Text)
            Me.LblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpense.ID, Me.LblExpense.Text)
            Me.LblPostedPeriodFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblPostedPeriodFrom.ID, Me.LblPostedPeriodFrom.Text)
            Me.LblTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblTo.ID, Me.LblTo.Text)

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            Me.LblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblCurrency.ID, Me.LblCurrency.Text)
            'Pinkal (30-Mar-2021) -- End

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            Me.chkShowCRetirementNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowCRetirementNo.ID, Me.chkShowCRetirementNo.Text)
            'S.SANDEEP |21-FEB-2022| -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Please Select From Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Please Select To Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, " To Period cannot be less than From Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "Posted")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 8, "Not Posted")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 9, "Not Retired")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 10, "Imprest Retired Balance Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 11, "Imprest UnRetired Balance Report")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
