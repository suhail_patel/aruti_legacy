﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_ImprestBalanceReport.aspx.vb"
    Inherits="Reports_Claim_Retirement_Reports_Rpt_ImprestBalanceReport" Title="Imprest Balance Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Imprest Balance Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" ToolTip="Analysis By">
                                                <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblReportType" runat="server" Text="Report Type"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblPostedPeriodFrom" runat="server" Text="Posted Period From"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFromPeriod" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblTo" runat="server" Text="To"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboToPeriod" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTranFromDate" runat="server" Text="From Date"></asp:Label>
                                        <uc2:DateCtrl ID="dtpTranFromDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTranToDate" runat="server" Text="To"></asp:Label>
                                        <uc2:DateCtrl ID="dtpTranToDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblExpenseCategory" runat="server" Text="Exp. Category"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpense" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                
                                 <%--Pinkal (30-Mar-2021)-- Start
                                         NMB Enhancement  -  Working on Employee Recategorization history Report.--%>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                         <asp:Label ID =  "LblCurrency" runat="server" Text = "Currency"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID = "cboCurrency" runat="server"></asp:DropDownList>     
                                        </div>
                                    </div>
                                </div>
                                   <%--'Pinkal (30-Mar-2021) -- End--%>
                                
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowRetirementStatus" runat="server" Text="Show Retirement Form Status" Checked="true" />
                                    </div>
                                </div>
                                
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowCRetirementNo" runat="server" Text="Show Claim Retirement Number" Checked="true" />
                                    </div>
                                </div>
                                
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc3:AnalysisBy ID="popupAnalysisBy" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
