﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="Rpt_LeaveAbsence_DetailedReport.aspx.vb" Inherits="Reports_Rpt_LeaveAbsence_DetailedReport" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:Panel ID="Panel1" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                      <asp:Label ID="lblPageHeader" runat="server" Text="Leave Absence Detailed Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                       <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip ="Analysis By">
                                                     <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                              <div class="body">
                                      <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpStartdate" runat="server" AutoPostBack="false" />
                                            </div>
                                     </div>
                                      <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                     <asp:Label ID="LblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                                     <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                            </div>
                                      </div>
                                      <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                     <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                      <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server"  />
                                                      </div>
                                            </div>
                                      </div>                   
                                      <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                             <asp:DropDownList ID="cboStatus" runat="server" />
                                                    </div>
                                            </div>
                                      </div> 
                                      <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                     <asp:CheckBox ID="chkSelectAll" runat="server" Text = "Select All" AutoPostBack="true" />
                                            </div>
                                      </div>    
                                      <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 200px">
                                                             <asp:CheckBoxList ID="chkLeaveType" runat="server" RepeatLayout="Flow" />
                                                    </div>
                                            </div>
                                     </div>              
                            </div>
                            <div class="footer">
                                   <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                   <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                                   <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                   </div>
                </div>        
                
                   <%-- <div class="panel-primary">
                        <div class="panel-heading">
                          
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                        
                                    </div>
                                    <div style="text-align: right">
                                        
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                               
                                            </td>
                                        </tr>
                                       <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                               
                                            </td>
                                       </tr>
                                        <tr style="width: 100%">
                                                <td style="width: 25%">       
                                                </td>
                                                <td style="width: 75%">
                                                <div style="margin-top: 5px; margin-bottom: 5px; border: 1px solid #DDD; height: auto;
                                                    overflow: auto">
                                                  
                                                </div>
                                                </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                         
                    </div>--%>
                    <uc3:AnalysisBy ID="popupAnalysisBy" runat="server" />
                     <uc4:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
