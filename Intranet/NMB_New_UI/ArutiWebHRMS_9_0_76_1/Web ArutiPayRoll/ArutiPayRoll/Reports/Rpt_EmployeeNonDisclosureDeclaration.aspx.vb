﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmployeeNonDisclosureDeclaration
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes

    Private ReadOnly mstrModuleName As String = "frmEmployeeNonDisclosureDeclarationReport"

    'Private objclsAssetDeclarationT2 As clsAssetDeclarationT2
    'Dim objAssetDeclare As New clsAssetdeclaration_masterT2
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
                'objAssetDeclare = New clsAssetdeclaration_masterT2

                SetLanguage()
                'Call Language._Object.SaveValue()
                Call SetControlCaptions()

                FillCombo()
                ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objclsMasterData As New clsMasterData 'Sohail (24 Jun 2020)
        Try


            objEmp = New clsEmployee_Master

            Dim blnIncludeInactiveEmp As Boolean = True

            Dim dsList As DataSet = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               blnIncludeInactiveEmp, "Employee", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            dsCombo = objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"))

            With cboFinalcialYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dsCombo.Tables("Year")
                .DataBind()
                .SelectedValue = CInt(Session("Fin_Year"))
            End With
            'Sohail (24 Jun 2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            FillCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim objND As New clsEmpnondisclosure_declaration_tran
        Dim objEND As New clsEmployeeNonDisclosureDeclarationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try


            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            If CInt(cboFinalcialYear.SelectedIndex) < 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Financial year is compulsory information.Please Select Financial year."), Me)
                cboFinalcialYear.Focus()
                Exit Sub
            End If
            'Sohail (24 Jun 2020) -- End

            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Select Employee to Continue"), Me)
            '    Exit Sub
            'End If

            If CInt(cboEmployee.SelectedValue) > 0 AndAlso objND.isExist(CInt(cboEmployee.SelectedValue), CInt(Session("Fin_year"))) = False Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Declaration for the current financial year is not done yet."), Me)
                Exit Sub
            End If

            objEND.SetDefaultValue()

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objEND._EmployeeId = CInt(cboEmployee.SelectedValue)
                objEND._EmployeeName = cboEmployee.SelectedItem.Text
            Else
                objEND._EmployeeId = 0
                objEND._EmployeeName = ""
            End If

            objEND._DeclarationStatusId = enAssetDeclarationStatusType.SUMBITTED
            objEND._Advance_Filter = mstrAdvanceFilter


            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            objEND._FinancialYearId = cboFinalcialYear.SelectedValue
            objEND._FinancialYearName = cboFinalcialYear.SelectedItem.Text
            'Sohail (24 Jun 2020) -- End


            objEND._ApplyUserAccessFilter = True

            objEND.generateReportNew(Session("Database_Name").ToString, _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   Session("UserAccessModeSetting").ToString, True, _
                                                   Session("ExportReportPath").ToString, _
                                                   False, _
                                                   0, enPrintAction.None, enExportAction.None)
            'Hemant (28 Jan 2021) -- [CBool(Session("OpenAfterExport")) --> False]
            Session("objRpt") = objEND._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (20 Jun 2020) -- Start
    'NMB Issue : # : The close button on employee confidentiality and employee non-disclosure agreement report are not working.
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (20 Jun 2020) -- End

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Language._Object.setCaption("gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Hemant (17 Sep 2020) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReport.ID, Me.btnReport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Declaration for the current financial year is not done yet.")


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Hemant (17 Sep 2020) -- End
            Me.lnkAdvanceFilter.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblFinalcialYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


End Class
