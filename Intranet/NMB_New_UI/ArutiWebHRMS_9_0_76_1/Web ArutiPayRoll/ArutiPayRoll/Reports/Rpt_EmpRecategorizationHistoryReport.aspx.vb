﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Rpt_EmpRecategorizationHistoryReport
    Inherits Basepage


#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmp_Recategorize_History_Report"
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

         
            If Not IsPostBack Then
                SetLanguage()
                Call FillCombo()
                ResetValue()
            Else
                mstrViewByIds = ViewState("mstrViewByIds").ToString
                mstrViewByName = ViewState("mstrViewByName").ToString
                mintViewIndex = CInt(ViewState("mintViewIndex"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrAnalysis_OrderBy_GName = ViewState("mstrAnalysis_OrderBy_GName").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrViewByIds") = mstrViewByIds
            Me.ViewState("mstrViewByName") = mstrViewByName
            Me.ViewState("mintViewIndex") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                               Session("UserId"), _
                                               Session("Fin_year"), _
                                               Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               Session("UserAccessModeSetting"), True, _
                                               Session("IsIncludeInactiveEmp"), "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            ObjEmp = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Function SetFilter(ByRef objEmpJobHistoryReport As clsEmp_RecategorizationHistory_Report) As Boolean
        Try
            objEmpJobHistoryReport.SetDefaultValue()

            If dtpStartdate.IsNull = False Then
                objEmpJobHistoryReport._FromDate = dtpStartdate.GetDate.Date
            Else
                objEmpJobHistoryReport._FromDate = Nothing
            End If
            If dtpToDate.IsNull = False Then
                objEmpJobHistoryReport._ToDate = dtpToDate.GetDate.Date
            Else
                objEmpJobHistoryReport._ToDate = Nothing
            End If
            objEmpJobHistoryReport._EmployeeID = CInt(cboEmployee.SelectedValue)
            objEmpJobHistoryReport._EmployeeName = cboEmployee.SelectedItem.Text
            objEmpJobHistoryReport._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpJobHistoryReport._ViewByIds = mstrViewByIds
            objEmpJobHistoryReport._ViewIndex = mintViewIndex
            objEmpJobHistoryReport._ViewByName = mstrViewByName
            objEmpJobHistoryReport._Analysis_Fields = mstrAnalysis_Fields
            objEmpJobHistoryReport._Analysis_Join = mstrAnalysis_Join
            objEmpJobHistoryReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpJobHistoryReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objEmpJobHistoryReport._Report_GroupName = mstrReport_GroupName

            objEmpJobHistoryReport._UserUnkId = CInt(Session("UserId"))
            objEmpJobHistoryReport._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objEmpJobHistoryReport._OpenAfterExport = False
            objEmpJobHistoryReport._CompanyUnkId = CInt(Session("CompanyUnkId"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Public Sub ResetValue()
        Try
            dtpStartdate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            cboEmployee.SelectedValue = 0
            chkInactiveemp.Checked = False
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objEmpRecategorize As New clsEmp_RecategorizationHistory_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(objEmpRecategorize) = False Then Exit Sub

            Call SetDateFormat()

            objEmpRecategorize.Generate_DetailReport(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           Session("EmployeeAsOnDate").ToString(), _
                                           CStr(Session("UserAccessModeSetting")), True)

            If objEmpRecategorize._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmpRecategorize._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpRecategorize = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = popupAnalysisBy._Analysis_OrderBy_GName
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)

            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
