﻿Option Strict On 'Shani(11 Feb 2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_LeaveStatementReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objLeaveStatement As clsLeaveStatement_Report
    'Pinkal (11-Sep-2020) -- End

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mstrModuleName As String = "frmLeaveStatement_Report"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet



            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            'If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    '    dsList = objEmployee.GetList("Employee", False, True)
            '    'Else
            '    '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            '    'End If
            '    dsList = objEmployee.GetList(Session("Database_Name").ToString(), _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                      Session("UserAccessModeSetting").ToString(), True, _
            '                                      CBool(Session("IsIncludeInactiveEmp")), "Employee", _
            '                                      CBool(Session("ShowFirstAppointmentDate")))
            '    'Shani(24-Aug-2015) -- End



            '    Dim dRow As DataRow = dsList.Tables(0).NewRow
            '    dRow("employeeunkid") = 0

            '    'Pinkal (06-May-2014) -- Start
            '    'Enhancement : Language Changes 
            '    'Language.setLanguage(mstrModuleName)
            '    dRow("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Select")
            '    'Pinkal (06-May-2014) -- End

            '    dsList.Tables(0).Rows.InsertAt(dRow, 0)

            'ElseIf Session("LoginBy") = Global.User.en_loginby.Employee Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'dsList = objEmployee.GetList("Employee", False, True, , , CInt(Session("Employeeunkid")), Session("AccessLevelFilterString"))
            '    dsList = objEmployee.GetList(Session("Database_Name"), _
            '                                      Session("UserId"), _
            '                                      Session("Fin_year"), _
            '                                      Session("CompanyUnkId"), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      Session("UserAccessModeSetting"), True, _
            '                                      Session("IsIncludeInactiveEmp"), "Employee", _
            '                                      Session("ShowFirstAppointmentDate"), _
            '                                      CInt(Session("Employeeunkid")), , _
            '                                      Session("AccessLevelFilterString"))
            '    'Shani(24-Aug-2015) -- End


            'End If
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    dsList = objEmployee.GetList("Employee", False, True)
            'Else
            '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'End If
            Dim blnSelect As Boolean = True
            Dim intEmpUnkId As Integer = -1
            Dim blnApplyAccess As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnApplyAccess = False
                blnSelect = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    Session("UserAccessModeSetting").ToString(), True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                    blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)
            'Shani(11-Feb-2016) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing

            Dim objLeave As New clsleavetype_master

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'dsList = objLeave.getListForCombo("Leave", True, 1)

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.


            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "ISNULL(deductfromlvtypeunkid,0) <=0", True)
            'Else
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "ISNULL(deductfromlvtypeunkid,0) <=0", False)
            'End If
            dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "ISNULL(deductfromlvtypeunkid,0) <=0", False)

            'Pinkal (06-Dec-2019) -- End

            'Pinkal (25-May-2019) -- End


            'Pinkal (03-May-2019) -- End
            With cboLeave
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeave = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Return False

            ElseIf CInt(cboLeave.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                cboLeave.Focus()
                Return False

            End If

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtpAsonDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation", Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Public Sub ResetValue()
        Try
            dtpAsonDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedIndex = 0
            cboLeave.SelectedIndex = 0
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLeaveStatement As clsLeaveStatement_Report) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim objDepartment As New clsDepartment

            objLeaveStatement.SetDefaultValue()
            objLeaveStatement._AsOnDate = dtpAsonDate.GetDate.Date
            objLeaveStatement._EmployeeId = CInt(cboEmployee.SelectedValue)
            objLeaveStatement._EmployeeName = cboEmployee.SelectedItem.Text
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
            objLeaveStatement._EmployeeCode = objEmployee._Employeecode

            objLeaveStatement._JobId = objEmployee._Jobunkid
            objJob._Jobunkid = objEmployee._Jobunkid
            objLeaveStatement._Job = objJob._Job_Name

            objLeaveStatement._DepartmentId = objEmployee._Departmentunkid
            objDepartment._Departmentunkid = objEmployee._Departmentunkid
            objLeaveStatement._Department = objDepartment._Name

            objLeaveStatement._LeaveId = CInt(cboLeave.SelectedValue)
            objLeaveStatement._LeaveName = cboLeave.SelectedItem.Text
            objLeaveStatement._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))

            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            objLeaveStatement._YearId = CInt(Session("Fin_year"))
            'Pinkal (24-May-2014) -- End

            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
            objLeaveStatement._DBStartdate = CDate(Session("fin_startdate")).Date
            objLeaveStatement._DBEnddate = CDate(Session("fin_enddate")).Date
            'Pinkal (25-Jul-2015) -- End



            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
            objLeaveStatement._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
            objLeaveStatement._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            'Pinkal (16-Dec-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeaveStatement = New clsLeaveStatement_Report
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End

            If Not IsPostBack Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End
                Call FillCombo()
                dtpAsonDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveStatement As New clsLeaveStatement_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try
            If Validation() Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'If SetFilter() = False Then Exit Sub
                If SetFilter(objLeaveStatement) = False Then Exit Sub
                'Pinkal (11-Sep-2020) -- End


                objLeaveStatement._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objLeaveStatement._UserUnkId = CInt(Session("UserId"))
                objLeaveStatement._UserAccessFilter = Session("AccessLevelFilterString").ToString()


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'Pinkal (01-Dec-2016) -- Start
                'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
                If CInt(Session("Employeeunkid")) > 0 Then
                    objLeaveStatement._UserName = Session("DisplayName").ToString()
                End If
                'Pinkal (01-Dec-2016) -- End

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objLeaveStatement.generateReport(0, enPrintAction.None, enExportAction.None)
                objLeaveStatement.generateReportNew(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    Session("UserAccessModeSetting").ToString, True, _
                                                    Session("ExportReportPath").ToString, _
                                                    CBool(Session("OpenAfterExport")), _
                                                    0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
                'Shani(20-Nov-2015) -- End

                Session("objRpt") = objLeaveStatement._Rpt
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                If Session("objRpt") IsNot Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
                'Shani(11-Feb-2016) -- End

                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveStatement = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, objLeaveStatement._ReportName)
            'Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, objLeaveStatement._ReportName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Pinkal (11-Sep-2020) -- End



            Me.LblAsOnDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAsOnDate.ID, Me.LblAsOnDate.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.lblLeaveName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveName.ID, Me.lblLeaveName.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

End Class
