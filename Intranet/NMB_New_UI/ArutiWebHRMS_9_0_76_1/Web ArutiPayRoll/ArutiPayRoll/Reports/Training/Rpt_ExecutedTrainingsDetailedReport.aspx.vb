﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Training_Rpt_ExecutedTrainingsDetailedReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objExecutedTrainingsDetailedReport As clsExecutedTrainingsDetailedReport
    Private ReadOnly mstrModuleName As String = "frmExecutedTrainingsDetailedReport"
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objExecutedTrainingsDetailedReport = New clsExecutedTrainingsDetailedReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                'Call ResetValue()              
            Else
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master

        Try
            With cboReportType
                .Items.Clear()
                'Language.setLanguage(mstrModuleName)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Planned Detailed Report"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Executed Detailed Report"))
                .SelectedIndex = 0
            End With

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              Session("UserAccessModeSetting").ToString, True, _
                                             True, "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
            objCommon = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboTrainingCalendar.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            chkInactiveemp.Checked = False
            'Hemant (25 Oct 2021) -- End
            objExecutedTrainingsDetailedReport.setDefaultOrderBy(0)
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Traininig Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            objExecutedTrainingsDetailedReport.SetDefaultValue()

            objExecutedTrainingsDetailedReport._ReportId = cboReportType.SelectedIndex
            objExecutedTrainingsDetailedReport._ReportTypeName = cboReportType.SelectedItem.Text
            objExecutedTrainingsDetailedReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objExecutedTrainingsDetailedReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objExecutedTrainingsDetailedReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objExecutedTrainingsDetailedReport._TrainingName = cboTrainingName.SelectedItem.Text
            objExecutedTrainingsDetailedReport._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objExecutedTrainingsDetailedReport._EmployeeName = cboEmployee.SelectedItem.Text
            objExecutedTrainingsDetailedReport._Advance_Filter = mstrAdvanceFilter
            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            objExecutedTrainingsDetailedReport._IsActive = CBool(chkInactiveemp.Checked)
            'Hemant (25 Oct 2021) -- End
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objExecutedTrainingsDetailedReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            If cboReportType.SelectedIndex = 0 Then
                objExecutedTrainingsDetailedReport.Generate_PlannedDetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                               Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                               OpenAfterExport)
            ElseIf cboReportType.SelectedIndex = 1 Then
                objExecutedTrainingsDetailedReport.Generate_ExecutedDetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                               Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                               OpenAfterExport)
            End If



            If objExecutedTrainingsDetailedReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objExecutedTrainingsDetailedReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblReportType.ID, Me.lblReportType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReportType.ID, Me.lblReportType.Text)
            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Trim.Replace("&", ""))
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Planned Detailed Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Executed Detailed Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Please Select Traininig Calendar.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
