﻿#Region " Imports "

Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Imports System.Drawing
Imports System.Web.UI.DataVisualization.Charting
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Imports iTextSharp.text.html.simpleparser

#End Region


Partial Class Reports_Training_Rpt_TrainingBudgetReportNew
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objTrainingBudgetReport As clsTrainingBudgetReportNew
    Private ReadOnly mstrModuleName As String = "frmTrainingBudgetReportNew"
    Private dsChartDataSet As New DataSet
#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTrainingBudgetReport = New clsTrainingBudgetReportNew(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()

                hfShowChart.Value = "0"
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master

        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboTrainingCalendar.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            chkIncludeZeroCost.Checked = False
            hfShowChart.Value = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Traininig Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            objTrainingBudgetReport.SetDefaultValue()

            objTrainingBudgetReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingBudgetReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objTrainingBudgetReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingBudgetReport._TrainingName = cboTrainingName.SelectedItem.Text
            objTrainingBudgetReport._IsIncludeZeroCost = CBool(chkIncludeZeroCost.Checked)
            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            objTrainingBudgetReport.setDefaultOrderBy(0)

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function

    Private Sub Fill_Chart()
        Try
            chAnalysis_Chart.Series(0).Points.DataBindXY(dsChartDataSet.Tables(0).DefaultView, "xAxis", dsChartDataSet.Tables(0).DefaultView, "yAxis1")

            chAnalysis_Chart.ChartAreas(0).AxisX.LabelStyle.Interval = 1
            chAnalysis_Chart.ChartAreas(0).AxisX.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Training")
            chAnalysis_Chart.ChartAreas(0).AxisX.LabelStyle.Angle = -25
            chAnalysis_Chart.ChartAreas(0).AxisY.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Total Approved Cost")
            chAnalysis_Chart.ChartAreas(0).AxisX.TitleFont = New System.Drawing.Font("Tahoma", 9, System.Drawing.FontStyle.Bold)
            chAnalysis_Chart.ChartAreas(0).AxisY.TitleFont = New System.Drawing.Font("Tahoma", 9, System.Drawing.FontStyle.Bold)

            chAnalysis_Chart.Titles(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Training Cost Analysis")
            chAnalysis_Chart.Titles(0).Font = New System.Drawing.Font(chAnalysis_Chart.Font.Name, 11, System.Drawing.FontStyle.Bold)
            chAnalysis_Chart.Series(0).IsValueShownAsLabel = True

            chAnalysis_Chart.Series(0)("DrawingStyle") = "Cylinder"

            chAnalysis_Chart.Series(0).IsVisibleInLegend = False

            chAnalysis_Chart.Series(0).Color = System.Drawing.Color.RoyalBlue

            chAnalysis_Chart.Series(0)("PixelPointWidth") = "25"

            chAnalysis_Chart.ChartAreas(0).AxisX.MajorGrid.Enabled = False
            chAnalysis_Chart.ChartAreas(0).AxisX.MinorGrid.Enabled = False

            chAnalysis_Chart.ChartAreas(0).AxisY.MajorGrid.Enabled = False
            chAnalysis_Chart.ChartAreas(0).AxisY.MinorGrid.Enabled = False

            chAnalysis_Chart.Series(0).ChartType = SeriesChartType.Column

            For i As Integer = 0 To chAnalysis_Chart.Series.Count - 1
                For Each dp As DataPoint In chAnalysis_Chart.Series(i).Points
                    If dp.YValues(0) = 0 Then
                        dp.Label = " "
                    End If
                Next
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Chart", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objTrainingBudgetReport.setDefaultOrderBy(0)

            hfShowChart.Value = "0"
            Call SetDateFormat()
            Dim strFilePath As String = System.IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = System.IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objTrainingBudgetReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                          Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                          OpenAfterExport)


            If objTrainingBudgetReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objTrainingBudgetReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try


    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnPDFExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPDFExport.Click
        Try
            Dim pdfDoc As Document = New Document(PageSize.A4, 20.0!, 20.0!, 20.0!, 0.0!)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            Dim stream As MemoryStream = New MemoryStream
            chAnalysis_Chart.SaveImage(stream, ChartImageFormat.Png)
            Dim chartImage As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(stream.GetBuffer)
            chartImage.ScalePercent(55.0!)
            pdfDoc.Add(chartImage)
            pdfDoc.Close()
            Response.ContentType = "application/pdf"
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Chart.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Write(pdfDoc)
            Response.Flush()
            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            HttpContext.Current.ApplicationInstance.CompleteRequest()
            Response.End()
            'Pinkal (11-Feb-2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link Buttons Events"

    Protected Sub lnkDisplayChart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDisplayChart.Click
        Dim dsList As DataSet
        Try
            If SetFilter() = False Then Exit Sub

            'objTrainingBudgetReport.generateReportNew(CStr(Session("Database_Name")), _
            '                                          CInt(Session("UserId")), _
            '                                          CInt(Session("Fin_year")), _
            '                                          CInt(Session("CompanyUnkId")), _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                          CStr(Session("UserAccessModeSetting")), True, _
            '                                          Session("ExportReportPath").ToString, _
            '                                          CBool(Session("OpenAfterExport")), _
            '                                          0, Aruti.Data.enPrintAction.None, Aruti.Data.enExportAction.None, _
            '                                          CInt(Session("Base_CurrencyId")))

            'Session("objRpt") = objTrainingBudgetReport._Rpt

            'If objTrainingBudgetReport._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            dsList = objTrainingBudgetReport.GetQueryData(CStr(Session("Database_Name")), _
                                                      CInt(Session("UserId")), _
                                                      CInt(Session("Fin_year")), _
                                                      CInt(Session("CompanyUnkId")), _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                            CStr(Session("UserAccessModeSetting")), _
                                                            True, _
                                                            True _
                                                            )

            Dim iList As List(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("trainingcourseunkid")).Distinct().ToList()
            dsChartDataSet = dsList.Clone()
            For Each iTrainingId As Integer In iList
                Dim dr() As DataRow = dsList.Tables(0).Select("trainingcourseunkid = " & CInt(iTrainingId))
                If dr.Length > 0 Then
                    Dim dt As DataTable = dr.CopyToDataTable()
                    Dim decTotApprovedCost As Decimal = CDec(dt.Compute("SUM(totalapprovedbudget)", "1=1"))
                    Dim drRow As DataRow = dsChartDataSet.Tables(0).NewRow
                    drRow("trainingcourseunkid") = CInt(iTrainingId)
                    drRow("trainingcoursename") = dr(0)("trainingcoursename")
                    drRow("totalapprovedbudget") = Format(CDec(decTotApprovedCost), GUI.fmtCurrency)
                    dsChartDataSet.Tables(0).Rows.Add(drRow)
                End If

            Next
            dsChartDataSet.Tables(0).Columns("trainingcoursename").ColumnName = "xAxis"
            dsChartDataSet.Tables(0).Columns("totalapprovedbudget").ColumnName = "yAxis1"

            If dsChartDataSet.Tables(0).Rows.Count > 0 Then
                Fill_Chart()
                hfShowChart.Value = "1"
            Else
                hfShowChart.Value = "0"
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, No Training data present for the selected Calendar."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkDisplayChart.ID, Me.lnkDisplayChart.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkIncludeZeroCost.ID, Me.chkIncludeZeroCost.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lnkDisplayChart.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkDisplayChart.ID, Me.lnkDisplayChart.Text)

            Me.chkIncludeZeroCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeZeroCost.ID, Me.chkIncludeZeroCost.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Trim.Replace("&", ""))
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Traininig Calendar.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Training")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Total Approved Cost")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Training Cost Analysis")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, No Training data present for the selected Calendar.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
