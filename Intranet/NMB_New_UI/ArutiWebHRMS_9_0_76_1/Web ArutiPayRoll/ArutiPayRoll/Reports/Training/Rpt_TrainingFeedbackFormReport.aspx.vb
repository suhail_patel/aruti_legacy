﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Training_Rpt_TrainingFeedbackFormReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objTrainingFeedbackFormReport As clsTrainingFeedbackFormReport
    Private ReadOnly mstrModuleName As String = "frmTrainingFeedbackFormReport"

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTrainingFeedbackFormReport = New clsTrainingFeedbackFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    pnlEmployeeList.Visible = True
                Else
                    pnlEmployeeList.Visible = False
                End If
            Else

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objMasterData As New clsMasterData
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master

        Try
            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objMasterData.GetTrainingEvaluationFeedbackModeList(True, "List")
            With cboEvaluationCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMasterData = Nothing
            objCalendar = Nothing
            objCommon = Nothing

        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEvaluationCategory.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            cboTrainingCalendar.SelectedIndex = 0

            objTrainingFeedbackFormReport.setDefaultOrderBy(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Training Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If CInt(cboEvaluationCategory.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select Evaluation Category."), Me)
                cboEvaluationCategory.Focus()
                Exit Function
            End If

            If CInt(cboTrainingName.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Training Name."), Me)
                cboTrainingName.Focus()
                Exit Function
            End If

            objTrainingFeedbackFormReport.SetDefaultValue()

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelect"), CheckBox).Checked = True)

                If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                    objTrainingFeedbackFormReport._EmployeeIds = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
                End If
            Else
                Dim lstEmployeeList As List(Of String) = New List(Of String)
                lstEmployeeList.Add(Session("Employeeunkid").ToString)
                objTrainingFeedbackFormReport._EmployeeIds = lstEmployeeList
            End If

            objTrainingFeedbackFormReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingFeedbackFormReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objTrainingFeedbackFormReport._FeedbackModeid = CInt(cboEvaluationCategory.SelectedValue)
            objTrainingFeedbackFormReport._FeedbackModeName = cboEvaluationCategory.SelectedItem.Text
            objTrainingFeedbackFormReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingFeedbackFormReport._TrainingName = cboTrainingName.SelectedItem.Text

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function
#End Region

#Region " Combobox's Events "
    Private Sub cboTrainingName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrainingCalendar.SelectedIndexChanged, _
                                                                                                                  cboTrainingName.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsCombo As New DataSet
        Dim dsTrainingRequest As New DataSet
        Dim strFilter As String = String.Empty
        Dim dtTable As DataTable
        Dim isblank As Boolean
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User AndAlso CInt(cboTrainingCalendar.SelectedValue) > 0 AndAlso CInt(cboTrainingName.SelectedValue) > 0 Then
                dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             Session("UserAccessModeSetting").ToString, True, _
                                            True, "Emp", True)

                strFilter = "periodunkid = " & CInt(cboTrainingCalendar.SelectedValue) & " AND coursemasterunkid = " & CInt(cboTrainingName.SelectedValue) & ""
                dsTrainingRequest = objTrainingRequest.GetList(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                True, "List", enTrainingRequestStatus.APPROVED, _
                                                                strFilter _
                                                               )
                Dim strempids As String = String.Join(",", dsTrainingRequest.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())

                If strempids.Trim.Length > 0 Then
                    dtTable = New DataView(dsCombo.Tables(0), "employeeunkid IN(" & strempids & ")", "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsCombo.Tables(0).Clone()
                End If


                If dtTable.Rows.Count <= 0 Then
                    Dim r As DataRow = dtTable.NewRow

                    r.Item(1) = "None" ' To Hide the row and display only Row Header
                    r.Item(2) = "0"

                    dtTable.Rows.Add(r)
                    isblank = True
                End If

                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()
                If isblank = True Then
                    dgvEmployee.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrainingName_SelectedIndexChanged", mstrModuleName)
        Finally
            dsCombo = Nothing : objEmp = Nothing
            dsTrainingRequest = Nothing
            objTrainingRequest = Nothing
        End Try

    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objTrainingFeedbackFormReport.generateReportNew(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            Session("ExportReportPath").ToString, _
                                                            CBool(Session("OpenAfterExport")), _
                                                            0, Aruti.Data.enPrintAction.None, Aruti.Data.enExportAction.None, _
                                                            CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objTrainingFeedbackFormReport._Rpt

            If objTrainingFeedbackFormReport._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEvaluationCategory.ID, Me.lblEvaluationCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingName.ID, Me.lblTrainingName.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(3).FooterText, dgvEmployee.Columns(3).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblEvaluationCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEvaluationCategory.ID, Me.lblEvaluationCategory.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)

            dgvEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)
            dgvEmployee.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(3).FooterText, dgvEmployee.Columns(3).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Training Calendar.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please Select Evaluation Category.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Please Select Training Name.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
