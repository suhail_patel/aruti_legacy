﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Training_Rpt_TrainingsCoveredProportionReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objTrainingsCoveredProportionReport As clsTrainingsCoveredProportionReport
    Private ReadOnly mstrModuleName As String = "frmTrainingsCoveredProportionReport"

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTrainingsCoveredProportionReport = New clsTrainingsCoveredProportionReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim objCMaster As New clsMasterData
        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCMaster.GetEAllocation_Notification("AList", "", False, False)
            Dim dRRow As DataRow = dsCombo.Tables(0).NewRow
            dRRow("id") = 0
            dRRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Select")
            dsCombo.Tables(0).Rows.InsertAt(dRRow, 0)
            With cboAllocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("AList")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCMaster = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboTrainingCalendar.SelectedIndex = 0
            cboAllocation.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            chkInactiveemp.Checked = False
            'Hemant (28 Oct 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Traininig Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If CInt(cboAllocation.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select Allocation."), Me)
                cboAllocation.Focus()
                Exit Function
            End If

            objTrainingsCoveredProportionReport.SetDefaultValue()

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelect"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objTrainingsCoveredProportionReport._Ids = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("id").ToString()).ToList
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Atleast one Allocation."), Me)
                Exit Function
            End If

            objTrainingsCoveredProportionReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingsCoveredProportionReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objTrainingsCoveredProportionReport._AllocationId = CInt(cboAllocation.SelectedValue)
            objTrainingsCoveredProportionReport._AllocationName = cboAllocation.SelectedItem.Text
            objTrainingsCoveredProportionReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingsCoveredProportionReport._TrainingName = cboTrainingName.SelectedItem.Text
            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            objTrainingsCoveredProportionReport._IsActive = CBool(chkInactiveemp.Checked)
            'Hemant (28 Oct 2021) -- End
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function

    Private Sub Fill_Data()
        Dim dtTable As DataTable
        Dim dList As New DataSet
        Dim isblank As Boolean
        Try
            Select Case CInt(cboAllocation.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    dList.Tables(0).Columns("jobunkid").ColumnName = "Id"
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                    dList.Tables(0).Columns("costcentername").ColumnName = "name"
            End Select

            dtTable = New DataView(dList.Tables("List"), "id > 0", "", DataViewRowState.CurrentRows).ToTable

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item(1) = "None" ' To Hide the row and display only Row Header
                r.Item(0) = "0"

                dtTable.Rows.Add(r)
                isblank = True
            End If

            dgvEmployee.DataSource = dtTable
            dgvEmployee.DataBind()
            If isblank = True Then
                dgvEmployee.Rows(0).Visible = False
            End If
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objTrainingsCoveredProportionReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objTrainingsCoveredProportionReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                OpenAfterExport)


            If objTrainingsCoveredProportionReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objTrainingsCoveredProportionReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Combobox Event(s) "
    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Try
            If CInt(cboAllocation.SelectedValue) > 0 Then
                Call Fill_Data()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAllocation.ID, Me.lblAllocation.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            'Hemant (28 Oct 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocation.ID, Me.lblAllocation.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            dgvEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            'Hemant (28 Oct 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Traininig Calendar.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please Select Allocation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Please Select Atleast one Allocation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
