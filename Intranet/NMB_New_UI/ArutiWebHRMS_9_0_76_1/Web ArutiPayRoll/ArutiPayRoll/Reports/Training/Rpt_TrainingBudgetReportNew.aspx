﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_TrainingBudgetReportNew.aspx.vb"
    Inherits="Reports_Training_Rpt_TrainingBudgetReportNew" Title="Training Budget Spreadsheet"
    EnableViewState="true" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Training Budget Spreadsheet"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingCalendar" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingName" runat="server" Text="Training Name"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingName" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                             <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkIncludeZeroCost" runat="server" Text="Include Zero Cost" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:LinkButton ID="lnkDisplayChart" runat="server" Text="Display Chart">
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlMain" runat="server">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 480px;">
                                            <asp:Chart ID="chAnalysis_Chart" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                                BorderlineWidth="2" AntiAliasing="All" Width="990px" Height="350px" EnableViewState="true"
                                                Visible="true">
                                                <Titles>
                                                    <asp:Title Name="Title1">
                                                    </asp:Title>
                                                </Titles>
                                                <Series>
                                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" Area3DStyle-WallWidth="5">
                                                        <Area3DStyle WallWidth="5" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                                <Legends>
                                                    <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                                    </asp:Legend>
                                                </Legends>
                                            </asp:Chart>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnPDFExport" runat="server" Text="Export To PDF" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <uc9:Export runat="server" ID="Export" />
            <asp:HiddenField ID="hfShowChart" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
            <asp:PostBackTrigger ControlID="btnPDFExport" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }
        
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
             if ( $("[id*=hfShowChart]").val() == "1") {
                $$('pnlMain').show();
            }
            else {
                $$('pnlMain').hide();
            }
          }
          
          
      $(document).ready(function() {
        $$('pnlMain').hide();
      
      });
     
      
     
    </script>

</asp:Content>
