﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_TrainingsCoveredProportionReport.aspx.vb"
    Inherits="Reports_Training_Rpt_TrainingsCoveredProportionReport" Title="Trainings Covered Proportion Report" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
          
            $("body").on("click", "[id*=chkSelectAll]", function() {    
                var chkHeader = $(this);
                var grid = $(this).closest(".body");
                $("[id*=chkSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=chkSelect]", function() {   
                var grid = $(this).closest(".body");
                var chkHeader = $("[id*=chkAllSelect]", grid);
                debugger;
                if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearchEmployee').val().length > 0) {
                    $('#<%= dgvEmployee.ClientID %> tbody tr').hide();
                    $('#<%= dgvEmployee.ClientID %> tbody tr:first').show();
                    $('#<%= dgvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmployee').val() + '\')').parent().show();
                }
                else if ($('#txtSearchEmployee').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= dgvEmployee.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearchEmployee').val('');
                $('#<%= dgvEmployee.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchEmployee').focus();
            }


    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Trainings Covered Proportion Report"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingCalendar" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingName" runat="server" Text="Training Name"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingName" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblAllocation" runat="server" Text="Allocation"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboAllocation" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="txtSearchEmployee" name="txtSearch" placeholder="Type To Search Text"
                                                maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 315px;">
                                        <asp:GridView ID="dgvEmployee" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered"
                                            RowStyle-Wrap="false" DataKeyNames="id">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="align-center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" Text=" " CssClass="chk-sm" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true" Visible="false"></asp:BoundField>
                                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhName">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle Font-Bold="False" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc9:Export runat="server" ID="Export" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
