﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_TrainingStatusSummaryReport.aspx.vb"
    Inherits="Reports_Training_Rpt_TrainingStatusSummaryReport" Title="Training Status Summary Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Training Status Summary Report"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingCalendar" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingName" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingStatus" runat="server" Text="Training Status" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkShowTrainingStatusChart" runat="server" Text="Show Training Status Chart" />
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
