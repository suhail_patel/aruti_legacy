﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Training_Rpt_PlannedTrainingReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmPlannedTrainingReport"
    Private objPlannedTReport As clsPlannedTrainingReport
    Private mintFirstOpenPeriod As Integer = 0
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPlannedTReport = New clsPlannedTrainingReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objPlannedTReport.setDefaultOrderBy(0)

            If IsPostBack = False Then
                SetLanguage()
                Call FillCombo()
            Else
                mintFirstOpenPeriod = CInt(ViewState("mintFirstOpenPeriod"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintFirstOpenPeriod", mintFirstOpenPeriod)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim dsCombo As New DataSet

        Try

            mintFirstOpenPeriod = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                mintFirstOpenPeriod = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If
            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintFirstOpenPeriod.ToString
            End With

            dsCombo = Nothing
            Dim objDeptTNeetMaster As New clsDepartmentaltrainingneed_master
            Dim strIds As String = "-1," & clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved & "," & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog
            dsCombo = objDeptTNeetMaster.getStatusComboList("List", True, strIds, "")
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "-1"
                .SelectedIndex = 0
            End With
            objDeptTNeetMaster = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Select Training Calendar."), Me)
                cboPeriod.Focus()
                Return False
                'ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select Status."), enMsgBoxStyle.Information)
                '    cboStatus.Select()
                '    Return False
            End If


            If dtpStartDate.GetDate <> Nothing OrElse dtpEndDate.GetDate <> Nothing Then
                Dim objPeriod As New clsTraining_Calendar_Master
                objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)

                If dtpStartDate.GetDate <> Nothing Then

                    If dtpStartDate.GetDate.Date < objPeriod._StartDate OrElse dtpStartDate.GetDate.Date > objPeriod._EndDate Then
                        eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Start Date must be between start date and end date of selected training calendar."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If

                End If

                If dtpEndDate.GetDate <> Nothing Then
                    If dtpEndDate.GetDate.Date < objPeriod._StartDate OrElse dtpEndDate.GetDate.Date > objPeriod._EndDate Then
                        eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "End Date must be between start date and end date of selected training calendar."), enMsgBoxStyle.Information)
                        dtpEndDate.Focus()
                        Return False
                    End If
                End If

                objPeriod = Nothing
            End If


            objPlannedTReport.SetDefaultValue()

            objPlannedTReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objPlannedTReport._PeriodName = cboPeriod.SelectedItem.Text

            If dtpStartDate.GetDate <> Nothing Then
                objPlannedTReport._StartDate = dtpStartDate.GetDate.Date
            End If

            If dtpEndDate.GetDate <> Nothing Then
                objPlannedTReport._EndDate = dtpEndDate.GetDate.Date
            End If

            objPlannedTReport._StatusId = CInt(cboStatus.SelectedValue)
            objPlannedTReport._StatusName = cboStatus.SelectedItem.Text




            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objPlannedTReport.setDefaultOrderBy(0)
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboStatus.SelectedIndex = 0
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clsTraining_Calendar_Master
            objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._StartDate
            dtPeriodEnd = objPeriod._EndDate

            Call SetDateFormat()

            objPlannedTReport.generateReportNew(Session("Database_Name"), _
                                         Session("UserId"), _
                                         Session("Fin_year"), _
                                         Session("CompanyUnkId"), _
                                         dtPeriodStart, _
                                         dtPeriodEnd, _
                                         Session("UserAccessModeSetting"), True, _
                                         Session("ExportReportPath"), _
                                         False, _
                                         0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objPlannedTReport._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)

            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblStatus.ID, Me.LblStatus.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.LblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblStartDate.ID, Me.LblStartDate.Text)
            Me.LblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblEndDate.ID, Me.LblEndDate.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
