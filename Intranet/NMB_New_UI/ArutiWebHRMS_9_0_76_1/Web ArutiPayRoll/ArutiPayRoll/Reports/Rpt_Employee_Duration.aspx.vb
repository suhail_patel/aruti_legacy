﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Employee_Duration
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmpDuration As clsEmployeeDurationReport
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private ReadOnly mstrModuleName As String = "frmEmployeeDurationReport"
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Private Function "

    Private Sub Fill_Combo()
        Dim objEmp As New clsEmployee_Master
        Dim objJob As New clsJobs
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Employee", True)
            'Shani(24-Aug-2015) -- End

            With drpEmployee
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
            End With

            dsList = objJob.getComboList("Job", True)
            With drpJob
                .DataTextField = "name"
                .DataValueField = "jobunkid"
                .DataSource = dsList.Tables("Job")
                .DataBind()
            End With

            dsList = objMaster.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ")", "", DataViewRowState.CurrentRows).ToTable
            With drpAllocation
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dtTable
                .DataBind()
            End With
            With drpReportType
                .Items.Clear()
                'S.SANDEEP [ 19 JUN 2014 ] -- START
                'ENHANCEMENT : LANGUAGE CHANGES
                '.Items.Add("Employee Duration on Job Title")
                '.Items.Add("Employee Duration Allocation Wise")
                'Language.setLanguage(mstrModuleName)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee Duration on Job Title"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee Duration Allocation Wise"))
                'S.SANDEEP [ 19 JUN 2014 ] -- END
                'Sohail (24 Dec 2018) -- Start
                'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Employee Duration On Experience"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Employee Year of Service"))
                'Sohail (24 Dec 2018) -- End
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpEmployee.SelectedValue = 0
            drpJob.SelectedValue = 0
            txtFromYear.Text = "0"
            TxtToYear.Text = "0"
            chkInactiveemp.Checked = False
            drpAllocation.SelectedValue = 1
            drpReportType.SelectedIndex = 0
            Call drpReportType_SelectedIndexChanged(New Object(), New EventArgs())
            chkYear.Checked = False : chkMonth.Checked = False : chkDays.Checked = False
            txtAllocFYear.Text = "" : txtAllocTYear.Text = ""
            txtAllocFMonth.Text = "" : txtAllocTMonth.Text = ""
            txtAllocFDay.Text = "" : txtAllocTDay.Text = ""

            txtAllocFYear.Enabled = chkYear.Checked
            txtAllocTYear.Enabled = chkYear.Checked
            txtAllocFMonth.Enabled = chkMonth.Checked
            txtAllocTMonth.Enabled = chkMonth.Checked
            txtAllocFDay.Enabled = chkDays.Checked
            txtAllocTDay.Enabled = chkDays.Checked

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpDuration.SetDefaultValue()

            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'ENHANCEMENT : LANGUAGE CHANGES
            If Validation() = False Then Return False
            'S.SANDEEP [ 19 JUN 2014 ] -- END

            objEmpDuration._EmployeeId = drpEmployee.SelectedValue

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'objEmpDuration._EmployeeName = drpEmployee.SelectedText
            objEmpDuration._EmployeeName = drpEmployee.SelectedItem.Text
            'Nilay (01-Feb-2015) -- End
            objEmpDuration._ReportTypeId = drpReportType.SelectedIndex
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'objEmpDuration._ReportTypeName = drpReportType.SelectedText
            objEmpDuration._ReportTypeName = drpReportType.SelectedItem.Text
            'Nilay (01-Feb-2015) -- End

            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            'If drpReportType.SelectedIndex = 0 Then
            If drpReportType.SelectedIndex = 0 OrElse drpReportType.SelectedIndex = 2 OrElse drpReportType.SelectedIndex = 3 Then
                'Sohail (24 Dec 2018) -- End
                objEmpDuration._JobId = drpJob.SelectedValue
                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'objEmpDuration._JobName = drpJob.SelectedText
                objEmpDuration._JobName = drpJob.SelectedItem.Text
                'Nilay (01-Feb-2015) -- End
                Dim mDec1, mDec2 As Decimal
                Decimal.TryParse(txtFromYear.Text, mDec1)
                objEmpDuration._FromYear = mDec1
                Decimal.TryParse(TxtToYear.Text, mDec2)
                objEmpDuration._ToYear = mDec2
                objEmpDuration._IsActive = chkInactiveemp.Checked

                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objEmpDuration._Exp_FromCurrDate = chkFromCurrDate.Checked
                objEmpDuration._CurrDate_Caption = chkFromCurrDate.Text
                'S.SANDEEP [ 22 FEB 2013 ] -- END

            ElseIf drpReportType.SelectedIndex = 1 Then
                Dim StrStringIds As String = ""
                For i As Integer = 0 To chkAllocation.Items.Count - 1
                    If chkAllocation.Items(i).Selected = True Then
                        StrStringIds &= "," & chkAllocation.Items(i).Value.ToString
                    End If
                Next
                If StrStringIds.Trim.Length > 0 Then StrStringIds = Mid(StrStringIds, 2)

                Select Case drpAllocation.SelectedValue
                    Case enAllocation.BRANCH
                        objEmpDuration._SelectAlloc = ",ISNULL(hrstation_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrstation_master ON hrstation_master.stationunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.stationunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "stationunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrstation_master ON hrstation_master.stationunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.DEPARTMENT_GROUP
                        objEmpDuration._SelectAlloc = ",ISNULL(hrdepartment_group_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.deptgroupunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "deptgroupunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.DEPARTMENT
                        objEmpDuration._SelectAlloc = ",ISNULL(hrdepartment_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.departmentunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "departmentunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.SECTION_GROUP
                        objEmpDuration._SelectAlloc = ",ISNULL(hrsectiongroup_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.sectiongroupunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "sectiongroupunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.SECTION
                        objEmpDuration._SelectAlloc = ",ISNULL(hrsection_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrsection_master ON hrsection_master.sectionunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.sectionunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "sectionunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrsection_master ON hrsection_master.sectionunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.UNIT_GROUP
                        objEmpDuration._SelectAlloc = ",ISNULL(hrunitgroup_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.unitgroupunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "unitgroupunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.UNIT
                        objEmpDuration._SelectAlloc = ",ISNULL(hrunit_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrunit_master ON hrunit_master.unitunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.unitunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "unitunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrunit_master ON hrunit_master.unitunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.TEAM
                        objEmpDuration._SelectAlloc = ",ISNULL(hrteam_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrteam_master ON hrteam_master.teamunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.teamunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "teamunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrteam_master ON hrteam_master.teamunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.JOB_GROUP
                        objEmpDuration._SelectAlloc = ",ISNULL(hrjobgroup_master.name,'') AS Allocation "
                        objEmpDuration._AlloctionJoin = "JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrallocation_tracking.allocationunkid "
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.JOBS
                        objEmpDuration._SelectAlloc = ",ISNULL(hrjob_master.job_name,'') AS Allocation "
                        objEmpDuration._AlloctionJoin = "JOIN hrjob_master ON hrjob_master.jobunkid = hrallocation_tracking.allocationunkid "
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.CLASS_GROUP
                        objEmpDuration._SelectAlloc = ",ISNULL(hrclassgroup_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.classgroupunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "classgroupunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                    Case enAllocation.CLASSES
                        objEmpDuration._SelectAlloc = ",ISNULL(hrclasses_master.name,'') AS Allocation "
                        'Sohail (24 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                        'objEmpDuration._AlloctionJoin = "JOIN hrclasses_master ON hrclasses_master.classesunkid = hrallocation_tracking.allocationunkid "
                        objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.classunkid,0) AS allocationunkid "
                        objEmpDuration._AllocationColName = "classunkid"
                        objEmpDuration._AlloctionJoin = "JOIN hrclasses_master ON hrclasses_master.classesunkid = [@EM].allocid "
                        'Sohail (24 Dec 2018) -- End
                        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

                End Select

                objEmpDuration._IsActive = False
                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'objEmpDuration._Allocation_Name = drpAllocation.SelectedText
                objEmpDuration._Allocation_Name = drpAllocation.SelectedItem.Text
                'Nilay (01-Feb-2015) -- End
                objEmpDuration._AllocationTypeId = drpAllocation.SelectedValue

                If chkYear.Checked = True Then
                    Dim mDec1, mDec2 As Decimal
                    If txtAllocFYear.Text <> "" AndAlso txtAllocTYear.Text <> "" Then
                        Decimal.TryParse(txtAllocFYear.Text, mDec1)
                        Decimal.TryParse(txtAllocTYear.Text, mDec2)
                        objEmpDuration._Alloc_YearFrom = mDec1
                        objEmpDuration._Alloc_YearTo = mDec2
                    End If
                End If

                If chkMonth.Checked = True Then
                    Dim mDec1, mDec2 As Decimal
                    If txtAllocFMonth.Text <> "" AndAlso txtAllocTMonth.Text <> "" Then
                        Decimal.TryParse(txtAllocFMonth.Text, mDec1)
                        Decimal.TryParse(txtAllocTMonth.Text, mDec2)
                        objEmpDuration._Alloc_MonthFrom = mDec1
                        objEmpDuration._Alloc_MonthTo = mDec2
                    End If
                End If

                If chkDays.Checked = True Then
                    Dim mDec1, mDec2 As Decimal
                    If txtAllocFDay.Text <> "" AndAlso txtAllocTDay.Text <> "" Then
                        Decimal.TryParse(txtAllocFDay.Text, mDec1)
                        Decimal.TryParse(txtAllocTDay.Text, mDec2)
                        objEmpDuration._Alloc_DayFrom = mDec1
                        objEmpDuration._Alloc_DayTo = mDec2
                    End If
                End If

                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objEmpDuration._CurrAllocation = chkCurrentAllocation.Checked
                objEmpDuration._CurrAllocationCaption = chkCurrentAllocation.Text
                'S.SANDEEP [ 22 FEB 2013 ] -- END

            End If

            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            If dtpAsOnDate.IsNull Then
                objEmpDuration._AsOnDate = DateAndTime.Today.Date
            Else
                objEmpDuration._AsOnDate = dtpAsOnDate.GetDate
            End If
            'Sohail (24 Dec 2018) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmpDuration._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case drpAllocation.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name", True)
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name", True)
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name", True)
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name", True)
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name", True)
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name", True)
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name", True)
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name", True)
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name", True)
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName", True)
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name", True)
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name", True)
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String, Optional ByVal blnFlag As Boolean = False)
        Try
            If blnFlag = True Then
                Me.ViewState("dTable") = dTable
            End If
            Me.ViewState("StrDisColName") = StrDisColName
            Me.ViewState("StrIdColName") = StrIdColName
            chkAllocation.Items.Clear()
            chkAllocation.DataTextField = StrDisColName
            chkAllocation.DataValueField = StrIdColName
            chkAllocation.DataSource = dTable
            chkAllocation.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'Language.setLanguage(mstrModuleName)
            'S.SANDEEP [ 19 JUN 2014 ] -- END

            If drpReportType.SelectedIndex = 0 Then
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If TxtFromYear.Text <= 0 Then
                If TxtToYear.Text <= 0 Then
                    'Pinkal (16-Apr-2016) -- End
                    'DisplayMessage.DisplayMessage("Please select to year Value greater than 0.", Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please select to year Value greater than 0."), Me)
                    Return False
                End If

                If TxtToYear.Text < txtFromYear.Text Then
                    'DisplayMessage.DisplayMessage("Invalid value for From Year.Please Select Proper value for From Year.", Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Invalid value for From Year.Please Select Proper value for From Year."), Me)
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Controls Events "

    Protected Sub drpReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpReportType.SelectedIndexChanged
        Try
            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            dtpAsOnDate.SetDate = Nothing
            dtpAsOnDate.Enabled = False
            'Sohail (24 Dec 2018) -- End

            Select Case drpReportType.SelectedIndex
                Case 0, 2
                    'Sohail (24 Dec 2018) - [2]
                    pnlRpt2.Visible = False : pnlRpt1.Visible = True
                    'S.SANDEEP [ 22 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    chkCurrentAllocation.Checked = False
                    'S.SANDEEP [ 22 FEB 2013 ] -- END

                    'Sohail (24 Dec 2018) -- Start
                    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                    If drpReportType.SelectedIndex = 0 Then
                        chkFromCurrDate.Checked = False
                        chkFromCurrDate.Enabled = False

                        chkFromCurrDate.Enabled = True

                        drpJob.Enabled = True

                    Else
                        chkFromCurrDate.Checked = False
                        chkFromCurrDate.Enabled = False

                        drpJob.Enabled = False

                    End If
                    'Sohail (24 Dec 2018) -- End

                Case 1
                    pnlRpt1.Visible = False : pnlRpt2.Visible = True
                    'S.SANDEEP [ 22 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    chkFromCurrDate.Checked = False
                    'S.SANDEEP [ 22 FEB 2013 ] -- END
                    Call drpAllocation_SelectedIndexChanged(sender, e)

                    'Sohail (24 Dec 2018) -- Start
                    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                Case 3
                    pnlRpt2.Visible = False : pnlRpt1.Visible = True
                    chkCurrentAllocation.Checked = False
                    chkFromCurrDate.Checked = False
                    chkFromCurrDate.Enabled = False

                    drpJob.Enabled = False

                    dtpAsOnDate.Enabled = True

                    objEmpDuration._ReportTypeId = drpReportType.SelectedIndex
                    Call chkFromCurrDate_CheckedChanged(chkFromCurrDate, New System.EventArgs)
                    'Sohail (24 Dec 2018) -- End
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub drpAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpAllocation.SelectedIndexChanged
        Try
            Call Fill_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub chkYear_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkYear.CheckedChanged
        Try
            txtAllocFYear.Enabled = chkYear.Checked
            txtAllocTYear.Enabled = chkYear.Checked
            If chkYear.Checked = True Then
                txtAllocFYear.Text = "" : txtAllocTYear.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub chkMonth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMonth.CheckedChanged
        Try
            txtAllocFMonth.Enabled = chkMonth.Checked
            txtAllocTMonth.Enabled = chkMonth.Checked
            If chkMonth.Checked = True Then
                txtAllocFMonth.Text = "" : txtAllocTMonth.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub chkDays_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDays.CheckedChanged
        Try
            txtAllocFDay.Enabled = chkDays.Checked
            txtAllocTDay.Enabled = chkDays.Checked
            If chkDays.Checked = True Then
                txtAllocFDay.Text = "" : txtAllocTDay.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If chkAllocation.Items.Count > 0 Then
                Dim dView As DataView = CType(Me.ViewState("dTable"), DataTable).DefaultView
                dView.RowFilter = Me.ViewState("StrDisColName") & " LIKE '%" & txtSearch.Text & "%' "
                Call Fill_List(dView.ToTable, Me.ViewState("StrIdColName"), Me.ViewState("StrDisColName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [ 22 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkFromCurrDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFromCurrDate.CheckedChanged
        Try
            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            'objEmpDuration.Set_Sort_Collection(True)
            objEmpDuration.Set_Sort_Collection(chkFromCurrDate.Checked)
            objEmpDuration.setDefaultOrderBy(0)
            'Sohail (24 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 22 FEB 2013 ] -- END

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEmpDuration = New clsEmployeeDurationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'S.SANDEEP [ 19 JUN 2014 ] -- START
                'ENHANCEMENT : LANGUAGE CHANGES
                Call SetLanguage()
                'S.SANDEEP [ 19 JUN 2014 ] -- END
                Call Fill_Combo()
                Call drpReportType_SelectedIndexChanged(sender, e)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmpDuration._CompanyUnkId = Session("CompanyUnkId")
            objEmpDuration._UserUnkId = Session("UserId")
            objEmpDuration._UserAccessFilter = Session("AccessLevelFilterString")

            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If drpReportType.SelectedIndex = 0 Then
            '    objEmpDuration.setDefaultOrderBy(0)
            'End If
            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            'If drpReportType.SelectedIndex = 0 AndAlso chkFromCurrDate.Checked = False Then
            '   objEmpDuration.setDefaultOrderBy(0)
            'End If
            objEmpDuration.Set_Sort_Collection(chkFromCurrDate.Checked)
            objEmpDuration.setDefaultOrderBy(0)
            'Sohail (24 Dec 2018) -- End

            'S.SANDEEP [ 22 FEB 2013 ] -- END

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpDuration.generateReport(drpReportType.SelectedIndex, enPrintAction.None, enExportAction.None)
            objEmpDuration.generateReportNew(Session("Database_Name"), _
                                             Session("UserId"), _
                                             Session("Fin_year"), _
                                             Session("CompanyUnkId"), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             Session("UserAccessModeSetting"), True, _
                                             Session("ExportReportPath"), _
                                             Session("OpenAfterExport"), _
                                             drpReportType.SelectedIndex, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objEmpDuration._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END
#End Region


    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocation.ID, Me.lblAllocation.Text)
            Me.lblDFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDFrom.ID, Me.lblDFrom.Text)
            Me.lblMFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMFrom.ID, Me.lblMFrom.Text)
            Me.lblYFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromYear.ID, Me.lblFromYear.Text)
            Me.lblDTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDTo.ID, Me.lblDTo.Text)
            Me.lblMTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMTo.ID, Me.lblMTo.Text)
            Me.lblYTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblYTo.ID, Me.lblYTo.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.lblFromYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromYear.ID, Me.lblFromYear.Text)
            Me.LblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblJob.ID, Me.LblJob.Text)
            Me.lblRType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRType.ID, Me.lblRType.Text)
            Me.lblToYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToYear.ID, Me.lblToYear.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.chkCurrentAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkCurrentAllocation.ID, Me.chkCurrentAllocation.Text)
            Me.chkDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkDays.ID, Me.chkDays.Text)
            Me.chkFromCurrDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkFromCurrDate.ID, Me.chkFromCurrDate.Text)
            Me.chkYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkYear.ID, Me.chkYear.Text)
            Me.chkMonth.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkMonth.ID, Me.chkMonth.Text)
            Me.lblAsOnDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAsOnDate.ID, Me.lblAsOnDate.Text) 'Sohail (24 Dec 2018)
            Me.BtnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END

End Class
