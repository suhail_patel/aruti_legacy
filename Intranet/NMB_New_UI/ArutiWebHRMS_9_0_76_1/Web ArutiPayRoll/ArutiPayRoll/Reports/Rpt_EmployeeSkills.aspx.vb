﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmployeeSkills
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objSkillReport As clsEmployeeSkillsReport
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private ReadOnly mstrModuleName As String = "frmEmployeeSkillsReport"
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjCMaster As New clsCommon_Master
        Dim ObjSMaster As New clsskill_master
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)

            'Shani(24-Aug-2015) -- End

            With drpEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = ObjCMaster.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SKCAT")
            With drpSkillCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("SKCAT")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = ObjSMaster.getComboList("SKMAST", True)
            With drpSkill
                .DataValueField = "skillunkid"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("SKMAST")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
            ObjCMaster = Nothing
            ObjSMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpEmployee.SelectedValue = 0
            drpSkillCategory.SelectedValue = 0
            drpSkill.SelectedValue = 0
            TxtCode.Text = ""
            objSkillReport.setDefaultOrderBy(0)
            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSkillReport.SetDefaultValue()
            objSkillReport._EmployeeId = drpEmployee.SelectedValue
            objSkillReport._EmployeeName = drpEmployee.SelectedItem.Text
            objSkillReport._EmployeeCode = TxtCode.Text
            objSkillReport._SkillCategoryId = drpSkillCategory.SelectedValue
            objSkillReport._SkillCategoryName = drpSkillCategory.SelectedItem.Text
            objSkillReport._SkillId = drpSkill.SelectedValue
            objSkillReport._SkillName = drpSkill.SelectedItem.Text
            objSkillReport._IsActive = chkInactiveemp.Checked

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objSkillReport._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objSkillReport = New clsEmployeeSkillsReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'S.SANDEEP [ 19 JUN 2014 ] -- START
                'ENHANCEMENT : LANGUAGE CHANGES
                Call SetLanguage()
                'S.SANDEEP [ 19 JUN 2014 ] -- END
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objSkillReport._CompanyUnkId = Session("CompanyUnkId")
            objSkillReport._UserUnkId = Session("UserId")
            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objSkillReport._UserAccessFilter = Session("AccessLevelFilterString")
            'S.SANDEEP [ 12 NOV 2012 ] -- END
            objSkillReport.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objSkillReport.generateReport(0, enPrintAction.None, enExportAction.None)
            objSkillReport.generateReportNew(Session("Database_Name"), _
                                             Session("UserId"), _
                                             Session("Fin_year"), _
                                             Session("CompanyUnkId"), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             Session("UserAccessModeSetting"), True, _
                                             Session("ExportReportPath"), _
                                             Session("OpenAfterExport"), _
                                             0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objSkillReport._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub drpSkillCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpSkillCategory.SelectedIndexChanged
        Try
            If CInt(drpSkillCategory.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objSKMaster As New clsskill_master
                dsList = objSKMaster.getComboList("SMAST", True, CInt(drpSkillCategory.SelectedValue))
                With drpSkill
                    .DataValueField = "skillunkid"
                    .DataTextField = "NAME"
                    .DataSource = dsList.Tables("SMAST")
                    .DataBind()
                    .SelectedValue = 0
                End With
                dsList.Dispose()
                objSKMaster = Nothing
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpSkillCategory_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Protected Sub drpEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.DataBound
        Try         'Hemant (13 Aug 2020)
            If drpEmployee.Items.Count > 0 Then
                For Each lstItem As ListItem In drpEmployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                drpEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblEmployeeCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
            Me.lblSkill.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSkill.ID, Me.lblSkill.Text)
            Me.lblSkillCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSkillCategory.ID, Me.lblSkillCategory.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

            Me.BtnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END
End Class
