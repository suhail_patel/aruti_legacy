﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Employee_Listing.aspx.vb"
    Inherits="Reports_Rpt_Employee_Listing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Listing Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By" CssClass="lnkhover">
                                        <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpemployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeementType" runat="server" Text="Employement Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployeementType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkGeneratePeriod" runat="server" Text="Generate Based on Period"
                                            AutoPostBack="true" OnCheckedChanged="chkGeneratePeriod_CheckedChanged" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblBirthdateFrom" runat="server" Text="Birthdate From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblBirthdateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpBirthdateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAnniversaryFrom" runat="server" Text="Anniversary Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAnniversaryDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAnniversaryDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAnniversaryDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblFirstAppointDateFrom" runat="server" Text="First Appointment Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFAppointDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblFirstAppointDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFAppointDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAppointDateFrom" runat="server" Text="Appointment Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAppointDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAppointDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAppointDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblConfirmationDateFrom" runat="server" Text="Confirmation Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpConfirmationDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblConfirmationDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpConfirmationDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationStartDateFrom" runat="server" Text="Probation Start Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationStartDateFrom" runat="server" AutoPostBack="false">
                                        </uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationStartDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationStartDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationEndDateFrom" runat="server" Text="Probation End Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationEndDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationEndDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedStartDateFrom" runat="server" Text="Suspended Start Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedStartDateFrom" runat="server" AutoPostBack="false">
                                        </uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedStartDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedStartDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedEndDateFrom" runat="server" Text="Suspended End Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedEndDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedEndDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblReinstatementDateFrom" runat="server" Text="Reinstatement Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpReinstatementFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblReinstatementDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpReinstatementTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblEOCDateFrom" runat="server" Text="End Of Contract Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEOCDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblEOCDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEOCDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                            Checked="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" Checked="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
