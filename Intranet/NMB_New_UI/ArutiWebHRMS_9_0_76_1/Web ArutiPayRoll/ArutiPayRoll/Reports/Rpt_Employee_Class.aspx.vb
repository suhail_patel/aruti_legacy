﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Employee_Class
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmpWorkingStation As clsEmployeeWorkingStation

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeWorkingStation"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objClass As New clsClass
            Dim objDept As New clsDepartment
            Dim objJob As New clsJobs

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Employee", True)
            'Shani(24-Aug-2015) -- End

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'drpemployee.DataTextField = "employeename"
            drpemployee.DataTextField = "EmpCodeName"
            'Nilay (09-Aug-2016) -- End
            drpemployee.DataValueField = "employeeunkid"
            drpemployee.DataSource = dsList.Tables("Employee")
            drpemployee.DataBind()

            dsList = objClass.getComboList("Class", True)
            drpclass.DataTextField = "name"
            drpclass.DataValueField = "classesunkid"
            drpclass.DataSource = dsList.Tables("Class")
            drpclass.DataBind()

            dsList = objDept.getComboList("Department", True)
            drpDepartment.DataTextField = "name"
            drpDepartment.DataValueField = "departmentunkid"
            drpDepartment.DataSource = dsList.Tables("Department")
            drpDepartment.DataBind()

            dsList = objJob.getComboList("Job", True)
            drpJob.DataTextField = "name"
            drpJob.DataValueField = "jobunkid"
            drpJob.DataSource = dsList.Tables("Job")
            drpJob.DataBind()


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpemployee.SelectedValue = 0
            drpclass.SelectedValue = 0
            drpDepartment.SelectedValue = 0
            drpJob.SelectedValue = 0
            objEmpWorkingStation.setDefaultOrderBy(0)
            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpWorkingStation.SetDefaultValue()

            objEmpWorkingStation._EmployeeId = CInt(drpemployee.SelectedValue)

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objEmpWorkingStation._EmployeeName = drpemployee.Text
            objEmpWorkingStation._EmployeeName = drpemployee.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            objEmpWorkingStation._ClassId = CInt(drpclass.SelectedValue)

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objEmpWorkingStation._ClassName = drpclass.Text
            objEmpWorkingStation._ClassName = drpclass.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            objEmpWorkingStation._DepartmentId = CInt(drpDepartment.SelectedValue)

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objEmpWorkingStation._Department = drpDepartment.Text
            objEmpWorkingStation._Department = drpDepartment.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End

            objEmpWorkingStation._JobId = CInt(drpJob.SelectedValue)

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objEmpWorkingStation._Job = drpJob.Text
            objEmpWorkingStation._Job = drpJob.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            objEmpWorkingStation._IncludeInactiveEmp = chkInactiveemp.Checked
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmpWorkingStation._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End
            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEmpWorkingStation = New clsEmployeeWorkingStation(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End
                Call FillCombo()
                'For i As Integer = 0 To objAgeAnalysis.Field_OnDetailReport.Count - 1
                '    chkSort.Items.Add(objAgeAnalysis.Field_OnDetailReport.ColumnItem(i).DisplayName)
                'Next
                'chkSort.Items(0).Selected = True
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmpWorkingStation.setDefaultOrderBy(0)
            objEmpWorkingStation._CompanyUnkId = Session("CompanyUnkId")
            objEmpWorkingStation._UserUnkId = Session("UserId")

            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            objEmpWorkingStation._UserAccessFilter = Session("AccessLevelFilterString")
            'Pinkal (12-Nov-2012) -- End

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpWorkingStation.generateReport(0, enPrintAction.None, enExportAction.None)
            objEmpWorkingStation.generateReportNew(Session("Database_Name"), _
                                                   Session("UserId"), _
                                                   Session("Fin_year"), _
                                                   Session("CompanyUnkId"), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   Session("UserAccessModeSetting"), True, _
                                                   Session("ExportReportPath"), _
                                                   Session("OpenAfterExport"), _
                                                   0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objEmpWorkingStation._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region

    Protected Sub drpemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpemployee.DataBound
        Try         'Hemant (13 Aug 2020)
            If drpemployee.Items.Count > 0 Then
                For Each lstItem As ListItem In drpemployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                drpemployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End



            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblClasses.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClasses.ID, Me.lblClasses.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End


End Class
