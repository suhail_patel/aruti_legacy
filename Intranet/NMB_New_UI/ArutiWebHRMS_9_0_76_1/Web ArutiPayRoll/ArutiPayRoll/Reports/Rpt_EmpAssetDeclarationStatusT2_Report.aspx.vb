﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmpAssetDeclarationStatusT2_Report
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmployeeAssetDeclarationStatusReportT2"
    Private objclsAssetDeclarationT2 As clsAssetDeclarationT2
    Dim objAssetDeclare As New clsAssetdeclaration_masterT2
    Dim objRptAssetDeclareStatus As ArutiReports.clsAsset_Declaration_Status_ReportT2
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
    Dim dtFinYearList As DataTable
    Private mdtFinStartDate As DateTime
    Private mdtFinEndDate As DateTime
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objAssetDeclare = New clsAssetdeclaration_masterT2
            objRptAssetDeclareStatus = New ArutiReports.clsAsset_Declaration_Status_ReportT2(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetControlCaptions()
                'FillCombo()
                ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                dtFinYearList = Me.ViewState("dtFinYearList")
                mdtFinStartDate = Me.ViewState("mdtFinStartDate")
                mdtFinEndDate = Me.ViewState("mdtFinEndDate")
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            Me.ViewState.Add("dtFinYearList", dtFinYearList)
            Me.ViewState.Add("mdtFinStartDate", mdtFinStartDate)
            Me.ViewState.Add("mdtFinEndDate", mdtFinEndDate)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objclsMasterData As New clsMasterData

        Try
            objEmp = New clsEmployee_Master
            Dim blnIncludeInactiveEmp As Boolean = False
            Dim dsList As DataSet = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               blnIncludeInactiveEmp, "Employee", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dsCombos As DataSet = objclsMasterData.getComboListAssetDeclarationStatusList("Status")
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Status").Copy
                .DataBind()
                .SelectedValue = 0
            End With

            If Session("AssetDeclarationFromDate").ToString = "" Then
                dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
                'Hemant (02 Feb 2019) -- Start
                'NMB Enhancement : It Should display previous and current year in FY combo in Asset Declaration Report as per the tenure start date set.
                'ElseIf eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
            ElseIf eZeeDate.convertDate(Session("AssetDeclarationFromDate")) IsNot Nothing Then
                'Hemant (02 Feb 2019) -- End

                'Hemant (02 Jan 2019) -- Start
                'NMB - ENHANCEMENT
                'dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"), , , True, Session("financialyear_name"))
                'Hemant (02 Feb 2019) -- Start
                'NMB Enhancement : It Should display previous and current year in FY combo in Asset Declaration Report as per the tenure start date set.
                'dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True)
                dsCombos = objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True)
                For Each drYearRow As DataRow In dsCombos.Tables(0).Select("yearunkid < 0 ")
                    If drYearRow.Item("yearunkid") = -99 Then
                        drYearRow.Item("start_date") = eZeeDate.convertDate(Session("fin_startdate").AddYears(1))
                        drYearRow.Item("end_date") = eZeeDate.convertDate(Session("fin_enddate").AddYears(1))
                    ElseIf drYearRow.Item("yearunkid") = -98 Then
                        drYearRow.Item("start_date") = eZeeDate.convertDate(Session("fin_startdate").AddYears(-1))
                        drYearRow.Item("end_date") = eZeeDate.convertDate(Session("fin_enddate").AddYears(-1))
                    End If
                Next
                Dim StrYear As String
                Dim intYear As Integer
                If eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    intYear = eZeeDate.convertDate(Session("AssetDeclarationFromDate")).Year - CDate(Session("fin_startdate")).Year
                Else
                    intYear = -1
                End If
                StrYear = CDate(Session("fin_startdate")).AddYears(intYear).Year & "-" & CDate(Session("fin_enddate")).AddYears(intYear).Year
                Dim drRow As DataRow() = dsCombos.Tables(0).Select("financialyear_name='" & StrYear & "'")
                If drRow.Length > 0 Then
                    For Each drRowNew As DataRow In dsCombos.Tables(0).Select("financialyear_name='" & StrYear & "'")
                        Dim TenureYear As Integer = 0
                        Dim TenureStartDate As String
                        TenureYear = drRowNew.Item("yearunkid")
                        TenureStartDate = eZeeDate.convertDate(drRowNew.Item("start_date"))

                        Dim i As Integer = 0
                        Do While i <= dsCombos.Tables(0).Rows.Count - 1
                            If dsCombos.Tables(0).Rows(i)("start_date") Is DBNull.Value Then
                                dsCombos.Tables(0).Rows(i).Delete()
                                dsCombos.Tables(0).AcceptChanges()
                                i -= 1
                            ElseIf eZeeDate.convertDate(dsCombos.Tables(0).Rows(i)("start_date")) <= TenureStartDate Then
                            Else
                                dsCombos.Tables(0).Rows(i).Delete()
                                dsCombos.Tables(0).AcceptChanges()
                                i -= 1
                            End If
                            i += 1
                        Loop
                    Next
                Else
                    dsCombos.Tables(0).Rows.Clear()
                End If
                'Hemant (02 Feb 2019) -- End
                'Hemant (02 Jan 2019) -- End
            Else
                dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
            End If

            dtFinYearList = dsCombos.Tables("Year").Copy

            With cboFinalcialYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dtFinYearList
                .DataBind()
                .SelectedValue = 0
            End With

            If Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").Trim.ToString <> "" Then
                cboFinalcialYear.SelectedValue = -99
            End If

            cboFinalcialYear_SelectedIndexChanged(cboFinalcialYear, New System.EventArgs)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            FillCombo()
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            objRptAssetDeclareStatus.setDefaultOrderBy(cboEmployee.SelectedIndex)
            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Hemant (02 Feb 2019) -- Start
    'NMB Enhancement : It Should display previous and current year in FY combo in Asset Declaration Report as per the tenure start date set.
    Private Function IsValidData() As Boolean
        Try
            If CInt(cboFinalcialYear.SelectedIndex) < 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "Financial year is compulsory information.Please Select Financial year."), Me)
                cboFinalcialYear.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
    'Hemant (02 Feb 2019) -- End

    Private Function SetFilter() As Boolean
        Try

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtpDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Exit Function
            End If
            'Pinkal (31-Jul-2020) -- End

            objRptAssetDeclareStatus._ViewByIds = mstrStringIds
            objRptAssetDeclareStatus._ViewIndex = mintViewIdx
            objRptAssetDeclareStatus._ViewByName = mstrStringName
            objRptAssetDeclareStatus._Analysis_Fields = mstrAnalysis_Fields
            objRptAssetDeclareStatus._Analysis_Join = mstrAnalysis_Join
            objRptAssetDeclareStatus._Analysis_OrderBy = mstrAnalysis_OrderBy
            objRptAssetDeclareStatus._Report_GroupName = mstrReport_GroupName
            objRptAssetDeclareStatus._IncluderInactiveEmp = chkInactiveemp.Checked
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try

            If SetFilter() = False Then Exit Sub

            'Hemant (02 Feb 2019) -- Start
            'NMB Enhancement : It Should display previous and current year in FY combo in Asset Declaration Report as per the tenure start date set.
            If IsValidData() = False Then Exit Sub
            'Hemant (02 Feb 2019) -- End


            Dim objMaster As New clsMasterData
            Dim blnApplyAccessFilter As Boolean = True

            Dim dicDB As New Dictionary(Of String, String)

            objRptAssetDeclareStatus._EmployeeId = cboEmployee.SelectedValue
            objRptAssetDeclareStatus._EmployeeName = cboEmployee.SelectedItem.Text
            objRptAssetDeclareStatus._StatusId = cboStatus.SelectedValue
            objRptAssetDeclareStatus._StatusName = cboStatus.SelectedItem.Text
            objRptAssetDeclareStatus._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objRptAssetDeclareStatus._AsOnDate = dtpDate.GetDate.Date
            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            If Session("AssetDeclarationFromDate").ToString.Trim = "" Or Session("AssetDeclarationToDate").ToString.Trim = "" Then
                objRptAssetDeclareStatus._AssetDeclarationFromDate = dtpDate.GetDate.Date
                objRptAssetDeclareStatus._AssetDeclarationToDate = dtpDate.GetDate.Date
            Else
                objRptAssetDeclareStatus._AssetDeclarationFromDate = eZeeDate.convertDate(Session("AssetDeclarationFromDate"))
                objRptAssetDeclareStatus._AssetDeclarationToDate = eZeeDate.convertDate(Session("AssetDeclarationToDate"))
            End If
            objRptAssetDeclareStatus._FinStartDate = mdtFinStartDate
            objRptAssetDeclareStatus._FinEndDate = mdtFinEndDate
            objRptAssetDeclareStatus._FinancialYearId = cboFinalcialYear.SelectedValue
            objRptAssetDeclareStatus._FinancialYearName = cboFinalcialYear.SelectedItem.Text
            'Hemant (05 Dec 2018) -- End

            objRptAssetDeclareStatus._Advance_Filter = mstrAdvanceFilter

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                objRptAssetDeclareStatus.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
            Else
                objRptAssetDeclareStatus.generateReportNew(Session("Database_Name").ToString(), 1, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
            End If
            Session("objRpt") = objRptAssetDeclareStatus._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub

            Dim objMaster As New clsMasterData
            Dim blnApplyAccessFilter As Boolean = True

            Dim dicDB As New Dictionary(Of String, String)

            objRptAssetDeclareStatus._EmployeeId = cboEmployee.SelectedValue
            objRptAssetDeclareStatus._EmployeeName = cboEmployee.SelectedItem.Text
            objRptAssetDeclareStatus._StatusId = cboStatus.SelectedValue
            objRptAssetDeclareStatus._StatusName = cboStatus.SelectedItem.Text
            objRptAssetDeclareStatus._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objRptAssetDeclareStatus._AsOnDate = dtpDate.GetDate.Date
            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            If Session("AssetDeclarationFromDate").ToString.Trim = "" Or Session("AssetDeclarationToDate").ToString.Trim = "" Then
                objRptAssetDeclareStatus._AssetDeclarationFromDate = dtpDate.GetDate.Date
                objRptAssetDeclareStatus._AssetDeclarationToDate = dtpDate.GetDate.Date
            Else
                objRptAssetDeclareStatus._AssetDeclarationFromDate = eZeeDate.convertDate(Session("AssetDeclarationFromDate"))
                objRptAssetDeclareStatus._AssetDeclarationToDate = eZeeDate.convertDate(Session("AssetDeclarationToDate"))
            End If
            objRptAssetDeclareStatus._FinStartDate = mdtFinStartDate
            objRptAssetDeclareStatus._FinEndDate = mdtFinEndDate
            objRptAssetDeclareStatus._FinancialYearId = cboFinalcialYear.SelectedValue
            objRptAssetDeclareStatus._FinancialYearName = cboFinalcialYear.SelectedItem.Text
            'Hemant (05 Dec 2018) -- End

            objRptAssetDeclareStatus._Advance_Filter = mstrAdvanceFilter
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'Hemant (28 Jan 2021) -- Start
            'objRptAssetDeclareStatus.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, strFilePath, CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.ExcelExtra)
            objRptAssetDeclareStatus.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, strFilePath, False, 0, enPrintAction.None, enExportAction.ExcelExtra)
            'Hemant (28 Jan 2021) -- End
            'Session("objRpt") = objRptAssetDeclareStatus._Rpt

            If objRptAssetDeclareStatus._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objRptAssetDeclareStatus._FileNameAfterExported

                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnExport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkSetAnalysis_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAnalysisBy_buttonApply_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAnalysisBy_buttonClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Dropdown Event"
    Protected Sub cboFinalcialYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFinalcialYear.SelectedIndexChanged
        Try
            'Hemant (02 Feb 2019) -- Start
            'NMB Enhancement : It Should display previous and current year in FY combo in Asset Declaration Report as per the tenure start date set.
            If cboFinalcialYear.SelectedIndex >= 0 Then
                'Hemant (02 Feb 2019) -- End
                If CInt(cboFinalcialYear.SelectedValue) = -99 Then
                    mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(1)
                    mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(1)
                    'Hemant (02 Jan 2019) -- Start
                    'NMB - ENHANCEMENT
                    'Hemant (02 Feb 2019) -- Start
                    'NMB Enhancement : It Should display previous and current year in FY combo in Asset Declaration Report as per the tenure start date set.
                    'ElseIf CInt(cboFinalcialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                    '    mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                    '    mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
                ElseIf CInt(cboFinalcialYear.SelectedValue) = -98 Then
                    mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                    mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
                ElseIf CInt(cboFinalcialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                    Dim objclsMasterData As New clsMasterData
                    For Each drRow As DataRow In objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True).Tables(0).Rows
                        If CInt(cboFinalcialYear.SelectedValue) = CInt(drRow.Item("yearunkid")) Then
                            mdtFinStartDate = CDate(eZeeDate.convertDate(drRow.Item("start_date"))).Date
                            mdtFinEndDate = CDate(eZeeDate.convertDate(drRow.Item("end_date"))).Date
                            Exit For
                        End If
                    Next
                    'Hemant (02 Feb 2019) -- End


                    'Hemant (02 Jan 2019) -- End
                Else
                    mdtFinStartDate = Session("Fin_StartDate")
                    mdtFinEndDate = Session("Fin_EndDate")
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboFinalcialYear_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            'Me.Title = objRptAssetDeclareStatus._ReportName
            'Me.lblPageHeader.Text = objRptAssetDeclareStatus._ReportName
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objRptAssetDeclareStatus._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objRptAssetDeclareStatus._ReportName)
            'Hemant (24 Dec 2018) -- End

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Hemant (17 Sep 2020) -- End
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            Me.lnkSetAnalysis.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.ToolTip)
            Me.lblFinalcialYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
            'Hemant (24 Dec 2018) -- End
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lnkAdvanceFilter.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)

            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.btnAdvanceFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdvanceFilter.ID, Me.btnAdvanceFilter.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            'Language.setLanguage(mstrModuleName)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, objRptAssetDeclareStatus._ReportName)
            'Hemant (24 Dec 2018) -- End            
            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Language._Object.setCaption("gbFilterCriteria", Me.gbFilterCriteria.Text)
            'Hemant (17 Sep 2020) -- End
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
            'Hemant (24 Dec 2018) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDate.ID, Me.lblDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAdvanceFilter.ID, Me.btnAdvanceFilter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReport.ID, Me.btnReport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region
End Class
