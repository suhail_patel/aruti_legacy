﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmpCurrSalaryGrade
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objSalaryGrade As clsCurrentGradeSalaryReport
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private ReadOnly mstrModuleName As String = "frmCurrSalaryGrade_Report"
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objGradeGroup As New clsGradeGroup
        Dim dsList As New DataSet
        Try
            dsList = objGradeGroup.GetList("List", True)
            chkGradeGrp.DataTextField = "name"
            chkGradeGrp.DataValueField = "gradegroupunkid"
            chkGradeGrp.DataSource = dsList.Tables(0)
            chkGradeGrp.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            chkIncludeInactiveEmp.Checked = False

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Dim item As ListItem
            For Each item In chkGradeGrp.Items
                item.Selected = False
            Next

            If colhGradeGroup.Checked = True Then
                colhGradeGroup.Checked = False
                Dim item1 As ListItem
                For Each item1 In chkGradeGrp.Items
                    item1.Selected = False
                Next
            End If
            'Nilay (01-Feb-2015) -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSalaryGrade.SetDefaultValue()

            Dim StrGradesIds, StrGradeNames As String
            StrGradesIds = String.Empty : StrGradeNames = String.Empty

            For i As Integer = 0 To chkGradeGrp.Items.Count - 1
                If chkGradeGrp.Items(i).Selected Then
                    StrGradesIds &= "," & chkGradeGrp.Items(i).Value
                    StrGradeNames &= "," & chkGradeGrp.Items(i).Text
                End If
            Next

            If StrGradesIds.Trim.Length > 0 Then StrGradesIds = Mid(StrGradesIds, 2)
            If StrGradeNames.Trim.Length > 0 Then StrGradeNames = Mid(StrGradeNames, 2)

            objSalaryGrade._GradesIds = StrGradesIds
            objSalaryGrade._GradeNames = StrGradeNames
            objSalaryGrade._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objSalaryGrade._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objSalaryGrade = New clsCurrentGradeSalaryReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'S.SANDEEP [ 19 JUN 2014 ] -- START
                'ENHANCEMENT : LANGUAGE CHANGES
                Call SetLanguage()
                'S.SANDEEP [ 19 JUN 2014 ] -- END
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objSalaryGrade._CompanyUnkId = Session("CompanyUnkId")
            objSalaryGrade._UserUnkId = Session("UserId")

            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            objSalaryGrade._UserAccessFilter = Session("AccessLevelFilterString")
            'Pinkal (12-Nov-2012) -- End

            objSalaryGrade.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objSalaryGrade.generateReport(0, enPrintAction.None, enExportAction.None)
            objSalaryGrade.generateReportNew(Session("Database_Name"), _
                                             Session("UserId"), _
                                             Session("Fin_year"), _
                                             Session("CompanyUnkId"), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             Session("UserAccessModeSetting"), True, _
                                             Session("ExportReportPath"), _
                                             Session("OpenAfterExport"), _
                                             0, enPrintAction.None, enExportAction.None, _
                                             Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objSalaryGrade._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "CheckBox Event"

    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
    '    Try
    '        For i As Integer = 0 To chkGradeGrp.Items.Count - 1
    '            chkGradeGrp.Items(i).Selected = chkSelectAll.Checked
    '        Next
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles colhGradeGroup.CheckedChanged
        Try
            For i As Integer = 0 To chkGradeGrp.Items.Count - 1
                chkGradeGrp.Items(i).Selected = colhGradeGroup.Checked
            Next
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblGradeGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGradeGroup.ID, Me.lblGradeGroup.Text)
            Me.colhGradeGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.colhGradeGroup.ID, Me.colhGradeGroup.Text)
            Me.chkIncludeInactiveEmp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeInactiveEmp.ID, Me.chkIncludeInactiveEmp.Text)

            Me.BtnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END

End Class
