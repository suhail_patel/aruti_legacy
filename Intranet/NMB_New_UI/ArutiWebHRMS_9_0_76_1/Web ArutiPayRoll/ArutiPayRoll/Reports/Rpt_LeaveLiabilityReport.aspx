﻿<%@ Page Title="Leave Liability Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_LeaveLiabilityReport.aspx.vb" Inherits="Reports_Rpt_LeaveLiabilityReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Liability Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                              <i class="fas fa-filter"></i>   
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                        </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblAsonDate" runat="server" Text="As on Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAsonDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveName" runat="server" Text="Leave" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLeave" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkIncludeLeaveBF" runat="server" Text="Include Leave BF" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkOtherEarningBasicSal" runat="server" Checked="false" AutoPostBack="true"
                                                    Text="Basic Salary As Other Earning" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnl_OtherEarning" runat="server" Enabled="false">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblOtherEarning" runat="server" Text="Other Earnings" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboOtherEarning" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix text-right">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSave" runat="server" Text="Save Settings" CssClass="lnkhover"
                                            Font-Bold="true"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AnalysisBy ID="popAnalysisby" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                 <Triggers>
                 <asp:PostBackTrigger ControlID="Export" />
</Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
