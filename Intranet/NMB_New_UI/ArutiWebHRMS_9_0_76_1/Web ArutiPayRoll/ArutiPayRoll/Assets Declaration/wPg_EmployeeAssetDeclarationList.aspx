﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EmployeeAssetDeclarationList.aspx.vb"
    Inherits="HR_wPg_EmployeeAssetDeclarationList" Title="Employee Asset Declaration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">

        //function pageLoad(sender, args) {
        //    $("select").searchable();

        //    $('#imgclose').click(function(evt) {
        //        $find('<%= popupAddEdit.ClientID %>').hide();
        //    });
        //}
        //        function closePopup() {
        //            $('#imgclose').click(function(evt) {
        //                $find('<%= popupAddEdit.ClientID %>').hide();
        //            });
        //        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }

        //    function onlyNumbers($char, $mozChar,txtBox) {
        //        //var e = event || evt; // for trans-browser compatibility
        //        //var charCode = e.which || e.keyCode;
        //        var charCode
        //        if ($mozChar != null) {
        //            charCode = $mozChar;
        //        }
        //        else {
        //            charCode = $char;
        //        }
        //        
        //        //var cval = document.getElementById(txtBoxId).value;
        //        var cval = txtBox.value;

        //        if (cval.length > 0)
        //            if (charCode == 46)
        //                if (cval.indexOf(".") > -1)
        //                    return false;

        //        if (charCode > 31 && (charCode < 46 || charCode > 57))
        //            return false;

        //        return true;
        //    }    

        function GetInstruct(str) {
            var dv = document.getElementById("divInstruction");
            if (dv != null && str != null)
                dv.innerHTML = str.toString();
        }
    </script>

    <asp:Panel ID="Panel2" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Asset Declaration List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="False">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkIncludeClosedYearTrans" runat="server" Text="Include Closed Year Transactions" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnScanAttachDocuments" runat="server" Text="Browse" CssClass="btn btn-default" />
                                <asp:Button ID="BtnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                                <asp:HiddenField ID="btnHidden1" runat="Server" />
                                <asp:HiddenField ID="btnHidden2" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetdeclarationunkid">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnGvRemove" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnGvFinalSave" runat="server" CausesValidation="False" CommandName="FinalSave"
                                                                        CommandArgument="<%# Container.DataItemIndex %>"><i class="fas fa-save"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Unlock Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnGvUnlockFinalSave" runat="server" CausesValidation="False"
                                                                        CommandName="UnlockFinalSave" CommandArgument="<%# Container.DataItemIndex %>"><i class="fas fa-unlock"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Preview">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnView3" runat="server" CausesValidation="False" CommandName="Print"
                                                                        CommandArgument="<%# Container.DataItemIndex %>"><i class="fas fa-file-invoice"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="noofemployment" HeaderText="Employee Code" ReadOnly="True"
                                                                FooterText="colhNoOfEmployment">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                                FooterText="colhEmployee">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="position" HeaderText="Office / Position" ReadOnly="True"
                                                                FooterText="colhPosition">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="centerforwork" HeaderText="Department" ReadOnly="True"
                                                                FooterText="colhCenterForWork">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="yearname" HeaderText="Year" ReadOnly="True" FooterText="colhYearName">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupAddEdit" runat="server" TargetControlID="btnHidden"
                    PopupControlID="pnlAddEdit" BackgroundCssClass="modal-backdrop" DropShadow="false"
                    CancelControlID="hdf_popupAddEdit" PopupDragHandleControlID="divBottom" Enabled="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlAddEdit" runat="server" CssClass="card modal-dialog modal-lg" Style="display: none;
                    height: 590px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:MultiView ID="mvwAssetDeclaration" runat="server" ActiveViewIndex="0">
                                <asp:View ID="vwStep1" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblInstruction" runat="server" Text="Instructions" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="divInstruction" style="width: 100%; height: 144px; overflow: auto;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="header">
                                                            <asp:Label ID="gbEmployee" runat="server" Text="Employee Selection" CssClass="form-label" />
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblStep1_Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpEmpListAddEdit" runat="server" AutoPostBack="False">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" CssClass="error" />
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblDate" runat="server" Text="Transaction Date" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                    <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblStep1_NoOfEmployment" runat="server" Text="Employee Code" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="txtEmployeeCode" runat="server" Font-Bold="True" Text="1" CssClass="form-label"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblOfficePosition" runat="server" Text="Office/Position" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="txtOfficePosition" runat="server" Font-Bold="True" Text="General Manager"
                                                                        CssClass="form-label"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblCenterforWork" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="txtDepartment" runat="server" Font-Bold="True" Text="Finance and Administration"
                                                                        CssClass="form-label"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblWorkStation" runat="server" Text="Work Station" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="txtWorkStation" runat="server" Font-Bold="True" Text="Class1" CssClass="form-label"></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep2" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblFinanceToDate_Step2" runat="server" Text="Current Assets" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCash_Step2" runat="server" Text="Cash" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtCash" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    CssClass="form-control text-right"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCashExistingBank_Step2" runat="server" Text="Cash Balances in Existing Banks or Financial Institutions"
                                                            CssClass="form-label"></asp:Label></label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtCashExistingBank" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    ReadOnly="true" CssClass="form-control text-right"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhBank_step3" runat="server" Text="Bank / Financial Institution"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBank" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtBank" ErrorMessage="Bank Information can not be Blank. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="Bank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhAccount_step3" runat="server" Text="Account Number" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtAccountNumber" ErrorMessage="Account Number can not be Blank. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="Bank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhAmount_step3" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtAmountBank" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtAmountBank" ErrorMessage="Please enter amount. " CssClass="error"
                                                                Style="z-index: 1000" ValidationGroup="Bank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyBank" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCBank" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step3" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantBank" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtServantBank"
                                                            CssClass="error" ErrorMessage="Please enter proper percentage. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Bank" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step3" runat="server" Text="Wife / Husband (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbandBank" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtWifeHusbandBank"
                                                            CssClass="error" ErrorMessage="Please enter proper percentage. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Bank" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step3" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenBank" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorBank" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtChildrenBank"
                                                            CssClass="error" ErrorMessage="Please enter proper percentage. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Bank" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="BtnAddBank" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="Bank" />
                                                    <asp:Button ID="BtnUpdateBank" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="Bank" Enabled="false" />
                                                    <asp:Button ID="btnResetBank" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="Bank" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvBank" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetbanktranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName=""
                                                                                            CommandArgument="" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName=""
                                                                                            CommandArgument="" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="bank" HeaderText="Bank / Financial Institution" ReadOnly="True"
                                                                                    FooterText="colhBank_step3">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="account" HeaderText="Account" ReadOnly="True" FooterText="colhAccount_step3">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" FooterText="colhAmount_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyBank">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep3" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblShareDividend_step2" runat="server" Text="Shares and Dividends from Shares"
                                                        CssClass="form-label" />
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhShareValue_step3" runat="server" Text="Value of Shares" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtShareValue" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                    CssClass="form-control text-right"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtShareValue" ErrorMessage="Please enter amount. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="ShareDividend" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhLocation_step3" runat="server" Text="Source" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLocationShareDividend" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtLocationShareDividend" ErrorMessage="Please enter source. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="ShareDividend" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhDividendAmount_step3" runat="server" Text="Dividend Amount" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtDividendAmount" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtDividendAmount" ErrorMessage="Please enter amount. " CssClass="error"
                                                                Style="z-index: 1000" ValidationGroup="ShareDividend" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyShare" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCShareDividend" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step3Shares" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantShareDividend" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtServantShareDividend"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ShareDividend"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step3Shares" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbandShareDividend" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtWifeHusbandShareDividend"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ShareDividend"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step3Shares" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenShareDividend" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorShareDividend" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="txtChildrenShareDividend"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ShareDividend"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddShareDividend" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="ShareDividend" />
                                                    <asp:Button ID="btnUpdateShareDividend" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="ShareDividend" Enabled="false" />
                                                    <asp:Button ID="btnResetShareDividend" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="ShareDividend" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvShareDividend" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetsharedividendtranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="sharevalue" HeaderText="Share Value" ReadOnly="True" FooterText="colhShareValue_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="location" HeaderText="Source" ReadOnly="True" FooterText="colhLocation_step3">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="dividendamount" HeaderText="Dividend Amount" ReadOnly="True"
                                                                                    FooterText="colhDividendAmount_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyShare">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step3Shares">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step3Shares">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step3Shares">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep4" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblHouseBuilding_step3" runat="server" Text="Houses and Other Buildings"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhHomesBuilding_step4" runat="server" Text="Home/Buildings/ Others"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtHomeBuilding" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtHomeBuilding" ErrorMessage="Please enter detail. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="HomeBuilding" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhLocation_step4" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLocationHomeBuilding" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtLocationHomeBuilding" ErrorMessage="Please enter location. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="HomeBuilding" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step4" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtValueHomeBuilding" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtValueHomeBuilding" ErrorMessage="Please enter amount. "
                                                                CssClass="error" Style="z-index: 1000" ValidationGroup="HomeBuilding" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyHouse" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCHouseBuilding" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step4House" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantHomeBuilding" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="txtServantHomeBuilding"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="HomeBuilding"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step4House" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbundHomeBuilding" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="txtWifeHusbundHomeBuilding"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="HomeBuilding"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step4House" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenHomeBuilding" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorHouseBuilding" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="txtChildrenHomeBuilding"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="HomeBuilding"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddHomeBuilding" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="HomeBuilding" />
                                                    <asp:Button ID="btnUpdateHomeBuilding" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="HomeBuilding" Enabled="false" />
                                                    <asp:Button ID="btnResetHomeBuilding" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="HomeBuilding" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvHouseBuilding" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assethousebuildingtranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="homebuilding" HeaderText="Home/ Buildings/ Others" ReadOnly="True"
                                                                                    FooterText="colhHomesBuilding_step4">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step4">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step4">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyHouse">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step4House">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step4House">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step4House">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep5" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblParkFarm_step3" runat="server" Text="Parks, Farms and Mines" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhParkFarmMines_step4Parks" runat="server" Text="Plots, Farms and Mines"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtParkFarmsMines" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtParkFarmsMines" ErrorMessage="Please enter detail. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="ParkFarmMine" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhAreaSize_step4Parks" runat="server" Text="Size of Area" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSizeofAreaParkFarmMine" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtSizeofAreaParkFarmMine" ErrorMessage="Please enter Size of Area. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="ParkFarmMine" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhPlaceClad_step4Parks" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLocationParkFarmMine" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtLocationParkFarmMine" ErrorMessage="Please enter location. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="ParkFarmMine" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step4Parks" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtValueParkFarmMine" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtValueParkFarmMine" ErrorMessage="Please enter amount. "
                                                                CssClass="error" Style="z-index: 1000" ValidationGroup="ParkFarmMine" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyPark" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCParkFarmMine" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step4Parks" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantParkFarmMine" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="txtServantParkFarmMine"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ParkFarmMine"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step4Parks" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbundParkFarmMine" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator16" runat="server" ControlToValidate="txtWifeHusbundParkFarmMine"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ParkFarmMine"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step4Parks" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenParkFarmMine" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorParkFarmMine" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator17" runat="server" ControlToValidate="txtChildrenParkFarmMine"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ParkFarmMine"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddParkFarmMine" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="ParkFarmMine" />
                                                    <asp:Button ID="btnUpdateParkFarmMine" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="ParkFarmMine" Enabled="false" />
                                                    <asp:Button ID="btnResetParkFarmMine" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="ParkFarmMine" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvParkFarm" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetparkfarmminestranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%-- <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="parkfarmmines" HeaderText="Plots, Farms and Mines" ReadOnly="True"
                                                                                    FooterText="colhParkFarmMines_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="sizeofarea" HeaderText="Size of area" ReadOnly="True"
                                                                                    FooterText="colhAreaSize_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="placeclad" HeaderText="Location" ReadOnly="True" FooterText="colhPlaceClad_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyPark">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step4Parks">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep6" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblVehicles_step4" runat="server" Text="Vehicle and Other Transport Equipment"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhCarMotorcycle_step5Vehicle" runat="server" Text="Type and Name of Car / Motorcycle"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtVehicleType" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtVehicleType" ErrorMessage="Please enter detail. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhCarModel_step5Vehicle" runat="server" Text="Model" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtModel" ErrorMessage="Please enter model. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhCarColour_step5Vehicle" runat="server" Text="Colour" CssClass="form-label"></asp:Label></label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtColour" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtColour" ErrorMessage="Please enter colour. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhCarRegNo_step5Vehicle" runat="server" Text="Reg.No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRegNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtRegNo" ErrorMessage="Please enter Reg.No. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhCarUse_step5Vehicle" runat="server" Text="Vehicle Use" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtVehicleUse" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtVehicleUse" ErrorMessage="Please enter Vehical use. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhCarAcqDate_step5Vehicle" runat="server" Text="Acquistion Date"
                                                            CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpAcquistionDate" runat="server" AutoPostBack="False" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step5Vehicle" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtValueVehicle" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtValueVehicle" ErrorMessage="Please enter amount. " CssClass="error"
                                                                Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyVehicle" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCVehicle" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step5Vehicle" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantVehicle" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="txtServantVehicle"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step5Vehicle" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbundVehicle" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator20" runat="server" ControlToValidate="txtWifeHusbundVehicle"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step5Vehicle" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenVehicle" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorVehicle" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator21" runat="server" ControlToValidate="txtChildrenVehicle"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddVehicle" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="Vehicle" />
                                                    <asp:Button ID="btnUpdateVehicle" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="Vehicle" Enabled="false" />
                                                    <asp:Button ID="btnResetVehicle" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="Vehicle" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvVehicles" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetvehiclestranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="vehicletype" HeaderText="Type and Name of Car / Motorcycle"
                                                                                    ReadOnly="True" FooterText="colhCarMotorcycle_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="model" HeaderText="Model" ReadOnly="True" FooterText="colhCarModel_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="colour" HeaderText="Colour" ReadOnly="True" FooterText="colhCarColour_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="regno" HeaderText="Reg.No" ReadOnly="True" FooterText="colhCarRegNo_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="vehicleuse" HeaderText="Vehicle Use" ReadOnly="True" FooterText="colhCarUse_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyVehicle">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step5Vehicle">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep7" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblGrindingMachine" runat="server" Text="Machinery for Grinding Grain, Industries/Factories and Other"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix d--f ai--fe">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhMachine_step5Machinery" runat="server" Text="Grinding Machine, Industries/Factories, Other Machines"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtMachines" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtMachines" ErrorMessage="Please enter detail. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Machinery" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhLocation_step5Machinery" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLocationMachinery" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtLocationMachinery" ErrorMessage="Please enter location. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="Machinery" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step5Machinery" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtValueMachinery" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtValueMachinery" ErrorMessage="Please enter amount. " CssClass="error"
                                                                Style="z-index: 1000" ValidationGroup="Machinery" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyMachine" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCMachinery" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step5Machinery" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantMachinery" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator23" runat="server" ControlToValidate="txtServantMachinery"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Machinery"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step5Machinery" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbundMachinery" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator24" runat="server" ControlToValidate="txtWifeHusbundMachinery"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Machinery"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step5Machinery" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenMachinery" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorMachinery" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator25" runat="server" ControlToValidate="txtChildrenMachinery"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Machinery"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddMachinery" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="Machinery" />
                                                    <asp:Button ID="btnUpdateMachinery" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="Machinery" Enabled="false" />
                                                    <asp:Button ID="btnResetMachinery" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="Machinery" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvMachinery" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetmachinerytranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="machines" HeaderText="Grinding Machine, Industries/Factories, Other Machines"
                                                                                    ReadOnly="True" FooterText="colhMachine_step5Machinery">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step5Machinery">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step5Machinery">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyMachine">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step5Machinery">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step5Machinery">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step5Machinery">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep8" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblOtherBusiness_step5" runat="server" Text="Other Businesses (Shops, Bars, Lodges etc.)"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhBusinessType_step6OtherBusiness" runat="server" Text="Type of Business"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBusinessType" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtBusinessType" ErrorMessage="Please enter Type of Business. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="OtherBusiness" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhPlaceClad_step6OtherBusiness" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLocationOtherBusiness" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtLocationOtherBusiness" ErrorMessage="Please enter Location. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="OtherBusiness" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step6OtherBusiness" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtValueOtherBusiness" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtValueOtherBusiness" ErrorMessage="Please enter amount. "
                                                                CssClass="error" Style="z-index: 1000" ValidationGroup="OtherBusiness" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyBusiness" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCOtherBusiness" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhServant_step6OtherBusiness" runat="server" Text="Employee (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtServantOtherBusiness" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator27" runat="server" ControlToValidate="txtServantOtherBusiness"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="OtherBusiness"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step6OtherBusiness" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWifeHusbundOtherBusiness" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator28" runat="server" ControlToValidate="txtWifeHusbundOtherBusiness"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="OtherBusiness"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step6OtherBusiness" runat="server" Text="Children (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChildrenOtherBusiness" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="lblErrorOtherBusiness" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator29" runat="server" ControlToValidate="txtChildrenOtherBusiness"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="OtherBusiness"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddOtherBusiness" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="OtherBusiness" />
                                                    <asp:Button ID="btnUpdateOtherBusiness" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="OtherBusiness" Enabled="false" />
                                                    <asp:Button ID="btnResetOtherBusiness" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="OtherBusiness" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvOtherBusiness" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetotherbusinesstranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="businesstype" HeaderText="Type of Business" ReadOnly="True"
                                                                                    FooterText="colhBusinessType_step6OtherBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="placeclad" HeaderText="Place clad" ReadOnly="True" FooterText="colhPlaceClad_step6OtherBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step6OtherBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step6OtherBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step6OtherBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step6OtherBusiness">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep9" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblResources_step5" runat="server" Text="Resources or other Commercial Interests including livestock"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhResources_step6Resources" runat="server" Text="Resources / Commercial Interests"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtResources" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtResources" ErrorMessage="Please enter Resources / Commercial Interests. "
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="Resources" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhLocation_step6Resources" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtResourcesLocation" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtResourcesLocation" ErrorMessage="Please enter Location."
                                                            CssClass="error" Style="z-index: 1000" ValidationGroup="Resources" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step6Resources" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtResourceValue" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtResourceValue" ErrorMessage="Please enter amount. " CssClass="error"
                                                                Style="z-index: 1000" ValidationGroup="Resources" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyResources" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="lblErrorCResources" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhEmployee_step6Resources" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtResourcesEmp" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtResourcesEmp"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Resources"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step6Resources" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtResourcesHusbandwife" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtResourcesHusbandwife"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Resources"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step6Resources" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtResourceChildren" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="LblErrorResources" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="txtResourceChildren"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Resources"
                                                            SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddResource" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="Resources" />
                                                    <asp:Button ID="btnUpdateResource" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="Resources" Enabled="false" />
                                                    <asp:Button ID="btnResetResource" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="Resources" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvResources" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetotherresourcestranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="otherresources" HeaderText="Resources / Commercial Interests"
                                                                                    ReadOnly="True" FooterText="colhResources_step6Resources">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step6Resources">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step6Resources">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyResources">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhEmployee_step6Resources">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step6Resources">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step6Resources">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="vwStep10" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDebt_step6" runat="server" Text="Debt" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="height: 482px;">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhDebts_step6Debts" runat="server" Text="Debt" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDebt" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtDebt" ErrorMessage="Please enter debt. " CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Debt" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhLocation_step6Debts" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDebtLocation" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtDebtLocation" ErrorMessage="Please enter Location." CssClass="error"
                                                            Style="z-index: 1000" ValidationGroup="Debt" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhValue_step6Debts" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtDebtValue" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        CssClass="form-control text-right"></asp:TextBox></div>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtDebtValue" ErrorMessage="Please enter amount. " CssClass="error"
                                                                Style="z-index: 1000" ValidationGroup="Debt" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpCurrencyDebt" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="LblCErrorDebt" runat="server" Text="" CssClass="error" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhEmployee_step6Debts" runat="server" Text="Employee (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDebtEmployee" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="txtDebtEmployee"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Debt" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhWifeHusband_step6Debts" runat="server" Text="Wife / Husband (%)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDebtWifeHusband" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="txtDebtWifeHusband"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Debt" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="colhChildren_step6Debts" runat="server" Text="Children (%)" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDebtChildren" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="LblErrorDebt" runat="server" Text="" CssClass="error" />
                                                        <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="txtDebtChildren"
                                                            CssClass="error" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                            MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Debt" SetFocusOnError="True"></asp:RangeValidator>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnAddDebt" runat="server" Text="Add" CssClass="btn btn-primary"
                                                        ValidationGroup="Debt" />
                                                    <asp:Button ID="btnUpdateDebt" runat="server" Text="Update" CssClass="btn btn-default"
                                                        ValidationGroup="Debt" Enabled="false" />
                                                    <asp:Button ID="btnResetDebt" runat="server" Text="Reset" CssClass="btn btn-default"
                                                        ValidationGroup="Debt" CausesValidation="false" />
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body_">
                                                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="gvDebt" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetdebtstranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="debts" HeaderText="Debt" ReadOnly="True" FooterText="colhDebts_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrency_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhEmployee_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step6Debts">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="footer">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-l-0">
                            <b>
                                <asp:Label ID="lblExchangeRate" runat="server" Text="" Style="text-align: center;"
                                    CssClass="form-label"></asp:Label>
                            </b>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-default" />
                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-default" ValidationGroup="Step" />
                            <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="btn btn-primary"
                                ValidationGroup="Step" />
                            <asp:Button ID="btnCloseAddEdit" runat="server" Text="Close" CssClass="btn btn-default"
                                CausesValidation="false" />
                            <asp:HiddenField ID="hdf_popupAddEdit" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <uc8:ConfirmYesNo ID="popupYesNo" runat="server" Title="Are you sure you want to Save this Asset Declaration?"
                    Message="Are you sure you want to Save this Asset Declaration?" />
                <uc8:ConfirmYesNo ID="popupFinalYesNo" runat="server" Title="Are you sure you want to Final Save this Asset Declaration?"
                    Message="After Final Save You can not Edit / Delete Asset Declaration. Are you sure you want to Final Save this Asset Declaration?" />
                <uc9:DeleteReason ID="popup1" runat="server" Title="Are ou Sure You Want To delete?:" />
                <uc8:ConfirmYesNo ID="popupUnlockFinalYesNo" runat="server" Title="Unlock Final Save"
                    Message="Are you sure you want to Unlock Final Save this Asset Declaration?" />
                <uc9:DeleteReason ID="popupDeleteBank" runat="server" Title="Are you sure you want to delete selected Current Assets?"
                    ValidationGroup="DeleteBank" />
                <uc9:DeleteReason ID="popupDeleteShareDividend" runat="server" Title="Are you sure you want to delete selected Shares and Dividends?"
                    ValidationGroup="DeleteShareDividend" />
                <uc9:DeleteReason ID="popupDeleteHouseBuilding" runat="server" Title="Are you sure you want to delete selected House and Other Buildings?"
                    ValidationGroup="DeleteHouseBuilding" />
                <uc9:DeleteReason ID="popupDeleteParkFarm" runat="server" Title="Are you sure you want to delete selected Parks, Farms and Mines?"
                    ValidationGroup="DeleteParkFarm" />
                <uc9:DeleteReason ID="popupDeleteVehicles" runat="server" Title="Are you sure you want to delete selected Vehicle and Other Transport Equipment?"
                    ValidationGroup="DeleteVehicles" />
                <uc9:DeleteReason ID="popupDeleteMachinery" runat="server" Title="Are you sure you want to delete selected Machinery?"
                    ValidationGroup="DeleteMachinery" />
                <uc9:DeleteReason ID="popupDeleteOtherBusiness" runat="server" Title="Are you sure you want to delete selected Other Business?"
                    ValidationGroup="DeleteOtherBusiness" />
                <uc9:DeleteReason ID="popupDeleteResources" runat="server" Title="Are you sure you want to delete selected Resources?"
                    ValidationGroup="DeleteResources" />
                <uc9:DeleteReason ID="popupDeleteDebts" runat="server" Title="Are you sure you want to delete selected Debts?"
                    ValidationGroup="DeleteDebts" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
