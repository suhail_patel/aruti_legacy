﻿<%@ Page Title="Employee Declaration" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeAssetDeclarationListT2.aspx.vb"
    Inherits="HR_wPg_EmployeeAssetDeclarationListT2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">


        //        function closePopup() {
        //            $('#imgclose').click(function(evt) {
        //                $find('<%= popupAddEdit.ClientID %>').hide();
        //            });
        //        }

        //        function removeactiveclass() {
        //            $(".menubar ul li a").removeClass("active");
        //        }
        
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }




        function GetInstruct(str) {
            var dv = document.getElementById("divInstruction");
            if (dv != null && str != null)
                dv.innerHTML = str.toString();
        }
		
		 function restrictChars(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (charCode == 60 || charCode == 62 )
                return false;   

            return true;
        }
    </script>

    <asp:Panel ID="Panel2" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Asset Declaration List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFinalcialYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpFinalcialYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpEmployee" runat="server" AutoPostBack="False">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkIncludeClosedYearTrans" runat="server" Text="Include Closed Year Transactions" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnScanAttachDocuments" runat="server" Text="Browse" Visible="false"
                                    CssClass="btn btn-default" />
                                <asp:Button ID="BtnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                                <asp:HiddenField ID="btnHidden1" runat="Server" />
                                <asp:HiddenField ID="btnHidden2" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                        AllowPaging="false" DataKeyNames="assetdeclarationt2unkid, finyear_start, finyear_end,yearname, employeeunkid, Tin_No, isacknowledged, transactiondate, noofemployment, employeename, workstation, position, centerforwork, databasename">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnGvRemove" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnGvFinalSave" runat="server" CommandName="FinalSave" CommandArgument="<%# Container.DataItemIndex %>"><i class="fas fa-save"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Unlock Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnGvUnlockFinalSave" runat="server" CausesValidation="False"
                                                                        CommandName="UnlockFinalSave" CommandArgument="<%# Container.DataItemIndex %>"><i class="fas fa-unlock"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Preview">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnView3" runat="server" CausesValidation="False" CommandName="Print"
                                                                        CommandArgument="<%# Container.DataItemIndex %>"><i class="fas fa-file-invoice"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="noofemployment" HeaderText="Employee Code" ReadOnly="True"
                                                                FooterText="colhNoOfEmployment">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                                FooterText="colhEmployee">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="position" HeaderText="Office / Position" ReadOnly="True"
                                                                FooterText="colhPosition">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="centerforwork" HeaderText="Department" ReadOnly="True"
                                                                FooterText="colhCenterForWork">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="yearname" HeaderText="Year" ReadOnly="True" FooterText="colhYearName">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupAddEdit" runat="server" TargetControlID="btnHidden"
                    PopupControlID="pnlAddEdit" BackgroundCssClass="modal-backdrop" DropShadow="false"
                    CancelControlID="hdf_popupAddEdit" PopupDragHandleControlID="divBottom" Enabled="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlAddEdit" runat="server" CssClass="card modal-dialog modal-lg" Style="display: none;
                    height: calc(100vh - 80px);">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 padding-0">
                                    <asp:Panel ID="Panelmenu" runat="server" CssClass="sidebar-popup assetdeclare-popup">
                                        <div class="menu">
                                            <div id="mainview" style="display: block;">
                                                <ul class="list mainview">
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_Instruction" runat="server" CssClass="menu-toggle text-color-1"
                                                            position="0" Text="Instruction"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_BusinessDealings" runat="server" position="1" CssClass="menu-toggle"
                                                            Text="Business Dealings"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_StaffOwnership" runat="server" position="2" CssClass="menu-toggle"
                                                            Text="Staff Ownership"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_BankAccounts" runat="server" position="3" CssClass="menu-toggle"
                                                            Text="Bank Accounts"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_Realproperties" runat="server" position="4" CssClass="menu-toggle"
                                                            Text="Real properties"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_Liabilities" runat="server" position="5" CssClass="menu-toggle"
                                                            Text="Liabilities"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_Relatives" runat="server" position="6" CssClass="menu-toggle"
                                                            Text="Relatives in Employment"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_DependantBusiness" runat="server" position="7" CssClass="menu-toggle"
                                                            Text="Dependant Business"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_DependantShares" runat="server" position="8" CssClass="menu-toggle"
                                                            Text="Dependant Shares"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_DependantBank" runat="server" position="9" CssClass="menu-toggle"
                                                            Text="Dependant Bank"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_DependantProperties" runat="server" position="10" CssClass="menu-toggle"
                                                            Text="Dependant Real properties"></asp:LinkButton>
                                                        <div class="linkbootom">
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lnkmenu_SignOff" runat="server" position="11" CssClass="menu-toggle"
                                                            Text="Sign Off"></asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                    <div class="assetdeclare-popup">
                                    <asp:MultiView ID="mvwAssetDeclaration" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="vwStep1" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblInstruction" runat="server" Text="Instructions"> </asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div id="divInstruction" style="width: 100%; height: 144px; overflow: auto;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="header">
                                                                    <asp:Label ID="gbEmployee" runat="server" Text="Employee Selection" CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="body">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_FinancialYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboStep1_FinancialYear" runat="server"
                                                                                    AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboStep1_Employee" runat="server" AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <asp:Label ID="lblStep1_EmployeeError" runat="server" Text="" ForeColor="Red" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_Date" runat="server" Text="Reporting Date" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <uc2:DateCtrl ID="dtpStep1_Date" runat="server" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_EmployeeCode" runat="server" Text="Employee Code" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="txtStep1_EmployeeCode" runat="server" Font-Bold="True" Text="1" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_OfficePosition" runat="server" Text="Office/Position" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="txtStep1_OfficePosition" runat="server" Font-Bold="True" Text="General Manager"
                                                                                CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_CenterforWork" runat="server" Text="Functions" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="txtStep1_Department" runat="server" Font-Bold="True" Text="Finance and Administration"
                                                                                CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_Departments" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="txtStep1_Department2" runat="server" Font-Bold="True" Text="Finance and Administration"
                                                                                CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_WorkStation" runat="server" Text="Work Station" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="txtStep1_WorkStation" runat="server" Font-Bold="True" Text="Class1"
                                                                                CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblStep1_Tin_No" runat="server" Text="Tin No" CssClass="form-label"></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtStep1_Tin_No" runat="server" Font-Bold="True" CssClass="form-control"></asp:TextBox></div>
                                                                            </div>
                                                                            <cc1:MaskedEditExtender ID="meStep1_Tin_No" runat="server" TargetControlID="txtStep1_Tin_No"
                                                                                MaskType="Date" Mask="999/999/999" MessageValidatorTip="true" UserDateFormat="None"
                                                                                UserTimeFormat="None" InputDirection="RightToLeft" ErrorTooltipEnabled="true">
                                                                            </cc1:MaskedEditExtender>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep2" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblBusinessDealings_Step2" runat="server" Text="Business Dealings"
                                                                CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body" >
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Sector" runat="server" Text="Sector" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="cboStep2_Sector" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="RfStep2_Sector" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="cboStep2_Sector" ErrorMessage="Please Select Sector"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Company_Name" runat="server" Text="Company Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_Company_Name" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="RfStep2_Company_NameError" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_Company_Name" ErrorMessage="Company Name can not be Blank."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_company_tin_no" runat="server" Text="Company TIN No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_CompanyTinNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                                </div>
                                                                <cc1:MaskedEditExtender ID="meeStep2_CompanyTinNo" runat="server" TargetControlID="txtStep2_CompanyTinNo"
                                                                    MaskType="Date" Mask="999/999/999" MessageValidatorTip="true" UserDateFormat="None"
                                                                    UserTimeFormat="None" InputDirection="RightToLeft" ErrorTooltipEnabled="true">
                                                                </cc1:MaskedEditExtender>
                                                                <asp:RequiredFieldValidator ID="rfStep2_CompanyTinNo" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_CompanyTinNo" ErrorMessage="Company TIN No can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_registration_date" runat="server" Text="Registration Date"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpStep2_RegistrationDate" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_address_location" runat="server" Text="Address" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_Addresslocation" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep2_Addresslocation" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_Addresslocation" ErrorMessage="Address can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_business_type" runat="server" Text="Business Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_BusinessType" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rFStep2_BusinessType" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_BusinessType" CssClass="error" ErrorMessage="Bussiness Type can not be Blank"
                                                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Business" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Position_held" runat="server" Text="Position Held" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_PositionHeld" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep2_PositionHeld" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_PositionHeld" ErrorMessage="Position Held can not be Blank"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Is_supplier_client" runat="server" Text="Supplier/Borrower"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep2_Is_supplier_client" runat="server"
                                                                        AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep2_Is_supplier_client" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep2_Is_supplier_client" ErrorMessage="Position Held can not be Blank"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Monthly_annual_earnings" runat="server" Text="Monthly Annual Earnings"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep2_MonthlyAnnualEarnings" runat="server" AutoPostBack="true"
                                                                                onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep2_MonthlyAnnualEarnings" runat="server" Display="Dynamic"
                                                                        ControlToValidate="txtStep2_MonthlyAnnualEarnings" ErrorMessage="Monthly Annual Earnings can not be Blank. "
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep2_CurrencyBusiness" runat="server"
                                                                            AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep2_CurrencyBusiness" runat="server" Display="Dynamic"
                                                                        InitialValue="0" ControlToValidate="drpStep2_CurrencyBusiness" ErrorMessage="Please select Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Business_contact_no" runat="server" Text="Business Contact No"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_BusinessContactNo" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                            CssClass="form-control text-right"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep2_BusinessContactNo" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_BusinessContactNo" CssClass="error" ErrorMessage="Business Contact No can not be Blank"
                                                                    ForeColor="White" ValidationGroup="Business" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep2_Shareholders_names" runat="server" Text="Shareholders Names"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep2_ShareholdersNames" runat="server" Rows="3" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep2_ShareholdersNames" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep2_ShareholdersNames" ErrorMessage="Shareholders Names can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="BtnAddBusiness" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="Business" />
                                                            <asp:Button ID="BtnUpdateBusiness" runat="server" Text="Update" CssClass="btn btn-default"
                                                                ValidationGroup="Business" Enabled="false" />
                                                            <asp:Button ID="btnResetBusiness" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="Business" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvBusiness" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetbusinessdealt2tranunkid, assetsectorunkid, is_supplier_client, countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="assetsectorname" HeaderText="Sector" ReadOnly="True" FooterText="colhStep2_Sector">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_org_name" HeaderText="Company Name" ReadOnly="True"
                                                                                            FooterText="colhStep2_Company_Name">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_tin_no" HeaderText="Company Tin No" ReadOnly="True"
                                                                                            FooterText="colhStep2_company_tin_no">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep2_CurrencyBusiness">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="registration_date" HeaderText="Registration Date" ReadOnly="True"
                                                                                            FooterText="colhStep2_registration_date">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="address_location" HeaderText="Address" ReadOnly="True"
                                                                                            FooterText="colhStep2_address_location">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="business_type" HeaderText="Business Type" ReadOnly="True"
                                                                                            FooterText="colhStep2_business_type">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="position_held" HeaderText="Position Held" ReadOnly="True"
                                                                                            FooterText="colhStep2_Position_held">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="is_supplier_clientname" HeaderText="Supplier/Borrower"
                                                                                            ReadOnly="True" FooterText="colhStep2_Is_supplier_client">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="monthly_annual_earnings" HeaderText="Monthly Annual Earnings"
                                                                                            ReadOnly="True" FooterText="colhStep2_Monthly_annual_earnings">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="business_contact_no" HeaderText="Business Contact No"
                                                                                            ReadOnly="True" FooterText="colhStep2_Business_contact_no">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="shareholders_names" HeaderText="Shareholders Names" ReadOnly="True"
                                                                                            FooterText="colhStep2_Shareholders_names">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep3" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblStaffOwnerShip_step3" runat="server" Text="Staff Ownership of Shares/Stocks or Bonds"
                                                                CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep3_Securitiestype" runat="server" Text="Securities" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep3_Securitiestype" runat="server"
                                                                        AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep3_Securitiestype" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep3_Securitiestype" ErrorMessage="Please Select Securities Type"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep3_Certificate_no" runat="server" Text="Certificate No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep3_Certificate_no" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep3_Certificate_no" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep3_Certificate_no" ErrorMessage="Certificate No can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep3_No_of_shares_step3" runat="server" Text="No of Shares" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep3_No_of_shares" runat="Server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                            CssClass="form-control text-right"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep3_No_of_shares" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep3_No_of_shares" ErrorMessage="No of Shares can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep3_Company_business_name" runat="server" Text="Company Business Name"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep3_Company_business_name" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep3_Company_business_name" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep3_Company_business_name" ErrorMessage="Company Business Name can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep3_Marketvalue_step3" runat="server" Text="Current Market Value"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep3_Marketvalue" runat="server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                                CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep3_Marketvalue" runat="server" Display="Dynamic"
                                                                        ControlToValidate="txtStep3_Marketvalue" ErrorMessage="Current Market Value can not be Blank"
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep3_CurrencyShare" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep3_CurrencyShare" runat="server" Display="Dynamic"
                                                                        InitialValue="0" ControlToValidate="drpStep3_CurrencyShare" ErrorMessage="Please select Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep3_Acquisition_date" runat="server" Text="Acquisition Date"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpStep3_Acquisition_date" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddStaffOwnerShip" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="StaffOwnerShip" />
                                                            <asp:Button ID="btnUpdateStaffOwnerShip" runat="server" Text="Update" CssClass="btn btn-default"
                                                                ValidationGroup="StaffOwnerShip" Enabled="false" />
                                                            <asp:Button ID="btnResetStaffOwnerShip" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="StaffOwnerShip" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvStaffOwnerShip" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetsecuritiest2tranunkid,securitiestypeunkid,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="securitiestypename" HeaderText="Securities Type" ReadOnly="True"
                                                                                            FooterText="colhStep3_Securitiestype">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="certificate_no" HeaderText="Certificate No" ReadOnly="True"
                                                                                            FooterText="colhStep3_Certificate_no">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="no_of_shares" HeaderText="No of Shares" ReadOnly="True"
                                                                                            FooterText="colhStep3_No_of_shares_step3">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_business_name" HeaderText="Company Business name"
                                                                                            ReadOnly="True" FooterText="colhStep3_Company_business_name">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep3_CurrencyShare">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="market_value" HeaderText="Market Value" ReadOnly="True"
                                                                                            FooterText="colhStep3_Marketvalue_step3">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep3_Acquisition_date">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep4" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblBankAccounts_step4" runat="server" Text="Bank Account – Local and International" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep4_BankName" runat="server" Text="Bank Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep4_BankName" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep4_BankName" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep4_BankName" ErrorMessage="Bank Name can not be Blank"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep4_AccountTypeBank" runat="server" Text="Account Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group" style="display: none;">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep4_AccountTypeBankAccounts" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep4_AccountTypeBankAccounts" runat="server"
                                                                        AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep4_AccountTypeBankAccounts" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep4_AccountTypeBankAccounts" ErrorMessage="Please Select Account Type"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep4_AccountNo" runat="server" Text="Account No" CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep4_AccountNoBank" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                    </div>
                                                                    <br />
                                                                    <asp:RequiredFieldValidator ID="rfStep4_AccountNoBank" runat="server" Display="Dynamic"
                                                                        ControlToValidate="txtStep4_AccountNoBank" ErrorMessage="Account No. can not be Blank "
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep4_CurrencyBank" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep4_CurrencyBank" runat="server" Display="Dynamic"
                                                                        InitialValue="0" ControlToValidate="drpStep4_CurrencyBank" ErrorMessage="Please select Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep4_Specify" runat="server" Text="Specify" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep4_BankSpecify" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep4_DepositsSourceBank" runat="server" Text="Deposits Source"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep4_DepositsSourceBankAccounts" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep4_DepositsSourceBankAccounts" runat="server"
                                                                    Display="Dynamic" ControlToValidate="txtStep4_DepositsSourceBankAccounts" ErrorMessage="Deposits Source can not be Blank "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddBankAccounts" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="BankAccounts" />
                                                            <asp:Button ID="btnUpdateBankAccounts" runat="server" Text="Update" CssClass="btn btn-default"
                                                                ValidationGroup="BankAccounts" Enabled="false" />
                                                            <asp:Button ID="btnResetBankAccounts" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="BankAccounts" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvBankAccounts" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetbankt2tranunkid,accounttypeunkid,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="bank_name" HeaderText="Bank Name" ReadOnly="True" FooterText="colhStep4_BankName">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                        <%--<asp:BoundField DataField="account_type" HeaderText="Account Type" ReadOnly="True"
                                                                                        FooterText="colhAccountType_step4Bank">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>--%>
                                                                                        <asp:BoundField DataField="accounttype_name" HeaderText="Account Type" ReadOnly="True"
                                                                                            FooterText="colhStep4_AccountTypeBank">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <%-- Hemant (19 Nov 2018) -- End--%>
                                                                                        <asp:BoundField DataField="account_no" HeaderText="Account No" ReadOnly="True" FooterText="colhStep4_AccountNo">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep4_CurrencyBank">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="deposits_source" HeaderText="Deposits Source" ReadOnly="True"
                                                                                            FooterText="colhStep4_DepositsSourceBank">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="specify" HeaderText="Specify" ReadOnly="True" FooterText="colhStep4_Specify"
                                                                                            NullDisplayText=" ">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep5" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblRealproperties_step5" runat="server" Text="Real properties" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_AssetNameProperties" runat="server" Text="Asset Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep5_AssetName" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep5_AssetName" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep5_AssetName" ErrorMessage="Asset Name can not be Blank "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_LocationProperties" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep5_Location" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_RegistrationTitleNoProperties" runat="server" Text="Registration Title No"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep5_RegistrationTitleNoRealProperties" runat="server" CssClass="form-control"  onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_EstimatedValueProperties" runat="server" Text="Estimated Value"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep5_EstimatedValueRealProperties" runat="server" AutoPostBack="true"
                                                                                onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <br />
                                                                    <asp:RequiredFieldValidator ID="rfStep5_EstimatedValueRealProperties" runat="server"
                                                                        Display="Dynamic" ControlToValidate="txtStep5_EstimatedValueRealProperties" ErrorMessage="Estimated Value can not be Blank"
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep5_CurrencyProperties" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep5_CurrencyProperties" runat="server" Display="Dynamic"
                                                                        InitialValue="0" ControlToValidate="drpStep5_CurrencyProperties" ErrorMessage="Please select Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_FundsSourceProperties" runat="server" Text="Funds Source"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep5_FundsSourceRealProperties" runat="server" CssClass="form-control"  onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep5_FundsSourceRealProperties" runat="server"
                                                                    Display="Dynamic" ControlToValidate="txtStep5_FundsSourceRealProperties" ErrorMessage="Funds Source can not be Blank "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display: none;">
                                                                <asp:Label ID="colhStep5_AssetLocationProperties" runat="server" Text="Asset Location"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep5_AssetLocationRealProperties" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:Label ID="lblStep5_RealPropertiesError" runat="server" Text="" CssClass="error" />
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_AcquisitionDateProperties" runat="server" Text="Acquisition Date"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpStep5_AcquisitionDateProperties" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep5_AssetUseProperties" runat="server" Text="Asset Use" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep5_AssetUseRealProperties" runat="server" CssClass="form-control"  onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep5_AssetUseRealProperties" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep5_AssetUseRealProperties" ErrorMessage="Asset Use can not be Blank"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddRealProperties" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="RealProperties" />
                                                            <asp:Button ID="btnUpdateRealProperties" runat="server" Text="Update" CssClass="btn btn-default"
                                                                ValidationGroup="RealProperties" Enabled="false" />
                                                            <asp:Button ID="btnResetRealProperties" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="RealProperties" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvRealProperties" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="countryunkid,assetpropertiest2tranunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="asset_name" HeaderText="Asset Name" ReadOnly="True" FooterText="colhStep5_AssetNameProperties">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhStep5_LocationProperties"
                                                                                            NullDisplayText=" ">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="registration_title_no" HeaderText="Registration Title No"
                                                                                            ReadOnly="True" FooterText="colhStep5_RegistrationTitleNoProperties" NullDisplayText=" ">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="estimated_value" HeaderText="Estimated Value" ReadOnly="True"
                                                                                            FooterText="colhStep5_EstimatedValueProperties">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep5_CurrencyProperties">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="funds_source" HeaderText="Funds Source" ReadOnly="True"
                                                                                            FooterText="colhStep5_FundsSourceProperties" HtmlEncode="false">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="asset_location" HeaderText="Asset Location" ReadOnly="True"
                                                                                            FooterText="colhAssetLocation_step5Properties" Visible="false">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="asset_use" HeaderText="Asset Use" ReadOnly="True" FooterText="colhStep5_AssetUseProperties">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep5_AcquisitionDateProperties">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep6" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblStep6_Liabilities" runat="server" Text="Liabilities" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_InstitutionNameLiabilities" runat="server" Text="Institution Name"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep6_InstitutionName" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep6_InstitutionName" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep6_InstitutionName" ErrorMessage="Institution Name can not be Blank"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_LiabilityTypeLiabilities" runat="server" Text="Liability Type"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep6_LiabilityTypeLiabilities"
                                                                        runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep6_LiabilityTypeLiabilities" runat="server"
                                                                    Display="Dynamic" InitialValue="0" ControlToValidate="drpStep6_LiabilityTypeLiabilities"
                                                                    ErrorMessage="Please select Liability Type." CssClass="error" ForeColor="White"
                                                                    Style="z-index: 1000" ValidationGroup="Liabilities" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_OriginalBalanceLiabilities" runat="server" Text="Original Balance"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep6_OriginalBalanceLiabilities" runat="server" AutoPostBack="true"
                                                                                onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <br />
                                                                    <asp:RequiredFieldValidator ID="rfStep6_OriginalBalanceLiabilities" runat="server"
                                                                        Display="Dynamic" ControlToValidate="txtStep6_OriginalBalanceLiabilities" ErrorMessage="Original Balance can not be Blank "
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep6_OrigCurrencyLiabilities" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep6_OrigCurrencyLiabilities" runat="server" InitialValue="0"
                                                                        Display="Dynamic" ControlToValidate="drpStep6_OrigCurrencyLiabilities" ErrorMessage="Please select Original Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_OutstandingBalanceLiabilities" runat="server" Text="Outstanding Balance"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep6_OutstandingBalanceLiabilities" runat="server" AutoPostBack="true"
                                                                                onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <br />
                                                                    <asp:RequiredFieldValidator ID="rfStep6_OutstandingBalanceLiabilities" runat="server"
                                                                        Display="Dynamic" ControlToValidate="txtStep6_OutstandingBalanceLiabilities"
                                                                        ErrorMessage="Outstanding Balance can not be Blank. " CssClass="error" ForeColor="White"
                                                                        Style="z-index: 1000" ValidationGroup="Liabilities" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep6_OutCurrencyLiabilities" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep6_OutCurrencyLiabilities" runat="server" InitialValue="0"
                                                                        Display="Dynamic" ControlToValidate="drpStep6_OutCurrencyLiabilities" ErrorMessage="Please select OutStanding Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_DurationLiabilities" runat="server" Text="Duration(In Years)"
                                                                    CssClass="form-label"></asp:Label></label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep6_Duration" runat="server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                            CssClass="form-control text-right"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep6_Duration" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep6_Duration" ErrorMessage="Duration can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_LoanPurposeLiabilities" runat="server" Text="Loan Purpose"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep6_LoanPurposeLiabilities" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep6_LoanPurposeLiabilities" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep6_LoanPurposeLiabilities" ErrorMessage="Loan Purpose can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_Liability_dateLiabilities" runat="server" Text="Date of Liability"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtStep6_Liability_dateLiabilities" runat="server" AutoPostBack="false" />
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_Payoff_dateLiabilities" runat="server" Text="Payoff Date"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtStep6_Payoff_dateLiabilities" runat="server" AutoPostBack="false" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep6_Remark_step6Liabilities" runat="server" Text="Ramark" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep6_RemarkLiabilities" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep6_RemarkLiabilities" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep6_RemarkLiabilities" ErrorMessage="Ramark can not be Blank"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddLiabilities" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="Liabilities" />
                                                            <asp:Button ID="btnUpdateLiabilities" runat="server" Text="Update" CssClass="btn btn-default"
                                                                ValidationGroup="Liabilities" Enabled="false" />
                                                            <asp:Button ID="btnResetLiabilities" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="Liabilities" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvLiabilities" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetliabilitiest2tranunkid, liabilitytypeunkid, liabilitydate, payoffdate, origcountryunkid, outcountryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="institution_name" HeaderText="Institution Name" ReadOnly="True"
                                                                                            FooterText="colhStep6_InstitutionNameLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="liability_typename" HeaderText="Liability Type" ReadOnly="True"
                                                                                            FooterText="colhStep6_LiabilityTypeLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="original_balance" HeaderText="Original Balance" ReadOnly="True"
                                                                                            FooterText="colhStep6_OriginalBalanceLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="origcurrency_sign" HeaderText="Currency" ReadOnly="True"
                                                                                            FooterText="colhStep6_origCurrencyLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="outstanding_balance" HeaderText="Outstanding Balance"
                                                                                            ReadOnly="True" FooterText="colhStep6_OutstandingBalanceLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="outcurrency_sign" HeaderText="Currency" ReadOnly="True"
                                                                                            FooterText="colhStep6_outCurrencyLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="duration" HeaderText="Duration" ReadOnly="True" FooterText="colhStep6_DurationLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="loan_purpose" HeaderText="Loan Purpose" ReadOnly="True"
                                                                                            FooterText="colhStep6_LoanPurposeLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="liability_type" HeaderText="Remark" ReadOnly="True" FooterText="colhStep6_Remark_step6Liabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="liabilitydate" HeaderText="Liability Date" ReadOnly="True"
                                                                                            FooterText="colhStep6_Liability_dateLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="payoffdate" HeaderText="Payoff Date" ReadOnly="True" FooterText="colhStep6_Payoff_dateLiabilities">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep7" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblRelativesInEmployment_Step7" runat="server" Text="Relatives in employment" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep7_RelativeEmployeeRelatives" runat="server" Text="Relatives in employment"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep7_RelativeEmployee" runat="server"
                                                                        AutoPostBack="False">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep7_RelativeEmployee" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep7_RelativeEmployee" ErrorMessage="Please select Relative Employee."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Relatives"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep7_relationshipRelatives" runat="server" Text="Relationship"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep7_Relationship" runat="server"
                                                                        AutoPostBack="False">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="rfStep7_Relationship" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep7_Relationship" ErrorMessage="Please select Relationship."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Relatives"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:CheckBox ID="chkOthers" runat="server" Text="Relative not in full time employment"
                                                                            OnCheckedChanged="chkOthers_CheckedChanged" AutoPostBack="true" />
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:Panel ID="pnlRelativeEmployee" runat="server">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtRelativeEmployee" runat="server" CssClass="form-control"  onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rftxtRelativeEmployee" runat="server" Display="Dynamic"
                                                                                ControlToValidate="txtRelativeEmployee" ErrorMessage="Please Add Employee Name."
                                                                                CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="Relatives"
                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="chkOthers" EventName="CheckedChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddRelatives" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="Relatives" />
                                                            <asp:Button ID="btnUpdateRelatives" runat="server" Text="Update" CssClass="btn btn-default"
                                                                ValidationGroup="Relatives" Enabled="false" />
                                                            <asp:Button ID="btnResetRelatives" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="Relatives" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvRelatives" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetrelativest2tranunkid, relationshipunkid,relative_employeeunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="relative_employeename" HeaderText="Relative Employeename"
                                                                                            ReadOnly="True" FooterText="colhRelativeEmployee_step7Relatives">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="relationshipname" HeaderText="Relationship" ReadOnly="True"
                                                                                            FooterText="colhrelationship_step7Relatives">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep8" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblDependantBusinessTitle_Step8" runat="server" Text="Business or Commercial undertaking(s) or operation(s) owned by the Dependant (Spouse/Child under 18 years)"
                                                                CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_RelationShip" runat="server" Text="RelationShip" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep8_RelationShip" runat="server"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_RelationShip" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep8_RelationShip" ErrorMessage="Please Select RelationShip."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_DependantName" runat="server" Text="Dependant Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep8_DependantName" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_DependantName" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep8_DependantName" ErrorMessage="Please Select Dependant Name."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_Sector" runat="server" Text="Sector" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep8_Sector" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Sector" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep8_Sector" ErrorMessage="Please Select Sector"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_CompanyName" runat="server" Text="Name of Company / Organisation"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_Companyname" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Companyname" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_Companyname" ErrorMessage="Company Name can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_CompanyTinNo" runat="server" Text="Company TIN No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_Companytinno" runat="Server" CssClass="form-control"></asp:TextBox></div>
                                                                </div>
                                                                <cc1:MaskedEditExtender ID="meStep8_CompnayTinno" runat="server" TargetControlID="txtStep8_Companytinno"
                                                                    MaskType="Date" Mask="999/999/999" MessageValidatorTip="true" UserDateFormat="None"
                                                                    UserTimeFormat="None" InputDirection="RightToLeft" ErrorTooltipEnabled="true">
                                                                </cc1:MaskedEditExtender>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Companytinno" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_Companytinno" ErrorMessage="Company TIN No can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_RegDate" runat="server" Text="Date of Registration" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtStep8_DateofRegistration" runat="server" AutoPostBack="false" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_BusinessType" runat="server" Text="Type / Nature of Business"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_BussinessType" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_BussinessType" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_BussinessType" ErrorMessage="Business Type can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_position_held" runat="server" Text="Position Held" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_Position" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Position" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_Position" ErrorMessage="Position Held can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_monthlyearning" runat="server" Text="Monthly Earning" CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep8_MonthlyEarning" runat="server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                                CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep8_MonthlyEarning" runat="server" Display="Dynamic"
                                                                        ControlToValidate="txtStep8_MonthlyEarning" ErrorMessage="Monthly Earning can not be Blank. "
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep8_CurrencyMonthlyEarning" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep8_CurrencyMonthlyEarning" runat="server" Display="Dynamic"
                                                                        InitialValue="0" ControlToValidate="drpStep8_CurrencyMonthlyEarning" ErrorMessage="Please select Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_is_supplier_client" runat="server" Text="Client Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep8_SupplierClient" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_SupplierClient" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep8_SupplierClient" ErrorMessage="Please select Client Type."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_contactno" runat="server" Text="Business Contact No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_Contactno" runat="Server" onKeypress="return onlyNumbers(this, event);"
                                                                            CssClass="form-control text-right"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Contactno" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_Contactno" ErrorMessage="Business Contact No can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_shareholders_names" runat="server" Text="Shareholders Names"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_Shareholder" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Shareholder" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_Shareholder" ErrorMessage="Shareholders Names can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep8_Address" runat="server" Text="Physical Address /Location"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep8_Address" runat="Server" TextMode="MultiLine" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep8_Address" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep8_Address" ErrorMessage="Address can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddDependantBusiness_step8" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="DeptBusiness" />
                                                            <asp:Button ID="btnUpdateDependantBusiness_step8" runat="server" Text="Update" CssClass="btn btn-default"
                                                                Enabled="false" ValidationGroup="DeptBusiness" />
                                                            <asp:Button ID="btnResetDependantBusiness_step8" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                ValidationGroup="DeptBusiness" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvDependantBusiness" runat="server" AutoGenerateColumns="False"
                                                                                    CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetbusinessdealt2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,assetsectorunkid,countryunkid,is_supplier_client">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep8_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep8_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>
                                                                                        <asp:BoundField DataField="assetsectorname" HeaderText="Sector" FooterText="colhStep8_Sector">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_org_name" HeaderText="Company Name" FooterText="colhStep8_CompanyName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_tin_no" HeaderText="Company TIN No" ReadOnly="True"
                                                                                            FooterText="colhStep8_CompanyTinNo">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="registration_date" HeaderText="Registration Date" ReadOnly="True"
                                                                                            FooterText="colhStep8_RegDate">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="address_location" HeaderText="Address" ReadOnly="True"
                                                                                            FooterText="colhStep8_Address">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="business_type" HeaderText="Business Type" ReadOnly="True"
                                                                                            FooterText="colhStep8_BusinessType">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="position_held" HeaderText="Position Held" ReadOnly="True"
                                                                                            FooterText="colhStep8_position_held">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="is_supplier_clientname" HeaderText="Client Type" ReadOnly="True"
                                                                                            FooterText="colhStep8_is_supplier_client">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep8_CurrencyShare">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="monthly_annual_earnings" HeaderText="Monthly Earning"
                                                                                            ReadOnly="True" FooterText="colhStep8_monthlyearning">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="business_contact_no" HeaderText="Contact No" ReadOnly="True"
                                                                                            FooterText="colhStep8_contactno">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="shareholders_names" HeaderText="Shareholders Names" ReadOnly="True"
                                                                                            FooterText="colhStep8_shareholders_names">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep9" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblDependantShareTitle_Step9" runat="server" Text="Shares/Stocks or Bonds owned by the Dependant (Spouse/Child under 18 years)"
                                                                CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_Relationship" runat="server" Text="Relationship" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep9_DependantRelationship" runat="server"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep9_DependantRelationship" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep9_DependantRelationship" ErrorMessage="Please select Relationship."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_DependantName" runat="server" Text="Dependant Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep9_DependantName" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep9_DependantName" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep9_DependantName" ErrorMessage="Please Select Dependant Name."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_Securitytype" runat="server" Text="Security Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="DrpStep9_DependantSecurityType" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep9_DependantSecurityType" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="DrpStep9_DependantSecurityType" ErrorMessage="Please select Security Type."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_certificateno" runat="server" Text="Certificate No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep9_DependantCertificateno" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep9_DependantCertificateno" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep9_DependantCertificateno" ErrorMessage="Certificate No can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_noofshares" runat="server" Text="No Of Shares" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep9_Dependantno_of_shares" runat="server" AutoPostBack="true"
                                                                            onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep9_Dependantno_of_shares" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep9_Dependantno_of_shares" ErrorMessage="No Of Shares can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_businessname" runat="server" Text="Business Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtDependantbusinessname_step9" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfDependantbusinessname_step9" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtDependantbusinessname_step9" ErrorMessage="Business Name can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_acquisition_date" runat="server" Text="Date of Acquisition"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtStep9_Dependantacquisition_date" runat="server" AutoPostBack="false" />
                                                                
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep9_market_value" runat="server" Text="Market Value" CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep9_Dependantmarketvalue" runat="server" AutoPostBack="true"
                                                                                onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep9_Dependantmarketvalue" runat="server" Display="Dynamic"
                                                                        ControlToValidate="txtStep9_Dependantmarketvalue" ErrorMessage="Market Value can not be Blank. "
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep9_Dependantmarketvalue" AutoPostBack="true"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep9_DependantmarketvalueCurrency" runat="server"
                                                                        Display="Dynamic" ControlToValidate="drpStep9_Dependantmarketvalue" ErrorMessage="Please select Currency."
                                                                        CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                        InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddDependantShare_step9" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="DeptShare" />
                                                            <asp:Button ID="btnUpdateDependantShare_step9" runat="server" Text="Update" CssClass="btn btn-default"
                                                                Enabled="false" ValidationGroup="DeptShare" />
                                                            <asp:Button ID="btnResetDependantShare_step9" runat="server" Text="Reset" CausesValidation="false"
                                                                CssClass="btn btn-default" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvDependantShare" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetsecuritiest2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,securitiestypeunkid,acquisition_date,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep9_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep9_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="securitiestypename" HeaderText="Security Type" ReadOnly="True"
                                                                                            FooterText="colhStep9_Securitytype">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="certificate_no" HeaderText="Certificate Number" ReadOnly="True"
                                                                                            FooterText="colhStep9_certificateno">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="no_of_shares" HeaderText="No Of Shares" ReadOnly="True"
                                                                                            FooterText="colhStep9_noofshares">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_business_name" HeaderText="Business Name" ReadOnly="True"
                                                                                            FooterText="colhStep9_businessname">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep9_Currency">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="market_value" HeaderText="Market Value" ReadOnly="True"
                                                                                            FooterText="colhStep9_market_value">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep9_acquisition_date">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep10" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblDependantBankTitle_Step10" runat="server" Text="Dependant Bank"
                                                                CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_RelationShip" runat="server" Text="RelationShip" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep10_RelationShip" runat="server"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep10_RelationShip" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep10_RelationShip" ErrorMessage="Please Select RelationShip."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_DependantName" runat="server" Text="Dependant Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep10_DependantName" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep10_DependantName" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep10_DependantName" ErrorMessage="Please Select Dependant Name"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_bankname" runat="server" Text="Bank Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep10_DependantBankname" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep10_DependantBankname" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep10_DependantBankname" ErrorMessage="Bank Name can not be Blank."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_accounttype" runat="server" Text="Account Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group" style="display:none">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep10_DependantAccounttype" runat="Server" Visible="false" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep10_DependantAccounttype" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep10_DependantAccounttype" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep10_DependantAccounttype" ErrorMessage="Please Select Account Type"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_accountno" runat="server" Text="Account Number" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep10_DependantAccountno" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep10_DependantAccountno" runat="server" Display="Dynamic"
                                                                    ControlToValidate="txtStep10_DependantAccountno" CssClass="error" ErrorMessage="Bank Account Number can not be Blank."
                                                                    ForeColor="White" ValidationGroup="DeptBank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_Specify" runat="server" Text="Specify" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtcolhStep10_Specify" runat="server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep10_countryunkid" runat="server" Text="Currency Held" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep10_DependantBankCurrency" AutoPostBack="true"
                                                                        runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep10_DependantBankCurrency" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep10_DependantBankCurrency" CssClass="error"
                                                                    ErrorMessage="Please select Currency." ForeColor="White" ValidationGroup="DeptBank"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddDependantBankAccount_step10" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="DeptBank" />
                                                            <asp:Button ID="btnUpdateDependantBankAccount_step10" runat="server" Text="Update"
                                                                CssClass="btn btn-default" Enabled="false" ValidationGroup="DeptBank" />
                                                            <asp:Button ID="btnResetDependantBankAccount_step10" runat="server" Text="Reset"
                                                                CssClass="btn btn-default" CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvDependantBank" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false" DataKeyNames="assetbankt2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,accounttypeunkid,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep10_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep10_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>
                                                                                        <asp:BoundField DataField="bank_name" HeaderText="Name of Bank" FooterText="colhStep10_bankname">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                        <%--<asp:BoundField DataField="account_type" HeaderText="Account Type" ReadOnly="True"
                                                                                            FooterText="colhaccounttype_step10">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>--%>
                                                                                        <asp:BoundField DataField="accounttype_name" HeaderText="Account Type" ReadOnly="True"
                                                                                            FooterText="colhStep10_accounttype">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (19 Nov 2018) -- End--%>
                                                                                        <asp:BoundField DataField="account_no" HeaderText="Account Number" ReadOnly="True"
                                                                                            FooterText="colhStep10_accountno">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep10_countryunkid">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="amount" Visible="false" HeaderText="amount" ReadOnly="True"
                                                                                            FooterText="colhStep10_amount">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="specify" HeaderText="Specify" ReadOnly="True" FooterText="colhStep10_Specify"
                                                                                            NullDisplayText=" ">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep11" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblDependantPropertiesTitle_Step11" runat="server" Text="Dependant Real properties (such as Residential House, Commercial Property, lands, vehicles, etc.)"
                                                                CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_RelationShip" runat="server" Text="RelationShip" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep11_RelationShip" runat="server"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep11_RelationShip" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep11_RelationShip" ErrorMessage="Please Select RelationShip."
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptProperties"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_DependantName" runat="server" Text="Dependant Name" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="drpStep11_DependantName" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep11_DependantName" runat="server" Display="Dynamic"
                                                                    InitialValue="0" ControlToValidate="drpStep11_DependantName" ErrorMessage="Please Select Dependant Name"
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptProperties"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_asset_name" runat="server" Text="Name of Asset" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStep11_DependantPropertiesAsset" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfStep11_DependantPropertiesAsset" runat="server"
                                                                    Display="Dynamic" ControlToValidate="txtStep11_DependantPropertiesAsset" ErrorMessage="Asset can not be Blank. "
                                                                    CssClass="error" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptProperties"
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_location" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtDependantPropertiesLocation_step11" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_registrationtitleno" runat="server" Text="Registration/Title Number"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtDependantPropertiesRegistrationNo_step11" runat="Server" CssClass="form-control" onKeypress="return restrictChars(this, event);"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_estimated_value" runat="server" Text="Estimated Value"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 p-l-0 p-r-5">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtStep11_DependantPropertiesEstimatevalue" runat="server" AutoPostBack="true"
                                                                                onKeypress="return onlyNumbers(this, event);" CssClass="form-control text-right"></asp:TextBox></div>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep11_DependantPropertiesEstimatevalue" runat="server"
                                                                        Display="Dynamic" ControlToValidate="txtStep11_DependantPropertiesEstimatevalue"
                                                                        ErrorMessage="Estimated Value can not be Blank. " CssClass="error" ForeColor="White"
                                                                        Style="z-index: 1000" ValidationGroup="DeptProperties" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="drpStep11_DependantPropertiesEstimatevalue"
                                                                            AutoPostBack="true" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfStep11_DependantPropertiesEstimatevalueCurrency"
                                                                        runat="server" InitialValue="0" Display="Dynamic" ControlToValidate="drpStep11_DependantPropertiesEstimatevalue"
                                                                        ErrorMessage="Please select Currency." CssClass="error" ForeColor="White" Style="z-index: 1000"
                                                                        ValidationGroup="DeptProperties" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="colhStep11_AcquisitionDateDepnProperties" runat="server" Text="Acquisition Date"
                                                                    CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpStep11_AcquisitionDateDepnProperties" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddDependantProperties_step11" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                ValidationGroup="DeptProperties" />
                                                            <asp:Button ID="btnUpdateDependantProperties_step11" runat="server" Text="Update"
                                                                CssClass="btn btn-default" Enabled="false" ValidationGroup="DeptProperties" />
                                                            <asp:Button ID="btnResetDependantProperties_step11" runat="server" Text="Reset" CssClass="btn btn-default"
                                                                CausesValidation="false" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="body_">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive">
                                                                                <asp:GridView ID="gvDependantProperties" runat="server" AutoGenerateColumns="False"
                                                                                    CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assetpropertiest2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,countryunkid,acquisition_date">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep11_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep11_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>
                                                                                        <asp:BoundField DataField="asset_name" HeaderText="Asset Name" FooterText="colhStep11_asset_name">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhStep11_location"
                                                                                            NullDisplayText=" ">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="registration_title_no" HeaderText="Registration No" ReadOnly="True"
                                                                                            FooterText="colhStep11_registrationtitleno" NullDisplayText=" ">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep11_Currency">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="estimated_value" HeaderText="Monthly Earning" ReadOnly="True"
                                                                                            FooterText="colhStep11_estimated_value">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%-- Hemant (23 Nov 2018) -- Start
                                                                                         Enhancement : Changes As per Rutta Request for UAT --%>
                                                                                        <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep11_AcquisitionDateDepnProperties">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--'Hemant (23 Nov 2018) -- End--%>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="vwStep12" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblSighOffTitle_Step12" runat="server" Text="Sign Off" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body assetdeclare-body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                                                <asp:Label ID="txtHeader_Step12" runat="server" Font-Bold="true" Font-Underline="true"
                                                                    Font-Size="Medium" Text="Declaration" CssClass="form-label"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="txtPara1_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="txtPara2_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="txtPara3_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="txtPara4_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="txtSignatureLine_Step12" runat="server" ></asp:Label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="txtDate_Step12" runat="server" ></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkAcknowledgement_Step13" runat="server" Text="Acknowledgement" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:View>
                                    </asp:MultiView>
                                    
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="footer">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-l-0">
                            <b>
                                <asp:Label ID="lblExchangeRate" runat="server" Text="" CssClass="form-label text-center"></asp:Label>
                            </b>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 p-l-0">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-default" />
                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-default" ValidationGroup="Step" />
                            <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="btn btn-primary"
                                ValidationGroup="Step" />
                            <asp:Button ID="btnFinalSave" runat="server" Text="Final Save" CssClass="btn btn-default"
                                ValidationGroup="Step" />
                            <asp:Button ID="btnCloseAddEdit" runat="server" Text="Close" CssClass="btn btn-default"
                                CausesValidation="false" />
                            <asp:HiddenField ID="hdf_popupAddEdit" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <uc8:ConfirmYesNo ID="popupYesNo" runat="server" Title="Are you sure you want to Save this Asset Declaration?"
                    Message="Are you sure you want to Save this Asset Declaration?" />
                <uc8:ConfirmYesNo ID="popupFinalYesNo" runat="server" Title="Are you sure you want to Final Save this Asset Declaration?"
                    Message="After Final Save You can not Edit / Delete Asset Declaration. Are you sure you want to Final Save this Asset Declaration?" />
                <uc9:DeleteReason ID="popup1" runat="server" Title="Are ou Sure You Want To delete?:" />
                <uc8:ConfirmYesNo ID="popupUnlockFinalYesNo" runat="server" Title="Unlock Final Save"
                    Message="Are you sure you want to Unlock Final Save this Asset Declaration?" />
                <uc9:DeleteReason ID="popupDeleteBusinessDeal" runat="server" Title="Are you sure you want to delete selected business deal?"
                    ValidationGroup="DeleteBusinessDeal" />
                <uc9:DeleteReason ID="popupDeleteStaffOwnership" runat="server" Title="Are you sure you want to delete selected staff Ownership?"
                    ValidationGroup="DeleteStaffownership" />
                <uc9:DeleteReason ID="popupDeleteBankAccount" runat="server" Title="Are you sure you want to delete selected bank account?"
                    ValidationGroup="DeleteBankAccount" />
                <uc9:DeleteReason ID="popupDeleteProperties" runat="server" Title="Are you sure you want to delete selected properties?"
                    ValidationGroup="DeleteProperties" />
                <uc9:DeleteReason ID="popupDeleteLiabilities" runat="server" Title="Are you sure you want to delete selected liabilities?"
                    ValidationGroup="DeleteLiabilities" />
                <uc9:DeleteReason ID="popupDeleteRelatives" runat="server" Title="Are you sure you want to delete selected relatives?"
                    ValidationGroup="DeleteRelatives" />
                <uc9:DeleteReason ID="popupDeleteDependantBusinessDeal" runat="server" Title="Are you sure you want to delete selected Dependant Business Deal?"
                    ValidationGroup="DeleteDependantBusinessDeal" />
                <uc9:DeleteReason ID="popupDeleteDependantShare" runat="server" Title="Are you sure you want to delete selected dependant share?"
                    ValidationGroup="DeleteDependantShare" />
                <uc9:DeleteReason ID="popupDeleteDependantBankAccount" runat="server" Title="Are you sure you want to delete selected Dependant Bank Account?"
                    ValidationGroup="DeleteDependantBankAccount" />
                <uc9:DeleteReason ID="popupDeleteDependantProperties" runat="server" Title="Are you sure you want to delete selected dependant properties?"
                    ValidationGroup="DeleteDependantProperties" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
