IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME='employeeunkid')
BEGIN
	ALTER TABLE rcapplicant_master ADD employeeunkid INT NULL
	EXECUTE('UPDATE rcapplicant_master SET employeeunkid = hremployee_master.employeeunkid 
			FROM hremployee_master 
			WHERE hremployee_master.email = rcapplicant_master.email
			AND hremployee_master.email <> ''''')
	EXECUTE('UPDATE rcapplicant_master SET employeeunkid = 0 WHERE employeeunkid IS NULL')
	EXEC sp_addextendedproperty
		@name = N'MS_Description' ,
		@value = N'Unkid from hremployee_master table.' ,
		@level0type = N'Schema', 
		@level0name = dbo ,
		@level1type = N'Table',
		@level1name = 'rcapplicant_master' ,
		@level2type = N'Column',
		@level2name = 'employeeunkid';
END