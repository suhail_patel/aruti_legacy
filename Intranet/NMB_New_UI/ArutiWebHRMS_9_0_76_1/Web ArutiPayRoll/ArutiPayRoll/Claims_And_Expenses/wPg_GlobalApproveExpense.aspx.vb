﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
#End Region

Partial Class Claims_And_Expenses_wPg_GlobalApproveExpense
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmGlobalApproveExpense"
    Private mstrEmployeeIDs As String = ""
    Dim mblnIsExternalApprover As Boolean = False
    'Gajanan [20-July-2020] -- Start
    'Enhancement: Optimize For NMB
    'Dim dtList As DataTable = Nothing
    'Gajanan [20-July-2020] -- End
    Dim mintPriority As Integer = -1

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsExpenseSaveClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Pinkal (25-Jan-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    Dim objClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (25-Jan-2022) -- End


#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If



            If IsPostBack = False Then

                SetLanguage()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                Call FillCombo()
            Else
                mstrEmployeeIDs = Me.ViewState("mstrEmployeeIDs").ToString()
                mblnIsExternalApprover = CBool(Me.ViewState("ExternalApprover"))
                'Gajanan [20-July-2020] -- Start
                'Enhancement: Optimize For NMB
                'dtList = CType(Session("GlobalApproval"), DataTable)
                'Gajanan [20-July-2020] -- End

                mintPriority = CInt(Me.ViewState("Priority"))
                mblnIsExpenseSaveClick = CBool(Me.ViewState("mblnIsExpenseSaveClick"))
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrEmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("ExternalApprover") = mblnIsExternalApprover
            Me.ViewState("Priority") = mintPriority
            'Gajanan [20-July-2020] -- Start
            'Enhancement: Optimize For NMB
            'Session("GlobalApproval") = dtList
            'Gajanan [20-July-2020] -- End

            Me.ViewState("mblnIsExpenseSaveClick") = mblnIsExpenseSaveClick
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            Dim objMasterData As New clsMasterData

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End


            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (1,3)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()
            End With
            objMasterData = Nothing


            Dim objcommonMst As New clsCommon_Master
            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSectorRoute
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objcommonMst = Nothing

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            cboExpenseCategory_SelectedIndexChanged(New Object, New EventArgs())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dsCombo.Dispose()
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillGrid()
        Dim intMode As Integer = 0
        Dim mstrSearch As String = ""
        Try

            Dim objApproval As New clsclaim_request_approval_tran

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_request_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_request_master.crmasterunkid IN (" & objApproval.GetClaimFormFromExpSector(CInt(cboExpense.SelectedValue), 0) & ")"
            End If

            If CInt(cboSectorRoute.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_approval_tran.secrouteunkid = " & CInt(cboSectorRoute.SelectedValue) & " "
            End If

            If ViewState("EmpAdvanceSearch") IsNot Nothing Then
                mstrSearch &= "AND " & ViewState("EmpAdvanceSearch").ToString()
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If

            Dim dtList As DataTable = objApproval.GetGlobalApprovalData(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                    , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                                    , Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(Session("LeaveBalanceSetting")) _
                                                                                                    , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboExpenseCategory.SelectedValue) _
                                                                                                    , CInt(cboApprover.SelectedValue), mstrSearch, True, mblnIsExternalApprover)




            dgvData.DataSource = dtList
            dgvData.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillGrid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private Sub GlobalSave()
        Try
            Dim objLeaveApprover As New clsleaveapprover_master
            Dim objExpApproverTran As New clsclaim_request_approval_tran
            Dim objExpAppr As New clsExpenseApprover_Master
            Dim objClaimMst As New clsclaim_request_master
            Dim mintMaxPriority As Integer = -1
            Dim mstrRejectRemark As String = ""
            Dim blnLastApprover As Boolean = False
            Dim mblnIssued As Boolean = False
            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING

            'Gajanan [20-July-2020] -- Start
            'Enhancement: Optimize For NMB


            '            Dim dtTemp() As DataRow = Nothing
            '            dtTemp = dtList.Select("IsChecked = 1 And IsGrp = True")



            '            Dim mblnWarning As Boolean = False

            '            For i As Integer = 0 To dtTemp.Length - 1

            '                objClaimMst._Crmasterunkid = CInt(dtTemp(i)("crmasterunkid"))

            '                Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
            '                                                                                                     , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpenseCategory.SelectedValue) _
            '                                                                                                     , False, True, -1, "", CInt(dtTemp(i)("crmasterunkid")))

            '                Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintPriority, "crpriority asc", DataViewRowState.CurrentRows).ToTable
            '                mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            '                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
            '                    objLeaveApprover = New clsleaveapprover_master
            '                    objLeaveApprover._Approverunkid = CInt(dtTemp(i)("crmasterunkid"))
            '                    Dim objLeaveForm As New clsleaveform
            '                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
            '                    If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = mintPriority) Then mblnIssued = True
            '                End If

            '                Dim mblnIsRejected As Boolean = False
            '                Dim mintApproverApplicationStatusId As Integer = 2

            '                For j As Integer = 0 To dtApproverTable.Rows.Count - 1
            '                    blnLastApprover = False
            '                    Dim mintApproverEmpID As Integer = -1
            '                    Dim mintApproverID As Integer = -1
            '                    Dim mdtApprovalDate As Date = Nothing


            '                    If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
            '                        objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))


            '                        If CInt(cboApprover.SelectedValue) = objLeaveApprover._Approverunkid Then
            '                            mintStatusID = CInt(cboStatus.SelectedValue)
            '                            mintVisibleID = CInt(cboStatus.SelectedValue)
            '                            mintApproverApplicationStatusId = mintStatusID
            '                            mdtApprovalDate = DateTime.Now
            '                            If CInt(cboStatus.SelectedValue) = 1 Then
            '                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
            '                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
            '                                mstrRejectRemark = txtRemarks.Text.Trim
            '                                blnLastApprover = True
            '                            End If

            '                        ElseIf CInt(cboApprover.SelectedValue) <> objLeaveApprover._Approverunkid Then

            '                            'Pinkal (22-Oct-2018) -- Start
            '                            'Enhancement - Implementing Claim & Request changes For NMB .
            '                            'Dim mintPriority As Integer = -1
            '                            Dim mintCurrentPriority As Integer = -1
            '                            'Pinkal (22-Oct-2018) -- End

            '                            Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

            '                            If dRow.Length > 0 Then
            '                                mintStatusID = 2
            '                                mintVisibleID = 1
            '                                mintApproverApplicationStatusId = mintStatusID

            '                            Else

            '                                'Pinkal (22-Oct-2018) -- Start
            '                                'Enhancement - Implementing Claim & Request changes For NMB .

            '                                'mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

            '                                'If mintPriority <= -1 Then
            '                                '    mintStatusID = 2
            '                                '    mintVisibleID = 1
            '                                '    mintApproverApplicationStatusId = mintStatusID
            '                                '    GoTo AssignApprover
            '                                'ElseIf mblnIsRejected Then
            '                                '    mintVisibleID = -1
            '                                '    mintStatusID = -1
            '                                'ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                '    mintVisibleID = 1
            '                                '    mintStatusID = 1
            '                                'ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                '    mintVisibleID = 2
            '                                '    mintStatusID = 2
            '                                'Else
            '                                '    mintVisibleID = -1
            '                                '    mintStatusID = -1
            '                                'End If

            '                                mintCurrentPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

            '                                If mintCurrentPriority <= -1 Then
            '                                    mintStatusID = 2
            '                                    mintVisibleID = 1
            '                                    mintApproverApplicationStatusId = mintStatusID
            '                                    GoTo AssignApprover
            '                                ElseIf mblnIsRejected Then
            '                                    mintVisibleID = -1
            '                                    mintStatusID = -1
            '                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 1
            '                                    mintStatusID = 1
            '                                ElseIf mintCurrentPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 2
            '                                    mintStatusID = 2
            '                                Else
            '                                    mintVisibleID = -1
            '                                    mintStatusID = -1
            '                                End If

            '                                'Pinkal (22-Oct-2018) -- End

            '                                mintApproverApplicationStatusId = mintStatusID
            '                            End If
            '                            mdtApprovalDate = Nothing
            '                        End If

            'AssignApprover:
            '                        mintApproverEmpID = objLeaveApprover._leaveapproverunkid
            '                        mintApproverID = objLeaveApprover._Approverunkid

            '                    Else

            '                        objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))

            '                        If CInt(cboApprover.SelectedValue) = objExpAppr._crApproverunkid Then

            '                            mintStatusID = CInt(cboStatus.SelectedValue)
            '                            mintVisibleID = CInt(cboStatus.SelectedValue)
            '                            mintApproverApplicationStatusId = mintStatusID
            '                            mdtApprovalDate = DateTime.Now
            '                            If CInt(cboStatus.SelectedValue) = 1 Then
            '                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then blnLastApprover = True
            '                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
            '                                mstrRejectRemark = txtRemarks.Text.Trim
            '                                blnLastApprover = True
            '                                mblnIsRejected = True
            '                            End If

            '                        ElseIf CInt(cboApprover.SelectedValue) <> objExpAppr._crApproverunkid Then

            '                            'Pinkal (22-Oct-2018) -- Start
            '                            'Enhancement - Implementing Claim & Request changes For NMB .
            '                            ' Dim mintPriority As Integer = -1
            '                            Dim mintCurrentPriority As Integer = -1
            '                            'Pinkal (22-Oct-2018) -- End

            '                            Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

            '                            If dRow.Length > 0 Then
            '                                mintStatusID = 2
            '                                mintVisibleID = 1
            '                                mintApproverApplicationStatusId = mintStatusID
            '                            Else

            '                                'Pinkal (22-Oct-2018) -- Start
            '                                'Enhancement - Implementing Claim & Request changes For NMB .

            '                                'mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

            '                                'If mintPriority <= -1 Then GoTo AssignApprover1

            '                                'If mblnIsRejected Then
            '                                '    mintStatusID = -1
            '                                '    mintVisibleID = -1
            '                                'ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                '    mintVisibleID = 1
            '                                '    mintStatusID = 1
            '                                'ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                '    mintVisibleID = 2
            '                                '    mintStatusID = 2
            '                                'Else
            '                                '    mintVisibleID = -1
            '                                '    mintStatusID = -1
            '                                'End If

            '                                mintCurrentPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

            '                                If mintCurrentPriority <= -1 Then GoTo AssignApprover1

            '                                If mblnIsRejected Then
            '                                    mintStatusID = -1
            '                                    mintVisibleID = -1
            '                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 1
            '                                    mintStatusID = 1
            '                                ElseIf mintCurrentPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 2
            '                                    mintStatusID = 2
            '                                Else
            '                                    mintVisibleID = -1
            '                                    mintStatusID = -1
            '                                End If

            '                                'Pinkal (22-Oct-2018) -- End
            '                                mintApproverApplicationStatusId = mintStatusID
            '                                mdtApprovalDate = Nothing
            '                            End If


            '                        End If

            'AssignApprover1:
            '                        mintApproverEmpID = objExpAppr._Employeeunkid
            '                        mintApproverID = objExpAppr._crApproverunkid

            '                    End If

            '                    Dim dtFilter As DataTable = New DataView(dtList, "IsChecked = 1 AND crmasterunkid = " & CInt(dtTemp(i)("crmasterunkid")), "", DataViewRowState.CurrentRows).ToTable

            '                    Dim intExpenseID As Integer = CInt(dtTemp(i)("expenseunkid"))

            '                    Dim drExpense As List(Of DataRow) = (From row In dtList.AsEnumerable() Where CBool(row.Item("IsGrp")) = False AndAlso CBool(row.Item("IsChecked")) = True _
            '                                            AndAlso CInt(row.Item("crmasterunkid")) = objClaimMst._Crmasterunkid AndAlso CInt(row.Item("expenseunkid")) > 0 Select (row)).ToList


            '                    Dim sMsg As String = String.Empty

            '                    'Pinkal (26-Feb-2019) -- Start
            '                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '                    'sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")))


            '                    'Pinkal (22-Jan-2020) -- Start
            '                    'Enhancements -  Working on OT Requisistion Reports for NMB.

            '                    'sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")), CInt(Session("Fin_year")) _
            '                    '                                                                          , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CDate(drExpense(0)("ClaimDate")).Date _
            '                    '                                                                          , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting))

            '                    sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")), CInt(Session("Fin_year")) _
            '                                                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CDate(drExpense(0)("ClaimDate")).Date _
            '                                                                                              , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), objClaimMst._Crmasterunkid, False)


            '                    'Pinkal (22-Jan-2020) -- End

            '                    'Pinkal (26-Feb-2019) -- End


            '                    If sMsg <> "" Then
            '                        DisplayMessage.DisplayMessage(sMsg, Me)
            '                        mblnWarning = True
            '                        Continue For
            '                    End If

            '                    Dim drRow() As DataRow = dtFilter.Select("IsGrp = False AND AUD = ''")
            '                    If drRow.Length > 0 Then
            '                        For Each dr As DataRow In drRow
            '                            dr("AUD") = "U"
            '                            dr.AcceptChanges()
            '                        Next
            '                        dtFilter.AcceptChanges()
            '                    End If

            '                    objExpApproverTran._DataTable = dtFilter

            '                    'Pinkal (04-Feb-2019) -- Start
            '                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '                    'objExpApproverTran._YearId = FinancialYear._Object._YearUnkid
            '                    'objExpApproverTran._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            '                    objExpApproverTran._YearId = CInt(Session("Fin_year"))
            '                    objExpApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            '                    objExpApproverTran._WebFormName = mstrModuleName
            '                    objExpApproverTran._WebClientIP = CStr(Session("IP_ADD"))
            '                    objExpApproverTran._WebHostName = CStr(Session("HOST_NAME"))
            '                    'Pinkal (04-Feb-2019) -- End

            '                    objExpApproverTran._EmployeeID = CInt(dtTemp(i)("employeeunkid"))



            '                    If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, CInt(Session("UserId")) _
            '                                                                                            , ConfigParameter._Object._CurrentDateAndTime, CInt(dtTemp(i)("crmasterunkid")), Nothing, mstrRejectRemark _
            '                                                                                            , blnLastApprover, mdtApprovalDate) = False Then
            '                        Exit For
            '                    End If

            '                    If mintApproverApplicationStatusId = 1 Then
            '                        objExpApproverTran.SendMailToApprover(CInt(cboExpenseCategory.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(dtTemp(i)("crmasterunkid")) _
            '                                                                                    , dtTemp(i)("CLno").ToString(), CInt(dtTemp(i)("employeeunkid")), mintPriority, 1, "crpriority > " & mintPriority, Session("Database_Name").ToString() _
            '                                                                                    , Session("EmployeeAsOnDate").ToString(), CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString() _
            '                                                                                    , enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), "")
            '                    End If

            '                    objClaimMst._Crmasterunkid = CInt(dtTemp(i)("crmasterunkid"))

            '                    If objClaimMst._Statusunkid = 1 AndAlso mintApproverApplicationStatusId = 1 Then
            '                        Dim objEmployee As New clsEmployee_Master
            '                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
            '                        objClaimMst._EmployeeCode = objEmployee._Employeecode
            '                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
            '                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
            '                        objClaimMst._EmployeeSurName = objEmployee._Surname
            '                        objClaimMst._EmpMail = objEmployee._Email

            '                        If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
            '                            Dim objLeaveForm As New clsleaveform
            '                            objLeaveForm._Formunkid = objClaimMst._Referenceunkid
            '                            objLeaveForm._EmployeeCode = objEmployee._Employeecode
            '                            objLeaveForm._EmployeeFirstName = objEmployee._Firstname
            '                            objLeaveForm._EmployeeMiddleName = objEmployee._Othername
            '                            objLeaveForm._EmployeeSurName = objEmployee._Surname
            '                            objLeaveForm._EmpMail = objEmployee._Email

            '                            If objLeaveForm._Statusunkid = 7 Then 'only Issued
            '                                Dim objCommonCode As New CommonCodes
            '                                Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
            '                                Dim strPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid _
            '                                                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date _
            '                                                                                                                            , CDate(Session("fin_enddate")).Date, True, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
            '                                objCommonCode = Nothing

            '                                Dim objLeaveType As New clsleavetype_master
            '                                objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid

            '                                'Pinkal (01-Apr-2019) -- Start
            '                                'Enhancement - Working on Leave Changes for NMB.
            '                                'objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate, objLeaveForm._Returndate _
            '                                '                                                    , objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "", CInt(dtTemp(i)("crmasterunkid")))
            '                                objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate _
            '                                                                                   , objLeaveForm._Returndate, objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "" _
            '                                                                                   , Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "" _
            '                                                                                   , CInt(dtTemp(i)("crmasterunkid")), objLeaveForm._Formno)
            '                                'Pinkal (01-Apr-2019) -- End


            '                                objLeaveType = Nothing
            '                                objLeaveForm = Nothing

            '                            End If
            '                        Else
            '                            objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "")
            '                        End If
            '                        objEmployee = Nothing

            '                    ElseIf objClaimMst._Statusunkid = 3 AndAlso mintApproverApplicationStatusId = 3 Then
            '                        Dim objEmployee As New clsEmployee_Master
            '                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = objClaimMst._Employeeunkid
            '                        objClaimMst._EmployeeCode = objEmployee._Employeecode
            '                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
            '                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
            '                        objClaimMst._EmployeeSurName = objEmployee._Surname
            '                        objClaimMst._EmpMail = objEmployee._Email

            '                        objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", _
            '                                                                           enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRemarks.Text.Trim)

            '                        objEmployee = Nothing
            '                    End If

            '                Next

            '            Next

            '            If mblnWarning = True Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Some of the Claim Expense Form(s) cannot be Approved. Reason : It is exceeded the current occurrence limit and will be highlighted in red."), Me)
            '                Exit Sub
            '            Else
            '                Call FillGrid()
            '            End If


            Dim dtDate As Date = Now



            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text) = False)


            Dim mblnWarning As Boolean = False

            For Each dgrow As DataGridItem In gRow

                objClaimMst._Crmasterunkid = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text)

                Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
                                                                                                     , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpenseCategory.SelectedValue) _
                                                                                                     , False, True, -1, "", CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text))

                Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintPriority, "crpriority asc", DataViewRowState.CurrentRows).ToTable
                mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                    objLeaveApprover = New clsleaveapprover_master
                    objLeaveApprover._Approverunkid = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text)
                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                    If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = mintPriority) Then mblnIssued = True
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objLeaveForm = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If



                Dim objApproval As New clsclaim_request_approval_tran
                Dim mstrSearch As String = ""

                mstrSearch = "AND cmclaim_request_master.crmasterunkid = " & CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text)

                If mstrSearch.Trim.Length > 0 Then
                    mstrSearch = mstrSearch.Trim.Substring(3)
                End If

                Dim dtFilter As DataTable = objApproval.GetGlobalApprovalData(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                                        , Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(Session("LeaveBalanceSetting")) _
                                                                                                        , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboExpenseCategory.SelectedValue) _
                                                                                                        , CInt(cboApprover.SelectedValue), mstrSearch, True, mblnIsExternalApprover)
                objApproval = Nothing

                If dtFilter IsNot Nothing AndAlso dtFilter.Rows.Count <= 0 Then Continue For

                Dim mblnIsRejected As Boolean = False
                Dim mintApproverApplicationStatusId As Integer = 2

                For j As Integer = 0 To dtApproverTable.Rows.Count - 1
                    blnLastApprover = False
                    Dim mintApproverEmpID As Integer = -1
                    Dim mintApproverID As Integer = -1
                    Dim mdtApprovalDate As Date = Nothing


                    If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                        objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))

                        If CInt(cboApprover.SelectedValue) = objLeaveApprover._Approverunkid Then
                            mintStatusID = CInt(cboStatus.SelectedValue)
                            mintVisibleID = CInt(cboStatus.SelectedValue)
                            mintApproverApplicationStatusId = mintStatusID
                            mdtApprovalDate = DateTime.Now
                            If CInt(cboStatus.SelectedValue) = 1 Then
                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                            End If

                        ElseIf CInt(cboApprover.SelectedValue) <> objLeaveApprover._Approverunkid Then

                            Dim mintCurrentPriority As Integer = -1

                            Dim dRow As DataRow() = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

                            If dRow.Length > 0 Then
                                mintStatusID = 2
                                mintVisibleID = 1
                                mintApproverApplicationStatusId = mintStatusID
                            Else
                                mintCurrentPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))
                                If mintCurrentPriority <= -1 Then
                                    mintStatusID = 2
                                    mintVisibleID = 1
                                    mintApproverApplicationStatusId = mintStatusID
                                    GoTo AssignApprover
                                ElseIf mblnIsRejected Then
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 1
                                    mintStatusID = 1
                                ElseIf mintCurrentPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 2
                                    mintStatusID = 2
                                Else
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                End If


                                mintApproverApplicationStatusId = mintStatusID
                            End If
                            mdtApprovalDate = Nothing
                        End If

AssignApprover:
                        mintApproverEmpID = objLeaveApprover._leaveapproverunkid
                        mintApproverID = objLeaveApprover._Approverunkid

                    Else

                        objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))
                        If CInt(cboApprover.SelectedValue) = objExpAppr._crApproverunkid Then

                            mintStatusID = CInt(cboStatus.SelectedValue)
                            mintVisibleID = CInt(cboStatus.SelectedValue)
                            mintApproverApplicationStatusId = mintStatusID
                            mdtApprovalDate = DateTime.Now
                            If CInt(cboStatus.SelectedValue) = 1 Then
                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then blnLastApprover = True
                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                                mblnIsRejected = True
                            End If

                        ElseIf CInt(cboApprover.SelectedValue) <> objExpAppr._crApproverunkid Then
                            Dim mintCurrentPriority As Integer = -1

                            Dim dRow As DataRow() = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

                            If dRow.Length > 0 Then
                                mintStatusID = 2
                                mintVisibleID = 1
                                mintApproverApplicationStatusId = mintStatusID
                            Else

                                mintCurrentPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

                                If mintCurrentPriority <= -1 Then GoTo AssignApprover1

                                If mblnIsRejected Then
                                    mintStatusID = -1
                                    mintVisibleID = -1
                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 1
                                    mintStatusID = 1
                                ElseIf mintCurrentPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 2
                                    mintStatusID = 2
                                Else
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                End If

                                mintApproverApplicationStatusId = mintStatusID
                                mdtApprovalDate = Nothing
                            End If


                        End If

AssignApprover1:
                        mintApproverEmpID = objExpAppr._Employeeunkid
                        mintApproverID = objExpAppr._crApproverunkid

                    End If


                    Dim drFilter As DataRow() = dtFilter.Select("IsGrp = 0 AND crmasterunkid = '" & CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text) & "'")

                    If IsNothing(drFilter) = False AndAlso drFilter.Length > 0 Then
                        drFilter(0)("amount") = dgrow.Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text
                        drFilter(0)("unitprice") = CType(dgrow.Cells(12).FindControl("txtdgcolhUnitPrice"), TextBox).Text
                        drFilter(0)("quantity") = CType(dgrow.Cells(11).FindControl("txtdgcolhQuantity"), TextBox).Text
                        drFilter(0)("Expense_Remark") = CType(dgrow.Cells(14).FindControl("txtdgcolhExpenseRemark"), TextBox).Text
                        drFilter(0).AcceptChanges()
                    End If

                    Dim intExpenseID As Integer = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhExpenseID", False, True)).Text)

                    Dim drExpense As List(Of DataRow) = (From row In dtFilter.AsEnumerable() Where CBool(row.Item("IsGrp")) = False _
                                            AndAlso CInt(row.Item("crmasterunkid")) = objClaimMst._Crmasterunkid AndAlso CInt(row.Item("expenseunkid")) > 0 Select (row)).ToList


                    Dim sMsg As String = String.Empty

                    sMsg = objClaimMst.IsValid_Expense(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), CInt(drExpense(0)("expenseunkid")), CInt(Session("Fin_year")) _
                                                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CDate(drExpense(0)("ClaimDate")).Date _
                                                                                              , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), objClaimMst._Crmasterunkid, False)




                    If sMsg <> "" Then
                        DisplayMessage.DisplayMessage(sMsg, Me)
                        mblnWarning = True
                        Continue For
                    End If

                    Dim drRow() As DataRow = dtFilter.Select("IsGrp = False AND AUD = ''")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            dr("AUD") = "U"
                            dr.AcceptChanges()
                        Next
                        dtFilter.AcceptChanges()
                    End If

                    objExpApproverTran._DataTable = dtFilter

                    objExpApproverTran._YearId = CInt(Session("Fin_year"))
                    objExpApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                    objExpApproverTran._WebFormName = mstrModuleName
                    objExpApproverTran._WebClientIP = CStr(Session("IP_ADD"))
                    objExpApproverTran._WebHostName = CStr(Session("HOST_NAME"))



                    'Pinkal (22-Oct-2021)-- Start
                    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                    objClaimMst._WebFormName = mstrModuleName
                    objClaimMst._WebClientIP = CStr(Session("IP_ADD"))
                    objClaimMst._WebHostName = CStr(Session("HOST_NAME"))
                    'Pinkal (22-Oct-2021)-- End

                    objExpApproverTran._EmployeeID = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text)



                    If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, CInt(Session("UserId")) _
                                                                                            , ConfigParameter._Object._CurrentDateAndTime, CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text), Nothing, mstrRejectRemark _
                                                                                            , blnLastApprover, mdtApprovalDate) = False Then
                        Exit For
                    End If

                    If mintApproverApplicationStatusId = 1 Then
                        objExpApproverTran.SendMailToApprover(CInt(cboExpenseCategory.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text) _
                                                                                    , dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCLno", False, True)).Text, CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), mintPriority, 1, "crpriority > " & mintPriority, Session("Database_Name").ToString() _
                                                                                    , Session("EmployeeAsOnDate").ToString(), CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString() _
                                                                                    , enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), "")
                    End If

                    objClaimMst._Crmasterunkid = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text)


                    If objClaimMst._Statusunkid = 1 AndAlso mintApproverApplicationStatusId = 1 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email

                        If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
                            Dim objLeaveForm As New clsleaveform
                            objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                            objLeaveForm._EmployeeCode = objEmployee._Employeecode
                            objLeaveForm._EmployeeFirstName = objEmployee._Firstname
                            objLeaveForm._EmployeeMiddleName = objEmployee._Othername
                            objLeaveForm._EmployeeSurName = objEmployee._Surname
                            objLeaveForm._EmpMail = objEmployee._Email

                            If objLeaveForm._Statusunkid = 7 Then 'only Issued
                                Dim objCommonCode As New CommonCodes
                                Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
                                Dim strPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid _
                                                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date _
                                                                                                                            , CDate(Session("fin_enddate")).Date, True, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                                objCommonCode = Nothing

                                Dim objLeaveType As New clsleavetype_master
                                objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid

                                objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate _
                                                                                   , objLeaveForm._Returndate, objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "" _
                                                                                   , Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "" _
                                                                                   , CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text), objLeaveForm._Formno)
                                'Pinkal (01-Apr-2019) -- End


                                objLeaveType = Nothing
                                objLeaveForm = Nothing

                            End If
                        Else

                            objClaimMst.SendMailToEmployee(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "")
                        End If
                        objEmployee = Nothing

                    ElseIf objClaimMst._Statusunkid = 3 AndAlso mintApproverApplicationStatusId = 3 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email


                        objClaimMst.SendMailToEmployee(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", _
                                                                           enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRemarks.Text.Trim)

                        objEmployee = Nothing
                    End If

                Next

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtApproverTable IsNot Nothing Then dtApproverTable.Clear()
                dtApproverTable = Nothing
                If dtFilter IsNot Nothing Then dtFilter.Clear()
                dtFilter = Nothing
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                'Pinkal (05-Sep-2020) -- End

            Next


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveApprover = Nothing
            objExpApproverTran = Nothing
            objExpAppr = Nothing
            objClaimMst = Nothing
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (05-Sep-2020) -- End

            If mblnWarning = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Some of the Claim Expense Form(s) cannot be Approved. Reason : It is exceeded the current occurrence limit and will be highlighted in red."), Me)
                Exit Sub
            Else
                Call FillGrid()
            End If
            'Gajanan [20-July-2020] -- End

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            objClaimEmailList.AddRange(gobjEmailList)


            ''Pinkal (25-Aug-2021)-- Start
            ''KBC Enhancement : Creating Approved Claim Summary Report in Desktop and MSS.
            'If gobjEmailList.Count > 0 Then
            '    Dim objThread As Threading.Thread
            '    If HttpContext.Current Is Nothing Then
            '        objThread = New Threading.Thread(AddressOf Send_Notification)
            '        objThread.IsBackground = True
            '        Dim arr(1) As Object
            '        arr(0) = CInt(Session("CompanyUnkId"))
            '        objThread.Start(arr)
            '    Else
            '        Call Send_Notification(CInt(Session("CompanyUnkId")))
            '    End If
            'End If
            ''Pinkal (25-Aug-2021) -- End

            If objClaimEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = CInt(Session("CompanyUnkId"))
                    objThread.Start(arr)
                Else
                    Call Send_Notification(CInt(Session("CompanyUnkId")))
                End If
            End If

            'Pinkal (25-Jan-2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
        Finally
            popupAdvanceFilter.Hide()
            'Pinkal (25-Jan-2022) -- End
        End Try
    End Sub
    'Pinkal (22-Oct-2018) -- End


    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'If gobjEmailList.Count > 0 Then
            If objClaimEmailList.Count > 0 Then
                'Pinkal (25-Jan-2022) -- End
                Dim objSendMail As New clsSendMail
SendEmail:
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'For Each objEmail In gobjEmailList
                For Each objEmail In objClaimEmailList
                    gobjEmailList.Remove(objEmail)
                    'Pinkal (25-Jan-2022) -- End
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    'Pinkal (22-Oct-2021)-- Start
                    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                    objSendMail._Form_Name = mstrModuleName
                    objSendMail._WebClientIP = Session("IP_ADD").ToString()
                    objSendMail._WebHostName = Session("HOST_NAME").ToString()
                    'Pinkal (22-Oct-2021)-- End


                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = intCompanyID(0)
                    End If

                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
                    '                            objEmail._ExportReportPath).ToString.Length > 0 Then
                    '    gobjEmailList.Remove(objEmail)
                    '    GoTo SendEmail
                    'End If
                    objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), objEmail._ExportReportPath)
                    'Pinkal (25-Jan-2022) -- End
                Next
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'gobjEmailList.Clear()
                objClaimEmailList.Clear()
                'Pinkal (25-Jan-2022) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region
    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Dim dsCombo As DataSet = Nothing
        Try

            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", 0, False)
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


            If CBool(Session("PaymentApprovalwithLeaveApproval")) And CInt(cboExpenseCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                Dim objLeaveApprover As New clsleaveapprover_master
                dsCombo = objLeaveApprover.GetEmployeeFromUser(Session("Database_Name").ToString(), _
                                                                                             CInt(Session("Fin_year")), _
                                                                                             CInt(Session("CompanyUnkId")), _
                                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                             True, CInt(Session("UserId")), CBool(Session("AllowtoApproveLeave")), CBool(Session("AllowIssueLeave")))
                Dim lstIDs As List(Of String) = (From p In dsCombo.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

                objLeaveApprover = Nothing
            Else

                Dim objEmployee As New clsEmployee_Master
                dsCombo = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                         CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                         Session("UserAccessModeSetting").ToString(), _
                                         True, False, "List", True)

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objEmployee = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            Dim objExpApproverMst As New clsExpenseApprover_Master
            dsCombo = objExpApproverMst.GetApproverWithFromUserLogin(CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(Session("UserId")), CInt(cboExpenseCategory.SelectedValue))
            objExpApproverMst = Nothing
            If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsCombo.Tables(0).NewRow
                drRow("approverunkid") = 0
                drRow("Approver") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Select")
                dsCombo.Tables(0).Rows.Add(drRow)
                drRow = Nothing
            End If

            With cboApprover
                .DataValueField = "approverunkid"
                .DataTextField = "Approver"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            cboApprover_SelectedIndexChanged(New Object, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Osptimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.SelectedIndexChanged
        Try
            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                Dim objLeaveapprover As New clsleaveapprover_master
                objLeaveapprover._Approverunkid = CInt(cboApprover.SelectedValue)
                mblnIsExternalApprover = objLeaveapprover._Isexternalapprover

                Dim objApproverLevel As New clsapproverlevel_master
                objApproverLevel._Levelunkid = objLeaveapprover._Levelunkid
                mintPriority = objApproverLevel._Priority

                objApproverLevel = Nothing
                objLeaveapprover = Nothing

            Else
                Dim objExApproverMaster As New clsExpenseApprover_Master
                objExApproverMaster._crApproverunkid = CInt(cboApprover.SelectedValue)
                mblnIsExternalApprover = objExApproverMaster._Isexternalapprover

                Dim objExapproverLevel As New clsExApprovalLevel
                objExapproverLevel._Crlevelunkid = objExApproverMaster._crLevelunkid
                mintPriority = objExapproverLevel._Crpriority
                objExApproverMaster = Nothing

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objExapproverLevel = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Links Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try         'Hemant (13 Aug 2020)
            popupAdvanceFilter.Show()
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            Me.ViewState("EmpAdvanceSearch") = Nothing
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("GlobalApproval") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Dim strAdvanceSearch As String
        Try
            strAdvanceSearch = popupAdvanceFilter._GetFilterString
            Me.ViewState.Add("EmpAdvanceSearch", strAdvanceSearch)
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Gajanan [20-July-2020] -- Start
            'Enhancement: Optimize For NMB

            'If CInt(cboApprover.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), Me)
            '    cboApprover.Focus()
            '    Exit Sub
            'End If

            'Dim dtTemp() As DataRow = Nothing
            'dtTemp = dtList.Select("IsChecked = 1 And IsGrp = True")



            'If dtTemp.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select atleast one Claim Expense Form to Approve/Reject."), Me)
            '    Exit Sub
            'End If

            'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemarks.Text.Trim.Length <= 0 Then  'Rejected
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Remark is compulsory information.Please Enter Remark."), Me)
            '    txtRemarks.Focus()
            '    Exit Sub
            'End If


            ''Pinkal (22-Oct-2018) -- Start
            ''Enhancement - Implementing Claim & Request changes For NMB .

            'mblnIsExpenseSaveClick = True

            'If dtList IsNot Nothing AndAlso dtList.Rows.Count > 0 Then
            '    Dim drUnit() As DataRow = dtList.Select("IsChecked = 1 And IsGrp = 0 AND unitprice <= 0")
            '    If drUnit.Length > 0 Then
            '        popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Some of the expense(s) had 0 unit price for checked employee(s).") & vbCrLf & _
            '                                                            Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Do you wish to continue?")
            '        popup_UnitPriceYesNo.Show()
            '        drUnit = Nothing
            '        Exit Sub
            '    Else
            '        GlobalSave()
            '    End If
            '    drUnit = Nothing
            'Else
            '    GlobalSave()
            'End If

            If CInt(cboApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), Me)
                cboApprover.Focus()
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text) = True)


            If gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select atleast one Claim Expense Form to Approve/Reject."), Me)
                Exit Sub
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (05-Sep-2020) -- End


            If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemarks.Text.Trim.Length <= 0 Then  'Rejected
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Remark is compulsory information.Please Enter Remark."), Me)
                txtRemarks.Focus()
                Exit Sub
            End If


            mblnIsExpenseSaveClick = True


            If dgvData.Items.Count > 0 Then
                Dim drUnit As IEnumerable(Of DataGridItem) = Nothing
                drUnit = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And _
                                                                 CBool(x.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text) = False And _
                                                                 CInt(IIf(CType(x.FindControl("txtdgcolhUnitPrice"), TextBox).Text = "", 0, CType(x.FindControl("txtdgcolhUnitPrice"), TextBox).Text)) <= 0)
                If drUnit.Count > 0 Then
                    popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Some of the expense(s) had 0 unit price for checked employee(s).") & vbCrLf & _
                                                                        Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Do you wish to continue?")
                    popup_UnitPriceYesNo.Show()
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If drUnit IsNot Nothing Then drUnit.ToList().Clear()
                    'Pinkal (05-Sep-2020) -- End
                    drUnit = Nothing
                    Exit Sub
                Else
                    GlobalSave()
                End If
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If drUnit IsNot Nothing Then drUnit.ToList().Clear()
                'Pinkal (05-Sep-2020) -- End
                drUnit = Nothing
            Else
                GlobalSave()
            End If

            'Gajanan [20-July-2020] -- End

            '            Dim objLeaveApprover As New clsleaveapprover_master
            '            Dim objExpApproverTran As New clsclaim_request_approval_tran
            '            Dim objExpAppr As New clsExpenseApprover_Master
            '            Dim objClaimMst As New clsclaim_request_master
            '            Dim mintMaxPriority As Integer = -1
            '            Dim mstrRejectRemark As String = ""
            '            Dim blnLastApprover As Boolean = False
            '            Dim mblnIssued As Boolean = False
            '            Dim mintStatusID As Integer = 2 'PENDING
            '            Dim mintVisibleID As Integer = 2 'PENDING


            '            Dim mblnWarning As Boolean = False

            '            For i As Integer = 0 To dtTemp.Length - 1

            '                objClaimMst._Crmasterunkid = CInt(dtTemp(i)("crmasterunkid"))

            '                Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
            '                                                                                                     , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpenseCategory.SelectedValue) _
            '                                                                                                     , False, True, -1, "", CInt(dtTemp(i)("crmasterunkid")))

            '                Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintPriority, "crpriority asc", DataViewRowState.CurrentRows).ToTable
            '                mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            '                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
            '                    objLeaveApprover = New clsleaveapprover_master
            '                    objLeaveApprover._Approverunkid = CInt(dtTemp(i)("crmasterunkid"))
            '                    Dim objLeaveForm As New clsleaveform
            '                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
            '                    If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = mintPriority) Then mblnIssued = True
            '                End If

            '                Dim mblnIsRejected As Boolean = False
            '                Dim mintApproverApplicationStatusId As Integer = 2

            '                For j As Integer = 0 To dtApproverTable.Rows.Count - 1
            '                    blnLastApprover = False
            '                    Dim mintApproverEmpID As Integer = -1
            '                    Dim mintApproverID As Integer = -1
            '                    Dim mdtApprovalDate As Date = Nothing


            '                    If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
            '                        objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))


            '                        If CInt(cboApprover.SelectedValue) = objLeaveApprover._Approverunkid Then
            '                            mintStatusID = CInt(cboStatus.SelectedValue)
            '                            mintVisibleID = CInt(cboStatus.SelectedValue)
            '                            mintApproverApplicationStatusId = mintStatusID
            '                            mdtApprovalDate = DateTime.Now
            '                            If CInt(cboStatus.SelectedValue) = 1 Then
            '                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
            '                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
            '                                mstrRejectRemark = txtRemarks.Text.Trim
            '                                blnLastApprover = True
            '                            End If

            '                        ElseIf CInt(cboApprover.SelectedValue) <> objLeaveApprover._Approverunkid Then

            '                            Dim mintPriority As Integer = -1
            '                            Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

            '                            If dRow.Length > 0 Then
            '                                mintStatusID = 2
            '                                mintVisibleID = 1
            '                                mintApproverApplicationStatusId = mintStatusID

            '                            Else

            '                                mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

            '                                If mintPriority <= -1 Then
            '                                    mintStatusID = 2
            '                                    mintVisibleID = 1
            '                                    mintApproverApplicationStatusId = mintStatusID
            '                                    GoTo AssignApprover
            '                                ElseIf mblnIsRejected Then
            '                                    mintVisibleID = -1
            '                                    mintStatusID = -1
            '                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 1
            '                                    mintStatusID = 1
            '                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 2
            '                                    mintStatusID = 2
            '                                Else
            '                                    mintVisibleID = -1
            '                                    mintStatusID = -1
            '                                End If

            '                                mintApproverApplicationStatusId = mintStatusID
            '                            End If
            '                            mdtApprovalDate = Nothing
            '                        End If

            'AssignApprover:
            '                        mintApproverEmpID = objLeaveApprover._leaveapproverunkid
            '                        mintApproverID = objLeaveApprover._Approverunkid

            '                    Else

            '                        objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))

            '                        If CInt(cboApprover.SelectedValue) = objExpAppr._crApproverunkid Then

            '                            mintStatusID = CInt(cboStatus.SelectedValue)
            '                            mintVisibleID = CInt(cboStatus.SelectedValue)
            '                            mintApproverApplicationStatusId = mintStatusID
            '                            mdtApprovalDate = DateTime.Now
            '                            If CInt(cboStatus.SelectedValue) = 1 Then
            '                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then blnLastApprover = True
            '                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
            '                                mstrRejectRemark = txtRemarks.Text.Trim
            '                                blnLastApprover = True
            '                                mblnIsRejected = True
            '                            End If

            '                        ElseIf CInt(cboApprover.SelectedValue) <> objExpAppr._crApproverunkid Then

            '                            Dim mintPriority As Integer = -1
            '                            Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

            '                            If dRow.Length > 0 Then
            '                                mintStatusID = 2
            '                                mintVisibleID = 1
            '                                mintApproverApplicationStatusId = mintStatusID
            '                            Else
            '                                mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

            '                                If mintPriority <= -1 Then GoTo AssignApprover1

            '                                If mblnIsRejected Then
            '                                    mintStatusID = -1
            '                                    mintVisibleID = -1
            '                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 1
            '                                    mintStatusID = 1
            '                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
            '                                    mintVisibleID = 2
            '                                    mintStatusID = 2
            '                                Else
            '                                    mintVisibleID = -1
            '                                    mintStatusID = -1
            '                                End If
            '                                mintApproverApplicationStatusId = mintStatusID
            '                                mdtApprovalDate = Nothing
            '                            End If


            '                        End If

            'AssignApprover1:
            '                        mintApproverEmpID = objExpAppr._Employeeunkid
            '                        mintApproverID = objExpAppr._crApproverunkid

            '                    End If

            '                    Dim dtFilter As DataTable = New DataView(dtList, "IsChecked = 1 AND crmasterunkid = " & CInt(dtTemp(i)("crmasterunkid")), "", DataViewRowState.CurrentRows).ToTable

            '                    Dim intExpenseID As Integer = CInt(dtTemp(i)("expenseunkid"))

            '                    Dim drExpense As List(Of DataRow) = (From row In dtList.AsEnumerable() Where CBool(row.Item("IsGrp")) = False AndAlso CBool(row.Item("IsChecked")) = True _
            '                                            AndAlso CInt(row.Item("crmasterunkid")) = objClaimMst._Crmasterunkid AndAlso CInt(row.Item("expenseunkid")) > 0 Select (row)).ToList


            '                    Dim sMsg As String = String.Empty
            '                    sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")))
            '                    If sMsg <> "" Then
            '                        DisplayMessage.DisplayMessage(sMsg, Me)
            '                        mblnWarning = True
            '                        Continue For
            '                    End If

            '                    Dim drRow() As DataRow = dtFilter.Select("IsGrp = False AND AUD = ''")
            '                    If drRow.Length > 0 Then
            '                        For Each dr As DataRow In drRow
            '                            dr("AUD") = "U"
            '                            dr.AcceptChanges()
            '                        Next
            '                        dtFilter.AcceptChanges()
            '                    End If

            '                    objExpApproverTran._DataTable = dtFilter
            '                    objExpApproverTran._YearId = FinancialYear._Object._YearUnkid
            '                    objExpApproverTran._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            '                    objExpApproverTran._EmployeeID = CInt(dtTemp(i)("employeeunkid"))
            '                    If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, CInt(Session("UserId")) _
            '                                                                                            , ConfigParameter._Object._CurrentDateAndTime, CInt(dtTemp(i)("crmasterunkid")), Nothing, mstrRejectRemark _
            '                                                                                            , blnLastApprover, mdtApprovalDate) = False Then
            '                        Exit For
            '                    End If

            '                    If mintApproverApplicationStatusId = 1 Then
            '                        objExpApproverTran.SendMailToApprover(CInt(cboExpenseCategory.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(dtTemp(i)("crmasterunkid")) _
            '                                                                                    , dtTemp(i)("CLno").ToString(), CInt(dtTemp(i)("employeeunkid")), mintPriority, 1, "crpriority > " & mintPriority, Session("Database_Name").ToString() _
            '                                                                                    , Session("EmployeeAsOnDate").ToString(), CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString() _
            '                                                                                    , enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), "")
            '                    End If

            '                    objClaimMst._Crmasterunkid = CInt(dtTemp(i)("crmasterunkid"))

            '                    If objClaimMst._Statusunkid = 1 AndAlso mintApproverApplicationStatusId = 1 Then
            '                        Dim objEmployee As New clsEmployee_Master
            '                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
            '                        objClaimMst._EmployeeCode = objEmployee._Employeecode
            '                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
            '                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
            '                        objClaimMst._EmployeeSurName = objEmployee._Surname
            '                        objClaimMst._EmpMail = objEmployee._Email

            '                        If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
            '                            Dim objLeaveForm As New clsleaveform
            '                            objLeaveForm._Formunkid = objClaimMst._Referenceunkid
            '                            objLeaveForm._EmployeeCode = objEmployee._Employeecode
            '                            objLeaveForm._EmployeeFirstName = objEmployee._Firstname
            '                            objLeaveForm._EmployeeMiddleName = objEmployee._Othername
            '                            objLeaveForm._EmployeeSurName = objEmployee._Surname
            '                            objLeaveForm._EmpMail = objEmployee._Email

            '                            If objLeaveForm._Statusunkid = 7 Then 'only Issued
            '                                Dim objCommonCode As New CommonCodes
            '                                Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp


            '                                'Pinkal (16-Dec-2016) -- Start
            '                                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
            '                                'Dim strPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid _
            '                                '                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date _
            '                                '                                                                                            , CDate(Session("fin_enddate")).Date, True)
            '                                Dim strPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid _
            '                                                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date _
            '                                                                                                                            , CDate(Session("fin_enddate")).Date, True, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
            '                                'Pinkal (16-Dec-2016) -- End



            '                                objCommonCode = Nothing

            '                                Dim objLeaveType As New clsleavetype_master
            '                                objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid
            '                                'Sohail (30 Nov 2017) -- Start
            '                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '                                'objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate, objLeaveForm._Returndate _
            '                                '                                                    , objLeaveForm._Statusunkid, "", Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "", CInt(dtTemp(i)("crmasterunkid")))
            '                                objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate, objLeaveForm._Returndate _
            '                                                                                    , objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "", CInt(dtTemp(i)("crmasterunkid")))
            '                                'Sohail (30 Nov 2017) -- End
            '                                objLeaveType = Nothing
            '                                objLeaveForm = Nothing

            '                            End If
            '                        Else
            '                            'Sohail (30 Nov 2017) -- Start
            '                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '                            'objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "")
            '                            objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "")
            '                            'Sohail (30 Nov 2017) -- End
            '                        End If
            '                        objEmployee = Nothing

            '                    ElseIf objClaimMst._Statusunkid = 3 AndAlso mintApproverApplicationStatusId = 3 Then
            '                        Dim objEmployee As New clsEmployee_Master
            '                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = objClaimMst._Employeeunkid
            '                        objClaimMst._EmployeeCode = objEmployee._Employeecode
            '                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
            '                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
            '                        objClaimMst._EmployeeSurName = objEmployee._Surname
            '                        objClaimMst._EmpMail = objEmployee._Email
            '                        'Sohail (30 Nov 2017) -- Start
            '                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '                        'objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", _
            '                        '                                                   enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRemarks.Text.Trim)
            '                        objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", _
            '                                                                           enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRemarks.Text.Trim)
            '                        'Sohail (30 Nov 2017) -- End
            '                        objEmployee = Nothing
            '                    End If

            '                Next

            '            Next

            '            If mblnWarning = True Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Some of the Claim Expense Form(s) cannot be Approved. Reason : It is exceeded the current occurrence limit and will be highlighted in red."), Me)
            '                Exit Sub
            '            Else
            '                Call FillGrid()
            '            End If

            'Pinkal (22-Oct-2018) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If mblnIsExpenseSaveClick Then
                GlobalSave()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGridView Event"

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try

            If e.Item.ItemIndex >= 0 Then

                If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.SelectedItem Then

                    If e.Item.Cells(15).Text.Trim.Length > 0 AndAlso CBool(e.Item.Cells(15).Text.Trim) = False AndAlso e.Item.Cells(15).Text.Trim <> "&nbsp;" Then  'Is Grp
                        e.Item.Cells(1).Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & e.Item.Cells(1).Text
                        Dim chk As CheckBox = CType(e.Item.Cells(0).FindControl("chkSelect"), CheckBox)
                        chk.Enabled = False
                    Else
                        e.Item.Cells(1).ColumnSpan = 7
                        e.Item.Cells(1).Style.Add("text-align", "left")
                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(1).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End

                        For i = 2 To 7
                            e.Item.Cells(i).Visible = False
                        Next

                        e.Item.Cells(8).ColumnSpan = e.Item.Cells.Count - 1
                        e.Item.Cells(8).Style.Add("text-align", "left")
                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(8).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(8).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End


                        For i = 9 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next
                    End If


                    If e.Item.Cells(5).Text.Trim.Length > 0 AndAlso e.Item.Cells(5).Text.Trim <> "&nbsp;" Then  'Balance
                        e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), "#0.00")
                    End If

                    'If e.Item.Cells(6).Text.Trim.Length > 0 AndAlso e.Item.Cells(6).Text.Trim <> "&nbsp;" Then  'Applied quantity
                    '    e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString())
                    'End If

                    'If e.Item.Cells(7).Text.Trim.Length > 0 AndAlso e.Item.Cells(7).Text.Trim <> "&nbsp;" Then  'Applied Amount" Then
                    '    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString())
                    'End If

                    'Dim Txtbox As TextBox = CType(e.Item.Cells(9).FindControl("txtdgcolhQuantity"), TextBox)  'Quantity
                    'If Txtbox.Text.Trim <> "" Then
                    '    Txtbox.Text = Format(CDec(Txtbox.Text), Session("fmtCurrency").ToString)
                    'End If
                    'Txtbox = Nothing

                    'Txtbox = CType(e.Item.Cells(10).FindControl("txtdgcolhUnitPrice"), TextBox)   'Unit Price
                    'If Txtbox.Text.Trim <> "" Then
                    '    Txtbox.Text = Format(CDec(Txtbox.Text), Session("fmtCurrency").ToString)
                    'End If

                    'If e.Item.Cells(11).Text.Trim.Length > 0 AndAlso e.Item.Cells(11).Text.Trim <> "&nbsp;" Then  'Amount
                    '    e.Item.Cells(11).Text = Format(CDec(e.Item.Cells(11).Text), Session("fmtCurrency").ToString())
                    'End If

                    If e.Item.Cells(7).Text.Trim.Length > 0 AndAlso e.Item.Cells(7).Text.Trim <> "&nbsp;" Then  'Applied quantity
                        e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString())
                    End If

                    If e.Item.Cells(8).Text.Trim.Length > 0 AndAlso e.Item.Cells(8).Text.Trim <> "&nbsp;" Then  'Applied Amount" Then
                        e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString())
                    End If

                    Dim Txtbox As TextBox = CType(e.Item.Cells(11).FindControl("txtdgcolhQuantity"), TextBox)  'Quantity
                    If Txtbox.Text.Trim <> "" Then
                        Txtbox.Text = Format(CDec(Txtbox.Text), Session("fmtCurrency").ToString)
                    End If
                    Txtbox = Nothing

                    Txtbox = CType(e.Item.Cells(12).FindControl("txtdgcolhUnitPrice"), TextBox)   'Unit Price
                    If Txtbox.Text.Trim <> "" Then
                        Txtbox.Text = Format(CDec(Txtbox.Text), Session("fmtCurrency").ToString)
                    End If

                    If e.Item.Cells(13).Text.Trim.Length > 0 AndAlso e.Item.Cells(13).Text.Trim <> "&nbsp;" Then  'Amount
                        e.Item.Cells(13).Text = Format(CDec(e.Item.Cells(13).Text), Session("fmtCurrency").ToString())
                    End If

                    ' For Cost Center
                    Dim dgcolhCostCenter As DropDownList = CType(e.Item.Cells(6).FindControl("dgcolhCostCenter"), DropDownList)  'Cost Center
                    Dim objCostCenter As New clscostcenter_master
                    Dim dtTable As DataTable = Nothing
                    Dim dsCombo As DataSet = objCostCenter.getComboList("List", False)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objCostCenter = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    With dgcolhCostCenter
                        .DataValueField = "costcenterunkid"
                        .DataTextField = "costcentername"
                        If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            dtTable = dsCombo.Tables(0)
                        Else
                            Dim dRow As DataRow = dsCombo.Tables(0).NewRow
                            dRow("costcenterunkid") = "0"
                            dRow("costcentername") = ""
                            dsCombo.Tables(0).Rows.InsertAt(dRow, 0)
                            dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                        End If
                        .DataSource = dtTable
                        .DataBind()
                        .SelectedValue = CType(e.Item.FindControl("hdcolhCostCenter"), HiddenField).Value
                    End With
                    dgcolhCostCenter.Enabled = False

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End


                    'Currency
                    Dim dgcolhCurrency As DropDownList = CType(e.Item.Cells(9).FindControl("dgcolhCurrency"), DropDownList)  'Currency
                    Dim objExchange As New clsExchangeRate
                    dsCombo = objExchange.getComboList("List", True, False)
                    With dgcolhCurrency
                        .DataValueField = "countryunkid"
                        .DataTextField = "currency_sign"
                        .DataSource = dsCombo.Tables(0)
                        .DataBind()
                        .SelectedValue = CType(e.Item.FindControl("hdcolhCurrency"), HiddenField).Value
                    End With
                    dgcolhCurrency.Enabled = False

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsCombo IsNot Nothing Then dsCombo.Clear()
                    dsCombo = Nothing
                    objExchange = Nothing
                    'Pinkal (05-Sep-2020) -- End

                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvData_ItemDataBound:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Protected Sub txtdgcolhUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Gajanan [20-July-2020] -- Start
            'Enhancement: Optimize For NMB

            'Dim txtUnit As TextBox = CType(sender, TextBox)
            'Dim item As DataGridItem = CType(txtUnit.NamingContainer, DataGridItem)

            ''Pinkal (22-Oct-2018) -- Start
            ''Enhancement - Implementing Claim & Request changes For NMB .
            ''If txtUnit.Text.Trim = "" OrElse txtUnit.Text = "0" Then
            ''    txtUnit.Text = CStr(1)
            ''End If
            'If txtUnit.Text.Trim = "" Then txtUnit.Text = "0"
            ''Pinkal (22-Oct-2018) -- End

            'txtUnit.Text = Format(CDec(txtUnit.Text), Session("fmtCurrency").ToString())
            'dtList.Rows(item.ItemIndex).Item("unitprice") = CDec(txtUnit.Text)

            ''Pinkal (04-Feb-2019) -- Start
            ''Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            ''dtList.Rows(item.ItemIndex).Item("amount") = Format(CDec(CType(item.Cells(16).FindControl("txtdgcolhQuantity"), TextBox).Text) * CDec(txtUnit.Text), Session("fmtCurrency").ToString())
            'dtList.Rows(item.ItemIndex).Item("amount") = Format(CDec(CType(item.Cells(11).FindControl("txtdgcolhQuantity"), TextBox).Text) * CDec(txtUnit.Text), Session("fmtCurrency").ToString())
            ''Pinkal (04-Feb-2019) -- End
            'dtList.AcceptChanges()

            ''Pinkal (04-Feb-2019) -- Start
            ''Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            ''dgvData.Items(item.ItemIndex).Cells(11).Text = Format(CDec(dtList.Rows(item.ItemIndex).Item("amount")), Session("fmtCurrency").ToString())
            'dgvData.Items(item.ItemIndex).Cells(13).Text = Format(CDec(dtList.Rows(item.ItemIndex).Item("amount")), Session("fmtCurrency").ToString())
            ''Pinkal (04-Feb-2019) -- End
            'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)

            Dim txtUnit As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtUnit.NamingContainer, DataGridItem)

            If txtUnit.Text.Trim = "" Then txtUnit.Text = "0"

            txtUnit.Text = Format(CDec(txtUnit.Text), Session("fmtCurrency").ToString())

            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(CType(item.Cells(11).FindControl("txtdgcolhQuantity"), TextBox).Text) * CDec(txtUnit.Text), Session("fmtCurrency").ToString())

            dgvData.Items(item.ItemIndex).Cells(13).Text = Format(CDec(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
            'Gajanan [20-July-2020] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtdgcolhQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Gajanan [20-July-2020] -- Start
            'Enhancement: Optimize For NMB

            'Dim txtQty As TextBox = CType(sender, TextBox)
            'Dim item As DataGridItem = CType(txtQty.NamingContainer, DataGridItem)
            'If txtQty.Text.Trim = "" OrElse txtQty.Text = "0" Then
            '    txtQty.Text = CStr(1)
            'End If


            ''Pinkal (25-Oct-2018) -- Start
            ''Enhancement - Implementing Claim & Request changes For NMB .
            'Dim objExpense As New clsExpense_Master

            ''Pinkal (04-Feb-2019) -- Start
            ''Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            ''objExpense._Expenseunkid = CInt(item.Cells(18).Text)
            'objExpense._Expenseunkid = CInt(item.Cells(20).Text) 'expenseunkid
            ''Pinkal (04-Feb-2019) -- End


            'If objExpense._IsConsiderDependants Then
            '    Dim mintEmpDepedentsCoutnForCR As Integer = 0
            '    Dim objDependents As New clsDependants_Beneficiary_tran

            '    'Pinkal (04-Feb-2019) -- Start
            '    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '    'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(item.Cells(15).Text))
            '    'Sohail (18 May 2019) -- Start
            '    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            '    'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(item.Cells(17).Text))  'Employeeunkid
            '    Dim objClaimMst As New clsclaim_request_master
            '    objClaimMst._Crmasterunkid = CInt(item.Cells(21).Text) 'crmasterunkid
            '    Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(item.Cells(17).Text))  'Employeeunkid
            '    'Sohail (18 May 2019) -- End
            '    'Pinkal (04-Feb-2019) -- End


            '    mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            '    objDependents = Nothing
            '    If mintEmpDepedentsCoutnForCR < CDec(txtQty.Text) Then
            '        'Sohail (23 Mar 2019) -- Start
            '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '        'DisplayMessage.DisplayError(ex, Me)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit for this selected employee."), Me)
            '        'Sohail (23 Mar 2019) -- End
            '        txtQty.Focus()
            '        Exit Sub
            '    End If
            'End If
            ''Pinkal (25-Oct-2018) -- End


            ''Pinkal (10-Jun-2020) -- Start
            ''Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            'If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmExpenseApproval", 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
            '    txtQty.Focus()
            '    Exit Sub
            'End If
            ''Pinkal (10-Jun-2020) -- End


            'txtQty.Text = Format(CDec(txtQty.Text), Session("fmtCurrency").ToString())
            'dtList.Rows(item.ItemIndex).Item("quantity") = CDec(txtQty.Text)

            ''Pinkal (04-Feb-2019) -- Start
            ''Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            ''dtList.Rows(item.ItemIndex).Item("amount") = Format(CDec(txtQty.Text) * CDec(CType(item.Cells(16).FindControl("txtdgcolhUnitPrice"), TextBox).Text), Session("fmtCurrency").ToString())
            'dtList.Rows(item.ItemIndex).Item("amount") = Format(CDec(txtQty.Text) * CDec(CType(item.Cells(12).FindControl("txtdgcolhUnitPrice"), TextBox).Text), Session("fmtCurrency").ToString())
            ''Pinkal (04-Feb-2019) -- End

            'dtList.AcceptChanges()

            ''Pinkal (04-Feb-2019) -- Start
            ''Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            ''dgvData.Items(item.ItemIndex).Cells(11).Text = Format(CDec(dtList.Rows(item.ItemIndex).Item("amount")), Session("fmtCurrency").ToString())
            'dgvData.Items(item.ItemIndex).Cells(13).Text = Format(CDec(dtList.Rows(item.ItemIndex).Item("amount")), Session("fmtCurrency").ToString())
            ''Pinkal (04-Feb-2019) -- End


            'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)


            Dim txtQty As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtQty.NamingContainer, DataGridItem)
            If txtQty.Text.Trim = "" OrElse txtQty.Text = "0" Then
                txtQty.Text = CStr(1)
            End If

            Dim objExpense As New clsExpense_Master

            objExpense._Expenseunkid = CInt(item.Cells(20).Text)

            If objExpense._IsConsiderDependants Then
                Dim mintEmpDepedentsCoutnForCR As Integer = 0


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim objClaimMst As New clsclaim_request_master
                'objClaimMst._Crmasterunkid = CInt(item.Cells(21).Text)
                'Pinkal (05-Sep-2020) -- End

                Dim objDependents As New clsDependants_Beneficiary_tran
                Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(item.Cells(17).Text))
                mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtDependents IsNot Nothing Then dtDependents.Clear()
                dtDependents = Nothing
                'Pinkal (05-Sep-2020) -- End
                objDependents = Nothing

                If mintEmpDepedentsCoutnForCR < CDec(txtQty.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit for this selected employee."), Me)
                    txtQty.Focus()
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpense = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
            End If


            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmExpenseApproval", 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objExpense = Nothing
                'Pinkal (05-Sep-2020) -- End
                Exit Sub
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpense = Nothing
            'Pinkal (05-Sep-2020) -- End



            txtQty.Text = Format(CDec(txtQty.Text), Session("fmtCurrency").ToString())

            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(txtQty.Text) * CDec(CType(item.Cells(12).FindControl("txtdgcolhUnitPrice"), TextBox).Text), Session("fmtCurrency").ToString())

            dgvData.Items(item.ItemIndex).Cells(13).Text = Format(CDec(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
            'Gajanan [20-July-2020] -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtdgcolhQuantity_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub txtdgcolhExpenseRemark_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim txtExpRemark As TextBox = CType(sender, TextBox)
    '        Dim item As DataGridItem = CType(txtExpRemark.NamingContainer, DataGridItem)

    '        dtList.Rows(item.ItemIndex).Item("Expense_Remark") = txtExpRemark.Text.Trim
    '        dtList.AcceptChanges()
    '        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtdgcolhExpenseRemark_TextChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

#End Region

#Region "Checkbox Event"
    'Gajanan [20-July-2020] -- Start
    'Enhancement: Optimize For NMB

    'Protected Sub chkSelect_Checkchanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)


    '        'Pinkal (04-Feb-2019) -- Start
    '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '        'Dim dRow() As DataRow = dtList.Select("crmasterunkid =" & CInt(item.Cells(19).Text))
    '        Dim dRow() As DataRow = dtList.Select("crmasterunkid =" & CInt(item.Cells(21).Text)) 'crmasterunkid
    '        'Pinkal (04-Feb-2019) -- End

    '        If dRow.Length > 0 Then
    '            For Each row In dRow
    '                row.Item("IsChecked") = chkSelect.Checked
    '                CType(dgvData.Items(dtList.Rows.IndexOf(row)).Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkSelect.Checked
    '            Next
    '            dtList.AcceptChanges()
    '        End If
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelect_Checkchanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkSelectAll_Checkchanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgvData.Items.Count <= 0 Then Exit Sub
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelectAll.NamingContainer, DataGridItem)

    '        Dim Query = From Row In dtList.AsEnumerable() Select Row
    '        For Each rows In Query
    '            rows.Item("ischecked") = chkSelectAll.Checked
    '            CType(dgvData.Items(dtList.Rows.IndexOf(rows)).Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '        Next
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelectAll_Checkchanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    'Gajanan [20-July-2020] -- End

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.gbFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbFilter.ID, Me.gbFilter.Text)
            Me.gbInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbInfo.ID, Me.gbInfo.Text)
            Me.gbPending.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbPending.ID, Me.gbPending.Text)


            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lnkAllocation.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.ToolTip)
            'Gajanan [17-Sep-2020] -- End



            Me.lblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblSector.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSector.ID, Me.lblSector.Text)


            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            dgvData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(1).FooterText, dgvData.Columns(1).HeaderText)
            dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)
            dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(7).FooterText, dgvData.Columns(7).HeaderText)
            dgvData.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(8).FooterText, dgvData.Columns(8).HeaderText)
            dgvData.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(9).FooterText, dgvData.Columns(9).HeaderText)
            dgvData.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(10).FooterText, dgvData.Columns(10).HeaderText)
            dgvData.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(11).FooterText, dgvData.Columns(11).HeaderText)
            dgvData.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(12).FooterText, dgvData.Columns(12).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage :- " & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

End Class
