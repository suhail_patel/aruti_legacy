﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_Approval_List.aspx.vb"
    Inherits="Claims_And_Expenses_Approval_List" Title="Expense Approval List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="confirmyesno" TagPrefix="cnfpopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pblMian" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Expense Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExpenseCat" runat="server" Text="Expense Category" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExCategory" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Staus" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLevel" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboApprover" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="GvApprovalList" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit">
                                                                     <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                    CssClass="griddelete">
                                                                          <i class="fas fa-trash text-danger"></i>       
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkActive" runat="server" ToolTip="Make Approver Active" Text="Active"
                                                                CommandName="Active" Font-Underline="false">
                                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkInActive" runat="server" ToolTip="Make Approver Inactive"
                                                                Text="Inactive" CommandName="Inactive" Font-Underline="false">
                                                                         <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="expensetype" HeaderText="Exp. Type" ReadOnly="true" FooterText="colhExpType" />
                                                    <asp:BoundColumn DataField="ename" HeaderText="Expense Approval" ReadOnly="true"
                                                        FooterText="colhApprover" />
                                                    <asp:BoundColumn DataField="clevel" HeaderText="Level" ReadOnly="true" FooterText="colhLevel" />
                                                    <asp:BoundColumn DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDepartment" />
                                                    <asp:BoundColumn DataField="jobname" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                    <asp:BoundColumn DataField="usermapped" HeaderText="Mapped User" ReadOnly="true"
                                                        FooterText="colhMappedUser" />
                                                    <asp:BoundColumn DataField="ExAppr" HeaderText="External Approver" ReadOnly="true"
                                                        FooterText="objcolhExCat"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="crapproverunkid" HeaderText="Expese CateGory" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" />
                <cnfpopup:confirmyesno ID="popupActive" runat="server" />
                <cnfpopup:confirmyesno ID="popupInactive" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
