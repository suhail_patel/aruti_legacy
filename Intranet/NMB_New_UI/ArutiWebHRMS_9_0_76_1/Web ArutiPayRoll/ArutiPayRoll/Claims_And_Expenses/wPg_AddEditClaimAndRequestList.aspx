﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AddEditClaimAndRequestList.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_AddEditClaimAndRequestList" Title="Add/Edit Claim & Request" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script>
        function IsValidAttach() {
            if (parseInt($('select.cboScanDcoumentType')[0].value) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Claim & Request"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="LnkViewDependants" runat="server" ToolTip="View Depedents List">
                                             <i class="fas fa-restroom"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" />
                                                        </div>
                                                        <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" Enabled="False" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblValue" runat="server" Text="Leave Form" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="true" Enabled="False" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                    ReadOnly="true" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblSector" runat="server" Text="Sector/Route" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtQty" runat="server" Text="1" AutoPostBack="true" onKeypress="return onlyNumbers(this,event);" Style="text-align: right"
                                                                    class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                        <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" Text="1.00" onKeypress="return onlyNumbers(this,event);"
                                                                    Style="text-align: right" class="form-control"></asp:TextBox>
                                                                <asp:TextBox ID="txtCosting" runat="server" Text="0.00" Enabled="false" Visible="false"
                                                                    Style="text-align: right" class="form-control"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Panel ID="pnlBalAsonDate" runat="server">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-l--15">
                                                                <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtBalanceAsOnDate" runat="server" Style="text-align: right" Enabled="false"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-l--15">
                                                                <asp:Label ID="lblBalance" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtBalance" runat="server" Enabled="false" Style="text-align: right"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                            <li role="presentation" class="active"><a href="#tbpExpenseRemark" data-toggle="tab">
                                                                <asp:Label ID="tbExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label" />
                                                            </a></li>
                                                            <li role="presentation"><a href="#tbpClaimRemark" data-toggle="tab">
                                                                <asp:Label ID="tbClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label" />
                                                            </a></li>
                                                        </ul>
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane fade in active" id="tbpExpenseRemark">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade" id="tbpClaimRemark">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-primary" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="dgvData" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                ShowFooter="False" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                    ToolTip="Edit">
                                                                               <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                    ToolTip="Delete">
                                                                                <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgView" runat="server" CommandName="attachment" ToolTip="Attachment">
                                                                            <i class="fa fa-paperclip" aria-hidden="true" style="font-size:20px;font-weight:bold"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc" />
                                                    <asp:BoundColumn DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route" />
                                                    <asp:BoundColumn DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" />
                                                    <asp:BoundColumn DataField="quantity" FooterText="dgcolhQty" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Quantity" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Unit Price" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="amount" FooterText="dgcolhAmount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Amount" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="expense_remark" FooterText="dgcolhExpenseRemark" HeaderText="Expense Remark"
                                                        Visible="true" />
                                                    <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" Visible="false" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" Visible="false" />
                                                    <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" Visible="false" />
                                                </Columns>
                                                <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="form-control"
                                                    Style="text-align: right"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnScanAttchment" runat="server" Text="Scan/Attchment" Visible="false"
                                    CssClass="btn btn-default" />
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnEmppnlEmpDepedentsClose" PopupControlID="pnlEmpDepedents"
                    TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlEmpDepedents">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px;">
                                    <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" FooterText="dgcolhName" />
                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" FooterText="dgcolhGender" />
                                            <asp:BoundColumn DataField="age" HeaderText="Age" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAge" />
                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhMonth" />
                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" FooterText="dgcolhRelation" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" TargetControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-25">
                                <div id="fileuploader">
                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Add" />
                                </div>
                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                    CssClass="btn btn-primary" Text="Browse" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 150px;">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                          <i class="fas fa-trash text-danger"></i>     
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download">
                                                                        <i class="fa fa-download"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                </asp:Panel>
                <ucCfnYesno:Confirmation ID="popup_UnitPriceYesNo" runat="server" Message="" Title="Confirmation" />
                <ucCfnYesno:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Message="" Title="Confirmation" />
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To delete?:" />
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_AddEditClaimAndRequestList.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $("body").on("click", "input[type=file]", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>
