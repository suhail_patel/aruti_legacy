﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO

#End Region


Partial Class Claims_And_Expenses_wPg_ExpenseAdjustment
    Inherits Basepage


#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmExpenseAdjustment"
    Private DisplayMessage As New CommonCodes
    Dim mstrAdvanceFilter As String = ""
#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If



            If Not IsPostBack Then
                SetLanguage()
                FillCombo()
                dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                txtAdjustmentAmount.Text = "0.00"
                rdOption.SelectedValue = "1"
                FillData(True)
            Else
                If Me.ViewState("AdvanceFilter") IsNot Nothing Then
                    mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            dsFill = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsFill.Tables(0)
                .DataBind()
            End With

            cboExpenseCategory_SelectedIndexChanged(cboExpenseCategory, New EventArgs())

            Dim objEmp As New clsEmployee_Master
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsFill = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsFill.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillData(ByVal isblank As Boolean)
        Dim objExpBalance As New clsEmployeeExpenseBalance
        Dim strSearch As String = ""
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch = "cmexpbalance_tran.employeeunkid  = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                If strSearch.Trim.Length > 0 Then strSearch &= " AND "
                strSearch &= mstrAdvanceFilter
            End If


            Dim dsList As DataSet = Nothing

            If isblank = False Then
                dsList = objExpBalance.GetEmployeeExpesneDataForAdjustment(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                            , Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString(), True _
                                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(cboExpense.SelectedValue), dtpDate.GetDate.Date, CDate(Session("fin_enddate")).Date _
                                                                                                            , True, True, strSearch)
            Else
                dsList = objExpBalance.GetEmployeeExpesneDataForAdjustment(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                            , Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString(), True _
                                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(cboExpense.SelectedValue), dtpDate.GetDate.Date, CDate(Session("fin_enddate")).Date _
                                                                                                            , True, True, strSearch).Clone()
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow()
                drRow("ischeck") = False
                drRow("crexpbalanceunkid") = 0
                drRow("employeeunkid") = 0
                drRow("Employee") = ""
                drRow("Job") = ""
                drRow("appointeddate") = DBNull.Value
                drRow("Accrue_amount") = 0.0
                drRow("Issue_amount") = 0.0
                drRow("Remaining_Bal") = 0.0
                drRow("Adjustment_Amt") = 0.0
                dsList.Tables(0).Rows.InsertAt(drRow, 0)
                isblank = True
            End If


            dgEmployee.DataSource = dsList.Tables(0)
            dgEmployee.DataBind()

            If isblank = True Then dgEmployee.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function UpdateRow(ByVal drRow As GridViewRow) As Boolean
        Try
            If drRow IsNot Nothing Then
                drRow.Cells(GetColumnIndex.getColumnID_Griview(dgEmployee, "dgcolhAdjustmentAmt", False, True)).Text = IIf(txtAdjustmentAmount.Text.Trim.Length <= 0, "0.00", txtAdjustmentAmount.Text.Trim).ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "Dropdown Events"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Dim objExpense As New clsExpense_Master
        Try
            Dim dsList As DataSet = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", 0, False, 0, "", "")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "isaccrue=1", "", DataViewRowState.CurrentRows).ToTable()

            Dim dRow As DataRow = dtTable.NewRow
            dRow("Id") = 0
            dRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Select")
            dRow("isaccrue") = False
            dtTable.Rows.InsertAt(dRow, 0)
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objExpense = Nothing
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Exit Sub
            ElseIf CInt(cboExpense.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Expense is compulsory information.Please Select Expense."), Me)
                cboExpense.Focus()
                Exit Sub
            End If
            Call FillData(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim drRow As DataRow() = Nothing

            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Exit Sub

            ElseIf CInt(cboExpense.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Expense is compulsory information.Please Select Expense."), Me)
                cboExpense.Focus()
                Exit Sub

            ElseIf txtRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Remark is compulsory information.Please Select Remark."), Me)
                txtRemark.Focus()
                Exit Sub
            End If


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee."), Me)
                Exit Sub
            End If


            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                Dim objExpAdjustment As New clsexpadjustment_Tran
                Dim mblnFlag As Boolean = False
                For Each dr As GridViewRow In gRow
                    objExpAdjustment._Crexpbalanceunkid = CInt(dgEmployee.DataKeys(dr.RowIndex)("crexpbalanceunkid"))
                    objExpAdjustment._Employeeunkid = CInt(dgEmployee.DataKeys(dr.RowIndex)("employeeunkid"))
                    objExpAdjustment._Expenseunkid = CInt(cboExpense.SelectedValue)
                    objExpAdjustment._Adjustment_Amt = CDec(IIf(txtAdjustmentAmount.Text.Trim.Length <= 0, "0.00", txtAdjustmentAmount.Text.Trim))
                    objExpAdjustment._Remarks = txtRemark.Text.Trim
                    objExpAdjustment._Userunkid = CInt(Session("UserId"))
                    objExpAdjustment._Isopenelc = CBool(dgEmployee.DataKeys(dr.RowIndex)("Isopenelc"))
                    objExpAdjustment._Iselc = CBool(dgEmployee.DataKeys(dr.RowIndex)("Iselc"))
                    objExpAdjustment._Isclose_Fy = CBool(dgEmployee.DataKeys(dr.RowIndex)("Isclose_Fy"))
                    objExpAdjustment._Isvoid = False
                    objExpAdjustment._WebFormName = mstrModuleName
                    objExpAdjustment._WebClientIP = Session("IP_ADD").ToString()
                    objExpAdjustment._WebHostName = Session("HOST_NAME").ToString()
                    objExpAdjustment._IsWeb = True

                    If objExpAdjustment.Insert() = False Then
                        mblnFlag = False
                        DisplayMessage.DisplayMessage(objExpAdjustment._Message, Me)
                        Exit Sub
                    End If
                    mblnFlag = True
                Next

                If mblnFlag Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Adjustment done successfully."), Me)
                    btnReset_Click(btnReset, New EventArgs())
                End If
                objExpAdjustment = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboExpenseCategory.SelectedValue = "0"
            cboExpense.SelectedValue = "0"
            cboEmployee.SelectedValue = "0"
            txtAdjustmentAmount.Text = "0.00"
            txtRemark.Text = ""
            dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            rdOption.SelectedValue = "1"
            mstrAdvanceFilter = ""
            FillData(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try
            If dgEmployee.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "There is no data to set adjustment amount."), Me)
                Exit Sub
            Else
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                If rdOption.SelectedValue = "1" Then
                    gRow = dgEmployee.Rows.Cast(Of GridViewRow)()

                ElseIf rdOption.SelectedValue = "2" Then
                    gRow = dgEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True)
                    If gRow Is Nothing OrElse gRow.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee."), Me)
                        Exit Sub
                    End If
                End If
                gRow.ToList().ForEach(Function(x) UpdateRow(x))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview Events"

    Protected Sub dgEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgEmployee.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If IsDBNull(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAppointmentDt", False, True)).Text) = False AndAlso e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAppointmentDt", False, True)).Text <> "&nbsp;" Then
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAppointmentDt", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAppointmentDt", False, True)).Text).ToShortDateString()
            End If

            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)

            If objExpense._Uomunkid = enExpUoM.UOM_QTY Then
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAccrueAmt", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAccrueAmt", False, True)).Text), "#00.00")
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhIssueAmt", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhIssueAmt", False, True)).Text), "#00.00")
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhRemaining_Bal", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhRemaining_Bal", False, True)).Text), "#00.00")
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhAdjustmentAmt", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhAdjustmentAmt", False, True)).Text), "#00.00")
            ElseIf objExpense._Uomunkid = enExpUoM.UOM_AMOUNT Then
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAccrueAmt", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgColhAccrueAmt", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhIssueAmt", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhIssueAmt", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhRemaining_Bal", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhRemaining_Bal", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhAdjustmentAmt", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgEmployee, "dgcolhAdjustmentAmt", False, True)).Text), Session("fmtcurrency").ToString())
            End If
            objExpense = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.LblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblExpense.ID, Me.LblExpense.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblDate.ID, Me.LblDate.Text)
            Me.lblAdjustmentAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAdjustmentAmount.ID, Me.lblAdjustmentAmount.Text)
            Me.rdOption.Items(0).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"rdApplytoall", Me.rdOption.Items(0).Text)
            Me.rdOption.Items(1).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"rdApplytoChecked", Me.rdOption.Items(1).Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(1).FooterText, Me.dgEmployee.Columns(1).HeaderText)
            Me.dgEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(2).FooterText, Me.dgEmployee.Columns(2).HeaderText)
            Me.dgEmployee.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(3).FooterText, Me.dgEmployee.Columns(3).HeaderText)
            Me.dgEmployee.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(4).FooterText, Me.dgEmployee.Columns(4).HeaderText)
            Me.dgEmployee.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(5).FooterText, Me.dgEmployee.Columns(5).HeaderText)
            Me.dgEmployee.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(6).FooterText, Me.dgEmployee.Columns(6).HeaderText)
            Me.dgEmployee.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgEmployee.Columns(5).FooterText, Me.dgEmployee.Columns(7).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
  
  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Expense is compulsory information.Please Select Expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Remark is compulsory information.Please Select Remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "Adjustment done successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "There is no data to set adjustment amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "Select")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
