﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ExpenseApproval.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_ExpenseApproval" Title="Expense Approval Information" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<%@ Register Src="~/Controls/ViewEmployeeAllocation.ascx" TagName="ViewEmployeeAllocation"
    TagPrefix="ViewEmployeeAllocation" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Expense Approval Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                 <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkViewEmployeeAllocation" runat="server" ToolTip="View Employee Allocation">
                                                           <i class="fas fa-street-view"></i>
                                            </asp:LinkButton>
                                         </li>   
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" />
                                                        </div>
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label" Visible="false"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblName" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" Enabled="False"
                                                                EnableTheming="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" Enabled="False" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblValue" runat="server" Text="Leave Form" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="true" Enabled="False" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                    ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblUoM" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblSector" runat="server" Text="Sector/Route" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                                    onKeypress="return onlyNumbers(this,event);" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                        <asp:Label ID="lblCosting" runat="server" Text="Costing" CssClass="form-label" Visible="false"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="1.00"
                                                                    onKeypress="return onlyNumbers(this,event);" CssClass="form-control" />
                                                                <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                                    Enabled="false" CssClass="form-control" Visible="false" />
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <asp:Panel ID="pnlBalAsonDate" runat="server">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-l--15">
                                                                <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtBalanceAsOnDate" runat="server" Enabled="false" Style="text-align: right"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-l--15">
                                                                <asp:Label ID="lblBalance" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" Enabled="false"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                            <li role="presentation" class="active"><a href="#tbpExpenseRemark" data-toggle="tab">
                                                                <asp:Label ID="tbExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label" />
                                                            </a></li>
                                                            <li role="presentation"><a href="#tbpClaimRemark" data-toggle="tab">
                                                                <asp:Label ID="tbClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label" />
                                                            </a></li>
                                                        </ul>
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane fade in active" id="tbpExpenseRemark">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade" id="tbpClaimRemark">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtClaimRemark" runat="server" Width="98%" TextMode="MultiLine"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-primary" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                ShowFooter="False" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit">
                                                                                  <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                    CssClass="griddelete">
                                                                                <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                                CommandName="Preview">
                                                                            <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                        FooterText="dgcolhExpense" />
                                                    <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" ReadOnly="true" FooterText="dgcolhSectorRoute" />
                                                    <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                                                    <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" ReadOnly="true"
                                                        Visible="true" FooterText="dgcolhExpenseRemark" />
                                                    <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrandTotal" Style="margin-right: 5px" runat="server" Text="Grand Total"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtGrandTotal" runat="server" Style="text-align: right" Enabled="False"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnViewAttchment" runat="server" Visible="false" Text="View Scan/Attchment"
                                    CssClass="btn btn-default" />
                                 <%--'Pinkal (20-Nov-2020) -- Start
                                        'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.     --%>
                                        <asp:LinkButton ID="lnkShowFuelConsumptionReport" runat="server" Visible="false" Text="Show Monthly Fuel Consumption Report" CssClass="lnkhover" style="margin-right:47%" />    
                                 <%--'Pinkal (20-Nov-2020) -- End--%>    
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary" />
                                <asp:Button ID="btnOK" runat="server" Text="Ok" CssClass="btn btn-default" Visible="false" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupRejectRemark" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnRemarkClose" PopupControlID="pnlReject" TargetControlID="txtRejectRemark">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlReject" runat="server" CssClass="card modal-dialog" Style="display: none;"
                    DefaultButton="btnRemarkOk">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Rejection Remark"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtRejectRemark" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnRemarkOk" runat="server" Text="Ok" CssClass="btn btn-primary" />
                        <asp:Button ID="btnRemarkClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To delete?:" />
                <uc3:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" Message="" />
                <uc3:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Message="" Title="Confirmation" />
                <ViewEmployeeAllocation:ViewEmployeeAllocation ID="ViewEmployeeAllocation" runat="server" />
                <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
