﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
#End Region
Partial Class Claims_And_Expenses_wPg_ApprovalMigration
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objExAssessorMaster As New clsExpenseApprover_Master
    'Private objExAssessorTran As New clsExpenseApprover_Tran
    'Pinkal (05-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmExpApprMigration"
    Dim dtAssignedEmp As DataTable = Nothing
    Dim dtOldEmp As DataTable = Nothing
    Dim dtNewEmp As DataTable = Nothing
    Dim mintOldApproverId As Integer = 0
    Dim mintNewApproverId As Integer = 0
#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack = False Then
                SetLanguage()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End
                FillCombo()
            Else
                dtAssignedEmp = CType(Me.ViewState("dtAssignedEmp"), DataTable)
                dtOldEmp = CType(Me.ViewState("dtOldEmp"), DataTable)
                dtNewEmp = CType(Me.ViewState("dtNewEmp"), DataTable)
                mintOldApproverId = CInt(Me.ViewState("OldApproverId"))
                mintNewApproverId = CInt(Me.ViewState("NewApproverId"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dtAssignedEmp") = dtAssignedEmp
            Me.ViewState("dtOldEmp") = dtOldEmp
            Me.ViewState("dtNewEmp") = dtNewEmp
            Me.ViewState("OldApproverId") = mintOldApproverId
            Me.ViewState("NewApproverId") = mintNewApproverId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            Dim dtTable As DataTable = Nothing

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            cboExCategory.DataTextField = "Name"
            cboExCategory.DataValueField = "Id"
            cboExCategory.DataSource = dtTable
            cboExCategory.DataBind()
            Call cboExCategory_SelectedIndexChanged(Nothing, Nothing)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub Fill_Assigneed_Employee()
        Try
            'Dim dtAssignedEmp As DataView = CType(ViewState("dtAssignedEmp"), DataTable).DefaultView
            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'If txtNewApproverAssignEmp.Text.Trim.Length > 0 Then
            '    dtAssignedEmp.RowFilter = "ecode like '%" & txtNewApproverAssignEmp.Text.Trim & "%'  OR ename like '%" & txtNewApproverAssignEmp.Text.Trim & "%' "
            'End If
            dgNewApproverAssignEmp.AutoGenerateColumns = False
            dgNewApproverAssignEmp.DataSource = dtAssignedEmp
            dgNewApproverAssignEmp.DataBind()

            'Gajanan [23-May-2020] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Assigneed_Employee:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_Old_Appr_Employee()
        Try
            'Dim dtOldEmp As DataView = CType(ViewState("dtOldEmp"), DataTable).DefaultView

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'If txtOldSearchEmployee.Text.Trim.Length > 0 Then
            '    dtOldEmp.RowFilter = "ecode like '%" & txtOldSearchEmployee.Text.Trim & "%'  OR ename like '%" & txtOldSearchEmployee.Text.Trim & "%' "
            'End If
            dgOldApproverEmp.AutoGenerateColumns = False
            dgOldApproverEmp.DataSource = dtOldEmp
            dgOldApproverEmp.DataBind()

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If dgNewApproverMigratedEmp.Rows.Count > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Gajanan [23-May-2020] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Old_Appr_Employee:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Fill_Migrated_Employee()
        Try
            'Dim dtNewEmp As DataView = CType(ViewState("dtNewEmp"), DataTable).DefaultView

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

            'If txtNewApproverMigratedEmp.Text.Trim.Length > 0 Then
            '    dtNewEmp.RowFilter = "ecode like '%" & txtNewApproverMigratedEmp.Text.Trim & "%'  OR ename like '%" & txtNewApproverMigratedEmp.Text.Trim & "%' "
            'End If
            dgNewApproverMigratedEmp.AutoGenerateColumns = False
            dgNewApproverMigratedEmp.DataSource = dtNewEmp
            dgNewApproverMigratedEmp.DataBind()

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If dgNewApproverMigratedEmp.Rows.Count > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Gajanan [23-May-2020] -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Migrated_Employee:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.

    'Private Sub GetOldApproverEmpPageRecordNo()
    '    Try
    '        If dgOldApproverEmp.PageIndex > 0 Then
    '            Me.ViewState("OldFirstRecordNo") = (((dgOldApproverEmp.PageIndex + 1) * dgOldApproverEmp.Rows.Count) - dgOldApproverEmp.Rows.Count)
    '        Else
    '            If dgOldApproverEmp.Rows.Count < dgOldApproverEmp.PageSize Then
    '                Me.ViewState("OldFirstRecordNo") = ((1 * dgOldApproverEmp.Rows.Count) - dgOldApproverEmp.Rows.Count)
    '            Else
    '                Me.ViewState("OldFirstRecordNo") = ((1 * dgOldApproverEmp.PageSize) - dgOldApproverEmp.Rows.Count)
    '            End If

    '        End If
    '        Me.ViewState("OldLastRecordNo") = ((dgOldApproverEmp.PageIndex + 1) * dgOldApproverEmp.Rows.Count)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GetOldApproverEmpPageRecordNo:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Private Sub GetNewApproverEmpPageRecordNo()
    '    Try
    '        If dgNewApproverMigratedEmp.PageIndex > 0 Then
    '            Me.ViewState("NewFirstRecordNo") = (((dgNewApproverMigratedEmp.PageIndex + 1) * dgNewApproverMigratedEmp.Rows.Count) - dgNewApproverMigratedEmp.Rows.Count)
    '        Else
    '            If dgNewApproverMigratedEmp.Rows.Count < dgNewApproverMigratedEmp.PageSize Then
    '                Me.ViewState("NewFirstRecordNo") = ((1 * dgNewApproverMigratedEmp.Rows.Count) - dgNewApproverMigratedEmp.Rows.Count)
    '            Else
    '                Me.ViewState("NewFirstRecordNo") = ((1 * dgNewApproverMigratedEmp.PageSize) - dgNewApproverMigratedEmp.Rows.Count)
    '            End If

    '        End If
    '        Me.ViewState("NewLastRecordNo") = ((dgNewApproverMigratedEmp.PageIndex + 1) * dgNewApproverMigratedEmp.Rows.Count)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GetNewApproverEmpPageRecordNo:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Pinkal (06-Jan-2016) -- End

#End Region

#Region "Button Event"

    Protected Sub objbtnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click

        'Gajanan [23-May-2020] -- Start
        'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.



        'Try
        '    Dim dtOldEmp As DataTable = CType(ViewState("dtOldEmp"), DataTable)
        '    Dim dtAssignedEmp As DataTable = CType(ViewState("dtAssignedEmp"), DataTable)
        '    Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
        '    If dtOldEmp IsNot Nothing Then
        '        dtOldEmp.AcceptChanges() : Dim drRow() As DataRow = dtOldEmp.Select("ischeck = true")
        '        If drRow.Length <= 0 Then
        '            'Language.setLanguage(mstrModuleName)
        '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one employee to migrate."), Me)
        '            Exit Sub
        '        End If

        '        If CInt(cboNewApprover.SelectedValue) <= 0 Then
        '            'Language.setLanguage(mstrModuleName)
        '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
        '            cboNewApprover.Focus()
        '            Exit Sub
        '        ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
        '            'Language.setLanguage(mstrModuleName)
        '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
        '            cboNewLevel.Focus()
        '            Exit Sub
        '        End If
        '        Dim blnFlag As Boolean = False
        '        Dim blnPendingFlag As Boolean = False
        '        If dgOldApproverEmp.Rows.Count <= 0 Then Exit Sub
        '        If drRow.Length > 0 Then
        '            For i As Integer = 0 To drRow.Length - 1
        '                If dtAssignedEmp IsNot Nothing AndAlso dtAssignedEmp.Rows.Count > 0 Then
        '                    Dim dRow() As DataRow = dtAssignedEmp.Select("employeeunkid =" & drRow(i)("employeeunkid").ToString())
        '                    If dRow.Length > 0 Then
        '                        blnFlag = True : drRow(i)("ischeck") = False : Continue For
        '                    End If
        '                End If

        '                drRow(i)("ischeck") = False

        '                'Pinkal (19-Mar-2015) -- Start
        '                'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.


        '                'Pinkal (22-Jun-2017) -- Start
        '                'Enhancement -Bug Solved For Claim Approver Migration .
        '                'If CInt(drRow(i)("employeeunkid")) = CInt(cboNewApprover.SelectedValue) Then Continue For
        '                If CInt(drRow(i)("employeeunkid")) = CInt(Me.ViewState("NewApproverId")) Then Continue For
        '                'Pinkal (22-Jun-2017) -- End


        '                'Pinkal (19-Mar-2015) -- End

        '                If dtNewEmp Is Nothing Then
        '                    dtNewEmp = dtOldEmp.Clone
        '                    dtNewEmp.ImportRow(drRow(i))
        '                    'dgNewView = dtNewEmp.DefaultView
        '                Else
        '                    'Pinkal (22-Jun-2017) -- Start
        '                    'Enhancement -Bug Solved For Claim Approver Migration .
        '                    Dim dr As DataRow() = dtNewEmp.Select("employeeunkid = " & CInt(drRow(i)("employeeunkid")))
        '                    If dr.Length <= 0 Then
        '                        dtNewEmp.ImportRow(drRow(i))
        '                    End If
        '                    'Pinkal (22-Jun-2017) -- End
        '                End If
        '                dtOldEmp.Rows.Remove(drRow(i))
        '            Next
        '            dtOldEmp.AcceptChanges()
        '            'ChkOldAll.Checked = False

        '            If blnFlag Then
        '                'Language.setLanguage(mstrModuleName)
        '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, Some of the checked employee is already binded with the selected approver."), Me)
        '            End If
        '        End If
        '        ViewState("dtOldEmp") = dtOldEmp
        '        ViewState("dtAssignedEmp") = dtAssignedEmp
        '        ViewState("dtNewEmp") = dtNewEmp
        '        Fill_Old_Appr_Employee()
        '        Fill_Migrated_Employee()
        '    End If



        Try
            'Dim dtOldEmp As DataTable = CType(ViewState("dtOldEmp"), DataTable)
            'Dim dtAssignedEmp As DataTable = CType(ViewState("dtAssignedEmp"), DataTable)
            'Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
            If dtOldEmp IsNot Nothing Then

                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = dgOldApproverEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvOldSelect"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one employee to migrate."), Me)
                    Exit Sub
                End If

                If CInt(cboNewApprover.SelectedValue) <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
                    cboNewApprover.Focus()
                    Exit Sub
                ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                    cboNewLevel.Focus()
                    Exit Sub
                End If

                Dim blnFlag As Boolean = False
                Dim blnPendingFlag As Boolean = False
                If dgOldApproverEmp.Rows.Count <= 0 Then Exit Sub

                For Each dgrow As GridViewRow In gRow
                    Dim chk As CheckBox = CType(dgrow.FindControl("ChkgvOldSelect"), CheckBox)
                    'Dim hfempid As HiddenField = CType(dgrow.FindControl("hfEmployeeid"), HiddenField)
                    Dim xEmpId As Integer = CInt(dgOldApproverEmp.DataKeys(dgrow.DataItemIndex)("employeeunkid"))

                    'If chk.Checked = False Then
                    '    Continue For
                    'End If

                    If dtAssignedEmp IsNot Nothing AndAlso dtAssignedEmp.Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtAssignedEmp.Select("employeeunkid =" & xEmpId)
                        If dRow.Length > 0 Then
                            blnFlag = True
                            'chk.Checked = False
                            Continue For
                        End If
                    End If

                    chk.Checked = False

                    If xEmpId = CInt(cboNewApprover.SelectedValue) Then Continue For
                    Dim drnew As DataRow = dtOldEmp.Select("employeeunkid= " & xEmpId).SingleOrDefault()

                    If dtNewEmp Is Nothing Then
                        dtNewEmp = dtOldEmp.Clone
                        dtNewEmp.ImportRow(drnew)
                    Else
                        Dim dr As DataRow() = dtNewEmp.Select("employeeunkid = " & xEmpId)
                        If dr.Length <= 0 Then
                            dtNewEmp.ImportRow(drnew)
                        End If
                    End If
                    dtOldEmp.Rows.Remove(drnew)
                Next
                dtOldEmp.AcceptChanges()

                If blnFlag Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, Some of the checked employee is already binded with the selected approver."), Me)
                End If

                'ViewState("dtOldEmp") = dtOldEmp
                'ViewState("dtAssignedEmp") = dtAssignedEmp
                'ViewState("dtNewEmp") = dtNewEmp

                Fill_Old_Appr_Employee()
                Fill_Migrated_Employee()


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If gRow IsNot Nothing Then gRow.ToList().Clear()
                gRow = Nothing
                'Pinkal (05-Sep-2020) -- End


            End If
            'Gajanan [23-May-2020] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub objbtnUnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try
            'Dim dtOldEmp As DataTable = CType(ViewState("dtOldEmp"), DataTable)
            'Dim dtAssignedEmp As DataTable = CType(ViewState("dtAssignedEmp"), DataTable)
            'Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
            'If dtNewEmp IsNot Nothing Then
            '    dtNewEmp.AcceptChanges() : Dim drRow() As DataRow = dtNewEmp.Select("ischeck = true")
            '    If drRow.Length <= 0 Then
            '        'Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Please check atleast one employee to unassign."), Me)
            '        Exit Sub
            '    End If
            '    If dgNewApproverMigratedEmp.Rows.Count <= 0 Then Exit Sub
            '    If drRow.Length > 0 Then
            '        For i As Integer = 0 To drRow.Length - 1
            '            drRow(i)("ischeck") = False
            '            If dtOldEmp Is Nothing Then
            '                dtOldEmp = dtNewEmp.Clone
            '                dtOldEmp.ImportRow(drRow(i))
            '            Else
            '                dtOldEmp.ImportRow(drRow(i))
            '            End If
            '            dtNewEmp.Rows.Remove(drRow(i))
            '        Next
            '        ViewState("dtOldEmp") = dtOldEmp
            '        ViewState("dtAssignedEmp") = dtAssignedEmp
            '        ViewState("dtNewEmp") = dtNewEmp
            '        'dgOldView = dtOldEmp.DefaultView
            '        'objchkNewCheck.Checked = False
            '        Fill_Old_Appr_Employee()
            '        Fill_Migrated_Employee()
            '    End If
            'End If

            'Dim dtOldEmp As DataTable = CType(ViewState("dtOldEmp"), DataTable)
            'Dim dtAssignedEmp As DataTable = CType(ViewState("dtAssignedEmp"), DataTable)
            'Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
            If dtNewEmp IsNot Nothing Then

                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = dgNewApproverMigratedEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvNewSelect"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Please check atleast one employee to unassign."), Me)
                    Exit Sub
                End If

                If dgNewApproverMigratedEmp.Rows.Count <= 0 Then Exit Sub

                For Each dgrow As GridViewRow In gRow
                    Dim chk As CheckBox = CType(dgrow.FindControl("ChkgvNewSelect"), CheckBox)

                    'Dim hfempid As HiddenField = CType(dgrow.FindControl("hfEmployeeid"), HiddenField)
                    Dim xEmpId As Integer = CInt(dgNewApproverMigratedEmp.DataKeys(dgrow.DataItemIndex)("employeeunkid"))

                    If chk.Checked = False Then
                        Continue For
                    End If

                    chk.Checked = False
                    Dim drnew As DataRow = dtNewEmp.Select("employeeunkid= " & xEmpId).SingleOrDefault()

                    If dtOldEmp Is Nothing Then
                        dtOldEmp = dtNewEmp.Clone
                        dtOldEmp.ImportRow(drnew)
                    Else
                        dtOldEmp.ImportRow(drnew)
                    End If
                    dtNewEmp.Rows.Remove(drnew)
                Next

                'ViewState("dtOldEmp") = dtOldEmp
                'ViewState("dtAssignedEmp") = dtAssignedEmp
                'ViewState("dtNewEmp") = dtNewEmp
                Fill_Old_Appr_Employee()
                Fill_Migrated_Employee()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If gRow IsNot Nothing Then gRow.ToList().Clear()
                gRow = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExAssessorTran As New clsExpenseApprover_Tran
        'Pinkal (05-Sep-2020) -- End
        Try
            'Dim dtOldEmp As DataTable = CType(ViewState("dtOldEmp"), DataTable)
            'Dim dtAssignedEmp As DataTable = CType(ViewState("dtAssignedEmp"), DataTable)
            'Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Please Select From Approver to migrate employee(s)."), Me)
                cboOldApprover.Focus()
                Exit Sub
            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboOldLevel.Focus()
                Exit Sub
            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
                cboNewApprover.Focus()
                Exit Sub
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboNewLevel.Focus()
                Exit Sub
            ElseIf dgNewApproverMigratedEmp.Rows.Count = 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "There is no employee(s) for migration."), Me)
                dgNewApproverMigratedEmp.Focus()
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmExpApprMigration"
            StrModuleName2 = "mnuUtilitiesMain"
            StrModuleName3 = "mnuClaimsExpenses"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If objExAssessorTran.Perform_Migration(CInt(cboOldApprover.SelectedValue), CInt(cboNewApprover.SelectedValue), dtNewEmp, CInt(Session("UserId")), CInt(Me.ViewState("NewApproverId")), ConfigParameter._Object._CurrentDateAndTime) = False Then
            If objExAssessorTran.Perform_Migration(mintOldApproverId, mintNewApproverId, dtNewEmp, CInt(Session("UserId")), CInt(cboNewApprover.SelectedValue), ConfigParameter._Object._CurrentDateAndTime) = False Then
                'Pinkal (18-Jun-2020) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Problem in Approver Migration."), Me)
            Else
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Approver Migration done successfully."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Approver Migration done successfully."), Me, Session("rootpath").ToString() & "UserHome.aspx")
                'Gajanan [17-Sep-2020] -- End
                btnReset_Click(btnReset, New EventArgs())
            End If

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            chkShowInactiveApprovers.Checked = False
            chkShowInactiveApprovers_CheckedChanged(chkShowInactiveApprovers, New EventArgs())
            chkShowInActiveEmployees.Checked = False
            chkShowInActiveEmployees_CheckedChanged(chkShowInactiveApprovers, New EventArgs())
            chkShowInActiveEmployees.Enabled = True
            'Gajanan [23-May-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExAssessorTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboOldApprover.SelectedValue = "0"
            cboOldLevel.SelectedValue = "0"
            cboNewApprover.SelectedValue = "0"
            cboNewLevel.SelectedValue = "0"
            cboExCategory_SelectedIndexChanged(sender, e)
            cboOldApprover_SelectedIndexChanged(sender, e)
            cboOldLevel_SelectedIndexChanged(sender, e)
            cboNewApprover_SelectedIndexChanged(sender, e)
            cboNewLevel_SelectedIndexChanged(sender, e)

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'txtNewApproverAssignEmp.Text = String.Empty
            'txtNewApproverMigratedEmp.Text = String.Empty
            'txtOldSearchEmployee.Text = String.Empty

            'Gajanan [23-May-2020] -- end

            Me.ViewState("dtAssignedEmp") = Nothing
            Me.ViewState("dtOldEmp") = Nothing
            Me.ViewState("dtNewEmp") = Nothing
            Me.ViewState("OldFirstRecordNo") = Nothing
            Me.ViewState("OldLastRecordNo") = Nothing
            Me.ViewState("NewFirstRecordNo") = Nothing
            Me.ViewState("NewLastRecordNo") = Nothing
            dgNewApproverAssignEmp.DataSource = Nothing
            dgNewApproverAssignEmp.DataBind()
            dgNewApproverMigratedEmp.DataSource = Nothing
            dgNewApproverMigratedEmp.DataBind()
            dgOldApproverEmp.DataSource = Nothing
            dgOldApproverEmp.DataBind()

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
            'Gajanan [17-Sep-2020] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"
    'Protected Sub dgNewApproverAssignEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgNewApproverAssignEmp.PageIndexChanging
    '    Try
    '        'dgNewApproverAssignEmp.PageIndex = e.NewPageIndex
    '        'Fill_Assigneed_Employee()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub dgNewApproverMigratedEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgNewApproverMigratedEmp.PageIndexChanging
    '    Try
    '        'dgNewApproverMigratedEmp.PageIndex = e.NewPageIndex
    '        'Fill_Migrated_Employee()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub dgOldApproverEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgOldApproverEmp.PageIndexChanging
    '    Try
    '        'dgOldApproverEmp.PageIndex = e.NewPageIndex
    '        'Fill_Old_Appr_Employee()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
#End Region

#Region "CheckBox Event"

    'Protected Sub chkOldAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim dtOldEmp As DataTable = CType(ViewState("dtOldEmp"), DataTable)
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgOldApproverEmp.Rows.Count <= 0 Then Exit Sub

    '        If dgOldApproverEmp.Rows.Count > 0 Then
    '            For i As Integer = 0 To dgOldApproverEmp.Rows.Count - 1
    '                If dtOldEmp.Rows.Count - 1 < i Then Exit For
    '                Dim DtRow As DataRow() = dtOldEmp.Select("ecode ='" & dgOldApproverEmp.Rows(i).Cells(1).Text.Trim & "'") 'Nilay (01-Feb-2015) -- dgOldApproverEmp.Rows(mintRowIndex).Cells(1).Text.Trim
    '                If DtRow.Length > 0 Then
    '                    DtRow(0)("ischeck") = CBool(cb.Checked)
    '                    CType(dgOldApproverEmp.Rows(i).FindControl("ChkgvOldSelect"), CheckBox).Checked = cb.Checked 'Nilay (01-Feb-2015) -- CType(dgOldApproverEmp.Rows(mintRowIndex).FindControl("ChkgvOldSelect"), CheckBox).Checked = cb.Checked
    '                End If
    '                dtOldEmp.AcceptChanges()
    '            Next
    '            Session("dtOldEmp") = dtOldEmp
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkOldAll_CheckedChanged:-" & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvOldSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            Dim drRow() As DataRow = CType(ViewState("dtOldEmp"), DataTable).Select("ecode = '" & gvRow.Cells(1).Text & "'")
    '            If drRow.Length > 0 Then
    '                drRow(0)("ischeck") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvOldSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkNewAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
    '        Dim cb As CheckBox = CType(sender, CheckBox)

    '        If dgNewApproverMigratedEmp.Rows.Count <= 0 Then Exit Sub

    '        If dgNewApproverMigratedEmp.Rows.Count > 0 Then
    '            For i As Integer = 0 To dgNewApproverMigratedEmp.Rows.Count - 1
    '                If dtNewEmp.Rows.Count - 1 < i Then Exit For
    '                Dim DtRow As DataRow() = dtNewEmp.Select("ecode ='" & dgNewApproverMigratedEmp.Rows(i).Cells(1).Text.Trim & "'") 'Nilay (01-Feb-2015) -- dgNewApproverMigratedEmp.Rows(mintRowIndex).Cells(1).Text.Trim
    '                If DtRow.Length > 0 Then
    '                    DtRow(0)("ischeck") = CBool(cb.Checked)
    '                    CType(dgNewApproverMigratedEmp.Rows(i).FindControl("ChkgvNewSelect"), CheckBox).Checked = cb.Checked 'Nilay (01-Feb-2015) -- dgNewApproverMigratedEmp.Rows(mintRowIndex).FindControl("ChkgvNewSelect"), CheckBox
    '                End If
    '                dtNewEmp.AcceptChanges()
    '                'mintRowIndex += 1
    '            Next
    '            Session("dtNewEmp") = dtNewEmp
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkNewAll_CheckedChanged:-" & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvNewSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            Dim drRow() As DataRow = CType(ViewState("dtNewEmp"), DataTable).Select("ecode = '" & gvRow.Cells(1).Text & "'")
    '            If drRow.Length > 0 Then
    '                drRow(0)("ischeck") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvNewSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub


    'Gajanan [23-May-2020] -- Start
    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

    Protected Sub chkShowInactiveApprovers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInactiveApprovers.CheckedChanged
        Try
            chkShowInActiveEmployees.Enabled = True
            chkShowInActiveEmployees.Checked = False
            cboExCategory_SelectedIndexChanged(cboExCategory, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkShowInActiveEmployees_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployees.CheckedChanged
        Try
            cboOldLevel_SelectedIndexChanged(cboOldLevel, New EventArgs())
            cboNewLevel_SelectedIndexChanged(cboNewLevel, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Gajanan [23-May-2020] -- End
#End Region

#Region "ComboBox Event"

    Protected Sub cboExCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objExAssessorMaster As New clsExpenseApprover_Master
            'Pinkal (05-Sep-2020) -- End


            'dsList = objExAssessorMaster.getListForCombo(CInt(IIf(cboExCategory.SelectedValue = "", 0, cboExCategory.SelectedValue)), True, "List")
            dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), Session("Database_Name").ToString(), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         Session("EmployeeAsOnDate").ToString(), _
                                                         Session("UserAccessModeSetting").ToString(), True, _
                                                         "List", "", True, chkShowInactiveApprovers.Checked)
            'Gajanan [23-May-2020] -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

            'Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "id", "ApproverEmpunkid", "name")
            'With cboOldApprover
            '    .DataValueField = "id"
            '    .DataTextField = "Name"
            '    .DataSource = dtTable
            '    .DataBind()
            'End With

            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
            With cboOldApprover
                .DataValueField = "ApproverEmpunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
            End With

            'Pinkal (18-Jun-2020) -- End

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End


            Call cboOldApprover_SelectedIndexChanged(cboOldApprover, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboNewApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            'Dim dtAssignedEmp As New DataTable


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objExAssessorMaster As New clsExpenseApprover_Master
            'Pinkal (05-Sep-2020) -- End


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'Dim objApprover As New clsExpenseApprover_Master
            'objApprover._crApproverunkid = CInt(cboNewApprover.SelectedValue)

            dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), Session("Database_Name").ToString(), _
                                                      CInt(Session("UserId")), _
                                                      CInt(Session("Fin_year")), _
                                                      CInt(Session("CompanyUnkId")), _
                                                      Session("EmployeeAsOnDate").ToString(), _
                                                      Session("UserAccessModeSetting").ToString(), True, _
                                                      "List", "", True, chkShowInactiveApprovers.Checked)
            'Pinkal (18-Jun-2020) -- End

            

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), CInt(cboNewApprover.SelectedValue), objApprover._Employeeunkid, True, "List")
            Dim dtNewApprover As DataTable = New DataView(dsList.Tables(0), "ApproverEmpunkid = " & CInt(cboNewApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
            If dtNewApprover IsNot Nothing AndAlso dtNewApprover.Rows.Count > 0 Then
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboNewApprover.SelectedValue), CBool(dtNewApprover.Rows(0)("isexternalapprover")), True, "List")
            Else
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboNewApprover.SelectedValue), False, True, "List")
            End If
            'Pinkal (18-Jun-2020) -- End

            cboNewLevel.DataValueField = "Id"
            cboNewLevel.DataTextField = "Name"
            cboNewLevel.DataSource = dsList.Tables(0)
            cboNewLevel.DataBind()
            cboNewLevel.SelectedValue = "0"


            cboNewLevel_SelectedIndexChanged(cboNewLevel, Nothing)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtNewApprover IsNot Nothing Then dtNewApprover.Clear()
            dtNewApprover = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOldApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExAssessorMaster As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End

        Try
            Dim dsList As New DataSet

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List")
            dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), Session("Database_Name").ToString(), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         Session("EmployeeAsOnDate").ToString(), _
                                                         Session("UserAccessModeSetting").ToString(), True, _
                                                         "List", "", True, chkShowInactiveApprovers.Checked)
            'Gajanan [23-May-2020] -- End


            Dim dtTable As DataTable = Nothing


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

            'If CInt(cboOldApprover.SelectedValue) > 0 Then
            '    dtTable = New DataView(dsList.Tables(0), " id <> " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "id", "ApproverEmpunkid", "name")
            'Else
            '    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "id", "ApproverEmpunkid", "name")
            'End If

            If CInt(cboOldApprover.SelectedValue) > 0 Then
                dtTable = New DataView(dsList.Tables(0), " ApproverEmpunkid <> " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
            End If

            With cboNewApprover
                .DataValueField = "ApproverEmpunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            Call cboNewApprover_SelectedIndexChanged(cboNewApprover, Nothing)
            'Pinkal (18-Jun-2020) -- End


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'Dim objApprover As New clsExpenseApprover_Master
            'objApprover._crApproverunkid = CInt(cboOldApprover.SelectedValue)
            'dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), CInt(cboOldApprover.SelectedValue), objApprover._Employeeunkid, True, "List")
            Dim dtOldApprover As DataTable = New DataView(dsList.Tables(0), "ApproverEmpunkid = " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
            If dtOldApprover IsNot Nothing AndAlso dtOldApprover.Rows.Count > 0 Then
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboOldApprover.SelectedValue), CBool(dtOldApprover.Rows(0)("isexternalapprover")), True, "List")
            Else
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboOldApprover.SelectedValue), False, True, "List")
            End If
            'Pinkal (18-Jun-2020) -- End

            cboOldLevel.DataValueField = "Id"
            cboOldLevel.DataTextField = "Name"
            cboOldLevel.DataSource = dsList.Tables(0)
            cboOldLevel.DataBind()

            Call cboOldLevel_SelectedIndexChanged(cboOldLevel, Nothing)

            'Dim dtNewEmp As DataTable = CType(ViewState("dtNewEmp"), DataTable)
            'If dtNewEmp IsNot Nothing Then dtNewEmp.Rows.Clear() : ViewState("dtNewEmp") = dtNewEmp


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dtOldApprover IsNot Nothing Then dtOldApprover.Clear()
            dtOldApprover = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboOldLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExAssessorMaster As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try

            Dim dsList As DataSet = Nothing

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'Dim objApprover As New clsExpenseApprover_Master
            'objApprover._crApproverunkid = CInt(cboOldApprover.SelectedValue)
            'Pinkal (18-Jun-2020) -- End

            

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'dsList = objExAssessorMaster.GetApproverAccess(CInt(cboOldApprover.SelectedValue), objApprover._Employeeunkid, CInt(cboOldLevel.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
            dsList = objExAssessorMaster.GetApproverAccess(Session("Database_Name").ToString(), _
                                                            0, CInt(cboOldApprover.SelectedValue), _
                                                           CInt(cboOldLevel.SelectedValue), _
                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                           chkShowInActiveEmployees.Checked)

            'Gajanan [23-May-2020] -- End


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'objApprover = Nothing
            'Pinkal (18-Jun-2020) -- End


            'ViewState("dtOldEmp") = dsList.Tables(0)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dtOldEmp = dsList.Tables(0)
            dtOldEmp = dsList.Tables(0).Copy()
            'Pinkal (05-Sep-2020) -- End


            Call Fill_Old_Appr_Employee()


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    Me.ViewState("OldApproverId") = CInt(dsList.Tables(0).Rows(0)("approverempid"))
            'Else
            '    Me.ViewState("OldApproverId") = Nothing
            'End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintOldApproverId = CInt(dsList.Tables(0).Rows(0)("crapproverunkid"))
            Else
                mintOldApproverId = 0
            End If
            'Pinkal (18-Jun-2020) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboNewLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExAssessorMaster As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim dsList As DataSet = Nothing
            'Dim dtAssignedEmp As DataTable = Nothing


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'Dim objApprover As New clsExpenseApprover_Master
            'objApprover._crApproverunkid = CInt(cboNewApprover.SelectedValue)
            'Pinkal (18-Jun-2020) -- End

            


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'dsList = objExAssessorMaster.GetApproverAccess(CInt(cboNewApprover.SelectedValue), objApprover._Employeeunkid, CInt(cboNewLevel.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
            dsList = objExAssessorMaster.GetApproverAccess(Session("Database_Name").ToString(), _
                                                            0, CInt(cboNewApprover.SelectedValue), _
                                                            CInt(cboNewLevel.SelectedValue), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                            chkShowInActiveEmployees.Checked)

            'Gajanan [23-May-2020] -- End


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'objApprover = Nothing
            'Pinkal (18-Jun-2020) -- End


            'ViewState("dtAssignedEmp") = dsList.Tables(0)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dtAssignedEmp = dsList.Tables(0)
            dtAssignedEmp = dsList.Tables(0).Copy()
            'Pinkal (05-Sep-2020) -- End


            Call Fill_Assigneed_Employee()

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                dgNewApproverAssignEmp.DataSource = Nothing
                dgNewApproverMigratedEmp.DataSource = Nothing
            End If


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    Me.ViewState("NewApproverId") = CInt(dsList.Tables(0).Rows(0)("approverempid"))
            'Else
            '    Me.ViewState("NewApproverId") = Nothing
            'End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintNewApproverId = CInt(dsList.Tables(0).Rows(0)("crapproverunkid"))
            Else
                mintNewApproverId = 0
            End If
            'Pinkal (18-Jun-2020) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    'Protected Sub txtNewApproverAssignEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewApproverAssignEmp.TextChanged
    '    Try
    '        If ViewState("dtAssignedEmp") IsNot Nothing Then
    '            Dim dtAssignedEmp As DataView = CType(ViewState("dtAssignedEmp"), DataTable).DefaultView
    '            If dtAssignedEmp IsNot Nothing Then
    '                If dtAssignedEmp.Table.Rows.Count > 0 Then
    '                    dtAssignedEmp.RowFilter = "ecode like '%" & txtNewApproverAssignEmp.Text.Trim & "%'  OR ename like '%" & txtNewApproverAssignEmp.Text.Trim & "%' "
    '                    dgNewApproverAssignEmp.DataSource = dtAssignedEmp
    '                    dgNewApproverAssignEmp.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtNewApproverAssignEmp_TextChanged:-" & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub txtNewApproverMigratedEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewApproverMigratedEmp.TextChanged
    '    Try
    '        If ViewState("dtNewEmp") IsNot Nothing Then
    '            Dim dtNewEmp As DataView = CType(ViewState("dtNewEmp"), DataTable).DefaultView
    '            If dtNewEmp IsNot Nothing Then
    '                If dtNewEmp.Table.Rows.Count > 0 Then
    '                    dtNewEmp.RowFilter = "ecode like '%" & txtNewApproverMigratedEmp.Text.Trim & "%'  OR ename like '%" & txtNewApproverMigratedEmp.Text.Trim & "%' "
    '                    dgNewApproverMigratedEmp.DataSource = dtNewEmp
    '                    dgNewApproverMigratedEmp.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtNewApproverAssignEmp_TextChanged:-" & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub txtOldSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOldSearchEmployee.TextChanged
    '    Try
    '        If ViewState("dtOldEmp") IsNot Nothing Then
    '            Dim dtOldEmp As DataView = CType(ViewState("dtOldEmp"), DataTable).DefaultView
    '            If dtOldEmp IsNot Nothing Then
    '                If dtOldEmp.Table.Rows.Count > 0 Then
    '                    dtOldEmp.RowFilter = "ecode like '%" & txtOldSearchEmployee.Text.Trim & "%'  OR ename like '%" & txtOldSearchEmployee.Text.Trim & "%' "
    '                    dgOldApproverEmp.DataSource = dtOldEmp
    '                    dgOldApproverEmp.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtNewApproverAssignEmp_TextChanged:-" & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblDetialHeader.Text)

            Me.lblExpenseCat.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblExpenseCat.ID, Me.lblExpenseCat.Text)
            Me.lblOldApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblOldApprover.ID, Me.lblOldApprover.Text)
            Me.lblOldLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblOldLevel.ID, Me.lblOldLevel.Text)

            Me.lblNewLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblNewLevel.ID, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblNewApprover.ID, Me.lblNewApprover.Text)

            Me.dgOldApproverEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgOldApproverEmp.Columns(1).FooterText, Me.dgOldApproverEmp.Columns(1).HeaderText)
            Me.dgOldApproverEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgOldApproverEmp.Columns(2).FooterText, Me.dgOldApproverEmp.Columns(2).HeaderText)

            Me.dgNewApproverMigratedEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgNewApproverMigratedEmp.Columns(1).FooterText, Me.dgNewApproverMigratedEmp.Columns(1).HeaderText)
            Me.dgNewApproverMigratedEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgNewApproverMigratedEmp.Columns(2).FooterText, Me.dgNewApproverMigratedEmp.Columns(2).HeaderText)

            Me.dgNewApproverAssignEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgNewApproverAssignEmp.Columns(0).FooterText, Me.dgNewApproverAssignEmp.Columns(0).HeaderText)
            Me.dgNewApproverAssignEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgNewApproverAssignEmp.Columns(1).FooterText, Me.dgNewApproverAssignEmp.Columns(1).HeaderText)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            Me.tbMigrationEmp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbMigrationEmp.ID, Me.tbMigrationEmp.Text)
            Me.tbAssignedEmp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbAssignedEmp.ID, Me.tbAssignedEmp.Text)
            'Gajanan [17-Sep-2020] -- End



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnReset.Text).Replace("&", "")
            'Pinkal (06-Jan-2016) -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


End Class
