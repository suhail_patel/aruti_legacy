﻿<%@ Page Title="Global Approve Expense" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_GlobalApproveExpense.aspx.vb" Inherits="Claims_And_Expenses_wPg_GlobalApproveExpense" %>

<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function setscrollPosition(sender) {
            $(".101010").scrollLeft(document.getElementById("<%=hdf_leftposition.ClientID%>").value);
            $(".101010").scrollTop(document.getElementById("<%=hdf_topposition.ClientID%>").value)

        }
        function getscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = $(".101010").scrollTop();
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = $(".101010").scrollLeft();
            document.getElementById("<%=hdf_pagetop.ClientID%>").value = $(window).scrollTop();
        }
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);

        function endRequestHandler(sender, args) {
            $(window).scrollTop(document.getElementById("<%=hdf_pagetop.ClientID%>").value);
        }

        function selectall(chk) {
            $("[id*='chkSelect']").prop('checked', chk.checked);
        }

        function selectallgroupchild(chk) {
            if ($(chk).closest('tr').next('tr').children('td').length > $(chk).closest('tr').children('td').length) {
                selectnextrow($(chk).closest('tr').next('tr'), chk);
            }
        }

        function selectnextrow(obj, chk) {
            $(obj).find("[id*='chkSelect']").prop('checked', chk.checked);
            if ($(obj).next('tr').children('td').length == $(obj).children('td').length) {
                selectnextrow($(obj.next('tr')), chk);
            }
        }
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <asp:Label ID="lblPageHeader" runat="server" Text="Global Approve Expense"></asp:Label>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbFilter" runat="server" Text="Filter Criteria"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r-30 p-l-0">
                                                        <asp:LinkButton ID="lnkAllocation" Font-Underline="false" runat="server" ToolTip = "Allocation">
                                                                     <i class="fas fa-sliders-h"></i>
                                                        </asp:LinkButton>
                                                </ul>
                                                <ul class="header-dropdown m-r--14  p-l-0">
                                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" ToolTip = "Reset">
                                                                               <i class="fas fa-sync-alt"></i>
                                                        </asp:LinkButton>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblExpenseCategory" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblSector" runat="server" Text="Sector/Route" Visible="false" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="false" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbInfo" runat="server" Text="Information"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApprover" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" Rows="2" TextMode="MultiLine" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <asp:Label ID="gbPending" Text="Pending Claim Expense Application(s)." runat="server"></asp:Label>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 450px">
                                            <asp:UpdatePanel ID="DgUpdatePanel" ChildrenAsTriggers="true" runat="server">
                                                <ContentTemplate>
                                                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                        CssClass="table table-hover table-bordered" Width = "185%">
                                                        <ItemStyle CssClass="griviewitem" />
                                                        <Columns>
                                                            <asp:TemplateColumn ItemStyle-Width="25" >
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" Enabled="true" onclick="selectall(this)" CssClass="filled-in" Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" Enabled="true" onclick="selectallgroupchild(this)" CssClass="filled-in" Text=" "  />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="ClaimNo" HeaderText="Claim No" FooterText="dgcolhClaimNo" ItemStyle-Wrap ="false" />
                                                            <asp:BoundColumn DataField="Expense" HeaderText="Claim/Expense Desc" FooterText="dgcolhClaimExpense" ItemStyle-Wrap ="false"  HeaderStyle-Wrap="false" />
                                                            <asp:BoundColumn DataField="Sector" HeaderText="Sector/Route" FooterText="dgcolhSectorRoute"  ItemStyle-Wrap ="false" />
                                                            <asp:BoundColumn DataField="UOM" HeaderText="UOM" FooterText="dgcolhUOM"  ItemStyle-Wrap ="false"/>
                                                            <asp:BoundColumn DataField="Balance" HeaderText="Balance" FooterText="dgcolhBalance" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" ItemStyle-Wrap ="false" />
                                                            <asp:TemplateColumn HeaderText="Cost Center" FooterText="dgcolhCostCenter" ItemStyle-Wrap ="false">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="dgcolhCostCenter" runat="server" Width ="200px">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hdcolhCostCenter" runat="server" Value='<%# Eval("costcenterunkid") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="AppliedQty" HeaderText="Applied Quantity" FooterText="dgcolhAppliedQty"  ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap = "false"  ItemStyle-Width = "300px" />
                                                            <asp:BoundColumn DataField="AppliedAmount" HeaderText="Applied Amount" FooterText="dgcolhAppliedAmount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap = "false" ItemStyle-Width = "300px" />
                                                            <asp:TemplateColumn HeaderText="Currency" FooterText="dgcolhCurrency" >
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="dgcolhCurrency" runat="server"  Width ="150px">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hdcolhCurrency" runat="server" Value='<%# Eval("countryunkid") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="Claim_Remark" HeaderText="Claim Remark" FooterText="dgcolhClaimRemark"   ItemStyle-Wrap="true" ItemStyle-VerticalAlign="Top" HeaderStyle-Wrap = "false" />
                                                            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhQuantity" >
                                                                <ItemTemplate>
                                                                <div class="form-group">
                                                                        <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhQuantity" Style="text-align: right" runat="server" AutoPostBack="True" Width = "150px"
                                                                        Text='<%# Eval("quantity") %>' OnTextChanged="txtdgcolhQuantity_TextChanged"  class="form-control"
                                                                        onblur="getscrollPosition()"></asp:TextBox>
                                                                        </div>
                                                                   </div>         
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                <ItemStyle Width="150px" />
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Right"  FooterText="dgcolhUnitPrice" >
                                                                <ItemTemplate>
                                                                     <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtdgcolhUnitPrice" Style="text-align: right" runat="server" AutoPostBack="True" Width = "150px"
                                                                                Text='<%# Eval("unitprice") %>' OnTextChanged="txtdgcolhUnitPrice_TextChanged" class="form-control"
                                                                                onblur="getscrollPosition()"></asp:TextBox>
                                                                            </div>
                                                                       </div>         
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:TemplateColumn HeaderText = "Expense Remark" ItemStyle-Wrap="false"
                                                                FooterText="dgcolhExpenseRemark" >
                                                                <ItemTemplate>
                                                                <div class="form-group">
                                                                        <div class="form-line">
                                                                                <asp:TextBox ID="txtdgcolhExpenseRemark" runat="server" Text='<%# Eval("Expense_Remark") %>' class="form-control" Width= "250px"
                                                                                    onblur="getscrollPosition()"></asp:TextBox>
                                                                         </div>
                                                                   </div>                 
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="IsGrp" HeaderText="IsGrp" FooterText="objdgcolhIsGrp"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GrpId" HeaderText="Group Id" FooterText="objdgcolhGrpId"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="Employee ID" FooterText="objdgcolhEmployeeunkid"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="isleaveencashment" HeaderText="Leave Encashment" FooterText="objdgcolhIsLeaveEncashment"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="isaccrue" HeaderText="Is Accrue" FooterText="objdgcolhIsAccrue"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="expenseunkid" HeaderText="ExpenseID" FooterText="objdgcolhExpenseID"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="CrMasterID" FooterText="objdgcolhCrmasterunkid"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crapprovaltranunkid" HeaderText="CrApprovalTranunkid"
                                                                FooterText="objdgcolhCrapprovaltranunkid" Visible="false" />
                                                            <asp:BoundColumn DataField="uomunkid" HeaderText="UOMID" FooterText="objdgcolhUOMID"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="CLno" HeaderText="CLno" FooterText="objdgcolhCLno" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdf_topposition" runat="server" />
                <asp:HiddenField ID="hdf_leftposition" runat="server" />
                <asp:HiddenField ID="hdf_pagetop" runat="server" />
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc3:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" Message="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
