<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ApprovalMigration.aspx.vb" Inherits="Claims_And_Expenses_wPg_ApprovalMigration"
    Title="Expense Approval Migration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
    </script>

    <script type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(ctrl) {
        
            var txtSearch = $(ctrl);
            
            switch(txtSearch[0].id) {
              case 'txtOldSearchEmployee':
              if ($(txtSearch).val().length > 0) {
                    $('#<%=dgOldApproverEmp.ClientID %> tbody tr').hide();
                    $('#<%=dgOldApproverEmp.ClientID %> tbody tr:first').show();
                    $('#<%=dgOldApproverEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtOldSearchEmployee').val() + '\')').parent().show();
               }
               
               else if ($(txtSearch).val().length == 0) {
                    $('#dgOldApproverEmp').val('');
                    $('#<%=dgOldApproverEmp.ClientID %> tr').show();
                    $('#dgOldApproverEmp').focus();
              }
              break;
              
              case 'txtNewApproverMigratedEmp':
                if ($(txtSearch).val().length > 0) {
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tbody tr').hide();
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tbody tr:first').show();
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtNewApproverMigratedEmp').val() + '\')').parent().show();
               }
               
               else if ($(txtSearch).val().length == 0) {
                    $('#dgNewApproverMigratedEmp').val('');
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tr').show();
                    $('#dgNewApproverMigratedEmp').focus();
              }
              break;
              
              case 'txtNewApproverAssignEmp':
                if ($(txtSearch).val().length > 0) {
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tbody tr').hide();
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tbody tr:first').show();
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtNewApproverAssignEmp').val() + '\')').parent().show();
               }
               
               else if ($(txtSearch).val().length == 0) {
                    $('#dgNewApproverAssignEmp').val('');
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tr').show();
                    $('#dgNewApproverAssignEmp').focus();
              }
              break;
              default:
                // code block
            } 
        }


           
//        function gridCheckBoxAll(ctrl)
//        {
//              var chkHeader = $(ctrl);
//              var grid = $(ctrl).closest("table");
//              $("input[type=checkbox]", grid).each(function() {
//                  if (chkHeader.is(":checked")) {
//                     if ($(this).is(":visible")) {
//                         $(this).prop("checked", "checked");
//                     }
//                  } 
//                  else {
//                      $(this).removeProp("checked");
//                 }
//             });
//        }  

//        function gridSelectCheckBox(ctrl,checkboxall,checkboxselect)
//        {
//            var grid = $(ctrl).closest("table");
//            var chkHeader = $("[id*="+checkboxall+"]", grid);
//            var row = $(ctrl).closest("tr")[0];

//            if (!$(ctrl).is(":checked")) {
//                chkHeader.removeProp("checked");
//                
//            } else {
//                if ($("[id*="+checkboxselect+"]", grid).length == $("[id*="+checkboxselect+"]:checked", grid).length) {
//                    chkHeader.prop("checked", "checked");
//                }
//            }
        //        }

        $("body").on("click", "[id*=ChkOldAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvOldSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkgvOldSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkOldAll]", grid);
            debugger;
            if ($("[id*=ChkgvOldSelect]", grid).length == $("[id*=ChkgvOldSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        $("body").on("click", "[id*=ChkNewAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvNewSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkgvNewSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkNewAll]", grid);
            debugger;
            if ($("[id*=ChkgvNewSelect]", grid).length == $("[id*=ChkgvNewSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
        
    </script>

        <asp:Panel ID="pnlmain" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                
                 <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approver Information"></asp:Label>
                    </h2>
                </div>
                 <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                              <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExpenseCat" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                         <div class="form-group">
                                                <asp:DropDownList ID="cboExCategory" runat="server" AutoPostBack="true"/>
                                        </div>
                                    </div>
                              </div>
                               <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowInactiveApprovers" runat="server" AutoPostBack="true" Text="Include Inactive Approver" />
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-12 col-xs-12">
                                             <asp:CheckBox ID="chkShowInActiveEmployees" runat="server" AutoPostBack="true" Text="Include Inactive Employee" />
                                    </div>
                               </div>
                               <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                            <asp:Label ID="lblOldApprover" runat="server" Text="From Approver"  CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                     <asp:DropDownList ID="cboOldApprover" runat="server" AutoPostBack="true"/>
                                            </div>
                                     </div> 
                                      <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                             <asp:Label ID="lblNewApprover" runat="server" Text="To Approver" CssClass="form-label"></asp:Label>
                                             <div class="form-group">
                                                        <asp:DropDownList ID="cboNewApprover" runat="server" AutoPostBack="true"/>
                                             </div>
                                      </div>
                               </div>
                                <div class="row clearfix">
                                         <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                                  <asp:Label ID="lblOldLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                   <div class="form-group">
                                                          <asp:DropDownList ID="cboOldLevel" runat="server" AutoPostBack="true"/>
                                                   </div>
                                         </div>
                                         <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                                  <asp:Label ID="lblNewLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                  <div class="form-group">
                                                        <asp:DropDownList ID="cboNewLevel" runat="server" AutoPostBack="true"/>
                                                  </div>
                                        </div>
                                </div>
                                <div class="row clearfix">
                                         <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <div class="form-line">
                                                     <input type="text" id="txtOldSearchEmployee" name="txtOldSearchEmployee" placeholder="type search text"  maxlength="50" onkeyup="FromSearching(this);" class = "form-control" />
                                                  </div>
                                               </div>   
                                         </div>
                                          <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                          </div>
                                </div>
                                <div class="row clearfix">
                                        <div class="col-xs-5 col-md-5 col-sm-5 col-xs-5">
                                            <div class="table-responsive" style="height: 400px">
                                                    <asp:GridView ID="dgOldApproverEmp" runat="server"  AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                    AllowPaging="false" DataKeyNames = "employeeunkid">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                        <asp:CheckBox ID="ChkOldAll" runat="server" CssClass="filled-in" Text=" "  />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                        <asp:CheckBox ID="ChkgvOldSelect" runat="server" CssClass="filled-in" Text=" "  />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="ecode" HeaderText="Employee Code" ReadOnly="true" FooterText="colhdgEmployeecode" />
                                                                        <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgEmployee" />
                                                                    </Columns>
                                                     </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-md-2 col-sm-2 col-xs-2 m-t-100">
                                                <div>
                                                    <asp:Button ID="objbtnAssign" runat="server" Text=">>" CssClass="btn btn-primary" />
                                                </div>
                                                <div style="margin-top:10px">
                                                    <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btn btn-primary" />
                                                </div>
                                                  
                                                  
                                        </div>
                                         <div class="col-xs-5 col-md-5 col-sm-5 col-xs-5">
                                                 <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                          <li role="presentation" class="active"><a href="#tbpMigrationEmp" data-toggle="tab">
                                                                <asp:Label ID="tbMigrationEmp" runat="server" Text="Migrated Employee" CssClass="form-label" />
                                                            </a></li>
                                                            <li role="presentation"><a href="#tbpAssignedEmp" data-toggle="tab">
                                                                <asp:Label ID="tbAssignedEmp" runat="server" Text="Assigned Employee" CssClass="form-label" />
                                                           </a></li>
                                                 </ul>
                                                    <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="tbpMigrationEmp">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                 <input type="text" id="txtNewApproverMigratedEmp" name="txtNewApproverMigratedEmp"
                                                                                placeholder="type search text" maxlength="50" onkeyup="FromSearching(this);"  class = "form-control" />
                                                            </div>
                                                        </div>
                                                         <div class="table-responsive" style="height: 350px">
                                                             <asp:GridView ID="dgNewApproverMigratedEmp" runat="server" AllowPaging="false" DataKeyNames="employeeunkid"  CssClass="table table-hover table-bordered">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                        <asp:CheckBox ID="ChkNewAll" runat="server" onclick="gridCheckBoxAll(this)" CssClass="filled-in" Text=" " />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                        <asp:CheckBox ID="ChkgvNewSelect" runat="server" onclick="gridSelectCheckBox(this,'ChkNewAll','ChkgvNewSelect')" CssClass="filled-in" Text=" " />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="ecode" HeaderText="Employee Code" ReadOnly="True" FooterText="colhdgMigratedEmpCode">
                                                                            <HeaderStyle Width="30%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="True" FooterText="colhdgMigratedEmp">
                                                                            <HeaderStyle Width="70%" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                    <RowStyle CssClass="griviewitem" />
                                                            </asp:GridView>
                                                            </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="tbpAssignedEmp">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                   <input type="text" id="txtNewApproverAssignEmp" name="txtNewApproverAssignEmp" placeholder="type search text"
                                                                                maxlength="50" onkeyup="FromSearching(this);"  class = "form-control" />
                                                            </div>
                                                        </div>
                                                         <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="dgNewApproverAssignEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="false" >
                                                                            <Columns>
                                                                                <asp:BoundField DataField="ecode" HeaderStyle-Width="30%" HeaderText="Employee Code"
                                                                                    ReadOnly="true" FooterText="colhdgAssignEmpCode" />
                                                                                <asp:BoundField DataField="ename" HeaderStyle-Width="70%" HeaderText="Employee" ReadOnly="true"
                                                                                    FooterText="colhdgAssignEmployee" />
                                                                                <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                    Visible="false" />
                                                                            </Columns>
                                                                   </asp:GridView>
                                                         </div>
                                                    </div>
                                                </div>
                                         </div>
                                </div>
                         </div>
                            <div class="footer">
                                     <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                     <asp:Button ID="btnReset" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                      </div>
                  </div>   
              </div>       
                
                    <%--<div class="panel-primary">
                        <div class="panel-heading">
                           
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                                            
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row2">
                                        <div class="ib" style="width: 42%; vertical-align: top">
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                    
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                            
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                       
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                          
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                          
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                  
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                            <div id="scrollable-container" style="vertical-align: top; overflow: auto; width: 100%;
                                                                height: 400px; margin-top: 5px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                
                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ib" style="width: 12%; vertical-align: top; margin-top: 20%">
                                            <div class="row2">
                                                <div class="ib" style="width: 100%; text-align: center">
                                                          
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%; text-align: center">
                                                            
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ib" style="width: 42%; vertical-align: top">
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                   
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                   
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                    
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                  
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                    
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                            <cc1:TabContainer ID="tabMain" runat="server" ActiveTabIndex="0">
                                                                <cc1:TabPanel ID="tbMigrationEmp" runat="server" HeaderText="Migrated Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                           
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container1" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                                        
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                                <cc1:TabPanel ID="tbAssignedEmp" runat="server" HeaderText="Assigned Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                         
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container2" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                                        
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                            </cc1:TabContainer>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
