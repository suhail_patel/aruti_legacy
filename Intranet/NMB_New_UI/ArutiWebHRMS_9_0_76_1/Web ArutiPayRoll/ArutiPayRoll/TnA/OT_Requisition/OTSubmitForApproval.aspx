﻿<%@ Page Title="Global Submit For Approval" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="OTSubmitForApproval.aspx.vb" Inherits="TnA_OT_Requisition_OTSubmitForApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
    
        $("body").on("click","[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked")); 
            
        });
        
        $("body").on("click","[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
            }
            else{
                    chkHeader.prop("checked", false);
            }
            
        });
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Global Submit For Approval"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpFromDate" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpToDate" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="gvOTRequisitionList" runat="server" DataKeyNames="otrequisitiontranunkid, empcodename,employeeunkid,ischecked,IsGrp,plannedot_hours"
                                                AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="false"
                                                ShowFooter="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objdgcolhSelect">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Employee" DataField="empcodename" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqemp" ItemStyle-Width="200px" />
                                                    <asp:BoundField HeaderText="Request Date" DataField="requestdate" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotrequestdate" ItemStyle-Width="100px" />
                                                    <asp:BoundField HeaderText="Planned Start Time" DataField="plannedstart_time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="140px" FooterText="colhotreqplannedstart_time" />
                                                    <asp:BoundField HeaderText="Planned End Time" DataField="plannedend_time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="140px" FooterText="colhotreqplannedend_time" />
                                                    <asp:BoundField HeaderText="Planned OT Hours" DataField="PlannedworkedDuration" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="130px" FooterText="colhotreqplannedot_hours" />
                                                    <asp:BoundField HeaderText="Request Reason" DataField="request_reason" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="250px" FooterText="colhotrequestreason" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSubmitForApproval" runat="server" Text="Submit For Approval" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--     
                <div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 80%">
                                        <td style="width: 5%">
                                        </td>
                                        <td style="width: 7%">
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td style="width: 25%">
                                        </td>
                                    </tr>
                                    <tr style="width: 80%">
                                        <td style="width: 5%">
                                        </td>
                                        <td style="width: 7%">
                                        </td>
                                        <td style="width: 30%" colspan="3">
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                        max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                      
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <uc3:ConfirmYesNo ID="popupConfirmationForOTCap" Title="" runat="server" Message="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
