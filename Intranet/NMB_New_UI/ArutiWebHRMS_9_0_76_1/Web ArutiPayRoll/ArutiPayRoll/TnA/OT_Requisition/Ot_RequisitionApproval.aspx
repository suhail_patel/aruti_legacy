﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Ot_RequisitionApproval.aspx.vb"
    Inherits="TnA_OT_Requisition_Ot_RequisitionApproval" Title="OT Requisition Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        //        $("body").on("click", "[id*=chkSelectAll]", function() {
        //            var chkHeader = $(this);
        //            var grid = $(this).closest("table");
        //            $("input[type=checkbox]", grid).each(function() {
        //                if (chkHeader.is(":checked")) {
        //                    debugger;
        //                    $(this).attr("checked", "checked");

        //                } else {
        //                    $(this).removeAttr("checked");
        //                }
        //            });
        //        });

        //        $("body").on("click", "[id*=chkSelect]", function() {
        //            var grid = $(this).closest("table");
        //            var chkHeader = $("[id*=chkHeader]", grid);
        //            var row = $(this).closest("tr")[0];

        //            debugger;
        //            if (!$(this).is(":checked")) {
        //                var row = $(this).closest("tr")[0];
        //                chkHeader.removeAttr("checked");
        //            } else {

        //                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
        //                    chkHeader.attr("checked", "checked");
        //                }
        //            }
        //        });

        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));

        });

        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);

            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
                }
            else {
                chkHeader.prop("checked", false);
            }

        });

        

        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="OT Requisition Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboListEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListOTReqFromDate" runat="server" Text="Req. From Date " CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpListOTFromDate" runat="server" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRequisitionStatus" runat="server" Text="Requisition Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboRequisitionStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListOTReqToDate" runat="server" Text="Req. To Date " CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpListOTReqToDate" runat="server" AutoPostBack="true" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-35">
                                        <asp:CheckBox ID="chkMyApprovals" runat="server" Text="My Approval" Checked="true">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:GridView ID="gvOTRequisitionApproverList" runat="server" CssClass="table table-hover table-bordered"
                                                DataKeyNames="IsGrp,Employee,Particulars,ischecked,employeeunkid,otrequisitiontranunkid,otrequestapprovaltranunkid,RDate,tnalevelunkid,tnapriority,mapuserunkid,OTRequisitionStatusId,ApprovalStatusId"
                                                AutoGenerateColumns="False" AllowPaging="false" ShowFooter="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Particulars" HeaderText="Particulars" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotParticulars" Visible="false" />
                                                    <asp:BoundField DataField="Approver" HeaderText="Approver" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqapprover" ItemStyle-Width="450px" />
                                                    <asp:BoundField DataField="plannedstart_time" HeaderText="Planned Start Time" ItemStyle-Wrap="false"
                                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="230px" FooterText="colhotreqplannedstart_time" />
                                                    <asp:BoundField DataField="plannedend_time" HeaderText="Planned End Time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Wrap="false" ItemStyle-Width="230px" FooterText="colhotreqplannedend_time" />
                                                    <asp:BoundField DataField="plannedot_hours" HeaderText="Planned OT Hrs" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="125px" FooterText="colhotreqplannedOTHrs" />
                                                    <asp:BoundField DataField="approvaldate" HeaderText="Approval Date" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="200px" FooterText="colhotapprovaldate" />
                                                    <asp:BoundField DataField="actualstart_time" HeaderText="Actual Start Time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="250px" FooterText="colhotreqactualstart_time" />
                                                    <asp:BoundField DataField="actualend_time" HeaderText="Actual End Time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="250px" FooterText="colhotreqactualend_time" />
                                                    <asp:BoundField DataField="actualot_hours" HeaderText="Actual OT Hrs" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="90px" FooterText="colhotactualHrs" />
                                                    <asp:BoundField DataField="TotalApprovedHrs" HeaderText="Total Approved Hrs" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="100px" FooterText="colhotTotalapprovedhrs" />
                                                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="300px" FooterText="colhotreqstatus" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnChangeStatus" runat="server" CssClass="btn btn-primary" Text="Change Status" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupOTRequisition" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblReasonForAdjustment" runat="server" PopupControlID="pnlOTRequisition"
                    DropShadow="false" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlOTRequisition" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblOTApprovalTitle" Text="OT Requisition Approval" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <asp:Label ID="LblFromDate" runat="server" Text="Req. From Date " CssClass="form-label"></asp:Label>
                                <uc1:DateCtrl ID="dtpFromDate" runat="server" />
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <asp:Label ID="LblToDate" runat="server" Text="Req. To Date " CssClass="form-label"></asp:Label>
                                <uc1:DateCtrl ID="dtpToDate" runat="server" />
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-12 col-xs-12">
                                <asp:Label ID="lblReasonForAdjustment" runat="server" Text="Adjustment Reason" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtReasonForAdjustment" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <div class="table-responsive" style="height: 300px;">
                                    <asp:GridView ID="gvOTRequisitionApproval" runat="server" DataKeyNames="IsGrp,Particulars,Employee,ischecked,employeeunkid,actualstart_time,actualend_time"
                                        AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" ShowFooter="false" HeaderStyle-Wrap="false" RowStyle-Wrap="false">
                                        <Columns>
                                            <asp:BoundField DataField="Particulars" HeaderText="Particulars" ItemStyle-VerticalAlign="Top"
                                                FooterText="colhApprovalotreqemp" Visible="false" />
                                            <asp:BoundField DataField="Approver" HeaderText="Approver" ItemStyle-VerticalAlign="Top"
                                                FooterText="colhApprovalotreqapprover" ItemStyle-Width="200px" />
                                            <asp:BoundField DataField="requestdate" HeaderText="Request Date" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="120px" FooterText="colhApprovalotrequestdate" />
                                            <asp:BoundField DataField="plannedstart_time" HeaderText="Planned Start Time" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="180px" FooterText="colhApprovalotreqplannedstart_time" />
                                            <asp:BoundField DataField="plannedend_time" HeaderText="Planned End Time" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="180px" FooterText="colhApprovalotreqplannedend_time" />
                                            <asp:BoundField DataField="plannedot_hours" HeaderText="Planned OT Hrs" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="140px" FooterText="colhApprovalotreqplannedOTHrs" />
                                            <asp:BoundField DataField="request_reason" HeaderText="Request Reason" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="120px" FooterText="colhApprovalotreqreason" />
                                            <asp:BoundField DataField="TotalApprovedHrs" HeaderText="Total Approved Hrs" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="80px" FooterText="colhApprovalotTotalapprovedhrs" />
                                            <asp:TemplateField FooterText="colhApprovalActualStartTime" HeaderText="Actual Start Time"
                                                HeaderStyle-Width="250px" ItemStyle-Wrap="false" ItemStyle-Width="250px">
                                                <ItemTemplate>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                            <asp:DropDownList ID="cboActualStartTimehr" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboActualStartTimehr_SelectedIndexChanged" />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                            <asp:Label ID="lblActualStarTimecolon" runat="server" Text=" : " />
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                            <asp:DropDownList ID="cboActualStartTimemin" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboActualStartTimehr_SelectedIndexChanged" />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField FooterText="colhApprovalActualEndTime" HeaderText="Actual End Time"
                                                HeaderStyle-Width="250px" ItemStyle-Wrap="false" ItemStyle-Width="250px" HeaderStyle-Wrap="true"
                                                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-lg-12 col-md-12 col-xs-12">
                                                            <asp:DropDownList ID="cboActualEndTimehr" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboActualEndTimehr_SelectedIndexChanged" />
                                                        </div>
                                                        <div class="col-lg-2 col-lg-2 col-md-2 col-xs-2">
                                                            <asp:Label ID="lblActualEndTimecolon" runat="server" Text=" : " />
                                                        </div>
                                                        <div class="col-lg-12 col-lg-12 col-md-12 col-xs-12">
                                                            <asp:DropDownList ID="cboActualEndTimemin" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboActualEndTimehr_SelectedIndexChanged" />
                                                        </div>
                                                    </div>
                                                    <div style="width: 32%;" class="ib">
                                                    </div>
                                                    <div style="width: 10%;" class="ibwm">
                                                    </div>
                                                    <div style="width: 32%;" class="ib">
                                                    </div>
                                                </ItemTemplate>
                                                <HeaderStyle Width="800px" />
                                                <ItemStyle Width="800px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField FooterText="colhhiddenValue">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdactual_startime" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdactual_endtime" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdactualot_hoursinsecs" runat="server" Value="0" />
                                                    <asp:HiddenField ID="hdactualnight_hrs" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdactualnight_hoursinsecs" runat="server" Value="0" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnOtApprove" runat="server" CssClass="btn btn-primary" Text="Approve" />
                        <asp:Button ID="btnOtDisApprove" runat="server" CssClass="btn btn-default" Text="Disapprove" />
                        <asp:Button ID="btnpopupOtClose" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
                <uc2:Confirmation ID="popupConfirmationForOTCap" Title="" runat="server" Message="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
