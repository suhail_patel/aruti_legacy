﻿Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class TnA_OT_Requisition_Ot_RequisitionApproval
    Inherits Basepage


#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "frmOtRequisitionApprovalList"
    Private ReadOnly mstrModuleName1 As String = "frmOtRequisitionApproval"
    Private DisplayMessage As New CommonCodes
    Private mstrEmployeeIDs As String = String.Empty
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objotrequisition_approval_Tran As New clsTnaotrequisition_approval_Tran
    'Pinkal (03-Sep-2020) -- End


    Private mintPriority As Integer = -1
    Private mintTnAMappingunkid As Integer = -1
    Private mintTnALevelunkid As Integer = -1
    Private mdtOTApprovalList As DataTable = Nothing
    Private mblnShowPopup As Boolean = False
    Private mdtOTApproval As DataTable = Nothing
    Private mblnIsWarning As Boolean = False
    Private mintPeriodID As Integer = 0
    Private objCONN As SqlConnection
    Private mblnIsFromEmailLink As Boolean = False
    Private mblnIncludeCapApprover As Boolean = False

    Private mstrOtRequisitionApprovalIds As String = ""

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count <= 0 Then Exit Sub
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END


                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End


                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                Try
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If
                Catch ex1 As Exception
                    DisplayMessage.DisplayError(ex1, Me)
                End Try
                'Pinkal (13-Aug-2020) -- End


                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                If arr.Length > 0 Then

                    'Pinkal (29-Jan-2020) -- Start
                    'Enhancement - Changes related To OT NMB Testing.
                    'mintPeriodID = CInt(arr(0))
                    'mintTnAMappingunkid = CInt(arr(1))
                    'HttpContext.Current.Session("CompanyUnkId") = CInt(arr(2))
                    'HttpContext.Current.Session("UserId") = CInt(arr(3))
                    mdtPeriodStartDate = eZeeDate.convertDate(arr(0).ToString())
                    mdtPeriodEndDate = eZeeDate.convertDate(arr(1).ToString())
                    mintTnAMappingunkid = CInt(arr(2))
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(3))
                    HttpContext.Current.Session("UserId") = CInt(arr(4))
                    'Pinkal (29-Jan-2020) -- End




                    mblnIsFromEmailLink = True

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    'Pinkal (03-Sep-2020) -- End
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    'Pinkal (03-Sep-2020) -- End

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)


                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'Dim clsConfig As New clsConfigOptions
                    'clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    'If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                    '    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    'Else
                    '    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    'End If

                    'Session("DateFormat") = clsConfig._CompanyDateFormat
                    'Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    'Session("OTTenureDays") = clsConfig._OTTenureDays
                    'clsConfig = Nothing

                    If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                    End If

                    Session("DateFormat") = ConfigParameter._Object._CompanyDateFormat
                    Session("DateSeparator") = ConfigParameter._Object._CompanyDateSeparator
                    Session("OTTenureDays") = ConfigParameter._Object._OTTenureDays
                    'Pinkal (03-Sep-2020) -- End


                    Call SetDateFormat()

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    objUser = Nothing
                    'Pinkal (03-Sep-2020) -- End

                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                End If  '  If arr.Length > 0 Then


                'Pinkal (29-Jan-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                'Dim strFilter As String = "AND tnaotrequisition_approval_tran.periodunkid=" & mintPeriodID & " " & _
                '                          "AND tnaotrequisition_approval_tran.tnamappingunkid=" & mintTnAMappingunkid & " " & _
                '                          "AND tnaotrequisition_approval_tran.statusunkid=2 "

                Dim strFilter As String = "AND CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) BETWEEN '" & eZeeDate.convertDate(mdtPeriodStartDate.Date) & "' AND '" & eZeeDate.convertDate(mdtPeriodEndDate.Date) & "' " & _
                                                   " AND tnaotrequisition_approval_tran.tnamappingunkid=" & mintTnAMappingunkid & " " & _
                                                   " AND tnaotrequisition_approval_tran.statusunkid=2 "
                'Pinkal (29-Jan-2020) -- End



                If strFilter.Trim.Length > 0 Then
                    strFilter = strFilter.Substring(3)
                End If


                'Pinkal (27-Feb-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.

                'Dim dsList As DataSet = objotrequisition_approval_Tran.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                                                       CInt(Session("UserId")), chkMyApprovals.Checked, Nothing, True, -1, True, strFilter)


                'Pinkal (02-Jun-2020) -- Start
                'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
                'If Session("AllowApplyOTForEachMonthDay") IsNot Nothing AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) > 0 Then
                'mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, CInt(Session("AllowApplyOTForEachMonthDay")) + 1)
                If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
                    mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                    If mdtPeriodStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
                        'mdtPeriodStartDate = mdtPeriodStartDate.AddDays(-31)
                        mdtPeriodStartDate = mdtPeriodStartDate.AddDays(CInt(Session("OTTenureDays")) + 1)
                    Else
                        Dim mdtTempDate As DateTime = mdtPeriodStartDate.AddMonths(-1)
                        If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
                            mdtPeriodStartDate = mdtTempDate
                        End If
                    End If
                    'mdtPeriodEndDate = mdtPeriodStartDate.AddDays(30)
                    mdtPeriodEndDate = mdtPeriodStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
                    'Pinkal (02-Jun-2020) -- End
                Else
                    mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                    mdtPeriodEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
                End If


                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                Dim objotrequisition_approval_Tran As New clsTnaotrequisition_approval_Tran
                'Pinkal (03-Sep-2020) -- End


                Dim dsList As DataSet = objotrequisition_approval_Tran.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                                       CInt(Session("UserId")), chkMyApprovals.Checked, mdtPeriodStartDate, mdtPeriodEndDate, Nothing, True, -1, True, strFilter)

                'Pinkal (27-Feb-2020) -- End

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objotrequisition_approval_Tran = Nothing
                'Pinkal (03-Sep-2020) -- End


                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "There is no any pending OT Requisition application for approval."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    'Pinkal (03-Sep-2020) -- End
                    Exit Sub
                End If

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                'Pinkal (03-Sep-2020) -- End


                chkMyApprovals.Enabled = False

            End If   'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then


            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                Call FillCombo()
                SetVisibility()

                dtpListOTFromDate_TextChanged(dtpListOTFromDate, New EventArgs())

                dtpListOTFromDate.SetDate = mdtPeriodStartDate.Date
                dtpListOTReqToDate.SetDate = mdtPeriodEndDate.Date

                dtpListOTFromDate.Enabled = False
                dtpListOTReqToDate.Enabled = False

                If mblnIsFromEmailLink AndAlso Request.QueryString.Count > 0 Then
                    btnListSearch_Click(btnListSearch, New EventArgs())
                End If

            Else
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
                mdtPeriodStartDate = Me.ViewState("PeriodStartDate")
                mdtPeriodEndDate = Me.ViewState("PeriodEndDate")
                mintTnAMappingunkid = CInt(Me.ViewState("TnAMappingunkid"))
                mintTnALevelunkid = CInt(Me.ViewState("TnALevelunkid"))
                mintPriority = CInt(Me.ViewState("Priority"))
                mblnShowPopup = CBool(Me.ViewState("ShowPopup"))
                mdtOTApproval = Me.ViewState("OTApproval")

                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.
                'mdtOTApprovalList = Me.ViewState("OTApprovalList")
                'If mblnShowPopup = True AndAlso mdtOTApprovalList IsNot Nothing Then
                '    gvOTRequisitionApproverList.DataSource = mdtOTApprovalList
                '    gvOTRequisitionApproverList.DataBind()
                'End If
                'Pinkal (12-Oct-2021)-- End

                mblnIsWarning = CBool(Me.ViewState("IsWarning"))
                mblnIsFromEmailLink = CBool(Me.ViewState("IsFromEmailLink"))
                mblnIncludeCapApprover = CBool(Me.ViewState("IncludeCapApprover"))

                mstrOtRequisitionApprovalIds = Me.ViewState("OtRequisitionApprovalIds")
            End If

            If mblnShowPopup Then
                popupOTRequisition.Show()
            End If

        Catch ex As Exception
           DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("TnAMappingunkid") = mintTnAMappingunkid
            Me.ViewState("TnALevelunkid") = mintTnALevelunkid
            Me.ViewState("Priority") = mintPriority
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            ''Me.ViewState("OTApprovalList") = mdtOTApprovalList
            'Pinkal (12-Oct-2021)-- End
            Me.ViewState("ShowPopup") = mblnShowPopup
            Me.ViewState("OTApproval") = mdtOTApproval
            Me.ViewState("IsWarning") = mblnIsWarning
            Me.ViewState("IsFromEmailLink") = mblnIsFromEmailLink
            Me.ViewState("IncludeCapApprover") = mblnIncludeCapApprover
            Me.ViewState("OtRequisitionApprovalIds") = mstrOtRequisitionApprovalIds
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

#Region "  OT Requisition Approval List"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objMaster As New clsMasterData

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename â€œRescheduledâ€ status, when approver is changing leave form status.
            'Dim dtList As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid IN (0,1,2,3)").CopyToDataTable
            Dim dtList As DataTable = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2,3)").CopyToDataTable
            'Pinkal (03-Jan-2020) -- End


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objMaster = Nothing
            'Pinkal (03-Sep-2020) -- End


            With cboRequisitionStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtList.Copy()
                .DataBind()
                .SelectedValue = "2"
            End With



            Dim objApprover As New clsTnaapprover_master
            Dim dtTable As DataTable = objApprover.GetUserPriority(enTnAApproverType.OT_Requisition_Approver, CInt(Session("UserId")), mintPriority, Nothing)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mintTnAMappingunkid = CInt(dtTable.Rows(0)("tnamappingunkid"))
                mintTnALevelunkid = CInt(dtTable.Rows(0)("tnalevelunkid"))
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtTable IsNot Nothing Then dtTable.Clear()
            'Pinkal (03-Sep-2020) -- End
            dtTable = Nothing
            objApprover = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                btnChangeStatus.Visible = False
                chkMyApprovals.Visible = False
                gvOTRequisitionApproval.Columns(0).Visible = False
            Else
                btnChangeStatus.Visible = CBool(Session("AllowtoApproveOTRequisition"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillOTRequisitionApprovalList(ByVal isblank As Boolean)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objotrequisition_approval_Tran As clsTnaotrequisition_approval_Tran
        'Pinkal (03-Sep-2020) -- End
        Try

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                If CBool(Session("AllowtoViewOTRequisitionApprovalList")) = False Then Exit Sub
            End If

            Dim strFiler As String = ""


            If CInt(cboListEmployee.SelectedValue) > 0 Then
                strFiler &= "AND tnaot_requisition_tran.employeeunkid = " & CInt(cboListEmployee.SelectedValue) & " "
            End If


            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'If dtpListOTFromDate.IsNull = False Then
            '    strFiler &= "AND CONVERT(Char(8),tnaot_requisition_tran.requestdate,112) >= '" & eZeeDate.convertDate(dtpListOTFromDate.GetDate) & "' "
            'End If

            'If dtpListOTReqToDate.IsNull = False Then
            '    strFiler &= "AND CONVERT(Char(8),tnaot_requisition_tran.requestdate,112) <= '" & eZeeDate.convertDate(dtpListOTReqToDate.GetDate) & "' "
            'End If
            'Pinkal (27-Feb-2020) -- End


            If CInt(cboRequisitionStatus.SelectedValue) > 0 Then
                strFiler &= "AND ISNULL(tnaot_requisition_tran.statusunkid,2) = " & CInt(cboRequisitionStatus.SelectedValue) & " "
            End If

            'If CInt(cboRequisitionStatus.SelectedValue) > 0 Then
            '    strFiler &= "AND ISNULL(tnaot_requisition_tran.statusunkid, 2) = " & CInt(cboRequisitionStatus.SelectedValue) & " "
            'End If

            strFiler &= "AND tnaotrequisition_approval_tran.visibleunkid <> -1 "


            If isblank Then
                strFiler &= "AND 1=2 "
            End If

            If strFiler.Trim.Length > 0 Then
                strFiler = strFiler.Substring(3)
            End If



            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            'Dim dsList As DataSet = objotrequisition_approval_Tran.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                                                                            CInt(Session("UserId")), chkMyApprovals.Checked, Nothing, True, -1, True, strFiler)


            'Pinkal (27-Jun-2020) -- Start
            'Enhancement NMB -   Working on Employee Signature Report.
            'Dim dsList As DataSet = objotrequisition_approval_Tran.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                                                                        CInt(Session("UserId")), chkMyApprovals.Checked, dtpListOTFromDate.GetDate.Date, dtpListOTReqToDate.GetDate.Date, Nothing, True, -1, True, strFiler)

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objotrequisition_approval_Tran = New clsTnaotrequisition_approval_Tran
            'Pinkal (03-Sep-2020) -- End

            Dim dsList As DataSet = objotrequisition_approval_Tran.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                                    CInt(Session("UserId")), chkMyApprovals.Checked, mdtPeriodStartDate _
                                                                                                    , mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month) - 1).Date, Nothing, True, -1, True, strFiler)

            'Pinkal (27-Jun-2020) -- End


            'Pinkal (27-Feb-2020) -- End

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'mdtOTApprovalList = dsList.Tables("List")
            mdtOTApprovalList = dsList.Tables("List").Copy()
            'Pinkal (12-Oct-2021)-- End

            Dim mblnHideFirstRow As Boolean = False


            If isblank = False Then

                Dim xOTRequisitionApprovalTranId As Integer = 0
                Dim dList As DataTable = Nothing
                Dim strStaus As String = ""

                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.

                'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                '    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

                '    For Each drRow As DataRow In dsList.Tables(0).Rows

                '        If CInt(drRow("otrequestapprovaltranunkid")) <= 0 Then
                '            drRow("Particulars") = info1.ToTitleCase(drRow("Particulars").ToString().ToLower())
                '            Continue For
                '        End If

                '        strStaus = ""
                '        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND otrequisitiontranunkid = " & CInt(drRow("otrequisitiontranunkid").ToString()), "tnapriority asc", DataViewRowState.CurrentRows).ToTable


                '        drRow("Approver") = info1.ToTitleCase(drRow("Approver").ToString().ToLower())

                '        If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                '            Dim dr As DataRow() = dList.Select("tnapriority >= " & CInt(drRow("tnapriority")))
                '            If dr.Length > 0 Then
                '                For i As Integer = 0 To dr.Length - 1
                '                    If CInt(drRow("ApprovalStatusId")) = 1 Then
                '                        strStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Approved By :-  ") & info1.ToTitleCase(drRow("ApproverName").ToString().ToLower())
                '                        Exit For

                '                    ElseIf CInt(drRow("ApprovalStatusId")) = 2 Then
                '                        If CInt(dr(i)("ApprovalStatusId")) = 1 Then
                '                            strStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Approved By :-  ") & info1.ToTitleCase(dr(i)("ApproverName").ToString().ToLower())
                '                            Exit For

                '                        ElseIf CInt(dr(i)("ApprovalStatusId")) = 3 Then
                '                            strStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Rejected By :-  ") & info1.ToTitleCase(dr(i)("ApproverName").ToString().ToLower())
                '                            Exit For
                '                        End If

                '                    ElseIf CInt(drRow("ApprovalStatusId")) = 3 Then
                '                        strStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Rejected By :-  ") & info1.ToTitleCase(drRow("ApproverName").ToString().ToLower())
                '                        Exit For
                '                    End If
                '                Next
                '            End If
                '        End If
                '        If strStaus <> "" Then
                '            drRow("status") = strStaus.Trim
                '        End If
                '    Next

                'End If

                'Pinkal (12-Oct-2021)-- End

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    If chkMyApprovals.Checked Then

                        If CInt(cboRequisitionStatus.SelectedValue) > 0 Then
                            mdtOTApprovalList = New DataView(dsList.Tables(0), "Mapuserunkid = " & CInt(Session("UserId")) & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtOTApprovalList = New DataView(dsList.Tables(0), "Mapuserunkid = " & CInt(Session("UserId")) & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        Dim dtTemp As DataTable = New DataView(mdtOTApprovalList, "", "", DataViewRowState.CurrentRows).ToTable(True, "RDate", "employeeunkid")

                        Dim dr = From drTable In mdtOTApprovalList Group Join drTemp In dtTemp On drTable.Field(Of String)("RDate") Equals drTemp.Field(Of String)("RDate") And _
                                     drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable

                        If dr.Count > 0 Then
                            dr.ToList.ForEach(Function(x) DeleteRow(x, mdtOTApprovalList))
                        End If

                    Else
                        If mblnHideFirstRow = False AndAlso mstrEmployeeIDs.Trim.Length > 0 Then
                            'Pinkal (12-Oct-2021)-- Start
                            'NMB OT Requisition Performance Issue.
                            'mdtOTApprovalList = New DataView(mdtOTApprovalList, "employeeunkid  in (" & mstrEmployeeIDs & ") OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                            mdtOTApprovalList = New DataView(mdtOTApprovalList, "employeeunkid  in (" & mstrEmployeeIDs & ")", "", DataViewRowState.CurrentRows).ToTable
                            'Pinkal (12-Oct-2021)-- End
                        End If
                    End If
                Else
                    mdtOTApprovalList = New DataView(mdtOTApprovalList, "", "RDate desc", DataViewRowState.CurrentRows).ToTable
                End If

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                If dList IsNot Nothing Then dList.Clear()
                dList = Nothing
                'Pinkal (03-Sep-2020) -- End
            End If

            If mdtOTApprovalList.Rows.Count <= 0 Then
                Dim drRow As DataRow = mdtOTApprovalList.NewRow()
                drRow("IsGrp") = False
                drRow("otrequestapprovaltranunkid") = 0
                drRow("employeeunkid") = 0
                drRow("ischecked") = False
                drRow("visibleunkid") = 2
                drRow("mapuserunkid") = CInt(Session("UserId"))
                mdtOTApprovalList.Rows.Add(drRow)
                mblnHideFirstRow = True
            End If


            gvOTRequisitionApproverList.DataSource = mdtOTApprovalList
            gvOTRequisitionApproverList.DataBind()

            If mblnHideFirstRow Then
                gvOTRequisitionApproverList.Rows(0).Visible = False
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objotrequisition_approval_Tran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub ResetOTRequisitionApprovalList()
        Try
            cboRequisitionStatus.SelectedValue = 2

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then cboListEmployee.SelectedValue = 0
            'cboRequisitionStatus.SelectedValue = 2
            FillOTRequisitionApprovalList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            'rw.Visible = False
            'Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            'row.BackColor = ColorTranslator.FromHtml("#ECECEC")
            'Dim cell As TableCell = New TableCell()
            'cell.Text = title
            'cell.Font.Bold = True
            'cell.ColumnSpan = gd.Columns.Count
            'row.Cells.Add(cell)
            'gd.Controls(0).Controls.Add(row)

            rw.BackColor = ColorTranslator.FromHtml("#ECECEC")
            rw.Cells(0).FindControl("chkselect").Visible = False
            rw.Cells(0).Font.Bold = True
            rw.Cells(0).Text = title
            rw.Cells(0).Style.Add("text-align", "left")

            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'rw.Cells(0).ColumnSpan = rw.Cells.Count - 5
            'For i = (rw.Cells(0).ColumnSpan) To rw.Cells.Count - 1
            rw.Cells(0).ColumnSpan = rw.Cells.Count - 6
            For i = (rw.Cells(0).ColumnSpan) To rw.Cells.Count - 2
                rw.Cells(i).Visible = False
            Next
            'Pinkal (27-Feb-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("Rdate = '" & dr("Rdate").ToString() & "' AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 AndAlso drRow.Length > 0 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

#End Region

#Region "  OT Requisition Approval"

    Private Sub FillPeriod()
        Dim dsPeriod As DataSet = Nothing
        Try

            'Dim objPeriod As New clscommom_period_Tran
            'dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
            '                                   CDate(Session("fin_startdate")).Date, "List", True)
            'With cboPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsPeriod.Tables("List")
            '    .DataBind()
            '    .SelectedValue = CInt(cboListPeriod.SelectedValue)
            'End With
            'objPeriod = Nothing
            'dsPeriod = Nothing

            txtReasonForAdjustment.Text = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillApprovalData()
        Try
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'If mdtOTApprovalList IsNot Nothing Then
            '    mdtOTApproval = New DataView(mdtOTApprovalList, "ischecked = 1", "", DataViewRowState.CurrentRows).ToTable()
            '    gvOTRequisitionApproval.DataSource = mdtOTApproval
            '    gvOTRequisitionApproval.DataBind()
            'End If

            Dim strFilter As String = ""
            Dim objotrequisition_approval_Tran As New clsTnaotrequisition_approval_Tran

            strFilter = "AND tnaotrequisition_approval_tran.visibleunkid <> -1 "

            If mstrOtRequisitionApprovalIds.Trim.Length > 0 Then
                strFilter &= "AND tnaotrequisition_approval_tran.otrequestapprovaltranunkid IN (" & mstrOtRequisitionApprovalIds.Trim & ")"
            End If

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If

            Dim dsList As DataSet = objotrequisition_approval_Tran.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                                 CInt(Session("UserId")), chkMyApprovals.Checked, mdtPeriodStartDate _
                                                                                                 , mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month) - 1).Date, Nothing, True, -1, False, strFilter)

            mdtOTApproval = dsList.Tables(0).Copy

                gvOTRequisitionApproval.DataSource = mdtOTApproval
                gvOTRequisitionApproval.DataBind()

            dsList.Clear()
            dsList = Nothing

            objotrequisition_approval_Tran = Nothing

            'Pinkal (12-Oct-2021)-- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddApprovalGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.BackColor = ColorTranslator.FromHtml("#ECECEC")
            rw.Cells(1).Font.Bold = True
            rw.Cells(1).Text = title
            rw.Cells(1).Style.Add("text-align", "left")

            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'rw.Cells(1).ColumnSpan = rw.Cells.Count - 4
            'For i = (rw.Cells(1).ColumnSpan) To rw.Cells.Count - 1
            rw.Cells(1).ColumnSpan = rw.Cells.Count - 5
            For i = (rw.Cells(1).ColumnSpan) To rw.Cells.Count - 2
                rw.Cells(i).Visible = False
            Next
            'Pinkal (27-Feb-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function SetValueForApproval(ByVal sender As Button) As Boolean
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproval As New clsTnaotrequisition_approval_Tran
        'Pinkal (03-Sep-2020) -- End
        Try

            objApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime

            objApproval._Mapuserunkid = CInt(Session("UserId"))
            'objApproval._Periodunkid = CInt(cboPeriod.SelectedValue)

            If sender.ID.ToUpper = btnOtApprove.ID.ToUpper() Then
                objApproval._Statusunkid = 1 'Approve
            ElseIf sender.ID.ToUpper = btnOtDisApprove.ID.ToUpper() Then
                objApproval._Statusunkid = 3 'Reject
            End If
            objApproval._Visibleunkid = objApproval._Statusunkid

            objApproval._Adjustment_Reason = txtReasonForAdjustment.Text.Trim()
            objApproval._Userunkid = CInt(Session("UserId"))
            objApproval._Isvoid = False

            objApproval._Iscancel = False
            objApproval._Canceldatetime = Nothing
            objApproval._Canceluserunkid = -1
            objApproval._Cancelreason = ""

            objApproval._Audituserunkid = CInt(Session("UserId"))
            objApproval._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproval._ClientIp = CStr(Session("IP_ADD"))
            objApproval._Hostname = CStr(Session("HOST_NAME"))
            objApproval._Form_Name = mstrModuleName1
            objApproval._Isweb = True

            mdtOTApproval = New DataView(mdtOTApproval, "isGrp = 0", "Employee,RDate", DataViewRowState.CurrentRows).ToTable()


            'Pinkal (29-Jan-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            'If objApproval.UpdateGlobalData(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), CInt(Session("CompanyUnkId")) _
            '                                             , False, mdtOTApproval, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mblnIncludeCapApprover) = False Then
            '    DisplayMessage.DisplayMessage(objApproval._Message, Me)
            '    Return False
            'End If

            If objApproval.UpdateGlobalData(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), CInt(Session("CompanyUnkId")) _
                                                     , False, mdtOTApproval, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mblnIncludeCapApprover _
                                                     , dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date) = False Then
                DisplayMessage.DisplayMessage(objApproval._Message, Me)
                Return False
            End If

            'Pinkal (29-Jan-2020) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            If mblnShowPopup Then
                popupOTRequisition.Show()
            End If
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objApproval = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
        Return False
    End Function

    Private Sub CalculateOTHrs()
        Try
            If mdtOTApproval Is Nothing OrElse mdtOTApproval.Rows.Count <= 0 Then
                Exit Sub
            End If

            For Each dr As DataRow In mdtOTApproval.Rows

                If IsDBNull(dr("requestdate")) = False Then


                End If

            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If txtReasonForAdjustment.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "Adjustment Reason is compulsory information.Please enter Adjustment reason."), Me)
                txtReasonForAdjustment.Focus()
                Return False
            End If

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            If mblnIsWarning Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "Invalid Actual start time or Actual end time.Please check Actual start time or Actual end time."), Me)
                Return False
            End If
            'Pinkal (24-Oct-2019) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            If mblnShowPopup Then
                popupOTRequisition.Show()
            End If
        End Try
        Return False
    End Function


    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Private Function GetDateTimeFromCombos(ByVal dtCombo As DateTime, ByVal intHour As Integer, ByVal intMins As Integer) As DateTime
        Try
            If (intHour > 0) Then
                dtCombo = dtCombo.AddHours(intHour)
            End If
            If (intMins > 0) Then
                dtCombo = dtCombo.AddMinutes(intMins)
            End If
            Return dtCombo
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'Pinkal (24-Oct-2019) -- End


    'Pinkal (07-Nov-2019) -- Start
    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

    Private Sub SendNotificationToEmployee(ByVal xStatusId As Integer)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objOTRequisition As New clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End
        Try

            Dim mintLoginTypeID As Integer = 0
            Dim mintLoginEmployeeID As Integer = 0
            Dim mintUserID As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                mintLoginEmployeeID = 0
                mintUserID = CInt(Session("UserId"))
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                mintUserID = 0
            End If

            objOTRequisition._Userunkid = CInt(Session("UserId"))
            objOTRequisition._WebHostName = CStr(Session("HOST_NAME"))
            objOTRequisition._WebClientIP = CStr(Session("IP_ADD"))
            objOTRequisition._WebFormName = mstrModuleName1
            objOTRequisition._IsFromWeb = True


            Dim strEmployeeIDs As String = String.Join(",", mdtOTApproval.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
            Dim dtSelectedData As DataTable = mdtOTApproval.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True).ToList().CopyToDataTable()

            If xStatusId = 1 Then 'SEND ONLY WHEN OT REQUISITION IS APPROVED


                'Pinkal (29-Jan-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.

                'objOTRequisition.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
                '                                                                 , CInt(cboPeriod.SelectedValue), mintPriority, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID _
                '                                                               , mintUserID, CStr(Session("ArutiSelfServiceURL")), True, mblnIncludeCapApprover, txtReasonForAdjustment.Text)

                objOTRequisition.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
                                                                              , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, mintPriority, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID _
                                                                            , mintUserID, CStr(Session("ArutiSelfServiceURL")), True, mblnIncludeCapApprover, txtReasonForAdjustment.Text)

                'Pinkal (29-Jan-2020) -- End

            End If  'If xStatusId = 1 Then


            For Each strEmpID As String In strEmployeeIDs.Split(CChar(","))
                Dim intEmployeeID As Integer = CInt(strEmpID)

                If xStatusId = 1 Then  'CHECK ONLY WHEN OT REQUISITION IS APPROVED
                    Dim dtApproverList As DataTable = Nothing
                    Dim objApprover As New clsTnaapprover_master

                    'Pinkal (10-Jan-2020) -- Start
                    'Enhancements -  Working on OT Requisistion for NMB.

                    'dtApproverList = objApprover.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                    '                                                 CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                    '                                                 CBool(Session("IsIncludeInactiveEmp")), -1, CStr(intEmployeeID), -1, Nothing, False, "tnapriority > " & mintPriority & " " )

                    dtApproverList = objApprover.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     CBool(Session("IsIncludeInactiveEmp")), -1, CStr(intEmployeeID), -1, Nothing, False, "tnapriority > " & mintPriority & " ", mblnIncludeCapApprover)

                    'Pinkal (10-Jan-2020) -- End


                    If dtApproverList Is Nothing OrElse dtApproverList.Rows.Count > 0 Then Continue For

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    If dtApproverList IsNot Nothing Then dtApproverList.Clear()
                    dtApproverList = Nothing
                    objApprover = Nothing
                    'Pinkal (03-Sep-2020) -- End


                End If  'If xStatusId = 1 Then

                Dim strOTRequisitionIDs As String = String.Join(",", dtSelectedData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID _
                                                                                                      AndAlso x.Field(Of Boolean)("IsGrp") = False).Select(Function(x) x.Field(Of Integer)("otrequisitiontranunkid").ToString).Distinct().ToArray())


                'Pinkal (29-Jan-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.

                'objOTRequisition.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                '                                                                    CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                                    CBool(Session("IsIncludeInactiveEmp")), Session("UserAccessModeSetting").ToString(), _
                '                                                                    CStr(intEmployeeID), strOTRequisitionIDs, CInt(cboPeriod.SelectedValue), xStatusId, _
                '                                                                    mintLoginTypeID, mintLoginEmployeeID, mintUserID, txtReasonForAdjustment.Text)

                objOTRequisition.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                                                    CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                    CBool(Session("IsIncludeInactiveEmp")), Session("UserAccessModeSetting").ToString(), _
                                                                                    CStr(intEmployeeID), strOTRequisitionIDs, dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, xStatusId, _
                                                                                    mintLoginTypeID, mintLoginEmployeeID, mintUserID, txtReasonForAdjustment.Text)

                'Pinkal (29-Jan-2020) -- End

            Next


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtSelectedData IsNot Nothing Then dtSelectedData.Clear()
            dtSelectedData = Nothing
            'Pinkal (03-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objOTRequisition = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (07-Nov-2019) -- End


    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.
    Private Sub SaveOTRequisitionApproval(ByVal sender As Object)
        Try
            If SetValueForApproval(sender) = False Then
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 6, "OT Requisition approved successfully."), Me)
                'SendNotificationToEmployee(1)  'Approved
            End If
            If mblnIsFromEmailLink = True AndAlso Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            Else
                mblnShowPopup = False
                popupOTRequisition.Hide()
                FillOTRequisitionApprovalList(False)
                mblnIsWarning = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Nov-2019) -- End


#End Region

#End Region

#Region " ComboBox's Events "

#Region "OT Requisition Approval List "

    'Protected Sub cboListPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboListPeriod.SelectedIndexChanged
    '    Try
    '        Dim objPeriod As New clscommom_period_Tran

    '        dtpListOTFromDate.SetDate = Nothing
    '        dtpListOTReqToDate.SetDate = Nothing

    '        'If CInt(cboListPeriod.SelectedValue) > 0 Then
    '        '    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboListPeriod.SelectedValue)
    '        '    mdtPeriodStartDate = CDate(objPeriod._TnA_StartDate).Date
    '        '    mdtPeriodEndDate = CDate(objPeriod._TnA_EndDate).Date
    '        'Else
    '        '    mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
    '        '    mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
    '        'End If

    '        dtpListOTFromDate.SetDate = mdtPeriodStartDate.Date
    '        dtpListOTReqToDate.SetDate = mdtPeriodEndDate.Date

    '        'Pinkal (07-Nov-2019) -- Start
    '        'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
    '        If mblnIsFromEmailLink AndAlso Request.QueryString.Count > 0 Then
    '            dtpListOTFromDate.Enabled = False
    '            dtpListOTReqToDate.Enabled = False
    '        End If
    '        'Pinkal (07-Nov-2019) -- End

    '        Dim blnApplyFilter As Boolean = True
    '        Dim blnSelect As Boolean = True
    '        Dim intEmpId As Integer = 0
    '        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
    '            blnSelect = False
    '            intEmpId = CInt(Session("Employeeunkid"))
    '            blnApplyFilter = False
    '        End If


    '        'Pinkal (23-Nov-2019) -- Start
    '        'Enhancement NMB - Working On OT Enhancement for NMB.
    '        Dim dsList As DataSet = Nothing

    '        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then

    '            Dim objEmployee As New clsEmployee_Master
    '            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
    '                                               Session("UserId"), _
    '                                               Session("Fin_year"), _
    '                                               Session("CompanyUnkId"), _
    '                                               mdtPeriodStartDate, _
    '                                               mdtPeriodEndDate, _
    '                                               Session("UserAccessModeSetting"), True, _
    '                                               Session("IsIncludeInactiveEmp"), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

    '            'Dim objEmpAssignOT As New clsassignemp_ot
    '            'Dim dsList As DataSet = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
    '            '                                                     , mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)

    '            'objEmpAssignOT = Nothing

    '            objEmployee = Nothing

    '        ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

    '            Dim objApprover As New clsTnaapprover_master
    '            dsList = objApprover.GetEmployeeFromUser(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '                                                                                                  , mdtPeriodEndDate, Session("IsIncludeInactiveEmp"), CInt(Session("UserId")))
    '            objApprover = Nothing
    '        End If

    '        'Pinkal (23-Nov-2019) -- End

    '        With cboListEmployee
    '            .DataValueField = "employeeunkid"
    '            .DataTextField = "EmpCodeName"
    '            .DataSource = dsList.Tables(0)
    '            .DataBind()
    '            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then cboListEmployee.SelectedValue = "0"
    '        End With
    '        'objEmployee = Nothing

    '        Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
    '        mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

    '        ResetOTRequisitionApprovalList()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("cboListPeriod_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region "OT Requisition Approval"

    Protected Sub cboActualStartTimehr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim item As GridViewRow = CType(CType(sender, DropDownList).NamingContainer, GridViewRow)
            Dim dtActualStartTime As DateTime = CDate(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text)
            dtActualStartTime = CDate(dtActualStartTime.AddHours(CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualStartTime", False, True)).FindControl("cboActualStartTimehr"), DropDownList).SelectedItem.Text).AddMinutes(CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualStartTime", False, True)).FindControl("cboActualStartTimemin"), DropDownList).SelectedValue))

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.

            Dim cboActualStartTimehr As DropDownList = CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualStartTime", False, True)).FindControl("cboActualStartTimehr"), DropDownList)
            Dim cboActualStartTimemin As DropDownList = CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualStartTime", False, True)).FindControl("cboActualStartTimemin"), DropDownList)

            Dim objEmpshiftTran As New clsEmployee_Shift_Tran
            Dim objshiftTran As New clsshift_tran
            Dim mintShiftId As Integer = objEmpshiftTran.GetEmployee_Current_ShiftId(dtActualStartTime.Date, CInt(gvOTRequisitionApproval.DataKeys(item.RowIndex).Values("employeeunkid")))
            objshiftTran.GetShiftTran(mintShiftId)
            If objshiftTran._dtShiftday IsNot Nothing AndAlso objshiftTran._dtShiftday.Rows.Count > 0 Then
                Dim IStartTime As IEnumerable(Of DateTime) = objshiftTran._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(dtActualStartTime.Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("endtime"))
                If IStartTime.Count > 0 Then
                    'Pinkal (01-Apr-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                    'If GetDateTimeFromCombos(dtActualStartTime.Date, IStartTime(0).Hour, IStartTime(0).Minute) > GetDateTimeFromCombos(dtActualStartTime.Date, Convert.ToInt32(cboActualStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualStartTimemin.SelectedItem.ToString)) Then
                    If GetDateTimeFromCombos(dtActualStartTime.Date, IStartTime(0).Hour, IStartTime(0).Minute) >= GetDateTimeFromCombos(dtActualStartTime.Date, Convert.ToInt32(cboActualStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualStartTimemin.SelectedItem.ToString)) Then
                        'Pinkal (01-Apr-2020) -- End
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 3, "Actual Start Time should be greater than shift end time."), Me)
                        mblnIsWarning = True
                        'Pinkal (03-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                        objEmpshiftTran = Nothing
                        objshiftTran = Nothing
                        'Pinkal (03-Sep-2020) -- End
                        Exit Sub
                    End If
                End If
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objEmpshiftTran = Nothing
            objshiftTran = Nothing
            'Pinkal (03-Sep-2020) -- End


            mblnIsWarning = False
            mdtOTApproval.Rows(item.RowIndex)("actualstart_time") = dtActualStartTime
            mdtOTApproval.Rows(item.RowIndex)("actualot_hoursinsecs") = DateDiff(DateInterval.Second, mdtOTApproval.Rows(item.RowIndex)("actualstart_time"), mdtOTApproval.Rows(item.RowIndex)("actualend_time"))

            mdtOTApproval.Rows(item.RowIndex)("actualnight_hoursinsecs") = CalculateNightHrs(mintShiftId, CDate(mdtOTApproval.Rows(item.RowIndex)("actualstart_time")), CDate(mdtOTApproval.Rows(item.RowIndex)("actualend_time")))
            mdtOTApproval.Rows(item.RowIndex)("actualnight_hrs") = CalculateTime(True, CInt(mdtOTApproval.Rows(item.RowIndex)("actualnight_hoursinsecs")))
            mdtOTApproval.AcceptChanges()
            'Pinkal (24-Oct-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mblnShowPopup Then
                popupOTRequisition.Show()
            End If
        End Try
    End Sub

    Protected Sub cboActualEndTimehr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim item As GridViewRow = CType(CType(sender, DropDownList).NamingContainer, GridViewRow)
            Dim dtActualEndTime As DateTime = CDate(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text)
            dtActualEndTime = CDate(dtActualEndTime.AddHours(CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualEndTime", False, True)).FindControl("cboActualEndTimehr"), DropDownList).SelectedItem.Text).AddMinutes(CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualEndTime", False, True)).FindControl("cboActualEndTimemin"), DropDownList).SelectedValue))

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.

            Dim cboActualEndTimehr As DropDownList = CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualEndTime", False, True)).FindControl("cboActualEndTimehr"), DropDownList)
            Dim cboActualEndTimemin As DropDownList = CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualEndTime", False, True)).FindControl("cboActualEndTimemin"), DropDownList)

            Dim dtActualStartTime As DateTime = CDate(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text)
            dtActualStartTime = CDate(dtActualStartTime.AddHours(CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualStartTime", False, True)).FindControl("cboActualStartTimehr"), DropDownList).SelectedItem.Text).AddMinutes(CType(item.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalActualStartTime", False, True)).FindControl("cboActualStartTimemin"), DropDownList).SelectedValue))

            Dim mintShiftId As Integer = 0
            If dtActualStartTime > dtActualEndTime Then
                Dim objEmpshiftTran As New clsEmployee_Shift_Tran
                Dim objshiftTran As New clsshift_tran
                mintShiftId = objEmpshiftTran.GetEmployee_Current_ShiftId(dtActualStartTime.AddDays(1).Date, CInt(gvOTRequisitionApproval.DataKeys(item.RowIndex).Values("employeeunkid")))
                objshiftTran.GetShiftTran(mintShiftId)
                If objshiftTran._dtShiftday IsNot Nothing AndAlso objshiftTran._dtShiftday.Rows.Count > 0 Then
                    Dim IStartTime As IEnumerable(Of DateTime) = objshiftTran._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(dtActualStartTime.AddDays(1).Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("starttime"))
                    If IStartTime.Count > 0 Then
                        If GetDateTimeFromCombos(dtActualStartTime.AddDays(1).Date, IStartTime(0).Hour, IStartTime(0).Minute) <= GetDateTimeFromCombos(dtActualStartTime.AddDays(1).Date, Convert.ToInt32(cboActualEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualEndTimemin.SelectedItem.ToString)) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Sorry, You cannot apply for this OT.Reason : Planned End Time should not be overlap the next day shift start time."), Me)
                            mblnIsWarning = True
                            'Pinkal (03-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                            objEmpshiftTran = Nothing
                            objshiftTran = Nothing
                            'Pinkal (03-Sep-2020) -- End
                            Exit Sub
                        Else
                            dtActualEndTime = dtActualStartTime.AddDays(1).Date.AddHours(cboActualEndTimehr.SelectedItem.Text).AddMinutes(cboActualEndTimehr.SelectedItem.Text)
                        End If
                    End If
                End If
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objEmpshiftTran = Nothing
                objshiftTran = Nothing
                'Pinkal (03-Sep-2020) -- End
            ElseIf dtActualStartTime = dtActualEndTime Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 5, "Sorry, Actual Start Time should not be same as Actual End Time ."), Me)
                mblnIsWarning = True
                Exit Sub
            End If
            mblnIsWarning = False

            mdtOTApproval.Rows(item.RowIndex)("actualend_time") = dtActualEndTime
            mdtOTApproval.Rows(item.RowIndex)("actualot_hoursinsecs") = DateDiff(DateInterval.Second, mdtOTApproval.Rows(item.RowIndex)("actualstart_time"), mdtOTApproval.Rows(item.RowIndex)("actualend_time"))

            mdtOTApproval.Rows(item.RowIndex)("actualnight_hoursinsecs") = CalculateNightHrs(mintShiftId, CDate(mdtOTApproval.Rows(item.RowIndex)("actualstart_time")), CDate(mdtOTApproval.Rows(item.RowIndex)("actualend_time")))
            mdtOTApproval.Rows(item.RowIndex)("actualnight_hrs") = CalculateTime(True, CInt(mdtOTApproval.Rows(item.RowIndex)("actualnight_hoursinsecs")))

            'Pinkal (24-Oct-2019) -- End

            mdtOTApproval.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mblnShowPopup Then
                popupOTRequisition.Show()
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Button's Event"

#Region "OT Requisition Approval List"

    Protected Sub btnListSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListSearch.Click
        Try
            'If CInt(cboListPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Period is compulsory information.please select period to continue.", Me)
            '    cboListPeriod.Focus()
            '    Exit Sub
            'End If
            FillOTRequisitionApprovalList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvOTRequisitionApproverList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select atleast one requisition to do further operation on it."), Me)
                Exit Sub
            End If

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.

            ''Pinkal (09-Mar-2020) -- Start
            ''Enhancement OT Requisition  - OT Requisition Enhancement given by NMB .
            'If mdtOTApprovalList IsNot Nothing AndAlso mdtOTApprovalList.Rows.Count > 0 Then
            '    Dim dRow = mdtOTApprovalList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).ToList()
            '    If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
            '        For Each dr In dRow
            '            dr("ischecked") = False
            '        Next
            '    End If
            'End If
            ''Pinkal (09-Mar-2020) -- End

        
            ''Dim xCount As Integer = -1
            ''For i As Integer = 0 To gRow.Count - 1
            ''    xCount = i
            ''    'Pinkal (12-Oct-2021)-- Start
            ''    'NMB OT Requisition Performance Issue.
            ''    'Dim dRow As DataRow() = mdtOTApprovalList.Select("employeeunkid = " & CInt(gvOTRequisitionApproverList.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) & " AND RDate = '" & gvOTRequisitionApproverList.DataKeys(gRow(xCount).DataItemIndex)("RDate").ToString() & "'")
            ''    Dim dRow As DataRow() = mdtOTApprovalList.Select("employeeunkid = " & CInt(gvOTRequisitionApproverList.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) & " AND RDate = '" & gvOTRequisitionApproverList.DataKeys(gRow(xCount).DataItemIndex)("RDate").ToString() & "' AND mapuserunkid = " & CInt(gvOTRequisitionApproverList.DataKeys(gRow(xCount).DataItemIndex)("mapuserunkid")))
            ''    'Pinkal (12-Oct-2021)-- End
            ''    If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
            ''        For Each dr In dRow
            ''            dr("ischecked") = True
            ''            dr.AcceptChanges()
            ''        Next
            ''    End If
            ''Next

            'Dim mdtUserList As List(Of DataRow) = mdtOTApprovalList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False And x.Field(Of Boolean)("ischecked") = True).ToList()
            'If mdtUserList.Count > 0 Then
            '    For Each dr In mdtUserList
            '        If CInt(Session("UserId")) <> CInt(dr("mapuserunkid")) Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "You can not change status of this OT Requisition. Reason: some of the OT Requistion(s) are not mapped with this user."), Me)
            '            'Pinkal (03-Sep-2020) -- Start
            '            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            '            If mdtUserList IsNot Nothing Then mdtUserList.Clear()
            '            mdtUserList = Nothing
            '            'Pinkal (03-Sep-2020) -- End
            '            Exit Sub
            '        End If
            '    Next
            'End If

            mstrOtRequisitionApprovalIds = String.Join(",", gRow.AsEnumerable().Select(Function(x) gvOTRequisitionApproverList.DataKeys(x.DataItemIndex)("otrequestapprovaltranunkid").ToString()).ToArray())

            Dim mdtUserList As List(Of GridViewRow) = gRow.AsEnumerable().Where(Function(x) CBool(gvOTRequisitionApproverList.DataKeys(x.DataItemIndex)("isGrp")) = False And CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CInt(gvOTRequisitionApproverList.DataKeys(x.DataItemIndex)("mapuserunkid")) <> CInt(Session("UserId"))).ToList()
            If mdtUserList IsNot Nothing AndAlso mdtUserList.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "You can not change status of this OT Requisition. Reason: some of the OT Requistion(s) are not mapped with this user."), Me)
                        If mdtUserList IsNot Nothing Then mdtUserList.Clear()
                        mdtUserList = Nothing
                        Exit Sub
                    End If

            'Pinkal (12-Oct-2021)-- End

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If mdtUserList IsNot Nothing Then mdtUserList.Clear()
            mdtUserList = Nothing
            'Pinkal (03-Sep-2020) -- End



            ''Pinkal (10-Jan-2020) -- Start
            ''Enhancements -  Working on OT Requisistion for NMB.
            'If CInt(Session("AllowApplyOTForEachMonthDay")) > 0 AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) < ConfigParameter._Object._CurrentDateAndTime.Day Then
            '    'If CInt(Session("AllowApplyOTForEachMonthDay")) < ConfigParameter._Object._CurrentDateAndTime.Day Then
            '    'Dim objMaster As New clsMasterData
            '    'Dim mintSelectedPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, mdtPeriodStartDate.Date, 0, Nothing)
            '    'Dim mintCurrentPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date, 0, Nothing)
            '    'If mintSelectedPeriodId <> mintCurrentPeriodId Then
            '    '    Dim objPeriod As New clscommom_period_Tran
            '    '    objPeriod._Periodunkid(Session("Database_Name").ToString()) = mintCurrentPeriodId
            '    '    If objPeriod._TnA_StartDate.Date > mdtPeriodEndDate.Date Then
            '    '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, you cannot do approve/reject ot .Reason : The date set on configuration is already passed."), Me)
            '    '        objPeriod = Nothing
            '    '        Exit Sub
            '    '    End If
            '    '    objPeriod = Nothing
            '    'End If
            '    'objMaster = Nothing

            '    Dim mdtDate As DateTime = New DateTime(mdtPeriodEndDate.AddMonths(1).Year, mdtPeriodEndDate.AddMonths(1).Month, CInt(Session("AllowApplyOTForEachMonthDay")))
            '    If ConfigParameter._Object._CurrentDateAndTime.Date > mdtDate Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, you cannot do approve/reject ot .Reason : The date set on configuration is already passed."), Me)
            '        Exit Sub
            '    End If

            'End If
            ''Pinkal (10-Jan-2020) -- Start

            'Pinkal (29-Jan-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If mdtPeriodStartDate.Date > dtpListOTFromDate.GetDate.Date OrElse mdtPeriodEndDate.Date < dtpListOTReqToDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Sorry, you cannot approve/disapprove this ot requisition(s).Reason : The date set on configuration is already passed."), Me)
                Exit Sub
            End If
            'Pinkal (29-Jan-2020) -- End

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.

            'Dim drRow() As DataRow = mdtOTApprovalList.Select("isGrp = 0 AND ischecked = 1")

            'For Each dR As DataRow In drRow
            '    If CBool(dR.Item("IsGrp")) = False Then

            '        'Pinkal (14-Nov-2019) -- Start
            '        'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            '        If CInt(dR.Item("OTRequisitionStatusId")) = 1 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
            '            Exit Sub
            '        ElseIf CInt(dR.Item("OTRequisitionStatusId")) = 3 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
            '            Exit Sub
            '        ElseIf CInt(dR.Item("OTRequisitionStatusId")) = 6 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already cancelled."), Me)
            '            Exit Sub
            '        End If
            '        'Pinkal (14-Nov-2019) -- End

            '        If CInt(dR.Item("ApprovalStatusId")) = 1 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
            '            Exit Sub
            '        ElseIf CInt(dR.Item("ApprovalStatusId")) = 3 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
            '            Exit Sub
            '        End If

            '        Dim dtList As DataTable = New DataView(mdtOTApprovalList, "IsGrp = 0 AND employeeunkid = " & CInt(dR.Item("employeeunkid")) _
            '                                                 & " AND RDate = '" & dR.Item("RDate").ToString() & "'" _
            '                                                 & " AND tnapriority = " & CInt(dR.Item("tnapriority")), "", _
            '                                                 DataViewRowState.CurrentRows).ToTable()

            '        If dtList.Rows.Count > 0 Then

            '            Dim objLevel As New clsTna_approverlevel_master
            '            objLevel._Tnalevelunkid = mintTnALevelunkid

            '            For i As Integer = 0 To dtList.Rows.Count - 1

            '                If objLevel._Tnapriority > CInt(dtList.Rows(i)("tnapriority")) Then

            '                    'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

            '                    Dim dList As DataTable = New DataView(dtList, "OTRequisitionStatusId <> 3 AND tnalevelunkid = " & CInt(dtList.Rows(i)("tnalevelunkid")) & " AND ApprovalStatusId = 1", "", DataViewRowState.CurrentRows).ToTable

            '                    If dList.Rows.Count > 0 Then Continue For

            '                    'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

            '                    'Pinkal (14-Nov-2019) -- Start
            '                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

            '                    If CInt(dtList.Rows(i)("OTRequisitionStatusId")) <> 3 AndAlso CInt(dtList.Rows(i)("ApprovalStatusId")) = 2 Then
            '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending."), Me)
            '                        Exit Sub

            '                    ElseIf CInt(dtList.Rows(i)("OTRequisitionStatusId")) <> 3 AndAlso CInt(dtList.Rows(i)("ApprovalStatusId")) = 3 Then
            '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
            '                        Exit Sub
            '                    End If

            '                    'Pinkal (14-Nov-2019) -- End

            '                ElseIf objLevel._Tnapriority <= CInt(dtList.Rows(i)("tnapriority")) Then

            '                    'Pinkal (14-Nov-2019) -- Start
            '                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

            '                    If CInt(dtList.Rows(i)("OTRequisitionStatusId")) <> 3 AndAlso CInt(dtList.Rows(i)("ApprovalStatusId")) = 1 Then
            '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
            '                        Exit Sub

            '                    ElseIf CInt(dtList.Rows(i)("OTRequisitionStatusId")) <> 3 AndAlso CInt(dtList.Rows(i)("ApprovalStatusId")) = 3 Then
            '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
            '                        Exit Sub
            '                    End If

            '                    'Pinkal (14-Nov-2019) -- End

            '                End If   ' If objLevel._Tnapriority > CInt(dtList.Rows(i)("tnapriority")) Then

            '            Next


            '            'Pinkal (03-Sep-2020) -- Start
            '            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            '            objLevel = Nothing
            '            'Pinkal (03-Sep-2020) -- End

            '        End If  'If dtList.Rows.Count > 0 Then

            '        'Pinkal (03-Sep-2020) -- Start
            '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            '        If dtList IsNot Nothing Then dtList.Clear()
            '        dtList = Nothing
            '        'Pinkal (03-Sep-2020) -- End

            '    End If   ' If CBool(dR.Item("IsGrp")) = False Then

            'Next


            For Each dR As GridViewRow In gRow

                If CBool(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("IsGrp")) = False Then

                    If CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) = 1 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
                        Exit Sub
                    ElseIf CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) = 3 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                        Exit Sub
                    ElseIf CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) = 6 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already cancelled."), Me)
                        Exit Sub
                    End If

                    If CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("ApprovalStatusId")) = 1 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
                        Exit Sub
                    ElseIf CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("ApprovalStatusId")) = 3 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                        Exit Sub
                    End If




                    'Dim dtList As DataTable = New DataView(mdtOTApprovalList, "IsGrp = 0 AND employeeunkid = " & CInt(dR.Item("employeeunkid")) _
                    '                                         & " AND RDate = '" & dR.Item("RDate").ToString() & "'" _
                    '                                         & " AND tnapriority = " & CInt(dR.Item("tnapriority")), "", _
                    '                                         DataViewRowState.CurrentRows).ToTable()

                    'If dtList.Rows.Count > 0 Then

                        Dim objLevel As New clsTna_approverlevel_master
                        objLevel._Tnalevelunkid = mintTnALevelunkid

                    '    For i As Integer = 0 To dtList.Rows.Count - 1

                    If objLevel._Tnapriority > CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("tnapriority")) Then

                                'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                        'Dim dList As DataTable = New DataView(dtList, "OTRequisitionStatusId <> 3 AND tnalevelunkid = " & CInt(dtList.Rows(i)("tnalevelunkid")) & " AND ApprovalStatusId = 1", "", DataViewRowState.CurrentRows).ToTable
                        Dim gSelectRow = gRow.AsEnumerable().Where(Function(x) CInt(gvOTRequisitionApproverList.DataKeys(x.DataItemIndex)("OTRequisitionStatusId")) <> 3 And _
                                                    CInt(gvOTRequisitionApproverList.DataKeys(x.DataItemIndex)("tnalevelunkid")) = CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("tnalevelunkid")) And _
                                                    CInt(gvOTRequisitionApproverList.DataKeys(x.DataItemIndex)("ApprovalStatusId")) = 1)

                        If gSelectRow IsNot Nothing AndAlso gSelectRow.Count > 0 Then Continue For

                                'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                        If CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) <> 3 AndAlso CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("ApprovalStatusId")) = 2 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending."), Me)
                                    Exit Sub

                        ElseIf CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) <> 3 AndAlso CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("ApprovalStatusId")) = 3 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                                    Exit Sub
                                End If

                    ElseIf objLevel._Tnapriority <= CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("tnapriority")) Then


                        If CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) <> 3 AndAlso CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("ApprovalStatusId")) = 1 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
                                    Exit Sub

                        ElseIf CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("OTRequisitionStatusId")) <> 3 AndAlso CInt(gvOTRequisitionApproverList.DataKeys(dR.DataItemIndex)("ApprovalStatusId")) = 3 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                                    Exit Sub
                                End If

                            End If   ' If objLevel._Tnapriority > CInt(dtList.Rows(i)("tnapriority")) Then

                    ' Next


                        objLevel = Nothing

                    'End If  'If dtList.Rows.Count > 0 Then

                    'If dtList IsNot Nothing Then dtList.Clear()
                    'dtList = Nothing

                End If   ' If CBool(dR.Item("IsGrp")) = False Then

            Next

            'Pinkal (12-Oct-2021)-- End

            dtpFromDate.SetDate = mdtPeriodStartDate.Date
            dtpToDate.SetDate = mdtPeriodEndDate.Date
            dtpFromDate.Enabled = False
            dtpToDate.Enabled = False

            mblnShowPopup = True
            FillPeriod()
            FillApprovalData()
            popupOTRequisition.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtOTApprovalList IsNot Nothing Then
                gvOTRequisitionApproverList.DataSource = mdtOTApprovalList
                gvOTRequisitionApproverList.DataBind()
            End If
        End Try
    End Sub

    Protected Sub btnListReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListReset.Click
        Try
            ResetOTRequisitionApprovalList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            gvOTRequisitionApproverList.DataSource = Nothing
            gvOTRequisitionApproverList.DataBind()
            'Pinkal (12-Oct-2021)-- End

            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            If mblnIsFromEmailLink AndAlso Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            Else

                'Pinkal (11-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	
                'Response.Redirect("~/Userhome.aspx")
                Response.Redirect("~/Userhome.aspx", False)
                'Pinkal (11-Feb-2022) -- End
            End If
            'Pinkal (07-Nov-2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "OT Requisition Approval"

    Protected Sub btnOtApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOtApprove.Click
        Try
            If Validation() = False Then Exit Sub

            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            Dim strCapOTHrsForHODApprovers As String = Session("CapOTHrsForHODApprovers").ToString()

            If strCapOTHrsForHODApprovers.Length > 0 Then
                Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
                Dim intTotalMins As Integer = 0
                If arrCapOTHrsForHODApprovers.Length > 0 Then
                    intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
                End If

                Dim objOTRequisition As New clsOT_Requisition_Tran

                For Each dr As DataRow In mdtOTApproval.Rows
                    Dim xEmployeeId As Integer = CInt(dr("employeeunkid"))

                    'Pinkal (27-Jun-2020) -- Start
                    'Enhancement NMB -   Working on Employee Signature Report.
                    'Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisition.GetEmpTotalOThours(Nothing, Nothing, xEmployeeId.ToString(), Nothing, False, True))
                    'If intTotalMins <= (intAppliedOThours + CalculateTime(False, mdtOTApproval.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId).DefaultIfEmpty.Sum(Function(x) x.Field(Of Integer)("actualot_hoursinsecs")))) Then
                    '    mblnIncludeCapApprover = True
                    'End If

                    Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisition.GetEmpTotalOThours(mdtPeriodStartDate, mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month) - 1).Date, xEmployeeId.ToString(), Nothing, False, True))
                    If intTotalMins <= (intAppliedOThours + CalculateTime(False, mdtOTApproval.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                                                        And x.Field(Of String)("RDate") >= eZeeDate.convertDate(mdtPeriodStartDate.Date) _
                                                                                                                        And x.Field(Of String)("RDate") <= eZeeDate.convertDate(mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month) - 1).Date)) _
                                                                                                                        .DefaultIfEmpty.Sum(Function(x) x.Field(Of Integer)("actualot_hoursinsecs")))) Then
                        mblnIncludeCapApprover = True
                    End If
                    'Pinkal (27-Jun-2020) -- End


                Next
                objOTRequisition = Nothing

                If mblnIncludeCapApprover Then
                    popupConfirmationForOTCap.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 7, "Confirmation")
                    popupConfirmationForOTCap.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 8, "Some of the employee(s) will exceed limit for the OT cap which is configured on configuration.Are you sure you want to approve OT Requisition(s) ?")
                    popupConfirmationForOTCap.Show()
                    Exit Sub
                End If

            End If

            SaveOTRequisitionApproval(btnOtApprove)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mblnShowPopup Then
                popupOTRequisition.Show()
            End If
        End Try
    End Sub

    Protected Sub btnOtDisApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOtDisApprove.Click
        Try
            If Validation() = False Then Exit Sub
            If SetValueForApproval(sender) = False Then
                Exit Sub
            Else
                DisplayMessage.DisplayMessage("OT Requisition disapproved successfully.", Me)
                'Pinkal (07-Nov-2019) -- Start
                'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                'SendNotificationToEmployee(3) 'Reject
                'Pinkal (07-Nov-2019) -- End
            End If

            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            If mblnIsFromEmailLink = True AndAlso Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            Else
                mblnShowPopup = False
                popupOTRequisition.Hide()
                FillOTRequisitionApprovalList(False)
                'Pinkal (24-Oct-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                mblnIsWarning = False
                'Pinkal (24-Oct-2019) -- End
            End If
            'Pinkal (07-Nov-2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnpopupOtClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupOtClose.Click
        Try
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            mdtOTApproval.Rows.Clear()
            mstrOtRequisitionApprovalIds = ""
            'Pinkal (12-Oct-2021)-- End
            mblnShowPopup = False
            popupOTRequisition.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Protected Sub popupConfirmationForOTCap_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmationForOTCap.buttonYes_Click
        Try
            SaveOTRequisitionApproval(btnOtApprove)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Nov-2019) -- End

#End Region

#End Region

#Region "GridView Events"

#Region "OT Requisition Approval List"

    Protected Sub gvOTRequisitionApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOTRequisitionApproverList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If CBool(gvOTRequisitionApproverList.DataKeys(e.Row.RowIndex).Values("IsGrp")) Then

                    'AddGroup(e.Row, gvOTRequisitionApproverList.DataKeys(e.Row.RowIndex).Values("Particulars").ToString(), gvOTRequisitionApproverList)

                    Dim chk As CheckBox = TryCast(e.Row.FindControl("chkSelect"), CheckBox)
                    chk.Visible = False

                    e.Row.Cells(2).Text = gvOTRequisitionApproverList.DataKeys(e.Row.RowIndex).Values("Particulars").ToString()
                    e.Row.Cells(2).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Row.Font.Bold = True

                    For i As Integer = 3 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                Else
                    Dim chkSelect As CheckBox = CType(e.Row.FindControl("chkselect"), CheckBox)
                    chkSelect.Checked = CBool(gvOTRequisitionApproverList.DataKeys(e.Row.RowIndex).Values("ischecked"))

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotapprovaldate", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotapprovaldate", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotapprovaldate", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotapprovaldate", False, True)).Text).ToShortDateString()
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedstart_time", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedstart_time", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedstart_time", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedstart_time", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedstart_time", False, True)).Text).ToString("HH:mm")
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedend_time", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedend_time", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedend_time", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedend_time", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqplannedend_time", False, True)).Text).ToString("HH:mm")
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualstart_time", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualstart_time", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualstart_time", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualstart_time", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualstart_time", False, True)).Text).ToString("HH:mm")
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualend_time", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualend_time", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualend_time", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualend_time", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqactualend_time", False, True)).Text).ToString("HH:mm")
                    End If

                    'Pinkal (12-Oct-2021)-- Start
                    'NMB OT Requisition Performance Issue.
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqstatus", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqstatus", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqstatus", False, True)).Text = info1.ToTitleCase(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproverList, "colhotreqstatus", False, True)).Text.ToLower())
                    End If
                    info1 = Nothing
                    'Pinkal (12-Oct-2021)-- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "OT Requisition Approval"

    Protected Sub gvOTRequisitionApproval_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOTRequisitionApproval.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If CBool(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("IsGrp")) Then
                    e.Row.FindControl("cboActualStartTimehr").Visible = False
                    e.Row.FindControl("lblActualStarTimecolon").Visible = False
                    e.Row.FindControl("cboActualStartTimemin").Visible = False

                    e.Row.FindControl("cboActualEndTimehr").Visible = False
                    e.Row.FindControl("lblActualEndTimecolon").Visible = False
                    e.Row.FindControl("cboActualEndTimemin").Visible = False

                    AddApprovalGroup(e.Row, gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("Particulars").ToString(), gvOTRequisitionApproval)
                Else
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedstart_time", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedstart_time", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedstart_time", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedstart_time", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedstart_time", False, True)).Text).ToString("HH:mm")
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedend_time", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedend_time", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedend_time", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedend_time", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedend_time", False, True)).Text).ToString("HH:mm")
                    End If

                    Dim mdtRequestDate As DateTime = Nothing

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text).ToShortDateString()
                        mdtRequestDate = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text)
                    End If

                    Dim cboActualStartTimehr As DropDownList = CType(e.Row.FindControl("cboActualStartTimehr"), DropDownList)
                    Dim cboActualEndTimehr As DropDownList = CType(e.Row.FindControl("cboActualEndTimehr"), DropDownList)
                    For I As Integer = 0 To 23
                        Dim strHour As String = I.ToString
                        If (Len(strHour) = 1) Then
                            strHour = "0" + strHour
                        End If
                        cboActualStartTimehr.Items.Add(strHour)
                        cboActualEndTimehr.Items.Add(strHour)
                    Next

                    Dim cboActualStartTimemin As DropDownList = CType(e.Row.FindControl("cboActualStartTimemin"), DropDownList)
                    Dim cboActualEndTimemin As DropDownList = CType(e.Row.FindControl("cboActualEndTimemin"), DropDownList)
                    For I As Integer = 0 To 59
                        Dim strMins As String = I.ToString
                        If (Len(strMins) = 1) Then
                            strMins = "0" + strMins
                        End If
                        cboActualStartTimemin.Items.Add(strMins)
                        cboActualEndTimemin.Items.Add(strMins)
                    Next

                    'If IsDBNull(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("actualstart_time")) = False Then
                    Dim mdttDateTime As DateTime = CDate(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("actualstart_time"))
                    cboActualStartTimehr.SelectedValue = mdttDateTime.ToString("HH")
                    cboActualStartTimemin.SelectedValue = mdttDateTime.ToString("mm")

                    mdttDateTime = Nothing
                    mdttDateTime = CDate(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("actualend_time"))
                    cboActualEndTimehr.SelectedValue = mdttDateTime.ToString("HH")
                    cboActualEndTimemin.SelectedValue = mdttDateTime.ToString("mm")

                    mdtOTApproval.Rows(e.Row.RowIndex)("actualot_hoursinsecs") = DateDiff(DateInterval.Second, CDate(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("actualstart_time")), CDate(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("actualend_time")))

                    'Else

                    'Dim mdtRDate As DateTime = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotrequestdate", False, True)).Text)
                    'Dim mintSelectedPriority As Integer = CInt(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("tnapriority"))

                    'Dim dr As List(Of DataRow) = mdtOTApprovalList.AsEnumerable().Where(Function(x) x.Field(Of String)("RDate") = eZeeDate.convertDate(mdtRDate).ToString() And x.Field(Of Integer)("tnapriority") < mintSelectedPriority And x.Field(Of Integer)("approvalstatusId") = 1).ToList()

                    'If dr.Count > 0 Then
                    '    Dim mdtActualStartTime As Date = CDate(dr(0)("actualstart_time"))
                    '    Dim mdtActualEndTime As Date = CDate(dr(0)("actualend_time"))

                    '    cboActualStartTimehr.SelectedValue = Format(mdtActualStartTime, "HH")
                    '    cboActualStartTimemin.SelectedValue = Format(mdtActualStartTime, "mm")

                    '    cboActualEndTimehr.SelectedValue = Format(mdtActualEndTime, "HH")
                    '    cboActualEndTimemin.SelectedValue = Format(mdtActualEndTime, "mm")

                    '    If mdtOTApproval IsNot Nothing AndAlso mdtOTApproval.Rows.Count > 0 Then
                    '        mdtOTApproval.Rows(e.Row.RowIndex)("actualstart_time") = mdtActualStartTime
                    '        mdtOTApproval.Rows(e.Row.RowIndex)("actualend_time") = mdtActualEndTime
                    '        mdtOTApproval.Rows(e.Row.RowIndex)("actualot_hoursinsecs") = DateDiff(DateInterval.Second, mdtActualStartTime, mdtActualEndTime)
                    '        mdtOTApproval.AcceptChanges()
                    '    End If

                    'Else
                    '    Dim mdtPlannedStartTime As Date = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedstart_time", False, True)).Text)
                    '    Dim mdtPlannedEndTime As Date = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionApproval, "colhApprovalotreqplannedend_time", False, True)).Text)

                    '    cboActualStartTimehr.SelectedValue = Format(mdtPlannedStartTime, "HH")
                    '    cboActualStartTimemin.SelectedValue = Format(mdtPlannedStartTime, "mm")

                    '    cboActualEndTimehr.SelectedValue = Format(mdtPlannedEndTime, "HH")
                    '    cboActualEndTimemin.SelectedValue = Format(mdtPlannedEndTime, "mm")

                    '    If mdtOTApproval IsNot Nothing AndAlso mdtOTApproval.Rows.Count > 0 Then
                    '        mdtOTApproval.Rows(e.Row.RowIndex)("actualstart_time") = mdtPlannedStartTime
                    '        mdtOTApproval.Rows(e.Row.RowIndex)("actualend_time") = mdtPlannedEndTime
                    '        mdtOTApproval.Rows(e.Row.RowIndex)("actualot_hoursinsecs") = DateDiff(DateInterval.Second, mdtPlannedStartTime, mdtPlannedEndTime)
                    '        mdtOTApproval.AcceptChanges()
                    '    End If

                    'End If  ' If dr.Count > 0 Then

                    ' End If 'If IsDBNull(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("actualstart_time")) = False Then

                End If '  If CBool(gvOTRequisitionApproval.DataKeys(e.Row.RowIndex).Values("IsGrp")) Then

            End If  '  If e.Row.RowType = DataControlRowType.DataRow Then
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " CheckBox's Events "

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            If mdtOTApprovalList.Rows.Count <= 1 Then Exit Sub '1 STANDS HIDDEN BLANK ROW.

            For Each item As GridViewRow In gvOTRequisitionApproverList.Rows
                If item.RowType = DataControlRowType.DataRow Then
                    CType(item.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                    mdtOTApprovalList.Rows(item.RowIndex)("ischecked") = chkSelectAll.Checked
                End If
            Next
            mdtOTApprovalList.AcceptChanges()
            gvOTRequisitionApproverList.DataSource = mdtOTApprovalList
            gvOTRequisitionApproverList.DataBind()

            CType(gvOTRequisitionApproverList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = CType(sender, CheckBox).Checked

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If gvOTRequisitionApproverList.Rows.Count <= 0 Then Exit Sub

            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)

            Dim dsRow As DataRow() = mdtOTApprovalList.Select("otrequestapprovaltranunkid = " & CInt(gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("otrequestapprovaltranunkid")))
            If dsRow.Length > 0 Then
                For Each dR In dsRow
                    dR.Item("ischecked") = chkSelect.Checked
                Next
            End If
            mdtOTApprovalList.AcceptChanges()
            gvOTRequisitionApproverList.DataSource = mdtOTApprovalList
            gvOTRequisitionApproverList.DataBind()

            Dim drRow As DataRow() = mdtOTApprovalList.Select("IsGrp=0 AND employeeunkid=" & CInt(gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("employeeunkid")) & " AND RDate='" & gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("RDate").ToString() & "'")

            Dim dRow As DataRow() = mdtOTApprovalList.Select("IsGrp=0 AND ischecked=0 AND employeeunkid=" & CInt(gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("employeeunkid")) & " AND RDate='" & gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("RDate").ToString() & "'")

            Dim gRow As DataRow() = mdtOTApprovalList.Select("IsGrp=1 AND employeeunkid=" & CInt(gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("employeeunkid")) & " AND RDate='" & gvOTRequisitionApproverList.DataKeys(item.RowIndex).Values("RDate").ToString() & "'")

            If gRow.Length <= 0 Then Exit Sub

            If drRow.Length = dRow.Length Then
                gRow(0).Item("ischecked") = False
            Else
                gRow(0).Item("ischecked") = True
            End If
            gRow(0).AcceptChanges()


            Dim xSelectedCount As Integer = mdtOTApprovalList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False And x.Field(Of Boolean)("ischecked") = True).DefaultIfEmpty().Count
            Dim xTotalCount As Integer = mdtOTApprovalList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False).DefaultIfEmpty().Count
            If xSelectedCount = xTotalCount Then
                CType(gvOTRequisitionApproverList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DateControl Events"

#Region "OT Requisition Approval List "

    Protected Sub dtpListOTFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpListOTFromDate.TextChanged, dtpListOTReqToDate.TextChanged
        Try
            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            'If Session("AllowApplyOTForEachMonthDay") IsNot Nothing AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) > 0 Then
            'mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, CInt(Session("AllowApplyOTForEachMonthDay")) + 1)
            If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
                mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                If mdtPeriodStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
                    'mdtPeriodStartDate = mdtPeriodStartDate.AddDays(-31)
                    mdtPeriodStartDate = mdtPeriodStartDate.AddDays(-1 * CInt(Session("OTTenureDays")) + 1)
                Else
                    Dim mdtTempDate As DateTime = mdtPeriodStartDate.AddMonths(-1)
                    If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
                        mdtPeriodStartDate = mdtTempDate
                    End If
                End If
                mdtPeriodEndDate = mdtPeriodStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
                'Pinkal (02-Jun-2020) -- End
            Else
                mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                mdtPeriodEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
            End If

            Dim dsList As DataSet = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then

                Dim blnApplyFilter As Boolean = True
                Dim blnSelect As Boolean = True
                Dim intEmpId As Integer = 0
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    blnSelect = False
                    intEmpId = CInt(Session("Employeeunkid"))
                    blnApplyFilter = False
                End If


                Dim objEmployee As New clsEmployee_Master
                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                   Session("UserId"), _
                                                   Session("Fin_year"), _
                                                   Session("CompanyUnkId"), _
                                                   mdtPeriodStartDate, _
                                                   mdtPeriodEndDate, _
                                                   Session("UserAccessModeSetting"), True, _
                                                   Session("IsIncludeInactiveEmp"), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

                objEmployee = Nothing

            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                Dim objApprover As New clsTnaapprover_master
                dsList = objApprover.GetEmployeeFromUser(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                      , mdtPeriodEndDate, Session("IsIncludeInactiveEmp"), CInt(Session("UserId")))
                objApprover = Nothing
            End If

            With cboListEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then cboListEmployee.SelectedValue = "0"
            End With

            Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            ResetOTRequisitionApprovalList()

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If lstIDs IsNot Nothing Then lstIDs.Clear()
            lstIDs = Nothing

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCaption.ID, Me.lblCaption.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblListPeriod.ID, Me.lblListPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblListOTReqFromDate.ID, Me.lblListOTReqFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblRequisitionStatus.ID, Me.lblRequisitionStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblListEmployee.ID, Me.lblListEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblListOTReqToDate.ID, Me.lblListOTReqToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnListSearch.ID, Me.btnListSearch.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnListReset.ID, Me.btnListReset.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnChangeStatus.ID, Me.btnChangeStatus.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(1).FooterText, Me.gvOTRequisitionApproverList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(2).FooterText, Me.gvOTRequisitionApproverList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(3).FooterText, Me.gvOTRequisitionApproverList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(4).FooterText, Me.gvOTRequisitionApproverList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(5).FooterText, Me.gvOTRequisitionApproverList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(6).FooterText, Me.gvOTRequisitionApproverList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(7).FooterText, Me.gvOTRequisitionApproverList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(8).FooterText, Me.gvOTRequisitionApproverList.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(9).FooterText, Me.gvOTRequisitionApproverList.Columns(9).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(10).FooterText, Me.gvOTRequisitionApproverList.Columns(10).HeaderText)
            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionApproverList.Columns(11).FooterText, Me.gvOTRequisitionApproverList.Columns(11).HeaderText)
            'Pinkal (27-Feb-2020) -- End



            ''Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.LblOTApprovalTitle.ID, Me.LblOTApprovalTitle.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.LblPeriod.ID, Me.LblPeriod.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.LblPeriod.ID, Me.LblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblReasonForAdjustment.ID, Me.lblReasonForAdjustment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnOtApprove.ID, Me.btnOtApprove.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnOtDisApprove.ID, Me.btnOtDisApprove.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnpopupOtClose.ID, Me.btnpopupOtClose.Text.Trim.Replace("&", ""))


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(0).FooterText, Me.gvOTRequisitionApproval.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(1).FooterText, Me.gvOTRequisitionApproval.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(2).FooterText, Me.gvOTRequisitionApproval.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(3).FooterText, Me.gvOTRequisitionApproval.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(4).FooterText, Me.gvOTRequisitionApproval.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(5).FooterText, Me.gvOTRequisitionApproval.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(6).FooterText, Me.gvOTRequisitionApproval.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(7).FooterText, Me.gvOTRequisitionApproval.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(8).FooterText, Me.gvOTRequisitionApproval.Columns(8).HeaderText)
            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvOTRequisitionApproval.Columns(9).FooterText, Me.gvOTRequisitionApproval.Columns(9).HeaderText)
            'Pinkal (27-Feb-2020) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)
            'Me.lblListPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblListPeriod.ID, Me.lblListPeriod.Text)
            Me.lblListOTReqFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblListOTReqFromDate.ID, Me.lblListOTReqFromDate.Text)
            Me.lblRequisitionStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRequisitionStatus.ID, Me.lblRequisitionStatus.Text)
            Me.lblListEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblListEmployee.ID, Me.lblListEmployee.Text)
            Me.lblListOTReqToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblListOTReqToDate.ID, Me.lblListOTReqToDate.Text)
            Me.chkMyApprovals.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
            Me.btnListSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnListSearch.ID, Me.btnListSearch.Text.Trim.Replace("&", ""))
            Me.btnListReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnListReset.ID, Me.btnListReset.Text.Trim.Replace("&", ""))
            Me.btnChangeStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnChangeStatus.ID, Me.btnChangeStatus.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Me.gvOTRequisitionApproverList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(1).FooterText, Me.gvOTRequisitionApproverList.Columns(1).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(2).FooterText, Me.gvOTRequisitionApproverList.Columns(2).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(3).FooterText, Me.gvOTRequisitionApproverList.Columns(3).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(4).FooterText, Me.gvOTRequisitionApproverList.Columns(4).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(5).FooterText, Me.gvOTRequisitionApproverList.Columns(5).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(6).FooterText, Me.gvOTRequisitionApproverList.Columns(6).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(7).FooterText, Me.gvOTRequisitionApproverList.Columns(7).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(8).FooterText, Me.gvOTRequisitionApproverList.Columns(8).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(9).FooterText, Me.gvOTRequisitionApproverList.Columns(9).HeaderText)
            Me.gvOTRequisitionApproverList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(10).FooterText, Me.gvOTRequisitionApproverList.Columns(10).HeaderText)

            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Me.gvOTRequisitionApproverList.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproverList.Columns(11).FooterText, Me.gvOTRequisitionApproverList.Columns(11).HeaderText)
            'Pinkal (27-Feb-2020) -- End



            ''Language.setLanguage(mstrModuleName1)
            Me.LblOTApprovalTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.LblOTApprovalTitle.ID, Me.LblOTApprovalTitle.Text)
            'Me.LblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.LblPeriod.ID, Me.LblPeriod.Text)
            'Me.LblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.lblReasonForAdjustment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblReasonForAdjustment.ID, Me.lblReasonForAdjustment.Text)
            Me.btnOtApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnOtApprove.ID, Me.btnOtApprove.Text.Trim.Replace("&", ""))
            Me.btnOtDisApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnOtDisApprove.ID, Me.btnOtDisApprove.Text.Trim.Replace("&", ""))
            Me.btnpopupOtClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnpopupOtClose.ID, Me.btnpopupOtClose.Text.Trim.Replace("&", ""))


            Me.gvOTRequisitionApproval.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(0).FooterText, Me.gvOTRequisitionApproval.Columns(0).HeaderText)
            Me.gvOTRequisitionApproval.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(1).FooterText, Me.gvOTRequisitionApproval.Columns(1).HeaderText)
            Me.gvOTRequisitionApproval.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(2).FooterText, Me.gvOTRequisitionApproval.Columns(2).HeaderText)
            Me.gvOTRequisitionApproval.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(3).FooterText, Me.gvOTRequisitionApproval.Columns(3).HeaderText)
            Me.gvOTRequisitionApproval.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(4).FooterText, Me.gvOTRequisitionApproval.Columns(4).HeaderText)
            Me.gvOTRequisitionApproval.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(5).FooterText, Me.gvOTRequisitionApproval.Columns(5).HeaderText)
            Me.gvOTRequisitionApproval.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(6).FooterText, Me.gvOTRequisitionApproval.Columns(6).HeaderText)
            Me.gvOTRequisitionApproval.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(7).FooterText, Me.gvOTRequisitionApproval.Columns(7).HeaderText)
            Me.gvOTRequisitionApproval.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(8).FooterText, Me.gvOTRequisitionApproval.Columns(8).HeaderText)

            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Me.gvOTRequisitionApproval.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionApproval.Columns(9).FooterText, Me.gvOTRequisitionApproval.Columns(9).HeaderText)
            'Pinkal (27-Feb-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Please select atleast one requisition to do further operation on it.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 8, "You can not change status of this OT Requisition. Reason: some of the OT Requistion(s) are not mapped with this user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 10, "There is no any pending OT Requisition application for approval.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 11, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 12, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 13, "You can't change checked transaction(s) detail. Reason: Some of the transaction(s) are already cancelled.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 14, "Sorry, you cannot approve/disapprove this ot requisition(s).Reason : The date set on configuration is already passed.")


            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 1, "Adjustment Reason is compulsory information.Please enter Adjustment reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 2, "Invalid Actual start time or Actual end time.Please check Actual start time or Actual end time.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 3, "Actual Start Time should be greater than shift end time.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 4, "Sorry, You cannot apply for this OT.Reason : Planned End Time should not be overlap the next day shift start time.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 5, "Sorry, Actual Start Time should not be same as Actual End Time .")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 6, "OT Requisition approved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 7, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 8, "Some of the employee(s) will exceed limit for the OT cap which is configured on configuration.Are you sure you want to approve OT Requisition(s) ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
