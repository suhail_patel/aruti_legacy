﻿<%@ Page Title="Cancel OT Requisition" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="OT_CancelRequisition.aspx.vb" Inherits="TnA_OT_Requisition_OT_CancelRequisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">


        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

     
        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

            
    </script>

    <%--<input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />--%>

   <%-- <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>--%>

        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Cancel OT Requisition"></asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </h2>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="true" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="true"></uc1:DateCtrl>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-sm-6 col-md-12 col-xs-12">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 300px">
                                                <asp:GridView ID="gvOTRequisitionList" runat="server" DataKeyNames="otrequisitiontranunkid, empcodename,employeeunkid,ischecked,IsGrp"
                                                    AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" ShowFooter="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="objdgcolhSelect">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAllSelect" runat="server"  CssClass="filled-in" Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server"  CssClass="filled-in" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Employee" DataField="empcodename" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhotreqemp" ItemStyle-Width="200px" />
                                                        <asp:BoundField HeaderText="Request Date" DataField="requestdate" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhotrequestdate" ItemStyle-Width="100px" />
                                                        <asp:BoundField HeaderText="Planned Start Time" DataField="plannedstart_time" ItemStyle-VerticalAlign="Top"
                                                            ItemStyle-Width="140px" FooterText="colhotreqplannedstart_time" />
                                                        <asp:BoundField HeaderText="Planned End Time" DataField="plannedend_time" ItemStyle-VerticalAlign="Top"
                                                            ItemStyle-Width="140px" FooterText="colhotreqplannedend_time" />
                                                        <asp:BoundField HeaderText="Planned OT Hours" DataField="PlannedworkedDuration" ItemStyle-VerticalAlign="Top"
                                                            ItemStyle-Width="130px" FooterText="colhotreqplannedot_hours" />
                                                        <asp:BoundField HeaderText="Actual Start Time" DataField="actualstart_time" ItemStyle-VerticalAlign="Top"
                                                            ItemStyle-Width="140px" FooterText="colhotreqactualstart_time" />
                                                        <asp:BoundField HeaderText="Actual End Time" DataField="actualend_time" ItemStyle-VerticalAlign="Top"
                                                            ItemStyle-Width="140px" FooterText="colhotreqactualend_time" />
                                                        <asp:BoundField HeaderText="Actual OT Hours" DataField="ActualworkedDuration" ItemStyle-VerticalAlign="Top"
                                                            ItemStyle-Width="130px" FooterText="colhotreqactualot_hours" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblCancelReason" runat="server" Text="Reason" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtCancelReason" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc3:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
