﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization

Partial Class TnA_OT_Requisition_OTRequisitionApprLevel
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmOTRequisitionApproverLevelList"
    Private ReadOnly mstrModuleName1 As String = "frmOTRequisitionApproverLevelAddEdit"
    Private mintLevelmstid As Integer
    Private blnpopupOTRequiApprover As Boolean = False

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objclsTna_approverlevel_master As New clsTna_approverlevel_master
    'Pinkal (03-Sep-2020) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                If CBool(Session("AllowToViewOTRequisitionApproverLevel")) Then
                    FillList(False)
                Else
                    FillList(True)

                End If
                SetVisibility()

            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If

                If ViewState("blnpopupOTRequiApprover") IsNot Nothing Then
                    blnpopupOTRequiApprover = Convert.ToBoolean(ViewState("blnpopupOTRequiApprover").ToString())
                End If

                If blnpopupOTRequiApprover Then
                    popupOTRequiApproverLevel.Show()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupOTRequiApprover") = blnpopupOTRequiApprover
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsOTRequiApproverList As New DataSet
        Dim strfilter As String = ""

        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTna_approverlevel_master As clsTna_approverlevel_master
        'Pinkal (03-Sep-2020) -- End
        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTna_approverlevel_master = New clsTna_approverlevel_master
            'Pinkal (03-Sep-2020) -- End

            dsOTRequiApproverList = objclsTna_approverlevel_master.GetList("List", True, False, strfilter)

            If dsOTRequiApproverList.Tables(0).Rows.Count <= 0 Then
                dsOTRequiApproverList = objclsTna_approverlevel_master.GetList("List", True, True, "1 = 2")
                isblank = True
            End If

            GvApprLevelList.DataSource = dsOTRequiApproverList.Tables("List")
            GvApprLevelList.DataBind()
            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTna_approverlevel_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try

    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Visible = CBool(Session("AllowToAddOTRequisitionApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objclsTna_approverlevel_master As clsTna_approverlevel_master)
        'Pinkal (03-Sep-2020) -- End
        Try

            objclsTna_approverlevel_master._Tnalevelunkid = mintLevelmstid
            objclsTna_approverlevel_master._Tnatypeid = enTnAApproverType.OT_Requisition_Approver
            objclsTna_approverlevel_master._Tnalevelcode = txtlevelcode.Text.Trim
            objclsTna_approverlevel_master._Tnalevelname = txtlevelname.Text.Trim
            objclsTna_approverlevel_master._Tnapriority = Convert.ToInt32(txtlevelpriority.Text)

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Call SetAtValue()
            Call SetAtValue(objclsTna_approverlevel_master)
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetAtValue()
    Private Sub SetAtValue(ByVal objclsTna_approverlevel_master As clsTna_approverlevel_master)
        'Pinkal (03-Sep-2020) -- End
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsTna_approverlevel_master._AuditUserId = CInt(Session("UserId"))
            End If
            objclsTna_approverlevel_master._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objclsTna_approverlevel_master._ClientIP = CStr(Session("IP_ADD"))
            objclsTna_approverlevel_master._LoginEmployeeunkid = -1
            objclsTna_approverlevel_master._HostName = CStr(Session("HOST_NAME"))
            objclsTna_approverlevel_master._FormName = mstrModuleName1
            objclsTna_approverlevel_master._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTna_approverlevel_master As clsTna_approverlevel_master
        'Pinkal (03-Sep-2020) -- End
        Try
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTna_approverlevel_master = New clsTna_approverlevel_master
            'Pinkal (03-Sep-2020) -- End
            If mintLevelmstid > 0 Then
                objclsTna_approverlevel_master._Tnalevelunkid = mintLevelmstid
            End If
            txtlevelcode.Text = objclsTna_approverlevel_master._Tnalevelcode
            txtlevelname.Text = objclsTna_approverlevel_master._Tnalevelname
            txtlevelpriority.Text = objclsTna_approverlevel_master._Tnapriority.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTna_approverlevel_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub cleardata()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            popupOTRequiApproverLevel.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnnew_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSaveOTRequiApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOTRequiApprover.Click
        Dim blnFlag As Boolean = False
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTna_approverlevel_master As clsTna_approverlevel_master
        'Pinkal (03-Sep-2020) -- End
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Call SetValue()
            objclsTna_approverlevel_master = New clsTna_approverlevel_master
            Call SetValue(objclsTna_approverlevel_master)
            'Pinkal (03-Sep-2020) -- End

            If mintLevelmstid > 0 Then
                blnFlag = objclsTna_approverlevel_master.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objclsTna_approverlevel_master.Insert()
            End If

            If blnFlag = False And objclsTna_approverlevel_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsTna_approverlevel_master._Message, Me)
                blnpopupOTRequiApprover = True
                popupOTRequiApproverLevel.Show()
                Exit Sub
            Else
                cleardata()
                FillList(False)
                mintLevelmstid = 0
                blnpopupOTRequiApprover = False
                popupOTRequiApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTna_approverlevel_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValue()
            blnpopupOTRequiApprover = True
            popupOTRequiApproverLevel.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)

        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTna_approverlevel_master As clsTna_approverlevel_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            'GetValue()

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTna_approverlevel_master = New clsTna_approverlevel_master
            'Pinkal (03-Sep-2020) -- End
            If objclsTna_approverlevel_master.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Are You sure you want to delete this Approver Level ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTna_approverlevel_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTna_approverlevel_master As clsTna_approverlevel_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim blnFlag As Boolean = False

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            '  SetAtValue()
            objclsTna_approverlevel_master = New clsTna_approverlevel_master
            SetAtValue(objclsTna_approverlevel_master)
            'Pinkal (03-Sep-2020) -- End

            objclsTna_approverlevel_master._FormName = mstrModuleName

            blnFlag = objclsTna_approverlevel_master.Delete(mintLevelmstid)

            If blnFlag = False And objclsTna_approverlevel_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsTna_approverlevel_master._Message, Me)
                Exit Sub
            Else
                FillList(False)
                mintLevelmstid = 0
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTna_approverlevel_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnCloseOTRequiApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOTRequiApprover.Click
        Try
            cleardata()
            blnpopupOTRequiApprover = False
            popupOTRequiApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("tnalevelunkid").ToString) > 0 Then

                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                        lnkedit.Visible = CBool(Session("AllowToEditOTRequisitionApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteOTRequisitionApproverLevel"))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvApprLevelList.Columns(0).FooterText, GvApprLevelList.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvApprLevelList.Columns(1).FooterText, GvApprLevelList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))


            ''Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lbllevelname.ID, Me.lbllevelname.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnSaveOTRequiApprover.ID, Me.btnSaveOTRequiApprover.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnCloseOTRequiApprover.ID, Me.btnCloseOTRequiApprover.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPageHeader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            GvApprLevelList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvApprLevelList.Columns(0).FooterText, GvApprLevelList.Columns(0).HeaderText)
            GvApprLevelList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvApprLevelList.Columns(1).FooterText, GvApprLevelList.Columns(1).HeaderText)
            GvApprLevelList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            GvApprLevelList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            GvApprLevelList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))


            ''Language.setLanguage(mstrModuleName1)
            Me.lblCancelText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Me.lbllevelcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Me.lbllevelname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lbllevelname.ID, Me.lbllevelname.Text)
            Me.lbllevelpriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)

            Me.btnSaveOTRequiApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnSaveOTRequiApprover.ID, Me.btnSaveOTRequiApprover.Text.Replace("&", ""))
            Me.btnCloseOTRequiApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnCloseOTRequiApprover.ID, Me.btnCloseOTRequiApprover.Text.Replace("&", ""))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Are You sure you want to delete this Approver Level ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
