﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
#End Region
Partial Class TnA_OT_Requisition_OTApproverMigration
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objApproverMaster As New clsTnaapprover_master
    'Private objApproverTran As New clsTnaapprover_Tran
    'Pinkal (03-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmOTRequisitionApproverMigration"
    Private mintOldApproverId As Integer = 0
    Private mintOldMapUserId As Integer = 0
    Private mintNewApproverId As Integer = 0
    Private mintNewMapUserId As Integer = 0
    Private mblnOldExternalApprover As Boolean = False
    Private mblnNewExternalApprover As Boolean = False

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                'objApproverMaster = New clsTnaapprover_master
                'objApproverTran = New clsTnaapprover_Tran
                'Pinkal (03-Sep-2020) -- End

                Fillcombo()
                FillOldApproverEmployee(mintOldApproverId)
                FillAssigneedEmployee(mintNewApproverId)
            Else
                mintOldApproverId = CInt(Me.ViewState("OldApproverId"))
                mintNewApproverId = CInt(Me.ViewState("NewApproverId"))
                mintOldMapUserId = CInt(Me.ViewState("OldMapUserId"))
                mintNewMapUserId = CInt(Me.ViewState("NewMapUserId"))
                mblnOldExternalApprover = CBool(Me.ViewState("OldExternalApprover"))
                mblnNewExternalApprover = CBool(Me.ViewState("NewExternalApprover"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("OldApproverId") = mintOldApproverId
            Me.ViewState("NewApproverId") = mintNewApproverId
            Me.ViewState("OldMapUserId") = mintOldMapUserId
            Me.ViewState("NewMapUserId") = mintNewMapUserId
            Me.ViewState("OldExternalApprover") = mblnOldExternalApprover
            Me.ViewState("NewExternalApprover") = mblnNewExternalApprover
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub Fillcombo()

        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverMaster As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End

        Try

            Dim mstrFilter As String = ""
            mstrFilter = "tnaapprover_master.isotcap_hod = " & IIf(chkCapApprovers.Checked, "1", "0").ToString()


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.



            'Dim dsList As DataSet = objApproverMaster.GetList("List", CStr(Session("Database_Name")), enTnAApproverType.OT_Requisition_Approver _
            '                                                                       , Session("EmployeeAsOnDate").ToString(), True, True, Nothing, mstrFilter, 0, chkCapApprovers.Checked)

            Dim dsList As DataSet = objApproverMaster.GetList("List", CStr(Session("Database_Name")), enTnAApproverType.OT_Requisition_Approver _
                                                                       , Session("EmployeeAsOnDate").ToString(), chkShowInactiveApprovers.Checked, True, Nothing, mstrFilter, 0, chkCapApprovers.Checked)

            'Gajanan [23-May-2020] -- End

            If dsList IsNot Nothing Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("approveremployeeunkid") = 0
                drRow("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsTnaapprover_master", 4, "Select")
                dsList.Tables(0).Rows.InsertAt(drRow, 0)
            End If

            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "approveremployeeunkid", "name", "isexternalapprover")

            With cboOldApprover
                .DataTextField = "name"
                .DataValueField = "approveremployeeunkid"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            cboOldApprover_SelectedIndexChanged(cboOldApprover, New EventArgs())


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objApproverMaster = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillAssigneedEmployee(ByVal xApproverTranId As Integer)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverTran As New clsTnaapprover_Tran
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim mblnblank As Boolean = False
            objApproverTran._TnAMappingUnkId = xApproverTranId
            objApproverTran._EmployeeAsonDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'objApproverTran.Get_Data()
            objApproverTran.Get_Data(Nothing, Session("Database_Name").ToString(), chkShowInActiveEmployees.Checked)
            'Gajanan [23-May-2020] -- End

            Dim dtTable As DataTable = objApproverTran._DataTable

            If xApproverTranId <= 0 AndAlso (dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0) Then
                Dim drRow As DataRow = dtTable.NewRow()
                drRow("tnaapprovertranunkid") = 0
                drRow("employeeunkid") = 0
                drRow("ename") = ""
                dtTable.Rows.Add(drRow)
                mblnblank = True
            End If

            dgNewApproverAssignEmp.AutoGenerateColumns = False
            dgNewApproverAssignEmp.DataSource = dtTable
            dgNewApproverAssignEmp.DataBind()

            If mblnblank Then
                dgNewApproverAssignEmp.Rows(0).Visible = False
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objApproverTran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillOldApproverEmployee(ByVal xApproverTranId As Integer)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverTran As New clsTnaapprover_Tran
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim mblnblank As Boolean = False
            objApproverTran._TnAMappingUnkId = xApproverTranId
            objApproverTran._EmployeeAsonDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'objApproverTran.Get_Data()
            objApproverTran.Get_Data(Nothing, Session("Database_Name").ToString(), chkShowInActiveEmployees.Checked)
            'Gajanan [23-May-2020] -- End

            Dim dtTable As DataTable = objApproverTran._DataTable

            If xApproverTranId <= 0 AndAlso (dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0) Then
                Dim drRow As DataRow = dtTable.NewRow()
                drRow("tnaapprovertranunkid") = 0
                drRow("employeeunkid") = 0
                drRow("ename") = ""
                dtTable.Rows.Add(drRow)
                mblnblank = True
            End If

            dgOldApproverEmp.AutoGenerateColumns = False
            dgOldApproverEmp.DataSource = dtTable
            dgOldApproverEmp.DataBind()

            If mblnblank Then
                dgOldApproverEmp.Rows(0).Visible = False
            End If

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objApproverTran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverMaster As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Select From Approver to migrate employee(s)."), Me)
                cboOldApprover.Focus()
                Exit Sub

            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select Level to migrate employee(s)."), Me)
                cboOldLevel.Focus()
                Exit Sub

            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select To Approver to migrate employee(s)."), Me)
                cboNewApprover.Focus()
                Exit Sub

            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please Select Level to migrate employee(s)."), Me)
                cboNewLevel.Focus()
                Exit Sub
            End If


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgOldApproverEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            Dim objApproverTran As New clsTnaapprover_Tran

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'objApproverTran.Get_Data()
            objApproverTran.Get_Data(Nothing, Session("Database_Name").ToString(), chkShowInActiveEmployees.Checked)
            'Gajanan [23-May-2020] -- End

            Dim mdtTable As DataTable = objApproverTran._DataTable.Clone()
            objApproverTran = Nothing

            objApproverMaster._Userunkid = CInt(Session("UserId"))
            objApproverMaster._AuditUserId = CInt(Session("UserId"))
            objApproverMaster._ClientIP = Session("IP_ADD").ToString()
            objApproverMaster._HostName = Session("HOST_NAME").ToString()
            objApproverMaster._FormName = mstrModuleName
            objApproverMaster._IsFromWeb = True

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement OT Requisition  - OT Requisition Enhancement given by NMB .
            objApproverMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            'Pinkal (09-Mar-2020) -- End


            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If CInt(cboNewApprover.SelectedValue) = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) Then Continue For
                'Pinkal (11-Sep-2020) -- End

                Dim drRow As DataRow = mdtTable.NewRow
                drRow("tnaapprovertranunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("tnaapprovertranunkid"))
                drRow("tnamappingunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("tnamappingunkid"))
                drRow("employeeunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                mdtTable.Rows.Add(drRow)
                mdtTable.AcceptChanges()
            Next

            If objApproverMaster.Migration_Insert(mintOldApproverId, mintNewApproverId, mdtTable, CInt(Session("UserId")), Nothing) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Problem in Approver Migration."), Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Approver Migration done successfully."), Me)
                btnReset_Click(btnReset, New EventArgs())
            End If

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If mdtTable IsNot Nothing Then mdtTable.Clear()
            mdtTable = Nothing
            'Pinkal (03-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objApproverMaster = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            mintOldApproverId = -1
            mintNewApproverId = -1
            mintOldMapUserId = -1
            mintNewMapUserId = -1
            mblnOldExternalApprover = False
            mblnNewExternalApprover = False
            cboOldApprover.SelectedValue = "0"
            cboOldLevel.SelectedValue = "0"
            cboNewApprover.SelectedValue = "0"
            cboNewLevel.SelectedValue = "0"
            cboOldApprover_SelectedIndexChanged(sender, e)
            cboOldLevel_SelectedIndexChanged(sender, e)
            cboNewApprover_SelectedIndexChanged(sender, e)
            cboNewLevel_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkCapApprovers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCapApprovers.CheckedChanged
        Try
            Fillcombo()
            mintOldApproverId = -1
            mintNewApproverId = -1
            mintOldMapUserId = -1
            mintNewMapUserId = -1
            mblnOldExternalApprover = False
            mblnNewExternalApprover = False
            cboOldLevel_SelectedIndexChanged(cboOldLevel, New EventArgs())
            cboNewLevel_SelectedIndexChanged(cboNewLevel, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [23-May-2020] -- Start
    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
    Protected Sub chkShowInactiveApprovers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInactiveApprovers.CheckedChanged
        Try
            chkShowInActiveEmployees.Enabled = True
            chkShowInActiveEmployees.Checked = False
            Fillcombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkShowInActiveEmployees_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployees.CheckedChanged
        Try
            cboOldLevel_SelectedIndexChanged(cboOldLevel, New EventArgs())
            cboNewLevel_SelectedIndexChanged(cboNewLevel, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [23-May-2020] -- End
#End Region

#Region "ComboBox Event"

    Protected Sub cboOldApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverMaster As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try

            Dim mstrFilter As String = "tnaapprover_master.isotcap_hod = " & IIf(chkCapApprovers.Checked, "1", "0").ToString() & " "

            If CInt(cboOldApprover.SelectedValue) > 0 Then
                mstrFilter &= "AND tnaapprover_master.approveremployeeunkid <> " & CInt(cboOldApprover.SelectedValue)
            End If

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'Dim dsList As DataSet = objApproverMaster.GetList("List", CStr(Session("Database_Name")), enTnAApproverType.OT_Requisition_Approver _
            '                                                                   , Session("EmployeeAsOnDate").ToString(), True, True, Nothing, mstrFilter, 0, chkCapApprovers.Checked)

            Dim dsList As DataSet = objApproverMaster.GetList("List", CStr(Session("Database_Name")), enTnAApproverType.OT_Requisition_Approver _
                                                              , Session("EmployeeAsOnDate").ToString(), chkShowInactiveApprovers.Checked, True, Nothing, mstrFilter, 0, chkCapApprovers.Checked)

            'Gajanan [23-May-2020] -- End


            If dsList IsNot Nothing Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("approveremployeeunkid") = 0
                drRow("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsTnaapprover_master", 4, "Select")
                dsList.Tables(0).Rows.InsertAt(drRow, 0)
            End If

            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "approveremployeeunkid", "name", "isexternalapprover")

            With cboNewApprover
                .DataValueField = "approveremployeeunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With


            Call cboNewApprover_SelectedIndexChanged(cboNewApprover, Nothing)


            Dim dtOldApprover As DataTable = objApproverMaster.GetOTApproverDetails(CInt(cboOldApprover.SelectedValue), chkCapApprovers.Checked, -1)

            If dtOldApprover IsNot Nothing AndAlso dtOldApprover.Rows.Count > 0 Then
                mintOldApproverId = CInt(dtOldApprover.Rows(0)("tnamappingunkid"))
                mblnOldExternalApprover = CBool(dtOldApprover.Rows(0)("isexternalapprover"))
            End If

            Dim dsLevel As DataSet = objApproverMaster.GetOTApproverLevels(CInt(cboOldApprover.SelectedValue), mblnOldExternalApprover, True)

            With cboOldLevel
                .DataTextField = "NAME"
                .DataValueField = "Id"
                .DataSource = dsLevel.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With


            Call cboOldLevel_SelectedIndexChanged(cboOldLevel, Nothing)

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            mintNewApproverId = 0
            mintNewMapUserId = 0
            FillAssigneedEmployee(mintNewApproverId)
            'Gajanan [23-May-2020] -- End

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dtOldApprover IsNot Nothing Then dtOldApprover.Clear()
            dtOldApprover = Nothing
            If dsLevel IsNot Nothing Then dsLevel.Clear()
            dsLevel = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objApproverMaster = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboOldLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        Try
            If CInt(cboOldLevel.SelectedValue) > 0 Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                Dim objApproverMaster As New clsTnaapprover_master
                'Pinkal (03-Sep-2020) -- End
                Dim mdtOldApprover As DataTable = objApproverMaster.GetOTApproverDetails(CInt(cboOldApprover.SelectedValue), chkCapApprovers.Checked, CInt(cboOldLevel.SelectedValue))
                If mdtOldApprover IsNot Nothing AndAlso mdtOldApprover.Rows.Count > 0 Then
                    mintOldApproverId = CInt(mdtOldApprover.Rows(0)("tnamappingunkid"))
                    mintOldMapUserId = CInt(mdtOldApprover.Rows(0)("mapuserunkid"))
                    mblnOldExternalApprover = CBool(mdtOldApprover.Rows(0)("isexternalapprover"))
                End If

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                If mdtOldApprover IsNot Nothing Then mdtOldApprover.Clear()
                mdtOldApprover = Nothing
                objApproverMaster = Nothing
                'Pinkal (03-Sep-2020) -- End
            Else
                mintOldApproverId = -1
                mintOldMapUserId = -1
                mblnOldExternalApprover = False
            End If
            Call FillOldApproverEmployee(mintOldApproverId)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboNewApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverMaster As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim dtNewApprover As DataTable = objApproverMaster.GetOTApproverDetails(CInt(cboNewApprover.SelectedValue), chkCapApprovers.Checked)
            If dtNewApprover IsNot Nothing AndAlso dtNewApprover.Rows.Count > 0 Then
                mintNewApproverId = CInt(dtNewApprover.Rows(0)("tnamappingunkid"))
                mintNewMapUserId = CInt(dtNewApprover.Rows(0)("mapuserunkid"))
                mblnNewExternalApprover = CBool(dtNewApprover.Rows(0)("isexternalapprover"))
            End If

            Dim dsLevel As DataSet = objApproverMaster.GetOTApproverLevels(CInt(cboNewApprover.SelectedValue), mblnNewExternalApprover, True)

            With cboNewLevel
                .DataTextField = "NAME"
                .DataValueField = "Id"
                .DataSource = dsLevel.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dtNewApprover IsNot Nothing Then dtNewApprover.Clear()
            dtNewApprover = Nothing
            If dsLevel IsNot Nothing Then dsLevel.Clear()
            dsLevel = Nothing
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objApproverMaster = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboNewLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objApproverMaster As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim mdtNewApprover As DataTable = objApproverMaster.GetOTApproverDetails(CInt(cboNewApprover.SelectedValue), chkCapApprovers.Checked, CInt(cboNewLevel.SelectedValue))

            If mdtNewApprover IsNot Nothing AndAlso mdtNewApprover.Rows.Count > 0 Then
                mintNewApproverId = CInt(mdtNewApprover.Rows(0)("tnamappingunkid"))
                mintNewMapUserId = CInt(mdtNewApprover.Rows(0)("mapuserunkid"))
                mblnNewExternalApprover = CBool(mdtNewApprover.Rows(0)("isexternalapprover"))
            End If
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If mdtNewApprover IsNot Nothing Then mdtNewApprover.Clear()
            mdtNewApprover = Nothing
            'Pinkal (03-Sep-2020) -- End
            Call FillAssigneedEmployee(mintNewApproverId)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objApproverMaster = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.chkCapApprovers.ID, Me.chkCapApprovers.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblOldApprover.ID, Me.lblOldApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblOldLevel.ID, Me.lblOldLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblNewLevel.ID, Me.lblNewLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblNewApprover.ID, Me.lblNewApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnTransfer.ID, Me.btnTransfer.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,"btnClose", Me.btnReset.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.tbAssignedEmp.ID, Me.tbAssignedEmp.HeaderText)
            'Gajanan [17-Sep-2020] -- End


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.dgOldApproverEmp.Columns(0).FooterText, Me.dgOldApproverEmp.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.dgOldApproverEmp.Columns(1).FooterText, Me.dgOldApproverEmp.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.dgNewApproverAssignEmp.Columns(0).FooterText, Me.dgNewApproverAssignEmp.Columns(0).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblDetialHeader.Text)
            Me.chkCapApprovers.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.chkCapApprovers.ID, Me.chkCapApprovers.Text)
            Me.lblOldApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblOldApprover.ID, Me.lblOldApprover.Text)
            Me.lblOldLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblOldLevel.ID, Me.lblOldLevel.Text)
            Me.lblNewLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblNewLevel.ID, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblNewApprover.ID, Me.lblNewApprover.Text)
            Me.btnTransfer.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnTransfer.ID, Me.btnTransfer.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnReset.Text).Replace("&", "")

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.tbAssignedEmp.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbAssignedEmp.ID, Me.tbAssignedEmp.HeaderText)
            'Gajanan [17-Sep-2020] -- End



            Me.dgOldApproverEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgOldApproverEmp.Columns(0).FooterText, Me.dgOldApproverEmp.Columns(0).HeaderText)
            Me.dgOldApproverEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgOldApproverEmp.Columns(1).FooterText, Me.dgOldApproverEmp.Columns(1).HeaderText)

            Me.dgNewApproverAssignEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgNewApproverAssignEmp.Columns(0).FooterText, Me.dgNewApproverAssignEmp.Columns(0).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Please Select From Approver to migrate employee(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Please Select Level to migrate employee(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Please Select To Approver to migrate employee(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Please Select Level to migrate employee(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "Problem in Approver Migration.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "Approver Migration done successfully.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsTnaapprover_master", 4, "Select")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
