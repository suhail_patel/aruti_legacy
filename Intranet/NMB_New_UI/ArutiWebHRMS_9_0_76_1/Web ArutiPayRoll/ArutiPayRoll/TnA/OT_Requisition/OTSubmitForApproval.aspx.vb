﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
Imports System.Drawing

#End Region

Partial Class TnA_OT_Requisition_OTSubmitForApproval
    Inherits Basepage

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "frmOTSubmitForApproval"
    Private DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objOTRequisition As clsOT_Requisition_Tran
    'Pinkal (03-Sep-2020) -- End

    Private mdicETS As Dictionary(Of String, Integer)
    Private mdtOTTimesheet As DataTable = Nothing
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintPeriodID As Integer = -1

    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.
    Private mblnIncludeCapApprover As Boolean = False
    'Pinkal (23-Nov-2019) -- End

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objOTRequisition = New clsOT_Requisition_Tran
            'Pinkal (03-Sep-2020) -- End

            mdicETS = gvOTRequisitionList.Columns.Cast(Of DataControlField).ToDictionary(Function(x) x.FooterText, Function(x) gvOTRequisitionList.Columns.IndexOf(x))

            If IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                'Call FillCombo()
                dtpFromDate_TextChanged(dtpFromDate, New EventArgs())
                dtpFromDate.SetDate = mdtStartDate.Date
                dtpToDate.SetDate = mdtEndDate.Date
                dtpFromDate.Enabled = False
                dtpToDate.Enabled = False
                FillList(True)
            Else
                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.
                'mdtOTTimesheet = CType(Me.ViewState("mdtOTTimesheet"), DataTable)
                'Pinkal (12-Oct-2021)-- End
                mdtStartDate = CDate(Me.ViewState("StartDate"))
                mdtEndDate = CDate(Me.ViewState("EndDate"))
                mblnIncludeCapApprover = CBool(Me.ViewState("IncludeCapApprover"))
            End If
            Call SetControlVisibility()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'Me.ViewState("mdtOTTimesheet") = mdtOTTimesheet
            'Pinkal (12-Oct-2021)-- End
            Me.ViewState("StartDate") = mdtStartDate
            Me.ViewState("EndDate") = mdtEndDate
            Me.ViewState("IncludeCapApprover") = mblnIncludeCapApprover
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    'Private Sub FillCombo()
    '    Try
    '        Dim dsList As DataSet = Nothing
    '        Dim objMaster As New clsMasterData
    '        Dim objPeriod As New clscommom_period_Tran


    '        'Pinkal (08-Jan-2020) -- Start
    '        'Enhancement - NMB - Working on NMB OT Requisition Requirement.
    '        'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
    '        '                                   CDate(Session("fin_startdate")).Date, "List", True)

    '        dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
    '                                           CDate(Session("fin_startdate")).Date, "List", True, enStatusType.OPEN)

    '        Dim intFirstOpenPeriodID As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN, False, True)

    '        'Pinkal (08-Jan-2020) -- End


    '        With cboPeriod
    '            .DataValueField = "periodunkid"
    '            .DataTextField = "name"
    '            .DataSource = dsList.Tables("List")
    '            .DataBind()
    '            'Pinkal (08-Jan-2020) -- Start
    '            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
    '            '.SelectedValue = "0"
    '            .SelectedValue = intFirstOpenPeriodID.ToString()
    '            'Pinkal (08-Jan-2020) -- End
    '        End With
    '        objPeriod = Nothing
    '        dsList = Nothing


    '        'Pinkal (08-Jan-2020) -- Start
    '        'Enhancement - NMB - Working on NMB OT Requisition Requirement.
    '        If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
    '            cboPeriod.Enabled = False
    '        End If
    '        'Pinkal (08-Jan-2020) -- End


    '        Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objOTRequisition As clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End
        Try

            Dim dsList As DataSet = Nothing
            Dim strSearch As String = ""

            If dtpFromDate.IsNull = False AndAlso dtpFromDate.GetDate <> Nothing Then
                strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) >='" & eZeeDate.convertDate(dtpFromDate.GetDate) & "' "
            End If

            If dtpToDate.IsNull = False AndAlso dtpToDate.GetDate <> Nothing Then
                strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) <='" & eZeeDate.convertDate(dtpToDate.GetDate) & "' "
            End If


            'Select Case menOperation
            '    Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
            '        strSearch &= "AND ltbemployee_timesheet.issubmit_approval=0 AND ltbemployee_timesheet.statusunkid=2 "

            '    Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
            '        strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 "
            '        If CInt(cboStatus.SelectedValue) > 0 Then
            '            strSearch &= "AND ltbemployee_timesheet.statusunkid=" & CInt(cboStatus.SelectedValue) & " "
            '        End If

            '    Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
            '        strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 AND ltbemployee_timesheet.statusunkid=1 "

            'End Select

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            Dim mblnApplyAccessFilter As Boolean = True
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                mblnApplyAccessFilter = False
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objOTRequisition = New clsOT_Requisition_Tran
            'Pinkal (03-Sep-2020) -- End

            If isblank Then

                'Pinkal (29-Jan-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.

                'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                        , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                '                                        , True, mblnApplyAccessFilter, "", Nothing, CInt(cboEmployee.SelectedValue), "", mdtStartDate.Date, mdtEndDate.Date _
                '                                        , 2, CInt(cboPeriod.SelectedValue), False, 0).Clone()



                'Pinkal (08-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	

                'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                            , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                '                                            , True, mblnApplyAccessFilter, "", Nothing, CInt(cboEmployee.SelectedValue), "", dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date _
                '                                            , 2, 0, False, 0, True, False).Clone()


                dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                            , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                                                         , True, mblnApplyAccessFilter, "", Nothing, CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), "", dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date _
                                                            , 2, 0, False, 0, True, False).Clone()

                'Pinkal (08-Feb-2022) -- End


                'Pinkal (29-Jan-2020) -- End

            Else

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.

                'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                           , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                '                                          , True, mblnApplyAccessFilter, "", Nothing,  CInt(cboEmployee.SelectedValue)) , "", dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, 2, 0, False, 0, True, False)


                'Pinkal (08-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	
                'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                          , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                '                                          , True, mblnApplyAccessFilter, "", Nothing, CInt(IIf(CInt(cboEmployee.SelectedValue) <= 0, -1, CInt(cboEmployee.SelectedValue))) _
                '                                          , "", dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, 2, 0, False, 0, True, False)

                dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                          , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                                                                     , True, mblnApplyAccessFilter, "", Nothing, CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) _
                                                                     , "", dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, 2, 0, False, 0, True, False)
                'Pinkal (08-Feb-2022) -- End


                'Pinkal (03-Sep-2020) -- End


            End If


            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'If dsList.Tables("List").Columns.Contains("IsGrp") = False Then
            '    Dim dc As New DataColumn("IsGrp", Type.GetType("System.Boolean"))
            '    dc.DefaultValue = False
            '    dsList.Tables(0).Columns.Add(dc)
            '    dc = Nothing
            'End If
            'Pinkal (12-Oct-2021)-- End

            If dsList.Tables("List").Columns.Contains("ischecked") = False Then
                Dim dc As New DataColumn("ischecked", Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                dsList.Tables(0).Columns.Add(dc)
                dc = Nothing
            End If

            If dsList.Tables("List").Columns.Contains("PlannedworkedDuration") = False Then
                Dim dc As New DataColumn("PlannedworkedDuration", Type.GetType("System.String"))
                dc.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dc)
                dc = Nothing
            End If

            Dim TotalSec As Integer = 0
            For Each dtRow As DataRow In dsList.Tables("List").Rows()
                dtRow("PlannedworkedDuration") = CalculateTime(True, CInt(dtRow("plannedot_hours"))).ToString("#00.00").Replace(".", ":")
            Next

            If isblank = True OrElse dsList.Tables("List").Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables("List").NewRow()
                drRow("IsGrp") = False
                drRow("otrequisitiontranunkid") = -1
                drRow("employeeunkid") = -1
                drRow("empcodename") = ""
                dsList.Tables("List").Rows.Add(drRow)
                isblank = True
            End If

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.

            'Dim mdtTran As DataTable = Nothing
            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso isblank = False Then
            '    mdtTran = New DataView(dsList.Tables("List"), "", "Employeeunkid", DataViewRowState.CurrentRows).ToTable()
            '    Dim dtTable As DataTable = mdtTran.Clone
            '    Dim strEmployeeunkid As String = ""
            '    Dim dtRow As DataRow = Nothing

            '    For Each drow As DataRow In mdtTran.Rows
            '        If CStr(drow("Employeeunkid")).Trim <> strEmployeeunkid.Trim Then
            '            dtRow = dtTable.NewRow
            '            dtRow("IsGrp") = True
            '            dtRow("otrequisitiontranunkid") = -1
            '            dtRow("empcodename") = drow("empcodename")
            '            dtRow("issubmit_approval") = drow("issubmit_approval")

            '            If IsDBNull(drow("requestdate")) = False Then
            '                dtRow("requestdate") = CDate(drow("requestdate"))
            '            End If

            '            dtRow("employeeunkid") = CInt(drow("employeeunkid"))
            '            strEmployeeunkid = drow("Employeeunkid").ToString()
            '            dtTable.Rows.Add(dtRow)
            '        End If
            '        dtRow = dtTable.NewRow
            '        For Each dtcol As DataColumn In mdtTran.Columns
            '            If dtcol.ColumnName.Contains("empcodename") Then
            '                dtRow(dtcol.ColumnName) = ""
            '            Else
            '                dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
            '            End If
            '        Next
            '        dtTable.Rows.Add(dtRow)
            '    Next
            '    mdtOTTimesheet = dtTable
            'Else
            '    mdtOTTimesheet = dsList.Tables("List")
            'End If

            mdtOTTimesheet = New DataView(dsList.Tables("List"), "", "employeeunkid", DataViewRowState.CurrentRows).ToTable()

            'Pinkal (12-Oct-2021)-- End

            gvOTRequisitionList.DataSource = mdtOTTimesheet
            gvOTRequisitionList.DataBind()

            If isblank Then
                gvOTRequisitionList.Rows(0).Visible = False
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing
            'Pinkal (12-Oct-2021)-- End


            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objOTRequisition = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetControlVisibility()
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'btnSubmitForApproval.Enabled = CBool(Session("AllowToSubmitForApprovalForPendingEmpBudgetTimesheet"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetAtValue(ByVal xFormName As String)
    Private Sub SetAtValue(ByVal xFormName As String, ByVal objOTRequisition As clsOT_Requisition_Tran)
        'Pinkal (03-Sep-2020) -- End
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objOTRequisition._Userunkid = CInt(Session("UserId"))
            Else
                objOTRequisition._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objOTRequisition._WebClientIP = CStr(Session("IP_ADD"))
            objOTRequisition._WebHostName = CStr(Session("HOST_NAME"))
            objOTRequisition._WebFormName = xFormName
            objOTRequisition._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    'Private Sub SubmitForApproval(ByVal dtEmpTimeSheet As DataTable)
    '    'Pinkal (03-Sep-2020) -- Start
    '    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Dim objOTRequisition As clsOT_Requisition_Tran
    '    'Pinkal (03-Sep-2020) -- End
    '    Try


    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '        'SetAtValue(mstrModuleName)
    '        objOTRequisition = New clsOT_Requisition_Tran
    '        SetAtValue(mstrModuleName, objOTRequisition)
    '        'Pinkal (03-Sep-2020) -- End


    '        If objOTRequisition.UpdateGlobalSubmitForApproval(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
    '                                    CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                     dtEmpTimeSheet, mblnIncludeCapApprover, mdtStartDate.Date, mdtEndDate.Date) = False Then
    '            DisplayMessage.DisplayMessage(objOTRequisition._Message, Me)
    '            Exit Sub

    '        Else
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Employee(s) OT Requisition submitted successfully."), Me)

    '            Dim mintLoginTypeID As Integer = 0
    '            Dim mintLoginEmployeeID As Integer = 0
    '            Dim mintUserID As Integer = 0
    '            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
    '                mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
    '                mintLoginEmployeeID = 0
    '                mintUserID = CInt(Session("UserId"))
    '            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
    '                mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
    '                mintLoginEmployeeID = CInt(Session("Employeeunkid"))
    '                mintUserID = 0
    '            End If

    '            Dim strEmployeeIDs As String = String.Join(",", dtEmpTimeSheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

    '            Dim dtSelectedData As DataTable = dtEmpTimeSheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True).ToList().CopyToDataTable()

    '            'Pinkal (10-Jan-2020) -- Start
    '            'Enhancements -  Working on OT Requisistion for NMB.
    '            'objOTRequisition.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '            '                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
    '            '                                                                 , CInt(cboPeriod.SelectedValue), -1, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID _
    '            '                                                                 , mintUserID, CStr(Session("ArutiSelfServiceURL")), False)


    '            'Pinkal (29-Jan-2020) -- Start
    '            'Enhancement - Changes related To OT NMB Testing.

    '            'objOTRequisition.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '            '                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
    '            '                                                                 , CInt(cboPeriod.SelectedValue), -1, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID _
    '            '                                                                , mintUserID, CStr(Session("ArutiSelfServiceURL")), False, mblnIncludeCapApprover)

    '            objOTRequisition.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
    '                                                                           , dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, -1, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID _
    '                                                                          , mintUserID, CStr(Session("ArutiSelfServiceURL")), False, mblnIncludeCapApprover)



    '            'Pinkal (29-Jan-2020) -- End

    '            'Pinkal (10-Jan-2020) -- End


    '            'Pinkal (03-Sep-2020) -- Start
    '            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '            If dtSelectedData IsNot Nothing Then dtSelectedData.Clear()
    '            dtSelectedData = Nothing
    '            objOTRequisition = Nothing
    '            'Pinkal (03-Sep-2020) -- End


    '            Call FillList(False)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Finally
    '        objOTRequisition = Nothing
    '        'Pinkal (03-Sep-2020) -- End
    '    End Try
    'End Sub

    Private Sub SubmitForApproval()
        Dim objOTRequisition As clsOT_Requisition_Tran
        Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(gvOTRequisitionList.DataKeys(x.RowIndex)("IsGrp")) = False)

            Dim mstrOTRequisitionIds As String = ""
            mstrOTRequisitionIds = String.Join(",", gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True _
                                                             And CBool(gvOTRequisitionList.DataKeys(x.RowIndex)("IsGrp")) = False).Select(Function(x) gvOTRequisitionList.DataKeys(x.RowIndex)("otrequisitiontranunkid").ToString()).Distinct.ToArray())


            objOTRequisition = New clsOT_Requisition_Tran
            SetAtValue(mstrModuleName, objOTRequisition)


            If objOTRequisition.UpdateGlobalSubmitForApproval(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                         mstrOTRequisitionIds, mblnIncludeCapApprover, mdtStartDate.Date, mdtEndDate.Date) = False Then
                DisplayMessage.DisplayMessage(objOTRequisition._Message, Me)
                Exit Sub

            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Employee(s) OT Requisition submitted successfully."), Me)

                Dim mintLoginTypeID As Integer = 0
                Dim mintLoginEmployeeID As Integer = 0
                Dim mintUserID As Integer = 0
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                End If

                Dim strEmployeeIDs As String = String.Join(",", gRow.AsEnumerable().Select(Function(x) gvOTRequisitionList.DataKeys(x.RowIndex)("employeeunkid").ToString).Distinct().ToArray)

                objOTRequisition.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                               , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
                                                                               , dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, -1, Nothing, mintLoginTypeID, mintLoginEmployeeID _
                                                                              , mintUserID, CStr(Session("ArutiSelfServiceURL")), False, mblnIncludeCapApprover)

                objOTRequisition = Nothing
                Call FillList(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objOTRequisition = Nothing
        End Try
    End Sub

    'Pinkal (12-Oct-2021)-- End



#End Region

#Region " ComboBox's Events "

    'Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '    Try
    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

    '            mdtStartDate = CDate(objPeriod._TnA_StartDate).Date
    '            mdtEndDate = CDate(objPeriod._TnA_EndDate).Date
    '        Else
    '            mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
    '            mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
    '        End If

    '        dtpFromDate.SetDate = mdtStartDate.Date
    '        dtpToDate.SetDate = mdtEndDate.Date


    '        'Pinkal (24-Oct-2019) -- Start
    '        'Enhancement NMB - Working On OT Enhancement for NMB.

    '        'If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then

    '        '    Dim objEmployee As New clsEmployee_Master
    '        '    Dim dsList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
    '        '                                                     CInt(Session("CompanyUnkId")), mdtStartDate, mdtEndDate, _
    '        '                                                        CStr(Session("UserAccessModeSetting")), True, _
    '        '                                                        CBool(Session("IsIncludeInactiveEmp")), "List", True)


    '        '    With cboEmployee
    '        '        .DataValueField = "employeeunkid"
    '        '        .DataTextField = "EmpCodeName"
    '        '        .DataSource = dsList.Tables("List")
    '        '        .DataBind()
    '        '        .SelectedValue = "0"
    '        '    End With
    '        '    objEmployee = Nothing
    '        '    dsList = Nothing

    '        'ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
    '        '    Dim objglobalassess = New GlobalAccess
    '        '    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '        '    cboEmployee.DataSource = objglobalassess.ListOfEmployee
    '        '    cboEmployee.DataTextField = "loginname"
    '        '    cboEmployee.DataValueField = "employeeunkid"
    '        '    cboEmployee.DataBind()
    '        'End If

    '        Dim blnApplyFilter As Boolean = True
    '        Dim blnSelect As Boolean = True
    '        Dim intEmpId As Integer = 0
    '        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
    '            blnSelect = False
    '            intEmpId = CInt(Session("Employeeunkid"))
    '            blnApplyFilter = False
    '        End If

    '        Dim objEmpAssignOT As New clsassignemp_ot
    '        Dim dsCombo As DataSet = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
    '                                                             , mdtEndDate, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)
    '        With cboEmployee
    '            .DataValueField = "employeeunkid"
    '            .DataTextField = "EmpCodeName"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then cboEmployee.SelectedValue = "0"
    '        End With
    '        objEmpAssignOT = Nothing

    '        'Pinkal (24-Oct-2019) -- End

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is compulsory information. Please Select Period."), Me)
            '    cboPeriod.Focus()
            '    Exit Sub
            'End If
            If dtpFromDate.IsNull OrElse dtpToDate.IsNull Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Please Select proper Date Range."), Me)
                Exit Sub

            ElseIf mdtStartDate.Date > dtpFromDate.GetDate.Date OrElse mdtEndDate.Date < dtpToDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, you cannot do submit for approval for ot requisition(s).Reason : The date set on configuration is already passed."), Me)
                Exit Sub
            End If

            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'cboPeriod.SelectedValue = "0"
            'cboPeriod_SelectedIndexChanged(New Object(), New EventArgs())
            dtpFromDate.SetDate = mdtStartDate
            dtpToDate.SetDate = mdtEndDate
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then cboEmployee.SelectedValue = "0"
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            FillList(True)
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub btnSubmitForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
    '    'Pinkal (03-Sep-2020) -- Start
    '    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Dim objOTRequisition As New clsOT_Requisition_Tran
    '    'Pinkal (03-Sep-2020) -- End
    '    Try
    '        Dim blnFlag As Boolean = False


    '        'Pinkal (10-Jan-2020) -- Start
    '        'Enhancements -  Working on OT Requisistion for NMB.

    '        'Dim dtEmpTimeSheet As DataTable = New DataView(mdtOTTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable

    '        'If dtEmpTimeSheet.Rows.Count <= 0 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
    '        '    Exit Sub
    '        'End If

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
            '    Exit Sub
    '        End If

    '        Dim dtEmpTimeSheet As DataTable = mdtOTTimesheet.Clone
    '        Dim xCount As Integer = -1
    '        For i As Integer = 0 To gRow.Count - 1
    '            xCount = i
    '            Dim dRow As DataRow() = mdtOTTimesheet.Select("isGrp = 0 AND otrequisitiontranunkid = " & CInt(gvOTRequisitionList.DataKeys(gRow(xCount).DataItemIndex)("otrequisitiontranunkid")))
    '            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
    '                dRow(0)("ischecked") = True
    '                dRow(0).AcceptChanges()
    '                dtEmpTimeSheet.ImportRow(dRow(0))
    '            End If
    '        Next

    '        'Pinkal (10-Jan-2020) -- End



    '        'STILL PENDING TO CHANGE CAP APPROVER PARAMETER CHANGE.

    '        'Pinkal (23-Nov-2019) -- Start
    '        'Enhancement NMB - Working On OT Enhancement for NMB.
    '        Dim strCapOTHrsForHODApprovers As String = Session("CapOTHrsForHODApprovers").ToString()

    '        If strCapOTHrsForHODApprovers.Length > 0 Then
    '            Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
    '            Dim intTotalMins As Integer = 0
    '            If arrCapOTHrsForHODApprovers.Length > 0 Then
    '                intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
    '            End If


    '            For Each dr As DataRow In dtEmpTimeSheet.Rows

    '                Dim xEmployeeId As Integer = CInt(dr("employeeunkid"))

    '                'Pinkal (29-Jan-2020) -- Start
    '                'Enhancement - Changes related To OT NMB Testing.
    '                'Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisition.GetEmpTotalOThours(CInt(cboPeriod.SelectedValue), xEmployeeId.ToString(), Nothing, True, True))
    '                Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisition.GetEmpTotalOThours(mdtStartDate.Date, mdtEndDate.Date, xEmployeeId.ToString(), Nothing, True, True))
    '                'Pinkal (29-Jan-2020) -- End

    '                If intTotalMins <= intAppliedOThours Then
    '                    mblnIncludeCapApprover = True
    '                ElseIf intAppliedOThours <= 0 Then
    '                    Dim xAppliedOThours As Double = CalculateTime(False, dtEmpTimeSheet.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId).DefaultIfEmpty.Sum(Function(x) x.Field(Of Integer)("plannedot_hours")))
    '                    If intTotalMins <= xAppliedOThours Then
    '                        mblnIncludeCapApprover = True
    '                    End If
    '                End If

    '            Next

    '            If mblnIncludeCapApprover Then
    '                popupConfirmationForOTCap.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Confirmation")
    '                popupConfirmationForOTCap.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Some of the employee(s) will exceed limit for the OT cap which is configured on configuration.Are you sure you want to do submit for approval ?")
    '                popupConfirmationForOTCap.Show()
    '                Exit Sub
    '            End If  'If mblnIncludeCapApprover Then

    '        End If  ' If strCapOTHrsForHODApprovers.Length > 0 Then

    '        SubmitForApproval(dtEmpTimeSheet)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Finally
    '        objOTRequisition = Nothing
    '        'Pinkal (03-Sep-2020) -- End
    '    End Try
    'End Sub

    Protected Sub btnSubmitForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Dim objOTRequisition As New clsOT_Requisition_Tran
        Try
            Dim blnFlag As Boolean = False

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            'STILL PENDING TO CHANGE CAP APPROVER PARAMETER CHANGE.

            Dim strCapOTHrsForHODApprovers As String = Session("CapOTHrsForHODApprovers").ToString()

            If strCapOTHrsForHODApprovers.Length > 0 Then
                Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
                Dim intTotalMins As Integer = 0
                If arrCapOTHrsForHODApprovers.Length > 0 Then
                    intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
                End If


                For Each dr As GridViewRow In gRow

                    Dim xEmployeeId As Integer = CInt(gvOTRequisitionList.DataKeys(dr.RowIndex)("employeeunkid"))
                    Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisition.GetEmpTotalOThours(mdtStartDate.Date, mdtEndDate.Date, xEmployeeId.ToString(), Nothing, True, True))

                    If intTotalMins <= intAppliedOThours Then
                        mblnIncludeCapApprover = True
                    ElseIf intAppliedOThours <= 0 Then
                        Dim xAppliedOThours As Double = CalculateTime(False, gRow.AsEnumerable().Where(Function(x) CInt(gvOTRequisitionList.DataKeys(x.RowIndex)("employeeunkid")) = xEmployeeId).DefaultIfEmpty.Sum(Function(x) CInt(gvOTRequisitionList.DataKeys(x.RowIndex)("plannedot_hours"))))
                        If intTotalMins <= xAppliedOThours Then
                            mblnIncludeCapApprover = True
                        End If
                    End If

                Next

                If mblnIncludeCapApprover Then
                    popupConfirmationForOTCap.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Confirmation")
                    popupConfirmationForOTCap.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Some of the employee(s) will exceed limit for the OT cap which is configured on configuration.Are you sure you want to do submit for approval ?")
                    popupConfirmationForOTCap.Show()
                    Exit Sub
                End If  'If mblnIncludeCapApprover Then

            End If  ' If strCapOTHrsForHODApprovers.Length > 0 Then

            SubmitForApproval()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objOTRequisition = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupConfirmationForOTCap_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmationForOTCap.buttonYes_Click
        Try
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'Dim dtEmpTimeSheet As DataTable = New DataView(mdtOTTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable()
            'SubmitForApproval(dtEmpTimeSheet)
            SubmitForApproval()
            'Pinkal (12-Oct-2021)-- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub gvOTRequisitionList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOTRequisitionList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                SetDateFormat()
                If CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("IsGrp")) Then

                    Dim intVisible As Integer = gvOTRequisitionList.Columns.Cast(Of DataControlField).Where(Function(x) x.Visible = True).Count
                    e.Row.Cells(mdicETS("colhotreqemp")).ColumnSpan = intVisible

                    For i = mdicETS("colhotreqemp") + 1 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next


                    'Pinkal (01-Jun-2021)-- Start
                    'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                    'e.Row.Cells(mdicETS("colhotreqemp")).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Row.Cells(mdicETS("colhotreqemp")).CssClass = "group-header"
                    'Pinkal (01-Jun-2021) -- End
                    e.Row.Cells(mdicETS("colhotreqemp")).Style.Add("text-align", "left")
                    CType(e.Row.Cells(mdicETS("objdgcolhSelect")).FindControl("chkSelect"), CheckBox).Visible = False

                    'Pinkal (01-Jun-2021)-- Start
                    'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                    'e.Row.Cells(mdicETS("objdgcolhSelect")).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Row.Cells(mdicETS("objdgcolhSelect")).CssClass = "group-header"
                    e.Row.Cells(mdicETS("colhotreqemp")).Font.Bold = True
                    'Pinkal (01-Jun-2021) -- End


                Else
                    Dim chkSelect As CheckBox = CType(e.Row.FindControl("chkselect"), CheckBox)
                    chkSelect.Checked = CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("ischecked"))

                    If e.Row.Cells(2).Text <> "&nbsp;" Then
                        e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text).ToShortDateString()
                        e.Row.Cells(3).Text = CDate(e.Row.Cells(3).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(3).Text), "HH:mm")
                        e.Row.Cells(4).Text = CDate(e.Row.Cells(4).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(4).Text), "HH:mm")
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "


    'Pinkal (10-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion for NMB.

    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

    '        If mdtOTTimesheet.Rows.Count <= 1 Then Exit Sub '1 STANDS HIDDEN BLANK ROW.

    '        For Each item As GridViewRow In gvOTRequisitionList.Rows
    '            If item.RowType = DataControlRowType.DataRow Then
    '                CType(item.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '                mdtOTTimesheet.Rows(item.RowIndex)("ischecked") = chkSelectAll.Checked
    '            End If
    '        Next
    '        mdtOTTimesheet.AcceptChanges()
    '        gvOTRequisitionList.DataSource = mdtOTTimesheet
    '        gvOTRequisitionList.DataBind()

    '        CType(gvOTRequisitionList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = CType(sender, CheckBox).Checked
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If gvOTRequisitionList.Rows.Count <= 0 Then Exit Sub

    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)

    '        Dim dsRow As DataRow() = mdtOTTimesheet.Select("employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")
    '        If dsRow.Length > 0 Then
    '            For Each dR In dsRow
    '                dR.Item("ischecked") = chkSelect.Checked
    '                dR.AcceptChanges()
    '            Next
    '        End If

    '        gvOTRequisitionList.DataSource = mdtOTTimesheet
    '        gvOTRequisitionList.DataBind()

    '        Dim drRow As DataRow() = mdtOTTimesheet.Select("IsGrp=0 AND employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")

    '        Dim dRow As DataRow() = mdtOTTimesheet.Select("IsGrp=0 AND ischecked=0 AND employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")

    '        Dim gRow As DataRow() = mdtOTTimesheet.Select("IsGrp=1 AND employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")

    '        If gRow.Length <= 0 Then Exit Sub

    '        If drRow.Length = dRow.Length Then
    '            gRow(0).Item("ischecked") = False
    '        Else
    '            gRow(0).Item("ischecked") = True
    '        End If
    '        gRow(0).AcceptChanges()


    '        Dim xSelectedCount As Integer = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False And x.Field(Of Boolean)("ischecked") = True).DefaultIfEmpty().Count
    '        Dim xTotalCount As Integer = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False).DefaultIfEmpty().Count
    '        If xSelectedCount = xTotalCount Then
    '            CType(gvOTRequisitionList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Pinkal (10-Jan-2020) -- End

#End Region

#Region "DateControl Events"

    Protected Sub dtpFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged, dtpToDate.TextChanged
        Try


            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            'If Session("AllowApplyOTForEachMonthDay") IsNot Nothing AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) > 0 Then
            'mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, CInt(Session("AllowApplyOTForEachMonthDay")) + 1)
            If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
                mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                If mdtStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
                    'mdtStartDate = mdtStartDate.AddDays(-31)
                    mdtStartDate = mdtStartDate.AddDays(-1 * CInt(Session("OTTenureDays")) + 1)
                Else
                    Dim mdtTempDate As DateTime = mdtStartDate.AddMonths(-1)
                    If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
                        mdtStartDate = mdtTempDate
                    End If
                End If
                'mdtEndDate = mdtStartDate.AddDays(30)
                mdtEndDate = mdtStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
                'Pinkal (02-Jun-2020) -- End
            Else
                mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                mdtEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
            End If

            Dim dsCombo As DataSet = Nothing
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If


            Dim objEmpAssignOT As New clsassignemp_ot


            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
            '                                                     , mdtEndDate.Date, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)

            dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)
            'Pinkal (25-Jan-2022) -- End

            


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpcodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEmpAssignOT = Nothing


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language & UI Settings "

    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,"gbFilterCriteria", Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPeriod.ID, Me.lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblFromDate.ID, Me.lblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblToDate.ID, Me.lblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblEmployee.ID, Me.lblEmployee.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnSubmitForApproval.ID, Me.btnSubmitForApproval.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvOTRequisitionList.Columns(1).FooterText, gvOTRequisitionList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvOTRequisitionList.Columns(2).FooterText, gvOTRequisitionList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvOTRequisitionList.Columns(3).FooterText, gvOTRequisitionList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvOTRequisitionList.Columns(4).FooterText, gvOTRequisitionList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvOTRequisitionList.Columns(5).FooterText, gvOTRequisitionList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvOTRequisitionList.Columns(6).FooterText, gvOTRequisitionList.Columns(6).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblPageHeader.Text)


            'Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnSubmitForApproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSubmitForApproval.ID, Me.btnSubmitForApproval.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.gvOTRequisitionList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvOTRequisitionList.Columns(1).FooterText, gvOTRequisitionList.Columns(1).HeaderText)
            Me.gvOTRequisitionList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvOTRequisitionList.Columns(2).FooterText, gvOTRequisitionList.Columns(2).HeaderText)
            Me.gvOTRequisitionList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvOTRequisitionList.Columns(3).FooterText, gvOTRequisitionList.Columns(3).HeaderText)
            Me.gvOTRequisitionList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvOTRequisitionList.Columns(4).FooterText, gvOTRequisitionList.Columns(4).HeaderText)
            Me.gvOTRequisitionList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvOTRequisitionList.Columns(5).FooterText, gvOTRequisitionList.Columns(5).HeaderText)
            Me.gvOTRequisitionList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvOTRequisitionList.Columns(6).FooterText, gvOTRequisitionList.Columns(6).HeaderText)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Period is compulsory information. Please Select Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Please tick atleast one record for further process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Employee(s) OT Requisition submitted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "Some of the employee(s) will exceed limit for the OT cap which is configured on configuration.Are you sure you want to do submit for approval ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
