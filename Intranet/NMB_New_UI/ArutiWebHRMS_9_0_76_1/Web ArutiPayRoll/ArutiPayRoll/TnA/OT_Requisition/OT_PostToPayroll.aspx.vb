﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region

Partial Class TnA_OT_Requisition_OT_PostToPayroll
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOTRequisitionPayrollPosting"

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objOTPosting As clstnaotrequisition_process_Tran
    'Pinkal (03-Sep-2020) -- End


    Private DisplayMessage As New CommonCodes
    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
    'Private mdtData As DataTable = Nothing
    'Pinkal (16-Jul-2020) -- End
#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objOTPosting = New clstnaotrequisition_process_Tran
            'Pinkal (03-Sep-2020) -- End


            If IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpFromDate_TextChanged(dtpFromDate, New EventArgs())
                Call FillCombo()
                'Pinkal (16-Jul-2020) -- Start
                'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                'Else
                'mdtData = CType(Me.ViewState("mdtData"), DataTable)
                'Pinkal (16-Jul-2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'Me.ViewState("mdtData") = mdtData
            'Pinkal (16-Jul-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "List", True, 1)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objPeriod = Nothing

            With cboViewBy
                .Items.Clear()
                ''Language.setLanguage(mstrModuleName)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Show Unposted OT Requisition Transactions"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Show Posted OT Requisition Transactions"))
                .SelectedIndex = 0
            End With

            Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As DataSet = Nothing
        Dim mdtFilterData As DataTable = Nothing
        Dim strSearch As String = ""
        'Pinkal (16-Jul-2020) -- Start
        'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
        Dim mdtData As DataTable = Nothing
        'Pinkal (16-Jul-2020) -- End

        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objOTPosting As clstnaotrequisition_process_Tran
        'Pinkal (03-Sep-2020) -- End

        Try

            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                strSearch = "AND CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) BETWEEN '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' AND '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND tnaotrequisition_process_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboViewBy.SelectedIndex) = 1 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND tnaotrequisition_process_tran.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            If isblank Then
                strSearch &= "AND 1 = 2 "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objOTPosting = New clstnaotrequisition_process_Tran
            'Pinkal (03-Sep-2020) -- End


            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                dsList = objOTPosting.GetList("List", True, 0, strSearch, True)
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                dsList = objOTPosting.GetList("List", True, 1, strSearch, True)
            End If



            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

            'Dim dc As DataColumn = Nothing

            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("Isgroup") = False Then
            '    dc = New DataColumn("Isgroup", Type.GetType("System.Boolean"))
            '    dc.DefaultValue = False
            '    dsList.Tables(0).Columns.Add(dc)
            'End If

            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("Ischecked") = False Then
            '    dc = New DataColumn("ischecked", Type.GetType("System.Boolean"))
            '    dc.DefaultValue = False
            '    dsList.Tables(0).Columns.Add(dc)
            'End If

            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("ischange") = False Then
            '    dc = New DataColumn("ischange", Type.GetType("System.Boolean"))
            '    dc.DefaultValue = False
            '    dsList.Tables(0).Columns.Add(dc)
            'End If

            'mdtData = dsList.Tables(0).Clone
            mdtData = dsList.Tables(0).Copy()

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = mdtData.NewRow()
                dr("ischange") = False
                dr("Ischecked") = False
                dr("Isgroup") = False
                dr("otrequisitiontranunkid") = -1
                dr("otrequisitionprocesstranunkid") = -1
                dr("otrequestapprovaltranunkid") = -1
                mdtData.Rows.Add(dr)
                isblank = True
            End If


            'If dsList.Tables(0).Rows.Count > 0 AndAlso isblank = False Then
            '    Dim mintOTRequisitionId As Integer = -1
            '    Dim drNewRow As DataRow = Nothing
            '    Dim intRowCount As Integer = -1
            '    For Each dr As DataRow In dsList.Tables(0).Rows

            '        If mintOTRequisitionId <> CInt(dr("otrequisitiontranunkid")) Then
            '            mintOTRequisitionId = CInt(dr("otrequisitiontranunkid"))
            '            drNewRow = mdtData.NewRow
            '            drNewRow("ischange") = False
            '            drNewRow("Ischecked") = False
            '            drNewRow("Isgroup") = True
            '            drNewRow("otrequisitiontranunkid") = mintOTRequisitionId
            '            drNewRow("otrequisitionprocesstranunkid") = -1
            '            drNewRow("otrequestapprovaltranunkid") = -1
            '            drNewRow("requestdate") = DBNull.Value
            '            drNewRow("rdate") = ""
            '            drNewRow("employeeunkid") = -1
            '            drNewRow("periodunkid") = -1
            '            drNewRow("EmployeeCode") = ""
            '            drNewRow("Employee") = ""
            '            drNewRow("Particulars") = dr("Particulars")
            '            drNewRow("Period") = ""
            '            drNewRow("actualstart_time") = DBNull.Value
            '            drNewRow("actualend_time") = DBNull.Value
            '            drNewRow("actualot_hours") = ""
            '            drNewRow("finalmappedapproverunkid") = -1
            '            drNewRow("periodunkid") = -1
            '            drNewRow("period") = ""
            '            mdtData.Rows.Add(drNewRow)
            '            intRowCount += 1
            '        End If

            '        drNewRow = mdtData.NewRow
            '        drNewRow("ischange") = False
            '        drNewRow("Ischecked") = False
            '        drNewRow("Isgroup") = False
            '        drNewRow("otrequisitiontranunkid") = mintOTRequisitionId
            '        drNewRow("otrequisitionprocesstranunkid") = dr("otrequisitionprocesstranunkid")
            '        drNewRow("otrequestapprovaltranunkid") = dr("otrequestapprovaltranunkid")
            '        drNewRow("requestdate") = dr("requestdate")
            '        drNewRow("rdate") = dr("rdate")
            '        drNewRow("employeeunkid") = dr("employeeunkid")
            '        drNewRow("periodunkid") = dr("periodunkid")
            '        drNewRow("EmployeeCode") = dr("EmployeeCode")
            '        drNewRow("Employee") = dr("Employee")
            '        drNewRow("Particulars") = ""
            '        drNewRow("Period") = dr("Period")
            '        drNewRow("actualstart_time") = dr("actualstart_time")
            '        drNewRow("actualend_time") = dr("actualend_time")
            '        drNewRow("actualot_hours") = dr("actualot_hours")
            '        drNewRow("finalmappedapproverunkid") = dr("finalmappedapproverunkid")
            '        drNewRow("periodunkid") = -1
            '        drNewRow("period") = dr("period").ToString()
            '        mdtData.Rows.Add(drNewRow)
            '        intRowCount += 1
            '    Next
            'End If

            'Pinkal (16-Jul-2020) -- End

            gvOTRequisitionList.DataSource = mdtData
            gvOTRequisitionList.DataBind()

            If isblank Then
                gvOTRequisitionList.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            btnSave.Enabled = False
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If mdtData IsNot Nothing Then mdtData.Clear()
            mdtData = Nothing
            objOTPosting = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

    'Private Function UpdateData(ByVal dr As DataRow, ByVal mblnChecked As Boolean) As Boolean
    '    Dim mblnFlag As Boolean = False
    '    Try
    '        dr("ischecked") = mblnChecked
    '        dr.AcceptChanges()
    '        mblnFlag = True
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '        mblnFlag = False
    '    End Try
    '    Return mblnFlag
    'End Function

    'Pinkal (16-Jul-2020) -- End

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            For i = 2 To gvOTRequisitionList.Columns.Count - 1
                rw.Cells(i).Visible = False
            Next
            rw.Cells(0).FindControl("chkselect").Visible = False
            rw.Cells(1).Text = title
            rw.Cells(1).Font.Bold = True
            rw.Cells(1).ColumnSpan = gvOTRequisitionList.Columns.Count
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'rw.Cells(0).CssClass = "GroupHeaderStyle"
            'rw.Cells(1).CssClass = "GroupHeaderStyle"
            rw.Cells(0).CssClass = "group-header"
            rw.Cells(1).CssClass = "group-header"
            'Gajanan [17-Sep-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

    Private Function UpdateRow(ByVal drRow As GridViewRow, ByVal mstrPeriod As String) As Boolean
        Try
            If drRow IsNot Nothing Then
                drRow.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhPeriod", False, True)).Text = mstrPeriod
                drRow.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "objcolhIschange", False, True)).Text = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (16-Jul-2020) -- End

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboViewBy.SelectedIndex) = 1 And CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromDate_TextChanged(dtpFromDate, New EventArgs())
            cboViewBy.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'If mdtData IsNot Nothing Then mdtData.Rows.Clear()
            'gvOTRequisitionList.DataSource = mdtData
            'gvOTRequisitionList.DataBind()
            'Pinkal (16-Jul-2020) -- End
            Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPosting.Click, btnUnposting.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

            Dim GvRow As IEnumerable(Of GridViewRow) = Nothing
            GvRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelect"), CheckBox).Checked = True And CType(x.FindControl("ChkSelect"), CheckBox).Visible = True)

            If GvRow Is Nothing OrElse GvRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please check atleast one ot requisition to do futher operation on it."), Me)
                Exit Sub
            End If

            'Dim xCount As Integer = -1
            'For i As Integer = 0 To GvRow.Count - 1
            '    xCount = i
            '    Dim dRow As DataRow() = mdtData.Select("employeeunkid = " & CInt(gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("employeeunkid")) & " AND RDate = '" & gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("RDate").ToString() & "'")
            '    If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
            '        For Each dr In dRow
            '            dr("ischecked") = True
            '            dr.AcceptChanges()
            '        Next
            '    End If
            'Next

            'Pinkal (16-Jul-2020) -- End

            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                Dim objPrd As New clscommom_period_Tran

                objPrd._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)

                Dim mdtStartDate As Date = objPrd._Start_Date
                Dim mdtEndDate As Date = objPrd._End_Date

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objPrd = Nothing
                'Pinkal (03-Sep-2020) -- End


                'Pinkal (16-Jul-2020) -- Start
                'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                'Dim lstIDs As List(Of String) = (From p In mdtData Where (CBool(p.Item("Isgroup")) = False And CBool(p.Item("ischecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct.ToList()
                Dim lstIDs As List(Of String) = GvRow.AsEnumerable().Select(Function(x) gvOTRequisitionList.DataKeys(x.RowIndex).Values("employeeunkid").ToString()).Distinct.ToList()
                'Pinkal (16-Jul-2020) -- End

                Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

                Dim objLeaveTran As New clsTnALeaveTran
                If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), Me)
                    objLeaveTran = Nothing
                    Exit Sub
                End If
                objLeaveTran = Nothing
            End If

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'xCount = -1
            'For i As Integer = 0 To GvRow.Count - 1
            '    xCount = i
            '    Dim dRow As DataRow() = mdtData.Select("employeeunkid = " & CInt(gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("employeeunkid")) & " AND RDate = '" & gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("RDate").ToString() & "'")
            '    If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
            '        For Each dr In dRow
            '            dr("ischange") = True
            '            dr("period") = cboPeriod.SelectedItem.Text
            '            dr("periodunkid") = CInt(cboPeriod.SelectedValue)
            '            dr.AcceptChanges()
            '            gvOTRequisitionList.Rows(GvRow(i).RowIndex).Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhPeriod", False, True)).Text = cboPeriod.SelectedItem.Text
            '        Next
            '    End If
            'Next

            If CType(sender, Button).ID = btnPosting.ID Then
                GvRow.ToList().ForEach(Function(x) UpdateRow(x, cboPeriod.SelectedItem.Text))
            ElseIf CType(sender, Button).ID = btnUnposting.ID Then
                GvRow.ToList().ForEach(Function(x) UpdateRow(x, ""))
            End If
            'Pinkal (16-Jul-2020) -- End

            btnSave.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

    'Private Sub btnUnposting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnposting.Click
    '    Try
    '        If CInt(cboPeriod.SelectedValue) <= 0 Then
    '            ''Language.setLanguage(mstrModuleName)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
    '            cboPeriod.Focus()
    '            Exit Sub
    '        End If

    '        Dim GvRow As IEnumerable(Of GridViewRow) = Nothing
    '        GvRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelect"), CheckBox).Checked = True And CType(x.FindControl("ChkSelect"), CheckBox).Visible = True)

    '        If GvRow Is Nothing OrElse GvRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please check atleast one ot requisition to do futher operation on it."), Me)
    '            Exit Sub
    '        End If

    '        Dim xCount As Integer = -1
    '        For i As Integer = 0 To GvRow.Count - 1
    '            xCount = i
    '            Dim dRow As DataRow() = mdtData.Select("employeeunkid = " & CInt(gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("employeeunkid")) & " AND RDate = '" & gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("RDate").ToString() & "'")
    '            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
    '                For Each dr In dRow
    '                    dr("ischecked") = True
    '                    dr.AcceptChanges()
    '                Next
    '            End If
    '        Next


    '        If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
    '            Dim objPrd As New clscommom_period_Tran

    '            objPrd._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)

    '            Dim mdtStartDate As Date = objPrd._Start_Date
    '            Dim mdtEndDate As Date = objPrd._End_Date

    '            Dim lstIDs As List(Of String) = (From p In mdtData Where (CBool(p.Item("Isgroup")) = False And CBool(p.Item("ischecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct.ToList()
    '            Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

    '            Dim objLeaveTran As New clsTnALeaveTran
    '            If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), Me)
    '                objLeaveTran = Nothing
    '                Exit Sub
    '            End If
    '            objLeaveTran = Nothing
    '            objPrd = Nothing
    '        End If

    '        xCount = -1
    '        For i As Integer = 0 To GvRow.Count - 1
    '            xCount = i
    '            Dim dRow As DataRow() = mdtData.Select("employeeunkid = " & CInt(gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("employeeunkid")) & " AND RDate = '" & gvOTRequisitionList.DataKeys(GvRow(xCount).DataItemIndex)("RDate").ToString() & "'")
    '            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
    '                For Each dr In dRow
    '                    dr("ischange") = True
    '                    dr("period") = ""
    '                    dr("periodunkid") = 0
    '                    dr.AcceptChanges()
    '                    gvOTRequisitionList.Rows(GvRow(i).RowIndex).Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhPeriod", False, True)).Text = ""
    '                Next
    '            End If
    '        Next

    '        btnSave.Enabled = True
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
    '            Dim drRow() As DataRow = mdtData.Select("Isgroup = 0 AND ischange = 1")
    '            If drRow.Length <= 0 Then
    '                ''Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Please post/unpost atleast one ot requisition."), Me)
    '                Exit Sub
    '            End If

    '            Dim dtTable As DataTable = mdtData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("Isgroup") = False And x.Field(Of Boolean)("ischecked") = True And x.Field(Of Boolean)("ischange") = True).ToList().CopyToDataTable()

    '            objOTPosting._Userunkid = CInt(Session("UserId"))
    '            objOTPosting._WebClientIP = CStr(Session("IP_ADD"))
    '            objOTPosting._WebHostName = CStr(Session("HOST_NAME"))
    '            objOTPosting._WebFormName = mstrModuleName
    '            objOTPosting._Isweb = True

    '            If CInt(cboViewBy.SelectedIndex) = 0 Then

    '                If objOTPosting.Posting_Unposting_OTRequisition(True, dtTable) = False Then
    '                    DisplayMessage.DisplayMessage(objOTPosting._Message, Me)
    '                Else
    '                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "OT Requisition posting done successfully."), Me)
    '                    btnReset_Click(sender, New EventArgs())
    '                End If

    '            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then

    '                If objOTPosting.Posting_Unposting_OTRequisition(False, dtTable) = False Then
    '                    DisplayMessage.DisplayMessage(objOTPosting._Message, Me)
    '                Else
    '                    ''Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "OT Requisition unposting done successfully."), Me)
    '                    btnReset_Click(sender, New EventArgs())
    '                End If

    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objOTPosting As New clstnaotrequisition_process_Tran
        'Pinkal (03-Sep-2020) -- End
        Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True And x.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "objcolhIschange", False, True)).Text = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                ''Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Please post/unpost atleast one ot requisition."), Me)
                Exit Sub
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Dim mstrOTRequisitionProcessIds As String = String.Join(",", (From p In gRow Select (gvOTRequisitionList.DataKeys(p.RowIndex)("otrequisitionprocesstranunkid").ToString)).ToArray())
            Dim mstrOTRequisitionProcessIds As String = ""
            Dim arProcessIdList As New ArrayList()
            Dim xBunchDays As Integer = 10
            Dim mintDays As Integer = DateDiff(DateInterval.Day, dtpFromDate.GetDate.Date, dtpToDate.GetDate.AddDays(1))
            If mintDays > xBunchDays Then
                Dim mdtFromDate As Date = dtpFromDate.GetDate.Date
                Dim mdtToDate As Date = dtpFromDate.GetDate.Date
                Dim xCount As Integer = Math.Ceiling(mintDays / xBunchDays)

                For i As Integer = 0 To xCount - 1

                    If dtpToDate.GetDate.Date <= mdtToDate.Date.AddDays(xBunchDays - 1) Then
                        mdtToDate = mdtToDate.AddDays(DateDiff(DateInterval.Day, mdtFromDate.Date, dtpToDate.GetDate.Date.AddDays(1)))
                    Else
                        mdtToDate = mdtFromDate.AddDays(xBunchDays - 1)
                    End If

                    mstrOTRequisitionProcessIds = String.Join(",", (From p In gRow Where gvOTRequisitionList.DataKeys(p.RowIndex)("RDate") >= eZeeDate.convertDate(mdtFromDate.Date) _
                                                                                        And gvOTRequisitionList.DataKeys(p.RowIndex)("RDate") <= eZeeDate.convertDate(mdtToDate.Date) _
                                                                                        Select (gvOTRequisitionList.DataKeys(p.RowIndex)("otrequisitionprocesstranunkid").ToString)).ToArray())

                    If mstrOTRequisitionProcessIds.Trim.Length > 0 Then
                        arProcessIdList.Add(mstrOTRequisitionProcessIds)
                    End If

                    mdtFromDate = mdtFromDate.Date.AddDays(xBunchDays)
                    mstrOTRequisitionProcessIds = ""
                Next
            Else
                mstrOTRequisitionProcessIds = String.Join(",", (From p In gRow Select (gvOTRequisitionList.DataKeys(p.RowIndex)("otrequisitionprocesstranunkid").ToString)).ToArray())
                arProcessIdList.Add(mstrOTRequisitionProcessIds)
            End If
            'Pinkal (03-Sep-2020) -- End


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objOTPosting = New clstnaotrequisition_process_Tran
            'Pinkal (03-Sep-2020) -- End


            objOTPosting._Userunkid = CInt(Session("UserId"))
            objOTPosting._WebClientIP = CStr(Session("IP_ADD"))
            objOTPosting._WebHostName = CStr(Session("HOST_NAME"))
            objOTPosting._WebFormName = mstrModuleName
            objOTPosting._Isweb = True

            If CInt(cboViewBy.SelectedIndex) = 0 Then


                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                'If objOTPosting.Posting_Unposting_OTRequisition(True, CInt(cboPeriod.SelectedValue), mstrOTRequisitionProcessIds) = False Then
                If objOTPosting.Posting_Unposting_OTRequisition(True, CInt(cboPeriod.SelectedValue), arProcessIdList) = False Then
                    'Pinkal (03-Sep-2020) -- End
                    DisplayMessage.DisplayMessage(objOTPosting._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "OT Requisition posting done successfully."), Me)
                    btnReset_Click(sender, New EventArgs())
                End If

            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                'If objOTPosting.Posting_Unposting_OTRequisition(False, 0, mstrOTRequisitionProcessIds) = False Then
                If objOTPosting.Posting_Unposting_OTRequisition(False, 0, arProcessIdList) = False Then
                    'Pinkal (03-Sep-2020) -- End
                    DisplayMessage.DisplayMessage(objOTPosting._Message, Me)
                Else
                    ''Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "OT Requisition unposting done successfully."), Me)
                    btnReset_Click(sender, New EventArgs())
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objOTPosting = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (16-Jul-2020) -- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx")
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            cboPeriod.SelectedValue = "0"
            If CInt(cboViewBy.SelectedIndex) = 0 Then
                cboPeriod.Enabled = True
                btnPosting.Visible = True
                btnUnposting.Visible = False
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then
                btnPosting.Visible = False
                btnUnposting.Visible = True
            End If
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Protected Sub gvOTRequisitionList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOTRequisitionList.RowDataBound
        Try
            SetDateFormat()
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("Isgroup")) Then
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    AddGroup(e.Row, info1.ToTitleCase(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("Particulars").ToString().ToLower()), gvOTRequisitionList)
                    'Pinkal (16-Jul-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "objcolhIschange", False, True)).Text = False
                    'Pinkal (16-Jul-2020) -- End`
                Else
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAStarttime", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAStarttime", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAStarttime", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAStarttime", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAStarttime", False, True)).Text).ToString("HH:mm")
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAEndtime", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAEndtime", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAEndtime", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAEndtime", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "dgcolhAEndtime", False, True)).Text).ToString("HH:mm")
                    End If

                    'Pinkal (16-Jul-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvOTRequisitionList, "objcolhIschange", False, True)).Text = CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("ischange"))
                    'Pinkal (16-Jul-2020) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DateControl Events "

    Protected Sub dtpFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged, dtpToDate.TextChanged
        Try

            Dim dsCombo As DataSet = Nothing
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If


            Dim objEmpAssignOT As New clsassignemp_ot
            dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                 , dtpToDate.GetDate.Date, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpcodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEmpAssignOT = Nothing


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (03-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,"gbFilterCriteria", Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPeriod.ID, Me.lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblViewBy.ID, Me.lblViewBy.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnUnposting.ID, Me.btnUnposting.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnPosting.ID, Me.btnPosting.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnSave.ID, Me.btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionList.Columns(1).FooterText, Me.gvOTRequisitionList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionList.Columns(2).FooterText, Me.gvOTRequisitionList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionList.Columns(3).FooterText, Me.gvOTRequisitionList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionList.Columns(4).FooterText, Me.gvOTRequisitionList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.gvOTRequisitionList.Columns(5).FooterText, Me.gvOTRequisitionList.Columns(5).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblViewBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblViewBy.ID, Me.lblViewBy.Text)
            Me.btnUnposting.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnUnposting.ID, Me.btnUnposting.Text).Replace("&", "")
            Me.btnPosting.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnPosting.ID, Me.btnPosting.Text).Replace("&", "")
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.gvOTRequisitionList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionList.Columns(1).FooterText, Me.gvOTRequisitionList.Columns(1).HeaderText)
            Me.gvOTRequisitionList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionList.Columns(2).FooterText, Me.gvOTRequisitionList.Columns(2).HeaderText)
            Me.gvOTRequisitionList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionList.Columns(3).FooterText, Me.gvOTRequisitionList.Columns(3).HeaderText)
            Me.gvOTRequisitionList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionList.Columns(4).FooterText, Me.gvOTRequisitionList.Columns(4).HeaderText)
            Me.gvOTRequisitionList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gvOTRequisitionList.Columns(5).FooterText, Me.gvOTRequisitionList.Columns(5).HeaderText)


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Show Unposted OT Requisition Transactions")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Show Posted OT Requisition Transactions")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Period is compulsory information.Please Select Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Please check atleast one ot requisition to do futher operation on it.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "Please post/unpost atleast one ot requisition.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "OT Requisition posting done successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 8, "OT Requisition unposting done successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
