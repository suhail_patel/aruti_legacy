﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System

#End Region

Partial Class TnA_OT_Requisition_OT_EmployeeAssignmentList
    Inherits Basepage

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmOTEmployeeAssignmentList"
    Dim DisplayMessage As New CommonCodes
    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objAssignOT As clsassignemp_ot
    'Pinkal (03-Sep-2020) -- End
    Private mstrAdvanceFilter As String = String.Empty
    Private mintTnAEmployeeOTunkid As Integer = -1
#End Region

#Region "Page's Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objAssignOT = New clsassignemp_ot
            'Pinkal (03-Sep-2020) -- End

            If IsPostBack = False Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                Call FillCombo()
                Call FillList(True)

                'Pinkal (08-Jan-2020) -- Start
                'Enhancement - NMB - Working on NMB OT Requisition Requirement.
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    btnNew.Visible = CBool(Session("AllowToAddEmpOTAssignment"))
                    dgvEmployeeOTList.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployeeOTList, "btnDelete", False, True)).Visible = CBool(Session("AllowToDeleteEmpOTAssignment"))
                End If
                'Pinkal (08-Jan-2020) -- End

            Else
                mintTnAEmployeeOTunkid = CInt(Me.ViewState("tnaemployeeotunkid"))
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("tnaemployeeotunkid") = mintTnAEmployeeOTunkid
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "Employee", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objEmployee = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            'Pinkal (03-Sep-2020) -- End
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim dsList As DataSet = Nothing
        Dim objAssignOT As clsassignemp_ot
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim strSearching As String = ""
            Dim blnFlag As Boolean = False

            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewEmpOTAssignment")) = False Then Exit Sub
            End If
            'Pinkal (08-Jan-2020) -- End


            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter.Trim
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objAssignOT = New clsassignemp_ot
            'Pinkal (03-Sep-2020) -- End


            If isblank = False Then
                dsList = objAssignOT.GetList(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                         , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True _
                                                         , True, False, CInt(cboEmployee.SelectedValue), True, strSearching)
            Else
                dsList = objAssignOT.GetList(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                         , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True _
                                                         , True, False, CInt(cboEmployee.SelectedValue), True, strSearching).Clone()
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = dsList.Tables(0).NewRow()
                dr("tnaemployeeotunkid") = -1
                dr("employeeunkid") = -1
                dr("employeecode") = ""
                dr("employee") = ""
                dr("EmpcodeName") = ""
                dsList.Tables(0).Rows.Add(dr)
                isblank = True
            End If

            dgvEmployeeOTList.AutoGenerateColumns = False
            dgvEmployeeOTList.DataSource = dsList.Tables(0)
            dgvEmployeeOTList.DataBind()

            If isblank Then
                dgvEmployeeOTList.Items(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objAssignOT = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "TnA/OT_Requisition/OT_EmployeeAssignment.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            mstrAdvanceFilter = ""
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Response.Redirect("~/Userhome.aspx")
            Response.Redirect("~/Userhome.aspx", False)
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonNo_Click
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonYes_Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objAssignOT As New clsassignemp_ot
        'Pinkal (03-Sep-2020) -- End`
        Try
            objAssignOT._Isvoid = True
            objAssignOT._Voidreason = popDeleteReason.Reason
            objAssignOT._Userunkid = CInt(Session("UserId"))
            objAssignOT._Voiduserunkid = CInt(Session("UserId"))

            objAssignOT._WebFormName = mstrModuleName
            objAssignOT._WebClientIP = CStr(Session("IP_ADD"))
            objAssignOT._WebHostName = CStr(Session("HOST_NAME"))
            objAssignOT._IsFromWeb = True

            If objAssignOT.Delete(mintTnAEmployeeOTunkid) = False Then
                DisplayMessage.DisplayMessage(objAssignOT._Message, Me)
                mintTnAEmployeeOTunkid = 0
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Employee(s) OT Assignment deleted successfully."), Me)
                Call FillList(False)
                mintTnAEmployeeOTunkid = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objAssignOT = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region " LinkButton's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmployeeOTList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvEmployeeOTList.ItemCommand
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objAssignOT As clsassignemp_ot
        'Pinkal (03-Sep-2020) -- End
        Try
            If e.CommandName.ToUpper = "EDIT" Then
                Session.Add("tnaemployeeotunkid", CInt(dgvEmployeeOTList.DataKeys(e.Item.ItemIndex)))
                Response.Redirect(Session("rootpath").ToString & "TnA/OT_Requisition/OT_EmployeeAssignment.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                mintTnAEmployeeOTunkid = CInt(dgvEmployeeOTList.DataKeys(e.Item.ItemIndex))

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objAssignOT = New clsassignemp_ot
                'Pinkal (03-Sep-2020) -- End

                If objAssignOT.isUsed(CInt(e.Item.Cells(getColumnId_Datagrid(dgvEmployeeOTList, "objdgcolhemployeeunkid", False, True)).Text)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, you cannot be deleted this employee's OT assignment.Reason : This employee is already applied for OT requisistion."), Me)
                    Exit Sub
                End If

                ''Language.setLanguage(mstrModuleName)
                popDeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Are you sure you want to Delete this employee's OT assignment ?")
                popDeleteReason.Reason = ""
                popDeleteReason.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objAssignOT = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub dgvEmployeeOTList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmployeeOTList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployeeOTList, "dgcolhEmployee", False, True)).Text = info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployeeOTList, "dgcolhEmployee", False, True)).Text.Trim.ToLower())
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAllocation.ID, Me.lnkAllocation.ToolTip)
            'Gajanan [17-Sep-2020] -- End


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNew.ID, Me.btnNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployeeOTList.Columns(2).FooterText, dgvEmployeeOTList.Columns(2).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lnkAllocation.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.ToolTip)
            'Gajanan [17-Sep-2020] -- End


            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvEmployeeOTList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployeeOTList.Columns(2).FooterText, dgvEmployeeOTList.Columns(2).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Are you sure you want to Delete this employee's OT assignment ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, you cannot be deleted this employee's OT assignment.Reason : This employee is already applied for OT requisistion.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Employee(s) OT Assignment deleted successfully.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>
End Class
