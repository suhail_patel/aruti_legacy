<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="OTApproverMigration.aspx.vb"
    Inherits="TnA_OT_Requisition_OTApproverMigration" Title="OT Approver Migration" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }

      
        $("body").on("click", "[id*=chkSelectAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkSelectAll]", grid);
            debugger;
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtFrmSearch').val().length > 0) {
                $('#<%= dgOldApproverEmp.ClientID %> tbody tr').hide();
                $('#<%= dgOldApproverEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgOldApproverEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtFrmSearch').val() + '\')').parent().show();
            }
            else if ($('#txtFrmSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgOldApproverEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtFrmSearch').val('');
            $('#<%= dgOldApproverEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtFrmSearch').focus();
        }


        function ToSearching() {
            if ($('#txtToSearch').val().length > 0) {
                $('#<%= dgNewApproverAssignEmp.ClientID %> tbody tr').hide();
                $('#<%= dgNewApproverAssignEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgNewApproverAssignEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtToSearch').val() + '\')').parent().show();
            }
            else if ($('#txtToSearch').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= dgNewApproverAssignEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtToSearch').val('');
            $('#<%= dgNewApproverAssignEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtToSearch').focus();
        }
        
        
    </script>

    <asp:Panel ID="pnlmain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approver Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkCapApprovers" runat="server" Text="HOD Cap OT Approvers" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowInactiveApprovers" runat="server" AutoPostBack="true" Text="Include Inactive Approver" />
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowInActiveEmployees" runat="server" AutoPostBack="true" Text="Include Inactive Employee" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblOldApprover" runat="server" Text="From Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboOldApprover" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblNewApprover" runat="server" Text="To Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboNewApprover" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblOldLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboOldLevel" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblNewLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboNewLevel" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="txtFrmSearch" name="txtSearch" placeholder="Type To Search Text"
                                                    maxlength="50" onkeyup="FromSearching();" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="txtToSearch" name="txtSearch" placeholder="Type To Search Text"
                                                    maxlength="50" onkeyup="ToSearching();" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="dgOldApproverEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false" DataKeyNames="tnaapprovertranunkid,tnamappingunkid,employeeunkid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgEmployee" ItemStyle-Width = "500px" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="dgNewApproverAssignEmp" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="tnaapprovertranunkid,tnamappingunkid,employeeunkid">
                                                <Columns>
                                                    <asp:BoundField DataField="ename" HeaderText="Assigned Employee" ReadOnly="true"
                                                        FooterText="colhdgAssignEmployee" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnTransfer" runat="server" Text="Transfer" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
