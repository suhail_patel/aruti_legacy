<%@ Page Title="OT Requisition List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="OTRequisition.aspx.vb" Inherits="OT_OTRequisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="OT Requisition List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListOTReqFromDate" runat="server" Text="Req. From Date " Width="100%"
                                            CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpListOTReqFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListEmployee" runat="server" Text="Employee" Width="100%" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboListEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListOTReqToDate" runat="server" Text="Req. To Date " Width="100%"
                                            CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpListOTReqToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListStatus" runat="server" Text="Status" Width="100%" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboListStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnListNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                    <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                    <asp:Button ID="btnListClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card" style="height: 350px;overflow:auto">
                            <div class="body" >
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" >
                                            <asp:GridView ID="gvOTRequisitionList" runat="server" DataKeyNames="otrequisitiontranunkid, empcodename, issubmit_approval"
                                                AutoGenerateColumns="False" AllowPaging="false" ShowFooter="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-Width="35px" HeaderStyle-Width="35px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" CommandArgument='<%#Eval("otrequisitiontranunkid")%>'
                                                                    ToolTip="Edit" CommandName="Change" OnClick="lnkedit_Click">
                                                                      <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-Width="35px" HeaderStyle-Width="35px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                    CommandArgument='<%#Eval("otrequisitiontranunkid")%>' CommandName="Remove" OnClick="lnkdelete_Click">
                                                                      <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                  <%--  <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhcancel" Visible="false"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkcancel" runat="server" ToolTip="cancel" CommandArgument='<%#Eval("otrequisitiontranunkid")%>'
                                                                CommandName="Cancel">
                                                                    <i class="fa fa-ban" style="color:Red; font-size: 18px"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    
                                                    <asp:BoundField HeaderText="Employee" DataField="empcodename" ItemStyle-VerticalAlign="Top"
                                                        Visible="false" FooterText="colhotreqemp" />
                                                    <asp:BoundField HeaderText="Request Date" DataField="requestdate" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotrequestdate" />
                                                    <asp:BoundField HeaderText="Planned Start Time" DataField="plannedstart_time" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqplannedstart_time" />
                                                    <asp:BoundField HeaderText="Planned End Time" DataField="plannedend_time" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqplannedend_time" />
                                                    <asp:BoundField HeaderText="Planned OT Hours" DataField="PlannedworkedDuration" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqplannedot_hours" />
                                                    <asp:BoundField HeaderText="Actual Start Time" DataField="actualstart_time" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqactualstart_time" />
                                                    <asp:BoundField HeaderText="Actual End Time" DataField="actualend_time" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqactualend_time" />
                                                    <asp:BoundField HeaderText="Actual OT Hours" DataField="ActualworkedDuration" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqactualot_hours" />
                                                    <asp:BoundField HeaderText="Status" DataField="status" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqstatus" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupOTRequisition" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblEmployee" runat="server" PopupControlID="pnlOTRequisition"
                    DropShadow="false" CancelControlID="lblPageHeader">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlOTRequisition" runat="server" CssClass="card modal-dialog modal-lg "
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lbladdeditPageHeader" Text="OT Requisition Add/Edit" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="height: 315px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblRequestDate" runat="server" Text="Request Date" Width="100%" CssClass="form-label"></asp:Label>
                                <uc1:DateCtrl ID="dtpRequestDate" runat="server" AutoPostBack="True" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" Width="100%" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="lblPlannedStartTime" runat="server" Text="Planned Start Time" CssClass="form-label"></asp:Label>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-l-0 p-r-0">
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboPlannedStartTimehr" runat="server" />
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-l-0 p-r-0 text-center m-t-10">
                                    <asp:Label ID="lblPlannedStarTimecolon" runat="server" Text=" : " CssClass="form-label"></asp:Label>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-l-0 p-r-0">
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboPlannedStartTimemin" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="lblPlannedEndTime" runat="server" Text="Planned End Time" CssClass="form-label"></asp:Label>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-l-0 p-r-0">
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboPlannedEndTimehr" runat="server" />
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-l-0 p-r-0 text-center m-t-10">
                                    <asp:Label ID="lblPlannedEndTimecolon" runat="server" Text=" : " CssClass="form-label"></asp:Label>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-l-0 p-r-0">
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboPlannedEndTimemin" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                <asp:Label ID="lblRequestReason" runat="server" Text="Reason For Request" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtRequestReason" runat="server" Width="99%" TextMode="MultiLine"
                                            CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <asp:Panel ID="pnlActualTime" runat="server">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblActualStartTime" runat="server" Text="Actual Start Time" Width="100%"
                                        CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboActualStartTimehr" runat="server" />
                                    </div>
                                    <asp:Label ID="lblActualStarTimecolon" runat="server" Text=" : "></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboActualStartTimemin" runat="server" />
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblActualEndTime" runat="server" Text="Actual End Time" Width="100%"
                                        CssClass="form-control"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboActualEndTimehr" runat="server" />
                                    </div>
                                    <asp:Label ID="lblActualEndTimecolon" runat="server" Text=" : "></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboActualEndTimemin" runat="server" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                    <asp:Label ID="lblReasonForAdjustment" runat="server" Text="Reason For Adjustment"
                                        CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtReasonForAdjustment" runat="server" Width="99%" TextMode="MultiLine"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="footer">
                        <div class="pull-left d--f">
                            <h4>
                                <asp:Label ID="lblPeriodStartDate" runat="server" Text="Start Date: " CssClass="label label-primary m-r-10"></asp:Label>
                            </h4>
                            <h4>
                                <asp:Label ID="lblPeriodEndDate" runat="server" Text="End Date: " CssClass="label label-primary"></asp:Label></h4>
                        </div>
                        <asp:Button ID="btnSaveOTRequiApprover" runat="server" CssClass="btn btn-primary"
                            Text="Save" />
                        <asp:Button ID="btnSaveSubmitOTRequiApprover" runat="server" CssClass="btn btn-default"
                            Text="Save And Submit" />
                        <asp:Button ID="btnCloseOTRequiApprover" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                    </div>
                </asp:Panel>
                <uc7:Confirmation ID="popupConfirmationForOTCap" Title="" runat="server" Message="" />
                <uc7:Confirmation ID="popupConfirmationOTRequiReason" Title="" runat="server" Message="" />
                <ucDel:DeleteReason ID="popupDeleteOTRequiReason" runat="server" Title="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
