﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
Imports System.Drawing

#End Region

Partial Class TnA_OT_Requisition_OT_CancelRequisition
    Inherits Basepage

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "frmOTCancelRequisition"
    Private DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objOTRequisition As clsOT_Requisition_Tran
    'Pinkal (03-Sep-2020) -- End

    Private mdicETS As Dictionary(Of String, Integer)
    Private mdtOTTimesheet As DataTable = Nothing
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintPeriodID As Integer = -1
    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.
    Private mstrOTRequisitionIds As String = ""
    'Pinkal (12-Oct-2021)-- End
#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objOTRequisition = New clsOT_Requisition_Tran
            'Pinkal (03-Sep-2020) -- End

            mdicETS = gvOTRequisitionList.Columns.Cast(Of DataControlField).ToDictionary(Function(x) x.FooterText, Function(x) gvOTRequisitionList.Columns.IndexOf(x))

            If IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                'Call FillCombo()

                'Pinkal (27-Feb-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                'dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpFromDate_TextChanged(dtpFromDate, New EventArgs())
                dtpFromDate.SetDate = mdtStartDate.Date
                dtpToDate.SetDate = mdtEndDate.Date
                dtpFromDate.Enabled = False
                dtpToDate.Enabled = False
                'Pinkal (27-Feb-2020) -- End

                FillList(True)
            Else
                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.
                'mdtOTTimesheet = CType(Me.ViewState("mdtOTTimesheet"), DataTable)
                mstrOTRequisitionIds = Me.ViewState("OTRequisitionIds").ToString()
                'Pinkal (12-Oct-2021)-- End
                mdtStartDate = CDate(Me.ViewState("StartDate"))
                mdtEndDate = CDate(Me.ViewState("EndDate"))
            End If
            Call SetControlVisibility()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'Me.ViewState("mdtOTTimesheet") = mdtOTTimesheet
            Me.ViewState("OTRequisitionIds") = mstrOTRequisitionIds
            'Pinkal (12-Oct-2021)-- End
            Me.ViewState("StartDate") = mdtStartDate
            Me.ViewState("EndDate") = mdtEndDate
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            'Dim dsList As DataSet = Nothing
            'Dim objMaster As New clsMasterData
            'Dim objPeriod As New clscommom_period_Tran

            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
            '                                   CDate(Session("fin_startdate")).Date, "List", True, enStatusType.OPEN)
            'With cboPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("List")
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'objPeriod = Nothing
            'dsList = Nothing
            'Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    'Private Sub FillList(ByVal isblank As Boolean)
    '    'Pinkal (03-Sep-2020) -- Start
    '    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Dim dsList As DataSet = Nothing
    '    Dim objOTRequisition As clsOT_Requisition_Tran
    '    'Pinkal (03-Sep-2020) -- End
    '    Try


    '        Dim strSearch As String = ""

    '        If dtpFromDate.IsNull = False AndAlso dtpFromDate.GetDate <> Nothing Then
    '            'Pinkal (27-Jun-2020) -- Start
    '            'Enhancement NMB -   Working on Employee Signature Report.
    '            'strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) >='" & eZeeDate.convertDate(dtpFromDate.GetDate) & "' "
    '            strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) >='" & eZeeDate.convertDate(mdtStartDate.Date) & "' "
    '            'Pinkal (27-Jun-2020) -- End
    '        End If

    '        If dtpToDate.IsNull = False AndAlso dtpToDate.GetDate <> Nothing Then
    '            'Pinkal (27-Jun-2020) -- Start
    '            'Enhancement NMB -   Working on Employee Signature Report.
    '            'strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) <='" & eZeeDate.convertDate(dtpToDate.GetDate) & "' "
    '            strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) <='" & eZeeDate.convertDate(mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date) & "' "
    '            'Pinkal (27-Jun-2020) -- End
    '        End If

    '        If CInt(cboEmployee.SelectedValue) > 0 Then
    '            strSearch &= "AND tnaot_requisition_tran.employeunkid  =" & CInt(cboEmployee.SelectedValue) & " "
    '        End If

    '        If strSearch.Trim.Length > 0 Then
    '            strSearch = strSearch.Substring(3)
    '        End If

    '        Dim mblnApplyAccessFilter As Boolean = True
    '        If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
    '            mblnApplyAccessFilter = False
    '        End If


    '        'Pinkal (27-Feb-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.
    '        Dim xEmployeeId As Integer = -1
    '        If CInt(cboEmployee.SelectedValue) > 0 Then
    '            xEmployeeId = CInt(cboEmployee.SelectedValue)
    '        End If
    '        'Pinkal (27-Feb-2020) -- End



    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '        objOTRequisition = New clsOT_Requisition_Tran
    '        'Pinkal (03-Sep-2020) -- End


    '        ' GET ONLY APPROVED OT REQUISITION
    '        If isblank Then

    '            'Pinkal (27-Feb-2020) -- Start
    '            'Enhancement - Changes related To OT NMB Testing.

    '            'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '            '                                           , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
    '            '                                           , True, mblnApplyAccessFilter, "", Nothing, CInt(cboEmployee.SelectedValue), "", mdtStartDate.Date, mdtEndDate.Date _
    '            '                                          , 1, 0, False, 1, True, True).Clone()



    '            'Pinkal (27-Jun-2020) -- Start
    '            'Enhancement NMB -   Working on Employee Signature Report.

    '            'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '            '                                       , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
    '            '                                       , True, mblnApplyAccessFilter, "", Nothing, xEmployeeId, "", mdtStartDate.Date, mdtEndDate.Date _
    '            '                                       , 1, 0, False, 1, True, True).Clone()

    '            dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '                                                      , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
    '                                                   , True, mblnApplyAccessFilter, "", Nothing, xEmployeeId, "", mdtStartDate.Date, mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date _
    '                                                      , 1, 0, False, 1, True, True).Clone()

    '            'Pinkal (27-Jun-2020) -- End



    '            'Pinkal (27-Feb-2020) -- End

    '        Else


    '            'Pinkal (27-Feb-2020) -- Start
    '            'Enhancement - Changes related To OT NMB Testing.

    '            'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '            '                                           , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
    '            '                                           , True, mblnApplyAccessFilter, "", Nothing, CInt(cboEmployee.SelectedValue), "", mdtStartDate.Date, mdtEndDate.Date _
    '            '                                           , 1, 0, False, 1, True, True)


    '            'Pinkal (27-Jun-2020) -- Start
    '            'Enhancement NMB -   Working on Employee Signature Report.

    '            'dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '            '                                           , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
    '            '                                          , True, mblnApplyAccessFilter, "", Nothing, xEmployeeId, "", mdtStartDate.Date, mdtEndDate.Date _
    '            '                                           , 1, 0, False, 1, True, True)

    '            dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
    '                                                       , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
    '                                                      , True, mblnApplyAccessFilter, "", Nothing, xEmployeeId, "", mdtStartDate.Date, mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date _
    '                                                       , 1, 0, False, 1, True, True)

    '            'Pinkal (27-Jun-2020) -- End



    '            'Pinkal (27-Feb-2020) -- End

    '        End If


    '        If dsList.Tables("List").Columns.Contains("IsGrp") = False Then
    '            Dim dc As New DataColumn("IsGrp", Type.GetType("System.Boolean"))
    '            dc.DefaultValue = False
    '            dsList.Tables(0).Columns.Add(dc)
    '            dc = Nothing
    '        End If

    '        If dsList.Tables("List").Columns.Contains("ischecked") = False Then
    '            Dim dc As New DataColumn("ischecked", Type.GetType("System.Boolean"))
    '            dc.DefaultValue = False
    '            dsList.Tables(0).Columns.Add(dc)
    '            dc = Nothing
    '        End If

    '        If dsList.Tables("List").Columns.Contains("PlannedworkedDuration") = False Then
    '            Dim dc As New DataColumn("PlannedworkedDuration", Type.GetType("System.String"))
    '            dc.DefaultValue = ""
    '            dsList.Tables(0).Columns.Add(dc)
    '            dc = Nothing
    '        End If

    '        If dsList.Tables("List").Columns.Contains("ActualworkedDuration") = False Then
    '            Dim dc As New DataColumn("ActualworkedDuration", Type.GetType("System.String"))
    '            dc.DefaultValue = ""
    '            dsList.Tables(0).Columns.Add(dc)
    '            dc = Nothing
    '        End If

    '        Dim TotalSec As Integer = 0
    '        For Each dtRow As DataRow In dsList.Tables("List").Rows()
    '            dtRow("PlannedworkedDuration") = CalculateTime(True, CInt(dtRow("plannedot_hours"))).ToString("#00.00").Replace(".", ":")
    '            dtRow("ActualworkedDuration") = CalculateTime(True, CInt(dtRow("actualot_hours"))).ToString("#00.00").Replace(".", ":")
    '        Next

    '        If isblank = True OrElse dsList.Tables("List").Rows.Count <= 0 Then
    '            Dim drRow As DataRow = dsList.Tables("List").NewRow()
    '            drRow("otrequisitiontranunkid") = -1
    '            drRow("employeeunkid") = -1
    '            drRow("empcodename") = ""
    '            dsList.Tables("List").Rows.Add(drRow)
    '            isblank = True
    '        End If

    '        Dim mdtTran As DataTable = Nothing
    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso isblank = False Then
    '            mdtTran = New DataView(dsList.Tables("List"), "", "Employeeunkid", DataViewRowState.CurrentRows).ToTable()
    '            Dim dtTable As DataTable = mdtTran.Clone
    '            Dim strEmployeeunkid As String = ""
    '            Dim dtRow As DataRow = Nothing

    '            For Each drow As DataRow In mdtTran.Rows
    '                If CStr(drow("Employeeunkid")).Trim <> strEmployeeunkid.Trim Then
    '                    dtRow = dtTable.NewRow
    '                    dtRow("IsGrp") = True
    '                    dtRow("otrequisitiontranunkid") = -1
    '                    dtRow("empcodename") = drow("empcodename")
    '                    dtRow("issubmit_approval") = drow("issubmit_approval")

    '                    If IsDBNull(drow("requestdate")) = False Then
    '                        dtRow("requestdate") = CDate(drow("requestdate"))
    '                    End If

    '                    dtRow("employeeunkid") = CInt(drow("employeeunkid"))
    '                    strEmployeeunkid = drow("Employeeunkid").ToString()
    '                    dtTable.Rows.Add(dtRow)
    '                End If
    '                dtRow = dtTable.NewRow
    '                For Each dtcol As DataColumn In mdtTran.Columns
    '                    If dtcol.ColumnName.Contains("empcodename") Then
    '                        dtRow(dtcol.ColumnName) = ""
    '                    Else
    '                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
    '                    End If
    '                Next
    '                dtTable.Rows.Add(dtRow)
    '            Next

    '            'Pinkal (03-Sep-2020) -- Start
    '            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '            'mdtOTTimesheet = dtTable
    '            mdtOTTimesheet = dtTable.Copy
    '            If dtTable IsNot Nothing Then dtTable.Clear()
    '            dtTable = Nothing
    '            'Pinkal (03-Sep-2020) -- End
    '        Else
    '            mdtOTTimesheet = dsList.Tables("List")
    '        End If


    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '        If mdtTran IsNot Nothing Then mdtTran.Clear()
    '        mdtTran = Nothing
    '        'Pinkal (03-Sep-2020) -- End


    '        gvOTRequisitionList.DataSource = mdtOTTimesheet
    '        gvOTRequisitionList.DataBind()

    '        If isblank Then
    '            gvOTRequisitionList.Rows(0).Visible = False
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Clear()
    '        dsList = Nothing
    '        objOTRequisition = Nothing
    '        ''Pinkal (03-Sep-2020) -- End
    '    End Try
    'End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As DataSet = Nothing
        Dim objOTRequisition As clsOT_Requisition_Tran
        Try

            gvOTRequisitionList.DataSource = Nothing
            gvOTRequisitionList.DataBind()

            Dim strSearch As String = ""

            If dtpFromDate.IsNull = False AndAlso dtpFromDate.GetDate <> Nothing Then
                strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) >='" & eZeeDate.convertDate(mdtStartDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False AndAlso dtpToDate.GetDate <> Nothing Then
                strSearch &= "AND CONVERT(char(8),tnaot_requisition_tran.requestdate,112) <='" & eZeeDate.convertDate(mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND tnaot_requisition_tran.employeunkid  =" & CInt(cboEmployee.SelectedValue) & " "
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            Dim mblnApplyAccessFilter As Boolean = True
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                mblnApplyAccessFilter = False
            End If

            Dim xEmployeeId As Integer = -1
            If CInt(cboEmployee.SelectedValue) > 0 Then
                xEmployeeId = CInt(cboEmployee.SelectedValue)
            End If

            objOTRequisition = New clsOT_Requisition_Tran

            ' GET ONLY APPROVED OT REQUISITION

            If isblank Then
                dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                          , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                                                       , True, mblnApplyAccessFilter, "", Nothing, xEmployeeId, "", mdtStartDate.Date, mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date _
                                                          , 1, 0, False, 1, True, True).Clone()

            Else
                dsList = objOTRequisition.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                           , CBool(Session("IsIncludeInactiveEmp")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                                                          , True, mblnApplyAccessFilter, "", Nothing, xEmployeeId, "", mdtStartDate.Date, mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date _
                                                       , 1, 0, True, 1, True, True)
            End If



            If dsList.Tables("List").Columns.Contains("ischecked") = False Then
                Dim dc As New DataColumn("ischecked", Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                dsList.Tables(0).Columns.Add(dc)
                dc = Nothing
            End If

            If dsList.Tables("List").Columns.Contains("PlannedworkedDuration") = False Then
                Dim dc As New DataColumn("PlannedworkedDuration", Type.GetType("System.String"))
                dc.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dc)
                dc = Nothing
            End If

            If dsList.Tables("List").Columns.Contains("ActualworkedDuration") = False Then
                Dim dc As New DataColumn("ActualworkedDuration", Type.GetType("System.String"))
                dc.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dc)
                dc = Nothing
            End If


            Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False)
            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                For Each dtRow As DataRow In drRow
                    dtRow("PlannedworkedDuration") = CalculateTime(True, CInt(dtRow("plannedot_hours"))).ToString("#00.00").Replace(".", ":")
                    dtRow("ActualworkedDuration") = CalculateTime(True, CInt(dtRow("actualot_hours"))).ToString("#00.00").Replace(".", ":")
                    dtRow.AcceptChanges()
                Next
            End If

            Dim mdtTran As DataTable = Nothing
            mdtTran = New DataView(dsList.Tables("List"), "", "Employeeunkid", DataViewRowState.CurrentRows).ToTable()

            gvOTRequisitionList.DataSource = mdtTran.Copy()
            gvOTRequisitionList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables("List").NewRow()
                drRow("IsGrp") = False
                drRow("otrequisitiontranunkid") = -1
                drRow("employeeunkid") = -1
                drRow("empcodename") = ""
                dsList.Tables("List").Rows.Add(drRow)

                gvOTRequisitionList.DataSource = dsList.Tables(0).Copy()
                gvOTRequisitionList.DataBind()

                gvOTRequisitionList.Rows(0).Visible = False

            End If

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objOTRequisition = Nothing
        End Try
    End Sub

    'Pinkal (12-Oct-2021)--End 

    Private Sub SetControlVisibility()
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetAtValue(ByVal xFormName As String)
    Private Sub SetAtValue(ByVal xFormName As String, ByVal objOTRequisition As clsOT_Requisition_Tran)
        'Pinkal (03-Sep-2020) -- End
        Try
            objOTRequisition._Statusunkid = 6 'Cancelled Status
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objOTRequisition._Userunkid = CInt(Session("UserId"))
            Else
                objOTRequisition._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objOTRequisition._WebClientIP = CStr(Session("IP_ADD"))
            objOTRequisition._WebHostName = CStr(Session("HOST_NAME"))
            objOTRequisition._WebFormName = xFormName
            objOTRequisition._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Reset()
        Try
            'cboPeriod.SelectedValue = "0"
            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            'dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                dtpFromDate.SetDate = mdtStartDate
                dtpToDate.SetDate = mdtEndDate
            Else
                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpFromDate_TextChanged(dtpFromDate, New EventArgs())
            End If
            'Pinkal (02-Jun-2020) -- End
            txtCancelReason.Text = ""
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then cboEmployee.SelectedValue = "0"
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    'Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '    Try
    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

    '            mdtStartDate = CDate(objPeriod._TnA_StartDate).Date
    '            mdtEndDate = CDate(objPeriod._TnA_EndDate).Date
    '        Else
    '            mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
    '            mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
    '        End If

    '        dtpFromDate.SetDate = mdtStartDate.Date
    '        dtpToDate.SetDate = mdtEndDate.Date



    '        'Pinkal (24-Oct-2019) -- Start
    '        'Enhancement NMB - Working On OT Enhancement for NMB.

    '        'Dim objEmployee As New clsEmployee_Master
    '        'Dim dsList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
    '        '                                                                               CInt(Session("CompanyUnkId")), mdtStartDate, mdtEndDate, _
    '        '                                                                               CStr(Session("UserAccessModeSetting")), True, _
    '        '                                                                               CBool(Session("IsIncludeInactiveEmp")), "List", True)

    '        Dim objEmpAssignOT As New clsassignemp_ot
    '        Dim dsList As DataSet = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
    '                                                                             , mdtEndDate, CStr(Session("UserAccessModeSetting")), True, True, False, 0, True, "", True)

    '        objEmpAssignOT = Nothing

    '        With cboEmployee
    '            .DataValueField = "employeeunkid"
    '            .DataTextField = "EmpCodeName"
    '            .DataSource = dsList.Tables("List")
    '            .DataBind()
    '            .SelectedValue = "0"
    '        End With
    '        'objEmployee = Nothing
    '        dsList = Nothing

    '        'Pinkal (24-Oct-2019) -- End

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is compulsory information. Please Select Period."), Me)
            '    cboPeriod.Focus()
            '    Exit Sub
            'End If
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Reset()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        If txtCancelReason.Text.Trim.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Cancel Reason cannot be blank.Cancel Reason is required information."), Me)
    '            txtCancelReason.Focus()
    '            Exit Sub
    '        End If


    '        'Pinkal (10-Jan-2020) -- Start
    '        'Enhancements -  Working on OT Requisistion for NMB.

    '        'Dim dtEmpTimeSheet As DataTable = New DataView(mdtOTTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable

    '        'If dtEmpTimeSheet.Rows.Count <= 0 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
    '        '    Exit Sub
    '        'End If


    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
    '    Exit Sub
    '        End If


    '        'Pinkal (02-Jun-2020) -- Start
    '        'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
    '        Dim mstrDates As String = ""
    '        'Pinkal (02-Jun-2020) -- End

    '        Dim dtEmpTimeSheet As DataTable = mdtOTTimesheet.Clone
    '        Dim xCount As Integer = -1
    '        For i As Integer = 0 To gRow.Count - 1
    '            xCount = i
    '            Dim dRow As DataRow() = mdtOTTimesheet.Select("isGrp = 0 AND otrequisitiontranunkid = " & CInt(gvOTRequisitionList.DataKeys(gRow(xCount).DataItemIndex)("otrequisitiontranunkid")))
    '            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
    '                dRow(0)("ischecked") = True
    '                dRow(0).AcceptChanges()
    '                dtEmpTimeSheet.ImportRow(dRow(0))
    '                'Pinkal (02-Jun-2020) -- Start
    '                'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
    '                mstrDates &= eZeeDate.convertDate(CDate(dRow(0)("requestdate")).Date).ToString() & ","
    '                'Pinkal (02-Jun-2020) -- End
    '            End If
    '        Next
    '        'Pinkal (10-Jan-2020) -- End


    '        'Pinkal (02-Jun-2020) -- Start
    '        'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
    '        If mstrDates.Trim.Length > 0 Then
    '            mstrDates = mstrDates.Substring(0, mstrDates.Length - 1)


    '            Dim arDates As String() = mstrDates.Trim.Split(CChar(","))

    '            If arDates.Length > 0 Then

    '                'Pinkal (04-Aug-2020) -- Start
    '                'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	

    '                '    If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse CBool(Session("IsArutiDemo")) Then
    '                '        Dim objMstdata As New clsMasterData
    '                '        Dim objPeriod As New clscommom_period_Tran

    '                '        For i As Integer = 0 To arDates.Length - 1

    '                '            'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, dtpFromDate.GetDate.Date, CInt(Session("Fin_year")), 0, True)
    '                '            Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, eZeeDate.convertDate(arDates(i).ToString()).Date, CInt(Session("Fin_year")), 0, True)
    '                '            objPeriod._Periodunkid(Session("Database_Name").ToString) = intPeriod
    '                '            If objPeriod._Statusid = enStatusType.CLOSE Then
    '                '                ''Language.setLanguage(mstrModuleName)
    '                '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry,you cannot cancel ot requisition for selected employee(s).Reason:Period is already over."), Me)

    '                '                Dim dr = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False And x.Field(Of DateTime)("requestdate") = eZeeDate.convertDate(arDates(i).ToString()).Date)
    '                '                If dr IsNot Nothing AndAlso dr.Count > 0 Then
    '                '                    For x As Integer = 0 To dr.Count - 1
    '                '                        dr(i)("ischecked") = False
    '                '                        dr(i).AcceptChanges()
    '                '                    Next
    '                '                End If
    '                '                Exit Sub
    '                '            End If

    '                '            If intPeriod > 0 Then

    '                '                Dim mdtTnADate As DateTime = Nothing

    '                '                'If objPeriod._TnA_EndDate.Date < dtpFromDate.GetDate.Date Then
    '                '                '    mdtTnADate = dtpFromDate.GetDate.Date
    '                '                'Else
    '                '                '    mdtTnADate = objPeriod._TnA_EndDate.Date
    '                '                'End If

    '                '                If objPeriod._TnA_EndDate.Date < eZeeDate.convertDate(arDates(i).ToString()).Date Then
    '                '                    mdtTnADate = eZeeDate.convertDate(arDates(i).ToString()).Date
    '                '                Else
    '                '                    mdtTnADate = objPeriod._TnA_EndDate.Date
    '                '                End If

    '                '                Dim mstrEmployeeIDs As String = String.Join(",", dtEmpTimeSheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())

    '                '                Dim objLeaveTran As New clsTnALeaveTran
    '                '                If objLeaveTran.IsPayrollProcessDone(intPeriod, mstrEmployeeIDs, mdtTnADate.Date) Then
    '                '                    ''Language.setLanguage(mstrModuleName)
    '                '                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry,you cannot cancel ot requisition for selected employee(s).Reason:Payroll Process already done for selected employee(s) for last date of current open period."), Me)
    '                '                    Dim dr = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of DateTime)("requestdate") = eZeeDate.convertDate(arDates(i).ToString()).Date)
    '                '                    If dr IsNot Nothing AndAlso dr.Count > 0 Then
    '                '                        For x As Integer = 0 To dr.Count - 1
    '                '                            dr(i)("ischecked") = False
    '                '                            dr(i).AcceptChanges()
    '                '                        Next
    '                '                    End If
    '                '                    Exit Sub
    '                '                End If
    '                '                objLeaveTran = Nothing

    '                '            End If

    '                '        Next

    '                '    End If

    '                Dim count As Integer = 0
    '        For i As Integer = 0 To arDates.Length - 1
    '                    count = i
    '                    If Not (eZeeDate.convertDate(arDates(i).ToString()).Date >= mdtStartDate.Date AndAlso eZeeDate.convertDate(arDates(i).ToString()).Date <= mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date) Then
    '                ''Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry,you cannot cancel ot requisition for selected employee(s).Reason:Period is already over."), Me)

    '                        Dim dr = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False And x.Field(Of DateTime)("requestdate") = eZeeDate.convertDate(arDates(count).ToString()).Date)
    '                If dr IsNot Nothing AndAlso dr.Count > 0 Then
    '                    For x As Integer = 0 To dr.Count - 1
    '                        dr(i)("ischecked") = False
    '                        dr(i).AcceptChanges()
    '                    Next
    '                End If
    '                Exit Sub
    '            End If
    '                Next

    '                'Pinkal (04-Aug-2020) -- End

    '            End If

    '        End If

    '        'Pinkal (02-Jun-2020) -- End


    '        ''Language.setLanguage(mstrModuleName)
    '        popupConfirmYesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Cancel OT Requisition")
    '        popupConfirmYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Are you sure you want to cancel this ot requisition?")
    '        popupConfirmYesNo.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
    '    'Pinkal (03-Sep-2020) -- Start
    '    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Dim objOTRequisition As clsOT_Requisition_Tran
    '    'Pinkal (03-Sep-2020) -- End
    '    Try
    '        Dim dtEmpTimeSheet As DataTable = New DataView(mdtOTTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable

    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '        'SetAtValue(mstrModuleName)
    '        objOTRequisition = New clsOT_Requisition_Tran
    '        SetAtValue(mstrModuleName, objOTRequisition)
    '        'Pinkal (03-Sep-2020) -- End

    '        If objOTRequisition.CancelApprovedOTRequisition(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
    '                                                                               , CInt(Session("UserId")), dtEmpTimeSheet, txtCancelReason.Text.Trim()) = False Then
    '            DisplayMessage.DisplayMessage(objOTRequisition._Message, Me)
    '            Exit Sub
    '                Else
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "OT Requisition cancelled successfully."), Me)


    '            Dim mintLoginTypeID As Integer = 0
    '            Dim mintLoginEmployeeID As Integer = 0
    '            Dim mintUserID As Integer = 0
    '            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
    '                mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
    '                mintLoginEmployeeID = 0
    '                mintUserID = CInt(Session("UserId"))
    '            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
    '                mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
    '                mintLoginEmployeeID = CInt(Session("Employeeunkid"))
    '                mintUserID = 0
    '                End If

    '            Dim strEmployeeIDs As String = String.Join(",", dtEmpTimeSheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
    '            Dim dtSelectedData As DataTable = dtEmpTimeSheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True).ToList().CopyToDataTable()

    '            For Each strEmpID As String In strEmployeeIDs.Split(CChar(","))
    '                Dim intEmployeeID As Integer = CInt(strEmpID)

    '                Dim strOTRequisitionIDs As String = String.Join(",", dtSelectedData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID _
    '                                                                                                 AndAlso x.Field(Of Boolean)("IsGrp") = False).Select(Function(x) x.Field(Of Integer)("otrequisitiontranunkid").ToString).Distinct().ToArray())


    '                'Pinkal (02-Jun-2020) -- Start
    '                'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
    '                'If Session("AllowApplyOTForEachMonthDay") IsNot Nothing AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) > 0 Then
    '                'mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, CInt(Session("AllowApplyOTForEachMonthDay")) + 1)
    '                If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
    '                    mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
    '                    If mdtStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
    '                        'mdtStartDate = mdtStartDate.AddDays(-31)
    '                        mdtStartDate = mdtStartDate.AddDays(-1 * CInt(Session("OTTenureDays")) + 1)
    '                    Else
    '                        Dim mdtTempDate As DateTime = mdtStartDate.AddMonths(-1)
    '                        If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
    '                            mdtStartDate = mdtTempDate
    '                    End If
    '                End If

    '                    'mdtEndDate = mdtStartDate.AddDays(30)\
    '                    mdtEndDate = mdtStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
    '                    'Pinkal (02-Jun-2020) -- End
    '                Else
    '                    mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
    '                    mdtEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
    '            End If



    '                objOTRequisition.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
    '                                                                                 CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                                                 CBool(Session("IsIncludeInactiveEmp")), Session("UserAccessModeSetting").ToString(), _
    '                                                                                 CStr(intEmployeeID), strOTRequisitionIDs, dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, 6, _
    '                                                                                 mintLoginTypeID, mintLoginEmployeeID, mintUserID, txtCancelReason.Text)
    '        Next


    '            'Pinkal (03-Sep-2020) -- Start
    '            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '            If dtSelectedData IsNot Nothing Then dtSelectedData.Clear()
    '            dtSelectedData = Nothing
    '            If dtEmpTimeSheet IsNot Nothing Then dtEmpTimeSheet.Clear()
    '            dtEmpTimeSheet = Nothing
    '            'Pinkal (03-Sep-2020) -- End
    '            Reset()
    '    End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Finally
    '        objOTRequisition = Nothing
    '        'Pinkal (03-Sep-2020) -- End
    '    End Try
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If txtCancelReason.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Cancel Reason cannot be blank.Cancel Reason is required information."), Me)
                txtCancelReason.Focus()
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            Dim mstrDates As String = String.Join(",", gRow.AsEnumerable().Where(Function(x) CBool(gvOTRequisitionList.DataKeys(x.RowIndex)("IsGrp")) = False).Select(Function(x) eZeeDate.convertDate(CDate(x.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate", False, True)).Text).Date).ToString()).Distinct().ToArray())
            mstrOTRequisitionIds = String.Join(",", (From p In gRow.Where(Function(x) CBool(gvOTRequisitionList.DataKeys(x.RowIndex)("IsGrp")) = False) Select (gvOTRequisitionList.DataKeys(p.RowIndex)("otrequisitiontranunkid").ToString)).ToArray())

            If mstrDates.Trim.Length > 0 Then

                Dim arDates As String() = mstrDates.Trim.Split(CChar(","))

                If arDates.Length > 0 Then

                    Dim count As Integer = 0
                    For i As Integer = 0 To arDates.Length - 1
                        count = i
                        If Not (eZeeDate.convertDate(arDates(i).ToString()).Date >= mdtStartDate.Date AndAlso eZeeDate.convertDate(arDates(i).ToString()).Date <= mdtStartDate.AddDays(Date.DaysInMonth(mdtStartDate.Year, mdtStartDate.Month) - 1).Date) Then
                            ''Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry,you cannot cancel ot requisition for selected employee(s).Reason:Period is already over."), Me)
                            Exit Sub
                        End If
                    Next

                End If

            End If

            ''Language.setLanguage(mstrModuleName)
            popupConfirmYesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Cancel OT Requisition")
            popupConfirmYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Are you sure you want to cancel this ot requisition?")
            popupConfirmYesNo.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Dim objOTRequisition As clsOT_Requisition_Tran
        Try
            objOTRequisition = New clsOT_Requisition_Tran
            SetAtValue(mstrModuleName, objOTRequisition)

            If objOTRequisition.CancelApprovedOTRequisition(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                   , CInt(Session("UserId")), mstrOTRequisitionIds, txtCancelReason.Text.Trim()) = False Then
                DisplayMessage.DisplayMessage(objOTRequisition._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "OT Requisition cancelled successfully."), Me)


                Dim mintLoginTypeID As Integer = 0
                Dim mintLoginEmployeeID As Integer = 0
                Dim mintUserID As Integer = 0
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                End If

                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = gvOTRequisitionList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(gvOTRequisitionList.DataKeys(x.RowIndex)("IsGrp")) = False)


                Dim strEmployeeIDs As String = String.Join(",", gRow.AsEnumerable().Select(Function(x) gvOTRequisitionList.DataKeys(x.RowIndex)("employeeunkid").ToString).Distinct().ToArray)

                For Each strEmpID As String In strEmployeeIDs.Split(CChar(","))
                    Dim intEmployeeID As Integer = CInt(strEmpID)

                    Dim strOTRequisitionIDs As String = String.Join(",", gRow.AsEnumerable().Where(Function(x) CInt(gvOTRequisitionList.DataKeys(x.RowIndex)("employeeunkid")) = intEmployeeID _
                                                                                                     AndAlso CBool(gvOTRequisitionList.DataKeys(x.RowIndex)("IsGrp")) = False).Select(Function(x) CInt(gvOTRequisitionList.DataKeys(x.RowIndex)("otrequisitiontranunkid")).ToString).Distinct().ToArray())


                    If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
                        mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                        If mdtStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
                            mdtStartDate = mdtStartDate.AddDays(-1 * CInt(Session("OTTenureDays")) + 1)
                        Else
                            Dim mdtTempDate As DateTime = mdtStartDate.AddMonths(-1)
                            If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
                                mdtStartDate = mdtTempDate
                            End If
                        End If
                        mdtEndDate = mdtStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
                    Else
                        mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                        mdtEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
                    End If


                    objOTRequisition.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                                                     CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                     CBool(Session("IsIncludeInactiveEmp")), Session("UserAccessModeSetting").ToString(), _
                                                                                     CStr(intEmployeeID), strOTRequisitionIDs, dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, 6, _
                                                                                     mintLoginTypeID, mintLoginEmployeeID, mintUserID, txtCancelReason.Text)
                Next

                Reset()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objOTRequisition = Nothing
        End Try
    End Sub

    'Pinkal (12-Oct-2021)-- End

#End Region

#Region "Gridview Event"

    Protected Sub gvOTRequisitionList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOTRequisitionList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                SetDateFormat()
                If CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("IsGrp")) Then

                    Dim intVisible As Integer = gvOTRequisitionList.Columns.Cast(Of DataControlField).Where(Function(x) x.Visible = True).Count
                    e.Row.Cells(mdicETS("colhotreqemp")).ColumnSpan = intVisible

                    For i = mdicETS("colhotreqemp") + 1 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    ''e.Row.Cells(mdicETS("colhotreqemp")).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Row.Cells(mdicETS("colhotreqemp")).Style.Add("text-align", "left")
                    CType(e.Row.Cells(mdicETS("objdgcolhSelect")).FindControl("chkSelect"), CheckBox).Visible = False
                    ''e.Row.Cells(mdicETS("objdgcolhSelect")).CssClass = "GroupHeaderStyleBorderLeft"

                    e.Row.Cells(mdicETS("colhotreqemp")).BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Row.Cells(mdicETS("objdgcolhSelect")).BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Row.Cells(mdicETS("colhotreqemp")).Font.Bold = True
                Else
                    Dim chkSelect As CheckBox = CType(e.Row.FindControl("chkselect"), CheckBox)
                    chkSelect.Checked = CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex).Values("ischecked"))

                    If e.Row.Cells(2).Text <> "&nbsp;" Then
                        e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text).ToShortDateString()
                        e.Row.Cells(3).Text = CDate(e.Row.Cells(3).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(3).Text), "HH:mm")
                        e.Row.Cells(4).Text = CDate(e.Row.Cells(4).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(4).Text), "HH:mm")
                        e.Row.Cells(6).Text = CDate(e.Row.Cells(6).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(6).Text), "HH:mm")
                        e.Row.Cells(7).Text = CDate(e.Row.Cells(7).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(7).Text), "HH:mm")
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    'Pinkal (10-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion for NMB.

    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

    '        If mdtOTTimesheet.Rows.Count <= 1 Then Exit Sub '1 STANDS HIDDEN BLANK ROW.

    '        For Each item As GridViewRow In gvOTRequisitionList.Rows
    '            If item.RowType = DataControlRowType.DataRow Then
    '                CType(item.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '                mdtOTTimesheet.Rows(item.RowIndex)("ischecked") = chkSelectAll.Checked
    '            End If
    '        Next
    '        mdtOTTimesheet.AcceptChanges()
    '        gvOTRequisitionList.DataSource = mdtOTTimesheet
    '        gvOTRequisitionList.DataBind()

    '        CType(gvOTRequisitionList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = CType(sender, CheckBox).Checked
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If gvOTRequisitionList.Rows.Count <= 0 Then Exit Sub

    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)

    '        Dim dsRow As DataRow() = mdtOTTimesheet.Select("employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")
    '        If dsRow.Length > 0 Then
    '            For Each dR In dsRow
    '                dR.Item("ischecked") = chkSelect.Checked
    '                dR.AcceptChanges()
    '            Next
    '        End If

    '        gvOTRequisitionList.DataSource = mdtOTTimesheet
    '        gvOTRequisitionList.DataBind()

    '        Dim drRow As DataRow() = mdtOTTimesheet.Select("IsGrp=0 AND employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")

    '        Dim dRow As DataRow() = mdtOTTimesheet.Select("IsGrp=0 AND ischecked=0 AND employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")

    '        Dim gRow As DataRow() = mdtOTTimesheet.Select("IsGrp=1 AND employeeunkid=" & CInt(gvOTRequisitionList.DataKeys(item.RowIndex - 1).Values("employeeunkid")) & " AND requestdate='" & CDate(item.Cells(getColumnID_Griview(gvOTRequisitionList, "colhotrequestdate")).Text).Date & "'")

    '        If gRow.Length <= 0 Then Exit Sub

    '        If drRow.Length = dRow.Length Then
    '            gRow(0).Item("ischecked") = False
    '        Else
    '            gRow(0).Item("ischecked") = True
    '        End If
    '        gRow(0).AcceptChanges()


    '        Dim xSelectedCount As Integer = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False And x.Field(Of Boolean)("ischecked") = True).DefaultIfEmpty().Count
    '        Dim xTotalCount As Integer = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isGrp") = False).DefaultIfEmpty().Count
    '        If xSelectedCount = xTotalCount Then
    '            CType(gvOTRequisitionList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Pinkal (10-Jan-2020) -- End

#End Region

#Region "DateControl Events"

#Region "DateControl Events "

    Protected Sub dtpFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged, dtpToDate.TextChanged
        Try

            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            'If Session("AllowApplyOTForEachMonthDay") IsNot Nothing AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) > 0 Then
            'mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, CInt(Session("AllowApplyOTForEachMonthDay")) + 1)
            If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
                mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                If mdtStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
                    'mdtStartDate = mdtStartDate.AddDays(-31)
                    mdtStartDate = mdtStartDate.AddDays(-1 * CInt(Session("OTTenureDays")) + 1)
                Else
                    Dim mdtTempDate As DateTime = mdtStartDate.AddMonths(-1)
                    If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
                        mdtStartDate = mdtTempDate
                    End If
                End If
                'mdtEndDate = mdtStartDate.AddDays(30)
                mdtEndDate = mdtStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
                'Pinkal (02-Jun-2020) -- End
            Else
                mdtStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                mdtEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
            End If


            Dim dsCombo As DataSet = Nothing
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If


            Dim objEmpAssignOT As New clsassignemp_ot
            dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                 , dtpToDate.GetDate.Date, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpcodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEmpAssignOT = Nothing


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (03-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


#End Region

#Region " Language & UI Settings "

    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPeriod.ID, Me.lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFromDate.ID, Me.lblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblToDate.ID, Me.lblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblCancelReason.ID, Me.LblCancelReason.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCancel.ID, Me.btnCancel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(1).FooterText, gvOTRequisitionList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(2).FooterText, gvOTRequisitionList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(3).FooterText, gvOTRequisitionList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(4).FooterText, gvOTRequisitionList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(5).FooterText, gvOTRequisitionList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(6).FooterText, gvOTRequisitionList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(7).FooterText, gvOTRequisitionList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvOTRequisitionList.Columns(8).FooterText, gvOTRequisitionList.Columns(8).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            'Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblCancelReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblCancelReason.ID, Me.LblCancelReason.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnCancel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCancel.ID, Me.btnCancel.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.gvOTRequisitionList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(1).FooterText, gvOTRequisitionList.Columns(1).HeaderText)
            Me.gvOTRequisitionList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(2).FooterText, gvOTRequisitionList.Columns(2).HeaderText)
            Me.gvOTRequisitionList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(3).FooterText, gvOTRequisitionList.Columns(3).HeaderText)
            Me.gvOTRequisitionList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(4).FooterText, gvOTRequisitionList.Columns(4).HeaderText)
            Me.gvOTRequisitionList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(5).FooterText, gvOTRequisitionList.Columns(5).HeaderText)
            Me.gvOTRequisitionList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(6).FooterText, gvOTRequisitionList.Columns(6).HeaderText)
            Me.gvOTRequisitionList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(7).FooterText, gvOTRequisitionList.Columns(7).HeaderText)
            Me.gvOTRequisitionList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvOTRequisitionList.Columns(8).FooterText, gvOTRequisitionList.Columns(8).HeaderText)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Period is compulsory information. Please Select Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please tick atleast one record for further process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "OT Requisition cancelled successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Cancel Reason cannot be blank.Cancel Reason is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Cancel OT Requisition")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Are you sure you want to cancel this ot requisition?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry,you cannot cancel ot requisition for selected employee(s).Reason:Period is already over.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry,you cannot cancel ot requisition for selected employee(s).Reason:Payroll Process already done for selected employee(s) for last date of current open period.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
