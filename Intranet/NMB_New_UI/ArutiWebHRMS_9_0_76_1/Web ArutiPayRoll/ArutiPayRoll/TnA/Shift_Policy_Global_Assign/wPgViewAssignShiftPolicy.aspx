﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Home1.master" CodeFile="wPgViewAssignShiftPolicy.aspx.vb"
    Inherits="TnA_Shift_Policy_Global_Assign_wPgViewAssignShiftPolicy" Title="View Assigned Shift/Policy List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="Allocation" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


 <script type="text/javascript">


     $("body").on("click", "[id*=chkAllSelect]", function() {
           var chkHeader = $(this);
           debugger;
           var grid = $(this).closest("table");
           $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
       });

       $("body").on("click", "[id*=chkSelect]", function() {
           var grid = $(this).closest("table");
           var chkHeader = $("[id*=chkAllSelect]", grid);
           debugger;
           if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                   chkHeader.prop("checked", true);
               }
               else {
                   chkHeader.prop("checked", false);
               }
       });
       
    </script>


    <asp:Panel ID="pnlMain" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Assigned Shift/Policy List"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocation">
                                            <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblViewAssign" runat="server" Text="View Assign" CssClass="form-label"></asp:Label>
                                        <asp:RadioButtonList ID="RdShiftPolicy" runat="server" RepeatDirection="Horizontal"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="1" Text="Shift" Selected="True" />
                                            <asp:ListItem Value="2" Text="Policy" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblShiftType" runat="server" Text="Shift Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboShiftType" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblShift" runat="server" Text="Shift" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboShift" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpDate1" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPolicy" runat="server" Text="Policy" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPolicy" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTo" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpDate2" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-default" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 300px">
                                            <asp:UpdatePanel ID="UpdateViewShiftPolicyList" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                        CssClass="table table-hover table-bordered" DataKeyNames = "shifttranunkid,emppolicytranunkid,employeeunkid,shiftunkid,policyunkid">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkAllSelect" runat="server"  CssClass="filled-in" Text=" " /> <%-- AutoPostBack="true"  OnCheckedChanged="chkSelectAll_CheckedChanged" --%>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />    <%--AutoPostBack="true"   OnCheckedChanged="chkSelect_CheckedChanged" --%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="ecode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhCode" />
                                                            <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="True" FooterText="dgcolhEmployee" />
                                                            <asp:BoundField DataField="effectivedate" HeaderText="Effective Date" ReadOnly="True"
                                                                FooterText="dgcolhEffectiveDate" />
                                                            <asp:BoundField DataField="shiftname" HeaderText="Shift" ReadOnly="True" FooterText="dgcolhShift" />
                                                            <asp:BoundField DataField="policyname" HeaderText="Policy" ReadOnly="True" />
                                                            <asp:BoundField DataField="shifttranunkid" HeaderText="Shifttranunkid" ReadOnly="True"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="emppolicytranunkid" HeaderText="Emppolicytranunkid" ReadOnly="True"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <uc9:Confirmation ID="DeleteConfirmation" runat="server" Title="Confirmation" Message="Are you sure you want to delete checked transaction(s)?" />
                <uc8:DeleteReason ID="DeleteReason" runat="server" Title="Delete Reason" />
                <uc6:Allocation ID="popupAdvanceFilter" runat="server" Title="Advance Filter" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
