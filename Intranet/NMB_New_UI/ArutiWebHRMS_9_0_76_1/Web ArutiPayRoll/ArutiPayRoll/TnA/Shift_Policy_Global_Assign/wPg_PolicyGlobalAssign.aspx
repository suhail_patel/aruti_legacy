﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_PolicyGlobalAssign.aspx.vb"
    Inherits="TnA_Shift_Policy_Global_Assign_wPg_PolicyGlobalAssign" Title="Global Policy Assign" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   <script type="text/javascript">


       $("body").on("click", "[id*=chkEmpAllSelect]", function() {
           var chkHeader = $(this);
           debugger;
           var grid = $(this).closest("table");
           $("[id*=chkEmpSelect]").prop("checked", $(chkHeader).prop("checked"));
       });

       $("body").on("click", "[id*=chkEmpSelect]", function() {
           var grid = $(this).closest("table");
           var chkHeader = $("[id*=chkEmpAllSelect]", grid);
           debugger;
               if ($("[id*=chkEmpSelect]", grid).length == $("[id*=chkEmpSelect]:checked", grid).length) {
                   chkHeader.prop("checked", true);
               }
               else {
                   chkHeader.prop("checked", false);
               }
       });

       $.expr[":"].containsNoCase = function(el, i, m) {
           var search = m[3];
           if (!search) return false;
           return eval("/" + search + "/i").test($(el).text());
       };

       function Searching() {
           if ($('#txtSearch').val().length > 0) {
               $('#<%= dgvEmp.ClientID %> tbody tr').hide();
               $('#<%= dgvEmp.ClientID %> tbody tr:first').show();
               $('#<%= dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
           }
           else if ($('#txtSearch').val().length == 0) {
               resetFromSearchValue();
           }
           if ($('#<%= dgvEmp.ClientID %> tr:visible').length == 1) {
               $('.norecords').remove();
           }

           if (event.keyCode == 27) {
               resetFromSearchValue();
           }
       }
       function resetFromSearchValue() {
           $('#txtSearch').val('');
           $('#<%= dgvEmp.ClientID %> tr').show();
           $('.norecords').remove();
           $('#txtSearch').focus();
       }


   </script>  


    <asp:Panel ID="MainPan" runat="server" >
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Shift/Policy Assignment"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblPolicyFilterHeader" runat="server" Text="Policy Filter"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblFilterPolicy" runat="server" Text="Policy" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboFilterPolicy" runat="server"  AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Policy Assignment"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="LblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-t-35">
                                                        <asp:CheckBox ID="chkFirstPolicy" runat="server" Text="Assign First Policy" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPolicy" runat="server" Text="Policy" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPolicy" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                              <%--  <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>--%>
                                                                <input type="text" id="txtSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                        maxlength="50" class="form-control" onkeyup="Searching();" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 400px">
                                                            <asp:UpdatePanel ID="UpdateEmployeeList" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="false" AllowPaging="false" DataKeyNames = "employeeunkid,Appointed Date"
                                                                        CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkEmpAllSelect" runat="server"  CssClass="filled-in" Text=" "  />  <%-- AutoPostBack="true"  OnCheckedChanged="chkEmpSelectAll_CheckedChanged"--%>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkEmpSelect" runat="server" CssClass="filled-in"  Text=" " />  <%--AutoPostBack="true"  OnCheckedChanged="chkEmpSelect_CheckedChanged" --%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhEcode" />
                                                                            <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="True" FooterText="dgcolhEName" />
                                                                            <asp:BoundField DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                                                Visible="false" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
