﻿<%@ Page Title="Daily Attendance" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgDailyAttendanceOperation.aspx.vb" Inherits="TnA_Daily_Attendance_wPgDailyAttendanceOperation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnlMain" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Daily Attendance"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblShift" runat="server" Text="Shift" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboShift" runat="server" AutoPostBack="False" Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnCheckIn" runat="server" CssClass="btn btn-primary" Text="Log In" />
                                <asp:Button ID="btnCheckOut" runat="server" CssClass="btn btn-primary" Text="Log Out" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 150px">
                                            <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundField DataField="logindate" HeaderText="Login Date" FooterText="dgcolhLoginDate" />
                                                    <asp:BoundField DataField="checkintime" HeaderText="LogIn Time" FooterText="dgcolhLogInTime"
                                                        DataFormatString="{0:HH:mm}" />
                                                    <asp:BoundField DataField="checkouttime" HeaderText="LogOut Time" FooterText="dgcolhLogOutTime"
                                                        DataFormatString="{0:HH:mm}" />
                                                    <asp:BoundField DataField="workedDuration" HeaderText="Worked Hour" FooterText="dgcolhworkhour" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer" style="height:40px">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 5col-xs-5">
                                        <asp:Label ID="lblTotalWorkedHour" runat="server" Text="Total Worked Hour :" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <asp:Label ID="txtTotalWorkedHour" runat="server" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
