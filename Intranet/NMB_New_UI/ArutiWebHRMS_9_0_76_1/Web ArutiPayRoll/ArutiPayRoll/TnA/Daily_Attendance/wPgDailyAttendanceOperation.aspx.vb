﻿
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing
#End Region

Partial Class TnA_Daily_Attendance_wPgDailyAttendanceOperation
    Inherits Basepage

#Region " Private Variables "

    Private clsuser As New User
    Private cmsg As New CommonCodes
    Private dtEmployee As DataTable
    Private mintEmployeeUnkId As Integer = -1
    Private mintShiftID As Integer = -1
    Private ReadOnly mstrModuleName As String = "frmDailyAttendanceOperation"
    Dim objLogin As New clslogin_Tran

    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.
    Private DisplayMessage As New CommonCodes
    'Pinkal (09-Aug-2021) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                cmsg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                SetLanguage()
                SetMessages()
                'Pinkal (09-Aug-2021) -- End

                Call FillCombo()
                Call Fill_Employee_List()
                Call CheckButtonVisibility()
            Else
                mintEmployeeUnkId = Me.ViewState("mintEmployeeUnkId")
            End If

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch exArg As ArgumentException
            cmsg.DisplayError(exArg, Me)
            'Pinkal (13-Aug-2020) -- End
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintEmployeeUnkId", mintEmployeeUnkId)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'cmsg.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            cmsg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Dim dsList As New DataSet
        Try

            dsList = objShift.getListForCombo("Shift", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Shift")
                .DataBind()
                .SelectedValue = "0"
            End With

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then


                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Employee", True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
                'cboEmployee_SelectedIndexChanged(Nothing, Nothing)
                cboEmployee_SelectedIndexChanged(cboEmployee, New System.EventArgs)
            End If

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch exArg As ArgumentException
            cmsg.DisplayError(exArg, Me)
            'Pinkal (13-Aug-2020) -- End
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing : objShift = Nothing : objPolicy = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_Employee_List()

        Dim dsEmployee As New DataSet
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
            objLogin._Logindate = Basepage.GetCurrentDateTime().Date
            'Pinkal (25-Jan-2022) -- End

            dsEmployee = objLogin.GetList("List", True)

            If Not dsEmployee.Tables("List").Columns.Contains("workedDuration") Then
                dsEmployee.Tables("List").Columns.Add("workedDuration", GetType(String))
            End If


            Dim TotalSec As Integer = 0
            For Each dtRow As DataRow In dsEmployee.Tables("List").Rows
                dtRow("workedDuration") = CalculateTime(True, CInt(dtRow("workhour"))).ToString("#00.00").Replace(".", ":")
                TotalSec += CInt(dtRow("workhour"))
            Next


            txtTotalWorkedHour.Text = CalculateTime(True, TotalSec).ToString("#00.00").Replace(".", ":")


            dsEmployee.Tables("List").AcceptChanges()
            dtEmployee = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()


            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'If Me.ViewState("dtEmployee") Is Nothing Then
            'Me.ViewState.Add("dtEmployee", dtEmployee)
            'Else
            'Me.ViewState("dtEmployee") = dtEmployee
            'End If
            'Pinkal (13-Aug-2020) -- End


            dgvEmp.AutoGenerateColumns = False
            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()


        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'dgvEmp.DataSource = dtEmployee
            'dgvEmp.DataBind()
            'Pinkal (13-Aug-2020) -- End
            dsEmployee.Dispose()
        End Try
    End Sub

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim objEmpShift As New clsEmployee_Shift_Tran
            mintEmployeeUnkId = cboEmployee.SelectedValue

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'mintShiftID = objEmpShift.GetEmployee_Current_ShiftId(ConfigParameter._Object._CurrentDateAndTime.Date, mintEmployeeUnkId)
            mintShiftID = objEmpShift.GetEmployee_Current_ShiftId(Basepage.GetCurrentDateTime().Date, mintEmployeeUnkId)
            'Pinkal (25-Jan-2022) -- End
            cboShift.SelectedValue = mintShiftID
            objEmpShift = Nothing

        Catch exArg As ArgumentException
            cmsg.DisplayError(exArg, Me)
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    Private Sub CheckButtonVisibility()
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
            ' If objLogin.GetLoginType(mintEmployeeUnkId, ConfigParameter._Object._CurrentDateAndTime.Date) = 1 Then
            objLogin._Logindate = Basepage.GetCurrentDateTime.Date
            If objLogin.GetLoginType(mintEmployeeUnkId, objLogin._Logindate.Date) = 1 Then
                'Pinkal (25-Jan-2022) -- End
                btnCheckIn.Visible = False
                btnCheckOut.Visible = True
            Else
                btnCheckIn.Visible = True
                btnCheckOut.Visible = False
            End If
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (13-Aug-2020) -- End

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLogin._Userunkid = CInt(Session("UserId"))
            End If
            objLogin._WebClientIP = CStr(Session("IP_ADD"))

            objLogin._WebHostName = CStr(Session("HOST_NAME"))
            objLogin._WebFormName = mstrModuleName

        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try

    End Sub
#End Region

#Region " Button's Events "

    Protected Sub btnCheckIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckIn.Click
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
            'objLogin._checkintime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm"))
            Dim dtDate As DateTime = Basepage.GetCurrentDateTime
            objLogin._Logindate = dtDate.Date
            objLogin._checkintime = CDate(dtDate.Date & " " & dtDate.ToString("HH:mm"))
            'Pinkal (25-Jan-2022) -- End



            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If cboShift.SelectedValue IsNot Nothing AndAlso CInt(cboShift.SelectedValue) > 0 Then
                objLogin._Shiftunkid = CInt(cboShift.SelectedValue)
            Else
                objLogin._Shiftunkid = 0
            End If
            'Pinkal (27-Aug-2020) -- End

            objLogin._Original_InTime = objLogin._checkintime
            objLogin._Checkouttime = Nothing
            objLogin._Original_OutTime = Nothing
            objLogin._SourceType = enInOutSource.Manual
            objLogin._InOutType = 0
            objLogin._Workhour = 0
            objLogin._Holdunkid = 0
            objLogin._Voiddatetime = Nothing
            objLogin._Isvoid = False
            objLogin._Voidreason = ""
            objLogin._LoginEmployeeUnkid = mintEmployeeUnkId
            SetAtValue()

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

            'objLogin.Insert(Session("Database_Name") _
            '                                                      , CInt(Session("UserId")) _
            '                                                      , Session("Fin_year") _
            '                                                      , Session("CompanyUnkId") _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , Session("UserAccessModeSetting") _
            '                                                      , True, Session("IsIncludeInactiveEmp") _
            '                                                      , Session("PolicyManagementTNA") _
            '                                                      , Session("DonotAttendanceinSeconds") _
            '                                                      , Session("FirstCheckInLastCheckOut") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , Session("IsDayOffConsiderOnWeekend") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , False, -1, Nothing, "", "")

            objLogin.Insert(Session("Database_Name") _
                                                                  , CInt(Session("UserId")) _
                                                                  , Session("Fin_year") _
                                                                  , Session("CompanyUnkId") _
                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                  , Session("UserAccessModeSetting") _
                                                                  , True, Session("IsIncludeInactiveEmp") _
                                                                  , Session("PolicyManagementTNA") _
                                                                  , Session("DonotAttendanceinSeconds") _
                                                                  , Session("FirstCheckInLastCheckOut") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , Session("IsDayOffConsiderOnWeekend") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , False, -1, Nothing, "", "")

            'Pinkal (20-Aug-2020) -- End

            CheckButtonVisibility()
            Fill_Employee_List()

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "You Have Successfully Logged in."), Me)
            'Pinkal (09-Aug-2021) -- End

        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckOut.Click
        Try
            Dim dsList As DataSet
            objLogin._Employeeunkid = mintEmployeeUnkId

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
            Dim dtDate As DateTime = Basepage.GetCurrentDateTime
            objLogin._Logindate = dtDate.Date
            'Pinkal (25-Jan-2022) -- End
            dsList = objLogin.GetList("List", True)
            Dim drCheckIn() As DataRow = dsList.Tables(0).Select("checkouttime IS NULL AND inouttype = 0 ")
            For Each dr In drCheckIn
                objLogin._Loginunkid = CInt(dr("loginunkid").ToString)
                Exit For
            Next


            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'objLogin._Checkouttime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm"))
            'objLogin._Original_OutTime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm"))
            objLogin._Checkouttime = CDate(dtDate.Date & " " & dtDate.ToString("HH:mm"))
            objLogin._Original_OutTime = CDate(dtDate.Date & " " & dtDate.ToString("HH:mm"))
            'Pinkal (25-Jan-2022) -- End


            objLogin._SourceType = enInOutSource.Manual
            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
            objLogin._Workhour = CInt(wkmins)
            objLogin._InOutType = 1
            objLogin._Holdunkid = 0
            objLogin._Voiddatetime = Nothing
            objLogin._Isvoid = False
            objLogin._Voidreason = ""
            objLogin._LoginEmployeeUnkid = mintEmployeeUnkId

            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If cboShift.SelectedValue IsNot Nothing AndAlso CInt(cboShift.SelectedValue) > 0 Then
                objLogin._Shiftunkid = CInt(cboShift.SelectedValue)
            Else
                objLogin._Shiftunkid = 0
            End If
            'Pinkal (27-Aug-2020) -- End

            SetAtValue()


            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

            'objLogin.Update(Session("Database_Name") _
            '                                                      , CInt(Session("UserId")) _
            '                                                      , Session("Fin_year") _
            '                                                      , Session("CompanyUnkId") _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , Session("UserAccessModeSetting") _
            '                                                      , True, Session("IsIncludeInactiveEmp") _
            '                                                      , Session("PolicyManagementTNA") _
            '                                                      , Session("DonotAttendanceinSeconds") _
            '                                                      , Session("FirstCheckInLastCheckOut") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , Session("IsDayOffConsiderOnWeekend") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , False, -1, Nothing, "", "")

            objLogin.Update(Session("Database_Name") _
                                                                  , CInt(Session("UserId")) _
                                                                  , Session("Fin_year") _
                                                                  , Session("CompanyUnkId") _
                                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                  , Session("UserAccessModeSetting") _
                                                                  , True, Session("IsIncludeInactiveEmp") _
                                                                  , Session("PolicyManagementTNA") _
                                                                  , Session("DonotAttendanceinSeconds") _
                                                                  , Session("FirstCheckInLastCheckOut") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , Session("IsDayOffConsiderOnWeekend") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , False, -1, Nothing, "", "")

            'Pinkal (20-Aug-2020) -- End

            CheckButtonVisibility()
            Fill_Employee_List()

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "You Have Successfully Logged out."), Me)
            'Pinkal (09-Aug-2021) -- End

        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Gajanan [17-Sep-2020] -- Start
    'New UI Change
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [17-Sep-2020] -- End

#End Region

#Region " GridView's Events "
    Protected Sub dgvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvEmp.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(0).Text <> "&nbsp;" Then
                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    'SetDateFormat()
                    'Pinkal (13-Aug-2020) -- End
                    e.Row.Cells(0).Text = CDate(e.Row.Cells(0).Text).ToShortDateString()
                End If
            End If
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblTotalWorkedHour.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTotalWorkedHour.ID, Me.lblTotalWorkedHour.Text)
            Me.txtTotalWorkedHour.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtTotalWorkedHour.ID, Me.txtTotalWorkedHour.Text)
            Me.lblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblShift.ID, Me.lblShift.Text)

            Me.dgvEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmp.Columns(0).FooterText, Me.dgvEmp.Columns(0).HeaderText)
            Me.dgvEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
            Me.dgvEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)
            Me.dgvEmp.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmp.Columns(3).FooterText, Me.dgvEmp.Columns(3).HeaderText)


            Me.btnCheckIn.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCheckIn.ID, Me.btnCheckIn.Text).Replace("&", "")
            Me.btnCheckOut.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCheckOut.ID, Me.btnCheckOut.Text).Replace("&", "")
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Gajanan [17-Sep-2020] -- End
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (13-Aug-2020) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(Session("mdbname").ToString(), mstrModuleName, 1, "You Have Successfully Logged in.")
            Basepage.SetWebMessage(Session("mdbname").ToString(), mstrModuleName, 2, "You Have Successfully Logged out.")
        Catch Ex As Exception
            cmsg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
