﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

Partial Class Loan_Savings_wPg_Employee_Saving_List
    Inherits Basepage

#Region "Private Variables"

    Private msg As New CommonCodes

    Private objEmployeeSavingData As clsSaving_Tran
    Private objEmployeeData As clsEmployee_Master
    Private objPeriodData As clscommom_period_Tran
    Private objSavingData As New clsSavingScheme
    Private objMasterData As New clsMasterData
    Private mintSeletedValue As Integer = 0


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEmployeeSavingsList"
    Private ReadOnly mstrModuleName1 As String = "frmSavingStatus_AddEdit"
    'Anjan [04 June 2014] -- End

    'SHANI (09 Mar 2015) -- Start
    'Enhancement - Provide Multi Currency on Saving scheme contribution.
    Private mdicCurrency As New Dictionary(Of Integer, String)
    Private mstrBaseCurrencySign As String = ""
    'SHANI (09 Mar 2015) -- End

    'Nilay (15-Dec-2015) -- Start
    Dim strURL As String = ""
    'Nilay (15-Dec-2015) -- End

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (07-Feb-2016) -- CBool(Session("IsArutiDemo"))

         

            objEmployeeSavingData = New clsSaving_Tran
            If Not IsPostBack Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End

                FillCombo()
                'Nilay (02 Feb 2017) -- Start
                Call FillEmployee()
                'Nilay (02 Feb 2017) -- End
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                Call SetVisibility()
                'Varsha Rana (17-Oct-2017) -- End
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Saving_EmpUnkID") IsNot Nothing Then
                    cboEmployee.SelectedValue = CStr(Session("Saving_EmpUnkID"))
                    Session.Remove("Saving_EmpUnkID")
                    Call btnSearch_Click(btnSearch, Nothing)
                End If
                'SHANI [09 Mar 2015]--END 

                'SHANI (09 Mar 2015) -- Start
                'Enhancement - Provide Multi Currency on Saving scheme contribution.
            Else
                mdicCurrency = CType(ViewState("mdicCurrency"), Global.System.Collections.Generic.Dictionary(Of Integer, String))
                mstrBaseCurrencySign = CStr(ViewState("mstrBaseCurrencySign"))
                'SHANI (09 Mar 2015) -- End
            End If

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
            'Nilay (01-Feb-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'Nilay (01-Feb-2015) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleDeleteButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleExitButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Sohail (09 Mar 2015) -- Start
    'Enhancement - Provide Multi Currency on Saving scheme contribution.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            ViewState("mdicCurrency") = mdicCurrency
            ViewState("mstrBaseCurrencySign") = mstrBaseCurrencySign
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
    'Sohail (09 Mar 2015) -- End
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dsEmployeeScheme As New DataSet
        Dim dtTable As DataTable
        Dim objExchangeRate As New clsExchangeRate

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        'Shani(24-Aug-2015) -- End

        Try

            If CBool(Session("AllowToViewEmployeeSavingsList")) = True Then

                Dim mdecConribution As Decimal = 0

                Decimal.TryParse(txtContribution.Text, mdecConribution)

                objEmployeeSavingData = New clsSaving_Tran
                objExchangeRate = New clsExchangeRate

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                objPeriodData = New clscommom_period_Tran
                'Shani(18-Dec-2015) -- End

                'SHANI (09 Mar 2015) -- Start
                'Enhancement - Provide Multi Currency on Saving scheme contribution.
                Dim ds As DataSet = objExchangeRate.getComboList("List", False)
                mdicCurrency = (From p In ds.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("countryunkid")), Key .NAME = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
                'SHANI (09 Mar 2015) -- End

                objExchangeRate._ExchangeRateunkid = 1
                mstrBaseCurrencySign = objExchangeRate._Currency_Sign 'SHANI (09 Mar 2015)

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.

                'If Session("IsIncludeInactiveEmp") = False Then
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List", , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("AccessLevelFilterString"))
                'Else
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List")
                'End If

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    strSearching &= "AND employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
                'End If

                Dim mstrEmployeeIDs As String = ""

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mstrEmployeeIDs = cboEmployee.SelectedValue.ToString()
                End If

                'Sohail (25 Aug 2021) -- Start
                'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
                Dim strFilter As String = ""
                If CInt(cboPayPeriod.SelectedValue) > 0 Then
                    strFilter &= " AND svsaving_tran.payperiodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
                End If

                If CInt(cboSavingsScheme.SelectedValue) > 0 Then
                    strFilter &= " AND svsaving_tran.savingschemeunkid = " & CInt(cboSavingsScheme.SelectedValue) & " "
                End If

                If strFilter.Length > 0 Then
                    strFilter = strFilter.Substring(4)
                End If


                Dim strOuterFilter As String = ""
                If mdecConribution <> 0 Then
                    strOuterFilter &= "AND b.contribution = " & mdecConribution & " "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    strOuterFilter &= " AND a.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                End If
                'Sohail (25 Aug 2021) -- End

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List", , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("AccessLevelFilterString"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), mstrEmployeeIDs)
                'Else
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List", , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), mstrEmployeeIDs)
                'End If

                If CInt(cboPayPeriod.SelectedValue) > 0 Then
                    objPeriodData._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
                    dtPeriodStart = objPeriodData._Start_Date
                    dtPeriodEnd = objPeriodData._End_Date
                Else

                    'Shani(18-Dec-2015) -- Start
                    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                    'Dim intPeriodUnkid As Integer = 0
                    'intPeriodUnkid = objMasterData.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")))

                    'If intPeriodUnkid > 0 Then
                    '    objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodUnkid
                    '    dtPeriodStart = objPeriodData._Start_Date
                    '    dtPeriodEnd = objPeriodData._End_Date
                    'Else
                    '    dtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    '    dtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    'End If
                    dtPeriodStart = CDate(Session("fin_startdate"))
                    dtPeriodEnd = CDate(Session("fin_enddate"))
                    'Shani(18-Dec-2015) -- End
                End If

                dsEmployeeScheme = objEmployeeSavingData.GetList(CStr(Session("Database_Name")), _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 dtPeriodStart, _
                                                                 dtPeriodEnd, _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                                 "List", strFilter, , , CInt(cboEmployee.SelectedValue), , , , , , strOuterFilter)
                'Sohail (25 Aug 2021) - [strFilter, strOuterFilter]
                'Shani (18-Dec-2015) -- [CInt(cboEmployee.SelectedValue)]
                'Shani(24-Aug-2015) -- End

                'Pinkal (12 Jan 2015) -- End

                'Sohail (25 Aug 2021) -- Start
                'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
                'If mdecConribution <> 0 Then
                '    strSearching &= "AND contribution =" & mdecConribution & " "
                'End If

                'If CInt(cboStatus.SelectedValue) > 0 Then
                '    strSearching &= "AND savingstatus =" & CInt(cboStatus.SelectedValue) & " "
                'End If

                'If CInt(cboPayPeriod.SelectedValue) > 0 Then
                '    strSearching &= "AND payperiodunkid =" & CInt(cboPayPeriod.SelectedValue) & " "
                'End If


                'If CInt(cboSavingsScheme.SelectedValue) > 0 Then
                '    strSearching &= "AND savingschemeunkid =" & CInt(cboSavingsScheme.SelectedValue) & " "
                'End If

                'If strSearching.Length > 0 Then
                '    strSearching = strSearching.Substring(3)
                '    dtTable = New DataView(dsEmployeeScheme.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = dsEmployeeScheme.Tables("List")
                'End If
                dtTable = dsEmployeeScheme.Tables("List")
                'Sohail (25 Aug 2021) -- End

                dgView.DataSource = dtTable
                dgView.DataKeyField = "SavingtranunkId"
                dgView.DataBind()

            End If

        Catch ex As Exception

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                msg.DisplayError(ex, Me)
            End If
            'Pinkal (12 Jan 2015) -- End
        Finally
            dsEmployeeScheme.Dispose()
            objExchangeRate = Nothing
        End Try
    End Sub

    'Nilay (02 Feb 2017) -- Start
    'Issue : first open payroll period was supposed to be passed and financial year first period was passed so it was not giving active employee in search combo.
    Private Sub FillEmployee(Optional ByVal intPeriodunkid As Integer = 0)
        Dim dsList As DataSet
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        Dim mintPeriodid As Integer = intPeriodunkid
        Dim objPeriodData As New clscommom_period_Tran
        Dim objMasterData As New clsMasterData
        If objEmployeeData Is Nothing Then objEmployeeData = New clsEmployee_Master 'Sohail (25 Aug 2021)
        Try
            If intPeriodunkid > 0 Then
                objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodid
                dtPeriodStart = objPeriodData._Start_Date
                dtPeriodEnd = objPeriodData._End_Date
            Else
                mintPeriodid = objMasterData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.OPEN)

                If mintPeriodid > 0 Then
                    objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodid
                    dtPeriodStart = objPeriodData._Start_Date
                    dtPeriodEnd = objPeriodData._End_Date
                Else
                    dtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                    dtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                End If
            End If

            dsList = objEmployeeData.GetEmployeeList(CStr(Session("Database_Name")), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     dtPeriodStart, _
                                                     dtPeriodEnd, _
                                                     CStr(Session("UserAccessModeSetting")), True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), "List", True)

            cboEmployee.DataValueField = "employeeunkid"
            cboEmployee.DataTextField = "EmpCodeName"
            cboEmployee.DataSource = dsList.Tables("List")
            cboEmployee.DataBind()
            cboEmployee.SelectedValue = "0"

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("FillEmployee:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (02 Feb 2017) -- End

    Private Sub FillCombo()
        Dim dsList As DataSet
        objEmployeeData = New clsEmployee_Master
        objMasterData = New clsMasterData
        objPeriodData = New clscommom_period_Tran
        objSavingData = New clsSavingScheme
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Session("IsIncludeInactiveEmp") = False Then
            '    dsList = objEmployeeData.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'Else
            '    dsList = objEmployeeData.GetEmployeeList("List", True)
            'End If
            'Nilay (28 Jan 2017) -- Start
            'Issue : first open payroll period was supposed to be passed and financial year first period was passed so it was not giving active employee in search combo.
            'dsList = objEmployeeData.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                        CInt(Session("UserId")), _
            '                                        CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                        CStr(Session("UserAccessModeSetting")), True, _
            '                                        CBool(Session("IsIncludeInactiveEmp")), "List", True)
            ''Shani(24-Aug-2015) -- End
            'cboEmployee.DataValueField = "employeeunkid"
            ''Nilay (09-Aug-2016) -- Start
            ''ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            ''cboEmployee.DataTextField = "employeename"
            'cboEmployee.DataTextField = "EmpCodeName"
            ''Nilay (09-Aug-2016) -- End

            'cboEmployee.DataSource = dsList.Tables("List")
            'cboEmployee.DataBind()
            'cboEmployee.SelectedValue = "0"
            'Nilay (28 Jan 2017) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, 1)
            dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, 1)
            'Shani(24-Aug-2015) -- End

            cboPayPeriod.DataValueField = "periodunkid"
            cboPayPeriod.DataTextField = "name"
            cboPayPeriod.DataSource = dsList.Tables("List")
            cboPayPeriod.DataBind()
            cboPayPeriod.SelectedValue = "0"

            dsList = objMasterData.GetLoan_Saving_Status("List", True)
            Dim dtTable As DataTable = New DataView(dsList.Tables("List"), "Id <> 3", "", DataViewRowState.CurrentRows).ToTable
            cboStatus.DataValueField = "Id"
            cboStatus.DataTextField = "name"
            cboStatus.DataSource = dtTable
            cboStatus.DataBind()
            cboStatus.SelectedValue = "0"
            Me.ViewState.Add("Status", dtTable)


            dtTable = New DataView(dtTable, "Id NOT IN (3,4)", "", DataViewRowState.CurrentRows).ToTable
            cboStatusChange.DataValueField = "Id"
            cboStatusChange.DataTextField = "name"
            cboStatusChange.DataSource = dtTable
            cboStatusChange.DataBind()
            cboStatusChange.SelectedValue = "0"

            dsList = objSavingData.getComboList(True, "List")
            cboSavingsScheme.DataValueField = "savingschemeunkid"
            cboSavingsScheme.DataTextField = "name"
            cboSavingsScheme.DataSource = dsList.Tables("List")
            cboSavingsScheme.DataBind()
            cboSavingsScheme.SelectedValue = "0"

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            Dim objPeriod As New clscommom_period_Tran

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True, 1)
            'Shani(24-Aug-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objPeriod = Nothing
            If Me.ViewState("Period") Is Nothing Then
                Me.ViewState.Add("Period", dsList.Tables(0))
            Else
                Me.ViewState("Period") = dsList.Tables(0)
            End If
            'Pinkal (12 Jan 2015) -- End


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = CBool(Session("AddEmployeeSavings"))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Fill_Info(ByVal intIndex As Integer)
        Try
            Dim objEmployee As New clsEmployee_Master

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            'objEmployee._Employeeunkid = CInt(dgView.Items(intIndex).Cells(13).Text)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(dgView.Items(intIndex).Cells(14).Text)

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(dgView.Items(intIndex).Cells(14).Text)
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(dgView.Items(intIndex).Cells(15).Text)
            'Shani(18-Dec-2015) -- End

            'Shani(24-Aug-2015) -- End


            'Pinkal (12 Jan 2015) -- End

            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " "
            txtSavingScheme.Text = dgView.Items(intIndex).Cells(5).Text

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            'cboStatusChange.SelectedValue = CInt(dgView.Items(intIndex).Cells(12).Text)
            'Me.ViewState.Add("StatusId", CInt(dgView.Items(intIndex).Cells(12).Text))

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            'cboStatusChange.SelectedValue = CInt(dgView.Items(intIndex).Cells(13).Text)
            'Me.ViewState.Add("StatusId", CInt(dgView.Items(intIndex).Cells(13).Text))
            cboStatusChange.SelectedValue = dgView.Items(intIndex).Cells(14).Text
            Me.ViewState.Add("StatusId", CInt(dgView.Items(intIndex).Cells(14).Text))
            'Shani(18-Dec-2015) -- End


            FillHistory()

            'Pinkal (12 Jan 2015) -- End


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetSaving_Status()
        Try
            txtSavingScheme.Text = ""
            txtEmployee.Text = ""
            cboStatusChange.SelectedValue = "0"
            txtStatusRemark.Text = ""
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

    Private Sub FillHistory()
        Try
            Dim objStatusTran As New clsSaving_Status_Tran
            Dim dsList As DataSet = objStatusTran.GetPeriodWiseSavingStatusHistory("List", True, "savingtranunkid = " & CInt(Me.ViewState("SavingtranunkId")))
            dgvHistory.DataSource = dsList.Tables(0)
            dgvHistory.DataBind()
            If Me.ViewState("SavingStatusHistory") Is Nothing Then
                Me.ViewState.Add("SavingStatusHistory", dsList.Tables(0))
            Else
                Me.ViewState("SavingStatusHistory") = dsList.Tables(0)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillHistory", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (12 Jan 2015) -- End


#End Region

#Region "Button's Event"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Session("Saving_EmpUnkID") = cboEmployee.SelectedValue
            'SHANI [09 Mar 2015]--END 
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/wPg_Add_Edit_Employee_Saving.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboPayPeriod.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            cboSavingsScheme.SelectedIndex = 0
            txtContribution.Text = "0"
            dgView.DataSource = Nothing
            dgView.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Nilay (01-Feb-2015) -- End
        Try
            If CInt(Me.ViewState("SavingtranunkId")) > 0 Then
                objEmployeeSavingData._Savingtranunkid = CInt(Me.ViewState("SavingtranunkId"))
                objEmployeeSavingData._Voiduserunkid = CInt(Session("UserId"))
                objEmployeeSavingData._Userunkid = CInt(Session("UserId"))
                objEmployeeSavingData._VoidReason = popup_DeleteReason.Reason 'Nilay (01-Feb-2015) -- popupDelete.Reason
                objEmployeeSavingData._Voiddatetime = DateAndTime.Now.Date

                Blank_ModuleName()
                objEmployeeSavingData._WebFormName = "frmEmployeeSavings_AddEdit"
                StrModuleName2 = "mnuLoan_Advance_Savings"
                objEmployeeSavingData._WebIP = CStr(Session("IP_ADD"))
                objEmployeeSavingData._WebHost = CStr(Session("HOST_NAME"))


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployeeSavingData.Delete(CInt(Me.ViewState("SavingtranunkId")))
                objEmployeeSavingData.Delete(CInt(Me.ViewState("SavingtranunkId")), DateAndTime.Now.Date)
                'Shani(24-Aug-2015) -- End

            End If
            FillList()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim objStatusTran As New clsSaving_Status_Tran

            If CInt(cboStatusChange.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Please Select Status. Status is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                cboStatus.Focus()
                popupChangeStatus.Show()
                Exit Sub
            ElseIf CInt(cboStatusChange.SelectedValue) = CInt(Me.ViewState("StatusId")) Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Sorry, This status is already set. Please select different status."), Me)
                'Sohail (23 Mar 2019) -- End
                cboStatus.Focus()
                popupChangeStatus.Show()
                Exit Sub


                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.

            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName1)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Please Select Period. Period is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                popupChangeStatus.Show()
                Exit Sub

                'Pinkal (12 Jan 2015) -- End

            Else
                Dim dtpStatus As Date = Nothing
                Dim dsList = objStatusTran.GetLastStatus("Status", CInt(Me.ViewState("SavingtranunkId")))
                If dsList.Tables("Status").Rows.Count > 0 Then
                    With dsList.Tables("Status").Rows(0)
                        dtpStatus = CDate(.Item("status_date").ToString)
                    End With
                    If dtpStatus.Date > System.DateTime.Today.Date Then

                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName1)
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Sorry, Status date should be greater than last change status date : ") & dtpStatus.Date, Me)
                        'Sohail (23 Mar 2019) -- End
                        'Anjan [04 June 2014] -- End


                        popupChangeStatus.Show()
                        Exit Sub
                    End If
                End If
            End If


            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            If Me.ViewState("SavingStatusHistory") IsNot Nothing Then
                Dim dtTable As DataTable = CType(Me.ViewState("SavingStatusHistory"), DataTable)
                Dim dtPeriod As DataTable = CType(Me.ViewState("Period"), DataTable)
                If (dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0) AndAlso (dtPeriod IsNot Nothing AndAlso dtPeriod.Rows.Count > 0) Then
                    Dim drRow() As DataRow = dtPeriod.Select("Periodunkid = " & CInt(cboPeriod.SelectedValue))
                    If drRow.Length > 0 Then
                        If eZeeDate.convertDate(drRow(0)("end_date").ToString).Date < CDate(dtTable.Rows(0)("end_date")).Date Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'msg.DisplayError(ex, Me)
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 8, "Saving status period should be greater than or equal to the Last saving status period. Please set saving status period greater than or equal to the the Last saving status period."), Me)
                            'Sohail (23 Mar 2019) -- End
                            popupChangeStatus.Show()
                            Exit Sub
                        End If
                    End If
                End If
            End If
            'Pinkal (12 Jan 2015) -- End


            objStatusTran._Isvoid = False
            objStatusTran._Savingtranunkid = CInt(Me.ViewState("SavingtranunkId"))
            objStatusTran._Remark = txtStatusRemark.Text

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            'objStatusTran._Status_Date = System.DateTime.Today
            objStatusTran._Status_Date = DateAndTime.Now.Date
            objStatusTran._PeriodID = CInt(cboPeriod.SelectedValue)
            objStatusTran._WebFormName = "frmSavingStatus_AddEdit"
            objStatusTran._WebIP = CStr(Session("IP_ADD"))
            objStatusTran._WebHost = CStr(Session("HOST_NAME"))
            'Pinkal (12 Jan 2015) -- End


            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboStatusChange.SelectedValue)
            objStatusTran._Userunkid = CInt(Session("UserId"))




            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim blnFlag As Boolean = objStatusTran.Insert
            Dim blnFlag As Boolean = objStatusTran.Insert(DateAndTime.Now.Date)
            'Shani(24-Aug-2015) -- End


            If blnFlag Then
                popupChangeStatus.Hide()
                FillList()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
            popupChangeStatus.Show()
        End Try
    End Sub


    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE. DON'T REMOVE THIS METHOD.

    'Protected Sub popupDelSavingStatus_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelSavingStatus.buttonDelReasonYes_Click
    '    Try
    '        Dim objStatusTran As New clsSaving_Status_Tran
    '        objStatusTran._Savingstatustranunkid = CInt(Me.ViewState("savingstatustranunkid"))
    '        objStatusTran._Savingtranunkid = CInt(Me.ViewState("SavingtranunkId"))

    '        Dim dtTable As DataTable = CType(Me.ViewState("SavingStatusHistory"), DataTable)
    '        dtTable = New DataView(dtTable, "savingstatustranunkid <> " & CInt(Me.ViewState("savingstatustranunkid")), "", DataViewRowState.CurrentRows).ToTable
    '        Dim drRow() As DataRow = dtTable.Select("status_date = MAX(status_date) AND grp = 0")
    '        If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
    '            objStatusTran._Statusunkid = CInt(drRow(0)("statusunkid"))
    '        End If
    '        objStatusTran._WebFormName = "frmSavingStatus_AddEdit"
    '        objStatusTran._WebIP = Session("IP_ADD")
    '        objStatusTran._WebHost = Session("HOST_NAME")
    '        objStatusTran._Isvoid = True
    '        objStatusTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        objStatusTran._Voiduserunkid = Session("UserId")
    '        If objStatusTran.Delete() = False Then
    '            msg.DisplayError(ex, Me)
    '        Else
    '            msg.DisplayError(ex, Me)
    '            FillHistory()
    '            FillList()
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '        popupChangeStatus.Show()
    '    End Try
    'End Sub

    'Pinkal (12 Jan 2015) -- End



    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "DataGridview Event"

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Try

            If e.CommandName = "Change" Then

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'If CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = 4 Then

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'If CInt(dgView.Items(e.Item.ItemIndex).Cells(13).Text) = 4 Then
                If CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text) = 4 Then
                    'Shani(18-Dec-2015) -- End
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, You cannot edit this transaction Reason : This transaction is in completed mode."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Sub
                End If

                Session.Add("SavingtranunkId", dgView.DataKeys(e.Item.ItemIndex))
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                Session("Saving_EmpUnkID") = cboEmployee.SelectedValue
                'SHANI [09 Mar 2015]--END 
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/wPg_Add_Edit_Employee_Saving.aspx", False)

            ElseIf e.CommandName = "Remove" Then

                'Pinkal (12 Jan 2015) -- Start 
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'If clsPayment_tran.enPayTypeId.REPAYMENT = CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) Then

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'If clsPayment_tran.enPayTypeId.REPAYMENT = CInt(dgView.Items(e.Item.ItemIndex).Cells(13).Text) Then
                If clsPayment_tran.enPayTypeId.REPAYMENT = CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text) Then
                    'Shani(18-Dec-2015) -- End

                    'Pinkal (12 Jan 2015) -- End 

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, You cannot delete this Saving Scheme. Reason :This Saving Scheme is in Complete mode."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End

                    Exit Sub

                    'Sohail (12 Jan 2015) -- Start
                    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                    'ElseIf CDec(dgView.Items(e.Item.ItemIndex).Cells(7).Text) > 0 Then

                    '    'Anjan [04 June 2014] -- Start
                    '    'ENHANCEMENT : Implementing Language,requested by Andrew
                    '    'Language.setLanguage(mstrModuleName)
                    '    msg.DisplayError(ex, Me)
                    '    'Anjan [04 June 2014] -- End
                    '    Exit Sub

                    'Shani(18-Dec-2015) -- Start
                    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                    'ElseIf CDec(dgView.Items(e.Item.ItemIndex).Cells(7).Text) > 0 AndAlso (CDec(dgView.Items(e.Item.ItemIndex).Cells(15).Text) <> CDec(dgView.Items(e.Item.ItemIndex).Cells(7).Text) OrElse CBool(dgView.Items(e.Item.ItemIndex).Cells(16).Text)) Then
                ElseIf CDec(dgView.Items(e.Item.ItemIndex).Cells(8).Text) > 0 AndAlso (CDec(dgView.Items(e.Item.ItemIndex).Cells(16).Text) <> CDec(dgView.Items(e.Item.ItemIndex).Cells(8).Text) OrElse CBool(dgView.Items(e.Item.ItemIndex).Cells(17).Text)) Then
                    'Shani(18-Dec-2015) -- End
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason :Deduction of savings is in progress."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                Else
                    Dim objMaster As New clsMasterData
                    Dim objTnALeaveTran As New clsTnALeaveTran
                    Dim objPeriod As New clscommom_period_Tran
                    Dim intOpenPeriod As Integer = 0
                    Dim dtOpenPeriodEnd As DateTime = Nothing

                    'Nilay (15-Dec-2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1)
                    intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
                    'Nilay (15-Dec-2015) -- End

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objPeriod._Periodunkid = intOpenPeriod
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intOpenPeriod
                    'Shani(24-Aug-2015) -- End

                    dtOpenPeriodEnd = objPeriod._End_Date
                    'Shani(18-Dec-2015) -- Start
                    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                    'If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, dgView.Items(e.Item.ItemIndex).Cells(14).Text, dtOpenPeriodEnd, enModuleReference.Payroll) = True Then
                    If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, dgView.Items(e.Item.ItemIndex).Cells(15).Text, dtOpenPeriodEnd, enModuleReference.Payroll) = True Then
                        'Shani(18-Dec-2015) -- End
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, You can not Delete this Saving Scheme. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete this Saving Scheme."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Try
                    End If
                    'Sohail (12 Jan 2015) -- End
                End If

                Me.ViewState.Add("SavingtranunkId", dgView.DataKeys(e.Item.ItemIndex))

                popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popupDelete.Show()

            ElseIf e.CommandName = "Status" Then

                'S.SANDEEP [ 04 DEC 2013 ] -- START

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'If CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = 4 Then

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'If CInt(dgView.Items(e.Item.ItemIndex).Cells(13).Text) = 4 Then
                If CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text) = 4 Then
                    'Shani(18-Dec-2015) -- End

                    'Pinkal (12 Jan 2015) -- End

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, You cannot change startus of this transaction Reason : This transaction is in completed mode."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Sub
                End If
                'S.SANDEEP [ 04 DEC 2013 ] -- END

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'Dim decTotContribution As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(9).Text)
                'Dim decTotInterest As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(8).Text)

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'Dim decTotContribution As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(10).Text)
                'Dim decTotInterest As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(9).Text)
                ''Pinkal (12 Jan 2015) -- End
                'Dim decBalAmt As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(7).Text)
                Dim decTotContribution As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(11).Attributes("contribution"))
                Dim decTotInterest As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(10).Text)
                Dim decBalAmt As Decimal = CDec(dgView.Items(e.Item.ItemIndex).Cells(8).Text)
                'Shani(18-Dec-2015) -- End

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'If CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = 4 _
                '        And Math.Abs(decTotContribution - (decBalAmt - decTotInterest)) >= 1 Then '4=Complete

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'If CInt(dgView.Items(e.Item.ItemIndex).Cells(13).Text) = 4 _
                '                        And Math.Abs(decTotContribution - (decBalAmt - decTotInterest)) >= 1 Then '4=Complete
                If CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text) = 4 _
                        And Math.Abs(decTotContribution - (decBalAmt - decTotInterest)) >= 1 Then '4=Complete
                    'Shani(18-Dec-2015) -- End
                    'Pinkal (12 Jan 2015) -- End

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, You cannot change status when it is complete and payment is done."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Exit Sub
                End If

                Me.ViewState.Add("SavingtranunkId", dgView.DataKeys(e.Item.ItemIndex))
                popupChangeStatus.Show()
                ResetSaving_Status()
                Fill_Info(e.Item.ItemIndex)


            ElseIf e.CommandName = "Payment" Then

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'Select Case CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text)
                'Select Case CInt(dgView.Items(e.Item.ItemIndex).Cells(13).Text)
                'Pinkal (12 Jan 2015) -- End

                'Case 4 'Complete

                '    Case Else 'Inprogress, OnHold, Redemption

                '        'Anjan [04 June 2014] -- Start
                '        'ENHANCEMENT : Implementing Language,requested by Andrew
                '        'Language.setLanguage(mstrModuleName)
                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'msg.DisplayError(ex, Me)
                'msg.DisplayError(ex, Me)
                'Pinkal (12 Jan 2015) -- End
                '        'Anjan [04 June 2014] -- End
                '        Exit Sub
                'End Select

                'S.SANDEEP [ 04 DEC 2013 ] -- START
                'Response.Redirect(Session("servername") & "~/Loan_Savings/wPg_Payment_List.aspx?ProcessId=" _
                '                                  & Server.UrlEncode(clsCrypto.Encrypt(clsPayment_tran.enPaymentRefId.SAVINGS & "|" _
                '                                & dgView.DataKeys(e.Item.ItemIndex) & "|" & clsPayment_tran.enPayTypeId.REPAYMENT)))
                'Response.Redirect(Session("servername") & "~/Payment/wPg_Payment_List.aspx?ProcessId=" _
                '                  & Server.UrlEncode(clsCrypto.Encrypt(clsPayment_tran.enPaymentRefId.SAVINGS & "|" _
                '                & dgView.DataKeys(e.Item.ItemIndex) & "|" & clsPayment_tran.enPayTypeId.REPAYMENT)))
                'S.SANDEEP [ 04 DEC 2013 ] -- END


                'Nilay (15-Dec-2015) -- Start
                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'Select Case CInt(dgView.Items(e.Item.ItemIndex).Cells(13).Text)
                'Select Case CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text)
                '    'Shani(18-Dec-2015) -- End
                '    Case enSavingStatus.IN_PROGRESS, enSavingStatus.ON_HOLD
                '        Response.Redirect(Session("servername") & "~/Payment/wPg_Payment_List.aspx?ProcessId=" _
                '                          & Server.UrlEncode(clsCrypto.Encrypt(clsPayment_tran.enPaymentRefId.SAVINGS & "|" _
                '                            & dgView.DataKeys(e.Item.ItemIndex) & "|" _
                '                            & clsPayment_tran.enPayTypeId.WITHDRAWAL & "|" _
                '                            & e.Item.Cells(19).Text.ToString.Trim _
                '                            )))
                '    Case enSavingStatus.REDEMPTION '<TODO when redemption feature given>
                '        Response.Redirect(Session("servername") & "~/Payment/wPg_Payment_List.aspx?ProcessId=" _
                '                          & Server.UrlEncode(clsCrypto.Encrypt(clsPayment_tran.enPaymentRefId.SAVINGS & "|" _
                '                            & dgView.DataKeys(e.Item.ItemIndex) & "|" _
                '                            & clsPayment_tran.enPayTypeId.REPAYMENT & "|" _
                '                            & e.Item.Cells(19).Text.ToString.Trim _
                '                            )))
                '    Case enSavingStatus.COMPLETED
                '        Response.Redirect(Session("servername") & "~/Payment/wPg_Payment_List.aspx?ProcessId=" _
                '                          & Server.UrlEncode(clsCrypto.Encrypt(clsPayment_tran.enPaymentRefId.SAVINGS & "|" _
                '                                    & dgView.DataKeys(e.Item.ItemIndex) & "|" _
                '                                    & clsPayment_tran.enPayTypeId.REPAYMENT & "|" _
                '                                    & e.Item.Cells(19).Text.ToString.Trim _
                '                                    )))
                'End Select
                ''SHANI [12 JAN 2015]--END 

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'strURL &= clsPayment_tran.enPaymentRefId.SAVINGS
                'strURL &= "|" & dgView.DataKeys(e.Item.ItemIndex).ToString
                'Select Case CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text)
                '    Case enSavingStatus.IN_PROGRESS, enSavingStatus.ON_HOLD
                '        strURL &= "|" & clsPayment_tran.enPayTypeId.WITHDRAWAL
                '    Case enSavingStatus.REDEMPTION '<TODO when redemption feature given>
                '        strURL &= "|" & clsPayment_tran.enPayTypeId.REPAYMENT
                '    Case enSavingStatus.COMPLETED
                '        strURL &= "|" & clsPayment_tran.enPayTypeId.REPAYMENT
                'End Select
                'strURL &= "|" & eZeeDate.convertDate(e.Item.Cells(19).Text.ToString.Trim)
                'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)))

                Session("ReferenceId") = clsPayment_tran.enPaymentRefId.SAVINGS
                Session("TransactionId") = dgView.DataKeys(e.Item.ItemIndex).ToString
                Select Case CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text)
                    Case enSavingStatus.IN_PROGRESS, enSavingStatus.ON_HOLD
                        Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.WITHDRAWAL
                    Case enSavingStatus.REDEMPTION '<TODO when redemption feature given>
                        Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.REPAYMENT
                    Case enSavingStatus.COMPLETED
                        Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.REPAYMENT
                End Select
                Session("LoanSavingEffectiveDate") = eZeeDate.convertDate(e.Item.Cells(19).Text.ToString.Trim)

                Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx", False)
                'Nilay (06-Aug-2016) -- END



                '    'Shani(18-Dec-2015) -- Start
                '    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                'ElseIf e.CommandName = "Deposit" Then
                '    If CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text) = enSavingStatus.COMPLETED Then
                '        msg.DisplayError(ex, Me)
                '    End If

                '    Response.Redirect(Session("servername") & "~/Payment/wPg_Payment_List.aspx?ProcessId=" _
                '                                & Server.UrlEncode(clsCrypto.Encrypt(clsPayment_tran.enPaymentRefId.SAVINGS & "|" _
                '                                & dgView.DataKeys(e.Item.ItemIndex) & "|" _
                '                                & clsPayment_tran.enPayTypeId.DEPOSIT & "|" _
                '                                & e.Item.Cells(19).Text.ToString.Trim _
                '                                )))

                '    'Shani(18-Dec-2015) -- End


            ElseIf e.CommandName = "Deposit" Then

                If CInt(dgView.Items(e.Item.ItemIndex).Cells(14).Text) = enSavingStatus.COMPLETED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, You cannot perform further operation on this transaction. Reason : This transaction is in completed mode."), Me)
                    'Sohail (23 Mar 2019) -- End
                End If

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'strURL &= clsPayment_tran.enPaymentRefId.SAVINGS
                'strURL &= "|" & dgView.DataKeys(e.Item.ItemIndex).ToString
                'strURL &= "|" & clsPayment_tran.enPayTypeId.DEPOSIT
                'strURL &= "|" & eZeeDate.convertDate(e.Item.Cells(19).Text.ToString.Trim)
                'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)))
                Session("ReferenceId") = clsPayment_tran.enPaymentRefId.SAVINGS
                Session("TransactionId") = dgView.DataKeys(e.Item.ItemIndex).ToString
                Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.DEPOSIT
                Session("LoanSavingEffectiveDate") = eZeeDate.convertDate(e.Item.Cells(19).Text.ToString.Trim)
                Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx", False)
                'Nilay (06-Aug-2016) -- END

                'Nilay (15-Dec-2015) -- End

                'SHANI [12 JAN 2015]--END 
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then
                dgView.Columns(0).Visible = CBool(Session("EditEmployeeSavings"))
                dgView.Columns(1).Visible = CBool(Session("DeleteEmployeeSavings"))
                dgView.Columns(2).Visible = CBool(Session("AllowChangeSavingStatus"))


                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                ''Pinkal (12 Jan 2015) -- Start
                ''Enhancement - CHANGE IN SAVINGS MODULE.
                ''e.Item.Cells(7).Text = CDec(e.Item.Cells(7).Text).ToString(Session("fmtcurrency"))
                ''e.Item.Cells(8).Text = CDec(e.Item.Cells(8).Text).ToString(Session("fmtcurrency"))
                ''e.Item.Cells(9).Text = CDec(e.Item.Cells(9).Text).ToString(Session("fmtcurrency"))
                'e.Item.Cells(7).Text = CDec(e.Item.Cells(7).Text).ToString(Session("fmtcurrency"))
                'e.Item.Cells(8).Text = CDec(e.Item.Cells(8).Text).ToString(Session("fmtcurrency"))
                'e.Item.Cells(9).Text = Math.Round(CDec(e.Item.Cells(9).Text), 2)
                ''SHANI (09 Mar 2015) -- Start
                ''Enhancement - Provide Multi Currency on Saving scheme contribution.
                ''e.Item.Cells(10).Text = CDec(e.Item.Cells(10).Text).ToString(Session("fmtcurrency"))
                'If mdicCurrency.ContainsKey(CInt(e.Item.Cells(17).Text)) = True Then
                '    e.Item.Cells(10).Text = CDec(e.Item.Cells(10).Text).ToString(Session("fmtcurrency")).ToString & " " & mdicCurrency.Item(CInt(e.Item.Cells(17).Text))
                'Else
                '    e.Item.Cells(10).Text = CDec(e.Item.Cells(10).Text).ToString(Session("fmtcurrency")).ToString & " " & mstrBaseCurrencySign
                'End If
                ''SHANI (09 Mar 2015) -- End


                ''Dim drRow() As DataRow = CType(Me.ViewState("Status"), DataTable).Select("Id =" & e.Item.Cells(10).Text)

                ''If drRow.Length > 0 Then
                ''    e.Item.Cells(10).Text = drRow(0)("name").ToString()
                ''Else
                ''    e.Item.Cells(10).Text = ""
                ''End If

                ''If e.Item.Cells(11).Text.ToString().Trim <> "" Then
                ''    'S.SANDEEP [ 31 DEC 2013 ] -- START
                ''    'e.Item.Cells(11).Text = eZeeDate.convertDate(e.Item.Cells(11).Text)
                ''    If Session("DateFormat") <> Nothing Then
                ''        e.Item.Cells(11).Text = eZeeDate.convertDate(e.Item.Cells(11).Text).Date.ToString(Session("DateFormat"))
                ''    Else
                ''    e.Item.Cells(11).Text = eZeeDate.convertDate(e.Item.Cells(11).Text)
                ''End If
                ''    'S.SANDEEP [ 31 DEC 2013 ] -- END
                ''End If

                'Dim drRow() As DataRow = CType(Me.ViewState("Status"), DataTable).Select("Id =" & e.Item.Cells(11).Text)

                'If drRow.Length > 0 Then
                '    e.Item.Cells(11).Text = drRow(0)("name").ToString()
                'Else
                '    e.Item.Cells(11).Text = ""
                'End If

                'If e.Item.Cells(12).Text.ToString().Trim <> "" Then
                '    If Session("DateFormat") <> Nothing Then
                '        e.Item.Cells(12).Text = eZeeDate.convertDate(e.Item.Cells(12).Text).Date.ToString(Session("DateFormat"))
                '    Else
                '        e.Item.Cells(12).Text = eZeeDate.convertDate(e.Item.Cells(12).Text)
                'End If
                'End If


                ''Pinkal (12 Jan 2015) -- End

                ''Anjan [04 June 2014] -- Start
                ''ENHANCEMENT : Implementing Language,requested by Andrew
                'If e.Item.HasControls Then
                '    'Language.setLanguage(mstrModuleName)
                '    Dim lnkPayment As LinkButton = DirectCast(e.Item.Cells(3).FindControl("lnkPayment"), LinkButton)
                '    lnkPayment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnPayment", lnkPayment.Text).Replace("&", "")
                '    lnkPayment.ToolTip = lnkPayment.Text

                '    Dim lnkchgstatus As LinkButton = DirectCast(e.Item.Cells(2).FindControl("lnkChangeStatus"), LinkButton)
                '    lnkchgstatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnChangeStatus", lnkchgstatus.Text).Replace("&", "")
                '    lnkchgstatus.ToolTip = lnkchgstatus.Text
                'End If
                ''Anjan [04 June 2014] -- End

                e.Item.Cells(8).Text = CDec(e.Item.Cells(8).Text).ToString(Session("fmtcurrency").ToString)
                e.Item.Cells(9).Text = CDec(e.Item.Cells(9).Text).ToString(Session("fmtcurrency").ToString)
                e.Item.Cells(10).Text = CStr(Math.Round(CDec(e.Item.Cells(10).Text), 2))

                'Nilay (15-Dec-2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                e.Item.Cells(11).Attributes.Add("contribution", e.Item.Cells(11).Text)
                'Nilay (15-Dec-2015) -- End

                If mdicCurrency.ContainsKey(CInt(e.Item.Cells(18).Text)) = True Then
                    e.Item.Cells(11).Text = CDec(e.Item.Cells(11).Text).ToString(Session("fmtcurrency").ToString).ToString & " " & mdicCurrency.Item(CInt(e.Item.Cells(18).Text))
                Else
                    e.Item.Cells(11).Text = CDec(e.Item.Cells(11).Text).ToString(Session("fmtcurrency").ToString).ToString & " " & mstrBaseCurrencySign
                End If

                'Sohail (25 Aug 2021) -- Start
                'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
                'Dim drRow() As DataRow = CType(Me.ViewState("Status"), DataTable).Select("Id =" & e.Item.Cells(12).Text)

                'If drRow.Length > 0 Then
                '    e.Item.Cells(12).Text = drRow(0)("name").ToString()
                'Else
                '    e.Item.Cells(12).Text = ""
                'End If
                'Sohail (25 Aug 2021) -- End

                'Shani(18-Dec-2015) -- Start
                'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                e.Item.Cells(13).Attributes.Add("dtstopdate", e.Item.Cells(13).Text)
                'Shani(18-Dec-2015) -- End

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(13).Text.ToString().Trim <> "" Then
                '    If Session("DateFormat").ToString <> Nothing Then
                '        e.Item.Cells(13).Text = eZeeDate.convertDate(e.Item.Cells(13).Text).Date.ToString(Session("DateFormat").ToString)
                '    Else
                '        e.Item.Cells(13).Text = CStr(eZeeDate.convertDate(e.Item.Cells(13).Text))
                '    End If
                'End If
                If e.Item.Cells(13).Text.ToString().Trim <> "" AndAlso e.Item.Cells(13).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(13).Text = eZeeDate.convertDate(e.Item.Cells(13).Text).Date.ToShortDateString
                End If

                'Pinkal (16-Apr-2016) -- End



                'Hemant (17 Sep 2020) -- Start
                'New UI Change
                'If e.Item.HasControls Then
                '    'Language.setLanguage(mstrModuleName)
                '    Dim lnkPayment As LinkButton = DirectCast(e.Item.Cells(3).FindControl("lnkPayment"), LinkButton)
                '    lnkPayment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"mnuPayment", lnkPayment.Text).Replace("&", "")
                '    lnkPayment.ToolTip = lnkPayment.Text

                '    Dim lnkchgstatus As LinkButton = DirectCast(e.Item.Cells(2).FindControl("lnkChangeStatus"), LinkButton)
                '    lnkchgstatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnChangeStatus", lnkchgstatus.Text).Replace("&", "")
                '    lnkchgstatus.ToolTip = lnkchgstatus.Text

                '    Dim lnkDeposit As LinkButton = DirectCast(e.Item.Cells(4).FindControl("lnkDeposit"), LinkButton)
                '    lnkDeposit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"mnuDeposit", lnkDeposit.Text).Replace("&", "")
                '    lnkDeposit.ToolTip = lnkDeposit.Text
                'End If
                'Hemant (17 Sep 2020) -- End
                'Shani(18-Dec-2015) -- End
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

    Protected Sub dgvHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvHistory.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then
                e.Item.Cells(0).Text = e.Item.Cells(0).Text.Replace(" ", "&nbsp;")
                If CBool(e.Item.Cells(3).Text) Then
                    'e.Item.Cells(0).ColumnSpan = dgvHistory.Columns.Count - 5
                    'Dim imgbutton As ImageButton = CType(e.Item.Cells(0).FindControl("DeleteImgSavingStatus"), ImageButton)
                    'imgbutton.Visible = False
                    'e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    'e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                    'e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                    'e.Item.Cells(3).CssClass = "GroupHeaderStyleBorderRight"

                    e.Item.Cells(0).ColumnSpan = dgvHistory.Columns.Count - 4
                    e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderRight"
                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Else
                    If e.Item.Cells(1).Text.ToString().Trim <> "" AndAlso e.Item.Cells(1).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(1).Text = CDate(e.Item.Cells(1).Text).Date.ToShortDateString
                    End If
                    'Pinkal (16-Apr-2016) -- End

                End If
            End If
        Catch ex As Exception
            popupChangeStatus.Show()
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'DON'T REMOVE THIS METHOD.

    'Protected Sub dgvHistory_DeleteCommand(ByVal source As Object, ByVal fe As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.DeleteCommand
    '    Try
    '        If e.CommandName = "Delete" Then
    '            Dim dtTable As DataTable = CType(Me.ViewState("SavingStatusHistory"), DataTable)

    '            Dim dtTemp As DataTable = New DataView(dtTable, "grp = 0", "", DataViewRowState.CurrentRows).ToTable
    '            If dtTemp IsNot Nothing AndAlso dtTemp.Rows.Count <= 1 Then
    '                msg.DisplayError(ex, Me)
    '                Exit Sub
    '            End If


    '            Dim drRow() As DataRow = dtTable.Select("status_date = MAX(status_date) AND grp = 0")
    '            If drRow.Length > 0 Then
    '                Dim objperiod As New clscommom_period_Tran
    '                objperiod._Periodunkid = CInt(drRow(0)("periodunkid"))
    '                If objperiod._Statusid = 2 Then '2 Close
    '                    msg.DisplayError(ex, Me)
    '                    Exit Sub
    '                End If
    '                objperiod = Nothing

    '                If CInt(drRow(0)("savingstatustranunkid")) = CInt(e.Item.Cells(5).Text) Then
    '                    If Me.ViewState("savingstatustranunkid") Is Nothing Then
    '                        Me.ViewState.Add("savingstatustranunkid", CInt(e.Item.Cells(5).Text))
    '                    Else
    '                        Me.ViewState("savingstatustranunkid") = CInt(e.Item.Cells(5).Text)
    '                    End If
    '                    popupDelSavingStatus.Reason = ""
    '                    popupDelSavingStatus.Show()
    '                Else
    '                    msg.DisplayError(ex, Me)
    '                    popupChangeStatus.Show()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        popupChangeStatus.Show()
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub


    'Pinkal (12 Jan 2015) -- End



#End Region

#Region "Toolbar Event"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try
    '        Response.Redirect("~\Loan_Savings\wPg_Add_Edit_Employee_Saving.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

    'Nilay (02 Feb 2017) -- Start
    'Issue : first open payroll period was supposed to be passed and financial year first period was passed so it was not giving active employee in search combo.
#Region " ComboBox's Events "
    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Call FillEmployee(CInt(cboPayPeriod.SelectedValue))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("cboPayPeriod_SelectedIndexChanged" & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region
    'Nilay (02 Feb 2017) -- End



    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblContribution.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblContribution.ID, Me.lblContribution.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblPayPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblSvScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblSavingScheme", Me.lblSvScheme.Text)

            Me.dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(4).FooterText, Me.dgView.Columns(4).HeaderText)
            Me.dgView.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(5).FooterText, Me.dgView.Columns(5).HeaderText)
            Me.dgView.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(6).FooterText, Me.dgView.Columns(6).HeaderText)
            Me.dgView.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(7).FooterText, Me.dgView.Columns(7).HeaderText)
            Me.dgView.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(8).FooterText, Me.dgView.Columns(8).HeaderText)
            Me.dgView.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(9).FooterText, Me.dgView.Columns(9).HeaderText)
            Me.dgView.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(10).FooterText, Me.dgView.Columns(10).HeaderText)
            Me.dgView.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(11).FooterText, Me.dgView.Columns(11).HeaderText)
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Me.dgView.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(12).FooterText, Me.dgView.Columns(12).HeaderText)
            'SHANI [12 JAN 2015]-END
            'Language.setLanguage(mstrModuleName1)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End


            Me.lblStatusform.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.lblStatusform.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lblSvStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblSvStatus.ID, Me.lblSvStatus.Text)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.LblSavingScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblSavingScheme.ID, Me.LblSavingScheme.Text)

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.dgView.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(1).FooterText, Me.dgView.Columns(1).HeaderText)
            Me.dgView.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(2).FooterText, Me.dgView.Columns(2).HeaderText)
            Me.dgView.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(3).FooterText, Me.dgView.Columns(3).HeaderText)
            Me.dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(4).FooterText, Me.dgView.Columns(4).HeaderText)
            'Pinkal (12 Jan 2015) -- End



        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
