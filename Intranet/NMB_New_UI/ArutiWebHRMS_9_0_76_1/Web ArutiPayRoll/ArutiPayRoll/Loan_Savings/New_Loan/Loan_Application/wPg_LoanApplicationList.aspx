﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LoanApplicationList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_LoanApplicationList" MaintainScrollPositionOnPostback="true"
    Title="Loan Application List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
  
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                
                <uc2:DeleteReason ID="popDeleteReason" runat="server" />
                <uc4:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Application List" CssClass="form-label"> </asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" ToolTip="Allocations">
                                                    <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblLoanAdvance" runat="server" Text="Loan/Advance" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblAppNo" runat="server" Text="App. No." CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApplicationNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label">
                                        </asp:Label>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                            <div class="form-group m-t-0">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control money-euro" onKeypress="return onlyNumbers(this, event);" 
                                                        Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <div class="form-group m-t-0">
                                                <asp:DropDownList ID="cboAmountCondition" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblAppDate" runat="server" Text="App. Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpAppDate" runat="server" CssClass="form-control" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblApprovedAmount" runat="server" Text="App. Amount" CssClass="form-label">
                                        </asp:Label>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                            <div class="form-group m-t-0">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtApprovedAmount" runat="server" CssClass="form-control" onKeypress="return onlyNumbers(this, event);"
                                                        Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <div class="form-group m-t-0">
                                                <asp:DropDownList ID="cboAppAmountCondition" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="btn-group m-l-0" style="float: left">
                                    <asp:LinkButton ID="btnOperation" runat="server"  Text="Global Approve" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Operation<span class="caret"></span>
                                    </asp:LinkButton>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <asp:LinkButton ID="lnkGlobalApprove" runat="server" Text="Global Approve"></asp:LinkButton></li>
                                        <li>
                                            <asp:LinkButton ID="lnkGlobalCancelApproved" runat="server" Text="Global Cancel Approved"></asp:LinkButton></li>
                                        <li>
                                            <asp:LinkButton ID="lnkGlobalAssign" runat="server" Text="Global Assign"></asp:LinkButton></li>
                                    </ul>
                                </div>
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvLoanApplicationList" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="brnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                     <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="60px" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkAssign" runat="server" ToolTip="Assign" CommandName="Assign"
                                                                    Font-Underline="false" Text="Assign"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Application_No" HeaderText="Application No" ReadOnly="true"
                                                        FooterText="dgcolhApplicationNo" />
                                                    <asp:BoundColumn DataField="applicationdate" HeaderText="App Date" FooterText="dgcolhAppDate" />
                                                    <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhEmployee" />
                                                    <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" ReadOnly="true"
                                                        FooterText="dgcolhLoanScheme" />
                                                    <asp:BoundColumn DataField="Loan_Advance" HeaderText="Loan/Advance" ReadOnly="true"
                                                        FooterText="dgcolhLoan_Advance" />
                                                    <asp:BoundColumn DataField="Amount" HeaderText="Loan Amount" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="dgcolhAmount" />
                                                    <asp:BoundColumn DataField="Approved_Amount" HeaderText="Approved Amount" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="dgcolhApp_Amount" />
                                                    <asp:BoundColumn DataField="LoanStatus" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                    <asp:BoundColumn DataField="isloan" HeaderText="IsLoan" ReadOnly="true" Visible="false"
                                                        FooterText="objdgcolhIsLoan" />
                                                    <asp:BoundColumn DataField="loan_statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhStatusunkid" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmpID" ReadOnly="true" Visible="false"
                                                        FooterText="objdgcolhEmpId" />
                                                    <asp:BoundColumn DataField="loanschemeunkid" HeaderText="SchemeID" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhSchemeID" />
                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="Approver" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhApprover" />
                                                    <asp:BoundColumn DataField="isexternal_entity" HeaderText="IsExternal" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhIsExternal" />
                                                    <asp:BoundColumn DataField="processpendingloanunkid" HeaderText="" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhprocesspendingloanunkid" />
                                                    <asp:BoundColumn DataField="remark" HeaderText="Cancel Remarks" ReadOnly="true" FooterText="dgcolhCancelRemark" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
