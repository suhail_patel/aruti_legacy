﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Application_wPg_AddEditLoanApplication
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmLoanApplication_AddEdit"
    Dim DisplayMessage As New CommonCodes
    Private objProcesspendingloan As New clsProcess_pending_loan
    'Nilay (08-Dec-2016) -- Start
    'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
    'Private objStatusTran As clsProcess_pending_loan
    'Nilay (08-Dec-2016) -- End
    Private mdecInstallmentAmt As Decimal = 0
    Private mdecEMIInstallments As Decimal = 0
    'Nilay (06-Aug-2016) -- Start
    'CHANGES : Replace Query String with Session and ViewState
    Private mintProcesspendingloanunkid As Integer = 0
    'Nilay (06-Aug-2016) -- END

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52
    Private mdecMaxLoanAmountDefined As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private mintLastCalcTypeId As Integer = 0
    Private mdecInstallmentAmount As Decimal = 0
    Private mintNoOfInstallment As Integer = 1

    Private decInstallmentAmount As Decimal = 0
    Private decInstrAmount As Decimal = 0
    Private decTotInstallmentAmount As Decimal = 0
    Private decTotIntrstAmount As Decimal = 0
    Private decNetAmount As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mdecEMI_NetPayPercentage As Decimal = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    'Sohail (22 Sep 2017) -- End
    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private mintTranheadUnkid As Integer = 0
    'Sohail (11 Apr 2018) -- End
#End Region

#Region "Page's Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                Call FillPeriod(False)
                'Sohail (16 May 2018) -- End

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                    'Hemant (19 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
                    radAdvance.Visible = Not CBool(Session("DontAllowAdvanceOnESS"))
                    'Hemant (19 Aug 2019) -- End
                End If
                'Nilay (10-Dec-2016) -- End

                If Session("ProcessPendingLoanunkid") Is Nothing Then
                    'dtpApplicationDate.SetDate = Session("fin_startdate")

                    'Nilay (07-Feb-2016) -- Start
                    'If ConfigParameter._Object._CurrentDateAndTime.Date < CDate(Session("fin_startdate")) Then
                    '    dtpApplicationDate.SetDate = CDate(Session("fin_startdate"))
                    'ElseIf ConfigParameter._Object._CurrentDateAndTime.Date > CDate(Session("fin_enddate")) Then
                    '    dtpApplicationDate.SetDate = CDate(Session("fin_enddate"))
                    'Else
                    '    dtpApplicationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    'End If
                    If DateAndTime.Now.Date < CDate(Session("fin_startdate")) Then
                        dtpApplicationDate.SetDate = CDate(Session("fin_startdate"))
                    ElseIf DateAndTime.Now.Date > CDate(Session("fin_enddate")) Then
                        dtpApplicationDate.SetDate = CDate(Session("fin_enddate"))
                    Else
                        dtpApplicationDate.SetDate = DateAndTime.Now.Date
                    End If
                    'Nilay (07-Feb-2016) -- End

                End If

                If Session("ProcessPendingLoanunkid") IsNot Nothing Then
                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    'objProcesspendingloan._Processpendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
                    mintProcesspendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
                    objProcesspendingloan._Processpendingloanunkid = mintProcesspendingloanunkid
                    Session("ProcessPendingLoanunkid") = Nothing
                    'Nilay (06-Aug-2016) -- END

                    radLoan.Enabled = False
                    radAdvance.Enabled = False
                    'If CBool(objProcesspendingloan._Isloan) = True Then
                    '    Call radLoan_CheckedChanged(Nothing, Nothing)
                    'Else
                    '    Call radAdvance_CheckedChanged(Nothing, Nothing)
                    'End If

                    Call GetValue()
                    'Call SetVisibility()
                    cboEmpName.Focus()
                    mdecEMIInstallments = objProcesspendingloan._NoOfInstallment
                    mdecInstallmentAmt = objProcesspendingloan._InstallmentAmount
                    Call chkExternalEntity_CheckedChanged(sender, Nothing)
                End If

            Else
                mdecInstallmentAmt = CDec(Me.ViewState("InstallmentAmt"))
                mdecEMIInstallments = CDec(Me.ViewState("EMIInstallments"))
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'objProcesspendingloan._Processpendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
                mintProcesspendingloanunkid = CInt(Me.ViewState("Processpendingloanunkid"))
                'Nilay (06-Aug-2016) -- END

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 52
                mdecMaxLoanAmountDefined = CDec(Me.ViewState("MaxLoanAmountDefined"))
                'S.SANDEEP [20-SEP-2017] -- END

                'ISSUE/ENHANCEMENT : REF-ID # 50
                mintLastCalcTypeId = CInt(Me.ViewState("LastCalcTypeId"))
                mdecInstallmentAmount = CDec(Me.ViewState("InstallmentAmount"))
                mintNoOfInstallment = CInt(Me.ViewState("NoOfInstallment"))
                decInstallmentAmount = CDec(Me.ViewState("decInstallmentAmount"))
                decInstrAmount = CDec(Me.ViewState("decInstrAmount"))
                decTotInstallmentAmount = CDec(Me.ViewState("decTotInstallmentAmount"))
                decTotIntrstAmount = CDec(Me.ViewState("decTotIntrstAmount"))
                decNetAmount = CDec(Me.ViewState("decNetAmount"))
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdtPeriodStart = CDate(ViewState("mdtPeriodStart"))
                mdtPeriodEnd = CDate(ViewState("mdtPeriodEnd"))
                mdecEMI_NetPayPercentage = CDec(ViewState("mdecEMI_NetPayPercentage"))
                mstrBaseCurrSign = ViewState("mstrBaseCurrSign").ToString()
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mintPaidCurrId = CInt(ViewState("mintPaidCurrId"))
                mdecBaseExRate = CDec(ViewState("mdecBaseExRate"))
                mdecPaidExRate = CDec(ViewState("mdecPaidExRate"))
                'Sohail (22 Sep 2017) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                mintTranheadUnkid = CInt(ViewState("mintTranheadUnkid"))
                'Sohail (11 Apr 2018) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me) 'Hemant (13 Aug 2020)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("InstallmentAmt") = mdecInstallmentAmt
            Me.ViewState("EMIInstallments") = mdecEMIInstallments
            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            Me.ViewState("Processpendingloanunkid") = mintProcesspendingloanunkid
            'Nilay (06-Aug-2016) -- END

            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            Me.ViewState("MaxLoanAmountDefined") = mdecMaxLoanAmountDefined
            'S.SANDEEP [20-SEP-2017] -- END

            'ISSUE/ENHANCEMENT : REF-ID # 50
            Me.ViewState("LastCalcTypeId") = mintLastCalcTypeId
            Me.ViewState("InstallmentAmount") = mdecInstallmentAmount
            Me.ViewState("NoOfInstallment") = mintNoOfInstallment

            Me.ViewState("decInstallmentAmount") = decInstallmentAmount
            Me.ViewState("decInstrAmount") = decInstrAmount
            Me.ViewState("decTotInstallmentAmount") = decTotInstallmentAmount
            Me.ViewState("decTotIntrstAmount") = decTotIntrstAmount
            Me.ViewState("decNetAmount") = decNetAmount
            'S.SANDEEP [20-SEP-2017] -- END

            'Sohail (22 Sep 2017) -- Start
            'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
            Me.ViewState("mdtPeriodStart") = mdtPeriodStart
            Me.ViewState("mdtPeriodEnd") = mdtPeriodEnd
            Me.ViewState("mdecEMI_NetPayPercentage") = mdecEMI_NetPayPercentage
            Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("mintBaseCurrId") = mintBaseCurrId
            Me.ViewState("mintPaidCurrId") = mintPaidCurrId
            Me.ViewState("mdecBaseExRate") = mdecBaseExRate
            Me.ViewState("mdecPaidExRate") = mdecPaidExRate
            'Sohail (22 Sep 2017) -- End
            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            Me.ViewState("mintTranheadUnkid") = mintTranheadUnkid
            'Sohail (11 Apr 2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objMasterData As New clsMasterData
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            'Dim objPeriod As New clscommom_period_Tran'Sohail (16 May 2018)
            'ISSUE/ENHANCEMENT : REF-ID # 50
            Dim objLoan_Advance As New clsLoan_Advance
            'S.SANDEEP [20-SEP-2017] -- END

            'Nilay (05-May-2016) -- Start
            'GDS Issue - Loan application not shown in approval process
            'dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                     Session("UserAccessModeSetting").ToString, _
            '                                     True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                     "List", True)

            'With cboEmpName
            '    .DataValueField = "employeeunkid"
            '    .DataTextField = "employeename"
            '    .DataSource = dsList.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "List", True)

                With cboEmpName
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmpName.DataSource = objglobalassess.ListOfEmployee
                cboEmpName.DataTextField = "loginname"
                cboEmpName.DataValueField = "employeeunkid"
                cboEmpName.DataBind()
            End If
            'Nilay (05-May-2016) -- End

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Hemant (25 Apr 2019) -- Start
            'Isssue#0003774(Sport Pesa):  net pay transhead is not showing on the list. Optional basic salary head not working. when you map other transaction head on "basic salary head (Optional)" (loan setting->configuration) the system gives error during application
            cboLoanScheme_SelectedIndexChanged(Nothing, Nothing)
            'Hemant (25 Apr 2019) -- End


            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedIndex = 0
            End With

            'Sohail (16 May 2018) -- Start
            'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
            '                                   CInt(Session("Fin_year")), Session("Database_Name").ToString, _
            '                                   CDate(Session("fin_startdate")), _
            '                                   "List", True, 1)

            'With cboDeductionPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'Sohail (16 May 2018) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With
            'S.SANDEEP [20-SEP-2017] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)   'Hemant (13 Aug 2020)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmpName.Focus()
                Return False
            End If
            If radLoan.Checked = True Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                    cboLoanScheme.Focus()
                    Return False
                End If

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 57
                Dim objLA As New clsLoan_Advance
                If objLA.GetExistLoan(CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), True) = True Then
                    cnftopup.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, you can not apply loan for the selected scheme: Reason, The selected loan scheme is already assigned to you and having (In-progress or On hold) status. Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations.") & vbCrLf & _
                                        "Do you want to add now?"
                    cnftopup.Show()
                    'Language.setLanguage(mstrModuleName)
                    objLA = Nothing
                    Return False
                End If
                objLA = Nothing
                'S.SANDEEP [20-SEP-2017] -- END

            End If
            If CDec(txtLoanAmt.Text) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information."), Me)
                txtLoanAmt.Focus()
                Return False
            End If
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Currency is compulsory information.Please select currency."), Me)
                cboCurrency.Focus()
                Return False
            End If
            If CInt(Session("LoanApplicationNoType")) = 0 Then
                If txtApplicationNo.Text = "" Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Application No cannot be blank. Application No is compulsory information."), Me)
                    txtApplicationNo.Focus()
                    Return False
                End If
            End If
            If radLoan.Checked = True Then
                If chkExternalEntity.Checked = True Then
                    If txtExternalEntity.Text = "" Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "External Entity cannot be blank. External Entity is compulsory information."), Me)
                    End If
                End If
            End If

            If dtpApplicationDate.GetDate.Date > CDate(Session("fin_enddate")) Or dtpApplicationDate.GetDate.Date < CDate(Session("fin_startdate")) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Application date should be between current financial year."), Me)
                dtpApplicationDate.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Deduction Period is compulsory information.Please select deduction period."), Me)
                cboDeductionPeriod.Focus()
                Return False
            End If

            If radLoan.Checked = True Then
                'If CInt(txtDurationInMths.Text) <= 0 Then
                '    'Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Duration in Months cannot be 0.Please define duration in months greater than 0."), Me)
                '    txtDurationInMths.Focus()
                '    Return False
                'End If

                If CDec(txtInstallmentAmt.Text) <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Installment Amount cannot be 0.Please define installment amount greater than 0."), Me)
                    txtInstallmentAmt.Focus()
                    Return False
                End If

                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                    Dim decValue As Decimal
                    Decimal.TryParse(txtPrincipalAmt.Text, decValue)
                    If decValue <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Principal Amount cannot be 0. Please enter Principal amount."), Me)
                        txtPrincipalAmt.Focus()
                        Return False
                    End If
                End If
                'Sohail (09 Feb 2018) -- End

                If CDec(txtInstallmentAmt.Text) > CDec(txtLoanAmt.Text) Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- Start
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Installment Amount cannot be greater than Loan Amount."), Me)
                    txtInstallmentAmt.Focus()
                    Return False
                End If


                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                'If CDec(txtEMIInstallments.Text) <= 0 Then
                '    'Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "No of Installment cannot be 0.Please define No of Installment greater than 0."), Me)
                '    txtEMIInstallments.Focus()
                '    Return False
                'End If
                'S.SANDEEP [20-SEP-2017] -- END



                'If Format(CDec(txtLoanAmt.Text), Session("fmtCurrency").ToString) <> Format((CDec(txtEMIInstallments.Text) * mdecInstallmentAmt), Session("fmtCurrency").ToString) Then
                '    'If Format(CDec(txtLoanAmt.Text), Session("fmtCurrency")) <> Format((mdecEMIInstallments * mdecInstallmentAmt), Session("fmtCurrency")) Then
                '    'Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "You are changing loan amount.We recommanded you that you have to change No of installments or Installment amount."), Me)
                '    txtEMIInstallments.Focus()
                '    Return False
                'End If

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 52
                If mdecMaxLoanAmountDefined > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    If CDec(txtLoanAmt.Text) > mdecMaxLoanAmountDefined Then
                        'Varsha (25 Nov 2017) -- Start
                        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                        'Language.setLanguage(mstrModuleName)
                        'Varsha (25 Nov 2017) -- Start
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you can not apply for this loan scheme: Reason, amount applied is exceeding the max amount") & " [" & Format(mdecMaxLoanAmountDefined, Session("fmtCurrency").ToString) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, " set for selected scheme."), Me)
                        txtLoanAmt.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                If mdecEMI_NetPayPercentage > 0 Then
                    'Dim objTnALeave As New clsTnALeaveTran 'Sohail (11 Apr 2018)
                    'Sohail (19 Jan 2018) -- Start
                    'Issue: PACRA - issue # 0001894: Unable to apply for loan when user tries to apply loan, he gets the message "No previous salary history for employee" - Employee not able to apply for loan in ESS if loan installement limitation is set in 70.1.
                    'Dim ds As DataSet = objTnALeave.GetPreviousNetPay(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, False, True, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    Dim blnApplyUserAccess As Boolean = True
                    If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                        blnApplyUserAccess = False
                    End If
                    'Sohail (11 Apr 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                    'Dim ds As DataSet = objTnALeave.GetPreviousNetPay(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, False, blnApplyUserAccess, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    Dim ds As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    Dim dsMapped As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- End
                    'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                    Dim objTnALeave As New clsTnALeaveTran
                    ds = objTnALeave.GetPreviousNetPay(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, False, blnApplyUserAccess, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    'Else 'Sohail (06 Jul 2018)
                    If mintTranheadUnkid > 0 Then 'Sohail (06 Jul 2018)
                        Dim objMaster As New clsMasterData
                        Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                        Dim strFilter As String = ""
                        Dim dtStart As Date = mdtPeriodStart
                        Dim dtEnd As Date = mdtPeriodEnd
                        Dim strDBName As String = Session("Database_Name").ToString()
                        Dim intYearId As Integer = CInt(Session("UserId"))
                        If intPrevPeriod <= 0 Then
                            strFilter = " 1 = 2 "
                        Else
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(Session("Database_Name").ToString()) = intPrevPeriod
                            dtStart = objPeriod._Start_Date
                            dtEnd = objPeriod._End_Date
                            If intYearId <> objPeriod._Yearunkid Then
                                intYearId = objPeriod._Yearunkid
                                strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, CInt(Session("CompanyUnkId")))
                            End If
                            strFilter = " prpayrollprocess_tran.tranheadunkid = " & mintTranheadUnkid & " "
                        End If
                        Dim objProcess As New clsPayrollProcessTran
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'ds = objProcess.GetList(strDBName, CInt(Session("UserId")), intYearId, CInt(Session("CompanyUnkId")), dtStart, dtEnd, Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , blnApplyUserAccess, strFilter)
                        dsMapped = objProcess.GetList(strDBName, CInt(Session("UserId")), intYearId, CInt(Session("CompanyUnkId")), dtStart, dtEnd, Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , blnApplyUserAccess, strFilter)
                        'Sohail (06 Jul 2018) -- End
                    End If
                    'Sohail (11 Apr 2018) -- End
                    'Sohail (19 Jan 2018) -- End
                    If ds.Tables(0).Rows.Count <= 0 Then
                        'Varsha (25 Nov 2017) -- Start
                        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                        'Language.setLanguage(mstrModuleName)
                        'Varsha (25 Nov 2017) -- Start
                        'Sohail (11 Apr 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee."), Me)
                        'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee."), Me)
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Else
                        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, you can not apply for this loan scheme: Reason, Either There is no previous salary history for this employee for the net pay head set on loan scheme master screen."), Me)
                        'End If
                        'Sohail (06 Jul 2018) -- End
                        'Sohail (11 Apr 2018) -- End
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    ElseIf mintTranheadUnkid > 0 AndAlso dsMapped.Tables(0).Rows.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, you can not apply for this loan scheme: Reason, Either There is no previous salary history for this employee for the net pay head set on loan scheme master screen."), Me)
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- End
                    Else
                        Dim decAmt As Decimal = 0
                        Decimal.TryParse(txtInstallmentAmt.Text, decAmt)
                        If mdecBaseExRate <> 0 Then
                            decAmt = decAmt * mdecBaseExRate / mdecPaidExRate
                        Else
                            decAmt = decAmt * mdecPaidExRate
                        End If
                        'Sohail (11 Apr 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                        'If decAmt > ((CDec(ds.Tables(0).Rows(0)("total_amount")) * mdecEMI_NetPayPercentage) / 100) Then
                        Dim decPrevNetPay As Decimal = 0
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        Dim decPrevActualNetPay As Decimal = 0
                        Dim decOtherLoanEMI As Decimal = 0
                        'Sohail (06 Jul 2018) -- End
                        If mintTranheadUnkid <= 0 Then
                            decPrevNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))
                        Else
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("amountPaidCurrency"))
                            decPrevNetPay = CDec(dsMapped.Tables(0).Rows(0)("amountPaidCurrency"))
                            'Sohail (06 Jul 2018) -- End

                        End If
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Dim decCheckPoint As Decimal = decPrevNetPay - decAmt
                        decPrevActualNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))

                        Dim objLoanAdvance As New clsLoan_Advance
                        Dim dsLoanCalc As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                  mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, _
                                                  "Loan", mdtPeriodEnd.AddDays(1), cboEmpName.SelectedValue.ToString(), 0, _
                                                  mdtPeriodStart, True, , , True, True, , True, True, , , , 0, , , , , Nothing)
                        Dim dtTable As DataTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ") AND transactionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows

                            If CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = False Then
                                decOtherLoanEMI += 0
                            ElseIf CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = True Then
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyInterestPaidCurrency").ToString) + CDec(dtRow.Item("TotInterestAmountPaidCurrency").ToString)
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(dtRow.Item("TotPMTAmountPaidCurrency").ToString)
                            End If
                        Next
                        Dim dsLoan As DataSet = objProcesspendingloan.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, False, "Loan", 0, "lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & CInt(enLoanApplicationStatus.PENDING) & ", " & CInt(enLoanApplicationStatus.APPROVED) & " ) ")
                        For Each dtRow As DataRow In dsLoan.Tables(0).Rows
                            If CBool(dtRow.Item("isloan")) = True Then
                                Dim dtEnd_Date As Date = DateAdd(DateInterval.Month, CInt(dtRow.Item("noofinstallment")), mdtPeriodStart).AddDays(-1)
                                Dim decIntAmt As Decimal
                                Dim decEMI As Decimal
                                Dim decTotIntAmt As Decimal
                                Dim decTotEMI As Decimal
                                objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dtRow.Item("Amount")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart, dtEnd_Date.Date)), CDec(dtRow.Item("interest_rate")), CType(dtRow.Item("loancalctype_id"), enLoanCalcId), CType(dtRow.Item("interest_calctype_id"), enLoanInterestCalcType), CInt(dtRow.Item("noofinstallment")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart.Date, mdtPeriodEnd.Date.AddDays(1))), CDec(dtRow.Item("installmentamt")), decIntAmt, decEMI, decTotIntAmt, decTotEMI)
                                decOtherLoanEMI += decEMI
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("amount"))
                            End If

                        Next
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'Dim decCheckPoint As Decimal = decPrevActualNetPay - decAmt - decOtherLoanEMI
                        Dim decCheckPoint As Decimal = decAmt + decOtherLoanEMI
                        'Sohail (19 Mar 2020) -- End
                        'Sohail (06 Jul 2018) -- End
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) > decCheckPoint Then
                        If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) < decCheckPoint Then
                            'Sohail (19 Mar 2020) -- End
                            'Sohail (11 Apr 2018) -- End
                            'Varsha (25 Nov 2017) -- Start
                            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                            'Language.setLanguage(mstrModuleName)
                            'Varsha (25 Nov 2017) -- Start
                            'Sohail (11 Apr 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "of pervious net pay.") & " " & Format(CDec(ds.Tables(0).Rows(0)("total_amount")), Session("fmtCurrency").ToString()), Me)
                            If mintTranheadUnkid <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "of pervious net pay.") & " " & Format(decPrevNetPay, Session("fmtCurrency").ToString()), Me)
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "of pervious net pay head") & " " & Format(decPrevNetPay, Session("fmtCurrency").ToString()), Me)
                            End If
                            'Sohail (11 Apr 2018) -- End
                            txtLoanAmt.Focus()
                            Return False
                        End If
                    End If
                End If
                'Sohail (22 Sep 2017) -- End

                'Sohail (17 Dec 2019) -- Start
                'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                Dim intNoOfInstallment As Integer
                Integer.TryParse(txtEMIInstallments.Text, intNoOfInstallment)
                If intNoOfInstallment > 1 Then
                    Dim dtLoanEnd As Date = mdtPeriodStart.AddMonths(intNoOfInstallment).AddDays(-1)
                    Dim objEmpDates As New clsemployee_dates_tran
                    Dim ds As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, cboEmpName.SelectedValue.ToString)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, eZeeDate.convertDate(ds.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                        If intNoOfInstallment > intEmpTenure Then
                            'Hemant (24 Nov 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                            If CBool(Session("SkipEOCValidationOnLoanTenure")) = False Then
                                'Hemant (24 Nov 2021) -- End
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 41, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "].", Me)
                                If txtEMIInstallments.Enabled = True Then txtEMIInstallments.Focus()
                                Return False
                            End If 'Hemant (24 Nov 2021)
                        End If
                    End If
                End If
                'Sohail (17 Dec 2019) -- End

                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            Else
                If CDec(Session("Advance_NetPayPercentage")) > 0 Then
                    Dim blnApplyUserAccess As Boolean = True
                    If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                        blnApplyUserAccess = False
                    End If
                    Dim ds As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    Dim dsMapped As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- End
                    'If CInt(Session("Advance_NetPayTranheadUnkid")) <= 0 Then 'Sohail (06 Jul 2018)
                    Dim objTnALeave As New clsTnALeaveTran
                    ds = objTnALeave.GetPreviousNetPay(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, False, blnApplyUserAccess, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    'Else 'Sohail (06 Jul 2018)
                    If CInt(Session("Advance_NetPayTranheadUnkid")) > 0 Then 'Sohail (06 Jul 2018)
                        Dim objMaster As New clsMasterData
                        Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                        Dim strFilter As String = ""
                        Dim dtStart As Date = mdtPeriodStart
                        Dim dtEnd As Date = mdtPeriodEnd
                        Dim strDBName As String = Session("Database_Name").ToString()
                        'Sohail (12 Oct 2018) -- Start
                        'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                        'Dim intYearId As Integer = CInt(Session("UserId"))
                        Dim intYearId As Integer = CInt(Session("Fin_year"))
                        'Sohail (12 Oct 2018) -- End
                        If intPrevPeriod <= 0 Then
                            strFilter = " 1 = 2 "
                        Else
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(Session("Database_Name").ToString()) = intPrevPeriod
                            dtStart = objPeriod._Start_Date
                            dtEnd = objPeriod._End_Date
                            If intYearId <> objPeriod._Yearunkid Then
                                intYearId = objPeriod._Yearunkid
                                strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, CInt(Session("CompanyUnkId")))
                            End If
                            strFilter = " prpayrollprocess_tran.tranheadunkid = " & mintTranheadUnkid & " "
                        End If
                        Dim objProcess As New clsPayrollProcessTran
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'ds = objProcess.GetList(strDBName, CInt(Session("UserId")), intYearId, CInt(Session("CompanyUnkId")), dtStart, dtEnd, Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , blnApplyUserAccess, strFilter)
                        dsMapped = objProcess.GetList(strDBName, CInt(Session("UserId")), intYearId, CInt(Session("CompanyUnkId")), dtStart, dtEnd, Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , blnApplyUserAccess, strFilter)
                        'Sohail (06 Jul 2018) -- End
                    End If

                    If ds.Tables(0).Rows.Count <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        'If CInt(Session("Advance_NetPayTranheadUnkid")) <= 0 Then 'Sohail (06 Jul 2018)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee."), Me)
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Else
                        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Sorry, you can not apply for this Advance: Reason, Either There is no previous salary history for this employee for the net pay head set on Configuration -> Option screen."), Me)
                        'End If
                        'Sohail (06 Jul 2018) -- End
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    ElseIf CInt(Session("Advance_NetPayTranheadUnkid")) > 0 AndAlso dsMapped.Tables(0).Rows.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Sorry, you can not apply for this Advance: Reason, Either There is no previous salary history for this employee for the net pay head set on Configuration -> Option screen."), Me)
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- End
                    Else
                        Dim decAmt As Decimal = 0
                        'Hemant (25 Apr 2019) -- Start
                        'Isssue#0003774(Sport Pesa): Restriction only applies to desktop application, i.e on self service user can apply for any amount even beyond his/her actual basic salary. there is no validation done.
                        'Decimal.TryParse(txtInstallmentAmt.Text, decAmt)
                        Decimal.TryParse(txtLoanAmt.Text, decAmt)
                        'Hemant (25 Apr 2019) -- End
                        If mdecBaseExRate <> 0 Then
                            decAmt = decAmt * mdecBaseExRate / mdecPaidExRate
                        Else
                            decAmt = decAmt * mdecPaidExRate
                        End If

                        Dim decPrevNetPay As Decimal = 0
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        Dim decPrevActualNetPay As Decimal = 0
                        Dim decOtherLoanEMI As Decimal = 0
                        'Sohail (06 Jul 2018) -- End
                        If CInt(Session("Advance_NetPayTranheadUnkid")) <= 0 Then

                            'Gajanan (23-May-2018) -- Start
                            'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                            decPrevNetPay = CDec(ds.Tables(0).Rows(0)("basicsalary"))
                            'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))
                            'Gajanan (23-May-2018) -- End   
                        Else
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("amountPaidCurrency"))
                            decPrevNetPay = CDec(dsMapped.Tables(0).Rows(0)("amountPaidCurrency"))
                            'Sohail (06 Jul 2018) -- End
                        End If
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Dim decCheckPoint As Decimal = decPrevNetPay - decAmt
                        decPrevActualNetPay = CDec(ds.Tables(0).Rows(0)("basicsalary"))

                        Dim objLoanAdvance As New clsLoan_Advance
                        Dim dsLoanCalc As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                  mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, _
                                                  "Loan", mdtPeriodEnd.AddDays(1), cboEmpName.SelectedValue.ToString(), 0, _
                                                  mdtPeriodStart, True, , , True, True, , True, True, , , , 0, , , , , Nothing)
                        Dim dtTable As DataTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ") AND transactionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows

                            If CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = False Then
                                decOtherLoanEMI += 0
                            ElseIf CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = True Then
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyInterestPaidCurrency").ToString) + CDec(dtRow.Item("TotInterestAmountPaidCurrency").ToString)
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(dtRow.Item("TotPMTAmountPaidCurrency").ToString)
                            End If
                        Next
                        Dim dsLoan As DataSet = objProcesspendingloan.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString(), True, False, "Loan", 0, "lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & CInt(enLoanApplicationStatus.PENDING) & ", " & CInt(enLoanApplicationStatus.APPROVED) & " ) ")
                        For Each dtRow As DataRow In dsLoan.Tables(0).Rows
                            If CBool(dtRow.Item("isloan")) = True Then
                                Dim dtEnd_Date As Date = DateAdd(DateInterval.Month, CInt(dtRow.Item("noofinstallment")), mdtPeriodStart).AddDays(-1)
                                Dim decIntAmt As Decimal
                                Dim decEMI As Decimal
                                Dim decTotIntAmt As Decimal
                                Dim decTotEMI As Decimal
                                objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dtRow.Item("Amount")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart, dtEnd_Date.Date)), CDec(dtRow.Item("interest_rate")), CType(dtRow.Item("loancalctype_id"), enLoanCalcId), CType(dtRow.Item("interest_calctype_id"), enLoanInterestCalcType), CInt(dtRow.Item("noofinstallment")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart.Date, mdtPeriodEnd.Date.AddDays(1))), CDec(dtRow.Item("installmentamt")), decIntAmt, decEMI, decTotIntAmt, decTotEMI)
                                decOtherLoanEMI += decEMI
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("amount"))
                            End If

                        Next
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'Dim decCheckPoint As Decimal = decPrevActualNetPay - decAmt - decOtherLoanEMI
                        Dim decCheckPoint As Decimal = decAmt + decOtherLoanEMI
                        'Sohail (19 Mar 2020) -- End
                        'Sohail (06 Jul 2018) -- End
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'If decPrevNetPay <= 0 OrElse ((decPrevNetPay * CDec(Session("Advance_NetPayPercentage"))) / 100) > decCheckPoint Then
                        If decPrevNetPay <= 0 OrElse ((decPrevNetPay * CDec(Session("Advance_NetPayPercentage"))) / 100) < decCheckPoint Then
                            'Sohail (19 Mar 2020) -- End
                            'Language.setLanguage(mstrModuleName)
                            If CInt(Session("Advance_NetPayTranheadUnkid")) <= 0 Then
                                'Sohail (29 Apr 2019) -- Start
                                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 37, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(CDec(Session("Advance_NetPayPercentage")), "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "of pervious net pay.") & " " & Format(decPrevNetPay, GUI.fmtCurrency), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(CDec(Session("Advance_NetPayPercentage")), "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "of pervious net pay.") & " " & Format(decPrevNetPay, Session("fmtCurrency").ToString), Me)
                                'Sohail (29 Apr 2019) -- End
                            Else
                                'Sohail (29 Apr 2019) -- Start
                                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(CDec(Session("Advance_NetPayPercentage")), "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "of pervious net pay head") & " " & Format(decPrevNetPay, GUI.fmtCurrency), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(CDec(Session("Advance_NetPayPercentage")), "00.00") & "%] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "of pervious net pay head") & " " & Format(decPrevNetPay, Session("fmtCurrency").ToString), Me)
                                'Sohail (29 Apr 2019) -- End
                            End If
                            txtLoanAmt.Focus()
                            Return False
                        End If
                    End If

                    Dim objLA As New clsLoan_Advance
                    If objLA.GetExistLoan(0, CInt(cboEmpName.SelectedValue), False) = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Sorry, you can not apply for Advance: Reason, The Advance is already assigned to you and having (In-progress or On hold) status."), Me)
                        objLA = Nothing
                        Return False
                    End If
                    objLA = Nothing
                End If
                'Sohail (16 May 2018) -- End

                'Gajanan (23-May-2018) -- Start
                'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                If CInt(Session("Advance_DontAllowAfterDays")) > 0 Then
                    If mdtPeriodStart.AddDays(CInt(Session("Advance_DontAllowAfterDays"))) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Sorry, you can not apply for Advance: Reason, Days to Advance Has Been Exceeded."), Me)
                        Return False
                    End If
                End If
                'Gajanan (23-May-2018) -- End

            End If

            Dim objLoanApprover As New clsLoanApprover_master

            Dim dtList As DataTable = objLoanApprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                                                                          CInt(Session("UserId")), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          CInt(cboEmpName.SelectedValue), _
                                                                          CBool(Session("LoanApprover_ForLoanScheme")))

            If dtList.Rows.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Please Assign Approver(s) to this employee."), Me)
                Return False
            End If

            If Session("LoanApprover_ForLoanScheme") IsNot Nothing Then

                dtList = objLoanApprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             CInt(cboEmpName.SelectedValue), _
                                                             CBool(Session("LoanApprover_ForLoanScheme")), _
                                                             CInt(cboLoanScheme.SelectedValue))

                If dtList.Rows.Count <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Please Map this Loan scheme(s) to this employee's Approver(s)."), Me)
                    Return False
                End If
            End If


            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) > objLoanScheme._MaxNoOfInstallment Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, " for ") & objLoanScheme._Name & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, " Scheme."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Installment months cannot be greater than ") & " " & objLoanScheme._MaxNoOfInstallment & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, " Scheme."), Me)
                'Sohail (11 Apr 2018) -- End
                txtEMIInstallments.Focus()
                Return False
            End If
            'Varsha (25 Nov 2017) -- End

            dtList.Rows.Clear()
            dtList = Nothing
            objLoanApprover = Nothing

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub GetValue()
        Try
            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'If Session("ProcessPendingLoanunkid") IsNot Nothing Then
            If mintProcesspendingloanunkid > 0 Then
                'Nilay (06-Aug-2016) -- END
                If objProcesspendingloan._Isloan = True Then
                    radLoan.Checked = True
                    Call radLoan_CheckedChanged(Nothing, Nothing)
                Else
                    radAdvance.Checked = True
                    Call radAdvance_CheckedChanged(Nothing, Nothing)
                End If
                chkExternalEntity.Checked = objProcesspendingloan._Isexternal_Entity
                txtExternalEntity.Text = objProcesspendingloan._External_Entity_Name
            Else
                radLoan.Checked = True
                Call radLoan_CheckedChanged(Nothing, Nothing)
                chkExternalEntity.Checked = True
            End If

            txtApplicationNo.Text = objProcesspendingloan._Application_No
            If Not (objProcesspendingloan._Application_Date = Nothing) Then
                dtpApplicationDate.SetDate = objProcesspendingloan._Application_Date
            End If
            cboEmpName.SelectedValue = CStr(objProcesspendingloan._Employeeunkid)
            cboLoanScheme.SelectedValue = CStr(objProcesspendingloan._Loanschemeunkid)
            txtLoanAmt.Text = Format(CDec(objProcesspendingloan._Loan_Amount), Session("fmtCurrency").ToString)
            cboCurrency.SelectedValue = CStr(objProcesspendingloan._Countryunkid)
            'txtDurationInMths.Text = objProcesspendingloan._DurationInMonths
            cboDeductionPeriod.SelectedValue = CStr(objProcesspendingloan._DeductionPeriodunkid)
            'txtInstallmentAmt.Text = Format(CDec(objProcesspendingloan._InstallmentAmount), Session("fmtCurrency").ToString)
            'txtEMIInstallments.Text = CStr(objProcesspendingloan._NoOfInstallment)
            txtEmployeeRemark.Text = objProcesspendingloan._Emp_Remark

            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            txtLoanAccNumber.Text = objProcesspendingloan._Loan_Account_No
            'Hemant (02 Jan 2019) -- End


            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            cboDeductionPeriod.SelectedValue = CStr(objProcesspendingloan._DeductionPeriodunkid)
            RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtPrincipalAmt.Text = Format(objProcesspendingloan._InstallmentAmount, Session("fmtCurrency").ToString)
            AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            'Sohail (09 Feb 2018) -- Start
            'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            'txtEMIInstallments.Text = CStr(IIf(objProcesspendingloan._NoOfInstallment <= 0, 1, objProcesspendingloan._NoOfInstallment))
            RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
            txtEMIInstallments.Text = CStr(IIf(objProcesspendingloan._NoOfInstallment <= 0, 1, objProcesspendingloan._NoOfInstallment))
            AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
            'Sohail (09 Feb 2018) -- End
            RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            RemoveHandler cboLoanCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
            txtLoanAmt.Text = Format(objProcesspendingloan._Loan_Amount, Session("fmtCurrency").ToString)
            AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            AddHandler cboLoanCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
            'S.SANDEEP [20-SEP-2017] -- START
            Call cboDeductionPeriod_SelectedIndexChanged(cboDeductionPeriod, Nothing)
            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            Call cboLoanScheme_SelectedIndexChanged(cboLoanScheme, Nothing)
            'S.SANDEEP [20-SEP-2017] -- END

            'Sohail (22 Sep 2017) -- Start
            'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
            Call cboCurrency_SelectedIndexChanged(cboCurrency, New System.EventArgs())
            'Sohail (22 Sep 2017) -- End

            objProcesspendingloan._Isvoid = objProcesspendingloan._Isvoid
            objProcesspendingloan._Voiddatetime = objProcesspendingloan._Voiddatetime
            objProcesspendingloan._Voiduserunkid = objProcesspendingloan._Voiduserunkid

            'Varsha (11 Jan 2018) -- Start
            'Issue : In Edit mode Installment amount is not get from database and when we will Change the no. of installment months then it will not modified.
            'Sohail (09 Feb 2018) -- Start
            'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            'txtInstallmentAmt.Text = Format(CDec(objProcesspendingloan._InstallmentAmount), Session("fmtCurrency").ToString)
            'txtEMIInstallments.Text = CStr(objProcesspendingloan._NoOfInstallment)
            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(CDec(objProcesspendingloan._InstallmentAmount), Session("fmtCurrency").ToString)
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)
            'Sohail (09 Feb 2018) -- End
            'Varsha (11 Jan 2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try

            If radLoan.Checked = True Then
                objProcesspendingloan._Isloan = True
            Else
                objProcesspendingloan._Isloan = False
            End If
            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'If Session("ProcessPendingLoanunkid") IsNot Nothing AndAlso CInt(Session("ProcessPendingLoanunkid")) > 0 Then
            '    objProcesspendingloan._Processpendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
            'End If
            If mintProcesspendingloanunkid > 0 Then
                objProcesspendingloan._Processpendingloanunkid = mintProcesspendingloanunkid
            End If
            'Nilay (06-Aug-2016) -- END

            objProcesspendingloan._Application_No = txtApplicationNo.Text
            objProcesspendingloan._Application_Date = dtpApplicationDate.GetDate.Date
            objProcesspendingloan._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objProcesspendingloan._Loan_Amount = CDec(txtLoanAmt.Text)
            objProcesspendingloan._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            objProcesspendingloan._Isexternal_Entity = CBool(chkExternalEntity.Checked)
            objProcesspendingloan._External_Entity_Name = txtExternalEntity.Text
            objProcesspendingloan._Countryunkid = CInt(cboCurrency.SelectedValue)
            objProcesspendingloan._DeductionPeriodunkid = CInt(cboDeductionPeriod.SelectedValue)
            'objProcesspendingloan._DurationInMonths = CInt(txtDurationInMths.Text)


            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            'objProcesspendingloan._InstallmentAmount = mdecInstallmentAmt
            'objProcesspendingloan._NoOfInstallment = CInt(txtEMIInstallments.Text)

            objProcesspendingloan._InstallmentAmount = CDec(txtPrincipalAmt.Text)
            objProcesspendingloan._NoOfInstallment = CInt(txtEMIInstallments.Text)
            'S.SANDEEP [20-SEP-2017] -- END


            objProcesspendingloan._Loan_Statusunkid = 1
            objProcesspendingloan._Remark = txtEmployeeRemark.Text
            objProcesspendingloan._Userunkid = CInt(Session("UserId").ToString)
            objProcesspendingloan._Isvoid = False
            objProcesspendingloan._Voiddatetime = Nothing
            objProcesspendingloan._Voiduserunkid = -1
            objProcesspendingloan._IsLoanApprover_ForLoanScheme = CBool(Session("LoanApprover_ForLoanScheme"))
            'Nilay (05-May-2016) -- Start
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objProcesspendingloan._WebClientIP = Session("IP_ADD").ToString
            objProcesspendingloan._WebFormName = "frmLoanApplication_AddEdit"
            objProcesspendingloan._WebHostName = Session("HOST_NAME").ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objProcesspendingloan._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            'Nilay (05-May-2016) -- End

            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            objProcesspendingloan._Loan_Account_No = txtLoanAccNumber.Text
            'Hemant (02 Jan 2019) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Private Sub SetVisibility()
    '    Try
    '        If ConfigParameter._Object._LoanApplicationNoType = 1 Then
    '            txtApplicationNo.Enabled = False
    '            txtApplicationNo.Text = ConfigParameter._Object._LoanApplicationPrifix & ConfigParameter._Object._NextLoanApplicationNo
    '        Else
    '            txtApplicationNo.Enabled = True
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("SetVisibility:- " & ex.Message, Me)
    '    End Try
    'End Sub


    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, CInt(txtEMIInstallments.Text), Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)
                'Sohail (16 May 2018) -- End
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(txtLoanAmt.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , CDec(txtLoanRate.Text) _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(txtEMIInstallments.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtPrincipalAmt.Text, txtInstallmentAmt.Text)) _
                                                            , decInstrAmount _
                                                            , decInstallmentAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            txtInterestAmt.Text = Format(decTotIntrstAmount, Session("fmtCurrency").ToString)
            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, Session("fmtCurrency").ToString)
            AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtIntAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)
            decNetAmount = decTotInstallmentAmount + decTotIntrstAmount
            txtNetAmount.Text = Format(decNetAmount, Session("fmtCurrency").ToString)
            objLoan_Advance = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (16 May 2018) -- Start
    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
    Private Sub FillPeriod(ByVal blnOnlyFirstPeriod As Boolean)
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Dim mdtTable As DataTable
        Try


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 CInt(Session("Fin_year")), _
                                                 Session("Database_Name").ToString, _
                                                 CDate(Session("fin_startdate")), _
                                                 "List", True, enStatusType.OPEN)

            If blnOnlyFirstPeriod = True Then
                'Sohail (19 Jun 2020) -- Start
                'Ifakara enhancement : 0004734 : Show next open period on salary advance dropdown when system date is of next period.
                'Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN)
                'If intFirstPeriodId > 0 Then
                '    mdtTable = New DataView(dsCombos.Tables("List"), "periodunkid IN (0, " & intFirstPeriodId & " )", "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    mdtTable = New DataView(dsCombos.Tables("List"), "1=2", "", DataViewRowState.CurrentRows).ToTable
                'End If
                'Sohail (15 Jul 2020) -- Start
                'IHI Issue # : Current period not coming in period list on loan application screen.
                'mdtTable = New DataView(dsCombos.Tables("List"), "end_date <= '" & eZeeDate.convertDate(DateTime.Now) & "' ", "", DataViewRowState.CurrentRows).ToTable
                mdtTable = New DataView(dsCombos.Tables("List"), "start_date <= '" & eZeeDate.convertDate(DateTime.Now) & "' ", "", DataViewRowState.CurrentRows).ToTable
                'Sohail (15 Jul 2020) -- End
                'Sohail (19 Jun 2020) -- End
            Else
                mdtTable = New DataView(dsCombos.Tables("List")).ToTable
            End If


            With cboDeductionPeriod
                .DataSource = Nothing
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = mdtTable
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (16 May 2018) -- End

    'Gajanan (23-May-2018) -- Start
    'Issue -When Advances Option Selected, Loan Data Values Was Not Clear.
    Private Sub EnableControl(ByVal status As Boolean)
        Try         'Hemant (13 Aug 2020)
            If status = False Then
                cboLoanScheme.SelectedValue = "0"
                cboLoanScheme.Enabled = False
                txtNetAmount.Text = "0"
                txtLoanRate.Text = "0"
                txtLoanRate.Enabled = False
                txtEMIInstallments.Text = "1"
                txtEMIInstallments.Enabled = False
                txtPrincipalAmt.Text = "0"
                txtPrincipalAmt.Enabled = False
                txtIntAmt.Text = "0"
                txtIntAmt.Enabled = False
                txtInstallmentAmt.Text = "0"
                txtInstallmentAmt.Enabled = False
                txtLoanAmt.Text = "0"
                'Hemant (02 Jan 2019) -- Start
                'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
                txtLoanAccNumber.Text = ""
                txtLoanAccNumber.Enabled = False
                'Hemant (02 Jan 2019) -- End
            ElseIf status = True Then
                cboLoanScheme.Enabled = True

                txtEMIInstallments.Enabled = True
                txtPrincipalAmt.Enabled = True
                txtIntAmt.Enabled = True
                txtInstallmentAmt.Enabled = True
                txtLoanAmt.Text = "0"
                'Hemant (02 Jan 2019) -- Start
                'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
                txtLoanAccNumber.Enabled = True
                'Hemant (02 Jan 2019) -- End
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
    'Gajanan (23-May-2018) -- End

#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'Session("ProcessPendingLoanunkid") = Nothing
            'Nilay (06-Aug-2016) -- END
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim btnFlag As Boolean = False

            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'If Session("ProcessPendingLoanunkid") IsNot Nothing Then
            If mintProcesspendingloanunkid > 0 Then
                'Nilay (06-Aug-2016) -- END

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'btnFlag = objProcesspendingloan.Update()
                btnFlag = objProcesspendingloan.Update(True)
                'Nilay (13-Sept-2016) -- End
            Else
                btnFlag = objProcesspendingloan.Insert(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       CInt(Session("LoanApplicationNoType")), _
                                                       CStr(Session("LoanApplicationPrifix")))
            End If

            If btnFlag = False And objProcesspendingloan._Message <> "" Then
                DisplayMessage.DisplayMessage(objProcesspendingloan._Message, Me)
            Else
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0 'Nilay (08-Dec-2016)
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0 'Nilay (08-Dec-2016)
                End If

                'Nilay (08-Dec-2016) -- Start
                'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                'If radLoan.Checked = True Then
                '    objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                '                                                     CInt(cboLoanScheme.SelectedValue), _
                '                                                     CInt(cboEmpName.SelectedValue), -1, _
                '                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                '                                                     False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                '                                                     Session("ArutiSelfServiceURL").ToString, True, enloginmode, intEmpId, intUserId)
                'Else
                '    objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                '                                                 CInt(cboLoanScheme.SelectedValue), _
                '                                                 CInt(cboEmpName.SelectedValue), -1, _
                '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                '                                                 False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                '                                                 Session("ArutiSelfServiceURL").ToString, True, enloginmode, intEmpId, intUserId)
                'End If

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If mblnIsSendEmailNotification = True Then

                    If radLoan.Checked Then

                        If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then

                            If enLoginMode = enLogin_Mode.MGR_SELF_SERVICE Then

                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, _
                                '                                                 clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                '                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , _
                                '                                                 enLoginMode, 0, intUserId)
                                objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, _
                                                                                 clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), , _
                                                                                 enLoginMode, 0, intUserId)
                                'Sohail (30 Nov 2017) -- End

                                objProcesspendingloan.Send_Notification_Assign(CInt(cboEmpName.SelectedValue), _
                                                                               CInt(Session("Fin_year")), _
                                                                               CStr(Session("ArutiSelfServiceURL")), _
                                                                               objProcesspendingloan._Processpendingloanunkid, _
                                                                               CStr(Session("Database_Name")), _
                                                                               CInt(Session("CompanyUnkId")), _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                               CStr(Session("UserAccessModeSetting")), _
                                                                               CBool(Session("IsIncludeInactiveEmp")), _
                                                                               False, enLoginMode, 0, intUserId, , _
                                                                               Session("Notify_LoanAdvance_Users").ToString)
                                'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString)]	
                            End If
                        Else
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                            '                                                         CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                            '                                                         CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                            '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                            '                                                 False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                            '                                                         CStr(Session("ArutiSelfServiceURL")), True, enLoginMode, _
                            '                                                         intLoginByEmployeeId, intUserId)
                            objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                     CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                                                                                     CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                             False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                                                                                             CStr(Session("ArutiSelfServiceURL")), CInt(Session("CompanyUnkId")), True, enLoginMode, _
                                                                                     intLoginByEmployeeId, intUserId)
                            'Sohail (30 Nov 2017) -- End
                        End If

                    ElseIf radAdvance.Checked Then

                        If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then

                            If enLoginMode = enLogin_Mode.MGR_SELF_SERVICE Then

                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                                '                                                 objProcesspendingloan._Processpendingloanunkid, _
                                '                                                 clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                '                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , _
                                '                                                 enLoginMode, 0, intUserId)
                                objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                                                                                 objProcesspendingloan._Processpendingloanunkid, _
                                                                                 clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), , _
                                                                                 enLoginMode, 0, intUserId)
                                'Sohail (30 Nov 2017) -- End

                                objProcesspendingloan.Send_Notification_Assign(CInt(cboEmpName.SelectedValue), _
                                                                               CInt(Session("Fin_year")), _
                                                                               CStr(Session("ArutiSelfServiceURL")), _
                                                                               objProcesspendingloan._Processpendingloanunkid, _
                                                                               CStr(Session("Database_Name")), _
                                                                               CInt(Session("CompanyUnkId")), _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                               CStr(Session("UserAccessModeSetting")), _
                                                                               CBool(Session("IsIncludeInactiveEmp")), _
                                                                               False, enLoginMode, 0, intUserId, , _
                                                                               Session("Notify_LoanAdvance_Users").ToString)
                                'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString)]	
                            End If
                        Else
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                            '                                                         CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                            '                                                         CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                            '                                             clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                            '                                             False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                            '                                                         CStr(Session("ArutiSelfServiceURL")), True, enLoginMode, intLoginByEmployeeId, intUserId)
                            objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                     CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                                                                                     CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                                                                         clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                         False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                                                                                             CStr(Session("ArutiSelfServiceURL")), CInt(Session("CompanyUnkId")), True, enLoginMode, intLoginByEmployeeId, intUserId)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    End If

                End If
                'Nilay (10-Dec-2016) -- End

                'Nilay (08-Dec-2016) -- End

                'Shani (21-Jul-2016) -- End

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'Session("ProcessPendingLoanunkid") = Nothing
                'Nilay (06-Aug-2016) -- END
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 57
    Protected Sub cnftopup_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnftopup.buttonYes_Click
        Try
            Response.Redirect("~/Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx?ID=" & Server.UrlEncode(b64encode(CStr(CInt(cboEmpName.SelectedValue)) & "|" & CStr(CInt(cboLoanScheme.SelectedValue)))), False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

#End Region

#Region "TextBox Events"

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    'Nilay (05-May-2016) -- Start
    'Protected Sub txtLoanAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
    '    Try
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If txtLoanAmt.Text = Nothing Then Exit Sub
    '        'Nilay (04-Nov-2016) -- End

    '        If CDec(txtLoanAmt.Text) > 0 AndAlso CDec(txtInstallmentAmt.Text) > 0 Then
    '            txtEMIInstallments.Text = CInt(Math.Ceiling(CDec(txtLoanAmt.Text) / CDec(txtInstallmentAmt.Text))).ToString
    '        End If
    '        txtLoanAmt.Text = CDec(txtLoanAmt.Text).ToString("############0.00#")

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("txtLoanAmt_TextChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub
    ''Nilay (05-May-2016) -- End

    'Protected Sub txtInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
    '    Try
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If txtInstallmentAmt.Text = Nothing Then Exit Sub
    '        'Nilay (04-Nov-2016) -- End

    '        If CDec(txtLoanAmt.Text) > 0 AndAlso CDec(txtInstallmentAmt.Text) > 0 Then
    '            txtEMIInstallments.Text = CInt(Math.Ceiling(CDec(txtLoanAmt.Text)) / CDec(txtInstallmentAmt.Text)).ToString
    '        Else
    '            txtEMIInstallments.Text = "0"
    '        End If

    '        txtInstallmentAmt.Text = CDec(txtInstallmentAmt.Text).ToString("#############.00#")
    '        mdecInstallmentAmt = CDec(txtInstallmentAmt.Text)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("txtInstallmentAmt_TextChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub txtEMIInstallments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
    '    Try
    '        'Nilay (04-Nov-2016) -- Start
    '        'Enhancements: Global Change Status feature requested by {Rutta}
    '        If txtEMIInstallments.Text = Nothing Then Exit Sub
    '        'Nilay (04-Nov-2016) -- End

    '        If CDec(txtLoanAmt.Text) > 0 AndAlso CDec(txtEMIInstallments.Text) > 0 Then
    '            'mdecInstallmentAmt = CDec(txtLoanAmt.Text / txtEMIInstallments.Text)
    '            mdecInstallmentAmt = CDec(CDec(txtLoanAmt.Text) / CDec(txtEMIInstallments.Text))
    '            'txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Text / txtEMIInstallments.Text), Session("fmtCurrency"))
    '            txtInstallmentAmt.Text = Format(mdecInstallmentAmt, Session("fmtCurrency").ToString)
    '        Else
    '            mdecInstallmentAmt = CDec(txtLoanAmt.Text)
    '            txtInstallmentAmt.Text = Format(txtLoanAmt.Text, Session("fmtCurrency").ToString)
    '        End If
    '        mdecEMIInstallments = CDec(txtEMIInstallments.Text)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("txtEMIInstallments_TextChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrincipalAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                Dim decLoanAmt, decPrinc As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtPrincipalAmt.Text, decPrinc)
                Dim decValue As Decimal = 0
                decValue = decLoanAmt / decPrinc
                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                txtEMIInstallments.Text = CStr(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                'Sohail (09 Feb 2018) -- End
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtPrincipalAmt_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                'Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)

                If CDbl(txtInstallmentAmt.Text) > 0 Then
                    RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    Dim dblvalue As Double = 0
                    dblvalue = CDbl(txtLoanAmt.Text) / CDbl(txtInstallmentAmt.Text)
                    'S.SANDEEP [28-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : LEFT OVER CHANGES
                    'txtEMIInstallments.Text = Math.Ceiling(dblvalue).ToString()
                    txtEMIInstallments.Text = CStr(IIf(dblvalue <= 0, 1, Math.Ceiling(dblvalue).ToString()))
                    'S.SANDEEP [28-SEP-2017] -- END
                    AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged

                    'S.SANDEEP [28-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : LEFT OVER CHANGES
                    txtInterestAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtPrincipalAmt.Text = Format(CDbl(txtInstallmentAmt.Text), Session("fmtCurrency").ToString)
                    AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    decNetAmount = CDec(txtInstallmentAmt.Text)
                    txtNetAmount.Text = Format(decNetAmount, Session("fmtCurrency").ToString)
                    'S.SANDEEP [28-SEP-2017] -- END

                End If

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then

                If CDbl(txtInstallmentAmt.Text) > 0 Then
                    RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    Dim dblvalue As Double = 0
                    dblvalue = CDbl(txtLoanAmt.Text) / CDbl(txtInstallmentAmt.Text)
                    txtEMIInstallments.Text = CStr(IIf(dblvalue <= 0, 1, Math.Ceiling(dblvalue).ToString()))

                    AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    txtInterestAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtPrincipalAmt.Text = Format(CDbl(txtInstallmentAmt.Text), Session("fmtCurrency").ToString)
                    AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    decNetAmount = CDec(txtInstallmentAmt.Text)
                    txtNetAmount.Text = Format(decNetAmount, Session("fmtCurrency").ToString)
                End If
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Dim decLoanAmt, decPrinc As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtPrincipalAmt.Text, decPrinc)
                Dim decValue As Decimal = 0
                If decPrinc > 0 Then
                    decValue = decLoanAmt / decPrinc
                End If
                txtEMIInstallments.Text = CStr(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                'Sohail (09 Feb 2018) -- End
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)
                'Sohail (09 Feb 2018) -- End

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtLoanAmt_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub txtLoanRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanRate.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtLoanRate_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub txtEMIInstallments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtPrincipalAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtNetAmount.Text = Format(decLoanAmt, Session("fmtCurrency").ToString)
                'Sohail (09 Feb 2018) -- End

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                txtPrincipalAmt.ReadOnly = False
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtPrincipalAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                'Sohail (09 Feb 2018) -- End
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtPrincipalAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtNetAmount.Text = Format(decLoanAmt, Session("fmtCurrency").ToString)
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtEMIInstallments_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

#End Region

#Region "ComboBox Events"

    Protected Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            Dim objPeriod As New clscommom_period_Tran
            objvalPeriodDuration.Visible = True
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)
                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                'Sohail (22 Sep 2017) -- End

                'Language.setLanguage(mstrModuleName)
                objvalPeriodDuration.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
                objvalPeriodDuration.Visible = False
                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                mdtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                'Sohail (22 Sep 2017) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboDeductionPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52,53
    Protected Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            'Hemant (25 Apr 2019) -- Start
            'Isssue#0003774(Sport Pesa):  net pay transhead is not showing on the list. Optional basic salary head not working. when you map other transaction head on "basic salary head (Optional)" (loan setting->configuration) the system gives error during application
            'If CInt(IIf(cboLoanScheme.SelectedValue = "", 0, cboLoanScheme.SelectedValue)) > 0 Then
            'Hemant (25 Apr 2019) -- End
            txtExternalEntity.Text = cboLoanScheme.SelectedItem.Text
            If cboLoanScheme.SelectedValue IsNot Nothing AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                Dim objLScheme As New clsLoan_Scheme
                objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit
                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                txtLoanRate.Text = CStr(objLScheme._InterestRate)
                cboLoanCalcType.SelectedValue = CStr(objLScheme._LoanCalcTypeId)
                cboInterestCalcType.SelectedValue = CStr(objLScheme._InterestCalctypeId)
                Call cboLoanCalcType_SelectedIndexChanged(sender, e)
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdecEMI_NetPayPercentage = objLScheme._EMI_NetPayPercentage
                'Sohail (22 Sep 2017) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                mintTranheadUnkid = objLScheme._TranheadUnkid
                'Sohail (11 Apr 2018) -- End

                objLScheme = Nothing
            Else
                mdecMaxLoanAmountDefined = 0

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                cboLoanCalcType.SelectedValue = "0"
                cboInterestCalcType.SelectedValue = "0"
                cboLoanCalcType.Text = ""
                cboInterestCalcType.Text = ""
                txtInstallmentAmt.Text = "0"
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdecEMI_NetPayPercentage = 0
                'Sohail (22 Sep 2017) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                'Hemant (25 Apr 2019) -- Start
                'Isssue#0003774(Sport Pesa):  net pay transhead is not showing on the list. Optional basic salary head not working. when you map other transaction head on "basic salary head (Optional)" (loan setting->configuration) the system gives error during application
                'mintTranheadUnkid = 0
                mintTranheadUnkid = CInt(Session("Advance_NetPayTranheadUnkid"))
                'Hemant (25 Apr 2019) -- End
                'Sohail (11 Apr 2018) -- End

            End If
            'End If 'Hemant (25 Apr 2019) -- Start/End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged, cboInterestCalcType.SelectedIndexChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) > 0 Then

                Select Case CInt(cboLoanCalcType.SelectedValue)

                    Case enLoanCalcId.Simple_Interest
                        txtEMIInstallments.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Simple_Interest
                        Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)
                        'If mintProcesspendingloanunkid <= 0 Then
                        txtInstallmentAmt.ReadOnly = True
                        If txtLoanRate.Enabled = False Then
                            txtLoanRate.Enabled = True
                        End If
                        'End If
                    Case enLoanCalcId.Reducing_Amount
                        txtEMIInstallments.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Amount
                        Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)
                        'If mintProcesspendingloanunkid <= 0 Then
                        txtInstallmentAmt.ReadOnly = True
                        If txtLoanRate.Enabled = False Then
                            txtLoanRate.Enabled = True
                        End If
                        'End If
                    Case enLoanCalcId.No_Interest
                        cboInterestCalcType.SelectedValue = "0"
                        txtInstallmentAmt.ReadOnly = False
                        'If mintProcesspendingloanunkid <= 0 Then
                        txtPrincipalAmt.Text = "0"
                        'Sohail (09 Feb 2018) -- Start
                        'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                        'txtEMIInstallments.Text = CStr(mintNoOfInstallment)
                        'txtEMIInstallments.Enabled = False
                        'txtInstallmentAmt.Text = Format(mdecInstallmentAmount, Session("fmtCurrency").ToString)
                        If mintProcesspendingloanunkid <= 0 Then
                            txtEMIInstallments.Text = CStr(mintNoOfInstallment)
                            txtEMIInstallments.Enabled = True
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, Session("fmtCurrency").ToString)
                        End If
                        'Sohail (09 Feb 2018) -- End
                        txtLoanRate.Text = "0"
                        txtLoanRate.Enabled = False
                        'End If
                        'Gajanan (25-May-2018) -- Start
                        'Issue -When Amount Enter First And After That Loan Scheme Is Select It Does Not Call Loan Installment Method.
                        'Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)
                        Call txtEMIInstallments_TextChanged(New Object, New EventArgs)
                        'Gajanan (25-May-2018) -- End
                        mintLastCalcTypeId = enLoanCalcId.No_Interest
                    Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                        txtEMIInstallments.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                        txtPrincipalAmt.ReadOnly = False
                        txtEMIInstallments.ReadOnly = False
                        txtLoanRate.ReadOnly = True
                        If mintProcesspendingloanunkid <= 0 Then
                            txtPrincipalAmt.Text = "0"
                        End If
                        Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)
                        txtInstallmentAmt.ReadOnly = True
                        If txtLoanRate.Enabled = False Then
                            txtLoanRate.Enabled = True
                        End If
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    Case enLoanCalcId.No_Interest_With_Mapped_Head
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = "0"
                        txtInstallmentAmt.ReadOnly = False
                        txtPrincipalAmt.Text = "0"
                        If mintProcesspendingloanunkid <= 0 Then
                            txtEMIInstallments.Text = CStr(mintNoOfInstallment)
                            txtEMIInstallments.Enabled = False
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, Session("fmtCurrency").ToString)
                        End If
                        txtLoanRate.Text = "0"
                        txtLoanRate.Enabled = False
                        Call txtEMIInstallments_TextChanged(New Object, New EventArgs)
                        mintLastCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head
                        'Sohail (29 Apr 2019) -- End
                End Select
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboLoanCalcType_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString()) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Sep 2017) -- End

#End Region

#Region "CheckBox Events"
    Protected Sub chkExternalEntity_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExternalEntity.CheckedChanged
        Try
            If chkExternalEntity.Checked = True Then
                txtExternalEntity.Enabled = True
            Else
                txtExternalEntity.Text = ""
                txtExternalEntity.Enabled = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkExternalEntity_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "RadioButtonList Events"

    Protected Sub radLoan_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        Try
            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            'If radLoan.Checked = True Then
            '    cboLoanScheme.Enabled = True
            '    txtDurationInMths.Enabled = True
            '    'nudDurationInMths.Enabled = True
            '    txtInstallmentAmt.Enabled = True
            '    txtEMIInstallments.Enabled = True
            'End If
            If radLoan.Checked Then
                'Gajanan (23-May-2018) -- Start
                'Issue - When Advance Option Selected,Loan Data Was Not Clear.
                'cboLoanScheme.Enabled = True
                EnableControl(True)
                'Gajanan (23-May-2018) -- End

                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                Call FillPeriod(False)
                'Sohail (16 May 2018) -- End
            Else
                If CInt(cboLoanScheme.SelectedValue) > 0 Then
                    cboLoanScheme.SelectedValue = "0"
                    txtEMIInstallments.Text = "1"
                    txtPrincipalAmt.Text = "0"
                    txtLoanAmt.Text = "0"
                End If
                cboLoanScheme.Enabled = False
            End If
            'S.SANDEEP [20-SEP-2017] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radLoan_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub radAdvance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAdvance.CheckedChanged
        Try
            If radAdvance.Checked = True Then

                'Gajanan (23-May-2018) -- Start
                'Issue - When Advances Option Selected,Loan Data Was Not Clear.
                'cboLoanScheme.SelectedValue = "0"
                'cboLoanScheme.Enabled = False
                'txtDurationInMths.Enabled = False
                ''nudDurationInMths.Enabled = False
                'txtInstallmentAmt.Enabled = False
                'txtEMIInstallments.Enabled = False
                ''Nilay (20-Sept-2016) -- Start
                ''Enhancement : Cancel feature for approved but not assigned loan application
                'txtInstallmentAmt.Text = "0"
                'txtEMIInstallments.Text = "0"
                ''Nilay (20-Sept-2016) -- End
                EnableControl(False)
                'Gajanan (23-May-2018) -- End

                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                Call FillPeriod(True)
                'Sohail (16 May 2018) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radAdvance_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            'Hemant (19 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
            'Language.setLanguage(mstrModuleName)
            'Hemant (19 Aug 2019) -- End
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbProcessPendingLoanInfo", Me.lblDetialHeader.Text)
            'Hemant (17 Sep 2020) -- End
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblLoanAdvance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.radAdvance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radAdvance.ID, Me.radAdvance.Text)
            Me.radLoan.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radLoan.ID, Me.radLoan.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmt.ID, Me.lblAmt.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApplicationDate.ID, Me.lblApplicationDate.Text)
            Me.lblApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApplicationNo.ID, Me.lblApplicationNo.Text)
            Me.lblExternalEntity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkExternalEntity.ID, Me.lblExternalEntity.Text)
            Me.lblEmpRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpRemark.ID, Me.lblEmpRemark.Text)
            Me.lblDeductionPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDeductionPeriod.ID, Me.lblDeductionPeriod.Text)
            Me.lblEMIInstallments.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEMIInstallments.ID, Me.lblEMIInstallments.Text)
            Me.lblEMIAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEMIAmount.ID, Me.lblEMIAmount.Text)
            Me.lblDuration.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDuration.ID, Me.lblDuration.Text)
            'Hemant (19 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
            Me.lblLoanAccNumber.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanAccNumber.ID, Me.lblLoanAccNumber.Text)
            Me.lblLoanCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanCalcType.ID, Me.lblLoanCalcType.Text)
            Me.lblInterestCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterestCalcType.ID, Me.lblInterestCalcType.Text)
            Me.lblInterestAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterestAmt.ID, Me.lblInterestAmt.Text)
            Me.lblNetAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNetAmount.ID, Me.lblNetAmount.Text)
            Me.lblLoanInterest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanInterest.ID, Me.lblLoanInterest.Text)
            Me.lblPrincipalAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPrincipalAmt.ID, Me.lblPrincipalAmt.Text)
            Me.lblIntAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIntAmt.ID, Me.lblIntAmt.Text)
            Me.lnProjectedAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnProjectedAmount.ID, Me.lnProjectedAmount.Text)
            Me.elProjectedINSTLAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.elProjectedINSTLAmt.ID, Me.elProjectedINSTLAmt.Text)
            'Hemant (19 Aug 2019) -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage" & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
