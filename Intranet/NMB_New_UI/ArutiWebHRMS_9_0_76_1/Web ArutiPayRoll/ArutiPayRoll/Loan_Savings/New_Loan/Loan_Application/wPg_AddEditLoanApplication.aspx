﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AddEditLoanApplication.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_AddEditLoanApplication"
    Title="Add/Edit Loan Application" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">
    function pageLoad(sender, args) { $("select").searchable(); } </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:Cnf_YesNo ID="cnftopup" runat="server" Title="Aruti" />
                <div class="row clearfix  d--f jc--c ai--c ">
                    <!-- Task Info -->
                    <div class="col-lg-8 col-md-8  col-xs-8 col-sm-8 ">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%--<asp:Label ID="lblDetialHeader" runat="server" Text="Loan Application Info" CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan Application" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplicationNo" runat="server" Text="Application No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApplicationNo" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplicationDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpApplicationDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanCalcType" runat="server" Text="Loan Calc. Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanCalcType" runat="server" Enabled="False" AutoPostBack="true"
                                                data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpName" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInterestCalcType" runat="server" Text="Int. Calc. Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true" Enabled="False"
                                                data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanAdvance" runat="server" Text="Select" CssClass="form-label"></asp:Label>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                            <asp:RadioButton ID="radLoan" runat="server" Text="Loan" GroupName="LoanAdvance"
                                                AutoPostBack="true" Checked="true" CssClass="with-gap" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <asp:RadioButton ID="radAdvance" runat="server" Text="Advance" GroupName="LoanAdvance"
                                                AutoPostBack="true" CssClass="with-gap" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduction Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmt" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                            <div class="form-group m-t-0">
                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <div class="form-group m-t-0">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtLoanAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanAccNumber" runat="server" Text="Loan Account Number." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLoanAccNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpRemark" runat="server" Text="Employee Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployeeRemark" TextMode="MultiLine" Rows="6" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lnProjectedAmount" runat="server" Text="Projected Amount" Font-Bold="true"
                                            CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInterestAmt" runat="server" Text="Interest Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtInterestAmt" runat="server" ReadOnly="true" Style="text-align: right"
                                                    Width="99%" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNetAmount" runat="server" Text="Net Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtNetAmount" runat="server" ReadOnly="true" Style="text-align: right"
                                                    Width="99%" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="elProjectedINSTLAmt" runat="server" Text="Projected Installment Amount (First Installment)"
                                            Font-Bold="true" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanInterest" runat="server" Text="Rate (%)" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLoanRate" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEMIInstallments" runat="server" Text="INSTL(In Months)" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEMIInstallments" AutoPostBack="true" Text="1" Style="text-align: right"
                                                    runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPrincipalAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIntAmt" runat="server" Text="Interest Amt." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIntAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEMIAmount" runat="server" Text="INSTL Amt." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExternalEntity" runat="server" Text="Is External Entity" Visible="False"
                                            CssClass="form-label"></asp:Label>
                                        <asp:CheckBox ID="chkExternalEntity" runat="server" Checked="True" AutoPostBack="true"
                                            CssClass="filled-in" Visible="False" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlExternalEntity" runat="server" Visible="False">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtExternalEntity" runat="server" Visible="False" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlDuraction" runat="server" Visible="False">
                                            <asp:Label ID="lblDuration" runat="server" Text="Duration In Months" Visible="false"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDurationInMths" Visible="false" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right; background-color: White" Text="0" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlEMIAmount1" runat="server" Visible="False">
                                            <asp:Label ID="lblEMIAmount1" runat="server" Text="Installment Amt." Visible="false"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtInstallmentAmt1" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        AutoPostBack="true" Style="text-align: right" Text="0.0" Visible="false" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlEMIInstallments1" runat="server" Visible="False">
                                            <asp:Label ID="lblEMIInstallments1" runat="server" Text="No. of Installments" Visible="false"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtEMIInstallments1" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        AutoPostBack="true" Style="text-align: right; background-color: White" Text="0"
                                                        Visible="false" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="btn-group m-l-0" style="float: left">
                                    <asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" Style="display: block;"
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="objlblExRate" runat="server" Style="float: left;" Text="" CssClass="form-label"></asp:Label>
                                </div>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
