﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LoanApproverMigration.aspx.vb"
    Inherits="Loan_Savings_New_Loan_wPg_LoanApproverMigration" Title="Loan Approver Migration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <script type="text/javascript">
     $(document).ready(function()
		{
		    RetriveTab();
            
        });
      function  RetriveTab () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "MigratedEmployee";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });        
       }
    </script>

    <asp:Panel ID="pnlmain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Approver Information" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix  d--f ai--c jc--c">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 as--fs">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOldApprover" runat="server" Text="From Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboOldApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOldLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboOldLevel" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtOldSearchEmployee" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-15">
                                                        <div style="float: left;">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations">
                                                                <i class="fas fa-sliders-h"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                        <div style="float: right;">
                                                            <asp:LinkButton ID="lnkReset" runat="server" ToolTip="Reset">
                                                                 <i class="fas fa-redo-alt"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 400px;">
                                                            <asp:DataGrid ID="dgOldApproverEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered">
                                                                <ItemStyle />
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkOldAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkOldAll_CheckedChanged"
                                                                                Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvOldSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvOldSelect_CheckedChanged"
                                                                                Text=" " />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="true"
                                                                        FooterText="colhdgEmployeecode"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgEmployee">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                </Columns>
                                                                <HeaderStyle Font-Bold="False" />
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12  text-center">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Button ID="objbtnAssign" runat="server" Text=">>" CssClass="btn btn-primary" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12  as--fs">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblNewApprover" runat="server" Text="To Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboNewApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblNewLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboNewLevel" runat="server" AutoPostBack="True" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 450px;">
                                                            <ul class="nav nav-tabs" role="tablist" id="Tabs">
                                                                <li role="presentation" class="active"><a href="#MigratedEmployee" data-toggle="tab">
                                                                    Migrated Employee </a></li>
                                                                <li role="presentation"><a href="#AssignedEmployee" data-toggle="tab">Assigned Employee
                                                                </a></li>
                                                            </ul>
                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane fade in active" id="MigratedEmployee">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtNewApproverMigratedEmp" runat="server" CssClass="form-control"
                                                                                AutoPostBack="True"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table-responsive" style="max-height: 450px;">
                                                                        <asp:DataGrid ID="dgNewApproverMigratedEmp" runat="server" AutoGenerateColumns="False"
                                                                            CssClass="table table-hover table-bordered" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                            Width="99%">
                                                                            <ItemStyle />
                                                                            <Columns>
                                                                                <asp:TemplateColumn>
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkNewAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkNewAll_CheckedChanged"
                                                                                            Text=" " />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvNewSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvNewSelect_CheckedChanged"
                                                                                            Text=" " />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="employeecode" FooterText="colhdgMigratedEmpCode" HeaderText="Employee Code"
                                                                                    ReadOnly="True">
                                                                                    <HeaderStyle Width="30%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeename" FooterText="colhdgMigratedEmp" HeaderText="Employee"
                                                                                    ReadOnly="True">
                                                                                    <HeaderStyle Width="70%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="True"
                                                                                    Visible="False" />
                                                                            </Columns>
                                                                            <HeaderStyle Font-Bold="False" />
                                                                        </asp:DataGrid>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane fade" id="AssignedEmployee">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtNewApproverAssignEmp" runat="server" CssClass="form-control"
                                                                                        AutoPostBack="True"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 450px;">
                                                                                <asp:DataGrid ID="dgNewApproverAssignEmp" runat="server" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered">
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="True"
                                                                                            FooterText="colhdgAssignEmpCode">
                                                                                            <HeaderStyle Width="30%" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhdgAssignEmployee">
                                                                                            <HeaderStyle Width="70%" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="True"
                                                                                            Visible="False" />
                                                                                    </Columns>
                                                                                    <HeaderStyle Font-Bold="False" />
                                                                                    <ItemStyle />
                                                                                </asp:DataGrid>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
