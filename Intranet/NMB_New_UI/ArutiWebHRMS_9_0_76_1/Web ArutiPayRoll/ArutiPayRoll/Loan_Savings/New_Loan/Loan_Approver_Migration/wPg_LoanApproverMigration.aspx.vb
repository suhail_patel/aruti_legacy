﻿Option Strict On

#Region "Import"

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Loan_Savings_New_Loan_wPg_LoanApproverMigration
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmLoanApprover_Migration"
    Dim DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = String.Empty
    Private dtOldApproverEmp As DataTable = Nothing
    Private dtNewApproverAssignEmp As DataTable = Nothing
    Private dtNewApproverMigratedEmp As DataTable = Nothing
    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Private objlnApprover As New clsLoanApprover_master
    'Private objlnApproverLevel As New clslnapproverlevel_master
    'Hemant (04 Sep 2020) -- End   
    Dim dtFromApprover As DataTable = Nothing
    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    'Private mintNewApproverunkid As Integer = -1
    'Private mintOldApproverunkid As Integer = -1
    Private mintOldApproverEmpunkid As Integer = -1
    Private mintNewApproverEmpunkid As Integer = -1
    Private mblnIsExternalApprover As Boolean = False
    Private dtToApprover As DataTable = Nothing
    'Nilay (01-Mar-2016) -- End

#End Region

#Region "Page's Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Hemant (04 Sep 2020) -- Start
                'Issue : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call SetLanguage()
                Call FillCombo()

            Else
                dtFromApprover = CType(Me.ViewState("dtFromApprover"), DataTable)
                dtOldApproverEmp = CType(Me.ViewState("dvOldApproverEmp"), DataTable)
                dtNewApproverAssignEmp = CType(Me.ViewState("dtNewApproverAssignEmp"), DataTable)
                dtNewApproverMigratedEmp = CType(Me.ViewState("dtNewApproverMigratedEmp"), DataTable)
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'mintNewApproverunkid = CInt(Me.ViewState("mintNewApproverunkid"))
                mintOldApproverEmpunkid = CInt(Me.ViewState("OldApproverEmpunkid"))
                mintNewApproverEmpunkid = CInt(Me.ViewState("NewApproverEmpunkid"))
                mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))
                dtToApprover = CType(Me.ViewState("dtToApprover"), DataTable)
                'Nilay (01-Mar-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dtFromApprover") = dtFromApprover
            Me.ViewState("dvOldApproverEmp") = dtOldApproverEmp
            Me.ViewState("dtNewApproverAssignEmp") = dtNewApproverAssignEmp
            Me.ViewState("dtNewApproverMigratedEmp") = dtNewApproverMigratedEmp
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Me.ViewState("mintNewApproverunkid") = mintNewApproverunkid
            Me.ViewState("OldApproverEmpunkid") = mintOldApproverEmpunkid
            Me.ViewState("NewApproverEmpunkid") = mintNewApproverEmpunkid
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover
            Me.ViewState("dtToApprover") = dtToApprover
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        'Hemant (04 Sep 2020) -- Start
        'Issue : Application Performance issue
        Dim objlnApprover As New clsLoanApprover_master
        Dim objlnApproverLevel As New clslnapproverlevel_master
        'Hemant (04 Sep 2020) -- End

        Dim dsList As DataSet = Nothing
        Dim dsFill As DataSet = Nothing
        Try

            dsList = objlnApprover.getListForCombo("List", True, True)

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'dtFromApprover = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name")
            dtFromApprover = dsList.Tables("List")
            'Nilay (01-Mar-2016) -- End

            With cboOldApprover
                .DataTextField = "name"
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                '.DataValueField = "lnempapproverunkid"
                .DataValueField = "lnapproverunkid"
                'Nilay (01-Mar-2016) -- End

                .DataSource = dtFromApprover
                .DataBind()
            End With
            Call cboOldApprover_SelectedIndexChanged(cboOldApprover, Nothing)

            dsFill = objlnApproverLevel.getListForCombo("List", True)
            With cboOldLevel
                .DataTextField = "name"
                .DataValueField = "lnlevelunkid"
                .DataSource = dsFill.Tables(0)
                .DataBind()
            End With

            With cboNewLevel
                .DataTextField = "name"
                .DataValueField = "lnlevelunkid"
                .DataSource = dsFill.Tables(0).Copy
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Issue : Application Performance issue
        Finally
            objlnApprover = Nothing
            objlnApproverLevel = Nothing
            If IsNothing(dsFill) = False Then
                dsFill.Clear()
                dsFill = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    'Private Sub GetLoanApproverLevel(ByVal sender As Object, ByVal intApproverEmpID As Integer)
    Private Sub GetLoanApproverLevel(ByVal sender As Object, ByVal intApproverEmpID As Integer, Optional ByVal blnExtarnalApprover As Boolean = False)
        'Nilay (01-Mar-2016) -- End
        'Hemant (04 Sep 2020) -- Start
        'Issue : Application Performance issue
        Dim objlnApprover As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End

        Dim dsList As DataSet = Nothing
        Try

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'dsList = objlnApprover.GetLevelFromLoanApprover(True, intApproverEmpID)
            dsList = objlnApprover.GetLevelFromLoanApprover(intApproverEmpID, blnExtarnalApprover, True)
            'Nilay (01-Mar-2016) -- End

            If CType(sender, DropDownList).ID.ToUpper = "CBOOLDAPPROVER" Then
                With cboOldLevel
                    .DataValueField = "lnlevelunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedIndex = 0
                End With

            ElseIf CType(sender, DropDownList).ID.ToUpper = "CBONEWAPPROVER" Then
                With cboNewLevel
                    .DataValueField = "lnlevelunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedIndex = 0
                End With
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetLoanApproverLevel:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Issue : Application Performance issue
        Finally
            objlnApprover = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub FillOldApproverEmployeeList()
        Try
            Dim strSearch As String = ""
            Dim dvFromEmp As New DataView

            If dtOldApproverEmp Is Nothing Then Exit Sub

            dvFromEmp = dtOldApproverEmp.DefaultView
            If mstrAdvanceFilter.Trim.Length > 0 Then
                dvFromEmp.RowFilter = mstrAdvanceFilter.Trim.Replace("ADF.", "")
                dvFromEmp.Table.AcceptChanges()
                dtOldApproverEmp = dvFromEmp.Table
            End If

            dgOldApproverEmp.AutoGenerateColumns = False
            dgOldApproverEmp.DataSource = dtOldApproverEmp
            dgOldApproverEmp.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillOldApproverEmployeeList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillNewApproverAssignEmployeeList()
        Try
            If dtNewApproverAssignEmp Is Nothing Then Exit Sub

            dgNewApproverAssignEmp.AutoGenerateColumns = False
            dgNewApproverAssignEmp.DataSource = dtNewApproverAssignEmp
            dgNewApproverAssignEmp.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillNewApproverAssignEmployeeList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillNewApproverMigratedEmployeeList()
        Try
            If dtNewApproverMigratedEmp Is Nothing Then Exit Sub

            dgNewApproverMigratedEmp.AutoGenerateColumns = False
            dgNewApproverMigratedEmp.DataSource = dtNewApproverMigratedEmp
            dgNewApproverMigratedEmp.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillNewApproverMigratedEmployeeList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillOldApproverEmployeeList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub objbtnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            If dtOldApproverEmp Is Nothing Then Exit Sub

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
                cboNewApprover.Focus()
                Exit Sub
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboNewLevel.Focus()
                Exit Sub
            End If

            Dim drRow() As DataRow = dtOldApproverEmp.Select("select=true")
            If drRow.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please check atleast one employee to migrate."), Me)
                Exit Sub
            End If


            Dim blnFlag As Boolean = False

            If dgOldApproverEmp.Items.Count <= 0 Then Exit Sub

            If drRow.Length > 0 Then
                Dim dtGetExistData As DataTable = Nothing

                For i As Integer = 0 To drRow.Length - 1

                    If dtNewApproverAssignEmp IsNot Nothing AndAlso dtNewApproverAssignEmp.Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtNewApproverAssignEmp.Select("employeeunkid=" & drRow(i)("employeeunkid").ToString())
                        If dRow.Length > 0 Then
                            blnFlag = True
                            If dtGetExistData Is Nothing Then
                                dtGetExistData = dtNewApproverAssignEmp.Clone()
                            End If
                            dtGetExistData.ImportRow(drRow(i))
                            Continue For
                        End If
                    End If

                    drRow(i)("select") = False
                    If CInt(drRow(i)("employeeunkid")) = CInt(cboNewApprover.SelectedValue) Then Continue For

                    If dtNewApproverMigratedEmp Is Nothing Then
                        dtNewApproverMigratedEmp = dtOldApproverEmp.Clone
                        dtNewApproverMigratedEmp.ImportRow(drRow(i))
                    Else
                        dtNewApproverMigratedEmp.ImportRow(drRow(i))
                    End If
                    dtOldApproverEmp.Rows.Remove(drRow(i))

                Next
                dtOldApproverEmp.AcceptChanges()
                If blnFlag Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Some of the checked employee is already binded with the selected approver."), Me)
                End If

            End If

            Call FillNewApproverMigratedEmployeeList()
            Call FillOldApproverEmployeeList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnAssign_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub objbtnUnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try
            If dtNewApproverMigratedEmp Is Nothing Then Exit Sub

            Dim drRow() As DataRow = dtNewApproverMigratedEmp.Select("select=true")
            If drRow.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please check atleast one employee to unassign."), Me)
                Exit Sub
            End If

            If dtNewApproverMigratedEmp.Rows.Count <= 0 Then Exit Sub

            If drRow.Length > 0 Then
                lnkReset_Click(lnkReset, Nothing)
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("select") = False

                    If dtOldApproverEmp Is Nothing Then
                        dtOldApproverEmp = dtNewApproverMigratedEmp.Clone()
                        dtOldApproverEmp.ImportRow(drRow(i))
                    Else
                        dtOldApproverEmp.ImportRow(drRow(i))
                    End If
                    dtNewApproverMigratedEmp.Rows.Remove(drRow(i))
                Next
                dtOldApproverEmp.AcceptChanges()
                dtNewApproverMigratedEmp.AcceptChanges()
                Call FillOldApproverEmployeeList()
                Call FillNewApproverMigratedEmployeeList()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnUnAssign_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Hemant (04 Sep 2020) -- Start
        'Issue : Application Performance issue
        Dim objApproverTran As New clsLoanApprover_tran
        'Hemant (04 Sep 2020) -- End
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Please Select From Approver to migrate employee(s)."), Me)
                cboOldApprover.Focus()
                Exit Sub

            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboOldLevel.Focus()
                Exit Sub

            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
                cboNewApprover.Focus()
                Exit Sub

            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboNewLevel.Focus()
                Exit Sub

            ElseIf dgNewApproverMigratedEmp.Items.Count = 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "There is no employee(s) for migration."), Me)
                Exit Sub

            End If

            'Hemant (04 Sep 2020) -- Start
            'Issue : Application Performance issue
            'Dim objApproverTran As New clsLoanApprover_tran
            'Hemant (04 Sep 2020) -- End

            objApproverTran._UserId = CInt(Session("UserId"))

            'Nilay (05-May-2016) -- Start
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objApproverTran._WebClientIP = Session("IP_ADD").ToString
            objApproverTran._WebFormName = "frmLoanApprover_Migration"
            objApproverTran._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If objApproverTran.Approver_Migration(dtNewApproverMigratedEmp, mintNewApproverunkid, CInt(cboNewApprover.SelectedValue)) = False Then
            If objApproverTran.Approver_Migration(dtNewApproverMigratedEmp, CInt(cboNewApprover.SelectedValue), mintNewApproverEmpunkid) = False Then
                'Nilay (01-Mar-2016) -- End
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Problem in Approver Migration."), Me)
            Else
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Approver Migration done successfully."), Me)
                cboOldApprover.SelectedIndex = 0
                cboOldLevel.SelectedIndex = 0
                cboNewApprover.SelectedValue = "0"
                cboNewLevel.SelectedValue = "0"
                dgOldApproverEmp.DataSource = New List(Of String)
                dgOldApproverEmp.DataBind()
                dgNewApproverMigratedEmp.DataSource = New List(Of String)
                dgNewApproverMigratedEmp.DataBind()
                'Hemant (04 Sep 2020) -- Start
                'Issue : Application Performance issue
                dtNewApproverMigratedEmp = Nothing
                'Hemant (04 Sep 2020) -- End
                dgNewApproverAssignEmp.DataSource = New List(Of String)
                dgNewApproverAssignEmp.DataBind()
                cboOldApprover.Focus()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Issue : Application Performance issue
        Finally
            objApproverTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub cboOldApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dtToApprover As DataTable = Nothing
            'Nilay (01-Mar-2016) -- End
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                dgOldApproverEmp.DataSource = New List(Of String)
                dgOldApproverEmp.DataBind()
                cboOldLevel.SelectedIndex = 0
                cboNewLevel.SelectedIndex = 0
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dtToApprover = New DataView(dtFromApprover, "", "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name")
                dtToApprover = New DataView(dtFromApprover, "", "", DataViewRowState.CurrentRows).ToTable()
                mblnIsExternalApprover = False
                'Nilay (01-Mar-2016) -- End
            Else
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dtToApprover = New DataView(dtFromApprover, "lnempapproverunkid <> " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name")
                Dim dRow As DataRow() = dtFromApprover.Select("lnapproverunkid = " & CInt(cboOldApprover.SelectedValue))
                If dRow.Length > 0 Then
                    mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                    mintOldApproverEmpunkid = CInt(dRow(0).Item("lnempapproverunkid"))
                End If
                dtToApprover = New DataView(dtFromApprover, "lnapproverunkid NOT IN(" & CInt(cboOldApprover.SelectedValue) & ") ", "", DataViewRowState.CurrentRows).ToTable()
                'Nilay (01-Mar-2016) -- End
            End If

            With cboNewApprover
                .DataTextField = "name"
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                '.DataValueField = "lnempapproverunkid"
                .DataValueField = "lnapproverunkid"
                'Nilay (01-Mar-2016) -- End
                .DataSource = dtToApprover
                .DataBind()
            End With

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'GetLoanApproverLevel(sender, CInt(cboOldApprover.SelectedValue))
            GetLoanApproverLevel(sender, mintOldApproverEmpunkid, mblnIsExternalApprover)
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboOldApprover_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            cboNewApprover.SelectedIndex = 0
            cboNewLevel.SelectedIndex = 0
            dgOldApproverEmp.DataSource = New List(Of String)
            dgOldApproverEmp.DataBind()
            dgNewApproverMigratedEmp.DataSource = New List(Of String)
            dgNewApproverMigratedEmp.DataBind()
            dgNewApproverAssignEmp.DataSource = New List(Of String)
            dgNewApproverAssignEmp.DataBind()
        End Try
    End Sub

    Protected Sub cboNewApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboNewApprover.SelectedValue) <= 0 Then
            '    cboNewLevel.SelectedIndex = 0
            'End If
            'If CInt(cboNewApprover.SelectedValue) <= 0 Then
            '    cboNewLevel.SelectedIndex = 0
            '    dgNewApproverAssignEmp.DataSource = Nothing
            '    dgNewApproverAssignEmp.DataBind()
            '    dgNewApproverMigratedEmp.DataSource = Nothing
            '    dgNewApproverMigratedEmp.DataBind()
            'End If
            'GetLoanApproverLevel(sender, CInt(cboNewApprover.SelectedValue))
            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                cboNewLevel.SelectedIndex = 0
                dgNewApproverAssignEmp.DataSource = Nothing
                dgNewApproverAssignEmp.DataBind()
                dgNewApproverMigratedEmp.DataSource = Nothing
                dgNewApproverMigratedEmp.DataBind()
                mblnIsExternalApprover = False
            Else
                Dim dRow As DataRow() = dtToApprover.Select("lnapproverunkid = " & CInt(cboNewApprover.SelectedValue))
                If dRow.Length > 0 Then
                    mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                    mintNewApproverEmpunkid = CInt(dRow(0).Item("lnempapproverunkid"))
                End If
            End If
            GetLoanApproverLevel(sender, mintNewApproverEmpunkid, mblnIsExternalApprover)
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboNewApprover_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboOldLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        'Hemant (04 Sep 2020) -- Start
        'Issue : Application Performance issue
        Dim objlnApprover As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End
        Dim dsOldApproverEmp As DataSet = Nothing
        Try

            If CInt(cboOldLevel.SelectedValue) <= 0 Then
                cboOldLevel.SelectedIndex = 0
                dgOldApproverEmp.DataSource = New List(Of String)
                dgOldApproverEmp.DataBind()
            Else
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dsOldApproverEmp = objlnApprover.GetEmployeeFromLoanApprover(CStr(Session("Database_Name")), _
                '                                                             CInt(Session("UserId")), _
                '                                                             CInt(Session("Fin_year")), _
                '                                                             CInt(Session("CompanyUnkId")), _
                '                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                             CStr(Session("UserAccessModeSetting")), _
                '                                                             True, _
                '                                                             CBool(Session("IsIncludeInactiveEmp")), _
                '                                                             "List", _
                '                                                             CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue))

                dsOldApproverEmp = objlnApprover.GetEmployeeFromLoanApprover(CStr(Session("Database_Name")), _
                                                                             CInt(Session("UserId")), _
                                                                             CInt(Session("Fin_year")), _
                                                                             CInt(Session("CompanyUnkId")), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             CStr(Session("UserAccessModeSetting")), _
                                                                             True, _
                                                                             CBool(Session("IsIncludeInactiveEmp")), _
                                                                             "List", _
                                                                             mintOldApproverEmpunkid, CInt(cboOldLevel.SelectedValue), _
                                                                             mblnIsExternalApprover)
                'Nilay (01-Mar-2016) -- End

                dtOldApproverEmp = dsOldApproverEmp.Tables(0)

                Call FillOldApproverEmployeeList()
            End If

            If dsOldApproverEmp IsNot Nothing Then
                If dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'mintOldApproverunkid = CInt(dsOldApproverEmp.Tables(0).Rows(0)("lnapproverunkid"))
                    'Nilay (01-Mar-2016) -- End
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboOldLevel_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Issue : Application Performance issue
        Finally
            objlnApprover = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub cboNewLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        'Hemant (04 Sep 2020) -- Start
        'Issue : Application Performance issue
        Dim objlnApprover As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End
        Dim dsNewApproverAssignEmp As DataSet = Nothing
        Try

            If CInt(cboNewLevel.SelectedValue) <= 0 Then
                cboNewLevel.SelectedIndex = 0
            Else
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dsNewApproverAssignEmp = objlnApprover.GetEmployeeFromLoanApprover(CStr(Session("Database_Name")), _
                '                                                                   CInt(Session("UserId")), _
                '                                                                   CInt(Session("Fin_year")), _
                '                                                                   CInt(Session("CompanyUnkId")), _
                '                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                                   CStr(Session("UserAccessModeSetting")), _
                '                                                                   True, _
                '                                                                   CBool(Session("IsIncludeInactiveEmp")), _
                '                                                                   "List", _
                '                                                                   CInt(cboNewApprover.SelectedValue), _
                '                                                                   CInt(cboNewLevel.SelectedValue))

                dsNewApproverAssignEmp = objlnApprover.GetEmployeeFromLoanApprover(CStr(Session("Database_Name")), _
                                                                                   CInt(Session("UserId")), _
                                                                                   CInt(Session("Fin_year")), _
                                                                                   CInt(Session("CompanyUnkId")), _
                                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                   CStr(Session("UserAccessModeSetting")), _
                                                                                   True, _
                                                                                   CBool(Session("IsIncludeInactiveEmp")), _
                                                                                   "List", _
                                                                                   mintNewApproverEmpunkid, _
                                                                                   CInt(cboNewLevel.SelectedValue), _
                                                                                   mblnIsExternalApprover)
                'Nilay (01-Mar-2016) -- End

                dtNewApproverAssignEmp = dsNewApproverAssignEmp.Tables(0)

                Call FillNewApproverAssignEmployeeList()
            End If

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                If dtNewApproverMigratedEmp IsNot Nothing Then
                    dtNewApproverMigratedEmp.Rows.Clear()
                End If
            End If

            If dsNewApproverAssignEmp IsNot Nothing Then
                If dsNewApproverAssignEmp.Tables(0).Rows.Count > 0 Then
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'mintNewApproverunkid = CInt(dsNewApproverAssignEmp.Tables(0).Rows(0)("lnapproverunkid"))
                    'Nilay (01-Mar-2016) -- End
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboNewLevel_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Issue : Application Performance issue
        Finally
            objlnApprover = Nothing
            If IsNothing(dsNewApproverAssignEmp) = False Then
                dsNewApproverAssignEmp.Clear()
                dsNewApproverAssignEmp = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Protected Sub txtOldSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOldSearchEmployee.TextChanged
        Try
            If dtOldApproverEmp IsNot Nothing Then
                Dim dvOldApproverEmp As DataView = dtOldApproverEmp.DefaultView
                If dvOldApproverEmp.Table.Rows.Count > 0 Then
                    If txtOldSearchEmployee.Text.Trim.Length > 0 Then
                        dvOldApproverEmp.RowFilter = "employeecode like '%" & txtOldSearchEmployee.Text.Trim & "%' OR employeename like '%" & txtOldSearchEmployee.Text.Trim & "%' "
                    End If
                    If mstrAdvanceFilter.Trim.Length > 0 Then
                        dvOldApproverEmp.RowFilter &= IIf(dvOldApproverEmp.RowFilter.Trim.Length > 0, " AND ", "").ToString & mstrAdvanceFilter
                    End If
                    dgOldApproverEmp.DataSource = dvOldApproverEmp.ToTable
                    dgOldApproverEmp.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtOldSearchEmployee_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtNewApproverAssignEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewApproverAssignEmp.TextChanged
        Try
            If dtNewApproverAssignEmp IsNot Nothing Then
                Dim dvNewAssignEmp As DataView = dtNewApproverAssignEmp.DefaultView
                If dvNewAssignEmp.Table.Rows.Count > 0 Then
                    dvNewAssignEmp.RowFilter = "employeecode like '%" & txtNewApproverAssignEmp.Text.Trim & "%' or employeename like '%" & txtNewApproverAssignEmp.Text.Trim & "%' "
                    dgNewApproverAssignEmp.DataSource = dvNewAssignEmp.ToTable
                    dgNewApproverAssignEmp.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtNewApproverAssignEmp_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtNewApproverMigratedEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewApproverMigratedEmp.TextChanged
        Try
            If dtNewApproverMigratedEmp IsNot Nothing Then
                Dim dvNewMigratedEmp As DataView = dtNewApproverMigratedEmp.DefaultView
                If dvNewMigratedEmp.Table.Rows.Count > 0 Then
                    dvNewMigratedEmp.RowFilter = "employeecode like '%" & txtNewApproverMigratedEmp.Text.Trim & "%' or employeename like '%" & txtNewApproverMigratedEmp.Text.Trim & "%'"
                    dgNewApproverMigratedEmp.DataSource = dvNewMigratedEmp.ToTable
                    dgNewApproverMigratedEmp.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtNewApproverMigratedEmp_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Protected Sub ChkOldAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)

            Dim dvEmployee As DataView = dtOldApproverEmp.DefaultView
            If mstrAdvanceFilter.Trim.Length > 0 Then
                dvEmployee.RowFilter = mstrAdvanceFilter.Trim
            Else
                dvEmployee.RowFilter = ""
            End If

            'Nilay (23-Aug-2016) -- Start
            'ENHANCEMENT : Search option on payslip in MSS
            dvEmployee.RowFilter = "employeecode like '%" & txtOldSearchEmployee.Text.Trim & "%' OR employeename like '%" & txtOldSearchEmployee.Text.Trim & "%' "
            'Nilay (23-Aug-2016) -- END

            For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
                Dim gvRow As DataGridItem = dgOldApproverEmp.Items(i)
                CType(gvRow.FindControl("ChkgvOldSelect"), CheckBox).Checked = cb.Checked
                Dim dRow() As DataRow = dtOldApproverEmp.Select("employeeunkid = '" & gvRow.Cells(3).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("select") = cb.Checked
                End If
                dvEmployee.Table.AcceptChanges()
            Next
            dtOldApproverEmp = dvEmployee.Table
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkOldAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkgvOldSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim drRow() As DataRow = dtOldApproverEmp.Select("employeeunkid = " & item.Cells(3).Text)
            If drRow.Length > 0 Then
                drRow(0).Item("select") = chkBox.Checked
            End If
            dtOldApproverEmp.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkgvOldSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkNewAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)

            If dgNewApproverMigratedEmp.Items.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgNewApproverMigratedEmp.Items
                CType(item.Cells(0).FindControl("ChkgvNewSelect"), CheckBox).Checked = chkBox.Checked
                Dim drRow() As DataRow = dtNewApproverMigratedEmp.Select("employeeunkid=" & item.Cells(3).Text)
                If drRow.Length > 0 Then
                    drRow(0).Item("select") = chkBox.Checked
                End If
            Next
            dtNewApproverMigratedEmp.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkNewAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkgvNewSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim drRow() As DataRow = dtNewApproverMigratedEmp.Select("employeeunkid = " & item.Cells(3).Text)
            If drRow.Length > 0 Then
                drRow(0).Item("select") = chkBox.Checked
            End If
            dtNewApproverMigratedEmp.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkgvNewSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "From Approver is compulsory information.Please select From Approver."), Me)
                cboOldApprover.Focus()
                Exit Sub
            End If
            If CInt(cboOldLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Level is compulsory information.Please select Level."), Me)
                cboOldLevel.Focus()
                Exit Sub
            End If

            If dtOldApproverEmp IsNot Nothing Then

                Dim dRow() As DataRow = dtOldApproverEmp.Select("select = True")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("select") = False
                        dRow(i).AcceptChanges()
                    Next
                End If

            End If

            popupAdvanceFilter.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            mstrAdvanceFilter = ""
            txtOldSearchEmployee.Text = ""
            If dtOldApproverEmp IsNot Nothing Then
                Dim dRow() As DataRow = dtOldApproverEmp.Select("select = True")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("select") = False
                        dRow(i).AcceptChanges()
                    Next
                End If

            End If
            Call FillOldApproverEmployeeList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Data Grid Events"
    Protected Sub dgOldApproverEmp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOldApproverEmp.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                If dtOldApproverEmp IsNot Nothing AndAlso dtOldApproverEmp.Rows.Count > 0 Then
                    Dim dRow() As DataRow = CType(dtOldApproverEmp, DataTable).Select("employeecode='" & e.Item.Cells(1).Text & "'")
                    If dRow.Length > 0 Then
                        If CBool(dRow(0).Item("select")) = True Then
                            CType(e.Item.Cells(0).FindControl("ChkgvOldSelect"), CheckBox).Checked = CBool(dRow(0).Item("select"))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgOldApproverEmp_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgNewApproverMigratedEmp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgNewApproverMigratedEmp.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                If dtNewApproverMigratedEmp IsNot Nothing AndAlso dtNewApproverMigratedEmp.Rows.Count > 0 Then
                    Dim dRow() As DataRow = dtNewApproverMigratedEmp.Select("employeecode='" & e.Item.Cells(1).Text & "'")
                    If dRow.Length > 0 Then
                        If CBool(dRow(0).Item("select")) = True Then
                            CType(e.Item.Cells(0).FindControl("ChkgvNewSelect"), CheckBox).Checked = CBool(dRow(0).Item("select"))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgNewApproverMigratedEmp_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbInfo", Me.lblDetialHeader.Text)

            Me.lblNewLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewLevel.ID, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewApprover.ID, Me.lblNewApprover.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.tbMigrationEmp.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbMigrationEmp.ID, Me.tbMigrationEmp.HeaderText)
            'Me.tbAssignedEmp.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbAssignedEmp.ID, Me.tbAssignedEmp.HeaderText)
            'Hemant (17 Sep 2020) -- End

            Me.dgOldApproverEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgOldApproverEmp.Columns(1).FooterText, Me.dgOldApproverEmp.Columns(1).HeaderText)
            Me.dgOldApproverEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgOldApproverEmp.Columns(2).FooterText, Me.dgOldApproverEmp.Columns(2).HeaderText)

            Me.dgNewApproverMigratedEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverMigratedEmp.Columns(1).FooterText, Me.dgNewApproverMigratedEmp.Columns(1).HeaderText)
            Me.dgNewApproverMigratedEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverMigratedEmp.Columns(2).FooterText, Me.dgNewApproverMigratedEmp.Columns(2).HeaderText)

            Me.dgNewApproverAssignEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverAssignEmp.Columns(0).FooterText, Me.dgNewApproverAssignEmp.Columns(0).HeaderText)
            Me.dgNewApproverAssignEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverAssignEmp.Columns(1).FooterText, Me.dgNewApproverAssignEmp.Columns(1).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
