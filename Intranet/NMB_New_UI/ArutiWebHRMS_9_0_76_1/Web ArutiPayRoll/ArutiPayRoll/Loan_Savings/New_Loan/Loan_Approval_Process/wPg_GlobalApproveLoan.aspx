﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_GlobalApproveLoan.aspx.vb"
    Inherits="Loan_Savings_wPg_GlobalApproveLoan" Title="Global Approve Loan" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").not(".combo").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollLeft($(scroll.Y).val());
                $("#scrollable-container").scrollTop($(scroll1.Y).val());
            }
            setIcon()
        }
    </script>

    <script type="text/javascript">
        function setIcon() {
            $(".isgroupclass").each(function() {
                if ($(this).attr("title") == "True") {
                    $(this).parent().parent().children("td:first").children("a").attr("title", "Collapse")
                    $(this).parent().parent().children("td:first").children().children("div").children("i").removeClass("fa-chevron-circle-down")
                }
                else {
                    $(this).parent().parent().children("td:first").children("a").attr("title", "Expand")
                    $(this).parent().parent().children("td:first").children().children("div").children("i").addClass("fa-chevron-circle-down")
                }
            });
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Global Approve Loan" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbFilter" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                <h2/>
                                               
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" ToolTip="Allocations">
                                                            <i class="fas fa-sliders-h"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDeductionPeriod" runat="server" Text="Deduction Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDeductionPeriod" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoanAmount" runat="server" Text="Loan Amount" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-0">
                                                            <div class="form-group m-t-0">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtLoanAmount" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                            <div class="form-group m-t-0">
                                                                <asp:DropDownList ID="cboAmountCondition" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <div style="float: left;">
                                                    <asp:Label ID="gbInfo" runat="server" Text="Information" CssClass="form-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApprover" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <div style="float: left">
                                    <asp:Label ID="gbPending" runat="server" Text="Pending Loan/Advance Application(s)."
                                        CssClass="form-label"></asp:Label>
                                </div>
                                <div style="text-align: right; vertical-align: top;">
                                    <asp:RadioButton ID="radLoan" runat="server" Text="Loan" GroupName="GlobalApprove"
                                        AutoPostBack="true"></asp:RadioButton>
                                    <asp:RadioButton ID="radAdvance" runat="server" Text="Advance" GroupName="GlobalApprove"
                                        AutoPostBack="true"></asp:RadioButton>
                                </div>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" Width="125%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                RowStyle-Wrap="false">
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objdgcolhCollapse">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgCollapse" runat="server" ToolTip="Collapse">
                                                            <div style="color:#004474"><i id="expcol" class="fas fa-chevron-circle-up"></i></div>
                                                            </asp:LinkButton>
                                                        </ItemTemplate> 
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objdgcolhCheck">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged1" />
                                                            <asp:Label ID="lblgroupid" runat="server" Text="" Style="display: none" CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="EName" HeaderText="Employee" FooterText="dgcolhEName" />
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="BasicSal" HeaderText="Basic Salary" FooterText="dgcolhBasicSal"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="AppNo" HeaderText="Application No." FooterText="dgcolhAppNo" />
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="Amount" HeaderText="Applied Amount" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhRAmount"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="DeductionPeriodID" HeaderText="DeductionPeriodID" Visible="false"
                                                        FooterText="dgcolhDeductionPeriod"></asp:BoundColumn>
                                                    <%--7--%>
                                                    <asp:TemplateColumn HeaderText="Deduction Period" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cbodgcolhDeductionPeriod" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="cbodgcolhDeductionPeriod_SelectedIndexChanged" CssClass="combo">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--8--%>
                                                    <asp:TemplateColumn HeaderText="Duration" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="dgcolhDuration" Visible="false">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhDuration" onKeypress="return onlyNumbers(this, event);"
                                                                        Text='<%# Eval("duration") %>' Style="text-align: right;" runat="server" OnTextChanged="txtdgcolhDuration_TextChanged"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                    </asp:TemplateColumn>
                                                    <%--9--%>
                                                    <asp:TemplateColumn HeaderText="Installment Amount" HeaderStyle-HorizontalAlign="Right"
                                                        FooterText="dgcolhInstallmentAmt">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhInstallmentAmt" Text='<%# Eval("installmentamt") %>' Style="text-align: right;"
                                                                        runat="server" AutoPostBack="true" OnTextChanged="txtdgcolhInstallmentAmt_TextChanged"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--10--%>
                                                    <asp:TemplateColumn HeaderText="No of Installment" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhNoOfInstallments">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhNoOfInstallments" onKeypress="return onlyNumbers(this, event);"
                                                                        Text='<%# Eval("noofinstallment") %>' Style="text-align: right;" runat="server"
                                                                        AutoPostBack="true" OnTextChanged="txtdgcolhNoOfInstallments_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--11--%>
                                                    <asp:TemplateColumn HeaderText="Approved Amount" HeaderStyle-HorizontalAlign="Right"
                                                        FooterText="dgcolhAAmount">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhAAmount" Text='<%# Eval("AppAmount") %>' Style="text-align: right;"
                                                                        runat="server" OnTextChanged="txtdgcolhAAmount_TextChanged" CssClass="form-control"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="PId" HeaderText="objcolhPendingUnkid" Visible="false"
                                                        ReadOnly="true" FooterText="objcolhPendingUnkid" />
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="IsGrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="GrpId" HeaderText="objdgcolhGrpId" Visible="false" FooterText="objdgcolhGrpId" />
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="IsVisible" Visible="false" />
                                                    <%--16--%>
                                                </Columns>
                                                <HeaderStyle Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
