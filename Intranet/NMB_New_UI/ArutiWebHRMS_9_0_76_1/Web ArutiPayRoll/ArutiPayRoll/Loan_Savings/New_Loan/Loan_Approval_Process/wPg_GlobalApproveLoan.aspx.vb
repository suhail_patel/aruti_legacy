﻿Option Strict On
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region


Partial Class Loan_Savings_wPg_GlobalApproveLoan
    Inherits Basepage

#Region " Private Variables "

    Private objPendingLoan As clsProcess_pending_loan
    Dim msg As New CommonCodes

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Shared ReadOnly mstrModuleName As String = "frmGlobalApproveLoan"
    'Anjan [04 June 2014] -- End

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrEmployeeIDs As String = String.Empty
    Private mdtTable As DataTable = Nothing
    'Private mdecInstallmentAmnt As Decimal = 0
    Private objLoanapproval As New clsloanapproval_process_Tran
    'Nilay (01-Jun-2015) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

       

            objPendingLoan = New clsProcess_pending_loan
            If Not IsPostBack Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                End If
                'Nilay (10-Dec-2016) -- End

                Call FillCombo()
                If dgvData.Items.Count <= 0 Then
                    dgvData.DataSource = New List(Of String)
                    dgvData.DataBind()
                End If
            Else
                mdtTable = CType(Me.Session("DataList"), DataTable)
                'mdecInstallmentAmnt = CDec(Me.ViewState("InstallmentAmnt"))

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Me.ViewState.Add("InstallmentAmnt", mdecInstallmentAmnt)
            Me.Session("DataList") = mdtTable

            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
    'Nilay (01-Jun-2015) -- End


#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsCombo As New DataSet
            Dim objAMaster As New clsLoanApprover_master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMasterData As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim objLevel As New clslnapproverlevel_master

            dsCombo = objAMaster.GetEmployeeFromUser(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                     "List")

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objAMaster = Nothing

            Dim lstIDs As List(Of String) = (From p In dsCombo.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombo = objLoanScheme.getComboList(True, "LoanScheme")
            dsCombo = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            objLoanScheme = Nothing

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), _
                                                "Period", True, 1)
            With cboDeductionPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()
            End With
            objPeriod = Nothing

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombo = objMasterData.GetCondition(False, True)
            dsCombo = objMasterData.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsCombo.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dtCondition
                .DataBind()
            End With
            objMasterData = Nothing

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            RemoveHandler cboApprover.SelectedIndexChanged, AddressOf cboApprover_SelectedIndexChanged
            'Nilay (21-Jul-2016) -- End
            dsCombo = objLevel.GetLevelFromUserLogin(CInt(Session("UserId")))
            With cboApprover
                .DataValueField = "lnapproverunkid"
                .DataTextField = "Approver"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objLevel = Nothing
            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            AddHandler cboApprover.SelectedIndexChanged, AddressOf cboApprover_SelectedIndexChanged
            'Nilay (21-Jul-2016) -- End

            dsCombo = objPendingLoan.GetLoan_Status("Status", True)
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'Dim dtTemp() As DataRow = dsCombo.Tables("Status").Select("Id NOT IN(2)")
            'For i As Integer = 0 To dtTemp.Length - 1
            '    dsCombo.Tables("Status").Rows.Remove(dtTemp(i))
            'Next
            Dim xRow = From p In dsCombo.Tables("Status") Where CInt(p.Item("Id")) = enLoanApplicationStatus.APPROVED OrElse CInt(p.Item("Id")) = enLoanApplicationStatus.REJECTED Select p
            Dim dtTable As DataTable = xRow.CopyToDataTable
            'Nilay (20-Sept-2016) -- End
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable 'Nilay (20-Sept-2016) -- [REMOVED: dsCombo.Tables("List")]
                .DataBind()
                .SelectedValue = "2"
            End With
            objPendingLoan = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("FillCombo:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Jun-2015) -- End

    Private Sub FillGrid()
        Try
            Dim mintMode As Integer = -1
            Dim mstrSearch As String = ""

            If radAdvance.Checked Then
                mintMode = 0
            ElseIf radLoan.Checked Then
                mintMode = 1
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " "
            End If

            If txtLoanAmount.Text.Trim <> "" AndAlso CDec(txtLoanAmount.Text) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loan_amount " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtLoanAmount.Text) & " "
            End If

            If mstrEmployeeIDs.Trim.Length > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.employeeunkid IN (" & mstrEmployeeIDs & ") "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                mstrSearch &= "AND " & mstrAdvanceFilter
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If

            mdtTable = objPendingLoan.GetDataList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                  Session("UserAccessModeSetting").ToString, _
                                                  True, _
                                                  CBool(Session("IsIncludeInactiveEmp")), _
                                                  "List", _
                                                  CInt(cboApprover.SelectedValue), mintMode, mstrSearch)

            mdtTable.Columns.Add("IsVisible", Type.GetType("System.Boolean"))

            For Each xRow As DataRow In mdtTable.Rows
                xRow("IsVisible") = True
            Next

            dgvData.AutoGenerateColumns = False
            dgvData.DataSource = mdtTable
            dgvData.DataBind()

            'Nilay (01-Jun-2015) -- Start
            'New Loan Savings
            'dtTab = objPendingLoan.GetDataList(1, _
            '                                   CInt(cboDepartment.SelectedValue), _
            '                                   CInt(cboBranch.SelectedValue), _
            '                                   CInt(cboSection.SelectedValue), _
            '                                   CInt(cboJob.SelectedValue), _
            '                                   intMode)

            'If dtTab.Columns.Contains("ExpCol") = False Then
            '    Dim dc As New DataColumn
            '    dc.DataType = Type.GetType("System.String")
            '    dc.ColumnName = "ExpCol"
            '    dc.DefaultValue = ""
            '    dtTab.Columns.Add(dc)

            '    Dim drRow() As DataRow = dtTab.Select("IsGrp = 1")
            '    If drRow.Length > 0 Then
            '        For i As Integer = 0 To drRow.Length - 1
            '            drRow(i)("ExpCol") = "+"
            '        Next
            '        dtTab.AcceptChanges()
            '    End If

            'End If
            'dgvData.AutoGenerateColumns = False

            'Nilay (01-Jun-2015) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Nilay (01-Jun-2015) -- Start
            'New Loan Savings
            'If dtTab.Rows.Count <= 0 Then
            '    Dim drRow As DataRow = dtTab.NewRow
            '    drRow("AppNo") = "None"
            '    drRow("Amount") = 0
            '    dtTab.Rows.Add(drRow)
            'End If
            'Me.ViewState.Add("PendingLoan", dtTab)
            'dgvData.DataSource = dtTab
            'dgvData.DataBind()
            'Nilay (01-Jun-2015) -- End
        End Try
    End Sub

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    Private Function IsValidate() As Boolean
        Try
            For Each item As DataGridItem In dgvData.Items

                If CBool(item.Cells(13).Text) = False Then

                    If CInt(item.Cells(6).Text) <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Deduction Period is compulsory information.Please select deduction period."), Me)
                        CType(item.Cells(9).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Focus()
                        Return False

                    ElseIf radLoan.Checked = True AndAlso CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text) <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0."), Me)
                        CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Focus()
                        Return False

                    ElseIf radLoan.Checked = True AndAlso CInt(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text) <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "No of Installment cannot be 0.Please define No of Installment greater than 0."), Me)
                        CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Focus()
                        Return False

                    ElseIf radLoan.Checked = True AndAlso CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text) > 0 AndAlso CDec(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text) > 0 _
                            AndAlso CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text) > CDec(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text) Then
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Installment Amount cannot be greater than approved loan amount."), Me)
                        CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Focus()
                        Return False

                        'ElseIf radLoan.Checked = True AndAlso Format(CDec(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text), Session("fmtCurrency").ToString) <> _
                        '        Format(CDec(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text) * CDec(mdtTable.Rows(item.ItemIndex)("installmentamt")), Session("fmtCurrency").ToString) Then
                        '    'Format(CInt(CType(item.Cells(9).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text) * CDec(mdtTable.Rows(item.ItemIndex)("installmentamt")), Session("fmtCurrency")) Then
                        '    'Language.setLanguage(mstrModuleName)
                        '    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "You are changing approved loan amount.We recommanded you that you have to change No of installments or Installment amount."), Me)
                        '    dgvData.Focus()
                        '    Return False
                    End If
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Dim objPendingLoan As New clsProcess_pending_loan
                    objPendingLoan._Processpendingloanunkid = CInt(item.Cells(12).Text)
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = objPendingLoan._Loanschemeunkid
                    If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text) > objLoanScheme._MaxNoOfInstallment Then
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, " for ") & objLoanScheme._Name & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, " Scheme , Application No : [ ") & objPendingLoan._Application_No & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, " ] , Employee Code and Name is ") & CStr(item.Cells(2).Text) & ".", Me)
                        CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Focus()
                        Return False
                    End If
                    'Varsha (25 Nov 2017) -- End
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    If CInt(cboStatus.SelectedValue) <= 0 Then
                        'Varsha (25 Nov 2017) -- Start
                        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                        'Language.setLanguage(mstrModuleName)
                        'Varsha (25 Nov 2017) -- End
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Please select atleast one Status from the list to perform further operation.."), Me)
                        cboStatus.Focus()
                        Return False
                    End If

                    If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED AndAlso txtRemarks.Text.Trim.Length <= 0 Then
                        'Varsha (25 Nov 2017) -- Start
                        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                        'Language.setLanguage(mstrModuleName)
                        'Varsha (25 Nov 2017) -- End
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Remark cannot be blank. Remark is compulsory information."), Me)
                        txtRemarks.Focus()
                        Return False
                    End If
                    'Nilay (20-Sept-2016) -- End
                End If
            Next

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("IsValidate:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    'Private Sub SetTableValue()
    '    Try
    '        For Each item As DataGridItem In dgvData.Items
    '            If CBool(item.Cells(12).Text) = False And CType(item.Cells(1).FindControl("chkSelect"), CheckBox).Checked = True Then
    '                mdtTable.Rows(item.ItemIndex)("AppAmount") = CType(item.Cells(10).FindControl(""), TextBox).ToolTip
    '                mdtTable.Rows(item.ItemIndex)("noofinstallment") = CType(item.Cells(3).FindControl(""), TextBox).ToolTip
    '                mdtTable.Rows(item.ItemIndex)("installmentamt") = CType(item.Cells(3).FindControl(""), TextBox).ToolTip
    '                mdtTable.Rows(item.ItemIndex)("duration") = CType(item.Cells(3).FindControl(""), TextBox).ToolTip
    '            End If
    '        Next
    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Nilay (01-Jun-2015) -- End
#End Region

#Region " Button's Events "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                cboApprover.Focus()
                Exit Sub
            End If

            'Nilay (01-Jun-2015) -- Start
            'New Loan Savings
            For Each dRow As DataRow In mdtTable.Rows
                If CBool(dRow.Item("IsGrp")) = False Then
                    If CDec(dRow.Item("AppAmount")) = 0 Then
                        dRow.Item("AppAmount") = dRow.Item("Amount")
                    End If
                End If
            Next

            mdtTable.AcceptChanges()

            Dim dtTemp() As DataRow = Nothing
            'Dim dtTab As DataTable = CType(Me.ViewState("PendingLoan"), DataTable)
            'dtTemp = dtTab.Select("IsChecked = 1 And IsGrp = false")
            dtTemp = mdtTable.Select("IsChecked = 1 And IsGrp = false")
            If dtTemp.Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select atleast one information to Approve."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If
            'Nilay (01-Jun-2015) -- End

            Blank_ModuleName()

            'Nilay (01-Jun-2015) -- Start
            'New Loan Savings
            If IsValidate() = True Then
                'For i As Integer = 0 To dtTemp.Length - 1
                '    objPendingLoan._Processpendingloanunkid = CInt(dtTemp(i).Item("PId"))
                '    objPendingLoan._Approverunkid = CInt(cboApprover.SelectedValue)
                '    objPendingLoan._Loan_Statusunkid = CInt(cboStatus.SelectedValue)
                '    objPendingLoan._Approved_Amount = CDec(dtTemp(i).Item("AppAmount"))
                '    objPendingLoan.Update()
                'Next

                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                Dim dsList As DataSet = Nothing
                dsList = (New clslnapproverlevel_master).GetLevelFromUserLogin(CInt(Session("UserId")))
                Dim intPriority As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("lnapproverunkid") = CInt(cboApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("priority")).First
                'Shani (21-Jul-2016) -- End


                For i As Integer = 0 To dtTemp.Length - 1
                    Blank_ModuleName() 'Nilay (05-May-2016)
                    objPendingLoan._WebFormName = "frmGlobalApproveLoan"
                    StrModuleName2 = "mnuLoan_Advance_Savings"
                    objPendingLoan._WebClientIP = Session("IP_ADD").ToString
                    objPendingLoan._WebHostName = Session("HOST_NAME").ToString
                    objPendingLoan._Userunkid = CInt(Session("UserId"))
                    'Nilay (05-May-2016) -- Start
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        objPendingLoan._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    End If
                    'Nilay (05-May-2016) -- End

                    objLoanapproval._Pendingloantranunkid = CInt(dtTemp(i).Item("pendingloantranunkid"))
                    objLoanapproval._Processpendingloanunkid = CInt(dtTemp(i).Item("PId"))
                    objLoanapproval._Approvertranunkid = CInt(cboApprover.SelectedValue)
                    objLoanapproval._Approvaldate = DateAndTime.Now.Date
                    objLoanapproval._Statusunkid = CInt(cboStatus.SelectedValue)
                    objLoanapproval._Deductionperiodunkid = CInt(dtTemp(i).Item("DeductionPeriodID"))
                    'objLoanapproval._Duration = CInt(dtTemp(i).Item("duration"))
                    objLoanapproval._Installmentamt = CDec(dtTemp(i).Item("installmentamt"))
                    objLoanapproval._Noofinstallment = CInt(dtTemp(i).Item("noofinstallment"))
                    objLoanapproval._Loan_Amount = CDec(dtTemp(i).Item("AppAmount"))
                    objLoanapproval._Userunkid = CInt(Session("UserId"))
                    objLoanapproval._Remark = txtRemarks.Text.ToString
                    'Nilay (05-May-2016) -- Start
                    Blank_ModuleName()
                    StrModuleName2 = "mnuLoan_Advance_Savings"
                    objLoanapproval._WebClientIP = Session("IP_ADD").ToString
                    objLoanapproval._WebFormName = "frmGlobalApproveLoan"
                    objLoanapproval._WebHostName = Session("HOST_NAME").ToString
                    'Nilay (05-May-2016) -- End

                    If objLoanapproval.Update(CBool(Session("LoanApprover_ForLoanScheme")), CInt(dtTemp(i)("GrpId")), Nothing) = False Then
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(objLoanapproval._Message, Me)
                        Exit Sub
                        'Shani (21-Jul-2016) -- Start
                        'Enhancement - Create New Loan Notification 
                    Else
                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                        If mblnIsSendEmailNotification = True Then
                            'Nilay (10-Dec-2016) -- End

                            Dim objProcessLoan As New clsProcess_pending_loan
                            'Nilay (20-Sept-2016) -- Start
                            'Enhancement : Cancel feature for approved but not assigned loan application
                            'If CInt(cboStatus.SelectedValue) = 2 Then 'Approve
                            If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                                'Nilay (20-Sept-2016) -- End
                                If objLoanapproval.IsPendingLoanApplication(CInt(dtTemp(i).Item("PId")), True) = False Then
                                    'Nilay (23-Aug-2016) -- Start
                                    'Enhancement - Create New Loan Notification 
                                    If radLoan.Checked Then
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                        '                                          CInt(cboLoanScheme.SelectedValue), _
                                        '                                          CInt(dtTemp(i).Item("employeeunkid")), _
                                        '                                          intPriority, _
                                        '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                        '                                          False, CInt(dtTemp(i).Item("PId")).ToString, _
                                        '                                          Session("ArutiSelfServiceURL").ToString, True, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                  CInt(cboLoanScheme.SelectedValue), _
                                                                                  CInt(dtTemp(i).Item("employeeunkid")), _
                                                                                  intPriority, _
                                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                                  False, CInt(dtTemp(i).Item("PId")).ToString, _
                                                                                  Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), True, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                    Else
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                        '                                          CInt(cboLoanScheme.SelectedValue), _
                                        '                                          CInt(dtTemp(i).Item("employeeunkid")), _
                                        '                                          intPriority, _
                                        '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                        '                                          False, CInt(dtTemp(i).Item("PId")).ToString, _
                                        '                                          Session("ArutiSelfServiceURL").ToString, True, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                  CInt(cboLoanScheme.SelectedValue), _
                                                                                  CInt(dtTemp(i).Item("employeeunkid")), _
                                                                                  intPriority, _
                                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                                  False, CInt(dtTemp(i).Item("PId")).ToString, _
                                                                                  Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), True, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                    End If
                                    'Nilay (23-Aug-2016) -- End

                                Else 'Assign

                                    'Nilay (08-Dec-2016) -- Start
                                    'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                                    If radLoan.Checked Then

                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                        '                                      CInt(dtTemp(i).Item("PId")), _
                                        '                                      clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                        '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
                                        '                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                                                             CInt(dtTemp(i).Item("PId")), _
                                                                             clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), , _
                                                                             enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                    Else
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                        '                                          CInt(dtTemp(i).Item("PId")), _
                                        '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                        '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                        '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
                                        '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                                                                 CInt(dtTemp(i).Item("PId")), _
                                                                                 clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), , _
                                                                                 enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                    End If

                                    objProcessLoan.Send_Notification_Assign(CInt(dtTemp(i).Item("employeeunkid")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                Session("ArutiSelfServiceURL").ToString, _
                                                                               CInt(dtTemp(i).Item("PId")), _
                                                                                Session("Database_Name").ToString, _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                CStr(Session("UserAccessModeSetting")), _
                                                                                CBool(Session("IsIncludeInactiveEmp")), _
                                                                               False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), , _
                                                                               Session("Notify_LoanAdvance_Users").ToString)
                                    'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString)]	
                                    'Nilay (08-Dec-2016) -- End
                                End If
                                'Nilay (20-Sept-2016) -- Start
                                'Enhancement : Cancel feature for approved but not assigned loan application
                                'ElseIf CInt(cboStatus.SelectedValue) = 3 Then ' Reject
                            ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                                'Nilay (20-Sept-2016) -- End

                                'Nilay (23-Aug-2016) -- Start
                                'Enhancement - Create New Loan Notification 
                                If radLoan.Checked Then
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                    '                                          CInt(dtTemp(i).Item("PId")), _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                    '                                          txtRemarks.Text, _
                                    '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                                                              CInt(dtTemp(i).Item("PId")), _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), _
                                                                              txtRemarks.Text, _
                                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    'Sohail (30 Nov 2017) -- End
                                Else
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                    '                                          CInt(dtTemp(i).Item("PId")), _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                    '                                          txtRemarks.Text, _
                                    '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), _
                                                                              CInt(dtTemp(i).Item("PId")), _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), _
                                                                              txtRemarks.Text, _
                                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    'Sohail (30 Nov 2017) -- End
                                End If
                                'Nilay (23-Aug-2016) -- End
                            End If
                        End If 'Nilay (10-Dec-2016)
                    End If
                Next
                Call FillGrid()
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                txtRemarks.Text = ""
                'Nilay (20-Sept-2016) -- End

            End If
            'Nilay (01-Jun-2015) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            Session("DataList") = Nothing
            Me.ViewState("InstallmentAmnt") = Nothing
        End Try
    End Sub

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If radLoan.Checked = False AndAlso radAdvance.Checked = False Then
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Please Select Loan/Advance to continue global approval process."), Me)
                Exit Sub
            End If

            Call FillGrid()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboDeductionPeriod.SelectedValue = "0"
            txtLoanAmount.Text = "0"
            cboAmountCondition.SelectedIndex = 0
            radLoan.Checked = False
            radAdvance.Checked = False
            dgvData.DataSource = New List(Of String)
            dgvData.DataBind()
            Me.Session("DataList") = Nothing
            Me.ViewState("Mode") = Nothing
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Call FillGrid()
            'Nilay (20-Sept-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("DataList") = Nothing
            Me.ViewState("InstallmentAmnt") = Nothing

            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            'Nilay (01-Mar-2016) -- End
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillGrid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Jun-2015) -- End

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("wPg_LA_PendingList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END
#End Region

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    '#Region " CheckBox Event"

    '    'Protected Sub chkSelect_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    '    Try
    '    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '    '        If cb Is Nothing Then Exit Try
    '    '        Dim dtTab As DataTable = CType(Me.ViewState("PendingLoan"), DataTable)
    '    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)

    '    '        If gvRow.Cells.Count > 0 Then
    '    '            dtTab.Rows(gvRow.RowIndex).Item("IsChecked") = cb.Checked
    '    '            If dtTab.Rows(gvRow.RowIndex).Item("IsGrp") Then
    '    '                Dim drRow() As DataRow = dtTab.Select("GrpID = " & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & " AND IsGrp = 0")

    '    '                If drRow.Length > 0 Then
    '    '                    For i As Integer = 1 To drRow.Length
    '    '                        dtTab.Rows(gvRow.RowIndex + i).Item("IsChecked") = cb.Checked
    '    '                        Dim gRow As GridViewRow = dgvData.Rows(gvRow.RowIndex + i)
    '    '                        CType(gRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
    '    '                        CType(gRow.FindControl("lnkExpand"), LinkButton).Text = ""
    '    '                    Next
    '    '                End If

    '    '            Else
    '    '                Dim dTotalChildRow() As DataRow = dtTab.Select("GrpID = '" & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & "' AND IsGrp =  0")
    '    '                Dim dCheckedchildRow() As DataRow = dtTab.Select("GrpID = '" & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & "' AND IsChecked =  1 AND IsGrp =  0")

    '    '                Dim mintRowIndex As Integer = -1
    '    '                Dim drParentRow() As DataRow = dtTab.Select("GrpID = '" & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & "' AND IsGrp =  1")
    '    '                mintRowIndex = dtTab.Rows.IndexOf(drParentRow(0))
    '    '                Dim gParentRow As GridViewRow = dgvData.Rows(mintRowIndex)
    '    '                If dTotalChildRow.Length = dCheckedchildRow.Length Then
    '    '                    CType(gParentRow.FindControl("chkSelect"), CheckBox).Checked = True
    '    '                Else
    '    '                    CType(gParentRow.FindControl("chkSelect"), CheckBox).Checked = False
    '    '                End If


    '    '                ' START TO TEXT BLANK OF LINK BUTTON FOR CHILD ROW
    '    '                If dTotalChildRow.Length > 0 Then
    '    '                    For i As Integer = 1 To dTotalChildRow.Length
    '    '                        mintRowIndex = dtTab.Rows.IndexOf(dTotalChildRow(i - 1))
    '    '                        Dim gRow As GridViewRow = dgvData.Rows(mintRowIndex)
    '    '                        CType(gRow.FindControl("lnkExpand"), LinkButton).Text = ""
    '    '                    Next
    '    '                End If
    '    '                ' END TO TEXT BLANK OF LINK BUTTON FOR CHILD ROW

    '    '            End If
    '    '        End If
    '    '        Me.ViewState("PendingLoan") = dtTab
    '    '    Catch ex As Exception
    '    '        msg.DisplayError(ex, Me)
    '    '    End Try
    '    'End Sub


    '#End Region

    'Nilay (01-Jun-2015) -- End

#Region " LinkButton Event"

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    'Protected Sub lnkExpand_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        Dim lnkBtn As LinkButton = CType(sender, LinkButton)
    '        Dim dtTab As DataTable = CType(Me.ViewState("PendingLoan"), DataTable)
    '        Dim gvRow As GridViewRow = CType(lnkBtn.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            If dtTab.Rows(gvRow.RowIndex).Item("IsGrp") Then
    '                Dim drRow() As DataRow = dtTab.Select("GrpID = " & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & " AND IsGrp = 0")

    '                If drRow.Length > 0 Then

    '                    If lnkBtn.Text = "+" Then
    '                        For i As Integer = 1 To drRow.Length
    '                            Dim gRow As GridViewRow = dgvData.Rows(gvRow.RowIndex + i)
    '                            gRow.Visible = True
    '                            gRow.Cells(0).Text = ""
    '                        Next
    '                        lnkBtn.Text = "-"
    '                    Else
    '                        For i As Integer = 1 To drRow.Length
    '                            Dim gRow As GridViewRow = dgvData.Rows(gvRow.RowIndex + i)
    '                            gRow.Visible = False
    '                        Next
    '                        lnkBtn.Text = "+"
    '                    End If
    '                End If
    '            End If

    '            dtTab.Rows(gvRow.RowIndex)("ExpCol") = lnkBtn.Text
    '            Me.ViewState("PendingLoan") = dtTab
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Jun-2015) -- End

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            'popupAdvanceFilter._Hr_EmployeeTable_Alias = "emp."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Jun-2015) -- End

#End Region

#Region "CheckBox Events"

    Protected Sub chkSelect_CheckedChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvData.Items.Count <= 0 Then Exit Sub
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            If CBool(item.Cells(13).Text) Then
                Dim intgroupid As Integer = CInt(CType(item.Cells(1).FindControl("lblgroupid"), Label).Text)
                Dim xrow() As DataRow = mdtTable.Select("GrpId=" & intgroupid)
                For Each row In xrow
                    row.Item("IsChecked") = chkBox.Checked
                    CType(dgvData.Items(mdtTable.Rows.IndexOf(row)).Cells(1).FindControl("chkSelect"), CheckBox).Checked = chkBox.Checked
                Next
            Else
                mdtTable.Rows(item.ItemIndex)("IsChecked") = chkBox.Checked
                mdtTable.AcceptChanges()
                Dim intgroupid As Integer = CInt(CType(item.Cells(1).FindControl("lblgroupid"), Label).Text)
                Dim xrow() As DataRow = mdtTable.Select("GrpId=" & intgroupid & "AND IsChecked = 'False' AND IsGrp = 'False'")
                Dim xGrpRow() As DataRow = mdtTable.Select("GrpId=" & intgroupid & "AND IsGrp = 'True'")
                If xrow.Length > 0 Then
                    CType(dgvData.Items(mdtTable.Rows.IndexOf(xGrpRow(0))).Cells(1).FindControl("chkSelect"), CheckBox).Checked = False
                Else
                    CType(dgvData.Items(mdtTable.Rows.IndexOf(xGrpRow(0))).Cells(1).FindControl("chkSelect"), CheckBox).Checked = True
                End If

            End If

            mdtTable.AcceptChanges()

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If CBool(e.Item.Cells(13).Text) = True Then
                    Dim lbl As Label = CType(e.Item.Cells(1).FindControl("lblgroupid"), Label)
                    Dim bln As Boolean = CBool(lbl.ToolTip)
                    If bln Then
                        Dim xrow() As DataRow = mdtTable.Select("GrpId = " & lbl.Text)
                        For Each drow In xrow
                            drow("IsVisible") = False
                        Next
                        For Each Items As DataGridItem In dgvData.Items
                            If CInt(CType(Items.Cells(1).FindControl("lblgroupid"), Label).Text) = CInt(lbl.Text) AndAlso CBool(Items.Cells(13).Text) = False Then
                                Items.Visible = False
                            End If
                        Next
                        lbl.ToolTip = CStr(False)
                    Else
                        Dim xrow() As DataRow = mdtTable.Select("GrpId = " & lbl.Text)
                        For Each drow In xrow
                            drow("IsVisible") = True
                        Next
                        For Each Items As DataGridItem In dgvData.Items
                            If CInt(CType(Items.Cells(1).FindControl("lblgroupid"), Label).Text) = CInt(lbl.Text) AndAlso CBool(Items.Cells(13).Text) = False Then
                                Items.Visible = True
                            End If
                        Next
                        lbl.ToolTip = CStr(True)
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
    '    Try
    '        Dim dtTab As DataTable = Me.ViewState("PendingLoan")

    '        If e.Row.RowIndex >= 0 Then

    '            If e.Row.Cells(5).Text = "None" AndAlso e.Row.Cells(6).Text = "0" Then
    '                e.Row.Visible = False

    '            Else
    '                If CBool(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
    '                    For i = 4 To dgvData.Columns.Count - 1
    '                        e.Row.Cells(i).Visible = False
    '                    Next
    '                    e.Row.Cells(2).Visible = False
    '                    e.Row.Cells(3).ColumnSpan = dgvData.Columns.Count - 1
    '                    e.Row.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
    '                    e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
    '                    e.Row.Cells(3).CssClass = "GroupHeaderStyleBorderRight"

    '                    If (DataBinder.Eval(e.Row.DataItem, "ExpCol")) = "+" Then
    '                        Me.ViewState.Add("IsExpand", False)
    '                    ElseIf (DataBinder.Eval(e.Row.DataItem, "ExpCol")) = "-" Then
    '                        Me.ViewState.Add("IsExpand", True)
    '                    End If

    '                Else

    '                    If Me.ViewState("IsExpand") = True Then
    '                        e.Row.Visible = True
    '                    ElseIf Me.ViewState("IsExpand") = False Then
    '                        e.Row.Visible = False
    '                    End If


    '                    'SHANI [01 FEB 2015]-START
    '                    'Enhancement - REDESIGN SELF SERVICE.
    '                    'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#ddd'")
    '                    'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=''")
    '                    'SHANI [01 FEB 2015]--END
    '                End If
    '            End If

    '            If e.Row.Cells(6).Text.Trim <> "&nbsp;" AndAlso e.Row.Cells(6).Text.Trim <> "" Then
    '                e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtcurrency"))
    '            End If
    '            If e.Row.Cells(7).Text.Trim <> "&nbsp;" AndAlso e.Row.Cells(7).Text.Trim <> "" Then
    '                e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtcurrency"))
    '                e.Row.Cells(7).ForeColor = Drawing.Color.Blue
    '                e.Row.Cells(7).Font.Bold = True
    '            End If

    '            If e.Row.Cells(7).Controls.Count > 0 Then
    '                CType(e.Row.Cells(7).Controls(0), TextBox).CssClass = "RightTextAlign"
    '                CType(e.Row.Cells(7).Controls(0), TextBox).Text = CDec(CType(e.Row.Cells(7).Controls(0), TextBox).Text).ToString(Session("fmtcurrency"))
    '                CType(e.Row.Cells(7).Controls(0), TextBox).Focus()
    '            End If

    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub dgvData_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgvData.RowEditing
    '    Try
    '        dgvData.DataSource = Me.ViewState("PendingLoan")
    '        dgvData.EditIndex = e.NewEditIndex
    '        dgvData.DataBind()

    '        dgvData.Columns(1).Visible = False

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub dgvData_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgvData.RowUpdating
    '    Try

    '        Dim dtTab As DataTable = Me.ViewState("PendingLoan")

    '        For i = 0 To dgvData.Columns.Count - 1

    '            If TypeOf (dgvData.Columns(i)) Is CommandField Or TypeOf (dgvData.Columns(i)) Is TemplateField Then Continue For

    '            If DirectCast(DirectCast(dgvData.Columns(i), System.Web.UI.WebControls.DataControlField), System.Web.UI.WebControls.BoundField).ReadOnly = False Then

    '                Dim cell As DataControlFieldCell = dgvData.Rows(e.RowIndex).Cells(i)
    '                cell.HorizontalAlign = HorizontalAlign.Right
    '                dgvData.Columns(i).ExtractValuesFromCell(e.NewValues, cell, DataControlRowState.Edit, True)


    '                For Each key As String In e.NewValues.Keys
    '                    Dim str As String = e.NewValues(key)

    '                    If dtTab.Columns(key).DataType Is System.Type.GetType("System.String") Then
    '                        Dim decTemp As Decimal = 0
    '                        If Decimal.TryParse(str, decTemp) = True Then

    '                            If CDec(dtTab.Rows(e.RowIndex)("Amount")) < decTemp Then
    '                                msg.DisplayError(ex, Me)
    '                                e.Cancel = True
    '                                Exit Sub
    '                            End If

    '                            dtTab.Rows(e.RowIndex).Item(key) = decTemp.ToString(Session("fmtcurrency"))
    '                        Else
    '                            msg.DisplayError(ex, Me)
    '                            cell.Focus()
    '                            e.Cancel = True
    '                            Exit Sub
    '                        End If

    '                    Else
    '                        dtTab.Rows(e.RowIndex).Item(key) = str
    '                    End If

    '                Next
    '            End If
    '        Next

    '        If e.NewValues.Count > 0 Then
    '            dgvData.DataSource = dtTab
    '            dgvData.EditIndex = -1
    '            dgvData.DataBind()
    '            dgvData.Columns(1).Visible = True
    '        End If

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub dgvData_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgvData.RowCancelingEdit
    '    Try
    '        dgvData.DataSource = Me.ViewState("PendingLoan")
    '        dgvData.EditIndex = -1
    '        dgvData.DataBind()
    '        dgvData.Columns(1).Visible = True
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
    '    Try
    '        If e.Row.RowIndex >= 0 Then
    '            If e.Row.RowType = DataControlRowType.DataRow Then

    '                If dtTab IsNot Nothing Then
    '                    If CBool(dtTab.Rows(e.Row.RowIndex).Item("IsGrp")) = True Then
    '                        CType(e.Row.Cells(5).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Visible = False
    '                        CType(e.Row.Cells(6).FindControl("txtdgcolhDuration"), TextBox).Visible = False
    '                        CType(e.Row.Cells(7).FindControl("txtdgcolhInstallmentAmt"), TextBox).Visible = False
    '                        CType(e.Row.Cells(8).FindControl("txtdgcolhNoOfInstallments"), TextBox).Visible = False
    '                        CType(e.Row.Cells(9).FindControl("txtdgcolhAAmount"), TextBox).Visible = False

    '                        e.Row.Cells(2).ColumnSpan = e.Row.Cells.Count - 1
    '                        For i As Integer = 3 To e.Row.Cells.Count - 1
    '                            e.Row.Cells(i).Visible = False
    '                        Next
    '                        e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
    '                        e.Row.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
    '                        e.Row.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
    '                    End If

    '                    Dim dsCombo As New DataSet
    '                    Dim objPeriod As New clscommom_period_Tran

    '                    dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, 1)

    '                    If CInt(dtTab.Rows(e.Row.RowIndex).Item("DeductionPeriodID")) > 0 Then
    '                        Dim cboDeductionPeriod As DropDownList = CType(e.Row.Cells(5).FindControl("cbodgcolhDeductionPeriod"), DropDownList)
    '                        With cboDeductionPeriod
    '                            .DataValueField = "periodunkid"
    '                            .DataTextField = "name"
    '                            .DataSource = dsCombo.Tables("Period")
    '                            .DataBind()
    '                            .SelectedValue = CInt(dtTab.Rows(e.Row.RowIndex).Item("DeductionPeriodID"))
    '                        End With
    '                    End If

    '                End If

    '                If e.Row.Cells(4).Text <> "&nbsp;" AndAlso e.Row.Cells(4).Text.Trim <> "" Then
    '                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
    '                End If

    '                Dim txtInstallmentamt As TextBox = CType(e.Row.Cells(7).FindControl("txtdgcolhInstallmentAmt"), TextBox)
    '                If txtInstallmentamt.Text.Trim <> "" Then
    '                    txtInstallmentamt.Text = Format(CDec(txtInstallmentamt.Text), Session("fmtCurrency"))
    '                End If

    '                Dim txtApprovedAmount As TextBox = CType(e.Row.Cells(9).FindControl("txtdgcolhAAmount"), TextBox)
    '                If txtApprovedAmount.Text.Trim <> "" Then
    '                    txtApprovedAmount.Text = Format(CDec(txtApprovedAmount.Text), Session("fmtCurrency"))
    '                End If
    '            End If
    '        End If

    '    Catch ex As Exception
    '        msg.DisplayMessage("dgvData_RowDataBound:- " & ex.Message, Me)
    '    End Try
    'End Sub
    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            e.Item.Cells(12).Visible = False
            'Varsha (25 Nov 2017) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                If e.Item.ItemIndex >= 0 Then

                    If CBool(e.Item.Cells(13).Text) = True Then

                        CType(e.Item.Cells(1).FindControl("lblgroupid"), Label).Text = e.Item.Cells(14).Text
                        CType(e.Item.Cells(1).FindControl("lblgroupid"), Label).ToolTip = e.Item.Cells(e.Item.Cells.Count - 1).Text
                        CType(e.Item.Cells(1).FindControl("lblgroupid"), Label).CssClass = "isgroupclass"

                        e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1
                        For i As Integer = 3 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).CssClass = "GridViewRowVisibleFalse"
                        Next
                        e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                    Else

                        CType(e.Item.Cells(1).FindControl("lblgroupid"), Label).Text = e.Item.Cells(14).Text
                        CType(e.Item.Cells(1).FindControl("lblgroupid"), Label).ToolTip = e.Item.Cells(e.Item.Cells.Count - 1).Text
                        CType(e.Item.Cells(0).FindControl("imgCollapse"), LinkButton).Visible = False

                        Dim dsCombo As New DataSet
                        Dim objPeriod As New clscommom_period_Tran

                        dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), _
                                                            Session("Database_Name").ToString, _
                                                            CDate(Session("fin_startdate")), _
                                                            "Period", True, 1)

                        If CInt(e.Item.Cells(5).Text) > 0 Then
                            Dim cboDeductionPeriod As DropDownList = CType(e.Item.Cells(6).FindControl("cbodgcolhDeductionPeriod"), DropDownList)
                            With cboDeductionPeriod
                                .DataValueField = "periodunkid"
                                .DataTextField = "name"
                                .DataSource = dsCombo.Tables("Period")
                                .DataBind()
                                .SelectedValue = e.Item.Cells(6).Text
                            End With
                        End If

                        If e.Item.Cells(3).Text <> "&nbsp;" AndAlso e.Item.Cells(3).Text.Trim <> "" Then
                            e.Item.Cells(3).Text = Format(CDec(e.Item.Cells(3).Text), Session("fmtCurrency").ToString)
                        End If

                        If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.Trim <> "" Then
                            e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency").ToString)
                        End If

                        Dim txtInstallmentamt As TextBox = CType(e.Item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox)
                        If txtInstallmentamt.Text.Trim <> "" Then
                            txtInstallmentamt.Text = Format(CDec(txtInstallmentamt.Text), Session("fmtCurrency").ToString)
                        End If

                        Dim txtApprovedAmount As TextBox = CType(e.Item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox)
                        If CDec(txtApprovedAmount.Text) <= 0 Then
                            txtApprovedAmount.Text = e.Item.Cells(5).Text
                        End If
                        If txtApprovedAmount.Text.Trim <> "" Then
                            txtApprovedAmount.Text = Format(CDec(txtApprovedAmount.Text), Session("fmtCurrency").ToString)
                        End If
                        If CBool(e.Item.Cells(e.Item.Cells.Count - 1).Text) Then
                            e.Item.Visible = True
                        Else
                            e.Item.Visible = False
                        End If
                    End If
                End If
            End If
            'Sohail (15 May 2018) -- Start
            'CCK Enhancement - Ref # 179 : On loan approval screen on MSS, not all approvers should see the basic salary of the staff. in 72.1.
            e.Item.Cells(3).Visible = CBool(Session("ViewScale"))
            'Sohail (15 May 2018) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("dgvData_ItemDataBound:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Protected Sub txtdgcolhInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvData.Items.Count <= 0 Then Exit Sub

            Dim txt As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txt.NamingContainer, DataGridItem)

            Dim intNoOfInstallments As Integer
            Integer.TryParse(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text, intNoOfInstallments)
            Dim decAAmount, decInstallmentAmt As Decimal
            Decimal.TryParse(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text, decAAmount)
            Decimal.TryParse(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text, decInstallmentAmt)

            If item Is Nothing OrElse CBool(item.Cells(13).Text) = True Then Exit Sub

            If decAAmount > 0 AndAlso decInstallmentAmt > 0 Then
                CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text = CInt(Math.Ceiling(decAAmount / decInstallmentAmt)).ToString
            Else
                CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text = CStr(1)
                'CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text = decAAmount.ToString
            End If

            txt.Text = CDec(txt.Text).ToString("############0.00#")

            'mdecInstallmentAmnt = CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text)
            mdtTable.Rows(item.ItemIndex)("noofinstallment") = CInt(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text)
            'mdtTable.Rows(item.ItemIndex)("installmentamt") = mdecInstallmentAmnt
            mdtTable.Rows(item.ItemIndex)("installmentamt") = CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text)
            'mdtTable.Rows(item.ItemIndex)("AppAmount") = CDec(CType(item.Cells(10).FindControl("txtdgcolhAAmount"), TextBox).Text)
            mdtTable.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("txtdgcolhInstallmentAmt_TextChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtdgcolhNoOfInstallments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvData.Items.Count <= 0 Then Exit Sub

            Dim txt As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txt.NamingContainer, DataGridItem)

            Dim intNoOfInstallments As Integer
            Integer.TryParse(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text, intNoOfInstallments)
            Dim decAAmount, decInstallmentAmt As Decimal
            Decimal.TryParse(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text, decAAmount)
            Decimal.TryParse(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text, decInstallmentAmt)

            If item Is Nothing OrElse CBool(item.Cells(13).Text) = True Then Exit Sub

            If decAAmount > 0 AndAlso intNoOfInstallments > 0 Then
                'mdecInstallmentAmnt = CDec(decAAmount / intNoOfInstallments)
                CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text = Format(CDec(decAAmount / intNoOfInstallments), Session("fmtCurrency").ToString)
            Else
                'mdecInstallmentAmnt = CDec(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text)
                CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text = CStr(1)
                CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text = decAAmount.ToString
            End If

            mdtTable.Rows(item.ItemIndex)("noofinstallment") = CInt(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text)
            mdtTable.Rows(item.ItemIndex)("installmentamt") = CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text)
            'mdtTable.Rows(item.ItemIndex)("installmentamt") = mdecInstallmentAmnt
            'mdtTable.Rows(item.ItemIndex)("AppAmount") = CDec(CType(item.Cells(10).FindControl("txtdgcolhAAmount"), TextBox).Text)
            mdtTable.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("txtdgcolhNoOfInstallments_TextChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtdgcolhAAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txt As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txt.NamingContainer, DataGridItem)

            Dim intNoOfInstallments As Integer
            Integer.TryParse(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text, intNoOfInstallments)
            Dim decAAmount, decInstallmentAmt As Decimal
            Decimal.TryParse(CType(item.Cells(11).FindControl("txtdgcolhAAmount"), TextBox).Text, decAAmount)
            Decimal.TryParse(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text, decInstallmentAmt)


            If item Is Nothing OrElse CBool(item.Cells(13).Text) = True Then Exit Sub

            If decAAmount > 0 AndAlso decInstallmentAmt > 0 Then
                intNoOfInstallments = CInt(Math.Ceiling(decAAmount / decInstallmentAmt))
                CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text = intNoOfInstallments.ToString

                mdtTable.Rows(item.ItemIndex)("AppAmount") = decAAmount
                'mdtTable.Rows(item.ItemIndex)("installmentamt") = CDec(CType(item.Cells(9).FindControl("txtdgcolhInstallmentAmt"), TextBox).Text)
                mdtTable.Rows(item.ItemIndex)("noofinstallment") = CInt(CType(item.Cells(10).FindControl("txtdgcolhNoOfInstallments"), TextBox).Text)
                mdtTable.AcceptChanges()
            End If

            txt.Text = CDec(txt.Text).ToString("############0.00#")

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("txtdgcolhAAmount_TextChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtdgcolhDuration_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    Dim txt As TextBox = CType(sender, TextBox)
        '    Dim item As DataGridItem = CType(txt.NamingContainer, DataGridItem)

        '    If item Is Nothing OrElse CBool(item.Cells(13).Text) = True Then Exit Sub

        '    If CInt(CType(item.Cells(8).FindControl("txtdgcolhDuration"), TextBox).Text) > 0 Then
        '        mdtTable.Rows(item.ItemIndex)("duration") = CInt(CType(item.Cells(8).FindControl("txtdgcolhDuration"), TextBox).Text)
        '        mdtTable.AcceptChanges()
        '    End If

        'Catch ex As Exception
        '    msg.DisplayMessage("txtdgcolhDuration_TextChanged:- " & ex.Message, Me)
        'End Try
    End Sub

#End Region

#Region "Combobox Event"

    Protected Sub cbodgcolhDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvData.Items.Count <= 0 Then Exit Sub

            Dim dropDownList As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(dropDownList.NamingContainer, DataGridItem)

            If item Is Nothing OrElse CBool(item.Cells(13).Text) = True Then Exit Sub

            If CInt(CType(item.Cells(7).FindControl("cbodgcolhDeductionPeriod"), DropDownList).SelectedValue) > 0 Then
                mdtTable.Rows(item.ItemIndex)("DeductionPeriodID") = CInt(CType(item.Cells(7).FindControl("cbodgcolhDeductionPeriod"), DropDownList).SelectedValue)
                mdtTable.Rows(item.ItemIndex)("DeductionPeriod") = (CType(item.Cells(7).FindControl("cbodgcolhDeductionPeriod"), DropDownList).SelectedItem.Text)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("cbodgcolhDeductionPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Protected Sub cboApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.SelectedIndexChanged
        Try
            If CInt(cboApprover.SelectedValue) > 0 Then
                Call btnSearch_Click(btnSearch, New EventArgs())
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("cboApprover_SelectedIndexChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (21-Jul-2016) -- End


#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

    'Nilay (01-Jun-2015) -- Start
    'New Loan Savings
#Region "RadioButton Events"

    Protected Sub radLoan_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        Try
            If radLoan.Checked = True Then
                'dgvData.Columns(6).Visible = True
                dgvData.Columns(9).Visible = True
                dgvData.Columns(10).Visible = True
                'dgvData.Columns(8).Visible = True

                dgvData.Width = Unit.Percentage(125)
                Call FillGrid()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("radLoan_CheckedChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub radAdvance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAdvance.CheckedChanged
        Try
            dgvData.Columns(9).Visible = False
            dgvData.Columns(10).Visible = False
            'dgvData.Columns(8).Visible = False

            dgvData.Width = Unit.Percentage(99)
            Call FillGrid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("radLoan_CheckedChanged:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region
    'Nilay (01-Jun-2015) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.gbPending.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbPending.ID, Me.gbPending.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            'Nilay (01-Jun-2015) -- Start
            'New Loan Savings
            'Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDepartment.ID, Me.lblDepartment.Text)
            'Me.lblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblSection.ID, Me.lblSection.Text)
            'Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblJob.ID, Me.lblJob.Text)
            'Me.lblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblBranch.ID, Me.lblBranch.Text)
            'Me.radAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.radAll.ID, Me.radAll.Text)
            'Nilay (01-Jun-2015) -- End
            Me.radAdvance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radAdvance.ID, Me.radAdvance.Text)
            Me.radLoan.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radLoan.ID, Me.radLoan.Text)

            Me.gbFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbFilter.ID, Me.gbFilter.Text)
            Me.gbInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbInfo.ID, Me.gbInfo.Text)

            Me.dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)
            Me.dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
