﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Approval_Process_wPg_LoanApprovalList
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmLoanApprovalList"
    Dim DisplayMessage As New CommonCodes
    Private objApprovaltran As New clsloanapproval_process_Tran
    Dim objLoanApprover As New clsLoanApprover_master
    Private mstrAdvanceFilter As String = ""
    Private dtApprovalList As New DataTable
    Private mstrEmployeeIDs As String = ""

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()

                chkMyApprovals.Checked = True

                If dgLoanApproval.Items.Count <= 0 Then
                    dgLoanApproval.DataSource = dtApprovalList
                    dgLoanApproval.DataBind()
                End If

            Else
                dtApprovalList = CType(Me.ViewState("ApprovalList"), DataTable)
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ApprovalList") = dtApprovalList
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objLoanApplication As New clsProcess_pending_loan
            Dim objMasterData As New clsMasterData
            Dim objLevel As New clslnapproverlevel_master
            Dim objLoanApprover As New clsLoanApprover_master

            dsList = objLoanApprover.GetEmployeeFromUser(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, _
                                                         CBool(Session("IsIncludeInactiveEmp")), _
                                                         "List")

            With cboEmployee
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoanApplication.GetLoan_Status("Status", True, False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "1"
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMasterData.GetCondition(False, True)
            dsList = objMasterData.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dtCondition
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dsListLevel As DataSet = objLevel.GetLevelFromUserLogin(CInt(Session("UserId")))

            If dsListLevel IsNot Nothing AndAlso dsListLevel.Tables(0).Rows.Count > 0 Then
                txtApprover.Text = dsListLevel.Tables(0).Rows(0)("ApproverName").ToString()
                objLoanApprover._lnApproverunkid = CInt(dsListLevel.Tables(0).Rows(0)("lnapproverunkid"))
            End If

            'Dim dsCombo As DataSet = Nothing
            'dsCombo = objLevel.GetLevelFromUserLogin(CInt(Session("UserId")))
            'With cboApprover
            '    .DataTextField = "Approver"
            '    .DataValueField = "priority"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            'If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count > 0 Then
            '    mintCurrentPriority = CInt(dsCombo.Tables(0).Compute("Min(priority)", "1=1"))
            '    cboApprover.SelectedValue = mintCurrentPriority
            'End If

            'dsCombo = Nothing
            'mintLowerPriority = objLevel.GetLowerLevelPriority(mintCurrentPriority)
            'objLevel = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewLoanApprovalList")) = False Then Exit Sub
            End If
            'Varsha Rana (17-Oct-2017) -- End


            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                strSearch &= "AND isloan = " & CInt(IIf(CInt(cboLoanAdvance.SelectedIndex) = 1, 1, 0)) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            'If txtExternalEntity.Text.Trim <> "" Then
            '    strSearch &= "AND external_entity_name like '" & txtExternalEntity.Text.Trim & "%' "
            'End If

            If txtApplicationNo.Text.Trim <> "" Then
                strSearch &= "AND lnloan_process_pending_loan.Application_No like '" & txtApplicationNo.Text.Trim & "%' "
            End If

            If txtLoanAmount.Text.Trim <> "" AndAlso CDec(txtLoanAmount.Text) > 0 Then
                strSearch &= "AND lnloanapproval_process_tran.loan_amount " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtLoanAmount.Text) & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND lnloan_process_pending_loan.application_date >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND lnloan_process_pending_loan.application_date >= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            'If chkMyApprovals.Checked = True Then
            '    strSearch &= "AND lnloan_approver_mapping.userunkid = " & CInt(Session("UserId")) & " "
            'Else
            '    chkMyApprovals.Checked = False
            '    If mstrEmployeeIDs.Trim.Length > 0 Then
            '        strSearch &= "AND lnloan_process_pending_loan.employeeunkid  in (" & mstrEmployeeIDs & ")" & " "
            '    End If
            'End If

            strSearch &= "AND lnloanapproval_process_tran.visibleid <> -1 " & " "

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    strSearch &= "AND " & mstrAdvanceFilter
            'End If
            'Nilay (01-Mar-2016) -- End

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objApprovaltran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                             "List", strSearch, mstrAdvanceFilter)
            'Nilay (01-Mar-2016) -- [mstrAdvanceFilter]

            Dim mintProcesspendingloanunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each drRow As DataRow In dsList.Tables(0).Rows

                    If CInt(drRow("pendingloantranunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintProcesspendingloanunkid <> CInt(drRow("processpendingloanunkid")) Then
                        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND processpendingloanunkid = " & CInt(drRow("processpendingloanunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintProcesspendingloanunkid = CInt(drRow("processpendingloanunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Approved By :-  ") & drRow("Approver").ToString()
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 1 Then

                                    If CInt(dr(i)("statusunkid")) = 2 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Approved By :-  ") & dr(i)("Approver").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Rejected By :-  ") & dr(i)("Approver").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Rejected By :-  ") & drRow("Approver").ToString()
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next

            End If

            dtApprovalList = dsList.Tables(0).Clone

            dtApprovalList.Columns("application_date").DataType = GetType(Object)

            Dim dtRow As DataRow

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dtRow = dtApprovalList.NewRow
                For Each dtCol As DataColumn In dtApprovalList.Columns
                    dtRow(dtCol.ColumnName) = dRow(dtCol.ColumnName)
                Next

                If CBool(dRow("Isgrp")) = True Then
                    dtRow("application_date") = dRow("application_no")
                End If
                dtApprovalList.Rows.Add(dtRow)
            Next

            dtApprovalList.AcceptChanges()

            Dim dtTable As New DataTable
            If chkMyApprovals.Checked Then
                dtTable = New DataView(dtApprovalList, "MappedUserId = " & CInt(Session("UserId")) & " OR MappedUserId <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                If mstrEmployeeIDs.Trim.Length > 0 Then
                    dtTable = New DataView(dtApprovalList, "employeeunkid  in (" & mstrEmployeeIDs & ") OR MappedUserId <= 0", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            'Pinkal (20-Sep-2017) -- Start
            'Bug : Issue-On Loan Approval list ,Grouping on application no. is not display

            Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "processpendingloanunkid", "employeeunkid")

            Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("processpendingloanunkid") Equals drTemp.Field(Of Integer)("processpendingloanunkid") And _
                         drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable


            If dRow1.Count > 0 Then
                dRow1.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            End If

            'Pinkal (20-Sep-2017) -- End

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            dgLoanApproval.Columns(0).Visible = CBool(Session("AllowToChangeLoanStatus"))
            'Varsha Rana (17-Oct-2017) -- End


            dgLoanApproval.AutoGenerateColumns = False
            dgLoanApproval.DataSource = dtTable
            dgLoanApproval.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Pinkal (20-Sep-2017) -- Start
    'Bug : Issue-On Loan Approval list ,Grouping on application no. is not display

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("processpendingloanunkid = " & CInt(dr("processpendingloanunkid").ToString()) & " AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "DeleteRow", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    'Pinkal (20-Sep-2017) -- End
#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dgLoanApproval.DataSource = New List(Of String)
            dgLoanApproval.DataBind()

            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboLoanAdvance.SelectedIndex = 0
            cboStatus.SelectedValue = "1"
            cboAmountCondition.SelectedIndex = 0

            txtApplicationNo.Text = ""
            txtLoanAmount.Text = "0"
            'txtExternalEntity.Text = ""

            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            'Nilay (01-Mar-2016) -- End
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Link Button's Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "emp."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgLoanApproval_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgLoanApproval.ItemCommand
        Try
            If e.CommandName.ToUpper = "STATUS" Then

                Dim objLoanMapping As New clsloan_approver_mapping
                objLoanMapping.GetData(CInt(e.Item.Cells(15).Text), CInt(Session("UserId")))

                If objLoanMapping._Approvertranunkid > 0 Then
                    objLoanApprover._lnApproverunkid = objLoanMapping._Approvertranunkid
                End If

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(14).Text) Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If

                Dim dsList As DataSet
                Dim dtList As DataTable

                Dim objapproverlevel As New clslnapproverlevel_master
                objapproverlevel._lnLevelunkid = objLoanApprover._lnLevelunkid

                dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                             CInt(e.Item.Cells(13).Text), CInt(e.Item.Cells(19).Text), "approvertranunkid <> " & CInt(e.Item.Cells(15).Text))

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                        If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                            dtList = New DataView(dsList.Tables(0), "lnlevelunkid = " & CInt(dsList.Tables(0).Rows(i)("lnlevelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                            If dtList.Rows.Count > 0 Then Continue For

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                                'Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                                'Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If

                        ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                                'Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                                'Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                If CDate(e.Item.Cells(2).Text).Date < DateAndTime.Now.Date Then

                    If CInt(CInt(e.Item.Cells(17).Text)) = 2 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(17).Text) = 3 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                        Exit Sub
                    End If
                Else
                    If CInt(e.Item.Cells(18).Text) = 3 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                        Exit Sub
                    End If
                End If

                Dim mintPendingloantranunkid As Integer = -1

                If CInt(e.Item.Cells(20).Text) > 0 Then
                    mintPendingloantranunkid = CInt(e.Item.Cells(20).Text)
                Else
                    mintPendingloantranunkid = objApprovaltran.GetLastApproverApprovalID(CInt(e.Item.Cells(19).Text), objapproverlevel._Priority)
                End If

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                Session("PendingLoanTranunkid") = mintPendingloantranunkid
                Session("Approverunkid") = e.Item.Cells(15).Text.ToString
                Session("ProcessPendingLoanunkid") = e.Item.Cells(19).Text.ToString

                'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApproval.aspx?" _
                '                  & HttpUtility.UrlEncode(clsCrypto.Encrypt(mintPendingloantranunkid.ToString & "|" & e.Item.Cells(15).Text.ToString & "|" & e.Item.Cells(19).Text.ToString)), False)
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApproval.aspx", False)
                'Nilay (06-Aug-2016) -- END

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgLoanApproval_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgLoanApproval_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLoanApproval.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End
            If e.Item.ItemIndex >= 0 Then

                If CBool(e.Item.Cells(12).Text) = True Then
                    CType(e.Item.Cells(0).FindControl("lnkChangeStatus"), LinkButton).Visible = False


                    e.Item.Cells(0).Visible = False
                    e.Item.Cells(1).Visible = False
                    e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 9
                    'e.Item.Cells(3).ColumnSpan = e.Item.Cells.Count - 10

                    For i = 3 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(2).Style.Add("text-align", "left")

                Else
                    If e.Item.Cells(2).Text.Trim <> "" AndAlso e.Item.Cells(2).Text <> "&nbsp;" Then
                        'Pinkal (16-Apr-2016) -- Start
                        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                        'e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo("")) 'Nilay (01-Apr-2016)
                        e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToShortDateString
                        'Pinkal (16-Apr-2016) -- End
                    End If

                    If e.Item.Cells(7).Text.Trim <> "" AndAlso e.Item.Cells(7).Text <> "&nbsp;" Then
                        e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)
                    End If

                    If e.Item.Cells(8).Text.Trim <> "" AndAlso e.Item.Cells(8).Text <> "&nbsp;" Then
                        e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString)
                    End If

                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgLoanApproval_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Protected Sub chkMyApprovals_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkMyApprovals_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApplicationNo.ID, Me.lblApplicationNo.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lblExternalEntity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblExternalEntity.ID, Me.lblExternalEntity.Text)
            Me.lblLoanAdvance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanAmount.ID, Me.lblLoanAmount.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgLoanApproval.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(1).FooterText, Me.dgLoanApproval.Columns(1).HeaderText)
            Me.dgLoanApproval.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(2).FooterText, Me.dgLoanApproval.Columns(2).HeaderText)
            Me.dgLoanApproval.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(3).FooterText, Me.dgLoanApproval.Columns(3).HeaderText)
            Me.dgLoanApproval.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(4).FooterText, Me.dgLoanApproval.Columns(4).HeaderText)
            Me.dgLoanApproval.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(5).FooterText, Me.dgLoanApproval.Columns(5).HeaderText)
            Me.dgLoanApproval.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(6).FooterText, Me.dgLoanApproval.Columns(6).HeaderText)
            Me.dgLoanApproval.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(7).FooterText, Me.dgLoanApproval.Columns(7).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
