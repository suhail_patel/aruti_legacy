﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Approver_wPg_LoanApproversList
    Inherits Basepage

#Region "Private Variables"
    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Private objLoanApprover As New clsLoanApprover_master
    'Hemant (04 Sep 2020) -- End
    Private Shared ReadOnly mstrModuleName As String = "frmLoanApproverList"
    Private mintUserMappunkid As Integer = -1
    Dim DisplayMessage As New CommonCodes
    Private mstrlnapproverunkid As Integer = -1
#End Region

#Region "Page's Events"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call SetLanguage()
                Call FillCombo()
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                Call SetVisibility()
                'Varsha Rana (17-Oct-2017) -- End

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                dgvApproversList.DataSource = New List(Of String)
                dgvApproversList.DataBind()
                'Nilay (01-Mar-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objLevel As New clslnapproverlevel_master
        Dim dsList As DataSet
        'Hemant (04 Sep 2020) -- End
        Try
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            'Dim objLevel As New clslnapproverlevel_master
            'Dim dsList As DataSet = objLevel.getListForCombo("Level", True)
            dsList = objLevel.getListForCombo("Level", True)
            'Hemant (04 Sep 2020) -- End

            With cboLoanLevel
                .DataValueField = "lnlevelunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Call cboLoanLevel_SelectedIndexChanged(cboLoanLevel, Nothing)

            cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Active"))
            cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "InActive"))
            cboStatus.SelectedIndex = 0

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objLevel = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objLoanApprover As New clsLoanApprover_master
        Dim dsApproverList As DataSet = Nothing
        Dim dtApprover As DataTable = Nothing
        'Hemant (04 Sep 2020) -- End
        Try

            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            'Dim dsApproverList As DataSet = Nothing
            'Dim dtApprover As DataTable = Nothing
            'Hemant (04 Sep 2020) -- End

            Dim strSearch As String = ""

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewLoanApproverList")) = False Then Exit Sub
            End If
            'Varsha Rana (17-Oct-2017) -- End


            If CInt(cboLoanLevel.SelectedValue) > 0 Then
                strSearch &= "AND lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue) & " "
            End If

            If CInt(cboLoanApprover.SelectedValue) > 0 Then
                strSearch &= "AND lnloanapprover_master.lnapproverunkid = " & CInt(cboLoanApprover.SelectedValue) & " "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsApproverList = objLoanApprover.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                 CBool(IIf(cboStatus.SelectedIndex = 0, True, False)), False, strSearch)

            dtApprover = New DataView(dsApproverList.Tables("List"), "", "priority,name", DataViewRowState.CurrentRows).ToTable

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            dgvApproversList.Columns(0).Visible = CBool(Session("AllowToEditLoanApprover"))
            dgvApproversList.Columns(1).Visible = CBool(Session("AllowToDeleteLoanApprover"))
            dgvApproversList.Columns(2).Visible = CBool(Session("AllowToSetActiveInactiveLoanApprover"))
            'Varsha Rana (17-Oct-2017) -- End


            dgvApproversList.DataSource = dtApprover
            dgvApproversList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objLoanApprover = Nothing
            If IsNothing(dsApproverList) = False Then
                dsApproverList.Clear()
                dsApproverList = Nothing
            End If
            If IsNothing(dtApprover) = False Then
                dtApprover.Clear()
                dtApprover = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    'Nilay (05-May-2016) -- Start
    Private Sub setATWebParameters(ByVal objLoanApprover As clsLoanApprover_master)
        Try
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objLoanApprover._WebClientIP = Session("IP_ADD").ToString
            objLoanApprover._WebFormName = "frmLoanApproverList"
            objLoanApprover._WebHostName = Session("HOST_NAME").ToString
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("setATWebParameters:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (05-May-2016) -- End


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()

        Try

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                btnNew.Enabled = CBool(Session("AllowToAddLoanApprover"))
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End


#End Region

#Region "ComboBox Events"

    Protected Sub cboLoanLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanLevel.SelectedIndexChanged
        Dim objlnApprover As New clsLoanApprover_master
        Dim dtTable As DataTable = Nothing
        Try

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboLoanLevel.SelectedValue) > 0 Then
            '    dtTable = objlnApprover.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, False, _
            '                                    "lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue)).Tables(0)
            'Else
            '    dtTable = objlnApprover.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                    "List", True, False).Tables(0)
            'End If

            dtTable = objlnApprover.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), _
                                            True, _
                                            CBool(Session("IsIncludeInactiveEmp")), _
                                            "List", True, False, _
                                            CStr(IIf(CInt(cboLoanLevel.SelectedValue) > 0, "lnapproverlevel_master.lnlevelunkid = " & CInt(cboLoanLevel.SelectedValue), ""))).Tables(0)
            'Nilay (01-Mar-2016) -- End

            Dim drRow As DataRow = dtTable.NewRow
            drRow("lnapproverunkid") = 0
            drRow("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Select")
            dtTable.Rows.InsertAt(drRow, 0)

            With cboLoanApprover
                .DataTextField = "name"
                .DataValueField = "lnapproverunkid"
                .DataSource = dtTable
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboLoanLevel_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApprover = Nothing
            If IsNothing(dtTable) = False Then
                dtTable.Clear()
                dtTable = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approver/wPg_AddEditLoanApprover.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboLoanApprover.SelectedValue) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage("Sorry, Loan Approver is mandatory information. Please select Loan Approver to continue.", Me)
            '    Exit Sub
            'End If
            'Call FillList()
            'If cboStatus.SelectedIndex = 0 Then
            '    If CInt(cboLoanApprover.SelectedValue) <= 0 Then
            '        'Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage("Sorry, Loan Approver is mandatory information. Please select Loan Approver to continue.", Me)
            '        cboLoanApprover.Focus()
            '        dgvApproversList.DataSource = New List(Of String)
            '        Call FillList()
            '        Exit Sub
            '    End If
            'End If
            Call FillList()
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboLoanLevel.SelectedIndex = 0
            cboLoanApprover.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            dgvApproversList.DataSource = New List(Of String)
            dgvApproversList.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objLoanApprover As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End
        Try
            If dgvApproversList.Items.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), Me)
            End If
            If popup_DeleteReason.Reason.Trim.Length <= 0 Then
                Exit Sub
            Else
                objLoanApprover._VoidReason = popup_DeleteReason.Reason.ToString
            End If

            objLoanApprover._Isvoid = True
            objLoanApprover._VoidDateTime = DateAndTime.Now.Date
            objLoanApprover._VoidUserunkid = CInt(Session("UserId"))
            'Nilay (05-May-2016) -- Start
            Call setATWebParameters(objLoanApprover)
            'Hemant (04 Sep 2020) -- [objLoanApprover]
            'Nilay (05-May-2016) -- End

            objLoanApprover.Delete(CInt(Me.ViewState("LoanApproverunkid")))

            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popup_DeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            popup_DeleteReason.Reason = ""
            Me.ViewState("LoanApproverunkid") = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            objLoanApprover = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region "DataGrid events"

    Protected Sub dgvApproversList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvApproversList.ItemCommand
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objLoanApprover As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End
        Try
            If e.CommandName.ToUpper = "EDIT" Then
                Session.Add("lnapproverunkid", e.Item.Cells(4).Text)
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approver/wPg_AddEditLoanApprover.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                Me.ViewState.Add("LoanApproverunkid", e.Item.Cells(4).Text)
                'Language.setLanguage(mstrModuleName)
                popup_DeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Are you sure you want to delete this Approver?")
                popup_DeleteReason.Show()

            ElseIf e.CommandName.ToUpper = "INACTIVE" Then
                objLoanApprover._Isactive = False
                'Nilay (05-May-2016) -- Start
                Call setATWebParameters(objLoanApprover)
                'Hemant (04 Sep 2020) -- [objLoanApprover]
                'Nilay (05-May-2016) -- End

                If objLoanApprover.MakeActiveInActive(CInt(e.Item.Cells(4).Text)) = False Then
                    DisplayMessage.DisplayMessage(objLoanApprover._Message, Me)
                    Exit Sub
                Else
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Approver InActive successfully."), Me)
                    Call FillList()
                End If

            ElseIf e.CommandName.ToUpper = "ACTIVE" Then
                objLoanApprover._Isactive = True
                'Nilay (05-May-2016) -- Start
                Call setATWebParameters(objLoanApprover)
                'Hemant (04 Sep 2020) -- [objLoanApprover]
                'Nilay (05-May-2016) -- End

                If objLoanApprover.MakeActiveInActive(CInt(e.Item.Cells(4).Text)) = False Then
                    DisplayMessage.DisplayMessage(objLoanApprover._Message, Me)
                    Exit Sub
                Else
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Approver Active successfully."), Me)
                    Call FillList()
                End If


            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvApproversList_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objLoanApprover = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub dgvApproversList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvApproversList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                If cboStatus.SelectedItem.Text.ToUpper = "ACTIVE" Then
                    CType(e.Item.Cells(2).FindControl("lnkSetActive"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkSetInActive"), LinkButton).Visible = True
                Else
                    CType(e.Item.Cells(2).FindControl("lnkSetInActive"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkSetActive"), LinkButton).Visible = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)   'Hemant (13 Aug 2020)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblLoanLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanLevel.ID, Me.lblLoanLevel.Text)
            Me.lblLoanApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanApprover.ID, Me.lblLoanApprover.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
