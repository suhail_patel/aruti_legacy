﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_GlobalAssignLoanAdvance.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_GlobalAssignLoanAdvance"
    MaintainScrollPositionOnPostback="true" Title="Global Assigned Loan / Advance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
        
    </script>--%>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
            RateOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);
        prm.add_endRequest(RateOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }

        function RateOnly() {

            $(".percentage").keyup(function(e) {
                if (($(this).val() < 0 || $(this).val() > 100)) {
                    $(this).val(0);
                    return false;
                }
            });
            $(".percentage").keypress(function(e) {
                if (($(this).val().indexOf('.') != -1) && $(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2) {
                    return false;
                }
                if (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollLeft($(scroll.Y).val());
                $("#scrollable-container").scrollTop($(scroll1.Y).val());
            }
            $("a[title='Collapse']").children().children().attr('class', 'fa fa-chevron-circle-down');
            $("a[title='Expand']").children().children().attr('class', 'fa fa-chevron-circle-up');
        }
    </script>

    <%--<script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc3:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Global Assigned Loan/Advance"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" ToolTip="Allocation">
                                                                <i class="fas fa-sliders-h"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMode" runat="server" Text="Mode" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboMode" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoan" runat="server" Text="Loan" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLoan" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApprover" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblBasicInfo" runat="server" Text="Basic Info" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Assigned Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPayPeriod" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblVoucherNo" runat="server" Text="Voc #" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtPrefix" runat="server" Text="ISGLA_" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtManualNo" runat="server" Text="1" Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblGLoanCalcType" runat="server" Text="Loan Calculation" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboGLoanCalcType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblInterestCalcType" runat="server" Text="Interest Calculation" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboGInterestCalcType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMappedHead" runat="server" Text="Mapped Head" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboMappedHead" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblGRate" runat="server" Text="Rate %" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtGRate" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                    Text="0" Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPurpose" runat="server" Text="Purpose" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPurpose" TextMode="MultiLine" runat="server" Rows="3" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                
                                                    <div style="float: left">
                                                        <asp:RadioButton ID="radApplyToChecked" runat="server" Text="Apply To Checked" GroupName="APPLY"  />
                                                        <asp:RadioButton ID="radApplyToAll" runat="server" Text="Apply To All" GroupName="APPLY" />
                                                    </div>
                                                
                                                <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvDataList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                CssClass="table table-hover table-bordered" RowStyle-Wrap="false" HeaderStyle-Font-Bold="false"
                                                Width="175%">
                                                <ItemStyle />
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:TemplateColumn FooterText="objdgcolhCollapse">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgCollapse" runat="server" ToolTip="Collapse" CommandName="ExpCol">
                                                            <div style="color:#004474; text-align:center"><i id="expcol" class="fas fa-chevron-circle-up"></i></div>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle />
                                                        <ItemStyle />
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="IsCheck" HeaderText="IsCheck" Visible="false"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objdgcolhSelect">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"
                                                                Text=" " />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="AppNo" HeaderText="Application No." FooterText="dgcolhAppNo">
                                                        <HeaderStyle />
                                                        <ItemStyle />
                                                    </asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEmployee"
                                                        HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <HeaderStyle Width="20%" />
                                                        <ItemStyle Width="20%" />
                                                    </asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="Approver" HeaderText="Approver" FooterText="dgcolhApprover" />
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="Amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAmount">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <%--7--%>
                                                    <asp:TemplateColumn HeaderText="Deduction Period" ItemStyle-HorizontalAlign="Center"
                                                        FooterText="dgcolhDeductionPeriod">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cbodgcolhDeductionPeriod" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="cbodgcolhDeductionPeriod_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="dstartdate" HeaderText="Effective Date" FooterText="dgcolhEffDate">
                                                        <HeaderStyle />
                                                        <ItemStyle />
                                                    </asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:TemplateColumn HeaderText="Loan Calculation" HeaderStyle-Width="150px" ItemStyle-Width="150px"
                                                        ItemStyle-HorizontalAlign="Center" FooterText="dgcolhLoanType">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cbodgcolhLoanType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cbodgcolhLoanType_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--10--%>
                                                    <asp:TemplateColumn HeaderText="Interest Calculation" ItemStyle-HorizontalAlign="Center"
                                                        FooterText="dgcolhInterestType">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cbodgcolhInterestCalcType" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="cbodgcolhInterestCalcType_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--11--%>
                                                    <asp:TemplateColumn HeaderText="Mapped Head" ItemStyle-HorizontalAlign="Center" FooterText="dgcolhMappedHead">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cbodgcolhMappedHead" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cbodgcolhMappedHead_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="rate" HeaderText="Rate" Visible="false"></asp:BoundColumn>
                                                    <%--13--%>
                                                    <asp:TemplateColumn HeaderText="Rate %" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="90px"
                                                        FooterText="dgcolhRate">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhRate" onKeypress="return onlyNumbers(this, event);" Style="text-align: right"
                                                                        runat="server" AutoPostBack="True" Text='<%# Eval("rate") %>' OnTextChanged="txtdgcolhRate_TextChanged"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle />
                                                    </asp:TemplateColumn>
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="noofinstallment" HeaderText="No. Of Instl." Visible="false">
                                                    </asp:BoundColumn>
                                                    <%--15--%>
                                                    <asp:TemplateColumn HeaderText="No. Of Instl." HeaderStyle-HorizontalAlign="Right"
                                                        FooterText="dgcolhNoOfEMI">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhNoOfEMI" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right" runat="server" AutoPostBack="True" Text='<%# Eval("noofinstallment") %>'
                                                                        OnTextChanged="txtdgcolhNoOfEMI_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle />
                                                    </asp:TemplateColumn>
                                                    <%--16--%>
                                                    <asp:BoundColumn DataField="installmentamt" HeaderText="Installment Amount" Visible="false">
                                                    </asp:BoundColumn>
                                                    <%--17--%>
                                                    <asp:TemplateColumn HeaderText="Installment Amount" HeaderStyle-HorizontalAlign="Right"
                                                        FooterText="dgcolhInstallmentAmount">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhInstallmentAmount" Style="text-align: right" runat="server"
                                                                        AutoPostBack="True" Text='<%# Eval("installmentamt") %>' OnTextChanged="txtdgcolhInstallmentAmount_TextChanged"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle />
                                                    </asp:TemplateColumn>
                                                    <%--18--%>
                                                    <asp:BoundColumn DataField="IsGrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp">
                                                    </asp:BoundColumn>
                                                    <%--19--%>
                                                    <asp:BoundColumn DataField="IsEx" HeaderText="objdgcolhIsEx" Visible="false" FooterText="objdgcolhIsEx">
                                                    </asp:BoundColumn>
                                                    <%--20--%>
                                                    <asp:BoundColumn DataField="Pendingunkid" HeaderText="objdgcolhPendingunkid" Visible="false"
                                                        FooterText="objdgcolhPendingunkid"></asp:BoundColumn>
                                                    <%--21--%>
                                                    <asp:BoundColumn DataField="GrpId" HeaderText="objdgcolhGrpId" Visible="false" FooterText="objdgcolhGrpId">
                                                    </asp:BoundColumn>
                                                    <%--22--%>
                                                    <asp:BoundColumn DataField="ApprId" HeaderText="objdgcolhApprId" Visible="false"
                                                        FooterText="objdgcolhApprId"></asp:BoundColumn>
                                                    <%--23--%>
                                                    <asp:BoundColumn DataField="EmpId" HeaderText="objdgcolhEmpId" Visible="false" FooterText="objdgcolhEmpId">
                                                    </asp:BoundColumn>
                                                    <%--24--%>
                                                    <asp:BoundColumn DataField="deductionperiodunkid" HeaderText="objdgcolhDeductionPeriodId"
                                                        Visible="false" FooterText="objdgcolhDeductionPeriodId"></asp:BoundColumn>
                                                    <%--25--%>
                                                    <asp:BoundColumn DataField="calctypeId" HeaderText="objdgcolhCalcTypeId" Visible="false"
                                                        FooterText="objdgcolhCalcTypeId"></asp:BoundColumn>
                                                    <%--26--%>
                                                    <asp:BoundColumn DataField="instlamt" HeaderText="objdgMatchInstallmentAmt" Visible="false"
                                                        FooterText="objdgMatchInstallmentAmt"></asp:BoundColumn>
                                                    <%--27--%>
                                                    <asp:BoundColumn DataField="intrsamt" HeaderText="objdgcolhInterestAmt" Visible="false"
                                                        FooterText="objdgcolhInterestAmt"></asp:BoundColumn>
                                                    <%--28--%>
                                                    <asp:BoundColumn DataField="netamt" HeaderText="objdgcolhNetAmount" Visible="false"
                                                        FooterText="objdgcolhNetAmount"></asp:BoundColumn>
                                                    <%--29--%>
                                                    <asp:BoundColumn DataField="interest_calctype_id" HeaderText="objdgcolhIntCalcTypeId"
                                                        Visible="false" FooterText="objdgcolhIntCalcTypeId"></asp:BoundColumn>
                                                    <%--30--%>
                                                    <asp:BoundColumn DataField="mapped_tranheadunkid" HeaderText="objdgcolhMappedHead"
                                                        Visible="false" FooterText="objdgcolhMappedHead"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAssign" runat="server" Text="Assign" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
