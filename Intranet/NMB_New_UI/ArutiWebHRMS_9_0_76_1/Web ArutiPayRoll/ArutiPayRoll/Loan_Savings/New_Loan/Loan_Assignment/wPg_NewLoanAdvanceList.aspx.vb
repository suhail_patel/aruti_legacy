﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Assignment_wPg_NewLoanAdvanceList
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmNewLoanAdvanceList"
    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Private ReadOnly mstrModuleName1 As String = "frmBBL_Loan_Report"
    'Hemant (12 Nov 2021) -- End
    Private DisplayMessage As New CommonCodes
    Private objLoan_Advance As clsLoan_Advance
    Private objBBL As clsBBL_Loan_Report
    Private mstrFilterTitle As String = String.Empty
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintEmployeeunkid As Integer = -1
    Private mintLoanAdvanceunkid As Integer = -1
    Private mdtLoanAdvanceList As DataTable = Nothing
    Private mdtLoanBalance As DataTable = Nothing
    'Nilay (23-Aug-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private mblnIsLoan As Boolean = True
    Private mintProcessPendingUnkid As Integer = -1
    'Nilay (23-Aug-2016) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))

                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    btnPreview.Visible = True
                    dgvLoanAdvanceList.Columns(0).Visible = True
                    dgvLoanAdvanceList.Columns(1).Visible = True
                    dgvLoanAdvanceList.Columns(2).Visible = True
                    dgvLoanAdvanceList.Columns(3).Visible = True
                    dgvLoanAdvanceList.Columns(4).Visible = True
                    dgvLoanAdvanceList.Columns(5).Visible = True
                    ' Varsha Rana (12-Sept-2017) -- End


                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))

                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    btnPreview.Visible = False
                    dgvLoanAdvanceList.Columns(0).Visible = False
                    dgvLoanAdvanceList.Columns(1).Visible = False
                    dgvLoanAdvanceList.Columns(2).Visible = False
                    dgvLoanAdvanceList.Columns(3).Visible = False
                    dgvLoanAdvanceList.Columns(4).Visible = False
                    dgvLoanAdvanceList.Columns(5).Visible = False
                    'Varsha Rana (12-Sept-2017) -- End


                End If
                'Nilay (10-Dec-2016) -- End

                Call SetLanguage()
                Call FillCombo()
                'Call FillList()

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 57
                If Request.QueryString.Count > 0 Then
                    Dim intEmpId, intSchemeId As Integer
                    intEmpId = 0 : intSchemeId = 0
                    Try
                        Dim strvalue() As String = Nothing
                        strvalue = b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3))).Split(CChar("|"))
                        If strvalue.Length > 0 Then
                            intEmpId = CInt(strvalue(0))
                            intSchemeId = CInt(strvalue(1))
                        End If
                    Catch
                        Session("clsuser") = Nothing
                        DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../../index.aspx")
                        Exit Sub
                    End Try
                    cboEmployee.SelectedValue = CStr(intEmpId) : cboLoanScheme.SelectedValue = CStr(intSchemeId)
                    btnSearch_Click(New Object(), New EventArgs)
                    btnSearch.Enabled = False : btnReset.Enabled = False
                    cboEmployee.Enabled = False : cboLoanScheme.Enabled = False
                End If
                'S.SANDEEP [20-SEP-2017] -- END


                If dgvLoanAdvanceList.Items.Count <= 0 Then
                    dgvLoanAdvanceList.DataSource = New List(Of String)
                    dgvLoanAdvanceList.DataBind()
                End If

                Dim intOpenPeriodId As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), _
                                                                                  1, , True)
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = intOpenPeriodId
                lblCurrOpenPeriod.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "* Balance As On:") & " " & objPeriod._Period_Name.ToString

                'Nilay (04-Nov-2016) -- Start
                'Enhancements: Global Change Status feature requested by {Rutta}
                btnGlobalChangeStatus.Visible = CBool(Session("AllowChangeLoanAvanceStatus"))
                Dim dgv = dgvLoanAdvanceList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanAdvanceList.Columns.IndexOf(x))
                If CBool(Session("AllowChangeLoanAvanceStatus")) = False Then
                    dgvLoanAdvanceList.Columns(dgv("btnChangeStatus")).Visible = False
                    dgvLoanAdvanceList.Width = Unit.Percentage(200)
                Else
                    dgvLoanAdvanceList.Columns(dgv("btnChangeStatus")).Visible = True
                    dgvLoanAdvanceList.Width = Unit.Percentage(225)
                End If
                'Nilay (04-Nov-2016) -- End
            Else
                mdtLoanAdvanceList = CType(Me.ViewState("LoanAdvanceList"), DataTable)
                mstrBaseCurrSign = Me.ViewState("BaseCurrSign").ToString
                mdtLoanBalance = CType(Me.ViewState("dtLoanBalance"), DataTable)
                mintEmployeeunkid = CInt(Me.ViewState("Employeeunkid"))
                mintLoanAdvanceunkid = CInt(Me.ViewState("LoanAdvanceunkid"))
                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                mblnIsLoan = CBool(Me.ViewState("IsLoan"))
                mintProcessPendingUnkid = CInt(Me.ViewState("ProcessPendingId"))
                'Nilay (23-Aug-2016) -- End

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("LoanAdvanceList") = mdtLoanAdvanceList
            Me.ViewState("BaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("dtLoanBalance") = mdtLoanBalance
            Me.ViewState("Employeeunkid") = mintEmployeeunkid
            Me.ViewState("LoanAdvanceunkid") = mintLoanAdvanceunkid
            'Nilay (23-Aug-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.ViewState("IsLoan") = mblnIsLoan
            Me.ViewState("ProcessPendingId") = mintProcessPendingUnkid
            'Nilay (23-Aug-2016) -- End

            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            'Nilay (02-Jul-2016) -- Start
            Dim objOtherOpApproval As New clsloanotherop_approval_tran
            'Nilay (02-Jul-2016) -- End
            objLoan_Advance = New clsLoan_Advance 'Nilay (04-Nov-2016) -- Start


            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                ' Varsha Rana (12-Sept-2017) -- End
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, True, False, _
                                                     "Employee", True)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With

                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If
            ' Varsha Rana (12-Sept-2017) -- End
            objEmployee = Nothing


            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            With cboAFLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Hemant (12 Nov 2021) -- End
            objLoanScheme = Nothing

            dsList = objMaster.GetLoan_Saving_Status("Status", False)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(False, True)
            dsList = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dtCondition
                .DataBind()
                .SelectedIndex = 0
            End With
            dtCondition = Nothing
            objMaster = Nothing

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                               CInt(Session("Fin_year")), _
                                               Session("Database_Name").ToString, _
                                               CDate(Session("fin_startdate")), _
                                               "Period", True)
            With cboAssignPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
                .SelectedValue = "0"
            End With
            objPeriod = Nothing

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Select"))
            cboLoanAdvance.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Loan"))
            cboLoanAdvance.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
            End With

            Dim dtTable As DataTable = New DataView(dsList.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = mintBaseCurrId.ToString
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If

            'Nilay (02-Jul-2016) -- Start
            dsList = objOtherOpApproval.getComboOtherOpStatus("List", True)
            With cboOtherOpStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Nilay (02-Jul-2016) -- End

            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Nilay (04-Nov-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()

        objLoan_Advance = New clsLoan_Advance
        Dim dsLoanAdvance As New DataSet
        Dim strSearch As String = String.Empty
        Dim objExchangeRate As New clsExchangeRate
        'Hemant (16 Jan 2019) -- Start
        'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
        Dim strExtraReportFilterString As String = String.Empty
        'Hemant (16 Jan 2019) -- End
        Try
            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewLoanAdvanceList")) = False Then Exit Sub
            End If
            ' If CBool(Session("AllowToViewLoanAdvanceList")) = True Then
            ' Varsha Rana (12-Sept-2017) -- End
            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND LN.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblEmployee.Text & " : " & cboEmployee.Text
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND LN.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblLoanScheme.Text & " : " & cboLoanScheme.Text
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND LN.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblStatus.Text & " : " & cboStatus.Text
            End If

            If CInt(cboAssignPeriod.SelectedValue) > 0 Then
                strSearch &= "AND LN.periodunkid = " & CInt(cboAssignPeriod.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblPayPeriod.Text & " : " & cboAssignPeriod.Text
                'Hemant (16 Jan 2019) -- Start
                'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                strExtraReportFilterString &= "AND lnloan_advance_tran.periodunkid = " & CInt(cboAssignPeriod.SelectedValue) & " "
                'Hemant (16 Jan 2019) -- End
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                strSearch &= "AND LN.Loan_AdvanceUnkid = " & CInt(cboLoanAdvance.SelectedIndex) & " "
                mstrFilterTitle &= ", " & lblLoanAdvance.Text & " : " & cboLoanAdvance.Text
            End If

            If txtAmount.Text.Trim <> "" Then
                If CDec(txtAmount.Text) > 0 Then
                    strSearch &= "AND LN.Amount " & cboCondition.SelectedItem.Text.Trim & CDec(txtAmount.Text) & " "
                    mstrFilterTitle &= ", " & lblAmount.Text & " : " & txtAmount.Text
                End If
            End If

            If txtVocNo.Text.Trim <> "" Then
                strSearch &= "AND LN.VocNo LIKE '%" & CStr(txtVocNo.Text) & "%'" & " "
                mstrFilterTitle &= ", " & lblVocNo.Text & " : " & txtVocNo.Text
                'Hemant (16 Jan 2019) -- Start
                'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                strExtraReportFilterString &= "AND lnloan_advance_tran.loanvoucher_no LIKE '%" & CStr(txtVocNo.Text) & "%'" & " "
                'Hemant (16 Jan 2019) -- End
            End If

            If CInt(cboCurrency.SelectedValue) > 0 Then
                strSearch &= "AND LN.countryunkid = '" & cboCurrency.SelectedValue.ToString & "' "
                mstrFilterTitle &= ", " & lblCurrency.Text & " : " & cboCurrency.SelectedItem.Text
                'Hemant (16 Jan 2019) -- Start
                'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                strExtraReportFilterString &= "AND lnloan_advance_tran.countryunkid = '" & cboCurrency.SelectedValue.ToString & "' "
                'Hemant (16 Jan 2019) -- End
            Else
                mstrFilterTitle &= ", " & lblCurrency.Text & " : ALL"
            End If

            'Nilay (02-Jul-2016) -- Start
            If CInt(cboOtherOpStatus.SelectedValue) > 0 Then
                strSearch &= "AND LN.opstatus_id = " & CInt(cboOtherOpStatus.SelectedValue)
                mstrFilterTitle &= ", " & lblOtherOpStatus.Text & " : " & cboOtherOpStatus.SelectedItem.Text 'Nilay (04-Nov-2016)
            End If
            'Nilay (02-Jul-2016) -- End

            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            If CInt(cboCalcType.SelectedValue) > 0 Then
                strSearch &= "AND LN.calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblCalcType.Text & " : " & cboCalcType.SelectedItem.Text
                'Hemant (16 Jan 2019) -- Start
                'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                strExtraReportFilterString &= "AND lnloan_advance_tran.calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
                'Hemant (16 Jan 2019) -- End
            End If
            'Nilay (04-Nov-2016) -- End

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                mstrFilterTitle = mstrFilterTitle.Substring(2)
            Else
                mstrFilterTitle = mstrFilterTitle.Substring(2)
            End If

            Dim objclsMaster As New clsMasterData

            Dim intFirstOpenPeriodID As Integer = objclsMaster.getFirstPeriodID(enModuleReference.Payroll, _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("Fin_year")), , True)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = intFirstOpenPeriodID

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            'dsLoanAdvance = objLoan_Advance.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
            '                                        objPeriod._Start_Date, _
            '                                        objPeriod._End_Date, _
            '                                        objPeriod._End_Date, _
            '                                        Session("UserAccessModeSetting").ToString, "List", strSearch)

            dsLoanAdvance = objLoan_Advance.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
                                                    objPeriod._Start_Date, _
                                                    objPeriod._End_Date, _
                                                    objPeriod._End_Date, _
                                                Session("UserAccessModeSetting").ToString, "List", strSearch, , _
                                                CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))


            ' Varsha Rana (12-Sept-2017) -- End

            mdtLoanAdvanceList = dsLoanAdvance.Tables(0).Clone

            'Nilay (25-Mar-2016) -- Start
            'Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo("Balance", objPeriod._End_Date.AddDays(1), _
            '                                       IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue.ToString, "").ToString, _
            '                                       0, objPeriod._Start_Date, True, True, "", True, True, _
            '                                       CBool(Session("FirstNamethenSurname")), True, True)

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS


            'Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                                                         CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, _
            '                                                                         CStr(Session("UserAccessModeSetting")), True, "Balance", _
            '                                                                         objPeriod._End_Date.AddDays(1), _
            '                                        IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue.ToString, "").ToString, _
            '                                        0, objPeriod._Start_Date, True, True, "", True, True, _
            '                                        CBool(Session("FirstNamethenSurname")), True, True)
            'Hemant (16 Jan 2019) -- Start
            'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
            'Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                                                             CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, _
            '                                                                             CStr(Session("UserAccessModeSetting")), True, "Balance", _
            '                                                                             objPeriod._End_Date.AddDays(1), _
            '                                            IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue.ToString, "").ToString, _
            '                                            0, objPeriod._Start_Date, True, True, "", True, True, _
            '                                       CBool(Session("FirstNamethenSurname")), True, True, , , , , _
            '                                       CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

            Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                                     CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, _
                                                                                     CStr(Session("UserAccessModeSetting")), True, "Balance", _
                                                                                     objPeriod._End_Date.AddDays(1), _
                                                    IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue.ToString, "").ToString, _
                                                                0, objPeriod._Start_Date, True, True, cboLoanScheme.SelectedValue.ToString, True, True, _
                                                           CBool(Session("FirstNamethenSurname")), True, True, strExtraReportFilterString, , , , _
                                               CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))


            'Hemant (16 Jan 2019) -- End

            ' Varsha Rana (12-Sept-2017) -- End
            'Nilay (25-Mar-2016) -- End


            mdtLoanBalance = dsLoanBalance.Tables("Balance")


            Dim decTotalBalance As Decimal = 0
            Dim decTotalPaid As Decimal = 0
            Dim dRow As DataRow

            mdtLoanAdvanceList.Columns.Add("loanbalance", Type.GetType("System.String"))
            mdtLoanAdvanceList.Columns.Add("paidlaon", Type.GetType("System.String"))
            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            Dim objPaymentTran As New clsPayment_tran
            ' Varsha Rana (12-Sept-2017) -- End

            For Each dtRow As DataRow In dsLoanAdvance.Tables(0).Rows
                dRow = mdtLoanAdvanceList.NewRow
                For Each dtCol As DataColumn In dsLoanAdvance.Tables(0).Columns
                    dRow(dtCol.ColumnName) = dtRow(dtCol.ColumnName)
                Next
                Dim intLoanAdvanceTranunkid As Integer = CInt(dtRow.Item("loanadvancetranunkid"))
                Dim dr_Row As List(Of DataRow) = (From p In dsLoanBalance.Tables("Balance") Where (CInt(p.Item("loanadvancetranunkid")) = intLoanAdvanceTranunkid) Select (p)).ToList
                If dr_Row.Count > 0 Then


                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    Dim ePRef As clsPayment_tran.enPaymentRefId
                    If CBool(dtRow.Item("isloan")) = True Then
                        ePRef = clsPayment_tran.enPaymentRefId.LOAN
                    Else
                        ePRef = clsPayment_tran.enPaymentRefId.ADVANCE
                    End If
                    ' Varsha Rana (12-Sept-2017) -- End

                    If CInt(cboCurrency.SelectedValue) > 0 Then

                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        If CBool(dtRow("isexternal_entity")) = True OrElse objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, CInt(dtRow.Item("loanadvancetranunkid")), ePRef) = True Then
                            ' Varsha Rana (12-Sept-2017) -- End
                            dRow("loanbalance") = Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), Session("fmtCurrency").ToString.ToString.ToString)

                            'Varsha Rana (12-Sept-2017) -- Start
                            'Enhancement - Loan Topup in ESS
                            'dRow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), Session("fmtCurrency").ToString.ToString)
                            dRow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString)
                            ' Varsha Rana (12-Sept-2017) -- End

                            decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), Session("fmtCurrency").ToString.ToString))

                            'Varsha Rana (12-Sept-2017) -- Start
                            'Enhancement - Loan Topup in ESS
                            'decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), Session("fmtCurrency").ToString.ToString))
                            decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString))
                            dRow("Amount") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString)
                        Else
                            dRow("loanbalance") = Format(0, Session("fmtCurrency").ToString.ToString.ToString)
                            dRow("paidlaon") = Format(0, Session("fmtCurrency").ToString)
                            decTotalBalance += 0
                            decTotalPaid += 0

                        End If
                        dRow("Amount") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString)

                        ' Varsha Rana (12-Sept-2017) -- End

                    Else
                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        If CBool(dtRow("isexternal_entity")) = True OrElse objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, CInt(dtRow.Item("loanadvancetranunkid")), ePRef) = True Then
                            ' Varsha Rana (12-Sept-2017) -- End

                            dRow("loanbalance") = Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), Session("fmtCurrency").ToString.ToString)
                            'Varsha Rana (12-Sept-2017) -- Start
                            'Enhancement - Loan Topup in ESS
                            'dRow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), Session("fmtCurrency").ToString.ToString)
                            dRow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString.ToString)
                            ' Varsha Rana (12-Sept-2017) -- End
                            dRow("cSign") = mstrBaseCurrSign
                            decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), Session("fmtCurrency").ToString.ToString))
                            'Varsha Rana (12-Sept-2017) -- Start
                            'Enhancement - Loan Topup in ESS
                            'decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), Session("fmtCurrency").ToString.ToString))
                            decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString.ToString))

                            dRow("Amount") = Format(CDec(dr_Row(0).Item("loan_amount")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString.ToString)
                        Else
                            dRow("loanbalance") = Format(0, Session("fmtCurrency").ToString.ToString.ToString)
                            dRow("paidlaon") = Format(0, Session("fmtCurrency").ToString)
                            decTotalBalance += 0
                            decTotalPaid += 0

                        End If
                        dRow("Amount") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) + CDec(dr_Row(0).Item("total_topup")), Session("fmtCurrency").ToString)
                        ' Varsha Rana (12-Sept-2017) -- End

                    End If
                Else
                    dRow("loanbalance") = Format(0, Session("fmtCurrency").ToString)
                    dRow("paidlaon") = Format(0, Session("fmtCurrency").ToString)
                    decTotalBalance += CDec(Format(0, Session("fmtCurrency").ToString))
                    decTotalPaid += CDec(Format(0, Session("fmtCurrency").ToString))
                End If
                mdtLoanAdvanceList.Rows.Add(dRow)
            Next
            If dsLoanAdvance.Tables(0).Rows.Count > 0 Then
                dRow = mdtLoanAdvanceList.NewRow
                dRow("Employee") = "GRAND TOTAL :"
                dRow("loanbalance") = decTotalBalance.ToString
                dRow("paidlaon") = decTotalPaid.ToString
                dRow("Amount") = (From p In mdtLoanAdvanceList Select (CDec(p.Item("Amount")))).DefaultIfEmpty.Sum()
                dRow("basecurrency_amount") = (From p In mdtLoanAdvanceList Select (CDec(p.Item("basecurrency_amount")))).DefaultIfEmpty.Sum()
                dRow("loanadvancetranunkid") = -1
                mdtLoanAdvanceList.Rows.Add(dRow)
            End If

            dgvLoanAdvanceList.AutoGenerateColumns = False

            If dsLoanAdvance.Tables(0) IsNot Nothing Then
                dgvLoanAdvanceList.DataSource = mdtLoanAdvanceList
                dgvLoanAdvanceList.DataKeyField = "loanadvancetranunkid"
                dgvLoanAdvanceList.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (05-May-2016) -- Start
    Private Sub setATWebParameters()
        Try
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objLoan_Advance._WebClientIP = Session("IP_ADD").ToString
            objLoan_Advance._WebFormName = "frmNewLoanAdvanceList"
            objLoan_Advance._WebHostName = Session("HOST_NAME").ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objLoan_Advance._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("getATWebParameters:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (05-May-2016) -- End


#End Region

#Region "Button's Events"

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboLoanScheme.SelectedIndex = 0
            cboLoanAdvance.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            cboAssignPeriod.SelectedIndex = 0
            cboCondition.SelectedIndex = 0
            'Nilay (02-Jul-2016) -- Start
            cboOtherOpStatus.SelectedValue = CStr(0)
            'Nilay (02-Jul-2016) -- End
            txtVocNo.Text = ""
            txtAmount.Text = "0"
            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            cboCalcType.SelectedValue = "0"
            'Nilay (04-Nov-2016) -- End
            dgvLoanAdvanceList.DataSource = New List(Of String)
            dgvLoanAdvanceList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        Try
            objBBL = New clsBBL_Loan_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objBBL._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objBBL._EmployeeId = mintEmployeeunkid
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            objBBL._LoanSchemeId = CInt(cboLoanScheme.SelectedValue)
            objBBL._ProcessPendingLoanunkId = CInt(cboAFLoanApplicationNo.SelectedValue)
            objBBL._LoanApplicationNo = CStr(cboAFLoanApplicationNo.SelectedItem.Text)
            objBBL._LoanVoucherNo = CStr(cboAFLoanVoucherNo.SelectedItem.Text)
            'Hemant (12 Nov 2021) -- End
            'Sohail (29 Apr 2020) -- Start
            'Ifakara Enhancement # 0004668 : Advance Approve Form.
            objBBL._IsLoan = mblnIsLoan
            'Sohail (29 Apr 2020) -- End

            Select Case radView.SelectedValue
                Case CStr(1)
                    objBBL._DisplayModeSetting = clsBBL_Loan_Report.enDisplayMode.BASIC_SALARY
                Case CStr(2)
                    objBBL._DisplayModeSetting = clsBBL_Loan_Report.enDisplayMode.GROSS_SALARY
            End Select

            objBBL._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objBBL._UserUnkId = CInt(Session("UserId"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'objBBL.generateReport(0, enPrintAction.None, enExportAction.None)
            objBBL.generateReportNew(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                     Session("UserAccessModeSetting").ToString, True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                     0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objBBL._Rpt

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            'Response.Redirect(Session("rootpath").ToString & "Aruti Report Structure/Report.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnViewReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            If mdtLoanAdvanceList Is Nothing OrElse mdtLoanAdvanceList.Rows.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Please generate list to Preview report."), Me)
                Exit Sub
            End If

            Dim dsList As DataSet = Nothing
            Dim dtTable As DataTable = mdtLoanAdvanceList.Copy
            dtTable.Rows.RemoveAt(dtTable.Rows.Count - 1)

            Dim objMaster As New clsMasterData
            Dim objDic As New Dictionary(Of Integer, String)

            dsList = objMaster.GetLoan_Saving_Status("Status", True)

            Dim dRow As DataRow = dsList.Tables(0).NewRow
            dRow.Item("Id") = "-999"
            dRow.Item("NAME") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Future Loan")
            dsList.Tables(0).Rows.Add(dRow)
            dsList.Tables(0).AcceptChanges()

            objDic = (From p In dsList.Tables("Status") Select (p)).ToDictionary(Function(x) CInt(x.Item("Id")), _
                                                                                                Function(y) y.Item("Name").ToString)

            'For Each dsRow As DataRow In dsList.Tables(0).Rows
            '    objDic.Add(CInt(dsRow.Item("Id")), dsRow.Item("Name").ToString)
            'Next

            Dim objRptLoanAdvance As New ArutiReports.clsLoanAdvanceReport

            objRptLoanAdvance._Data_LoanAdvance = dtTable
            objRptLoanAdvance._Status_Dic = objDic
            objRptLoanAdvance._FilterTitle = mstrFilterTitle
            objRptLoanAdvance._Data_LoanBalance = mdtLoanBalance
            objRptLoanAdvance._CountryUnkid = CInt(cboCurrency.SelectedValue)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'objRptLoanAdvance.generateReport(0, enPrintAction.Preview, enExportAction.None)
            objRptLoanAdvance.generateReportNew(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                Session("UserAccessModeSetting").ToString, True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                                0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objRptLoanAdvance._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnPreview_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click

        objLoan_Advance = New clsLoan_Advance
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceunkid
            objLoan_Advance._Isvoid = True
            objLoan_Advance._Voidreason = popupDeleteReason.Reason
            objLoan_Advance._Voiddatetime = DateAndTime.Now.Date
            objLoan_Advance._Voiduserunkid = CInt(Session("UserId"))
            'Nilay (05-May-2016) -- Start
            Call setATWebParameters()
            'Nilay (05-May-2016) -- End

            'Nilay (25-Mar-2016) -- Start
            'objLoan_Advance.Delete(mintLoanAdvanceunkid)

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'objLoan_Advance.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                       CStr(Session("UserAccessModeSetting")), True, mintLoanAdvanceunkid)
            objLoan_Advance.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                   CStr(Session("UserAccessModeSetting")), True, mintLoanAdvanceunkid, True)
            'Nilay (20-Sept-2016) -- End

            'Dim enEmailType As clsProcess_pending_loan.enApproverEmailType
            'If mblnIsLoan = True Then
            '    enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
            'Else
            '    enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
            'End If

            'objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
            '                                          mintLoanAdvanceunkid, _
            '                                          clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
            '                                          enEmailType, _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                          popupDeleteReason.Reason)

            'objProcessLoan.Send_Notification_Assign(mintEmployeeunkid, _
            '                                        CInt(Session("Fin_year")), _
            '                                        CStr(Session("ArutiSelfServiceURL")), _
            '                                        mintProcessPendingUnkid, _
            '                                        FinancialYear._Object._DatabaseName, _
            '                                        CInt(Session("CompanyUnkId")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                        CStr(Session("UserAccessModeSetting")), _
            '                                        CBool(Session("IsIncludeInactiveEmp")), _
            '                                        True, enLogin_Mode.MGR_SELF_SERVICE, 0, 0)
            ''Nilay (23-Aug-2016) -- End
            'Pinkal (16-Apr-2016) -- End
            If objLoan_Advance._Message <> "" Then
                DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
            Else

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If mblnIsSendEmailNotification = True Then

                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    Dim objProcessLoan As New clsProcess_pending_loan

                    Dim enEmailType As clsProcess_pending_loan.enApproverEmailType
                    If mblnIsLoan = True Then
                        enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                    Else
                        enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                    End If

                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                    '                                  mintLoanAdvanceunkid, _
                    '                                  clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
                    '                                  enEmailType, _
                    '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                    '                                  popupDeleteReason.Reason)
                    objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                                              mintLoanAdvanceunkid, _
                                                              clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
                                                              enEmailType, _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), _
                                                              popupDeleteReason.Reason)
                    'Sohail (30 Nov 2017) -- End

                    objProcessLoan.Send_Notification_Assign(mintEmployeeunkid, _
                                                            CInt(Session("Fin_year")), _
                                                            CStr(Session("ArutiSelfServiceURL")), _
                                                            mintProcessPendingUnkid, _
                                                            FinancialYear._Object._DatabaseName, _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                            CStr(Session("UserAccessModeSetting")), _
                                                            CBool(Session("IsIncludeInactiveEmp")), _
                                                                    True, enLogin_Mode.MGR_SELF_SERVICE, 0, 0, , _
                                                                    Session("Notify_LoanAdvance_Users").ToString)
                    'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString)]	
                    'Pinkal (16-Apr-2016) -- End
                End If
                'Nilay (10-Dec-2016) -- End

                Call FillList()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (04-Nov-2016) -- Start
    'Enhancements: Global Change Status feature requested by {Rutta}
    Protected Sub btnGlobalChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGlobalChangeStatus.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Status/wPg_GlobalChangeStatus.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnGlobalChangeStatus_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (04-Nov-2016) -- End


#End Region

#Region "DataGrid Events"

    Protected Sub dgvLoanAdvanceList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvLoanAdvanceList.ItemCommand

        objLoan_Advance = New clsLoan_Advance
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                Select Case CInt(e.Item.Cells(22).Text)
                    Case 2 'OnHold
                        If e.CommandName.ToUpper <> "CHANGESTATUS" Then
                            DisplayMessage.DisplayMessage("You cannot perform this operation. Reason: Loan/Advance status is " & e.Item.Cells(20).Text, Me)
                            Exit Sub
                        End If
                    Case 3, 4 'Written off or Completed

                        'Nilay (06-Aug-2016) -- Start
                        'CHANGES : Replace Query String with Session and ViewState
                        If e.CommandName.ToUpper = "EDIT" Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, you cannot Edit/Delete Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), Me)
                            Exit Sub
                        Else
                            DisplayMessage.DisplayMessage("You cannot perform this operation. Reason: Loan/Advance status is " & e.Item.Cells(20).Text, Me)
                            Exit Sub
                        End If
                        'Nilay (06-Aug-2016) -- END

                    Case -999 'Future Loan
                        If e.CommandName.ToUpper <> "EDIT" AndAlso e.CommandName.ToUpper <> "DELETE" Then
                            DisplayMessage.DisplayMessage("You cannot perform this operation. Reason: Loan/Advance status is " & e.Item.Cells(20).Text, Me)
                            Exit Sub
                        End If

                End Select

                If e.CommandName.ToUpper = "EDIT" Then
                    mintLoanAdvanceunkid = CInt(e.Item.Cells(24).Text)
                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    Session("LoanAdvanceunkid") = CInt(e.Item.Cells(24).Text)
                    Session("IsFromEdit") = True
                    'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" _
                    '                  & HttpUtility.UrlEncode(clsCrypto.Encrypt(mintLoanAdvanceunkid.ToString)), False)
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx", False)
                    'Nilay (06-Aug-2016) -- END
                End If

                If e.CommandName.ToUpper = "DELETE" Then

                    'Nilay (23-Aug-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    mintProcessPendingUnkid = CInt(e.Item.Cells(29).Text)
                    mblnIsLoan = CBool(e.Item.Cells(21).Text)
                    mintEmployeeunkid = CInt(e.Item.Cells(23).Text)
                    'Nilay (23-Aug-2016) -- End

                    mintLoanAdvanceunkid = CInt(e.Item.Cells(24).Text)
                    objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceunkid 'Nilay (01-Apr-2016)

                    If CBool(e.Item.Cells(25).Text) = True Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, you cannot delete this Loan/Advance. Reason : Loan/Advance is brought forward."), Me)
                        Exit Sub
                    End If

                    Select Case CInt(objLoan_Advance._LoanStatus) 'Nilay (01-Apr-2016)
                        Case 3, 4
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, you cannot Edit/Delete Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), Me)
                            Exit Sub
                    End Select

                    If objLoan_Advance._Isloan = True Then
                        Dim objPaymentTran As New clsPayment_tran
                        If objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, mintLoanAdvanceunkid, clsPayment_tran.enPaymentRefId.LOAN) = True Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Payment is already done for this transaction."), Me)
                            Exit Sub
                        End If

                        If objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.RECEIVED, mintLoanAdvanceunkid, clsPayment_tran.enPaymentRefId.LOAN) = True Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, you cannot delete the transaction. Reason : Repayment is already done for this transaction."), Me)
                            Exit Sub
                        End If
                        objPaymentTran = Nothing
                    Else
                        Dim objPaymentTran As New clsPayment_tran
                        If objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, mintLoanAdvanceunkid, clsPayment_tran.enPaymentRefId.ADVANCE) = True Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Payment is already done for this transaction."), Me)
                            Exit Sub
                        End If

                        If objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.RECEIVED, mintLoanAdvanceunkid, clsPayment_tran.enPaymentRefId.ADVANCE) = True Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, you cannot delete the transaction. Reason : Repayment is already done for this transaction."), Me)
                            Exit Sub
                        End If
                        objPaymentTran = Nothing
                    End If

                    Dim dsList As New DataSet
                    dsList = objLoan_Advance.GetLastLoanBalance("List", mintLoanAdvanceunkid)
                    If dsList.Tables(0).Rows.Count > 0 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done against this transaction."), Me)
                        Exit Sub
                    End If

                    'Language.setLanguage(mstrModuleName)
                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Are you sure you want to delete this Loan?")
                    popupDeleteReason.Show()

                End If

                If e.CommandName.ToUpper = "PAYMENT" Then

                    Dim objPendingLoan As New clsProcess_pending_loan
                    objLoan_Advance._Loanadvancetranunkid = CInt(e.Item.Cells(24).Text)
                    objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
                    If objPendingLoan._Isexternal_Entity = True Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), Me)
                        Exit Sub
                    End If
                    objPendingLoan = Nothing

                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    'Dim strURL As String = ""
                    'If CBool(e.Item.Cells(21).Text) = True Then 'IS LOAN
                    '    strURL &= clsPayment_tran.enPaymentRefId.LOAN
                    'ElseIf CBool(e.Item.Cells(21).Text) = False Then 'IS ADVANCCE
                    '    strURL &= clsPayment_tran.enPaymentRefId.ADVANCE
                    'End If
                    'strURL &= "|" & dgvLoanAdvanceList.DataKeys(e.Item.ItemIndex).ToString
                    'strURL &= "|" & clsPayment_tran.enPayTypeId.PAYMENT
                    'strURL &= "|" & eZeeDate.convertDate(e.Item.Cells(26).Text.Trim)
                    'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)))

                    If CBool(e.Item.Cells(21).Text) = True Then 'IS LOAN
                        Session("ReferenceId") = clsPayment_tran.enPaymentRefId.LOAN
                    ElseIf CBool(e.Item.Cells(21).Text) = False Then 'IS ADVANCCE
                        Session("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE
                    End If
                    Session("TransactionId") = dgvLoanAdvanceList.DataKeys(e.Item.ItemIndex).ToString
                    Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT
                    Session("LoanSavingEffectiveDate") = eZeeDate.convertDate(e.Item.Cells(26).Text.Trim)

                    Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx", False)
                    'Nilay (06-Aug-2016) -- END
                End If

                If e.CommandName.ToUpper = "RECIEVED" Then

                    Dim blnFlag As Boolean
                    Dim objPaymentTran As New clsPayment_tran
                    Dim ePRef As clsPayment_tran.enPaymentRefId

                    If CBool(e.Item.Cells(21).Text) = True Then
                        ePRef = clsPayment_tran.enPaymentRefId.LOAN
                    Else
                        ePRef = clsPayment_tran.enPaymentRefId.ADVANCE
                    End If

                    Dim mnuReceivedEnabled = True 'Varsha Rana (12-Sept-2017)

                    If objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, CInt(e.Item.Cells(24).Text), ePRef) = False Then

                        mnuReceivedEnabled = False 'Varsha Rana (12-Sept-2017)

                        blnFlag = False

                        Dim objPendingLn As New clsProcess_pending_loan
                        objLoan_Advance._Loanadvancetranunkid = CInt(e.Item.Cells(24).Text)
                        objPendingLn._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        'If objPendingLn._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 Then


                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        'If objPendingLn._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 AndAlso objLoan_Advance._LoanStatus = 1 Then
                        '    DisplayMessage.DisplayMessage("You canot recieved payment. Reason: Loan Status is not In Progress.", Me)
                        '    Exit Sub
                        'Else
                        '    'Nilay (04-Nov-2016) -- End
                        '    blnFlag = True
                        'End If
                        'Sohail (19 Dec 2018) -- Start
                        'Telematics Issue : Unable to do other loan operation due to loan status in advance_tran table is 'completed' but loan is 'in progress' in loan_status_tran table in 76.1.
                        'If objPendingLn._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 _
                        '    AndAlso objLoan_Advance._LoanStatus = 1 Then
                        If objPendingLn._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 _
                             AndAlso CInt(e.Item.Cells(22).Text) = 1 Then
                            'Sohail (19 Dec 2018) -- End

                            mnuReceivedEnabled = True
                            'Sohail (14 Jul 2018) -- Start
                            'Issue : Audit Trail Issues in Loan module in 72.1.
                            blnFlag = True
                            'Sohail (14 Jul 2018) -- End
                        End If

                        If mnuReceivedEnabled = False Then
                            DisplayMessage.DisplayMessage("You cannot perform Loan other operations on this transaction. Reason: Payment is not done.", Me)
                            Exit Sub
                        End If
                        ' Varsha Rana (12-Sept-2017) -- End

                    Else
                        blnFlag = True
                    End If
                    objPaymentTran = Nothing

                    If blnFlag = True Then

                        Dim objPendingLoan As New clsProcess_pending_loan
                        objLoan_Advance._Loanadvancetranunkid = CInt(e.Item.Cells(24).Text)
                        objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
                        If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount <= 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), Me)
                            Exit Sub
                        End If
                        objPendingLoan = Nothing

                        'Nilay (06-Aug-2016) -- Start
                        'CHANGES : Replace Query String with Session and ViewState
                        'Dim strURL As String = ""
                        'If CBool(e.Item.Cells(21).Text) = True Then 'IS LOAN
                        '    strURL &= clsPayment_tran.enPaymentRefId.LOAN
                        'ElseIf CBool(e.Item.Cells(21).Text) = True Then 'IS ADVANCCE
                        '    strURL &= clsPayment_tran.enPaymentRefId.ADVANCE
                        'End If
                        'strURL &= "|" & dgvLoanAdvanceList.DataKeys(e.Item.ItemIndex).ToString
                        'strURL &= "|" & clsPayment_tran.enPayTypeId.RECEIVED
                        'strURL &= "|" & eZeeDate.convertDate(e.Item.Cells(26).Text.Trim)
                        'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)))
                        If CBool(e.Item.Cells(21).Text) = True Then 'IS LOAN
                            Session("ReferenceId") = clsPayment_tran.enPaymentRefId.LOAN
                        ElseIf CBool(e.Item.Cells(21).Text) = True Then 'IS ADVANCCE
                            Session("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE
                        End If
                        Session("TransactionId") = dgvLoanAdvanceList.DataKeys(e.Item.ItemIndex).ToString
                        Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED
                        Session("LoanSavingEffectiveDate") = eZeeDate.convertDate(e.Item.Cells(26).Text.Trim)

                        Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx", False)
                        'Nilay (06-Aug-2016) -- END
                    Else
                        DisplayMessage.DisplayMessage("You canot recieved payment. Reason: Payment is not done.", Me)
                        Exit Sub 'Nilay (01-Apr-2016)
                    End If

                End If

                If e.CommandName.ToUpper = "VIEWFORM" Then

                    'S.SANDEEP [04-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                    'txtEName.Text = ""
                    'txtEName.Text = e.Item.Cells(10).Text.ToString

                    'mintEmployeeunkid = CInt(e.Item.Cells(23).Text)

                    'popupViewApproveForm.Show()
                    'Sohail (29 Apr 2020) -- Start
                    'Ifakara Enhancement # 0004668 : Advance Approve Form.
                    'If CBool(e.Item.Cells(21).Text) = True Then 'IS LOAN
                    If 1 = 1 Then 'IS LOAN OR ADVANCE
                        'Sohail (29 Apr 2020) -- End
                        txtEName.Text = ""
                        txtEName.Text = e.Item.Cells(10).Text.ToString
                        mintEmployeeunkid = CInt(e.Item.Cells(23).Text)
                        'Sohail (29 Apr 2020) -- Start
                        'Ifakara Enhancement # 0004668 : Advance Approve Form.
                        mblnIsLoan = CBool(e.Item.Cells(21).Text)
                        'Sohail (29 Apr 2020) -- End
                        'Hemant (12 Nov 2021) -- Start
                        'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
                        cboAFLoanScheme.SelectedValue = CStr(e.Item.Cells(30).Text)
                        cboAFLoanScheme_SelectedIndexChanged(Nothing, Nothing)
                        cboAFLoanApplicationNo.SelectedValue = CStr(e.Item.Cells(29).Text)
                        cboAFLoanVoucherNo.SelectedValue = CStr(e.Item.Cells(29).Text)
                        cboAFLoanScheme.Enabled = False
                        cboAFLoanApplicationNo.Enabled = False
                        cboAFLoanVoucherNo.Enabled = False
                        'Hemant (12 Nov 2021) -- End
                        popupViewApproveForm.Show()
                    Else
                        DisplayMessage.DisplayMessage("You cannot view loan approved form for advance taken.", Me)
                        Exit Sub
                    End If
                    'S.SANDEEP [04-May-2018] -- END




                End If

                If e.CommandName.ToUpper = "CHANGESTATUS" Then
                    'Hemant (16 Jan 2019) -- Start
                    'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                    'objLoan_Advance._Loanadvancetranunkid = CInt(e.Item.Cells(24).Text)

                    'Select Case objLoan_Advance._LoanStatus
                    Select Case CInt(e.Item.Cells(22).Text)
                        'Hemant (16 Jan 2019) -- End

                        Case 3, 4 'Written off or Completed
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, you cannot edit Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), Me)
                            Exit Sub
                    End Select

                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    Session("IsFromLoan") = e.Item.Cells(21).Text.ToString
                    Session("StatusId") = e.Item.Cells(22).Text.ToString
                    Session("LoanAdvanceTranunkid") = e.Item.Cells(24).Text.ToString
                    'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Status/wPg_LoanStatus_AddEdit.aspx?" & _
                    '                  HttpUtility.UrlEncode(clsCrypto.Encrypt(e.Item.Cells(24).Text.ToString & "|" & _
                    '                                        e.Item.Cells(21).Text.ToString & "|" & e.Item.Cells(22).Text.ToString)), False)
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Status/wPg_LoanStatus_AddEdit.aspx", False)
                    'Nilay (06-Aug-2016) -- END
                End If
                If e.CommandName.ToUpper = "LOANOPERATION" Then


                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    'objLoan_Advance._Loanadvancetranunkid = CInt(e.Item.Cells(24).Text)

                    'Select Case objLoan_Advance._LoanStatus
                    '    Case 3, 4 'Written off or Completed
                    '        'Language.setLanguage(mstrModuleName)
                    '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, you cannot edit Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), Me)
                    '        Exit Sub
                    'End Select
                    'Nilay (06-Aug-2016) -- END

                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    Dim objPaymentTran As New clsPayment_tran
                    Dim ePRef As clsPayment_tran.enPaymentRefId

                    If CBool(e.Item.Cells(21).Text) = True Then
                        ePRef = clsPayment_tran.enPaymentRefId.LOAN
                    Else
                        ePRef = clsPayment_tran.enPaymentRefId.ADVANCE
                    End If

                    Dim mnuLoanOperationEnabled = True 'Varsha Rana (12-Sept-2017)

                    If objPaymentTran.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, CInt(e.Item.Cells(24).Text), ePRef) = False Then

                        mnuLoanOperationEnabled = False 'Varsha Rana (12-Sept-2017)

                        Dim objPendingLoan As New clsProcess_pending_loan
                        objLoan_Advance._Loanadvancetranunkid = CInt(e.Item.Cells(24).Text)
                        objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid

                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        'If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount <= 0 Then
                        '    DisplayMessage.DisplayMessage("You cannot perform Loan other operations on this transaction. Reason: Payment is not done.", Me)
                        '    Exit Sub
                        'End If

                        'Sohail (19 Dec 2018) -- Start
                        'Telematics Issue : Unable to do other loan operation due to loan status in advance_tran table is 'completed' but loan is 'in progress' in loan_status_tran table in 76.1.
                        'If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 _
                        '    AndAlso objLoan_Advance._LoanStatus = 1 Then
                        If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 _
                            AndAlso CInt(e.Item.Cells(22).Text) = 1 Then
                            'Sohail (19 Dec 2018) -- End

                            If ePRef = clsPayment_tran.enPaymentRefId.LOAN Then
                                mnuLoanOperationEnabled = True
                            ElseIf ePRef = clsPayment_tran.enPaymentRefId.ADVANCE Then
                                mnuLoanOperationEnabled = False
                            End If

                        End If

                        If mnuLoanOperationEnabled = False Then
                            DisplayMessage.DisplayMessage("You cannot perform Loan other operations on this transaction. Reason: Payment is not done.", Me)
                            Exit Sub
                        End If
                        ' Varsha Rana (12-Sept-2017) -- End
                    End If
                    'Nilay (01-Apr-2016) -- End

                    If CBool(e.Item.Cells(21).Text) = True Then

                        'Nilay (06-Aug-2016) -- Start
                        'CHANGES : Replace Query String with Session and ViewState
                        Session("VocNo") = e.Item.Cells(7).Text.ToString
                        Session("Employee") = e.Item.Cells(10).Text.ToString
                        Session("LoanScheme") = e.Item.Cells(11).Text.ToString
                        Session("LoanAdvanceTranunkid") = e.Item.Cells(24).Text.ToString

                        'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_LoanAdvanceOperationList.aspx?" & _
                        '              HttpUtility.UrlEncode(clsCrypto.Encrypt(e.Item.Cells(10).Text.ToString & "|" & e.Item.Cells(11).Text.ToString & _
                        '            "|" & e.Item.Cells(7).Text.ToString & "|" & e.Item.Cells(24).Text.ToString)), False)
                        Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_LoanAdvanceOperationList.aspx", False)
                        'Nilay (06-Aug-2016) -- END

                    Else
                        'Nilay (01-Apr-2016) -- Start
                        'ENHANCEMENT - Approval Process in Loan Other Operations
                        'DisplayMessage.DisplayError(ex, Me)
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("You cannot perform Loan other operation on Advance.", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                        'Nilay (01-Apr-2016) -- End
                    End If

                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanAdvanceList_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvLoanAdvanceList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLoanAdvanceList.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If CInt(dgvLoanAdvanceList.DataKeys(e.Item.ItemIndex)) = -1 Then
                    For i As Integer = 0 To 9
                        e.Item.Cells(i).Visible = False
                    Next
                    'Nilay (04-Nov-2016) -- Start
                    'Enhancements: Global Change Status feature requested by {Rutta}
                    'e.Item.Cells(10).ColumnSpan = e.Item.Cells.Count - 19
                    If CBool(Session("AllowChangeLoanAvanceStatus")) = True Then
                        e.Item.Cells(10).ColumnSpan = e.Item.Cells.Count - 19
                    Else

                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        ' e.Item.Cells(10).ColumnSpan = e.Item.Cells.Count - 20
                        If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                            e.Item.Cells(10).ColumnSpan = e.Item.Cells.Count - 19
                        Else
                            e.Item.Cells(10).ColumnSpan = e.Item.Cells.Count - 19 - 6
                        End If
                        ' Varsha Rana (12-Sept-2017) -- End


                    End If
                    'Nilay (04-Nov-2016) -- End

                    e.Item.Cells(10).Style.Add("text-align", "right")
                    For i = 9 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).CssClass = "GroupHeaderStyleBorderLeft"
                    Next

                End If

                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(15).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanAdvanceList_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Combo Box's Events "
    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Protected Sub cboAFLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, _
                                                                                                                   cboAFLoanScheme.SelectedIndexChanged
        Dim objProcessPendingLoan As New clsProcess_pending_loan
        Dim objLoanAdvance As New clsLoan_Advance
        Dim dsList As New DataSet
        Try
            If mintEmployeeunkid > 0 AndAlso CInt(cboAFLoanScheme.SelectedValue) > 0 Then
                dsList = objProcessPendingLoan.getLoanApplicationNumberList(True, "LoanApplication", mintEmployeeunkid, CInt(cboAFLoanScheme.SelectedValue))
                With cboAFLoanApplicationNo
                    .DataTextField = "application_no"
                    .DataValueField = "processpendingloanunkid"
                    .DataSource = dsList.Tables("LoanApplication")
                    .DataBind()
                    .SelectedValue = "0"
                End With

                dsList = objLoanAdvance.getLoanVoucherNumberList(True, "LoanVoucher", mintEmployeeunkid, CInt(cboAFLoanScheme.SelectedValue))
                With cboAFLoanVoucherNo
                    .DataValueField = "processpendingloanunkid"
                    .DataTextField = "loanvoucher_no"
                    .DataSource = dsList.Tables("LoanVoucher")
                    .DataBind()
                    .SelectedValue = "0"
                End With
            End If
            'popupViewApproveForm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing
            objProcessPendingLoan = Nothing
            objLoanAdvance = Nothing
        End Try
    End Sub
    'Hemant (12 Nov 2021) -- End
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            'Language.setLanguage(mstrModuleName)
            ' Varsha Rana (12-Sept-2017) -- End

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)

            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblLoanAdvance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblPayPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblVocNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblVocNo.ID, Me.lblVocNo.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnPreview.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnPreview.ID, Me.btnPreview.Text).Replace("&", "")

            Me.dgvLoanAdvanceList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(6).FooterText, Me.dgvLoanAdvanceList.Columns(6).HeaderText)
            Me.dgvLoanAdvanceList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(7).FooterText, Me.dgvLoanAdvanceList.Columns(7).HeaderText)
            Me.dgvLoanAdvanceList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(8).FooterText, Me.dgvLoanAdvanceList.Columns(8).HeaderText)
            Me.dgvLoanAdvanceList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(9).FooterText, Me.dgvLoanAdvanceList.Columns(9).HeaderText)
            Me.dgvLoanAdvanceList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(10).FooterText, Me.dgvLoanAdvanceList.Columns(10).HeaderText)
            Me.dgvLoanAdvanceList.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(11).FooterText, Me.dgvLoanAdvanceList.Columns(11).HeaderText)
            Me.dgvLoanAdvanceList.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(12).FooterText, Me.dgvLoanAdvanceList.Columns(12).HeaderText)
            Me.dgvLoanAdvanceList.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(13).FooterText, Me.dgvLoanAdvanceList.Columns(13).HeaderText)
            Me.dgvLoanAdvanceList.Columns(14).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvLoanAdvanceList.Columns(14).FooterText, Me.dgvLoanAdvanceList.Columns(14).HeaderText)

            'Loan Approval Form
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            'Language.setLanguage(mstrModuleName1)
            Me.lblAFLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),"lblLoanScheme", Me.lblAFLoanScheme.Text)
            Me.lblAFLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),"lblLoanApplicationNo", Me.lblAFLoanApplicationNo.Text)
            Me.lblAFLoanVoucherNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),"lblLoanVoucherNo", Me.lblAFLoanVoucherNo.Text)
            'Hemant (12 Nov 2021) -- End
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
