<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_NewLoanAdvanceList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Assignment_wPg_NewLoanAdvanceList" Title="Loan/Advance List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <link href="../../../App_Themes/PA_Style.css" rel="Stylesheet" type="text/css" />--%>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnl_ViewApproveForm" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Loan Approve Form" CssClass="form-label"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 525px;">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblEmployeeName" runat="server" Text="Employee Name" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtEName" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAFLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboAFLoanScheme" runat="server" Enabled = "false" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAFLoanApplicationNo" runat="server" Text="Loan Application No" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboAFLoanApplicationNo" runat="server"  Enabled = "false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAFLoanVoucherNo" runat="server" Text="Loan Voucher No" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboAFLoanVoucherNo" runat="server" Enabled = "false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include inactive employee"
                                    CssClass="filled-in"></asp:CheckBox>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="gbMonthlySal" runat="server" Text="Monthly Salary Display Setting"
                                    CssClass="form-label"></asp:Label>
                                <asp:RadioButtonList ID="radView" runat="server" RepeatDirection="Vertical" CssClass="with-gap">
                                    <asp:ListItem Selected="True" Text="Display Monthly Basic Salary" Value="1"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="Display Monthly Gross Amount" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnViewReport" runat="server" Text="View Report" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupViewApproveForm" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnCancel" PopupControlID="pnl_ViewApproveForm" TargetControlID="txtEName">
                </cc1:ModalPopupExtender>
                <uc1:DeleteReason ID="popupDeleteReason" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan/Advance List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Assigned Period" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboAssignPeriod" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblVocNo" runat="server" Text="Voucher No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtVocNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboLoanScheme" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                            <div class="form-group m-t-0">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAmount" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <div class="form-group m-t-0">
                                                <asp:DropDownList ID="cboCondition" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblLoanAdvance" runat="server" Text="Loan/Advance" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblCalcType" runat="server" Text="Calc. Type" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboCalcType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblOtherOpStatus" runat="server" Text="Other Opr. Status" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboOtherOpStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboCurrency" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Label ID="lblCurrOpenPeriod" runat="server" Text="" CssClass="form-label"></asp:Label>
                                </div>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvLoanAdvanceList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" Width="225%">
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                    <i class="fas fa-trash text-danger"></i> 
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnPayment">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgPayment" runat="server" ToolTip="Payment" CommandName="Payment">
                                                                    <i class="far fa-money-bill-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnRecieved">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgRecieved" runat="server" ToolTip="Recieved" CommandName="Recieved">
                                                                    <i class="fas fa-hand-holding-usd"></i>
                                                                </asp:LinkButton></span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnApprovedForm">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgApprovedForm" runat="server" ToolTip="View Approved Form"
                                                                    CommandName="ViewForm">
                                                                <i class="fas fa-chalkboard-teacher"></i>
                                                                </asp:LinkButton></span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnChangeStatus">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgChangeStatus" runat="server" ToolTip="Change Status" CommandName="ChangeStatus">
                                                                    <i class="fas fa-tasks"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgLoanOperation" CssClass="gridicon" runat="server" ToolTip="Other Loan Operation"
                                                                    CommandName="LoanOperation"><i class="fa fa-list-alt"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="VocNo" HeaderText="Voc #" FooterText="dgcolhVocNo"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PeriodName" HeaderText="Period" FooterText="dgcolhPeriod">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DeductionPeriodName" HeaderText="Deduction Period" FooterText="dgcolhDeductionPeriod">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEmployee">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" FooterText="dgcolhLoanScheme">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Loan_Advance" HeaderText="Loan/Advance" FooterText="dgcolhLoanAdvance">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LoanCalcType" HeaderStyle-Width="200px" ItemStyle-Width="200px"
                                                        HeaderText="Loan Calculation Type" FooterText="dgcolhLoanCalcType"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="InterestCalcType" HeaderText="Interest Type" FooterText="dgcolhInterestCalcType">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Amount" HeaderText="Loan Amount" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhAmount"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="cSign" HeaderText="Currency" FooterText="dgcolhCurrency">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="installments" HeaderText="No. Of INSTL" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhInstallments"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loanbalance" HeaderText="Curr. Principal Balance" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhCurrPrincipalBal"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="paidlaon" HeaderText="Paid Loan" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhPaidLoan"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loan_status" HeaderText="Status" FooterText="dgcolhStatus">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isloan" HeaderText="Is Loan" Visible="false" FooterText="objdgcolhIsLoan">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loan_statusunkid" HeaderText="Statusunkid" Visible="false"
                                                        FooterText="objdgcolhstatusunkid"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" Visible="false"
                                                        FooterText="objdgcolhemployeeunkid"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loanadvancetranunkid" HeaderText="objdgcolhLoanAdvanceunkid"
                                                        Visible="false" FooterText="objdgcolhLoanAdvanceunkid"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isbrought_forward" HeaderText="objdgcolhhisbrought_forward"
                                                        Visible="false" FooterText="objdgcolhhisbrought_forward"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="effective_date" HeaderText="EffectiveDate" Visible="false"
                                                        FooterText="dgcolhEffectiveDate"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="otherop_approval" HeaderText="Other Opr. Approval" FooterText="dgcolhOtherOpApproval">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="opstatus_id" Visible="false" FooterText="objdgcolhOpStatusId">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="processpendingloanunkid" Visible="false" FooterText="objdgcolhProcessPendingUnkid">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loanschemeunkid" Visible="false" FooterText="objdgcolhLoanSchemeUnkid">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Button ID="btnGlobalChangeStatus" runat="server" Text="Global Change Status"
                                        CssClass="btn btn-default" />
                                    <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btn btn-default" />
                                </div>
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
