﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Application_wPg_GlobalAssignLoanAdvance
    Inherits Basepage

#Region "Private Variables"

    Public Shared ReadOnly mstrModuleName As String = "frmGlobalAssignLoanAdvance"
    Dim DisplayMessage As New CommonCodes
    Private objLoan_Advance As clsLoan_Advance
    Private objPendingLoan As clsProcess_pending_loan
    Private objlnInterest As clslnloan_interest_tran
    Private objlnEMI As clslnloan_emitenure_tran

    Private mstrAdvanceFilter As String = String.Empty
    Private mdtTran As DataTable

    'Nilay (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (21-Jul-2016) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

#End Region

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
#Region " Enum "

    Private Enum enGridColumn
        Collapse = 0
        IsChecked_Hidden = 1
        IsChecked = 2
        ApplicationNo = 3
        Employee = 4
        Approver = 5
        Amount = 6
        DeductionPeriod = 7
        EffectiveDate = 8
        LoanCalcType = 9
        InterestCalcType = 10
        MappedHead = 11
        Rate_Hidden = 12
        Rate = 13
        NoOfInstallment_Hidden = 14
        NoOfInstallment = 15
        InstallmentAmount_Hidden = 16
        InstallmentAmount = 17
        IsGroup = 18
        IsEx = 19
        PendingUnkid = 20
        GroupId_Hidden = 21
        ApproverId_Hidden = 22
        EmployeeId_Hidden = 23
        DeductionPeriodId = 24
        LoanCalcTypeId_Hidden = 25
        MatchInstallmentAmt = 26
        InterestAmt = 27
        NetAmount = 28
        InterestCalcTypeId_Hidden = 29
        MappedHeadId_Hidden = 30
    End Enum
#End Region
    'Sohail (29 Apr 2019) -- End

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                End If
                'Nilay (10-Dec-2016) -- End

                Call SetLanguage()
                Call SetVisibility()
                Call FillCombo()

                If dgvDataList.Items.Count <= 0 Then
                    dgvDataList.DataSource = New List(Of String)
                    dgvDataList.DataBind()
                End If

            Else
                mdtTran = CType(Me.Session("TranDataTable"), DataTable)
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.Session("TranDataTable") = mdtTran
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()

        Dim dsList As New DataSet
        Dim objApprover As New clsLoanApprover_master
        Dim objScheme As New clsLoan_Scheme
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objCurrency As New clsExchangeRate
        Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)
        Try
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, _
                                            True, _
                                            CBool(Session("IsIncludeInactiveEmp")), _
                                            "EmpList", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("EmpList")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objScheme.getComboList(True, "Loan")
            dsList = objScheme.getComboList(True, "Loan", -1, "", False)
            'Pinkal (07-Dec-2017) -- End
            With cboLoan
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Loan")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objApprover.getListForCombo("Approver", True, True, True)
            With cboApprover
                .DataValueField = "lnapproverunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Approver")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                               CInt(Session("Fin_year")), _
                                               Session("Database_Name").ToString, _
                                               CDate(Session("fin_startdate")), _
                                               "Period", True, 1)
            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedValue = objCurrency.getBaseCurrencyCountryID().ToString
            End With

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
            'cboMode.Items.Clear()
            'cboMode.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Loan"))
            'cboMode.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Advance"))
            'cboMode.SelectedIndex = 0
            Dim objMaster As New clsMasterData
            dsList = objMaster.GetLoanAdvance("List")
            With cboMode
                .DataValueField = "ID"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            'Nilay (08-Dec-2016) -- End

            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            Dim dsLType As New DataSet
            Dim objLnAdv As New clsLoan_Advance

            dsLType = objLnAdv.GetLoanCalculationTypeList("List", True)
            With cboGLoanCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsLType.Tables("List")
                .DataBind()
            End With

            Dim dsItype As New DataSet
            dsItype = objLnAdv.GetLoan_Interest_Calculation_Type("List", True)

            With cboGInterestCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsItype.Tables("List")
                .DataBind()
            End With
            'Nilay (08-Aug-2016) -- END

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("_LoanVocNoType")) = 1 Then
                txtManualNo.Enabled = False
                'Nilay (08-Aug-2016) -- Start
                'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
                'nudManualNo.Enabled = False
                'Nilay (08-Aug-2016) -- End
            Else
                txtManualNo.Enabled = True
                'Nilay (08-Aug-2016) -- Start
                'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
                'nudManualNo.Enabled = True
                'Nilay (08-Aug-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillGrid()
        objPendingLoan = New clsProcess_pending_loan
        Try
            Dim strSearch As String = ""

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
            'Select Case CInt(cboMode.SelectedIndex)
            '    Case 0
            '        strSearch &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 1 "
            '    Case 1
            '        strSearch &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 2 "
            'End Select
            Select Case CInt(cboMode.SelectedValue)
                Case enLoanAdvance.LOAN
                    strSearch &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 1 "
                Case enLoanAdvance.ADVANCE
                    strSearch &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = 2 "
            End Select
            'Nilay (08-Dec-2016) -- End

            If CInt(cboLoan.SelectedValue) > 0 Then
                strSearch &= "AND ISNULL(lnloan_scheme_master.loanschemeunkid,0) = '" & CInt(cboLoan.SelectedValue) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND hremployee_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                strSearch &= "AND lnloanapprover_master.lnapproverunkid = '" & CInt(cboApprover.SelectedValue) & "' "
            End If

            If CInt(cboCurrency.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.countryunkid = '" & CInt(cboCurrency.SelectedValue) & "' "
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    strSearch &= "AND " & mstrAdvanceFilter
            'End If
            'Nilay (01-Mar-2016) -- End

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'mdtTran = objPendingLoan.GetToAssignList(Session("Database_Name").ToString, _
            '                                         CInt(Session("UserId")), _
            '                                         CInt(Session("Fin_year")), _
            '                                         CInt(Session("CompanyUnkId")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                         Session("UserAccessModeSetting").ToString, _
            '                                         True, _
            '                                         CBool(Session("IsIncludeInactiveEmp")), _
            '                                         2, strSearch)
            mdtTran = objPendingLoan.GetToAssignList(Session("Database_Name").ToString, _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                    2, strSearch, mstrAdvanceFilter)
            'Nilay (01-Mar-2016) -- End

            dgvDataList.AutoGenerateColumns = False

            If mdtTran.Rows.Count <= 0 Then
                cboMode.Enabled = True
                dgvDataList.DataSource = New List(Of String)
                dgvDataList.DataBind()
            Else
                dgvDataList.DataSource = mdtTran
                dgvDataList.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillGrid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId, _
                                                 ByVal eIntRateCalcTypeID As enLoanInterestCalcType, _
                                                 Optional ByVal blnIsDurationChange As Boolean = False, _
                                                 Optional ByVal item As DataGridItem = Nothing)
        Try
            Dim objLoan_Advance As New clsLoan_Advance

            If eLnCalcType <= 0 Then
                If mdtTran.Rows(item.ItemIndex).Item("installmentamt").ToString.Trim.Length <= 0 Then
                    mdtTran.Rows(item.ItemIndex).Item("installmentamt") = mdtTran.Rows(item.ItemIndex).Item("orginstlamt")
                End If
            End If

            Dim decInstallmentAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0

            If mdtTran.Rows(item.ItemIndex).Item("noofinstallment").ToString.Trim.Length <= 0 Then
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'item.Cells(13).Text = "1"
                item.Cells(enGridColumn.NoOfInstallment_Hidden).Text = "1"
                'Sohail (29 Apr 2019) -- End
                mdtTran.Rows(item.ItemIndex).Item("noofinstallment") = 1
            End If

            Dim dblEffMonth As Double = 0
            If CDbl(mdtTran.Rows(item.ItemIndex).Item("noofinstallment")) <= 0 Then
                dblEffMonth = 1
            Else
                dblEffMonth = CDbl(mdtTran.Rows(item.ItemIndex).Item("noofinstallment"))
            End If

            Dim dtEndDate As Date = DateAdd(DateInterval.Month, dblEffMonth, CDate(mdtTran.Rows(item.ItemIndex).Item("dstartdate")))

            Dim mdecRate As Decimal = 0
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If mdtTran.Rows(item.ItemIndex).Item("rate").ToString = "" OrElse _
            '                CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox).Text = "" Then
            If mdtTran.Rows(item.ItemIndex).Item("rate").ToString = "" OrElse _
                            CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox).Text = "" Then
                'Sohail (29 Apr 2019) -- End
                mdecRate = 0
            Else
                mdecRate = CDec(mdtTran.Rows(item.ItemIndex).Item("rate"))
            End If

            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If CInt(item.Cells(23).Text) > 0 Then
            If CInt(item.Cells(enGridColumn.DeductionPeriodId).Text) > 0 Then
                'Sohail (29 Apr 2019) -- End
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(item.Cells(23).Text)
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(item.Cells(enGridColumn.DeductionPeriodId).Text)
                'Sohail (29 Apr 2019) -- End
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If

            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(mdtTran.Rows(item.ItemIndex).Item("Amount")), _
                                                            CInt(DateDiff(DateInterval.Day, CDate(mdtTran.Rows(item.ItemIndex).Item("dstartdate")), dtEndDate.Date)), _
                                                            mdecRate, _
                                                            eLnCalcType, _
                                                            eIntRateCalcTypeID, _
                                                            CInt(dblEffMonth), _
                                                            CInt(DateDiff(DateInterval.Day, dtFPeriodStart, dtFPeriodEnd.Date.AddDays(1))), _
                                                            CDec(mdtTran.Rows(item.ItemIndex).Item("orginstlamt")), _
                                                            decInstrAmount, _
                                                            decInstallmentAmount, _
                                                            decTotIntrstAmount, _
                                                            decTotInstallmentAmount)

            mdtTran.Rows(item.ItemIndex).Item("instlnum") = CInt(mdtTran.Rows(item.ItemIndex).Item("noofinstallment"))
            mdtTran.Rows(item.ItemIndex).Item("instlamt") = decInstallmentAmount
            mdtTran.Rows(item.ItemIndex).Item("intrsamt") = decInstrAmount
            'mdtTran.Rows(item.ItemIndex).Item("netamt") = decInstrAmount + CDec(mdtTran.Rows(item.ItemIndex).Item("Amount"))
            mdtTran.Rows(item.ItemIndex).Item("netamt") = decTotIntrstAmount + decTotInstallmentAmount
            mdtTran.Rows(item.ItemIndex).Item("installmentamt") = decInstallmentAmount
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox).Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox).Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            'Sohail (29 Apr 2019) -- End

            mdtTran.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Calculate_Projected_Loan_Balance:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Dim objLoan_Advance As New clsLoan_Advance 'Sohail (14 Jul 2018)
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, Assignment period is mandatory information. Please select assignment period to continue."), Me)
                cboPayPeriod.Focus()
                Return False
            End If

            Dim dtAssignDate As String = ""
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
            dtAssignDate = eZeeDate.convertDate(objPrd._Start_Date).ToString
            objPrd = Nothing

            Dim dtmp() As DataRow = Nothing
            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")
            If dtmp.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, Please check atleast one approved loan to assign."), Me)
                dgvDataList.Focus()
                Return False
            End If

            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND dstartdate = ''")
            If dtmp.Length > 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), Me)
                dgvDataList.Focus()
                Return False
            End If

            'Hemant (10 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT#(Sport Pesa) - employees were posted loan applications, on September period which payroll is already done and the applications were approved.they now found out that they cant assign them in October.
            'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND mdate < '" & dtAssignDate & "'")
            'If dtmp.Length > 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, Effective date cannot be less then the assigned period date.") & "(" & eZeeDate.convertDate(dtAssignDate.ToString).ToShortDateString & ")", Me)
            '    dgvDataList.Focus()
            '    Return False
            'End If
            'Hemant (10 Oct 2019) -- End

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
            'If CInt(cboMode.SelectedIndex) = 0 Then
            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                'Nilay (08-Dec-2016) -- End
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <= 0")
                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type to continue."), Me)
                    dgvDataList.Focus()
                    Return False
                End If

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> " & enLoanCalcId.No_Interest & " AND interest_calctype_Id <= 0")
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> " & enLoanCalcId.No_Interest & " AND calctypeId <> " & enLoanCalcId.No_Interest_With_Mapped_Head & " AND interest_calctype_Id <= 0")
                'Sohail (29 Apr 2019) -- End
                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry, Interest calculation type is mandatory information. Please select Interest calculation type to continue."), Me)
                    dgvDataList.Focus()
                    Return False
                End If

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId = " & enLoanCalcId.No_Interest_With_Mapped_Head & " AND mapped_tranheadunkid <= 0")
                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "Please select Mapped Head to continue."), Me)
                    dgvDataList.Focus()
                    Return False
                End If
                'Sohail (29 Apr 2019) -- End

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> '" & enLoanCalcId.No_Interest & "' AND (rate IS NULL OR rate = '') ")
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND calctypeId <> '" & enLoanCalcId.No_Interest & "' AND calctypeId <> " & enLoanCalcId.No_Interest_With_Mapped_Head & " AND (rate IS NULL OR rate = '')")
                'Sohail (29 Apr 2019) -- End
                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sorry, Loan rate is mandatory information. Please enter loan rate to continue."), Me)
                    dgvDataList.Focus()
                    Return False
                End If

                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND instlnum <= 0")
                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, No Of Installment cannot be less than Zero. Please enter correct no of installment to continue."), Me)
                    dgvDataList.Focus()
                    Return False
                End If

                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false AND instlamt <= 0")
                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Installment amount cannot be less than Zero. Please enter correct installment amount to continue."), Me)
                    dgvDataList.Focus()
                    Return False
                End If

            End If

            Dim dtTable() As DataRow = Nothing
            dtTable = mdtTran.Select("IsCheck = true AND IsGrp = false")
            If dtTable.Length > 0 Then
                For Each dtRow As DataRow In dtTable
                    Dim objPayment As New clsPayment_tran
                    Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                   Session("UserAccessModeSetting").ToString, _
                                                                   True, _
                                                                   CBool(Session("IsIncludeInactiveEmp")), _
                                                                   "List", _
                                                                   clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                                   CInt(dtRow.Item("deductionperiodunkid")), _
                                                                   clsPayment_tran.enPayTypeId.PAYMENT, _
                                                                   dtRow.Item("EmpId").ToString, False)
                    If ds.Tables("List").Rows.Count > 0 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, you cannot assign Loan/Advance. Reason: Payslip Payment is already done for selected period for Employee") & " : " & dtRow.Item("Employee").ToString.Trim, Me)
                        Return False
                    End If

                    Dim objDeducPeriod As New clscommom_period_Tran
                    objDeducPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(dtRow.Item("deductionperiodunkid"))
                    Dim objTnA As New clsTnALeaveTran
                    If objTnA.IsPayrollProcessDone(CInt(dtRow.Item("deductionperiodunkid")), dtRow.Item("EmpId").ToString, objDeducPeriod._End_Date, enModuleReference.Payroll) = True Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period for Employee.") & " : " & dtRow.Item("Employee").ToString.Trim, Me)
                        Return False
                    End If

                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Dim objPendingLoan As New clsProcess_pending_loan
                    objPendingLoan._Processpendingloanunkid = CInt(dtRow.Item("Pendingunkid").ToString)
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = objPendingLoan._Loanschemeunkid
                    If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(dtRow.Item("noofinstallment").ToString) > objLoanScheme._MaxNoOfInstallment Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, " for ") & objLoanScheme._Name & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, " Scheme and Application No is ") & objPendingLoan._Application_No & ".", Me)
                        Return False
                    End If
                    'Varsha (25 Nov 2017) -- End

                    'Sohail (21 Mar 2018) -- Start
                    'TRA Issue - Support Issue Id # 0002126 : Duplicate loan transactions are coming at time of salary in 70.2.
                    If CInt(cboMode.SelectedValue) = CInt(enLoanAdvance.LOAN) Then 'Sohail (16 May 2018)
                        If objLoan_Advance.GetExistLoan(objPendingLoan._Loanschemeunkid, CInt(dtRow.Item("EmpId")), True) = True Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Sorry, you can not assign loan for the selected scheme: Reason, The selected loan scheme is already assigned and having (In-progress or On hold) status for selected period for Employee") & " : " & dtRow.Item("Employee").ToString.Trim & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "for Scheme") & " " & objLoanScheme._Name & ". " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations."), Me)
                            Return False
                        End If

                        'Sohail (17 Dec 2019) -- Start
                        'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                        If CInt(dtRow.Item("noofinstallment")) > 1 Then
                            Dim dtLoanEnd As Date = objDeducPeriod._Start_Date.Date.AddMonths(CInt(dtRow.Item("noofinstallment"))).AddDays(-1)
                            Dim objEmpDates As New clsemployee_dates_tran
                            Dim dsEmp As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, dtRow.Item("EmpId").ToString)
                            If dsEmp.Tables(0).Rows.Count > 0 Then
                                Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, objDeducPeriod._Start_Date.Date, eZeeDate.convertDate(dsEmp.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                                If CInt(dtRow.Item("noofinstallment")) > intEmpTenure Then
                                    'Hemant (24 Nov 2021) -- Start
                                    'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                                    If CBool(Session("SkipEOCValidationOnLoanTenure")) = False Then
                                        'Hemant (24 Nov 2021) -- End
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "].", Me)
                                        Return False
                                    End If 'Hemant (24 Nov 2021)
                                End If
                            End If
                        End If
                        'Sohail (17 Dec 2019) -- End
                    End If 'Sohail (16 May 2018)
                    'Sohail (21 Mar 2018) -- End
                    'Sohail (16 May 2018) -- Start
                    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                    If CDec(Session("Advance_NetPayPercentage")) > 0 AndAlso CInt(cboMode.SelectedValue) = CInt(enLoanAdvance.ADVANCE) Then
                        If objLoan_Advance.GetExistLoan(0, CInt(dtRow.Item("EmpId")), False) = True Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "Sorry, you can not assign Advance: Reason, The Advance is already assigned and having (In-progress or On hold) status for selected period for Employee") & " : " & dtRow.Item("Employee").ToString.Trim, Me)
                            Return False
                        End If
                    End If
                    'Sohail (16 May 2018) -- End
                Next
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidData:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub SetValue(ByVal xrow As DataRow)
        Try
            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtStartDate = objPeriod._Start_Date
            mdtEndDate = objPeriod._End_Date
            'Nilay (21-Jul-2016) -- End

            objLoan_Advance._Deductionperiodunkid = CInt(xrow.Item("deductionperiodunkid"))
            objLoan_Advance._Effective_Date = eZeeDate.convertDate(xrow.Item("mdate").ToString).Date
            objLoan_Advance._Employeeunkid = CInt(xrow.Item("EmpId"))
            objLoan_Advance._Isbrought_Forward = False
            objLoan_Advance._Loan_Purpose = txtPurpose.Text

            If CInt(Session("_LoanVocNoType")) = 1 Then
                objLoan_Advance._Loanvoucher_No = ""
            Else
                objLoan_Advance._Loanvoucher_No = txtPrefix.Text.Trim & txtManualNo.Text
            End If

            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objLoan_Advance._LoanStatus = 1
            objLoan_Advance._Processpendingloanunkid = CInt(xrow.Item("Pendingunkid"))
            objLoan_Advance._Approverunkid = CInt(xrow.Item("ApprId"))
            objLoan_Advance._CountryUnkid = CInt(xrow.Item("countryunkid"))

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
            'If CInt(cboMode.SelectedIndex) = 0 Then 'LOAN
            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                'Nilay (08-Dec-2016) -- End
                objLoan_Advance._Isloan = True
                'Select Case CInt(xrow.Item("calctypeId"))
                '    Case enLoanCalcId.Simple_Interest
                '        objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
                '    Case enLoanCalcId.Reducing_Amount
                '        objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
                '    Case enLoanCalcId.No_Interest
                '        objLoan_Advance._Calctype_Id = enLoanCalcId.No_Interest '4
                'End Select

                objLoan_Advance._Calctype_Id = CInt(xrow.Item("calctypeId"))
                objLoan_Advance._Interest_Calctype_Id = CInt(xrow.Item("interest_calctype_Id"))
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objLoan_Advance._Mapped_TranheadUnkid = CInt(xrow.Item("mapped_tranheadunkid"))
                'Sohail (29 Apr 2019) -- End
                objLoan_Advance._Loanschemeunkid = CInt(xrow.Item("GrpId"))
                objLoan_Advance._Loan_Amount = CDec(xrow.Item("Amount"))

                If xrow.Item("rate").ToString.Trim.Length <= 0 Then
                    objLoan_Advance._Interest_Rate = CDec(0)
                Else
                    objLoan_Advance._Interest_Rate = CDec(xrow.Item("rate"))
                End If

                objLoan_Advance._Loan_Duration = CInt(xrow.Item("noofinstallment"))
                objLoan_Advance._Interest_Amount = CDec(xrow.Item("intrsamt")) / CDec(xrow.Item("PaidExRate"))
                objLoan_Advance._Net_Amount = CDec(xrow.Item("netamt")) / CDec(xrow.Item("PaidExRate"))
                objLoan_Advance._Emi_Tenure = CInt(xrow.Item("noofinstallment"))
                objLoan_Advance._Emi_Amount = CDec(xrow.Item("instlamt")) / CDec(xrow.Item("PaidExRate"))

                'Nilay (08-Dec-2016) -- Start
                'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                'ElseIf CInt(cboMode.SelectedIndex) = 1 Then 'ADVANCE
            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                'Nilay (08-Dec-2016) -- End
                objLoan_Advance._Isloan = False
                objLoan_Advance._Advance_Amount = CDec(xrow.Item("Amount"))

            End If

            objLoan_Advance._Exchange_rate = CDec(xrow.Item("PaidExRate"))
            objLoan_Advance._Basecurrency_amount = CDec(xrow.Item("Amount")) / CDec(xrow.Item("PaidExRate"))

            If CBool(xrow.Item("IsEx")) = True Then
                objLoan_Advance._Balance_Amount = CDec(xrow.Item("Amount")) / CDec(xrow.Item("PaidExRate"))
                objLoan_Advance._Balance_AmountPaidCurrency = CDec(xrow.Item("Amount"))
            End If
            If xrow.Item("rate").ToString.Trim.Length <= 0 Then
                objLoan_Advance._RateOfInterest = CDec(0)
            Else
                objLoan_Advance._RateOfInterest = CDec(xrow.Item("rate"))
            End If

            objLoan_Advance._NoOfInstallments = CInt(xrow.Item("noofinstallment"))
            objLoan_Advance._InstallmentAmount = CDec(xrow.Item("instlamt"))
            objLoan_Advance._Userunkid = CInt(Session("UserId"))
            objLoan_Advance._Voiduserunkid = -1
            objLoan_Advance._Voiddatetime = Nothing
            objLoan_Advance._Isvoid = False
            objLoan_Advance._Opening_balance = CDec(xrow.Item("Amount")) / CDec(xrow.Item("PaidExRate"))
            objLoan_Advance._Opening_balancePaidCurrency = CDec(xrow.Item("Amount"))

            objLoan_Advance._EMI_Principal_amount = CDec(xrow.Item("instlamt")) - CDec(xrow.Item("intrsamt"))
            objLoan_Advance._EMI_Interest_amount = CDec(xrow.Item("intrsamt"))

            'Nilay (05-May-2016) -- Start
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objLoan_Advance._WebClientIP = Session("IP_ADD").ToString
            objLoan_Advance._WebFormName = "frmGlobalAssignLoanAdvance"
            objLoan_Advance._WebHostName = Session("HOST_NAME").ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objLoan_Advance._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If
            'Nilay (05-May-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (08-Aug-2016) -- Start
    'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
    Private Sub SetCalculation(ByVal blnIsApplyToAll As Boolean)
        Try
            Dim dtmp As DataRow() = Nothing
            If blnIsApplyToAll = True Then
                dtmp = mdtTran.Select("1=1")
            Else
                dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")
            End If
            If dtmp.Length > 0 Then
                For Each dRow As DataRow In dtmp
                    If blnIsApplyToAll = True Then
                        'CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(2).FindControl("chkSelect"), CheckBox).Checked = radApplyToAll.Checked
                    End If
                    If CBool(dRow.Item("IsGrp")) = False Then
                        dRow.Item("calctypeId") = CInt(cboGLoanCalcType.SelectedValue)
                        dRow.Item("mapped_tranheadunkid") = CInt(cboGInterestCalcType.SelectedValue)
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        dRow.Item("interest_calctype_id") = CInt(cboMappedHead.SelectedValue)
                        'Sohail (29 Apr 2019) -- End
                        dRow.Item("rate") = IIf(CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest, CDec(txtGRate.Text), "")
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        'CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(9).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue = cboGLoanCalcType.SelectedValue
                        'Call cbodgcolhLoanType_SelectedIndexChanged(CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(9).FindControl("cbodgcolhLoanType"), DropDownList), New EventArgs())
                        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue = cboGLoanCalcType.SelectedValue
                        Call cbodgcolhLoanType_SelectedIndexChanged(CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList), New EventArgs())
                        'Sohail (29 Apr 2019) -- End

                        'Varsha (25 Nov 2017) -- Start
                        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                        'CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(10).FindControl("cbodgcolhInterestCalcType"), DropDownList).SelectedValue = cboGInterestCalcType.SelectedValue
                        'Call cbodgcolhLoanType_SelectedIndexChanged(CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(10).FindControl("cbodgcolhInterestCalcType"), DropDownList), New EventArgs())
                        'CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(12).FindControl("txtdgcolhRate"), TextBox).Text = CStr(IIf(CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest, CDec(txtGRate.Text), ""))
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        'CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(12).FindControl("txtdgcolhRate"), TextBox).Text = CStr(IIf(CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest, CDec(txtGRate.Text), ""))
                        'CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(10).FindControl("cbodgcolhInterestCalcType"), DropDownList).SelectedValue = cboGInterestCalcType.SelectedValue
                        'Call cbodgcolhInterestCalcType_SelectedIndexChanged(CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(10).FindControl("cbodgcolhInterestCalcType"), DropDownList), New EventArgs())
                        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox).Text = CStr(IIf(CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest, CDec(txtGRate.Text), ""))
                        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.InterestCalcType).FindControl("cbodgcolhInterestCalcType"), DropDownList).SelectedValue = cboGInterestCalcType.SelectedValue
                        Call cbodgcolhInterestCalcType_SelectedIndexChanged(CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.InterestCalcType).FindControl("cbodgcolhInterestCalcType"), DropDownList), New EventArgs())
                        'Sohail (29 Apr 2019) -- End
                        'Varsha (25 Nov 2017) -- End

                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.MappedHead).FindControl("cbodgcolhMappedHead"), DropDownList).SelectedValue = cboMappedHead.SelectedValue
                        Call cbodgcolhMappedHead_SelectedIndexChanged(CType(dgvDataList.Items(mdtTran.Rows.IndexOf(dRow)).Cells(enGridColumn.MappedHead).FindControl("cbodgcolhMappedHead"), DropDownList), New EventArgs())
                        'Sohail (29 Apr 2019) -- End

                    End If
                Next
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetCalculation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (08-Aug-2016) -- End


#End Region

#Region "DataGrid's Events"

    Protected Sub dgvDataList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvDataList.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                If e.Item.ItemIndex >= 0 Then

                    If e.CommandName.ToUpper = "EXPCOL" Then
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        'If CBool(e.Item.Cells(17).Text) = True Then
                        '    If CType(e.Item.Cells(0).FindControl("imgCollapse"), LinkButton).ToolTip.ToUpper = "COLLAPSE" Then
                        '        For Each dgvItem As DataGridItem In dgvDataList.Items
                        '            If CBool(dgvItem.Cells(17).Text) = False AndAlso CInt(dgvItem.Cells(20).Text) = CInt(e.Item.Cells(20).Text) Then
                        '                dgvItem.Visible = False
                        '            End If
                        '        Next
                        '        CType(e.Item.Cells(0).FindControl("imgCollapse"), LinkButton).ToolTip = "Expand"
                        '    Else
                        '        For Each dgvItem As DataGridItem In dgvDataList.Items
                        '            If CBool(dgvItem.Cells(17).Text) = False AndAlso CInt(dgvItem.Cells(20).Text) = CInt(e.Item.Cells(20).Text) Then
                        '                dgvItem.Visible = True
                        '            End If
                        '        Next
                        '        CType(e.Item.Cells(0).FindControl("imgCollapse"), LinkButton).ToolTip = "Collapse"
                        '    End If
                        'End If
                        If CBool(e.Item.Cells(enGridColumn.IsGroup).Text) = True Then
                            If CType(e.Item.Cells(enGridColumn.Collapse).FindControl("imgCollapse"), LinkButton).ToolTip.ToUpper = "COLLAPSE" Then
                                For Each dgvItem As DataGridItem In dgvDataList.Items
                                    If CBool(dgvItem.Cells(enGridColumn.IsGroup).Text) = False AndAlso CInt(dgvItem.Cells(enGridColumn.GroupId_Hidden).Text) = CInt(e.Item.Cells(enGridColumn.GroupId_Hidden).Text) Then
                                        dgvItem.Visible = False
                                    End If
                                Next
                                CType(e.Item.Cells(enGridColumn.Collapse).FindControl("imgCollapse"), LinkButton).ToolTip = "Expand"
                            Else
                                For Each dgvItem As DataGridItem In dgvDataList.Items
                                    If CBool(dgvItem.Cells(enGridColumn.IsGroup).Text) = False AndAlso CInt(dgvItem.Cells(enGridColumn.GroupId_Hidden).Text) = CInt(e.Item.Cells(enGridColumn.GroupId_Hidden).Text) Then
                                        dgvItem.Visible = True
                                    End If
                                Next
                                CType(e.Item.Cells(enGridColumn.Collapse).FindControl("imgCollapse"), LinkButton).ToolTip = "Collapse"
                            End If
                        End If
                        'Sohail (29 Apr 2019) -- End
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvDataList_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvDataList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvDataList.ItemDataBound
        Try
            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'e.Item.Cells(19).Visible = False
            e.Item.Cells(enGridColumn.PendingUnkid).Visible = False
            'Sohail (29 Apr 2019) -- End
            'Varsha (25 Nov 2017) -- End

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End
            Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)

            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemIndex >= 0 Then
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If CBool(e.Item.Cells(17).Text) = True Then 'isgroup 
                    '    e.Item.Cells(3).Text = e.Item.Cells(4).Text.ToString
                    '    e.Item.Cells(3).ColumnSpan = e.Item.Cells.Count - 3
                    '    For i As Integer = 4 To e.Item.Cells.Count - 1
                    '        e.Item.Cells(i).Visible = False
                    '    Next
                    '    e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    '    e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                    '    e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                    '    e.Item.Cells(3).CssClass = "GroupHeaderStyleBorderLeft"
                    '    'e.Item.Cells(4).CssClass = "GroupHeaderStyleBorderLeft"
                    '    CType(e.Item.Cells(0).FindControl("imgCollapse"), LinkButton).ToolTip = "Collapse"

                    'Else 'not isgroup

                    '    CType(e.Item.Cells(0).FindControl("imgCollapse"), LinkButton).Visible = False

                    '    If e.Item.Cells(6).Text <> Nothing AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                    '        e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString)
                    '    End If

                    '    'Nilay (01-Apr-2016) -- Start
                    '    'ENHANCEMENT - Approval Process in Loan Other Operations

                    '    'Pinkal (16-Apr-2016) -- Start
                    '    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    '    'If e.Item.Cells(8).Text <> Nothing AndAlso e.Item.Cells(8).Text <> "&nbsp;" Then
                    '    '    e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                    '    'End If

                    '    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                    '    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    '    'If e.Item.Cells(8).Text <> Nothing AndAlso e.Item.Cells(8).Text <> "&nbsp;" Then
                    '    '    e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
                    '    'End If
                    '    'Nilay (20-Sept-2016) -- Start
                    '    'Enhancement : Cancel feature for approved but not assigned loan application
                    '    'If e.Item.Cells(8).Text <> Nothing AndAlso e.Item.Cells(8).Text <> "&nbsp;" Then
                    '    '    e.Item.Cells(8).Text = eZeeDate.convertDate(e.Item.Cells(8).Text.ToString).Date.ToShortDateString
                    '    'End If
                    '    'Nilay (20-Sept-2016) -- End
                    '    'Pinkal (16-Apr-2016) -- End

                    '    'Pinkal (16-Apr-2016) -- End

                    '    'Nilay (01-Apr-2016) -- End

                    '    If e.Item.Cells(15).Text <> Nothing AndAlso e.Item.Cells(15).Text <> "&nbsp;" Then
                    '        e.Item.Cells(15).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString)
                    '    End If

                    '    Dim txtRate As TextBox = CType(e.Item.Cells(12).FindControl("txtdgcolhRate"), TextBox)
                    '    If txtRate.Text.Trim <> "" Then
                    '        txtRate.Text = Format(CDec(txtRate.Text), Session("fmtCurrency").ToString)
                    '    End If

                    '    Dim txtInstallmentAmount As TextBox = CType(e.Item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                    '    If txtInstallmentAmount.Text.Trim <> "" Then
                    '        txtInstallmentAmount.Text = Format(CDec(txtInstallmentAmount.Text), Session("fmtCurrency").ToString)
                    '    End If

                    '    Dim dsList As New DataSet
                    '    Dim objPrd As New clscommom_period_Tran

                    '    dsList = objPrd.getListForCombo(enModuleReference.Payroll, _
                    '                                    CInt(Session("Fin_year")), _
                    '                                    Session("Database_Name").ToString, _
                    '                                    CDate(Session("fin_startdate")), _
                    '                                    "Period", True, 1)

                    '    If CInt(e.Item.Cells(23).Text) > 0 Then
                    '        Dim cboDeductionPeriod As DropDownList = CType(e.Item.Cells(7).FindControl("cbodgcolhDeductionPeriod"), DropDownList)
                    '        With cboDeductionPeriod
                    '            .DataValueField = "periodunkid"
                    '            .DataTextField = "name"
                    '            .DataSource = dsList.Tables("Period")
                    '            .DataBind()
                    '            .SelectedValue = e.Item.Cells(23).Text
                    '        End With
                    '    End If
                    '    dsList = Nothing

                    '    Dim objLnAdv As New clsLoan_Advance

                    '    'Nilay (08-Dec-2016) -- Start
                    '    'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                    '    'If CInt(cboMode.SelectedIndex) = 0 Then
                    '    '    dsList = objLnAdv.GetLoanCalculationTypeList("LoanType", True)
                    '    'Else
                    '    '    dsList = objLnAdv.GetLoanCalculationTypeList("LoanType", True, True)
                    '    'End If
                    '    If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                    '        dsList = objLnAdv.GetLoanCalculationTypeList("LoanType", True)
                    '    ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                    '        dsList = objLnAdv.GetLoanCalculationTypeList("LoanType", True, True)
                    '    End If
                    '    'Nilay (08-Dec-2016) -- End

                    '    Dim cboLoanType As DropDownList = CType(e.Item.Cells(9).FindControl("cbodgcolhLoanType"), DropDownList)
                    '    With cboLoanType
                    '        .DataValueField = "Id"
                    '        .DataTextField = "name"
                    '        .DataSource = dsList.Tables("LoanType")
                    '        .DataBind()
                    '        .SelectedValue = "0"
                    '    End With
                    '    dsList = Nothing

                    '    dsList = objLnAdv.GetLoan_Interest_Calculation_Type("IntType", True)

                    '    Dim cboInterestCalctype As DropDownList = CType(e.Item.Cells(10).FindControl("cbodgcolhInterestCalcType"), DropDownList)
                    '    With cboInterestCalctype
                    '        .DataValueField = "Id"
                    '        .DataTextField = "Name"
                    '        .DataSource = dsList.Tables("IntType")
                    '        .DataBind()
                    '        .SelectedValue = "0"
                    '    End With

                    '    If cboLoanType.SelectedValue = "0" Then
                    '        CType(e.Item.Cells(12).FindControl("txtdgcolhRate"), TextBox).Enabled = False
                    '        CType(e.Item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = False
                    '        CType(e.Item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False
                    '    End If

                    '    If CInt(cboMode.SelectedIndex) = enLoanAdvance.ADVANCE Then 'Nilay (08-Dec-2016)
                    '        cboLoanType.Enabled = False
                    '    End If

                    'End If
                    If CBool(e.Item.Cells(enGridColumn.IsGroup).Text) = True Then 'isgroup 
                        e.Item.Cells(enGridColumn.ApplicationNo).Text = e.Item.Cells(enGridColumn.Employee).Text.ToString
                        e.Item.Cells(enGridColumn.ApplicationNo).ColumnSpan = e.Item.Cells.Count - 3
                        For i As Integer = 4 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next
                        e.Item.Cells(enGridColumn.Collapse).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(enGridColumn.IsChecked_Hidden).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(enGridColumn.IsChecked).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(enGridColumn.ApplicationNo).CssClass = "GroupHeaderStyleBorderLeft"
                        CType(e.Item.Cells(enGridColumn.Collapse).FindControl("imgCollapse"), LinkButton).ToolTip = "Collapse"

                    Else 'not isgroup

                        CType(e.Item.Cells(enGridColumn.Collapse).FindControl("imgCollapse"), LinkButton).Visible = False

                        If e.Item.Cells(enGridColumn.Amount).Text <> Nothing AndAlso e.Item.Cells(enGridColumn.Amount).Text <> "&nbsp;" Then
                            e.Item.Cells(enGridColumn.Amount).Text = Format(CDec(e.Item.Cells(enGridColumn.Amount).Text), Session("fmtCurrency").ToString)
                        End If

                        If e.Item.Cells(enGridColumn.InstallmentAmount_Hidden).Text <> Nothing AndAlso e.Item.Cells(enGridColumn.InstallmentAmount_Hidden).Text <> "&nbsp;" Then
                            e.Item.Cells(enGridColumn.InstallmentAmount_Hidden).Text = Format(CDec(e.Item.Cells(enGridColumn.InstallmentAmount_Hidden).Text), Session("fmtCurrency").ToString)
                        End If

                        Dim txtRate As TextBox = CType(e.Item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox)
                        If txtRate.Text.Trim <> "" Then
                            txtRate.Text = Format(CDec(txtRate.Text), Session("fmtCurrency").ToString)
                        End If

                        Dim txtInstallmentAmount As TextBox = CType(e.Item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                        If txtInstallmentAmount.Text.Trim <> "" Then
                            txtInstallmentAmount.Text = Format(CDec(txtInstallmentAmount.Text), Session("fmtCurrency").ToString)
                        End If

                        Dim dsList As New DataSet
                        Dim objPrd As New clscommom_period_Tran

                        dsList = objPrd.getListForCombo(enModuleReference.Payroll, _
                                                        CInt(Session("Fin_year")), _
                                                        Session("Database_Name").ToString, _
                                                        CDate(Session("fin_startdate")), _
                                                        "Period", True, 1)

                        If CInt(e.Item.Cells(enGridColumn.DeductionPeriodId).Text) > 0 Then
                            Dim cboDeductionPeriod As DropDownList = CType(e.Item.Cells(enGridColumn.DeductionPeriod).FindControl("cbodgcolhDeductionPeriod"), DropDownList)
                            With cboDeductionPeriod
                                .DataValueField = "periodunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables("Period")
                                .DataBind()
                                .SelectedValue = e.Item.Cells(enGridColumn.DeductionPeriodId).Text
                            End With
                        End If
                        dsList = Nothing

                        Dim objLnAdv As New clsLoan_Advance

                        If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                            dsList = objLnAdv.GetLoanCalculationTypeList("LoanType", True)
                        ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                            dsList = objLnAdv.GetLoanCalculationTypeList("LoanType", True, True)
                        End If

                        Dim cboLoanType As DropDownList = CType(e.Item.Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList)
                        With cboLoanType
                            .DataValueField = "Id"
                            .DataTextField = "name"
                            .DataSource = dsList.Tables("LoanType")
                            .DataBind()
                            .SelectedValue = "0"
                        End With
                        dsList = Nothing

                        dsList = objLnAdv.GetLoan_Interest_Calculation_Type("IntType", True)

                        Dim cboInterestCalctype As DropDownList = CType(e.Item.Cells(enGridColumn.InterestCalcType).FindControl("cbodgcolhInterestCalcType"), DropDownList)
                        With cboInterestCalctype
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsList.Tables("IntType")
                            .DataBind()
                            .SelectedValue = "0"
                        End With

                        dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
                        Dim cboMappedHead As DropDownList = CType(e.Item.Cells(enGridColumn.MappedHead).FindControl("cbodgcolhMappedHead"), DropDownList)
                        With cboMappedHead
                            .DataValueField = "tranheadunkid"
                            .DataTextField = "Name"
                            .DataSource = dsList.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With

                        If cboLoanType.SelectedValue = "0" Then
                            CType(e.Item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox).Enabled = False
                            CType(e.Item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = False
                            CType(e.Item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False
                        End If

                        If CInt(cboMode.SelectedIndex) = enLoanAdvance.ADVANCE Then 'Nilay (08-Dec-2016)
                            cboLoanType.Enabled = False
                        End If

                    End If
                    'Sohail (29 Apr 2019) -- End
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvDataList_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Currency is mandatory information. Please select currency to continue."), Me)
                Exit Sub
            End If

            cboMode.Enabled = False

            Call FillGrid()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            cboApprover.SelectedValue = "0"
            cboMode.SelectedValue = "0" 'Nilay (08-Dec-2016) 
            cboLoan.SelectedValue = "0"
            mstrAdvanceFilter = ""
            cboCurrency.SelectedValue = "0"
            cboMode.Enabled = True

            dgvDataList.DataSource = New List(Of String)
            dgvDataList.DataBind()

            Session("TranDataTable") = Nothing
            mdtTran = CType(Session("TranDataTable"), DataTable)

            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            cboGLoanCalcType.Enabled = True
            cboGLoanCalcType.SelectedValue = CStr(0)
            cboGInterestCalcType.Enabled = True
            cboGInterestCalcType.SelectedValue = CStr(0)
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboMappedHead.Enabled = True
            cboMappedHead.SelectedValue = "0"
            'Sohail (29 Apr 2019) -- End
            txtGRate.Enabled = True
            txtGRate.Text = "0"
            radApplyToChecked.Checked = False
            radApplyToAll.Checked = False
            'Nilay (08-Aug-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim blnFlag As Boolean = False
        Try
            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If IsValidData() = False Then Exit Sub

            Dim dtmp() As DataRow = Nothing
            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")

            'Nilay (08-Aug-2016) -- Start
            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
            'If dtmp.Length > 0 Then
            '    For ridx As Integer = 0 To dtmp.Length - 1
            '        If objLoan_Advance IsNot Nothing Then objLoan_Advance = Nothing
            '        objLoan_Advance = New clsLoan_Advance

            '        Call SetValue(dtmp(ridx))

            '        'Nilay (25-Mar-2016) -- Start
            '        'blnFlag = objLoan_Advance.Insert(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name").ToString)

            '        blnFlag = objLoan_Advance.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                         Session("UserAccessModeSetting").ToString, True)
            '        'Nilay (25-Mar-2016) -- End

            '        If blnFlag = True Then
            '            If CInt(Session("_LoanVocNoType")) = 0 Then
            '                txtManualNo.Text = CStr(CInt(CInt(txtManualNo.Text) + 1))
            '            End If
            '            'Shani (21-Jul-2016) -- Start
            '            'Enhancement - Create New Loan Notification 
            '            Dim objProcessLoan As New clsProcess_pending_loan

            '            GUI.fmtCurrency = Session("fmtCurrency").ToString

            '            objProcessLoan.Send_Notification_After_Assign(Session("Database_Name").ToString, _
            '                                                          CInt(Session("UserId")), _
            '                                                          CInt(Session("Fin_year")), _
            '                                                          CInt(Session("CompanyUnkId")), _
            '                                                          CBool(Session("IsIncludeInactiveEmp")), _
            '                                                          mdtStartDate, mdtEndDate, _
            '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                                          CStr(Session("UserAccessModeSetting")), _
            '                                                          CStr(Session("ArutiSelfServiceURL")), _
            '                                                          objLoan_Advance._Loanadvancetranunkid, enLogin_Mode.MGR_SELF_SERVICE)

            '            objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
            '                                                      CInt(dtmp(ridx).Item("Pendingunkid")), _
            '                                                      clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
            '                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
            '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
            '                                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
            '            'Shani (21-Jul-2016) -- End
            '        Else
            '            If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
            '                Dim iRowIdx As Integer = -1
            '                iRowIdx = mdtTran.Rows.IndexOf(dtmp(ridx))
            '                If iRowIdx <> -1 Then
            '                    'dgvDataList.Rows(iRowIdx).DefaultCellStyle.ForeColor = Color.Red
            '                End If
            '                DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
            '                Exit Sub
            '            End If
            '        End If
            '    Next
            '    If blnFlag = True Then
            '        Call FillGrid()
            '    End If
            'End If

            If mdtTran.Select("IsGrp = false").Length <> dtmp.Length Then
                popupConfirmYesNo.Show()
                popupConfirmYesNo.Title = "Aruti"
                popupConfirmYesNo.Message = "You have not ticked all records for assignment." & "<BR></BR>" & "Do you want to continue?"
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
            Else
                popupConfirmYesNo_buttonYes_Click(sender, New EventArgs())
                'Nilay (20-Sept-2016) -- End
            End If
            'Nilay (08-Aug-2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAssign_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillGrid()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (08-Aug-2016) -- Start
    'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If dgvDataList.Items.Count <= 0 Then
                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                'Language.setLanguage(mstrModuleName)
                'Varsha (25 Nov 2017) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "No Data records found."), Me)
                Exit Sub
            End If

            If radApplyToAll.Checked = False AndAlso radApplyToChecked.Checked = False Then
                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                'Language.setLanguage(mstrModuleName)
                'Varsha (25 Nov 2017) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Please either tick Apply To Checked OR Apply To All for further process."), Me)
                Exit Sub
            End If

            If radApplyToChecked.Checked = True Then
                Dim dR() As DataRow = Nothing
                dR = mdtTran.Select("IsCheck = true AND IsGrp = false")
                If dR.Length <= 0 Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Please check atleast one record from the list for further process."), Me)
                    dgvDataList.Focus()
                    Exit Sub
                End If
            End If

            If CInt(cboGLoanCalcType.SelectedValue) <= 0 Then
                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                'Language.setLanguage(mstrModuleName)
                'Varsha (25 Nov 2017) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please select atleast one Loan calculation type for further process."), Me)
                cboGLoanCalcType.Focus()
                Exit Sub
            End If

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If CInt(cboGLoanCalcType.SelectedValue) > 0 AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest Then
            If CInt(cboGLoanCalcType.SelectedValue) > 0 AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                'Sohail (29 Apr 2019) -- End
                If CInt(cboGInterestCalcType.SelectedValue) <= 0 Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Please select atleast one Loan Interest calculation type for further process."), Me)
                    cboGInterestCalcType.Focus()
                    Exit Sub
                End If
                If CInt(cboGInterestCalcType.SelectedValue) > 0 Then

                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'If CInt(txtGRate.Text) <= 0 OrElse txtGRate.Text.Trim = "" Then
                    If CDec(txtGRate.Text) <= 0 OrElse txtGRate.Text.Trim = "" Then
                        'Language.setLanguage(mstrModuleName)
                        'Varsha (25 Nov 2017) -- End
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Please specify interest rate for further process."), Me)
                        txtGRate.Focus()
                        Exit Sub
                    End If
                End If
            End If

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboGLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "Please select Mapped Head to continue."), Me)
                cboMappedHead.Focus()
                Exit Sub
            End If
            'Sohail (29 Apr 2019) -- End

            Call SetCalculation(radApplyToAll.Checked)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Dim dtmp() As DataRow = Nothing
            dtmp = mdtTran.Select("IsCheck = true AND IsGrp = false")

            If dtmp.Length > 0 Then
                For ridx As Integer = 0 To dtmp.Length - 1
                    If objLoan_Advance IsNot Nothing Then objLoan_Advance = Nothing
                    objLoan_Advance = New clsLoan_Advance

                    Call SetValue(dtmp(ridx))

                    'Nilay (25-Mar-2016) -- Start
                    'blnFlag = objLoan_Advance.Insert(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name").ToString)

                    blnFlag = objLoan_Advance.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, True)
                    'Nilay (25-Mar-2016) -- End

                    If blnFlag = True Then
                        If CInt(Session("_LoanVocNoType")) = 0 Then
                            txtManualNo.Text = CStr(CInt(CInt(txtManualNo.Text) + 1))
                        End If
                        'Shani (21-Jul-2016) -- Start
                        'Enhancement - Create New Loan Notification 
                        Dim objProcessLoan As New clsProcess_pending_loan

                        GUI.fmtCurrency = Session("fmtCurrency").ToString

                        'Nilay (10-Sept-2016) -- Start
                        'Enhancement - Create New Loan Notification 
                        '            objProcessLoan.Send_Notification_After_Assign(Session("Database_Name").ToString, _
                        '                                                          CInt(Session("UserId")), _
                        '                                                          CInt(Session("Fin_year")), _
                        '                                                          CInt(Session("CompanyUnkId")), _
                        '                                                          CBool(Session("IsIncludeInactiveEmp")), _
                        '                                                          mdtStartDate, mdtEndDate, _
                        '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                        '                                                          CStr(Session("UserAccessModeSetting")), _
                        '                                                          CStr(Session("ArutiSelfServiceURL")), _
                        '                                                          objLoan_Advance._Loanadvancetranunkid, enLogin_Mode.MGR_SELF_SERVICE)

                        '            objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
                        '                                                      CInt(dtmp(ridx).Item("Pendingunkid")), _
                        '                                                      clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                        '                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                        '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
                        '                                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))

                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                        If mblnIsSendEmailNotification = True Then
                            Dim enMailType As New clsProcess_pending_loan.enApproverEmailType
                            'Nilay (08-Dec-2016) -- Start
                            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                            'If CInt(cboMode.SelectedIndex) = 0 Then 'LOAN
                            '    enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                            'ElseIf CInt(cboMode.SelectedIndex) = 1 Then 'ADVANCE
                            '    enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                            'End If
                            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                                enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                                enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                            End If
                            'Nilay (08-Dec-2016) -- End

                            objProcessLoan.Send_Notification_After_Assign(Session("Database_Name").ToString, _
                                                                          CInt(Session("UserId")), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          CBool(Session("IsIncludeInactiveEmp")), _
                                                                          mdtStartDate, mdtEndDate, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                          CStr(Session("UserAccessModeSetting")), _
                                                                          CStr(Session("ArutiSelfServiceURL")), _
                                                                          objLoan_Advance._Loanadvancetranunkid, _
                                                                          enMailType, _
                                                                              enLogin_Mode.MGR_SELF_SERVICE, , , _
                                                                              Session("Notify_LoanAdvance_Users").ToString _
                                                                              )
                            'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString]
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
                            '                                      CInt(dtmp(ridx).Item("Pendingunkid")), _
                            '                                      clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                            '                                      enMailType, _
                            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
                            '                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            objProcessLoan.Send_Notification_Employee(CInt(dtmp(ridx).Item("EmpId")), _
                                                                  CInt(dtmp(ridx).Item("Pendingunkid")), _
                                                                  clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                                                                  enMailType, _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), , _
                                                                  enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                        'Nilay (10-Dec-2016) -- End


                        'Nilay (10-Sept-2016) -- End

                        'Shani (21-Jul-2016) -- End
                    Else
                        If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                            Dim iRowIdx As Integer = -1
                            iRowIdx = mdtTran.Rows.IndexOf(dtmp(ridx))
                            If iRowIdx <> -1 Then
                                'dgvDataList.Rows(iRowIdx).DefaultCellStyle.ForeColor = Color.Red
                            End If
                            DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
                            Exit Sub
                        End If
                    End If
                Next
                If blnFlag = True Then
                    Call FillGrid()
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupConfirmYesNo_buttonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (08-Aug-2016) -- End

#End Region

#Region "ComboBox's Events"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, _
                                                                                                                cboApprover.SelectedIndexChanged, _
                                                                                                                cboMode.SelectedIndexChanged, _
                                                                                                                cboLoan.SelectedIndexChanged
        Try
            Select Case CType(sender, DropDownList).ID.ToUpper
                Case "CBOMODE"
                    Select Case CInt(cboMode.SelectedValue) 'Nilay (08-Dec-2016)
                        Case enLoanAdvance.LOAN 'Nilay (08-Dec-2016)
                            cboLoan.SelectedValue = "0"
                            cboLoan.Enabled = True
                            'Nilay (08-Aug-2016) -- Start
                            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
                            cboGLoanCalcType.Enabled = True
                            cboGInterestCalcType.Enabled = True
                            txtGRate.Enabled = True
                            'Nilay (04-Nov-2016) -- Start
                            'Enhancements: Global Change Status feature requested by {Rutta}
                            radApplyToAll.Enabled = True
                            radApplyToChecked.Enabled = True
                            btnApply.Enabled = True
                            'Nilay (04-Nov-2016) -- End
                            'Nilay (08-Aug-2016) -- End
                        Case enLoanAdvance.ADVANCE 'Nilay (08-Dec-2016)
                            cboLoan.SelectedValue = "0"
                            cboLoan.Enabled = False
                            'Nilay (08-Aug-2016) -- Start
                            'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
                            cboGLoanCalcType.Enabled = False
                            cboGInterestCalcType.Enabled = False
                            txtGRate.Enabled = False
                            'Nilay (04-Nov-2016) -- Start
                            'Enhancements: Global Change Status feature requested by {Rutta}
                            radApplyToAll.Enabled = False
                            radApplyToChecked.Enabled = False
                            btnApply.Enabled = False
                            'Nilay (04-Nov-2016) -- End
                            'Nilay (08-Aug-2016) -- End
                    End Select
            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cbodgcolhDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cboDeductionPeriod As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(cboDeductionPeriod.NamingContainer, DataGridItem)

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)
                item.Cells(enGridColumn.EffectiveDate).Text = objPeriod._Start_Date.ToShortDateString
                mdtTran.Rows(item.ItemIndex).Item("mdate") = eZeeDate.convertDate(objPeriod._Start_Date).ToString

                Dim dsList As New DataSet
                Dim objExRate As New clsExchangeRate
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, CDate(objPeriod._Start_Date.ToShortDateString), True)
                If dsList.Tables("ExRate").Rows.Count > 0 Then
                    mdtTran.Rows(item.ItemIndex).Item("PaidExRate") = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                End If

                mdtTran.AcceptChanges()
                objPeriod = Nothing
                objExRate = Nothing

            Else
                mdtTran.Rows(item.ItemIndex).Item("PaidExRate") = 1
                item.Cells(enGridColumn.EffectiveDate).Text = ""
                mdtTran.Rows(item.ItemIndex).Item("mdate") = ""
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cbodgcolhDeductionPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cbodgcolhLoanType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cboLoanType As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(cboLoanType.NamingContainer, DataGridItem)
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'Dim cbodgcolhInterestCalcType As DropDownList = CType(item.Cells(10).FindControl("cbodgcolhInterestCalcType"), DropDownList)
            Dim cbodgcolhInterestCalcType As DropDownList = CType(item.Cells(enGridColumn.InterestCalcType).FindControl("cbodgcolhInterestCalcType"), DropDownList)
            Dim cbodgcolhMappedHead As DropDownList = CType(item.Cells(enGridColumn.MappedHead).FindControl("cbodgcolhMappedHead"), DropDownList)
            'Sohail (29 Apr 2019) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If (item.Cells(8).Text) = "" Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), Me)
            '    CType(item.Cells(7).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Focus()
            '    cboLoanType.SelectedValue = "0"
            '    Exit Sub
            'End If
            If (item.Cells(enGridColumn.EffectiveDate).Text) = "" Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), Me)
                CType(item.Cells(enGridColumn.DeductionPeriod).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Focus()
                cboLoanType.SelectedValue = "0"
                Exit Sub
            End If
            'Sohail (29 Apr 2019) -- End

            If CInt(cboLoanType.SelectedValue) > 0 Then

                cbodgcolhInterestCalcType.Enabled = True
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                cbodgcolhMappedHead.Enabled = False
                'Sohail (29 Apr 2019) -- End

                If CInt(cboLoanType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    '    CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox).Enabled = True
                    '    CType(item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = True
                    '    CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False

                    'ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    '    CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox).Enabled = True
                    '    CType(item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = True
                    '    CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False

                    'ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.No_Interest Then

                    '    cbodgcolhInterestCalcType.SelectedValue = "0"
                    '    cbodgcolhInterestCalcType.Enabled = False

                    '    Dim txtRate As TextBox = CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox)
                    '    txtRate.Text = ""
                    '    txtRate.Enabled = False

                    '    Dim txtNoOfEMI As TextBox = CType(item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox)
                    '    txtNoOfEMI.Text = item.Cells(13).Text
                    '    txtNoOfEMI.Enabled = False

                    '    Dim txtInstallmentAmount As TextBox = CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                    '    txtInstallmentAmount.Text = item.Cells(15).Text
                    '    txtInstallmentAmount.Enabled = False
                    '    mdtTran.Rows(item.ItemIndex).Item("installmentamt") = item.Cells(15).Text

                    'ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    '    CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox).Enabled = True
                    '    CType(item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = True
                    '    CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False
                    CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox).Enabled = True
                    CType(item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = True
                    CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False

                ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox).Enabled = True
                    CType(item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = True
                    CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False

                ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.No_Interest Then

                    cbodgcolhInterestCalcType.SelectedValue = "0"
                    cbodgcolhInterestCalcType.Enabled = False

                    Dim txtRate As TextBox = CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox)
                    txtRate.Text = ""
                    txtRate.Enabled = False

                    Dim txtNoOfEMI As TextBox = CType(item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox)
                    txtNoOfEMI.Text = item.Cells(enGridColumn.NoOfInstallment_Hidden).Text
                    txtNoOfEMI.Enabled = False

                    Dim txtInstallmentAmount As TextBox = CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                    txtInstallmentAmount.Text = item.Cells(enGridColumn.InstallmentAmount_Hidden).Text
                    txtInstallmentAmount.Enabled = False
                    mdtTran.Rows(item.ItemIndex).Item("installmentamt") = item.Cells(enGridColumn.InstallmentAmount_Hidden).Text

                ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox).Enabled = True
                    CType(item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox).Enabled = True
                    CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox).Enabled = False
                    'Sohail (29 Apr 2019) -- End

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then

                    cbodgcolhInterestCalcType.SelectedValue = "0"
                    cbodgcolhInterestCalcType.Enabled = False
                    cbodgcolhMappedHead.Enabled = True

                    Dim txtRate As TextBox = CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox)
                    txtRate.Text = ""
                    txtRate.Enabled = False

                    Dim txtNoOfEMI As TextBox = CType(item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox)
                    txtNoOfEMI.Text = item.Cells(enGridColumn.NoOfInstallment_Hidden).Text
                    txtNoOfEMI.Enabled = False

                    Dim txtInstallmentAmount As TextBox = CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                    txtInstallmentAmount.Text = item.Cells(enGridColumn.InstallmentAmount_Hidden).Text
                    txtInstallmentAmount.Enabled = False
                    mdtTran.Rows(item.ItemIndex).Item("installmentamt") = item.Cells(enGridColumn.InstallmentAmount_Hidden).Text
                    'Sohail (29 Apr 2019) -- End

                End If

                mdtTran.Rows(item.ItemIndex).Item("calctypeId") = CInt(cboLoanType.SelectedValue)
                mdtTran.AcceptChanges()

                Call Calculate_Projected_Loan_Balance(CType(CInt(cboLoanType.SelectedValue), enLoanCalcId), _
                                                      CType(cbodgcolhInterestCalcType.SelectedValue, enLoanInterestCalcType), _
                                                      False, item)

            Else
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'Dim txtRate As TextBox = CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox)
                'txtRate.Text = ""
                'txtRate.Enabled = False
                'Dim txtNoOfEMI As TextBox = CType(item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox)
                'txtNoOfEMI.Text = item.Cells(13).Text
                'txtNoOfEMI.Enabled = False
                'Dim txtInstallmentAmount As TextBox = CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                'txtInstallmentAmount.Text = item.Cells(15).Text
                'txtInstallmentAmount.Enabled = False
                Dim txtRate As TextBox = CType(item.Cells(enGridColumn.Rate).FindControl("txtdgcolhRate"), TextBox)
                txtRate.Text = ""
                txtRate.Enabled = False
                Dim txtNoOfEMI As TextBox = CType(item.Cells(enGridColumn.NoOfInstallment).FindControl("txtdgcolhNoOfEMI"), TextBox)
                txtNoOfEMI.Text = item.Cells(enGridColumn.NoOfInstallment_Hidden).Text
                txtNoOfEMI.Enabled = False
                Dim txtInstallmentAmount As TextBox = CType(item.Cells(enGridColumn.InstallmentAmount).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                txtInstallmentAmount.Text = item.Cells(enGridColumn.InstallmentAmount_Hidden).Text
                txtInstallmentAmount.Enabled = False
                'Sohail (29 Apr 2019) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cbodgcolhLoanType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cbodgcolhInterestCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cboInterestCalcType As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(cboInterestCalcType.NamingContainer, DataGridItem)

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If (item.Cells(8).Text) = "" Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), Me)
            '    CType(item.Cells(7).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Focus()
            '    CType(item.Cells(9).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue = "0"
            '    cboInterestCalcType.SelectedValue = "0"
            '    Exit Sub
            'End If
            If (item.Cells(enGridColumn.EffectiveDate).Text) = "" Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), Me)
                CType(item.Cells(enGridColumn.DeductionPeriod).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Focus()
                CType(item.Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue = "0"
                cboInterestCalcType.SelectedValue = "0"
                Exit Sub
            End If
            'Sohail (29 Apr 2019) -- End

            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                mdtTran.Rows(item.ItemIndex).Item("interest_calctype_id") = CInt(cboInterestCalcType.SelectedValue)
                mdtTran.AcceptChanges()
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'Call Calculate_Projected_Loan_Balance(CType(CType(item.Cells(9).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue, enLoanCalcId), _
                '                                      CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType), False, item)
                Call Calculate_Projected_Loan_Balance(CType(CType(item.Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue, enLoanCalcId), _
                                                      CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType), False, item)
                'Sohail (29 Apr 2019) -- End
            Else
                'If CInt(cboLoanType.SelectedValue) <= 0 Then
                '    Dim txtRate As TextBox = CType(item.Cells(12).FindControl("txtdgcolhRate"), TextBox)
                '    txtRate.Text = ""
                '    txtRate.Enabled = False
                '    Dim txtNoOfEMI As TextBox = CType(item.Cells(14).FindControl("txtdgcolhNoOfEMI"), TextBox)
                '    txtNoOfEMI.Text = item.Cells(13).Text
                '    txtNoOfEMI.Enabled = False
                '    Dim txtInstallmentAmount As TextBox = CType(item.Cells(16).FindControl("txtdgcolhInstallmentAmount"), TextBox)
                '    txtInstallmentAmount.Text = item.Cells(15).Text
                '    txtInstallmentAmount.Enabled = False
                'End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cbodgcolhInterestCalcType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (08-Aug-2016) -- Start
    'ENHANCEMENT : Apply to check/all Calc. Type in Loan Global Assign for TRA & KBC Requested by {Ruta}
    Private Sub cboGLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGLoanCalcType.SelectedIndexChanged
        Try
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboGLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.SelectedValue = "0"
                cboMappedHead.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End

            If CInt(cboGLoanCalcType.SelectedValue) > 0 Then
                If CInt(cboGLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                    cboGInterestCalcType.SelectedValue = CStr(0)
                    cboGInterestCalcType.Enabled = False
                    txtGRate.Text = CStr(0)
                    txtGRate.Enabled = False
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboGLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                    cboGInterestCalcType.SelectedValue = "0"
                    cboGInterestCalcType.Enabled = False
                    txtGRate.Text = CStr(0)
                    txtGRate.Enabled = False
                    cboMappedHead.Enabled = True
                    'Sohail (29 Apr 2019) -- End
                Else
                    cboGInterestCalcType.SelectedValue = CStr(0)
                    cboGInterestCalcType.Enabled = True
                    txtGRate.Text = "0"
                    txtGRate.Enabled = True
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboGLoanCalcType_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (08-Aug-2016) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Protected Sub cbodgcolhMappedHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cboMappedHead As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(cboMappedHead.NamingContainer, DataGridItem)

            If (item.Cells(enGridColumn.EffectiveDate).Text) = "" Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Deduction period is mandatory information. Please select deduction period to continue."), Me)
                CType(item.Cells(enGridColumn.DeductionPeriod).FindControl("cbodgcolhDeductionPeriod"), DropDownList).Focus()
                CType(item.Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue = "0"
                cboMappedHead.SelectedValue = "0"
                Exit Sub
            End If

            If CInt(cboMappedHead.SelectedValue) > 0 Then
                mdtTran.Rows(item.ItemIndex).Item("mapped_tranheadunkid") = CInt(cboMappedHead.SelectedValue)
                mdtTran.AcceptChanges()
                Call Calculate_Projected_Loan_Balance(CType(CType(item.Cells(enGridColumn.LoanCalcType).FindControl("cbodgcolhLoanType"), DropDownList).SelectedValue, enLoanCalcId), _
                                                      CType(CType(item.Cells(enGridColumn.InterestCalcType).FindControl("cbodgcolhInterestCalcType"), DropDownList).SelectedValue, enLoanInterestCalcType), False, item)
            Else

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (29 Apr 2019) -- End

#End Region

#Region "TextBox's Events"

    Protected Sub txtdgcolhRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtRate As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtRate.NamingContainer, DataGridItem)

            If txtRate.Text <> "" Then
                If CDec(txtRate.Text) > 100 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Rate of Interest cannot exceed 100. Please enter correct rate of interest."), Me)
                    txtRate.Text = "0.0"
                    Exit Sub
                End If
                txtRate.Text = CDec(txtRate.Text).ToString("###0.00#")
                mdtTran.Rows(item.ItemIndex).Item("rate") = CDec(txtRate.Text)

            End If

            Call Calculate_Projected_Loan_Balance(CType(CInt(mdtTran.Rows(item.ItemIndex).Item("calctypeId")), enLoanCalcId), _
                                                  CType(CInt(mdtTran.Rows(item.ItemIndex).Item("interest_calctype_id")), enLoanInterestCalcType), _
                                                  False, item)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtdgcolhRate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtdgcolhNoOfEMI_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtNoOfEMI As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtNoOfEMI.NamingContainer, DataGridItem)

            If txtNoOfEMI.Text.Trim = "" OrElse txtNoOfEMI.Text = "0" Then
                txtNoOfEMI.Text = CStr(1)
            End If

            mdtTran.Rows(item.ItemIndex).Item("noofinstallment") = CDec(txtNoOfEMI.Text)
            Call Calculate_Projected_Loan_Balance(CType(CInt(mdtTran.Rows(item.ItemIndex).Item("calctypeId")), enLoanCalcId), _
                                                  CType(CInt(mdtTran.Rows(item.ItemIndex).Item("interest_calctype_id")), enLoanInterestCalcType), _
                                                  True, item)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtdgcolhNoOfEMI_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtdgcolhInstallmentAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtIntallmentAmount As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtIntallmentAmount.NamingContainer, DataGridItem)

            txtIntallmentAmount.Text = CDec(txtIntallmentAmount.Text).ToString("#############.00#")
            mdtTran.Rows(item.ItemIndex).Item("installmentamt") = CDec(txtIntallmentAmount.Text)
            Call Calculate_Projected_Loan_Balance(CType(CInt(mdtTran.Rows(item.ItemIndex).Item("calctypeId")), enLoanCalcId), _
                                                  CType(CInt(mdtTran.Rows(item.ItemIndex).Item("interest_calctype_id")), enLoanInterestCalcType), _
                                                  False, item)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtdgcolhInstallmentAmount_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox'S Events"

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvDataList.Items.Count <= 0 Then Exit Sub
            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If CBool(item.Cells(17).Text) = True Then

            '    Dim dRow() As DataRow = mdtTran.Select("GrpId =" & CInt(item.Cells(20).Text))
            '    For Each row In dRow
            '        row.Item("IsCheck") = chkSelect.Checked
            '        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(row)).Cells(2).FindControl("chkSelect"), CheckBox).Checked = chkSelect.Checked
            '    Next

            'Else
            '    mdtTran.Rows(item.ItemIndex)("IsCheck") = chkSelect.Checked
            '    mdtTran.AcceptChanges()
            '    Dim dRow() As DataRow = mdtTran.Select("GrpId =" & CInt(item.Cells(20).Text) & " AND IsCheck = 'False' AND IsGrp = 'False'")
            '    Dim grpRow() As DataRow = mdtTran.Select("GrpId=" & CInt(item.Cells(20).Text) & " AND IsGrp = 'True'")
            '    If dRow.Length > 0 Then
            '        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(grpRow(0))).Cells(1).FindControl("chkSelect"), CheckBox).Checked = False
            '    Else
            '        CType(dgvDataList.Items(mdtTran.Rows.IndexOf(grpRow(0))).Cells(1).FindControl("chkSelect"), CheckBox).Checked = True
            '    End If

            'End If
            If CBool(item.Cells(enGridColumn.IsGroup).Text) = True Then

                Dim dRow() As DataRow = mdtTran.Select("GrpId =" & CInt(item.Cells(enGridColumn.GroupId_Hidden).Text))
                For Each row In dRow
                    row.Item("IsCheck") = chkSelect.Checked
                    CType(dgvDataList.Items(mdtTran.Rows.IndexOf(row)).Cells(enGridColumn.IsChecked).FindControl("chkSelect"), CheckBox).Checked = chkSelect.Checked
                Next

            Else
                mdtTran.Rows(item.ItemIndex)("IsCheck") = chkSelect.Checked
                mdtTran.AcceptChanges()
                Dim dRow() As DataRow = mdtTran.Select("GrpId =" & CInt(item.Cells(enGridColumn.GroupId_Hidden).Text) & " AND IsCheck = 'False' AND IsGrp = 'False'")
                Dim grpRow() As DataRow = mdtTran.Select("GrpId=" & CInt(item.Cells(enGridColumn.GroupId_Hidden).Text) & " AND IsGrp = 'True'")
                If dRow.Length > 0 Then
                    CType(dgvDataList.Items(mdtTran.Rows.IndexOf(grpRow(0))).Cells(enGridColumn.IsChecked_Hidden).FindControl("chkSelect"), CheckBox).Checked = False
                Else
                    CType(dgvDataList.Items(mdtTran.Rows.IndexOf(grpRow(0))).Cells(enGridColumn.IsChecked_Hidden).FindControl("chkSelect"), CheckBox).Checked = True
                End If

            End If
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "LinkButton Event"
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.lblLoan.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoan.ID, Me.lblLoan.Text)
            Me.lblMode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMode.ID, Me.lblMode.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblPurpose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPurpose.ID, Me.lblPurpose.Text)
            Me.lblVoucherNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVoucherNo.ID, Me.lblVoucherNo.Text)
            Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)

            Me.btnAssign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAssign.ID, Me.btnAssign.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'Me.dgvDataList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(3).FooterText, Me.dgvDataList.Columns(3).HeaderText)
            'Me.dgvDataList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(4).FooterText, Me.dgvDataList.Columns(4).HeaderText)
            'Me.dgvDataList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(5).FooterText, Me.dgvDataList.Columns(5).HeaderText)
            'Me.dgvDataList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(6).FooterText, Me.dgvDataList.Columns(6).HeaderText)
            'Me.dgvDataList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(7).FooterText, Me.dgvDataList.Columns(7).HeaderText)
            'Me.dgvDataList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(8).FooterText, Me.dgvDataList.Columns(8).HeaderText)
            'Me.dgvDataList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(9).FooterText, Me.dgvDataList.Columns(9).HeaderText)
            'Me.dgvDataList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(10).FooterText, Me.dgvDataList.Columns(10).HeaderText)
            'Me.dgvDataList.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(12).FooterText, Me.dgvDataList.Columns(12).HeaderText)
            'Me.dgvDataList.Columns(14).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(14).FooterText, Me.dgvDataList.Columns(14).HeaderText)
            'Me.dgvDataList.Columns(16).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvDataList.Columns(16).FooterText, Me.dgvDataList.Columns(16).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.ApplicationNo).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.ApplicationNo).FooterText, Me.dgvDataList.Columns(enGridColumn.ApplicationNo).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.Employee).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.Employee).FooterText, Me.dgvDataList.Columns(enGridColumn.Employee).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.Approver).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.Approver).FooterText, Me.dgvDataList.Columns(enGridColumn.Approver).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.Amount).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.Amount).FooterText, Me.dgvDataList.Columns(enGridColumn.Amount).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.DeductionPeriod).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.DeductionPeriod).FooterText, Me.dgvDataList.Columns(enGridColumn.DeductionPeriod).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.EffectiveDate).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.EffectiveDate).FooterText, Me.dgvDataList.Columns(enGridColumn.EffectiveDate).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.LoanCalcType).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.LoanCalcType).FooterText, Me.dgvDataList.Columns(enGridColumn.LoanCalcType).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.InterestCalcType).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.InterestCalcType).FooterText, Me.dgvDataList.Columns(enGridColumn.InterestCalcType).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.MappedHead).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.MappedHead).FooterText, Me.dgvDataList.Columns(enGridColumn.MappedHead).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.Rate).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.Rate).FooterText, Me.dgvDataList.Columns(enGridColumn.Rate).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.NoOfInstallment).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.NoOfInstallment).FooterText, Me.dgvDataList.Columns(enGridColumn.NoOfInstallment).HeaderText)
            Me.dgvDataList.Columns(enGridColumn.InstallmentAmount).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvDataList.Columns(enGridColumn.InstallmentAmount).FooterText, Me.dgvDataList.Columns(enGridColumn.InstallmentAmount).HeaderText)

            Me.lblInterestCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterestCalcType.ID, Me.lblInterestCalcType.Text)
            Me.lblGLoanCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGLoanCalcType.ID, Me.lblGLoanCalcType.Text)
            Me.lblMappedHead.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMappedHead.ID, Me.lblMappedHead.Text)
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
