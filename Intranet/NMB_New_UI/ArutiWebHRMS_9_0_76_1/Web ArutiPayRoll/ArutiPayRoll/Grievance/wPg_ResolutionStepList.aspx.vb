﻿
#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Grievance_wPg_ResolutionStepList
    Inherits Basepage
#Region "Private Variables"
    Private objresolution_step_tran As New clsResolution_Step_Tran
    Private objclsgrievanceapprover_master As New clsgrievanceapprover_master
    Dim DisplayMessage As New CommonCodes
    Private dtResolutionStepList As New DataTable
    Private mintgrievancemasterunkid As Integer = 0

    Dim objGrievance_Master As New clsGrievance_Master

    'Gajanan [5-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance NMB Report
    'Private objGrievanceDetailReport As New clsGrievanceDetailReport
    Private objGrievanceDetailReport As clsGrievanceReport
    'Gajanan [5-July-2019] -- End

    Private Shared ReadOnly mstrModuleName As String = "frmResolutionStepList"



#End Region

#Region "Page's Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try   'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            'Private objGrievanceDetailReport As New clsGrievanceDetailReport
            objGrievanceDetailReport = New clsGrievanceReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            'Gajanan [5-July-2019] -- End


            If IsPostBack = False Then
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                'Sohail (22 Nov 2018) -- End

                FillCombo()
                FillList(True)
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    btnNew.Visible = CBool(Session("AllowToAddGrievanceResolutionStep"))
                    dgResolutionStepList.Columns(0).Visible = CBool(Session("AllowToEditGrievanceResolutionStep"))
                    dgResolutionStepList.Columns(1).Visible = CBool(Session("AllowToDeleteGrievanceResolutionStep"))
                End If

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            Else
                If ViewState("dtResolutionStepList") IsNot Nothing Then
                    dtResolutionStepList = CType(ViewState("dtResolutionStepList"), DataTable)
                    If dtResolutionStepList.Rows.Count > 0 Then
                        dgResolutionStepList.DataSource = dtResolutionStepList
                        dgResolutionStepList.DataBind()
                    End If
                End If
                'Gajanan [27-June-2019] -- End
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub
#End Region

#Region "Private Methods"
    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objclsgrievanceapprover_master.GetEmployeeByApprover(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) _
                                                                              , "List", CInt(Session("UserId")), (Session("Database_Name").ToString) _
                                                                              , CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")) _
                                                                              , Session("UserAccessModeSetting").ToString _
                                                                              , Session("EmployeeAsOnDate").ToString(), True, True)

                With cboEmpName
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    'S.SANDEEP [02-NOV-2018] -- START
                    '.DataSource = dsList.Tables(0)
                    .DataSource = dsList.Tables(0).DefaultView.ToTable("employeecode", True, "EmpCodeName", "employeeunkid")
                    'S.SANDEEP [02-NOV-2018] -- END
                    .DataBind()
                    .SelectedValue = "0"
                End With

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmpName.DataSource = objglobalassess.ListOfEmployee
                cboEmpName.DataTextField = "loginname"
                cboEmpName.DataValueField = "employeeunkid"
                cboEmpName.DataBind()
            End If

            dsList = objresolution_step_tran.GetResponse_Type("Response Type", True)

            With cboRepsonseType
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Response Type")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "Grievance Type")
            With cboGrievanceType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Grievance Type")
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)   'Hemant (13 Aug 2020)
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As New DataSet
        Dim strfilter As String = String.Empty
        Dim dtTable As New DataTable
        Dim mintprioriy As Integer = 0
        Dim objApprover As New clsgrievanceapprover_master
        Dim dspriority As New DataSet

        Try
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewResolutionStepList")) = False Then Exit Sub
            End If

            If dtGrievanceDateFrom.IsNull = False Then
                strfilter &= "AND greresolution_step_tran.meetingdate >= '" & eZeeDate.convertDate(dtGrievanceDateFrom.GetDate.Date) & "' "
            End If

            If dtGrievanceDateTo.IsNull = False Then
                strfilter &= "AND greresolution_step_tran.meetingdate <= '" & eZeeDate.convertDate(dtGrievanceDateTo.GetDate.Date) & "' "
            End If

            If cboRepsonseType.SelectedIndex > 0 Then
                strfilter &= "AND greresolution_step_tran.responsetypeunkid = '" & cboRepsonseType.SelectedValue & "' "
            End If

            If cboEmpName.SelectedIndex > 0 Then

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                'strfilter &= "AND gregrievance_master.fromemployeeunkid = '" & cboEmpName.SelectedValue & "' "
                strfilter &= "AND gregrievance_master.againstemployeeunkid = '" & cboEmpName.SelectedValue & "' "
                'Gajanan [27-June-2019] -- End

            End If

            If TxtRefNo.Text.Length > 0 Then
                strfilter &= "AND gregrievance_master.grievancerefno = '" & TxtRefNo.Text & "' "
            End If

            If cboRepsonseType.SelectedIndex > 0 Then
                strfilter &= "AND greresolution_step_tran.responsetypeunkid = '" & cboRepsonseType.SelectedValue & "' "
            End If

            If cboGrievanceType.SelectedIndex > 0 Then
                strfilter &= "AND gregrievance_master.grievancetypeid = '" & cboGrievanceType.SelectedValue & "' "
            End If


            If isblank Then
                strfilter = "and 1 = 2 "
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes

            'dspriority = objApprover.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
            '                                  Session("Database_Name").ToString, _
            '                                  CInt(Session("UserId")), _
            '                                  CInt(Session("Fin_year")), _
            '                                  CInt(Session("CompanyUnkId")), 1202, _
            '                                  Session("UserAccessModeSetting").ToString, True, False, _
            '                                  True, Session("EmployeeAsOnDate").ToString(), True, Nothing, "", False, " And greapprover_master.mapuserunkid= " + CInt(Session("UserId")).ToString)

            dspriority = objApprover.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
                                              Session("Database_Name").ToString, _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), 1202, _
                                              Session("UserAccessModeSetting").ToString, True, False, _
                                              True, Session("EmployeeAsOnDate").ToString(), True, Nothing, "", False)

            'Gajanan [4-July-2019] -- End

            If dspriority.Tables(0).Rows.Count > 0 Then
                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                'mintprioriy = CInt(dspriority.Tables(0).Rows(0)("priority").ToString)
                mintprioriy = CInt(dspriority.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = CInt(Session("UserId"))).Select(Function(x) x.Field(Of Integer)("priority")).FirstOrDefault())
                'Gajanan [24-OCT-2019] -- End
            End If


            If mintprioriy > 0 Then

                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                'strfilter &= " and greapproverlevel_master.priority <= " & mintprioriy & " "
                Select Case CType(Session("GrievanceApprovalSetting"), enGrievanceApproval)

                    Case enGrievanceApproval.ReportingTo
                        strfilter &= " and #Results.priority <= " & mintprioriy & " "
                    Case Else
                        strfilter &= " and greapproverlevel_master.priority <= " & mintprioriy & " "
                End Select
                'Gajanan [24-OCT-2019] -- End
            End If


            dsList = objresolution_step_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), True, False, strfilter)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = objresolution_step_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), True, True, "And 1=2")
                isblank = True
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes


            For Each iRow As DataRow In dsList.Tables(0).Rows
                Dim drRow As DataRow() = Nothing
                If iRow("approverempid").ToString() = iRow("mapuserunkid").ToString() Then
                    drRow = dspriority.Tables(0).Select("approverempid = " & iRow("mapuserunkid").ToString() & "")
                Else
                    drRow = dspriority.Tables(0).Select("approverempid = " & iRow("approverempid").ToString() & "")
                End If


                If drRow.Length > 0 Then
                    iRow("approvername") = drRow(0)("name").ToString()
                End If
            Next
            'Gajanan [4-July-2019] -- End



            Dim mdtTran As DataTable = dsList.Tables(0)

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            dtResolutionStepList = mdtTran.Clone

            If isblank = False Then

                Dim strRefno As String = ""
                dtResolutionStepList.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow() As DataRow = Nothing
                Dim grpRow As DataRow = Nothing
                For Each drow As DataRow In mdtTran.Rows
                    If CStr(drow("grievancerefno")).Trim <> strRefno.Trim Then
                        grpRow = dtResolutionStepList.NewRow
                        grpRow("IsGrp") = True
                        grpRow("grievancerefno") = drow("grievancerefno")
                        grpRow("RaisedByEmp") = drow("RaisedByEmp")


                        strRefno = drow("grievancerefno").ToString()
                        dtResolutionStepList.Rows.Add(grpRow)
                        dtRow = mdtTran.Select(("grievancerefno = '" & strRefno.Trim & "' "))
                        If dtRow.Count > 0 Then
                            For Each irow As DataRow In dtRow
                                dtResolutionStepList.ImportRow(irow)
                            Next
                        End If
                    End If
                Next
            End If
            'Gajanan [27-June-2019] -- End


            'dgResolutionStepList.DataSource = mdtTran

            If isblank = False Then
                dgResolutionStepList.DataSource = dtResolutionStepList
                Me.ViewState.Add("dtResolutionStepList", dtResolutionStepList)
            Else
                dgResolutionStepList.DataSource = mdtTran
                Me.ViewState.Add("dtResolutionStepList", mdtTran)
            End If
            dgResolutionStepList.DataBind()

            If isblank Then
                dgResolutionStepList.Rows(0).Visible = False
            End If

            'If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count <= 0 Then
            '    Dim drRow As DataRow = mdtTran.NewRow
            '    drRow("GrievanceType") = ""
            '    drRow("Employee") = ""
            '    drRow("MeetingDate") = DBNull.Value
            '    drRow("ResponseRemark") = ""
            '    mdtTran.Rows.Add(drRow)
            '    dgResolutionStepList.DataSource = mdtTran
            '    dgResolutionStepList.DataBind()
            '    dgResolutionStepList.Rows(0).Visible = False
            'Else
            '    dgResolutionStepList.AutoGenerateColumns = False
            '    dgResolutionStepList.DataSource = mdtTran
            '    dgResolutionStepList.DataBind()
            'End If


            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            'Me.ViewState.Add("ResolutionStep", dsList.Tables("ResolutionStepList"))
            'Gajanan [27-June-2019] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Private Sub ResetValue()
        Try

            cboEmpName.SelectedValue = "0"
            cboGrievanceType.SelectedValue = "0"
            cboRepsonseType.SelectedValue = "0"
            TxtRefNo.Text = ""
            dtGrievanceDateFrom.SetDate = Nothing
            dtGrievanceDateTo.SetDate = Nothing
            FillList(True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#DDD")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    'Gajanan [27-June-2019] -- End
#End Region

#Region "Button's Events"
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("ResolutionStepTranId") = Nothing
            Me.ViewState("ResolutionStepList") = Nothing
            Session("grievancemasteid") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Grievance/wPg_ResolutionStep.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub




    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
        Try
            Dim dtResolutionStep As DataTable = CType(Me.ViewState("ResolutionStep"), DataTable)
            Dim intRowIndex As Integer = CInt(Me.ViewState("RIndex"))
            objresolution_step_tran._FormName = mstrModuleName
            objresolution_step_tran._ClientIP = CStr(Session("IP_ADD"))
            objresolution_step_tran._HostName = CStr(Session("HOST_NAME"))
            objresolution_step_tran._AuditDate = Now
            objresolution_step_tran._AuditUserId = CInt(Session("UserId"))

            objresolution_step_tran.Delete(CInt(dtResolutionStep.Rows(intRowIndex)("resolutionsteptranunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason, CStr(Session("Database_Name")))


            FillList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview Events"


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes



    'Protected Sub dgResolutionStepList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgResolutionStepList.RowCommand
    '    Try
    '        Dim dtResolutionStep As DataTable = CType(Me.ViewState("ResolutionStep"), DataTable)
    '        If e.CommandName = "Change" Then
    '            Session.Add("ResolutionStepTranid", CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("resolutionsteptranunkid")))
    '            Session.Add("grievancemasteid", CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("grievancemasterunkid")))
    '            Response.Redirect(Session("rootpath").ToString & "Grievance/wPg_ResolutionStep.aspx", False)

    '        ElseIf e.CommandName = "Remove" Then
    '            Me.ViewState.Add("RIndex", e.CommandArgument)

    '            'Gajanan [24-June-2019] -- Start      
    '            popupDelete.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Are you sure you want to delete this Resolution Step?")
    '            'Gajanan [24-June-2019] -- End
    '            popupDelete.Show()

    '        ElseIf e.CommandName = "Preview" Then


    '            mintgrievancemasterunkid = CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("grievancemasterunkid"))
    '            objGrievance_Master._Grievancemasterunkid = mintgrievancemasterunkid


    '            objGrievanceDetailReport.SetDefaultValue()



    '            objGrievanceDetailReport._UserAccessFilter = ""
    '            objGrievanceDetailReport._AdvanceFilter = ""
    '            objGrievanceDetailReport._IncludeAccessFilterQry = False

    '            objGrievanceDetailReport._Refno = objGrievance_Master._Grievancerefno





    '            objGrievanceDetailReport.setDefaultOrderBy(0)

    '            objGrievanceDetailReport._CompanyUnkid = CInt(Session("CompanyUnkId"))
    '            objGrievanceDetailReport._UserUnkid = CInt(Session("UserId"))

    '            objGrievanceDetailReport.generateReportNew(Session("Database_Name"), _
    '                                          Session("UserId"), _
    '                                          Session("Fin_year"), _
    '                                          Session("CompanyUnkId"), _
    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                          Session("UserAccessModeSetting"), True, _
    '                                          Session("ExportReportPath"), _
    '                                          Session("OpenAfterExport"), 0, _
    '                                          enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

    '            Session("objRpt") = objGrievanceDetailReport._Rpt
    '            If Session("objRpt") IsNot Nothing Then
    '                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
    '            End If

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim dtResolutionStep As DataTable = CType(Me.ViewState("dtResolutionStepList"), DataTable)

            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            Session.Add("ResolutionStepTranid", CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("resolutionsteptranunkid")))
            Session.Add("grievancemasteid", CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("grievancemasterunkid")))
            Response.Redirect(Session("rootpath").ToString & "Grievance/wPg_ResolutionStep.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim dtResolutionStep As DataTable = CType(Me.ViewState("dtResolutionStepList"), DataTable)

            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintgrievancemasterunkid = CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("grievancemasterunkid"))
            objGrievance_Master._Grievancemasterunkid = mintgrievancemasterunkid


            objGrievanceDetailReport.SetDefaultValue()



            objGrievanceDetailReport._UserAccessFilter = ""
            objGrievanceDetailReport._AdvanceFilter = ""
            objGrievanceDetailReport._IncludeAccessFilterQry = False

            objGrievanceDetailReport._Refno = objGrievance_Master._Grievancerefno

            objGrievanceDetailReport._Approvalsetting = Convert.ToInt32(Session("GrievanceApprovalSetting").ToString())

            objGrievanceDetailReport.setDefaultOrderBy(0)

            objGrievanceDetailReport._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objGrievanceDetailReport._UserUnkid = CInt(Session("UserId"))

            objGrievanceDetailReport.generateReportNew(Session("Database_Name"), _
                                          Session("UserId"), _
                                          Session("Fin_year"), _
                                          Session("CompanyUnkId"), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                          Session("UserAccessModeSetting"), True, _
                                          Session("ExportReportPath"), _
                                          Session("OpenAfterExport"), 0, _
                                          enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objGrievanceDetailReport._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            Me.ViewState.Add("RIndex", lnk.CommandArgument)
            popupDelete.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Are you sure you want to delete this Resolution Step?")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    'Gajanan [27-June-2019] -- End


    Protected Sub dgResolutionStepList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgResolutionStepList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                If dt.Columns.Contains("IsGrp") = True AndAlso IsDBNull(dt.Rows(e.Row.RowIndex)("IsGrp")) = False AndAlso CBool(dt.Rows(e.Row.RowIndex)("IsGrp").ToString()) = True Then
                    Me.AddGroup(e.Row, "<b> " & dt.Rows(e.Row.RowIndex)("RaisedByEmp").ToString() & " (" & dt.Rows(e.Row.RowIndex)("grievancerefno").ToString() & ")</b>", dgResolutionStepList)
                End If
                'Gajanan [27-June-2019] -- End


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                'If CInt(dgResolutionStepList.DataKeys(e.Row.RowIndex)("resolutionsteptranunkid").ToString) > 0 Then
                If IsDBNull(dgResolutionStepList.DataKeys(e.Row.RowIndex)("resolutionsteptranunkid")) = False AndAlso CInt(dgResolutionStepList.DataKeys(e.Row.RowIndex)("resolutionsteptranunkid").ToString) > 0 Then
                    'Gajanan [27-June-2019] -- End

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgResolutionStepList, "colhMeetingDate", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgResolutionStepList, "colhMeetingDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgResolutionStepList, "colhMeetingDate", False, True)).Text).ToShortDateString()
                    End If


                    'Gajanan [4-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changesx
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgResolutionStepList, "colhmembers", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgResolutionStepList, "colhmembers", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgResolutionStepList, "colhmembers", False, True)).Text.Replace(",", "<br/>")
                    End If
                    'Gajanan [4-July-2019] -- End      




                    Dim lnkselect As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                    Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                    'Gajanan [24-OCT-2019] -- Start   
                    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    Select Case CType(Session("GrievanceApprovalSetting"), enGrievanceApproval)



                        Case enGrievanceApproval.ReportingTo
                            Dim objuser As New clsUserAddEdit
                            objuser._Userunkid = CInt(Session("UserId"))


                            If CInt(dgResolutionStepList.DataKeys(e.Row.RowIndex)("mapuserunkid").ToString) <> objuser._EmployeeUnkid Then
                                lnkselect.Visible = False
                                lnkDelete.Visible = False
                            Else
                                If Convert.ToBoolean(dgResolutionStepList.DataKeys(e.Row.RowIndex)("issubmitted").ToString) AndAlso CInt(dgResolutionStepList.DataKeys(e.Row.RowIndex)("mapuserunkid").ToString) = objuser._EmployeeUnkid Then
                                    lnkselect.Visible = False
                                    lnkDelete.Visible = False
                                Else
                                    lnkselect.Visible = True
                                    lnkDelete.Visible = True
                                End If
                            End If
                        Case Else
                            If CInt(dgResolutionStepList.DataKeys(e.Row.RowIndex)("mapuserunkid").ToString) <> CInt(Session("UserId")) Then
                                lnkselect.Visible = False
                                lnkDelete.Visible = False
                            Else
                                If Convert.ToBoolean(dgResolutionStepList.DataKeys(e.Row.RowIndex)("issubmitted").ToString) AndAlso CInt(dgResolutionStepList.DataKeys(e.Row.RowIndex)("mapuserunkid").ToString) = CInt(Session("UserId")) Then
                                    lnkselect.Visible = False
                                    lnkDelete.Visible = False
                                Else
                                    lnkselect.Visible = True
                                    lnkDelete.Visible = True
                                End If
                            End If
                    End Select

                    'Gajanan [24-OCT-2019] -- End

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region

#Region " Link Button's Events "

#End Region

#Region " Language & UI Settings "
    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblGrievanceDateFrom.ID, Me.lblGrievanceDateFrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblGrievanceDateTo.ID, Me.lblGrievanceDateTo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblGrievanceType.ID, Me.lblGrievanceType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRefNo.ID, Me.lblRefNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblResponseType.ID, Me.lblResponseType.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNew.ID, Me.btnNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(0).FooterText, dgResolutionStepList.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(1).FooterText, dgResolutionStepList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(3).FooterText, dgResolutionStepList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(4).FooterText, dgResolutionStepList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(5).FooterText, dgResolutionStepList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(6).FooterText, dgResolutionStepList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(7).FooterText, dgResolutionStepList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(8).FooterText, dgResolutionStepList.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(9).FooterText, dgResolutionStepList.Columns(9).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(10).FooterText, dgResolutionStepList.Columns(10).HeaderText)

            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgResolutionStepList.Columns(11).FooterText, dgResolutionStepList.Columns(11).HeaderText)
            'Gajanan [5-July-2019] -- End

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblGrievanceDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrievanceDateFrom.ID, Me.lblGrievanceDateFrom.Text)
            Me.lblGrievanceDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrievanceDateTo.ID, Me.lblGrievanceDateTo.Text)
            Me.lblGrievanceType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrievanceType.ID, Me.lblGrievanceType.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRefNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRefNo.ID, Me.lblRefNo.Text)
            Me.lblResponseType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblResponseType.ID, Me.lblResponseType.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            dgResolutionStepList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(0).FooterText, dgResolutionStepList.Columns(0).HeaderText)
            dgResolutionStepList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(1).FooterText, dgResolutionStepList.Columns(1).HeaderText)
            dgResolutionStepList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(3).FooterText, dgResolutionStepList.Columns(3).HeaderText)
            dgResolutionStepList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(4).FooterText, dgResolutionStepList.Columns(4).HeaderText)
            dgResolutionStepList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(5).FooterText, dgResolutionStepList.Columns(5).HeaderText)
            dgResolutionStepList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(6).FooterText, dgResolutionStepList.Columns(6).HeaderText)
            dgResolutionStepList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(7).FooterText, dgResolutionStepList.Columns(7).HeaderText)
            dgResolutionStepList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(8).FooterText, dgResolutionStepList.Columns(8).HeaderText)
            dgResolutionStepList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(9).FooterText, dgResolutionStepList.Columns(9).HeaderText)
            dgResolutionStepList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(10).FooterText, dgResolutionStepList.Columns(10).HeaderText)
            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            dgResolutionStepList.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgResolutionStepList.Columns(11).FooterText, dgResolutionStepList.Columns(11).HeaderText)
            'Gajanan [5-July-2019] -- End

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Are you sure you want to delete this Resolution Step?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
