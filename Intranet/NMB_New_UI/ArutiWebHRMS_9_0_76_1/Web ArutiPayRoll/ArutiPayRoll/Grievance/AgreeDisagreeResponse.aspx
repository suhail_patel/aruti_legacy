﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="AgreeDisagreeResponse.aspx.vb"
    Inherits="Grievance_AgreeDisagreeResponse" Title="Agree Disagree Response" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Agree/Disagree Response"></asp:Label>
                                    <asp:LinkButton ID="btnViewPreviousResolution" runat="server" Visible="false">
                                        <i class="fas fa-comments text-primary"> </i>
                                    </asp:LinkButton>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGreType" runat="server" Text="Grievnce Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpGreType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAgainstEmp" runat="server" Text="Against Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAgainstEmp" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblgrerefno" runat="server" Text="Ref No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtgrerefno" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblgredate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtgredate" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li role="presentation" class="active"><a href="#GrievanceDescription" data-toggle="tab">
                                                <asp:Label ID="TabGre" runat="server" Text="Grievance Description" CssClass="form-label"></asp:Label>
                                            </a></li>
                                            <li role="presentation"><a href="#ReliefWanted" data-toggle="tab">
                                                <asp:Label ID="TabRelifWanted" runat="server" Text="Relief Wanted" CssClass="form-label"></asp:Label>
                                            </a></li>
                                            <li role="presentation"><a href="#AttachDocument" data-toggle="tab">
                                                <asp:Label ID="TabAttachDocument" runat="server" Text="Attach Document" CssClass="form-label"></asp:Label>
                                            </a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="GrievanceDescription">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtGreDesc" runat="server" Rows="5" CssClass="form-control" TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="ReliefWanted">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtRelifWanted" runat="server" Rows="5" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AttachDocument">
                                                <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gvGrievanceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            DataKeyNames="GUID,scanattachtranunkid,localpath">
                                                            <Columns>
                                                                <asp:TemplateField FooterText="objcohdownload" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="DownloadLink" runat="server" ToolTip="Delete" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                            OnClick="lnkdownloadAttachment_Click">
                                                                  <i class="fas fa-download text-primary"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                    ItemStyle-HorizontalAlign="Right" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:Button ID="btndownloadall" runat="server" Text="Download All" CssClass="btn btn-primary" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="gvGrievanceAttachment" />
                                                        <asp:PostBackTrigger ControlID="btndownloadall" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblresolutiontype" runat="server" Text="Resolution Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpresolutiontype" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblresolutionremark" runat="server" Text="Resolution Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtresolutionremark" TextMode="MultiLine" Rows="5" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblQualificationRemark" runat="server" Text="Qualification Remark"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtQualificationRemark" TextMode="MultiLine" Rows="5" runat="server"
                                                    CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblresponsetype" runat="server" Text="Response Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpresponsetype" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblresponseremark" runat="server" Text="Response Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtresponseremark" TextMode="MultiLine" Rows="5" runat="server"
                                                    CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnsubmit" runat="server" Text=" Save & Submit" CssClass="btn btn-default" />
                                <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupGrePreviousResponse" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblPopupheader" runat="server" PopupControlID="pnlGrePreviousResponse"
                    DropShadow="false" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGrePreviousResponse" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPopupheader" Text="Previous Lower Level Approver Resolution" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <asp:Label ID="lblPopupReferanceNo" runat="server" Text="Referance Number" CssClass="form-label"></asp:Label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                <asp:Label ID="txtPopupReferanceNo" runat="server" Text="0" Font-Bold="true" CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                        <div class="table-responsive" style="max-height: 350px">
                            <asp:GridView ID="GvPreviousResponse" runat="server" AutoGenerateColumns="False"
                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="levelname" HeaderText="Approver Level" FooterText="colhlevelname">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="name" HeaderText="Approver Name" FooterText="colhApproverName">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="priority" HeaderText="Priority" FooterText="colhPriority">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="qualifyremark" HeaderText="Qualification Remark" ItemStyle-Width="20%"
                                        FooterText="colhqualifyremark"></asp:BoundField>
                                    <asp:BoundField DataField="responseremark" HeaderText="Response Remark" ItemStyle-Width="20%"
                                        FooterText="colhresponseremark"></asp:BoundField>
                                    <asp:BoundField DataField="EmpResponseRemark" HeaderText="Employee Response" ItemStyle-Width="20%"
                                        FooterText="colhempresponseremark"></asp:BoundField>
                                    <asp:BoundField DataField="EmployeeStatus" HeaderText="Employee Status" FooterText="colhempstatus">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EmployeeIds" HeaderText="Committee Members" ItemStyle-Width="25%"
                                        FooterText="colhcommittee"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnCloseGrePreviousResponse" runat="server" CssClass="btn btn-primary"
                            Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
                <uc2:Cnf_YesNo ID="cnfSubmit" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
