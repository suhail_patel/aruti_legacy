﻿<%@ Page Title="Resolution Step List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_ResolutionStepList.aspx.vb" Inherits="Grievance_wPg_ResolutionStepList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Resolution Step"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrievanceDateFrom" runat="server" Text="Grievnce Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtGrievanceDateFrom" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrievanceDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtGrievanceDateTo" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrievanceType" runat="server" Text="Grievance Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboGrievanceType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRefNo" runat="server" Text="Ref No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtRefNo" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponseType" runat="server" Text="ResponseType" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboRepsonseType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="max-height: 250px">
                                    <asp:GridView ID="dgResolutionStepList" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="resolutionsteptranunkid,responsetypeunkid,issubmitted,mapuserunkid">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                        CommandName="Change" CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkEdit_Click">
                                                                <i class="fas fa-pencil-alt text-primary"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Remove"
                                                        CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkdelete_Click">
                                                  <i class="fas fa-trash text-danger"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPreview" runat="server" CommandName="Preview" ToolTip="View Detail"
                                                        Font-Underline="false" CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkPreview_Click"> 
                                                                    <i class="fas fa-eye"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="grievancetype" HeaderText="Grievance Type" ReadOnly="True"
                                                FooterText="colhGrievanceType" />
                                            <asp:BoundField DataField="RaisedByEmp" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee"
                                                Visible="false" />
                                            <asp:BoundField DataField="grievancerefno" HeaderText="Ref No" ReadOnly="True" FooterText="colhRefNo"
                                                Visible="false" />
                                            <asp:BoundField DataField="grievancedate" HeaderText="Meeting Date" ReadOnly="True"
                                                FooterText="colhMeetingDate" />
                                            <asp:BoundField DataField="responseremark" HeaderText="Resolution Remark" ReadOnly="True"
                                                FooterText="colhResolutionRemark" />
                                            <asp:BoundField DataField="responsetype" HeaderText="Resolution Type" ReadOnly="True"
                                                FooterText="colhResolutiontype" />
                                            <asp:BoundField DataField="members" HeaderText="Committee Member" ReadOnly="True"
                                                FooterText="colhmembers" ItemStyle-Width="20%" />
                                            <asp:BoundField DataField="approvername" HeaderText="Approver Name" ReadOnly="True"
                                                FooterText="colhapprover" />
                                            <asp:BoundField DataField="againstEmployee" HeaderText="Against Employee" ReadOnly="True"
                                                FooterText="colhAgainstEmployee" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc10:DeleteReason ID="popupDelete" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
