﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="GrievanceInitiatorResponseList.aspx.vb"
    Inherits="Grievance_GrievanceInitiatorResponseList" Title="Initiator Response List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Initialtor Response List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAgainstEmp" runat="server" Text="Against Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpAgainstEmp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRefno" runat="server" Text="Ref No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpRefno" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="max-height: 350px">
                                    <asp:GridView ID="GvIntiatorResponse" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="initiatortranunkid,resolutionsteptranunkid,responsetypeunkid,issubmitted,priority">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                FooterText="btnEdit">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="View/ Edit Response" CommandArgument="<%# Container.DataItemIndex %>"
                                                        OnClick="lnkView_Click"> 
                                        <i class="fas fa-comment-dots text-info"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="grievancetype" HeaderText="Grievance Type" ReadOnly="True"
                                                FooterText="colhGrievanceType" />
                                            <asp:BoundField DataField="againstEmployee" HeaderText="Against Employee" ReadOnly="True"
                                                FooterText="colhEmployee" Visible="false" />
                                            <asp:BoundField DataField="grievancerefno" HeaderText="Ref No" ReadOnly="True" FooterText="colhRefNo"
                                                Visible="false" />
                                            <asp:BoundField DataField="grievancedate" HeaderText="Meeting Date" ReadOnly="True"
                                                FooterText="colhMeetingDate" />
                                            <asp:BoundField DataField="responseremark" HeaderText="Resolution Remark" ReadOnly="True"
                                                FooterText="colhResolutionRemark" />
                                            <asp:BoundField DataField="responsetype" HeaderText="Resolution Type" ReadOnly="True"
                                                FooterText="colhResolutiontype" />
                                            <asp:BoundField DataField="remark" HeaderText="Your Response" ReadOnly="True" FooterText="colhEmployeeRemark" />
                                            <asp:BoundField DataField="levelname" HeaderText="Approverlevel" ReadOnly="True"
                                                FooterText="colhApproverlevel" />
                                            <asp:BoundField DataField="approvername" HeaderText="Approver Name" ReadOnly="True"
                                                FooterText="colhApprovername" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc10:DeleteReason ID="popupDelete" runat="server" Title="Are you sure you want to delete this Resolution Step?" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
