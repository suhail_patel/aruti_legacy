﻿Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO
Imports ArutiReports

Partial Class Grievance_EmployeeGrievance
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Dim objEmployee As New clsEmployee_Master
    Dim objGrievance_Master As New clsGrievance_Master
    Private Shared ReadOnly mstrModuleName As String = "frmGrievanceEmployeeList"
    Private ReadOnly mstrModuleName1 As String = "frmGrievanceEmployeeAddEdit"
    Private mintgrievancemasterunkid As Integer = 0
    Private mblnpopupEmployeeGrievance As Boolean = False
    Private mdtGrievanceAttachment As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents

    'Gajanan [5-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance NMB Report
    'Private objGrievanceDetailReport As New clsGrievanceDetailReport
    Dim objGrievanceDetailReport As clsGrievanceReport
    'Gajanan [5-July-2019] -- End


    Dim mintattachempid As Integer = 0
#End Region

#Region "Private Function"

    Private Sub SetProcessVisiblity()
        Try
            If CInt(Session("GrievanceRefNoType")) = 1 Then
                txtpopupRefno.Enabled = False
            Else
                txtpopupRefno.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Form Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CInt(Session("Employeeunkid")) > 0 Then
                mintattachempid = CInt(Session("Employeeunkid"))
            ElseIf CInt(Session("UserId")) > 0 Then
                mintattachempid = 0
            End If

            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            'Private objGrievanceDetailReport As New clsGrievanceDetailReport
            objGrievanceDetailReport = New clsGrievanceReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            'Gajanan [5-July-2019] -- End

            If (Page.IsPostBack = False) Then
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                If IsPostBack = False Then
                    Call SetControlCaptions()
                    SetMessages()
                    'Call Language._Object.SaveValue()
                    Call SetLanguage()
                End If
                'Sohail (22 Nov 2018) -- End


                FillListCombo()
                FillGrievanceList(True)
                mdtGrievanceAttachment = objDocument.GetQulificationAttachment(mintattachempid, enScanAttactRefId.GRIEVANCES, mintgrievancemasterunkid, CStr(Session("Document_Path")))

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                mdtGrievanceAttachment.Rows.Clear()
                'Gajanan [27-June-2019] -- End

                fillAttachment()
            Else
                mblnpopupEmployeeGrievance = ViewState("mblnpopupEmployeeGrievance")
                mintgrievancemasterunkid = ViewState("mintgrievancemasterunkid")
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'mdtGrievanceAttachment = objDocument.GetQulificationAttachment(mintattachempid, enScanAttactRefId.GRIEVANCES, mintgrievancemasterunkid, CStr(Session("Document_Path")))
                mdtGrievanceAttachment = ViewState("mdtGrievanceAttachment")
                'S.SANDEEP |16-MAY-2019| -- END
                fillAttachment()
            End If




            If mblnpopupEmployeeGrievance Then
                popupEmployeeGrievance.Show()
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mblnpopupEmployeeGrievance") = mblnpopupEmployeeGrievance
            ViewState("mintgrievancemasterunkid") = mintgrievancemasterunkid
            ViewState("mdtGrievanceAttachment") = mdtGrievanceAttachment
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "For List"

#Region "Private Method"
    Private Sub FillGrievanceList(ByVal isblank As Boolean)
        Try
            Dim objCMaster As New clsCommon_Master
            Dim dsList As New DataSet
            Dim strfilter As String = ""

            If DtListGrDatefrom.IsNull = False Then
                strfilter += "and grievancedate >= '" + eZeeDate.convertDate(DtListGrDatefrom.GetDate.Date) + "' "
            End If

            If DtListGrDateto.IsNull = False Then
                strfilter += "and grievancedate <=  '" + eZeeDate.convertDate(DtListGrDateto.GetDate.Date) + "' "
            End If

            If txtListGrrefno.Text <> "" Then
                strfilter += "and grievancerefno = '" + txtListGrrefno.Text + "' "
            End If

            If isblank Then
                strfilter = "and 1 = 2 "
            End If

            If strfilter.Trim.Length > 0 Then strfilter = strfilter.Substring(3)


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            'dsList = objGrievance_Master.GetList("List", CInt(Session("Employeeunkid")), txtListGrrefno.Text, True, False, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), strfilter)
            dsList = objGrievance_Master.GetList("List", clsGrievance_Master.EmployeeType.RaisedEmployee, CInt(Session("Employeeunkid")), txtListGrrefno.Text, True, False, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), strfilter)
            'Gajanan [10-June-2019] -- End

            If dsList.Tables(0).Rows.Count <= 0 Then
                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'dsList = objGrievance_Master.GetList("List", CInt(Session("Employeeunkid")), txtListGrrefno.Text, True, True, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), "1=2")
                dsList = objGrievance_Master.GetList("List", clsGrievance_Master.EmployeeType.RaisedEmployee, CInt(Session("Employeeunkid")), txtListGrrefno.Text, True, True, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), "1=2")
                'Gajanan [10-June-2019] -- End
                isblank = True
            End If

            gvGrievanceList.DataSource = dsList
            gvGrievanceList.DataBind()
            If isblank = True Then
                gvGrievanceList.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillListCombo()
        Try
            Dim objCMaster As New clsCommon_Master
            Dim dsList As DataSet
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "List")

            drpListGrType.DataSource = dsList
            drpListGrType.DataTextField = "name"
            drpListGrType.DataValueField = "masterunkid"
            drpListGrType.DataBind()
            drpListGrType.SelectedValue = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If mintgrievancemasterunkid > 0 Then
                objGrievance_Master._Grievancemasterunkid = mintgrievancemasterunkid
            End If
            drppopupEmployee.SelectedValue = objGrievance_Master._Againstemployeeunkid
            drppopupGreType.SelectedValue = objGrievance_Master._Grievancetypeid
            popupGrievancedate.SetDate = objGrievance_Master._Grievancedate.Date
            txtpopupRefno.Text = objGrievance_Master._Grievancerefno
            txtpopupGreDesc.Text = objGrievance_Master._Grievance_Description
            txtpopupRelifWanted.Text = objGrievance_Master._Grievance_Reliefwanted

            drppopupEmployee.Enabled = False

            popupGrievancedate.Enabled = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Public Sub SetDateFormat()
        Try
            Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
            NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
            NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
            System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetFilter()
        Try
            drpListGrType.SelectedValue = 0
            txtListGrrefno.Text = ""
            DtListGrDatefrom.SetDate = Nothing
            DtListGrDateto.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region

#Region "Button Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListNew.Click
        Try
            'ClearData()
            mblnpopupEmployeeGrievance = True
            popupEmployeeGrievance.Show()
            FillAddEditCombo()
            mintgrievancemasterunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListSearch.Click
        Try
            FillGrievanceList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnListReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListReset.Click
        Try
            ResetFilter()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupConfirmationGrievanceReason_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmationGrievanceReason.buttonYes_Click
        Try


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            popupDeleteGrievanceReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Grievance Void Reason")
            'Gajanan [10-June-2019] -- End

            popupDeleteGrievanceReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDeleteGrievanceReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteGrievanceReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            objGrievance_Master._Voidreason = popupDeleteGrievanceReason.Reason

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objGrievance_Master._Voiduserunkid = CInt(Session("UserId"))
            Else
                objGrievance_Master._Voiduserunkid = 0
            End If
            objGrievance_Master._Isvoid = True

            SetAtValue()


            Dim strScreenList As List(Of String) = New List(Of String)()
            strScreenList.Add(mstrModuleName)
            strScreenList.Add(mstrModuleName1)

            blnFlag = objGrievance_Master.Delete(mintgrievancemasterunkid, CStr(Session("Employeeunkid")), CStr(Session("Document_Path")), mstrModuleName)
            If blnFlag = False And objGrievance_Master._Message <> "" Then
                DisplayMessage.DisplayMessage(objGrievance_Master._Message, Me)
            Else
                ClearData()
                FillGrievanceList(False)
                mintgrievancemasterunkid = 0
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Gridview Event"
    Protected Sub gvGrievanceList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGrievanceList.RowDataBound
        Try
            SetDateFormat()
            If e.Row.RowType = DataControlRowType.DataRow Then

                If CInt(gvGrievanceList.DataKeys(e.Row.RowIndex)("grievancemasterunkid").ToString) > 0 Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvGrievanceList, "colhgriedate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvGrievanceList, "colhgriedate", False, True)).Text).ToShortDateString()

                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                    If gvGrievanceList.DataKeys(e.Row.RowIndex)("issubmitted").ToString Then
                        lnkedit.Visible = False
                        lnkdelete.Visible = objGrievance_Master.GrievanceEligibleForDelete(CInt(gvGrievanceList.DataKeys(e.Row.RowIndex)("grievancemasterunkid").ToString))
                    Else
                        lnkedit.Visible = True
                        lnkdelete.Visible = True
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            mintgrievancemasterunkid = 0
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            FillAddEditCombo()
            mintgrievancemasterunkid = Convert.ToInt32(lnk.CommandArgument.ToString())

            mdtGrievanceAttachment = objDocument.GetQulificationAttachment(mintattachempid, enScanAttactRefId.GRIEVANCES, mintgrievancemasterunkid, CStr(Session("Document_Path")))
            fillAttachment()
            GetValue()

            mblnpopupEmployeeGrievance = True
            popupEmployeeGrievance.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkviewreport_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintgrievancemasterunkid = Convert.ToInt32(lnk.CommandArgument.ToString())
            objGrievance_Master._Grievancemasterunkid = mintgrievancemasterunkid


            objGrievanceDetailReport.SetDefaultValue()



            objGrievanceDetailReport._UserAccessFilter = ""
            objGrievanceDetailReport._AdvanceFilter = ""
            objGrievanceDetailReport._IncludeAccessFilterQry = False

            objGrievanceDetailReport._Refno = objGrievance_Master._Grievancerefno

            objGrievanceDetailReport._Approvalsetting = Convert.ToInt32(Session("GrievanceApprovalSetting").ToString())



            objGrievanceDetailReport.setDefaultOrderBy(0)

            objGrievanceDetailReport._CompanyUnkid = Session("CompanyUnkId")
            objGrievanceDetailReport._UserUnkid = Session("UserId")

            objGrievanceDetailReport.generateReportNew(Session("Database_Name"), _
                                          Session("UserId"), _
                                          Session("Fin_year"), _
                                          Session("CompanyUnkId"), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                          Session("UserAccessModeSetting"), True, _
                                          Session("ExportReportPath"), _
                                          Session("OpenAfterExport"), 0, _
                                          enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objGrievanceDetailReport._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintgrievancemasterunkid = Convert.ToInt32(lnk.CommandArgument.ToString())


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            popupConfirmationGrievanceReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Confirmation")
            popupConfirmationGrievanceReason.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Are You Sure You Want To Delete This Grievance?")
            'Gajanan [10-June-2019] -- End  
            popupConfirmationGrievanceReason.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region "For AddEdit"

#Region "Private Function"

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtGrievanceAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtGrievanceAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(Session("Employeeunkid"))
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Discipline_Module
                dRow("scanattachrefid") = enScanAttactRefId.GRIEVANCES
                dRow("transactionunkid") = mintgrievancemasterunkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = Date.Today()
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName1
                dRow("userunkid") = CInt(Session("userid"))
                dRow("filesize_kb") = f.Length / 1024
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END
            Else

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Selected information is already present for particular employee."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Selected information is already present for particular employee."), Me)
                'Gajanan [10-June-2019] -- End
                Exit Sub
            End If
            mdtGrievanceAttachment.Rows.Add(dRow)
            Call fillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub fillAttachment()
        Dim dtView As DataView
        Try
            If mdtGrievanceAttachment Is Nothing Then Exit Sub

            dtView = New DataView(mdtGrievanceAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            gvGrievanceAttachment.AutoGenerateColumns = False
            gvGrievanceAttachment.DataSource = dtView
            gvGrievanceAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAddEditCombo()
        Try
            Dim objCMaster As New clsCommon_Master
            Dim dsList As DataSet
            Dim strfilter As String

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

            Else
                strfilter = "hremployee_master.employeeunkid <> " + CStr(Session("Employeeunkid")) + ""
            End If


            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "List")

            drppopupGreType.DataSource = dsList
            drppopupGreType.DataTextField = "name"
            drppopupGreType.DataValueField = "masterunkid"
            drppopupGreType.DataBind()
            drppopupGreType.SelectedValue = 0

            dsList = Nothing

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    Session("UserAccessModeSetting").ToString(), True, _
                                                    False, "List", False, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strfilter, , False, False)

            Dim dr As DataRow = dsList.Tables(0).NewRow()
            dr("employeeunkid") = "0"
            dr("employeename") = "Select"
            dsList.Tables(0).Rows.InsertAt(dr, 0)

            drppopupEmployee.DataSource = dsList
            drppopupEmployee.DataTextField = "employeename"
            drppopupEmployee.DataValueField = "employeeunkid"
            drppopupEmployee.DataBind()

            dsList = Nothing

            Dim objCommon As New clsCommon_Master
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With

            If CInt(Session("GrievanceRefNoType")) = 1 Then
                txtpopupRefno.Enabled = False
            Else
                txtpopupRefno.Enabled = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            If popupGrievancedate.GetDate > Date.Today Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 27, "Grievance Date Should Not Greater Than Today's Date."), Me)
                Return False
            End If
            'Gajanan [24-OCT-2019] -- End

            If drppopupEmployee.SelectedValue = 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage("Sorry,Employee is mandatory information.Please select employee to continue.", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Sorry,Employee is mandatory information.Please select employee to continue."), Me)
                'Gajanan [10-June-2019] -- End
                'Sohail (23 Mar 2019) -- End
                Return False
            ElseIf drppopupGreType.SelectedValue = 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage("Sorry,grievance type is mandatory information.Please select grievance type to continue.", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 18, "Sorry,grievance type is mandatory information.Please select grievance type to continue."), Me)
                'Gajanan [10-June-2019] -- End

                'Sohail (23 Mar 2019) -- End
                Return False

            ElseIf popupGrievancedate.IsNull Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage("Sorry,date is mandatory information.Please select Date to continue.", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Sorry,date is mandatory information.Please select Date to continue."), Me)
                'Gajanan [10-June-2019] -- End
                'Sohail (23 Mar 2019) -- End
                Return False

            ElseIf CInt(Session("GrievanceRefNoType")) = 0 Then
                If txtpopupRefno.Text = "" Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)

                    'Gajanan [10-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                    'DisplayMessage.DisplayMessage("Please Set Reference no.", Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Please Set Reference no."), Me)
                    'Gajanan [10-June-2019] -- End
                    'Sohail (23 Mar 2019) -- End
                    Return False
                End If
            End If
            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Function CheckValidaApprover() As Boolean
        Try
            Dim dsApprovers As New DataSet
            Dim objAppr As New clsgrievanceapprover_master


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'Dim eApprType As enGrievanceApproval

            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If
            'Gajanan [24-OCT-2019] -- End


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        


            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(Session("Employeeunkid")).ToString())

            'Gajanan [10-June-2019] -- End



            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(drppopupEmployee.SelectedValue))
            dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting").ToString(), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(drppopupEmployee.SelectedValue))
            If objAppr._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objAppr._Message, Me)
                Return False
            End If

            'Gajanan [24-OCT-2019] -- End


            If dsApprovers.Tables(0).Rows.Count <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage("Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance.", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                'Gajanan [10-June-2019] -- End

                'Sohail (23 Mar 2019) -- End
                Return False
            End If
            Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1

            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
                dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                If dt.Length > 0 Then
                    If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Return False
                    End If
                    'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                Else
                    If IsDBNull(intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Return False
                    End If
                    'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                End If

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "' ")

                If dt.Length > 0 Then
                    If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Return False
                    End If
                    'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                Else
                    If IsDBNull(intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End

                        'Sohail (23 Mar 2019) -- End
                        Return False
                    End If
                    'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                End If
            End If
            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Public Sub SetValue()
        Try

            If mintgrievancemasterunkid > 0 Then
                objGrievance_Master._Grievancemasterunkid = mintgrievancemasterunkid
            End If

            If objGrievance_Master._ApprovalSettingid <= 0 Then
                objGrievance_Master._ApprovalSettingid = Convert.ToInt32(Session("GrievanceApprovalSetting").ToString())
            End If

            objGrievance_Master._Grievancetypeid = drppopupGreType.SelectedValue
            objGrievance_Master._Grievancedate = popupGrievancedate.GetDate
            objGrievance_Master._Grievancerefno = txtpopupRefno.Text
            objGrievance_Master._Grievance_Description = txtpopupGreDesc.Text
            objGrievance_Master._Grievance_Reliefwanted = txtpopupRelifWanted.Text
            objGrievance_Master._Appealremark = txtpopupApplealRemark.Text
            objGrievance_Master._Againstemployeeunkid = drppopupEmployee.SelectedValue
            objGrievance_Master._Fromemployeeunkid = CInt(Session("Employeeunkid"))
            SetAtValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try


    End Sub

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objGrievance_Master._AuditUserid = CInt(Session("UserId"))
            Else
                objGrievance_Master._loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objGrievance_Master._ClientIp = CStr(Session("IP_ADD"))
            objGrievance_Master._HostName = CStr(Session("HOST_NAME"))
            objGrievance_Master._FormName = mstrModuleName1
            objGrievance_Master._IsFromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ClearData()
        Try
            drppopupEmployee.Enabled = True
            popupGrievancedate.Enabled = True
            drppopupGreType.Enabled = True

            drppopupEmployee.SelectedValue = 0
            drppopupGreType.SelectedValue = 0

            popupGrievancedate.SetDate = Nothing
            txtpopupRefno.Text = ""
            txtpopupGreDesc.Text = ""
            txtpopupRelifWanted.Text = ""
            mintattachempid = 0
            mdtGrievanceAttachment.Clear()
            gvGrievanceAttachment.DataSource = mdtGrievanceAttachment
            gvGrievanceAttachment.DataBind()
            mblnpopupEmployeeGrievance = False
            popupEmployeeGrievance.Hide()
            FillGrievanceList(False)

            mintgrievancemasterunkid = 0

        Catch ex As Exception

        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SaveGrievanceData(ByVal blnIsSubmmitted As Boolean, ByVal dt As DataRow())
        Dim blnFlag As Boolean = False
        Dim objAppr As New clsgrievanceapprover_master
        Try
            SetValue()

            If blnIsSubmmitted Then
                objGrievance_Master._Issubmitted = True
            End If

            objGrievance_Master._Issubmitted = blnIsSubmmitted

            Dim strScreenList As List(Of String) = New List(Of String)()
            strScreenList.Add(mstrModuleName)
            strScreenList.Add(mstrModuleName1)

            If mintgrievancemasterunkid > 0 Then
                blnFlag = objGrievance_Master.Update(ConfigParameter._Object._CurrentDateAndTime, mdtGrievanceAttachment, mstrModuleName1, CInt(Session("CompanyUnkId")), CStr(Session("Document_Path")), strScreenList, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Database_Name").ToString(), CInt(Session("Fin_year")), CStr(Session("ArutiSelfServiceURL")))
            Else
                blnFlag = objGrievance_Master.Insert(CInt(Session("GrievanceRefNoType")), CStr(Session("GrievanceRefPrefix")), CInt(Session("CompanyUnkId")), mdtGrievanceAttachment, CStr(Session("Document_Path")), strScreenList, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Database_Name").ToString(), mstrModuleName1, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString))
            End If



            If blnIsSubmmitted And blnFlag Then
                'SEND NOTIFICATION
                Dim objEmp As New clsEmployee_Master
                'Hemant (29 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                Dim strAgainstEmpName = String.Empty
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(objGrievance_Master._Againstemployeeunkid)
                strAgainstEmpName = StrConv(objEmp._Firstname & " " & objEmp._Surname, VbStrConv.ProperCase)
                'Hemant (29 May 2019) -- End
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                'Hemant (29 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'objAppr.SendNotificationToApprover(dt, Session("ArutiSelfServiceURL"), mstrModuleName1, enLogin_Mode.EMP_SELF_SERVICE, IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email), drppopupEmployee.SelectedItem.Text, objGrievance_Master._Grievancerefno, objGrievance_Master._Grievancemasterunkid, objEmp._Firstname & " " & objEmp._Surname, CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        


                'objAppr.SendNotificationToApprover(dt, Session("ArutiSelfServiceURL"), mstrModuleName1, enLogin_Mode.EMP_SELF_SERVICE, IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email), strAgainstEmpName, objGrievance_Master._Grievancerefno, objGrievance_Master._Grievancemasterunkid, objEmp._Firstname & " " & objEmp._Surname, CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                objAppr.SendNotificationToApprover(dt, Session("ArutiSelfServiceURL"), mstrModuleName1, enLogin_Mode.EMP_SELF_SERVICE, IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email), strAgainstEmpName, objGrievance_Master._Grievancerefno, objGrievance_Master._Grievancemasterunkid, StrConv(objEmp._Firstname & " " & objEmp._Surname, VbStrConv.ProperCase), CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), drppopupGreType.SelectedItem.Text)
                'Gajanan [10-June-2019] -- End


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                objAppr.SendNotificationToEmployeeOnRaisedGrievance(dt, mstrModuleName1, objEmp._Email, enLogin_Mode.EMP_SELF_SERVICE, IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email), strAgainstEmpName, objGrievance_Master._Grievancerefno, objGrievance_Master._Grievancemasterunkid, StrConv(objEmp._Firstname & " " & objEmp._Surname, VbStrConv.ProperCase), CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), drppopupGreType.SelectedItem.Text)
                'Gajanan [10-June-2019] -- End


                'Hemant (29 May 2019) -- End
                objEmp = Nothing


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "Save Successfully."), Me)


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes

                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 19, "Save Successfully."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 25, "Submitted Successfully."), Me)
                'Gajanan [27-June-2019] -- End

                'Gajanan [10-June-2019] -- End

                ClearData()

            ElseIf blnIsSubmmitted = False And blnFlag Then

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "Save Successfully."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 19, "Save Successfully."), Me)
                'Gajanan [10-June-2019] -- End

                ClearData()
            ElseIf blnFlag = False Then
                'S.SANDEEP |30-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Grievance UAT]
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(objGrievance_Master._Message, Me)
                'S.SANDEEP |30-MAY-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


#End Region

#Region "Button Event"
    Protected Sub btnSaveGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveGreApprover.Click, btnSaveSubmitGreApprover.Click
        'Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            If CheckValidaApprover() = False Then
                Exit Sub
            End If

            Dim dsApprovers As New DataSet
            'Dim objAppr As New clsgrievanceapprover_master
            'Dim eApprType As enGrievanceApproval
            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If

            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, True, Nothing, CInt(Session("Employeeunkid")).ToString())
            'If dsApprovers.Tables(0).Rows.Count <= 0 Then
            '    DisplayMessage.DisplayMessage("Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance.", Me)
            '    Exit Sub
            'End If
            'Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1

            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
            '    If dt.Length > 0 Then
            '        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
            '    Else
            '        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
            '    End If
            '    dt = Nothing
            '    If intPriority <> -1 Then
            '        dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "' AND activestatus = 1")
            '        If dt.Length <= 0 Then
            '            DisplayMessage.DisplayMessage("Sorry, Approver is inactive. Please contact system admin in order to post grievance.", Me)
            '            Exit Sub
            '        End If
            '    End If
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then

            'End If

            'SetValue()

            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRow As DataRow In mdtGrievanceAttachment.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    'Pinkal (20-Nov-2018) -- End
                    If File.Exists(CStr(dRow("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRow("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath
                        dRow.AcceptChanges()
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, "File does not exist on localpath"), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)


                            'Gajanan [10-June-2019] -- Start      
                            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                            'DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, "File does not exist on localpath"), Me)
                            'Gajanan [10-June-2019] -- End

                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, "File does not exist on localpath"), Me)
                        'Gajanan [10-June-2019] -- End

                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If

            Next

            If CType(sender, Button).ID.ToUpper() = btnSaveSubmitGreApprover.ID.ToUpper() Then

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        


                PopupSubmitGrievance.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 20, "Confirmation")
                'Gajanan [10-June-2019] -- End

                'Gajanan [24-June-2019] -- Start      
                'PopupSubmitGrievance.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 3, "You are about to submit the selected grievance to approver for resolution,due to this you will not be able to edit or delete the grievance. Are you sure you want to continue ?")
                PopupSubmitGrievance.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 24, "You are about to submit the selected grievance to approver for resolution,due to this you will not be able to edit or delete the grievance. Are you sure you want to continue ?")
                'Gajanan [24-June-2019] -- End
                PopupSubmitGrievance.Show()
            Else
                Call SaveGrievanceData(False, Nothing)
            End If

            'Dim strScreenList As List(Of String) = New List(Of String)()
            'strScreenList.Add(mstrModuleName)
            'strScreenList.Add(mstrModuleName1)

            'If mintgrievancemasterunkid > 0 Then
            '    blnFlag = objGrievance_Master.Update(ConfigParameter._Object._CurrentDateAndTime, mdtGrievanceAttachment, mstrModuleName1, CInt(Session("CompanyUnkId")), CStr(Session("Document_Path")), strScreenList, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Database_Name").ToString(), CInt(Session("Fin_year")), CStr(Session("ArutiSelfServiceURL")))
            'Else
            '    blnFlag = objGrievance_Master.Insert(CInt(Session("GrievanceRefNoType")), CStr(Session("GrievanceRefPrefix")), CInt(Session("CompanyUnkId")), mdtGrievanceAttachment, CStr(Session("Document_Path")), strScreenList, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Database_Name").ToString(), mstrModuleName1)
            'End If

            'If blnFlag Then
            '    Dim objEmp As New clsEmployee_Master
            '    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            '    ionToApprover(dt, Session("ArutiSelfServiceURL"), mstrModuleName1, enLogin_Mode.EMP_SELF_SERVICE, IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email), drppopupEmployee.SelectedItem.Text, objGrievance_Master._Grievancerefno, objGrievance_Master._Grievancemasterunkid, objEmp._Firstname & " " & objEmp._Surname, CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
            '    objEmp = Nothing
            '    DisplayMessage.DisplayMessage("Save Successfully", Me)
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnCloseGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseGreApprover.Click
        Try
            mintgrievancemasterunkid = 0
            mblnpopupEmployeeGrievance = False
            popupEmployeeGrievance.Hide()
            ClearData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                AddDocumentAttachment(f, f.FullName)
                Call fillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAttachment_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAttachment_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtGrievanceAttachment.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtGrievanceAttachment.AcceptChanges()
                Call fillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub PopupSubmitGrievance_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PopupSubmitGrievance.buttonYes_Click
        Try
            Dim dsApprovers As New DataSet
            Dim objAppr As New clsgrievanceapprover_master

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            'Dim eApprType As enGrievanceApproval
            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If
            'Gajanan [24-OCT-2019] -- End

            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(Session("Employeeunkid")).ToString())
            dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting").ToString(), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(drppopupEmployee.SelectedValue).ToString())
            'Gajanan [10-June-2019] -- End

            If dsApprovers.Tables(0).Rows.Count <= 0 Then

                'Gajanan [24-June-2019] -- Start      
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 3, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 22, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                'Gajanan [24-June-2019] -- End

                Exit Sub
            End If
            Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1

            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping OrElse _
               Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ReportingTo Then
                dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                If dt.Length > 0 Then

                    If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If

                    'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                Else
                    If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                    intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                End If
                dt = Nothing
                If intPriority <> -1 Then
                    dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "'")
                    If dt.Length <= 0 Then

                        'Gajanan [24-June-2019] -- Start      
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 23, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                        'Gajanan [24-June-2019] -- End
                        Exit Sub
                    End If
                End If
            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "' ")

                If dt.Length > 0 Then

                    If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End

                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If

                    'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                Else
                    If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "")) = False Then
                        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)

                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        'DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne."), Me)
                        'Gajanan [10-June-2019] -- End


                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                    intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                End If
                dt = Nothing
                If intPriority <> -1 Then
                    dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "'")
                    If dt.Length <= 0 Then

                        'Gajanan [24-June-2019] -- Start      
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 23, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                        'Gajanan [24-June-2019] -- End
                        Exit Sub
                    End If
                End If
            End If

            Call SaveGrievanceData(True, dt)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If gvGrievanceAttachment.Rows.Count <= 0 Then

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage("No Files to download.", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 17, "No Files to download."), Me)
                'Gajanan [10-June-2019] -- End

                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & drppopupEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtGrievanceAttachment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END


    'Hemant (29 May 2019) -- Start
    'ISSUE/ENHANCEMENT : UAT Changes
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (29 May 2019) -- End


#End Region

#Region "Gridview Event"
    Protected Sub gvGrievanceAttachment_RowCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGrievanceAttachment.RowCommand
        Try
            Dim Row As GridViewRow = CType(((CType(e.CommandSource, Control)).NamingContainer), GridViewRow)

            If Row.RowIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(gvGrievanceAttachment.DataKeys(Row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                    xrow = mdtGrievanceAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(Row.RowIndex)("scanattachtranunkid").ToString) & "")
                Else
                    xrow = mdtGrievanceAttachment.Select("GUID = '" & gvGrievanceAttachment.DataKeys(Row.RowIndex)("GUID").ToString.Trim & "'")
                End If
                If e.CommandName.ToUpper = "REMOVEATTACHMENT" Then
                    If xrow.Length > 0 Then


                        'Gajanan [10-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                        popupAttachment_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 20, "Confirmation")

                        'popupAttachment_YesNo.Message = "Are you sure you want to delete this attachment?"

                        'Gajanan [24-June-2019] -- Start      
                        'popupAttachment_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "Are you sure you want to delete this attachment?")
                        popupAttachment_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 21, "Are you sure you want to delete this attachment?")
                        'Gajanan [24-June-2019] -- End
                        'Gajanan [10-June-2019] -- End

                        Me.ViewState("DeleteAttRowIndex") = mdtGrievanceAttachment.Rows.IndexOf(xrow(0))
                        popupAttachment_YesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("localpath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then

                                'Gajanan [10-June-2019] -- Start      
                                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                                'DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 18, "File does not Exist..."), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 26, "File does not Exist..."), Me)
                                'Gajanan [10-June-2019] -- End

                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCaption.ID, Me.lblCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblListGrDatefrom.ID, Me.lblListGrDatefrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblListGrType.ID, Me.lblListGrType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblListGrDateto.ID, Me.lblListGrDateto.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblListGrrefno.ID, Me.lblListGrrefno.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnListNew.ID, Me.btnListNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnListSearch.ID, Me.btnListSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnListReset.ID, Me.btnListReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(3).FooterText, gvGrievanceList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(4).FooterText, gvGrievanceList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(5).FooterText, gvGrievanceList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(6).FooterText, gvGrievanceList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(7).FooterText, gvGrievanceList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(8).FooterText, gvGrievanceList.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvGrievanceList.Columns(9).FooterText, gvGrievanceList.Columns(9).HeaderText)

            'Language.setLanguage(mstrModuleName1)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveGreApprover.ID, Me.btnSaveGreApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveSubmitGreApprover.ID, Me.btnSaveSubmitGreApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCloseGreApprover.ID, Me.btnCloseGreApprover.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblPopUpEmployee.ID, Me.lblPopUpEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblpopupdate.ID, Me.lblpopupdate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblpopupGreType.ID, Me.lblpopupGreType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblpopupRefno.ID, Me.lblpopupRefno.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblDocumentType.ID, Me.lblDocumentType.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.TabGre.ID, Me.TabGre.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.TabRelifWanted.ID, Me.TabRelifWanted.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.TabAttachDocument.ID, Me.TabAttachDocument.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.TabApplealRemark.ID, Me.TabApplealRemark.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvGrievanceAttachment.Columns(2).FooterText, gvGrievanceAttachment.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvGrievanceAttachment.Columns(3).FooterText, gvGrievanceAttachment.Columns(3).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblListGrDatefrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblListGrDatefrom.ID, Me.lblListGrDatefrom.Text)
            Me.lblListGrType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblListGrType.ID, Me.lblListGrType.Text)
            Me.lblListGrDateto.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblListGrDateto.ID, Me.lblListGrDateto.Text)
            Me.lblListGrrefno.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblListGrrefno.ID, Me.lblListGrrefno.Text)

            Me.btnListNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnListNew.ID, Me.btnListNew.Text).Replace("&", "")
            Me.btnListSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnListSearch.ID, Me.btnListSearch.Text).Replace("&", "")
            Me.btnListReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnListReset.ID, Me.btnListReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            gvGrievanceList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(3).FooterText, gvGrievanceList.Columns(3).HeaderText)
            gvGrievanceList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(4).FooterText, gvGrievanceList.Columns(4).HeaderText)
            gvGrievanceList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(5).FooterText, gvGrievanceList.Columns(5).HeaderText)
            gvGrievanceList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(6).FooterText, gvGrievanceList.Columns(6).HeaderText)
            gvGrievanceList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(7).FooterText, gvGrievanceList.Columns(7).HeaderText)
            gvGrievanceList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(8).FooterText, gvGrievanceList.Columns(8).HeaderText)
            gvGrievanceList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvGrievanceList.Columns(9).FooterText, gvGrievanceList.Columns(9).HeaderText)



            'Language.setLanguage(mstrModuleName1)

            Me.lblCancelText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Me.lblPopUpEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpEmployee.ID, Me.lblPopUpEmployee.Text)
            Me.lblpopupdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblpopupdate.ID, Me.lblpopupdate.Text)
            Me.lblpopupGreType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblpopupGreType.ID, Me.lblpopupGreType.Text)
            Me.lblpopupRefno.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblpopupRefno.ID, Me.lblpopupRefno.Text)
            Me.TabGre.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.TabGre.ID, Me.TabGre.Text)
            Me.TabRelifWanted.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.TabRelifWanted.ID, Me.TabRelifWanted.Text)
            Me.TabApplealRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.TabApplealRemark.ID, Me.TabApplealRemark.Text)
            Me.TabAttachDocument.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.TabAttachDocument.ID, Me.TabAttachDocument.Text)
            Me.lblDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblDocumentType.ID, Me.lblDocumentType.Text)
            Me.btnDownloadAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnDownloadAll.ID, Me.btnDownloadAll.Text).Replace("&", "")
            Me.btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text).Replace("&", "")
            Me.btnSaveGreApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveGreApprover.ID, Me.btnSaveGreApprover.Text).Replace("&", "")
            Me.btnSaveSubmitGreApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveSubmitGreApprover.ID, Me.btnSaveSubmitGreApprover.Text).Replace("&", "")
            Me.btnCloseGreApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseGreApprover.ID, Me.btnCloseGreApprover.Text).Replace("&", "")

            gvGrievanceAttachment.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvGrievanceAttachment.Columns(2).FooterText, gvGrievanceAttachment.Columns(1).HeaderText)
            gvGrievanceAttachment.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvGrievanceAttachment.Columns(3).FooterText, gvGrievanceAttachment.Columns(2).HeaderText)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Sorry,Employee is mandatory information.Please select employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "Sorry,date is mandatory information.Please select Date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 5, "Please Set Reference no.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 6, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 7, "Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 11, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 17, "No Files to download.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 18, "Sorry,grievance type is mandatory information.Please select grievance type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 19, "Save Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 21, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 22, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 23, "Sorry, Approver is inactive. Please contact system admin in order to post grievance.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 24, "You are about to submit the selected grievance to approver for resolution,due to this you will not be able to edit or delete the grievance. Are you sure you want to continue ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 25, "Submitted Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 26, "File does not Exist...")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 27, "Grievance Date Should Not Greater Than Today's Date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Are You Sure You Want To Delete This Grievance?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Grievance Void Reason")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 20, "Confirmation")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
