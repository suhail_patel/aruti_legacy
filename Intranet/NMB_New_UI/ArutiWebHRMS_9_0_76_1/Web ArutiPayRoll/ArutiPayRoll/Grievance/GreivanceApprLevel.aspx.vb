﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Partial Class Grievance_GreivanceApprLevel
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmGrievanceApproverLevelList"
    Private ReadOnly mstrModuleName1 As String = "frmGrievanceApproverLevelAddEdit"
    Private mintLevelmstid As Integer
    Private blnpopupGreApprover As Boolean = False
    Private objclsGrievanceApproverLevel As New clsGrievanceApproverLevel
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                Call SetMessages()
                'Sohail (22 Nov 2018) -- End

                If CBool(Session("AllowToViewGrievanceApproverLevel")) Then
                    FillList(False)
                Else
                    FillList(True)

                End If
                SetVisibility()

            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If

                If ViewState("blnpopupGreApprover") IsNot Nothing Then
                    blnpopupGreApprover = Convert.ToBoolean(ViewState("blnpopupGreApprover").ToString())
                End If

                If blnpopupGreApprover Then
                    popupGreApproverLevel.Show()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupGreApprover") = blnpopupGreApprover
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsGrievanceApproverList As New DataSet
        Dim strfilter As String = ""

        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If

            dsGrievanceApproverList = objclsGrievanceApproverLevel.GetList("List", True, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), False, strfilter)

            If dsGrievanceApproverList.Tables(0).Rows.Count <= 0 Then
                dsGrievanceApproverList = objclsGrievanceApproverLevel.GetList("List", True, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), True, "1 = 2")
                isblank = True
            End If

            GvApprLevelList.DataSource = dsGrievanceApproverList.Tables("List")
            GvApprLevelList.DataBind()
            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Visible = CBool(Session("AllowToAddGrievanceApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetValue()
        Try
            If mintLevelmstid > 0 Then
                objclsGrievanceApproverLevel._ApprLevelunkid = mintLevelmstid
            Else
                objclsGrievanceApproverLevel._ApprovalSetting = Convert.ToInt32(Session("GrievanceApprovalSetting").ToString())
            End If

            objclsGrievanceApproverLevel._Levelcode = txtlevelcode.Text.Trim
            objclsGrievanceApproverLevel._Levelname = txtlevelname.Text.Trim
            objclsGrievanceApproverLevel._Priority = Convert.ToInt32(txtlevelpriority.Text)
            Call SetAtValue()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsGrievanceApproverLevel._AuditUserid = CInt(Session("UserId"))
            End If
            objclsGrievanceApproverLevel._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objclsGrievanceApproverLevel._ClientIp = CStr(Session("IP_ADD"))
            objclsGrievanceApproverLevel._loginemployeeunkid = -1
            objclsGrievanceApproverLevel._HostName = CStr(Session("HOST_NAME"))
            objclsGrievanceApproverLevel._FormName = mstrModuleName1
            objclsGrievanceApproverLevel._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub GetValue()
        Try

            If mintLevelmstid > 0 Then
                objclsGrievanceApproverLevel._ApprLevelunkid = mintLevelmstid
            End If
            txtlevelcode.Text = objclsGrievanceApproverLevel._Levelcode
            txtlevelname.Text = objclsGrievanceApproverLevel._Levelname
            txtlevelpriority.Text = objclsGrievanceApproverLevel._Priority.ToString()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub cleardata()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            blnpopupGreApprover = True
            popupGreApproverLevel.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnnew_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSaveGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveGreApprover.Click
        Dim blnFlag As Boolean = False
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            End If
            Call SetValue()
            If mintLevelmstid > 0 Then
                blnFlag = objclsGrievanceApproverLevel.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objclsGrievanceApproverLevel.Insert()
            End If

            If blnFlag = False And objclsGrievanceApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsGrievanceApproverLevel._Message, Me)
                blnpopupGreApprover = True
                popupGreApproverLevel.Show()
            Else
                cleardata()
                FillList(False)
                mintLevelmstid = 0
                blnpopupGreApprover = False
                popupGreApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValue()
            blnpopupGreApprover = True
            popupGreApproverLevel.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            'GetValue()

            If objclsGrievanceApproverLevel.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()

            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            popup_YesNo.Title = "Confirmation"
            popup_YesNo.Message = "Are You Sure You Want To Delete This Approver Level ?"
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Are You Sure You Want To Delete This Approver Level ?")
            'Gajanan [10-June-2019] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False

            SetAtValue()
            objclsGrievanceApproverLevel._FormName = mstrModuleName

            blnFlag = objclsGrievanceApproverLevel.Delete(mintLevelmstid)

            If blnFlag = False And objclsGrievanceApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsGrievanceApproverLevel._Message, Me)
            Else
                FillList(False)
                mintLevelmstid = 0
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnCloseGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseGreApprover.Click
        Try
            cleardata()
            blnpopupGreApprover = False
            popupGreApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region


#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("apprlevelunkid").ToString) > 0 Then

                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                        lnkedit.Visible = CBool(Session("AllowToEditGrievanceApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteGrievanceApproverLevel"))

                        'If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("ApprovalSettingid").ToString) = Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) Then
                        '    lnkedit.Visible = True
                        '    lnkdelete.Visible = True
                        'Else
                        '    lnkedit.Visible = False
                        '    lnkdelete.Visible = False
                        'End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(5).FooterText, GvApprLevelList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnnew.ID, Me.btnnew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.popup_YesNo.ID, Me.popup_YesNo.Message)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.popup_YesNo.ID, Me.popup_YesNo.Title)

            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lbllevelname.ID, Me.lbllevelname.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveGreApprover.ID, Me.btnSaveGreApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCloseGreApprover.ID, Me.btnCloseGreApprover.Text)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)
            GvApprLevelList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            GvApprLevelList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            GvApprLevelList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)
            GvApprLevelList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(5).FooterText, GvApprLevelList.Columns(5).HeaderText)
            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnnew.ID, Me.btnnew.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            popup_YesNo.Message = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.popup_YesNo.ID, Me.popup_YesNo.Message)
            popup_YesNo.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.popup_YesNo.ID, Me.popup_YesNo.Title)

            'Language.setLanguage(mstrModuleName1)
            Me.lblCancelText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Me.lbllevelcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Me.lbllevelname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lbllevelname.ID, Me.lbllevelname.Text)
            Me.lbllevelpriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)
            Me.btnSaveGreApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveGreApprover.ID, Me.btnSaveGreApprover.Text).Replace("&", "")
            Me.btnCloseGreApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseGreApprover.ID, Me.btnCloseGreApprover.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Are You Sure You Want To Delete This Approver Level ?")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
