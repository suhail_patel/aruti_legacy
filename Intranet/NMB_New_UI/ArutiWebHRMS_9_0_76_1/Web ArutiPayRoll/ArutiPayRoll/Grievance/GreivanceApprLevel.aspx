﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="GreivanceApprLevel.aspx.vb"
    Inherits="Grievance_GreivanceApprLevel" Title="Grievance Approver Level" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader2" runat="server" Text="Grievance Approver Level List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:GridView ID="GvApprLevelList" DataKeyNames="apprlevelunkid,ApprovalSettingid"
                                                runat="server" AutoGenerateColumns="False" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("apprlevelunkid") %>'>
                                                            <i class="fas fa-pencil-alt text-primary"> </i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("apprlevelunkid") %>'> 
                                                            <i class="fas fa-trash text-danger"> </i>                                             
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="levelcode" HeaderText="Level Code" HeaderStyle-Width="20%"
                                                        FooterText="colhLevelCode"></asp:BoundField>
                                                    <asp:BoundField DataField="levelname" HeaderText="Level Name" HeaderStyle-Width="25%"
                                                        FooterText="colhLevelName"></asp:BoundField>
                                                    <asp:BoundField DataField="priority" HeaderText="Priority Level" HeaderStyle-Width="15%"
                                                        FooterText="colhPriority" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ApprovalSetting" HeaderText="Approval Setting" HeaderStyle-Width="30%"
                                                        FooterText="colhApprovalSetting"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" CssClass="btn btn-primary" Text="New" OnClick="btnnew_Click" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupGreApproverLevel" BackgroundCssClass="modal-backdrop"
                    TargetControlID="txtlevelcode" runat="server" PopupControlID="pnlGreApprover"
                    DropShadow="false" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGreApprover" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Grievance Approver Level Add/ Edit" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelcode" runat="server" Text="Level Code" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelcode" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelname" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelpriority" runat="server" Text="Priority" CssClass="form-label" />
                                <uc2:NumericText ID="txtlevelpriority" runat="server" Type="Point" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveGreApprover" runat="server" CssClass="btn btn-primary" Text="Save" />
                        <asp:Button ID="btnCloseGreApprover" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
                <uc7:Confirmation ID="popup_YesNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
