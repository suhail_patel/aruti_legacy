﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="GrievanceApprover.aspx.vb"
    Inherits="Grievance_GrievanceApprover" Title="Grievance Approver Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                <div class="block-header">
                    <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Approver"></asp:Label>
                    </h2>
                        </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                        <asp:Label ID="lblCaption" runat="server" Text="Grievance Approver List"></asp:Label>
                                </h2>
                                    </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblLevel" runat="server" Text="Approver Level" CssClass="form-label" />
                                        <div class="form-group">
                                                <asp:DropDownList ID="drpLevel" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label" />
                                        <div class="form-group">
                                                <asp:DropDownList ID="drpApprover" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" />
                                        <div class="form-group">
                                                <asp:DropDownList ID="drpStatus" runat="server">
                                                </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                <asp:GridView ID="gvApproverList" DataKeyNames="approvermasterunkid" runat="server"
                                                AutoGenerateColumns="False" Width="99%" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" ToolTip="Edit" OnClick="lnkedit_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>'>
                                                            <i class="fas fa-pencil-alt text-primary"> </i>
                                                
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("approvermasterunkid") %>'
                                                                ToolTip="Delete">
                                                            <i class="fas fa-trash text-danger"> </i>                                             
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Active" OnClick="lnkActive_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>'>
                                                    <i class="fa fa-user-plus text-success" ></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="DeActive" OnClick="lnkDeActive_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>'>
                                                    <i class="fa fa-user-times text-danger" ></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--'S.SANDEEP |30-MAY-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : [Grievance UAT]--%>
                                        <%--<asp:BoundField HeaderText="Approver Name" DataField="Name" />--%>
                                        <%--<asp:BoundField HeaderText="Department" DataField="departmentname" />--%>
                                        <%--<asp:BoundField HeaderText="Job" DataField="jobname" />--%>
                                        <%--<asp:BoundField HeaderText="External Approver" DataField="exappr" />--%>
                                        <asp:BoundField HeaderText="Approver Name" DataField="Name" FooterText="dgcolhName" />
                                        <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhdepartmentname" />
                                        <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhjobname" />
                                        <asp:BoundField HeaderText="External Approver" DataField="exappr" FooterText="dgcolhexappr" />
                                        <%--'S.SANDEEP |30-MAY-2019| -- END--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupGreAddEditApproverEmpMapping" BackgroundCssClass="modal-backdrop"
                        TargetControlID="lblCaption" runat="server" PopupControlID="pnlGreAddEditApproverEmpMapping"
                        CancelControlID="lblPageHeader1" />
                <asp:Panel ID="pnlGreAddEditApproverEmpMapping" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPageHeader1" runat="server" Text="Add/Edit Grievance Approver"></asp:Label>
                        </h2>
                            </div>
                    <div class="body" style="max-height:450px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblApproverInfo" runat="server" Text="Approvers Info"  />
                                                </h2>
                                        </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkAddEditExtApprover" runat="server" AutoPostBack="true" Text="Make External Approver" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproverName" runat="server" Text="Name" CssClass="form-label"/>
                                                        <asp:DropDownList ID="drpAddEditApproverName" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprovalUser" runat="server" Text="User" CssClass="form-label"/>
                                                        <asp:DropDownList ID="drpAddEditUser" runat="server" >
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="PanelApprovalUser" runat="server" CssClass="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprovalevel" runat="server" Text="Level" CssClass="form-label"/>
                                                        <asp:DropDownList ID="drpAddEditApprovalLevel" runat="server" >
                                                        </asp:DropDownList>
                                                        </div>
                                                </asp:Panel>
                                                <div class="row clearfix">
                                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSearchEmployee" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    </div>                                                    
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset">
                                                            <i class="fas fa-sync-alt text-primary"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 175px">
                                                    <asp:GridView ID="gvAddEditEmployee" runat="server" AutoGenerateColumns="false" Width="99%"
                                                                CssClass="table table-hover table-bordered" DataKeyNames="employeeunkid,employeecode"
                                                                AllowPaging="false" >
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAllAddEditEmployee" runat="server" AutoPostBack="true" Text=" " OnCheckedChanged="ChkAllAddEditEmployee_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkSelectAddEditEmployee" runat="server" AutoPostBack="true" Text=" " OnCheckedChanged="ChkSelectAddEditEmployee_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--'S.SANDEEP |30-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : [Grievance UAT]--%>
                                                            <%--<asp:BoundField HeaderText="Employee Code" DataField="employeecode" />--%>
                                                            <%--<asp:BoundField HeaderText="Employee" DataField="name" />--%>
                                                            <asp:BoundField HeaderText="Employee Code" DataField="employeecode" FooterText="dgEcode" />
                                                            <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEname" />
                                                            <%--'S.SANDEEP |30-MAY-2019| -- END--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAddEditAdd" runat="server" Text="Add" CssClass="btn btn-default" />
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="LblGreAddEditLeaveApprover" runat="server" Text="Selected Employee" />
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="table-responsive" style="height: 475px">
                                                <asp:GridView ID="GvApproverEmpMapping" runat="server" AutoGenerateColumns="false"
                                                        Width="99%" CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="employeeunkid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAllSelectedEmp_CheckedChanged" Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged="chkAddEditSelectEmpChkgvSelect_CheckedChanged" Text=" "/>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEmpName" />
                                                            <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhEDept" />
                                                            <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhEJob" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAddeditDelete" runat="server" Text="Delete" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                    <div class="footer">
                        <asp:Button ID="btnpopupsave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnpopupclose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </asp:Panel>
                <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modal-backdrop"
                        TargetControlID="lblApproverInfo" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblApproverName" />
                <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader2" runat="server" Text="Add/ Edit Grievance Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 480px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblApproverInfo1" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="height: 300px; margin: auto; padding: auto">
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_level" runat="server" Text="Level"></asp:Label>
                                            </div>
                                            <div style="width: 80%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_level" runat="server" Width="450px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_user" runat="server" Text="User"></asp:Label>
                                            </div>
                                            <div style="width: 80%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_user" runat="server" Width="450px" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div id="Div11" style="width: 100%; height: 225px; overflow: auto">
                                            <%--  <asp:TreeView ID="TvApproverUseraccess" ExpandDepth="0" LeafNodeStyle-ChildNodesPadding="0"
                                                ShowLines="true" runat="server">
                                            </asp:TreeView>--%>
                                            <asp:GridView ID="TvApproverUseraccess" runat="server" ShowHeader="false" AutoGenerateColumns="False"
                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="UserAccess" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnApproverUseraccessSave" runat="server" Text="Save" CssClass="btndefault" />
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                    <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" />
                    <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" />
                    <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
