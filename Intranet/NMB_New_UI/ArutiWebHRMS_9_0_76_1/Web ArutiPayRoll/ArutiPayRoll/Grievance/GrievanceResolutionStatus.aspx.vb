﻿Imports Aruti.Data
Imports System.Data
Imports System.Drawing

Partial Class Grievance_GrievanceResolutionStatus
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Dim objResolution_Step_Tran As New clsResolution_Step_Tran
    Dim objGrievance_Master As New clsGrievance_Master
    Private Shared ReadOnly mstrModuleName As String = "frmGrievanceResolutionStatus"
    Private mintgrievancemasterunkid As Integer = 0
    Dim objEmployee As New clsEmployee_Master


    Dim mintattachempid As Integer = 0
#End Region

#Region "Form Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CInt(Session("Employeeunkid")) > 0 Then
                mintattachempid = CInt(Session("Employeeunkid"))
            ElseIf CInt(Session("UserId")) > 0 Then
                mintattachempid = 0
            End If


            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                FillCombo()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Method"
    Private Sub FillGrievanceList(ByVal isblank As Boolean)
        Try
            Dim objCMaster As New clsCommon_Master
            Dim dtList As New DataTable
            Dim strfilter As String = ""

            'If drpRefno.SelectedValue > 0 Then
            '    strfilter += "and grievancerefno = '" + drpRefno.SelectedItem.Text + "' "
            'End If

            If isblank Then
                strfilter = "and 1 = 2 "
            End If

            If strfilter.Trim.Length > 0 Then strfilter = strfilter.Substring(3)


            'Gajanan [24-June-2019] -- Start      
            Dim refNo As String = ""
            If IsNothing(drpRefno.SelectedItem) = False AndAlso drpRefno.SelectedValue > 0 Then
                refNo = drpRefno.SelectedItem.ToString()
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            Dim dsApprovers As New DataSet
            Dim eApprType As enGrievanceApproval
            Dim objAppr As New clsgrievanceapprover_master


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If
            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing)

            dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, "", False, "", True, -1, False)

            'Gajanan [24-OCT-2019] -- End


            'Gajanan [4-July-2019] -- End



            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes



            'dtList = objResolution_Step_Tran.GetEmployeeResolutionStatus(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
            '                                                             0, Session("Database_Name").ToString, _
            '                                                             CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
            '                                                             Session("UserAccessModeSetting").ToString, Session("EmployeeAsOnDate").ToString(), _
            '                                                            drpEmployee.SelectedValue.ToString(), drpRefno.SelectedItem.ToString(), CInt(drpRefno.SelectedValue), False)

            dtList = objResolution_Step_Tran.GetEmployeeResolutionStatus(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
                                                                         0, Session("Database_Name").ToString, _
                                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                         Session("UserAccessModeSetting").ToString, Session("EmployeeAsOnDate").ToString(), _
                                                                         IIf(drpEmployee.SelectedValue > 0, drpEmployee.SelectedValue.ToString(), ""), _
                                                                         refNo, 0, False, drpApproverStatus.SelectedValue, drpEmployeeStatus.SelectedValue, CInt(Session("Employeeunkid")))

            'Gajanan [27-June-2019] -- End

            'GvPreviousResponse.DataSource = dtList
            'GvPreviousResponse.DataBind()


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes

            For Each iRow As DataRow In dtList.Rows
                Dim drRow As DataRow() = Nothing
                If iRow("approverempid").ToString() = iRow("mapuserunkid").ToString() Then
                    drRow = dsApprovers.Tables(0).Select("approverempid = " & iRow("mapuserunkid").ToString() & "")
                Else
                    drRow = dsApprovers.Tables(0).Select("approverempid = " & iRow("approverempid").ToString() & "")
                End If


                If drRow.Length > 0 Then
                    iRow("name") = drRow(0)("name").ToString()
                End If
            Next
            'Gajanan [4-July-2019] -- End

            Dim dtStatus As DataTable = dtList.Clone
            Dim strRefno As String = ""
            dtStatus.Columns.Add("IsGrp", Type.GetType("System.String"))
            Dim dtRow() As DataRow = Nothing
            Dim grpRow As DataRow = Nothing
            For Each drow As DataRow In dtList.Rows
                If CStr(drow("grievancerefno")).Trim <> strRefno.Trim Then
                    grpRow = dtStatus.NewRow
                    grpRow("IsGrp") = True
                    grpRow("grievancerefno") = drow("grievancerefno").ToString()
                    'Gajanan [4-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
                    grpRow("AgainstEmployee") = drow("AgainstEmployee").ToString()
                    'Gajanan [4-July-2019] -- End      
                    strRefno = drow("grievancerefno").ToString()

                    'Gajanan [24-OCT-2019] -- Start   
                    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    grpRow("priority") = 0
                    'Gajanan [24-OCT-2019] -- End


                    dtStatus.Rows.Add(grpRow)
                    dtRow = dtList.Select(("grievancerefno = '" & strRefno.Trim & "' "))
                    If dtRow.Count > 0 Then
                        For Each irow As DataRow In dtRow
                            dtStatus.ImportRow(irow)
                        Next
                    End If
                End If
            Next


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   

            If CInt(Session("GrievanceReportingToMaxApprovalLevel")) > 0 Then
                Dim dv As DataView = dtStatus.DefaultView
                dv.RowFilter = "priority <= " & CInt(Session("GrievanceReportingToMaxApprovalLevel"))
                dtStatus = dv.ToTable()
            End If

            'Gajanan [24-OCT-2019] -- End



            GvPreviousResponse.DataSource = dtStatus
            GvPreviousResponse.DataBind()

            'Gajanan [24-June-2019] -- End

            If dtList.Rows.Count <= 0 Then
                'dtList = objResolution_Step_Tran.GetEmployeeResolutionStatus("List", CStr(Session("Database_Name")), Session("EmployeeAsOnDate").ToString, False, _
                '                                            True, False, Nothing, strfilter, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString), 0, CInt(Session("Employeeunkid")).ToString())

                'isblank = True
            End If


            'If isblank = True Then
            '    GvPreviousResponse.Rows(0).Visible = False
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [24-June-2019] -- Start      
    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#DDD")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    'Gajanan [24-June-2019] -- End


    Private Sub FillCombo()
        Try
            Dim dsList As DataSet
            dsList = Nothing
            Dim strfilter As String = String.Empty

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            'dsList = objGrievance_Master.GetRefno("GrievanceList", CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), 0, "", True, "")            
            dsList = objGrievance_Master.GetRefno("GrievanceList", CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CInt(Session("Employeeunkid")), "", True, "", False)
            'Gajanan [27-June-2019] -- End
            drpEmployee.DataSource = dsList.Tables("GrievanceList").DefaultView.ToTable(True, "againstemployeeunkid", "Againstemployee")
            drpEmployee.DataTextField = "Againstemployee"
            drpEmployee.DataValueField = "againstemployeeunkid"
            drpEmployee.DataBind()

            dsList = Nothing

            dsList = objResolution_Step_Tran.GetApproverResponse_Type("ApproverStatus", True)

            drpApproverStatus.DataSource = dsList.Tables("ApproverStatus")
            drpApproverStatus.DataTextField = "NAME"
            drpApproverStatus.DataValueField = "Id"
            drpApproverStatus.DataBind()


            dsList = Nothing
            dsList = objResolution_Step_Tran.getEmployeeStatusFilter("EmployeeStatus")

            drpEmployeeStatus.DataSource = dsList.Tables("EmployeeStatus")
            drpEmployeeStatus.DataTextField = "NAME"
            drpEmployeeStatus.DataValueField = "Id"
            drpEmployeeStatus.DataBind()


            'dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
            '                                        CInt(Session("UserId")), _
            '                                        CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        Session("UserAccessModeSetting").ToString(), True, _
            '                                        False, "List", False, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strfilter, , False, False)

            'Dim dr As DataRow = dsList.Tables(0).NewRow()
            'dr("employeeunkid") = "0"
            'dr("employeename") = "Select"
            'dsList.Tables(0).Rows.InsertAt(dr, 0)

            'drpEmployee.DataSource = dsList
            'drpEmployee.DataTextField = "employeename"
            'drpEmployee.DataValueField = "employeeunkid"
            'drpEmployee.DataBind()


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Dropdown Event"
    Protected Sub drpEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.SelectedIndexChanged
        Try
            Dim dsList As DataSet
            dsList = Nothing
            Dim strfilter As String = String.Empty
            dsList = objGrievance_Master.GetRefno("RefnoList", CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CInt(drpEmployee.SelectedValue), "", True, "")
            drpRefno.DataSource = dsList.Tables("RefnoList").DefaultView.ToTable(True, "grievancerefno", "grievancemasterunkid")
            drpRefno.DataTextField = "grievancerefno"
            drpRefno.DataValueField = "grievancemasterunkid"
            drpRefno.DataBind()

        Catch ex As Exception

            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button Event"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            'Gajanan [24-June-2019] -- Start      

            'If drpEmployee.SelectedValue <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Select Against Employee To Continue."), Me)
            '    Exit Sub
            'End If


            'If drpRefno.Items.Count > 0 AndAlso drpRefno.SelectedValue <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select Referance Number To Continue."), Me)
            '    Exit Sub
            'End If
            'Gajanan [24-June-2019] -- End

            FillGrievanceList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Gajanan [24-June-2019] -- Start      
#Region "Gridview Event"
    Protected Sub GvPreviousResponse_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvPreviousResponse.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                If IsDBNull(dt.Rows(e.Row.RowIndex)("IsGrp")) = False AndAlso CBool(dt.Rows(e.Row.RowIndex)("IsGrp").ToString()) = True Then
                    'Gajanan [4-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
                    'Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("grievancerefno").ToString(), GvPreviousResponse)
                    Me.AddGroup(e.Row, "<b>" + dt.Rows(e.Row.RowIndex)("grievancerefno").ToString() + " (" + dt.Rows(e.Row.RowIndex)("AgainstEmployee").ToString() + ") </b>", GvPreviousResponse)
                    'Gajanan [4-July-2019] -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region
    'Gajanan [24-June-2019] -- End

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblGrrefno.ID, Me.lblGrrefno.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApproverStatus.ID, Me.lblApproverStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployeeStatus.ID, Me.lblEmployeeStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(0).FooterText, GvPreviousResponse.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(1).FooterText, GvPreviousResponse.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(2).FooterText, GvPreviousResponse.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(3).FooterText, GvPreviousResponse.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(4).FooterText, GvPreviousResponse.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(5).FooterText, GvPreviousResponse.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPreviousResponse.Columns(6).FooterText, GvPreviousResponse.Columns(6).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblGrrefno.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrrefno.ID, Me.lblGrrefno.Text)
            Me.lblApproverStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApproverStatus.ID, Me.lblApproverStatus.Text)
            Me.lblEmployeeStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployeeStatus.ID, Me.lblEmployeeStatus.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            GvPreviousResponse.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(0).FooterText, GvPreviousResponse.Columns(0).HeaderText)
            GvPreviousResponse.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(1).FooterText, GvPreviousResponse.Columns(1).HeaderText)
            GvPreviousResponse.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(2).FooterText, GvPreviousResponse.Columns(2).HeaderText)
            GvPreviousResponse.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(3).FooterText, GvPreviousResponse.Columns(3).HeaderText)
            GvPreviousResponse.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(4).FooterText, GvPreviousResponse.Columns(4).HeaderText)
            GvPreviousResponse.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(5).FooterText, GvPreviousResponse.Columns(5).HeaderText)
            GvPreviousResponse.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPreviousResponse.Columns(6).FooterText, GvPreviousResponse.Columns(6).HeaderText)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub




End Class
