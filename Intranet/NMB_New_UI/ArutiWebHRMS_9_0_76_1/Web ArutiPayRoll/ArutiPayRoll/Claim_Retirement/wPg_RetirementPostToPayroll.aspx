﻿<%@ Page Title="Post To Payroll" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_RetirementPostToPayroll.aspx.vb" Inherits="wPg_RetirementPostToPayroll" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkselect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkselect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkselect]", grid).length == $("[id*=chkselect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
        
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Post To Payroll"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Retirement Posting"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblViewBy" runat="server" Text="View Type"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboViewBy" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblRetirementNo" runat="server" Text="Retirement No"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRetirementNo" runat="server" Width="99%" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExpCategory" runat="server" Text="Exp. Cat."></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpCategory" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblToDate" runat="server" Text="To Date"></asp:Label>
                                        <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpense" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-35">
                                        <asp:CheckBox ID="chkUnReitreTransaction" runat="server" Text="Show UnRetire Transaction"
                                            AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <%--'S.SANDEEP |25-FEB-2022| -- START--%>
                                <%--'ISSUE : OLD-575--%>
                                <asp:CheckBox ID="chkSkipZeroBalance" runat="server" Text="Don't Show Zero Balance"
                                                AutoPostBack="true" CssClass="pull-left"></asp:CheckBox>
                                <%--'S.SANDEEP |25-FEB-2022| -- END--%>                               
                                <asp:Button ID="btnUnposting" runat="server" Text="UnPost" CssClass="btn btn-primary" />
                                <asp:Button ID="btnPosting" runat="server" Text="Post" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:GridView ID="dgvRetirementPosting" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                ShowFooter="false" Width="99%" DataKeyNames="crretirementprocessunkid,claimretirementunkid,claimretirementtranunkid,crmasterunkid,crapprovaltranunkid,Retirement,employeeunkid,Isgroup,Ischecked,ischange,tranheadunkid,transactiondate">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="25">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkselect" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Retirement" HeaderText="Particulars" FooterText="colhParticulars" />
                                                    <asp:BoundField DataField="Expense" HeaderText="Expenses" FooterText="colhExpense" />
                                                    <asp:BoundField DataField="bal_amount" HeaderText="Balance" FooterText="colhbalamount"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="currency_sign" HeaderText="Currency" FooterText="colhCurrency" />
                                                    <asp:BoundField DataField="Period" HeaderText="Period" FooterText="colhPeriod" />
                                                    <asp:BoundField DataField="transactiondate" HeaderText="Transaction Date" FooterText="objcolhtransactiondate"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="ischange" HeaderText="IsChange" FooterText="objcolhIschange"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
