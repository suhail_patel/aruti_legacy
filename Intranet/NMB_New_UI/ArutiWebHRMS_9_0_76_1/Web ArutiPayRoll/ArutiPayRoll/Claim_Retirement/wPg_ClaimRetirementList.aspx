<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ClaimRetirementList.aspx.vb"
    Inherits="Claim_Retirement_wPg_ClaimRetirementList" Title="Retirement List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <h2>
                    <asp:Label ID="lblPageHeader" runat="server" Text="Retirement List"></asp:Label>
                </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmployee" runat="server" />
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblExpenseCategory" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboExpenseCategory" runat="server" />
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboStatus" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblDateFrom" runat="server" Text="Date from" CssClass="form-label"></asp:Label>
                                    <datectrl:uc ID="dtpDatefrom" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblDateto" runat="server" Text="Date To" CssClass="form-label"></asp:Label>
                                    <datectrl:uc ID="dtpDateto" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 350px">
                                        <asp:GridView ID="gvRetirement" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                            CssClass="table table-hover table-bordered" DataKeyNames="IsGrp,crmasterunkid,claimretirementunkid,employeeunkid,expensetypeid,statusunkid,issubmit_approval,employeename">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="25px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                            <asp:LinkButton ID="lnkRetire" runat="server" ToolTip="Retire" CommandName="Retire"> 
                                                                         <i class="fa fa-list-alt iconsize"></i> 
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="25px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                            <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit" CommandName="Edit">
                                                                             <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="claimretirementno" HeaderText="Retirement No." FooterText="dgcolhRetirementNo"
                                                    ItemStyle-Width="100px" />
                                                <asp:BoundField DataField="claimrequestno" HeaderText="Claim No." FooterText="dgcolhClaimNo"
                                                    Visible="false" />
                                                <asp:BoundField DataField="expensetype" HeaderText="Expense Category" FooterText="dgcolhExpenseCategory" />
                                                <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="dgcolhEmployee"
                                                    ItemStyle-Width="210px" Visible="false" />
                                                <asp:BoundField DataField="transactiondate" HeaderText="Date" FooterText="dgcolhtransactionDate" />
                                                <asp:BoundField DataField="approved_amount" HeaderText="Approved Amount" FooterText="dgcolhApprovedAmount"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="retirement_amount" HeaderText="Retired  Amount" FooterText="dgcolhRetiredAmount"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="balance" HeaderText="Balance" FooterText="dgcolhBalance"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="status" HeaderText="Status" FooterText="dgcolhStatus" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
