<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ClaimRetirement_ApprovalList.aspx.vb"
    Inherits="Claim_Retirement_wPg_ClaimRetirement_ApprovalList" Title="Claim Retirement Approval List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Claim Retirement Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r-30 p-l-0">
                                    <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations" Visible="false">
                                                                  <i class="fas fa-filter"></i>
                                    </asp:LinkButton>
                                </ul>
                                <ul class="header-dropdown m-r--14  p-l-0">
                                    <asp:LinkButton ID="lnkReset" runat="server" ToolTip="Reset" Visible="false">
                                                                       <i class="fas fa-sync-alt"></i>
                                    </asp:LinkButton>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFDate" AutoPostBack="false" runat="server" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRetirementNo" runat="server" Text="Retirement No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRetirementNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpTDate" AutoPostBack="false" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRetirementApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRetirementApprover" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-35">
                                        <asp:CheckBox ID="ChkMyApprovals" runat="server" Text="My Approvals" Checked="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnSearch" CssClass="btn btn-primary" runat="server" Text="Search" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="lvClaimRetirementList" runat="server" AutoGenerateColumns="False"
                                                DataKeyField="claimretirementunkid" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" ItemStyle-Width = "50px"
                                                        FooterText="btnChangeStatus">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Approval" ToolTip="Change Status">
                                                                                  <img src="../images/change_status.png" height="20px" width="20px" alt="Change Status" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="claimretirementno" HeaderText="Retirement No" ReadOnly="true"
                                                        FooterText="dgcolhretirementno" />
                                                    <asp:BoundColumn DataField="claimrequestno" HeaderText="Claim No" ReadOnly="true"
                                                        FooterText="dgcolhclaimno" />
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Emp. Code" ReadOnly="true"
                                                        FooterText="dgcolhEmployeecode" />
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhemployee" />
                                                    <asp:BoundColumn DataField="approvername" HeaderText="Approver" ReadOnly="true" FooterText="dgcolhRetirementApprover" />
                                                    <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" ReadOnly="true"
                                                        FooterText="dgcolhapprovaldate" />
                                                    <asp:BoundColumn DataField="amount" HeaderText="Imprest Amount" ReadOnly="true" FooterText="dgcolhamount"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="balance" HeaderText="Balance" ReadOnly="true" FooterText="dgcolhbalance"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="AppprovalStatus" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeunkId" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhemployeeunkid" />
                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="approverunkid" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhapproverunkid" />
                                                    <asp:BoundColumn DataField="approveremployeeunkid" HeaderText="approverEmpID" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhapproveremployeeunkid" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhstatusunkid" />
                                                    <asp:BoundColumn DataField="iscancel" HeaderText="IsCancel" ReadOnly="true" Visible="false"
                                                        FooterText="objdgcolhIsCancel" />
                                                    <asp:BoundColumn DataField="mapuserunkid" HeaderText="Map UserId" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhmapuserunkid" />
                                                    <asp:BoundColumn DataField="crpriority" HeaderText="Priority" ReadOnly="true" Visible="false"
                                                        FooterText="objdgcolhcrpriority" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="crmasterunkid" ReadOnly="true"
                                                        Visible="false" FooterText="objdgcolhcrmasterunkid" />
                                                    <asp:BoundColumn DataField="crlevelname" ReadOnly="true" Visible="false" FooterText="objdgcolhcrlevelname">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="tdate" HeaderText="tdate" Visible="false" FooterText="objcolhTranscationDate" />
                                                    <asp:BoundColumn DataField="isexternalapprover" Visible="false" FooterText="objdgcolhisexternalapprover" />
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                    <asp:BoundColumn DataField="claimretirementunkid" HeaderText="claimretirementunkid"
                                                        ReadOnly="true" FooterText="objdgcolhclaimretirementunkid" Visible="false" />
                                                    <asp:BoundColumn DataField="claim_amount" HeaderText="Claim Amount" ReadOnly="true"
                                                        FooterText="objdgcolhclaimamount" Visible="false" />
                                                      <%--'Pinkal (28-Oct-2021)-- Start
                                                      'Problem in Assigning Leave Accrue Issue.--%>
                                                    <asp:BoundColumn DataField="crlevelunkid" HeaderText="LevelId" ReadOnly="true" Visible="false" FooterText = "objdgcolhcrlevelunkid" />
                                                    <%--'Pinkal (28-Oct-2021)-- End--%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="panel-primary">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                       
                                    </div>
                                    <div style="float: right">

                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 9%">
                                           
                                        </div>
                                        <div class="ib" style="width: 28%">
                                            
                                        </div>
                                        <div class="ib" style="width: 9%">
                                           
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            
                                        </div>
                                        <div class="ib" style="width: 9%">
                                           
                                        </div>
                                        <div class="ib" style="width: 22%">
                                           
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 9%">
                                            
                                        </div>
                                        <div class="ib" style="width: 28%">
                                           
                                        </div>
                                        <div class="ib" style="width: 9%">
                                           
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 9%">
                                         
                                        </div>
                                        <div class="ib" style="width: 28%">
                                           
                                        </div>
                                        <div class="ib" style="width: 50%">
                                           
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <asp:Panel ID="pnl_lvClaimRetirementList" ScrollBars="Auto" Height="300px" runat="server">
                                            
                                        </asp:Panel>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                <uc5:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
