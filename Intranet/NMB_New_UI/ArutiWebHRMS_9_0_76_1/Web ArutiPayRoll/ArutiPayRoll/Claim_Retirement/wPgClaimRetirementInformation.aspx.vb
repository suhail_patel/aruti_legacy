﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class Claim_Retirement_wPgClaimRetirementInformation
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmClaimRetirementInformation"
    Private ReadOnly mstrModuleName1 As String = "frmDependentsList"

    Private DisplayMessage As New CommonCodes
    Private mintClaimRequestMstId As Integer = 0
    Private mintClaimRetirementId As Integer = 0
    Private mintClaimretirementtranunkid As Integer = 0
    Private mintCREmpId As Integer = 0
    Private mintExpeseTypeId As Integer = 0
    Private mintBaseCountryId As Integer = 0
    Private mdtImprestDetail As DataTable = Nothing
    Private mdtAttachment As DataTable = Nothing
    Private mdtFinalAttachment As DataTable = Nothing
    Private mblnIsClaimFormBudgetMandatory As Boolean = False
    Private mblnIsHRExpense As Boolean = False
    Private mdtApplyDate As DateTime = Nothing
    Private mintEditRowIndex As Integer = -1
    Private mintAttachmentIndex As Integer = -1
    Private mdecFinalCRapprovedAmount As Decimal = 0
    Private mdecExpenseBalance As Decimal = 0
    Private mdecEditOldExpenseAmt As Decimal = 0
    Private mblnIsEditClick As Boolean = False

    Private mblnShowAttchmentPopup As Boolean
    Private blnClaimDetailPopup As Boolean = False
    Private mblnShowDependantPopup As Boolean

    Private objClaimRetirement As New clsclaim_retirement_master
    Private objClaimRetirementTran As New clsclaim_retirement_Tran


    'Pinkal (07-Nov-2019) -- Start
    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
    Private mstrExpenseIDs As String = ""
    'Pinkal (07-Nov-2019) -- End


    'Pinkal (18-Mar-2021) -- Start
    'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
    Private mintRetirementRowIndex As Integer = -1
    'Pinkal (18-Mar-2021) -- End


    'Pinkal (06-Sep-2021)-- Start
    'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
    Private mintClaimApplicationCountryId As Integer = 0
    'Pinkal (06-Sep-2021) -- End


#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then

                If Session("ClaimRetirementId") IsNot Nothing AndAlso CInt(Session("ClaimRetirementId")) > 0 Then
                    mintClaimRetirementId = CInt(Session("ClaimRetirementId"))
                End If

                If Session("ClaimRequestId") IsNot Nothing AndAlso CInt(Session("ClaimRequestId")) > 0 Then
                    mintClaimRequestMstId = CInt(Session("ClaimRequestId"))
                End If

                If Session("CREmpId") IsNot Nothing AndAlso CInt(Session("CREmpId")) > 0 Then
                    mintCREmpId = CInt(Session("CREmpId"))
                End If


                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                FillCombo()
                GetClaimDetails()
                GetValue()
                SetVisibility()
                objClaimRetirementTran._Claimretirementunkid = mintClaimRetirementId
                mdtImprestDetail = objClaimRetirementTran._DataTable

                If mdtFinalAttachment Is Nothing Then
                    Dim objAttchement As New clsScan_Attach_Documents
                    'Pinkal (04-Jul-2020) -- Start
                    'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "ISNULL(hrdocuments_tran.scanattachrefid,-1) = " & enScanAttactRefId.CLAIM_RETIREMENT)
                    End If
                    'Pinkal (04-Jul-2020) -- End
                    Dim strTranIds As String = String.Join(",", mdtImprestDetail.AsEnumerable().Select(Function(x) x.Field(Of Integer)("Claimretirementtranunkid").ToString).ToArray)
                    If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                    mdtFinalAttachment = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_RETIREMENT & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    objAttchement = Nothing
                Else
                    mdtAttachment = mdtFinalAttachment.Copy()
                End If

                FillImprestDetail()

            Else
                Call SetLanguage()
                mintClaimRetirementId = CInt(Me.ViewState("ClaimRetirementId"))
                mintClaimRequestMstId = CInt(ViewState("ClaimRequestMstId"))
                mintClaimretirementtranunkid = CInt(ViewState("Claimretirementtranunkid"))

                mdtImprestDetail = CType(Me.ViewState("ImprestDetail"), DataTable)
                mdtAttachment = CType(Me.ViewState("Attachment"), DataTable)
                mdtFinalAttachment = CType(Me.ViewState("FinalAttachment"), DataTable)

                mintCREmpId = CInt(ViewState("CREmpId"))
                mintExpeseTypeId = CInt(ViewState("ExpeseTypeId"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mdtApplyDate = CType(Me.ViewState("ApplyDate"), DateTime)
                mintEditRowIndex = CInt(Me.ViewState("EditRowIndex"))
                mintAttachmentIndex = CInt(Me.ViewState("AttachmentIndex"))
                mdecFinalCRapprovedAmount = CDec(Me.ViewState("FinalCRapprovedAmount"))
                mdecExpenseBalance = CDec(Me.ViewState("ExpenseBalance"))
                mdecEditOldExpenseAmt = CDec(Me.ViewState("EditOldExpenseAmt"))
                mblnShowAttchmentPopup = CBool(Me.ViewState("ShowAttchmentPopup"))
                blnClaimDetailPopup = CBool(Me.ViewState("blnClaimDetailPopup"))
                mblnShowDependantPopup = CBool(Me.ViewState("ShowDependantPopup"))
                mblnIsEditClick = CBool(Me.ViewState("mblnIsEditClick"))
                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))


                'Pinkal (07-Nov-2019) -- Start
                'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                mstrExpenseIDs = Me.ViewState("ExpenseIDs").ToString()
                'Pinkal (07-Nov-2019) -- End


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                mintRetirementRowIndex = CInt(ViewState("mintRetirementRowIndex"))
                'Pinkal (18-Mar-2021) -- End

                'Pinkal (06-Sep-2021)-- Start
                'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
                mintClaimApplicationCountryId = CInt(ViewState("ClaimApplicationCountryId"))
                'Pinkal (06-Sep-2021) -- End

            End If




            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            If mblnShowAttchmentPopup Then
                popup_ScanAttchment.Show()
            End If

            If blnClaimDetailPopup Then
                popupClaimDetail.Show()
            End If

            'If mblnShowDependantPopup Then
            '    popupDependantList.Show()
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            Me.ViewState("ClaimRetirementId") = mintClaimRetirementId
            Me.ViewState("ClaimRequestMstId") = mintClaimRequestMstId
            Me.ViewState("Claimretirementtranunkid") = mintClaimretirementtranunkid
            Me.ViewState("ImprestDetail") = mdtImprestDetail
            Me.ViewState("Attachment") = mdtAttachment
            Me.ViewState("FinalAttachment") = mdtFinalAttachment
            Me.ViewState("CREmpId") = mintCREmpId
            Me.ViewState("ExpeseTypeId") = mintExpeseTypeId
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("ApplyDate") = mdtApplyDate
            Me.ViewState("EditRowIndex") = mintEditRowIndex
            Me.ViewState("AttachmentIndex") = mintAttachmentIndex
            Me.ViewState("mdecFinalCRapprovedAmount") = mdecFinalCRapprovedAmount
            Me.ViewState("ExpenseBalance") = mdecExpenseBalance
            Me.ViewState("EditOldExpenseAmt") = mdecEditOldExpenseAmt
            Me.ViewState("blnClaimDetailPopup") = blnClaimDetailPopup
            Me.ViewState("ShowAttchmentPopup") = mblnShowAttchmentPopup
            Me.ViewState("ShowDependantPopup") = mblnShowDependantPopup
            Me.ViewState("mblnIsEditClick") = mblnIsEditClick
            Me.ViewState("IsHRExpense") = mblnIsHRExpense

            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            Me.ViewState("ExpenseIDs") = mstrExpenseIDs
            'Pinkal (07-Nov-2019) -- End

            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            ViewState("mintRetirementRowIndex") = mintRetirementRowIndex
            'Pinkal (18-Mar-2021) -- End

            'Pinkal (06-Sep-2021)-- Start
            'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
            ViewState("ClaimApplicationCountryId") = mintClaimApplicationCountryId
            'Pinkal (06-Sep-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            'Session.Remove("ClaimRetirementId")
            'Session.Remove("ClaimRequestId")
            'Session.Remove("CREmpId")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try
            If CInt(Session("ClaimRetirementFormNoType")) = 1 Then
                txtClaimRetirementNo.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim blnSelect As Boolean = True
            Dim blnApplyFilter As Boolean = True
            Dim objEMaster As New clsEmployee_Master

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If mintCREmpId > 0 Then
                    blnSelect = False
                End If
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyFilter = False
            End If

            Dim dsCombo As DataSet = objEMaster.GetEmployeeList(Session("Database_Name").ToString() _
                                                                                             , CInt(Session("UserId")) _
                                                                                             , CInt(Session("Fin_year")) _
                                                                                             , CInt(Session("CompanyUnkId")) _
                                                                                             , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                             , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                             , Session("UserAccessModeSetting").ToString(), True _
                                                                                             , False, "List", blnSelect, mintCREmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintCREmpId.ToString()
            End With

            With cboCDClaimEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCREmpId.ToString()
            End With

            objEMaster = Nothing

            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
                .DataBind()
            End With

            With cboCDClaimCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dtTable.Copy()
                .DataBind()
                .Enabled = False
            End With

            objCostCenter = Nothing


            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.

            Dim objClaim As New clsclaim_request_master
            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            objClaim._Crmasterunkid = mintClaimRequestMstId
            'Pinkal (10-Feb-2021) -- End
            dsCombo = objClaim.GetFinalApproverApprovedDetails(CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboEmployee.SelectedValue), objClaim._Expensetypeid, mintClaimRequestMstId, 0, True)
            objClaim = Nothing


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            'mstrExpenseIDs = String.Join(",", dsCombo.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("expenseunkid").ToString()).ToArray())
            mstrExpenseIDs = String.Join(",", dsCombo.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("expenseunkid").ToString()).Distinct().ToArray())
            'Pinkal (10-Feb-2021) -- End



            Dim dRow As DataRow = dsCombo.Tables(0).NewRow
            dRow("expenseunkid") = 0
            dRow("Expense") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Select")
            dsCombo.Tables(0).Rows.InsertAt(dRow, 0)


            cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
            'Pinkal (24-Oct-2019) -- End

            'cboImprest_SelectedIndexChanged(cboImprest, New EventArgs())

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Dim dtExpense As DataTable = dsCombo.Tables(0).DefaultView.ToTable(True, "expenseunkid", "Expense")
            With cboCDClaimExpense
                .DataValueField = "expenseunkid"
                .DataTextField = "Expense"
                '.DataSource = dsCombo.Tables(0).Copy
                .DataSource = dtExpense
                .DataBind()
            End With
            objClaim = Nothing
            'Pinkal (10-Feb-2021) -- End


            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With

            With cboCDClaimCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0).Copy()
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            objExchange = Nothing

            Dim objcommonMst As New clsCommon_Master
            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With
            objcommonMst = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetClaimDetails()
        Try
            If mintClaimRequestMstId > 0 Then
                Dim objclaim As New clsclaim_request_master
                objclaim._Crmasterunkid = mintClaimRequestMstId
                mintExpeseTypeId = objclaim._Expensetypeid
                txtClaimNo.Text = objclaim._Claimrequestno
                txtImprestAmount.Text = Format(CDec(objclaim.GetFinalApproverApprovedAmount(objclaim._Employeeunkid, objclaim._Expensetypeid, mintClaimRequestMstId, True)), Session("fmtcurrency").ToString())

                '/ START  SET CLAIM DETAIL FOR POPUP 
                FillClaimDetailCombo()
                txtCDClaimNo.Text = txtClaimNo.Text
                dtpCDClaimDate.SetDate = objclaim._Transactiondate.Date

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboCDClaimEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtCDClaimDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtCDClaimDomicileAddress.Text = ""
                End If

                txtCDClaimRemark.Text = objclaim._Claim_Remark

                '/ END  SET CLAIM DETAIL FOR POPUP 
                objclaim = Nothing

                'Pinkal (06-Sep-2021)-- Start
                'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
                Dim objClaimRequestTran As New clsclaim_request_tran
                objClaimRequestTran._ClaimRequestMasterId = mintClaimRequestMstId
                Dim mdtClaimRequest As DataTable = objClaimRequestTran._DataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isimprest") = True).ToList().CopyToDataTable()
                objClaimRequestTran = Nothing

                If mdtClaimRequest IsNot Nothing AndAlso mdtClaimRequest.Rows.Count > 0 Then
                    If mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        mintClaimApplicationCountryId = mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First()
                    Else
                        mintClaimApplicationCountryId = mintBaseCountryId
                    End If
                Else
                    mintClaimApplicationCountryId = mintBaseCountryId
                End If
                mdtClaimRequest.Clear()
                mdtClaimRequest = Nothing
                'Pinkal (06-Sep-2021) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objClaimRetirement._Claimretirementunkid = mintClaimRetirementId
            txtClaimRetirementNo.Text = objClaimRetirement._ClaimRetirementNo
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpCDClaimDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Function Validation() As Boolean
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboImprest.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Imprest is mandatory information. Please select Imprest to continue."), Me)
                cboImprest.Focus()
                Return False
            End If

            If txtQuantity.Text.Trim() = "" OrElse CDec(txtQuantity.Text) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQuantity.Focus()
                Return False
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboCurrency.Focus()
                Return False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try
            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddRetirement()
        Try
            Dim dtmp() As DataRow = Nothing
            If mdtImprestDetail.Rows.Count > 0 Then
                dtmp = mdtImprestDetail.Select("expenseunkid = '" & CInt(cboImprest.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQuantity.Text.Trim.Length > 0, CDec(txtQuantity.Text), 0)) & _
                                                              "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtRemark.Text.Trim() & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot add same Imprest again in the below list."), Me)
                    Exit Sub
                End If
            End If
            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboImprest.SelectedValue)

            mblnIsHRExpense = objExpMaster._IsHRExpense
            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""

            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            'If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '    If CDec(txtQuantity.Text) > CDec(txtBalance.Text) Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set quantity greater than balance set."), Me)
            '        txtQuantity.Focus()
            '        Exit Sub
            '    End If

            'ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '    If CDec(CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) > CDec(txtBalance.Text) Then
            '        'Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, you cannot set amount greater than balance set."), Me)
            '        txtQuantity.Focus()
            '        Exit Sub
            '    End If
            'End If

            'If Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
            '    If CDec(txtBalance.Text) < (CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry,you cannot apply for this retirement.Reason : This retirement amout is exceeded the balance amount."), Me)
            '        Exit Sub
            '    End If
            'End If

            objExpMaster = Nothing


            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), ConfigParameter._Object._CurrentDateAndTime.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            Dim xCount = mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Claimretirementtranunkid") = -999).Count
            If xCount > 0 Then
                mdtImprestDetail.Rows.Clear()
            End If
            'Pinkal (04-Jul-2020) -- End



            Dim dRow As DataRow = mdtImprestDetail.NewRow

            dRow.Item("Claimretirementtranunkid") = -1
            dRow.Item("Claimretirementunkid") = mintClaimRetirementId
            dRow.Item("crmasterunkid") = mintClaimRequestMstId
            dRow.Item("expenseunkid") = CInt(cboImprest.SelectedValue)
            dRow.Item("expense") = cboImprest.SelectedItem.Text
            dRow.Item("quantity") = txtQuantity.Text
            dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQuantity.Text)
            dRow.Item("balance") = CDec(mdecExpenseBalance - CDec(dRow.Item("amount"))).ToString(Session("fmtcurrency").ToString())
            dRow.Item("expense_remark") = txtRemark.Text
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("iscancel") = False
            dRow.Item("canceluserunkid") = -1
            dRow.Item("cancel_datetime") = DBNull.Value
            dRow.Item("cancel_remark") = ""

            If CInt(Session("employeeunkid")) > 0 Then
                dRow.Item("loginemployeeunkid") = CInt(Session("employeeunkid"))
            Else
                dRow.Item("loginemployeeunkid") = -1
            End If
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("voidloginemployeeunkid") = -1


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            mdecExpenseBalance = mdecExpenseBalance - CDec(dRow.Item("amount"))
            dRow.Item("claim_amount") = mdecExpenseBalance
            dRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            'Pinkal (04-Jul-2020) -- End

            mdtImprestDetail.Rows.Add(dRow)

            FillImprestDetail()
            Enable_Disable_Ctrls(False)
            Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EditRetirement()
        Try
            Dim iRow As DataRow() = Nothing
            If CInt(dgvRetirement.DataKeys(mintEditRowIndex).Values("Claimretirementtranunkid").ToString()) > 0 Then
                iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & CInt(dgvRetirement.DataKeys(mintEditRowIndex).Values("Claimretirementtranunkid").ToString()) & "' AND AUD <> 'D'")
            Else
                iRow = mdtImprestDetail.Select("GUID = '" & dgvRetirement.DataKeys(mintEditRowIndex).Values("GUID").ToString() & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("Claimretirementtranunkid")) > 0 Then
                    dtmp = mdtImprestDetail.Select("Claimretirementtranunkid <> '" & iRow(0).Item("Claimretirementtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboImprest.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQuantity.Text.Length > 0, CDec(txtQuantity.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtImprestDetail.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboImprest.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQuantity.Text.Length > 0, CDec(txtQuantity.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot add same Imprest again in the below list."), Me)
                    Exit Sub
                End If

                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboImprest.SelectedValue)
                mblnIsHRExpense = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""

                If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If

                'If objExpMaster._Isaccrue  Then
                'If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                '    If CDec(txtQuantity.Text) > CDec(txtBalance.Text) Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set quantity greater than balance set."), Me)
                '        txtQuantity.Focus()
                '        Exit Sub
                '    End If
                'ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                '    If CDec(CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) > CDec(txtBalance.Text) Then
                '        'Language.setLanguage(mstrModuleName)
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, you cannot set amount greater than balance set."), Me)
                '        txtQuantity.Focus()
                '        Exit Sub
                '    End If
                'End If
                'End If

                'If Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                '    If CDec(txtBalance.Text) < (CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry,you cannot apply for this retirement.Reason : This retirement amout is exceeded the balance amount."), Me)
                '        Exit Sub
                '    End If
                'End If

                objExpMaster = Nothing

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), ConfigParameter._Object._CurrentDateAndTime.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)


                iRow(0).Item("Claimretirementtranunkid") = iRow(0).Item("Claimretirementtranunkid")
                iRow(0).Item("Claimretirementunkid") = mintClaimRetirementId
                iRow(0).Item("crmasterunkid") = mintClaimRequestMstId
                iRow(0).Item("expenseunkid") = CInt(cboImprest.SelectedValue)
                iRow(0).Item("expense") = cboImprest.SelectedItem.Text
                iRow(0).Item("quantity") = CDec(txtQuantity.Text)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQuantity.Text)
                iRow(0).Item("balance") = CDec(CDec(mdecExpenseBalance + mdecEditOldExpenseAmt) - CDec(iRow(0).Item("amount"))).ToString(Session("fmtcurrency").ToString())
                iRow(0).Item("expense_remark") = txtRemark.Text
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                iRow(0).Item("iscancel") = False
                iRow(0).Item("canceluserunkid") = -1
                iRow(0).Item("cancel_datetime") = DBNull.Value
                iRow(0).Item("cancel_remark") = ""
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""

                If CInt(Session("employeeunkid")) > 0 Then
                    iRow(0).Item("loginemployeeunkid") = CInt(Session("employeeunkid"))
                Else
                    iRow(0).Item("loginemployeeunkid") = -1
                End If

                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("voidloginemployeeunkid") = -1

                mdecExpenseBalance = CDec((mdecExpenseBalance + mdecEditOldExpenseAmt) - CDec(iRow(0).Item("amount")))

                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                iRow(0).Item("claim_amount") = mdecExpenseBalance
                iRow(0).Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                'Pinkal (04-Jul-2020) -- End

                iRow(0).AcceptChanges()

                FillImprestDetail()
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboImprest.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQuantity.Text.Trim.Length > 0, txtQuantity.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillImprestDetail()
        Try


            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Dim mdView As New DataView
            mdView = mdtImprestDetail.DefaultView
            mdView.RowFilter = "AUD <> 'D'"

            Dim mblnBlank As Boolean = False
            If mdView IsNot Nothing AndAlso mdView.ToTable().Rows.Count <= 0 Then
                Dim drRow As DataRow = mdView.Table.NewRow()
                drRow("Claimretirementtranunkid") = -999
                drRow("expenseunkid") = 0
                'Pinkal (06-Sep-2021)-- Start
                'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
                'drRow("countryunkid") = mintBaseCountryId
                drRow("countryunkid") = mintClaimApplicationCountryId
                'Pinkal (06-Sep-2021) -- End
                drRow("quantity") = 0.0
                drRow("unitprice") = 0.0
                drRow("amount") = 0.0
                drRow("balance") = 0.0
                drRow("AUD") = ""
                mdView.Table.Rows.Add(drRow)
                mblnBlank = True
            End If
            'Pinkal (10-Mar-2021) -- End



            If mdView IsNot Nothing AndAlso mdView.ToTable().Rows.Count > 0 Then
                txtRetirementGrandTotal.Text = CDec(mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("amount"))).ToString(Session("fmtcurrency").ToString())
            Else
                txtRetirementGrandTotal.Text = "0.00"
            End If

            txtBalanceDue.Text = CDec(CDec(txtImprestAmount.Text) - CDec(txtRetirementGrandTotal.Text)).ToString(Session("fmtcurrency").ToString())

            dgvRetirement.DataSource = mdView
            dgvRetirement.DataBind()


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            If mblnBlank Then dgvRetirement.Rows(0).Visible = False
            'Pinkal (04-Jul-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboEmployee.Enabled = iFlag

            If mdtImprestDetail IsNot Nothing Then
                If mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = CStr(mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                    cboCurrency.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboImprest.SelectedValue = "0"
            cboImprest_SelectedIndexChanged(cboImprest, New EventArgs())
            txtRemark.Text = ""

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            'txtQuantity.Text = "0.00"
            'txtUnitPrice.Text = "0.00"
            txtQuantity.Text = "1.00"
            txtUnitPrice.Text = "1.00"
            'Pinkal (10-Mar-2021) -- End
            txtBalance.Text = "0.00"
            txtAmount.Text = "0.00"
            cboCurrency.SelectedValue = mintBaseCountryId.ToString()
            cboCostCenter.SelectedValue = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub UpdateBalance(ByVal iRowindex As Integer, ByVal mdecAmount As Decimal)
        Try
            Dim dr As DataRow() = mdtImprestDetail.Select("AUD <> 'D' ")
            If dr.Length > 0 Then
                For i As Integer = iRowindex To dr.Length - 1
                    dr(i)("balance") = CDec(dr(i)("balance")) + mdecAmount
                    dr(i).AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRetirementFormNoType")) = 0 Then
                If txtClaimRetirementNo.Text.Trim.Length <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Retirement No. is mandatory information. Please select Retirement No. to continue."), Me)
                    txtClaimRetirementNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            'If dgvRetirement.Rows.Count <= 0 Then
            If dgvRetirement.Rows.Cast(Of GridViewRow).Where(Function(x) x.Visible = True).Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please add atleast one expense retirement in order to save."), Me)
                dgvRetirement.Focus()
                Return False
            End If

            Dim objExapprover As New clsExpenseApprover_Master
            Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(enExpenseType.EXP_IMPREST, CInt(cboEmployee.SelectedValue), "List", Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please Assign Expense Approver to this employee and also map retirement Approver to system user."), Me)
                Return False
            End If
            objExapprover = Nothing

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function


    'Pinkal (28-Apr-2020) -- Start
    'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
    'Private Sub SetValue()
    Private Sub SetValue(ByVal mblnIsSubmitted As Boolean)
        'Pinkal (28-Apr-2020) -- End
        Try
            objClaimRetirement._Claimretirementunkid = mintClaimRetirementId
            objClaimRetirement._ClaimRetirementNo = txtClaimRetirementNo.Text
            objClaimRetirement._Crmasterunkid = mintClaimRequestMstId
            objClaimRetirement._Expensetypeunkid = enExpenseType.EXP_IMPREST
            objClaimRetirement._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objClaimRetirement._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            objClaimRetirement._Approverunkid = -1
            objClaimRetirement._Approveremployeeunkid = -1
            objClaimRetirement._Retirement_Remark = ""
            objClaimRetirement._Claim_Amount = CDec(txtImprestAmount.Text)
            objClaimRetirement._Imprest_Amount = 0
            objClaimRetirement._statusunkid = 2
            objClaimRetirement._P2prequisitionid = ""
            objClaimRetirement._P2pposteddata = ""
            objClaimRetirement._P2pstatuscode = ""
            objClaimRetirement._P2pmessage = ""

            objClaimRetirement._Isvoid = False
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                objClaimRetirement._Userunkid = CInt(Session("UserId"))
                objClaimRetirement._Loginemployeeunkid = -1
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objClaimRetirement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objClaimRetirement._Userunkid = -1
            End If
            objClaimRetirement._Voiddatetime = Nothing
            objClaimRetirement._Voiduserunkid = -1
            objClaimRetirement._Voidloginemployeeunkid = -1

            objClaimRetirement._Iscancel = False
            objClaimRetirement._Cancel_Remark = ""
            objClaimRetirement._Cancel_Datetime = Nothing
            objClaimRetirement._Canceluserunkid = -1


            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
            objClaimRetirement._Issubmit_Approval = mblnIsSubmitted
            'Pinkal (28-Apr-2020) -- End


            If CInt(Session("UserId")) > 0 Then
                objClaimRetirement._LoginMode = enLogin_Mode.MGR_SELF_SERVICE
            ElseIf CInt(Session("Employeeunkid")) > 0 Then
                objClaimRetirement._LoginMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            objClaimRetirement._FormName = mstrModuleName
            objClaimRetirement._WebClientIP = Session("IP_ADD").ToString()
            objClaimRetirement._WebHostName = Session("HOST_NAME").ToString()
            objClaimRetirement._IsWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function GetP2PToken() As String
        Dim mstrToken As String = ""
        Dim mstrError As String = ""
        Dim mstrGetData As String = ""
        Try
            If Session("GetTokenP2PServiceURL") IsNot Nothing AndAlso Session("GetTokenP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim objMstData As New clsMasterData
                Dim objCRUser As New clsCRP2PUserDetail
                If objMstData.GetSetP2PWebRequest(Session("GetTokenP2PServiceURL").ToString().Trim(), True, True, objCRUser.username, mstrGetData, mstrError, objCRUser, "", "") = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Return mstrToken
                End If
                objCRUser = Nothing

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mstrToken = dtTable.Rows(0)("token").ToString()
                    End If
                End If
                objMstData = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mstrToken
    End Function

    Private Function UpdateDeleteAttachment(ByVal dr As DataRow, ByVal mblnDeleteAttachment As Boolean, ByVal mstrAUD As String) As Boolean
        Try
            If dr IsNot Nothing Then dr("AUD") = mstrAUD
            dr.AcceptChanges()

            If mblnDeleteAttachment Then
                Try
                    If dr IsNot Nothing Then File.Delete(dr("filepath").ToString())
                Catch ex As Exception
                End Try
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function


#Region "Scan Attachment"

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttachment Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Claim_Retirement
                dRow("scanattachrefid") = enScanAttactRefId.CLAIM_RETIREMENT

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("transactionunkid") = CInt(dgvRetirement.DataKeys(mintAttachmentIndex).Values("Claimretirementtranunkid"))
                dRow("transactionunkid") = CInt(dgvRetirement.DataKeys(mintRetirementRowIndex).Values("Claimretirementtranunkid"))
                'Pinkal (18-Mar-2021) -- End


                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("GUID") = dgvRetirement.DataKeys(mintAttachmentIndex).Values("GUID").ToString()
                dRow("GUID") = dgvRetirement.DataKeys(mintRetirementRowIndex).Values("GUID").ToString()
                'Pinkal (18-Mar-2021) -- End


                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))

                dRow("Documentype") = GetMimeType(strfullpath)
                Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)
                dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData


            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtAttachment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing

            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            'If CInt(dgvRetirement.DataKeys(mintAttachmentIndex).Values("Claimretirementtranunkid")) > 0 Then
            '    xRow = mdtFinalAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_RETIREMENT & "' AND transactionunkid = '" & CInt(dgvRetirement.DataKeys(mintAttachmentIndex).Values("Claimretirementtranunkid")) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'ElseIf CStr(dgvRetirement.DataKeys(mintAttachmentIndex).Values("GUID").ToString().Trim).Length > 0 Then
            '    xRow = mdtFinalAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_RETIREMENT & "' AND GUID = '" & CStr(dgvRetirement.DataKeys(mintAttachmentIndex).Values("GUID").ToString().Trim) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'End If

            If CInt(dgvRetirement.DataKeys(mintRetirementRowIndex).Values("Claimretirementtranunkid")) > 0 Then
                xRow = mdtFinalAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_RETIREMENT & "' AND transactionunkid = '" & CInt(dgvRetirement.DataKeys(mintRetirementRowIndex).Values("Claimretirementtranunkid")) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvRetirement.DataKeys(mintRetirementRowIndex).Values("GUID").ToString().Trim).Length > 0 Then
                xRow = mdtFinalAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_RETIREMENT & "' AND GUID = '" & CStr(dgvRetirement.DataKeys(mintRetirementRowIndex).Values("GUID").ToString().Trim) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If
            'Pinkal (18-Mar-2021) -- End

            If xRow.Length <= 0 Then
                mdtFinalAttachment.ImportRow(dr)
            Else
                mdtFinalAttachment.Rows.Remove(xRow(0))
                mdtFinalAttachment.ImportRow(dr)
            End If
            mdtFinalAttachment.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Claim Details"

    Private Sub FillClaimDetailCombo()
        Dim dsCombo As DataSet = Nothing
        Try

            Select Case CInt(Me.ViewState("mintExpenseCategoryID"))
                Case enExpenseType.EXP_LEAVE
                    dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, False)
                Case Else
                    dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            End Select

            With cboCDExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintExpeseTypeId.ToString()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Button Events"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            mblnIsEditClick = False

            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtRemark.Text.Trim.Length <= 0 Then
                    popup_RemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "You have not set your retirement remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                    popup_RemarkYesNo.Show()
                Else
                    AddRetirement()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            mblnIsEditClick = True

            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtRemark.Text.Trim.Length <= 0 Then
                    popup_RemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "You have not set your retirement remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                    popup_RemarkYesNo.Show()
                Else
                    EditRetirement()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveAndSubmit.Click
        Dim blnFlag As Boolean = False
        Dim mstrFolderName As String = ""
        Dim strFileName As String = ""
        Try
            If Is_Valid() = False Then Exit Sub


            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .

            Dim mblnIsSubmitted As Boolean = False
            If CType(sender, Button).ID.ToUpper() = btnSaveAndSubmit.ID.ToUpper() Then
                mblnIsSubmitted = True
            Else
                mblnIsSubmitted = False
            End If

            Call SetValue(mblnIsSubmitted)

            'Pinkal (28-Apr-2020) -- End

            If mdtFinalAttachment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_RETIREMENT) Select (p.Item("Name").ToString)).FirstOrDefault

                For Each dRow As DataRow In mdtFinalAttachment.Rows
                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then

                        strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If mintClaimRetirementId > 0 Then
                blnFlag = objClaimRetirement.Update(mdtImprestDetail, mdtFinalAttachment)
            Else
                blnFlag = objClaimRetirement.Insert(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), mdtImprestDetail, mdtFinalAttachment)
            End If

            If blnFlag = False And objClaimRetirement._Message <> "" Then
                DisplayMessage.DisplayMessage(objClaimRetirement._Message, Me)
                Exit Sub
            End If

            If mintClaimRetirementId <= 0 AndAlso mblnIsHRExpense = False AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then

                Dim mstrUserName As String = ""
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mstrUserName = Session("UserName").ToString()
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mstrUserName = Session("DisplayName").ToString()
                End If

                Dim objP2P As New clsClaimRetirementP2PIntegration
                objClaimRetirement.GenerateNewRequisitionRequestForP2P(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                                                   , CInt(Session("CompanyUnkId")), Session("UserAccessModeSetting").ToString() _
                                                                                                   , objClaimRetirement._Employeeunkid, txtClaimNo.Text.Trim(), objClaimRetirement._ClaimRetirementNo _
                                                                                                   , objClaimRetirement._Transactiondate, mstrUserName, mdtImprestDetail, mdtFinalAttachment, objP2P)

                Dim objMstData As New clsMasterData
                Dim mstrGetData As String = ""
                Dim mstrError As String = ""
                Dim mstrP2PToken As String = GetP2PToken()
                Dim mstrPostedData As String = ""

                If objMstData.GetSetP2PWebRequest(Session("NewRequisitionRequestP2PServiceURL").ToString().Trim(), True, True, "ClaimRetirement", mstrGetData, mstrError, objP2P, mstrPostedData, mstrToken:=mstrP2PToken) = False Then

                    If mdtFinalAttachment IsNot Nothing AndAlso mdtFinalAttachment.Rows.Count > 0 Then
                        Dim dr As List(Of DataRow) = mdtFinalAttachment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").ToList()
                        If dr.Count > 0 Then
                            dr.ForEach(Function(x) UpdateDeleteAttachment(x, False, "D"))
                        End If
                    End If

                    objClaimRetirement._Isvoid = True
                    objClaimRetirement._Voiduserunkid = CInt(Session("userunkid"))
                    objClaimRetirement._Voidloginemployeeunkid = CInt(Session("employeeunkid"))
                    objClaimRetirement._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objClaimRetirement._Voidreason = "Voided due to connection fail or error from P2P system."

                    objClaimRetirement._FormName = mstrModuleName
                    objClaimRetirement._WebClientIP = Session("IP_ADD").ToString()
                    objClaimRetirement._WebHostName = Session("HOST_NAME").ToString()
                    objClaimRetirement._IsWeb = True

                    If objClaimRetirement.Delete(objClaimRetirement._Claimretirementunkid, mdtImprestDetail, mdtAttachment) = False Then
                        DisplayMessage.DisplayMessage(objClaimRetirement._Message, Me)
                        Exit Sub
                    End If

                    '/* END TO DELETE WHOLE CLAIM RETIREMENT WHEN P2P REQUEST IS NOT SUCCEED.
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Exit Sub

                Else
                    '/* START TO UPDATE CLAIM RETIREMENT P2P RESPONSE IN CLAIM RETIREMENT MASTER.
                    'mstrGetData = "{""requisitionId"": ""NMBHR-011118-000012"",""message"": ""New requisition details saved successfully."",""status"": 200,""timestamp"": ""01-Nov-2018 12:58:59 PM""}"
                    If mstrGetData.Trim.Length > 0 Then
                        Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                        If objClaimRetirement.UpdateP2PResponseToClaimRetirement(dtTable, objClaimRetirement._Claimretirementunkid, CInt(Session("UserId")), mstrPostedData) = False Then
                            DisplayMessage.DisplayMessage(objClaimRetirement._Message, Me)
                            Exit Sub
                        End If
                    End If
                    '/* END TO UPDATE CLAIM RETIREMENT P2P RESPONSE IN CLAIM RETIREMENT MASTER.
                End If
                objMstData = Nothing
            End If


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            If mblnIsSubmitted Then
                objClaimRetirement.SendSubmissionEmailToEmployee(objClaimRetirement._Employeeunkid, objClaimRetirement._ClaimRetirementNo, txtClaimNo.Text.Trim(), Session("EmployeeAsOnDate").ToString() _
                                                                                             , CInt(Session("CompanyUnkId")), CInt(IIf(CInt(Session("Employeeunkid")) <= 0, enLogin_Mode.EMP_SELF_SERVICE, enLogin_Mode.MGR_SELF_SERVICE)) _
                                                                                             , CInt(IIf(CInt(Session("Employeeunkid")) > 0, CInt(Session("Employeeunkid")), 0)), CInt(Session("UserId")))
            End If
            'Pinkal (10-Feb-2021) -- End


            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Claim Retirement saved successfully."), Me.Page, Session("rootpath").ToString & "Claim_Retirement/wPg_ClaimRetirementList.aspx")


            Session.Remove("ClaimRetirementId")
            Session.Remove("ClaimRequestId")
            Session.Remove("CREmpId")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Try
            Dim iRow As DataRow() = Nothing

            If mintClaimRetirementId > 0 Then
                If popup_DeleteReason.Reason.Trim.Length > 0 Then
                    iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & mintClaimretirementtranunkid & "' AND AUD <> 'D'")
                    iRow(0).Item("isvoid") = True
                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = popup_DeleteReason.Reason.Trim()
                    iRow(0).Item("AUD") = "D"

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        iRow(0).Item("voiduserunkid") = CInt(Session("UserId"))
                        iRow(0).Item("voidloginemployeeunkid") = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        iRow(0).Item("voiduserunkid") = -1
                        iRow(0).Item("voidloginemployeeunkid") = Session("Employeeunkid")
                    End If

                    iRow(0).AcceptChanges()

                    Dim dRow() As DataRow = Nothing
                    If mintClaimretirementtranunkid > 0 Then
                        dRow = mdtFinalAttachment.Select("transactionunkid = '" & mintClaimretirementtranunkid & "' AND AUD <> 'D'")
                    End If
                    If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                        For Each dtRow As DataRow In dRow
                            dtRow.Item("AUD") = "D"
                        Next
                    End If
                    mdtFinalAttachment.AcceptChanges()

                    FillImprestDetail()
                    Clear_Controls()
                    btnEdit.Visible = False : btnAdd.Visible = True
                    cboImprest.Enabled = True
                    iRow = Nothing
                Else
                    popup_DeleteReason.Show()
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Expense Delete process fail.", Me)
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboImprest.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session.Remove("ClaimRetirementId")
            Session.Remove("ClaimRequestId")
            Session.Remove("CREmpId")
            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            'Response.Redirect("wPg_ClaimRetirementList.aspx")
            Response.Redirect(Session("rootpath").ToString & "Claim_Retirement/wPg_ClaimRetirementList.aspx", False)
            'Pinkal (07-Nov-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewClaimDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewClaimDetail.Click
        Try

            Dim objClaimRequestTran As New clsclaim_request_tran
            objClaimRequestTran._ClaimRequestMasterId = mintClaimRequestMstId
            Dim mdtClaimRequest As DataTable = objClaimRequestTran._DataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isimprest") = True).ToList().CopyToDataTable()
            dgvClaimData.DataSource = mdtClaimRequest
            dgvClaimData.DataBind()
            objClaimRequestTran = Nothing

            If mdtClaimRequest IsNot Nothing AndAlso mdtClaimRequest.Rows.Count > 0 Then
                If mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCDClaimCurrency.Enabled = False
                    cboCDClaimCurrency.SelectedValue = CStr(mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                End If
                txtClaimGrandTotal.Text = CDec(mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("amount"))).ToString(Session("fmtcurrency").ToString())
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, dtAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
            objDependant = Nothing

            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()

            blnClaimDetailPopup = True
            popupClaimDetail.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If txtRemark.Text.Trim.Length <= 0 Then
                popup_RemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "You have not set your retirement remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                popup_RemarkYesNo.Show()
            Else
                If mblnIsEditClick Then
                    EditRetirement()
                Else
                    AddRetirement()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_RemarkYesNo.buttonYes_Click
        Try
            If mblnIsEditClick Then
                EditRetirement()
            Else
                AddRetirement()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#Region "Scan Attachment Button"

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End


                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtAttachment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            mdtAttachment.Select("").ToList().ForEach(Function(x) Add_DataRow(x, mstrModuleName))
            mblnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            mblnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintAttachmentIndex >= 0 Then
                mdtAttachment.Rows(mintAttachmentIndex)("AUD") = "D"
                mdtAttachment.AcceptChanges()
                mintAttachmentIndex = 0
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If mintAttachmentIndex > 0 Then
                mintAttachmentIndex = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Claim Details Button"

    Protected Sub btnCDCloseClaimDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCDCloseClaimDetail.Click
        Try
            blnClaimDetailPopup = False
            popupClaimDetail.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Combobox Event"

    Protected Sub cboImprest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboImprest.SelectedIndexChanged
        Try
            If CInt(cboImprest.SelectedValue) > 0 Then
                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboImprest.SelectedValue)
                cboCostCenter.SelectedValue = "0"

                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If

                Dim objclaim As New clsclaim_request_master

                'Pinkal (24-Oct-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                If CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.General_Retirement Then
                    mdecFinalCRapprovedAmount = objclaim.GetFinalApproverApprovedAmount(CInt(cboEmployee.SelectedValue), mintExpeseTypeId, mintClaimRequestMstId, True, CInt(cboImprest.SelectedValue))
                ElseIf CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.ExpenseWise_Retirement Then
                    mdecFinalCRapprovedAmount = objclaim.GetFinalApproverApprovedAmount(CInt(cboEmployee.SelectedValue), mintExpeseTypeId, mintClaimRequestMstId, True, objExpMaster._LinkedExpenseId)
                End If
                'Pinkal (24-Oct-2019) -- End


                objclaim = Nothing

                If mdtImprestDetail Is Nothing OrElse mdtImprestDetail.Rows.Count <= 0 Then
                    txtBalance.Text = mdecFinalCRapprovedAmount.ToString(Session("fmtcurrency").ToString())
                    mdecExpenseBalance = mdecFinalCRapprovedAmount
                Else
                    Dim mdecAmount As Decimal = mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("expenseunkid") = CInt(cboImprest.SelectedValue)).Sum(Function(x) x.Field(Of Decimal)("amount"))
                    If mdecAmount <= 0 Then mdecExpenseBalance = mdecFinalCRapprovedAmount

                    If mintClaimRetirementId > 0 Then
                        txtBalance.Text = CDec(mdecFinalCRapprovedAmount - mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("expenseunkid") = CInt(cboImprest.SelectedValue)).Sum(Function(x) x.Field(Of Decimal)("amount"))).ToString(Session("fmtcurrency").ToString())
                    Else
                        txtBalance.Text = mdecExpenseBalance.ToString(Session("fmtcurrency").ToString())
                    End If
                End If

                If mdtImprestDetail IsNot Nothing Then
                    If mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = CStr(mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            Dim mstrSearch As String = ""

            If CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.General_Retirement Then

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) <= 0"
                mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) <= 0 AND ISNULL(cmexpense_master.expenseunkid,0) in (" & mstrExpenseIDs & ")"
                'Pinkal (10-Feb-2021) -- End
            ElseIf CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.ExpenseWise_Retirement Then
                mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) > 0 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) in (" & mstrExpenseIDs & ")"
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                dsCombo = objExpMaster.getComboList(enExpenseType.EXP_IMPREST, True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, mstrSearch)
            Else
                dsCombo = objExpMaster.getComboList(enExpenseType.EXP_IMPREST, True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, mstrSearch)
            End If

            With cboImprest
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (24-Oct-2019) -- End

#End Region

#Region "Textbox Events"

    Protected Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged, txtUnitPrice.TextChanged
        Try

            If IsNumeric(txtQuantity.Text) = False Then
                txtQuantity.Text = "1.00"
                Exit Sub
            End If

            If IsNumeric(txtUnitPrice.Text) = False Then
                txtUnitPrice.Text = "1.00"
                Exit Sub
            End If

            txtAmount.Text = CDec(CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)).ToString(Session("fmtcurrency").ToString())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Gridview Events"

#Region "Retirement "

    Protected Sub dgvRetirement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvRetirement.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvRetirement_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgvRetirement.RowUpdating
        Try
            If e.RowIndex >= 0 Then

                Dim iRow As DataRow() = Nothing

                If CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid").ToString()) > 0 Then
                    iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid").ToString()) & "' AND AUD <> 'D'")
                ElseIf dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Trim().Length > 0 Then
                    iRow = mdtImprestDetail.Select("GUID = '" & dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Trim() & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboImprest.SelectedValue = iRow(0).Item("expenseunkid").ToString
                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    Call cboImprest_SelectedIndexChanged(cboImprest, New EventArgs())

                    txtQuantity.Text = CDec(iRow(0).Item("quantity")).ToString(Session("fmtcurrency").ToString())
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString())).ToString()
                    txtAmount.Text = CDec(CDec(iRow(0).Item("quantity")) * CDec(iRow(0).Item("unitprice"))).ToString(Session("fmtCurrency").ToString())
                    'txtBalance.Text = mdecExpenseBalance.ToString(Session("fmtCurrency").ToString())
                    txtRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboImprest.Enabled = False
                    mdecEditOldExpenseAmt = CDec(CDec(iRow(0).Item("quantity")) * CDec(iRow(0).Item("unitprice")))
                End If

                mintEditRowIndex = e.RowIndex
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvRetirement_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgvRetirement.RowDeleting
        Try
            Dim iRow As DataRow() = Nothing
            If CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid")) > 0 Then
                iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid")) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    mintClaimretirementtranunkid = CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid"))
                    popup_DeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Are you sure you want to delete?")
                    popup_DeleteReason.Reason = ""
                    popup_DeleteReason.Show()
                End If

            ElseIf dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Length > 0 Then
                iRow = mdtImprestDetail.Select("GUID = '" & dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString() & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                    mdecExpenseBalance = mdecExpenseBalance + CDec(iRow(0).Item("amount"))
                    UpdateBalance(e.RowIndex, CDec(iRow(0).Item("amount")))
                End If
            End If

            Dim dRow() As DataRow = Nothing
            If dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Trim.Length > 0 Then
                dRow = mdtFinalAttachment.Select("GUID = '" & dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString() & "' AND AUD <> 'D'")
            End If
            If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                For Each dtRow As DataRow In dRow
                    dtRow.Item("AUD") = "D"
                Next
            End If
            mdtFinalAttachment.AcceptChanges()


            iRow(0).AcceptChanges()
            FillImprestDetail()
            Clear_Controls()
            btnEdit.Visible = False : btnAdd.Visible = True
            cboImprest.Enabled = True
            iRow = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvRetirement_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvRetirement.RowCommand
        Try
            If e.CommandName = "attachment" Then
                mdtAttachment = mdtFinalAttachment.Clone
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim xRow() As DataRow = Nothing

                If CInt(dgvRetirement.DataKeys(gridRow.RowIndex).Values("Claimretirementtranunkid").ToString()) > 0 Then
                    xRow = mdtFinalAttachment.Select("transactionunkid = '" & CInt(dgvRetirement.DataKeys(gridRow.RowIndex).Values("Claimretirementtranunkid").ToString()) & "' AND AUD <> 'D'")
                ElseIf dgvRetirement.DataKeys(gridRow.RowIndex).Values("GUID").ToString().Trim().Length > 0 Then
                    xRow = mdtFinalAttachment.Select("GUID = '" & dgvRetirement.DataKeys(gridRow.RowIndex).Values("GUID").ToString().Trim() & "' AND AUD <> 'D'")
                End If

                If xRow.Count > 0 Then
                    mdtAttachment = xRow.CopyToDataTable
                End If


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'mintAttachmentIndex = gridRow.RowIndex
                mintRetirementRowIndex = gridRow.RowIndex
                'Pinkal (18-Mar-2021) -- End

                Call FillAttachment()
                mblnShowAttchmentPopup = True
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Attachment"

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.CommandName = "Delete" Then
                    popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Are you sure you want to delete this attachment?")
                    mintAttachmentIndex = e.Item.ItemIndex
                    popup_YesNo.Show()

                ElseIf e.CommandName = "Download" Then
                    Dim xrow() As DataRow = Nothing

                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                        xrow = mdtAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & " AND AUD <> 'D'")
                    Else
                        xrow = mdtAttachment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "' AND AUD <> 'D' ")
                    End If

                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Claim Detail"

    Protected Sub dgvClaimData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvClaimData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(3).Text.Trim.Length > 0 Then  'Quantity
                    e.Item.Cells(3).Text = Format(CDec(e.Item.Cells(3).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(4).Text.Trim.Length > 0 Then  'Unit price
                    e.Item.Cells(4).Text = Format(CDec(e.Item.Cells(4).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(5).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency").ToString())
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region


    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblclaimRetirementNo.ID, Me.LblclaimRetirementNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblClaimNo.ID, Me.lblClaimNo.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprestAmount.ID, Me.lblImprestAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprest.ID, Me.lblImprest.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblBalance.ID, Me.lblBalance.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQuantity.ID, Me.lblQuantity.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRemark.ID, Me.lblRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblUnitprice.ID, Me.LblUnitprice.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAmount.ID, Me.lblAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCurrency.ID, Me.lblCurrency.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRetirementGrandTotal.ID, Me.LblRetirementGrandTotal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblBalanceDue.ID, Me.LblBalanceDue.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAdd.ID, Me.btnAdd.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnEdit.ID, Me.btnEdit.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSave.ID, Me.btnSave.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveAndSubmit.ID, Me.btnSaveAndSubmit.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(3).FooterText, Me.dgvRetirement.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(4).FooterText, Me.dgvRetirement.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(5).FooterText, Me.dgvRetirement.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(6).FooterText, Me.dgvRetirement.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(7).FooterText, Me.dgvRetirement.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(8).FooterText, Me.dgvRetirement.Columns(8).HeaderText)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, LblClaimDetails.ID, Me.LblClaimDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDExpCategory.ID, Me.lblCDExpCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lnkViewDependantList.ID, Me.lnkViewDependantList.Text)
            ' Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkViewDependantList.ID, Me.lnkViewDependantList.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimDate.ID, Me.lblCDClaimDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimEmployee.ID, Me.lblCDClaimEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimDomicileAddress.ID, Me.lblCDClaimDomicileAddress.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimExpense.ID, Me.lblCDClaimExpense.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimCurrency.ID, Me.lblCDClaimCurrency.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCDClaimCostCenter.ID, Me.lblCDClaimCostCenter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblCDClaimRemark.ID, Me.LblCDClaimRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblClaimGrandTotal.ID, Me.LblClaimGrandTotal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCDCloseClaimDetail.ID, Me.btnCDCloseClaimDetail.Text.Trim.Replace("&", ""))

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(0).FooterText, Me.dgvClaimData.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(1).FooterText, Me.dgvClaimData.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(2).FooterText, Me.dgvClaimData.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(3).FooterText, Me.dgvClaimData.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(4).FooterText, Me.dgvClaimData.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(5).FooterText, Me.dgvClaimData.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvClaimData.Columns(6).FooterText, Me.dgvClaimData.Columns(6).HeaderText)


            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, mstrModuleName1, Me.LblEmpDependentsList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgDepedent.Columns(0).FooterText, Me.dgDepedent.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgDepedent.Columns(1).FooterText, Me.dgDepedent.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgDepedent.Columns(2).FooterText, Me.dgDepedent.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgDepedent.Columns(3).FooterText, Me.dgDepedent.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgDepedent.Columns(4).FooterText, Me.dgDepedent.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, "btnClose", Me.btnCloseDependantList.Text.Replace("&", ""))


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblclaimRetirementNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblclaimRetirementNo.ID, Me.LblclaimRetirementNo.Text)
            Me.lblClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkViewClaimDetail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.Text)
            Me.lnkViewClaimDetail.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Me.lblImprestAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprestAmount.ID, Me.lblImprestAmount.Text)
            Me.lblImprest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprest.ID, Me.lblImprest.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Me.lblQuantity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQuantity.ID, Me.lblQuantity.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemark.ID, Me.lblRemark.Text)
            Me.LblUnitprice.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblUnitprice.ID, Me.LblUnitprice.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.LblRetirementGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRetirementGrandTotal.ID, Me.LblRetirementGrandTotal.Text)
            Me.LblBalanceDue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblBalanceDue.ID, Me.LblBalanceDue.Text)

            Me.btnAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdd.ID, Me.btnAdd.Text.Trim.Replace("&", ""))
            Me.btnEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnEdit.ID, Me.btnEdit.Text.Trim.Replace("&", ""))
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text.Trim.Replace("&", ""))
            Me.btnSaveAndSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveAndSubmit.ID, Me.btnSaveAndSubmit.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Me.dgvRetirement.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(3).FooterText, Me.dgvRetirement.Columns(3).HeaderText)
            Me.dgvRetirement.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(4).FooterText, Me.dgvRetirement.Columns(4).HeaderText)
            Me.dgvRetirement.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(5).FooterText, Me.dgvRetirement.Columns(5).HeaderText)
            Me.dgvRetirement.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(6).FooterText, Me.dgvRetirement.Columns(6).HeaderText)
            Me.dgvRetirement.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(7).FooterText, Me.dgvRetirement.Columns(7).HeaderText)
            Me.dgvRetirement.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(8).FooterText, Me.dgvRetirement.Columns(8).HeaderText)


            Me.LblClaimDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblClaimDetails.ID, Me.LblClaimDetails.Text)
            Me.lblCDExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDExpCategory.ID, Me.lblCDExpCategory.Text)
            Me.lblCDClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkViewDependantList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkViewDependantList.ID, Me.lnkViewDependantList.Text)
            Me.lnkViewDependantList.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkViewDependantList.ID, Me.lnkViewDependantList.ToolTip)
            btnSaveAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text).Replace("&", "")
            btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text).Replace("&", "")
            'Gajanan [17-Sep-2020] -- End

            Me.lblCDClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)
            Me.lblCDClaimDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimDate.ID, Me.lblCDClaimDate.Text)
            Me.lblCDClaimEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimEmployee.ID, Me.lblCDClaimEmployee.Text)
            Me.lblCDClaimDomicileAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimDomicileAddress.ID, Me.lblCDClaimDomicileAddress.Text)
            Me.lblCDClaimExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimExpense.ID, Me.lblCDClaimExpense.Text)
            Me.lblCDClaimCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimCurrency.ID, Me.lblCDClaimCurrency.Text)
            Me.lblCDClaimCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimCostCenter.ID, Me.lblCDClaimCostCenter.Text)
            Me.LblCDClaimRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblCDClaimRemark.ID, Me.LblCDClaimRemark.Text)
            Me.LblClaimGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblClaimGrandTotal.ID, Me.LblClaimGrandTotal.Text)
            Me.btnCDCloseClaimDetail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCDCloseClaimDetail.ID, Me.btnCDCloseClaimDetail.Text.Trim.Replace("&", ""))


            Me.dgvClaimData.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(0).FooterText, Me.dgvClaimData.Columns(0).HeaderText)
            Me.dgvClaimData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(1).FooterText, Me.dgvClaimData.Columns(1).HeaderText)
            Me.dgvClaimData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(2).FooterText, Me.dgvClaimData.Columns(2).HeaderText)
            Me.dgvClaimData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(3).FooterText, Me.dgvClaimData.Columns(3).HeaderText)
            Me.dgvClaimData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(4).FooterText, Me.dgvClaimData.Columns(4).HeaderText)
            Me.dgvClaimData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(5).FooterText, Me.dgvClaimData.Columns(5).HeaderText)
            Me.dgvClaimData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(6).FooterText, Me.dgvClaimData.Columns(6).HeaderText)

            'Language.setLanguage(mstrModuleName1)
            Me.LblEmpDependentsList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.LblEmpDependentsList.Text)
            Me.dgDepedent.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(0).FooterText, Me.dgDepedent.Columns(0).HeaderText)
            Me.dgDepedent.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(1).FooterText, Me.dgDepedent.Columns(1).HeaderText)
            Me.dgDepedent.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(2).FooterText, Me.dgDepedent.Columns(2).HeaderText)
            Me.dgDepedent.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(3).FooterText, Me.dgDepedent.Columns(3).HeaderText)
            Me.dgDepedent.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(4).FooterText, Me.dgDepedent.Columns(4).HeaderText)
            Me.btnCloseDependantList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnCloseDependantList.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Retirement No. is mandatory information. Please select Retirement No. to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Employee is mandatory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Imprest is mandatory information. Please select Imprest to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Currency is mandatory information. Please select Currency to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Please add atleast one expense retirement in order to save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Please Assign Expense Approver to this employee and also map retirement Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, you cannot add same Imprest again in the below list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Configuration Path does not Exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "You have not set your retirement remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Claim Retirement saved successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
