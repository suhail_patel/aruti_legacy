﻿Imports System.Data
Imports Aruti.Data

Partial Class Controls_CommonValidationList
    Inherits System.Web.UI.UserControl

    Public Event buttonOk_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    Private mblnShowYesNo As Boolean = False
    Private mstrMessage As String = ""
    Private mdtTable As DataTable = Nothing
#End Region

#Region " Properties "
    'Public Property Title() As String
    '    Get
    '        Return lblTitle.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblTitle.Text = value
    '    End Set
    'End Property

    Public Property Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property

    Public Property ShowYesNo() As Boolean
        Get
            Return mblnShowYesNo
        End Get
        Set(ByVal value As Boolean)
            mblnShowYesNo = value
        End Set
    End Property

    Public Property Data_Table() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

#End Region

#Region " Method & Function "

    Public Sub Show()
        Try
        If mstrMessage.Trim <> "" Then lblTitle.Text = "  " & mstrMessage
        gvDetails.AutoGenerateColumns = True
        gvDetails.DataSource = mdtTable
        gvDetails.DataBind()
        ModalPopupExtender1.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        ModalPopupExtender1.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

#End Region

#Region " Pages Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then

                If mstrMessage.Trim <> "" Then lblTitle.Text = "  " & mstrMessage

                If mblnShowYesNo = True Then
                    pnlOkCancel.Visible = False
                    pnlYesNo.Visible = True
                Else
                    pnlOkCancel.Visible = True
                    pnlYesNo.Visible = False
                End If

            Else

            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " Buttons Events "
    Protected Sub btnCValidationCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCValidationCancel.Click
        Try

            RaiseEvent buttonCancel_Click(sender, e)
            ModalPopupExtender1.Dispose()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnCValidationCancel :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnCValidationNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCValidationNo.Click
        Try

            RaiseEvent buttonNo_Click(sender, e)
            ModalPopupExtender1.Dispose()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnCValidationNo :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnCValidationOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCValidationOk.Click
        Try

            RaiseEvent buttonOk_Click(sender, e)
            ModalPopupExtender1.Dispose()

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnCValidationOk_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnCValidationYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCValidationYes.Click
        Try

            RaiseEvent buttonYes_Click(sender, e)
            ModalPopupExtender1.Dispose()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnCValidationYes_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " Gridview Events "

    
    'Protected Sub gvDetails_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails.DataBound
    '    Try
    '        'Dim lstCols As List(Of DataColumn) = (From p In CType(gvDetails.DataSource, DataTable).Columns Where (CType(p, DataColumn).DataType Is System.Type.GetType("System.Decimal") OrElse CType(p, DataColumn).DataType Is System.Type.GetType("System.Int32")) Select (CType(p, DataColumn))).ToList
    '        'For Each col In lstCols
    '        '    gvDetails.Columns(col.Ordinal - 1).ItemStyle.HorizontalAlign = HorizontalAlign.Right
    '        'Next

    '        ''If dgvList.ColumnCount > 0 Then dgvList.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    '    Catch ex As Exception
    '        Throw New Exception("gvDetails_DataBound :- " & ex.Message)
    '    End Try
    'End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lstCols As List(Of DataColumn) = (From p In mdtTable.Columns Where (CType(p, DataColumn).DataType Is System.Type.GetType("System.Decimal") OrElse CType(p, DataColumn).DataType Is System.Type.GetType("System.Int32")) Select (CType(p, DataColumn))).ToList
                For Each col In lstCols
                    e.Row.Cells(col.Ordinal).HorizontalAlign = HorizontalAlign.Right
                    e.Row.Cells(col.Ordinal).Text = Format(CDec(e.Row.Cells(col.Ordinal).Text), Session("fmtCurrency").ToString)
                Next

                'If dgvList.ColumnCount > 0 Then dgvList.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End If
            
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvDetails_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
#End Region

    
End Class
