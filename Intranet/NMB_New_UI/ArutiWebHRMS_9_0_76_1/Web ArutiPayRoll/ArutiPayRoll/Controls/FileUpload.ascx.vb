﻿
Partial Class Controls_FileUpload
    Inherits System.Web.UI.UserControl

    Public Event btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Public Default Properties "

    Public ReadOnly Property FileName() As String
        Get
            Return image_file.FileName
        End Get
    End Property

    Public ReadOnly Property PostedFile() As HttpPostedFile
        Get
            Return image_file.PostedFile
        End Get
    End Property

    Public Property Enabled() As Boolean
        Get
            Return image_file.Enabled
        End Get
        Set(ByVal value As Boolean)
            image_file.Enabled = value
        End Set
    End Property
#End Region

#Region " Public Custom Properties "

    'Protected mstrFileName As String
    'Public ReadOnly Property FileName() As String
    '    Get
    '        Return mstrFileName
    '    End Get
    'End Property

    Public ReadOnly Property HasFile() As Boolean
        Get
            Return image_file.HasFile
        End Get
    End Property

    Public ReadOnly Property FileBytes() As Byte()
        Get
            Return image_file.FileBytes
        End Get
    End Property

    Public WriteOnly Property OnClick() As String
        Set(ByVal value As String)
            image_file.Attributes.Add("OnClick", value)
        End Set
    End Property

#End Region

#Region " Public Default Methods "
    Public Sub SaveAs(ByVal filename As String)
        image_file.PostedFile.SaveAs(filename)
    End Sub

#End Region

    Protected Sub bttnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        RaiseEvent btnUpload_Click(sender, e)
    End Sub

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
    'Me.Page.Form.Enctype = "multipart/form-data"
    'End Sub
End Class
