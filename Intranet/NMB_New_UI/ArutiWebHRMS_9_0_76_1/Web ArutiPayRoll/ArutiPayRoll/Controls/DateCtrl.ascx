﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DateCtrl.ascx.vb" Inherits="Controls_DateCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="form-group datepicker">
    <div class="form-line" style="position: relative">
        <asp:TextBox runat="server" ID="TxtDate" AutoPostBack="True" CssClass="form-control"
            placeholder="Please choose a date..." />
        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" TargetControlID="TxtDate"
             PopupPosition="BottomLeft" Enabled="True" CssClass="cal_Theme1" />
    </div>
    
    <asp:ImageButton runat="Server" ID="objbtnDateReminder" ImageUrl="~/images/Reminder_32.png"
        Width="25px" Height="25px" ImageAlign="Top" Visible="false" CssClass="input-group-addon" />
</div>
