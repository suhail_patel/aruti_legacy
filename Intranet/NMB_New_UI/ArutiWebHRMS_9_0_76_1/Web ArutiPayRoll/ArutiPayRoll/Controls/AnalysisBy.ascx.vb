﻿Imports System.Data
Imports Aruti.Data

Partial Class Controls_AnalysisBy
    Inherits System.Web.UI.UserControl

    Public Event buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmViewAnalysis" 'Sohail (23 May 2017)

    Private mstrReportBy_Ids As String = String.Empty
    Private mstrReportBy_Name As String = String.Empty

    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrHr_EmployeeTable_Alias As String = "hremployee_master"
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    'Sohail (23 May 2017) -- Start
    'Enhancement - 67.1 - Link budget with Payroll.
    Private mdtEffectiveDate As Date = Nothing
    Private mstrAnalysis_CodeField As String = String.Empty
    'Sohail (23 May 2017) -- End

    Private dtTable As DataTable
#End Region

#Region " Properties "

    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return lblMessage.Text
        End Get
        Set(ByVal value As String)
            lblMessage.Text = value
        End Set
    End Property

    Public Property _ReportBy_Ids() As String
        Get
            Return mstrReportBy_Ids
        End Get
        Set(ByVal value As String)
            mstrReportBy_Ids = value
        End Set
    End Property

    Public Property _ReportBy_Name() As String
        Get
            Return mstrReportBy_Name
        End Get
        Set(ByVal value As String)
            mstrReportBy_Name = value
        End Set
    End Property

    Public ReadOnly Property _ViewIndex() As Integer
        Get
            Return mintViewIndex
        End Get
    End Property

    Public ReadOnly Property _Analysis_Fields() As String
        Get
            Return mstrAnalysis_Fields
        End Get
    End Property

    Public ReadOnly Property _Analysis_Join() As String
        Get
            Return mstrAnalysis_Join
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy() As String
        Get
            Return mstrAnalysis_OrderBy
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy_GName() As String
        Get
            Return mstrAnalysis_OrderBy_GroupName
        End Get
    End Property

    Public ReadOnly Property _Report_GroupName() As String
        Get
            Return mstrReport_GroupName
        End Get
    End Property

    Public ReadOnly Property _Analysis_TableName() As String
        Get
            Return mstrAnalysis_TableName
        End Get
    End Property

    Public WriteOnly Property _Hr_EmployeeTable_Alias() As String
        Set(ByVal value As String)
            mstrHr_EmployeeTable_Alias = value
        End Set
    End Property

    Public WriteOnly Property _EffectiveDate() As Date
        Set(ByVal value As Date)
            mdtEffectiveDate = value
        End Set
    End Property

    Public ReadOnly Property _Analysis_CodeField() As String
        Get
            Return mstrAnalysis_CodeField
        End Get
    End Property

#End Region

#Region " Methods & Functions "

    Private Sub CreateTable()
        Try
            dtTable = New DataTable("List")
            dtTable.Columns.Add("IsCheck", Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""

            'Call TabContainer1_ActiveTabChanged(TabContainer1, New System.EventArgs)

            SetViewIndex()

            popupAnalysisBy.Hide()

            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                dtTable.Rows.Add(r)
            End If

            gvDetails.DataSource = dtTable
            gvDetails.DataBind()

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Public Sub Show()
        Try
            Call Reset()
            If mdtEffectiveDate = Nothing Then mdtEffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            popupAnalysisBy.Show()
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Public Sub Hide()
        Try
            popupAnalysisBy.Hide()
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Private Sub Reset()
        Try
            Call CreateTable()
            mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            'mstrHr_EmployeeTable_Alias = "hremployee_master"
            mstrReport_GroupName = ""
            mstrAnalysis_TableName = " "
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Public Shared Function GetAnalysisByDetails(ByVal strViewAnalysisModuleName As String _
                                                , ByVal intViewIndex As Integer _
                                                , ByVal strReportBy_Ids As String _
                                                , ByVal dtEffectiveDate As Date _
                                                , Optional ByVal strHr_EmployeeTable_Alias As String = "" _
                                                , Optional ByRef stroutAnalysis_Fields As String = "" _
                                                , Optional ByRef stroutAnalysis_Join As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy_GroupName As String = "" _
                                                , Optional ByRef stroutReport_GroupName As String = "" _
                                                , Optional ByRef stroutAnalysis_TableName As String = "" _
                                                , Optional ByRef stroutAnalysis_CodeField As String = "" _
                                                ) As Boolean
        Try



            Dim strAllocationQry, strJobQry, strCCQry, strSalQry As String
            strAllocationQry = "" : strJobQry = "" : strCCQry = "" : strSalQry = ""

            strAllocationQry = "SELECT " & _
                               "     stationunkid " & _
                               "    ,deptgroupunkid " & _
                               "    ,departmentunkid " & _
                               "    ,sectiongroupunkid " & _
                               "    ,sectionunkid " & _
                               "    ,unitgroupunkid " & _
                               "    ,unitunkid " & _
                               "    ,teamunkid " & _
                               "    ,classgroupunkid " & _
                               "    ,classunkid " & _
                               "    ,employeeunkid " & _
                               "FROM " & _
                               "( " & _
                               "        SELECT " & _
                               "             stationunkid " & _
                               "            ,deptgroupunkid " & _
                               "            ,departmentunkid " & _
                               "            ,sectiongroupunkid " & _
                               "            ,sectionunkid " & _
                               "            ,unitgroupunkid " & _
                               "            ,unitunkid " & _
                               "            ,teamunkid " & _
                               "            ,classgroupunkid " & _
                               "            ,classunkid " & _
                               "            ,employeeunkid " & _
                               "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                               "        FROM hremployee_transfer_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                               ") AS A WHERE A.rno = 1 "

            strJobQry = "SELECT " & _
                        "    jobgroupunkid " & _
                        "   ,jobunkid " & _
                        "   ,employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "   FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                        ") AS A WHERE A.rno = 1 "

            strCCQry = "SELECT " & _
                       "     costcenterunkid " & _
                       "    ,employeeunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         cctranheadvalueid AS costcenterunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_cctranhead_tran WHERE isvoid = 0 AND istransactionhead = 0 " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                       ") AS A WHERE A.rno = 1 "

            strSalQry = "SELECT " & _
                        "    gradegroupunkid " & _
                        "   ,gradeunkid " & _
                        "   ,gradelevelunkid " & _
                        "   ,employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        gradegroupunkid " & _
                        "       ,gradeunkid " & _
                        "       ,gradelevelunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "   FROM prsalaryincrement_tran WHERE isvoid = 0 AND isapproved = 1 " & _
                        "       AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                        ") AS A WHERE A.rno = 1 "


            If strHr_EmployeeTable_Alias.Trim = "" Then strHr_EmployeeTable_Alias = "hremployee_master"

            Select Case intViewIndex

                Case enAnalysisReport.Branch

                    stroutAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrstation_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                   "(" & vbCrLf & _
                                       strAllocationQry & vbCrLf & _
                                   ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                   " JOIN hrstation_master ON hrstation_master.stationunkid = VB_TRF.stationunkid AND hrstation_master.stationunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrstation_master.stationunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrstation_master.name "
                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radBranch", "Branch") & " :"
                    stroutAnalysis_TableName = " hrstation_master "

                Case enAnalysisReport.Department
                    stroutAnalysis_Fields = ", hrdepartment_master.departmentunkid AS Id, hrdepartment_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrdepartment_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = VB_TRF.departmentunkid AND hrdepartment_master.departmentunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrdepartment_master.departmentunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrdepartment_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radDepartment", "Department") & " :"

                    stroutAnalysis_TableName = " hrdepartment_master "

                Case enAnalysisReport.Job
                    stroutAnalysis_Fields = ",  hrjob_master.jobunkid AS Id, hrjob_master.job_name AS GName "

                    stroutAnalysis_CodeField = ", hrjob_master.job_code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strJobQry & vbCrLf & _
                                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrjob_master ON hrjob_master.jobunkid = VB_RECAT.jobunkid AND hrjob_master.jobunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrjob_master.jobunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrjob_master.job_name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radJob", "Job") & " :"

                    stroutAnalysis_TableName = " hrjob_master "

                Case enAnalysisReport.Section
                    stroutAnalysis_Fields = ",  hrsection_master.sectionunkid AS Id, hrsection_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrsection_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrsection_master ON hrsection_master.sectionunkid = VB_TRF.sectionunkid AND hrsection_master.sectionunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrsection_master.sectionunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrsection_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radSection", "Section") & " :"

                    stroutAnalysis_TableName = " hrsection_master "

                Case enAnalysisReport.Unit
                    stroutAnalysis_Fields = ",  hrunit_master.unitunkid AS Id, hrunit_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrunit_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrunit_master ON hrunit_master.unitunkid = VB_TRF.unitunkid AND hrunit_master.unitunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrunit_master.unitunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrunit_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radUnit", "Unit") & " :"

                    stroutAnalysis_TableName = " hrunit_master "

                Case enAnalysisReport.CostCenter
                    stroutAnalysis_Fields = ", prcostcenter_master.costcenterunkid AS Id, prcostcenter_master.costcentername AS GName "

                    stroutAnalysis_CodeField = ", prcostcenter_master.costcentercode AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strCCQry & vbCrLf & _
                                        ") AS VB_CCR ON VB_CCR.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = VB_CCR.costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " prcostcenter_master.costcenterunkid "
                    stroutAnalysis_OrderBy_GroupName = " prcostcenter_master.costcentername "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radCostCenter", "Cost Center") & " :"

                    stroutAnalysis_TableName = " prcostcenter_master "

                Case enAnalysisReport.SectionGroup
                    stroutAnalysis_Fields = ", hrsectiongroup_master.sectiongroupunkid AS Id, hrsectiongroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrsectiongroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = VB_TRF.sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrsectiongroup_master.sectiongroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrsectiongroup_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radSectionGrp", "Section Group") & " :"

                    stroutAnalysis_TableName = " hrsectiongroup_master "

                Case enAnalysisReport.UnitGroup
                    stroutAnalysis_Fields = ", hrunitgroup_master.unitgroupunkid AS Id, hrunitgroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrunitgroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = VB_TRF.unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrunitgroup_master.unitgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrunitgroup_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radUnitGrp", "Unit Group") & " :"

                    stroutAnalysis_TableName = " hrunitgroup_master "

                Case enAnalysisReport.Team
                    stroutAnalysis_Fields = ", hrteam_master.teamunkid AS Id, hrteam_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrteam_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrteam_master ON hrteam_master.teamunkid = VB_TRF.teamunkid AND hrteam_master.teamunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrteam_master.teamunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrteam_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radTeam", "Team") & " :"

                    stroutAnalysis_TableName = " hrteam_master "

                Case enAnalysisReport.DepartmentGroup
                    stroutAnalysis_Fields = ", hrdepartment_group_master.deptgroupunkid AS Id, hrdepartment_group_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrdepartment_group_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = VB_TRF.deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrdepartment_group_master.deptgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrdepartment_group_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radDepartmentGrp", "Department Group") & " :"

                    stroutAnalysis_TableName = " hrdepartment_group_master "

                Case enAnalysisReport.JobGroup

                    stroutAnalysis_Fields = ", hrjobgroup_master.jobgroupunkid AS Id, hrjobgroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrjobgroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strJobQry & vbCrLf & _
                                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = VB_RECAT.jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrjobgroup_master.jobgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrjobgroup_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radJobGrp", "Job Group") & " :"

                    stroutAnalysis_TableName = " hrjobgroup_master "

                Case enAnalysisReport.ClassGroup
                    stroutAnalysis_Fields = ", hrclassgroup_master.classgroupunkid AS Id, hrclassgroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrclassgroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = VB_TRF.classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrclassgroup_master.classgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrclassgroup_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radClassGrp", "Class Group") & " :"

                    stroutAnalysis_TableName = " hrclassgroup_master "

                Case enAnalysisReport.Classs
                    stroutAnalysis_Fields = ", hrclasses_master.classesunkid AS Id, hrclasses_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrclasses_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrclasses_master ON hrclasses_master.classesunkid = VB_TRF.classunkid AND hrclasses_master.classesunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrclasses_master.classesunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrclasses_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radClass", "Class") & " :"

                    stroutAnalysis_TableName = " hrclasses_master "

                Case enAnalysisReport.GradeGroup
                    stroutAnalysis_Fields = ", hrgradegroup_master.gradegroupunkid AS Id, hrgradegroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrgradegroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                    "(" & vbCrLf & _
                                        strSalQry & vbCrLf & _
                                    ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                    " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = VB_SALI.gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgradegroup_master.gradegroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgradegroup_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radGradeGrp", "Grade Group") & " :"

                    stroutAnalysis_TableName = " hrgradegroup_master "

                Case enAnalysisReport.Grade
                    stroutAnalysis_Fields = ", hrgrade_master.gradeunkid AS Id, hrgrade_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrgrade_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strSalQry & vbCrLf & _
                                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = VB_SALI.gradeunkid AND hrgrade_master.gradeunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgrade_master.gradeunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgrade_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radGrade", "Grade") & " :"

                    stroutAnalysis_TableName = " hrgrade_master "

                Case enAnalysisReport.GradeLevel
                    stroutAnalysis_Fields = ", hrgradelevel_master.gradelevelunkid AS Id, hrgradelevel_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrgradelevel_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strSalQry & vbCrLf & _
                                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = VB_SALI.gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgradelevel_master.gradelevelunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgradelevel_master.name "

                    stroutReport_GroupName = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmViewAnalysis", CInt(HttpContext.Current.Session("LangId")), "radGradeLevel", "Grade Level") & " :"

                    stroutAnalysis_TableName = " hrgradelevel_master "

                Case Else
                    stroutAnalysis_Fields = ", 0 AS Id, '' AS GName "
                    stroutAnalysis_CodeField = ""
                    stroutAnalysis_Join = ""
                    stroutAnalysis_OrderBy = ""
                    stroutAnalysis_OrderBy_GroupName = ""
                    strHr_EmployeeTable_Alias = ""
                    stroutReport_GroupName = ""
                    stroutAnalysis_TableName = " "

            End Select

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Function

    Private Sub SetViewIndex()
        Try
            Select Case mintViewIndex

                Case mintViewIndex <= 0, enAnalysisReport.Branch
                    radBranch.Checked = True
                    radBranch_CheckedChanged(radBranch, New EventArgs())

                Case enAnalysisReport.DepartmentGroup
                    radDepartmentGrp.Checked = True
                    radBranch_CheckedChanged(radDepartmentGrp, New EventArgs())

                Case enAnalysisReport.Department
                    radDepartment.Checked = True
                    radBranch_CheckedChanged(radDepartment, New EventArgs())

                Case enAnalysisReport.SectionGroup
                    radSectionGrp.Checked = True
                    radBranch_CheckedChanged(radSectionGrp, New EventArgs())

                Case enAnalysisReport.Section
                    radSection.Checked = True
                    radBranch_CheckedChanged(radSection, New EventArgs())

                Case enAnalysisReport.UnitGroup
                    radUnitGrp.Checked = True
                    radBranch_CheckedChanged(radUnitGrp, New EventArgs())

                Case enAnalysisReport.Unit
                    radUnit.Checked = True
                    radBranch_CheckedChanged(radUnit, New EventArgs())

                Case enAnalysisReport.Team
                    radTeam.Checked = True
                    radBranch_CheckedChanged(radTeam, New EventArgs())

                Case enAnalysisReport.JobGroup
                    radJobGrp.Checked = True
                    radBranch_CheckedChanged(radJobGrp, New EventArgs())

                Case enAnalysisReport.Job
                    radJob.Checked = True
                    radBranch_CheckedChanged(radJob, New EventArgs())

                Case enAnalysisReport.ClassGroup
                    radClassGrp.Checked = True
                    radBranch_CheckedChanged(radClassGrp, New EventArgs())

                Case enAnalysisReport.Classs
                    radClass.Checked = True
                    radBranch_CheckedChanged(radClass, New EventArgs())

                Case enAnalysisReport.GradeGroup
                    radGradeGrp.Checked = True
                    radBranch_CheckedChanged(radGradeGrp, New EventArgs())

                Case enAnalysisReport.Grade
                    radGrade.Checked = True
                    radBranch_CheckedChanged(radGrade, New EventArgs())

                Case enAnalysisReport.GradeLevel
                    radGradeLevel.Checked = True
                    radBranch_CheckedChanged(radGradeLevel, New EventArgs())

                Case enAnalysisReport.CostCenter
                    radCostCenter.Checked = True
                    radBranch_CheckedChanged(radCostCenter, New EventArgs())

            End Select
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

#End Region

#Region " Page's Events "

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("dtTable", dtTable)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrAnalysis_OrderBy_GroupName", mstrAnalysis_OrderBy_GroupName)
            Me.ViewState.Add("mstrHr_EmployeeTable_Alias", mstrHr_EmployeeTable_Alias)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            Me.ViewState.Add("mstrAnalysis_TableName", mstrAnalysis_TableName)
            Me.ViewState.Add("mstrAnalysis_CodeField", mstrAnalysis_CodeField)
            Me.ViewState.Add("mdtEffectiveDate", mdtEffectiveDate)
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                Call CreateTable()
                Call SetLanguage()
                Me.ViewState.Add("AllocationTagId", -1)

            Else
                dtTable = ViewState("dtTable")
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy")
                mstrAnalysis_OrderBy_GroupName = ViewState("mstrAnalysis_OrderBy_GroupName")
                mstrHr_EmployeeTable_Alias = ViewState("mstrHr_EmployeeTable_Alias")
                mstrReport_GroupName = ViewState("mstrReport_GroupName")
                mstrAnalysis_TableName = ViewState("mstrAnalysis_TableName")
                mstrAnalysis_CodeField = ViewState("mstrAnalysis_CodeField")
                mdtEffectiveDate = ViewState("mdtEffectiveDate")
                mintViewIndex = CInt(ViewState("AllocationTagId"))
            End If
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If cb Is Nothing Then Exit Try
    '        Dim dtTab As DataTable = CType(Me.ViewState("dtTable"), DataTable)

    '        For i As Integer = 0 To dtTab.Rows.Count - 1
    '            If dtTab.Rows(i)("Name").ToString.Trim <> "None" AndAlso CBool(dtTab.Rows(i)("IsCheck")) <> cb.Checked Then
    '                Dim gvRow As GridViewRow = gvDetails.Rows(i)
    '                CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
    '                dtTab.Rows(i)("IsCheck") = cb.Checked

    '            End If
    '        Next
    '        dtTab.AcceptChanges()
    '        Me.ViewState("dtTable") = dtTab


    '    Catch ex As Exception
    '        CommonCodes.LogErrorOnly(ex)
    '    Finally
    '        popupAnalysisBy.Show()
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If cb Is Nothing Then Exit Try

    '        Dim dtTab As DataTable = CType(Me.ViewState("dtTable"), DataTable)
    '        Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
    '        dtTab.Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
    '        dtTab.AcceptChanges()
    '        Me.ViewState("dtTable") = dtTab

    '        Dim drcheckedRow() As DataRow = dtTab.Select("Ischeck = True")

    '        If drcheckedRow.Length = dtTab.Rows.Count Then
    '            CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
    '        Else
    '            CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
    '        End If

    '    Catch ex As Exception
    '        CommonCodes.LogErrorOnly(ex)
    '    Finally
    '        popupAnalysisBy.Show()
    '    End Try
    'End Sub

#End Region

#Region " Button's Events "

    'Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
    '    Try
    '        Call SetLanguage() 'Hemant (19 Jul 2019)
    '        If dtTable.Rows.Count > 0 Then
    '            Dim dtTemp() As DataRow = dtTable.Select("IsCheck=true")
    '            If dtTemp.Length <= 0 Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one report by"), Me.Page)
    '                Exit Sub
    '            End If
    '        Else
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 18, "Please select any one Analysis by"), Me.Page)
    '            Exit Sub
    '        End If

    '        Dim lstIDs As List(Of String) = (From p In dtTable Where CBool(p.Item("IsCheck")) = True Select (p.Item("Id").ToString)).ToList
    '        mstrReportBy_Ids = String.Join(",", lstIDs.ToArray)

    '        Dim lstNAMEs As List(Of String) = (From p In dtTable Where CBool(p.Item("IsCheck")) = True Select (p.Item("Name").ToString)).ToList
    '        mstrReportBy_Name = String.Join(",", lstNAMEs.ToArray)


    '        Select Case TabContainer1.ActiveTab.HeaderText
    '            Case tbpnlBranch.HeaderText

    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Branch
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Branch, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrstation_master ON hrstation_master.stationunkid = " & mstrHr_EmployeeTable_Alias & ".stationunkid AND hrstation_master.stationunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrstation_master.stationunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrstation_master.name "
    '                'mstrReport_GroupName = "Branch :"
    '                'mstrAnalysis_TableName = " hrstation_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlDept.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Department
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Department, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrdepartment_master.departmentunkid AS Id, hrdepartment_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = " & mstrHr_EmployeeTable_Alias & ".departmentunkid AND hrdepartment_master.departmentunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrdepartment_master.departmentunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrdepartment_master.name "
    '                'mstrReport_GroupName = "Department :"
    '                'mstrAnalysis_TableName = " hrdepartment_master"
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlDeptGroup.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.DepartmentGroup
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.DepartmentGroup, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrdepartment_group_master.deptgroupunkid AS Id, hrdepartment_group_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = " & mstrHr_EmployeeTable_Alias & ".deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrdepartment_group_master.deptgroupunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrdepartment_group_master.name "
    '                'mstrReport_GroupName = "Department Group :"
    '                'mstrAnalysis_TableName = " hrdepartment_group_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlSection.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Section
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Section, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ",  hrsection_master.sectionunkid AS Id, hrsection_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrsection_master ON hrsection_master.sectionunkid = " & mstrHr_EmployeeTable_Alias & ".sectionunkid AND hrsection_master.sectionunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrsection_master.sectionunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrsection_master.name "
    '                'mstrReport_GroupName = "Section :"
    '                'mstrAnalysis_TableName = " hrsection_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlUnit.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Unit
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Unit, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ",  hrunit_master.unitunkid AS Id, hrunit_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrunit_master ON hrunit_master.unitunkid = " & mstrHr_EmployeeTable_Alias & ".unitunkid AND hrunit_master.unitunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrunit_master.unitunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrunit_master.name "
    '                'mstrReport_GroupName = "Unit :"
    '                'mstrAnalysis_TableName = " hrunit_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlJob.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Job
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Job, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ",  hrjob_master.jobunkid AS Id, hrjob_master.job_name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrjob_master ON hrjob_master.jobunkid = " & mstrHr_EmployeeTable_Alias & ".jobunkid AND hrjob_master.jobunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrjob_master.jobunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrjob_master.job_name "
    '                'mstrReport_GroupName = "Job :"
    '                'mstrAnalysis_TableName = " hrjob_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlSectionGroup.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.SectionGroup
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.SectionGroup, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrsectiongroup_master.sectiongroupunkid AS Id, hrsectiongroup_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = " & mstrHr_EmployeeTable_Alias & ".sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrsectiongroup_master.sectiongroupunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrsectiongroup_master.name "
    '                'mstrReport_GroupName = "Section Group :"
    '                'mstrAnalysis_TableName = " hrsectiongroup_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlUnitGroup.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.UnitGroup
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.UnitGroup, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrunitgroup_master.unitgroupunkid AS Id, hrunitgroup_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = " & mstrHr_EmployeeTable_Alias & ".unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrunitgroup_master.unitgroupunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrunitgroup_master.name "
    '                'mstrReport_GroupName = "Unit Group :"
    '                'mstrAnalysis_TableName = " hrunitgroup_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlTeam.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Team
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Team, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrteam_master.teamunkid AS Id, hrteam_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrteam_master ON hrteam_master.teamunkid = " & mstrHr_EmployeeTable_Alias & ".teamunkid AND hrteam_master.teamunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrteam_master.teamunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrteam_master.name "
    '                'mstrReport_GroupName = "Team :"
    '                'mstrAnalysis_TableName = " hrteam_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlJobGroup.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.JobGroup
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.JobGroup, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrjobgroup_master.jobgroupunkid AS Id, hrjobgroup_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = " & mstrHr_EmployeeTable_Alias & ".jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrjobgroup_master.jobgroupunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrjobgroup_master.name "
    '                'mstrReport_GroupName = "Job Group :"
    '                'mstrAnalysis_TableName = " hrjobgroup_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlClassGroup.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.ClassGroup
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.ClassGroup, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrclassgroup_master.classgroupunkid AS Id, hrclassgroup_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = " & mstrHr_EmployeeTable_Alias & ".classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrclassgroup_master.classgroupunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrclassgroup_master.name "
    '                'mstrReport_GroupName = "Class Group :"
    '                'mstrAnalysis_TableName = " hrclassgroup_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlClass.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Classs
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Classs, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrclasses_master.classesunkid AS Id, hrclasses_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrclasses_master ON hrclasses_master.classesunkid = " & mstrHr_EmployeeTable_Alias & ".classunkid AND hrclasses_master.classesunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrclasses_master.classesunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrclasses_master.name "
    '                'mstrReport_GroupName = "Class :"
    '                'mstrAnalysis_TableName = " hrclasses_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlGradeGroup.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.GradeGroup
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.GradeGroup, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrgradegroup_master.gradegroupunkid AS Id, hrgradegroup_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = " & mstrHr_EmployeeTable_Alias & ".gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrgradegroup_master.gradegroupunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrgradegroup_master.name "
    '                'mstrReport_GroupName = "Grade Group :"
    '                'mstrAnalysis_TableName = " hrgradegroup_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlGrade.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.Grade
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Grade, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrgrade_master.gradeunkid AS Id, hrgrade_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrgrade_master ON hrgrade_master.gradeunkid = " & mstrHr_EmployeeTable_Alias & ".gradeunkid AND hrgrade_master.gradeunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrgrade_master.gradeunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrgrade_master.name "
    '                'mstrReport_GroupName = "Grade :"
    '                'mstrAnalysis_TableName = " hrgrade_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlGradeLevel.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.GradeLevel
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.GradeLevel, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", hrgradelevel_master.gradelevelunkid AS Id, hrgradelevel_master.name AS GName "
    '                'mstrAnalysis_Join = " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = " & mstrHr_EmployeeTable_Alias & ".gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " hrgradelevel_master.gradelevelunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " hrgradelevel_master.name "
    '                'mstrReport_GroupName = "Grade Level :"
    '                'mstrAnalysis_TableName = " hrgradelevel_master "
    '                'Sohail (23 May 2017) -- End

    '            Case tbpnlCostCenter.HeaderText
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'mintViewIndex = enAnalysisReport.CostCenter
    '                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.CostCenter, Me.ViewState("AllocationTagId")))
    '                'Shani [ 31 OCT 2014 ] -- END

    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'mstrAnalysis_Fields = ", prcostcenter_master.costcenterunkid AS Id, prcostcenter_master.costcentername AS GName "
    '                'mstrAnalysis_Join = " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = " & mstrHr_EmployeeTable_Alias & ".costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & mstrReportBy_Ids & " ) "
    '                'mstrAnalysis_OrderBy = " prcostcenter_master.costcenterunkid "
    '                'mstrAnalysis_OrderBy_GroupName = " prcostcenter_master.costcentername "
    '                'mstrReport_GroupName = "Cost Center :"
    '                'mstrAnalysis_TableName = " prcostcenter_master "
    '                'Sohail (23 May 2017) -- End


    '            Case Else
    '                mintViewIndex = 0 'Sohail (23 May 2017)
    '                mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
    '                mstrAnalysis_Join = ""
    '                mstrAnalysis_OrderBy = ""
    '                mstrAnalysis_OrderBy_GroupName = ""
    '                mstrHr_EmployeeTable_Alias = ""
    '                mstrReport_GroupName = ""
    '                mstrAnalysis_TableName = " "
    '        End Select

    '        'Sohail (23 May 2017) -- Start
    '        'Enhancement - 67.1 - Link budget with Payroll.
    '        Call GetAnalysisByDetails(mstrModuleName _
    '                                      , mintViewIndex _
    '                                      , mstrReportBy_Ids _
    '                                      , mdtEffectiveDate _
    '                                      , mstrHr_EmployeeTable_Alias _
    '                                      , mstrAnalysis_Fields _
    '                                      , mstrAnalysis_Join _
    '                                      , mstrAnalysis_OrderBy _
    '                                      , mstrAnalysis_OrderBy_GroupName _
    '                                      , mstrReport_GroupName _
    '                                      , mstrAnalysis_TableName _
    '                                      , mstrAnalysis_CodeField _
    '                                      )
    '        'Sohail (23 May 2017) -- End

    '        RaiseEvent buttonApply_Click(sender, e)
    '        popupAnalysisBy.Dispose()

    '    Catch ex As Exception
    '        'Gajanan (26-Aug-2020) -- Start
    '        ' Working on IIS Freezing and Dump Issue for NMB.
    '        'Throw New Exception("btnApply_Click :- " & ex.Message)
    '        CommonCodes.LogErrorOnly(ex)
    '        'Gajanan (26-Aug-2020) -- End
    '    End Try
    'End Sub


    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Call SetLanguage()


            'If dtTable.Rows.Count > 0 Then
            '    Dim dtTemp() As DataRow = dtTable.Select("IsCheck=true")
            '    If dtTemp.Length <= 0 Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one report by"), Me.Page)
            '        popupAnalysisBy.Show()
            '        Exit Sub
            '    End If
            'Else
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 18, "Please select any one Analysis by"), Me.Page)
            '    popupAnalysisBy.Show()
            '    Exit Sub
            'End If

            'Dim lstIDs As List(Of String) = (From p In dtTable Where CBool(p.Item("IsCheck")) = True Select (p.Item("Id").ToString)).ToList
            'mstrReportBy_Ids = String.Join(",", lstIDs.ToArray)

            'Dim lstNAMEs As List(Of String) = (From p In dtTable Where CBool(p.Item("IsCheck")) = True Select (p.Item("Name").ToString)).ToList
            'mstrReportBy_Name = String.Join(",", lstNAMEs.ToArray)

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            Dim lstIDs As List(Of String) = Nothing
            Dim lstNAMEs As List(Of String) = Nothing

            'S.SANDEEP |15-FEB-2022| -- START
            'ISSUE : SAME CONTROL NAME FOR ANALYSIS BY/ADVANCE FILTER [chkSelect]
            'gRow = gvDetails.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            gRow = gvDetails.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkAB_Select"), CheckBox).Checked = True)
            'S.SANDEEP |15-FEB-2022| -- END


            If gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one report by"), Me.Page)
                popupAnalysisBy.Show()
                Exit Sub
            End If


            If gRow.Count > 0 Then
                'lstIDs = gvDetails.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) gvDetails.DataKeys(x.DataItemIndex)("Id").ToString()).ToList()
                lstIDs = (From p In gRow Select (gvDetails.DataKeys(p.DataItemIndex)("Id").ToString())).ToList()
                mstrReportBy_Ids = String.Join(",", lstIDs.ToArray)

                lstNAMEs = (From p In gRow Select (p.Cells(getColumnID_Griview(gvDetails, "dgcolhReportBy", False, True)).Text)).ToList()
                mstrReportBy_Name = String.Join(",", lstNAMEs.ToArray)
            End If


            If radBranch.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Branch, Me.ViewState("AllocationTagId")))

            ElseIf radDepartmentGrp.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.DepartmentGroup, Me.ViewState("AllocationTagId")))

            ElseIf radDepartment.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Department, Me.ViewState("AllocationTagId")))

            ElseIf radSection.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Section, Me.ViewState("AllocationTagId")))

            ElseIf radUnit.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Unit, Me.ViewState("AllocationTagId")))

            ElseIf radJob.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Job, Me.ViewState("AllocationTagId")))

            ElseIf radSectionGrp.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.SectionGroup, Me.ViewState("AllocationTagId")))

            ElseIf radUnitGrp.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.UnitGroup, Me.ViewState("AllocationTagId")))

            ElseIf radTeam.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Team, Me.ViewState("AllocationTagId")))

            ElseIf radJobGrp.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.JobGroup, Me.ViewState("AllocationTagId")))

            ElseIf radClassGrp.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.ClassGroup, Me.ViewState("AllocationTagId")))

            ElseIf radClass.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Classs, Me.ViewState("AllocationTagId")))

            ElseIf radGradeGrp.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.GradeGroup, Me.ViewState("AllocationTagId")))

            ElseIf radGrade.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Grade, Me.ViewState("AllocationTagId")))

            ElseIf radGradeLevel.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.GradeLevel, Me.ViewState("AllocationTagId")))

            ElseIf radCostCenter.Checked Then
                mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.CostCenter, Me.ViewState("AllocationTagId")))

            Else
                mintViewIndex = 0 'Sohail (23 May 2017)
                mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
                mstrAnalysis_Join = ""
                mstrAnalysis_OrderBy = ""
                mstrAnalysis_OrderBy_GroupName = ""
                mstrHr_EmployeeTable_Alias = ""
                mstrReport_GroupName = ""
                mstrAnalysis_TableName = " "

            End If

            Call GetAnalysisByDetails(mstrModuleName _
                                          , mintViewIndex _
                                          , mstrReportBy_Ids _
                                          , mdtEffectiveDate _
                                          , mstrHr_EmployeeTable_Alias _
                                          , mstrAnalysis_Fields _
                                          , mstrAnalysis_Join _
                                          , mstrAnalysis_OrderBy _
                                          , mstrAnalysis_OrderBy_GroupName _
                                          , mstrReport_GroupName _
                                          , mstrAnalysis_TableName _
                                          , mstrAnalysis_CodeField _
                                          )

            RaiseEvent buttonApply_Click(sender, e)
            popupAnalysisBy.Dispose()

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrHr_EmployeeTable_Alias = ""
            mstrReport_GroupName = ""
            mstrAnalysis_TableName = " "
            mstrAnalysis_CodeField = "" 'Sohail (23 May 2017)

            RaiseEvent buttonClose_Click(sender, e)
            popupAnalysisBy.Dispose()
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

#End Region

#Region " GridView's Events "

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(DataBinder.Eval(e.Row.DataItem, "Id")) = -111 AndAlso DataBinder.Eval(e.Row.DataItem, "Name").ToString = "None" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

#End Region

#Region "RadioButton Events "

    Protected Sub radBranch_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBranch.CheckedChanged, radDepartmentGrp.CheckedChanged, radDepartment.CheckedChanged _
                                                                                                                                                             , radSectionGrp.CheckedChanged, radSection.CheckedChanged, radUnitGrp.CheckedChanged _
                                                                                                                                                             , radUnit.CheckedChanged, radTeam.CheckedChanged, radJobGrp.CheckedChanged _
                                                                                                                                                             , radJob.CheckedChanged, radClassGrp.CheckedChanged, radClass.CheckedChanged _
                                                                                                                                                             , radGradeGrp.CheckedChanged, radGradeLevel.CheckedChanged, radGrade.CheckedChanged _
                                                                                                                                                             , radCostCenter.CheckedChanged
        Dim intColType As Integer = 0
        Dim dsList As DataSet
        Try

            Select Case CType(sender, RadioButton).ID.ToUpper()

                Case radBranch.ID.ToUpper()

                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                    If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
                        If CInt(Session("UserId")) > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Branch

                Case radDepartmentGrp.ID.ToUpper()

                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                    If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.DepartmentGroup

                Case radDepartment.ID.ToUpper()

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                    If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Department

                Case radSectionGrp.ID.ToUpper()

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                    If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.SectionGroup

                Case radSection.ID.ToUpper()

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                    If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Section

                Case radUnitGrp.ID.ToUpper()

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                    If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.UnitGroup

                Case radUnit.ID.ToUpper()

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                    If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Unit

                Case radTeam.ID.ToUpper()

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                    If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Team

                Case radJobGrp.ID.ToUpper()

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                    If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.JobGroup

                Case radJob.ID.ToUpper()

                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                    If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Job

                Case radClassGrp.ID.ToUpper()

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                    If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.ClassGroup

                Case radClass.ID.ToUpper()

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                    If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
                        Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
                        StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                        dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Classs

                Case radGradeGrp.ID.ToUpper()

                    Dim objGradeGrp As New clsGradeGroup
                    dsList = objGradeGrp.GetList("List", True)
                    objGradeGrp = Nothing

                    dtTable = dsList.Tables("List")

                    Me.ViewState("AllocationTagId") = enAnalysisReport.GradeGroup


                Case radGrade.ID.ToUpper()

                    Dim objGrade As New clsGrade
                    dsList = objGrade.GetList("List", True)
                    radGrade = Nothing

                    dtTable = dsList.Tables("List")

                    Me.ViewState("AllocationTagId") = enAnalysisReport.Grade

                Case radGradeLevel.ID.ToUpper()

                    Dim objGradeLvl As New clsGradeLevel
                    dsList = objGradeLvl.GetList("List", True)
                    objGradeLvl = Nothing

                    dtTable = dsList.Tables("List")

                    Me.ViewState("AllocationTagId") = enAnalysisReport.GradeLevel

                Case radCostCenter.ID.ToUpper()

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2
                    dtTable = dsList.Tables("List")

                    Me.ViewState("AllocationTagId") = enAnalysisReport.CostCenter

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobgroupunkid").ColumnName = "Id"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_name").ColumnName = "Name"
            End If

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsCheck"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally

            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                dtTable.Rows.Add(r)
            End If

            gvDetails.DataSource = dtTable
            gvDetails.DataBind()

            popupAnalysisBy.Show()
        End Try
    End Sub


#End Region

    '#Region " Tab Control's Events "

    '    Protected Sub TabContainer1_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabContainer1.ActiveTabChanged
    '        Dim intColType As Integer = 0
    '        Dim dsList As DataSet
    '        Try
    '            If TabContainer1.ActiveTab Is tbpnlBranch Then
    '                Dim objBranch As New clsStation
    '                dsList = objBranch.GetList("List", True)

    '                If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
    '                    If CInt(Session("UserId")) > 0 Then
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    End If



    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Branch


    '            ElseIf TabContainer1.ActiveTab Is tbpnlDeptGroup Then
    '                Dim objDeptGrp As New clsDepartmentGroup
    '                dsList = objDeptGrp.GetList("List", True)

    '                If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.DEPARTMENT_GROUP
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.DepartmentGroup
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlDept Then
    '                Dim objDept As New clsDepartment
    '                dsList = objDept.GetList("List", True)

    '                If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.DEPARTMENT
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Department
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlSectionGroup Then
    '                Dim objSG As New clsSectionGroup
    '                dsList = objSG.GetList("List", True)

    '                If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.SECTION_GROUP
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.SectionGroup
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlSection Then
    '                Dim objSection As New clsSections
    '                dsList = objSection.GetList("List", True)

    '                If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.SECTION
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Section
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlUnitGroup Then
    '                Dim objUG As New clsUnitGroup
    '                dsList = objUG.GetList("List", True)

    '                If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.UNIT_GROUP
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.UnitGroup
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlUnit Then
    '                Dim objUnit As New clsUnits
    '                dsList = objUnit.GetList("List", True)

    '                If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.UNIT
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Unit
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlTeam Then
    '                Dim objTeam As New clsTeams
    '                dsList = objTeam.GetList("List", True)

    '                If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.TEAM
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Team
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlJobGroup Then
    '                Dim objjobGRP As New clsJobGroup
    '                dsList = objjobGRP.GetList("List", True)

    '                If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.JOB_GROUP
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.JobGroup
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlJob Then
    '                Dim objJobs As New clsJobs
    '                dsList = objJobs.GetList("List", True)
    '                intColType = 1

    '                If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.JOBS
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Job
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlClassGroup Then
    '                Dim objClassGrp As New clsClassGroup
    '                dsList = objClassGrp.GetList("List", True)

    '                If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.CLASS_GROUP
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.ClassGroup
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlClass Then
    '                Dim objClass As New clsClass
    '                dsList = objClass.GetList("List", True)

    '                If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
    '                    Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
    '                    StrFilter = StrFilter.Replace("classunkid", "classesunkid")
    '                    dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.CLASSES
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Classs
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlGradeGroup Then
    '                Dim objGradeGrp As New clsGradeGroup
    '                dsList = objGradeGrp.GetList("List", True)

    '                dtTable = dsList.Tables("List")

    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = -1
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.GradeGroup
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlGrade Then
    '                Dim objGrade As New clsGrade
    '                dsList = objGrade.GetList("List", True)

    '                dtTable = dsList.Tables("List")

    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = -1
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.Grade
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlGradeLevel Then
    '                Dim objGradeLvl As New clsGradeLevel
    '                dsList = objGradeLvl.GetList("List", True)

    '                dtTable = dsList.Tables("List")
    '                'ElseIf TabContainer1.ActiveTab Is tbpnlCostCenterGroup Then
    '                '    Dim objCCG As New clspayrollgroup_master
    '                '    mdsList = objCCG.getListForCombo(enPayrollGroupType.CostCenter, "List")

    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = -1
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.GradeLevel
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            ElseIf TabContainer1.ActiveTab Is tbpnlCostCenter Then
    '                Dim objConstCenter As New clscostcenter_master
    '                dsList = objConstCenter.GetList("List", True)
    '                intColType = 2

    '                dtTable = dsList.Tables("List")
    '                objConstCenter = Nothing
    '                'Shani [ 31 OCT 2014 ] -- START
    '                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
    '                'Sohail (23 May 2017) -- Start
    '                'Enhancement - 67.1 - Link budget with Payroll.
    '                'Me.ViewState("AllocationTagId") = enAllocation.COST_CENTER
    '                Me.ViewState("AllocationTagId") = enAnalysisReport.CostCenter
    '                'Sohail (23 May 2017) -- End
    '                'Shani [ 31 OCT 2014 ] -- END
    '            End If

    '            If intColType = 0 Then
    '                dtTable.Columns(0).ColumnName = "Id"
    '                dtTable.Columns("name").ColumnName = "Name"
    '            ElseIf intColType = 1 Then
    '                dtTable.Columns("jobgroupunkid").ColumnName = "Id"
    '                dtTable.Columns("jobname").ColumnName = "Name"
    '            ElseIf intColType = 2 Then
    '                dtTable.Columns(0).ColumnName = "Id"
    '                dtTable.Columns("costcentername").ColumnName = "Name"
    '            ElseIf intColType = 3 Then
    '                dtTable.Columns(1).ColumnName = "Id"
    '                dtTable.Columns(0).ColumnName = "Name"
    '            ElseIf intColType = 4 Then
    '                dtTable.Columns(0).ColumnName = "Id"
    '                dtTable.Columns("country_name").ColumnName = "Name"
    '            End If

    '            Dim dtCol As New DataColumn
    '            dtCol.ColumnName = "IsCheck"
    '            dtCol.Caption = ""
    '            dtCol.DataType = System.Type.GetType("System.Boolean")
    '            dtCol.DefaultValue = False
    '            dtTable.Columns.Add(dtCol)


    '        Catch ex As Exception
    '            'Gajanan (26-Aug-2020) -- Start
    '            ' Working on IIS Freezing and Dump Issue for NMB.
    '            'Throw New Exception("TabContainer1_ActiveTabChanged :- " & ex.Message)
    '            CommonCodes.LogErrorOnly(ex)
    '            'Gajanan (26-Aug-2020) -- End
    '        Finally

    '            If dtTable.Rows.Count <= 0 Then
    '                Dim r As DataRow = dtTable.NewRow

    '                r.Item("Id") = -111
    '                r.Item("Name") = "None" ' To Hide the row and display only Row Header

    '                dtTable.Rows.Add(r)
    '            End If

    '            gvDetails.DataSource = dtTable
    '            gvDetails.DataBind()

    '            popupAnalysisBy.Show()
    '        End Try
    '    End Sub

    '#End Region


    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)


            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnCancel", Me.btnClose.Text).Replace("&", "")
            Me.btnApply.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnSet", Me.btnApply.Text).Replace("&", "")
            Me.lblTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"lblAnalysisBy", Me.lblTitle.Text)
            Me.radCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radCostCenter.ID, Me.radCostCenter.Text)
            Me.radJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radJob.ID, Me.radJob.Text)
            Me.radUnit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radUnit.ID, Me.radUnit.Text)
            Me.radDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radDepartment.ID, Me.radDepartment.Text)
            Me.radSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radSection.ID, Me.radSection.Text)
            Me.radBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radBranch.ID, Me.radBranch.Text)
            Me.radTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radTeam.ID, Me.radTeam.Text)
            Me.radUnitGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radUnitGrp.ID, Me.radUnitGrp.Text)
            Me.radSectionGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radSectionGrp.ID, Me.radSectionGrp.Text)
            Me.radJobGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radJobGrp.ID, Me.radJobGrp.Text)
            Me.radDepartmentGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radDepartmentGrp.ID, Me.radDepartmentGrp.Text)
            Me.radGradeLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radGradeLevel.ID, Me.radGradeLevel.Text)
            Me.radGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radGrade.ID, Me.radGrade.Text)
            Me.radGradeGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radGradeGrp.ID, Me.radGradeGrp.Text)
            Me.radClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radClass.ID, Me.radClass.Text)
            Me.radClassGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),radClassGrp.ID, Me.radClassGrp.Text)

            gvDetails.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvDetails.Columns(2).FooterText, gvDetails.Columns(2).HeaderText)

            Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one report by")
            Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 18, "Please select any Analysis by")


        Catch Ex As Exception
            CommonCodes.LogErrorOnly(Ex)
        End Try
    End Sub


End Class
