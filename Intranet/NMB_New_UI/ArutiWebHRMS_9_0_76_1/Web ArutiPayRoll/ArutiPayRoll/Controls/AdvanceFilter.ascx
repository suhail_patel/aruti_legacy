﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdvanceFilter.ascx.vb"
    Inherits="Controls_AdvanceFilter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    </style>

<script type="text/javascript" language="javascript">
    $.expr[":"].containsNoCase = function(el, i, m) {
        var search = m[3];
        if (!search) return false;
        return eval("/" + search + "/i").test($(el).text());
    };

    function $$(id, context) {
        var el = $("#" + id, context);
        if (el.length < 1)
            el = $("[id$=_" + id + "]", context);
        return el;
    }

    $(document).ready(function() {
    var txtSearch = $$('txtSearch');
    });

    //'S.SANDEEP |25-OCT-2019| -- START
    //'ISSUE/ENHANCEMENT : Calibration Issues
    function Search(ctr) {
        var txtSearch = $$('txtSearch');
        var gvDetails = $$('gvDetails');

        if ($(txtSearch).length > 1) {
            for (i = 0; i <= $(txtSearch).length - 1; i++) {
                if ($(txtSearch)[i].id === ctr.id) {
                    txtSearch = $(txtSearch)[i];
                    gvDetails = $(txtSearch).closest('table').find('.gridview')[0];
                }
            }
        }
        //'S.SANDEEP |25-OCT-2019| -- END

        $('.norecords').remove();

        if ($(txtSearch).val().length > 0) {
            $('#' + $(gvDetails).attr('id') + ' tr').hide();            
            $('#' + $(gvDetails).attr('id') + ' tr:first').show();
            $('#' + $(gvDetails).attr('id') + ' tr td:containsNoCase(\'' + $(txtSearch).val() + '\')').parent().show();
            $('#' + $(gvDetails).attr('id') + ' tr:visible').find($$('hdnfld')).val('')
            $('#' + $(gvDetails).attr('id') + ' tr:hidden').find($$('hdnfld')).val('hidden')
        }
        else if ($(txtSearch).val().length == 0) {
            resetSearchValue();
        }

        if ($('#' + $(gvDetails).attr('id') + ' tr:visible').length == 1) {
            $('.norecords').remove();
            $(gvDetails).append('<tr class="norecords"><td colspan="6" class="Normal" style="text-align: center">No records were found</td></tr>');
        }
    }

    function resetSearchValue() {
        var txtSearch = $$('txtSearch');
        var gvDetails = $$('gvDetails');
        
        $(txtSearch).val('');
        $('#' + $(gvDetails).attr('id') + ' tr').show();
        $('#' + $(gvDetails).attr('id') + ' tr:visible').find($$('hdnfld')).val('')
        $('#' + $(gvDetails).attr('id') + ' tr:hidden').find($$('hdnfld')).val('hidden')
        $('.norecords').remove();
        $(txtSearch).focus();
    }
</script>

<ajaxToolkit:ModalPopupExtender ID="popupAdvanceFilter" runat="server" BackgroundCssClass="modal-backdrop"
    CancelControlID="HiddenField2" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog modal-lg" style="display:none" DefaultButton="btnApply">
    <div id="divAdvanceFilter" class="panel-primary" style="margin-bottom: 0px">
        <div class="header">
            <h2>
                <asp:Label ID="lblTitle" runat="server" Text="Advance Search"></asp:Label>
            </h2>
        </div>
        <div class="body" style="height: 400px">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbBranch" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Branch" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbDepartmentGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Department Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbDepartment" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Department" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbSectionGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Section Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbSection" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Section" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbUnitGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Unit Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbUnit" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Unit" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbTeam" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Team" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbJobGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Job Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbJob" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Job" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbClassGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Class Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbClass" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Class" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbGradeGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Grade Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbGrade" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Grade" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbGradeLevel" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Grade Level" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbCostCenterGroup" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Cost Center Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbCostCenter" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Cost Center" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbEmployementType" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Employement Type" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbReligion" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Religion" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbGender" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Gender" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbNationality" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Nationality" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbPayType" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Pay Type" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbPayPoint" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Pay Point" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbMaritalStatus" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Marital Status" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbRelationfromDependents" runat="server" AutoPostBack="true"
                                CssClass="with-gap" GroupName="AdvFilter" Text="Relation from Dependents" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdbSkill" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="AdvFilter" Text="Skill" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="form-line">
                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="false" CssClass="form-control"
                                    placeholder="Type to search" onkeyup="Search();" />
                            </div>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" DataKeyNames="Id"
                                CssClass="table table-hover table-bordered" AllowPaging="false">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"   ItemStyle-Width="25px" HeaderStyle-Width="25px">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" Text=" " CssClass="filled-in" AutoPostBack="true"
                                                OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " Checked='<%# Eval("IsCheck") %>'
                                                OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true" />
                                            <asp:HiddenField ID="hdnfld" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Name" HeaderText="Description" FooterText="colhDescription"
                                        ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvFilterInfo" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                CssClass="table table-hover table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Name" HeaderText="Filter Value" FooterText="colhFilterValue"
                                        ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <asp:Label ID="lblMessage" runat="server" Text="" Style="max-width: 475px; display: inline-block;" />
            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary" />
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:HiddenField ID="HiddenField2" runat="server" />
        </div>
    </div>
</asp:Panel>
