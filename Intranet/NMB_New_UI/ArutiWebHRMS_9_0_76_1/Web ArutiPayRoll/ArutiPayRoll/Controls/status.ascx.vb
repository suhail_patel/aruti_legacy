﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Controls_status
    Inherits System.Web.UI.UserControl
    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim clsstatus As New Aruti.Data.clsMasterData
    Private pWidth As Integer
    Private dtData As DataTable = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            binddata()

        End If
    End Sub
    Public Sub binddata(Optional ByVal mintStatusunkid As Integer = -1)
        Try
            Dim dttable As DataTable = Nothing

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'Dim ds As DataSet = clsstatus.getLeaveStatusList("Cfcommon_master", True)
            Dim ds As DataSet = clsstatus.getLeaveStatusList("Cfcommon_master", "", True)
            'Pinkal (03-Jan-2020) -- End

            If mintStatusunkid > 0 And mintStatusunkid <> 7 Then
                dttable = New DataView(ds.Tables(0), "statusunkid not in (6,7)", "", DataViewRowState.CurrentRows).ToTable
            ElseIf mintStatusunkid = 7 Then
                dttable = New DataView(ds.Tables(0), "statusunkid not in (1,2)", "", DataViewRowState.CurrentRows).ToTable
            Else
                dttable = ds.Tables(0)
            End If
            ddlstatus.DataSource = dttable
            ddlstatus.DataTextField = "name"
            ddlstatus.DataValueField = "statusunkid"
            ddlstatus.DataBind()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Status-->binddata Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Property width() As Integer
        Get
            Return pWidth
            Setwidth()
        End Get
        Set(ByVal value As Integer)
            pWidth = value
            Setwidth()
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlstatus.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            ddlstatus.AutoPostBack = value
        End Set
    End Property

    Public Sub Setwidth()
        ddlstatus.Width = pWidth - 20

    End Sub

    Public Property SelectedValue() As String
        Get
            Return ddlstatus.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlstatus.SelectedValue = value
        End Set
    End Property

    Public Property SelectedText() As String
        Get
            Return ddlstatus.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            ddlstatus.SelectedItem.Text = value
        End Set
    End Property

    Public Property SelectedIndex() As Integer
        Get
            Return ddlstatus.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            ddlstatus.SelectedIndex = value
        End Set
    End Property

    Public WriteOnly Property Stautsunkid() As Integer
        Set(ByVal value As Integer)
            binddata(value)
            dtData = ddlstatus.DataSource
        End Set
    End Property

    Public ReadOnly Property StatusData() As DataTable
        Get
            Return dtData
        End Get
    End Property

    Protected Sub Page_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub ddlstatus_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlstatus.DataBound
        If ddlstatus.Items.Count > 0 Then
            For Each lstItem As ListItem In ddlstatus.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            ddlstatus.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
    End Sub
End Class

