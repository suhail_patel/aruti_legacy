﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfirmYesNo.ascx.vb"
    Inherits="Controls_ConfirmYesNo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modal-backdrop bd-l5"
    CancelControlID="btnNo" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog modal-l5" Style="display: none;">
    <div class="header">
        <h2>
            <asp:Label ID="lblTitle" runat="server" Text="Title" />
        </h2>
    </div>
    <div class="body" style="max-height: 400px">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <asp:Label ID="lblMessage" runat="server" Text="Message :" CssClass="form-label"></asp:Label>
            </div>
        </div>
    </div>
    <div class="footer">
        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary" ValidationGroup="ConfirmYes" />
        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btn btn-default" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
    </div>
</asp:Panel>
