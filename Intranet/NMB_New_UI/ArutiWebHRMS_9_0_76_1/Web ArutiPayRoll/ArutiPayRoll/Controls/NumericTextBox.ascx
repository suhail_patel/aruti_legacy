﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NumericTextBox.ascx.vb"
    Inherits="Controls_NumericTextBox" %>
<div class="form-group spinner" data-trigger="spinner">
    <div class="form-line">
        <asp:TextBox ID="txtNumeric" runat="server" CssClass="form-control"></asp:TextBox>
    </div>
    <span class="input-group-addon">
        <asp:LinkButton ID="lnkup" runat=server href="javascript:;" CssClass="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></asp:LinkButton>
        <asp:LinkButton ID="lnkdown" runat=server href="javascript:;" CssClass="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></asp:LinkButton>
    </span>
</div>
