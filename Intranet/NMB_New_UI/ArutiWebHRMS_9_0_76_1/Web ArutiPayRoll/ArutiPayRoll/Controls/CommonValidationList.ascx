﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CommonValidationList.ascx.vb"
    Inherits="Controls_CommonValidationList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modal-backdrop"
    CancelControlID="btnCValidationCancel" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog" Style="display: none;">
    <div class="block-header">
        <h2>
            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti" CssClass="form-label"></asp:Label>
        </h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="header">
                <h2>
                    <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="form-label"></asp:Label>
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive" style="max-height: 400px;">
                            <asp:Panel ID="pnl_gvdetails" runat="server" ScrollBars="Auto" Width="100%">
                                <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="true" ShowFooter="False"
                                    Width="98%" CellPadding="3" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <asp:Panel ID="pnlYesNo" runat="server" Visible="false">
                    <asp:Button ID="btnCValidationYes" runat="server" Text="Yes" CssClass="btn btn-primary"
                        ValidationGroup="CValidation" />
                    <asp:Button ID="btnCValidationNo" runat="server" Text="No" CssClass="btn btn-default" />
                    <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlOkCancel" runat="server">
                    <asp:Button ID="btnCValidationOk" runat="server" Text="Ok" CssClass="btn btn-primary"
                        ValidationGroup="CValidation" />
                    <asp:Button ID="btnCValidationCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Panel>
