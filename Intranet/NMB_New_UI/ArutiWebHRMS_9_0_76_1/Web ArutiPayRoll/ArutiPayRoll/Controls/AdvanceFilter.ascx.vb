﻿Imports System.Data
Imports Aruti.Data

Partial Class Controls_AdvanceFilter
    Inherits System.Web.UI.UserControl

    Public Event buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAdvanceSearch"
    Dim DisplayMessage As New CommonCodes

    Private mstrFilterString As String = String.Empty
    Private mstrEmployeeTableAlias As String = ""

    Private mdicFilterText As New Dictionary(Of String, String)


    Private mdsList As DataSet
    Private mdtFilterInfo As DataTable
    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    Private dvDetail As DataView
    'Sohail (01 Aug 2019) -- End
    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private mintSelectedAllocationID As Integer = 0
    Private mdicAdvAlloc As New Dictionary(Of Integer, String)
    Private mblnShowSkill As Boolean = False
    'Sohail (14 Nov 2019) -- End

    Private mstrActiveGroup As String = ""
#End Region

#Region " Properties "
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return lblMessage.Text
        End Get
        Set(ByVal value As String)
            lblMessage.Text = value
        End Set
    End Property

    Public ReadOnly Property _GetFilterString() As String
        Get
            Return mstrFilterString
        End Get
    End Property

    Public WriteOnly Property _Hr_EmployeeTable_Alias() As String
        Set(ByVal value As String)
            mstrEmployeeTableAlias = value
        End Set
    End Property

    Public ReadOnly Property _GetFilterText() As Dictionary(Of String, String)
        Get
            Return mdicFilterText
        End Get
    End Property

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Public WriteOnly Property _ShowSkill() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSkill = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Dim dt As DataTable = Nothing
            If mdtFilterInfo IsNot Nothing Then
                dt = New DataView(mdtFilterInfo, "IsGroup = 0 ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Columns.Contains("GroupId") = True Then
                    dt.Columns("GroupId").ColumnName = "advancefilterallocationid"
                End If
                If dt.Columns.Contains("Id") = True Then
                    dt.Columns("Id").ColumnName = "advancefilterallocationtranunkid"
                End If
                If dt.Columns.Contains("UnkId") = False Then
                    Dim dtCol As New DataColumn("UnkId", System.Type.GetType("System.Int32"))
                    dtCol.AllowDBNull = False
                    dtCol.DefaultValue = -1
                    dt.Columns.Add(dtCol)
                End If
            End If
            Return dt
        End Get
        Set(ByVal value As DataTable)
            mdtFilterInfo = value
            If mdtFilterInfo IsNot Nothing Then
                If mdtFilterInfo.Columns.Contains("advancefilterallocationid") = True Then
                    mdtFilterInfo.Columns("advancefilterallocationid").ColumnName = "GroupId"
                End If
                If mdtFilterInfo.Columns.Contains("advancefilterallocationtranunkid") = True Then
                    mdtFilterInfo.Columns("advancefilterallocationtranunkid").ColumnName = "Id"
                End If
            End If
        End Set
    End Property
    'Sohail (14 Nov 2019) -- End

#End Region

#Region " Methods & Functions "
    Private Sub CreateTable()
        Try
            mdsList = New DataSet()
            mdsList.Tables.Add("List")
            mdsList.Tables(0).Columns.Add("IsCheck", Type.GetType("System.Boolean")).DefaultValue = False
            mdsList.Tables(0).Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
            mdsList.Tables(0).Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""

            mstrActiveGroup = rdbBranch.Text
            rdbBranch.Checked = True
            Call rdb_CheckedChanged(rdbBranch, New System.EventArgs)

            popupAdvanceFilter.Hide()

            If mdsList.Tables(0).Rows.Count <= 0 Then
                Dim r As DataRow = mdsList.Tables(0).NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                mdsList.Tables(0).Rows.Add(r)
            End If

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            Dim dtTable As DataTable = New DataView(mdsList.Tables(0), "", "IsCheck DESC, Name", DataViewRowState.CurrentRows).ToTable
            mdsList.Tables.Clear()
            mdsList.Tables.Add(dtTable)
            'Sohail (01 Aug 2019) -- End
            gvDetails.DataSource = mdsList.Tables(0)
            gvDetails.DataBind()

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateTable :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Private Sub CreateFilterInfoTable()
        Try
            If mdtFilterInfo Is Nothing Then 'Sohail (14 Nov 2019)
                mdtFilterInfo = New DataTable
                mdtFilterInfo.Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
                mdtFilterInfo.Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""
                mdtFilterInfo.Columns.Add("GroupName", Type.GetType("System.String")).DefaultValue = ""
                mdtFilterInfo.Columns.Add("IsGroup", Type.GetType("System.Boolean")).DefaultValue = False
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mdtFilterInfo.Columns.Add("GroupId", Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (14 Nov 2019) -- End
            End If 'Sohail (14 Nov 2019)

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'If mdtFilterInfo.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtFilterInfo.NewRow

            '    r.Item(0) = -111
            '    r.Item(1) = "None" ' To Hide the row and display only Row Header
            '    r.Item(2) = ""
            '    r.Item(3) = True

            '    mdtFilterInfo.Rows.Add(r)
            'End If

            'gvFilterInfo.DataSource = mdtFilterInfo
            'gvFilterInfo.DataBind()
            Call FillFilterInfo()
            'Sohail (14 Nov 2019) -- End
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateFilterInfoTable :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private Sub FillFilterInfo()
        Try

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If mdtFilterInfo Is Nothing Then Exit Sub

            'If mdtFilterInfo.Rows.Count <= 0 Then
            If mdtFilterInfo IsNot Nothing AndAlso mdtFilterInfo.Rows.Count <= 0 Then
                'Pinkal (20-Aug-2020) -- End
                Dim r As DataRow = mdtFilterInfo.NewRow

                r.Item(0) = -111
                r.Item(1) = "None" ' To Hide the row and display only Row Header
                r.Item(2) = ""
                r.Item(3) = True

                mdtFilterInfo.Rows.Add(r)
            End If

            Dim dt As DataTable = New DataView(mdtFilterInfo, " GroupId > 0 ", "", DataViewRowState.CurrentRows).ToTable(True, "GroupName", "GroupId")
            Dim dtCol As New DataColumn("IsGroup", System.Type.GetType("System.Boolean"))
            dtCol.DefaultValue = True
            dtCol.AllowDBNull = False
            dt.Columns.Add(dtCol)
            dt.Merge(mdtFilterInfo, False)

            dt = New DataView(dt, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            gvFilterInfo.DataSource = dt
            gvFilterInfo.DataBind()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("FillFilterInfo :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (14 Nov 2019) -- End

    Public Sub Show()
        Try
            Call Reset()
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
            popupAdvanceFilter.Hide()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide():- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Private Sub Reset()
        Try
            Call CreateFilterInfoTable()
            Call CreateTable()
            mstrFilterString = ""
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Reset :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " Page's Events "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrEmployeeTableAlias", mstrEmployeeTableAlias)
            Me.ViewState.Add("mdsList", mdsList)
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            Me.ViewState.Add("mdtFilterInfo", mdtFilterInfo)
            Me.ViewState.Add("mintSelectedAllocationID", mintSelectedAllocationID)
            Me.ViewState.Add("mdicAdvAlloc", mdicAdvAlloc)
            Me.ViewState.Add("mblnShowSkill", mblnShowSkill)
            'Sohail (14 Nov 2019) -- End

            Me.ViewState.Add("mstrActiveGroup", mstrActiveGroup)

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                If mstrEmployeeTableAlias.Trim.Length > 0 Then mstrEmployeeTableAlias = mstrEmployeeTableAlias & "."
                Call CreateTable()
                Call CreateFilterInfoTable()
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.

                rdbSkill.Visible = mblnShowSkill

                Dim objMaster As New clsMasterData
                Dim ds As DataSet = objMaster.GetAdvanceFilterAllocation("List", True)
                mdicAdvAlloc = (From p In ds.Tables(0) Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
                'Sohail (14 Nov 2019) -- End
                Call SetLanguage()
            Else
                mstrEmployeeTableAlias = ViewState("mstrEmployeeTableAlias")
                mdsList = ViewState("mdsList")
                mdtFilterInfo = ViewState("mdtFilterInfo")
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = CInt(ViewState("mintSelectedAllocationID"))
                mdicAdvAlloc = ViewState("mdicAdvAlloc")
                mblnShowSkill = CBool(ViewState("mblnShowSkill"))
                'Sohail (14 Nov 2019) -- End

                mstrActiveGroup = ViewState("mstrActiveGroup")

            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try
            Dim dtTab As DataSet = CType(Me.ViewState("mdsList"), DataSet)

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If dtTab Is Nothing Then Exit Sub
            If mdtFilterInfo Is Nothing Then Exit Sub
            'Pinkal (20-Aug-2020) -- End


            For i As Integer = 0 To dtTab.Tables(0).Rows.Count - 1
                If dtTab.Tables(0).Rows(i)("Name").ToString.Trim <> "None" AndAlso CBool(dtTab.Tables(0).Rows(i)("IsCheck")) <> cb.Checked Then
                    Dim gvRow As GridViewRow = gvDetails.Rows(i)

                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    If CType(gvRow.FindControl("hdnfld"), HiddenField).Value = "hidden" Then Continue For
                    'Sohail (01 Aug 2019) -- End

                    CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                    dtTab.Tables(0).Rows(i)("IsCheck") = cb.Checked

                    If cb.Checked = True Then
                        Dim dRow As DataRow = Nothing

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ").Length <= 0 Then
                        '    dRow = mdtFilterInfo.NewRow

                        '    dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                        '    dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                        '    dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                        '    dRow.Item("IsGroup") = True

                        '    mdtFilterInfo.Rows.Add(dRow)
                        'End If
                        'Sohail (14 Nov 2019) -- End
                        dRow = mdtFilterInfo.NewRow

                        dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                        dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.

                        'dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                        dRow.Item("GroupId") = mintSelectedAllocationID
                        If mdicAdvAlloc.ContainsKey(mintSelectedAllocationID) = True Then
                            dRow.Item("GroupName") = mdicAdvAlloc.Item(mintSelectedAllocationID)
                        Else
                            dRow.Item("GroupName") = mstrActiveGroup
                        End If
                        'Sohail (14 Nov 2019) -- End
                        dRow.Item("IsGroup") = False

                        mdtFilterInfo.Rows.Add(dRow)
                    Else
                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ")
                        Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ")
                        'Sohail (14 Nov 2019) -- End
                        If dRow.Length > 0 Then
                            mdtFilterInfo.Rows.Remove(dRow(0))
                        End If

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ").Length <= 0 Then
                        '   dRow = mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ")
                        If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ").Length <= 0 Then
                            dRow = mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ")
                            'Sohail (14 Nov 2019) -- End
                            If dRow.Length > 0 Then
                                mdtFilterInfo.Rows.Remove(dRow(0))
                            End If
                        End If
                    End If
                    mdtFilterInfo.AcceptChanges()
                End If
            Next
            dtTab.AcceptChanges()
            Me.ViewState("mdsList") = dtTab

            mdtFilterInfo = New DataView(mdtFilterInfo, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            txtSearch.Text = ""
            'Sohail (01 Aug 2019) -- End

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("chkSelectAll_CheckedChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        Finally

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'If mdtFilterInfo.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtFilterInfo.NewRow

            '    r.Item(0) = -111
            '    r.Item(1) = "None" ' To Hide the row and display only Row Header
            '    r.Item(2) = ""
            '    r.Item(3) = True

            '    mdtFilterInfo.Rows.Add(r)
            'End If

            'gvFilterInfo.DataSource = mdtFilterInfo
            'gvFilterInfo.DataBind()
            Call FillFilterInfo()
            'Sohail (14 Nov 2019) -- End

            popupAdvanceFilter.Show()
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataSet = CType(Me.ViewState("mdsList"), DataSet)

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If dtTab Is Nothing Then Exit Sub
            If mdtFilterInfo Is Nothing Then Exit Sub
            'Pinkal (20-Aug-2020) -- End

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
            dtTab.Tables(0).Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
            dtTab.AcceptChanges()
            Me.ViewState("mdsList") = dtTab

            Dim drcheckedRow() As DataRow = dtTab.Tables(0).Select("Ischeck = True")

            If drcheckedRow.Length = dtTab.Tables(0).Rows.Count Then
                CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

            If cb.Checked = True Then
                Dim dRow As DataRow = Nothing

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ").Length <= 0 Then
                '    dRow = mdtFilterInfo.NewRow

                '    dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                '    dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                '    dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                '    dRow.Item("IsGroup") = True

                '    mdtFilterInfo.Rows.Add(dRow)
                'End If
                'Sohail (14 Nov 2019) -- End
                dRow = mdtFilterInfo.NewRow

                dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                dRow.Item("GroupId") = mintSelectedAllocationID
                If mdicAdvAlloc.ContainsKey(mintSelectedAllocationID) = True Then
                    dRow.Item("GroupName") = mdicAdvAlloc.Item(mintSelectedAllocationID)
                Else
                    'dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                End If
                'Sohail (14 Nov 2019) -- End
                dRow.Item("IsGroup") = False

                mdtFilterInfo.Rows.Add(dRow)
            Else
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ")
                Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ")
                'Sohail (14 Nov 2019) -- End
                If dRow.Length > 0 Then
                    mdtFilterInfo.Rows.Remove(dRow(0))
                End If

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ").Length <= 0 Then
                '   dRow = mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ")
                If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ").Length <= 0 Then
                    dRow = mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ")
                    'Sohail (14 Nov 2019) -- End
                    If dRow.Length > 0 Then
                        mdtFilterInfo.Rows.Remove(dRow(0))
                    End If
                End If
            End If
            mdtFilterInfo.AcceptChanges()

            mdtFilterInfo = New DataView(mdtFilterInfo, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            txtSearch.Text = ""
            'Sohail (01 Aug 2019) -- End

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("chkSelect_CheckedChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        Finally

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'If mdtFilterInfo.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtFilterInfo.NewRow

            '    r.Item(0) = -111
            '    r.Item(1) = "None" ' To Hide the row and display only Row Header
            '    r.Item(2) = ""
            '    r.Item(3) = True

            '    mdtFilterInfo.Rows.Add(r)
            'End If

            'gvFilterInfo.DataSource = mdtFilterInfo
            'gvFilterInfo.DataBind()
            Call FillFilterInfo()
            'Sohail (14 Nov 2019) -- End

            popupAdvanceFilter.Show()
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If mstrEmployeeTableAlias.Trim.Length > 0 AndAlso mstrEmployeeTableAlias.EndsWith(".") = False Then
                If mstrEmployeeTableAlias.Trim.Length > 0 Then mstrEmployeeTableAlias = mstrEmployeeTableAlias & "."
            End If

            Dim dRow() As DataRow = mdtFilterInfo.Select("IsGroup = 0")
            If dRow.Length > 0 Then
                Dim mstr As String = ""
                For Each drRow As DataRow In dRow

                    Select Case CInt(drRow.Item("GroupId"))
                        Case enAdvanceFilterAllocation.BRANCH
                            If mdicFilterText.ContainsKey(rdbBranch.Text) = False Then
                                mdicFilterText.Add(rdbBranch.Text, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.stationunkid in ("
                            ElseIf mstr.Trim.Contains("stationunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.stationunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbBranch.Text) = mdicFilterText(rdbBranch.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlDept.Text
                        Case enAdvanceFilterAllocation.DEPARTMENT
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(rdbDepartment.Text) = False Then
                                mdicFilterText.Add(rdbDepartment.Text, "")
                            End If


                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.departmentunkid in ("
                            ElseIf mstr.Trim.Contains("departmentunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.departmentunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbDepartment.Text) = mdicFilterText(rdbDepartment.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlDeptGroup.Text
                        Case enAdvanceFilterAllocation.DEPARTMENT_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(rdbDepartmentGroup.Text) = False Then
                                mdicFilterText.Add(rdbDepartmentGroup.Text, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.deptgroupunkid in ("
                            ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= "  AND ADF.deptgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbDepartmentGroup.Text) = mdicFilterText(rdbDepartmentGroup.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlSection.Text
                        Case enAdvanceFilterAllocation.SECTION
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbSection.Text) = False Then
                                mdicFilterText.Add(rdbSection.Text, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.sectionunkid in ("
                            ElseIf mstr.Trim.Contains("sectionunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.sectionunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbSection.Text) = mdicFilterText(rdbSection.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlUnit.Text
                        Case enAdvanceFilterAllocation.UNIT
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbUnit.Text) = False Then
                                mdicFilterText.Add(rdbUnit.Text, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.unitunkid in ("
                            ElseIf mstr.Trim.Contains("unitunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.unitunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbUnit.Text) = mdicFilterText(rdbUnit.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlJob.Text
                        Case enAdvanceFilterAllocation.JOBS
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(rdbJob.Text) = False Then
                                mdicFilterText.Add(rdbJob.Text, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.jobunkid in ("
                            ElseIf mstr.Trim.Contains("jobunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.jobunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbJob.Text) = mdicFilterText(rdbJob.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlSectionGroup.Text
                        Case enAdvanceFilterAllocation.SECTION_GROUP
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbSectionGroup.Text) = False Then
                                mdicFilterText.Add(rdbSectionGroup.Text, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.sectiongroupunkid in ("
                            ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.sectiongroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbSectionGroup.Text) = mdicFilterText(rdbSectionGroup.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlUnitGroup.Text
                        Case enAdvanceFilterAllocation.UNIT_GROUP
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbUnitGroup.Text) = False Then
                                mdicFilterText.Add(rdbUnitGroup.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.unitgroupunkid in ("
                                'mdicFilterText.Add(tbpnlUnitGroup.Text, "")
                            ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.unitgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbUnitGroup.Text) = mdicFilterText(rdbUnitGroup.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlTeam.Text
                        Case enAdvanceFilterAllocation.TEAM
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbTeam.Text) = False Then
                                mdicFilterText.Add(rdbTeam.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.teamunkid in ("
                                'mdicFilterText.Add(tbpnlTeam.Text, "")
                            ElseIf mstr.Trim.Contains("teamunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.teamunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbTeam.Text) = mdicFilterText(rdbTeam.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlJobGroup.Text
                        Case enAdvanceFilterAllocation.JOB_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(rdbJobGroup.Text) = False Then
                                mdicFilterText.Add(rdbJobGroup.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.jobgroupunkid in ("
                                'mdicFilterText.Add(tbpnlJobGroup.Text, "")
                            ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.jobgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbJobGroup.Text) = mdicFilterText(rdbJobGroup.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlClassGroup.Text
                        Case enAdvanceFilterAllocation.CLASS_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(rdbClassGroup.Text) = False Then
                                mdicFilterText.Add(rdbClassGroup.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.classgroupunkid in ("
                                'mdicFilterText.Add(tbpnlClassGroup.Text, "")
                            ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.classgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbClassGroup.Text) = mdicFilterText(rdbClassGroup.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlClass.Text
                        Case enAdvanceFilterAllocation.CLASSES
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(rdbClass.Text) = False Then
                                mdicFilterText.Add(rdbClass.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.classunkid in ("
                                'mdicFilterText.Add(tbpnlClass.Text, "")
                            ElseIf mstr.Trim.Contains("classunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.classunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbClass.Text) = mdicFilterText(rdbClass.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGradeGroup.Text
                        Case enAdvanceFilterAllocation.GRADE_GROUP
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbGradeGroup.Text) = False Then
                                mdicFilterText.Add(rdbGradeGroup.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradegroupunkid in ("
                                'mdicFilterText.Add(tbpnlGradeGroup.Text, "")
                            ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradegroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbGradeGroup.Text) = mdicFilterText(rdbGradeGroup.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGrade.Text
                        Case enAdvanceFilterAllocation.GRADE
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbGrade.Text) = False Then
                                mdicFilterText.Add(rdbGrade.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradeunkid in ("
                                'mdicFilterText.Add(tbpnlGrade.Text, "")
                            ElseIf mstr.Trim.Contains("gradeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradeunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbGrade.Text) = mdicFilterText(rdbGrade.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGradeLevel.Text
                        Case enAdvanceFilterAllocation.GRADE_LEVEL
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbGradeLevel.Text) = False Then
                                mdicFilterText.Add(rdbGradeLevel.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradelevelunkid in ("
                                'mdicFilterText.Add(tbpnlGradeLevel.Text, "")
                            ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradelevelunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbGradeLevel.Text) = mdicFilterText(rdbGradeLevel.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlCostCenterGroup.Text
                        Case enAdvanceFilterAllocation.COST_CENTER_GROUP
                            'Sohail (14 Nov 2019) -- End

                            Dim StrIds As String = clscostcenter_master.GetCSV_CC(drRow.Item("Id").ToString)
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.costcenterunkid in ("
                                ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.costcenterunkid in ("
                                End If
                                mstr &= StrIds & ","
                            End If

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbCostCenter.Text
                        Case enAdvanceFilterAllocation.COST_CENTER
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbCostCenter.Text) = False Then
                                mdicFilterText.Add(rdbCostCenter.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.costcenterunkid in ("
                                ' mdicFilterText.Add(rdbCostCenter.Text, "")
                            ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.costcenterunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbCostCenter.Text) = mdicFilterText(rdbCostCenter.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbEmployementType.Text
                        Case enAdvanceFilterAllocation.EMPLOYEMENT_TYPE
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbEmployementType.Text) = False Then
                                mdicFilterText.Add(rdbEmployementType.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                                'mdicFilterText.Add(rdbEmployementType.Text, "")
                            ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbEmployementType.Text) = mdicFilterText(rdbEmployementType.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbReligion.Text
                        Case enAdvanceFilterAllocation.RELIGION
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbReligion.Text) = False Then
                                mdicFilterText.Add(rdbReligion.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
                                mdicFilterText.Add(rdbReligion.Text, "")
                            ElseIf mstr.Trim.Contains("religionunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbReligion.Text) = mdicFilterText(rdbReligion.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbGender.Text
                        Case enAdvanceFilterAllocation.GENDER
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbGender.Text) = False Then
                                mdicFilterText.Add(rdbGender.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "gender in ("
                                ' mdicFilterText.Add(rdbGender.Text, "")
                            ElseIf mstr.Trim.Contains("gender") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbGender.Text) = mdicFilterText(rdbGender.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbNationality.Text
                        Case enAdvanceFilterAllocation.NATIONALITY
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbNationality.Text) = False Then
                                mdicFilterText.Add(rdbNationality.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
                                'mdicFilterText.Add(rdbNationality.Text, "")
                            ElseIf mstr.Trim.Contains("nationality") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbNationality.Text) = mdicFilterText(rdbNationality.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (02 Jul 2014) -- Start
                            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbPayType.Text
                        Case enAdvanceFilterAllocation.PAY_TYPE
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbPayType.Text) = False Then
                                mdicFilterText.Add(rdbPayType.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
                                'mdicFilterText.Add(rdbPayType.Text, "")
                            ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbPayType.Text) = mdicFilterText(rdbPayType.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbPayPoint.Text
                        Case enAdvanceFilterAllocation.PAY_POINT
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(rdbPayPoint.Text) = False Then
                                mdicFilterText.Add(rdbPayPoint.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
                                'mdicFilterText.Add(rdbPayPoint.Text, "")
                            ElseIf mstr.Trim.Contains("paypointunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbPayPoint.Text) = mdicFilterText(rdbPayPoint.Text).ToString() & drRow.Item("Name").ToString() & ","
                            'Sohail (02 Jul 2014) -- End

                            'S.SANDEEP |04-JUN-2019| -- START
                            'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case rdbMaritalStatus.Text
                        Case enAdvanceFilterAllocation.MARITAL_STATUS
                            'Sohail (14 Nov 2019) -- End


                            If mdicFilterText.ContainsKey(rdbMaritalStatus.Text) = False Then
                                mdicFilterText.Add(rdbMaritalStatus.Text, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
                                'mdicFilterText.Add(rdbMaritalStatus.Text, "")
                            ElseIf mstr.Trim.Contains("maritalstatusunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(rdbMaritalStatus.Text) = mdicFilterText(rdbMaritalStatus.Text).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlRelationFromDpndt.Text
                        Case enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS
                            'Sohail (14 Nov 2019) -- End
                            Dim StrIds As String = clsDependants_Beneficiary_tran.GetCSV_EmpIds(CInt(drRow.Item("Id").ToString))
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= StrIds & ","
                            Else
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= "0,"
                            End If
                            'S.SANDEEP |04-JUN-2019| -- END

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                        Case enAdvanceFilterAllocation.SKILL
                            Dim StrIds As String = clsEmployee_Skill_Tran.GetCSV_EmpIds(CInt(drRow.Item("Id").ToString))
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= StrIds & ","
                            Else
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= "0,"
                            End If
                            'Sohail (14 Nov 2019) -- End

                    End Select
                    'S.SANDEEP [18 DEC 2015] -- END


                Next
                If mstr.Trim <> "" Then
                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                Else
                    mstr = " 1 = 1 "
                End If
                mstrFilterString = mstr
            End If

            RaiseEvent buttonApply_Click(sender, e)
            popupAdvanceFilter.Hide()
            popupAdvanceFilter.Dispose()

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnApply_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mstrFilterString = ""
            mstrEmployeeTableAlias = ""

            RaiseEvent buttonClose_Click(sender, e)
            popupAdvanceFilter.Dispose()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnClose_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " GridView's Events "

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(DataBinder.Eval(e.Row.DataItem, "Id")) = -111 AndAlso DataBinder.Eval(e.Row.DataItem, "Name").ToString = "None" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvDetails_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub gvFilterInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFilterInfo.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(DataBinder.Eval(e.Row.DataItem, "Id")) = -111 AndAlso DataBinder.Eval(e.Row.DataItem, "Name").ToString = "None" AndAlso DataBinder.Eval(e.Row.DataItem, "GroupName").ToString = "" Then
                    e.Row.Visible = False
                Else
                    If CBool(DataBinder.Eval(e.Row.DataItem, "IsGroup")) = True Then
                        e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "GroupName").ToString
                        e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        e.Row.Cells(1).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End
                    End If
                End If
            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvFilterInfo_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " Radio Button's Events "
    Protected Sub rdb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBranch.CheckedChanged, rdbClass.CheckedChanged, _
                                                                                                  rdbClassGroup.CheckedChanged, rdbCostCenter.CheckedChanged, _
                                                                                                  rdbCostCenterGroup.CheckedChanged, rdbDepartment.CheckedChanged, _
                                                                                                  rdbDepartmentGroup.CheckedChanged, rdbGender.CheckedChanged, _
                                                                                                  rdbGrade.CheckedChanged, rdbGradeGroup.CheckedChanged, _
                                                                                                  rdbGradeLevel.CheckedChanged, rdbJob.CheckedChanged, _
                                                                                                  rdbJobGroup.CheckedChanged, rdbMaritalStatus.CheckedChanged, _
                                                                                                  rdbNationality.CheckedChanged, rdbPayPoint.CheckedChanged, _
                                                                                                  rdbPayType.CheckedChanged, rdbRelationfromDependents.CheckedChanged, _
                                                                                                  rdbReligion.CheckedChanged, rdbSection.CheckedChanged, _
                                                                                                  rdbSectionGroup.CheckedChanged, rdbSkill.CheckedChanged, _
                                                                                                  rdbTeam.CheckedChanged, rdbUnit.CheckedChanged, _
                                                                                                  rdbUnitGroup.CheckedChanged, rdbEmployementType.CheckedChanged


        Dim intColType As Integer = 0
        Try
            Dim radio As RadioButton

            radio = CType(sender, RadioButton)

            mstrActiveGroup = radio.Text

            If radio.ID = rdbBranch.ID Then
                Dim objbranch As New clsStation
                mdsList = objbranch.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.BRANCH

            ElseIf radio.ID = rdbDepartmentGroup.ID Then
                Dim objdeptgrp As New clsDepartmentGroup
                mdsList = objdeptgrp.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT_GROUP

            ElseIf radio.ID = rdbDepartment.ID Then
                Dim objdept As New clsDepartment
                mdsList = objdept.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT

            ElseIf radio.ID = rdbSectionGroup.ID Then
                Dim objsg As New clsSectionGroup
                mdsList = objsg.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION_GROUP

            ElseIf radio.ID = rdbSection.ID Then
                Dim objsection As New clsSections
                mdsList = objsection.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION

            ElseIf radio.ID = rdbUnitGroup.ID Then
                Dim objug As New clsUnitGroup
                mdsList = objug.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT_GROUP

            ElseIf radio.ID = rdbUnit.ID Then
                Dim objunit As New clsUnits
                mdsList = objunit.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT

            ElseIf radio.ID = rdbTeam.ID Then
                Dim objteam As New clsTeams
                mdsList = objteam.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.TEAM

            ElseIf radio.ID = rdbJobGroup.ID Then
                Dim objjobgrp As New clsJobGroup
                mdsList = objjobgrp.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.JOB_GROUP

            ElseIf radio.ID = rdbJob.ID Then
                Dim objjobs As New clsJobs
                mdsList = objjobs.GetList("list", True)
                intColType = 1
                mintSelectedAllocationID = enAdvanceFilterAllocation.JOBS

            ElseIf radio.ID = rdbClassGroup.ID Then
                Dim objclassgrp As New clsClassGroup
                mdsList = objclassgrp.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.CLASS_GROUP

            ElseIf radio.ID = rdbClass.ID Then
                Dim objclass As New clsClass
                mdsList = objclass.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.CLASSES

            ElseIf radio.ID = rdbGradeGroup.ID Then
                Dim objgradegrp As New clsGradeGroup
                mdsList = objgradegrp.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_GROUP

            ElseIf radio.ID = rdbGrade.ID Then
                Dim objgrade As New clsGrade
                mdsList = objgrade.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE

            ElseIf radio.ID = rdbGradeLevel.ID Then
                Dim objgradelvl As New clsGradeLevel
                mdsList = objgradelvl.GetList("list", True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_LEVEL

            ElseIf radio.ID = rdbCostCenterGroup.ID Then
                Dim objccg As New clspayrollgroup_master
                mdsList = objccg.getListForCombo(enPayrollGroupType.CostCenter, "list")
                mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER_GROUP

            ElseIf radio.ID = rdbCostCenter.ID Then
                Dim objconstcenter As New clscostcenter_master
                mdsList = objconstcenter.GetList("list", True)
                intColType = 2
                mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER

            ElseIf radio.ID = rdbEmployementType.ID Then
                Dim objemptype As New clsCommon_Master
                mdsList = objemptype.GetList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, "list", -1, True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.EMPLOYEMENT_TYPE

            ElseIf radio.ID = rdbReligion.ID Then
                Dim objreligion As New clsCommon_Master
                mdsList = objreligion.GetList(clsCommon_Master.enCommonMaster.RELIGION, "list", -1, True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.RELIGION

            ElseIf radio.ID = rdbGender.ID Then
                Dim objgender As New clsMasterData
                mdsList = objgender.getGenderList("gender")
                intColType = 3
                mintSelectedAllocationID = enAdvanceFilterAllocation.GENDER

            ElseIf radio.ID = rdbNationality.ID Then
                Dim objnationality As New clsMasterData
                mdsList = objnationality.getCountryList("nationality", )
                intColType = 4
                mintSelectedAllocationID = enAdvanceFilterAllocation.NATIONALITY

            ElseIf radio.ID = rdbPayType.ID Then
                Dim objpaytype As New clsCommon_Master
                mdsList = objpaytype.GetList(clsCommon_Master.enCommonMaster.PAY_TYPE, "list", -1, True)
                mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_TYPE

            ElseIf radio.ID = rdbPayPoint.ID Then
                Dim objpaypoint As New clspaypoint_master
                mdsList = objpaypoint.GetList("list", True)
                intColType = 5
                mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_POINT

            ElseIf radio.ID = rdbMaritalStatus.ID Then
                Dim objcommonmst As New clsCommon_Master
                mdsList = objcommonmst.GetList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, "list", -1, True)
                intColType = 6
                objcommonmst = Nothing
                mintSelectedAllocationID = enAdvanceFilterAllocation.MARITAL_STATUS

            ElseIf radio.ID = rdbRelationfromDependents.ID Then
                Dim objcommonmst As New clsCommon_Master
                mdsList = objcommonmst.GetList(clsCommon_Master.enCommonMaster.RELATIONS, "list", -1, True)
                intColType = 7
                objcommonmst = Nothing
                mintSelectedAllocationID = enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS

            ElseIf radio.ID = rdbSkill.ID Then
                Dim objskill As New clsskill_master
                mdsList = objskill.getComboList("list", False)
                mintSelectedAllocationID = enAdvanceFilterAllocation.SKILL
                objskill = Nothing
                intColType = 8

            End If

            If intColType = 0 Then
                mdsList.Tables(0).Columns(0).ColumnName = "id"
                mdsList.Tables(0).Columns("name").ColumnName = "name"
            ElseIf intColType = 1 Then
                mdsList.Tables(0).Columns("jobunkid").ColumnName = "id"
                mdsList.Tables(0).Columns("jobname").ColumnName = "name"
            ElseIf intColType = 2 Then
                mdsList.Tables(0).Columns(0).ColumnName = "id"
                mdsList.Tables(0).Columns("costcentername").ColumnName = "name"
            ElseIf intColType = 3 Then
                mdsList.Tables(0).Columns(1).ColumnName = "id"
                mdsList.Tables(0).Columns(0).ColumnName = "name"
            ElseIf intColType = 4 Then
                mdsList.Tables(0).Columns(0).ColumnName = "id"
                mdsList.Tables(0).Columns("country_name").ColumnName = "name"
            ElseIf intColType = 5 Then
                mdsList.Tables(0).Columns(0).ColumnName = "id"
                mdsList.Tables(0).Columns("paypointname").ColumnName = "name"
            ElseIf intColType = 6 Then
                mdsList.Tables(0).Columns("masterunkid").ColumnName = "id"
                mdsList.Tables(0).Columns("name").ColumnName = "name"
            ElseIf intColType = 7 Then
                mdsList.Tables(0).Columns("masterunkid").ColumnName = "id"
                mdsList.Tables(0).Columns("name").ColumnName = "name"
            ElseIf intColType = 8 Then
                mdsList.Tables(0).Columns("skillunkid").ColumnName = "id"
                mdsList.Tables(0).Columns("name").ColumnName = "name"

            End If

            Dim dtcol As New DataColumn
            dtcol.ColumnName = "ischeck"
            dtcol.Caption = ""
            dtcol.DataType = GetType(System.Boolean)
            dtcol.DefaultValue = False
            mdsList.Tables(0).Columns.Add(dtcol)

            If mdtFilterInfo IsNot Nothing Then
                Dim allid As List(Of String) = (From p In mdtFilterInfo Where CInt(p.Item("groupid")) = mintSelectedAllocationID Select (p.Item("id").ToString)).ToList

                Dim strids As String = String.Join(",", allid.ToArray)
                If strids.Trim <> "" Then
                    Dim drow() As DataRow = mdsList.Tables(0).Select("id in (" & strids.Trim & ") ")
                    For Each drrow As DataRow In drow
                        drrow.Item("ischeck") = True
                    Next
                    mdsList.Tables(0).AcceptChanges()
                End If
            End If
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally

            If mdsList.Tables(0).Rows.Count <= 0 Then
                Dim r As DataRow = mdsList.Tables(0).NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None"

                mdsList.Tables(0).Rows.Add(r)
            End If

            Dim dtTable As DataTable = New DataView(mdsList.Tables(0), "", "IsCheck DESC, Name", DataViewRowState.CurrentRows).ToTable
            mdsList.Tables.Clear()
            mdsList.Tables.Add(dtTable)

            gvDetails.DataSource = mdsList.Tables(0)
            gvDetails.DataBind()

            popupAdvanceFilter.Show()
        End Try


    End Sub

#End Region

#Region " Tab Control's Events "

    'Protected Sub TabContainer1_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabContainer1.ActiveTabChanged
    '    Dim intColType As Integer = 0
    '    Try
    '        If TabContainer1.ActiveTab Is tbpnlBranch Then
    '            Dim objBranch As New clsStation
    '            mdsList = objBranch.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.BRANCH
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlDeptGroup Then
    '            Dim objDeptGrp As New clsDepartmentGroup
    '            mdsList = objDeptGrp.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlDept Then
    '            Dim objDept As New clsDepartment
    '            mdsList = objDept.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlSectionGroup Then
    '            Dim objSG As New clsSectionGroup
    '            mdsList = objSG.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlSection Then
    '            Dim objSection As New clsSections
    '            mdsList = objSection.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlUnitGroup Then
    '            Dim objUG As New clsUnitGroup
    '            mdsList = objUG.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlUnit Then
    '            Dim objUnit As New clsUnits
    '            mdsList = objUnit.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlTeam Then
    '            Dim objTeam As New clsTeams
    '            mdsList = objTeam.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.TEAM
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlJobGroup Then
    '            Dim objjobGRP As New clsJobGroup
    '            mdsList = objjobGRP.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.JOB_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlJob Then
    '            Dim objJobs As New clsJobs
    '            mdsList = objJobs.GetList("List", True)
    '            intColType = 1
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.JOBS
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlClassGroup Then
    '            Dim objClassGrp As New clsClassGroup
    '            mdsList = objClassGrp.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.CLASS_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlClass Then
    '            Dim objClass As New clsClass
    '            mdsList = objClass.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.CLASSES
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlGradeGroup Then
    '            Dim objGradeGrp As New clsGradeGroup
    '            mdsList = objGradeGrp.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlGrade Then
    '            Dim objGrade As New clsGrade
    '            mdsList = objGrade.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlGradeLevel Then
    '            Dim objGradeLvl As New clsGradeLevel
    '            mdsList = objGradeLvl.GetList("List", True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_LEVEL
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlCostCenterGroup Then
    '            Dim objCCG As New clspayrollgroup_master
    '            mdsList = objCCG.getListForCombo(enPayrollGroupType.CostCenter, "List")
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER_GROUP
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlCostCenter Then
    '            Dim objConstCenter As New clscostcenter_master
    '            mdsList = objConstCenter.GetList("List", True)
    '            intColType = 2
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlEmployementType Then
    '            Dim objEmpType As New clsCommon_Master
    '            mdsList = objEmpType.GetList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, "List", -1, True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.EMPLOYEMENT_TYPE
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlReligion Then
    '            Dim objReligion As New clsCommon_Master
    '            mdsList = objReligion.GetList(clsCommon_Master.enCommonMaster.RELIGION, "List", -1, True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.RELIGION
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlGender Then
    '            Dim objGender As New clsMasterData
    '            mdsList = objGender.getGenderList("Gender")
    '            intColType = 3
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.GENDER
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlNationality Then
    '            Dim objNationality As New clsMasterData
    '            mdsList = objNationality.getCountryList("Nationality", )
    '            intColType = 4
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.NATIONALITY
    '            'Sohail (14 Nov 2019) -- End
    '            'Sohail (02 Jul 2014) -- Start
    '            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
    '        ElseIf TabContainer1.ActiveTab Is tbpnlPayType Then
    '            Dim objPayType As New clsCommon_Master
    '            mdsList = objPayType.GetList(clsCommon_Master.enCommonMaster.PAY_TYPE, "List", -1, True)
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_TYPE
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlPayPoint Then
    '            Dim objPayPoint As New clspaypoint_master
    '            mdsList = objPayPoint.GetList("List", True)
    '            intColType = 5
    '            'Sohail (02 Jul 2014) -- End
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_POINT
    '            'Sohail (14 Nov 2019) -- End
    '            'S.SANDEEP |04-JUN-2019| -- START
    '            'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
    '        ElseIf TabContainer1.ActiveTab Is tbpnlMaritalStatus Then
    '            Dim objCommonMst As New clsCommon_Master
    '            mdsList = objCommonMst.GetList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, "List", -1, True)
    '            intColType = 6
    '            objCommonMst = Nothing
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.MARITAL_STATUS
    '            'Sohail (14 Nov 2019) -- End
    '        ElseIf TabContainer1.ActiveTab Is tbpnlRelationFromDpndt Then
    '            Dim objCommonMst As New clsCommon_Master
    '            mdsList = objCommonMst.GetList(clsCommon_Master.enCommonMaster.RELATIONS, "List", -1, True)
    '            intColType = 7
    '            objCommonMst = Nothing
    '            'S.SANDEEP |04-JUN-2019| -- END
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS

    '        ElseIf TabContainer1.ActiveTab Is tbpnlSkill Then
    '            Dim objSkill As New clsskill_master
    '            mdsList = objSkill.getComboList("List", False)
    '            mintSelectedAllocationID = enAdvanceFilterAllocation.SKILL
    '            objSkill = Nothing
    '            intColType = 8
    '            'Sohail (14 Nov 2019) -- End
    '        End If

    '        If intColType = 0 Then
    '            mdsList.Tables(0).Columns(0).ColumnName = "Id"
    '            mdsList.Tables(0).Columns("name").ColumnName = "Name"
    '        ElseIf intColType = 1 Then
    '            'Nilay (15-Dec-2015) -- Start
    '            'mdsList.Tables(0).Columns("jobgroupunkid").ColumnName = "Id"
    '            'mdsList.Tables(0).Columns("jobname").ColumnName = "Name"
    '            mdsList.Tables(0).Columns("jobunkid").ColumnName = "Id"
    '            mdsList.Tables(0).Columns("jobname").ColumnName = "Name"
    '            'Nilay (15-Dec-2015) -- End
    '        ElseIf intColType = 2 Then
    '            mdsList.Tables(0).Columns(0).ColumnName = "Id"
    '            mdsList.Tables(0).Columns("costcentername").ColumnName = "Name"
    '        ElseIf intColType = 3 Then
    '            mdsList.Tables(0).Columns(1).ColumnName = "Id"
    '            mdsList.Tables(0).Columns(0).ColumnName = "Name"
    '        ElseIf intColType = 4 Then
    '            mdsList.Tables(0).Columns(0).ColumnName = "Id"
    '            mdsList.Tables(0).Columns("country_name").ColumnName = "Name"
    '            'Sohail (02 Jul 2014) -- Start
    '            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
    '        ElseIf intColType = 5 Then
    '            mdsList.Tables(0).Columns(0).ColumnName = "Id"
    '            mdsList.Tables(0).Columns("paypointname").ColumnName = "Name"
    '            'Sohail (02 Jul 2014) -- End

    '            'S.SANDEEP |04-JUN-2019| -- START
    '            'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
    '        ElseIf intColType = 6 Then
    '            mdsList.Tables(0).Columns("masterunkid").ColumnName = "Id"
    '            mdsList.Tables(0).Columns("name").ColumnName = "Name"
    '        ElseIf intColType = 7 Then
    '            mdsList.Tables(0).Columns("masterunkid").ColumnName = "Id"
    '            mdsList.Tables(0).Columns("name").ColumnName = "Name"
    '            'S.SANDEEP |04-JUN-2019| -- END
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '        ElseIf intColType = 8 Then
    '            mdsList.Tables(0).Columns("skillunkid").ColumnName = "Id"
    '            mdsList.Tables(0).Columns("name").ColumnName = "Name"
    '            'Sohail (14 Nov 2019) -- End
    '        End If

    '        Dim dtCol As New DataColumn
    '        dtCol.ColumnName = "IsCheck"
    '        dtCol.Caption = ""
    '        dtCol.DataType = System.Type.GetType("System.Boolean")
    '        dtCol.DefaultValue = False
    '        mdsList.Tables(0).Columns.Add(dtCol)

    '        If mdtFilterInfo IsNot Nothing Then
    '            'Sohail (14 Nov 2019) -- Start
    '            'NMB UAT Enhancement # : Bind Allocation to training course.
    '            'Dim allID As List(Of String) = (From p In mdtFilterInfo Where p.Item("GroupName") = TabContainer1.ActiveTab.HeaderText Select (p.Item("Id").ToString)).ToList
    '            Dim allID As List(Of String) = (From p In mdtFilterInfo Where CInt(p.Item("GroupId")) = mintSelectedAllocationID Select (p.Item("Id").ToString)).ToList
    '            'Sohail (14 Nov 2019) -- End
    '            Dim strIDs As String = String.Join(",", allID.ToArray)
    '            If strIDs.Trim <> "" Then
    '                Dim dRow() As DataRow = mdsList.Tables(0).Select("Id IN (" & strIDs.Trim & ") ")
    '                For Each drRow As DataRow In dRow
    '                    drRow.Item("IsCheck") = True
    '                Next
    '                mdsList.Tables(0).AcceptChanges()
    '            End If

    '        End If


    '    Catch ex As Exception
    '        'Pinkal (20-Aug-2020) -- Start
    '        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    '        'Throw New Exception("TabContainer1_ActiveTabChanged :- " & ex.Message)
    '        CommonCodes.LogErrorOnly(ex)
    '        'Pinkal (20-Aug-2020) -- End
    '    Finally

    '        If mdsList.Tables(0).Rows.Count <= 0 Then
    '            Dim r As DataRow = mdsList.Tables(0).NewRow

    '            r.Item("Id") = -111
    '            r.Item("Name") = "None" ' To Hide the row and display only Row Header

    '            mdsList.Tables(0).Rows.Add(r)
    '        End If

    '        'Sohail (01 Aug 2019) -- Start
    '        'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    '        Dim dtTable As DataTable = New DataView(mdsList.Tables(0), "", "IsCheck DESC, Name", DataViewRowState.CurrentRows).ToTable
    '        mdsList.Tables.Clear()
    '        mdsList.Tables.Add(dtTable)
    '        'Sohail (01 Aug 2019) -- End

    '        gvDetails.DataSource = mdsList.Tables(0)
    '        gvDetails.DataBind()

    '        popupAdvanceFilter.Show()
    '    End Try
    'End Sub

#End Region


    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnApply.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnApply.ID, Me.btnApply.Text).Replace("&", "")
            'Me.tbpnlBranch.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnBranch", Me.tbpnlBranch.HeaderText)
            'Me.tbpnlDeptGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnDeptGrp", Me.tbpnlDeptGroup.HeaderText)
            'Me.tbpnlDept.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnDept", Me.tbpnlDept.HeaderText)
            'Me.tbpnlSection.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnSection", Me.tbpnlSection.HeaderText)
            'Me.tbpnlUnit.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnUnit", Me.tbpnlUnit.HeaderText)
            'Me.tbpnlJob.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnJob", Me.tbpnlJob.HeaderText)
            'Me.tbpnlSectionGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnSectionGroup", Me.tbpnlSectionGroup.HeaderText)
            'Me.tbpnlUnitGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnUnitGroup", Me.tbpnlUnitGroup.HeaderText)
            'Me.tbpnlTeam.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnTeam", Me.tbpnlTeam.HeaderText)
            'Me.tbpnlClass.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnClass", Me.tbpnlClass.HeaderText)
            'Me.tbpnlClassGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnClassGroup", Me.tbpnlClassGroup.HeaderText)
            'Me.tbpnlJobGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnJobGroup", Me.tbpnlJobGroup.HeaderText)
            'Me.tbpnlGradeGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGradeGroup", Me.tbpnlGradeGroup.HeaderText)
            'Me.tbpnlGrade.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGrade", Me.tbpnlGrade.HeaderText)
            'Me.tbpnlGradeLevel.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGradeLevel", Me.tbpnlGradeLevel.HeaderText)
            'Me.tbpnlCostCenterGroup.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnCostCenterGroup", Me.tbpnlCostCenterGroup.HeaderText)
            'Me.tbpnlCostCenter.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnCostCenter", Me.tbpnlCostCenter.HeaderText)
            'Me.tbpnlEmployementType.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnEmployementType", Me.tbpnlEmployementType.HeaderText)
            'Me.tbpnlReligion.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnReligion", Me.tbpnlReligion.HeaderText)
            'Me.tbpnlGender.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGender", Me.tbpnlGender.HeaderText)
            'Me.tbpnlNationality.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnNationality", Me.tbpnlNationality.HeaderText)
            'Me.tbpnlPayType.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnPayType", Me.tbpnlPayType.HeaderText)
            'Me.tbpnlPayPoint.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnPayPoint", Me.tbpnlPayPoint.HeaderText)

            'Me.tbpnlMaritalStatus.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnMaritalStatus.ID, Me.tbpnlMaritalStatus.HeaderText) -> this is not in SS
            'Hemant (25 Jan 2021) -- Start
            'Enhancement - Point # 8(Succession Testing Doc) : Provide allocation/prompt messages and email notifications on language to allow renaming as per NMB allocation names
            Me.rdbBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnBranch", Me.rdbBranch.Text)
            Me.rdbDepartmentGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnDeptGrp", Me.rdbDepartmentGroup.Text)
            Me.rdbDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnDept", Me.rdbDepartment.Text)
            Me.rdbSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnSection", Me.rdbSection.Text)
            Me.rdbUnit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnUnit", Me.rdbUnit.Text)
            Me.rdbJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnJob", Me.rdbJob.Text)
            Me.rdbSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnSectionGroup", Me.rdbSectionGroup.Text)
            Me.rdbUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnUnitGroup", Me.rdbUnitGroup.Text)
            Me.rdbTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnTeam", Me.rdbTeam.Text)
            Me.rdbClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnClass", Me.rdbClass.Text)
            Me.rdbClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnClassGroup", Me.rdbClassGroup.Text)
            Me.rdbJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnJobGroup", Me.rdbJobGroup.Text)
            Me.rdbGradeGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGradeGroup", Me.rdbGradeGroup.Text)
            Me.rdbGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGrade", Me.rdbGrade.Text)
            Me.rdbGradeLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGradeLevel", Me.rdbGradeLevel.Text)
            Me.rdbCostCenterGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnCostCenterGroup", Me.rdbCostCenterGroup.Text)
            Me.rdbCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnCostCenter", Me.rdbCostCenter.Text)
            Me.rdbEmployementType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnEmployementType", Me.rdbEmployementType.Text)
            Me.rdbReligion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnReligion", Me.rdbReligion.Text)
            Me.rdbGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnGender", Me.rdbGender.Text)
            Me.rdbNationality.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnNationality", Me.rdbNationality.Text)
            Me.rdbPayType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnPayType", Me.rdbPayType.Text)
            Me.rdbPayPoint.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnPayPoint", Me.rdbPayPoint.Text)
            Me.rdbMaritalStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnMaritalStatus", Me.rdbMaritalStatus.Text)
            Me.rdbRelationfromDependents.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"btnRelationFromDpndt", Me.rdbRelationfromDependents.Text)
            'Hemant (25 Jan 2021) -- End

            gvDetails.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvDetails.Columns(2).FooterText, gvDetails.Columns(2).HeaderText)
            gvFilterInfo.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvFilterInfo.Columns(1).FooterText, gvFilterInfo.Columns(1).HeaderText)


        Catch Ex As Exception
            'DisplayMessage.DisplayError(ex, Me)
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(Ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

End Class
