﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PreviewAttachment.ascx.vb"
    Inherits="Controls_PreviewAttachment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<script type="text/javascript">
    function opennewtab(url) {
        if (url != '') {
            window.open(url);
        }
    }
    
</script>--%>
<ajaxToolkit:ModalPopupExtender ID="popup_ShowAttchment" runat="server" BackgroundCssClass="modal-backdrop bd-l5"
    CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ShowAttchment" TargetControlID="hdf_ScanAttchment">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnl_ShowAttchment" runat="server" CssClass="card modal-dialog modal-l5" Style="display: none;">
    <div class="header">
        <h2>
            <asp:Label ID="lblAttchmentheader" runat="server" Text="View Scan/Attchment"></asp:Label>
        </h2>
    </div>
    <div class="body">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <asp:Image ID="imgAttchment" runat="server" Width="100%" Height="100%" Visible="true" />
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive" style="height: 300px;">
                    <asp:DataGrid ID="lvPreviewList" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                        CssClass="table table-hover table-bordered">
                        <Columns>
                            <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                        CommandName="Preview">
                                                                <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="code" HeaderText="Code" FooterText="colhCode" />
                            <asp:BoundColumn DataField="names" HeaderText="Employee/Applicant" FooterText="colhName" />
                            <asp:BoundColumn DataField="document" HeaderText="Document" FooterText="colhDocument" />
                            <asp:BoundColumn DataField="filename" HeaderText="Filename" FooterText="colhFileName" />
                            <asp:BoundColumn DataField="doctype" Visible="false" FooterText="objcolhDocType" />
                            <asp:BoundColumn DataField="filepath" Visible="false" FooterText="objcolhFullPath" />
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <asp:Button ID="btnDownloadView" runat="server" Text="Download & View" CssClass="btn btn-primary" />
        <asp:Button ID="btnAttchmentClose" runat="server" Text="Close" CssClass="btn btn-default" />
        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
    </div>
    <%--    <div class="panel-primary" style="margin-bottom: 0px">
        <div class="panel-heading">
            
        </div>
        <div class="panel-body">
            <div id="Div4" class="panel-default">
                <div id="Div5" class="panel-heading-default">
                    <div style="float: left;">
                        <asp:Label ID="lblHeader" runat="server" Text="View Scan/Attchment"></asp:Label>
                    </div>
                </div>
                <div id="Div6" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%; display: none">
                            <td style="width: 100%; height: 300px">
                                
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%;">
                                <asp:Panel ID="pnl_lvPreviewList" runat="server" Style="width: 550px; max-height: 150px"
                                    ScrollBars="Auto">
                                  
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Panel>

<script>
    var prm;
    prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(endRequestHandler);
    function endRequestHandler(sender, evemt) {
        $(".csspreview").css("z-index", "100013");
    }
</script>

