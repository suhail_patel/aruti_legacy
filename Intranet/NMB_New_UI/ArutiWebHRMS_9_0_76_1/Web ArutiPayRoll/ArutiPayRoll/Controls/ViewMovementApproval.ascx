﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewMovementApproval.ascx.vb"
    Inherits="Controls_ViewMovementApproval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popup_ViewMovementApproval" runat="server" BackgroundCssClass="modal-backdrop"
    CancelControlID="lblTitle" PopupControlID="pnl_ViewMovementApproval" TargetControlID="hdf_ViewMovementApproval">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnl_ViewMovementApproval" runat="server" CssClass="card modal-dialog modal-lg"
    Style="display: none;">
    <div class="header">
        <h2>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </h2>
        </div>
    <div class="body">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive" style="height: 350px;">
                            <asp:GridView ID="gvApproveRejectMovement" runat="server" AutoGenerateColumns="False"
                        Width="99%" CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="icheck">
                                <Columns>
                                    <asp:BoundField HeaderText="Code" DataField="ecode" FooterText="dgcolhecode" />
                                    <asp:BoundField HeaderText="Employee" DataField="ename" FooterText="dgcolhename" />
                                    <asp:BoundField HeaderText="Effective Date" HeaderStyle-Width="130px" DataField="EffDate"
                                        FooterText="dgcolhEffDate" />
                                    <asp:BoundField HeaderText="Branch" DataField="branch" FooterText="dgcolhbranch" />
                                    <asp:BoundField HeaderText="Department Group" DataField="deptgroup" FooterText="dgcolhdeptgroup" />
                                    <asp:BoundField HeaderText="Department" DataField="dept" FooterText="dgcolhdept" />
                                    <asp:BoundField HeaderText="Section Group" DataField="secgroup" FooterText="dgcolhsecgroup" />
                                    <asp:BoundField HeaderText="Section" DataField="section" FooterText="dgcolhsection" />
                                    <asp:BoundField HeaderText="Unit Group" DataField="unitgrp" FooterText="dgcolhunitgrp" />
                                    <asp:BoundField HeaderText="Unit" DataField="unit" FooterText="dgcolhunit" />
                                    <asp:BoundField HeaderText="Team" DataField="team" FooterText="dgcolhteam" />
                                    <asp:BoundField HeaderText="Class Group" DataField="classgrp" FooterText="dgcolhclassgrp" />
                                    <asp:BoundField HeaderText="Class" DataField="class" FooterText="dgcolhclass" />
                                    <asp:BoundField HeaderText="Reason" DataField="CReason" FooterText="dgcolhCReason" />
                                    <asp:BoundField HeaderText="Job Group" DataField="JobGroup" FooterText="dgcolhJobGroup" />
                                    <asp:BoundField HeaderText="Job" DataField="Job" FooterText="dgcolhJob" />
                                    <asp:BoundField HeaderText="Date1" DataField="dDate1" FooterText="dgcolhdDate1" />
                                    <asp:BoundField HeaderText="Date2" DataField="dDate2" FooterText="dgcolhdDate2" />
                                    <asp:BoundField HeaderText="Permit No." DataField="work_permit_no" FooterText="dgcolhwork_permit_no" />
                                    <asp:BoundField HeaderText="Issue Place" DataField="issue_place" FooterText="dgcolhissue_place" />
                                    <asp:BoundField HeaderText="Issue Date" DataField="IDate" FooterText="dgcolhIDate" />
                                    <asp:BoundField HeaderText="Expiry Date" DataField="EDate" FooterText="dgcolhExDate" />
                                    <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhCountry" />
                                    <asp:BoundField HeaderText="CostCenter" DataField="DispValue" FooterText="dgcolhDispValue" />
                                    <asp:BoundField HeaderText="Approver" DataField="username" FooterText="dgcolhApprover" />
                                    <asp:BoundField HeaderText="Level" DataField="levelname" FooterText="dgcolhLevel" />
                                    <asp:BoundField HeaderText="Status" DataField="iStatus" FooterText="dgcolhStatus" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
            </div>
        </div>
    <div class="footer">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
        <asp:HiddenField ID="hdf_ViewMovementApproval" runat="server" />
    </div>
</asp:Panel>
