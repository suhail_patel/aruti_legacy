﻿Imports System.Data
Imports System.Globalization
Imports Aruti.Data

Partial Class Controls_DateCtrl
    Inherits System.Web.UI.UserControl
    Dim clsdataopr As New eZeeCommonLib.clsDataOperation(True)
    Public Event objbtnDateReminder_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    'Private pMaxDate As Date
    'Public Property MaxDate() As Date
    '    Get
    '        Return pMaxDate
    '    End Get
    '    Set(ByVal value As Date)
    '        pMaxDate = value
    '    End Set
    'End Property

    'Private pMinDate As Date
    'Public Property MinDate() As Date
    '    Get
    '        Return pMinDate
    '    End Get
    '    Set(ByVal value As Date)
    '        pMinDate = value
    '    End Set
    'End Property

    'Public WriteOnly Property SetDate() As String
    '    Set(ByVal value As String)
    '        TxtDate.Text = IIf(IsDate(value), value, CDate("01/01/2000"))
    '    End Set
    'End Property

    Public WriteOnly Property SetDate() As Date
        Set(ByVal value As Date)

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Try


                'Pinkal (20-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If Session("DateFormat") IsNot Nothing AndAlso Session("DateSeparator") IsNot Nothing Then
            Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
            NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
            NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
            System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture
            calendarButtonExtender.Format = Session("DateFormat")
                End If
                'Pinkal (20-Aug-2020) -- End


            If (value = Nothing) Then
                TxtDate.Text = ""
            Else
                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    'TxtDate.Text = IIf(IsDate(value), value.ToString(calendarButtonExtender.Format), CDate("01/01/1900"))
                    TxtDate.Text = If(IsDate(value), value.ToString(calendarButtonExtender.Format), Format(New DateTime(1900, 1, 1), calendarButtonExtender.Format))
                    'Pinkal (13-Aug-2020) -- End
                End If
            Catch ex As Exception
                Dim objErrorLog As New clsErrorlog_Tran
                With objErrorLog
                    Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : SetDate : " & ex.Message & "; " & ex.StackTrace.ToString
                    If ex.InnerException IsNot Nothing Then
                        S_dispmsg &= "; " & ex.InnerException.Message
                    End If
                    S_dispmsg = S_dispmsg.Replace("'", "")
                    S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                    ._Error_Message = S_dispmsg
                    ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                    ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                    ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                    If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                        ._Database_Version = ""
                    Else
                        ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                    End If
                    If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                        ._Userunkid = HttpContext.Current.Session("UserId")
                        ._Loginemployeeunkid = 0
                    Else
                        ._Userunkid = 0
                        ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
            End If
                    ._Isemailsent = False
                    ._Isweb = True
                    ._Isvoid = False
                    ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                    ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
                End With
                Dim strDBName As String = ""
                If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                    If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                        strDBName = "hrmsConfiguration"
                    End If
                End If
                If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

                End If
            End Try
            'Pinkal (13-Aug-2020) -- End
        End Set
    End Property

    Public ReadOnly Property GetDate() As Date
        Get
            Try
                'Return IIf(IsDate(TxtDate.Text), TxtDate.Text, CDate("01/01/2000"))


                'Pinkal (20-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If Session("DateFormat") IsNot Nothing AndAlso Session("DateSeparator") IsNot Nothing Then
                Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
                NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
                NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture
                calendarButtonExtender.Format = Session("DateFormat")
                End If
                'Pinkal (20-Aug-2020) -- End

                If IsNull() = False Then

                    'Pinkal (07-Aug-2020) -- Start
                    'Resolved Date Control Problem.
                    If TxtDate.Text.Trim.Length > 0 Then
                        If IsDate(TxtDate.Text.Trim) Then
                        Return DateTime.ParseExact(TxtDate.Text, calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                        Else
                            Return Nothing
                        End If
                    End If
                Else

                    'Pinkal (31-Jul-2020) -- Start
                    'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
                    'Return DateTime.ParseExact("01/01/1900", calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                    Return Nothing
                    'Pinkal (31-Jul-2020) -- End
                End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

            Catch ex As Exception

                'S.SANDEEP |07-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : LOG FOR DATE ERROR (CLR EXCEPTION)
                Dim objErrorLog As New clsErrorlog_Tran
                With objErrorLog
                    Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : GetDate : " & ex.Message & "; " & ex.StackTrace.ToString
                    If ex.InnerException IsNot Nothing Then
                        S_dispmsg &= "; " & ex.InnerException.Message
                    End If
                    S_dispmsg = S_dispmsg.Replace("'", "")
                    S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                    ._Error_Message = S_dispmsg
                    ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                    ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                    ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                    If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                        ._Database_Version = ""
                    Else
                        ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                    End If
                    If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                        ._Userunkid = HttpContext.Current.Session("UserId")
                        ._Loginemployeeunkid = 0
                    Else
                        ._Userunkid = 0
                        ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                    End If
                    ._Isemailsent = False
                    ._Isweb = True
                    ._Isvoid = False
                    ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                    ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
                End With
                Dim strDBName As String = ""
                If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                    If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                        strDBName = "hrmsConfiguration"
                    End If
                End If
                If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

                End If
                'S.SANDEEP |07-AUG-2020| -- START



                TxtDate.Text = ""
            End Try
        End Get
    End Property

    Public ReadOnly Property IsNull() As Boolean
        Get
            Try
                'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
                'Pinkal (13-Apr-2015) -- Start
                'Enhancement - DATE CONTROL CHANGES
                'System.Threading.Thread.CurrentThread.CurrentUICulture = culture
                'System.Threading.Thread.CurrentThread.CurrentCulture = culture
                'Pinkal (13-Apr-2015) -- End

                'Pinkal (07-Aug-2020) -- Start
                'Resolved Date Control Problem.
                If TxtDate.Text.Trim.Length > 0 Then
                    If IsDate(TxtDate.Text.Trim()) = True Then
                    Dim dtDate As Date = DateTime.ParseExact(TxtDate.Text, calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                    If IsDate(dtDate) = False Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return True
                End If
                Else
                    Return True
                End If
            Catch ex As Exception

                'S.SANDEEP |07-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : LOG FOR DATE ERROR (CLR EXCEPTION)
                Dim objErrorLog As New clsErrorlog_Tran
                With objErrorLog
                    Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : IsNull : " & ex.Message & "; " & ex.StackTrace.ToString
                    If ex.InnerException IsNot Nothing Then
                        S_dispmsg &= "; " & ex.InnerException.Message
                    End If
                    S_dispmsg = S_dispmsg.Replace("'", "")
                    S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                    ._Error_Message = S_dispmsg
                    ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                    ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                    ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                    If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                        ._Database_Version = ""
                    Else
                        ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                    End If
                    If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                        ._Userunkid = HttpContext.Current.Session("UserId")
                        ._Loginemployeeunkid = 0
                    Else
                        ._Userunkid = 0
                        ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                    End If
                    ._Isemailsent = False
                    ._Isweb = True
                    ._Isvoid = False
                    ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                    ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
                End With
                Dim strDBName As String = ""
                If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                    If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                        strDBName = "hrmsConfiguration"
                    End If
                End If
                If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

                End If
                'S.SANDEEP |07-AUG-2020| -- START

                TxtDate.Text = ""
                Return True
            End Try
        End Get
    End Property

    Public Property Enabled() As Boolean
        Get
            Return TxtDate.Enabled
        End Get
        Set(ByVal value As Boolean)
            TxtDate.Enabled = value
        End Set
    End Property

    Public Property ReadonlyDate() As Boolean
        Get
            Return TxtDate.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            TxtDate.ReadOnly = value
        End Set
    End Property

    Public Property Width() As Integer
        Get
            Return TxtDate.Width.Value
        End Get
        Set(ByVal value As Integer)
            TxtDate.Width = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return TxtDate.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            TxtDate.AutoPostBack = value
        End Set
    End Property

    'Nilay (11-May-2015) -- Start
    'Enhancement - Payroll AT Log
    Public WriteOnly Property ReminderVisible() As Boolean
        Set(ByVal value As Boolean)
            Try
            objbtnDateReminder.Visible = value
            Catch ex As Exception
                Throw New Exception("ReminderVisible :- " & ex.Message.ToString())
            End Try
        End Set
    End Property
    'Nilay (11-May-2015) -- End

    Protected Sub TxtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtDate.TextChanged

        'Pinkal (13-Aug-2020) -- Start
        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Try
        RaiseEvent TextChanged(sender, e)
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : TxtDate_TextChanged : " & ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub


    Public Sub BindServerDateTime()
        Try
            Dim msql As String
            Dim ds As DataSet
            msql = "select Getdate() as ServerDt "
            ds = clsdataopr.WExecQuery(msql, "in")
            TxtDate.Text = ds.Tables(0).Rows(0)("ServerDt").ToString
            Me.SysDatetime = ds.Tables(0).Rows(0)("ServerDt").ToString
        Catch ex As Exception
            Throw New Exception("DateCtrl-->BindServerDateTime Method!!!" & ex.Message.ToString)
        Finally
        End Try
    End Sub
    Private _pSysdate As Date
    Public Property SysDatetime() As Date
        Get
            Try
            Return _pSysdate
            Catch ex As Exception
                Throw New Exception("SysDatetime Get :- " & ex.Message.ToString())
            End Try
        End Get
        Set(ByVal value As Date)
            Try
            _pSysdate = value
            Catch ex As Exception
                Throw New Exception("SysDatetime Set :- " & ex.Message.ToString())
            End Try
        End Set
    End Property
    Public ReadOnly Property GetDateTime() As DateTime
        Get
            Try
                If IsNull() = False Then
                    Return (CDate(TxtDate.Text & " " & Now.ToString("hh:mm:ss tt")).ToString())
                End If
            Catch ex As Exception

                'S.SANDEEP |07-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : LOG FOR DATE ERROR (CLR EXCEPTION)
                Dim objErrorLog As New clsErrorlog_Tran
                With objErrorLog
                    Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : GetDateTime : " & ex.Message & "; " & ex.StackTrace.ToString
                    If ex.InnerException IsNot Nothing Then
                        S_dispmsg &= "; " & ex.InnerException.Message
                    End If
                    S_dispmsg = S_dispmsg.Replace("'", "")
                    S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                    ._Error_Message = S_dispmsg
                    ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                    ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                    ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                    If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                        ._Database_Version = ""
                    Else
                        ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                    End If
                    If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                        ._Userunkid = HttpContext.Current.Session("UserId")
                        ._Loginemployeeunkid = 0
                    Else
                        ._Userunkid = 0
                        ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                    End If
                    ._Isemailsent = False
                    ._Isweb = True
                    ._Isvoid = False
                    ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                    ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
                End With
                Dim strDBName As String = ""
                If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                    If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                        strDBName = "hrmsConfiguration"
                    End If
                End If
                If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

                End If
                'S.SANDEEP |07-AUG-2020| -- START

                TxtDate.Text = ""
            End Try
        End Get
    End Property
    'Public Property ChkVisible() As Boolean
    '    Get
    '        Return ChkInclude.Visible
    '    End Get
    '    Set(ByVal value As Boolean)
    '        ChkInclude.Visible = value
    '    End Set
    'End Property

    'Public ReadOnly Property ChkInclude_Checked() As Boolean
    '    Get
    '        If (ChkInclude.Checked) Then
    '            Return True
    '        ElseIf (ChkInclude.Checked = False) Then
    '            Return False
    '        End If
    '    End Get
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If Session("DateFormat") IsNot Nothing AndAlso Session("DateFormat").ToString().Trim.Length > 0 Then
        calendarButtonExtender.Format = Session("DateFormat")
            End If
            'Pinkal (20-Aug-2020) -- End


        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : Page_Load : " & ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
        End Try
    End Sub

    Protected Sub objbtnDteReminder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles objbtnDateReminder.Click
        Try
        RaiseEvent objbtnDateReminder_Click(sender, e)
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                Dim S_dispmsg As String = "Value : " & TxtDate.Text & ", Error : objbtnDteReminder_Click : " & ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
        End Try
    End Sub

End Class
