﻿Imports Aruti.Data
Imports System.Data
Imports Aruti
Imports eZeeCommonLib


Partial Class Controls_Delete_Reason
    Inherits System.Web.UI.UserControl

    Public Delegate Sub buttonDelReasonYes_ClickEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonDelReasonYes_Click As buttonDelReasonYes_ClickEventHandler
    Public Delegate Sub buttonDelReasonNo_ClickEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonDelReasonNo_Click As buttonDelReasonNo_ClickEventHandler
    Private objVoidReason As clsVoidReason = New clsVoidReason()
    Private DisplayMessage As CommonCodes = New CommonCodes()
    Private ReasonMsg As String
    Private mintCategoryId As Integer
    Private blnReason As Boolean = False

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.ViewState("ReasonMsg") = ReasonMsg
        Me.ViewState("blnReason") = blnReason
    End Sub

    Private sCancelControlID As String = "btnDelReasonNo"

    Public Property CancelControlName() As String
        Get
            Return sCancelControlID
        End Get
        Set(ByVal value As String)
            sCancelControlID = value
            ModalPopupExtender1.CancelControlID = sCancelControlID
        End Set
    End Property

    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property ReasonType() As enVoidCategoryType
        Get
            Return ReasonType
        End Get
        Set(ByVal value As enVoidCategoryType)
            mintCategoryId = CInt(value)
            GetData()
        End Set
    End Property

    Public Property Reason() As String
        Get
            Return rdbReason.SelectedItem.ToString()
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    Protected Sub btnDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
        If rdbReason.SelectedIndex > 0 Then
            Reason = rdbReason.SelectedItem.ToString()
            blnReason = False
            ModalPopupExtender1.Hide()
            RaiseEvent buttonDelReasonYes_Click(sender, e)
        Else
            blnReason = True
            ModalPopupExtender1.Show()
            DisplayMessage.DisplayMessage("Please enter delete reason.", Me.Page)
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnDelReasonYes_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
        
    End Sub

    Public Sub Show()
        Try
        ModalPopupExtender1.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show() :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        ModalPopupExtender1.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide() :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

        ModalPopupExtender1.CancelControlID = sCancelControlID

        If Not IsPostBack Then
            GetData()
            rdbReason.ClearSelection()
        Else

            If ViewState("ReasonMsg") IsNot Nothing Then
                ReasonMsg = Me.ViewState("ReasonMsg").ToString()
            End If

            If ViewState("blnReason") IsNot Nothing Then
                blnReason = Convert.ToBoolean(Me.ViewState("blnReason").ToString())
            End If

            If blnReason Then
                ModalPopupExtender1.Show()
            End If
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        Try
        MyBase.OnInit(e)
        AddHandler btnDelReasonYes.Click, AddressOf btnDelReasonYes_Click
        AddHandler btnDelReasonNo.Click, AddressOf btnDelReasonNo_Click
        AddHandler Me.Load, AddressOf Page_Load
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("OnInit :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub GetData()
        Dim dsList As DataSet = New DataSet()

        Try
            dsList = objVoidReason.getComboList(mintCategoryId, True, "List")
            rdbReason.DataSource = dsList.Tables("List")
            rdbReason.DataTextField = "Name"
            rdbReason.DataValueField = "Id"
           
            rdbReason.DataBind()

            'rdbReason.SelectedIndex = 0
            'rdbReason.SelectedValue = "0"
            'rdbReason.Items(0).Selected = True

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnDelReasonNo_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
        ModalPopupExtender1.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnDelReasonNo_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    
End Class
