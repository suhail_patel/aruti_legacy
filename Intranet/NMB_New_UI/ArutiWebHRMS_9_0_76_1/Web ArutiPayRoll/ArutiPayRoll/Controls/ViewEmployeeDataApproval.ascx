﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewEmployeeDataApproval.ascx.vb"
    Inherits="Controls_ViewEmployeeDataApproval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popup_ViewMovementApproval" runat="server" BackgroundCssClass="modal-backdrop"
    CancelControlID="lblTitle" PopupControlID="pnl_ViewEmployeeDataApproval" TargetControlID="hdf_ViewMovementApproval">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnl_ViewEmployeeDataApproval" runat="server" CssClass="card modal-dialog modal-lg"
    Style="display: none">
    <div class="header">
        <h2>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </h2>
    </div>
    <div class="body">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:Label ID="lblSelectOperationType" runat="server" Text="Opration Type"></asp:Label>
                <asp:DropDownList ID="drpOprationType" runat="server" AutoPostBack="true" data-live-search="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 300px; overflow: auto">
                <div class="table-responsive">
                    <asp:GridView ID="gvApproveRejectEmployeeData" runat="server" AutoGenerateColumns="False"
                        Width="99%" CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="iCheck,isgrp,employeeunkid,qualificationgroupunkid,qualificationunkid">
                        <Columns>
                            <asp:BoundField HeaderText="Skill Category" DataField="scategory" FooterText="dgcolhSkillCategory" />
                            <asp:BoundField HeaderText="Skill" DataField="skill" FooterText="dgcolhSkill" />
                            <asp:BoundField HeaderText="Description" DataField="description" FooterText="dgcolhDescription" />
                            <asp:BoundField HeaderText="Qualification Group" DataField="qualificationgrpname"
                                FooterText="dgcolhQualifyGroup" />
                            <asp:BoundField HeaderText="Qualification" DataField="qualificationname" FooterText="dgcolhQualification" />
                            <asp:BoundField HeaderText="Award From Date" DataField="award_start_date" FooterText="dgcolhAwardDate" />
                            <asp:BoundField HeaderText="Award To Date" DataField="award_end_date" FooterText="dgcolhAwardToDate" />
                            <asp:BoundField HeaderText="Institute" DataField="institute_name" FooterText="dgcolhInstitute" />
                            <asp:BoundField HeaderText="Ref. No." DataField="reference_no" FooterText="dgcolhRefNo" />
                            <asp:BoundField HeaderText="Company" DataField="cname" FooterText="dgcolhCompany" />
                            <asp:BoundField HeaderText="Job" DataField="old_job" FooterText="dgcolhJob" />
                            <asp:BoundField HeaderText="Start Date" DataField="start_date" FooterText="dgcolhStartDate" />
                            <asp:BoundField HeaderText="End Date" DataField="end_date" FooterText="dgcolhEndDate" />
                            <asp:BoundField HeaderText="Supervisor" DataField="supervisor" FooterText="dgcolhSupervisor" />
                            <asp:BoundField HeaderText="Remark" DataField="remark" FooterText="dgcolhRemark" />
                            <asp:BoundField HeaderText="Refree Name" DataField="rname" FooterText="dgcolhRefreeName" />
                            <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhCountry" />
                            <asp:BoundField HeaderText="Id. No." DataField="Company" FooterText="dgcolhIdNo" />
                            <asp:BoundField HeaderText="Email" DataField="Email" FooterText="dgcolhEmail" />
                            <asp:BoundField HeaderText="Tel. No." DataField="telephone_no" FooterText="dgcolhTelNo" />
                            <asp:BoundField HeaderText="Mobile" DataField="mobile_no" FooterText="dgcolhMobile" />
                            <%--'Gajanan [22-Feb-2019] -- Start--%>
                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                            <asp:BoundField HeaderText="Identity Type" DataField="IdType" FooterText="dgcolhIdType" />
                            <asp:BoundField HeaderText="Serial No." DataField="serial_no" FooterText="dgcolhSrNo" />
                            <asp:BoundField HeaderText="Identity No." DataField="identity_no" FooterText="dgcolhIdentityNo" />
                            <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhIdCountry" />
                            <asp:BoundField HeaderText="Issue Place" DataField="issued_place" FooterText="dgcolhIdIssuePlace" />
                            <asp:BoundField HeaderText="Issue Date" DataField="issue_date" FooterText="dgcolhIdIssueDate" />
                            <asp:BoundField HeaderText="Expiry Date" DataField="expiry_date" FooterText="dgcolhIdExpiryDate" />
                            <asp:BoundField HeaderText="DL Class" DataField="dl_class" FooterText="dgcolhIdDLClass" />
                            <asp:BoundField HeaderText="Name" DataField="dependantname" FooterText="dgcolhDBName" />
                            <asp:BoundField HeaderText="Relation" DataField="relation" FooterText="dgcolhDBRelation" />
                            <asp:BoundField HeaderText="Birth Date" DataField="birthdate" FooterText="dgcolhDBbirthdate" />
                            <asp:BoundField HeaderText="Identify Number" DataField="identify_no" FooterText="dgcolhDBIdentifyNo" />
                            <asp:BoundField HeaderText="Gender" DataField="gender" FooterText="dgcolhDBGender" />
                            <%--'Gajanan [22-Feb-2019] -- End--%>
                            <%--'Gajanan [18-Mar-2019] -- Start--%>
                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                            <asp:BoundField HeaderText="Address Type" DataField="addresstype" FooterText="dgcolhAddressType" />
                            <asp:BoundField HeaderText="Address1" DataField="address1" FooterText="dgcolhAddress1" />
                            <asp:BoundField HeaderText="Address2" DataField="address2" FooterText="dgcolhAddress2" />
                            <asp:BoundField HeaderText="Firstname" DataField="firstname" FooterText="dgcolhFirstname" />
                            <asp:BoundField HeaderText="Lastname" DataField="lastname" FooterText="dgcolhLastname" />
                            <asp:BoundField HeaderText="Country" DataField="country" FooterText="dgcolhAddressCountry" />
                            <asp:BoundField HeaderText="State" DataField="state" FooterText="dgcolhAddressState" />
                            <asp:BoundField HeaderText="City" DataField="city" FooterText="dgcolhAddressCity" />
                            <asp:BoundField HeaderText="ZipCode" DataField="zipcode_code" FooterText="dgcolhAddressZipCode" />
                            <asp:BoundField HeaderText="Province" DataField="provicnce" FooterText="dgcolhAddressProvince" />
                            <asp:BoundField HeaderText="Road" DataField="road" FooterText="dgcolhAddressRoad" />
                            <asp:BoundField HeaderText="Estate" DataField="estate" FooterText="dgcolhAddressEstate" />
                            <asp:BoundField HeaderText="Province1" DataField="provicnce1" FooterText="dgcolhAddressProvince1" />
                            <asp:BoundField HeaderText="Road1" DataField="Road1" FooterText="dgcolhAddressRoad1" />
                            <asp:BoundField HeaderText="Chiefdom" DataField="chiefdom" FooterText="dgcolhAddressChiefdom" />
                            <asp:BoundField HeaderText="Village" DataField="village" FooterText="dgcolhAddressVillage" />
                            <asp:BoundField HeaderText="Town" DataField="town" FooterText="dgcolhAddressTown" />
                            <asp:BoundField HeaderText="Mobile" DataField="mobile" FooterText="dgcolhAddressMobile" />
                            <asp:BoundField HeaderText="Telephone Number" DataField="tel_no" FooterText="dgcolhAddressTel_no" />
                            <asp:BoundField HeaderText="Plot Number" DataField="plotNo" FooterText="dgcolhAddressPlotNo" />
                            <asp:BoundField HeaderText="Alternate Number" DataField="alternateno" FooterText="dgcolhAddressAltNo" />
                            <asp:BoundField HeaderText="Email" DataField="email" FooterText="dgcolhAddressEmail" />
                            <asp:BoundField HeaderText="Fax" DataField="fax" FooterText="dgcolhAddressFax" />
                            <%--'Gajanan [18-Mar-2019] -- End--%>
                            <%--'S.SANDEEP |15-APR-2019| -- START--%>
                            <asp:BoundField HeaderText="Membership Category" DataField="Category" FooterText="dgcolhCategory" />
                            <asp:BoundField HeaderText="Membership" DataField="membershipname" FooterText="dgcolhmembershipname" />
                            <asp:BoundField HeaderText="Membership No" DataField="membershipno" FooterText="dgcolhmembershipno" />
                            <asp:BoundField HeaderText="Eff. Period" DataField="period_name" FooterText="dgcolhperiod_name" />
                            <asp:BoundField HeaderText="Issue Date" DataField="issue_date" FooterText="dgcolhissue_date" />
                            <asp:BoundField HeaderText="Start Date" DataField="start_date" FooterText="dgcolhstart_date" />
                            <asp:BoundField HeaderText="End Date" DataField="expiry_date" FooterText="dgcolhexpiry_date" />
                            <asp:BoundField HeaderText="Remark" DataField="remark" FooterText="dgcolhMemRemark" />
                            <%--'Gajanan [17-April-2019]-- START--%>
                            <asp:BoundField HeaderText="Birth Country" DataField="country" FooterText="dgcolhBirthCountry" />
                            <asp:BoundField HeaderText="Birth State" DataField="state" FooterText="dgcolhBirthState" />
                            <asp:BoundField HeaderText="Birth City" DataField="city" FooterText="dgcolhBirthCity" />
                            <asp:BoundField HeaderText="Birth Certificate No." DataField="birthcertificateno"
                                FooterText="dgcolhBirthCertificateNo" />
                            <asp:BoundField HeaderText="Birth Town1" DataField="town" FooterText="dgcolhBirthTown1" />
                            <asp:BoundField HeaderText="Birth Village1" DataField="birth_village" FooterText="dgcolhBirthVillage1" />
                            <asp:BoundField HeaderText="Birth Ward" DataField="birth_ward" FooterText="dgcolhBirthWard" />
                            <asp:BoundField HeaderText="Birth Village" DataField="birth_village" FooterText="dgcolhBirthVillage" />
                            <asp:BoundField HeaderText="Birth Chiefdom" DataField="chiefdom" FooterText="dgcolhBirthChiefdom" />
                            <asp:BoundField HeaderText="Complexion" DataField="Complexion" FooterText="dgcolhComplexion" />
                            <asp:BoundField HeaderText="Blood Group" DataField="BloodGroup" FooterText="dgcolhBloodGroup" />
                            <asp:BoundField HeaderText="Eye Color" DataField="EyeColor" FooterText="dgcolhEyeColor" />
                            <asp:BoundField HeaderText="Nationality" DataField="Nationality" FooterText="dgcolhNationality" />
                            <asp:BoundField HeaderText="EthinCity" DataField="Ethnicity" FooterText="dgcolhEthinCity" />
                            <asp:BoundField HeaderText="Religion" DataField="Religion" FooterText="dgcolhReligion" />
                            <asp:BoundField HeaderText="Hair" DataField="HairColor" FooterText="dgcolhHair" />
                            <asp:BoundField HeaderText="Marital Status" DataField="Maritalstatus" FooterText="dgcolhMaritalStatus" />
                            <asp:BoundField HeaderText="Extra Telephone" DataField="ExtTelephoneno" FooterText="dgcolhExtraTel" />
                            <asp:BoundField HeaderText="Language1" DataField="language1" FooterText="dgcolhLanguage1" />
                            <asp:BoundField HeaderText="Language2" DataField="language2" FooterText="dgcolhLanguage2" />
                            <asp:BoundField HeaderText="Language3" DataField="language3" FooterText="dgcolhLanguage3" />
                            <asp:BoundField HeaderText="Language4" DataField="language4" FooterText="dgcolhLanguage4" />
                            <asp:BoundField HeaderText="Height" DataField="height" FooterText="dgcolhHeight" />
                            <asp:BoundField HeaderText="Weight" DataField="weight" FooterText="dgcolhWeight" />
                            <asp:BoundField HeaderText="Marital Date" DataField="anniversary_date" FooterText="dgcolhMaritalDate" />
                            <asp:BoundField HeaderText="Allergies" DataField="Allergies" FooterText="dgcolhAllergies" />
                            <asp:BoundField HeaderText="Sports/Hobbies" DataField="sports_hobbies" FooterText="dgcolhSportsHobbies" />
                            <%--'Gajanan [17-April-2019]-- End--%>
                            <asp:BoundField HeaderText="Void Reason" DataField="sports_hobbies" FooterText="dgcolhVoidReason" />
                            <%--'S.SANDEEP |15-APR-2019| -- END--%>
                            <asp:BoundField HeaderText="Approver" DataField="username" FooterText="dgcolhApprover" />
                            <asp:BoundField HeaderText="Level" DataField="levelname" FooterText="dgcolhLevel" />
                            <asp:BoundField HeaderText="Status" DataField="iStatus" FooterText="dgcolhStatus" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
        <asp:HiddenField ID="hdf_ViewMovementApproval" runat="server" />
        <asp:Label ID="lblotherqualificationnote" runat="server" ForeColor="Blue" Text="Note: This color indicates other qualification"
            Style="font-weight: bold; float: left;"></asp:Label>
    </div>
</asp:Panel>
