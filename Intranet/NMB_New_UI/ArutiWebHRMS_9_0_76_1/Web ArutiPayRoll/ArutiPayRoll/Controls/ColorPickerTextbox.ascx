﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ColorPickerTextbox.ascx.vb"
    Inherits="Controls_ColorPickerTextbox" %>
<div class="form-group colorpicker colorpicker-element">
    <div class="form-line">
        <asp:TextBox ID="txtColorPicker" runat="server" CssClass="form-control" Text="#00AABB"></asp:TextBox>
    </div>
    <span class="input-group-addon"><i></i></span>
</div>
