﻿Imports Aruti.Data
Imports System.Data

Partial Class Controls_GetCombolist
    Inherits System.Web.UI.UserControl
    Dim clsdataopr As New eZeeCommonLib.clsDataOperation(True)
    Dim msql As String
    Dim ds As DataSet
    Private _Comp_Code As String

    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Public Property Company_Code() As String
        Get
            Return _Comp_Code
        End Get
        Set(ByVal value As String)
            _Comp_Code = value
        End Set
    End Property

    Private _encommn As Aruti.Data.clsCommon_Master.enCommonMaster
    Public Property enMastertype() As String
        Get
            Return _encommn
        End Get
        Set(ByVal value As String)
            _encommn = value
        End Set
    End Property



    Public Sub getComboList(ByVal comp_code As String, ByVal enmastertype As Aruti.Data.clsCommon_Master.enCommonMaster)
        Try
            Select Case enmastertype

                Case clsCommon_Master.enCommonMaster.LANGUAGES
                    msql = "select masterunkid ,name as Language from cfcommon_master where mastertype =" & enmastertype & " and Comp_Code='" & comp_code & "'"
                    ds = clsdataopr.WExecQuery(msql, "cfcommon_master")
                    drpCombo.DataSource = ds
                    drpCombo.DataTextField = "Language"
                    drpCombo.DataValueField = "masterunkid"
                    drpCombo.DataBind()

                Case clsCommon_Master.enCommonMaster.SKILL_CATEGORY
                    msql = "select masterunkid ,name as Category from cfcommon_master where  mastertype =" & enmastertype & "  and  Comp_Code='" & comp_code & "'"
                    ds = clsdataopr.WExecQuery(msql, "cfcommon_master")
                    drpCombo.DataSource = ds
                    drpCombo.DataTextField = "Category"
                    drpCombo.DataValueField = "masterunkid"
                    drpCombo.DataBind()

                Case clsCommon_Master.enCommonMaster.MARRIED_STATUS
                    msql = "select masterunkid ,name as status from cfcommon_master where  mastertype =" & enmastertype & "  and  Comp_Code='" & comp_code & "'"
                    ds = clsdataopr.WExecQuery(msql, "cfcommon_master")
                    drpCombo.DataSource = ds
                    drpCombo.DataTextField = "status"
                    drpCombo.DataValueField = "masterunkid"
                    drpCombo.DataBind()

                Case clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP
                    msql = "select masterunkid ,name as Q_Group from cfcommon_master where  mastertype =" & enmastertype & "  and  Comp_Code='" & comp_code & "'"
                    ds = clsdataopr.WExecQuery(msql, "cfcommon_master")
                    drpCombo.DataSource = ds
                    drpCombo.DataTextField = "Q_Group"
                    drpCombo.DataValueField = "masterunkid"
                    drpCombo.DataBind()



                Case clsCommon_Master.enCommonMaster.TITLE
                    msql = "select masterunkid ,name as Title from cfcommon_master where  mastertype =" & enmastertype & "  and  Comp_Code='" & comp_code & "'"
                    ds = clsdataopr.WExecQuery(msql, "cfcommon_master")
                    drpCombo.DataSource = ds
                    drpCombo.DataTextField = "Title"
                    drpCombo.DataValueField = "masterunkid"
                    drpCombo.DataBind()


            End Select
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->getComboList Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Public Sub BindSkill(ByVal comp_code As String)
        Try
            msql = "select Skillunkid , (Skillcode + SkillName ) As Skill from hrskill_master where Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "hrskill_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "Skill"
            drpCombo.DataValueField = "Skillunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindSkill Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindQualification(ByVal comp_code As String)
        Try
            msql = "select qualificationunkid ,qualificationname as Q_name from hrqualification_master   where    Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "hrqualification_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "Q_name"
            drpCombo.DataValueField = "qualificationunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindQualification Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindInstitute(ByVal comp_code As String)
        Try
            msql = "select instituteunkid ,institute_name as I_name from hrinstitute_master   where  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "hrqualification_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "I_name"
            drpCombo.DataValueField = "instituteunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindInstitute Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End    Finally
        End Try
    End Sub
    Public Sub BindCountry(ByVal comp_code As String)
        Try
            msql = "select countryunkid ,Country_name  from  cfCountry_master   where  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfCountry_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "Country_name"
            drpCombo.DataValueField = "countryunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindCountry Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindState(ByVal comp_code As String)
        Try
            msql = "select stateunkid ,name  from cfState_master   where  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfState_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "name"
            drpCombo.DataValueField = "stateunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindState Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindCity(ByVal comp_code As String)
        Try
            msql = "select cityunkid ,name  from cfCity_master   where  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfCity_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "name"
            drpCombo.DataValueField = "cityunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindCity Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Public Sub BindZipCode(ByVal comp_code As String)
        Try
            msql = "select zipcodeunkid ,zipcode_no  from cfZipCode_master   where  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfZipCode_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "zipcode_no"
            drpCombo.DataValueField = "zipcodeunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindZipCode Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindStateAsPerCountry(ByVal country As Integer, ByVal comp_code As String)

        Try

            msql = "select stateunkid ,name from cfstate_master   where  countryunkid=" & country & " and  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfZipCode_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "name"
            drpCombo.DataValueField = "stateunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindStateAsPerCountry Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindCityAsPerState(ByVal State As Integer, ByVal Country As Integer, ByVal comp_code As String)
        Try
            msql = "select cityunkid ,name  from cfCity_master   where Countryunkid=" & Country & " and stateunkid=" & State & " and Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfCity_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "name"
            drpCombo.DataValueField = "cityunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindCityAsPerState Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindZipCodeAsPerCity(ByVal city As Integer, ByVal State As Integer, ByVal Country As Integer, ByVal comp_code As String)
        Try

            msql = "select zipcodeunkid ,zipcode_no  from cfZipCode_master  where   Countryunkid=" & Country & " and stateunkid=" & State & "  and Cityunkid=" & city & " and Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "cfZipCode_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "zipcode_no"
            drpCombo.DataValueField = "zipcodeunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindZipCodeAsPerCity Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Public Sub BindVacancy(ByVal comp_code As String)
        Try
            msql = "select vacancyunkid ,vacancytitle from rcvacancy_master   where    Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "rcvacancy_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "vacancytitle"
            drpCombo.DataValueField = "vacancyunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindVacancy Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Public Sub BindSkillAsPerSkillCategory(ByVal skillcate_unkid As Integer, ByVal comp_code As String)
        Try
            msql = "select Skillunkid , (Skillcode + SkillName ) As Skill from hrskill_master where   Skillcategoryunkid= " & skillcate_unkid & " and  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "hrskill_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "Skill"
            drpCombo.DataValueField = "Skillunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindSkillAsPerSkillCategory Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Sub BindQualificationAsPerQualificationGrp(ByVal qualigrp As Integer, ByVal comp_code As String)
        Try
            msql = "select qualificationunkid ,qualificationname as Q_name from hrqualification_master   where   qualificationgroupunkid =" & qualigrp & "  and  Comp_Code='" & comp_code & "'"
            ds = clsdataopr.WExecQuery(msql, "hrqualification_master")
            drpCombo.DataSource = ds
            drpCombo.DataTextField = "Q_name"
            drpCombo.DataValueField = "qualificationunkid"
            drpCombo.DataBind()
            Dim list As New ListItem
            list.Text = "Select"
            list.Value = -1
            drpCombo.Items.Insert(0, list)
            drpCombo.SelectedIndex = -1
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GetComboList-->BindQualificationAsPerQualificationGrp Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Property SelectedValue() As String
        Get
            Return drpCombo.SelectedValue
        End Get
        Set(ByVal value As String)
            drpCombo.SelectedValue = value
        End Set
    End Property
    Public Sub BindGender()
        drpCombo.Items.Add("Select")
        drpCombo.Items.Add("Male")
        drpCombo.Items.Add("Female")
        
    End Sub
    Public Property SelectedText() As String
        Get
            Return drpCombo.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            drpCombo.SelectedItem.Text = value
        End Set
    End Property

    Public Property SelectedIndex() As Integer
        Get
            Return drpCombo.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            drpCombo.SelectedIndex = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return drpCombo.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            drpCombo.AutoPostBack = value
        End Set
    End Property
    Protected Sub ddlEmpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpCombo.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub

    'Sohail (19 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property CssClass() As String
        Get
            Return drpCombo.CssClass
        End Get
        Set(ByVal value As String)
            drpCombo.CssClass = value
        End Set
    End Property

    Public Property Height() As Unit
        Get
            Return drpCombo.Height
        End Get
        Set(ByVal value As Unit)
            drpCombo.Height = value
        End Set
    End Property

    Public Property Width() As Unit
        Get
            Return drpCombo.Width
        End Get
        Set(ByVal value As Unit)
            drpCombo.Width = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return drpCombo.Text
        End Get
        Set(ByVal value As String)
            drpCombo.Text = value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return drpCombo.Enabled
        End Get
        Set(ByVal value As Boolean)
            drpCombo.Enabled = value
        End Set
    End Property

    Public ReadOnly Property Items() As ListItemCollection
        Get
            Return drpCombo.Items
        End Get
    End Property

    Public Property DataTextField() As String
        Get
            Return drpCombo.DataTextField
        End Get
        Set(ByVal value As String)
            drpCombo.DataTextField = value
        End Set
    End Property

    Public Property DataValueField() As String
        Get
            Return drpCombo.DataValueField
        End Get
        Set(ByVal value As String)
            drpCombo.DataValueField = value
        End Set
    End Property

    Public Property DataSource() As Object
        Get
            Return drpCombo.DataSource
        End Get
        Set(ByVal value As Object)
            drpCombo.DataSource = value
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return drpCombo.SelectedItem
        End Get
    End Property

    Protected Sub drpCombo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpCombo.DataBound
        Try
        If drpCombo.Items.Count > 0 Then
            For Each lstItem As ListItem In drpCombo.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            drpCombo.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("drpCombo_DataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (19 Dec 2012) -- End

End Class
