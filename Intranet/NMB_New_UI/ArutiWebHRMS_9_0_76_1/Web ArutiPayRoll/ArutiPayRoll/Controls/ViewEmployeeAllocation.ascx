﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewEmployeeAllocation.ascx.vb"
    Inherits="Controls_ViewEmployeeAllocation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popup_ViewEmployeeAllocation" runat="server"
    BackgroundCssClass="modalBackground" CancelControlID="btnClose" PopupControlID="pnl_ViewEmployeeAllocation"
    TargetControlID="hdf_ViewEmployeeAllocation">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnl_ViewEmployeeAllocation" runat="server" CssClass="newpopup csspreview"
    Style="display: none; width: 750px">
    <div class="panel-primary" style="margin-bottom: 0px">
        <div class="panel-heading">
            <asp:Label ID="lblTitle" runat="server" Text="Employee Allocation Detail"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                    <div class="row2">
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 80%">
                            <asp:Label ID="objlblEmployee" runat="server" Text="Dept. Group" Width="230px"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Div4" class="panel-default">
                <div id="Div6" class="panel-body-default">
                    <div class="row2">
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblBranch" runat="server" Text="Branch" Font-Bold=true> </asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblBranch" runat="server" Text="" ></asp:Label>
                        </div>
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblJobGroup" runat="server" Text="Job Group" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblJobGroup" runat="server" Text="" ></asp:Label>
                        </div>
                    </div>
                    <div class="row2">
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblDepartmentGroup" runat="server" Text="Dept. Group" Width="230px" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblDepartmentGroup" runat="server" ></asp:Label>
                        </div>
                        
                         <div class="ib" style="width: 16%">
                            <asp:Label ID="lblDepartment" runat="server" Text="Department" Font-Bold=true> </asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblDepartment" runat="server" ></asp:Label>
                        </div>
                        
                        
                    </div>
                   
                    <div class="row2">
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblSectionGroup" runat="server" Text="Section Group" Font-Bold=true> </asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblSectionGroup" runat="server" ></asp:Label>
                        </div>
                        
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblSection" runat="server" Text="Section" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblSection" runat="server" ></asp:Label>
                        </div>
                    
                    </div>
                    <div class="row2">
                            <div class="ib" style="width: 16%">
                            <asp:Label ID="lblClassGroup" runat="server" Text="Class Group" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblClassGroup" runat="server" ></asp:Label>
                        </div>
                        
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblClass" runat="server" Text="Class" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblClass" runat="server" ></asp:Label>
                        </div>
                    </div>
                    <div class="row2">
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblUnitGroup" runat="server" Text="Unit Group" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblUnitGroup" runat="server" ></asp:Label>
                        </div>
                        
                         <div class="ib" style="width: 16%">
                            <asp:Label ID="lblUnits" runat="server" Text="Unit" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblUnits" runat="server" ></asp:Label>
                        </div>
                        
                     
                    </div>
                    <div class="row2">
                          <div class="ib" style="width: 16%">
                            <asp:Label ID="lblGradeGroup" runat="server" Text="Grade Group" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblGradeGroup" runat="server" ></asp:Label>
                        </div>
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblGrade" runat="server" Text="Grade" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblGrade" runat="server" ></asp:Label>
                            
                        </div>
                    </div>
                    <div class="row2">
                       
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblGradeLevel" runat="server" ></asp:Label>
                        </div>
                        
                         <div class="ib" style="width: 16%">
                            <asp:Label ID="lblTeam" runat="server" Text="Team" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblTeam" runat="server" ></asp:Label>
                        </div>
                    </div>
                    
                     <div class="row2">
                       <div class="ib" style="width: 16%">
                            <asp:Label ID="lblJob" runat="server" Text="Job" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblJob" runat="server"  ></asp:Label>
                        </div>
                        
                        <div class="ib" style="width: 16%">
                            <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblCostCenter" runat="server" ></asp:Label>
                        </div>
                       
                    </div>
                    
                    <div class="row2">
                         <div class="ib" style="width: 16%">
                            <asp:Label ID="lblScale" runat="server" Text="Scale" Font-Bold=true></asp:Label>
                        </div>
                        <div class="ib" style="width: 30%">
                            <asp:Label ID="objlblScale" runat="server" Text ="0"></asp:Label>
                        </div>
                    </div>
                    <div class="btn-default">
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                        <asp:HiddenField ID="hdf_ViewEmployeeAllocation" runat="server" />
                    </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Panel>
