﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TimeCtrl.ascx.vb" Inherits="Controls_TimeCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div class="form-group">
     <div class="form-line">
        <asp:TextBox runat="server" ID="TxtTime"  AutoPostBack="True" MaxLength="10"  CssClass="form-control" />
      </div>  
 </div> 
