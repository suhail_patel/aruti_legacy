﻿Imports Aruti.Data
Imports System.Data
Imports System.Drawing

Partial Class Controls_ViewEmployeeDataApproval
    Inherits System.Web.UI.UserControl

#Region "Private Varible"
    Private blnShowPopup As Boolean = False
    Private DisplayMessage As New CommonCodes
    Private mintUserId As Integer
    Private mintPriority As Integer
    Private mintPrivilegeId As Integer
    Private meFillType As enScreenName
    Private mstrFilterString As String = ""
    Private mblnFromApprovalScreen As Boolean = True
    Private objApprovalTran As New clsEmployeeDataApproval
    'S.SANDEEP |17-JAN-2019| -- START
    Private meOperation As clsEmployeeDataApproval.enOperationType
    'S.SANDEEP |17-JAN-2019| -- END

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintStartIndex As Integer = 0
    Private mintEndIndex As Integer = 0
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region "Public Property(S)"

    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    Public Property _PrivilegeId() As Integer
        Get
            Return mintPrivilegeId
        End Get
        Set(ByVal value As Integer)
            mintPrivilegeId = value
        End Set
    End Property

    Public Property _FillType() As enScreenName
        Get
            Return meFillType
        End Get
        Set(ByVal value As enScreenName)
            meFillType = value
        End Set
    End Property

    Public Property _FilterString() As String
        Get
            Return mstrFilterString
        End Get
        Set(ByVal value As String)
            mstrFilterString = value
        End Set
    End Property

    Public Property _FromApprovalScreen() As Boolean
        Get
            Return mblnFromApprovalScreen
        End Get
        Set(ByVal value As Boolean)
            mblnFromApprovalScreen = value
        End Set
    End Property

    Public Property _OprationType() As clsEmployeeDataApproval.enOperationType
        Get
            Return meOperation
        End Get
        Set(ByVal value As clsEmployeeDataApproval.enOperationType)
            meOperation = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'blnShowPopup = CBool(Me.ViewState("blnShowPopup"))
            'If blnShowPopup Then
            '    popup_ViewMovementApproval.Show()
            'End If
            If (Page.IsPostBack = False) Then
                FillCombo()
            Else
                blnShowPopup = CBool(ViewState("blnShowPopup"))
                mintUserId = CInt(ViewState("mintUserId"))
                mintPriority = CInt(ViewState("mintPriority"))
                mintPrivilegeId = CInt(ViewState("mintPrivilegeId"))
                meFillType = CType(ViewState("meFillType"), enScreenName)
                mstrFilterString = CStr(ViewState("mstrFilterString"))
                mblnFromApprovalScreen = CBool(ViewState("mblnFromApprovalScreen"))
                meOperation = CType(ViewState("meOperation"), clsEmployeeDataApproval.enOperationType)
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintStartIndex = Me.ViewState("mintStartIndex")
                mintEndIndex = Me.ViewState("mintEndIndex")
                'Gajanan [22-Feb-2019] -- End
            If blnShowPopup Then
                popup_ViewMovementApproval.Show()
            End If
            End If

'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("blnShowPopup") = blnShowPopup
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState("mintUserId") = mintUserId
            Me.ViewState("mintPriority") = mintPriority
            Me.ViewState("mintPrivilegeId") = mintPrivilegeId
            Me.ViewState("meFillType") = meFillType
            Me.ViewState("mstrFilterString") = mstrFilterString
            Me.ViewState("mblnFromApprovalScreen") = mblnFromApprovalScreen
            Me.ViewState("meOperation") = meOperation
            'Gajanan [17-DEC-2018] -- End
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState("mintStartIndex") = mintStartIndex
            Me.ViewState("mintEndIndex") = mintEndIndex
            'Gajanan [22-Feb-2019] -- End
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub


#Region "Private Method(S)"
    Public Sub Show()

        Try

        
        blnShowPopup = True
        popup_ViewMovementApproval.Show()

        If mblnFromApprovalScreen = False Then
            lblTitle.Text = "Approval Status"
        Else
            lblTitle.Text = "My Report"
        End If

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        If meOperation <> clsEmployeeDataApproval.enOperationType.NONE Then
            drpOprationType.Enabled = False
        End If

        If CInt(meOperation) > 0 Then
            drpOprationType.SelectedValue = meOperation
        End If
        Call FillGrid()
        'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        lblTitle.Text = ""
        blnShowPopup = False
        popup_ViewMovementApproval.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub


    Private Sub FillGrid()
        Try

            Dim xColIndex As Integer = 0
            Dim bField As BoundField
            Dim colField As DataControlField

            If CInt(meFillType) = CInt(enScreenName.frmQualificationsList) Then

                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True))
                bField = CType(colField, BoundField)

            ElseIf CInt(meFillType) = CInt(enScreenName.frmEmployee_Skill_List) Then
                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True))
                bField = CType(colField, BoundField)


            ElseIf CInt(meFillType) = CInt(enScreenName.frmEmployeeRefereeList) Then
                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True))
                bField = CType(colField, BoundField)


            ElseIf CInt(meFillType) = CInt(enScreenName.frmJobHistory_ExperienceList) Then

                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True))
                bField = CType(colField, BoundField)


            ElseIf CInt(meFillType) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then

                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True))
                bField = CType(colField, BoundField)

            ElseIf CInt(meFillType) = CInt(enScreenName.frmIdentityInfoList) Then
                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True))
                bField = CType(colField, BoundField)

            ElseIf CInt(meFillType) = CInt(enScreenName.frmAddressList) Then

                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True))
                bField = CType(colField, BoundField)


            ElseIf CInt(meFillType) = CInt(enScreenName.frmEmergencyAddressList) Then
                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True))
                bField = CType(colField, BoundField)

            ElseIf CInt(meFillType) = CInt(enScreenName.frmMembershipInfoList) Then

                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True))
                bField = CType(colField, BoundField)


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

            ElseIf CInt(meFillType) = CInt(enScreenName.frmOtherinfo) Then

                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True))
                bField = CType(colField, BoundField)

            ElseIf CInt(meFillType) = CInt(enScreenName.frmBirthinfo) Then
                colField = gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True))
                bField = CType(colField, BoundField)
                'Gajanan [17-April-2019] -- End

            End If


            Dim dt As DataTable = objApprovalTran.GetNextEmployeeApprovers(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), mintPrivilegeId, meFillType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), CType(drpOprationType.SelectedValue, clsEmployeeDataApproval.enOperationType), Nothing, mintPriority, True, mstrFilterString)
            If mblnFromApprovalScreen Then
                dt = New DataView(dt, "priority <= " & mintPriority, "employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            End If
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If meFillType <> enScreenName.frmQualificationsList Then

                Dim newColumn1 As New Data.DataColumn("qualificationgroupunkid", GetType(Integer))
                newColumn1.DefaultValue = "0"
                dt.Columns.Add(newColumn1)

                Dim newColumn2 As New Data.DataColumn("qualificationunkid", GetType(Integer))
                newColumn2.DefaultValue = "0"
                dt.Columns.Add(newColumn2)
            End If
            'Gajanan [17-DEC-2018] -- End


            SetGridColums()


            If IsNothing(bField) = False Then
                Dim dtmp() As DataRow = dt.Select("isgrp = 1", bField.DataField.ToString())
                If dtmp.Length > 0 Then
                    Dim strGrpName As String = String.Empty
                    For i As Integer = 0 To dtmp.Length - 1
                        If strGrpName <> dtmp(i)(bField.DataField.ToString()).ToString() Then
                            strGrpName = dtmp(i)(bField.DataField.ToString()).ToString()
                        Else
                            dt.Rows.Remove(dtmp(i))
                        End If
                    Next
                End If

            End If

            gvApproveRejectEmployeeData.DataSource = dt
            gvApproveRejectEmployeeData.DataBind()

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub SetGridColums()
        Try

        For i As Integer = 0 To gvApproveRejectEmployeeData.Columns.Count - 1
            gvApproveRejectEmployeeData.Columns(i).Visible = False
        Next

        gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhApprover", False, True)).Visible = True
        gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLevel", False, True)).Visible = True
        gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStatus", False, True)).Visible = True
        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStatus", False, True)
        'Gajanan [22-Feb-2019] -- End

        Select Case meFillType
            Case enScreenName.frmQualificationsList
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True)
                'Gajanan [22-Feb-2019] -- End
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualification", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhInstitute", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)).Visible = True

            Case enScreenName.frmJobHistory_ExperienceList
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True)
                'Gajanan [22-Feb-2019] -- End
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhJob", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStartDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEndDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSupervisor", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRemark", False, True)).Visible = True


            Case enScreenName.frmEmployeeRefereeList
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True)
                'Gajanan [22-Feb-2019] -- End
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCountry", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEmail", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhTelNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMobile", False, True)).Visible = True


            Case enScreenName.frmEmployee_Skill_List
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True)
                'Gajanan [22-Feb-2019] -- End
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkill", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDescription", False, True)).Visible = True

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Case enScreenName.frmIdentityInfoList
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True)
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSrNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdentityNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdCountry", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssuePlace", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssueDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdExpiryDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdDLClass", False, True)).Visible = True


            Case enScreenName.frmDependantsAndBeneficiariesList
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True)
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBRelation", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBbirthdate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBIdentifyNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBGender", False, True)).Visible = True

                'Gajanan [22-Feb-2019] -- End


                'Gajanan [18-Mar-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Case enScreenName.frmAddressList
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)

                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddress1", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddress2", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCountry", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressState", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCity", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressZipCode", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEstate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince1", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad1", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressChiefdom", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressVillage", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTown", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressMobile", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTel_no", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressPlotNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressAltNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEmail", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)).Visible = True

            Case enScreenName.frmEmergencyAddressList
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)

                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhFirstname", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLastname", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCountry", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressState", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCity", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressZipCode", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEstate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressPlotNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressMobile", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressAltNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTel_no", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEmail", False, True)).Visible = True
                'Gajanan [18-Mar-2019] -- End

                'S.SANDEEP |15-APR-2019| -- START
            Case enScreenName.frmMembershipInfoList
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True)

                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhmembershipname", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhmembershipno", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhperiod_name", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhissue_date", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhstart_date", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhexpiry_date", False, True)).Visible = True
                'S.SANDEEP |15-APR-2019| -- END


            Case enScreenName.frmBirthinfo
                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True)

                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthState", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCity", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCertificateNo", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthTown1", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthVillage1", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthWard", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthVillage", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthChiefdom", False, True)).Visible = True

            Case enScreenName.frmOtherinfo

                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True)

                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBloodGroup", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEyeColor", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhNationality", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEthinCity", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhReligion", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhHair", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMaritalStatus", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhExtraTel", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage1", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage2", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage3", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage4", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhHeight", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhWeight", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMaritalDate", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAllergies", False, True)).Visible = True
                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSportsHobbies", False, True)).Visible = True
        End Select
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("SetGridColums :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            dsList = objApprovalTran.getOperationTypeList("List", True)
            With drpOprationType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub

    'Gajanan [17-DEC-2018] -- End
#End Region

#Region "Gridview Event"

    Protected Sub gvApproveRejectMovement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproveRejectEmployeeData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim intCount As Integer = 1
                If gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp") = 1 Then
                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'e.Row.BackColor = Color.Silver
                    For i = mintStartIndex + 1 To mintEndIndex
                        If gvApproveRejectEmployeeData.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(mintStartIndex).ColumnSpan = intCount
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    e.Row.Cells(mintStartIndex).CssClass = "group-header"
                    'Gajanan [17-Sep-2020] -- End

                    'Gajanan [22-Feb-2019] -- End
                ElseIf gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp") = 0 And gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("qualificationgroupunkid") <= 0 And meFillType = enScreenName.frmQualificationsList Then
                    e.Row.ForeColor = Color.Blue
                    lblotherqualificationnote.Visible = True
                ElseIf CBool(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) = False Then
                    'S.SANDEEP |15-APR-2019| -- START
                    Select Case CInt(meFillType)
                        Case enScreenName.frmJobHistory_ExperienceList
                            Dim intStartDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStartDate", False, True)
                            Dim intEndDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEndDate", False, True)
                            If e.Row.Cells(intStartDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intStartDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intStartDtIdx).Text = CDate(e.Row.Cells(intStartDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intEndDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intEndDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intEndDtIdx).Text = CDate(e.Row.Cells(intEndDtIdx).Text).Date.ToShortDateString
                            End If
                        Case enScreenName.frmQualificationsList
                            Dim intAwdStDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)
                            Dim intAwdEdDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)
                            If e.Row.Cells(intAwdStDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intAwdStDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intAwdStDtIdx).Text = CDate(e.Row.Cells(intAwdStDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intAwdEdDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intAwdEdDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intAwdEdDtIdx).Text = CDate(e.Row.Cells(intAwdEdDtIdx).Text).Date.ToShortDateString
                            End If
                        Case enScreenName.frmIdentityInfoList
                            Dim intIssueDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssueDate", False, True)
                            Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdExpiryDate", False, True)
                            If e.Row.Cells(intIssueDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intIssueDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intIssueDtIdx).Text = CDate(e.Row.Cells(intIssueDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
                            End If

                        Case enScreenName.frmDependantsAndBeneficiariesList
                            Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBbirthdate", False, True)
                            If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
                            End If
                        Case enScreenName.frmMembershipInfoList

                            Dim intIssueDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhissue_date", False, True)
                            Dim intStartDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhstart_date", False, True)
                            Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhexpiry_date", False, True)
                            If e.Row.Cells(intIssueDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intIssueDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intIssueDtIdx).Text = CDate(e.Row.Cells(intIssueDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intStartDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intStartDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intStartDtIdx).Text = CDate(e.Row.Cells(intStartDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
                            End If
                            'S.SANDEEP |15-APR-2019| -- END

                    End Select
                End If
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub
#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Protected Sub drpOprationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpOprationType.SelectedIndexChanged
        Try
            If CInt(drpOprationType.SelectedValue) > 0 Then
                FillGrid()
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub

'Gajanan [17-DEC-2018] -- End
End Class
