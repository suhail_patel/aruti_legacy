﻿
Partial Class Controls_ExportReport
    Inherits System.Web.UI.UserControl

    Private objError As CommonCodes = New CommonCodes()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
    End Sub


    Public Sub Show()
        Try
        popupExportReport.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show:- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        popupExportReport.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide:- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
        If Session("ExFileName") IsNot Nothing Then
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(Session("ExFileName").ToString())
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/x-msexcel"
            Response.WriteFile(Session("ExFileName").ToString())
            Response.Flush()
            System.IO.File.Delete(Session("ExFileName").ToString())
            Session("ExFileName") = Nothing
                'Pinkal (11-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	
                ApplicationInstance.CompleteRequest()
                Response.End()
                'Pinkal (11-Feb-2022) -- End
        Else
            objError.DisplayMessage("Please Export again.", Me.Page)
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnSave_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
End Class
