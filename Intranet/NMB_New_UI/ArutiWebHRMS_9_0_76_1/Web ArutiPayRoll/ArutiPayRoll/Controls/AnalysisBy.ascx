﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AnalysisBy.ascx.vb" Inherits="Controls_AnalysisBy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script type="text/javascript">

    $("body").on("click", "[id*=chkAB_AllSelect]", function() {
        var chkHeader = $(this);
        debugger;
        var grid = $(this).closest("table");
        $("[id*=chkAB_Select]").prop("checked", $(chkHeader).prop("checked"));
    });

    $("body").on("click", "[id*=chkAB_Select]", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkAB_AllSelect]", grid);
        debugger;
        if ($("[id*=chkAB_Select]", grid).length == $("[id*=chkAB_Select]:checked", grid).length) {
            chkHeader.prop("checked", true);
        }
        else {
            chkHeader.prop("checked", false);
        }
    });
        
</script>

<ajaxToolkit:ModalPopupExtender ID="popupAnalysisBy" runat="server" CancelControlID="HiddenField2"
    BackgroundCssClass="modal-backdrop" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog modal-lg" Style="display: none"
    DefaultButton="btnApply">
    <div id="divAnalysisBy" class="panel-primary" style="margin-bottom: 0px">
        <div class="header">
            <h2>
                <asp:Label ID="lblTitle" runat="server" Text="Analysis By"></asp:Label>
            </h2>
        </div>
        <div class="body" style="height: 400px">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radBranch" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Branch" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radDepartmentGrp" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Department Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radDepartment" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Department" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radSectionGrp" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Section Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radSection" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Section" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radUnitGrp" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Unit Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radUnit" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Unit" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radTeam" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Team" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radJobGrp" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Job Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radJob" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Job" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radClassGrp" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Class Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radClass" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Class" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radGradeGrp" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Grade Group" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radGrade" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Grade" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radGradeLevel" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Grade Level" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="radCostCenter" runat="server" AutoPostBack="true" CssClass="with-gap"
                                GroupName="Analysis" Text="Cost Center" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                DataKeyNames="Id" AllowPaging="false">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="25px" HeaderStyle-Width="25px">
                                        <HeaderTemplate>
                                            <%--'S.SANDEEP |15-FEB-2022| -- START--%>
                                            <%--'ISSUE : SAME CONTROL NAME FOR ANALYSIS BY/ADVANCE FILTER [chkSelect]--%>
                                            <%--<asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />--%>
                                            <asp:CheckBox ID="chkAB_AllSelect" runat="server" CssClass="filled-in" Text=" " />
                                            <%--'S.SANDEEP |15-FEB-2022| -- END--%>                                            
                                            <%--AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--'S.SANDEEP |15-FEB-2022| -- START--%>
                                            <%--'ISSUE : SAME CONTROL NAME FOR ANALYSIS BY/ADVANCE FILTER [chkSelect]--%>
                                            <%--<asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />--%>
                                            <asp:CheckBox ID="chkAB_Select" runat="server" CssClass="filled-in" Text=" " />
                                            <%--'S.SANDEEP |15-FEB-2022| -- END--%>
                                            <%--Checked='<%# Eval("IsCheck") %>' OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left"
                                        FooterText="objdgcolhId" />
                                    <asp:BoundField DataField="Name" HeaderText="Report By" FooterText="dgcolhReportBy"
                                        ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <asp:Label ID="lblMessage" runat="server" Text="" Style="max-width: 475px; display: block;" />
            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"
                ValidationGroup="Reason" />
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:HiddenField ID="HiddenField2" runat="server" />
        </div>
    </div>
</asp:Panel>
<%--       <div class="panel-primary">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <div id="FilterCriteria" class="panel-default">
                    <div id="FilterCriteriaBody" class="panel-body-default">
                        <table style="width: 100%">
                            <tr style="width: 100%">
                                <td style="width: 40%">
                                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Style="float: left; height: 350px;
                                        width: 220px; overflow: auto" AutoPostBack="true" CssClass="mycustomcss">
                                        <ajaxToolkit:TabPanel ID="tbpnlBranch" runat="server" Width="0px" Style="width: 0px;"
                                            HeaderText="Branch" CssClass="tabdata">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnlBranch" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                    <div>
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlDeptGroup" runat="server" Style="width: 0px;" HeaderText="Department Group">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlDept" runat="server" Style="width: 0px;" HeaderText="Department">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel22" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlSectionGroup" runat="server" Style="width: 0px;" HeaderText="Section Group">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlSection" runat="server" Style="width: 0px;" HeaderText="Section">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel4" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlUnitGroup" runat="server" Style="width: 0px;" HeaderText="Unit Group">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlUnit" runat="server" Style="width: 0px;" HeaderText="Unit">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel6" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlTeam" runat="server" Style="width: 0px;" HeaderText="Team">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel7" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlJobGroup" runat="server" Style="width: 0px;" HeaderText="Job Group">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel8" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlJob" runat="server" Style="width: 0px;" HeaderText="Job">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel9" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlClassGroup" runat="server" Style="width: 0px;" HeaderText="Class Group">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel10" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlClass" runat="server" Style="width: 0px;" HeaderText="Class">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel11" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlGradeGroup" runat="server" Style="width: 0px;" HeaderText="Grade Group">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel12" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlGrade" runat="server" Style="width: 0px;" HeaderText="Grade">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel13" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlGradeLevel" runat="server" Style="width: 0px;" HeaderText="Grade Level">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel14" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="tbpnlCostCenter" runat="server" Style="width: 0px;" HeaderText="Cost Center">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel16" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                    </ajaxToolkit:TabContainer>
                                </td>
                                <td style="width: 60%">
                                    <asp:Panel ID="pnl_gvDetails" runat="server" Height="350px" Width="100%" ScrollBars="Auto">
                                       
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <div class="btn-default">
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
