﻿
Partial Class Controls_ColorPickerTextbox
    Inherits System.Web.UI.UserControl

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)


#Region " Default Properties "

    Public Property Height() As Unit
        Get
            Return txtColorPicker.Height
        End Get
        Set(ByVal value As Unit)
            txtColorPicker.Height = value
        End Set
    End Property

    Public Property Width() As Unit
        Get
            Return txtColorPicker.Width
        End Get
        Set(ByVal value As Unit)
            txtColorPicker.Width = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return txtColorPicker.Text
        End Get
        Set(ByVal value As String)
            txtColorPicker.Text = value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return txtColorPicker.Enabled
        End Get
        Set(ByVal value As Boolean)
            txtColorPicker.Enabled = value
        End Set
    End Property

    Public Property Read_Only() As Boolean
        Get
            Return txtColorPicker.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtColorPicker.ReadOnly = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return txtColorPicker.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            txtColorPicker.AutoPostBack = value
        End Set
    End Property

    Public WriteOnly Property Max() As Integer
        Set(ByVal value As Integer)
            txtColorPicker.Attributes.Add("data-max", value)
        End Set
    End Property

    Public WriteOnly Property Min() As Integer
        Set(ByVal value As Integer)
            txtColorPicker.Attributes.Add("data-min", value)
        End Set
    End Property

#End Region

#Region " Default Events "

    Protected Sub txtColorPicker_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtColorPicker.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

#End Region

End Class
