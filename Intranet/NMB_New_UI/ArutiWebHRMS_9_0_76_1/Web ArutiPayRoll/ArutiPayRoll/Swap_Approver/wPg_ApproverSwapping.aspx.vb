﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
#End Region

Partial Class Swap_Approver_wPg_ApproverSwapping
    Inherits Basepage

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmApprover_Swapping"
    Private DisplayMessage As New CommonCodes
    Private objLeaveApprover As New clsleaveapprover_master
    Private objLoanApprover As clsLoanApprover_master
    Private objExpenseApprover As New clsExpenseApprover_Master
    Private objExAssessorTran As New clsExpenseApprover_Tran
    Dim dsFromApproverEmp As DataSet = Nothing
    Dim dsToApproverEmp As DataSet = Nothing
    Private ApproverType As Integer = -1
    Dim dtFromApproverEmp As DataTable = Nothing
    Dim dtToApproverEmp As DataTable = Nothing

    'S.SANDEEP [30 JAN 2016] -- START
    Private dtComboFromApprover As DataTable
    Private dtComboToApprover As DataTable
    'S.SANDEEP [30 JAN 2016] -- END

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Private mblnIsExternalApprover As Boolean
    Private mintFromApproverEmpunkid As Integer = 0
    Private mintToApproverEmpunkid As Integer = 0
    Private dtTemp As DataTable = Nothing
    'Nilay (01-Mar-2016) -- End


    'Pinkal (13-Jul-2017) -- Start
    'Enhancement - Bug Solved in Leave Approver Migration .
    Private mintFromApproverId As Integer = 0
    Private mintToApproverId As Integer = 0
    'Pinkal (13-Jul-2017) -- End



#End Region

#Region "Page's Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Nilay (07-Feb-2016) -- Start
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Nilay (07-Feb-2016) -- End

            Try
                ApproverType = CInt(b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3))))
            Catch ex As Exception
                Session("clsuser") = Nothing
                DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                Exit Sub
            End Try
            SetLanguage()
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) Then
                If IsPostBack = False Then
                    If ApproverType = enSwapApproverType.Leave Then
                        'Nilay (07-Feb-2016) -- Start
                        If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If
                        'Nilay (07-Feb-2016) -- End
                        LblCategory.Visible = False
                        cboExpenseCat.Visible = False
                        lblDetialHeader.Text = "Leave Swap Approver"
                    ElseIf ApproverType = enSwapApproverType.Loan Then
                        'Nilay (07-Feb-2016) -- Start
                        If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If
                        'Nilay (07-Feb-2016) -- End
                        LblCategory.Visible = False
                        cboExpenseCat.Visible = False
                        lblDetialHeader.Text = "Loan Swap Approver"
                    ElseIf ApproverType = enSwapApproverType.Claim_Request Then
                        'Nilay (07-Feb-2016) -- Start
                        If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If
                        'Nilay (07-Feb-2016) -- End
                        LblCategory.Visible = True
                        cboExpenseCat.Visible = True
                        lblDetialHeader.Text = "Claim & Request Swap Approver"
                    ElseIf ApproverType = enSwapApproverType.Performance Then
                        LblCategory.Visible = True
                        cboExpenseCat.Visible = True
                        lblDetialHeader.Text = "Perfotmance Swap Approver"
                    End If

                    Call FillCombo()
                    btnSave.Enabled = False
                    btnSwap.Enabled = True
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'Me.ViewState.Add("dsFromApproverEmp", dsFromApproverEmp)
                    'Me.ViewState.Add("dsToApproverEmp", dsToApproverEmp)
                    'Nilay (01-Mar-2016) -- End
                Else
                    dsFromApproverEmp = CType(Me.ViewState("dsFromApproverEmp"), DataSet)
                    dsToApproverEmp = CType(Me.ViewState("dsToApproverEmp"), DataSet)

                    'S.SANDEEP [30 JAN 2016] -- START
                    If Me.ViewState("dtComboFromApprover") IsNot Nothing Then
                        dtComboFromApprover = CType(Me.ViewState("dtComboFromApprover"), DataTable)
                    End If

                    If Me.ViewState("dtComboToApprover") IsNot Nothing Then
                        dtComboToApprover = CType(Me.ViewState("dtComboToApprover"), DataTable)
                    End If
                    'S.SANDEEP [30 JAN 2016] -- END

                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))
                    mintFromApproverEmpunkid = CInt(Me.ViewState("FromApproverEmpunkid"))
                    mintToApproverEmpunkid = CInt(Me.ViewState("ToApproverEmpunkid"))
                    dtTemp = CType(Me.ViewState("dtTemp"), DataTable)
                    'Nilay (01-Mar-2016) -- End


                    'Pinkal (13-Jul-2017) -- Start
                    'Enhancement - Bug Solved in Leave Approver Migration .
                    mintFromApproverId = CInt(Me.ViewState("FromApproverId"))
                    mintToApproverId = CInt(Me.ViewState("ToApproverId"))
                    'Pinkal (13-Jul-2017) -- End


                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dsFromApproverEmp") = dsFromApproverEmp
            Me.ViewState("dsToApproverEmp") = dsToApproverEmp

            'S.SANDEEP [30 JAN 2016] -- START
            If Me.ViewState("dtComboFromApprover") Is Nothing Then
                Me.ViewState("dtComboFromApprover") = dtComboFromApprover
            End If

            If Me.ViewState("dtComboToApprover") Is Nothing Then
                Me.ViewState("dtComboToApprover") = dtComboToApprover
            End If
            'S.SANDEEP [30 JAN 2016] -- END

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover
            Me.ViewState("FromApproverEmpunkid") = mintFromApproverEmpunkid
            Me.ViewState("ToApproverEmpunkid") = mintToApproverEmpunkid
            Me.ViewState("dtTemp") = dtTemp
            'Nilay (01-Mar-2016) -- End


            'Pinkal (13-Jul-2017) -- Start
            'Enhancement - Bug Solved in Leave Approver Migration .
            Me.ViewState("FromApproverId") = mintFromApproverId
            Me.ViewState("ToApproverId") = mintToApproverId
            'Pinkal (13-Jul-2017) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)   'Hemant (13 Aug 2020)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim dtTable As New DataTable

            If ApproverType = enSwapApproverType.Leave Then
                objLeaveApprover = New clsleaveapprover_master

                dsList = objLeaveApprover.GetList("List", _
                                                  Session("Database_Name").ToString, _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                               Session("EmployeeAsOnDate").ToString(), _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  CBool(Session("IsIncludeInactiveEmp")), True, True)


                dtTable = dsList.Tables("List").DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")

                Dim dr As DataRow = dtTable.NewRow
                dr("leaveapproverunkid") = 0
                dr("name") = "Select"
                dr("isexternalapprover") = False
                dtTable.Rows.InsertAt(dr, 0)
                cboFromApprover.DataValueField = "leaveapproverunkid"
                cboFromApprover.DataTextField = "name"
                cboFromApprover.DataSource = dtTable
                cboFromApprover.DataBind()
                dtTable = dsList.Tables(0)

                dtComboFromApprover = dtTable

                cboFromApprover_SelectedIndexChanged(cboFromApprover, New EventArgs)
                cboToApprover_SelectedIndexChanged(cboToApprover, New EventArgs)

            ElseIf ApproverType = enSwapApproverType.Loan Then
                Dim objlnApprover As New clsLoanApprover_master



                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                dsList = objlnApprover.getListForCombo("List", True, True)
                'dsList = objlnApprover.getListForCombo("List", True, True, False, True, Session("EmployeeAsOnDate").ToString(), CStr(Session("Database_Name")))
                dtTable = dsList.Tables("List").DefaultView.ToTable(True, "lnempapproverunkid", "name", "isexternalapprover")
                'Pinkal (18-Jun-2020) -- End


                cboFromApprover.DataTextField = "name"
                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'cboFromApprover.DataValueField = "lnapproverunkid"
                ' cboFromApprover.DataSource = dsList.Tables("List")
                cboFromApprover.DataValueField = "lnempapproverunkid"
                cboFromApprover.DataSource = dtTable
                'Pinkal (18-Jun-2020) -- End

                cboFromApprover.DataBind()

                cboFromApprover_SelectedIndexChanged(cboFromApprover, New EventArgs)
                cboToApprover_SelectedIndexChanged(cboToApprover, New EventArgs)

            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                    dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List")
                    dtTable = New DataView(dsList.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List")
                    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If

                cboExpenseCat.DataValueField = "Id"
                cboExpenseCat.DataTextField = "Name"
                cboExpenseCat.DataSource = dtTable
                cboExpenseCat.DataBind()
                cboExpenseCat.SelectedValue = "0"
                cboExpenseCat_SelectedIndexChanged(cboExpenseCat, New EventArgs)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetApproverLevel(ByVal sender As Object, ByVal intApproverID As Integer, Optional ByVal blnExternalApprover As Boolean = False, Optional ByVal intApproverEmpID As Integer = -1)
        Try
            Dim dsList As DataSet = Nothing

            If ApproverType = enSwapApproverType.Leave Then
                objLeaveApprover = New clsleaveapprover_master
                dsList = objLeaveApprover.GetLevelFromLeaveApprover(intApproverID, blnExternalApprover, True)

                If CType(sender, DropDownList).ID.ToUpper = "CBOFROMAPPROVER" Then
                    cboFromLevel.DataTextField = "levelname"
                    cboFromLevel.DataValueField = "levelunkid"
                    cboFromLevel.DataSource = dsList.Tables(0)
                    cboFromLevel.DataBind()
                    cboFromLevel.SelectedValue = "0"

                ElseIf CType(sender, DropDownList).ID.ToUpper = "CBOTOAPPROVER" Then
                    cboToLevel.DataTextField = "levelname"
                    cboToLevel.DataValueField = "levelunkid"
                    cboToLevel.DataSource = dsList.Tables(0)
                    cboToLevel.DataBind()
                    cboToLevel.SelectedValue = "0"
                End If

            ElseIf ApproverType = enSwapApproverType.Loan Then
                objLoanApprover = New clsLoanApprover_master

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'dsList = objLoanApprover.GetLevelFromLoanApprover(intApproverID, blnExternalApprover, True)
                dsList = objLoanApprover.GetLevelFromLoanApprover(intApproverEmpID, blnExternalApprover, True)
                'Pinkal (18-Jun-2020) -- End

                If CType(sender, DropDownList).ID.ToUpper = "CBOFROMAPPROVER" Then
                    cboFromLevel.DataTextField = "name"
                    cboFromLevel.DataValueField = "lnlevelunkid"
                    cboFromLevel.DataSource = dsList.Tables(0)
                    cboFromLevel.DataBind()
                    cboFromLevel.SelectedValue = "0"

                ElseIf CType(sender, DropDownList).ID.ToUpper = "CBOTOAPPROVER" Then
                    cboToLevel.DataTextField = "name"
                    cboToLevel.DataValueField = "lnlevelunkid"
                    cboToLevel.DataSource = dsList.Tables(0)
                    cboToLevel.DataBind()
                    cboToLevel.SelectedValue = "0"

                End If

            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'dsList = objExpenseApprover.GetExpApproverLevels(CInt(cboExpenseCat.SelectedValue), intApproverID, intApproverEmpID, True, "List")
                dsList = objExpenseApprover.GetExpApproverLevels(CInt(cboExpenseCat.SelectedValue), intApproverID, intApproverEmpID, blnExternalApprover, True, "List")
                'Pinkal (18-Jun-2020) -- End


                If CType(sender, DropDownList).ID.ToUpper = "CBOFROMAPPROVER" Then

                    cboFromLevel.DataTextField = "Name"
                    cboFromLevel.DataValueField = "Id"
                    cboFromLevel.DataSource = dsList.Tables(0)
                    cboFromLevel.DataBind()
                    cboFromLevel.SelectedValue = "0"
                    Call cboFromLevel_SelectedIndexChanged(cboFromLevel, New EventArgs)
                ElseIf CType(sender, DropDownList).ID.ToUpper = "CBOTOAPPROVER" Then
                    cboToLevel.DataTextField = "Name"
                    cboToLevel.DataValueField = "Id"
                    cboToLevel.DataSource = dsList.Tables(0)
                    cboToLevel.DataBind()
                    cboToLevel.SelectedValue = "0"
                    Call cboToLevel_SelectedIndexChanged(cboToLevel, New EventArgs)
                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure GetApproverLevel:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillFromApproverEmployeeList(ByVal dsFromApproverEmp As DataSet)
        Try
            If dsFromApproverEmp IsNot Nothing Then

                dgFromApprover.AutoGenerateColumns = False

                If ApproverType = enSwapApproverType.Leave Then

                    Dim field As BoundField

                    field = DirectCast(Me.dgFromApprover.Columns(0), BoundField)
                    field.DataField = "employeecode"

                    field = DirectCast(Me.dgFromApprover.Columns(1), BoundField)
                    field.DataField = "employeename"

                    field = DirectCast(Me.dgFromApprover.Columns(2), BoundField)
                    field.DataField = "employeeunkid"

                ElseIf ApproverType = enSwapApproverType.Loan Then

                    Dim field As BoundField

                    field = DirectCast(Me.dgFromApprover.Columns(0), BoundField)
                    field.DataField = "employeecode"

                    field = DirectCast(Me.dgFromApprover.Columns(1), BoundField)
                    field.DataField = "employeename"

                    field = DirectCast(Me.dgFromApprover.Columns(2), BoundField)
                    field.DataField = "employeeunkid"

                ElseIf ApproverType = enSwapApproverType.Performance Then

                ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                    Dim field As BoundField

                    field = DirectCast(Me.dgFromApprover.Columns(0), BoundField)
                    field.DataField = "ecode"

                    field = DirectCast(Me.dgFromApprover.Columns(1), BoundField)
                    field.DataField = "ename"

                    field = DirectCast(Me.dgFromApprover.Columns(2), BoundField)
                    field.DataField = "employeeunkid"

                End If

                dgFromApprover.DataSource = dsFromApproverEmp.Tables(0)
                dgFromApprover.DataBind()

                If Me.ViewState("FromApprover") IsNot Nothing Then
                    Me.ViewState("FromApprover") = dsFromApproverEmp
                Else
                    Me.ViewState.Add(("FromApprover"), dsFromApproverEmp)
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillFromApproverEmployeeList:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillToApproverEmployeeList(ByVal dsToApproverEmp As DataSet)
        Try
            If dsToApproverEmp IsNot Nothing Then

                dgToApprover.AutoGenerateColumns = False

                If ApproverType = enSwapApproverType.Leave Then

                    Dim field As BoundField

                    field = DirectCast(Me.dgToApprover.Columns(0), BoundField)
                    field.DataField = "employeecode"

                    field = DirectCast(Me.dgToApprover.Columns(1), BoundField)
                    field.DataField = "employeename"

                    field = DirectCast(Me.dgToApprover.Columns(2), BoundField)
                    field.DataField = "employeeunkid"

                ElseIf ApproverType = enSwapApproverType.Loan Then

                    Dim field As BoundField

                    field = DirectCast(Me.dgToApprover.Columns(0), BoundField)
                    field.DataField = "employeecode"

                    field = DirectCast(Me.dgToApprover.Columns(1), BoundField)
                    field.DataField = "employeename"

                    field = DirectCast(Me.dgToApprover.Columns(2), BoundField)
                    field.DataField = "employeeunkid"

                ElseIf ApproverType = enSwapApproverType.Performance Then

                ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                    Dim field As BoundField

                    field = DirectCast(Me.dgToApprover.Columns(0), BoundField)
                    field.DataField = "ecode"

                    field = DirectCast(Me.dgToApprover.Columns(1), BoundField)
                    field.DataField = "ename"

                    field = DirectCast(Me.dgToApprover.Columns(2), BoundField)
                    field.DataField = "employeeunkid"

                End If

                dgToApprover.DataSource = dsToApproverEmp.Tables(0)
                dgToApprover.DataBind()

                If Me.ViewState("ToApprover") IsNot Nothing Then
                    Me.ViewState("ToApprover") = dsToApproverEmp
                Else
                    Me.ViewState.Add(("ToApprover"), dsToApproverEmp)
                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillToApproverEmployeeList:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboFromApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please Select From Approver to swap employee(s)."), Me)
                Return False

            ElseIf CInt(cboFromLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Level to swap employee(s)."), Me)
                Return False

            ElseIf CInt(cboToApprover.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please Select To Approver to swap employee(s)."), Me)
                Return False

            ElseIf CInt(cboToLevel.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Level to swap employee(s)."), Me)
                Return False

            End If

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region "Button's Event"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() = False Then Exit Sub

            If Me.ViewState("FromApprover") IsNot Nothing Then
                dtFromApproverEmp = CType(Me.ViewState("FromApprover"), DataSet).Tables(0)
            End If
            If Me.ViewState("ToApprover") IsNot Nothing Then
                dtToApproverEmp = CType(Me.ViewState("ToApprover"), DataSet).Tables(0)
            End If

            If ApproverType = enSwapApproverType.Leave Then

                objLeaveApprover = New clsleaveapprover_master
                objLeaveApprover._SwapFromApproverunkid = mintFromApproverId
                objLeaveApprover._SwapToApproverunkid = mintToApproverId
                objLeaveApprover._Userumkid = CInt(Session("UserId"))

                Blank_ModuleName()
                clsCommonATLog._WebFormName = "frmApprover_Swapping"
                StrModuleName2 = "mnuLeaveInformation"
                clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
                clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

                If objLeaveApprover.SwapApprover(dtFromApproverEmp, dtToApproverEmp, CBool(Session("LeaveApproverForLeaveType")), CBool(Session("PaymentApprovalwithLeaveApproval"))) = False Then
                    DisplayMessage.DisplayMessage(objLeaveApprover._Message, Me)
                    Exit Sub
                Else
                    DisplayMessage.DisplayMessage("Approver Swapping done successfully.", Me)
                End If

            ElseIf ApproverType = enSwapApproverType.Loan Then

                objLoanApprover = New clsLoanApprover_master
                objLoanApprover._SwapFromApproverunkid = CInt(cboFromApprover.SelectedValue)
                objLoanApprover._SwapToApproverunkid = CInt(cboToApprover.SelectedValue)
                objLoanApprover._Userunkid = CInt(Session("UserId"))

                Blank_ModuleName()
                clsCommonATLog._WebFormName = "frmApprover_Swapping"
                StrModuleName2 = "mnuLoan_Advance_Savings"
                clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
                clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))


                Blank_ModuleName()
                StrModuleName2 = "mnuLoan_Advance_Savings"
                objLoanApprover._WebClientIP = Session("IP_ADD").ToString
                objLoanApprover._WebFormName = "frmApprover_Swapping"
                objLoanApprover._WebHostName = Session("HOST_NAME").ToString

                If objLoanApprover.SwapApprover(dtFromApproverEmp, dtToApproverEmp, CBool(Session("LoanApprover_ForLoanScheme"))) = False Then
                    DisplayMessage.DisplayMessage(objLoanApprover._Message, Me)
                    Exit Sub
                Else
                    DisplayMessage.DisplayMessage("Approver Swapping done successfully.", Me)
                End If


            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                Blank_ModuleName()
                clsCommonATLog._WebFormName = "frmApprover_Swapping"
                StrModuleName2 = "mnuUtilitiesMain"
                StrModuleName2 = "mnuClaimsExpenses"
                clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
                clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

                objExpenseApprover._SwapFromApproverunkid = CInt(Me.ViewState("cboFromApprover"))
                objExpenseApprover._SwapToApproverunkid = CInt(Me.ViewState("cboToApprover"))
                objExpenseApprover._Userunkid = CInt(Session("UserId"))
                If objExpenseApprover.SwapApprover(dtFromApproverEmp, dtToApproverEmp, DateAndTime.Now.Date) = False Then
                    DisplayMessage.DisplayMessage(objExpenseApprover._Message, Me)
                    Exit Sub
                Else
                    DisplayMessage.DisplayMessage("Approver Swapping done successfully.", Me)

                    cboExpenseCat.SelectedValue = "0"
                    cboExpenseCat.Enabled = True
                End If

            End If

            Call FillCombo()

            cboFromApprover.SelectedValue = "0"
            cboFromApprover.Enabled = True
            cboFromLevel.SelectedValue = "0"
            cboFromLevel.Enabled = True
            cboToApprover.SelectedValue = "0"
            cboToApprover.Enabled = True
            cboToLevel.SelectedValue = "0"
            cboToLevel.Enabled = True

            dgFromApprover.DataSource = Nothing
            dgFromApprover.DataBind()
            dgToApprover.DataSource = Nothing
            dgToApprover.DataBind()

            btnSwap.Enabled = True
            btnSave.Enabled = False

            Me.ViewState("dsFromApproverEmp") = Nothing
            Me.ViewState("dsToApproverEmp") = Nothing
            Me.ViewState("FromApprover") = Nothing
            Me.ViewState("ToApprover") = Nothing

            dtFromApproverEmp = Nothing
            dtToApproverEmp = Nothing
            dsFromApproverEmp = Nothing
            dsToApproverEmp = Nothing
            Me.ViewState("dtComboFromApprover") = Nothing
            Me.ViewState("dtComboToApprover") = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure btnSave_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSwap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSwap.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            Dim drFromRow() As DataRow = Nothing : Dim drToRow() As DataRow = Nothing
            If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                If ApproverType = enSwapApproverType.Loan OrElse ApproverType = enSwapApproverType.Claim_Request OrElse ApproverType = enSwapApproverType.Leave Then
                    drFromRow = dsFromApproverEmp.Tables(0).Select("employeeunkid = " & mintToApproverEmpunkid)
                Else
                    drFromRow = dsFromApproverEmp.Tables(0).Select("employeeunkid = " & CInt(cboToApprover.SelectedValue))
                End If
            End If

            If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                If ApproverType = enSwapApproverType.Loan OrElse ApproverType = enSwapApproverType.Claim_Request OrElse ApproverType = enSwapApproverType.Leave Then
                    drToRow = dsToApproverEmp.Tables(0).Select("employeeunkid = " & mintFromApproverEmpunkid)
                Else
                    drToRow = dsToApproverEmp.Tables(0).Select("employeeunkid = " & CInt(cboFromApprover.SelectedValue))
                End If
            End If


            If drFromRow IsNot Nothing AndAlso drFromRow.Length > 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, You cannot swap approver") & " " & cboToApprover.SelectedItem.Text & " " & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, ".Reason:the selected") & " " & lblToApprover.Text & " " & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "is already in the list of") & " " & lblFromApprover.Text & " " & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "as an employee."), Me)
                Exit Sub
            ElseIf drToRow IsNot Nothing AndAlso drToRow.Length > 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, You cannot swap approver") & " " & cboFromApprover.SelectedItem.Text & " " & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, ".Reason:the selected") & " " & lblFromApprover.Text & " " & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "is already in the list of") & " " & lblToApprover.Text & " " & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "as an employee."), Me)
                Exit Sub
            End If

            If ApproverType = enSwapApproverType.Leave Then
                FillFromApproverEmployeeList(dsToApproverEmp)
                FillToApproverEmployeeList(dsFromApproverEmp)

            ElseIf ApproverType = enSwapApproverType.Loan Then
                FillFromApproverEmployeeList(dsToApproverEmp)
                FillToApproverEmployeeList(dsFromApproverEmp)

            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then
                cboExpenseCat.Enabled = False
                FillFromApproverEmployeeList(dsToApproverEmp)
                FillToApproverEmployeeList(dsFromApproverEmp)

            End If

            cboFromApprover.Enabled = False
            cboFromLevel.Enabled = False
            cboToApprover.Enabled = False
            cboToLevel.Enabled = False
            btnSave.Enabled = True
            btnSwap.Enabled = False

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure btnSwap_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            'Response.Redirect("~/Userhome.aspx")
            Response.Redirect("~/Userhome.aspx", False)
            'Pinkal (11-Feb-2022) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure btnClose_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Protected Sub cboExpenseCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpenseCat.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            If ApproverType = enSwapApproverType.Claim_Request Then


                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsList = objExpenseApprover.getListForCombo(CInt(cboExpenseCat.SelectedValue), True, "List")
                dsList = objExpenseApprover.getListForCombo(CInt(cboExpenseCat.SelectedValue), Session("Database_Name").ToString(), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       Session("EmployeeAsOnDate").ToString(), _
                                                       Session("UserAccessModeSetting").ToString(), True, _
                                                       "List", "", True, True)
                'Gajanan [23-May-2020] -- End


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

                'cboFromApprover.DataValueField = "id"
                'cboFromApprover.DataTextField = "Name"
                'cboFromApprover.DataSource = dsList.Tables(0)
                'cboFromApprover.DataBind()
                'cboFromApprover.SelectedValue = "0"
                Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
                cboFromApprover.DataValueField = "ApproverEmpunkid"
                cboFromApprover.DataTextField = "Name"
                cboFromApprover.DataSource = dtTable
                cboFromApprover.DataBind()
                cboFromApprover.SelectedValue = "0"

                'Pinkal (18-Jun-2020) -- End

                cboFromApprover_SelectedIndexChanged(cboFromApprover, New EventArgs)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboExpenseCat_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboFromApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFromApprover.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing
            Dim iApproverID As Integer = 0
            Dim dtmp() As DataRow = Nothing


            If ApproverType = enSwapApproverType.Leave Then

                objLeaveApprover = New clsleaveapprover_master

                dsList = objLeaveApprover.GetList("List", _
                                                  Session("Database_Name").ToString, _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                Session("EmployeeAsOnDate").ToString(), _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  CBool(Session("IsIncludeInactiveEmp")), True, True)

                If CInt(cboFromApprover.SelectedValue) <= 0 Then
                    dtTemp = dsList.Tables(0)
                Else
                    'dtTemp = New DataView(dsList.Tables("List"), " approverunkid NOT IN(" & CInt(cboFromApprover.SelectedValue) & ") ", "", DataViewRowState.CurrentRows).ToTable()

                    Dim dRow As DataRow() = dsList.Tables("List").Select("leaveapproverunkid = " & CInt(cboFromApprover.SelectedValue))
                    If dRow.Length > 0 Then
                        mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                        mintFromApproverEmpunkid = CInt(dRow(0).Item("leaveapproverunkid"))
                    End If

                    dtTemp = New DataView(dsList.Tables(0), "leaveapproverunkid NOT IN(" & CInt(cboFromApprover.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable()
                End If

                dtTemp = dtTemp.DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")

                Dim dr As DataRow = dtTemp.NewRow
                dr("leaveapproverunkid") = 0
                dr("name") = "Select"
                dr("isexternalapprover") = False
                dtTemp.Rows.InsertAt(dr, 0)

                cboToApprover.DataTextField = "name"
                cboToApprover.DataValueField = "leaveapproverunkid"
                cboToApprover.DataSource = dtTemp
                cboToApprover.DataBind()
                cboToApprover.SelectedValue = "0"


            ElseIf ApproverType = enSwapApproverType.Loan Then

                objLoanApprover = New clsLoanApprover_master


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                dsList = objLoanApprover.getListForCombo("List", True, True)
                'dsList = objLoanApprover.getListForCombo("List", True, True, False, True, Session("EmployeeAsOnDate").ToString(), CStr(Session("Database_Name")))
                'Pinkal (18-Jun-2020) -- End


                If CInt(cboFromApprover.SelectedValue) > 0 Then

                    'Pinkal (18-Jun-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                    'Dim dRow As DataRow() = dsList.Tables("List").Select("lnapproverunkid = " & CInt(cboFromApprover.SelectedValue))
                    Dim dRow As DataRow() = dsList.Tables("List").Select("lnempapproverunkid = " & CInt(cboFromApprover.SelectedValue))
                    If dRow.Length > 0 Then
                        mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                        mintFromApproverEmpunkid = CInt(dRow(0).Item("lnempapproverunkid"))
                    End If
                    'dtTemp = New DataView(dsList.Tables("List"), " lnapproverunkid NOT IN(" & CInt(cboFromApprover.SelectedValue) & ") ", "", DataViewRowState.CurrentRows).ToTable()
                    dtTemp = New DataView(dsList.Tables("List"), " lnempapproverunkid NOT IN(" & CInt(cboFromApprover.SelectedValue) & ") ", "", DataViewRowState.CurrentRows).ToTable()
                    'Pinkal (18-Jun-2020) -- End
                Else
                    dtTemp = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable()
                    mblnIsExternalApprover = False
                End If

                With cboToApprover
                    .DataTextField = "name"

                    'Pinkal (18-Jun-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                    '.DataValueField = "lnapproverunkid"
                    .DataValueField = "lnempapproverunkid"
                    'Pinkal (18-Jun-2020) -- End
                    .DataSource = dtTemp
                    .DataBind()
                End With


            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then


                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsList = objExpenseApprover.getListForCombo(CInt(cboExpenseCat.SelectedValue), True, "List")
                dsList = objExpenseApprover.getListForCombo(CInt(cboExpenseCat.SelectedValue), Session("Database_Name").ToString(), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       Session("EmployeeAsOnDate").ToString(), _
                                       Session("UserAccessModeSetting").ToString(), True, _
                                       "List", "", True, True)
                'Gajanan [23-May-2020] -- End


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

                'Dim dtTable As DataTable = Nothing
                'If CInt(cboFromApprover.SelectedValue) > 0 Then
                '    Dim dRow As DataRow() = dsList.Tables(0).Select("id = " & CInt(cboFromApprover.SelectedValue))
                '    If dRow.Length > 0 Then
                '        mintFromApproverEmpunkid = CInt(dRow(0).Item("ApproverEmpunkid"))
                '    End If
                '    dtTemp = New DataView(dsList.Tables(0), " id <> " & CInt(cboFromApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                'Else
                '    dtTemp = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
                'End If

                'With cboToApprover
                '    .DataTextField = "Name"
                '    .DataValueField = "id"
                '    .DataSource = dtTemp
                '    .DataBind()
                '    .SelectedValue = "0"
                'End With

                Dim dRow As DataRow() = dsList.Tables("List").Select("ApproverEmpunkid = " & CInt(cboFromApprover.SelectedValue))
                If dRow.Length > 0 Then
                    mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                    mintFromApproverEmpunkid = CInt(dRow(0).Item("ApproverEmpunkid"))
                End If
                dRow = Nothing

                If CInt(cboFromApprover.SelectedValue) > 0 Then
                    dtTemp = New DataView(dsList.Tables(0), " ApproverEmpunkid <> " & CInt(cboFromApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
                Else
                    dtTemp = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
                End If

                With cboToApprover
                    .DataTextField = "Name"
                    .DataValueField = "ApproverEmpunkid"
                    .DataSource = dtTemp
                    .DataBind()
                    .SelectedValue = "0"
                End With

                'iApproverID = CInt(cboFromApprover.SelectedValue)

                'Pinkal (18-Jun-2020) -- End

                Call cboToApprover_SelectedIndexChanged(cboToApprover, New EventArgs)
            End If


            If ApproverType = enSwapApproverType.Leave Then
                Call GetApproverLevel(sender, mintFromApproverEmpunkid, mblnIsExternalApprover)

            ElseIf ApproverType = enSwapApproverType.Loan Then

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'Call GetApproverLevel(sender, mintFromApproverEmpunkid, mblnIsExternalApprover )
                Call GetApproverLevel(sender, iApproverID, mblnIsExternalApprover, mintFromApproverEmpunkid)
                'Pinkal (18-Jun-2020) -- End



            ElseIf ApproverType = enSwapApproverType.Claim_Request Then
                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'Call GetApproverLevel(sender, iApproverID, , mintFromApproverEmpunkid)
                Call GetApproverLevel(sender, iApproverID, mblnIsExternalApprover, mintFromApproverEmpunkid)
                'Pinkal (18-Jun-2020) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboFromApprover_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboFromLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFromLevel.SelectedIndexChanged
        Try

            If ApproverType = enSwapApproverType.Leave Then
                objLeaveApprover = New clsleaveapprover_master


                'Pinkal (14-May-2020) -- Start
                'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                'dsFromApproverEmp = objLeaveApprover.GetEmployeeFromApprover(mintFromApproverEmpunkid, CInt(cboFromLevel.SelectedValue), _
                '                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                '                                                            mblnIsExternalApprover)


                dsFromApproverEmp = objLeaveApprover.GetEmployeeFromApprover(Session("Database_Name").ToString(), mintFromApproverEmpunkid, CInt(cboFromLevel.SelectedValue), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                            mblnIsExternalApprover, True)
                'Pinkal (14-May-2020) -- End



                Call FillFromApproverEmployeeList(dsFromApproverEmp)

                If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                    mintFromApproverId = CInt(dsFromApproverEmp.Tables(0).Rows(0)("approverunkid"))
                Else
                    mintFromApproverId = 0
                End If


            ElseIf ApproverType = enSwapApproverType.Loan Then
                objLoanApprover = New clsLoanApprover_master
                'Hemant (29 May 2020) -- Start
                'ENHANCEMENT : Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsFromApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                                CStr(Session("UserAccessModeSetting")), True, _
                '                                                                CBool(Session("IsIncludeInactiveEmp")), "List", _
                '                                                                mintFromApproverEmpunkid, _
                '                                                                CInt(cboFromLevel.SelectedValue), _
                '                                                                mblnIsExternalApprover)
                dsFromApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, _
                                                                                True, "List", _
                                                                                mintFromApproverEmpunkid, _
                                                                                CInt(cboFromLevel.SelectedValue), _
                                                                                mblnIsExternalApprover)
                'Hemant (29 May 2020) -- End



                Call FillFromApproverEmployeeList(dsFromApproverEmp)


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                    Me.ViewState.Add("cboFromApprover", CInt(dsFromApproverEmp.Tables(0).Rows(0)("lnapproverunkid")))
                Else
                    Me.ViewState.Add("cboFromApprover", 0)
                End If
                'Pinkal (18-Jun-2020) -- End

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then



                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

                'dsFromApproverEmp = objExpenseApprover.GetApproverAccess(CInt(cboFromApprover.SelectedValue), mintFromApproverEmpunkid, CInt(cboFromLevel.SelectedValue), _
                '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                dsFromApproverEmp = objExpenseApprover.GetApproverAccess(Session("Database_Name").ToString(), _
                                                          0, CInt(cboFromApprover.SelectedValue), _
                                                          CInt(cboFromLevel.SelectedValue), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                          True)
                'Pinkal (18-Jun-2020) -- End


                FillFromApproverEmployeeList(dsFromApproverEmp)
                If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                    Me.ViewState.Add("cboFromApprover", CInt(dsFromApproverEmp.Tables(0).Rows(0)("crapproverunkid")))
                Else
                    Me.ViewState.Add("cboFromApprover", 0)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboFromLevel_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboToApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboToApprover.SelectedIndexChanged
        Try

            If ApproverType = enSwapApproverType.Leave Then
                If CInt(cboToApprover.SelectedValue) <= 0 Then
                    cboToLevel.SelectedValue = CStr(0)
                    mblnIsExternalApprover = False
                Else
                    Dim dRow As DataRow() = dtTemp.Select("leaveapproverunkid = " & CInt(cboToApprover.SelectedValue))
                    If dRow.Length > 0 Then
                        mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                        mintToApproverEmpunkid = CInt(dRow(0).Item("leaveapproverunkid"))
                    End If
                End If
                GetApproverLevel(sender, mintToApproverEmpunkid, mblnIsExternalApprover)

            ElseIf ApproverType = enSwapApproverType.Loan Then
                If CInt(cboToApprover.SelectedValue) <= 0 Then
                    cboToLevel.SelectedValue = CStr(0)
                    mblnIsExternalApprover = False
                Else

                    'Pinkal (18-Jun-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                    'Dim dRow As DataRow() = dtTemp.Select("lnapproverunkid = " & CInt(cboToApprover.SelectedValue))
                    Dim dRow As DataRow() = dtTemp.Select("lnempapproverunkid = " & CInt(cboToApprover.SelectedValue))
                    'Pinkal (18-Jun-2020) -- End
                    If dRow.Length > 0 Then
                        mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                        mintToApproverEmpunkid = CInt(dRow(0).Item("lnempapproverunkid"))
                    End If
                End If

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'GetApproverLevel(sender, mintToApproverEmpunkid, mblnIsExternalApprover)
                GetApproverLevel(sender, 0, mblnIsExternalApprover, mintToApproverEmpunkid)
                'Pinkal (18-Jun-2020) -- End

            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'Dim dRow As DataRow() = dtTemp.Select("id = " & CInt(cboToApprover.SelectedValue))
                'If dRow.Length > 0 Then
                '    mintToApproverEmpunkid = CInt(dRow(0).Item("ApproverEmpunkid"))
                'End If
                'GetApproverLevel(sender, CInt(cboToApprover.SelectedValue), , mintToApproverEmpunkid)

                Dim dRow As DataRow() = dtTemp.Select("ApproverEmpunkid = " & CInt(cboToApprover.SelectedValue))
                If dRow.Length > 0 Then
                    mblnIsExternalApprover = CBool(dRow(0).Item("isexternalapprover"))
                    mintToApproverEmpunkid = CInt(dRow(0).Item("ApproverEmpunkid"))
                End If
                GetApproverLevel(sender, 0, mblnIsExternalApprover, CInt(cboToApprover.SelectedValue))

                'Pinkal (18-Jun-2020) -- End



            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboToApprover_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboToLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboToLevel.SelectedIndexChanged
        Try
            objLeaveApprover = New clsleaveapprover_master

            If ApproverType = enSwapApproverType.Leave Then
                dsToApproverEmp = objLeaveApprover.GetEmployeeFromApprover(Session("Database_Name").ToString(), mintToApproverEmpunkid, CInt(cboToLevel.SelectedValue), _
                                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                          mblnIsExternalApprover, True)


                Call FillToApproverEmployeeList(dsToApproverEmp)

                If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                    mintToApproverId = CInt(dsToApproverEmp.Tables(0).Rows(0)("approverunkid"))
                Else
                    mintToApproverId = 0
                End If


            ElseIf ApproverType = enSwapApproverType.Loan Then

                objLoanApprover = New clsLoanApprover_master

                'Hemant (29 May 2020) -- Start
                'ENHANCEMENT : Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsToApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                                              CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                '                                                            mintToApproverEmpunkid, CInt(cboToLevel.SelectedValue), _
                '                                                            mblnIsExternalApprover)
                dsToApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                              CStr(Session("UserAccessModeSetting")), True, True, "List", _
                                                                            mintToApproverEmpunkid, CInt(cboToLevel.SelectedValue), _
                                                                            mblnIsExternalApprover)
                'Hemant (29 May 2020) -- End


                FillToApproverEmployeeList(dsToApproverEmp)

            ElseIf ApproverType = enSwapApproverType.Performance Then

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'dsToApproverEmp = objExpenseApprover.GetApproverAccess(CInt(cboToApprover.SelectedValue), mintToApproverEmpunkid, CInt(cboToLevel.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                dsToApproverEmp = objExpenseApprover.GetApproverAccess(Session("Database_Name").ToString(), _
                                                          0, mintToApproverEmpunkid, _
                                                          CInt(cboToLevel.SelectedValue), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                          True)
                'Pinkal (18-Jun-2020) -- End



                FillToApproverEmployeeList(dsToApproverEmp)
                If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                    Me.ViewState.Add("cboToApprover", CInt(dsToApproverEmp.Tables(0).Rows(0)("crapproverunkid")))
                Else
                    Me.ViewState.Add("cboToApprover", 0)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboToLevel_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, lblPageHeader.Text)
            lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, lblPageHeader.Text)

            Me.lblFromLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFromLevel.ID, Me.lblFromLevel.Text)
            Me.lblToApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblToApprover.ID, Me.lblToApprover.Text)
            Me.lblToLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblToLevel.ID, Me.lblToLevel.Text)

            Me.btnSwap.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSwap.ID, Me.btnSwap.Text)
            Me.dgFromApprover.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgFromApprover.Columns(0).FooterText, Me.dgFromApprover.Columns(0).HeaderText)
            Me.dgFromApprover.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgFromApprover.Columns(1).FooterText, Me.dgFromApprover.Columns(1).HeaderText)
            Me.dgToApprover.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgToApprover.Columns(0).FooterText, Me.dgToApprover.Columns(0).HeaderText)
            Me.dgToApprover.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgToApprover.Columns(1).FooterText, Me.dgToApprover.Columns(1).HeaderText)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"EZeeHeading1", Me.lblDetialHeader.Text)
            Me.LblCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblCategory.ID, Me.LblCategory.Text)
            Me.lblFromApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFromApprover.ID, Me.lblFromApprover.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
