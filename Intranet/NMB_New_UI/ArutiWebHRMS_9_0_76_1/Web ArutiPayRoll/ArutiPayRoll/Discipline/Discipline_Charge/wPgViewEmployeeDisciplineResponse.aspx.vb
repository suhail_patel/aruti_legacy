﻿#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.eZeeDataType
Imports Aruti.Data.clsEmployee_Master
Imports eZeeCommonLib.clsDataOperation
Imports System.IO
Imports System.Configuration
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading

#End Region

Partial Class Discipline_Discipline_Charge_wPgViewEmployeeDisciplineResponse
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineFiling"
    Dim DisplayMessage As New CommonCodes
    Private objDisciplineFileMaster As clsDiscipline_file_master
    Private mdtCharge As New DataTable
    Private objCONN As SqlConnection
    Private mintEmployeeId As Integer = 0
    Private mintDisciplineFileUnkid As Integer = 0
    Private mdtAttchment As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    Private mintDisiciplineFileTranUnkId As Integer = 0
    Private mintReportingEmployeeId As Integer = 0

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ChargeTable") = mdtCharge
            Session("mdtAttchment") = mdtAttchment
            Me.ViewState("mintDisiciplineFileTranUnkId") = mintDisiciplineFileTranUnkId
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            SetControlCaptions()
            Call SetLanguage()
            'S.SANDEEP |11-NOV-2019| -- END

            If Session("clsuser") Is Nothing OrElse (Request.QueryString.Count > 0 AndAlso Request.QueryString("uploadimage") Is Nothing) Then
                Call DirectNotificationLink()
                If Session("isUrlPost") Is Nothing Then
                    Session("isUrlPost") = True
                End If
            Else
                If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If
                If IsPostBack = False Then
                    Call FillCombo()
                End If
            End If


            If IsPostBack Then
                mdtCharge = CType(Me.ViewState("ChargeTable"), DataTable)
                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                mintDisiciplineFileTranUnkId = CInt(Me.ViewState("mintDisiciplineFileTranUnkId"))
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1 :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub DirectNotificationLink()
        Try
            If IsPostBack = False Then
                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim objPwdOpt As New clsPassowdOptions
                    Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length > 0 Then
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        mintDisciplineFileUnkid = CInt(arr(1))
                        mintEmployeeId = CInt(arr(2))

                        If CBool(arr(4)) = True Then
                            HttpContext.Current.Session("UserId") = CInt(arr(3))
                        Else
                            mintReportingEmployeeId = CInt(arr(3))
                        End If

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False) Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If

                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()

                        If HttpContext.Current.Session("EmployeeAsOnDate") Is Nothing Then
                            HttpContext.Current.Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                        End If

                        Dim clsuser As User = Nothing
                        If CBool(arr(4)) = True Then
                            Dim objUsr As New clsUserAddEdit
                            objUsr._Userunkid = Session("UserId")
                            clsuser = New User(objUsr._Username, objUsr._Password, Session("mdbname").ToString, objUsr._Userunkid)
                        Else
                            Dim objEmp As New clsEmployee_Master
                            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = mintReportingEmployeeId ' mintEmployeeId
                            clsuser = New User(objEmp._Displayname, objEmp._Password, Session("mdbname").ToString, mintEmployeeId)
                        End If


                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        If CBool(arr(4)) = True Then
                            HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        Else
                            HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        End If
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        HttpContext.Current.Session("DisplayName") = clsuser.DisplayName
                        strError = ""

                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        objDisciplineFileMaster = New clsDiscipline_file_master
                        objDisciplineFileMaster._Disciplinefileunkid = mintDisciplineFileUnkid

                        'If CInt(objDisciplineFileMaster._Responsetypeunkid) > 0 AndAlso objDisciplineFileMaster._Response_Date <> Nothing Then
                        '    DisplayMessage.DisplayMessage("Sorry, Response is already given for posted Charge.", Me, Session("rootpath").ToString() & "Index.aspx")
                        'End If

                        Call FillCombo()
                        cboReferenceNo.SelectedValue = CStr(mintDisciplineFileUnkid)
                        Call cboReferenceNo_SelectedIndexChanged(cboReferenceNo, New EventArgs())
                        'dtpResponseDate.SetDate = CDate(DateAndTime.Now.ToShortDateString).Date

                    Else
                        Response.Redirect("../Index.aspx", False)
                        Exit Sub
                    End If
                Else
                    Response.Redirect("../Index.aspx", False)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("DirectNotificationLink :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCMaster As New clsCommon_Master
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
lbl:            Dim objEmployee As New clsEmployee_Master
                Dim dsFill As New DataSet
                Dim blnApplyUAC As Boolean = True
                If mintReportingEmployeeId > 0 Then
                    blnApplyUAC = False
                End If
                dsFill = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                     CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                     CStr(Session("UserAccessModeSetting")), True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), "List", True, mintEmployeeId, , , , , , , , , , , , , , , , blnApplyUAC)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsFill.Tables("List")
                    .DataBind()
                    If mintEmployeeId > 0 Then
                        .SelectedValue = mintEmployeeId
                        Call cboEmployee_SelectedIndexChanged(New Object, New EventArgs)
                    Else
                        .SelectedValue = 0
                    End If
                End With
            Else
                If mintReportingEmployeeId > 0 Then
                    GoTo lbl
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    cboEmployee.DataSource = objglobalassess.ListOfEmployee
                    cboEmployee.DataTextField = "loginname"
                    cboEmployee.DataValueField = "employeeunkid"
                    cboEmployee.DataBind()
                End If
            End If
            Call cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillChargeList()
        Try
            If mdtCharge IsNot Nothing Then
                dgvChargeCountList.AutoGenerateColumns = False
                dgvChargeCountList.DataSource = mdtCharge
                dgvChargeCountList.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillChargeList :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValueByPersonInvolved()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim objDepartment As New clsDepartment

            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

            objJob._Jobunkid = objEmployee._Jobunkid
            objDepartment._Departmentunkid = objEmployee._Departmentunkid

            txtJobTitle.Text = objJob._Job_Name
            txtDepartment.Text = objDepartment._Name

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValueByPersonInvolved :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValueByReferenceNo()
        Try
            objDisciplineFileMaster = New clsDiscipline_file_master
            Dim objDisciplineFileTran As New clsDiscipline_file_tran

            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)

            If objDisciplineFileMaster._Chargedate <> Nothing Then
                txtChargeDate.Text = CStr(CDate(objDisciplineFileMaster._Chargedate).Date)
            Else
                txtChargeDate.Text = ""
            End If

            If objDisciplineFileMaster._Interdictiondate <> Nothing Then
                txtInterdictionDate.Text = CStr(CDate(objDisciplineFileMaster._Interdictiondate).Date)
            Else
                txtInterdictionDate.Text = ""
            End If

            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description

            '=====================  Fill Charges Counts ====================================
            objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            mdtCharge = objDisciplineFileTran._ChargesTable

            Call FillChargeList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValueByReferenceNo :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            mdtAttchment = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, mintDisiciplineFileTranUnkId, CStr(Session("Document_Path"))).Copy
            If mdtAttchment Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttchment, "AUD <> 'D' AND form_name = 'frmChargeCountResponse' ", "", DataViewRowState.CurrentRows)
            dgvResponse.AutoGenerateColumns = False
            dgvResponse.DataSource = dtView
            dgvResponse.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillAttachment", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvChargeCountList.Columns(9).FooterText, dgvChargeCountList.Columns(9).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |11-NOV-2019| -- END

#End Region

#Region " ComboBox's Events "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objDisciplineFileMaster As New clsDiscipline_file_master

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call GetValueByPersonInvolved()

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'dsList = objDisciplineFileMaster.GetComboList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                              CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, , _
                '                                              CInt(cboEmployee.SelectedValue), , , , False)
                dsList = objDisciplineFileMaster.GetComboList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                              CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, , _
                                                              CInt(cboEmployee.SelectedValue), , , , False, False)
                'S.SANDEEP |11-NOV-2019| -- END
                With cboReferenceNo
                    .DataValueField = "disciplinefileunkid"
                    .DataTextField = "reference_no"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    If dsList.Tables("List").Rows.Count = 2 Then
                        .SelectedValue = CStr(dsList.Tables("List").Rows(1).Item("disciplinefileunkid"))
                    Else
                        .SelectedValue = CStr(0)
                    End If
                End With
                Call cboReferenceNo_SelectedIndexChanged(cboReferenceNo, New EventArgs())
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
        Try
            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                Call GetValueByReferenceNo()
            Else
                dgvChargeCountList.DataSource = Nothing
                dgvChargeCountList.DataBind()
                txtChargeDate.Text = ""
                txtInterdictionDate.Text = ""
                txtChargeDescription.Text = ""
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboReferenceNo_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("ChargeTable") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Link Event "

    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim gvItem As DataGridItem = CType(lnk.NamingContainer, DataGridItem)
            mintDisiciplineFileTranUnkId = CInt(gvItem.Cells(10).Text)
            FillAttachment()
            popup_DisciplineCharge.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("link_Click" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Protected Sub dgvResponse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvResponse.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(4).Text) > 0 Then
                    xrow = mdtAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(4).Text) & "")
                Else
                    xrow = mdtAttchment.Select("GUID = '" & e.Item.Cells(3).Text.Trim & "'")
                End If
                If e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not Exist...", Me)
                            'Sohail (23 Mar 2019) -- End
                            popup_DisciplineCharge.Show()
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(0).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvResponse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvResponse.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                DirectCast(Master.FindControl("script1"), ScriptManager).RegisterPostBackControl(e.Item.Cells(2).FindControl("DownloadLink"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvResponse_ItemDataBound : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvChargeCountList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvChargeCountList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If e.Item.Cells(5).Text.Trim.Length > 0 AndAlso e.Item.Cells(5).Text <> "&nbsp;" Then
                    e.Item.Cells(5).Text = CDate(e.Item.Cells(5).Text).Date.ToShortDateString
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvChargeCountList_ItemDataBound : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbDiscipilneFiling", Me.lblDetialHeader.Text)
            Me.lblPersonalInvolved.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPersonalInvolved.ID, Me.lblPersonalInvolved.Text)
            Me.lblJobTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJobTitle.ID, Me.lblJobTitle.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblRefNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRefNo.ID, Me.lblRefNo.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.lblInterdictionDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterdictionDate.ID, Me.lblInterdictionDate.Text)
            Me.lblGeneralChargeDescr.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGeneralChargeDescr.ID, Me.lblGeneralChargeDescr.Text)
            Me.lblChargeCount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "elLine1", Me.lblChargeCount.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvChargeCountList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(0).FooterText, Me.dgvChargeCountList.Columns(0).HeaderText)
            Me.dgvChargeCountList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(1).FooterText, Me.dgvChargeCountList.Columns(1).HeaderText)
            Me.dgvChargeCountList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(2).FooterText, Me.dgvChargeCountList.Columns(2).HeaderText)
            Me.dgvChargeCountList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(3).FooterText, Me.dgvChargeCountList.Columns(3).HeaderText)
            Me.dgvChargeCountList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(4).FooterText, Me.dgvChargeCountList.Columns(4).HeaderText)
            Me.dgvChargeCountList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(5).FooterText, Me.dgvChargeCountList.Columns(5).HeaderText)
            Me.dgvChargeCountList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(6).FooterText, Me.dgvChargeCountList.Columns(6).HeaderText)
            Me.dgvChargeCountList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(7).FooterText, Me.dgvChargeCountList.Columns(7).HeaderText)
            Me.dgvChargeCountList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvChargeCountList.Columns(9).FooterText, Me.dgvChargeCountList.Columns(9).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings


End Class
