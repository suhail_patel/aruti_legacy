﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgViewEmployeeDisciplineResponse.aspx.vb" Inherits="Discipline_Discipline_Charge_wPgViewEmployeeDisciplineResponse" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <ajaxToolkit:ModalPopupExtender ID="popup_DisciplineCharge" runat="server" BackgroundCssClass="modal-backdrop"
                        CancelControlID="btnSOk" PopupControlID="pnl_ResponseCharge" TargetControlID="hdf_DiscplineCharge">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnl_ResponseCharge" runat="server" CssClass="card modal-dialog" Style="display: none;
                        z-index: 100002!important;" DefaultButton="btnSOk">
                        <div class="block-header">
                            <h2>
                                <asp:Label ID="lblDocumentHeader" runat="server" Text="View Attached Document(s)"
                                    CssClass="form-label"></asp:Label>
                            </h2>
                        </div>
                        <div class="row clearfix" style="max-height: 500px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Panel ID="pnl_dgvResponse" runat="server" ScrollBars="Auto">
                                                    <asp:DataGrid ID="dgvResponse" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                      CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                        Width="99%" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                            <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete">
                                                                        <i class="fas fa-download"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                Visible="false" />
                                                            <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:HiddenField ID="hdf_DiscplineCharge" runat="server" />
                                    <asp:Button ID="btnSOk" runat="server" Text="Ok" CssClass="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Discipline Charge Response" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Charges Details" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPersonalInvolved" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblJobTitle" runat="server" Text="Job Title" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtJobTitle" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblRefNo" runat="server"  Text="Reference No."
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboReferenceNo" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                                <asp:Label ID="lblDate" runat="server" Text="Charge Date" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtChargeDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-r-0">
                                                <asp:Label ID="lblInterdictionDate" runat="server" Text="Interdiction Date" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtInterdictionDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblGeneralChargeDescr" runat="server" Text="Charge Description" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtChargeDescription" runat="server" TextMode="MultiLine" Rows="3"
                                                        CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblChargeCount" runat="server" Text="Charge Count(s)" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 350px;">
                                                            <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server">
                                                                <asp:DataGrid ID="dgvChargeCountList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                     CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <ItemStyle CssClass="griviewitem" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="charge_count" FooterText="dgcolhCount" HeaderText="Count"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--0--%>
                                                                        <asp:BoundColumn DataField="incident_description" FooterText="dgcolhIncident" HeaderText="Incident"
                                                                            ReadOnly="true" />
                                                                        <%--1--%>
                                                                        <asp:BoundColumn DataField="charge_category" FooterText="dgcolhOffCategory" HeaderText="Offence Category"
                                                                            ReadOnly="true" />
                                                                        <%--2--%>
                                                                        <asp:BoundColumn DataField="charge_descr" FooterText="dgcolhOffence" HeaderText="Offence Discription"
                                                                            ReadOnly="true" />
                                                                        <%--3--%>
                                                                        <asp:BoundColumn DataField="charge_severity" FooterText="dgcolhSeverity" HeaderText="Severity"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--4--%>
                                                                        <asp:BoundColumn DataField="response_date" FooterText="dgcolhResponseDate" HeaderText="Response Date"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--5--%>
                                                                        <asp:BoundColumn DataField="response_type" FooterText="dgcolhResponseType" HeaderText="Response Type"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--6--%>
                                                                        <asp:BoundColumn DataField="response_remark" FooterText="dgcolhResponseText" HeaderText="Response Remark"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--7--%>
                                                                        <asp:BoundColumn DataField="disciplinefileunkid" FooterText="objcolhfileunkid" ReadOnly="true"
                                                                            Visible="false" />
                                                                        <%--8--%>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                            HeaderText="View Attachment" ItemStyle-Width="90px" FooterText="dgvalnkCol">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewattachment"
                                                                                    Font-Underline="false"><i class="fas fa-eye"></i></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--9--%>
                                                                        <asp:BoundColumn DataField="disciplinefiletranunkid" Visible="false" />
                                                                        <%--10--%>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
