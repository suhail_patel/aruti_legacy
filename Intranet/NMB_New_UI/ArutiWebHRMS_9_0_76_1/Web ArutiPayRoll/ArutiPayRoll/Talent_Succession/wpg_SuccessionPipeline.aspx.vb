﻿Imports System.Data
Imports Aruti.Data
Imports System.IO

Partial Class Talent_Succession_wpg_SuccessionPipeline
    Inherits Basepage

    '#Region " Private Variables "

    '    Private Shared Readonly mstrModuleName As String = "frmSuccessionPipeline"
    '    Private DisplayMessage As New CommonCodes
    '    Private mstrAdvanceFilter As String = ""
    '    Private mstrDeleteProcessMstIds As String = ""
    '#End Region

    '#Region " Page's Event "

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try
    '            If Not IsPostBack Then
    '                GC.Collect()
    '                FillCombo()
    '                'FillStage()
    '            Else
    '                If IsNothing(ViewState("mstrDeleteProcessMstIds")) = False Then
    '                    mstrDeleteProcessMstIds = ViewState("mstrDeleteProcessMstIds")
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '        Try
    '            Me.ViewState("mstrDeleteProcessMstIds") = mstrDeleteProcessMstIds

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    '    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '        Me.IsLoginRequired = True
    '    End Sub

    '#End Region

    '#Region "Private Method"
    '    Private Sub FillStage()
    '        Dim objStage As New clssucstages_master
    '        Dim dsList As New DataSet

    '        Try
    '            dsList = objStage.GetList("list")
    '            reptStage.DataSource = dsList.Tables("list")
    '            reptStage.DataBind()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objStage = Nothing
    '            If IsNothing(dsList) = False Then
    '                dsList.Clear()
    '                dsList = Nothing
    '            End If
    '        End Try
    '    End Sub

    '    Private Sub FillCombo()
    '        Dim objjob As New clsJobs
    '        Dim dsCombo As New DataSet
    '        Try
    '            dsCombo = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
    '            With drpJob
    '                .DataSource = dsCombo.Tables("keyJob")
    '                .DataTextField = "name"
    '                .DataValueField = "jobunkid"
    '                .DataBind()
    '                .SelectedValue = CStr(0)
    '            End With
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objjob = Nothing
    '            If IsNothing(dsCombo) = False Then
    '                dsCombo.Clear()
    '                dsCombo = Nothing
    '            End If
    '        End Try
    '    End Sub
    '#End Region

    '#Region "Button Event"
    '    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
    '        Try
    '            If drpJob.SelectedValue <= 0 Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, Job is mandatory information. Please select cycle to continue."), Me)
    '                Exit Sub
    '            End If
    '            FillStage()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    '    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub
    '#End Region

    '#Region "Repeater Event"

    '    Protected Sub StartScreening(ByVal sender As Object, ByVal e As EventArgs)
    '        Dim objScreener As New clssucscreener_master
    '        Try
    '            Dim item As RepeaterItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, RepeaterItem)
    '            Dim row As RepeaterItem = reptStage.Items(item.ItemIndex)

    '            Dim lnkStartScreening As LinkButton = TryCast(row.FindControl("lnkStartScreening"), LinkButton)
    '            Dim reptEmployeeList As Repeater = TryCast(row.FindControl("reptEmployeeList"), Repeater)
    '            Dim hfStageid As HiddenField = TryCast(row.FindControl("hfStageid"), HiddenField)

    '            Dim reptItem As IEnumerable(Of RepeaterItem) = Nothing
    '            Dim lstIDs As List(Of String) = Nothing

    '            reptItem = reptEmployeeList.Items.Cast(Of RepeaterItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
    '            If reptItem.Count <= 0 Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one employee from list to continue."), Me.Page)
    '                Exit Sub
    '            End If

    '            If objScreener.IsLoginUserIsScreener(CInt(Session("UserId"))) = False Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, You are not Screener. So You can't View Talent Profile Entries!!!"), Me)
    '                Exit Sub
    '            End If

    '            lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()

    '            Session("Suc_screening_employee_id") = String.Join(",", lstIDs.ToArray())
    '            Session("suc_screening_job_id") = CInt(drpJob.SelectedValue)
    '            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_Succession_Screening.aspx", False)

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)

    '        End Try

    '    End Sub

    '    Protected Sub DeleteScreening(ByVal sender As Object, ByVal e As EventArgs)
    '        Dim objScreener As New clssucscreener_master

    '        Try
    '            mstrDeleteProcessMstIds = ""
    '            Dim item As RepeaterItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, RepeaterItem)
    '            Dim row As RepeaterItem = reptStage.Items(item.ItemIndex)

    '            Dim reptEmployeeList As Repeater = TryCast(row.FindControl("reptEmployeeList"), Repeater)

    '            Dim reptItem As IEnumerable(Of RepeaterItem) = Nothing
    '            Dim lstIDs As List(Of String) = Nothing

    '            reptItem = reptEmployeeList.Items.Cast(Of RepeaterItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
    '            If reptItem.Count <= 0 Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please check atleast one employee from list to continue."), Me.Page)
    '                Exit Sub
    '            End If

    '            If objScreener.IsLoginUserIsScreener(CInt(Session("UserId"))) = False Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, You are not Screener. So You can't View Succession Screening!!!"), Me)
    '                Exit Sub
    '            End If


    '            lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfprocessmstunkid"), HiddenField).Value.ToString()).ToList()

    '            mstrDeleteProcessMstIds = String.Join(",", lstIDs.ToArray())
    '            popup_DeleteReason.Show()

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objScreener = Nothing
    '        End Try

    '    End Sub


    '    Public Function ImageToBase64() As String
    '        Dim base64String As String = String.Empty
    '        Dim path As String = Server.MapPath("../images/ChartUser.png")

    '        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

    '            Using m As MemoryStream = New MemoryStream()
    '                image.Save(m, image.RawFormat)
    '                Dim imageBytes As Byte() = m.ToArray()
    '                base64String = Convert.ToBase64String(imageBytes)
    '                Return base64String
    '            End Using
    '        End Using
    '    End Function

    '    Protected Sub reptStage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles reptStage.ItemDataBound
    '        Dim dsList As New DataSet
    '        Dim objSucpipeline_master As New clssucpipeline_master

    '        Try

    '            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
    '                Dim reptEmployeeList As Repeater = TryCast(e.Item.FindControl("reptEmployeeList"), Repeater)
    '                Dim hfFloworder As HiddenField = TryCast(e.Item.FindControl("hfFloworder"), HiddenField)
    '                Dim stageunkid As HiddenField = TryCast(e.Item.FindControl("hfStageid"), HiddenField)
    '                Dim lnkStartScreening As LinkButton = TryCast(e.Item.FindControl("lnkStartScreening"), LinkButton)
    '                Dim lnkRemoveScreening As LinkButton = TryCast(e.Item.FindControl("lnkRemoveScreening"), LinkButton)

    '                Dim strNoimage As String = ImageToBase64()

    '                If hfFloworder.Value = 1 Then
    '                    lnkStartScreening.Visible = True
    '                    lnkRemoveScreening.Visible = False
    '                    objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime

    '                    dsList = objSucpipeline_master.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                              CInt(Session("UserId")), _
    '                                              CInt(Session("Fin_year")), _
    '                                              CInt(Session("CompanyUnkId")), _
    '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                              CStr(Session("UserAccessModeSetting")), True, _
    '                                              CBool(Session("IsIncludeInactiveEmp")), strNoimage, CInt(drpJob.SelectedValue), "Emp", False, mstrAdvanceFilter, True)



    '                    If objSucpipeline_master._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
    '                        Exit Sub
    '                    End If

    '                    reptEmployeeList.DataSource = dsList.Tables("Emp")
    '                    reptEmployeeList.DataBind()
    '                Else
    '                    lnkStartScreening.Visible = False
    '                    lnkRemoveScreening.Visible = True

    '                    objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime


    '                    dsList = objSucpipeline_master.GetOtherStagesDetailList(CStr(Session("Database_Name")), _
    '                                              CInt(Session("UserId")), _
    '                                              CInt(Session("Fin_year")), _
    '                                              CInt(Session("CompanyUnkId")), _
    '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                              CStr(Session("UserAccessModeSetting")), True, _
    '                                              CBool(Session("IsIncludeInactiveEmp")), _
    '                                              CInt(stageunkid.Value), strNoimage, "Emp", False, mstrAdvanceFilter, True)

    '                    If objSucpipeline_master._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
    '                        Exit Sub
    '                    End If

    '                    reptEmployeeList.DataSource = dsList.Tables("Emp")
    '                    reptEmployeeList.DataBind()
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objSucpipeline_master = Nothing
    '        End Try
    '    End Sub

    '    Protected Sub reptEmployeeList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
    '        'Dim dsList As New DataSet

    '        'Try

    '        '    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
    '        '        'Dim hfempid As HiddenField = TryCast(e.Item.FindControl("hfempid"), HiddenField)
    '        '        'Dim hfimg As HiddenField = TryCast(e.Item.FindControl("hfimg"), HiddenField)



    '        '        'objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
    '        '        'objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
    '        '        'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(hfempid.Value)
    '        '        'Dim imgEmployeeProfilePic As Image = TryCast(e.Item.FindControl("imgEmployeeProfilePic"), Image)

    '        '        'Dim bytes As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(hfimg.Value)
    '        '        'Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
    '        '        'imgEmployeeProfilePic.ImageUrl = "data:image/png;base64," & base64String





    '        '        'If objEmployee._blnImgInDb Then
    '        '        '    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
    '        '        '        If objEmployee._Photo IsNot Nothing Then
    '        '        '            imgEmployeeProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
    '        '        '        Else
    '        '        '            imgEmployeeProfilePic.ImageUrl = "../images/no-image.png"
    '        '        '        End If
    '        '        '    End If
    '        '        'Else
    '        '        '    imgEmployeeProfilePic.ImageUrl = "../images/no-image.png"
    '        '        'End If


    '        '    End If
    '        'Catch ex As Exception
    '        '    DisplayMessage.DisplayError(ex, Me)
    '        'Finally
    '        'End Try
    '    End Sub

    '#End Region

    '    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
    '        Try
    '            popupAdvanceFilter.Show()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    '    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
    '        Try
    '            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
    '            popupAdvanceFilter.Hide()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    '    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
    '        Dim objSucscreening_process_master As New clssucscreening_process_master
    '        Try
    '            objSucscreening_process_master._Voidreason = popup_DeleteReason.Reason
    '            objSucscreening_process_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            objSucscreening_process_master._Voiduserunkid = CInt(Session("UserId"))
    '            objSucscreening_process_master._Isvoid = True

    '            objSucscreening_process_master._HostName = CStr(Session("HOST_NAME"))
    '            objSucscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
    '            objSucscreening_process_master._FormName = mstrModuleName
    '            objSucscreening_process_master._AuditUserId = Session("UserId")
    '            objSucscreening_process_master._FromWeb = True
    '            objSucscreening_process_master._Userunkid = CInt(Session("UserId"))

    '            objSucscreening_process_master.Delete(mstrDeleteProcessMstIds, Nothing)

    '            FillStage()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objSucscreening_process_master = Nothing
    '        End Try
    '    End Sub

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSuccessionPipeline"
    Private ReadOnly mstrModuleName1 As String = "frmPotentialSuccession"
    Private DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = ""
    Private mstrDeleteProcessMstIds As String = ""
    Private mlistProcessmstIds As String() = Nothing

    Private mintStageId As Integer = -1
    Private mblnpopupAddPotentialSuccessionEmployee As Boolean = False
    Private mintEmailTypeId As String = -1
    Private mintMinStageRowIndex As Integer = -1
    Private mintMaxToLastStageRowIndex As Integer = -1
    Private mintMaxStageRowIndex As Integer = -1


    Private mdtStageWisePageNo As DataTable

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GC.Collect()

                FillCombo()

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
            Else

                If IsNothing(ViewState("mstrDeleteProcessMstIds")) = False Then
                    mstrDeleteProcessMstIds = ViewState("mstrDeleteProcessMstIds").ToString()
                End If

                If IsNothing(ViewState("mlistProcessmstIds")) = False Then
                    mlistProcessmstIds = ViewState("mlistProcessmstIds")
                End If

                If IsNothing(ViewState("mblnpopupAddPotentialSuccessionEmployee")) = False Then
                    mblnpopupAddPotentialSuccessionEmployee = CBool(ViewState("mblnpopupAddPotentialSuccessionEmployee"))
                End If

                mintMinStageRowIndex = CInt(Me.ViewState("mintMinStageRowIndex"))
                mintMaxToLastStageRowIndex = CInt(Me.ViewState("mintMaxToLastStageRowIndex"))
                mintMaxStageRowIndex = CInt(Me.ViewState("mintMaxStageRowIndex"))
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()

                If mblnpopupAddPotentialSuccessionEmployee Then
                    popupAddPotentialSuccessionEmployee.Show()
                End If

                mintEmailTypeId = CInt(Me.ViewState("mintEmailTypeId"))


                If IsNothing(ViewState("mdtStageWisePageNo")) = False Then
                    mdtStageWisePageNo = CType(ViewState("mdtStageWisePageNo"), DataTable)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrDeleteProcessMstIds") = mstrDeleteProcessMstIds
            ViewState("mlistProcessmstIds") = mlistProcessmstIds
            ViewState("mblnpopupAddPotentialSuccessionEmployee") = mblnpopupAddPotentialSuccessionEmployee
            ViewState("mintEmailTypeId") = mintEmailTypeId
            ViewState("mintMinStageRowIndex") = mintMinStageRowIndex
            ViewState("mintMaxToLastStageRowIndex") = mintMaxToLastStageRowIndex
            ViewState("mintMaxStageRowIndex") = mintMaxStageRowIndex
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)

            ViewState("mdtStageWisePageNo") = mdtStageWisePageNo
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"
    Private Sub FillStage()
        Dim objStage As New clssucstages_master
        Dim objsetting As New clssucsettings_master
        Dim dsList As New DataSet
        Try
            CreateTable()
            If objsetting.isAllSuccessionSettingExist() Then
                dsList = objStage.GetList("list")
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intmaxStage As Integer = -1
                    Dim intminStage As Integer = -1
                    Dim intmaxToLastStage As Integer = -1
                    objStage.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)
                    Dim drRow As DataRow = dsList.Tables(0).Select("floworder = " & intminStage & "")(0)

                    mintMinStageRowIndex = dsList.Tables(0).Rows.IndexOf(drRow)


                    drRow = dsList.Tables(0).Select("floworder = " & intmaxToLastStage & "")(0)
                    mintMaxToLastStageRowIndex = dsList.Tables(0).Rows.IndexOf(drRow)
                    drRow = dsList.Tables(0).Select("floworder = " & intmaxStage & "")(0)
                    mintMaxStageRowIndex = dsList.Tables(0).Rows.IndexOf(drRow)

                    For Each drow As DataRow In dsList.Tables(0).Rows
                        mdtStageWisePageNo.Rows.Add(CInt(drow("stageunkid")), CInt(0), CInt(0), CInt(0), CInt(0))
                    Next

                End If
                reptStage.DataSource = dsList.Tables("list")
                reptStage.DataBind()
            Else
                If objsetting._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objsetting._Message, Me)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStage = Nothing
            objsetting = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objjob As New clsJobs
        Dim objEmployee As New clsEmployee_Master

        Dim dsCombo As New DataSet
        Try
            dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With



            dsCombo = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With drpJob
                .DataSource = dsCombo.Tables("keyJob")
                .DataTextField = "name"
                .DataValueField = "jobunkid"
                .DataBind()
                .SelectedValue = CStr(0)
                drpJob_SelectedIndexChanged(drpJob, New EventArgs())
            End With




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objjob = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub SendEmail()
        Dim objucpipeline_master As New clssucpipeline_master
        Try
            Select Case mintEmailTypeId
                Case clssucpipeline_master.enEmailType.INPROCESS_NOTIFICATIONAPPROVER, clssucpipeline_master.enEmailType.PROCESSDONE_NOTIFICATIONAPPROVER, _
                clssucpipeline_master.enEmailType.NOTIFICATIONSCREENER
                    objucpipeline_master.SendEmails(mintEmailTypeId, CInt(Session("CompanyUnkId")), _
                                                     CInt(Session("UserId")), "", _
                                                     mstrModuleName, _
                                                    enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), -1, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("Fin_year")))
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillPopupCombo()
        Dim objjob As New clsJobs
        Dim dsCombo As New DataSet
        Try
            dsCombo = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With drpAddPotentialJob
                .DataSource = dsCombo.Tables("keyJob")
                .DataTextField = "name"
                .DataValueField = "jobunkid"
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objjob = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub FillEmployeeStageWise(ByVal dsEmpList As DataList, ByVal listItem As DataListItem, ByVal intStageId As Integer, ByVal intFloworder As Integer, ByVal objSucpipeline_master As clssucpipeline_master, ByRef isPrevious As Boolean, ByRef iCount As Integer)
        Dim objSucstages_master As New clssucstages_master
        Dim objsucsettings_master As New clssucsettings_master
        Dim dsList As New DataSet

        Try
            Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL
            If rdbAutoFilter.Checked Then
                FilterType = enScreeningFilterType.ONLYQUALIFIED
            End If

            If rdbManual.Checked Then
                FilterType = enScreeningFilterType.ONLYMANUALLYADDED
            End If

            Dim intmaxStage As Integer = -1
            Dim intminStage As Integer = -1
            Dim intmaxToLastStage As Integer = -1
            objSucstages_master.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)

            mintStageId = intFloworder

            Dim strNoimage As String = ImageToBase64()

            Dim mdicSetting As Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String) = objsucsettings_master.GetSettingFromPeriod()

            Dim intmaxDataDisplay As Integer = 50
            If IsNothing(mdicSetting) = False AndAlso mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_DATA_DISPLAY) Then
                intmaxDataDisplay = CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_DATA_DISPLAY))
            End If

            Dim lnkGetNextChunkOfData As LinkButton = TryCast(listItem.FindControl("lnkGetNextChunkOfData"), LinkButton)
            Dim lnkGetPreviousChunkOfData As LinkButton = TryCast(listItem.FindControl("lnkGetPreviousChunkOfData"), LinkButton)
            Dim lblChunkPageNo As Label = TryCast(listItem.FindControl("lblChunkPageNo"), Label)

            If intFloworder = intminStage Then

                dsList = objSucpipeline_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               CStr(Session("UserAccessModeSetting")), True, _
                                               CBool(Session("IsIncludeInactiveEmp")), CInt(drpJob.SelectedValue), strNoimage, CInt(cboEmployee.SelectedValue), FilterType, "Emp", False, mstrAdvanceFilter, True)

                If IsNothing(dsList) AndAlso objSucpipeline_master._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
                    Exit Sub
                End If



            ElseIf intFloworder = intmaxToLastStage Then
                dsList = objSucpipeline_master.GetOtherStagesDetailList(CStr(Session("Database_Name")), _
                          CInt(Session("UserId")), _
                          CInt(Session("Fin_year")), _
                          CInt(Session("CompanyUnkId")), _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          CStr(Session("UserAccessModeSetting")), True, _
                          CBool(Session("IsIncludeInactiveEmp")), CInt(drpJob.SelectedValue), _
                          intStageId, strNoimage, intFloworder, CInt(cboEmployee.SelectedValue), FilterType, "Emp", False, mstrAdvanceFilter, True)

                If IsNothing(dsList) AndAlso objSucpipeline_master._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
                    Exit Sub
                End If


            ElseIf intFloworder = intmaxStage Then
                dsList = objSucpipeline_master.GetOtherStagesDetailList(CStr(Session("Database_Name")), _
                          CInt(Session("UserId")), _
                          CInt(Session("Fin_year")), _
                          CInt(Session("CompanyUnkId")), _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          CStr(Session("UserAccessModeSetting")), True, _
                          CBool(Session("IsIncludeInactiveEmp")), CInt(drpJob.SelectedValue), _
                          intStageId, strNoimage, intFloworder, CInt(cboEmployee.SelectedValue), FilterType, "Emp", False, mstrAdvanceFilter, True)

                If IsNothing(dsList) AndAlso objSucpipeline_master._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
                    Exit Sub
                End If

            Else
                dsList = objSucpipeline_master.GetOtherStagesDetailList(CStr(Session("Database_Name")), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                             CStr(Session("UserAccessModeSetting")), True, _
                                             CBool(Session("IsIncludeInactiveEmp")), CInt(drpJob.SelectedValue), _
                                             intStageId, strNoimage, intFloworder, CInt(cboEmployee.SelectedValue), FilterType, "Emp", False, mstrAdvanceFilter, True)

                If IsNothing(dsList) AndAlso objSucpipeline_master._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
                    Exit Sub
                End If




            End If


            If IsNothing(dsList) = False Then
                iCount = dsList.Tables("Emp").Rows.Count
                If iCount > 0 Then

                    Dim drRow As DataRow = mdtStageWisePageNo.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of String)("stageid") = intStageId.ToString()).FirstOrDefault()
                    If IsNothing(drRow) = False AndAlso dsList.Tables("Emp").Rows.Count > 0 Then
                        lblChunkPageNo.Visible = True
                        lnkGetPreviousChunkOfData.Visible = True
                        lnkGetNextChunkOfData.Visible = True

                        If CInt(drRow("currentpage")) = 0 Then
                            drRow("totalrecords") = dsList.Tables("Emp").Rows.Count
                            drRow("totalpages") = CInt(Math.Ceiling(CDbl(drRow("totalrecords") / intmaxDataDisplay)))
                            drRow("currentcount") = intmaxDataDisplay
                            drRow("currentpage") = 1
                            dsEmpList.DataSource = dsList.Tables("Emp").AsEnumerable().Skip(drRow("currentpage") - 1).Take(intmaxDataDisplay).CopyToDataTable()
                            lnkGetPreviousChunkOfData.Visible = False

                            If CInt(drRow("totalrecords")) <= intmaxDataDisplay Then
                                lnkGetPreviousChunkOfData.Visible = False
                                lnkGetNextChunkOfData.Visible = False
                                lblChunkPageNo.Visible = False
                            End If

                        Else

                            If isPrevious Then
                                If (CInt(drRow("currentpage")) - 1) = 1 Then
                                    lnkGetPreviousChunkOfData.Visible = False
                                    lnkGetNextChunkOfData.Visible = True
                                    drRow("currentpage") = 0
                                Else
                                    drRow("currentpage") = (CInt(drRow("currentpage") - 1)).ToString()
                                End If
                            Else
                                lnkGetPreviousChunkOfData.Visible = True
                                If CInt(drRow("currentpage")) + 1 = CInt(drRow("totalpages")) Then
                                    lnkGetNextChunkOfData.Visible = False
                                Else
                                    lnkGetNextChunkOfData.Visible = True
                                End If

                            End If


                            drRow("currentcount") = CInt((CInt(drRow("currentpage")) + 1) * intmaxDataDisplay)

                            If isPrevious = False Then
                                drRow("currentpage") = (CInt(drRow("currentpage") + 1)).ToString()
                            ElseIf isPrevious AndAlso CInt(drRow("currentpage")) = 0 Then
                                drRow("currentpage") = 1
                            End If

                            dsEmpList.DataSource = dsList.Tables("Emp").AsEnumerable().Skip(CInt(drRow("currentpage") - 1) * intmaxDataDisplay).Take(intmaxDataDisplay).CopyToDataTable()
                        End If
                        lblChunkPageNo.Text = drRow("currentpage").ToString()
                        dsEmpList.DataBind()
                    End If
                    iCount = dsList.Tables("Emp").Rows.Count
                End If

            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSucstages_master = Nothing
            objsucsettings_master = Nothing
            If IsNothing(dsList) = False Then
                dsList.Tables.Clear()
            End If
        End Try
    End Sub

    Private Sub CreateTable()
        Try
            mdtStageWisePageNo = New DataTable
            mdtStageWisePageNo.Columns.Add("stageid")
            mdtStageWisePageNo.Columns.Add("totalrecords")
            mdtStageWisePageNo.Columns.Add("totalpages")
            mdtStageWisePageNo.Columns.Add("currentcount")
            mdtStageWisePageNo.Columns.Add("currentpage")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button Event"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveAddPotentialTelant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddPotentialSuccession.Click
        Dim strMessage As String = ""
        Dim objPotentialSuccessionTran As New clsPotentialSuccession_tran
        Dim objScreeningProcesmaster As New clssucscreening_process_master
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvPSEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objPotentialSuccessionTran._Jobunkid = CInt(drpAddPotentialJob.SelectedValue)

                objPotentialSuccessionTran._Isvoid = False
                objPotentialSuccessionTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objPotentialSuccessionTran._Voiduserunkid = -1
                objPotentialSuccessionTran._Voidreason = ""
                objPotentialSuccessionTran._FromWeb = True
                objPotentialSuccessionTran._FormName = mstrModuleName1

                For Each dgRow As GridViewRow In gRow
                    objPotentialSuccessionTran._Employeeunkid = CInt(dgvPSEmployee.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())

                    If objPotentialSuccessionTran.Insert() = False Then
                        dgRow.Style.Add("background", "red")
                        strMessage &= " * " & objPotentialSuccessionTran._Message & "<br />"
                    End If

                Next
            End If
            If strMessage.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(strMessage, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Potential Succession saved successfully."), Me)
            End If
            dgvPSEmployee.DataSource = Nothing
            mblnpopupAddPotentialSuccessionEmployee = False
            popupAddPotentialSuccessionEmployee.Hide()

            FillStage()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPotentialSuccessionTran = Nothing
        End Try

    End Sub

    Protected Sub btnCloseAddPotentialSuccession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseAddPotentialSuccession.Click
        Try
            dgvPSEmployee.DataSource = Nothing
            mblnpopupAddPotentialSuccessionEmployee = False
            popupAddPotentialSuccessionEmployee.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupconfirmSendEmailInProgress_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmSendEmailInProgress.buttonYes_Click
        Try
            mintEmailTypeId = clssucpipeline_master.enEmailType.NOTIFICATIONSCREENER
            SendEmail()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmSendEmailProcessDone_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmSendEmailProcessDone.buttonYes_Click
        Try
            mintEmailTypeId = clssucpipeline_master.enEmailType.PROCESSDONE_NOTIFICATIONAPPROVER
            SendEmail()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmSendEmailApprovedEmployee_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmSendEmailApprovedEmployee.buttonYes_Click
        Dim objsucpipeline_master As New clssucpipeline_master
        Dim objjob As New clsJobs
        Try
            mintEmailTypeId = clssucpipeline_master.enEmailType.APPROVED_EMPLOYEE
            Dim row As DataListItem = reptStage.Items(mintMaxStageRowIndex)
            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)
            Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing

            If reptEmployeeList.Items.Count > 0 Then
                reptItem = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
                'lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()
                lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString() + "|" + CType(x.FindControl("hfNominatedJobunkid"), HiddenField).Value.ToString()).ToList()
            End If

            For Each lstID As String In lstIDs
                Dim tempList() As String = Nothing
                Dim intEmpId As Integer = -1
                Dim intNominateJobId As Integer = -1
                tempList = lstID.Split("|")
                intEmpId = tempList(0)
                intNominateJobId = tempList(1)
                objjob._Jobunkid = intNominateJobId
                objsucpipeline_master.SendEmails(mintEmailTypeId, CInt(Session("CompanyUnkId")), _
                                                                     CInt(Session("UserId")), objjob._Job_Name.ToString(), _
                                                                     mstrModuleName, _
                                                                    enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), intEmpId, _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("Fin_year")))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucpipeline_master = Nothing
            objjob = Nothing
        End Try
    End Sub

    Protected Sub popupconfirmSendEmailApprover_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmSendEmailApprover.buttonYes_Click
        Dim objStagesTran As New clssucscreening_stages_tran
        Dim objStage As New clssucstages_master
        Try
            mintEmailTypeId = clssucpipeline_master.enEmailType.INPROCESS_NOTIFICATIONAPPROVER
            Dim row As DataListItem = reptStage.Items(mintMaxToLastStageRowIndex)

            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)


            Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing

            If reptEmployeeList.Items.Count > 0 Then
                reptItem = reptEmployeeList.Items.Cast(Of DataListItem)()
                lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()
            End If

            If lstIDs IsNot Nothing AndAlso lstIDs.Count > 0 Then
                Dim intStageId As Integer = -1
                Dim dsList As DataSet = objStage.GetList("list")
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intmaxStage As Integer = -1
                    Dim intminStage As Integer = -1
                    Dim intmaxToLastStage As Integer = -1
                    objStage.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)
                    If intmaxToLastStage > 0 Then
                        Dim drRow As DataRow = dsList.Tables(0).Select("floworder = " & intmaxToLastStage & "")(0)
                        intStageId = drRow.Item("Stageunkid")
                    End If
                End If

                Dim dsMaxCountProcess As DataSet = objStagesTran.GetProcessListfromMaxScreening("List", intStageId)
                If dsMaxCountProcess.Tables(0).Rows.Count > 0 Then
                    Call SendEmail()
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, You can't Send Email to Approver(s). Because No Employee is get defined Numbers of Screening."), Me)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStagesTran = Nothing
        End Try
    End Sub

    'Hemant (28 Jan 2021) -- Start
    'Protected Sub cnfConfirmDeleteReasonPT_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirmDeleteReasonPT.buttonYes_Click
    '    Dim objPotentialSuccessionTran As New clsPotentialSuccession_tran
    '    Dim objScreeningProcesmaster As New clssucscreening_process_master
    '    Try

    '        Dim row As DataListItem = reptStage.Items(mintMinStageRowIndex)

    '        Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)

    '        Dim rowEmployeeList As DataListItem = reptEmployeeList.Items(mintItemIndex)
    '        Dim hfempid As HiddenField = TryCast(rowEmployeeList.FindControl("hfempid"), HiddenField)
    '        Dim hfNominatedJobunkid As HiddenField = TryCast(rowEmployeeList.FindControl("hfNominatedJobunkid"), HiddenField)


    '        If CInt(hfempid.Value) > 0 AndAlso objScreeningProcesmaster.IsSuccessionStartedForEmployees(CInt(hfNominatedJobunkid.Value), CInt(hfempid.Value)) Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, Succession Process is started for Selected Some Employees. So You can't Remove them from List"), Me)
    '            Exit Sub
    '        End If

    '        Dim dtPT As DataTable = objPotentialSuccessionTran.GetList("List", CInt(drpJob.SelectedValue), , CInt(hfempid.Value)).Tables(0)
    '        If dtPT.Rows.Count > 0 Then
    '            objPotentialSuccessionTran._Isvoid = True
    '            objPotentialSuccessionTran._Voidreason = ""
    '            objPotentialSuccessionTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            objPotentialSuccessionTran._Voiduserunkid = CInt(Session("UserId"))
    '            objPotentialSuccessionTran._UserId = CInt(Session("UserId"))
    '            objPotentialSuccessionTran._WebFormName = mstrModuleName1
    '            objPotentialSuccessionTran._FromWeb = True
    '            If objPotentialSuccessionTran.Delete(CInt(dtPT.Rows(0).Item("potentialSuccessiontranunkid"))) = True Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Potential Succession Deleted successfully."), Me)
    '            Else
    '                DisplayMessage.DisplayMessage(objPotentialSuccessionTran._Message, Me)
    '                Exit Sub
    '            End If
    '            FillStage()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objPotentialSuccessionTran = Nothing
    '    End Try
    'End Sub
    'Hemant (28 Jan 2021) -- End

    Protected Sub lnkLoadChunkOfData(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master
        Dim objSucpipeline_master As New clssucpipeline_master


        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = reptStage.Items(item.ItemIndex)

            objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime



            Dim lnkStartScreening As LinkButton = TryCast(row.FindControl("lnkStartScreening"), LinkButton)
            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)
            Dim hfStageid As HiddenField = TryCast(row.FindControl("hfStageid"), HiddenField)
            Dim hfFloworder As HiddenField = TryCast(row.FindControl("hfFloworder"), HiddenField)

            Dim lnkGetPreviousChunkOfData As LinkButton = TryCast(row.FindControl("lnkGetPreviousChunkOfData"), LinkButton)
            Dim iCount As Integer = 0


            If CType(sender, LinkButton).ID = lnkGetPreviousChunkOfData.ID Then
                FillEmployeeStageWise(reptEmployeeList, row, CInt(hfStageid.Value), CInt(hfFloworder.Value), objSucpipeline_master, True, iCount)
            Else
                FillEmployeeStageWise(reptEmployeeList, row, CInt(hfStageid.Value), CInt(hfFloworder.Value), objSucpipeline_master, False, iCount)
            End If



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try

    End Sub

    Protected Sub btnEmployeeScreeningDetailListClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmployeeScreeningDetailListClose.Click
        Try
            gvEmployeeScreeningDetailList.DataSource = Nothing
            popupEmployeeScreeningDetailList.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataList Event"

    Protected Sub StartScreening(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master
        Dim objclsuser As New clsUserAddEdit
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = reptStage.Items(item.ItemIndex)

            Dim lnkStartScreening As LinkButton = TryCast(row.FindControl("lnkStartScreening"), LinkButton)
            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)
            Dim hfStageid As HiddenField = TryCast(row.FindControl("hfStageid"), HiddenField)

            Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing

            reptItem = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            If reptItem.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please check atleast one employee from list to continue."), Me.Page)
                Exit Sub
            End If

            If objScreener.IsLoginUserIsScreener(CInt(Session("UserId"))) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, You can't do screening process, Reason: You are not screener."), Me)
                Exit Sub
            End If

            objclsuser._Userunkid = CInt(Session("UserId"))
            lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()
            If objclsuser._EmployeeUnkid > 0 Then
                If lstIDs.Where(Function(x) x = objclsuser._EmployeeUnkid).Count > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, You can't do screening process, Reason: You can't do your own screenering."), Me)
                    Exit Sub
                End If
            End If

            lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString() + "|" + CType(x.FindControl("hfNominatedJobunkid"), HiddenField).Value.ToString()).ToList()
            Session("suc_screening_employee_id") = String.Join(",", lstIDs.ToArray())

            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_Succession_Screening.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
            objclsuser = Nothing
        End Try

    End Sub

    Protected Sub DeleteScreening(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master
        Try
            mstrDeleteProcessMstIds = ""
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = reptStage.Items(item.ItemIndex)

            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)

            Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing
            Dim lstEmployeeIDs As List(Of String) = Nothing

            reptItem = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            If reptItem.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please check atleast one employee from list to continue."), Me.Page)
                Exit Sub
            End If

            lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfprocessmstunkid"), HiddenField).Value.ToString()).ToList()
            lstEmployeeIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString() + "|" + CType(x.FindControl("hfNominatedJobunkid"), HiddenField).Value.ToString()).ToList()


            Session("suc_approverejectfromqualified_process_id") = String.Join(",", lstIDs.ToArray())
            Session("suc_approverejectfromqualified_employee_id") = String.Join(",", lstEmployeeIDs.ToArray())
            Session("suc_approverejecttype") = clssucpipeline_master.enSuApproveRejectFormType.DELETESUCCESSIONPROCESS

            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_SuccessionApprovedDisapprove.aspx", False)

            'mstrDeleteProcessMstIds = String.Join(",", lstIDs.ToArray())
            'popup_DeleteReason.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try

    End Sub

    Protected Sub AddPotentialSuccession(ByVal sender As Object, ByVal e As EventArgs)
        Try
            mblnpopupAddPotentialSuccessionEmployee = True
            popupAddPotentialSuccessionEmployee.Show()
            FillPopupCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub ViewEmployeeScreeningDetail(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master
        Dim objsucpipeline_master As New clssucpipeline_master
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim hfempid As HiddenField = TryCast(item.FindControl("hfempid"), HiddenField)
            Dim hfNominatedJobunkid As HiddenField = TryCast(item.FindControl("hfNominatedJobunkid"), HiddenField)
            Dim lblNominatedJob As Label = TryCast(item.FindControl("lblNominatedJob"), Label)
            Dim lblEmployee As Label = TryCast(item.FindControl("lblEmployee"), Label)
            Dim dsScreeningDetailList As DataSet = objsucpipeline_master.GetSuccessionScreenerWiseScreeningDetail("ScreeningDetailList", CInt(Session("CompanyUnkId")), _
                                                                                            CInt(Session("Fin_year")), _
                                                                                           Session("EmployeeAsOnDate").ToString(), _
                                                                                            CStr(Session("UserAccessModeSetting")), _
                                                                                            CInt(hfempid.Value), _
                                                                                            CInt(hfNominatedJobunkid.Value))

            If IsNothing(dsScreeningDetailList) = False Then
                gvEmployeeScreeningDetailList.DataSource = dsScreeningDetailList.Tables("ScreeningDetailList")
                gvEmployeeScreeningDetailList.DataBind()
                popupEmployeeScreeningDetailList.Show()
                txtNominatedEmployeeName.Text = lblEmployee.Text
                txtNominatedJob.Text = lblNominatedJob.Text
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try

    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Protected Sub reptStage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles reptStage.ItemDataBound
        Dim dsList As New DataSet
        Dim objSucpipeline_master As New clssucpipeline_master
        Dim objStages_master As New clssucstages_master

        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim reptEmployeeList As DataList = TryCast(e.Item.FindControl("reptEmployeeList"), DataList)
                Dim hfFloworder As HiddenField = TryCast(e.Item.FindControl("hfFloworder"), HiddenField)
                Dim hfstageunkid As HiddenField = TryCast(e.Item.FindControl("hfStageid"), HiddenField)
                Dim lnkStartScreening As LinkButton = TryCast(e.Item.FindControl("lnkStartScreening"), LinkButton)
                Dim lnkRemoveScreening As LinkButton = TryCast(e.Item.FindControl("lnkRemoveScreening"), LinkButton)
                Dim lnkSetToApproveDisapprove As LinkButton = TryCast(e.Item.FindControl("lnkSetToApproveDisapprove"), LinkButton)
                Dim lnkSetToQulified As LinkButton = TryCast(e.Item.FindControl("lnkSetToQulified"), LinkButton)
                Dim lnkAddPotentialSuccession As LinkButton = TryCast(e.Item.FindControl("lnkAddPotentialSuccession"), LinkButton)
                Dim lnkSendEmail As LinkButton = TryCast(e.Item.FindControl("lnkSendEmail"), LinkButton)
                Dim lnkSendEmailToEmployee As LinkButton = TryCast(e.Item.FindControl("lnkSendEmailToEmployee"), LinkButton)
                Dim lnkSendEmailToApprover As LinkButton = TryCast(e.Item.FindControl("lnkSendEmailToApprover"), LinkButton)

                Dim lblCount As Label = TryCast(e.Item.FindControl("lblCount"), Label)

                Dim strNoimage As String = ImageToBase64()

                Dim intmaxStage As Integer = -1
                Dim intminStage As Integer = -1
                Dim intmaxToLastStage As Integer = -1
                objStages_master.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)

                mintStageId = CInt(hfFloworder.Value)

                If CInt(hfFloworder.Value) = intminStage Then
                    lnkStartScreening.Visible = CBool(Session("AllowForSuccessionScreeningProcess"))
                    lnkRemoveScreening.Visible = False
                    lnkSetToQulified.Visible = False
                    lnkSetToApproveDisapprove.Visible = False
                    lnkAddPotentialSuccession.Visible = CBool(Session("AllowToAddPotentialSuccessionEmployee"))

                    lnkSendEmail.Visible = CBool(Session("AllowToSendNotificationToSuccessionScreener"))
                    lnkSendEmailToEmployee.Visible = False
                    lnkSendEmailToApprover.Visible = False

                    objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime


                    Dim iRowCount As Integer = 0

                    FillEmployeeStageWise(reptEmployeeList, e.Item, CInt(hfstageunkid.Value), CInt(hfFloworder.Value), objSucpipeline_master, False, iRowCount)
                    lblCount.Text = iRowCount


                ElseIf CInt(hfFloworder.Value) = intmaxToLastStage Then
                    lnkStartScreening.Visible = False
                    lnkRemoveScreening.Visible = CBool(Session("AllowToRemoveSuccessionProcess"))
                    lnkSetToQulified.Visible = False
                    lnkSetToApproveDisapprove.Visible = CBool(Session("AllowToApproveRejectSuccessionPipelineEmployee"))
                    lnkAddPotentialSuccession.Visible = False

                    lnkSendEmail.Visible = False
                    lnkSendEmailToEmployee.Visible = False
                    lnkSendEmailToApprover.Visible = CBool(Session("AllowToSendNotificationToSuccessionApprover"))

                    objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime


                    Dim iRowCount As Integer = 0
                    FillEmployeeStageWise(reptEmployeeList, e.Item, CInt(hfstageunkid.Value), CInt(hfFloworder.Value), objSucpipeline_master, False, iRowCount)
                    lblCount.Text = iRowCount



                ElseIf CInt(hfFloworder.Value) = intmaxStage Then
                    lnkStartScreening.Visible = False
                    lnkRemoveScreening.Visible = False
                    lnkSetToQulified.Visible = CBool(Session("AllowToMoveApprovedEmployeeToQulifiedSuccession"))
                    lnkSetToApproveDisapprove.Visible = False
                    lnkAddPotentialSuccession.Visible = False

                    lnkSendEmail.Visible = False
                    lnkSendEmailToEmployee.Visible = CBool(Session("AllowToSendNotificationToSuccessionEmployee"))
                    lnkSendEmailToApprover.Visible = False
                    objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime

                    Dim iRowCount As Integer = 0
                    FillEmployeeStageWise(reptEmployeeList, e.Item, CInt(hfstageunkid.Value), CInt(hfFloworder.Value), objSucpipeline_master, False, iRowCount)
                    lblCount.Text = iRowCount

                Else
                    lnkStartScreening.Visible = False
                    lnkRemoveScreening.Visible = CBool(Session("AllowToRemoveSuccessionProcess"))
                    lnkSetToQulified.Visible = False
                    lnkSetToApproveDisapprove.Visible = False
                    lnkAddPotentialSuccession.Visible = False
                    lnkSendEmail.Visible = False
                    lnkSendEmailToEmployee.Visible = False
                    lnkSendEmailToApprover.Visible = False
                    objSucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime

                    Dim iRowCount As Integer = 0
                    FillEmployeeStageWise(reptEmployeeList, e.Item, CInt(hfstageunkid.Value), CInt(hfFloworder.Value), objSucpipeline_master, False, iRowCount)
                    lblCount.Text = iRowCount

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSucpipeline_master = Nothing
            objStages_master = Nothing
        End Try
    End Sub

    Protected Sub reptEmployeeList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        'Dim dsList As New DataSet

        'Try

        '    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
        '        'Dim hfempid As HiddenField = TryCast(e.Item.FindControl("hfempid"), HiddenField)
        '        'Dim hfimg As HiddenField = TryCast(e.Item.FindControl("hfimg"), HiddenField)



        '        'objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
        '        'objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
        '        'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(hfempid.Value)
        '        'Dim imgEmployeeProfilePic As Image = TryCast(e.Item.FindControl("imgEmployeeProfilePic"), Image)

        '        'Dim bytes As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(hfimg.Value)
        '        'Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
        '        'imgEmployeeProfilePic.ImageUrl = "data:image/png;base64," & base64String





        '        'If objEmployee._blnImgInDb Then
        '        '    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
        '        '        If objEmployee._Photo IsNot Nothing Then
        '        '            imgEmployeeProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
        '        '        Else
        '        '            imgEmployeeProfilePic.ImageUrl = "../images/no-image.png"
        '        '        End If
        '        '    End If
        '        'Else
        '        '    imgEmployeeProfilePic.ImageUrl = "../images/no-image.png"
        '        'End If


        '    End If
        'Catch ex As Exception
        '    DisplayMessage.DisplayError(ex, Me)
        'Finally
        'End Try

        Dim objStages_master As New clssucstages_master
        Try


            Dim lnkViewScreeningInfo As LinkButton = TryCast(e.Item.FindControl("lnkViewScreeningInfo"), LinkButton)
            lnkViewScreeningInfo.Visible = CBool(Session("AllowToViewSuccessionScreeningDetail"))

            If mintStageId > 0 Then
                Dim intmaxStage As Integer = -1
                Dim intminStage As Integer = -1
                Dim intmaxToLastStage As Integer = -1
                objStages_master.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)

                If mintStageId = intminStage Then
                    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                        Dim hfIsmanual As HiddenField = TryCast(e.Item.FindControl("hfIsmanual"), HiddenField)
                        Dim hfisDone As HiddenField = TryCast(e.Item.FindControl("hfisDone"), HiddenField)
                        Dim lnkDeletePTEmployee As LinkButton = TryCast(e.Item.FindControl("lnkDeletePTEmployee"), LinkButton)
                        Dim lnkisDone As Label = TryCast(e.Item.FindControl("lnkisDone"), Label)


                        If CInt(hfisDone.Value) > 0 Then
                            lnkisDone.Visible = True
                        End If

                        If CBool(hfIsmanual.Value) Then
                            lnkDeletePTEmployee.Visible = CBool(Session("AllowToAddPotentialSuccessionEmployee"))
                        Else
                            lnkDeletePTEmployee.Visible = False
                        End If
                    End If

                ElseIf mintStageId = intmaxToLastStage Then

                    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                        Dim hfisdisapproved As HiddenField = TryCast(e.Item.FindControl("hfisdisapproved"), HiddenField)
                        Dim hfcolor As HiddenField = TryCast(e.Item.FindControl("hfcolor"), HiddenField)
                        Dim lnkisdisapproved As Label = TryCast(e.Item.FindControl("lnkisdisapproved"), Label)
                        Dim hfIsmanual As HiddenField = TryCast(e.Item.FindControl("hfIsmanual"), HiddenField)
                        Dim lnkDeletePTEmployee As LinkButton = TryCast(e.Item.FindControl("lnkDeletePTEmployee"), LinkButton)
                        'Hemant (17 Jan 2022) -- Start
                        'ENHANCEMENT : #OLD-518 - Display rating description on mouse hover on Talent Pipeline screen and Succession Pipeline screen.
                        Dim hfratingdesc As HiddenField = TryCast(e.Item.FindControl("hfratingdesc"), HiddenField)
                        'Hemant (17 Jan 2022) -- End

                        If CBool(hfIsmanual.Value) Then
                            lnkDeletePTEmployee.Visible = CBool(Session("AllowToAddPotentialSuccessionEmployee"))
                        Else
                            lnkDeletePTEmployee.Visible = False
                        End If

                        If CBool(hfisdisapproved.Value) Then
                            lnkisdisapproved.Visible = True
                        End If

                        Dim lblColor As Label = TryCast(e.Item.FindControl("lblColor"), Label)
                        lblColor.BackColor = Drawing.ColorTranslator.FromHtml(hfcolor.Value)
                        lblColor.Visible = True
                        'Hemant (17 Jan 2022) -- Start
                        'ENHANCEMENT : #OLD-518 - Display rating description on mouse hover on Talent Pipeline screen and Succession Pipeline screen.
                        lblColor.ToolTip = hfratingdesc.Value
                        'Hemant (17 Jan 2022) -- End
                    End If

                ElseIf mintStageId = intmaxStage Then
                    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                        Dim pnlEmployeeBox As Panel = TryCast(e.Item.FindControl("pnlEmployeeBox"), Panel)
                        Dim hfcolor As HiddenField = TryCast(e.Item.FindControl("hfcolor"), HiddenField)
                        Dim lblColor As Label = TryCast(e.Item.FindControl("lblColor"), Label)
                        Dim hfIsmanual As HiddenField = TryCast(e.Item.FindControl("hfIsmanual"), HiddenField)
                        Dim lnkDeletePTEmployee As LinkButton = TryCast(e.Item.FindControl("lnkDeletePTEmployee"), LinkButton)
                        'Hemant (17 Jan 2022) -- Start
                        'ENHANCEMENT : #OLD-518 - Display rating description on mouse hover on Talent Pipeline screen and Succession Pipeline screen.
                        Dim hfratingdesc As HiddenField = TryCast(e.Item.FindControl("hfratingdesc"), HiddenField)
                        'Hemant (17 Jan 2022) -- End

                        If CBool(hfIsmanual.Value) Then
                            lnkDeletePTEmployee.Visible = CBool(Session("AllowToAddPotentialSuccessionEmployee"))
                        Else
                            lnkDeletePTEmployee.Visible = False
                        End If

                        lblColor.BackColor = Drawing.ColorTranslator.FromHtml(hfcolor.Value)
                        lblColor.Visible = True
                        'Hemant (17 Jan 2022) -- Start
                        'ENHANCEMENT : #OLD-518 - Display rating description on mouse hover on Talent Pipeline screen and Succession Pipeline screen.
                        lblColor.ToolTip = hfratingdesc.Value
                        'Hemant (17 Jan 2022) -- End
                    End If
                Else
                    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                        Dim pnlEmployeeBox As Panel = TryCast(e.Item.FindControl("pnlEmployeeBox"), Panel)
                        Dim hfcolor As HiddenField = TryCast(e.Item.FindControl("hfcolor"), HiddenField)
                        Dim lblColor As Label = TryCast(e.Item.FindControl("lblColor"), Label)
                        Dim hfIsmanual As HiddenField = TryCast(e.Item.FindControl("hfIsmanual"), HiddenField)
                        Dim lnkDeletePTEmployee As LinkButton = TryCast(e.Item.FindControl("lnkDeletePTEmployee"), LinkButton)
                        'Hemant (17 Jan 2022) -- Start
                        'ENHANCEMENT : #OLD-518 - Display rating description on mouse hover on Talent Pipeline screen and Succession Pipeline screen.
                        Dim hfratingdesc As HiddenField = TryCast(e.Item.FindControl("hfratingdesc"), HiddenField)
                        'Hemant (17 Jan 2022) -- End

                        If CBool(hfIsmanual.Value) Then
                            lnkDeletePTEmployee.Visible = CBool(Session("AllowToAddPotentialSuccessionEmployee"))
                        Else
                            lnkDeletePTEmployee.Visible = False
                        End If
                        lblColor.BackColor = Drawing.ColorTranslator.FromHtml(hfcolor.Value)
                        lblColor.Visible = True
                        'Hemant (17 Jan 2022) -- Start
                        'ENHANCEMENT : #OLD-518 - Display rating description on mouse hover on Talent Pipeline screen and Succession Pipeline screen.
                        lblColor.ToolTip = hfratingdesc.Value
                        'Hemant (17 Jan 2022) -- End
                    End If
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStages_master = Nothing
        End Try
    End Sub

    Protected Sub ApproveDisapprove(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master

        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = reptStage.Items(item.ItemIndex)

            Dim lnkSetToApproveDisapprove As LinkButton = TryCast(row.FindControl("lnkSetToApproveDisapprove"), LinkButton)
            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)
            Dim hfStageid As HiddenField = TryCast(row.FindControl("hfStageid"), HiddenField)

            Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing
            Dim lstEmployeeIDs As List(Of String) = Nothing

            reptItem = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            If reptItem.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please check atleast one employee from list to continue."), Me.Page)
                Exit Sub
            End If


            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            'reptItem = reptEmployeeList.Items.Cast(Of RepeaterItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CType(x.FindControl("hfisdisapproved"), HiddenField).Value = True)
            'If reptItem.Count > 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "You can't process rejected employee."), Me.Page)
            '    Exit Sub
            'End If
            'Pinkal (12-Dec-2020) -- End

            lstIDs = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) CType(x.FindControl("hfprocessmstunkid"), HiddenField).Value.ToString()).ToList()
            'lstEmployeeIDs = reptEmployeeList.Items.Cast(Of RepeaterItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()
            lstEmployeeIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString() + "|" + CType(x.FindControl("hfNominatedJobunkid"), HiddenField).Value.ToString()).ToList()
            'mlistProcessmstIds = String.Join(",", lstIDs.ToArray()).Split(CChar(",")).ToArray()
            'mintProcessId = 0

            Session("suc_approverejectfromqualified_process_id") = String.Join(",", lstIDs.ToArray())
            Session("suc_approverejectfromqualified_employee_id") = String.Join(",", lstEmployeeIDs.ToArray())
            Session("suc_approverejecttype") = clssucpipeline_master.enSuApproveRejectFormType.APPROVE_REJECT

            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_SuccessionApprovedDisapprove.aspx", False)

            'Hemant (14 Dec 2020) -- Start
            'mintProcessId = 0
            'Hemant (14 Dec 2020) -- End            


            'SetNextEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try

    End Sub

    Protected Sub SetQulified(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master
        Dim objsucscreening_stages_tran As New clssucscreening_stages_tran
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Dim objsucscreener_master As New clssucscreener_master
        objsucscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))
        Dim dsScreenerList As New DataSet
        Dim dsList As New DataSet
        Dim objsucpipeline_master As New clssucpipeline_master

        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = reptStage.Items(item.ItemIndex)

            Dim lnkSetToApproveDisapprove As LinkButton = TryCast(row.FindControl("lnkSetToApproveDisapprove"), LinkButton)
            Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)
            Dim hfStageid As HiddenField = TryCast(row.FindControl("hfStageid"), HiddenField)

            Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing

            reptItem = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            If reptItem.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please check atleast one employee from list to continue."), Me.Page)
                Exit Sub
            End If



            lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfprocessmstunkid"), HiddenField).Value.ToString()).ToList()
            Dim lstEmployeeIDs As List(Of String) = Nothing
            lstEmployeeIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()

            Dim mlistProcessmstIds As String = String.Join(",", lstIDs.ToArray())


            lstEmployeeIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString() + "|" + CType(x.FindControl("hfNominatedJobunkid"), HiddenField).Value.ToString()).ToList()

            Session("suc_approverejectfromqualified_process_id") = String.Join(",", lstIDs.ToArray())
            Session("suc_approverejectfromqualified_employee_id") = String.Join(",", lstEmployeeIDs.ToArray())
            Session("suc_approverejecttype") = clssucpipeline_master.enSuApproveRejectFormType.SET_BACK_TO_QULIFY





            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_SuccessionApprovedDisapprove.aspx", False)

            'objsucscreening_process_master.SetBackToQulify(CInt(drpJob.SelectedValue), mlistProcessmstIds)
            'FillStage()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
            objsucscreening_stages_tran = Nothing
            objsucscreening_process_master = Nothing
            objsucscreener_master = Nothing
        End Try

    End Sub

    Protected Sub RemoveManuallyAddedEmployee(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hfpotentialsuccessiontranunkid As HiddenField = TryCast(item.FindControl("hfpotentialsuccessiontranunkid"), HiddenField)
            Dim hfprocessmstunkid As HiddenField = TryCast(item.FindControl("hfprocessmstunkid"), HiddenField)
            Dim hfNominatedJobunkid As HiddenField = TryCast(item.FindControl("hfNominatedJobunkid"), HiddenField)
            Dim hfempid As HiddenField = TryCast(item.FindControl("hfempid"), HiddenField)

            Session("suc_approverejectfromqualified_process_id") = hfprocessmstunkid.Value
            Session("suc_approverejectfromqualified_employee_id") = hfempid.Value + "|" + hfNominatedJobunkid.Value
            Session("suc_approverejecttype") = clssucpipeline_master.enSuApproveRejectFormType.DELETEMANUALLYADDEDEMPLOYEE
            Session("suc_PotentialSuccessionTran_id") = hfpotentialsuccessiontranunkid.Value

            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_SuccessionApprovedDisapprove.aspx", False)
            Exit Sub
            cnfConfirmDeleteReasonPT.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Are you Sure You Want To delete Manually Added Employee? If You delete it, Your whole succession transection will also delete.")
            cnfConfirmDeleteReasonPT.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Protected Sub lnkSendEmail_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim reptItemPT As IEnumerable(Of DataListItem) = Nothing
            Dim lstPTIDs As List(Of String) = Nothing
            Dim reptItemQualify As IEnumerable(Of DataListItem) = Nothing
            Dim lstQualifyIDs As List(Of String) = Nothing

            Dim rowPT As DataListItem = reptStage.Items(mintMinStageRowIndex)
            Dim rowQualify As DataListItem = reptStage.Items(mintMaxToLastStageRowIndex)

            Dim reptPTEmployeeList As DataList = TryCast(rowPT.FindControl("reptEmployeeList"), DataList)
            Dim reptQualifyEmployeeList As DataList = TryCast(rowQualify.FindControl("reptEmployeeList"), DataList)

            reptItemPT = reptPTEmployeeList.Items.Cast(Of DataListItem)()

            If reptItemPT.Count > 0 Then
                popupconfirmSendEmailInProgress.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You are trying to send an email notification for screening employee(s) who falls under Succession Potential stage. we recommend you to send notification once all employee(s) are in Succession Potential stage. Do you still want to send notification?")
                popupconfirmSendEmailInProgress.Show()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, You can't Send Email to Screeners. Because No Employee is available for Screening them."), Me)
            End If
            'Else
            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, You can't Send Email to Screeners. Because No Employee is available for Approve/Disapprove them."), Me)
            'Exit Sub
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub lnkSendEmailToEmployee_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim reptItemApprove As IEnumerable(Of DataListItem) = Nothing
            Dim lstApproveIDs As List(Of String) = Nothing

            Dim rowApprove As DataListItem = reptStage.Items(mintMaxStageRowIndex)

            Dim reptApproveEmployeeList As DataList = TryCast(rowApprove.FindControl("reptEmployeeList"), DataList)

            reptItemApprove = reptApproveEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
            lstApproveIDs = reptItemApprove.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()

            If lstApproveIDs.Count > 0 Then
                popupconfirmSendEmailApprovedEmployee.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "You are about to send an email notification to the employee(s) approved to be in the Successions Pool but some are still on the Succession Screening or Approval Stage. We recommend you send this notification once everyone is processed. Do you still want to send notification?")
                popupconfirmSendEmailApprovedEmployee.Show()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please check atleast one employee from Approved list to continue."), Me)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub lnkSendEmailToApprover_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim reptItemApprove As IEnumerable(Of DataListItem) = Nothing
            Dim lstApproveIDs As List(Of String) = Nothing

            Dim rowApprove As DataListItem = reptStage.Items(mintMaxToLastStageRowIndex)

            Dim reptApproveEmployeeList As DataList = TryCast(rowApprove.FindControl("reptEmployeeList"), DataList)

            reptItemApprove = reptApproveEmployeeList.Items.Cast(Of DataListItem)()
            lstApproveIDs = reptItemApprove.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()

            If lstApproveIDs.Count > 0 Then
                popupconfirmSendEmailApprover.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "You are trying to send an email notification for approving/disapproving employee(s) who falls under qualified stage. As there some emplyoee are still under process and some are under potential stage, we recommend you to send notification once all employee(s) process is done. Do you still want to send notification?")
                popupconfirmSendEmailApprover.Show()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, You can't Send Email to Approvers. Because No Employee is available for Approve/Disapprove them."), Me)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    'Protected Sub rptOtherScreeners_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptOtherScreeners.ItemDataBound
    '    Dim dsList As New DataSet
    '    Dim objtlscreening_process_tran As New clstlscreening_process_tran

    '    Try

    '        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

    '            Dim hfprocessmstunkid As HiddenField = CType(e.Item.FindControl("hfprocessmstunkid"), HiddenField)
    '            Dim hfscreenermstunkid As HiddenField = CType(e.Item.FindControl("hfscreenermstunkid"), HiddenField)

    '            Dim rptOtherScreenerResponse As Repeater = TryCast(e.Item.FindControl("rptOtherScreenerResponse"), Repeater)

    '            dsList = objtlscreening_process_tran.GetOtherScreenerData("ResponseDetail", CInt(drpCycle.SelectedValue), CInt(hfprocessmstunkid.Value), CInt(hfscreenermstunkid.Value), -1)

    '            rptOtherScreenerResponse.DataSource = dsList.Tables("ResponseDetail")
    '            rptOtherScreenerResponse.DataBind()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objtlscreening_process_tran = Nothing
    '    End Try
    'End Sub

    'Protected Sub rptOtherScreenerResponse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
    '    Try

    '        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

    '            Dim hfOtherScreenerResponse As HiddenField = CType(e.Item.FindControl("hfOtherScreenerResponse"), HiddenField)
    '            Dim rdbYes As RadioButton = CType(e.Item.FindControl("rdbYes"), RadioButton)
    '            Dim rdbNo As RadioButton = CType(e.Item.FindControl("rdbNo"), RadioButton)
    '            Dim txtRemark As TextBox = CType(e.Item.FindControl("txtRemark"), TextBox)


    '            If CInt(hfOtherScreenerResponse.Value) > 0 Then
    '                rdbYes.Checked = True
    '            Else
    '                rdbNo.Checked = True
    '            End If

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region "Gridview Event"
    Protected Sub gvEmployeeScreeningDetailList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmployeeScreeningDetailList.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim hfIsScreeningDone As HiddenField = CType(e.Row.FindControl("hfIsScreeningDone"), HiddenField)
                Dim hfIsScreeningPending As HiddenField = CType(e.Row.FindControl("hfIsScreeningPending"), HiddenField)
                Dim lblCheckScreeningDoneTrue As Label = CType(e.Row.FindControl("lblCheckScreeningDoneTrue"), Label)
                Dim lblCheckScreeningDoneFalse As Label = CType(e.Row.FindControl("lblCheckScreeningDoneFalse"), Label)

                If CBool(hfIsScreeningDone.Value) AndAlso CBool(hfIsScreeningPending.Value) = False Then
                    lblCheckScreeningDoneTrue.Visible = True
                    lblCheckScreeningDoneFalse.Visible = False
                Else
                    lblCheckScreeningDoneTrue.Visible = False
                    lblCheckScreeningDoneFalse.Visible = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Combo Box Event"
    Protected Sub drpAddPotentialJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpAddPotentialJob.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim dsEmployee As New DataSet
        Dim objPotentialSuccessionTran As New clsPotentialSuccession_tran
        Dim dsPTEmployee As New DataSet
        Dim dtEmployee As DataTable = Nothing
        Dim dsList As DataSet = Nothing

        Dim objSucpipeline_master As New clssucpipeline_master

        Try
            Dim row As DataListItem = reptStage.Items(mintMinStageRowIndex)
            'Dim reptEmployeeList As DataList = TryCast(row.FindControl("reptEmployeeList"), DataList)
            'Dim reptItem As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing

            'If reptEmployeeList.Items.Count > 0 Then
            '    reptItem = reptEmployeeList.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("hfNominatedJobunkid"), HiddenField).Value = drpAddPotentialJob.SelectedValue)
            '    lstIDs = reptItem.Select(Function(x) CType(x.FindControl("hfempid"), HiddenField).Value.ToString()).ToList()
            'End If

            Dim strNoimage As String = ImageToBase64()
            dsList = objSucpipeline_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               CStr(Session("UserAccessModeSetting")), True, _
                                               CBool(Session("IsIncludeInactiveEmp")), CInt(drpAddPotentialJob.SelectedValue), strNoimage, -1, enScreeningFilterType.ALL, "Emp", False, mstrAdvanceFilter, True)

            If IsNothing(dsList) AndAlso objSucpipeline_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objSucpipeline_master._Message, Me)
                Exit Sub
            Else
                lstIDs = dsList.Tables(0).AsEnumerable.Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()
            End If

            dsEmployee = objPotentialSuccessionTran.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), CInt(drpAddPotentialJob.SelectedValue), lstIDs, "Employee", False, _
                                            , , , , , , , , , , , , , , mstrAdvanceFilter)
            dtEmployee = dsEmployee.Tables(0)

            dgvPSEmployee.DataSource = dtEmployee
            dgvPSEmployee.DataBind()
            If dtEmployee IsNot Nothing Then dtEmployee.Clear()
            dtEmployee = Nothing
            If dsEmployee IsNot Nothing Then dsEmployee.Clear()
            dsEmployee = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPotentialSuccessionTran = Nothing
            objEmp = Nothing
            objSucpipeline_master = Nothing
        End Try
    End Sub

    Protected Sub drpJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpJob.SelectedIndexChanged, _
                                                                                                           cboEmployee.SelectedIndexChanged, _
                                                                                                           rdbAll.CheckedChanged, _
                                                                                                           rdbAutoFilter.CheckedChanged, _
                                                                                                           rdbManual.CheckedChanged
        Try
            FillStage()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            popupAdvanceFilter.Hide()
            popupAdvanceFilter.Dispose()
            'Hemant (14 Dec 2020) -- Start
            FillStage()
            'Hemant (14 Dec 2020) -- End           
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
    '    Dim objSucscreening_process_master As New clssucscreening_process_master
    '    Try
    '        objSucscreening_process_master._Voidreason = popup_DeleteReason.Reason
    '        objSucscreening_process_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        objSucscreening_process_master._Voiduserunkid = CInt(Session("UserId"))
    '        objSucscreening_process_master._Isvoid = True
    '        objSucscreening_process_master._HostName = CStr(Session("HOST_NAME"))
    '        objSucscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
    '        objSucscreening_process_master._FormName = mstrModuleName
    '        objSucscreening_process_master._AuditUserId = CInt(Session("UserId"))
    '        objSucscreening_process_master._FromWeb = True
    '        objSucscreening_process_master._Userunkid = Session("UserId").ToString()

    '        objSucscreening_process_master.Delete(mstrDeleteProcessMstIds, Nothing)

    '        FillStage()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objSucscreening_process_master = Nothing
    '    End Try
    'End Sub

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblJob.ID, Me.lblJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblHeader.ID, Me.lblHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)

            'Potential Succession Popup'
            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblPopupAddPotentialSuccessionEmployeeheader.ID, Me.lblPopupAddPotentialSuccessionEmployeeheader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblAddPotentialJob.ID, Me.lblAddPotentialJob.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveAddPotentialSuccession.ID, Me.btnSaveAddPotentialSuccession.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCloseAddPotentialSuccession.ID, Me.btnCloseAddPotentialSuccession.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, dgvPSEmployee.Columns(1).FooterText, dgvPSEmployee.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, dgvPSEmployee.Columns(2).FooterText, dgvPSEmployee.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, dgvPSEmployee.Columns(3).FooterText, dgvPSEmployee.Columns(3).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPageHeader1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblHeader.ID, Me.lblHeader.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lnkAdvanceFilter.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)

            'Potential Succession Popup'
            'Language.setLanguage(mstrModuleName1)

            Me.lblAddPotentialJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblAddPotentialJob.ID, Me.lblAddPotentialJob.Text)
            Me.lblPopupAddPotentialSuccessionEmployeeheader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblPopupAddPotentialSuccessionEmployeeheader.ID, Me.lblPopupAddPotentialSuccessionEmployeeheader.Text)
            Me.btnSaveAddPotentialSuccession.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveAddPotentialSuccession.ID, Me.btnSaveAddPotentialSuccession.Text).Replace("&", "")
            Me.btnCloseAddPotentialSuccession.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseAddPotentialSuccession.ID, Me.btnCloseAddPotentialSuccession.Text).Replace("&", "")

            dgvPSEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), dgvPSEmployee.Columns(1).FooterText, dgvPSEmployee.Columns(1).HeaderText)
            dgvPSEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), dgvPSEmployee.Columns(2).FooterText, dgvPSEmployee.Columns(2).HeaderText)
            dgvPSEmployee.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), dgvPSEmployee.Columns(3).FooterText, dgvPSEmployee.Columns(3).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please check atleast one employee from list to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, You are not Screener. So You can't View Succession Screening!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, You can't Send Email to Screeners. Because No Employee is available for Screening them.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "You are trying to send an email notification for screening employee(s) who falls under Succession Potential stage. we recommend you to send notification once all employee(s) are in Succession Potential stage. Do you still want to send notification?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Please check atleast one employee from Approved list to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, Succession Process is started for Selected Some Employees. So You can't Remove them from List")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, You are not Screener. So You can't View Succession Profile Entries!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Potential Succession Deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "You are about to send an email notification to the employee(s) approved to be in the Successions Pool but some are still on the Succession Screening or Approval Stage. We recommend you send this notification once everyone is processed. Do you still want to send notification?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, You can't Send Email to Approver(s). Because No Employee is get defined Numbers of Screening.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "You are trying to send an email notification for approving/disapproving employee(s) who falls under qualified stage. As there some emplyoee are still under process and some are under potential stage, we recommend you to send notification once all employee(s) process is done. Do you still want to send notification?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, You can't Send Email to Approvers. Because No Employee is available for Approve/Disapprove them.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Are you Sure You Want To delete Manually Added Employee?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Sorry, You can't do screening process, Reason: You are not screener.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Sorry, You can't do screening process, Reason: You can't do your own screenering.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Potential Succession saved successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>



End Class
