﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class wPg_TalentSettings
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTalentSettingsAddEdit"
    Private ReadOnly mstrModuleName1 As String = "frmTalentCycleAddEdit"
    Private ReadOnly mstrModuleName2 As String = "frmTalentStagingAddEdit"
    Private ReadOnly mstrModuleName3 As String = "frmTalentQualifyingCriteriaSetup"
    Private ReadOnly mstrModuleName4 As String = "frmTalentScreenersSetup"
    Private ReadOnly mstrModuleName5 As String = "frmTalentQuestionnaireAddEdit"
    Private ReadOnly mstrModuleName6 As String = "frmTalentRatingsAddEdit"
    Private DisplayMessage As New CommonCodes
    Private mintCycleUnkid As Integer = 0
    Private mintStagingUnkid As Integer = 0
    Private mintQuestionnaireUnkid As Integer = 0
    Private mintRatingsUnkid As Integer = 0
    Private mintScreenerUnkid As Integer = 0
    Private mstrDeleteAction As String = ""
    Private mStagingActionMode As enAction
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                FillCycleCombo()
                FillStageCombo()
                FillCycle()
                FillStaging()
                FillComboForQuestionnaire()
                FillQuestionnaire()
                FillComboForRatings()
                FillRatings()
                FillScreenersCombo()
                FillScreener()
                FillQualifyingCombo()


                btnCycleAdd.Visible = CBool(Session("AllowToModifyTalentSetting"))
                btnSAdd.Visible = CBool(Session("AllowToModifyTalentSetting"))
                pnlQualifyFooter.Visible = CBool(Session("AllowToModifyTalentSetting"))
                btnAddScreener.Visible = CBool(Session("AllowToModifyTalentSetting"))
                btnQuestionAdd.Visible = CBool(Session("AllowToModifyTalentSetting"))
                btnRAdd.Visible = CBool(Session("AllowToModifyTalentSetting"))

            Else
                mintCycleUnkid = CInt(Me.ViewState("mintCycleUnkid"))
                mintScreenerUnkid = CInt(Me.ViewState("mintScreenerUnkid"))
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintStagingUnkid = CInt(Me.ViewState("mintStagingUnkid"))
                mintQuestionnaireUnkid = CInt(Me.ViewState("mintQuestionnaireUnkid"))
                mintRatingsUnkid = CInt(Me.ViewState("mintRatingsUnkid"))
                mStagingActionMode = CType(Me.ViewState("StagingAction"), enAction)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintCycleUnkid") = mintCycleUnkid
            Me.ViewState("mintScreenerUnkid") = mintScreenerUnkid
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintStagingUnkid") = mintStagingUnkid
            Me.ViewState("mintQuestionnaireUnkid") = mintQuestionnaireUnkid
            Me.ViewState("mintRatingsUnkid") = mintRatingsUnkid
            Me.ViewState("StagingAction") = mStagingActionMode
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Cycle Setup "

#Region " Button's Events "

    Protected Sub btnCycleAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCycleAdd.Click
        Dim objCycle As New clstlcycle_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidCycle() = False Then
                Exit Sub
            End If
            SetCycleValue(objCycle)
            If mintCycleUnkid > 0 Then
                blnFlag = objCycle.Update()
            Else
                blnFlag = objCycle.Insert()
            End If

            If blnFlag = False AndAlso objCycle._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objCycle._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 9, "Talent cycle defined successfully."), Me)
                FillCycle()
                'Pinkal (12-Dec-2020) -- Start
                'Enhancement  -  Working on Talent Issue which is given by Andrew.
                FillStageCombo()
                FillStaging()
                FillScreenersCombo()
                FillScreener()
                FillQualifyingCombo()
                FillComboForQuestionnaire()
                FillQuestionnaire()
                FillComboForRatings()
                FillRatings()
                'Pinkal (12-Dec-2020) -- End
                ClearCycleCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCycle = Nothing
        End Try
    End Sub

    Protected Sub btnCycleReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCycleReset.Click
        Try
            Call ClearCycleCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub ClearCycleCtrls()
        Try
            mintCycleUnkid = 0
            drpFinYear.SelectedValue = CStr(0)
            txtCode.Text = ""
            txtName.Text = ""
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCycle()
        Dim objCycle As New clstlcycle_master
        Dim dsList As New DataSet
        Try

            dsList = objCycle.GetList("List")
            gvCycle.DataSource = dsList.Tables(0)
            gvCycle.DataBind()

            gvCycle.Columns(0).Visible = CBool(Session("AllowToModifyTalentSetting"))
            gvCycle.Columns(1).Visible = CBool(Session("AllowToModifyTalentSetting"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCycle = Nothing
        End Try
    End Sub

    Private Sub SetCycleValue(ByRef objCycle As clstlcycle_master)
        Try
            objCycle._Cycleunkid = mintCycleUnkid
            objCycle._AuditUserId = CInt(Session("UserId"))
            objCycle._ClientIP = CStr(Session("IP_ADD"))
            objCycle._Closeuserunkid = 0
            objCycle._Code = txtCode.Text
            objCycle._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objCycle._DatabaseName = CStr(Session("Database_Name"))
            objCycle._End_Date = dtpEndDate.GetDate
            objCycle._FormName = mstrModuleName1
            objCycle._FromWeb = True
            objCycle._HostName = CStr(Session("HOST_NAME"))
            objCycle._Inactiveuserunkid = 0
            objCycle._Isactive = True
            objCycle._Name = txtName.Text
            objCycle._Start_Date = dtpStartDate.GetDate
            objCycle._Statusid = enStatusType.OPEN
            objCycle._Totaldays = CInt(DateDiff(DateInterval.Day, dtpStartDate.GetDate(), dtpEndDate.GetDate) + 1)
            objCycle._Userunkid = CInt(Session("UserId"))
            objCycle._Yearunkid = CInt(drpFinYear.SelectedValue)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetCycleValue(ByVal intCycleId As Integer)
        Dim objCycle As New clstlcycle_master
        Try
            objCycle._Cycleunkid = mintCycleUnkid
            txtCode.Text = objCycle._Code
            dtpEndDate.SetDate = objCycle._End_Date
            txtName.Text = objCycle._Name
            dtpStartDate.SetDate = objCycle._Start_Date
            drpFinYear.SelectedValue = CStr(objCycle._Yearunkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCycle = Nothing
        End Try
    End Sub

    Private Sub FillCycleCombo()
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objMst.getComboListPAYYEAR(CInt(Session("Fin_year")), _
                                                 CStr(Session("FinancialYear_Name")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 "Year", True)
            With drpFinYear
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Year")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidCycle() As Boolean
        Dim objFinYear As New clsCompany_Master
        Try
            If CInt(drpFinYear.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Sorry, Fincancial year is mandatory information. Please select year to continue."), Me)
                Return False
            End If

            If txtCode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Sorry, Code is mandatory information. Please enter code to continue."), Me)
                Return False
            End If

            If txtName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Sorry, Name is mandatory information. Please enter name to continue."), Me)
                Return False
            End If

            If dtpStartDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Sorry, Start date is mandatory information. Please select start date to continue."), Me)
                Return False
            End If

            If dtpEndDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Sorry, End date is mandatory information. Please select end date to continue."), Me)
                Return False
            End If

            If dtpEndDate.GetDate < dtpStartDate.GetDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Sorry, End date cannot be less than the start date of talent cycle. Please select proper end date to continue."), Me)
                Return False
            End If

            objFinYear._YearUnkid = CInt(drpFinYear.SelectedValue)
            If dtpStartDate.GetDate < objFinYear._Database_Start_Date Or dtpStartDate.GetDate > objFinYear._Database_End_Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry, Start date should be between selected financial year dates. Please select proper end date to continue."), Me)
                Return False
            End If

            If dtpEndDate.GetDate < objFinYear._Database_Start_Date Or dtpEndDate.GetDate > objFinYear._Database_End_Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 8, "Sorry, End date should be between selected financial year dates. Please select proper end date to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
        End Try
    End Function

#End Region

#Region " Link Event(s) "

    Protected Sub lnkCEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvCycle.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 12, "Sorry you can't delete this cycle,Reason: Talent Process is already started for this cycle."), Me)
                mintCycleUnkid = -1
                Exit Sub
            End If

            GetCycleValue(mintCycleUnkid)


            FillStageCombo()
            FillStaging()
            FillScreenersCombo()
            FillScreener()
            FillQualifyingCombo()
            FillComboForQuestionnaire()
            FillQuestionnaire()
            FillComboForRatings()
            FillRatings()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkCDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvCycle.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 12, "Sorry you can't delete this cycle,Reason: Talent Process is already started for this cycle."), Me)
                mintCycleUnkid = -1
                Exit Sub
            End If

            mstrDeleteAction = "delcy"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 10, "You are about to delete this cycle. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region



#End Region

#Region " Staging Setup "

#Region " Private Method(s)"

    Private Sub FillStageCombo()
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Dim objStg As New clstlstages_master
        Dim obCyl As New clstlcycle_master
        Try
            dsCombo = obCyl.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)
            With drpSPeriod
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objStg.getComboList("List", True)
            With drpPStage
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objStg.FlowOrder("List", True)
            With drpFlow
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            objStg = Nothing
            obCyl = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub ClearStagingCtrls()
        Try
            mintStagingUnkid = 0
            drpSPeriod.SelectedValue = CStr(0)
            txtStageName.Text = ""
            drpFlow.SelectedValue = CStr(0)
            drpPStage.SelectedValue = CStr(0)
            mStagingActionMode = enAction.ADD_ONE
            drpSPeriod.Enabled = True
            drpFlow.Enabled = True
            drpPStage.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Private Sub FillStaging()
    Private Sub FillStaging(Optional ByVal xCycleId As Integer = 0)
        'Pinkal (12-Dec-2020) -- End

        Dim objStaging As New clstlstages_master
        Dim dsList As New DataSet
        Try
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            'dsList = objStaging.GetList("List")
            dsList = objStaging.GetList("List", xCycleId)
            'Pinkal (12-Dec-2020) -- End
            gvobjStaging.DataSource = dsList.Tables(0)
            gvobjStaging.DataBind()

            gvobjStaging.Columns(0).Visible = CBool(Session("AllowToModifyTalentSetting"))
            gvobjStaging.Columns(1).Visible = CBool(Session("AllowToModifyTalentSetting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaging = Nothing
        End Try
    End Sub

    Private Function IsValidStaging() As Boolean

        Dim objstage As New clstlstages_master
        Dim intMinFlowOrder As Integer
        Dim intMaxFlowOrder As Integer
        Dim intMaxToLastFlowOrder As Integer
        Dim objtlscreening_process_master As New clstlscreening_process_master

        Try

            If CInt(drpSPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 1, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
                Return False
            End If

            If objtlscreening_process_master.IsTalentStartedForCycle(CInt(drpSPeriod.SelectedValue)) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 12, "Sorry you can't Add this Stage,Reason: Talent Process is already started for this cycle."), Me)
                Exit Function
            End If


            If txtStageName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 2, "Sorry, Stage Name is mandatory information. Please enter Stage Name to continue."), Me)
                Return False
            End If


            If mStagingActionMode <> enAction.EDIT_ONE AndAlso CInt(drpPStage.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 3, "Sorry, Stage is mandatory information. Please select Stage to continue."), Me)
                Return False
            End If



            objstage._Stageunkid = CInt(drpPStage.SelectedValue)

            objstage.Get_Min_Max_FlowOrder(intMinFlowOrder, intMaxFlowOrder, intMaxToLastFlowOrder, CInt(drpSPeriod.SelectedValue))

            If (intMinFlowOrder = objstage._Floworder AndAlso CInt(drpFlow.SelectedValue) = 1) OrElse _
            (intMaxFlowOrder = objstage._Floworder AndAlso CInt(drpFlow.SelectedValue) = 2) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 4, "Sorry, You can't Add ") & drpFlow.SelectedItem.Text & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 5, " of ") & drpPStage.SelectedItem.Text & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 6, " Stage "), Me)
                Return False
            End If

            If (intMinFlowOrder = objstage._Floworder AndAlso CInt(drpFlow.SelectedValue) = 0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 7, "Sorry,You can't Add Stage on ") & drpPStage.SelectedItem.Text & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 6, " Stage "), Me)
                Return False
            End If



            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objstage = Nothing
            objtlscreening_process_master = Nothing
        End Try
    End Function

    Private Sub SetStagingValue(ByRef objStaging As clstlstages_master)
        Try
            objStaging._Stageunkid = CInt(drpPStage.SelectedValue)
            objStaging._ApplyOnFloworder = objStaging._Floworder
            objStaging._ApplyOnStageunkid = CInt(drpPStage.SelectedValue)
            objStaging._Stageunkid = mintStagingUnkid
            objStaging._Cycleunkid = CInt(drpSPeriod.SelectedValue)
            objStaging._Stage_Name = txtStageName.Text
            If mStagingActionMode = enAction.EDIT_ONE Then
                'objStaging._Floworder = CInt(drpFlow.SelectedValue)
                'objStaging._Isdefault = False
            Else
                objStaging._Floworder = CInt(drpFlow.SelectedValue)
                objStaging._Isdefault = False
            End If

            objStaging._Isactive = True
            objStaging._AuditUserId = CInt(Session("UserId"))
            objStaging._ClientIP = CStr(Session("IP_ADD"))
            objStaging._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objStaging._DatabaseName = CStr(Session("Database_Name"))
            objStaging._FormName = mstrModuleName2
            objStaging._FromWeb = True
            objStaging._HostName = CStr(Session("HOST_NAME"))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetStagingValue(ByVal intStagingId As Integer)
        Dim objStaging As New clstlstages_master
        Try
            objStaging._Stageunkid = mintStagingUnkid
            drpSPeriod.SelectedValue = CStr(objStaging._Cycleunkid)
            txtStageName.Text = objStaging._Stage_Name


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaging = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Events "

    Protected Sub btnSAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSAdd.Click
        Dim objStaging As New clstlstages_master
        Dim blnFlag As Boolean = False
        Dim dsGetList As DataSet = Nothing
        Try
            If IsValidStaging() = False Then
                Exit Sub
            End If
            SetStagingValue(objStaging)

            dsGetList = objStaging.GetList("List", CInt(drpSPeriod.SelectedValue))

            If mintStagingUnkid > 0 Then
                blnFlag = objStaging.Update()
            Else
                blnFlag = objStaging.Insert(, dsGetList.Tables(0))
            End If

            If blnFlag = False AndAlso objStaging._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objStaging._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 8, "Talent Staging defined successfully."), Me)
                FillStaging()
                ClearStagingCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaging = Nothing
            dsGetList = Nothing
        End Try
    End Sub

    Protected Sub btnSReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSReset.Click
        Try
            Call ClearStagingCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkSEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvobjStaging.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 13, "Sorry you can't edit this cycle,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintStagingUnkid = CInt(gvobjStaging.DataKeys(row.RowIndex)("stageunkid"))
            GetStagingValue(mintStagingUnkid)
            mStagingActionMode = enAction.EDIT_ONE
            drpSPeriod.Enabled = False
            drpFlow.Enabled = False
            drpPStage.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkSDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvobjStaging.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 14, "Sorry you can't delete this cycle,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintStagingUnkid = CInt(gvobjStaging.DataKeys(row.RowIndex)("stageunkid"))
            mstrDeleteAction = "delsta"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 9, "You are about to delete this Stage. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "
    Protected Sub drpSPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpSPeriod.SelectedIndexChanged
        Dim objstages As New clstlstages_master
        Dim dsCombo As DataSet = Nothing
        Try
            If CInt(drpSPeriod.SelectedValue) > 0 Then
                dsCombo = objstages.getComboList("List", True, CInt(drpSPeriod.SelectedValue))

                With drpPStage
                    .DataValueField = "Id"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables("List")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            FillStaging(CInt(drpSPeriod.SelectedValue))
            'Pinkal (12-Dec-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region



#End Region

#Region " Qualifying Criteria Setup "

#Region " Private Methods "
    Private Sub FillQualifyingCombo()
        Dim objMst As New clsMasterData
        Dim obCyl As New clstlcycle_master
        Dim dsCombo As DataSet = Nothing
        Try

            dsCombo = obCyl.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)
            With drpQMPeriod
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objMst.GetEAllocation_Notification("Allocation")
            Dim dr As DataRow = dsCombo.Tables("Allocation").NewRow
            dr.Item("Id") = 99999
            dr.Item("NAME") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 7, "Select")
            dsCombo.Tables("Allocation").Rows.InsertAt(dr, 0)

            With drpQAllocBy
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Allocation")
                .DataBind()
                .SelectedValue = "99999"
            End With




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            obCyl = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearQualifyingCtrls()
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidQualifying() As Boolean
        Dim objFinYear As New clsCompany_Master
        Try

            If CInt(drpQMPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 1, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                Return False
            End If



            'Sohail (03 Nov 2020) -- Start
            'NMB Enhancement : # : Add insruction textbox in qualifying setting it should be mandatory information.
            If txtInstruction.Text.Trim = "" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 15, "Please enter Instruction."), Me)
                txtInstruction.Focus()
                Return False
            End If
            'Sohail (03 Nov 2020) -- End

            If chkPerformanceScore.Checked Then
                If txtMinPSValue.Enabled AndAlso CInt(txtMinPSValue.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 2, "Sorry, Minumum Score value should be greater than 0."), Me)
                    Return False
                End If

                If rdballPeriod.Checked = False AndAlso rdbanyPeriod.Checked = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 3, "Sorry, Please select one option in apply minimum score to continue."), Me)
                    Return False
                End If
            End If

            If chkQMaxAge.Checked AndAlso CInt(txtQMaxAgeNo.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 4, "Sorry, Maximum age value should be greater than 0."), Me)
                Return False
            End If

            If chkOrgYearNo.Checked AndAlso CInt(txtQOrgYrNo.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 5, "Sorry, Number of year with organization should be greater than 0."), Me)
                Return False
            End If

            If chkQMaxScreeners.Checked AndAlso CInt(txtMaxQScreener.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 6, "Sorry, Maximum number of screener should be greater than 0."), Me)
                Return False
            End If

            If chkQMinScrReq.Checked AndAlso CInt(txtQMinScrReq.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 13, "Sorry, Minimum number of screening required should be greater than 0."), Me)
                Return False
            End If

            If chkQTotalQueWeight.Checked AndAlso CInt(txtQTotalQueWeight.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 8, "Sorry, Total question weight should be greater than 0."), Me)
                Return False
            End If


            If chkAllocationBy.Checked AndAlso CInt(drpQAllocBy.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 9, "Sorry, Please select at lease one allocation to continue."), Me)
                Return False
            End If


            If chkAllocationBy.Checked AndAlso CInt(drpQAllocBy.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 10, "Sorry, Please select at lease one allocation to continue."), Me)
                Return False
            End If

            If chkAllocationBy.Checked Then
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                Dim lstIDs As List(Of String) = Nothing
                Dim allocation_Id As String = ""

                gRow = gvQAllocBy.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
                If gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 12, "Please check atleast one allocation from list to continue."), Me.Page)
                    Exit Function
                End If
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
        End Try
    End Function

    Private Sub SetQualifyValue(ByRef objsetting As clstlsettings_master)
        Try
            objsetting._AuditUserId = CInt(Session("UserId"))
            objsetting._ClientIP = CStr(Session("IP_ADD"))
            objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            objsetting._FormName = mstrModuleName3
            objsetting._FromWeb = True
            objsetting._HostName = CStr(Session("HOST_NAME"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Dropdown Event "
    Protected Sub drpQMPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpQMPeriod.SelectedIndexChanged
        Dim objtlsetting As New clstlsettings_master
        Dim blnFlag As Boolean = False
        Try

            If CInt(drpQMPeriod.SelectedValue) > 0 Then

                'Sohail (03 Nov 2020) -- Start
                'NMB Enhancement : # : add insruction textbox in qualifying setting it should be mandatory information.
                txtInstruction.Text = ""
                'Sohail (03 Nov 2020) -- End

                Dim TlSetting As Dictionary(Of clstlsettings_master.enTalentConfiguration, String) = objtlsetting.GetSettingFromPeriod(CInt(drpQMPeriod.SelectedValue))

                If IsNothing(TlSetting) = False Then

                    For Each kvp As KeyValuePair(Of clstlsettings_master.enTalentConfiguration, String) In TlSetting
                        Select Case kvp.Key
                            Case clstlsettings_master.enTalentConfiguration.ALL_PERIOD
                                chkPerformanceScore.Checked = CBool(kvp.Value)
                                chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                rdballPeriod.Checked = CBool(kvp.Value)

                            Case clstlsettings_master.enTalentConfiguration.ANY_PERIOD

                                chkPerformanceScore.Checked = CBool(kvp.Value)
                                chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                rdbanyPeriod.Checked = CBool(kvp.Value)


                            Case clstlsettings_master.enTalentConfiguration.ALLOC_TYPE
                                Dim allocation As String = kvp.Value.ToString()
                                Dim allocationType As String() = allocation.Split(CChar("|"))


                                If allocationType(0).Length > 0 Then
                                    chkAllocationBy.Checked = True
                                    chkAllocationBy_CheckedChanged(Nothing, Nothing)


                                    drpQAllocBy.SelectedValue = allocationType(0)
                                    drpQAllocBy_SelectedIndexChanged(Nothing, Nothing)


                                    If IsNothing(gvQAllocBy.DataSource) = False Then
                                        For Each row As GridViewRow In gvQAllocBy.Rows
                                            Dim chk As CheckBox = CType(row.FindControl("chkSelect"), CheckBox)


                                            If allocationType(1).Contains(gvQAllocBy.DataKeys(row.RowIndex)("Id").ToString()) Then
                                                chk.Checked = True
                                            End If
                                        Next
                                    End If

                                End If




                            Case clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO
                                If CInt(kvp.Value) > 0 Then
                                    chkOrgYearNo.Checked = True
                                    chkOrgYearNo_CheckedChanged(Nothing, Nothing)
                                    txtQOrgYrNo.Text = kvp.Value
                                Else
                                    chkOrgYearNo.Checked = False
                                    chkOrgYearNo_CheckedChanged(Nothing, Nothing)
                                    txtQOrgYrNo.Text = "0"
                                End If

                            Case clstlsettings_master.enTalentConfiguration.MAX_AGE_NO
                                If CInt(kvp.Value) > 0 Then
                                    chkQMaxAge.Checked = True
                                    chkQMaxAge_CheckedChanged(Nothing, Nothing)
                                    txtQMaxAgeNo.Text = kvp.Value
                                Else
                                    chkQMaxAge.Checked = False
                                    chkQMaxAge_CheckedChanged(Nothing, Nothing)
                                    txtQMaxAgeNo.Text = "0"
                                End If

                            Case clstlsettings_master.enTalentConfiguration.MAX_SCREENER
                                If CInt(kvp.Value) > 0 Then
                                    chkQMaxScreeners.Checked = True
                                    chkQMaxScreeners_CheckedChanged(Nothing, Nothing)
                                    txtMaxQScreener.Text = kvp.Value

                                Else
                                    chkQMaxScreeners.Checked = False
                                    chkQMaxScreeners_CheckedChanged(Nothing, Nothing)
                                    txtMaxQScreener.Text = "0"

                                End If


                            Case clstlsettings_master.enTalentConfiguration.MIN_PERF_NO
                                If CInt(kvp.Value) > 0 Then
                                    chkQMinScrReq.Checked = True
                                    chkQMinScrReq_CheckedChanged(Nothing, Nothing)
                                    txtQPrdNo.Text = kvp.Value
                                Else
                                    chkQMinScrReq.Checked = False
                                    chkQMinScrReq_CheckedChanged(Nothing, Nothing)
                                    txtQPrdNo.Text = "0"
                                End If
                            Case clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ
                                If CInt(kvp.Value) > 0 Then
                                    chkQMinScrReq.Checked = True
                                    chkQMinScrReq_CheckedChanged(Nothing, Nothing)
                                    txtQMinScrReq.Text = kvp.Value
                                Else
                                    chkQMinScrReq.Checked = False
                                    chkQMinScrReq_CheckedChanged(Nothing, Nothing)
                                    txtQMinScrReq.Text = "0"
                                End If

                            Case clstlsettings_master.enTalentConfiguration.PERF_SCORE
                                If CInt(kvp.Value) > 0 Then
                                    chkPerformanceScore.Checked = True
                                    chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                    txtMinPSValue.Text = kvp.Value

                                Else
                                    chkPerformanceScore.Checked = False
                                    chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                    txtMinPSValue.Text = "0"

                                End If

                            Case clstlsettings_master.enTalentConfiguration.SCREENING_GUIDELINE
                            Case clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT
                                If CInt(kvp.Value) > 0 Then
                                    chkQTotalQueWeight.Checked = True
                                    chkQTotalQueWeight_CheckedChanged(Nothing, Nothing)
                                    txtQTotalQueWeight.Text = kvp.Value
                                Else
                                    chkQTotalQueWeight.Checked = False
                                    chkQTotalQueWeight_CheckedChanged(Nothing, Nothing)
                                    txtQTotalQueWeight.Text = "0"
                                End If

                                'Sohail (03 Nov 2020) -- Start
                                'NMB Enhancement : # : add insruction textbox in qualifying setting it should be mandatory information.
                            Case clstlsettings_master.enTalentConfiguration.INSTRUCTION
                                txtInstruction.Text = CStr(kvp.Value)
                                'Sohail (03 Nov 2020) -- End

                            Case clstlsettings_master.enTalentConfiguration.MAX_DATA_DISPLAY
                                txtQTotalDataDisplayPerStage.Text = CStr(kvp.Value)

                        End Select
                    Next
                End If

            End If

            'If mintCycleUnkid > 0 Then
            '    blnFlag = objscreener.Update()
            'Else
            '    blnFlag = objscreener.Insert()
            'End If

            'If blnFlag = False AndAlso objscreener._Message.Trim.Length > 0 Then
            '    DisplayMessage.DisplayMessage(objscreener._Message, Me)
            'Else
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 9, "Screener Save successfully."), Me)
            '    FillScreener()
            '    ClearScreenerCtrls()
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlsetting = Nothing
        End Try
    End Sub

    Protected Sub drpQAllocBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpQAllocBy.SelectedIndexChanged
        Dim intColType As Integer = 0
        Dim dsList As DataSet
        Dim dtTable As DataTable

        Try
            Select Case CInt(drpQAllocBy.SelectedValue)

                Case CInt(enAllocation.BRANCH)

                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                    If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
                        If CInt(Session("UserId")) > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.DEPARTMENT_GROUP)

                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                    If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.DEPARTMENT)

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                    If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.SECTION_GROUP)

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                    If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.SECTION)

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                    If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.UNIT_GROUP)

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                    If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.UNIT)

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                    If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.TEAM)

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                    If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.JOB_GROUP)

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                    If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.JOBS)
                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                    If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.CLASS_GROUP)

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                    If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case CInt(enAllocation.CLASSES)

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                    If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
                        Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
                        StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                        dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case CInt(enAllocation.COST_CENTER)

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2
                    dtTable = dsList.Tables("List")

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobgroupunkid").ColumnName = "Id"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_name").ColumnName = "Name"
            End If

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsCheck"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                dtTable.Rows.Add(r)
            End If

            gvQAllocBy.DataSource = dtTable
            gvQAllocBy.DataBind()

        End Try
    End Sub
#End Region

#Region " Button's Event "
    Protected Sub btnQualifySave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualifySave.Click
        Dim objtlsetting As New clstlsettings_master
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidQualifying() = False Then
                Exit Sub
            End If

            SetQualifyValue(objtlsetting)

            Dim tlSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

            If chkPerformanceScore.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.PERF_SCORE, txtMinPSValue.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.PERF_SCORE, "0")
            End If

            If chkPerformanceScore.Checked = False Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.ANY_PERIOD, chkPerformanceScore.Checked.ToString())
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.ALL_PERIOD, chkPerformanceScore.Checked.ToString())
            Else
                If rdbanyPeriod.Checked Then
                    tlSetting.Add(clstlsettings_master.enTalentConfiguration.ANY_PERIOD, rdbanyPeriod.Checked.ToString())
                    tlSetting.Add(clstlsettings_master.enTalentConfiguration.ALL_PERIOD, (Not rdbanyPeriod.Checked).ToString())
                End If

                If rdballPeriod.Checked Then
                    tlSetting.Add(clstlsettings_master.enTalentConfiguration.ALL_PERIOD, rdballPeriod.Checked.ToString())
                    tlSetting.Add(clstlsettings_master.enTalentConfiguration.ANY_PERIOD, (Not rdballPeriod.Checked).ToString())
                End If
            End If


            If chkPerformanceScore.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO, txtQPrdNo.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO, "0")
            End If


            If chkQMaxAge.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO, txtQMaxAgeNo.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO, "0")
            End If

            If chkOrgYearNo.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO, txtQOrgYrNo.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO, "0")
            End If

            If chkQMaxScreeners.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MAX_SCREENER, txtMaxQScreener.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MAX_SCREENER, "0")
            End If

            If chkQMinScrReq.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ, txtQMinScrReq.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ, "0")
            End If

            If chkQTotalQueWeight.Checked Then
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT, txtQTotalQueWeight.Text)
            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT, "0")
            End If

            'Sohail (03 Nov 2020) -- Start
            'NMB Enhancement : # : add insruction textbox in qualifying setting it should be mandatory information.
            tlSetting.Add(clstlsettings_master.enTalentConfiguration.INSTRUCTION, txtInstruction.Text)
            'Sohail (03 Nov 2020) -- End

            tlSetting.Add(clstlsettings_master.enTalentConfiguration.MAX_DATA_DISPLAY, txtQTotalDataDisplayPerStage.Text)

            If chkAllocationBy.Checked Then
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                Dim lstIDs As List(Of String) = Nothing
                Dim allocation_Id As String = ""

                gRow = gvQAllocBy.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

                lstIDs = (From p In gRow Select (gvQAllocBy.DataKeys(p.DataItemIndex)("Id").ToString())).ToList()
                allocation_Id = drpQAllocBy.SelectedValue + "|" + String.Join(",", lstIDs.ToArray)
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE, allocation_Id)

            Else
                tlSetting.Add(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE, "")
            End If

            objtlsetting._DatabaseName = CStr(Session("Database_Name"))
            blnFlag = objtlsetting.SaveTlSetting(tlSetting, CInt(drpQMPeriod.SelectedValue))

            If blnFlag = False AndAlso objtlsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objtlsetting._Message, Me)
            Else
                If objtlscreening_process_master.IsTalentStartedForCycle(CInt(drpQMPeriod.SelectedValue)) Then
                    Dim objtlcycle_master As New clstlcycle_master

                    objtlcycle_master._Cycleunkid = CInt(drpQMPeriod.SelectedValue)

                    Dim dsOldList As DataSet = objtlscreening_process_master.GetList("OldList", CInt(CInt(drpQMPeriod.SelectedValue)), False)
                    Dim dskist As DataSet = objtlscreening_process_master.GetTalentProcessedDataAfterSettingChage(CStr(Session("Database_Name")), _
                                                                     CInt(Session("UserId")), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                     objtlcycle_master._Start_Date, _
                                                                     CInt(drpQMPeriod.SelectedValue))

                    If IsNothing(dsOldList) = False AndAlso IsNothing(dskist) = False Then
                        Dim dtGetIsMatch As DataTable = dskist.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ismatch") = True).AsDataView().ToTable()
                        Dim dtSetIsMatch As DataTable = dskist.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ismatch") = False).AsDataView().ToTable()

                        Dim intProcessmstunkids As Integer() = dsOldList.Tables("OldList").AsEnumerable().Select(Function(x) x.Field(Of Integer)("processmstunkid")).Except(dtGetIsMatch.AsEnumerable().Select(Function(y) y.Field(Of Integer)("processmstunkid"))).ToArray()
                        Dim intSetIsMatchProcessmstunkid As Integer() = dtSetIsMatch.AsEnumerable().Select(Function(x) x.Field(Of Integer)("processmstunkid")).ToArray()

                        objtlscreening_process_master._AuditUserId = CInt(Session("UserId"))
                        objtlscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                        objtlscreening_process_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
                        objtlscreening_process_master._DatabaseName = CStr(Session("Database_Name"))
                        objtlscreening_process_master._FormName = mstrModuleName3
                        objtlscreening_process_master._FromWeb = True
                        objtlscreening_process_master._HostName = CStr(Session("HOST_NAME"))

                        If intProcessmstunkids.Length > 0 Then
                            objtlscreening_process_master._Ismatch = True
                            blnFlag = objtlscreening_process_master.UpdateIsMatch(String.Join(",", intProcessmstunkids.Select(Function(x) x.ToString()).ToArray()), False)
                            If blnFlag = False AndAlso objtlsetting._Message.Trim.Length > 0 Then
                                DisplayMessage.DisplayMessage(objtlsetting._Message, Me)
                            End If
                        End If

                        If intSetIsMatchProcessmstunkid.Length > 0 Then
                            objtlscreening_process_master._Ismatch = False
                            blnFlag = objtlscreening_process_master.UpdateIsMatch(String.Join(",", intSetIsMatchProcessmstunkid.Select(Function(x) x.ToString()).ToArray()), True)
                            If blnFlag = False AndAlso objtlsetting._Message.Trim.Length > 0 Then
                                DisplayMessage.DisplayMessage(objtlsetting._Message, Me)
                            End If
                        End If

                    End If
                End If



                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 11, "Qualification Setting Save successfully."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlsetting = Nothing
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Checkbox Event "
    Protected Sub chkPerformanceScore_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPerformanceScore.CheckedChanged
        Try
            pnlPerformanceScore.Enabled = chkPerformanceScore.Checked
            txtMinPSValue.Enabled = chkPerformanceScore.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkQMaxAge_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMaxAge.CheckedChanged
        Try
            txtQMaxAgeNo.Enabled = chkQMaxAge.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOrgYearNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOrgYearNo.CheckedChanged
        Try
            txtQOrgYrNo.Enabled = chkOrgYearNo.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub chkQMaxScreeners_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMaxScreeners.CheckedChanged
        Try
            txtMaxQScreener.Enabled = chkQMaxScreeners.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub chkQMinScrReq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMinScrReq.CheckedChanged
        Try
            txtQMinScrReq.Enabled = chkQMinScrReq.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub chkQTotalQueWeight_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQTotalQueWeight.CheckedChanged
        Try
            txtQTotalQueWeight.Enabled = chkQTotalQueWeight.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAllocationBy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllocationBy.CheckedChanged
        Try
            pnlAllocation.Enabled = chkAllocationBy.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Radion Button's Event "

    Protected Sub rdballPeriod_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdballPeriod.CheckedChanged, rdbanyPeriod.CheckedChanged
        Try
            Dim rdb As RadioButton = CType(sender, RadioButton)

            If rdb.ID = rdballPeriod.ID Then
                txtQPrdNo.Enabled = False
                txtQPrdNo.Text = "0"
            Else
                txtQPrdNo.Enabled = True
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Screeners Setup "

#Region " Private Methods "
    Private currentId As String = ""

    Private Sub FillScreenersCombo()
        Dim objUser As New clsUserAddEdit
        Dim obCyl As New clstlcycle_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), , CInt(Session("Fin_year")), True)
            drpscreenerUser.DataSource = dsCombo.Tables("User")
            drpscreenerUser.DataTextField = "name"
            drpscreenerUser.DataValueField = "userunkid"
            drpscreenerUser.DataBind()


            dsCombo = obCyl.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)
            With drpScreenerPeriod
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUser = Nothing
            obCyl = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#DDD")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function IsValidScreener() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objtlsettings_master As New clstlsettings_master
        Dim objtlscreener_master As New clstlscreener_master
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            If CInt(drpScreenerPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 5, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
                Return False
            End If


            Dim mdicSetting As Dictionary(Of clstlsettings_master.enTalentConfiguration, String) = objtlsettings_master.GetSettingFromPeriod(CInt(drpScreenerPeriod.SelectedValue))
            If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 16, "Sorry you can't add screener,Reason: Talent Setting is not define."), Me)
                Return False
            End If

            If objtlscreening_process_master.IsTalentStartedForCycle(CInt(drpScreenerPeriod.SelectedValue)) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 12, "Sorry you can't add this screener,Reason: Talent Process is already started for this cycle."), Me)
                Exit Function
            End If


            If CInt(drpscreenerUser.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 1, "Sorry, User is mandatory information. Please select user to continue."), Me)
                Return False
            End If

            Dim MaxScreenerCount As String = objtlsettings_master.GetSettingValueFromKey(CInt(drpScreenerPeriod.SelectedValue), clstlsettings_master.enTalentConfiguration.MAX_SCREENER)
            Dim CurrentScreenerCount As Integer = objtlscreener_master.GetMaximumScreenerCountFromCycleId(CInt(drpScreenerPeriod.SelectedValue)) + 1

            If MaxScreenerCount.Length = 0 Then MaxScreenerCount = "0"
            If CDbl(MaxScreenerCount) < CurrentScreenerCount Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 6, "Sorry, Your Total Screeners limit exceeded."), Me)
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
            objtlsettings_master = Nothing
            objtlscreening_process_master = Nothing
        End Try
    End Function

    Private Sub ClearScreenerCtrls()
        Try
            mintScreenerUnkid = 0
            drpScreenerPeriod.SelectedValue = CStr(0)
            drpscreenerUser.SelectedValue = CStr(0)
            TvApproverUseraccess.DataSource = Nothing
            TvApproverUseraccess.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillScreener()
        Dim objScreener As New clstlscreener_master
        Dim dsList As New DataSet
        Try
            dsList = objScreener.GetList("List", CInt(drpScreenerPeriod.SelectedValue), False)
            gvScreener.DataSource = dsList.Tables(0)
            gvScreener.DataBind()

            gvScreener.Columns(0).Visible = CBool(Session("AllowToModifyTalentSetting"))
            gvScreener.Columns(1).Visible = CBool(Session("AllowToModifyTalentSetting"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try
    End Sub

    Private Sub SetScreenerValue(ByRef objScreener As clstlscreener_master)
        Try
            objScreener._Screenermstunkid = mintScreenerUnkid
            objScreener._Cycleunkid = CInt(drpScreenerPeriod.SelectedValue)
            objScreener._Mapuserunkid = CInt(drpscreenerUser.SelectedValue)
            objScreener._Userunkid = CInt(Session("UserId"))
            objScreener._Isactive = True
            objScreener._Isvoid = False

            objScreener._AuditUserId = CInt(Session("UserId"))
            objScreener._ClientIP = CStr(Session("IP_ADD"))
            objScreener._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objScreener._DatabaseName = CStr(Session("Database_Name"))
            objScreener._FormName = mstrModuleName4
            objScreener._FromWeb = True
            objScreener._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Gridview Event"
    Protected Sub drpApproverUseraccess_user_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpscreenerUser.SelectedIndexChanged
        Dim objUser As New clsUserAddEdit
        Try
            Dim dtTable As DataTable = Nothing
            If CInt(drpscreenerUser.SelectedValue) > 0 Then
                dtTable = objUser.GetUserAccessList(Convert.ToString(Session("UserAccessModeSetting")), CInt(drpscreenerUser.SelectedValue), Convert.ToInt32(Session("CompanyUnkId")))
                dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            End If
            TvApproverUseraccess.DataSource = dtTable
            TvApproverUseraccess.DataBind()
            dtTable = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUser = Nothing
        End Try
    End Sub

    Protected Sub TvApproverUseraccess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TvApproverUseraccess.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                oldid = dt.Rows(e.Row.RowIndex)("UserAccess").ToString()
                If CInt(dt.Rows(e.Row.RowIndex)("AllocationLevel").ToString()) = -1 AndAlso oldid <> currentId Then
                    Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("UserAccess").ToString(), TvApproverUseraccess)
                    currentId = oldid
                Else
                    Dim statusCell As TableCell = e.Row.Cells(0)
                    statusCell.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dt.Rows(e.Row.RowIndex)("UserAccess").ToString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

    Protected Sub gvScreener_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvScreener.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("t1ScreenerActive"), LinkButton)
                Dim lnkInActive As LinkButton = TryCast(e.Row.FindControl("t1ScreenerInActive"), LinkButton)


                If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                    If CBool(dt.Rows(e.Row.RowIndex)("isactive").ToString()) = True Then
                        lnkactive.Visible = False
                        lnkInActive.Visible = True
                    Else
                        lnkactive.Visible = True
                        lnkInActive.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link Event(s) "

    Protected Sub lnkt1ScreenerDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 13, "Sorry you can't delete this screener,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintScreenerUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("screenermstunkid"))
            mstrDeleteAction = "DELSC"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 3, "You are about to delete this screener. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkt1ScreenerInactive_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 14, "Sorry you can't inactive this screener,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintScreenerUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("screenermstunkid"))
            mstrDeleteAction = "Inactivesc"


            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 7, "You are about to inactive this screener. Are you sure you want to inactive?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkt1ScreenerActive_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 15, "Sorry you can't active this screener,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintScreenerUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("screenermstunkid"))
            mstrDeleteAction = "Activesc"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 8, "You are about to active this screener. Are you sure you want to active?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnAddScreener_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddScreener.Click
        Dim objscreener As New clstlscreener_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidScreener() = False Then
                Exit Sub
            End If

            SetScreenerValue(objscreener)
            If mintScreenerUnkid > 0 Then
                blnFlag = objscreener.Update()
            Else
                blnFlag = objscreener.Insert()
            End If

            If blnFlag = False AndAlso objscreener._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objscreener._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 2, "Screener Save successfully."), Me)
                FillScreener()
                ClearScreenerCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objscreener = Nothing
        End Try
    End Sub

#End Region

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.

#Region "Combbox Events"

    Protected Sub drpScreenerPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpScreenerPeriod.SelectedIndexChanged
        Try
            If CInt(drpScreenerPeriod.SelectedValue) > 0 Then
                FillScreener()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (12-Dec-2020) -- End

#End Region

#Region " Questionnaire Setup "

#Region " Private Method(s)"

    Private Sub ClearQuestionnaireCtrls()
        Try
            mintQuestionnaireUnkid = 0
            drpQPeriod.SelectedValue = CStr(0)
            txtQWeight.Text = "1"
            txtQuestion.Text = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Private Sub FillQuestionnaire()
    Private Sub FillQuestionnaire(Optional ByVal xCycleId As Integer = 0)
        'Pinkal (12-Dec-2020) -- End
        Dim objQuestionnaire As New clstlquestionnaire_master
        Dim dsList As New DataSet
        Try
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            'dsList = objQuestionnaire.GetList("List")
            dsList = objQuestionnaire.GetList("List", True, xCycleId)
            'Pinkal (12-Dec-2020) -- End
            gvobjQuestionnaire.DataSource = dsList.Tables(0)
            gvobjQuestionnaire.DataBind()

            gvobjQuestionnaire.Columns(0).Visible = CBool(Session("AllowToModifyTalentSetting"))
            gvobjQuestionnaire.Columns(1).Visible = CBool(Session("AllowToModifyTalentSetting"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestionnaire = Nothing
        End Try
    End Sub

    Private Sub FillComboForQuestionnaire()
        Dim obCyl As New clstlcycle_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = obCyl.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)

            With drpQPeriod
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            obCyl = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Function IsValidQuestionnaire() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objtlquestionnaire_master As New clstlquestionnaire_master
        Dim objtlsettings_master As New clstlsettings_master
        objtlquestionnaire_master._DatabaseName = CStr(Session("Database_Name"))
        objtlsettings_master._DatabaseName = CStr(Session("Database_Name"))
        Dim objtlscreening_process_master As New clstlscreening_process_master


        Try
            If CInt(drpQPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 1, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
                Return False
            End If


            Dim mdicSetting As Dictionary(Of clstlsettings_master.enTalentConfiguration, String) = objtlsettings_master.GetSettingFromPeriod(CInt(drpQPeriod.SelectedValue))
            If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 11, "Sorry you can't add question,Reason: Talent Setting is not define."), Me)
                Return False
            End If


            If objtlscreening_process_master.IsTalentStartedForCycle(CInt(drpQPeriod.SelectedValue)) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 8, "Sorry you can't add new question,Reason: Talent Process is already started for this cycle."), Me)
                Exit Function
            End If


            If txtQWeight.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 2, "Sorry, Weight is mandatory information. Please enter Weight to continue."), Me)
                Return False
            End If

            If txtQuestion.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 3, "Sorry, Question is mandatory information. Please enter Question to continue."), Me)
                Return False
            End If

            Dim MaxQuestionWeight As String = objtlsettings_master.GetSettingValueFromKey(CInt(drpQPeriod.SelectedValue), clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT)
            If MaxQuestionWeight.Length > 0 Then
                Dim CurrentQuestionWeight As Double = objtlquestionnaire_master.GetQuestionnairMaxWeightFromCycleId(CInt(drpQPeriod.SelectedValue), mintQuestionnaireUnkid) + CDbl(txtQWeight.Text)

                If CDbl(MaxQuestionWeight) < CurrentQuestionWeight Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 7, "Sorry, Your Total question weight limit exceeded."), Me)
                    Return False
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 11, "Total Question Weight is not defined in Selected Period. So Please Set Total Question Weight for Selected Period in Qualifying Criteria Setup "), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
            objtlquestionnaire_master = Nothing
            objtlsettings_master = Nothing
        End Try
    End Function

    Private Sub SetQuestionnaireValue(ByRef objQuestionnaire As clstlquestionnaire_master)
        Try
            objQuestionnaire._Questionnaireunkid = mintQuestionnaireUnkid
            objQuestionnaire._Cycleunkid = CInt(drpQPeriod.SelectedValue)
            objQuestionnaire._AuditUserId = CInt(Session("UserId"))
            objQuestionnaire._ClientIP = CStr(Session("IP_ADD"))
            objQuestionnaire._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objQuestionnaire._DatabaseName = CStr(Session("Database_Name"))
            objQuestionnaire._FormName = mstrModuleName5
            objQuestionnaire._FromWeb = True
            objQuestionnaire._HostName = CStr(Session("HOST_NAME"))
            objQuestionnaire._Isactive = True
            objQuestionnaire._Question = txtQuestion.Text
            objQuestionnaire._Weight = Convert.ToSingle(txtQWeight.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetQuestionnaireValue(ByVal intQuestionnaireId As Integer)
        Dim objQuestionnaire As New clstlquestionnaire_master
        Try
            objQuestionnaire._Questionnaireunkid = mintQuestionnaireUnkid
            drpQPeriod.SelectedValue = CStr(objQuestionnaire._Cycleunkid)
            txtQWeight.Text = CStr(objQuestionnaire._Weight)
            txtQuestion.Text = objQuestionnaire._Question

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestionnaire = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Events "

    Protected Sub btnQuestionAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuestionAdd.Click
        Dim objQuestion As New clstlquestionnaire_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidQuestionnaire() = False Then
                Exit Sub
            End If
            SetQuestionnaireValue(objQuestion)
            If mintQuestionnaireUnkid > 0 Then
                blnFlag = objQuestion.Update()
            Else
                blnFlag = objQuestion.Insert()
            End If

            If blnFlag = False AndAlso objQuestion._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objQuestion._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 4, "Talent Questionnaire defined successfully."), Me)
                FillQuestionnaire()
                ClearQuestionnaireCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestion = Nothing
        End Try
    End Sub

    Protected Sub btnQuestionReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuestionReset.Click
        Try
            Call ClearQuestionnaireCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkQEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvobjQuestionnaire.DataKeys(row.RowIndex)("Cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 9, "Sorry you can't edit this Question,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintQuestionnaireUnkid = CInt(gvobjQuestionnaire.DataKeys(row.RowIndex)("questionnaireunkid"))
            GetQuestionnaireValue(mintQuestionnaireUnkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkQDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvobjQuestionnaire.DataKeys(row.RowIndex)("Cycleunkid"))

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 10, "Sorry you can't delete this Question,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintQuestionnaireUnkid = CInt(gvobjQuestionnaire.DataKeys(row.RowIndex)("questionnaireunkid"))
            mstrDeleteAction = "delque"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 5, "You are about to delete this Question. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.

#Region "Combbox Events"

    Protected Sub drpQPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpQPeriod.SelectedIndexChanged
        Try
            FillQuestionnaire(CInt(drpQPeriod.SelectedValue))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (12-Dec-2020) -- End

#End Region

#Region " Ratings Setup "

#Region " Private Method(s)"

    Private Sub ClearRatingsCtrls()
        Try
            mintRatingsUnkid = 0
            drpRPeriod.SelectedValue = CStr(0)
            txtRDescription.Text = ""
            txtScrF.Text = "1"
            txtScrT.Text = "1"
            drpRStage.SelectedValue = CStr(0)
            txtRActionColor.Text = "#000000"
            txtScrF.Enabled = True
            txtScrT.Enabled = True
            drpRPeriod.Enabled = True
            drpRStage.Enabled = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Private Sub FillRatings()
    Private Sub FillRatings(Optional ByVal xCycleId As Integer = 0)
        'Pinkal (12-Dec-2020) -- End
        Dim objRatings As New clstlratings_master
        Dim dsList As New DataSet
        Try
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            'dsList = objRatings.GetList("List")
            dsList = objRatings.GetList("List", xCycleId)
            'Pinkal (12-Dec-2020) -- End
            gvobjRatings.DataSource = dsList.Tables(0)
            gvobjRatings.DataBind()

            gvobjRatings.Columns(0).Visible = CBool(Session("AllowToModifyTalentSetting"))
            gvobjRatings.Columns(1).Visible = CBool(Session("AllowToModifyTalentSetting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

    Private Sub FillComboForRatings()
        Dim obCyl As New clstlcycle_master
        Dim objstages As New clstlstages_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = obCyl.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)

            With drpRPeriod
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objstages.getComboList("List", True, CInt(drpRPeriod.SelectedValue), True)

            With drpRStage
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            obCyl = Nothing
            objstages = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Function IsValidRatings() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objRatings As New clstlratings_master
        Dim objtlsettings_master As New clstlsettings_master
        Dim dsList As New DataSet
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try

            If CInt(drpRPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 1, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
                Return False
            End If

            Dim mdicSetting As Dictionary(Of clstlsettings_master.enTalentConfiguration, String) = objtlsettings_master.GetSettingFromPeriod(CInt(drpRPeriod.SelectedValue))
            If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 16, "Sorry you can't add rating,Reason: Talent Setting is not define."), Me)
                Return False
            End If

            If objtlscreening_process_master.IsTalentStartedForCycle(CInt(drpRPeriod.SelectedValue)) AndAlso mintRatingsUnkid <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 13, "Sorry you can't save rating,Reason: Talent Process is already started for this cycle."), Me)
                Exit Function
            End If

            If txtScrF.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 2, "Sorry, Score From is mandatory information. Please enter Score From to continue."), Me)
                Return False
            End If

            If txtScrT.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 3, "Sorry, Score To is mandatory information. Please enter Score To to continue."), Me)
                Return False
            End If

            If txtRDescription.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 4, "Sorry, Description is mandatory information. Please enter Description to continue."), Me)
                Return False
            End If

            If CInt(drpRStage.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 5, "Sorry, Action is mandatory information. Please select Action to continue."), Me)
                Return False
            End If

            If CDbl(txtScrF.Text) > CDbl(txtScrT.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 6, "Sorry, Score from  should not be greater than Score To."), Me)
                Return False
            End If

            dsList = objRatings.GetList("List", CInt(drpRPeriod.SelectedValue))
            For Each drRow As DataRow In dsList.Tables(0).Select("ratingunkid <> " & mintRatingsUnkid)
                If (CDbl(txtScrF.Text) >= CDbl(drRow.Item("scorefrom")) AndAlso CDbl(txtScrF.Text) <= CDbl(drRow.Item("scoreto"))) OrElse _
                (CDbl(txtScrT.Text) >= CDbl(drRow.Item("scorefrom")) AndAlso CDbl(txtScrT.Text) <= CDbl(drRow.Item("scoreto"))) Then

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 7, "Sorry, Score from  or Score To is already defined Range."), Me)
                    Return False

                End If
            Next

            Dim drExistRow() As DataRow = dsList.Tables(0).Select("ratingunkid <> " & mintRatingsUnkid & " AND scoreto > " & CDbl(txtScrF.Text) & "  AND scoreto < " & CDbl(txtScrT.Text))
            If drExistRow.Length > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 8, "Sorry, Score from  and Score To is not valid defined Range."), Me)
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
            objFinYear = Nothing
        End Try
    End Function

    Private Sub SetRatingsValue(ByRef objRatings As clstlratings_master)
        Try
            objRatings._Ratingunkid = mintRatingsUnkid
            objRatings._Cycleunkid = CInt(drpRPeriod.SelectedValue)
            objRatings._Description = txtRDescription.Text
            objRatings._Color = txtRActionColor.Text
            objRatings._Scorefrom = Convert.ToSingle(txtScrF.Text)
            objRatings._Scoreto = Convert.ToSingle(txtScrT.Text)
            objRatings._Stageunkid = CInt(drpRStage.SelectedValue)
            objRatings._AuditUserId = CInt(Session("UserId"))
            objRatings._ClientIP = CStr(Session("IP_ADD"))
            objRatings._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objRatings._DatabaseName = CStr(Session("Database_Name"))
            objRatings._FormName = mstrModuleName6
            objRatings._FromWeb = True
            objRatings._HostName = CStr(Session("HOST_NAME"))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetRatingsValue(ByVal intRatingsId As Integer)
        Dim objRatings As New clstlratings_master
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            objRatings._Ratingunkid = mintRatingsUnkid
            drpRPeriod.SelectedValue = CStr(objRatings._Cycleunkid)
            txtScrF.Text = CStr(objRatings._Scorefrom)
            txtScrT.Text = CStr(objRatings._Scoreto)
            txtRDescription.Text = objRatings._Description
            drpRStage.SelectedValue = CStr(objRatings._Stageunkid)
            txtRActionColor.Text = objRatings._Color

            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                txtScrF.Enabled = False
                txtScrT.Enabled = False
                drpRPeriod.Enabled = False
                drpRStage.Enabled = False
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
            objRatings = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnRAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRAdd.Click
        Dim objRatings As New clstlratings_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidRatings() = False Then
                Exit Sub
            End If
            SetRatingsValue(objRatings)
            If mintRatingsUnkid > 0 Then
                blnFlag = objRatings.Update()
            Else
                blnFlag = objRatings.Insert()
            End If

            If blnFlag = False AndAlso objRatings._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objRatings._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 9, "Talent Ratings defined successfully."), Me)
                FillRatings()
                ClearRatingsCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

    Protected Sub btnRReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRReset.Click
        Try
            Call ClearRatingsCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkREdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvobjRatings.DataKeys(row.RowIndex)("cycleunkid"))
            mintRatingsUnkid = CInt(gvobjRatings.DataKeys(row.RowIndex)("ratingunkid"))
            GetRatingsValue(mintRatingsUnkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkRDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintCycleUnkid = CInt(gvobjRatings.DataKeys(row.RowIndex)("cycleunkid"))
            If objtlscreening_process_master.IsTalentStartedForCycle(mintCycleUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 15, "Sorry you can't delete this rating,Reason: Talent Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintRatingsUnkid = CInt(gvobjRatings.DataKeys(row.RowIndex)("ratingunkid"))
            mstrDeleteAction = "delrat"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 10, "You are about to delete this Rating. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "
    Protected Sub drpRPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpRPeriod.SelectedIndexChanged
        Dim objstages As New clstlstages_master
        Dim dsCombo As DataSet = Nothing
        Try
            If CInt(drpRPeriod.SelectedValue) > 0 Then
                dsCombo = objstages.getComboList("List", True, CInt(drpRPeriod.SelectedValue), True)

                With drpRStage
                    .DataValueField = "Id"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables("List")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            FillRatings(CInt(drpRPeriod.SelectedValue))
            'Pinkal (12-Dec-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Protected Sub gvobjRatings_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvobjRatings.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblColor As Label = TryCast(e.Row.FindControl("objLblcolor"), Label)
            Dim hfColor As HiddenField = TryCast(e.Row.FindControl("hfColor"), HiddenField)
            lblColor.BackColor = Drawing.ColorTranslator.FromHtml(hfColor.Value)
        End If
    End Sub

#End Region

#Region "Confirmation"
    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try

            Select Case mstrDeleteAction.ToUpper()
                Case "DELCY"
                    Dim objCycle As New clstlcycle_master

                    'Pinkal (12-Dec-2020) -- Start
                    'Enhancement  -  Working on Talent Issue which is given by Andrew.
                    If objCycle.isUsed(mintCycleUnkid) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 13, "Sorry, you cannot delete selected cycle. Reason the selected cycle is already linked with the transactions."), Me)
                        Exit Sub
                    End If
                    'Pinkal (12-Dec-2020) -- End

                    SetCycleValue(objCycle)
                    objCycle._Isactive = False
                    objCycle._Inactiveuserunkid = CInt(Session("UserId"))
                    blnFlag = objCycle.Delete(mintCycleUnkid)
                    If blnFlag = False AndAlso objCycle._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objCycle._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, "Talent cycle deleted successfully."), Me)
                        FillCycle()
                        'Pinkal (12-Dec-2020) -- Start
                        'Enhancement  -  Working on Talent Issue which is given by Andrew.
                        'Enhancement  -  Working on Talent Issue which is given by Andrew.
                        FillStageCombo()
                        FillStaging()
                        FillScreenersCombo()
                        FillScreener()
                        FillQualifyingCombo()
                        FillComboForQuestionnaire()
                        FillQuestionnaire()
                        FillComboForRatings()
                        FillRatings()
                        'Pinkal (12-Dec-2020) -- End
                    End If
                    objCycle = Nothing
                Case "DELSTA"
                    Dim objStaging As New clstlstages_master
                    Dim dsGetList As DataSet = Nothing
                    objStaging._Stageunkid = mintStagingUnkid
                    If objStaging._Isdefault = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 10, "Sorry, You can not delete this entry, Reason:This is auto generated entry from Talent Cycle Master."), Me)
                        Exit Sub
                    End If
                    objStaging._ApplyOnFloworder = objStaging._Floworder
                    objStaging._Isactive = False
                    objStaging._AuditUserId = CInt(Session("UserId"))
                    objStaging._ClientIP = CStr(Session("IP_ADD"))
                    objStaging._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    objStaging._DatabaseName = CStr(Session("Database_Name"))
                    objStaging._FormName = mstrModuleName2
                    objStaging._FromWeb = True
                    objStaging._HostName = CStr(Session("HOST_NAME"))

                    dsGetList = objStaging.GetList("List", objStaging._Cycleunkid)
                    blnFlag = objStaging.Delete(mintStagingUnkid, , dsGetList.Tables(0))
                    If blnFlag = False AndAlso objStaging._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objStaging._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 11, "Talent Stage deleted successfully."), Me)
                        FillStaging()
                    End If
                    objStaging = Nothing
                Case "DELQUE"
                    Dim objQuestionnaire As New clstlquestionnaire_master
                    SetQuestionnaireValue(objQuestionnaire)
                    objQuestionnaire._Isactive = False
                    blnFlag = objQuestionnaire.Delete(mintQuestionnaireUnkid)
                    If blnFlag = False AndAlso objQuestionnaire._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objQuestionnaire._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 6, "Talent Questionnaire deleted successfully."), Me)
                        FillQuestionnaire()
                    End If
                    objQuestionnaire = Nothing
                Case "DELRAT"

                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 12, "Rating delete reason.")
                    delReason.Show()

                Case "INACTIVESC"
                    Dim objScreener As New clstlscreener_master
                    SetScreenerValue(objScreener)
                    blnFlag = objScreener.ActiveInactiveScreener(mintScreenerUnkid, False)
                    If blnFlag = False AndAlso objScreener._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objScreener._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 9, "Screener Inactivated successfully."), Me)
                        FillScreener()
                    End If
                    objScreener = Nothing

                Case "ACTIVESC"
                    Dim objScreener As New clstlscreener_master
                    SetScreenerValue(objScreener)
                    blnFlag = objScreener.ActiveInactiveScreener(mintScreenerUnkid, True)
                    If blnFlag = False AndAlso objScreener._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objScreener._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 10, "Screener Activated successfully."), Me)
                        FillScreener()
                    End If
                    objScreener = Nothing

                Case "DELSC"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 4, "Screener delete reason.")
                    delReason.Show()

                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Delete Reason"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()

                Case "DELSC"
                    Dim objScreener As New clstlscreener_master
                    SetScreenerValue(objScreener)
                    objScreener._Isvoid = True
                    objScreener._Voidreason = delReason.Reason
                    objScreener._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objScreener._Voiduserunkid = CInt(Session("UserId"))
                    blnFlag = objScreener.Delete(mintScreenerUnkid)
                    If blnFlag = False AndAlso objScreener._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objScreener._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 11, "Screener deleted successfully."), Me)
                        FillScreener()
                    End If
                    objScreener = Nothing

                Case "DELRAT"
                    Dim objRatings As New clstlratings_master
                    SetRatingsValue(objRatings)
                    objRatings._Isvoid = True
                    objRatings._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objRatings._Voiduserunkid = CInt(Session("UserId"))
                    objRatings._Voidreason = delReason.Reason
                    blnFlag = objRatings.Delete(mintRatingsUnkid)
                    If blnFlag = False AndAlso objRatings._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objRatings._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 11, "Talent Ratings deleted successfully."), Me)
                        FillRatings()
                    End If
                    objRatings = Nothing

                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button's Event"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTalentCycle.ID, Me.lblTalentCycle.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStaging.ID, Me.lblStaging.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQualifyingCriteria.ID, Me.lblQualifyingCriteria.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblScreeners.ID, Me.lblScreeners.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQuestinnaire.ID, Me.lblQuestinnaire.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRatings.ID, Me.lblRatings.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.BtnClose.ID, Me.BtnClose.Text)

            'Cycle Setup'
            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblAddEditCycle.ID, Me.lblAddEditCycle.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblFinYear.ID, Me.lblFinYear.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCode.ID, Me.lblCode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblName.ID, Me.lblName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblStartDate.ID, Me.lblStartDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblEndDate.ID, Me.lblEndDate.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCycleAdd.ID, Me.btnCycleAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCycleReset.ID, Me.btnCycleReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvCycle.Columns(2).FooterText, gvCycle.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvCycle.Columns(3).FooterText, gvCycle.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvCycle.Columns(4).FooterText, gvCycle.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvCycle.Columns(5).FooterText, gvCycle.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvCycle.Columns(6).FooterText, gvCycle.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvCycle.Columns(7).FooterText, gvCycle.Columns(7).HeaderText)

            'Staging Setup'
            'Language.setLanguage(mstrModuleName2)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblStages.ID, Me.lblStages.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblStageName.ID, Me.lblStageName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblFlowOrder.ID, Me.lblFlowOrder.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblPStage.ID, Me.lblPStage.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnSAdd.ID, Me.btnSAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnSReset.ID, Me.btnSReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvobjStaging.Columns(2).FooterText, gvobjStaging.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvobjStaging.Columns(3).FooterText, gvobjStaging.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvobjStaging.Columns(4).FooterText, gvobjStaging.Columns(4).HeaderText)

            'QualifyingCriteriaSetup'
            'Language.setLanguage(mstrModuleName3)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblQCriteria.ID, Me.lblQCriteria.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblQPeriodMapped.ID, Me.lblQPeriodMapped.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblInstruction.ID, Me.lblInstruction.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblminscore.ID, Me.lblminscore.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.Label2.ID, Me.Label2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblQPeriodNo.ID, Me.lblQPeriodNo.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkPerformanceScore.ID, Me.chkPerformanceScore.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMaxAge.ID, Me.chkQMaxAge.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkOrgYearNo.ID, Me.chkOrgYearNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMaxScreeners.ID, Me.chkQMaxScreeners.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMinScrReq.ID, Me.chkQMinScrReq.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQTotalQueWeight.ID, Me.chkQTotalQueWeight.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkAllocationBy.ID, Me.chkAllocationBy.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.btnQualifySave.ID, Me.btnQualifySave.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.rdbanyPeriod.ID, Me.rdbanyPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.rdballPeriod.ID, Me.rdballPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblQTotalDataDisplayPerStage.ID, Me.lblQTotalDataDisplayPerStage.Text)

            'Screeners Setup'
            'Language.setLanguage(mstrModuleName4)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblScsSetupTitle.ID, Me.lblScsSetupTitle.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblScreenerPeriod.ID, Me.lblScreenerPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblscreenerUser.ID, Me.lblscreenerUser.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblScsSetupTitle2.ID, Me.lblScsSetupTitle2.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.btnAddScreener.ID, Me.btnAddScreener.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, gvScreener.Columns(2).FooterText, gvScreener.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, gvScreener.Columns(3).FooterText, gvScreener.Columns(3).HeaderText)

            'Questionnaire Setup'
            'Language.setLanguage(mstrModuleName5)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblDefineQuestions.ID, Me.lblDefineQuestions.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblQPeriod.ID, Me.lblQPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblQWeight.ID, Me.lblQWeight.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblQuestion.ID, Me.lblQuestion.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.btnQuestionAdd.ID, Me.btnQuestionAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.btnQuestionReset.ID, Me.btnQuestionReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, gvobjQuestionnaire.Columns(2).FooterText, gvobjQuestionnaire.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, gvobjQuestionnaire.Columns(3).FooterText, gvobjQuestionnaire.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, gvobjQuestionnaire.Columns(4).FooterText, gvobjQuestionnaire.Columns(4).HeaderText)


            'Ratings Setup'
            'Language.setLanguage(mstrModuleName6)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRSetups.ID, Me.lblRSetups.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRPeriod.ID, Me.lblRPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblScrFrom.ID, Me.lblScrFrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblScrTo.ID, Me.lblScrTo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRActionColor.ID, Me.lblRActionColor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRAction.ID, Me.lblRAction.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRDescription.ID, Me.lblRDescription.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.btnRAdd.ID, Me.btnRAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.btnRReset.ID, Me.btnRReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(2).FooterText, gvobjRatings.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(3).FooterText, gvobjRatings.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(4).FooterText, gvobjRatings.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(5).FooterText, gvobjRatings.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(6).FooterText, gvobjRatings.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(7).FooterText, gvobjRatings.Columns(7).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblTalentCycle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTalentCycle.ID, Me.lblTalentCycle.Text)
            Me.lblStaging.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStaging.ID, Me.lblStaging.Text)
            Me.lblQualifyingCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualifyingCriteria.ID, Me.lblQualifyingCriteria.Text)
            Me.lblScreeners.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblScreeners.ID, Me.lblScreeners.Text)
            Me.lblQuestinnaire.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQuestinnaire.ID, Me.lblQuestinnaire.Text)
            Me.lblRatings.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRatings.ID, Me.lblRatings.Text)

            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

            'Cycle Setup'
            'Language.setLanguage(mstrModuleName1)
            Me.lblAddEditCycle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblAddEditCycle.ID, Me.lblAddEditCycle.Text)
            Me.lblFinYear.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblFinYear.ID, Me.lblFinYear.Text)
            Me.lblCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCode.ID, Me.lblCode.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEndDate.ID, Me.lblEndDate.Text)

            Me.btnCycleAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCycleAdd.ID, Me.btnCycleAdd.Text).Replace("&", "")
            Me.btnCycleReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCycleReset.ID, Me.btnCycleReset.Text).Replace("&", "")

            gvCycle.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvCycle.Columns(2).FooterText, gvCycle.Columns(2).HeaderText)
            gvCycle.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvCycle.Columns(3).FooterText, gvCycle.Columns(3).HeaderText)
            gvCycle.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvCycle.Columns(4).FooterText, gvCycle.Columns(4).HeaderText)
            gvCycle.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvCycle.Columns(5).FooterText, gvCycle.Columns(5).HeaderText)
            gvCycle.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvCycle.Columns(6).FooterText, gvCycle.Columns(6).HeaderText)
            gvCycle.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvCycle.Columns(7).FooterText, gvCycle.Columns(7).HeaderText)

            'Staging Setup'
            'Language.setLanguage(mstrModuleName2)
            Me.lblStages.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblStages.ID, Me.lblStages.Text)
            Me.lblSPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            Me.lblStageName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblStageName.ID, Me.lblStageName.Text)
            Me.lblFlowOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblFlowOrder.ID, Me.lblFlowOrder.Text)
            Me.lblPStage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblPStage.ID, Me.lblPStage.Text)

            Me.btnSAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnSAdd.ID, Me.btnSAdd.Text).Replace("&", "")
            Me.btnSReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnSReset.ID, Me.btnSReset.Text).Replace("&", "")

            gvobjStaging.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvobjStaging.Columns(2).FooterText, gvobjStaging.Columns(2).HeaderText)
            gvobjStaging.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvobjStaging.Columns(3).FooterText, gvobjStaging.Columns(3).HeaderText)
            gvobjStaging.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvobjStaging.Columns(4).FooterText, gvobjStaging.Columns(4).HeaderText)

            'QualifyingCriteriaSetup'
            'Language.setLanguage(mstrModuleName3)
            Me.lblQCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblQCriteria.ID, Me.lblQCriteria.Text)
            Me.lblQPeriodMapped.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblQPeriodMapped.ID, Me.lblQPeriodMapped.Text)
            Me.lblInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblInstruction.ID, Me.lblInstruction.Text)
            Me.lblminscore.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblminscore.ID, Me.lblminscore.Text)
            Me.Label2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.Label2.ID, Me.Label2.Text)
            Me.lblQPeriodNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblQPeriodNo.ID, Me.lblQPeriodNo.Text)

            Me.chkPerformanceScore.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkPerformanceScore.ID, Me.chkPerformanceScore.Text)
            Me.chkQMaxAge.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMaxAge.ID, Me.chkQMaxAge.Text)
            Me.chkOrgYearNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkOrgYearNo.ID, Me.chkOrgYearNo.Text)
            Me.chkQMaxScreeners.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMaxScreeners.ID, Me.chkQMaxScreeners.Text)
            Me.chkQMinScrReq.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMinScrReq.ID, Me.chkQMinScrReq.Text)
            Me.chkQTotalQueWeight.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQTotalQueWeight.ID, Me.chkQTotalQueWeight.Text)
            Me.chkAllocationBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkAllocationBy.ID, Me.chkAllocationBy.Text)

            Me.btnQualifySave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.btnQualifySave.ID, Me.btnQualifySave.Text).Replace("&", "")

            Me.rdbanyPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.rdbanyPeriod.ID, Me.rdbanyPeriod.Text)
            Me.rdballPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.rdballPeriod.ID, Me.rdballPeriod.Text)

            'Screeners Setup'
            'Language.setLanguage(mstrModuleName4)
            Me.lblScsSetupTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblScsSetupTitle.ID, Me.lblScsSetupTitle.Text)
            Me.lblScreenerPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblScreenerPeriod.ID, Me.lblScreenerPeriod.Text)
            Me.lblscreenerUser.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblscreenerUser.ID, Me.lblscreenerUser.Text)
            Me.lblScsSetupTitle2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblScsSetupTitle2.ID, Me.lblScsSetupTitle2.Text)

            Me.btnAddScreener.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.btnAddScreener.ID, Me.btnAddScreener.Text).Replace("&", "")

            gvScreener.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), gvScreener.Columns(2).FooterText, gvScreener.Columns(2).HeaderText)
            gvScreener.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), gvScreener.Columns(3).FooterText, gvScreener.Columns(3).HeaderText)

            'Questionnaire Setup'
            'Language.setLanguage(mstrModuleName5)
            Me.lblDefineQuestions.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblDefineQuestions.ID, Me.lblDefineQuestions.Text)
            Me.lblQPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblQPeriod.ID, Me.lblQPeriod.Text)
            Me.lblQWeight.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblQWeight.ID, Me.lblQWeight.Text)
            Me.lblQuestion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblQuestion.ID, Me.lblQuestion.Text)

            Me.btnQuestionAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.btnQuestionAdd.ID, Me.btnQuestionAdd.Text).Replace("&", "")
            Me.btnQuestionReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.btnQuestionReset.ID, Me.btnQuestionReset.Text).Replace("&", "")

            gvobjQuestionnaire.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), gvobjQuestionnaire.Columns(2).FooterText, gvobjQuestionnaire.Columns(2).HeaderText)
            gvobjQuestionnaire.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), gvobjQuestionnaire.Columns(3).FooterText, gvobjQuestionnaire.Columns(3).HeaderText)
            gvobjQuestionnaire.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), gvobjQuestionnaire.Columns(4).FooterText, gvobjQuestionnaire.Columns(4).HeaderText)

            'Ratings Setup'
            'Language.setLanguage(mstrModuleName6)
            Me.lblRSetups.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRSetups.ID, Me.lblRSetups.Text)
            Me.lblRPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRPeriod.ID, Me.lblRPeriod.Text)
            Me.lblScrFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblScrFrom.ID, Me.lblScrFrom.Text)
            Me.lblScrTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblScrTo.ID, Me.lblScrTo.Text)
            Me.lblRActionColor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRActionColor.ID, Me.lblRActionColor.Text)
            Me.lblRAction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRAction.ID, Me.lblRAction.Text)
            Me.lblRDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRDescription.ID, Me.lblRDescription.Text)

            Me.btnRAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.btnRAdd.ID, Me.btnRAdd.Text).Replace("&", "")
            Me.btnRReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.btnRReset.ID, Me.btnRReset.Text).Replace("&", "")

            gvobjRatings.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(2).FooterText, gvobjRatings.Columns(2).HeaderText)
            gvobjRatings.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(3).FooterText, gvobjRatings.Columns(3).HeaderText)
            gvobjRatings.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(4).FooterText, gvobjRatings.Columns(4).HeaderText)
            gvobjRatings.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(5).FooterText, gvobjRatings.Columns(5).HeaderText)
            gvobjRatings.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(6).FooterText, gvobjRatings.Columns(6).HeaderText)
            gvobjRatings.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(7).FooterText, gvobjRatings.Columns(7).HeaderText)



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Sorry, Fincancial year is mandatory information. Please select year to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Sorry, Code is mandatory information. Please enter code to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Sorry, Name is mandatory information. Please enter name to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "Sorry, Start date is mandatory information. Please select start date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 5, "Sorry, End date is mandatory information. Please select end date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 6, "Sorry, End date cannot be less than the start date of talent cycle. Please select proper end date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 7, "Sorry, Start date should be between selected financial year dates. Please select proper end date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 8, "Sorry, End date should be between selected financial year dates. Please select proper end date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 9, "Talent cycle defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 10, "You are about to delete this cycle. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 11, "Talent cycle deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 12, "Sorry you can't delete this cycle,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 13, "Sorry, you cannot delete selected cycle. Reason the selected cycle is already linked with the transactions.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 2, "Sorry, Stage Name is mandatory information. Please enter Stage Name to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 3, "Sorry, Stage is mandatory information. Please select Stage to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 4, "Sorry, You can't Add")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 5, " of")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 6, " Stage")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 7, "Sorry,You can't Add Stage on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 8, "Talent Staging defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 9, "You are about to delete this Stage. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 10, "Sorry, You can not delete this entry, Reason:This is auto generated entry from Talent Cycle Master.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 11, "Talent Stage deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 12, "Sorry you can't Add this Stage,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 13, "Sorry you can't edit this cycle,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 14, "Sorry you can't delete this cycle,Reason: Talent Process is already started for this cycle.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 1, "Sorry, Period is mandatory information. Please select period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 2, "Sorry, Minumum Score value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 3, "Sorry, Please select one option in apply minimum score to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 4, "Sorry, Maximum age value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 5, "Sorry, Number of year with organization should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 6, "Sorry, Maximum number of screener should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 7, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 8, "Sorry, Total question weight should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 9, "Sorry, Please select at lease one allocation to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 10, "Sorry, Please select at lease one allocation to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 11, "Qualification Setting Save successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 12, "Please check atleast one allocation from list to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 13, "Sorry, Minimum number of screening required should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 14, "Sorry you can't change setting for this cycle,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 15, "Please enter Instruction.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 1, "Sorry, User is mandatory information. Please select user to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 2, "Screener Save successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 3, "You are about to delete this screener. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 4, "Screener delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 5, "Sorry, Period is mandatory information. Please select Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 6, "Sorry, Your Total Screeners limit exceeded.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 7, "You are about to inactive this screener. Are you sure you want to inactive?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 8, "You are about to active this screener. Are you sure you want to active?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 9, "Screener Inactivated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 10, "Screener Activated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 11, "Screener deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 12, "Sorry you can't add this screener,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 13, "Sorry you can't delete this screener,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 14, "Sorry you can't inactive this screener,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 15, "Sorry you can't active this screener,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 16, "Sorry you can't add screener,Reason: Talent Setting is not define.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 2, "Sorry, Weight is mandatory information. Please enter Weight to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 3, "Sorry, Question is mandatory information. Please enter Question to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 4, "Talent Questionnaire defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 5, "You are about to delete this Question. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 6, "Talent Questionnaire deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 7, "Sorry, Your Total question weight limit exceeded.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 8, "Sorry you can't add new question,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 9, "Sorry you can't edit this Question,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 10, "Sorry you can't delete this Question,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 11, "Sorry you can't add question,Reason: Talent Setting is not define.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 2, "Sorry, Score From is mandatory information. Please enter Score From to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 3, "Sorry, Score To is mandatory information. Please enter Score To to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 4, "Sorry, Description is mandatory information. Please enter Description to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 5, "Sorry, Action is mandatory information. Please select Action to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 6, "Sorry, Score from  should not be greater than Score To.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 7, "Sorry, Score from  or Score To is already defined Range.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 8, "Sorry, Score from  and Score To is not valid defined Range.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 9, "Talent Ratings defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 10, "You are about to delete this Rating. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 11, "Talent Ratings deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 12, "Rating delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 13, "Sorry you can't add rating,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 14, "Sorry you can't edit this rating,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 15, "Sorry you can't delete this rating,Reason: Talent Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 16, "Sorry you can't add rating,Reason: Talent Setting is not define.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>



End Class
