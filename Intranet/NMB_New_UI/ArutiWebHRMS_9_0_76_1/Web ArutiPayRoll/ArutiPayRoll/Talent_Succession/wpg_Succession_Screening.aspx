﻿<%@ Page Title="Succession Screening" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wpg_Succession_Screening.aspx.vb" Inherits="Talent_Succession_wpg_Succession_Screening" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
  $(document).ready(function () {
                    maintainScrollPosition();
            });

     Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
     function EndRequestHandler(sender, args) {
        maintainScrollPosition();
     }
        
        function maintainScrollPosition() {
            $("#dvScroll").scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
        }
        function setScrollPosition(scrollValue) {
            $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }        
        
    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblheader2" runat="server" Text="Succession Screening"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkOtherScreenerResponse" runat="server" ToolTip="Other Screener Response">
                                <i class="fas fa-users"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="header d--f ai--c jc--sb">
                                            <h2>
                                                <asp:Label ID="lblheader3" runat="server" Text="Employee Detail"></asp:Label>
                                            </h2>
                                            <div class="title">
                                                <asp:Label ID="lblNominatedJobTitle" runat="server" CssClass="tl-nominated-text"
                                                    Text="Nominated For"></asp:Label>
                                                <asp:Label ID="lblNominatedJob" runat="server" CssClass="label label-primary"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="body">
                                            <div class="row clearfix d--f ai--c">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
                                                    <asp:Image ID="imgEmployeeProfilePic" runat="server" Width="150px" Height="150px"
                                                        Style="border-radius: 50%" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="txtName" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblJob" runat="server" Text="Current Role" CssClass="form-label"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="txtJob" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblreportto" runat="server" Text="Line Manager" CssClass="form-label"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="txtreportto" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="divider">
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblLocation" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="txtLocation" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="txtDepartment" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblQualification" runat="server" Text="Qualification" CssClass="form-label"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="txtQualification" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                <asp:Label ID="lbl" runat="server" Text="Summary"></asp:Label>
                                            </h2>
                                        </div>
                                        <div class="body">
                                          <%--  <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblmaxpoint" runat="server" Text="Max Points"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtmaxpoint" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divider">
                                            </div>--%>
                                            <div class="row clearfix d--f ai--c">
                                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                    <asp:Label ID="lblscreenoutcome" runat="server" Text="Screening Outcome"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtscreenoutcome" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    <asp:Label runat="server" ID="lblColor" Visible="false" CssClass="colorbox"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                <asp:Label ID="Label17" runat="server" Text="Screening Questionnaire"></asp:Label>
                                            </h2>
                                            <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                    <asp:LinkButton ID="lnkScreeningInstruction" runat="server" ToolTip="Screening Instruction">
                                                <i class="fas fa-info-circle"></i>
                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="body overflow" style="max-height: 500px" id="dvScroll" onscroll="setScrollPosition(this.scrollTop);">
                                            <asp:DataList ID="rptQuestionnaire" runat="server" Width="100%">
                                                <ItemTemplate>
                                                    <div class="card inner-card m-b-10">
                                                        <div class="body">
                                                            <div class="row clearfix d--f ai--c">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("question") %>' CssClass="form-label"></asp:Label>
                                                                    <asp:HiddenField ID="hfquestionnaireunkid" runat="server" Value='<%# Eval("questionnaireunkid") %>' />
                                                                    <asp:HiddenField ID="hfweight" runat="server" Value='<%# Eval("weight") %>' />
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:RadioButton ID="rdbYes" runat="server" Text="Yes" GroupName="question" />
                                                                    <asp:RadioButton ID="rdbNo" runat="server" Text="No" GroupName="question" />
                                                                </div>
                                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtRemark" placeholder="Enter Your Remark" runat="server" CssClass="form-control"
                                                                                TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                        <div class="footer d--f jc--sb ai--c">
                                            <asp:Button ID="btnPrevious" runat="server" Text="Previous" CssClass="btn btn-default " />
                                            <h4>
                                                <asp:Label ID="lblemployeecount" runat="server" CssClass="form-lable label label-info" />
                                            </h4>
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" Visible="false" />
                                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
            <cc1:ModalPopupExtender ID="popupOtherScreenerResponse" BackgroundCssClass="modal-backdrop"
                TargetControlID="lblPopupheader" runat="server" CancelControlID="btnCloseGrePreviousResponse"
                PopupControlID="pnlGrePreviousResponse">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlGrePreviousResponse" runat="server" CssClass="card modal-dialog modal-lg"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupheader" Text="Previous Screener(s) Response" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 400px">
                    <asp:Panel ID="pnlnoperviousData" runat="server" CssClass="alert alert-danger" Visible="false">
                        <asp:Label ID="lblnoperviousData" Text="Previous Screener(s) Response Not Availabel"
                            runat="server" />
                    </asp:Panel>
                    <asp:DataList ID="rptOtherScreeners" runat="server" Width="100%">
                        <ItemTemplate>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <asp:Panel ID="pnlOtherScreeners" runat="server" aria-multiselectable="true"  CssClass="panel-group">
                                        <div class="panel" style="border: 1px solid #ddd">
                                            <div class="panel-heading" role="tab" id="headingOne_<%# Eval("screenermstunkid") %>">
                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_<%# Eval("screenermstunkid") %>"
                                                        aria-expanded="false" aria-controls="collapseOne_<%# Eval("screenermstunkid") %>"
                                                        style="flex: 1">
                                                        <asp:Label ID="lblOtherScreenerName" CssClass="text-primary" Text='<%# Eval("screener") %>'
                                                            runat="server" />
                                                    </a>
                                                    <asp:Label ID="lblpoint" CssClass="label label-warning m-r-10" Text='<%# Eval("totalpoint") %>'
                                                        runat="server" />
                                                    <asp:Label ID="lblStatus" CssClass="label label-primary" Text='<%# Eval("stage_name") %>'
                                                        runat="server" />
                                                    <asp:HiddenField ID="hfscreenermstunkid" runat="server" Value='<%# Eval("screenermstunkid") %>' />
                                                    <asp:HiddenField ID="hfprocessmstunkid" runat="server" Value='<%# Eval("processmstunkid") %>' />
                                                </h4>
                                            </div>
                                            <div id="collapseOne_<%# Eval("screenermstunkid") %>" class="panel-collapse collapse in"
                                                role="tabpanel" aria-labelledby="headingOne_<%# Eval("screenermstunkid") %>">
                                                <div class="panel-body" style="max-height: 300px">
                                                    <asp:DataList ID="rptOtherScreenerResponse" runat="server" OnItemDataBound="rptOtherScreenerResponse_ItemDataBound"
                                                        Width="100%">
                                                        <ItemTemplate>
                                                            <div class="card inner-card m-b-10">
                                                                <div class="body">
                                                                    <div class="row clearfix d--f ai--c">
                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("question") %>' CssClass="form-label"></asp:Label>
                                                                            <asp:HiddenField ID="hfOtherScreenerResponse" runat="server" Value='<%# Eval("result") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                            <asp:RadioButton ID="rdbYes" runat="server" Text="Yes" Checked="true" GroupName="question"
                                                                                Enabled="false" />
                                                                            <asp:RadioButton ID="rdbNo" runat="server" Text="No" GroupName="question" Enabled="false" />
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtRemark" placeholder="Screener Remark" runat="server" Text='<%# Eval("remark") %>'
                                                                                        CssClass="form-control" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div class="footer">
                    <asp:Button ID="btnCloseGrePreviousResponse" runat="server" CssClass="btn btn-primary"
                        Text="Close" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupScreeningInstruction" runat="server" CancelControlID="btnScreeningInstructionClose"
                PopupControlID="pnlScreeningInstruction" TargetControlID="HiddenField2" Drag="true"
                PopupDragHandleControlID="pnlScreeningInstruction" BackgroundCssClass="modal-backdrop">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlScreeningInstruction" runat="server" CssClass="modal-dialog card"
                Style="display: none;" DefaultButton="btnScreeningInstructionClose">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblScreeningInstruction" runat="server" Text="Screening Instruction" />
                    </h2>
                </div>
                <div class="body" style="height: 200px">
                    <div class="form-group">
                        <div class="form-line">
                            <asp:TextBox ID="txtScreeningInstruction" runat="server" CssClass="form-control"
                                TextMode="MultiLine" Rows="7" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnScreeningInstructionClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                    <asp:HiddenField ID="HiddenField2" runat="server" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
