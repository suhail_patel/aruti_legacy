﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpg_TalentPipeline.aspx.vb"
    Inherits="Talent_Succession_wpg_TalentPipeline" Title="Talent Pipeline" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">


    $("body").on("click", "[id*=chkStagecheckall]", function() {
        var chkHeader = $(this);
        var grid = $(this).closest(".card .inner-card");
        $("[id*=chkSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
    });

    $("body").on("click", "[id*=chkSelect]", function() {
        var grid = $(this).closest(".body");
        var chkHeader = $("[id*=chkAllSelect]", grid);
        if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
            chkHeader.prop("checked", true);
        }
        else {
            chkHeader.prop("checked", false);
        }
    });
        
            $("body").on("click", "[id*=ChkAll]", function() {    
                var chkHeader = $(this);
                $('#<%= dgvPTEmployee.ClientID %> tbody tr:visible td [id*=ChkgvSelect]').prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvSelect]", function() {   
                var grid = $(this).closest(".body");
                var chkHeader = $("[id*=chkAllSelect]", grid);
               
                if ($("[id*=ChkgvSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearchEmp').val().length > 0) {
                    $('#<%= dgvPTEmployee.ClientID %> tbody tr').hide();
                    $('#<%= dgvPTEmployee.ClientID %> tbody tr:first').show();
                    $('#<%= dgvPTEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmp').val() + '\')').parent().show();
                }
                else if ($('#txtSearchEmp').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= dgvPTEmployee.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearchEmp').val('');
                $('#<%= dgvPTEmployee.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchEmp').focus();
            }


    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <h2>
                    <asp:Label ID="lblPageHeader" runat="server" Text="Talent Pipeline"></asp:Label>
                </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader1" runat="server" Text="Filter"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblCycle" Text="Cycle" runat="server" CssClass="form-label" />
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpCycle" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmployee" Text="Employee" runat="server" CssClass="form-label" />
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card ">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblHeader" runat="server" Text="Talent Pipeline"></asp:Label>
                            </h2>
                             <ul class="header-dropdown">
                                <li class="dropdown">
                                    <asp:RadioButton ID="rdbAll" runat="server" Text="All" AutoPostBack="true" Checked="true"
                                        GroupName="filterScreeningData" />
                                </li>
                                <li class="dropdown">
                                    <asp:RadioButton ID="rdbAutoFilter" runat="server" Text="Filtered" GroupName="filterScreeningData"
                                        AutoPostBack="true" />
                                </li>
                                <li class="dropdown">
                                    <asp:RadioButton ID="rdbManual" runat="server" Text="Manual" AutoPostBack="true"
                                        GroupName="filterScreeningData" />
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <asp:DataList ID="reptStage" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                        <HeaderTemplate>
                                            <div class="row clearfix">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                        <div class="card inner-card" style="margin: 0 10px;">
                                                    <div class="header">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <h6>
                                                                    <asp:Label ID="lblStageTitle" runat="server" Text='<%# Eval("StageName") %>'></asp:Label>
                                                                    <asp:HiddenField ID="hfFloworder" runat="server" Value='<%# Eval("floworder") %>' />
                                                                    <asp:HiddenField ID="hfStageid" runat="server" Value='<%# Eval("stageunkid") %>' />
                                                            <asp:Label ID="lblCount" runat="server" CssClass="label label-primary"></asp:Label>
                                                                </h6>
                                                            </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 d--f jc--fs">
                                                        <asp:LinkButton runat="server" ID="lnkAddPotentialTalent" CssClass="m-r-10" OnClick="AddPotentialTalent"
                                                            ToolTip="Add Potential Talent Employee(s)">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton runat="server" ID="lnkStartScreening" Visible="false" CssClass="m-r-10"
                                                            CommandName="Screening" OnClick="StartScreening" ToolTip="Start Screening">
                                                                    <i class="fas fa-play-circle text-warning"></i>
                                                                </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkRemoveScreening" CssClass="m-r-10" OnClick="DeleteScreening"
                                                            ToolTip="Remove Talent from List">
                                                                          <i class="fas fa-user-minus text-danger"></i>
                                                                </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkSetToApproveDisapprove" CssClass="m-r-10" OnClick="ApproveDisapprove"
                                                            ToolTip="Talent Approve/Disapprove">
                                                                          <i class="fas fa-user-check text-info"></i>
                                                                </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkSetToQulified" CssClass="m-r-10" OnClick="SetQulified"
                                                            ToolTip="Remove Talent from the Pool">
                                                                          <i class="fas fa-user-times text-danger"></i>
                                                                </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkSendEmail" CssClass="m-r-10" ToolTip="Send Notification To Screener(s)"
                                                            OnClick="lnkSendEmail_Click">
                                                                          <i class="far fa-envelope text-color-2"></i>
                                                                </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkSendEmailToApprover" CssClass="m-r-10" ToolTip="Send Notification To Approver(s)"
                                                            OnClick="lnkSendEmailToApprover_Click">
                                                                          <i class="far fa-envelope text-color-3"></i>
                                                                </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkSendEmailToEmployee" CssClass="m-r-10" ToolTip="Send Notification To Employee(s)"
                                                            OnClick="lnkSendEmailToEmployee_Click">
                                                                          <i class="far fa-envelope text-color-4"></i>
                                                                </asp:LinkButton>
                                                    </div>
                                                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4 d--f jc--c ai--c">
                                                        <asp:LinkButton runat="server" ID="lnkGetPreviousChunkOfData" Visible="false" CssClass="m-r-10"
                                                            OnClick="lnkLoadChunkOfData" ToolTip="Previous">
                                                                          <i class="fas fa-angle-left"></i>
                                                        </asp:LinkButton>
                                                        <asp:Label ID="lblChunkPageNo" runat="server" Text="1" Visible="false"></asp:Label>
                                                        <asp:LinkButton runat="server" ID="lnkGetNextChunkOfData" Visible="false" CssClass="m-l-10"
                                                            OnClick="lnkLoadChunkOfData" ToolTip="Next">
                                                                          <i class="fas fa-angle-right"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4 d--f jc--fe">
                                                        <asp:CheckBox ID="chkStagecheckall" runat="server" Text=" " CssClass="chk-sm d--f jc--fe f--1" />
                                                            </div>
                                                        </div>
                                                    </div>
                                            <div class="body overflow" style="min-height: 400px; max-height: 400px; overflow-x: hidden">
                                                <asp:DataList ID="reptEmployeeList" runat="server" OnItemDataBound="reptEmployeeList_ItemDataBound"
                                                    Width="100%" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                            <ItemTemplate>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <asp:Panel runat="server" ID="pnlEmployeeBox" CssClass="pipeline-box">
                                                                            <div class="content">
                                                                        <div class="d--f ai--c m-b-10">
                                                                            <div class="m-r-10">
                                                                                        <asp:Image ID="imgEmployeeProfilePic" runat="server" ImageUrl='<%# Eval("empBaseImage") %>'
                                                                                            CssClass="tl-employee-img" />
                                                                                    </div>
                                                                            <div class="fg--1">
                                                                                        <asp:HiddenField ID="hfempid" runat="server" Value='<%# Eval("employeeunkid") %>' />
                                                                                        <asp:HiddenField ID="hfprocessmstunkid" runat="server" Value='<%# Eval("processmstunkid") %>' />
                                                                                        <asp:HiddenField ID="hfisdisapproved" runat="server" Value='<%# Eval("isdisapproved") %>' />
                                                                                <asp:HiddenField ID="hfcolor" runat="server" Value='<%# Eval("color") %>' />
                                                                                <asp:HiddenField ID="hfisDone" runat="server" Value='<%# Eval("IsDone") %>' />
                                                                                <asp:HiddenField ID="hfratingdesc" runat="server" Value='<%# Eval("ratingdesc") %>' />
                                                                                <asp:Label ID="lblEmployee" runat="server" Text='<%# Eval("employeename") %>' CssClass="tl-employee-text"></asp:Label>
                                                                                    </div>
                                                                            </div>
                                                                        <div class="d--f ai--c m-b-10">
                                                                            <div class="fg--1">
                                                                                <asp:Label ID="lbljobname" runat="server" Text='<%# Eval("job_name") %>' CssClass="tl-employee-text"></asp:Label>
                                                                                    </div>
                                                                            <div class="m-l-10" style="width: 20px">
                                                                                <asp:Label runat="server" ID="lblColor" Visible="false" CssClass="colorbox-sm"></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                        <div class="d--f ai--c m-b-10">
                                                                            <div class="fg--1">
                                                                                <asp:Label ID="lbldepartment" runat="server" ToolTip='<%# Eval("department") %>'
                                                                                    Text='<%# Eval("department") %>' CssClass="tl-nominated-text text-color-4"></asp:Label>
                                                                                    </div>
                                                                            <div class="m-l-10">
                                                                                        <asp:CheckBox ID="chkSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                                                    </div>
                                                                                </div>
                                                                        <div class="footer d--f ai--c">
                                                                            <div class="text-left fg--1">
                                                                                <asp:Label ID="lnkisDone" runat="server" ToolTip="Screening Done" Text='<i class="fas fa-clipboard-check text-success"></i>'
                                                                                    Visible="false"></asp:Label>
                                                                                <asp:Label ID="lnkisdisapproved" runat="server" Visible="false" ToolTip="Disapproved"
                                                                                    Text='<i class="fas fa-user-slash text-danger"></i>'></asp:Label>
                                                                            </div>
                                                                            <div class="text-right fg--1 fs--1">
                                                                                <asp:LinkButton runat="server" ID="lnkViewScreeningInfo" ToolTip="View Screening Info"
                                                                                    OnClick="ViewEmployeeScreeningDetail" CssClass="m-l-5">
                                                                                <i class="fas fa-info-circle"></i>
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton runat="server" ID="lnkDeletePTEmployee" Visible="false" ToolTip="Delete Potential Talent Employee"
                                                                                    OnClick="RemoveManuallyAddedEmployee" CssClass="m-l-5">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                </asp:LinkButton>
                                                                                <asp:HiddenField ID="hfpotentialtalenttranunkid" runat="server" Value='<%# Eval("potentialtalenttranunkid") %>' />
                                                                                <asp:HiddenField ID="hfIsManual" runat="server" Value='<%# Eval("IsManual") %>' />
                                                                            </div>
                                                                        </div>
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                </asp:DataList>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                            </asp:DataList>
                                </div>
                            </div>
                        </div>
                    </div>
            <%--            <cc1:ModalPopupExtender ID="popupOtherScreenerResponse" BackgroundCssClass="modal-backdrop"
                TargetControlID="lblPopupheader" runat="server" CancelControlID="hfCloseApproverReject"
                PopupControlID="pnlGrePreviousResponse">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlGrePreviousResponse" runat="server" CssClass="card modal-dialog modal-lg"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupheader" Text="Previous Screener(s) Response" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 400px">
                    <asp:Panel ID="pnlnoperviousData" runat="server" CssClass="alert alert-danger">
                        <asp:Label ID="lblnoperviousData" Text="Previous Screener(s) Response Not Availabel"
                            runat="server" />
                    </asp:Panel>
                    <asp:Repeater ID="rptOtherScreeners" runat="server">
                        <ItemTemplate>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <asp:Panel ID="pnlOtherScreeners" runat="server" aria-multiselectable="true" CssClass="panel-group full-body">
                                        <div class="panel" style="border: 1px solid #ddd">
                                            <div class="panel-heading" role="tab" id="headingOne_<%# Eval("screenermstunkid") %>">
                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_<%# Eval("screenermstunkid") %>"
                                                        aria-expanded="false" aria-controls="collapseOne_<%# Eval("screenermstunkid") %>"
                                                        style="flex: 1">
                                                        <asp:Label ID="lblOtherScreenerName" CssClass="text-primary" Text='<%# Eval("screener") %>'
                                                            runat="server" />
                                                    </a>
                                                    <asp:Label ID="lblStatus" CssClass="label label-primary" Text='<%# Eval("stage_name") %>'
                                                        runat="server" />
                                                    <asp:HiddenField ID="hfscreenermstunkid" runat="server" Value='<%# Eval("screenermstunkid") %>' />
                                                    <asp:HiddenField ID="hfprocessmstunkid" runat="server" Value='<%# Eval("processmstunkid") %>' />
                                                </h4>
                                            </div>
                                            <div id="collapseOne_<%# Eval("screenermstunkid") %>" class="panel-collapse collapse in"
                                                role="tabpanel" aria-labelledby="headingOne_<%# Eval("screenermstunkid") %>">
                                                <div class="panel-body" style="max-height: 300px">
                                                    <asp:Repeater ID="rptOtherScreenerResponse" runat="server" OnItemDataBound="rptOtherScreenerResponse_ItemDataBound">
                                                        <ItemTemplate>
                                                            <div class="card inner-card m-b-10">
                                                                <div class="body">
                                                                    <div class="row clearfix d--f ai--c">
                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("question") %>' CssClass="form-label"></asp:Label>
                                                                            <asp:HiddenField ID="hfOtherScreenerResponse" runat="server" Value='<%# Eval("result") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                            <asp:RadioButton ID="rdbYes" runat="server" Text="Yes" Checked="true" GroupName="question"
                                                                                Enabled="false" />
                                                                            <asp:RadioButton ID="rdbNo" runat="server" Text="No" GroupName="question" Enabled="false" />
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtRemark" placeholder="Enter Your Remark" runat="server" Text='<%# Eval("remark") %>'
                                                                                        CssClass="form-control" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="form-group">
                        <div class="form-line">
                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine"
                                Rows="3"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="footer d--f jc--sb ai--c">
                    <asp:HiddenField ID="hfCloseApproverReject" runat="server" />
                    <asp:Button ID="btnApprove" CssClass="btn btn-primary" runat="server" Text="Approve" />
                    <h4>
                        <asp:Label ID="lblemployeecount" runat="server" CssClass="form-lable label label-info" />
                    </h4>
                    <asp:Button ID="btnReject" CssClass="btn btn-danger pull-left" runat="server" Text="Reject" />
                </div>
            </asp:Panel>
--%>
            <cc1:ModalPopupExtender ID="popupAddPotentialTelentEmployee"
                TargetControlID="hfAddPotentialTelant" runat="server" PopupControlID="pnlAddPotentialTelentEmployee" BackgroundCssClass="modal-backdrop bd-l2">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddPotentialTelentEmployee" runat="server" CssClass="card modal-dialog"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupAddPotentialTelentEmployeeheader" Text="Add Potential Telent Employee(s)"
                            runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchEmp" name="txtSearch" placeholder="Type To Search Text"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvPTEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="employeeunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                            <HeaderTemplate>
                                                <%--<asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAll_CheckedChanged" Text=" " />--%>
                                                <asp:CheckBox ID="ChkAll" runat="server" Text=" " CssClass="chk-sm" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsCheck")) %>' OnCheckedChanged="ChkgvSelect_CheckedChanged" Text=" " />--%>
                                                <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhEName" />
                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                            FooterText="colhEmpId" Visible="false" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSaveAddPotentialTelant" runat="server" CssClass="btn btn-primary"
                        Text="Save" />
                    <asp:Button ID="btnCloseAddPotentialTelant" runat="server" CssClass="btn btn-default"
                        Text="Close" />
                    <asp:HiddenField ID="hfAddPotentialTelant" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupEmployeeScreeningDetailList" runat="server" PopupControlID="pnlEmployeeScreeningDetailList"
                TargetControlID="hfEmployeeScreeningDetailList" Drag="true" PopupDragHandleControlID="pnlEmployeeScreeningDetailList"
                BackgroundCssClass="modal-backdrop bd-l2">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlEmployeeScreeningDetailList" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblEmployeeScreeningDetailList" runat="server" Text="Employee Screening Detail" />
                    </h2>
                </div>
                <div class="body" style="max-height: 400px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblNominatedEmployeeName" runat="server" Text="Employee:" CssClass="form-label" />
                            <asp:Label ID="txtScreeningEmployeeName" runat="server" Text="" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 200px">
                                <asp:GridView ID="gvEmployeeScreeningDetailList" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-hover table-bordered" AllowPaging="false" Width="99%">
                                    <Columns>
                                        <asp:BoundField DataField="username" HeaderText="Screener" ReadOnly="True" FooterText="colhUsername">
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Is Screening Done?" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCheckScreeningDoneTrue" runat="server" Text="" Visible="false"
                                                    CssClass="fas fa-check-circle text-success" />
                                                <asp:Label ID="lblCheckScreeningDoneFalse" runat="server" Text="" Visible="false"
                                                    CssClass="fas fa-times-circle text-danger " />
                                                <asp:HiddenField ID="hfIsScreeningPending" runat="server" Value='<%# Eval("IsScreeningPending") %>' />
                                                <asp:HiddenField ID="hfIsScreeningDone" runat="server" Value='<%# Eval("IsScreeningDone") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnEmployeeScreeningDetailListClose" runat="server" Text="Cancel"
                        CssClass="btn btn-primary" />
                    <asp:HiddenField ID="hfEmployeeScreeningDetailList" runat="server" />
                </div>
            </asp:Panel>
            <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are ou Sure You Want To delete?:" />
            <uc9:ConfirmYesNo ID="popupconfirmSendEmailInProgress" runat="server" Title="Confirmation"
                        Message="You are trying to send an email notification for screening employee(s) who falls under Talent Potential stage. we recommend you to send notification once all employee(s) are in Talent Potential stage. Do you still want to send notification?" />
             <uc9:ConfirmYesNo ID="popupconfirmSendEmailProcessDone" runat="server" Title="Confirmation"
                        Message=" Are Sure to send notification to Screeners ?" />
              <uc9:ConfirmYesNo ID="popupconfirmSendEmailApprovedEmployee" runat="server" Title="Confirmation"
                        Message="You are about to send an email notification to the employee(s) approved to be in the Talents Pool but some are still on the Talent Screening or Approval Stage. We recommend you send this notification once everyone is processed. Do you still want to send notification?" />
              <uc9:ConfirmYesNo ID="popupconfirmSendEmailApprover" runat="server" Title="Confirmation"
                        Message="You are trying to send an email notification for approving/disapproving employee(s) who falls under qualified stage. As there some emplyoee are still under process and some are under potential stage, we recommend you to send notification once all employee(s) process is done. Do you still want to send notification?" />         
              <uc9:ConfirmYesNo ID="cnfConfirmDeleteReasonPT" runat="server" Title="Aruti" />
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
