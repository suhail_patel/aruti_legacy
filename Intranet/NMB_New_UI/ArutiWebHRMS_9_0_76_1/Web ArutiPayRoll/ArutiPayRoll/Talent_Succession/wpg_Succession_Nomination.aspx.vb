﻿Imports Aruti.Data
Imports System.Data

Partial Class Talent_Succession_wpg_Succession_Nomination
    Inherits Basepage

#Region "Page Event(s)"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Page.IsPostBack = False) Then

                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Dim objsucsettings_master As New clssucsettings_master
                    Dim mdicSetting As Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String) = objsucsettings_master.GetSettingFromPeriod()
                    If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry you can't do nomination,Reason: Succession Setting is not define."), Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Talent_Succession/wPg_SuccessionSettings.aspx")
                        Exit Sub
                    End If

                    Call FillCombo()
                    Call cboEmployee_SelectedIndexChanged(sender, e)
                    btnNominateEmployeeSave.Visible = CBool(Session("AllowToNominateEmployeeForSuccession"))
                End If
            Else
                If IsNothing(ViewState("mintNominationunkid")) = False Then
                    mintNominationunkid = CInt(ViewState("mintNominationunkid"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintNominationunkid") = mintNominationunkid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Private Variables(s)"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmployeeNominationMaster"
    Private mintNominationunkid As Integer = -1
    Private blnpopupNominateEmployee As Boolean = False
#End Region

#Region "Private Function(s) & Method(s)"

    Private Sub FillCombo()

        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objjob As New clsJobs
        Dim objSucsettings_master As New clssucsettings_master

        Try

            Dim mblnIncludeUserAccess As Boolean = CBool(objSucsettings_master.GetSettingValueFromKey(clssucsettings_master.enSuccessionConfiguration.IS_USERACESS_USE_FOR_NOMINATION))

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True, , , , , , , , , , , , , , , , , mblnIncludeUserAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Employee")
                .DataBind()
                .SelectedValue = CStr(0)
            End With


            dsCombos = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With drpNominateEmployeeJob
                .DataSource = dsCombos.Tables("keyJob")
                .DataTextField = "name"
                .DataValueField = "jobunkid"
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objjob = Nothing
        End Try
    End Sub

    Private Sub SetNominationValue(ByVal objEmployee_nomination_master As clsEmployee_nomination_master)
        Try
            objEmployee_nomination_master._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee_nomination_master._Jobunkid = CInt(drpNominateEmployeeJob.SelectedValue)
            objEmployee_nomination_master._Isvoid = False
            objEmployee_nomination_master._Ismatch = True
            SetATNominationValue(objEmployee_nomination_master)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetATNominationValue(ByVal objEmployee_nomination_master As clsEmployee_nomination_master)
        Try
            objEmployee_nomination_master._AuditUserId = CInt(Session("UserId"))
            objEmployee_nomination_master._ClientIP = CStr(Session("IP_ADD"))
            objEmployee_nomination_master._FormName = mstrModuleName
            objEmployee_nomination_master._FromWeb = True
            objEmployee_nomination_master._HostName = CStr(Session("HOST_NAME"))
            objEmployee_nomination_master._DatabaseName = CStr(Session("Database_Name"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillNomination()
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee_nomination_master.GetList("NominationList", CInt(cboEmployee.SelectedValue))
            dgvNominateEmployee.DataSource = dsList.Tables("NominationList")
            dgvNominateEmployee.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee_nomination_master = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Method(s)"
    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                FillNomination()
            Else
                dgvNominateEmployee.DataSource = Nothing
                dgvNominateEmployee.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region "Button Event(s)"

    Protected Sub btnNominateEmployeeSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominateEmployeeSave.Click
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master
        Dim blnFlag As Boolean = False
        Dim objsucsetting As New clssucsettings_master
        Dim dslist As New DataSet


        Try

            If CInt(drpNominateEmployeeJob.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Job is mandatory information. Please select job to continue."), Me)
                Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Empoloyee is mandatory information. Please select empoloyee to continue."), Me)
                Exit Sub
            End If


            Dim mdicSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)
            mdicSetting = objsucsetting.GetSettingFromPeriod()
            Dim mblnIncludeUserAccess As Boolean = CBool(objsucsetting.GetSettingValueFromKey(clssucsettings_master.enSuccessionConfiguration.IS_USERACESS_USE_FOR_NOMINATION))

            If IsNothing(mdicSetting) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry,Succession Setting Not Available."), Me)
                Exit Sub
            End If

            'Gajanan [26-Feb-2021] -- Start
            dslist = objEmployee_nomination_master.GetList("NominationList", -1, True, False, CInt(drpNominateEmployeeJob.SelectedValue))
            If IsNothing(dslist) = False Then
                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION) Then
                    If CInt(dslist.Tables("NominationList").Rows.Count) >= CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry,you can't nominated this employee for this job, Reason: maximum number of nomination already done for perticular job."), Me)
                        Dim objJobs As New clsJobs
                        objJobs._Jobunkid = (drpNominateEmployeeJob.SelectedValue)

                        dslist = objEmployee_nomination_master.GetNominatedEmployeeList("MaxNominateEmployeeList", _
                                                                                         CStr(Session("Database_Name")), _
                                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, -1, CInt(drpNominateEmployeeJob.SelectedValue))
                        txtNominatedJob.Text = objJobs._Job_Name
                        gvNominatedEmployeeList.DataSource = dslist.Tables("MaxNominateEmployeeList")
                        gvNominatedEmployeeList.DataBind()

                        popupNominatedEmployeeList.Show()
                        Exit Sub
                    End If
                End If
            End If
            'Gajanan [26-Feb-2021] -- End

            Dim blnQualification_level As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL) Then
                blnQualification_level = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              CStr(Session("UserAccessModeSetting")), _
                                              True, CBool(Session("IsIncludeInactiveEmp")), _
                                              CInt(cboEmployee.SelectedValue), _
                                              clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              "Emp", False, , , mblnIncludeUserAccess)

            Else
                blnQualification_level = True
            End If

            Dim blnPerf_score As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE) Then
                blnPerf_score = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, CBool(Session("IsIncludeInactiveEmp")), _
                                                         CInt(cboEmployee.SelectedValue), _
                                                         clssucsettings_master.enSuccessionConfiguration.PERF_SCORE, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         "Emp", False, , , mblnIncludeUserAccess)
            Else
                blnPerf_score = True
            End If

            Dim blnExp_year_no As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) Then

                blnExp_year_no = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, CBool(Session("IsIncludeInactiveEmp")), _
                                                         CInt(cboEmployee.SelectedValue), _
                                                         clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         "Emp", False, , , mblnIncludeUserAccess)

            Else
                blnExp_year_no = True
            End If

            Dim blnMinAge As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) Then

                blnMinAge = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, CBool(Session("IsIncludeInactiveEmp")), _
                                                         CInt(cboEmployee.SelectedValue), _
                                                         clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         "Emp", False, , , mblnIncludeUserAccess)

            Else
                blnMinAge = True
            End If


            If blnQualification_level = False OrElse blnPerf_score = False OrElse blnExp_year_no = False OrElse blnMinAge = False Then

                If blnQualification_level Then
                    lblQualificationNominationCheckTrue.Visible = True
                Else
                    lblQualificationNominationCheckFalse.Visible = True
                End If

                If blnPerf_score Then
                    lblPerfNominationCheckTrue.Visible = True
                Else
                    lblPerfNominationCheckFalse.Visible = True
                End If



                If blnExp_year_no Then
                    lblExpNominationCheckTrue.Visible = True
                Else
                    lblExpNominationCheckFalse.Visible = True
                End If

                If blnMinAge Then
                    lblMinimumAgeCheckTrue.Visible = True
                Else
                    lblMinimumAgeCheckFalse.Visible = True
                End If
                popupNominationCancleReason.Show()
                Exit Sub
            End If



            SetNominationValue(objEmployee_nomination_master)

            blnFlag = objEmployee_nomination_master.Insert(Nothing)

            If blnFlag = False AndAlso objEmployee_nomination_master._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objEmployee_nomination_master._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Employee nominated successfully for selected job."), Me)
                FillNomination()
            End If

            objEmployee_nomination_master.Insert()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee_nomination_master = Nothing
            objsucsetting = Nothing
        End Try
    End Sub

    Protected Sub btnNominateEmployeeClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominateEmployeeClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNominationCancleReasonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominationCancleReasonClose.Click
        Try
            lblQualificationNominationCheckFalse.Visible = False
            lblExpNominationCheckFalse.Visible = False
            lblMinimumAgeCheckFalse.Visible = False
            lblPerfNominationCheckFalse.Visible = False

            lblQualificationNominationCheckTrue.Visible = False
            lblExpNominationCheckTrue.Visible = False
            lblMinimumAgeCheckTrue.Visible = False
            lblPerfNominationCheckTrue.Visible = False

            popupNominationCancleReason.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkNominateEmployeeDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintNominationunkid = CInt(dgvNominateEmployee.DataKeys(row.RowIndex)("nominationunkid"))
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "If you delete this nomination, Your succession transection will also delete. Are you sure you want to delete?")
            popup_YesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Delete reason is mandatory information. Please select delete reason to continue")
            delReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Dim objEmployee_nomination_master As New clsEmployee_nomination_master

            objEmployee_nomination_master._Nominationunkid = mintNominationunkid
            objEmployee_nomination_master._Isvoid = True
            objEmployee_nomination_master._Voidreason = delReason.Reason
            objEmployee_nomination_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEmployee_nomination_master._Voiduserunkid = CInt(Session("UserId"))

            SetATNominationValue(objEmployee_nomination_master)


            blnFlag = objEmployee_nomination_master.Delete(mintNominationunkid)
            If blnFlag = False AndAlso objEmployee_nomination_master._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objEmployee_nomination_master._Message, Me)
                mintNominationunkid = -1
                FillNomination()
            Else
                mintNominationunkid = -1
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Screener deleted successfully."), Me)
                FillNomination()
            End If
            objEmployee_nomination_master = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNominatedEmployeeListClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominatedEmployeeListClose.Click
        Try
            popupNominatedEmployeeList.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview Event(s)"
    Protected Sub dgvNominateEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvNominateEmployee.RowDataBound
        Try
            If e.Row.RowIndex > -1 Then
                Dim lnkNominateEmployeeDelete As LinkButton = CType(e.Row.FindControl("lnkNominateEmployeeDelete"), LinkButton)
                lnkNominateEmployeeDelete.Visible = CBool(Session("AllowToNominateEmployeeForSuccession"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNominateEmployeeHeader.ID, Me.lblNominateEmployeeHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNominateEmployeeJob.ID, Me.lblNominateEmployeeJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNominateEmployeeSave.ID, Me.btnNominateEmployeeSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNominateEmployeeClose.ID, Me.btnNominateEmployeeClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvNominateEmployee.Columns(1).FooterText, dgvNominateEmployee.Columns(1).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblNominateEmployeeHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNominateEmployeeHeader.ID, Me.lblNominateEmployeeHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblNominateEmployeeJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNominateEmployeeJob.ID, Me.lblNominateEmployeeJob.Text)
            btnNominateEmployeeSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNominateEmployeeSave.ID, Me.btnNominateEmployeeSave.Text)
            btnNominateEmployeeClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNominateEmployeeClose.ID, Me.btnNominateEmployeeClose.Text)
            dgvNominateEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvNominateEmployee.Columns(1).FooterText, dgvNominateEmployee.Columns(1).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Job is mandatory information. Please select job to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, Empoloyee is mandatory information. Please select empoloyee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry,Succession Setting Not Available.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry,you can't nominated this employee for this job, Reason: maximum number of nomination already done for perticular job.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Employee nominated successfully for selected job.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "If you delete this nomination, Your succession transection will also delete. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, Delete reason is mandatory information. Please select delete reason to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Screener deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry you can't do nomination,Reason: Succession Setting is not define.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
