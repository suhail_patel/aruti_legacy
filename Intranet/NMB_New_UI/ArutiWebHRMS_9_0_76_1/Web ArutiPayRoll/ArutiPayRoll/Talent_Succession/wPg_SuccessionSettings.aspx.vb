﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class Talent_Succession_wPg_SuccessionSettings
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSuccessionSettingsAddEdit"
    Private ReadOnly mstrModuleName2 As String = "frmSuccessionStagingAddEdit"
    Private ReadOnly mstrModuleName3 As String = "frmSuccessionQualifyingCriteriaSetup"
    Private ReadOnly mstrModuleName4 As String = "frmSuccessionScreenersSetup"
    Private ReadOnly mstrModuleName5 As String = "frmSuccessionQuestionnaireAddEdit"
    Private ReadOnly mstrModuleName6 As String = "frmSuccessionRatingsAddEdit"
    Private DisplayMessage As New CommonCodes
    Private mintStagingUnkid As Integer = 0
    Private mintQuestionnaireUnkid As Integer = 0
    Private mintRatingsUnkid As Integer = 0
    Private mintScreenerUnkid As Integer = 0
    Private mstrDeleteAction As String = ""
    Private mStagingActionMode As enAction
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                FillStageCombo()
                FillStaging()
                FillComboForQuestionnaire()
                FillQuestionnaire()
                FillComboForRatings()
                FillRatings()
                FillScreenersCombo()
                FillScreener()
                FillQualifyingCombo()
                FillQualifyingSetting()


                btnSAdd.Visible = CBool(Session("AllowToModifySuccessionSetting"))
                pnlQualifyFooter.Visible = CBool(Session("AllowToModifySuccessionSetting"))
                btnAddScreener.Visible = CBool(Session("AllowToModifySuccessionSetting"))
                btnQuestionAdd.Visible = CBool(Session("AllowToModifySuccessionSetting"))
                btnRAdd.Visible = CBool(Session("AllowToModifySuccessionSetting"))
            Else
                mintScreenerUnkid = CInt(Me.ViewState("mintScreenerUnkid"))
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintStagingUnkid = CInt(Me.ViewState("mintStagingUnkid"))
                mintQuestionnaireUnkid = CInt(Me.ViewState("mintQuestionnaireUnkid"))
                mintRatingsUnkid = CInt(Me.ViewState("mintRatingsUnkid"))
                mStagingActionMode = CType(Me.ViewState("StagingAction"), enAction)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintScreenerUnkid") = mintScreenerUnkid
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintStagingUnkid") = mintStagingUnkid
            Me.ViewState("mintQuestionnaireUnkid") = mintQuestionnaireUnkid
            Me.ViewState("mintRatingsUnkid") = mintRatingsUnkid
            Me.ViewState("StagingAction") = mStagingActionMode
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Staging Setup "

#Region " Private Method(s)"

    Private Sub FillStageCombo()
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Dim objStg As New clssucstages_master
        'Dim obCyl As New clssuccycle_master
        Try

            dsCombo = objStg.getComboList("List", True)
            With drpPStage
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objStg.FlowOrder("List", True)
            With drpFlow
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            objStg = Nothing
            'obCyl = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub ClearStagingCtrls()
        Try
            mintStagingUnkid = 0
            txtStageName.Text = ""
            drpFlow.SelectedValue = CStr(0)
            drpPStage.SelectedValue = CStr(0)
            mStagingActionMode = enAction.ADD_ONE
            drpFlow.Enabled = True
            drpPStage.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillStaging()
        Dim objStaging As New clssucstages_master
        Dim dsList As New DataSet
        Try
            dsList = objStaging.GetList("List")
            gvobjStaging.DataSource = dsList.Tables(0)
            gvobjStaging.DataBind()

            gvobjStaging.Columns(0).Visible = CBool(Session("AllowToModifySuccessionSetting"))
            gvobjStaging.Columns(1).Visible = CBool(Session("AllowToModifySuccessionSetting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaging = Nothing
        End Try
    End Sub

    Private Function IsValidStaging() As Boolean

        Dim objstage As New clssucstages_master
        Dim intMinFlowOrder As Integer
        Dim intMaxFlowOrder As Integer
        Dim intMaxToLastFlowOrder As Integer

        Dim objsucscreening_process_master As New clssucscreening_process_master

        Try

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 12, "Sorry you can't Add this Stage,Reason: Succession Process is already started for this cycle."), Me)
                Exit Function
            End If


            If txtStageName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 2, "Sorry, Stage Name is mandatory information. Please enter Stage Name to continue."), Me)
                Return False
            End If


            If mStagingActionMode <> enAction.EDIT_ONE AndAlso CInt(drpPStage.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 3, "Sorry, Stage is mandatory information. Please select Stage to continue."), Me)
                Return False
            End If



            objstage._Stageunkid = CInt(drpPStage.SelectedValue)

            objstage.Get_Min_Max_FlowOrder(intMinFlowOrder, intMaxFlowOrder, intMaxToLastFlowOrder)

            If (intMinFlowOrder = objstage._Floworder AndAlso CInt(drpFlow.SelectedValue) = 1) OrElse _
            (intMaxFlowOrder = objstage._Floworder AndAlso CInt(drpFlow.SelectedValue) = 2) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 4, "Sorry, You can't Add ") & drpFlow.SelectedItem.Text & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 5, " of ") & drpPStage.SelectedItem.Text & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 6, " Stage "), Me)
                Return False
            End If

            If (intMinFlowOrder = objstage._Floworder AndAlso CInt(drpFlow.SelectedValue) = 0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 7, "Sorry,You can't Add Stage on ") & drpPStage.SelectedItem.Text & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 6, " Stage "), Me)
                Return False
            End If



            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objstage = Nothing
            objsucscreening_process_master = Nothing
        End Try
    End Function

    Private Sub SetStagingValue(ByRef objStaging As clssucstages_master)
        Try
            objStaging._Stageunkid = CInt(drpPStage.SelectedValue)
            objStaging._ApplyOnFloworder = objStaging._Floworder
            objStaging._ApplyOnStageunkid = CInt(drpPStage.SelectedValue)
            objStaging._Stageunkid = mintStagingUnkid
            objStaging._Stage_Name = txtStageName.Text
            If mStagingActionMode = enAction.EDIT_ONE Then
                'objStaging._Floworder = CInt(drpFlow.SelectedValue)
                'objStaging._Isdefault = False
            Else
                objStaging._Floworder = CInt(drpFlow.SelectedValue)
                objStaging._Isdefault = False
            End If

            objStaging._Isactive = True
            objStaging._AuditUserId = CInt(Session("UserId"))
            objStaging._ClientIP = CStr(Session("IP_ADD"))
            objStaging._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objStaging._DatabaseName = CStr(Session("Database_Name"))
            objStaging._FormName = mstrModuleName2
            objStaging._FromWeb = True
            objStaging._HostName = CStr(Session("HOST_NAME"))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetStagingValue(ByVal intStagingId As Integer)
        Dim objStaging As New clssucstages_master
        Try
            objStaging._Stageunkid = mintStagingUnkid
            txtStageName.Text = objStaging._Stage_Name


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaging = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Events "

    Protected Sub btnSAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSAdd.Click
        Dim objStaging As New clssucstages_master
        Dim blnFlag As Boolean = False
        Dim dsGetList As DataSet = Nothing
        Try
            If IsValidStaging() = False Then
                Exit Sub
            End If
            SetStagingValue(objStaging)

            dsGetList = objStaging.GetList("List")

            If mintStagingUnkid > 0 Then
                blnFlag = objStaging.Update()
            Else
                blnFlag = objStaging.Insert(, dsGetList.Tables(0))
            End If

            If blnFlag = False AndAlso objStaging._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objStaging._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 8, "Succession Staging defined successfully."), Me)
                FillStaging()
                ClearStagingCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaging = Nothing
            dsGetList = Nothing
        End Try
    End Sub

    Protected Sub btnSReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSReset.Click
        Try
            Call ClearStagingCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkSEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 13, "Sorry you can't edit this cycle,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintStagingUnkid = CInt(gvobjStaging.DataKeys(row.RowIndex)("stageunkid"))

            GetStagingValue(mintStagingUnkid)
            mStagingActionMode = enAction.EDIT_ONE
            drpFlow.Enabled = False
            drpPStage.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkSDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 14, "Sorry you can't delete this cycle,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintStagingUnkid = CInt(gvobjStaging.DataKeys(row.RowIndex)("stageunkid"))
            mstrDeleteAction = "delsta"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 9, "You are about to delete this Stage. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

#End Region

#End Region

#Region " Qualifying Criteria Setup "

#Region " Private Methods "
    Private Sub FillQualifyingCombo()
        Dim objMst As New clsMasterData
        'Dim obCyl As New clssuccycle_master
        Dim dsCombo As DataSet = Nothing
        Try


            dsCombo = objMst.GetEAllocation_Notification("Allocation")
            Dim dr As DataRow = dsCombo.Tables("Allocation").NewRow
            dr.Item("Id") = 99999
            dr.Item("NAME") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 7, "Select")
            dsCombo.Tables("Allocation").Rows.InsertAt(dr, 0)

            With drpQAllocBy
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Allocation")
                .DataBind()
                .SelectedValue = "99999"
            End With

            dsCombo = objMst.GetCondition(True, True, True, False, False)
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), " ID IN (" & enComparison_Operator.LESS_THAN & "," & enComparison_Operator.LESS_THAN_EQUAL & "," & enComparison_Operator.GREATER_THAN & "," & enComparison_Operator.GREATER_THAN_EQUAL & "," & enComparison_Operator.EQUAL & ") ", "", DataViewRowState.CurrentRows).ToTable
            With drpQualificationlevel
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = CStr(CInt(enComparison_Operator.EQUAL))
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            'obCyl = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearQualifyingCtrls()
        Try
            txtInstruction.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidQualifying() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try

            'If objsucscreening_process_master.IsSuccessionStarted() Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName3, 14, "Sorry you can't change setting for this cycle,Reason: Succession Process is already started for this cycle."), Me)
            '    Return False
            'End If

            If chkPerformanceScore.Checked Then
                If txtMinPSValue.Enabled AndAlso CInt(txtMinPSValue.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 2, "Sorry, Minumum Score value should be greater than 0."), Me)
                    Return False
                End If

                If rdballPeriod.Checked = False AndAlso rdbanyPeriod.Checked = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 3, "Sorry, Please select one option in apply minimum score to continue."), Me)
                    Return False
                End If
            End If
            'Gajanan [26-Feb-2021] -- Start
            If chkQMaxAge.Checked AndAlso CInt(txtQMaxAgeNo.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 4, "Sorry, Maximum age value should be greater than 0."), Me)
                Return False
            End If
            'Gajanan [26-Feb-2021] -- End

            If chkOrgYearNo.Checked AndAlso CInt(txtQOrgYrNo.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 5, "Sorry, Number of year with organization should be greater than 0."), Me)
                Return False
            End If

            If chkQMaxScreeners.Checked AndAlso CInt(txtMaxQScreener.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 6, "Sorry, Maximum number of screener should be greater than 0."), Me)
                Return False
            End If

            If chkQMinScrReq.Checked AndAlso CInt(txtQMinScrReq.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 13, "Sorry, Minimum number of screening required should be greater than 0."), Me)
                Return False
            End If

            'Gajanan [26-Feb-2021] -- Start
            If chkQMaxNomination.Checked AndAlso CInt(txtQMaxNomination.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 17, "Sorry, Maximum number of nomination required should be greater than 0."), Me)
                Return False
            End If
            'Gajanan [26-Feb-2021] -- End


            If chkQTotalQueWeight.Checked AndAlso CInt(txtQTotalQueWeight.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 8, "Sorry, Total question weight should be greater than 0."), Me)
                Return False
            End If


            If chkQualificationlevel.Checked AndAlso CInt(txtQualificationlevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 16, "Sorry, Qualification level should be greater than 0."), Me)
                Return False
            End If

            If chkAllocationBy.Checked AndAlso CInt(drpQAllocBy.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 9, "Sorry, Please select at lease one allocation to continue."), Me)
                Return False
            End If


            If chkAllocationBy.Checked AndAlso CInt(drpQAllocBy.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 10, "Sorry, Please select at lease one allocation to continue."), Me)
                Return False
            End If

            If chkAllocationBy.Checked Then
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                Dim lstIDs As List(Of String) = Nothing
                Dim allocation_Id As String = ""

                gRow = gvQAllocBy.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)
                If gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 12, "Please check atleast one allocation from list to continue."), Me.Page)
                    Exit Function
                End If
            End If

            If txtInstruction.Text.Trim = "" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 15, "Please enter Instruction."), Me)
                txtInstruction.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
            objsucscreening_process_master = Nothing
        End Try
    End Function

    Private Sub FillQualifyingSetting()
        Try
            Dim objsucsettings_master As New clssucsettings_master
            Dim blnFlag As Boolean = False
            Try
                Dim SuccessionSetting As Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String) = objsucsettings_master.GetSettingFromPeriod()
                If IsNothing(SuccessionSetting) = False Then
                    For Each kvp As KeyValuePair(Of clssucsettings_master.enSuccessionConfiguration, String) In SuccessionSetting
                        Select Case kvp.Key
                            Case clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL
                                chkQualificationlevel.Checked = CBool(kvp.Value)
                                chkQualificationlevel_CheckedChanged(Nothing, Nothing)
                                txtQualificationlevel.SelectedValue = kvp.Value


                            Case clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION
                                drpQualificationlevel.SelectedValue = kvp.Value

                            Case clssucsettings_master.enSuccessionConfiguration.ALL_PERIOD
                                chkPerformanceScore.Checked = CBool(kvp.Value)
                                chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                rdballPeriod.Checked = CBool(kvp.Value)

                            Case clssucsettings_master.enSuccessionConfiguration.ANY_PERIOD
                                chkPerformanceScore.Checked = CBool(kvp.Value)
                                chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                rdbanyPeriod.Checked = CBool(kvp.Value)

                            Case clssucsettings_master.enSuccessionConfiguration.ALLOC_TYPE
                                Dim allocation As String = kvp.Value.ToString()
                                Dim allocationType As String() = allocation.Split(CChar("|"))

                                If allocationType(0).Length > 0 Then
                                    chkAllocationBy.Checked = True
                                    chkAllocationBy_CheckedChanged(Nothing, Nothing)
                                    drpQAllocBy.SelectedValue = allocationType(0)
                                    drpQAllocBy_SelectedIndexChanged(Nothing, Nothing)
                                    If IsNothing(gvQAllocBy.DataSource) = False Then
                                        For Each row As GridViewRow In gvQAllocBy.Rows
                                            Dim chk As CheckBox = CType(row.FindControl("chkSelect"), CheckBox)
                                            If allocationType(1).Contains(gvQAllocBy.DataKeys(row.RowIndex)("Id").ToString()) Then
                                                chk.Checked = True
                                            End If
                                        Next
                                    End If
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO
                                If CInt(kvp.Value) > 0 Then
                                    chkOrgYearNo.Checked = True
                                    chkOrgYearNo_CheckedChanged(Nothing, Nothing)
                                    txtQOrgYrNo.Text = kvp.Value
                                Else
                                    chkOrgYearNo.Checked = False
                                    chkOrgYearNo_CheckedChanged(Nothing, Nothing)
                                    txtQOrgYrNo.Text = "0"
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO
                                If CInt(kvp.Value) > 0 Then
                                    chkQMaxAge.Checked = True
                                    chkQMaxAge_CheckedChanged(Nothing, Nothing)
                                    txtQMaxAgeNo.Text = kvp.Value
                                Else
                                    chkQMaxAge.Checked = False
                                    chkQMaxAge_CheckedChanged(Nothing, Nothing)
                                    txtQMaxAgeNo.Text = "0"
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER
                                If CInt(kvp.Value) > 0 Then
                                    chkQMaxScreeners.Checked = True
                                    chkQMaxScreeners_CheckedChanged(Nothing, Nothing)
                                    txtMaxQScreener.Text = kvp.Value
                                Else
                                    chkQMaxScreeners.Checked = False
                                    chkQMaxScreeners_CheckedChanged(Nothing, Nothing)
                                    txtMaxQScreener.Text = "0"

                                End If


                            Case clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO
                                If CInt(kvp.Value) > 0 Then
                                    txtQPrdNo.Text = kvp.Value
                                Else
                                    txtQPrdNo.Text = "0"
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.MIN_SCREENER_REQ
                                If CInt(kvp.Value) > 0 Then
                                    chkQMinScrReq.Checked = True
                                    chkQMinScrReq_CheckedChanged(Nothing, Nothing)
                                    txtQMinScrReq.Text = kvp.Value
                                Else
                                    chkQMinScrReq.Checked = False
                                    chkQMinScrReq_CheckedChanged(Nothing, Nothing)
                                    txtQMinScrReq.Text = "0"
                                End If

                                'Gajanan [26-Feb-2021] -- Start
                            Case clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION
                                If CInt(kvp.Value) > 0 Then
                                    chkQMaxNomination.Checked = True
                                    chkQMaxNomination_CheckedChanged(Nothing, Nothing)
                                    txtQMaxNomination.Text = kvp.Value
                                Else
                                    chkQMaxNomination.Checked = False
                                    chkQMaxNomination_CheckedChanged(Nothing, Nothing)
                                    txtQMaxNomination.Text = "0"
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.IS_USERACESS_USE_FOR_NOMINATION
                                If CBool(kvp.Value) Then
                                    chkQUserAccess.Checked = True
                                Else
                                    chkQUserAccess.Checked = False
                                End If
                                'Gajanan [26-Feb-2021] -- End



                            Case clssucsettings_master.enSuccessionConfiguration.PERF_SCORE
                                If CInt(kvp.Value) > 0 Then
                                    chkPerformanceScore.Checked = True
                                    chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                    txtMinPSValue.Text = kvp.Value
                                Else
                                    chkPerformanceScore.Checked = False
                                    chkPerformanceScore_CheckedChanged(Nothing, Nothing)
                                    txtMinPSValue.Text = "0"
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.TOTAL_QUESTION_WEIGHT
                                If CInt(kvp.Value) > 0 Then
                                    chkQTotalQueWeight.Checked = True
                                    chkQTotalQueWeight_CheckedChanged(Nothing, Nothing)
                                    txtQTotalQueWeight.Text = kvp.Value

                                Else
                                    chkQTotalQueWeight.Checked = False
                                    chkQTotalQueWeight_CheckedChanged(Nothing, Nothing)
                                    txtQTotalQueWeight.Text = "0"
                                End If

                            Case clssucsettings_master.enSuccessionConfiguration.INSTRUCTION
                                txtInstruction.Text = CStr(kvp.Value)


                            Case clssucsettings_master.enSuccessionConfiguration.MAX_DATA_DISPLAY
                                txtQMaxDataDisplayPerStage.Text = CStr(kvp.Value)

                        End Select
                    Next
                End If



            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            Finally
                objsucsettings_master = Nothing
            End Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Function SaveMaxNominationSetting() As Boolean
        Dim objsucsetting As New clssucsettings_master
        Dim blnFlag As Boolean = False
        Try
            Dim tlSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)
            tlSetting = New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)

            If chkQMaxNomination.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION, txtQMaxNomination.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION, "0")
            End If


            objsucsetting._AuditUserId = CInt(Session("UserId"))
            objsucsetting._ClientIP = CStr(Session("IP_ADD"))
            objsucsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsucsetting._DatabaseName = CStr(Session("Database_Name"))
            objsucsetting._FormName = mstrModuleName3
            objsucsetting._FromWeb = True
            objsucsetting._HostName = CStr(Session("HOST_NAME"))
            objsucsetting._DatabaseName = CStr(Session("Database_Name"))

            blnFlag = objsucsetting.SaveTlSetting(tlSetting)
            If blnFlag = False AndAlso objsucsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objsucsetting._Message, Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
#End Region

#Region " Dropdown Event "
    Protected Sub drpQAllocBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpQAllocBy.SelectedIndexChanged
        Dim intColType As Integer = 0
        Dim dsList As DataSet
        Dim dtTable As DataTable

        Try
            Select Case CInt(drpQAllocBy.SelectedValue)

                Case CInt(enAllocation.BRANCH)

                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                    If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
                        If CInt(Session("UserId")) > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.DEPARTMENT_GROUP)

                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                    If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.DEPARTMENT)

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                    If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.SECTION_GROUP)

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                    If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.SECTION)

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                    If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.UNIT_GROUP)

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                    If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.UNIT)

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                    If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.TEAM)

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                    If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.JOB_GROUP)

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                    If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.JOBS)
                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                    If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case CInt(enAllocation.CLASS_GROUP)

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                    If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case CInt(enAllocation.CLASSES)

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                    If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
                        Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
                        StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                        dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case CInt(enAllocation.COST_CENTER)

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2
                    dtTable = dsList.Tables("List")

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobgroupunkid").ColumnName = "Id"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_name").ColumnName = "Name"
            End If

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsCheck"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                dtTable.Rows.Add(r)
            End If

            gvQAllocBy.DataSource = dtTable
            gvQAllocBy.DataBind()

        End Try
    End Sub

#End Region

#Region " Button's Event "
    Protected Sub btnQualifySave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualifySave.Click
        Dim objsucsetting As New clssucsettings_master
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidQualifying() = False Then
                Exit Sub
            End If

            Dim tlSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)
            Dim OldSucSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)

            OldSucSetting = objsucsetting.GetSettingFromPeriod()

            If chkPerformanceScore.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE, txtMinPSValue.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE, "0")
            End If

            If chkPerformanceScore.Checked = False Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ANY_PERIOD, chkPerformanceScore.Checked.ToString())
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ALL_PERIOD, chkPerformanceScore.Checked.ToString())

            Else
                If rdbanyPeriod.Checked Then
                    tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ANY_PERIOD, rdbanyPeriod.Checked.ToString())
                    tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ALL_PERIOD, (Not rdbanyPeriod.Checked).ToString())
                End If

                If rdballPeriod.Checked Then
                    tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ALL_PERIOD, rdballPeriod.Checked.ToString())
                    tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ANY_PERIOD, (Not rdballPeriod.Checked).ToString())
                End If
            End If



            If chkPerformanceScore.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO, txtQPrdNo.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO, "0")
            End If


            If chkQMaxAge.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO, txtQMaxAgeNo.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO, "0")
            End If

            If chkOrgYearNo.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO, txtQOrgYrNo.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO, "0")
            End If

            If chkQMaxScreeners.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER, txtMaxQScreener.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER, "0")
            End If

            'Gajanan [26-Feb-2021] -- Start
            If chkQUserAccess.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.IS_USERACESS_USE_FOR_NOMINATION, chkQUserAccess.Checked.ToString())
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.IS_USERACESS_USE_FOR_NOMINATION, chkQUserAccess.Checked.ToString())
            End If
            'Gajanan [26-Feb-2021] -- End


            If chkQMinScrReq.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MIN_SCREENER_REQ, txtQMinScrReq.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MIN_SCREENER_REQ, "0")
            End If


            If chkQTotalQueWeight.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.TOTAL_QUESTION_WEIGHT, txtQTotalQueWeight.Text)
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.TOTAL_QUESTION_WEIGHT, "0")
            End If

            If chkQualificationlevel.Checked Then
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL, txtQualificationlevel.SelectedValue)
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION, CInt(drpQualificationlevel.SelectedValue).ToString())
            Else
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL, "0")
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION, "0")
            End If

            tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.MAX_DATA_DISPLAY, txtQMaxDataDisplayPerStage.Text)




            If chkAllocationBy.Checked Then
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                Dim lstIDs As List(Of String) = Nothing
                Dim allocation_Id As String = ""

                gRow = gvQAllocBy.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

                lstIDs = (From p In gRow Select (gvQAllocBy.DataKeys(p.DataItemIndex)("Id").ToString())).ToList()
                allocation_Id = drpQAllocBy.SelectedValue + "|" + String.Join(",", lstIDs.ToArray)
                tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.ALLOC_TYPE, allocation_Id)

            End If

            tlSetting.Add(clssucsettings_master.enSuccessionConfiguration.INSTRUCTION, txtInstruction.Text)

            objsucsetting._AuditUserId = CInt(Session("UserId"))
            objsucsetting._ClientIP = CStr(Session("IP_ADD"))
            objsucsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsucsetting._DatabaseName = CStr(Session("Database_Name"))
            objsucsetting._FormName = mstrModuleName3
            objsucsetting._FromWeb = True
            objsucsetting._HostName = CStr(Session("HOST_NAME"))
            objsucsetting._DatabaseName = CStr(Session("Database_Name"))

            blnFlag = objsucsetting.SaveTlSetting(tlSetting)

            If blnFlag = False AndAlso objsucsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objsucsetting._Message, Me)
            End If

            '-------------------------Update Nominated Old Data----------------
            If objsucscreening_process_master.IsSuccessionStarted() Then
                Dim dsNominatedOldList As DataSet = objEmployee_nomination_master.GetList("NominatedOldList", -1, True, False)
                Dim dsNominatedMatchList As DataSet = objsucscreening_process_master.GetNominatedProcessedDataAfterSettingChage(CStr(Session("Database_Name")), _
                                                                                                                                CInt(Session("CompanyUnkId")), _
                                                                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                                                                 CInt(Session("Fin_year")), _
                                                                                                                                 "SuccessionNominatedList")

                If IsNothing(dsNominatedOldList) = False AndAlso IsNothing(dsNominatedMatchList) = False Then
                    Dim dtGetNominatedIsMatch As DataTable = dsNominatedMatchList.Tables("SuccessionNominatedList").AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ismatch") = True).AsDataView().ToTable()
                    Dim dtSetNominatedIsMatch As DataTable = dsNominatedMatchList.Tables("SuccessionNominatedList").AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ismatch") = False).AsDataView().ToTable()

                    Dim intNominateunkids As Integer() = dsNominatedOldList.Tables("NominatedOldList").AsEnumerable().Select(Function(x) x.Field(Of Integer)("nominationunkid")).Except(dtGetNominatedIsMatch.AsEnumerable().Select(Function(y) y.Field(Of Integer)("nominationunkid"))).ToArray()
                    Dim intSetIsMatchNominatemstunkid As Integer() = dtSetNominatedIsMatch.AsEnumerable().Select(Function(x) x.Field(Of Integer)("nominationunkid")).ToArray()

                    objsucscreening_process_master._AuditUserId = CInt(Session("UserId"))
                    objsucscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                    objsucscreening_process_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    objsucscreening_process_master._DatabaseName = CStr(Session("Database_Name"))
                    objsucscreening_process_master._FormName = mstrModuleName3
                    objsucscreening_process_master._FromWeb = True
                    objsucscreening_process_master._HostName = CStr(Session("HOST_NAME"))

                    If intNominateunkids.Length > 0 Then

                        blnFlag = objsucscreening_process_master.UpdateNominatedDataAsIsMatch(String.Join(",", intNominateunkids.Select(Function(x) x.ToString()).ToArray()), False)
                        If blnFlag = False AndAlso objsucscreening_process_master._Message.Trim.Length > 0 Then
                            DisplayMessage.DisplayMessage(objsucscreening_process_master._Message, Me)
                        End If
                    End If

                    If intSetIsMatchNominatemstunkid.Length > 0 Then
                        blnFlag = objsucscreening_process_master.UpdateNominatedDataAsIsMatch(String.Join(",", intSetIsMatchNominatemstunkid.Select(Function(x) x.ToString()).ToArray()), True)
                        If blnFlag = False AndAlso objsucscreening_process_master._Message.Trim.Length > 0 Then
                            DisplayMessage.DisplayMessage(objsucscreening_process_master._Message, Me)
                        End If
                    End If
                End If


                '-------------------------Update Nominated Old Data End----------------


                '-------------------------Update Succession Data Start----------------

                If objsucscreening_process_master.IsSuccessionStarted() Then

                    Dim dsOldList As DataSet = objsucscreening_process_master.GetList(-1, "OldList", False)
                    Dim dskist As DataSet = objsucscreening_process_master.GetSuccessionProcessedDataAfterSettingChage(CStr(Session("Database_Name")), _
                                                                     CInt(Session("UserId")), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, "SuccessionProcessedList")

                    If IsNothing(dsOldList) = False AndAlso IsNothing(dskist) = False Then
                        Dim dtGetIsMatch As DataTable = dskist.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ismatch") = True).AsDataView().ToTable()
                        Dim dtSetIsMatch As DataTable = dskist.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ismatch") = False).AsDataView().ToTable()

                        Dim intProcessmstunkids As Integer() = dsOldList.Tables("OldList").AsEnumerable().Select(Function(x) x.Field(Of Integer)("processmstunkid")).Except(dtGetIsMatch.AsEnumerable().Select(Function(y) y.Field(Of Integer)("processmstunkid"))).ToArray()
                        Dim intSetIsMatchProcessmstunkid As Integer() = dtSetIsMatch.AsEnumerable().Select(Function(x) x.Field(Of Integer)("processmstunkid")).ToArray()

                        objsucscreening_process_master._AuditUserId = CInt(Session("UserId"))
                        objsucscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                        objsucscreening_process_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
                        objsucscreening_process_master._DatabaseName = CStr(Session("Database_Name"))
                        objsucscreening_process_master._FormName = mstrModuleName3
                        objsucscreening_process_master._FromWeb = True
                        objsucscreening_process_master._HostName = CStr(Session("HOST_NAME"))

                        If intProcessmstunkids.Length > 0 Then
                            blnFlag = objsucscreening_process_master.UpdateProcessDataAsIsMatch(String.Join(",", intProcessmstunkids.Select(Function(x) x.ToString()).ToArray()), False)
                            If blnFlag = False AndAlso objsucscreening_process_master._Message.Trim.Length > 0 Then
                                DisplayMessage.DisplayMessage(objsucscreening_process_master._Message, Me)
                            End If
                        End If

                        If intSetIsMatchProcessmstunkid.Length > 0 Then
                            blnFlag = objsucscreening_process_master.UpdateProcessDataAsIsMatch(String.Join(",", intSetIsMatchProcessmstunkid.Select(Function(x) x.ToString()).ToArray()), True)
                            If blnFlag = False AndAlso objsucscreening_process_master._Message.Trim.Length > 0 Then
                                DisplayMessage.DisplayMessage(objsucscreening_process_master._Message, Me)
                            End If
                        End If
                    End If
                End If
            End If

            Dim blnCheckMaxNomination As Boolean = False
            If IsNothing(OldSucSetting) OrElse OldSucSetting.Count <= 0 Then
                blnCheckMaxNomination = True
            ElseIf OldSucSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION) AndAlso CInt(OldSucSetting(clssucsettings_master.enSuccessionConfiguration.MAX_NOMINATION)) > CInt(txtQMaxNomination.Text) Then
                blnCheckMaxNomination = True
            End If

            If blnCheckMaxNomination Then
                Dim dsJobCount As DataSet

                dsJobCount = objsucscreening_process_master.GetNominatedJobCountList(CInt(txtQMaxNomination.Text), "NominatedJobCount")
                If IsNothing(dsJobCount) = False AndAlso dsJobCount.Tables("NominatedJobCount").Rows.Count > 0 Then
                    dgvMaxNominateEmployee.DataSource = dsJobCount.Tables("NominatedJobCount")
                    dgvMaxNominateEmployee.DataBind()
                    popupMaxNominateEmployee.Show()
                Else
                    SaveMaxNominationSetting()
                End If
            Else
                SaveMaxNominationSetting()
            End If

            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 11, "Qualification Setting Save successfully."), Me)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucsetting = Nothing
        End Try
    End Sub

    Protected Sub btnMaxNominateEmployeeClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMaxNominateEmployeeClose.Click
        Try
            popupMaxNominateEmployee.Hide()
            dgvMaxNominateEmployee.DataSource = Nothing
            dgvMaxNominateEmployee.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Checkbox Event "
    Protected Sub chkPerformanceScore_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPerformanceScore.CheckedChanged
        Try
            pnlPerformanceScore.Enabled = chkPerformanceScore.Checked
            txtMinPSValue.Enabled = chkPerformanceScore.Checked
            txtQPrdNo.Enabled = chkPerformanceScore.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkQMaxAge_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMaxAge.CheckedChanged
        Try
            txtQMaxAgeNo.Enabled = chkQMaxAge.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOrgYearNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOrgYearNo.CheckedChanged
        Try
            txtQOrgYrNo.Enabled = chkOrgYearNo.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub chkQMaxScreeners_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMaxScreeners.CheckedChanged
        Try
            txtMaxQScreener.Enabled = chkQMaxScreeners.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub chkQMinScrReq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMinScrReq.CheckedChanged
        Try
            txtQMinScrReq.Enabled = chkQMinScrReq.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    'Gajanan [26-Feb-2021] -- Start
    Protected Sub chkQMaxNomination_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQMaxNomination.CheckedChanged
        Try
            txtQMaxNomination.Enabled = chkQMaxNomination.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [26-Feb-2021] -- End

    Protected Sub chkQTotalQueWeight_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQTotalQueWeight.CheckedChanged
        Try
            txtQTotalQueWeight.Enabled = chkQTotalQueWeight.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkQualificationlevel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkQualificationlevel.CheckedChanged
        Try
            txtQualificationlevel.Enabled = chkQualificationlevel.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAllocationBy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllocationBy.CheckedChanged
        Try
            pnlAllocation.Enabled = chkAllocationBy.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#End Region

#Region " Screeners Setup "

#Region " Private Methods "
    Private currentId As String = ""

    Private Sub FillScreenersCombo()
        Dim objUser As New clsUserAddEdit
        'Dim obCyl As New clssuccycle_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), , CInt(Session("Fin_year")), True)
            drpscreenerUser.DataSource = dsCombo.Tables("User")
            drpscreenerUser.DataTextField = "name"
            drpscreenerUser.DataValueField = "userunkid"
            drpscreenerUser.DataBind()


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUser = Nothing
            'obCyl = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#DDD")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function IsValidScreener() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objsucsettings_master As New clssucsettings_master
        Dim objsucscreener_master As New clssucscreener_master
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try

            Dim mdicSetting As Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String) = objsucsettings_master.GetSettingFromPeriod()
            If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 16, "Sorry you can't add screener,Reason: Succession Setting is not define."), Me)
                Return False
            End If

            objsucscreening_process_master._DatabaseName = CStr(Session("Database_Name"))
            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 12, "Sorry you can't add this screener,Reason: Succession Process is already started for this cycle."), Me)
                Exit Function
            End If


            If CInt(drpscreenerUser.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 1, "Sorry, User is mandatory information. Please select user to continue."), Me)
                Return False
            End If

            Dim MaxScreenerCount As String = objsucsettings_master.GetSettingValueFromKey(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER)
            Dim CurrentScreenerCount As Integer = objsucscreener_master.GetMaximumScreenerCountFromCycleId() + 1

            If MaxScreenerCount.Length = 0 Then MaxScreenerCount = "0"
            If CDbl(MaxScreenerCount) < CurrentScreenerCount Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 6, "Sorry, Your Total Screeners limit exceeded."), Me)
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
            objsucsettings_master = Nothing
            objsucscreening_process_master = Nothing
        End Try
    End Function

    Private Sub ClearScreenerCtrls()
        Try
            mintScreenerUnkid = 0
            drpscreenerUser.SelectedValue = CStr(0)
            TvApproverUseraccess.DataSource = Nothing
            TvApproverUseraccess.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillScreener()
        Dim objScreener As New clssucscreener_master
        Dim dsList As New DataSet
        Try
            dsList = objScreener.GetList("List", False)
            gvScreener.DataSource = dsList.Tables(0)
            gvScreener.DataBind()

            gvScreener.Columns(0).Visible = CBool(Session("AllowToModifySuccessionSetting"))
            gvScreener.Columns(1).Visible = CBool(Session("AllowToModifySuccessionSetting"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try
    End Sub

    Private Sub SetScreenerValue(ByRef objScreener As clssucscreener_master)
        Try
            objScreener._Screenermstunkid = mintScreenerUnkid
            objScreener._Mapuserunkid = CInt(drpscreenerUser.SelectedValue)
            objScreener._Userunkid = CInt(Session("UserId"))
            objScreener._Isactive = True
            objScreener._Isvoid = False

            objScreener._AuditUserId = CInt(Session("UserId"))
            objScreener._ClientIP = CStr(Session("IP_ADD"))
            objScreener._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objScreener._DatabaseName = CStr(Session("Database_Name"))
            objScreener._FormName = mstrModuleName4
            objScreener._FromWeb = True
            objScreener._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Gridview Event"
    Protected Sub drpApproverUseraccess_user_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpscreenerUser.SelectedIndexChanged
        Dim objUser As New clsUserAddEdit
        Try
            Dim dtTable As DataTable = Nothing
            If CInt(drpscreenerUser.SelectedValue) > 0 Then
                dtTable = objUser.GetUserAccessList(Convert.ToString(Session("UserAccessModeSetting")), CInt(drpscreenerUser.SelectedValue), Convert.ToInt32(Session("CompanyUnkId")))
                dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            End If
            TvApproverUseraccess.DataSource = dtTable
            TvApproverUseraccess.DataBind()
            dtTable = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUser = Nothing
        End Try
    End Sub

    Protected Sub TvApproverUseraccess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TvApproverUseraccess.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                oldid = dt.Rows(e.Row.RowIndex)("UserAccess").ToString()
                If CInt(dt.Rows(e.Row.RowIndex)("AllocationLevel").ToString()) = -1 AndAlso oldid <> currentId Then
                    Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("UserAccess").ToString(), TvApproverUseraccess)
                    currentId = oldid
                Else
                    Dim statusCell As TableCell = e.Row.Cells(0)
                    statusCell.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dt.Rows(e.Row.RowIndex)("UserAccess").ToString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

    Protected Sub gvScreener_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvScreener.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("t1ScreenerActive"), LinkButton)
                Dim lnkInActive As LinkButton = TryCast(e.Row.FindControl("t1ScreenerInActive"), LinkButton)


                If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                    If CBool(dt.Rows(e.Row.RowIndex)("isactive").ToString()) = True Then
                        lnkactive.Visible = False
                        lnkInActive.Visible = True
                    Else
                        lnkactive.Visible = True
                        lnkInActive.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link Event(s) "

    Protected Sub lnkt1ScreenerDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 13, "Sorry you can't delete this screener,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintScreenerUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("screenermstunkid"))
            mstrDeleteAction = "DELSC"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 3, "You are about to delete this screener. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkt1ScreenerInactive_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 14, "Sorry you can't inactive this screener,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintScreenerUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("screenermstunkid"))
            mstrDeleteAction = "Inactivesc"

            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 7, "You are about to inactive this screener. Are you sure you want to inactive?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkt1ScreenerActive_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 15, "Sorry you can't active this screener,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintScreenerUnkid = CInt(gvScreener.DataKeys(row.RowIndex)("screenermstunkid"))
            mstrDeleteAction = "Activesc"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 8, "You are about to active this screener. Are you sure you want to active?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnAddScreener_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddScreener.Click
        Dim objscreener As New clssucscreener_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidScreener() = False Then
                Exit Sub
            End If

            SetScreenerValue(objscreener)
            If mintScreenerUnkid > 0 Then
                blnFlag = objscreener.Update()
            Else
                blnFlag = objscreener.Insert()
            End If

            If blnFlag = False AndAlso objscreener._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objscreener._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 2, "Screener Save successfully."), Me)
                FillScreener()
                ClearScreenerCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objscreener = Nothing
        End Try
    End Sub

#End Region


#End Region

#Region " Questionnaire Setup "

#Region " Private Method(s)"

    Private Sub ClearQuestionnaireCtrls()
        Try
            mintQuestionnaireUnkid = 0
            txtQWeight.Text = "1"
            txtQuestion.Text = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillQuestionnaire()
        Dim objQuestionnaire As New clssucquestionnaire_master
        Dim dsList As New DataSet
        Try
            dsList = objQuestionnaire.GetList("List")
            gvobjQuestionnaire.DataSource = dsList.Tables(0)
            gvobjQuestionnaire.DataBind()

            gvobjQuestionnaire.Columns(0).Visible = CBool(Session("AllowToModifySuccessionSetting"))
            gvobjQuestionnaire.Columns(1).Visible = CBool(Session("AllowToModifySuccessionSetting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestionnaire = Nothing
        End Try
    End Sub

    Private Sub FillComboForQuestionnaire()
        Dim objCommon_Master As New clsCommon_Master
        Dim dsCombo As DataSet = Nothing
        Try

            dsCombo = objCommon_Master.GetQualificationGrpFromLevel(-1, True)

            If IsNothing(dsCombo) = False Then
                With txtQualificationlevel
                    .DataSource = dsCombo.Tables(0)
                    .DataTextField = "name"
                    .DataValueField = "masterunkid"
                    .DataBind()
                End With
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'obCyl = Nothing
            'dsCombo.Dispose()
        End Try
    End Sub

    Private Function IsValidQuestionnaire() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objsucsetting As New clssucsettings_master
        Dim objsucquestionnaire_master As New clssucquestionnaire_master
        Dim objsucsettings_master As New clssucsettings_master
        objsucquestionnaire_master._DatabaseName = CStr(Session("Database_Name"))
        objsucsettings_master._DatabaseName = CStr(Session("Database_Name"))
        Dim objsucscreening_process_master As New clssucscreening_process_master

        Try

            Dim mdicSetting As Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String) = objsucsetting.GetSettingFromPeriod()
            If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 11, "Sorry you can't add question,Reason: Succession Setting is not define."), Me)
                Return False
            End If


            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 8, "Sorry you can't add new question,Reason: Succession Process is already started for this cycle."), Me)
                Return False

            End If


            If txtQWeight.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 2, "Sorry, Weight is mandatory information. Please enter Weight to continue."), Me)
                Return False
            End If

            If txtQuestion.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 3, "Sorry, Question is mandatory information. Please enter Question to continue."), Me)
                Return False
            End If

            Dim MaxQuestionWeight As String = objsucsettings_master.GetSettingValueFromKey(clssucsettings_master.enSuccessionConfiguration.TOTAL_QUESTION_WEIGHT)
            Dim CurrentQuestionWeight As Double = objsucquestionnaire_master.GetQuestionnairMaxWeightFromCycleId(mintQuestionnaireUnkid) + CDbl(txtQWeight.Text)

            If CDbl(MaxQuestionWeight) < CurrentQuestionWeight Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 7, "Sorry, Your Total question weight limit exceeded."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFinYear = Nothing
            objsucquestionnaire_master = Nothing
            objsucsettings_master = Nothing
        End Try
    End Function

    Private Sub SetQuestionnaireValue(ByRef objQuestionnaire As clssucquestionnaire_master)
        Try
            objQuestionnaire._Questionnaireunkid = mintQuestionnaireUnkid
            objQuestionnaire._AuditUserId = CInt(Session("UserId"))
            objQuestionnaire._ClientIP = CStr(Session("IP_ADD"))
            objQuestionnaire._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objQuestionnaire._DatabaseName = CStr(Session("Database_Name"))
            objQuestionnaire._FormName = mstrModuleName5
            objQuestionnaire._FromWeb = True
            objQuestionnaire._HostName = CStr(Session("HOST_NAME"))
            objQuestionnaire._Isactive = True
            objQuestionnaire._Question = txtQuestion.Text
            objQuestionnaire._Weight = Convert.ToSingle(txtQWeight.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetQuestionnaireValue(ByVal intQuestionnaireId As Integer)
        Dim objQuestionnaire As New clssucquestionnaire_master
        Try
            objQuestionnaire._Questionnaireunkid = mintQuestionnaireUnkid
            txtQWeight.Text = CStr(objQuestionnaire._Weight)
            txtQuestion.Text = objQuestionnaire._Question

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestionnaire = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Events "

    Protected Sub btnQuestionAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuestionAdd.Click
        Dim objQuestion As New clssucquestionnaire_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidQuestionnaire() = False Then
                Exit Sub
            End If
            SetQuestionnaireValue(objQuestion)
            If mintQuestionnaireUnkid > 0 Then
                blnFlag = objQuestion.Update()
            Else
                blnFlag = objQuestion.Insert()
            End If

            If blnFlag = False AndAlso objQuestion._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objQuestion._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 4, "Succession Questionnaire defined successfully."), Me)
                FillQuestionnaire()
                ClearQuestionnaireCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestion = Nothing
        End Try
    End Sub

    Protected Sub btnQuestionReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuestionReset.Click
        Try
            Call ClearQuestionnaireCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkQEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 16, "Sorry you can't edit this Question,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintQuestionnaireUnkid = CInt(gvobjQuestionnaire.DataKeys(row.RowIndex)("questionnaireunkid"))
            GetQuestionnaireValue(mintQuestionnaireUnkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub lnkQDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 10, "Sorry you can't delete this Question,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If

            mintQuestionnaireUnkid = CInt(gvobjQuestionnaire.DataKeys(row.RowIndex)("questionnaireunkid"))
            mstrDeleteAction = "delque"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 5, "You are about to delete this Question. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region " Ratings Setup "

#Region " Private Method(s)"

    Private Sub ClearRatingsCtrls()
        Try
            mintRatingsUnkid = 0
            txtRDescription.Text = ""
            txtScrF.Text = "1"
            txtScrT.Text = "1"
            drpRStage.SelectedValue = CStr(0)
            txtRActionColor.Text = "#000000"
            txtScrF.Enabled = True
            txtScrT.Enabled = True
            drpRStage.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillRatings()
        Dim objRatings As New clssucratings_master
        Dim dsList As New DataSet
        Try
            dsList = objRatings.GetList("List")
            gvobjRatings.DataSource = dsList.Tables(0)
            gvobjRatings.DataBind()

            gvobjRatings.Columns(0).Visible = CBool(Session("AllowToModifySuccessionSetting"))
            gvobjRatings.Columns(1).Visible = CBool(Session("AllowToModifySuccessionSetting"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

    Private Sub FillComboForRatings()
        'Dim obCyl As New clssuccycle_master
        Dim objstages As New clssucstages_master
        Dim dsCombo As DataSet = Nothing
        Try

            dsCombo = objstages.getComboList("List", True, True)

            With drpRStage
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'obCyl = Nothing
            objstages = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Function IsValidRatings() As Boolean
        Dim objFinYear As New clsCompany_Master
        Dim objRatings As New clssucratings_master
        Dim objsucsetting As New clssucsettings_master

        Dim dsList As New DataSet
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try

            Dim mdicSetting As Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String) = objsucsetting.GetSettingFromPeriod()
            If IsNothing(mdicSetting) = True OrElse mdicSetting.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 17, "Sorry you can't add rating,Reason: Succession Setting is not define."), Me)
                Return False
            End If


            If objsucscreening_process_master.IsSuccessionStarted() AndAlso mintRatingsUnkid <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 13, "Sorry you can't add rating,Reason: Succession Process is already started for this cycle."), Me)
                Exit Function
            End If

            If txtScrF.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 2, "Sorry, Score From is mandatory information. Please enter Score From to continue."), Me)
                Return False
            End If

            If txtScrT.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 3, "Sorry, Score To is mandatory information. Please enter Score To to continue."), Me)
                Return False
            End If

            If txtRDescription.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 4, "Sorry, Description is mandatory information. Please enter Description to continue."), Me)
                Return False
            End If

            If CInt(drpRStage.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 5, "Sorry, Action is mandatory information. Please select Action to continue."), Me)
                Return False
            End If

            If CDbl(txtScrF.Text) > CDbl(txtScrT.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 6, "Sorry, Score from  should not be greater than Score To."), Me)
                Return False
            End If

            dsList = objRatings.GetList("List")
            For Each drRow As DataRow In dsList.Tables(0).Select("ratingunkid <> " & mintRatingsUnkid)
                If (CDbl(txtScrF.Text) >= CDbl(drRow.Item("scorefrom")) AndAlso CDbl(txtScrF.Text) <= CDbl(drRow.Item("scoreto"))) OrElse _
                (CDbl(txtScrT.Text) >= CDbl(drRow.Item("scorefrom")) AndAlso CDbl(txtScrT.Text) <= CDbl(drRow.Item("scoreto"))) Then

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 7, "Sorry, Score from  or Score To is already defined Range."), Me)
                    Return False

                End If
            Next

            Dim drExistRow() As DataRow = dsList.Tables(0).Select("ratingunkid <> " & mintRatingsUnkid & " AND scoreto > " & CDbl(txtScrF.Text) & "  AND scoreto < " & CDbl(txtScrT.Text))
            If drExistRow.Length > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 8, "Sorry, Score from  and Score To is not valid defined Range."), Me)
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
            objFinYear = Nothing
        End Try
    End Function

    Private Sub SetRatingsValue(ByRef objRatings As clssucratings_master)
        Try
            objRatings._Ratingunkid = mintRatingsUnkid
            objRatings._Description = txtRDescription.Text
            objRatings._Color = txtRActionColor.Text
            objRatings._Scorefrom = Convert.ToSingle(txtScrF.Text)
            objRatings._Scoreto = Convert.ToSingle(txtScrT.Text)
            objRatings._Stageunkid = CInt(drpRStage.SelectedValue)
            objRatings._AuditUserId = CInt(Session("UserId"))
            objRatings._ClientIP = CStr(Session("IP_ADD"))
            objRatings._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objRatings._DatabaseName = CStr(Session("Database_Name"))
            objRatings._FormName = mstrModuleName6
            objRatings._FromWeb = True
            objRatings._HostName = CStr(Session("HOST_NAME"))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetRatingsValue(ByVal intRatingsId As Integer)
        Dim objRatings As New clssucratings_master
        Try
            objRatings._Ratingunkid = mintRatingsUnkid
            txtScrF.Text = CStr(objRatings._Scorefrom)
            txtScrT.Text = CStr(objRatings._Scoreto)
            txtRDescription.Text = objRatings._Description
            txtRActionColor.Text = objRatings._Color
            drpRStage.SelectedValue = CStr(objRatings._Stageunkid)

            Dim objsucscreening_process_master As New clssucscreening_process_master
            If objsucscreening_process_master.IsSuccessionStarted() Then
                txtScrF.Enabled = False
                txtScrT.Enabled = False
                drpRStage.Enabled = False
            End If
            objsucscreening_process_master = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnRAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRAdd.Click
        Dim objRatings As New clssucratings_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidRatings() = False Then
                Exit Sub
            End If
            SetRatingsValue(objRatings)
            If mintRatingsUnkid > 0 Then
                blnFlag = objRatings.Update()
            Else
                blnFlag = objRatings.Insert()
            End If

            If blnFlag = False AndAlso objRatings._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objRatings._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 9, "Succession Ratings defined successfully."), Me)
                FillRatings()
                ClearRatingsCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

    Protected Sub btnRReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRReset.Click
        Try
            Call ClearRatingsCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkREdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintRatingsUnkid = CInt(gvobjRatings.DataKeys(row.RowIndex)("ratingunkid"))
            GetRatingsValue(mintRatingsUnkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkRDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            If objsucscreening_process_master.IsSuccessionStarted() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 15, "Sorry you can't delete this rating,Reason: Succession Process is already started for this cycle."), Me)
                Exit Sub
            End If
            mintRatingsUnkid = CInt(gvobjRatings.DataKeys(row.RowIndex)("ratingunkid"))

            mstrDeleteAction = "delrat"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 10, "You are about to delete this Rating. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

#End Region

#Region " Gridview Events "
    Protected Sub gvobjRatings_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvobjRatings.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblColor As Label = TryCast(e.Row.FindControl("objLblcolor"), Label)
            Dim hfColor As HiddenField = TryCast(e.Row.FindControl("hfColor"), HiddenField)
            lblColor.BackColor = Drawing.ColorTranslator.FromHtml(hfColor.Value)
        End If
    End Sub
#End Region

#End Region

#Region "Confirmation"
    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try

            Select Case mstrDeleteAction.ToUpper()
                'Case "DELCY"
                '    Dim objCycle As New clssuccycle_master
                '    SetCycleValue(objCycle)
                '    objCycle._Isactive = False
                '    objCycle._Inactiveuserunkid = CInt(Session("UserId"))
                '    blnFlag = objCycle.Delete(mintCycleUnkid)
                '    If blnFlag = False AndAlso objCycle._Message.Trim.Length > 0 Then
                '        DisplayMessage.DisplayMessage(objCycle._Message, Me)
                '    Else
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 11, "Succession cycle deleted successfully."), Me)
                '        FillCycle()
                '    End If
                '    objCycle = Nothing
                Case "DELSTA"
                    Dim objStaging As New clssucstages_master
                    Dim dsGetList As DataSet = Nothing
                    objStaging._Stageunkid = mintStagingUnkid
                    If objStaging._Isdefault = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 10, "Sorry, You can not delete this entry, Reason:This is auto generated entry from Succession."), Me)
                        Exit Sub
                    End If
                    objStaging._ApplyOnFloworder = objStaging._Floworder
                    objStaging._Isactive = False
                    objStaging._AuditUserId = CInt(Session("UserId"))
                    objStaging._ClientIP = CStr(Session("IP_ADD"))
                    objStaging._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    objStaging._DatabaseName = CStr(Session("Database_Name"))
                    objStaging._FormName = mstrModuleName2
                    objStaging._FromWeb = True
                    objStaging._HostName = CStr(Session("HOST_NAME"))

                    dsGetList = objStaging.GetList("List")
                    blnFlag = objStaging.Delete(mintStagingUnkid, , dsGetList.Tables(0))
                    If blnFlag = False AndAlso objStaging._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objStaging._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 11, "Succession Stage deleted successfully."), Me)
                        FillStaging()
                    End If
                    objStaging = Nothing
                Case "DELQUE"
                    Dim objQuestionnaire As New clssucquestionnaire_master
                    SetQuestionnaireValue(objQuestionnaire)
                    objQuestionnaire._Isactive = False
                    blnFlag = objQuestionnaire.Delete(mintQuestionnaireUnkid)
                    If blnFlag = False AndAlso objQuestionnaire._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objQuestionnaire._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 6, "Succession Questionnaire deleted successfully."), Me)
                        FillQuestionnaire()
                    End If
                    objQuestionnaire = Nothing
                Case "DELRAT"

                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 12, "Rating delete reason.")
                    delReason.Show()

                Case "INACTIVESC"
                    Dim objScreener As New clssucscreener_master
                    SetScreenerValue(objScreener)
                    blnFlag = objScreener.ActiveInactiveScreener(mintScreenerUnkid, False)
                    If blnFlag = False AndAlso objScreener._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objScreener._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 9, "Screener Inactivated successfully."), Me)
                        FillScreener()
                    End If
                    objScreener = Nothing

                Case "ACTIVESC"
                    Dim objScreener As New clssucscreener_master
                    SetScreenerValue(objScreener)
                    blnFlag = objScreener.ActiveInactiveScreener(mintScreenerUnkid, True)
                    If blnFlag = False AndAlso objScreener._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objScreener._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 10, "Screener Activated successfully."), Me)
                        FillScreener()
                    End If
                    objScreener = Nothing

                Case "DELSC"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 4, "Screener delete reason.")
                    delReason.Show()

                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Delete Reason"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()

                Case "DELSC"
                    Dim objScreener As New clssucscreener_master
                    SetScreenerValue(objScreener)
                    objScreener._Isvoid = True
                    objScreener._Voidreason = delReason.Reason
                    objScreener._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objScreener._Voiduserunkid = CInt(Session("UserId"))
                    blnFlag = objScreener.Delete(mintScreenerUnkid)
                    If blnFlag = False AndAlso objScreener._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objScreener._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 11, "Screener deleted successfully."), Me)
                        FillScreener()
                    End If
                    objScreener = Nothing

                Case "DELRAT"
                    Dim objRatings As New clssucratings_master
                    SetRatingsValue(objRatings)
                    objRatings._Isvoid = True
                    objRatings._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objRatings._Voiduserunkid = CInt(Session("UserId"))
                    objRatings._Voidreason = delReason.Reason
                    blnFlag = objRatings.Delete(mintRatingsUnkid)
                    If blnFlag = False AndAlso objRatings._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objRatings._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 11, "Succession Ratings deleted successfully."), Me)
                        FillRatings()
                    End If
                    objRatings = Nothing

                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button's Event"
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStaging.ID, Me.lblStaging.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQualifyingCriteria.ID, Me.lblQualifyingCriteria.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblScreeners.ID, Me.lblScreeners.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQuestinnaire.ID, Me.lblQuestinnaire.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRatings.ID, Me.lblRatings.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.BtnClose.ID, Me.BtnClose.Text)

            'Staging Setup'
            'Language.setLanguage(mstrModuleName2)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblStages.ID, Me.lblStages.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblStageName.ID, Me.lblStageName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblFlowOrder.ID, Me.lblFlowOrder.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblPStage.ID, Me.lblPStage.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnSAdd.ID, Me.btnSAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnSReset.ID, Me.btnSReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvobjStaging.Columns(2).FooterText, gvobjStaging.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvobjStaging.Columns(3).FooterText, gvobjStaging.Columns(3).HeaderText)

            'Qualifying Criteria Setup'
            'Language.setLanguage(mstrModuleName3)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblQCriteria.ID, Me.lblQCriteria.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblminscore.ID, Me.lblminscore.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.Label2.ID, Me.Label2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblQPeriodNo.ID, Me.lblQPeriodNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblInstruction.ID, Me.lblInstruction.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkPerformanceScore.ID, Me.chkPerformanceScore.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkAllocationBy.ID, Me.chkAllocationBy.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkOrgYearNo.ID, Me.chkOrgYearNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMaxScreeners.ID, Me.chkQMaxScreeners.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMinScrReq.ID, Me.chkQMinScrReq.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMaxNomination.ID, Me.chkQMaxNomination.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQUserAccess.ID, Me.chkQUserAccess.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQTotalQueWeight.ID, Me.chkQTotalQueWeight.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQualificationlevel.ID, Me.chkQualificationlevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.chkQMaxAge.ID, Me.chkQMaxAge.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.btnQualifySave.ID, Me.btnQualifySave.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.rdbanyPeriod.ID, Me.rdbanyPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.rdballPeriod.ID, Me.rdballPeriod.Text)

            'Screeners Setup'
            'Language.setLanguage(mstrModuleName4)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblScsSetupTitle.ID, Me.lblScsSetupTitle.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblscreenerUser.ID, Me.lblscreenerUser.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblScsSetupTitle2.ID, Me.lblScsSetupTitle2.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.btnAddScreener.ID, Me.btnAddScreener.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, gvScreener.Columns(2).FooterText, gvScreener.Columns(2).HeaderText)

            'Questionnaire Setup'
            'Language.setLanguage(mstrModuleName5)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblDefineQuestions.ID, Me.lblDefineQuestions.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblQWeight.ID, Me.lblQWeight.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblQuestion.ID, Me.lblQuestion.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.btnQuestionAdd.ID, Me.btnQuestionAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.btnQuestionReset.ID, Me.btnQuestionReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, gvobjQuestionnaire.Columns(2).FooterText, gvobjQuestionnaire.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, gvobjQuestionnaire.Columns(3).FooterText, gvobjQuestionnaire.Columns(3).HeaderText)

            'Ratings Setup'
            'Language.setLanguage(mstrModuleName6)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRSetups.ID, Me.lblRSetups.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblScrFrom.ID, Me.lblScrFrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblScrTo.ID, Me.lblScrTo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblActionColor.ID, Me.lblActionColor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRAction.ID, Me.lblRAction.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblRDescription.ID, Me.lblRDescription.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.btnRAdd.ID, Me.btnRAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.btnRReset.ID, Me.btnRReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(2).FooterText, gvobjRatings.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(3).FooterText, gvobjRatings.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(4).FooterText, gvobjRatings.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(5).FooterText, gvobjRatings.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(6).FooterText, gvobjRatings.Columns(6).HeaderText)


            'Nomination Popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblnonomiationnote.ID, Me.lblnonomiationnote.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, dgvMaxNominateEmployee.Columns(0).FooterText, gvobjRatings.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, gvobjRatings.Columns(1).FooterText, gvobjRatings.Columns(1).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblStaging.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStaging.ID, Me.lblStaging.Text)
            Me.lblQualifyingCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualifyingCriteria.ID, Me.lblQualifyingCriteria.Text)
            Me.lblScreeners.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblScreeners.ID, Me.lblScreeners.Text)
            Me.lblQuestinnaire.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQuestinnaire.ID, Me.lblQuestinnaire.Text)
            Me.lblRatings.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRatings.ID, Me.lblRatings.Text)

            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

            'Staging Setup'
            'Language.setLanguage(mstrModuleName2)
            Me.lblStages.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblStages.ID, Me.lblStages.Text)
            Me.lblStageName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblStageName.ID, Me.lblStageName.Text)
            Me.lblFlowOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblFlowOrder.ID, Me.lblFlowOrder.Text)
            Me.lblPStage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblPStage.ID, Me.lblPStage.Text)

            Me.btnSAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnSAdd.ID, Me.btnSAdd.Text).Replace("&", "")
            Me.btnSReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnSReset.ID, Me.btnSReset.Text).Replace("&", "")

            gvobjStaging.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvobjStaging.Columns(2).FooterText, gvobjStaging.Columns(2).HeaderText)
            gvobjStaging.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvobjStaging.Columns(3).FooterText, gvobjStaging.Columns(3).HeaderText)


            'Qualifying Criteria Setup'
            'Language.setLanguage(mstrModuleName3)
            Me.lblQCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblQCriteria.ID, Me.lblQCriteria.Text)
            Me.lblminscore.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblminscore.ID, Me.lblminscore.Text)
            Me.Label2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.Label2.ID, Me.Label2.Text)
            Me.lblQPeriodNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblQPeriodNo.ID, Me.lblQPeriodNo.Text)
            Me.lblInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblInstruction.ID, Me.lblInstruction.Text)

            Me.chkPerformanceScore.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkPerformanceScore.ID, Me.chkPerformanceScore.Text)
            Me.chkAllocationBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkAllocationBy.ID, Me.chkAllocationBy.Text)
            Me.chkOrgYearNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkOrgYearNo.ID, Me.chkOrgYearNo.Text)
            Me.chkQMaxScreeners.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMaxScreeners.ID, Me.chkQMaxScreeners.Text)
            Me.chkQMinScrReq.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMinScrReq.ID, Me.chkQMinScrReq.Text)
            Me.chkQMaxNomination.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMaxNomination.ID, Me.chkQMaxNomination.Text)
            Me.chkQUserAccess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQUserAccess.ID, Me.chkQUserAccess.Text)
            Me.chkQTotalQueWeight.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQTotalQueWeight.ID, Me.chkQTotalQueWeight.Text)
            Me.chkQualificationlevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQualificationlevel.ID, Me.chkQualificationlevel.Text)
            Me.chkQMaxAge.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.chkQMaxAge.ID, Me.chkQMaxAge.Text)

            Me.btnQualifySave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.btnQualifySave.ID, Me.btnQualifySave.Text).Replace("&", "")

            Me.rdbanyPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.rdbanyPeriod.ID, Me.rdbanyPeriod.Text)
            Me.rdballPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.rdballPeriod.ID, Me.rdballPeriod.Text)

            'Screeners Setup'
            'Language.setLanguage(mstrModuleName4)
            Me.lblScsSetupTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblScsSetupTitle.ID, Me.lblScsSetupTitle.Text)
            Me.lblscreenerUser.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblscreenerUser.ID, Me.lblscreenerUser.Text)
            Me.lblScsSetupTitle2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblScsSetupTitle2.ID, Me.lblScsSetupTitle2.Text)

            Me.btnAddScreener.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.btnAddScreener.ID, Me.btnAddScreener.Text).Replace("&", "")

            gvScreener.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), gvScreener.Columns(2).FooterText, gvScreener.Columns(2).HeaderText)

            'Questionnaire Setup'
            'Language.setLanguage(mstrModuleName5)
            Me.lblDefineQuestions.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblDefineQuestions.ID, Me.lblDefineQuestions.Text)
            Me.lblQWeight.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblQWeight.ID, Me.lblQWeight.Text)
            Me.lblQuestion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblQuestion.ID, Me.lblQuestion.Text)

            Me.btnQuestionAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.btnQuestionAdd.ID, Me.btnQuestionAdd.Text).Replace("&", "")
            Me.btnQuestionReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.btnQuestionReset.ID, Me.btnQuestionReset.Text).Replace("&", "")

            gvobjQuestionnaire.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), gvobjQuestionnaire.Columns(2).FooterText, gvobjQuestionnaire.Columns(2).HeaderText)
            gvobjQuestionnaire.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), gvobjQuestionnaire.Columns(3).FooterText, gvobjQuestionnaire.Columns(3).HeaderText)

            'Ratings Setup'
            'Language.setLanguage(mstrModuleName6)
            Me.lblRSetups.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRSetups.ID, Me.lblRSetups.Text)
            Me.lblScrFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblScrFrom.ID, Me.lblScrFrom.Text)
            Me.lblScrTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblScrTo.ID, Me.lblScrTo.Text)
            Me.lblActionColor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblActionColor.ID, Me.lblActionColor.Text)
            Me.lblRAction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRAction.ID, Me.lblRAction.Text)
            Me.lblRDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblRDescription.ID, Me.lblRDescription.Text)

            Me.btnRAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.btnRAdd.ID, Me.btnRAdd.Text).Replace("&", "")
            Me.btnRReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.btnRReset.ID, Me.btnRReset.Text).Replace("&", "")

            gvobjRatings.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(2).FooterText, gvobjRatings.Columns(2).HeaderText)
            gvobjRatings.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(3).FooterText, gvobjRatings.Columns(3).HeaderText)
            gvobjRatings.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(4).FooterText, gvobjRatings.Columns(4).HeaderText)
            gvobjRatings.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(5).FooterText, gvobjRatings.Columns(5).HeaderText)
            gvobjRatings.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), gvobjRatings.Columns(6).FooterText, gvobjRatings.Columns(6).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 2, "Sorry, Stage Name is mandatory information. Please enter Stage Name to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 3, "Sorry, Stage is mandatory information. Please select Stage to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 4, "Sorry, You can't Add")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 5, " of")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 6, " Stage")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 7, "Sorry,You can't Add Stage on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 8, "Succession Staging defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 9, "You are about to delete this Stage. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 10, "Sorry, You can not delete this entry, Reason:This is auto generated entry from Succession.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 11, "Succession Stage deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 12, "Sorry you can't Add this Stage,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 13, "Sorry you can't edit this cycle,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 14, "Sorry you can't delete this cycle,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 2, "Sorry, Minumum Score value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 3, "Sorry, Please select one option in apply minimum score to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 4, "Sorry, Maximum age value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 5, "Sorry, Number of year with organization should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 6, "Sorry, Maximum number of screener should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 7, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 8, "Sorry, Total question weight should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 9, "Sorry, Please select at lease one allocation to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 10, "Sorry, Please select at lease one allocation to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 11, "Qualification Setting Save successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 12, "Please check atleast one allocation from list to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 13, "Sorry, Minimum number of screening required should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 14, "Sorry you can't change setting for this cycle,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 15, "Please enter Instruction.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 16, "Sorry, Qualification level should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 1, "Sorry, User is mandatory information. Please select user to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 2, "Screener Save successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 3, "You are about to delete this screener. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 4, "Screener delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 6, "Sorry, Your Total Screeners limit exceeded.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 7, "You are about to inactive this screener. Are you sure you want to inactive?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 8, "You are about to active this screener. Are you sure you want to active?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 9, "Screener Inactivated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 10, "Screener Activated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 11, "Screener deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 12, "Sorry you can't add this screener,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 13, "Sorry you can't delete this screener,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 14, "Sorry you can't inactive this screener,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 15, "Sorry you can't active this screener,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 16, "Sorry you can't add screener,Reason: Succession Setting is not define.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 2, "Sorry, Weight is mandatory information. Please enter Weight to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 3, "Sorry, Question is mandatory information. Please enter Question to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 4, "Succession Questionnaire defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 5, "You are about to delete this Question. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 6, "Succession Questionnaire deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 7, "Sorry, Your Total question weight limit exceeded.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 8, "Sorry you can't add new question,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 10, "Sorry you can't delete this Question,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 11, "Sorry you can't add question,Reason: Succession Setting is not define.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 2, "Sorry, Score From is mandatory information. Please enter Score From to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 3, "Sorry, Score To is mandatory information. Please enter Score To to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 4, "Sorry, Description is mandatory information. Please enter Description to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 5, "Sorry, Action is mandatory information. Please select Action to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 6, "Sorry, Score from  should not be greater than Score To.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 7, "Sorry, Score from  or Score To is already defined Range.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 8, "Sorry, Score from  and Score To is not valid defined Range.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 9, "Succession Ratings defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 10, "You are about to delete this Rating. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 11, "Succession Ratings deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 12, "Rating delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 13, "Sorry you can't add rating,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 15, "Sorry you can't delete this rating,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 16, "Sorry you can't edit this Question,Reason: Succession Process is already started for this cycle.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 17, "Sorry you can't add rating,Reason: Succession Setting is not define.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>



End Class
