﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpg_TalentProfile.aspx.vb"
    Inherits="Talent_Succession_wpg_TalentProfile" Title="Talent Profile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblheader2" runat="server" Text="Talent Profile"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                <asp:Label ID="lblheader3" runat="server" Text="Employee Detail"></asp:Label>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="row clearfix d--f ai--c">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
                                                    <asp:Image ID="imgEmployeeProfilePic" runat="server" Width="150px" Height="150px"
                                                        Style="border-radius: 50%" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                                            <asp:Label ID="txtName" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblJob" runat="server" Text="Current Role" CssClass="form-label"></asp:Label>
                                                            <asp:Label ID="txtJob" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblreportto" runat="server" Text="Line Manager" CssClass="form-label"></asp:Label>
                                                            <asp:Label ID="txtreportto" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="divider">
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblLocation" runat="server" Text="Location" CssClass="form-label"></asp:Label>
                                                            <asp:Label ID="txtLocation" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                            <asp:Label ID="txtDepartment" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblJobLevel" runat="server" Text="Job Level" CssClass="form-label"></asp:Label>
                                                            <asp:Label ID="txtJobLevel" runat="server" Text="Label"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnViewPDP" runat="server" Text="Open Talent Development Plan" CssClass="btn btn-default pull-left" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPreviousScreenersResponse" runat="server" Text="Previous Screener(s) Response"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 400px;">
                                        <asp:Panel ID="pnlnoperviousData" runat="server" CssClass="alert alert-danger">
                                            <asp:Label ID="lblnoperviousData" Text="Previous Screener(s) Response Not Availabel"
                                                runat="server" />
                                        </asp:Panel>
                                        <asp:DataList ID="rptOtherScreeners" runat="server" Width="100%">
                                            <ItemTemplate>
                                                <asp:Panel ID="pnlOtherScreeners" runat="server" aria-multiselectable="true" CssClass="panel-group full-body">
                                                    <div class="panel" style="border: 1px solid #ddd">
                                                        <div class="panel-heading" role="tab" id="headingOne_<%# Eval("screenermstunkid") %>">
                                                            <h4 class="panel-title d--f ai--c jc--sb">
                                                                <a role="button" data-toggle="collapse" href="#collapseOne_<%# Eval("screenermstunkid") %>"
                                                                    aria-expanded="false" aria-controls="collapseOne_<%# Eval("screenermstunkid") %>"
                                                                    style="flex: 1">
                                                                    <asp:Label ID="lblOtherScreenerName" CssClass="text-primary" Text='<%# Eval("screener") %>'
                                                                        runat="server" />
                                                                </a>
                                                                <asp:Label ID="lblpoint" CssClass="label label-warning m-r-10" Text='<%# Eval("totalpoint") %>'
                                                                    runat="server" />
                                                                <asp:Label ID="lblStatus" CssClass="label label-primary" Text='<%# Eval("stage_name") %>'
                                                                    runat="server" />
                                                                <asp:HiddenField ID="hfscreenermstunkid" runat="server" Value='<%# Eval("screenermstunkid") %>' />
                                                                <asp:HiddenField ID="hfprocessmstunkid" runat="server" Value='<%# Eval("processmstunkid") %>' />
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne_<%# Eval("screenermstunkid") %>" class="panel-collapse collapse"
                                                            role="tabpanel" aria-labelledby="headingOne_<%# Eval("screenermstunkid") %>">
                                                            <div class="panel-body" style="max-height: 300px">
                                                                <asp:DataList ID="rptOtherScreenerResponse" runat="server" OnItemDataBound="rptOtherScreenerResponse_ItemDataBound" Width="100%">
                                                                    <ItemTemplate>
                                                                        <div class="card inner-card m-b-10">
                                                                            <div class="body">
                                                                                <div class="row clearfix d--f ai--c">
                                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("question") %>' CssClass="form-label"></asp:Label>
                                                                                        <asp:HiddenField ID="hfOtherScreenerResponse" runat="server" Value='<%# Eval("result") %>' />
                                                                                    </div>
                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                        <asp:RadioButton ID="rdbYes" runat="server" Text="Yes" Checked="true" GroupName="question"
                                                                                            Enabled="false" />
                                                                                        <asp:RadioButton ID="rdbNo" runat="server" Text="No" GroupName="question" Enabled="false" />
                                                                                    </div>
                                                                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtRemark" placeholder="Screener Remark" runat="server" Text='<%# Eval("remark") %>'
                                                                                                    CssClass="form-control" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix" style="display:none">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblTalentPerformance" runat="server" Text="Talent Performance"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 400px;">
                                        <asp:GridView ID="gvTalentPerformance" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-hover table-bordered" AllowPaging="false">
                                            <Columns>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix" style="display:none">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblTalentProgress" runat="server" Text="Talent Progress"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 400px;">
                                        <asp:GridView ID="gvTalentProgress" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                            AllowPaging="false">
                                            <Columns>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
