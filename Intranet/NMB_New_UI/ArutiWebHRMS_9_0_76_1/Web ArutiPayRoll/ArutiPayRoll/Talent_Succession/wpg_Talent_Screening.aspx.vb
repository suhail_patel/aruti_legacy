﻿Imports System.Data
Imports Aruti.Data
Imports System.IO

Partial Class Talent_Succession_wpg_Talent_Screening
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTalentScreening"
    Private DisplayMessage As New CommonCodes
    Private mlistEmployeeIds As String() = Nothing
    Private mintCurrentEmpId As Integer = 0
    Private mintCycleId As Integer = 0
    Private mdtQuestionnaire As DataTable
    Private mintProcessmstunkid As Integer = -1
    Private mintScreenermstunkid As Integer = -1
    Private mdblTotalWeight As Double = 0

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                mintCycleId = Session("tl_screening_cycle_id")
                If IsNothing(Session("tl_screening_employee_id")) = False Then
                    Dim mstrEmplist As String = Session("tl_screening_employee_id")
                    mlistEmployeeIds = mstrEmplist.Split(",")
                End If

                Dim dslist As New DataSet
                Dim objtlscreener_master As New clstlscreener_master

                dslist = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))
                If IsNothing(dslist) = False AndAlso dslist.Tables(0).Rows.Count > 0 Then
                    mintScreenermstunkid = CInt(dslist.Tables(0).Rows(0)("screenermstunkid"))
                End If

                Dim objtlsettings_master As New clstlsettings_master
                mdblTotalWeight = CDbl(objtlsettings_master.GetSettingValueFromKey(mintCycleId, clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT))
                objtlsettings_master = Nothing

                GetTlSetting()
                FillQuestionnaire()
                SetNextEmployee()
            Else
                If IsNothing(ViewState("mlistEmployeeIds")) = False Then
                    mlistEmployeeIds = ViewState("mlistEmployeeIds")
                End If

                If IsNothing(ViewState("mintCurrentEmpId")) = False Then
                    mintCurrentEmpId = CInt(ViewState("mintCurrentEmpId"))
                End If

                If IsNothing(ViewState("mintCycleId")) = False Then
                    mintCycleId = CInt(ViewState("mintCycleId"))
                End If

                If IsNothing(ViewState("mintScreenermstunkid")) = False Then
                    mintScreenermstunkid = CInt(ViewState("mintScreenermstunkid"))
                End If

                If IsNothing(ViewState("mintProcessmstunkid")) = False Then
                    mintProcessmstunkid = CInt(ViewState("mintProcessmstunkid"))
                End If

                If IsNothing(ViewState("mdblTotalWeight")) = False Then
                    mdblTotalWeight = CDbl(ViewState("mdblTotalWeight"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mlistEmployeeIds") = mlistEmployeeIds
            Me.ViewState("mintCycleId") = mintCycleId
            Me.ViewState("mintCurrentEmpId") = mintCurrentEmpId
            Me.ViewState("mintScreenermstunkid") = mintScreenermstunkid
            Me.ViewState("mintProcessmstunkid") = mintProcessmstunkid
            Me.ViewState("mdblTotalWeight") = mdblTotalWeight
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"
    Private Sub FillQuestionnaire()
        Dim objtlquestionnaire_master As New clstlquestionnaire_master
        Dim dsList As New DataSet
        objtlquestionnaire_master._DatabaseName = CStr(Session("Database_Name"))

        Try
            dsList = objtlquestionnaire_master.GetQuestionListFromCycle("QuestionList", mintCycleId)
            If dsList.Tables("QuestionList").Rows.Count <= 0 Then
                Exit Sub
            End If
            rptQuestionnaire.DataSource = dsList.Tables("QuestionList")
            rptQuestionnaire.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlquestionnaire_master = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
        End Try
    End Sub

    Private Sub GetTlSetting()
        Dim objtlsettings_master As New clstlsettings_master
        Try
            Dim mdicSetting As Dictionary(Of clstlsettings_master.enTalentConfiguration, String) = objtlsettings_master.GetSettingFromPeriod(mintCycleId)
            If IsNothing(mdicSetting) = False Then
                'If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT) Then
                '    txtmaxpoint.Text = mdicSetting(clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT)
                'End If

                'Sohail (04 Nov 2020) -- Start
                'NMB Issue # : - Error Cannot insert duplicate key in applicant code column on import data in recruitment.
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.INSTRUCTION) Then
                    txtScreeningInstruction.Text = mdicSetting(clstlsettings_master.enTalentConfiguration.INSTRUCTION)
                End If
                'Sohail (04 Nov 2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlsettings_master = Nothing
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Private Sub FillDetail()
        Dim objEmployee As New clsEmployee_Master
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreener_master As New clstlscreener_master
        Dim objEmpQuali As New clsEmp_Qualification_Tran 'Sohail (03 Nov 2020)

        Dim objEmployee_transfer_tran As New clsemployee_transfer_tran
        Dim objEmployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim objSectionGroup As New clsSectionGroup
        Dim objClass As New clsClass

        objtlscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))
        objtlscreening_process_tran._DatabaseName = CStr(Session("Database_Name"))
        objtlscreener_master._DatabaseName = CStr(Session("Database_Name"))


        Dim mintScreenerId As Integer = -1
        Try
            ClearControl()
            'Check Old Data Available or not
            Dim dsScreener As DataSet = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))

            If IsNothing(dsScreener) = False AndAlso dsScreener.Tables(0).Rows.Count > 0 Then
                mintScreenerId = CInt(dsScreener.Tables(0).Rows(0)("screenermstunkid"))
            End If
            If objtlscreening_stages_tran.isExist(mintCycleId, mintCurrentEmpId, mintScreenerId) Then

                Dim dsScreening As DataSet = objtlscreening_stages_tran.GetList("Screening", mintCycleId, mintCurrentEmpId, mintScreenerId)
                If dsScreening.Tables("Screening").Rows.Count > 0 Then
                    lblColor.Visible = True
                    lblColor.BackColor = Drawing.ColorTranslator.FromHtml(dsScreening.Tables("Screening").Rows(0)("color").ToString())

                    txtscreenoutcome.Text = dsScreening.Tables("Screening").Rows(0)("stage_name")
                    mintProcessmstunkid = CInt(dsScreening.Tables("Screening").Rows(0)("processmstunkid"))
                    If objtlscreening_process_tran.isExist(CInt(dsScreening.Tables("Screening").Rows(0)("processmstunkid")), mintScreenerId) Then
                        Dim dsQue As DataSet = objtlscreening_process_tran.GetList("Que", CInt(dsScreening.Tables("Screening").Rows(0)("processmstunkid")), mintScreenerId)

                        If IsNothing(dsQue) = False AndAlso dsQue.Tables("Que").Rows.Count > 0 Then
                            For Each rptItem As DataListItem In rptQuestionnaire.Items
                                Dim rdbYes As RadioButton = CType(rptItem.FindControl("rdbYes"), RadioButton)
                                Dim rdbNo As RadioButton = CType(rptItem.FindControl("rdbNo"), RadioButton)
                                Dim txtRemark As TextBox = CType(rptItem.FindControl("txtRemark"), TextBox)
                                Dim hfquestionnaireunkid As HiddenField = CType(rptItem.FindControl("hfquestionnaireunkid"), HiddenField)

                                Dim drow As DataRow = dsQue.Tables("que").AsEnumerable.Where(Function(x) x.Field(Of Integer)("questionnaireunkid") = CInt(hfquestionnaireunkid.Value)).FirstOrDefault()
                                If IsNothing(drow) = False Then
                                    If CDbl(drow("result")) > 0 Then
                                        rdbYes.Checked = True
                                    Else
                                        rdbNo.Checked = True
                                    End If
                                    txtRemark.Text = drow("remark").ToString()
                                End If
                            Next
                        End If
                    End If
                End If
            End If

            'Fill Employee Detail
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintCurrentEmpId
            'txtName.Text = objEmployee._Surname + " " + objEmployee._Firstname

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Class_Group & "," & clsEmployee_Master.EmpColEnum.Col_Class & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Group & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Level & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Grade & "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type


            'Dim dsEmpDetail As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, CStr(Session("Database_Name")), _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    CStr(Session("UserAccessModeSetting")), True, _
            '                                    CBool(Session("IsIncludeInactiveEmp")), "List", mintCurrentEmpId)

            'If dsEmpDetail.Tables(0).Rows.Count > 0 Then
            '    txtName.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 46, "Employee Name"))))
            '    txtJob.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 118, "Job") = "", "Job", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 118, "Job"))))
            '    txtDepartment.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 210, "Section Group") = "", "section group", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 210, "Section Group"))))
            '    txtLocation.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 205, "Class") = "", "class", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 205, "Class"))))
            'End If


            txtName.Text = objEmployee._Firstname + " " + objEmployee._Othername + " " + objEmployee._Surname
            Dim dsEmloyeeAllocation As DataSet
            Dim dsEmloyeeJob As DataSet
            dsEmloyeeAllocation = objEmployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mintCurrentEmpId)
            dsEmloyeeJob = objEmployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mintCurrentEmpId)
            If IsNothing(dsEmloyeeJob) = False AndAlso dsEmloyeeJob.Tables(0).Rows.Count > 0 Then
                txtJob.Text = dsEmloyeeJob.Tables(0).Rows(0)("Job").ToString()
            End If
            If IsNothing(dsEmloyeeAllocation) = False AndAlso dsEmloyeeAllocation.Tables(0).Rows.Count > 0 Then
                objSectionGroup._Sectiongroupunkid = CInt(dsEmloyeeAllocation.Tables(0).Rows(0)("sectiongroupunkid"))
                txtDepartment.Text = objSectionGroup._Name
                objClass._Classesunkid = CInt(dsEmloyeeAllocation.Tables(0).Rows(0)("classunkid"))
                txtLocation.Text = objClass._Name
            End If


            Dim strNoimage As String = "data:image/png;base64," & ImageToBase64()

            If objEmployee._blnImgInDb Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    If objEmployee._Photo IsNot Nothing Then
                        imgEmployeeProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                    Else
                        imgEmployeeProfilePic.ImageUrl = strNoimage
                    End If
                End If
            Else
                imgEmployeeProfilePic.ImageUrl = strNoimage
            End If

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, mintCurrentEmpId, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            If dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0).Rows(0)("Employee Code").ToString().ToString().Length > 0 Then
                Dim drrow As DataRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishierarchy") = True).FirstOrDefault()
                txtreportto.Text = drrow("Employee")
            End If

            'Sohail (03 Nov 2020) -- Start
            'NMB Enhancement : # : Pick last qualification name on Talent Screening screen.
            dsList = clsEmp_Qualification_Tran.GetEmployeeQualification(mintCurrentEmpId, False, " CONVERT(CHAR(8), ISNULL(hremp_qualification_tran.award_end_date, GETDATE()), 112) DESC ")
            If dsList.Tables(0).Rows.Count > 0 Then
                txtQualification.Text = CStr(dsList.Tables(0).Rows(0).Item("Name"))
            Else
                txtQualification.Text = ""
            End If
            'Sohail (03 Nov 2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objtlscreening_stages_tran = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreener_master = Nothing

        End Try
    End Sub

    Private Sub SetNextEmployee()
        Try
            If mintCurrentEmpId <= 0 Then
                mintCurrentEmpId = mlistEmployeeIds(0)
                btnPrevious.Visible = False
            Else
                mintCurrentEmpId = mlistEmployeeIds.ToList()(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1).ToString()
                btnPrevious.Visible = True
            End If

            If mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) <= 0 Then
                btnPrevious.Visible = False
            End If

            If CInt(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1) = mlistEmployeeIds.Count Then
                btnNext.Visible = False
                btnSave.Visible = True
            Else
                btnNext.Visible = True
                btnSave.Visible = False

            End If

            lblemployeecount.Text = (mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1).ToString() + " / " + mlistEmployeeIds.Count.ToString()
            FillDetail()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetPreviousEmployee()
        Try

            If mintCurrentEmpId <= 0 Then
                mintCurrentEmpId = mlistEmployeeIds(0)
            Else
                mintCurrentEmpId = mlistEmployeeIds.ToList()(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) - 1).ToString()
            End If


            If CInt(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1) = mlistEmployeeIds.Count Then
                btnNext.Visible = False
                btnSave.Visible = True
            Else
                btnNext.Visible = True
                btnSave.Visible = False
            End If

            If mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) <= 0 Then
                btnPrevious.Visible = False
            End If


            lblemployeecount.Text = (mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1).ToString() + " / " + mlistEmployeeIds.Count.ToString()

            FillDetail()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function setProcessMasterValue(ByVal objtlscreening_process_master As clstlscreening_process_master) As Boolean
        Dim objtlscreener_master As New clstlscreener_master
        Dim objtlratings_master As New clstlratings_master
        Dim dsList As DataSet = Nothing
        Dim dsScore As DataSet = Nothing
        Try
            objtlscreening_process_master._Cycleunkid = mintCycleId
            objtlscreening_process_master._Employeeunkid = mintCurrentEmpId
            objtlscreening_process_master._Ismatch = True
            objtlscreening_process_master._Isvoid = False
            objtlscreening_process_master._Userunkid = CInt(Session("UserId"))

            objtlscreening_process_master._AuditUserId = CInt(Session("UserId"))
            objtlscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
            objtlscreening_process_master._FormName = mstrModuleName
            objtlscreening_process_master._FromWeb = True
            objtlscreening_process_master._HostName = CStr(Session("HOST_NAME"))
            objtlscreening_process_master._DatabaseName = CStr(Session("Database_Name"))

            mdtQuestionnaire = objtlscreening_process_master._DtQuestionnaire.Copy

            For Each rptItem As DataListItem In rptQuestionnaire.Items
                Dim dtquestion As DataRow
                dtquestion = mdtQuestionnaire.NewRow
                Dim hfweight As HiddenField = CType(rptItem.FindControl("hfweight"), HiddenField)
                Dim rdbYes As RadioButton = CType(rptItem.FindControl("rdbYes"), RadioButton)
                Dim txtRemark As TextBox = CType(rptItem.FindControl("txtRemark"), TextBox)

                Dim hfquestionnaireunkid As HiddenField = CType(rptItem.FindControl("hfquestionnaireunkid"), HiddenField)
                dtquestion.Item("questionnaireunkid") = hfquestionnaireunkid.Value

                If rdbYes.Checked Then
                    dtquestion.Item("result") = hfweight.Value
                Else
                    dtquestion.Item("result") = 0
                End If
                dtquestion.Item("remark") = txtRemark.Text
                mdtQuestionnaire.Rows.Add(dtquestion)
            Next

            objtlscreening_process_master._DtQuestionnaire = mdtQuestionnaire

            objtlscreening_process_master._Screenermstunkid = mintScreenermstunkid

            dsScore = objtlratings_master.GetAvgScore("score", mintCycleId, mintCurrentEmpId, mintScreenermstunkid, mintProcessmstunkid)
            Dim oldScore As Double = 0
            Dim TotalScreener As Integer = 1

            If IsNothing(dsScore) = False AndAlso dsScore.Tables(0).Rows.Count > 0 Then
                oldScore = dsScore.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Decimal)("totalScore")).ToList().Sum(Function(y) y)
                TotalScreener += dsScore.Tables(0).Rows.Count
            End If

            Dim TotalScore As Double = (oldScore + rptQuestionnaire.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("rdbYes"), RadioButton).Checked = True).Sum(Function(y) CDbl(CType(y.FindControl("hfweight"), HiddenField).Value))) / TotalScreener


            Dim currentScreenerScore As Double = rptQuestionnaire.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("rdbYes"), RadioButton).Checked = True).Sum(Function(y) CDbl(CType(y.FindControl("hfweight"), HiddenField).Value))


            Dim dtavgscreener As DataSet = objtlratings_master.GetStageFromScore("avgStage", mintCycleId, TotalScore)

            Dim dtscreener As DataSet = objtlratings_master.GetStageFromScore("Stage", mintCycleId, currentScreenerScore)


            If IsNothing(dtavgscreener) = False AndAlso dtavgscreener.Tables("avgStage").Rows.Count > 0 Then
                objtlscreening_process_master._AverageScreenermstunkid = dtavgscreener.Tables("avgStage").Rows(0)("stageunkid").ToString()
            Else
                objtlscreening_process_master._AverageScreenermstunkid = -1
            End If


            If IsNothing(dtscreener) = False AndAlso dtscreener.Tables("Stage").Rows.Count > 0 Then
                objtlscreening_process_master._Stageunkid = dtscreener.Tables("Stage").Rows(0)("stageunkid").ToString()
            Else
                objtlscreening_process_master._Stageunkid = -1
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this rating is not define in based on your score."), Me.Page)
                Return False
            End If
            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreener_master = Nothing
            objtlratings_master = Nothing
            If IsNothing(dsScore) = False Then
                dsScore.Tables(0).Clear()
                dsScore = Nothing
            End If
        End Try
    End Function

    Private Function IsValidate() As Boolean
        Try
            Dim gRow As IEnumerable(Of DataListItem) = Nothing
            Dim lstIDs As List(Of String) = Nothing
            Dim allocation_Id As String = ""

            gRow = rptQuestionnaire.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("rdbYes"), RadioButton).Checked = False And CType(x.FindControl("rdbNo"), RadioButton).Checked = False)

            If gRow.Count > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all question response is mandatory. please attempt all question to continue."), Me.Page)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub ClearControl()
        Try
            For Each rptItem As DataListItem In rptQuestionnaire.Items
                Dim rdbYes As RadioButton = CType(rptItem.FindControl("rdbYes"), RadioButton)
                Dim rdbNo As RadioButton = CType(rptItem.FindControl("rdbNo"), RadioButton)
                Dim txtRemark As TextBox = CType(rptItem.FindControl("txtRemark"), TextBox)
                rdbYes.Checked = False
                rdbNo.Checked = False
                txtRemark.Text = ""
            Next

            txtscreenoutcome.Text = ""
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Buttons Event"
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click, btnSave.Click
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            If setProcessMasterValue(objtlscreening_process_master) = False Then
                Exit Sub
            End If

            If objtlscreening_process_master.SaveScreening() = False Then
                DisplayMessage.DisplayMessage(objtlscreening_process_master._Message, Me)
                Exit Sub
            End If

            If btnSave.Visible Then
                FillDetail()
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Screening Complete Successfully."), Me)
                Exit Sub
            End If

            If btnNext.Visible Then
                SetNextEmployee()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Try
            SetPreviousEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Talent_Succession/wpg_TalentPipeline.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkOtherScreenerResponse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOtherScreenerResponse.Click
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran
        Dim objtlscreener_master As New clstlscreener_master
        objtlscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))

        Dim dsScreenerList As New DataSet
        Dim dsList As New DataSet
        Try

            dsList = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))

            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dsScreenerList = objtlscreening_stages_tran.GetOtherScreenerData("ScreenerDetail", mintCycleId, mintCurrentEmpId, CInt(dsList.Tables(0).Rows(0)("screenermstunkid")))
            Else
                dsScreenerList = objtlscreening_stages_tran.GetOtherScreenerData("ScreenerDetail", mintCycleId, mintCurrentEmpId, -1)
            End If

            If IsNothing(dsScreenerList) = False AndAlso dsScreenerList.Tables(0).Rows.Count > 0 Then
                pnlnoperviousData.Visible = False
                rptOtherScreeners.DataSource = dsScreenerList
                rptOtherScreeners.DataBind()
            Else
                pnlnoperviousData.Visible = True
            End If

            popupOtherScreenerResponse.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_stages_tran = Nothing
        End Try
    End Sub
#End Region

#Region "Radio Button Event"
    Protected Sub rdbQuestinResponse_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreener_master As New clstlscreener_master
        Dim objtlratings_master As New clstlratings_master
        Dim objtlscreening_process_master As New clstlscreening_process_master
        objtlratings_master._DatabaseName = CStr(Session("Database_Name"))

        Dim dsList As DataSet = Nothing
        Dim dsScore As DataSet = Nothing

        Try
            dsList = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))

            dsScore = objtlratings_master.GetAvgScore("score", mintCycleId, mintCurrentEmpId, CInt(dsList.Tables(0).Rows(0)("screenermstunkid")), mintProcessmstunkid)
            Dim oldScore As Double = 0
            Dim TotalScreener As Integer = 1

            If IsNothing(dsScore) = False AndAlso dsScore.Tables(0).Rows.Count > 0 Then
                oldScore = dsScore.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Decimal)("totalScore")).ToList().Sum(Function(y) y)
                TotalScreener += dsScore.Tables(0).Rows.Count
            End If

            Dim TotalScore As Double = (oldScore + rptQuestionnaire.Items.Cast(Of DataListItem).Where(Function(x) CType(x.FindControl("rdbYes"), RadioButton).Checked = True).Sum(Function(y) CDbl(CType(y.FindControl("hfweight"), HiddenField).Value))) / TotalScreener
            Dim dsscreener As DataSet = objtlratings_master.GetStageFromScore("Stage", mintCycleId, TotalScore)


            If IsNothing(dsscreener) = False AndAlso dsscreener.Tables("Stage").Rows.Count > 0 Then
                txtscreenoutcome.Text = dsscreener.Tables("Stage").Rows(0)("stage_name").ToString()
            Else
                objtlscreening_process_master._Stageunkid = -1
            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreener_master = Nothing
            objtlratings_master = Nothing
            objtlscreening_process_master = Nothing
        End Try
    End Sub

#End Region

#Region "DataList Event"
    Protected Sub rptOtherScreeners_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles rptOtherScreeners.ItemDataBound
        Dim dsList As New DataSet
        Dim objtlscreening_process_tran As New clstlscreening_process_tran

        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim hfprocessmstunkid As HiddenField = CType(e.Item.FindControl("hfprocessmstunkid"), HiddenField)
                Dim hfscreenermstunkid As HiddenField = CType(e.Item.FindControl("hfscreenermstunkid"), HiddenField)
                Dim lblpoint As Label = CType(e.Item.FindControl("lblpoint"), Label)
                Dim rptOtherScreenerResponse As DataList = TryCast(e.Item.FindControl("rptOtherScreenerResponse"), DataList)
                'lblpoint.Text = lblpoint.Text + " / " + mdblTotalWeight.ToString()

                dsList = objtlscreening_process_tran.GetOtherScreenerData("ResponseDetail", mintCycleId, hfprocessmstunkid.Value, hfscreenermstunkid.Value, mintScreenermstunkid)

                rptOtherScreenerResponse.DataSource = dsList.Tables("ResponseDetail")
                rptOtherScreenerResponse.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_tran = Nothing
        End Try
    End Sub


    Protected Sub rptOtherScreenerResponse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim hfOtherScreenerResponse As HiddenField = CType(e.Item.FindControl("hfOtherScreenerResponse"), HiddenField)

                Dim rdbYes As RadioButton = CType(e.Item.FindControl("rdbYes"), RadioButton)
                Dim rdbNo As RadioButton = CType(e.Item.FindControl("rdbNo"), RadioButton)
                Dim txtRemark As TextBox = CType(e.Item.FindControl("txtRemark"), TextBox)


                If hfOtherScreenerResponse.Value > 0 Then
                    rdbYes.Checked = True
                Else
                    rdbNo.Checked = True
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    'Sohail (03 Nov 2020) -- Start
    'NMB Enhancement : # : add insruction textbox in qualifying setting it should be mandatory information.
#Region " Links Events "
    Protected Sub lnkScreeningInstruction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkScreeningInstruction.Click
        Try
            popupScreeningInstruction.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Sohail (03 Nov 2020) -- End

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblHeader.ID, Me.lblHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblheader2.ID, Me.lblheader2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblheader3.ID, Me.lblheader3.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblName.ID, Me.lblName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblJob.ID, Me.lblJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblreportto.ID, Me.lblreportto.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLocation.ID, Me.lblLocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDepartment.ID, Me.lblDepartment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQualification.ID, Me.lblQualification.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lbl.ID, Me.lbl.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblscreenoutcome.ID, Me.lblscreenoutcome.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.Label17.ID, Me.Label17.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopupheader.ID, Me.lblPopupheader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblnoperviousData.ID, Me.lblnoperviousData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblScreeningInstruction.ID, Me.lblScreeningInstruction.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnPrevious.ID, Me.btnPrevious.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSave.ID, Me.btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNext.ID, Me.btnNext.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCloseGrePreviousResponse.ID, Me.btnCloseGrePreviousResponse.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnScreeningInstructionClose.ID, Me.btnScreeningInstructionClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkOtherScreenerResponse.ID, Me.lnkOtherScreenerResponse.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkScreeningInstruction.ID, Me.lnkScreeningInstruction.ToolTip)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblHeader.ID, Me.lblHeader.Text)
            Me.lblheader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblheader2.ID, Me.lblheader2.Text)
            Me.lblheader3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblheader3.ID, Me.lblheader3.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblreportto.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblreportto.ID, Me.lblreportto.Text)
            Me.lblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLocation.ID, Me.lblLocation.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualification.ID, Me.lblQualification.Text)
            Me.lbl.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lbl.ID, Me.lbl.Text)
            Me.lblscreenoutcome.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblscreenoutcome.ID, Me.lblscreenoutcome.Text)
            Me.Label17.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.Label17.ID, Me.Label17.Text)
            Me.lblPopupheader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopupheader.ID, Me.lblPopupheader.Text)
            Me.lblnoperviousData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblnoperviousData.ID, Me.lblnoperviousData.Text)
            Me.lblScreeningInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblScreeningInstruction.ID, Me.lblScreeningInstruction.Text)

            Me.btnPrevious.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnPrevious.ID, Me.btnPrevious.Text).Replace("&", "")
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnNext.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNext.ID, Me.btnNext.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnCloseGrePreviousResponse.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseGrePreviousResponse.ID, Me.btnCloseGrePreviousResponse.Text).Replace("&", "")
            Me.btnScreeningInstructionClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnScreeningInstructionClose.ID, Me.btnScreeningInstructionClose.Text).Replace("&", "")

            Me.lnkOtherScreenerResponse.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkOtherScreenerResponse.ID, Me.lnkOtherScreenerResponse.ToolTip)
            Me.lnkScreeningInstruction.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkScreeningInstruction.ID, Me.lnkScreeningInstruction.ToolTip)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, this rating is not define in based on your score.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, all question response is mandatory. please attempt all question to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Screening Complete Successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
