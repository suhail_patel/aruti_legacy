﻿Imports Aruti.Data
Imports System.Data
Imports System.IO

Partial Class Talent_Succession_wpg_TalentApprovedDisapprove
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTalentApproveReject"
    Private DisplayMessage As New CommonCodes
    Private mlistProcessIds As String() = Nothing
    Private mlistEmployeeIds As String() = Nothing
    Private mintCurrentEmpId As Integer = 0
    Private mintCycleId As Integer = 0
    Private mintProcessmstunkid As Integer = -1
    Private mstrEmailAction As String = ""
    Private mblnisLastProcessid As Boolean = False
    Private mdblTotalWeight As Double = 0
    Private menumOprationType As clstlpipeline_master.enTlApproveRejectFormType
    Private mintPotentialTalentTranunkid As Integer = -1

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                menumOprationType = CType(Session("tl_approverejecttype"), clstlpipeline_master.enTlApproveRejectFormType)
                mintCycleId = Session("tl_approverejectfromqualified_cycle_id")
                If IsNothing(Session("tl_approverejectfromqualified_employee_id")) = False Then
                    Dim mstrProcesslist As String = Session("tl_approverejectfromqualified_process_id")
                    mlistProcessIds = mstrProcesslist.Split(",")
                End If

                If IsNothing(Session("tl_approverejectfromqualified_employee_id")) = False Then
                    Dim mstrEmplist As String = Session("tl_approverejectfromqualified_employee_id")
                    mlistEmployeeIds = mstrEmplist.Split(",")
                End If

                If IsNothing(Session("tl_PotentialTalentTran_id")) = False Then
                    mintPotentialTalentTranunkid = CInt(Session("tl_PotentialTalentTran_id"))
                    Session("tl_PotentialTalentTran_id") = Nothing
                End If


                Dim objtlsettings_master As New clstlsettings_master
                mdblTotalWeight = CDbl(objtlsettings_master.GetSettingValueFromKey(mintCycleId, clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT))
                objtlsettings_master = Nothing

                If menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.APPROVE_REJECT Then
                    btnApprove.Visible = True
                    btnReject.Visible = True
                    lblheader2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Talent Approve/Disapprove")

                ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.DELETETALENTPROCESS Then
                    btnApprove.Visible = True
                    btnReject.Visible = False
                    lblheader2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Remove Talent from Rejected List")
                ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.SET_BACK_TO_QULIFY Then
                    btnApprove.Visible = True
                    btnReject.Visible = False
                    lblheader2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Remove Talent from the Pool")

                ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.DELETEMANUALLYADDEDEMPLOYEE Then
                    btnApprove.Visible = True
                    btnReject.Visible = False
                    lblheader2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Remove Manually Added Employee")

                End If

                SetNextEmployee()
            Else
                If IsNothing(ViewState("mlistProcessIds")) = False Then
                    mlistProcessIds = ViewState("mlistProcessIds")
                End If

                If IsNothing(ViewState("mlistEmployeeIds")) = False Then
                    mlistEmployeeIds = ViewState("mlistEmployeeIds")
                End If

                If IsNothing(ViewState("mintCurrentEmpId")) = False Then
                    mintCurrentEmpId = CInt(ViewState("mintCurrentEmpId"))
                End If

                If IsNothing(ViewState("mintCycleId")) = False Then
                    mintCycleId = CInt(ViewState("mintCycleId"))
                End If

                If IsNothing(ViewState("mintProcessmstunkid")) = False Then
                    mintProcessmstunkid = CInt(ViewState("mintProcessmstunkid"))
                End If

                If IsNothing(CStr(Me.ViewState("mstrEmailAction"))) = False Then
                    mstrEmailAction = CStr(Me.ViewState("mstrEmailAction"))
                End If

                If IsNothing(ViewState("mblnisLastProcessid")) = False Then
                    mblnisLastProcessid = CBool(ViewState("mblnisLastProcessid"))
                End If

                If IsNothing(ViewState("mdblTotalWeight")) = False Then
                    mdblTotalWeight = CDbl(ViewState("mdblTotalWeight"))
                End If

                If IsNothing(ViewState("menumOprationType")) = False Then
                    menumOprationType = CType(ViewState("menumOprationType"), clstlpipeline_master.enTlApproveRejectFormType)
                End If

                If IsNothing(ViewState("mintPotentialTalentTranunkid")) = False Then
                    mintPotentialTalentTranunkid = CInt(ViewState("mintPotentialTalentTranunkid"))
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mlistEmployeeIds") = mlistEmployeeIds
            Me.ViewState("mlistProcessIds") = mlistProcessIds
            Me.ViewState("mintCycleId") = mintCycleId
            Me.ViewState("mintCurrentEmpId") = mintCurrentEmpId
            Me.ViewState("mintProcessmstunkid") = mintProcessmstunkid
            ViewState("mstrEmailAction") = mstrEmailAction
            ViewState("mblnisLastProcessid") = mblnisLastProcessid
            Me.ViewState("mdblTotalWeight") = mdblTotalWeight
            Me.ViewState("menumOprationType") = menumOprationType
            ViewState("mintPotentialTalentTranunkid") = mintPotentialTalentTranunkid

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"
    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Private Sub FillDetail()
        Dim objEmployee As New clsEmployee_Master
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreener_master As New clstlscreener_master
        Dim objEmpQuali As New clsEmp_Qualification_Tran 'Sohail (03 Nov 2020)

        Dim objEmployee_transfer_tran As New clsemployee_transfer_tran
        Dim objEmployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim objSectionGroup As New clsSectionGroup
        Dim objClass As New clsClass

        objtlscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))
        objtlscreening_process_tran._DatabaseName = CStr(Session("Database_Name"))
        objtlscreener_master._DatabaseName = CStr(Session("Database_Name"))

        Dim mintScreenerId As Integer = -1
        Try

            If menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.APPROVE_REJECT Then
                Dim objtlscreening_process_master As New clstlscreening_process_master
                objtlscreening_process_master._DatabaseName = CStr(Session("Database_Name"))
                objtlscreening_process_master._Processmstunkid = mintProcessmstunkid
                txtRemark.Text = objtlscreening_process_master._Remark
                objtlscreening_process_master = Nothing
            End If

            'ClearControl()
            ''Check Old Data Available or not
            'Dim dsScreener As DataSet = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))

            'If IsNothing(dsScreener) = False AndAlso dsScreener.Tables(0).Rows.Count > 0 Then
            '    mintScreenerId = CInt(dsScreener.Tables(0).Rows(0)("screenermstunkid"))
            'End If
            'If objtlscreening_stages_tran.isExist(mintCycleId, mintCurrentEmpId, mintScreenerId) Then

            '    Dim dsScreening As DataSet = objtlscreening_stages_tran.GetList("Screening", mintCycleId, mintCurrentEmpId, mintScreenerId)
            '    If dsScreening.Tables("Screening").Rows.Count > 0 Then
            '        txtscreenoutcome.Text = dsScreening.Tables("Screening").Rows(0)("stage_name")
            '        mintProcessmstunkid = CInt(dsScreening.Tables("Screening").Rows(0)("processmstunkid"))
            '        If objtlscreening_process_tran.isExist(CInt(dsScreening.Tables("Screening").Rows(0)("processmstunkid")), mintScreenerId) Then
            '            Dim dsQue As DataSet = objtlscreening_process_tran.GetList("Que", CInt(dsScreening.Tables("Screening").Rows(0)("processmstunkid")), mintScreenerId)

            '            If IsNothing(dsQue) = False AndAlso dsQue.Tables("Que").Rows.Count > 0 Then
            '                For Each rptItem As RepeaterItem In rptQuestionnaire.Items
            '                    Dim rdbYes As RadioButton = CType(rptItem.FindControl("rdbYes"), RadioButton)
            '                    Dim rdbNo As RadioButton = CType(rptItem.FindControl("rdbNo"), RadioButton)
            '                    Dim txtRemark As TextBox = CType(rptItem.FindControl("txtRemark"), TextBox)
            '                    Dim hfquestionnaireunkid As HiddenField = CType(rptItem.FindControl("hfquestionnaireunkid"), HiddenField)

            '                    Dim drow As DataRow = dsQue.Tables("que").AsEnumerable.Where(Function(x) x.Field(Of Integer)("questionnaireunkid") = CInt(hfquestionnaireunkid.Value)).FirstOrDefault()
            '                    If IsNothing(drow) = False Then
            '                        If CDbl(drow("result")) > 0 Then
            '                            rdbYes.Checked = True
            '                        Else
            '                            rdbNo.Checked = True
            '                        End If
            '                        txtRemark.Text = drow("remark").ToString()
            '                    End If
            '                Next
            '            End If
            '        End If
            '    End If
            'End If

            'Fill Employee Detail
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintCurrentEmpId
            'txtName.Text = objEmployee._Surname + " " + objEmployee._Firstname

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Class_Group & "," & clsEmployee_Master.EmpColEnum.Col_Class & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Group & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Level & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Grade & "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type


            'Dim dsEmpDetail As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, CStr(Session("Database_Name")), _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    CStr(Session("UserAccessModeSetting")), True, _
            '                                    CBool(Session("IsIncludeInactiveEmp")), "List", mintCurrentEmpId)



            'If dsEmpDetail.Tables(0).Rows.Count > 0 Then
            '    txtName.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 46, "Employee Name"))))
            '    txtJob.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 118, "Job") = "", "Job", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 118, "Job"))))
            '    txtDepartment.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 210, "Section Group") = "", "section group", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 210, "Section Group"))))
            '    txtLocation.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 205, "Class") = "", "class", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 205, "Class"))))
            'End If


            txtName.Text = objEmployee._Firstname + " " + objEmployee._Othername + " " + objEmployee._Surname
            Dim dsEmloyeeAllocation As DataSet
            Dim dsEmloyeeJob As DataSet
            dsEmloyeeAllocation = objEmployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mintCurrentEmpId)
            dsEmloyeeJob = objEmployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mintCurrentEmpId)
            If IsNothing(dsEmloyeeJob) = False AndAlso dsEmloyeeJob.Tables(0).Rows.Count > 0 Then
                txtJob.Text = dsEmloyeeJob.Tables(0).Rows(0)("Job").ToString()
            End If
            If IsNothing(dsEmloyeeAllocation) = False AndAlso dsEmloyeeAllocation.Tables(0).Rows.Count > 0 Then
                objSectionGroup._Sectiongroupunkid = CInt(dsEmloyeeAllocation.Tables(0).Rows(0)("sectiongroupunkid"))
                txtDepartment.Text = objSectionGroup._Name
                objClass._Classesunkid = CInt(dsEmloyeeAllocation.Tables(0).Rows(0)("classunkid"))
                txtLocation.Text = objClass._Name
            End If


            Dim strNoimage As String = "data:image/png;base64," & ImageToBase64()

            If objEmployee._blnImgInDb Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    If objEmployee._Photo IsNot Nothing Then
                        imgEmployeeProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                    Else
                        imgEmployeeProfilePic.ImageUrl = strNoimage
                    End If
                End If
            Else
                imgEmployeeProfilePic.ImageUrl = strNoimage
            End If

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, mintCurrentEmpId, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            If dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0).Rows(0)("Employee Code").ToString().ToString().Length > 0 Then
                Dim drrow As DataRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishierarchy") = True).FirstOrDefault()
                txtreportto.Text = drrow("Employee")
            End If

            'Sohail (03 Nov 2020) -- Start
            'NMB Enhancement : # : Pick last qualification name on Talent Screening screen.
            dsList = clsEmp_Qualification_Tran.GetEmployeeQualification(mintCurrentEmpId, False, " CONVERT(CHAR(8), ISNULL(hremp_qualification_tran.award_end_date, GETDATE()), 112) DESC ")
            If dsList.Tables(0).Rows.Count > 0 Then
                txtQualification.Text = CStr(dsList.Tables(0).Rows(0).Item("Name"))
            Else
                txtQualification.Text = ""
            End If
            'Sohail (03 Nov 2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objtlscreening_stages_tran = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreener_master = Nothing

        End Try
    End Sub

    Private Sub SetNextEmployee()
        Dim dsScreenerList As New DataSet
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Dim objtlscreener_master As New clstlscreener_master
        objtlscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))

        Try
            If mintProcessmstunkid < 0 Then
                mintProcessmstunkid = CInt(mlistProcessIds(0))
                mintCurrentEmpId = CInt(mlistEmployeeIds(0))
            Else
                mintProcessmstunkid = CInt(mlistProcessIds.ToList()(mlistProcessIds.ToList().IndexOf(mintProcessmstunkid.ToString()) + 1).ToString())
                mintCurrentEmpId = CInt(mlistEmployeeIds.ToList()(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId.ToString()) + 1).ToString())
            End If

            If CInt(mlistProcessIds.ToList().IndexOf(mintProcessmstunkid.ToString()) + 1) = mlistProcessIds.Count Then
                mblnisLastProcessid = True
            End If

            If menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.APPROVE_REJECT Then

                If mblnisLastProcessid Then
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Approve and Close")
                    btnReject.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Reject and Close")
                Else
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Approve")
                    btnReject.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Reject")
                End If

            ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.DELETETALENTPROCESS Then
                If mblnisLastProcessid Then
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Remove and Close")
                Else
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Remove")
                End If
            ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.SET_BACK_TO_QULIFY Then
                If mblnisLastProcessid Then
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Remove and Close")
                Else
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Remove")
                End If

            ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.DELETEMANUALLYADDEDEMPLOYEE Then
                If mblnisLastProcessid Then
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Remove and Close")
                Else
                    btnApprove.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Remove")
                End If
            End If

            lblemployeecount.Text = (mlistProcessIds.ToList().IndexOf(mintProcessmstunkid.ToString()) + 1).ToString() + " / " + mlistProcessIds.Count.ToString()
            objtlscreening_process_master._Processmstunkid = mintProcessmstunkid
            dsScreenerList = objtlscreening_stages_tran.GetOtherScreenerData("ScreenerDetail", mintCycleId, objtlscreening_process_master._Employeeunkid, -1)
            If IsNothing(dsScreenerList) = False AndAlso dsScreenerList.Tables(0).Rows.Count > 0 Then
                pnlnoperviousData.Visible = False
            Else
                pnlnoperviousData.Visible = True
            End If

            rptOtherScreeners.DataSource = dsScreenerList
            rptOtherScreeners.DataBind()

            FillDetail()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If IsNothing(dsScreenerList) = False Then
                dsScreenerList.Clear()
            End If


            objtlscreening_stages_tran = Nothing
            objtlscreener_master = Nothing
        End Try
    End Sub

    'Private Sub SetPreviousEmployee()
    '    Try

    '        If mintCurrentEmpId <= 0 Then
    '            mintCurrentEmpId = mlistEmployeeIds(0)
    '        Else
    '            mintCurrentEmpId = mlistEmployeeIds.ToList()(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) - 1).ToString()
    '        End If


    '        If CInt(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1) = mlistEmployeeIds.Count Then
    '            btnNext.Visible = False
    '            btnSave.Visible = True
    '        Else
    '            btnNext.Visible = True
    '            btnSave.Visible = False
    '        End If

    '        If mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) <= 0 Then
    '            btnPrevious.Visible = False
    '        End If


    '        lblemployeecount.Text = (mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1).ToString() + " / " + mlistEmployeeIds.Count.ToString()

    '        FillDetail()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region "Buttons Event"
    'Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click, btnSave.Click
    '    Dim objtlscreening_process_master As New clstlscreening_process_master
    '    Dim objtlscreening_process_tran As New clstlscreening_process_tran
    '    Try
    '        If IsValidate() = False Then
    '            Exit Sub
    '        End If

    '        If setProcessMasterValue(objtlscreening_process_master) = False Then
    '            Exit Sub
    '        End If

    '        If objtlscreening_process_master.SaveScreening() = False Then
    '            DisplayMessage.DisplayMessage(objtlscreening_process_master._Message, Me)
    '            Exit Sub
    '        End If

    '        If btnSave.Visible Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Screening Complete Successfully."), Me, Session("rootpath").ToString & "Talent_Succession/wpg_TalentPipeline.aspx")
    '            Exit Sub
    '        End If

    '        If btnNext.Visible Then
    '            SetNextEmployee()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
    '    Dim objtlscreening_process_master As New clstlscreening_process_master
    '    Dim objtlscreening_process_tran As New clstlscreening_process_tran
    '    Try
    '        SetPreviousEmployee()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Talent_Succession/wpg_TalentPipeline.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Dim objpipeline_master As New clstlpipeline_master
        Dim objPotentialTalent_Tran As New clsPotentialTalent_Tran
        Dim blnFlag As Boolean = False

        Try

            Dim btn As Button = CType(sender, Button)
            If txtRemark.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Remark is mandatory information. Please add Remark to continue."), Me)
                Exit Sub
            End If


            If menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.APPROVE_REJECT Then

                objtlscreening_process_master._Processmstunkid = mintProcessmstunkid
                objtlscreening_process_master._Remark = txtRemark.Text
                objtlscreening_process_master._AuditUserId = CInt(Session("UserId"))
                objtlscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                objtlscreening_process_master._FormName = mstrModuleName
                objtlscreening_process_master._FromWeb = True
                objtlscreening_process_master._HostName = CStr(Session("HOST_NAME"))
                objtlscreening_process_master._DatabaseName = CStr(Session("Database_Name"))

                Dim objCycle As New clstlcycle_master
                objCycle._Cycleunkid = mintCycleId
                If btn.ID = btnApprove.ID Then
                    blnFlag = objtlscreening_process_master.SetApproveDisApprove(objtlscreening_process_master._Employeeunkid, mintCycleId, mintProcessmstunkid, True)
                ElseIf btn.ID = btnReject.ID Then
                    blnFlag = objtlscreening_process_master.SetApproveDisApprove(objtlscreening_process_master._Employeeunkid, mintCycleId, mintProcessmstunkid, False)
                End If

                If blnFlag = False AndAlso objtlscreening_process_master._Message.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(objtlscreening_process_master._Message, Me)
                End If
                objCycle = Nothing

            ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.DELETETALENTPROCESS Then
                objtlscreening_process_master._Voidreason = txtRemark.Text
                objtlscreening_process_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objtlscreening_process_master._Voiduserunkid = CInt(Session("UserId"))
                objtlscreening_process_master._Isvoid = True
                objtlscreening_process_master._HostName = CStr(Session("HOST_NAME"))
                objtlscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                objtlscreening_process_master._FormName = mstrModuleName
                objtlscreening_process_master._AuditUserId = CInt(Session("UserId"))
                objtlscreening_process_master._FromWeb = True
                objtlscreening_process_master._Userunkid = Session("UserId").ToString()

                blnFlag = objtlscreening_process_master.Delete(mintProcessmstunkid.ToString(), Nothing)
                If blnFlag = False AndAlso objtlscreening_process_master._Message.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(objtlscreening_process_master._Message, Me)
                End If
            ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.SET_BACK_TO_QULIFY Then

                objtlscreening_process_master._Remark = txtRemark.Text
                objtlscreening_process_master._HostName = CStr(Session("HOST_NAME"))
                objtlscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                objtlscreening_process_master._FormName = mstrModuleName
                objtlscreening_process_master._AuditUserId = CInt(Session("UserId"))
                objtlscreening_process_master._FromWeb = True
                objtlscreening_process_master._Userunkid = Session("UserId").ToString()

                blnFlag = objtlscreening_process_master.SetBackToQulify(mintCycleId, mintProcessmstunkid, txtRemark.Text)
                If blnFlag = False AndAlso objtlscreening_process_master._Message.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(objtlscreening_process_master._Message, Me)
                End If

            ElseIf menumOprationType = clstlpipeline_master.enTlApproveRejectFormType.DELETEMANUALLYADDEDEMPLOYEE Then

                objPotentialTalent_Tran._Voidreason = txtRemark.Text
                objPotentialTalent_Tran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objPotentialTalent_Tran._Voiduserunkid = CInt(Session("UserId"))
                objPotentialTalent_Tran._Isvoid = True
                objPotentialTalent_Tran._HostName = CStr(Session("HOST_NAME"))
                objPotentialTalent_Tran._ClientIP = CStr(Session("IP_ADD"))
                objPotentialTalent_Tran._FormName = mstrModuleName
                objPotentialTalent_Tran._AuditUserId = CInt(Session("UserId"))
                objPotentialTalent_Tran._FromWeb = True
                objPotentialTalent_Tran._ProcessmstUnkid = mintProcessmstunkid

                blnFlag = objPotentialTalent_Tran.Delete(mintPotentialTalentTranunkid)
                If blnFlag = False AndAlso objPotentialTalent_Tran._Message.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(objPotentialTalent_Tran._Message, Me)
                End If

            End If

            If mblnisLastProcessid Then
                Response.Redirect(Session("rootpath").ToString & "Talent_Succession/wpg_TalentPipeline.aspx", False)
            Else
                txtRemark.Text = ""
                SetNextEmployee()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub
#End Region

#Region "DataList Event"
    Protected Sub rptOtherScreeners_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles rptOtherScreeners.ItemDataBound
        Dim dsList As New DataSet
        Dim objtlscreening_process_tran As New clstlscreening_process_tran

        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim hfprocessmstunkid As HiddenField = CType(e.Item.FindControl("hfprocessmstunkid"), HiddenField)
                Dim hfscreenermstunkid As HiddenField = CType(e.Item.FindControl("hfscreenermstunkid"), HiddenField)
                Dim lblpoint As Label = CType(e.Item.FindControl("lblpoint"), Label)

                Dim rptOtherScreenerResponse As DataList = TryCast(e.Item.FindControl("rptOtherScreenerResponse"), DataList)
                lblpoint.Text = lblpoint.Text + " / " + mdblTotalWeight.ToString()
                dsList = objtlscreening_process_tran.GetOtherScreenerData("ResponseDetail", mintCycleId, hfprocessmstunkid.Value, hfscreenermstunkid.Value, -1)

                rptOtherScreenerResponse.DataSource = dsList.Tables("ResponseDetail")
                rptOtherScreenerResponse.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_tran = Nothing
        End Try
    End Sub


    Protected Sub rptOtherScreenerResponse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim hfOtherScreenerResponse As HiddenField = CType(e.Item.FindControl("hfOtherScreenerResponse"), HiddenField)

                Dim rdbYes As RadioButton = CType(e.Item.FindControl("rdbYes"), RadioButton)
                Dim rdbNo As RadioButton = CType(e.Item.FindControl("rdbNo"), RadioButton)
                Dim txtRemark As TextBox = CType(e.Item.FindControl("txtRemark"), TextBox)


                If hfOtherScreenerResponse.Value > 0 Then
                    rdbYes.Checked = True
                Else
                    rdbNo.Checked = True
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblheader2.ID, Me.lblheader2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblheader3.ID, Me.lblheader3.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblName.ID, Me.lblName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblJob.ID, Me.lblJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblreportto.ID, Me.lblreportto.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLocation.ID, Me.lblLocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDepartment.ID, Me.lblDepartment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQualification.ID, Me.lblQualification.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.Label17.ID, Me.Label17.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblnoperviousData.ID, Me.lblnoperviousData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRemark.ID, Me.lblRemark.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnApprove.ID, Me.btnApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReject.ID, Me.btnReject.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblheader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblheader2.ID, Me.lblheader2.Text)
            Me.lblheader3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblheader3.ID, Me.lblheader3.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblreportto.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblreportto.ID, Me.lblreportto.Text)
            Me.lblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLocation.ID, Me.lblLocation.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualification.ID, Me.lblQualification.Text)
            Me.Label17.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.Label17.ID, Me.Label17.Text)
            Me.lblnoperviousData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblnoperviousData.ID, Me.lblnoperviousData.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemark.ID, Me.lblRemark.Text)

            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, Remark is mandatory information. Please add Remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Approve and Close")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Reject and Close")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Approve")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Reject")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Remove and Close")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Remove")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Remove and Close")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Remove")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Talent Approve/Disapprove")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Remove Talent from Rejected List")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Remove Talent from the Pool")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
