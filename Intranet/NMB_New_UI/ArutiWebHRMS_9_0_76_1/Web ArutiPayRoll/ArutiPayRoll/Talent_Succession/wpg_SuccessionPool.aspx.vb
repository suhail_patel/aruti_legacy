﻿Imports System.Data
Imports Aruti.Data
Imports System.IO
Imports System.Drawing

Partial Class Talent_Succession_wpg_SuccessionPool
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSuccessionPool"
    Private DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = ""
    Private mstrDeleteProcessMstIds As String = ""

#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                FillCombo()
            Else
                If IsNothing(ViewState("mstrDeleteProcessMstIds")) = False Then
                    mstrDeleteProcessMstIds = ViewState("mstrDeleteProcessMstIds")
                End If
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrDeleteProcessMstIds") = mstrDeleteProcessMstIds
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Method"
    Private Sub FillCombo()
        Dim objjob As New clsJobs
        Dim dsCombo As New DataSet
        Try
            dsCombo = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With drpJob
                .DataSource = dsCombo.Tables("keyJob")
                .DataTextField = "name"
                .DataValueField = "jobunkid"
                .DataBind()
                .SelectedValue = CStr(0)
                drpJob_SelectedIndexChanged(drpJob, New EventArgs())
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objjob = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub FillList()
        Dim objsucpipeline_master As New clssucpipeline_master
        Dim objsetting As New clssucsettings_master
        Dim objjob As New clsJobs
        Dim dsList As New DataSet
        Dim dsJobList As New DataSet
        Dim dRow As DataRow = Nothing

        Try


            If objsetting.isAllSuccessionSettingExist() Then
                Dim strFilter As String = String.Empty
                strFilter = " sucscreening_process_master.isapproved = 1 "

                Dim strNoimage As String = ImageToBase64()

                objsucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime

                dsList = objsucpipeline_master.GetSuccessionPoolDetailList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), CInt(drpJob.SelectedValue), _
                                          strNoimage, "Emp", False, _
                                          IIf(mstrAdvanceFilter.Trim.Length > 0, mstrAdvanceFilter & " AND " & strFilter, strFilter), , True)


                If CInt(drpJob.SelectedValue) > 0 AndAlso objsucpipeline_master._Message.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(objsucpipeline_master._Message, Me)
                    Exit Sub
                End If

                dsJobList = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
                For Each drJob As DataRow In dsJobList.Tables(0).Rows
                    Dim drRow() As DataRow = dsList.Tables(0).Select("Nominate_JobId = " & drJob.Item("jobunkid") & " ")
                    If drRow.Length > 0 Then
                        dRow = dsList.Tables(0).NewRow()
                        dRow.Item("processmstunkid") = -1
                        dRow.Item("stageunkid") = -1
                        dRow.Item("stage_name") = ""
                        dRow.Item("employeecode") = ""
                        dRow.Item("employeename") = ""
                        dRow.Item("employeeunkid") = -1
                        dRow.Item("isapproved") = False
                        dRow.Item("EmpCodeName") = ""
                        dRow.Item("job_name") = ""
                        dRow.Item("age") = -1
                        dRow.Item("exyr") = 0
                        dRow.Item("department") = ""
                        dRow.Item("TotalScreener") = -1
                        dRow.Item("empImage") = Nothing
                        dRow.Item("empBaseImage") = ""
                        dRow.Item("IsGrp") = 1
                        dRow.Item("Nominate_JobId") = drJob.Item("jobunkid")
                        dRow.Item("Nominate_Job") = drJob.Item("name")
                        dsList.Tables(0).Rows.Add(dRow)
                    End If
                Next

                Dim dtTable As DataTable
                dtTable = New DataView(dsList.Tables("Emp"), "", "Nominate_JobId  , processmstunkid  ", DataViewRowState.CurrentRows).ToTable.Copy
                dsList.Tables.RemoveAt(0)
                dsList.Tables.Add(dtTable.Copy)

                gvSuccessionPool.DataSource = dsList.Tables("Emp")
                gvSuccessionPool.DataBind()
            Else
                If objsetting._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objsetting._Message, Me)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucpipeline_master = Nothing
            objsetting = Nothing
            objjob = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            If IsNothing(dsJobList) = False Then
                dsJobList.Clear()
                dsJobList = Nothing
            End If
        End Try
    End Sub
#End Region

#Region "Button Event"

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
    '    Try
    '        If drpCycle.SelectedValue <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, Cycle is mandatory information. Please select cycle to continue."), Me)
    '            Exit Sub
    '        End If
    '        FillList()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Pinkal (12-Dec-2020) -- End

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            FillList()
            'Pinkal (12-Dec-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Dim objsucscreening_process_master As New clssucscreening_process_master
        Dim objScreener As New clssucscreener_master
        Dim dsList As DataSet
        Dim intScreenerUnkId As Integer
        Try
            dsList = objScreener.GetList("List", True, CInt(Session("UserId")))
            If dsList.Tables(0).Rows.Count > 0 Then
                intScreenerUnkId = CInt(dsList.Tables(0).Rows(0).Item("ScreenerMstUnkId"))
            End If

            If intScreenerUnkId > 0 Then
                objsucscreening_process_master._Voidreason = popup_DeleteReason.Reason
                objsucscreening_process_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objsucscreening_process_master._Voiduserunkid = CInt(Session("UserId"))
                objsucscreening_process_master._Isvoid = True

                objsucscreening_process_master._HostName = CStr(Session("HOST_NAME"))
                objsucscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                objsucscreening_process_master._FormName = mstrModuleName
                objsucscreening_process_master._AuditUserId = Session("UserId")
                objsucscreening_process_master._FromWeb = True
                objsucscreening_process_master._Userunkid = CInt(Session("UserId"))

                objsucscreening_process_master.DeleteScreenerTranDetails(mstrDeleteProcessMstIds, intScreenerUnkId, Nothing)
            End If

            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsucscreening_process_master = Nothing
            objScreener = Nothing
        End Try
    End Sub

#End Region

#Region "Grid View Events"
    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Protected Sub gvSuccessionPool_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSuccessionPool.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If CBool(gvSuccessionPool.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "nominate_job").ToString
                e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                e.Row.BackColor = Color.Silver
                e.Row.ForeColor = Color.Black
                e.Row.Font.Bold = True

                For i As Integer = 2 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).Visible = False
                Next

                Dim lnkSuccessionProfile As LinkButton = TryCast(e.Row.FindControl("lnkSuccessionProfile"), LinkButton)
                lnkSuccessionProfile.Visible = False

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link Event(s) "
    Protected Sub lnkSEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clssucscreener_master
        Try
            If objScreener.IsLoginUserIsScreener(CInt(Session("UserId"))) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, You are not Screener. So You can't Edit Entries!!!"), Me)
                Exit Sub
            End If
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            Dim intEmployeeUnkId As Integer
            intEmployeeUnkId = CInt(gvSuccessionPool.DataKeys(row.RowIndex)("employeeunkid"))

            Session("suc_screening_job_id") = CInt(drpJob.SelectedValue)
            Session("suc_screening_employee_id") = intEmployeeUnkId.ToString()
            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_Succession_Screening.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objScreener As New clssucscreener_master
        Try
            If objScreener.IsLoginUserIsScreener(CInt(Session("UserId"))) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, You are not Screener. So You can't Delete Entries!!!"), Me)
                Exit Sub
            End If
            mstrDeleteProcessMstIds = ""
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            mstrDeleteProcessMstIds = gvSuccessionPool.DataKeys(row.RowIndex)("processmstunkid")

            popup_DeleteReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try
    End Sub

    Protected Sub lnkSuccessionProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objScreener As New clssucscreener_master
        objScreener._DatabaseName = CStr(Session("Database_Name"))
        Try
            If objScreener.IsLoginUserIsScreener(CInt(Session("UserId"))) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, You are not Screener. So You can't View Succession Profile Entries!!!"), Me)
                Exit Sub
            End If
            Dim lnkprofile As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkprofile).NamingContainer, GridViewRow)


            Dim status As String = gvSuccessionPool.Rows(row.RowIndex).Cells(getColumnID_Griview(gvSuccessionPool, "colhStatus", False)).Text

            Dim intEmployeeUnkId As Integer
            intEmployeeUnkId = CInt(gvSuccessionPool.DataKeys(row.RowIndex)("employeeunkid"))
            Dim intNominateJobId As Integer
            intNominateJobId = CInt(gvSuccessionPool.DataKeys(row.RowIndex)("Nominate_JobId"))

            Session("suc_screening_job_id") = intNominateJobId.ToString()
            Session("suc_screening_employee_id") = intEmployeeUnkId.ToString()
            Session("suc_screening_employee_status") = status
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_SuccessionProfile.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Dropdown Events"
    Protected Sub drpJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpJob.SelectedIndexChanged
        Dim objsetting As New clssucsettings_master
        Try
            If objsetting.isAllSuccessionSettingExist() Then
                FillList()
            Else
                If objsetting._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objsetting._Message, Me)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsetting = Nothing
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCycle.ID, Me.lblCycle.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblHeader.ID, Me.lblHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvSuccessionPool.Columns(1).FooterText, gvSuccessionPool.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvSuccessionPool.Columns(2).FooterText, gvSuccessionPool.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvSuccessionPool.Columns(3).FooterText, gvSuccessionPool.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvSuccessionPool.Columns(4).FooterText, gvSuccessionPool.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvSuccessionPool.Columns(5).FooterText, gvSuccessionPool.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvSuccessionPool.Columns(6).FooterText, gvSuccessionPool.Columns(6).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPageHeader1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Me.lblCycle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCycle.ID, Me.lblCycle.Text)
            Me.lblHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblHeader.ID, Me.lblHeader.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            gvSuccessionPool.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSuccessionPool.Columns(1).FooterText, gvSuccessionPool.Columns(1).HeaderText)
            gvSuccessionPool.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSuccessionPool.Columns(2).FooterText, gvSuccessionPool.Columns(2).HeaderText)
            gvSuccessionPool.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSuccessionPool.Columns(3).FooterText, gvSuccessionPool.Columns(3).HeaderText)
            gvSuccessionPool.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSuccessionPool.Columns(4).FooterText, gvSuccessionPool.Columns(4).HeaderText)
            gvSuccessionPool.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSuccessionPool.Columns(5).FooterText, gvSuccessionPool.Columns(5).HeaderText)
            gvSuccessionPool.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSuccessionPool.Columns(6).FooterText, gvSuccessionPool.Columns(6).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, You are not Screener. So You can't Edit Entries!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, You are not Screener. So You can't Delete Entries!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, You are not Screener. So You can't View Succession Profile Entries!!!")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
