﻿Imports System.Data
Imports Aruti.Data
Imports System.IO

Partial Class Talent_Succession_wpg_TalentProfile
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTalentProfile"
    Private DisplayMessage As New CommonCodes
    Private mlistEmployeeIds As String() = Nothing
    Private mintCurrentEmpId As Integer = 0
    Private mintCycleId As Integer = 0
    Private mintScreenermstunkid As Integer = -1
    Private mdblTotalWeight As Double = 0

#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                mintCycleId = Session("tl_screening_cycle_id")
                If IsNothing(Session("tl_screening_employee_id")) = False Then
                    Dim mstrEmplist As String = Session("tl_screening_employee_id")
                    mlistEmployeeIds = mstrEmplist.Split(",")
                End If

                Dim dslist As New DataSet
                Dim objtlscreener_master As New clstlscreener_master

                dslist = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))
                If IsNothing(dslist) = False AndAlso dslist.Tables(0).Rows.Count > 0 Then
                    mintScreenermstunkid = CInt(dslist.Tables(0).Rows(0)("screenermstunkid"))
                End If

                Dim objtlsettings_master As New clstlsettings_master
                mdblTotalWeight = CDbl(objtlsettings_master.GetSettingValueFromKey(mintCycleId, clstlsettings_master.enTalentConfiguration.TOTAL_QUESTION_WEIGHT))
                objtlsettings_master = Nothing

                SetEmployee()
            Else
                If IsNothing(ViewState("mlistEmployeeIds")) = False Then
                    mlistEmployeeIds = ViewState("mlistEmployeeIds")
                End If

                If IsNothing(ViewState("mintCurrentEmpId")) = False Then
                    mintCurrentEmpId = CInt(ViewState("mintCurrentEmpId"))
                End If

                If IsNothing(ViewState("mintCycleId")) = False Then
                    mintCycleId = CInt(ViewState("mintCycleId"))
                End If

                If IsNothing(ViewState("mintScreenermstunkid")) = False Then
                    mintScreenermstunkid = CInt(ViewState("mintScreenermstunkid"))
                End If

                If IsNothing(ViewState("mdblTotalWeight")) = False Then
                    mdblTotalWeight = CDbl(ViewState("mdblTotalWeight"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mlistEmployeeIds") = mlistEmployeeIds
            Me.ViewState("mintCycleId") = mintCycleId
            Me.ViewState("mintCurrentEmpId") = mintCurrentEmpId
            Me.ViewState("mintScreenermstunkid") = mintScreenermstunkid
            Me.ViewState("mdblTotalWeight") = mdblTotalWeight
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Method"
    Private Sub FillDetail()
        Dim objEmployee As New clsEmployee_Master
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreener_master As New clstlscreener_master
        Dim objJob As New clsJobs

        Dim objEmployee_transfer_tran As New clsemployee_transfer_tran
        Dim objEmployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim objSectionGroup As New clsSectionGroup
        Dim objClass As New clsClass


        objtlscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))
        objtlscreening_process_tran._DatabaseName = CStr(Session("Database_Name"))
        objtlscreener_master._DatabaseName = CStr(Session("Database_Name"))

        Dim mintScreenerId As Integer = -1
        Try

            'Fill Employee Detail
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintCurrentEmpId


            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Class_Group & "," & clsEmployee_Master.EmpColEnum.Col_Class & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Group & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Level & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Grade & "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type


            'Dim dsEmpDetail As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, CStr(Session("Database_Name")), _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    CStr(Session("UserAccessModeSetting")), True, _
            '                                    CBool(Session("IsIncludeInactiveEmp")), "List", mintCurrentEmpId)



            'If dsEmpDetail.Tables(0).Rows.Count > 0 Then
            '    txtName.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 46, "Employee Name"))))
            '    txtJob.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 118, "Job") = "", "Job", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 118, "Job"))))
            '    txtDepartment.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 210, "Section Group") = "", "section group", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 210, "Section Group"))))
            '    txtLocation.Text = dsEmpDetail.Tables(0).Rows(0)(CStr(IIf(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 205, "Class") = "", "class", Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsEmployee_Master", 205, "Class"))))
            'End If


            txtName.Text = objEmployee._Firstname + " " + objEmployee._Othername + " " + objEmployee._Surname
            Dim dsEmloyeeAllocation As DataSet
            Dim dsEmloyeeJob As DataSet
            dsEmloyeeAllocation = objEmployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mintCurrentEmpId)
            dsEmloyeeJob = objEmployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, mintCurrentEmpId)
            If IsNothing(dsEmloyeeJob) = False AndAlso dsEmloyeeJob.Tables(0).Rows.Count > 0 Then
                txtJob.Text = dsEmloyeeJob.Tables(0).Rows(0)("Job").ToString()
            End If
            If IsNothing(dsEmloyeeAllocation) = False AndAlso dsEmloyeeAllocation.Tables(0).Rows.Count > 0 Then
                objSectionGroup._Sectiongroupunkid = CInt(dsEmloyeeAllocation.Tables(0).Rows(0)("sectiongroupunkid"))
                txtDepartment.Text = objSectionGroup._Name
                objClass._Classesunkid = CInt(dsEmloyeeAllocation.Tables(0).Rows(0)("classunkid"))
                txtLocation.Text = objClass._Name
            End If

            objJob._Jobunkid = objEmployee._Jobunkid
            txtJobLevel.Text = objJob._Job_Level

            Dim strNoimage As String = "data:image/png;base64," & ImageToBase64()

            If objEmployee._blnImgInDb Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    If objEmployee._Photo IsNot Nothing Then
                        imgEmployeeProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                    Else
                        imgEmployeeProfilePic.ImageUrl = strNoimage
                    End If
                End If
            Else
                imgEmployeeProfilePic.ImageUrl = strNoimage
            End If

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, mintCurrentEmpId, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            If dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0).Rows(0)("Employee Code").ToString().ToString().Length > 0 Then
                Dim drrow As DataRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishierarchy") = True).FirstOrDefault()
                txtreportto.Text = drrow("Employee")
            End If

            Call PreviousScreenerResponse()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objtlscreening_stages_tran = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreener_master = Nothing
            objJob = Nothing
        End Try
    End Sub

    Private Sub SetEmployee()
        Try
            If mintCurrentEmpId <= 0 Then
                mintCurrentEmpId = mlistEmployeeIds(0)

            Else
                mintCurrentEmpId = mlistEmployeeIds.ToList()(mlistEmployeeIds.ToList().IndexOf(mintCurrentEmpId) + 1).ToString()

            End If

            FillDetail()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Protected Sub PreviousScreenerResponse()
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran
        Dim objtlscreener_master As New clstlscreener_master
        objtlscreening_stages_tran._DatabaseName = CStr(Session("Database_Name"))

        Dim dsScreenerList As New DataSet
        Dim dsList As New DataSet
        Try

            'dsList = objtlscreener_master.GetList("Screener", mintCycleId, True, CInt(Session("UserId")))

            'If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
            dsScreenerList = objtlscreening_stages_tran.GetOtherScreenerData("ScreenerDetail", mintCycleId, mintCurrentEmpId, -1)

            If IsNothing(dsScreenerList) = False AndAlso dsScreenerList.Tables(0).Rows.Count > 0 Then
                pnlnoperviousData.Visible = False
            Else
                pnlnoperviousData.Visible = True
            End If

            rptOtherScreeners.DataSource = dsScreenerList
            rptOtherScreeners.DataBind()

            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_stages_tran = Nothing
        End Try
    End Sub
#End Region

#Region "Button Events"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub btnViewPDP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewPDP.Click
        Try
            Session("TalentEmployeeId") = mintCurrentEmpId
            Response.Redirect(Session("rootpath").ToString & "PDP/wPgEmployeePdp.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "DataList Event"
    Protected Sub rptOtherScreeners_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles rptOtherScreeners.ItemDataBound
        Dim dsList As New DataSet
        Dim objtlscreening_process_tran As New clstlscreening_process_tran

        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim hfprocessmstunkid As HiddenField = CType(e.Item.FindControl("hfprocessmstunkid"), HiddenField)
                Dim hfscreenermstunkid As HiddenField = CType(e.Item.FindControl("hfscreenermstunkid"), HiddenField)
                Dim lblpoint As Label = CType(e.Item.FindControl("lblpoint"), Label)
                Dim rptOtherScreenerResponse As DataList = TryCast(e.Item.FindControl("rptOtherScreenerResponse"), DataList)

                lblpoint.Text = lblpoint.Text + " / " + mdblTotalWeight.ToString()
                dsList = objtlscreening_process_tran.GetOtherScreenerData("ResponseDetail", mintCycleId, hfprocessmstunkid.Value, hfscreenermstunkid.Value, -1)

                rptOtherScreenerResponse.DataSource = dsList.Tables("ResponseDetail")
                rptOtherScreenerResponse.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_tran = Nothing
        End Try
    End Sub

    Protected Sub rptOtherScreenerResponse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim hfOtherScreenerResponse As HiddenField = CType(e.Item.FindControl("hfOtherScreenerResponse"), HiddenField)

                Dim rdbYes As RadioButton = CType(e.Item.FindControl("rdbYes"), RadioButton)
                Dim rdbNo As RadioButton = CType(e.Item.FindControl("rdbNo"), RadioButton)
                Dim txtRemark As TextBox = CType(e.Item.FindControl("txtRemark"), TextBox)


                If hfOtherScreenerResponse.Value > 0 Then
                    rdbYes.Checked = True
                Else
                    rdbNo.Checked = True
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblheader2.ID, Me.lblheader2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblheader3.ID, Me.lblheader3.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblName.ID, Me.lblName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblJob.ID, Me.lblJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblreportto.ID, Me.lblreportto.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLocation.ID, Me.lblLocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDepartment.ID, Me.lblDepartment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblJobLevel.ID, Me.lblJobLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPreviousScreenersResponse.ID, Me.lblPreviousScreenersResponse.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblnoperviousData.ID, Me.lblnoperviousData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTalentPerformance.ID, Me.lblTalentPerformance.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTalentProgress.ID, Me.lblTalentProgress.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblheader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblheader2.ID, Me.lblheader2.Text)
            Me.lblheader3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblheader3.ID, Me.lblheader3.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblreportto.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblreportto.ID, Me.lblreportto.Text)
            Me.lblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLocation.ID, Me.lblLocation.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblJobLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJobLevel.ID, Me.lblJobLevel.Text)
            Me.lblPreviousScreenersResponse.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPreviousScreenersResponse.ID, Me.lblPreviousScreenersResponse.Text)
            Me.lblnoperviousData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblnoperviousData.ID, Me.lblnoperviousData.Text)
            Me.lblTalentPerformance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTalentPerformance.ID, Me.lblTalentPerformance.Text)
            Me.lblTalentProgress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTalentProgress.ID, Me.lblTalentProgress.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
End Class
