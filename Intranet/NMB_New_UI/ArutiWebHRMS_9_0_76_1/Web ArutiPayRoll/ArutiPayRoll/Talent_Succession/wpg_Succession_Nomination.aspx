﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpg_Succession_Nomination.aspx.vb"
    Inherits="Talent_Succession_wpg_Succession_Nomination" Title="Employee Succession Nomination" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <asp:Panel ID="pnlNominateEmployee" runat="server" CssClass="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblNominateEmployeeHeader" runat="server" Text="Nominate Employee" />
                                </h2>
                            </div>
                            <div class="body" style="max-height: 400px">
                                <div class="inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblNominateEmployeeJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="drpNominateEmployeeJob" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnNominateEmployeeSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <div class="inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="max-height: 250px">
                                                    <asp:GridView ID="dgvNominateEmployee" runat="server" AutoGenerateColumns="false"
                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="nominationunkid">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkNominateEmployeeDelete" runat="server" ToolTip="Delete" OnClick="lnkNominateEmployeeDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="job_name" HeaderText="Job Name" ReadOnly="True" FooterText="colhNominatedJob">
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNominateEmployeeClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                                <asp:HiddenField ID="hfNominateEmployee" runat="server" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupNominationCancleReason" runat="server" PopupControlID="pnlNominationCancleReason"
                    TargetControlID="hfNominationCancleReason" Drag="true" PopupDragHandleControlID="pnlNominationCancleReason"
                    BackgroundCssClass="modal-backdrop bd-l2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlNominationCancleReason" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label2" runat="server" Text="Succession Qualifying Criteria Info." />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="list-group">
                                    <li class="list-group-item">Is Qualification Level Matched?
                                        <asp:Label ID="lblQualificationNominationCheckTrue" Visible="false" runat="server"
                                            Text="" CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblQualificationNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" />
                                    </li>
                                    <li class="list-group-item">Is Minimum Number of Years with Organization Matched?
                                        <asp:Label ID="lblExpNominationCheckTrue" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblExpNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                    <li class="list-group-item">Is Minimum Performance Score Matched?
                                        <asp:Label ID="lblPerfNominationCheckTrue" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblPerfNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                    <li class="list-group-item">Is Minimum Age Matched?
                                        <asp:Label ID="lblMinimumAgeCheckTrue" runat="server" Text="" Visible="false" CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblMinimumAgeCheckFalse" runat="server" Text="" Visible="false" CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnNominationCancleReasonClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfNominationCancleReason" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupNominatedEmployeeList" runat="server" PopupControlID="pnlNominatedEmployeeList"
                    TargetControlID="hfNominatedEmployeeList" Drag="true" PopupDragHandleControlID="pnlNominatedEmployeeList"
                    BackgroundCssClass="modal-backdrop bd-l2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlNominatedEmployeeList" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblNominatedEmployeeListHeader" runat="server" Text="Nominated Employee List" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblNominatedJob" runat="server" Text="Nominated Job:" CssClass="form-label" />
                                <asp:Label ID="txtNominatedJob" runat="server" Text="" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="max-height: 200px">
                                    <asp:GridView ID="gvNominatedEmployeeList" runat="server" AutoGenerateColumns="False"
                                        CssClass="table table-hover table-bordered" AllowPaging="false" Width="99%">
                                        <Columns>
                                            <asp:BoundField DataField="employeecode" HeaderText="Employee Code" ReadOnly="True"
                                                FooterText="colhNominatedEmployeeCode"></asp:BoundField>
                                            <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                FooterText="colhNominatedEmployeeName"></asp:BoundField>
                                            <asp:BoundField DataField="Department" HeaderText="Department Name" ReadOnly="True"
                                                FooterText="colhNominatedEmployeeDept"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnNominatedEmployeeListClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfNominatedEmployeeList" runat="server" />
                    </div>
                </asp:Panel>
                <uc7:confirmation id="popup_YesNo" runat="server" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
