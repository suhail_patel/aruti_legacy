﻿
Imports System.Data
Imports Aruti.Data
Imports System.IO
Imports System.Drawing
Partial Class Talent_Succession_wpg_TalentPool
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTalentPool"
    Private DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = ""
    Private mstrDeleteProcessMstIds As String = ""

#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                FillCombo()
            Else
                If IsNothing(ViewState("mstrDeleteProcessMstIds")) = False Then
                    mstrDeleteProcessMstIds = ViewState("mstrDeleteProcessMstIds")
                End If
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrDeleteProcessMstIds") = mstrDeleteProcessMstIds
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Method"
    Private Sub FillCombo()
        Dim objCycle As New clstlcycle_master
        Dim dsCombo As New DataSet
        Try
            dsCombo = objCycle.getListForCombo(CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN)
            With drpCycle
                .DataValueField = "cycleunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                'Pinkal (12-Dec-2020) -- Start
                'Enhancement  -  Working on Talent Issue which is given by Andrew.
                '.SelectedValue = CStr(0)
                .SelectedValue = objCycle.getCurrentCycleId(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("Fin_year")), enStatusType.OPEN)
                drpCycle_SelectedIndexChanged(drpCycle, New EventArgs())
                'Pinkal (12-Dec-2020) -- End
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCycle = Nothing

            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub FillList()
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim objStage As New clstlstages_master
        Dim dsList As New DataSet
        Dim dsStageList As New DataSet
        Dim dRow As DataRow = Nothing
        Dim objCycle As New clstlcycle_master
        Try

            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.

            'Dim xMaxStageId As Integer = 0
            'dsStageList = objStage.GetList("list", drpCycle.SelectedValue)
            'xMaxStageId = (From n In dsStageList.Tables(0).AsEnumerable() Select n.Field(Of Integer)("stageunkid")).DefaultIfEmpty().Max()
            Dim strFilter As String = String.Empty
            strFilter = " tlscreening_process_master.isapproved = 1 "

            Dim strNoimage As String = ImageToBase64()
            objCycle._Cycleunkid = CInt(drpCycle.SelectedValue)
            objtlpipeline_master._DateAsOn = CDate(objCycle._Start_Date)

            dsList = objtlpipeline_master.GetTalentPoolDetailList(CStr(Session("Database_Name")), _
                                      CInt(Session("UserId")), _
                                      CInt(Session("Fin_year")), _
                                      CInt(Session("CompanyUnkId")), _
                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                      CStr(Session("UserAccessModeSetting")), True, _
                                      CBool(Session("IsIncludeInactiveEmp")), CInt(drpCycle.SelectedValue), _
                                      strNoimage, "Emp", False, IIf(mstrAdvanceFilter.Trim.Length > 0, mstrAdvanceFilter & " AND " & strFilter, strFilter), , True)


            If CInt(drpCycle.SelectedValue) > 0 AndAlso objtlpipeline_master._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objtlpipeline_master._Message, Me)
                Exit Sub
            End If

            'dsStageList = objStage.GetList("list", drpCycle.SelectedValue)
            'For Each drStage As DataRow In dsStageList.Tables(0).Rows
            '    Dim drRow() As DataRow = dsList.Tables(0).Select("stageunkid = " & drStage.Item("stageunkid") & " ")
            '    If drRow.Length > 0 Then
            '        dRow = dsList.Tables(0).NewRow()
            '        dRow.Item("processmstunkid") = -1
            '        dRow.Item("stageunkid") = drStage.Item("stageunkid")
            '        dRow.Item("stage_name") = drStage.Item("stagename")
            '        dRow.Item("employeecode") = ""
            '        dRow.Item("employeename") = ""
            '        dRow.Item("employeeunkid") = -1
            '        dRow.Item("isapproved") = False
            '        dRow.Item("EmpCodeName") = ""
            '        dRow.Item("job_name") = ""
            '        dRow.Item("age") = -1
            '        dRow.Item("exyr") = 0
            '        dRow.Item("department") = ""
            '        dRow.Item("TotalScreener") = -1
            '        dRow.Item("empImage") = Nothing
            '        dRow.Item("empBaseImage") = ""
            '        dRow.Item("IsGrp") = 1
            '        dsList.Tables(0).Rows.Add(dRow)
            '    End If
            'Next
            'Pinkal (12-Dec-2020) -- End

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("Emp"), "", "stageunkid  , processmstunkid  ", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

            gvTalentPool.DataSource = dsList.Tables("Emp")
            gvTalentPool.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlpipeline_master = Nothing
            objStage = Nothing
            objCycle = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            If IsNothing(dsStageList) = False Then
                dsStageList.Clear()
                dsStageList = Nothing
            End If
        End Try
    End Sub
#End Region

#Region "Button Event"

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
    '    Try
    '        If drpCycle.SelectedValue <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, Cycle is mandatory information. Please select cycle to continue."), Me)
    '            Exit Sub
    '        End If
    '        FillList()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Pinkal (12-Dec-2020) -- End

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            FillList()
            'Pinkal (12-Dec-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Dim objScreener As New clstlscreener_master
        Dim dsList As DataSet
        Dim intScreenerUnkId As Integer
        Try
            dsList = objScreener.GetList("List", CInt(drpCycle.SelectedValue), True, CInt(Session("UserId")))
            If dsList.Tables(0).Rows.Count > 0 Then
                intScreenerUnkId = CInt(dsList.Tables(0).Rows(0).Item("ScreenerMstUnkId"))
            End If

            If intScreenerUnkId > 0 Then
                objtlscreening_process_master._Voidreason = popup_DeleteReason.Reason
                objtlscreening_process_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objtlscreening_process_master._Voiduserunkid = CInt(Session("UserId"))
                objtlscreening_process_master._Isvoid = True

                objtlscreening_process_master._HostName = CStr(Session("HOST_NAME"))
                objtlscreening_process_master._ClientIP = CStr(Session("IP_ADD"))
                objtlscreening_process_master._FormName = mstrModuleName
                objtlscreening_process_master._AuditUserId = Session("UserId")
                objtlscreening_process_master._FromWeb = True
                objtlscreening_process_master._Userunkid = CInt(Session("UserId"))

                objtlscreening_process_master.DeleteScreenerTranDetails(mstrDeleteProcessMstIds, intScreenerUnkId, Nothing)
            End If

            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
            objScreener = Nothing
        End Try
    End Sub

#End Region

#Region "Grid View Events"
    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Protected Sub gvTalentPool_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTalentPool.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            'If CBool(gvTalentPool.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
            '    e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "stage_name").ToString
            '    e.Row.Cells(3).ColumnSpan = e.Row.Cells.Count - 3
            '    e.Row.BackColor = Color.Silver
            '    e.Row.ForeColor = Color.Black
            '    e.Row.Font.Bold = True

            '    For i As Integer = 4 To e.Row.Cells.Count - 1
            '        e.Row.Cells(i).Visible = False
            '    Next

            '    Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("lnkEdit"), LinkButton)
            '    lnkEdit.Visible = False

            '    Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("lnkDelete"), LinkButton)
            '    lnkDelete.Visible = False

            '    Dim lnkTalentProfile As LinkButton = TryCast(e.Row.FindControl("lnkTalentProfile"), LinkButton)
            '    lnkTalentProfile.Visible = False

            'End If
            'Pinkal (12-Dec-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link Event(s) "
    Protected Sub lnkSEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objScreener As New clstlscreener_master
        Try
            'If objScreener.IsLoginUserIsScreener(CInt(drpCycle.SelectedValue), CInt(Session("UserId"))) = False Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, You are not Screener. So You can't Edit Entries!!!"), Me)
            '    Exit Sub
            'End If
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            Dim intEmployeeUnkId As Integer
            intEmployeeUnkId = CInt(gvTalentPool.DataKeys(row.RowIndex)("employeeunkid"))

            Session("tl_screening_cycle_id") = CInt(drpCycle.SelectedValue)
            Session("tl_screening_employee_id") = intEmployeeUnkId.ToString()
            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_Talent_Screening.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objScreener As New clstlscreener_master
        Try
            'If objScreener.IsLoginUserIsScreener(CInt(drpCycle.SelectedValue), CInt(Session("UserId"))) = False Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, You are not Screener. So You can't Delete Entries!!!"), Me)
            '    Exit Sub
            'End If
            mstrDeleteProcessMstIds = ""
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            mstrDeleteProcessMstIds = gvTalentPool.DataKeys(row.RowIndex)("processmstunkid")

            popup_DeleteReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScreener = Nothing
        End Try
    End Sub

    Protected Sub lnkTalentProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objScreener As New clstlscreener_master
        objScreener._DatabaseName = CStr(Session("Database_Name"))
        Try
            'If objScreener.IsLoginUserIsScreener(CInt(drpCycle.SelectedValue), CInt(Session("UserId"))) = False Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, You are not Screener. So You can't View Talent Profile Entries!!!"), Me)
            '    Exit Sub
            'End If
            Dim lnkprofile As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkprofile).NamingContainer, GridViewRow)
            Dim intEmployeeUnkId As Integer
            intEmployeeUnkId = CInt(gvTalentPool.DataKeys(row.RowIndex)("employeeunkid"))

            Session("tl_screening_cycle_id") = CInt(drpCycle.SelectedValue)
            Session("tl_screening_employee_id") = intEmployeeUnkId.ToString()
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_TalentProfile.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
#Region "Dropdown Events"
    Protected Sub drpCycle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpCycle.SelectedIndexChanged
        Dim objsetting As New clstlsettings_master
        Try
            If objsetting.isAllTalentSettingExist(CInt(drpCycle.SelectedValue)) Then
                FillList()
            Else
                If objsetting._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objsetting._Message, Me)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsetting = Nothing
        End Try
    End Sub
#End Region
    'Pinkal (12-Dec-2020) -- End

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCycle.ID, Me.lblCycle.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblHeader.ID, Me.lblHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(1).FooterText, gvTalentPool.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(2).FooterText, gvTalentPool.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(3).FooterText, gvTalentPool.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(4).FooterText, gvTalentPool.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(5).FooterText, gvTalentPool.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(6).FooterText, gvTalentPool.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTalentPool.Columns(7).FooterText, gvTalentPool.Columns(7).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPageHeader1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Me.lblCycle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCycle.ID, Me.lblCycle.Text)
            Me.lblHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblHeader.ID, Me.lblHeader.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lnkAdvanceFilter.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.ToolTip)

            gvTalentPool.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(1).FooterText, gvTalentPool.Columns(1).HeaderText)
            gvTalentPool.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(2).FooterText, gvTalentPool.Columns(2).HeaderText)
            gvTalentPool.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(3).FooterText, gvTalentPool.Columns(3).HeaderText)
            gvTalentPool.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(4).FooterText, gvTalentPool.Columns(4).HeaderText)
            gvTalentPool.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(5).FooterText, gvTalentPool.Columns(5).HeaderText)
            gvTalentPool.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(6).FooterText, gvTalentPool.Columns(6).HeaderText)
            gvTalentPool.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTalentPool.Columns(7).FooterText, gvTalentPool.Columns(7).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, You are not Screener. So You can't Edit Entries!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, You are not Screener. So You can't Delete Entries!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, You are not Screener. So You can't View Talent Profile Entries!!!")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
