﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpg_SuccessionPool.aspx.vb"
    Inherits="Talent_Succession_wpg_SuccessionPool" Title="Succession Pool" %>

<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <h2>
                    <asp:Label ID="lblPageHeader" runat="server" Text="Succession Pool"></asp:Label>
                </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader1" runat="server" Text="Filter"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblCycle" Text="Job Title" runat="server" CssClass="form-label" />
                                    <div class="form-group">
                                        <%--  'Pinkal (12-Dec-2020) -- Start
                                               'Enhancement  -  Working on Talent Issue which is given by Andrew.--%>
                                        <asp:DropDownList ID="drpJob" runat="server" AutoPostBack="true">
                                            <%--  'Pinkal (12-Dec-2020) -- End      --%>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <%--  'Pinkal (12-Dec-2020) -- Start
                        'Enhancement  -  Working on Talent Issue which is given by Andrew.--%>
                            <%--<asp:Button ID="btnProcess" CssClass="btn btn-primary" runat="server" Text="Process" />--%>
                            <%--  'Pinkal (12-Dec-2020) -- End      --%>
                            <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card ">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblHeader" runat="server" Text="Succession Pool"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 400px">
                                        <asp:GridView ID="gvSuccessionPool" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                            AllowPaging="false" DataKeyNames="IsGrp,employeeunkid,processmstunkid,Nominate_JobId">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkSuccessionProfile" runat="server" ToolTip="Succession Profile" OnClick="lnkSuccessionProfile_Click">
                                                             <i class="fas fa-chart-bar"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EmpCodeName" HeaderText="Employee Name" ReadOnly="True"
                                                    FooterText="colhEmpCodeName" />
                                                <asp:BoundField DataField="Location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation" />
                                                <asp:BoundField DataField="job_name" HeaderText="Job Title" ReadOnly="True" FooterText="colhJobName" />
                                                <asp:BoundField DataField="department" HeaderText="Department" ReadOnly="True" FooterText="colhDepartment" />
                                                <asp:BoundField DataField="LineManager" HeaderText="Line Manager" ReadOnly="True"
                                                    FooterText="colhLineManager" />
                                                 <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True"
                                                    FooterText="colhStatus" />
                                                    
                                                <asp:BoundField DataField="Isgrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are ou Sure You Want To delete?:" />
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
