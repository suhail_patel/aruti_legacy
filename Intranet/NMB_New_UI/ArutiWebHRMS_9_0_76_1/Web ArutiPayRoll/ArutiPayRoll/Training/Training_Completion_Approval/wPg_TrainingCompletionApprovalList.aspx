﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingCompletionApprovalList.aspx.vb"
    Inherits="Training_Training_Completion_Approval_wPg_TrainingCompletionApprovalList"
    Title="Training Completion Approval List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Completion Approval List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Training Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevel" runat="server" Text="Training" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpTraining" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblemp" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpemp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompletionStatus" runat="server" Text="Completion Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCompletionStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px;">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="gvTrainingApprovalList" runat="server" AutoGenerateColumns="false"
                                                    AllowPaging="false" Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="trainingrequestunkid,IsGrp,completed_statusunkid,iscompleted_submit_approval">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnComplete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgComplete" runat="server" ToolTip="Complete" CommandArgument='<%#Eval("trainingrequestunkid")%>'
                                                                        OnClick="lnkComplete_Click">
                                                                       <i class="fas fa-check-circle"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Start_Date" HeaderText="Start Date" ReadOnly="True" FooterText="colhSDate" />
                                                        <asp:BoundField DataField="End_Date" HeaderText="End Date" ReadOnly="True" FooterText="colhEDate" />
                                                        <asp:BoundField DataField="Training" HeaderText="Training" ReadOnly="true" FooterText="colhTraining" />
                                                        <asp:BoundField DataField="totaltrainingcost" HeaderText="Total Training Cost" ReadOnly="true"
                                                            FooterText="colhtotal_training_cost" />
                                                        <asp:BoundField DataField="approvedamount" HeaderText="Approved Amount" ReadOnly="true"
                                                            FooterText="colhapproved_amount" />
                                                        <asp:BoundField DataField="CompletedStatus" HeaderText="Completion Status" ReadOnly="true"
                                                            FooterText="colhCompletedStatus" />
                                                        <asp:BoundField DataField="Isgrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close"
                                    Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
