﻿#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region
Partial Class Training_Training_Completion_Approval_wPg_TrainingCompletionApprovalList
    Inherits Basepage

#Region " Private Variable "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTrainingCompletionApprovalList"
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillCombo()
                FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objApprovalMaster As New clstraining_requisition_approval_master
        Dim dsCombo As New DataSet
        'Hemant (03 Dec 2021) -- Start
        'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
        Dim objTPeriod As New clsTraining_Calendar_Master
        'Hemant (03 Dec 2021) -- End
        Try
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.            
            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            'Hemant (03 Dec 2021) -- End

            dsCombo = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

            With drpTraining
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", True)
            With drpemp
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objApprovalMaster.getCompletionStatusComboList("List", True)
            With cboCompletionStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            objTPeriod = Nothing
            'Hemant (03 Dec 2021) -- End
            objEmp = Nothing
            objApprovalMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objRequestMaster As New clstraining_request_master
        Dim dtApprovalList As New DataTable
        Dim mstrEmployeeIDs As String = ""
        Try
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToApproveRejectTrainingCompletion")) = False Then Exit Try
            End If

            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            If CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If
            'Hemant (03 Dec 2021) -- End

            If CInt(drpemp.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.employeeunkid = " & CInt(drpemp.SelectedValue) & " "
            End If

            If CInt(drpTraining.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.coursemasterunkid = " & CInt(drpTraining.SelectedValue) & " "
            End If

            If CInt(cboCompletionStatus.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.completed_statusunkid = " & CInt(cboCompletionStatus.SelectedValue) & " "
            End If

            strSearch &= "AND trtraining_request_master.iscompleted_submit_approval = 1 "

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objRequestMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), _
                                              True, CBool(Session("IsIncludeInactiveEmp")), "Training", , strSearch, , _
                                              CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

            Dim dtEmployeeView As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "employeeunkid", "employeename")
            If dsList.Tables(0).Columns.Contains("IsGrp") = False Then
                Dim dtCol As New DataColumn
                dtCol = New DataColumn
                dtCol.ColumnName = "IsGrp"
                dtCol.Caption = "IsGrp"
                dtCol.DataType = System.Type.GetType("System.Boolean")
                dtCol.DefaultValue = False
                dsList.Tables(0).Columns.Add(dtCol)

            End If
            Dim dRow As DataRow = Nothing
            For Each drEmployee As DataRow In dtEmployeeView.Rows
                Dim drRow() As DataRow = dsList.Tables(0).Select("employeeunkid = " & CInt(drEmployee.Item("employeeunkid")) & " ")
                If drRow.Length > 0 Then
                    dRow = dsList.Tables(0).NewRow()
                    dRow.Item("trainingrequestunkid") = -1
                    dRow.Item("employeeunkid") = CInt(drEmployee.Item("employeeunkid"))
                    dRow.Item("employeename") = CStr(drEmployee.Item("employeename"))
                    dRow.Item("IsGrp") = 1
                    dsList.Tables(0).Rows.Add(dRow)
                End If

            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables(0), "", "employeeunkid  , trainingrequestunkid  ", DataViewRowState.CurrentRows).ToTable.Copy


            gvTrainingApprovalList.DataSource = dtTable
            gvTrainingApprovalList.DataBind()
            'Dim mintTrainingRequestunkid As Integer = 0
            'Dim dList As DataTable = Nothing
            'Dim mstrStaus As String = ""
            'Dim mstrCompletedStatus As String = ""

            'If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

            '    For Each drRow As DataRow In dsList.Tables(0).Rows

            '        If CInt(drRow("pendingtrainingtranunkid")) <= 0 Then Continue For
            '        mstrStaus = ""

            '        If mintTrainingRequestunkid <> CInt(drRow("trainingrequestunkid")) Then
            '            dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND trainingrequestunkid = " & CInt(drRow("trainingrequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
            '            mintTrainingRequestunkid = CInt(drRow("trainingrequestunkid"))
            '            mstrStaus = ""
            '            mstrCompletedStatus = ""
            '        End If

            '        If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
            '            Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

            '            If dr.Length > 0 Then

            '                For i As Integer = 0 To dr.Length - 1

            '                    If CInt(drRow("statusunkid")) = 2 Then
            '                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Approved By :-  ") & drRow("ApproverName").ToString()
            '                        Exit For

            '                    ElseIf CInt(drRow("statusunkid")) = 1 Then

            '                        If CInt(dr(i)("statusunkid")) = 2 Then
            '                            mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        ElseIf CInt(dr(i)("statusunkid")) = 3 Then
            '                            mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        End If

            '                    ElseIf CInt(drRow("statusunkid")) = 3 Then
            '                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Rejected By :-  ") & drRow("ApproverName").ToString()
            '                        Exit For

            '                    End If

            '                Next
            '                For i As Integer = 0 To dr.Length - 1

            '                    If CInt(drRow("completed_statusunkid")) = 2 Then
            '                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Approved By :-  ") & drRow("ApproverName").ToString()
            '                        Exit For

            '                    ElseIf CInt(drRow("completed_statusunkid")) = 1 Then

            '                        If CInt(dr(i)("completed_statusunkid")) = 2 Then
            '                            mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        ElseIf CInt(dr(i)("completed_statusunkid")) = 3 Then
            '                            mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        End If

            '                    ElseIf CInt(drRow("completed_statusunkid")) = 3 Then
            '                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Rejected By :-  ") & drRow("ApproverName").ToString()
            '                        Exit For

            '                    End If

            '                Next

            '            End If

            '        End If

            '        If mstrStaus <> "" Then
            '            drRow("status") = mstrStaus.Trim
            '        End If
            '        If mstrCompletedStatus <> "" Then
            '            drRow("completed_status") = mstrCompletedStatus.Trim
            '        End If

            '    Next

            'End If

            'dtApprovalList = dsList.Tables(0).Clone

            'dtApprovalList.Columns("application_date").DataType = GetType(Object)

            'Dim dtRow As DataRow

            'For Each dRow As DataRow In dsList.Tables(0).Rows
            '    dtRow = dtApprovalList.NewRow
            '    For Each dtCol As DataColumn In dtApprovalList.Columns
            '        dtRow(dtCol.ColumnName) = dRow(dtCol.ColumnName)
            '    Next

            '    dtApprovalList.Rows.Add(dtRow)
            'Next

            'dtApprovalList.AcceptChanges()

            'Dim dtTable As New DataTable

            'dtTable = New DataView(dtApprovalList, "", "", DataViewRowState.CurrentRows).ToTable


            'Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "trainingrequestunkid", "employeeunkid")

            'Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("trainingrequestunkid") Equals drTemp.Field(Of Integer)("trainingrequestunkid") And _
            '             drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable



            'gvTrainingApprovalList.AutoGenerateColumns = False
            'gvTrainingApprovalList.DataSource = dtTable
            'gvTrainingApprovalList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview's Events"

    Protected Sub gvTrainingApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingApprovalList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "employeename").ToString
                    e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = 2 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                    ImgComplete.Visible = False
                Else

                    If e.Row.Cells(1).Text.ToString().Trim <> "" AndAlso e.Row.Cells(1).Text.Trim <> "&nbsp;" Then
                        e.Row.Cells(1).Text = CDate(e.Row.Cells(1).Text).Date.ToShortDateString
                    End If

                    If e.Row.Cells(2).Text.ToString().Trim <> "" AndAlso e.Row.Cells(2).Text.Trim <> "&nbsp;" Then
                        e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text).Date.ToShortDateString
                    End If

                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))

                    Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "
    Protected Sub lnkComplete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objApprovaltran As New clstrainingapproval_process_tran
        Dim objTrainingApprover As New clstraining_approver_master
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                Exit Sub
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("completed_statusunkid")) = enTrainingRequestStatus.REJECTED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                Exit Sub
            End If

            Session("mblnIsAddMode") = True
            Session("mblnFromCompleteMSS") = True
            'Session("mintMappingUnkid") = txtRole.Attributes("mappingunkid")
            'Session("PendingTrainingTranunkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("pendingtrainingtranunkid")
            Session("TrainingRequestunkid") = Convert.ToString(lnk.CommandArgument.ToString())
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
            objTrainingApprover = Nothing
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, lblDetialHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllevel.ID, lbllevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblemp.ID, lblemp.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCompletionStatus.ID, lblCompletionStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSearch.ID, btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(1).FooterText, gvTrainingApprovalList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(2).FooterText, gvTrainingApprovalList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(3).FooterText, gvTrainingApprovalList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(4).FooterText, gvTrainingApprovalList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(5).FooterText, gvTrainingApprovalList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(6).FooterText, gvTrainingApprovalList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(7).FooterText, gvTrainingApprovalList.Columns(7).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, lblDetialHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Me.lbllevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllevel.ID, lbllevel.Text)
            Me.lblemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblemp.ID, lblemp.Text)
            Me.lblCompletionStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCompletionStatus.ID, lblCompletionStatus.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSearch.ID, btnSearch.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")

            gvTrainingApprovalList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(1).FooterText, gvTrainingApprovalList.Columns(1).HeaderText)
            gvTrainingApprovalList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(2).FooterText, gvTrainingApprovalList.Columns(2).HeaderText)
            gvTrainingApprovalList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(3).FooterText, gvTrainingApprovalList.Columns(3).HeaderText)
            gvTrainingApprovalList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(4).FooterText, gvTrainingApprovalList.Columns(4).HeaderText)
            gvTrainingApprovalList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(5).FooterText, gvTrainingApprovalList.Columns(5).HeaderText)
            gvTrainingApprovalList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(6).FooterText, gvTrainingApprovalList.Columns(6).HeaderText)
            gvTrainingApprovalList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(7).FooterText, gvTrainingApprovalList.Columns(7).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
