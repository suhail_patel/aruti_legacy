﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class Training_Skip_Training_Requisition_And_Approval_wPg_SkipTrainingRequisitionAndApproval
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmSkipTrainingRequisitionAndApproval"
    Private mintTrainingRequestunkid As Integer
    Private mintCourseMasterunkid As Integer
    Private mintDepartTrainingNeedId As Integer = -1
    Private mintDepartmentListId As Integer = -1
    Private mintTrainingNeedAllocationID As Integer = -1
    Private mblnShowTrainingNamePopup As Boolean = False
#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                dtpApplicationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                mintTrainingNeedAllocationID = CInt(Session("TrainingNeedAllocationID"))
                FillCombo()
            Else
                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintCourseMasterunkid = CInt(ViewState("mintCourseMasterunkid"))
                mblnShowTrainingNamePopup = CBool(ViewState("mblnShowTrainingNamePopup"))
                mintDepartmentListId = CInt(ViewState("mintDepartmentListId"))
                mintTrainingNeedAllocationID = CInt(Me.ViewState("mintTrainingNeedAllocationID"))
                mintDepartTrainingNeedId = CInt(Me.ViewState("mintDepartTrainingNeedId"))
            End If

            If mblnShowTrainingNamePopup = True Then
                popupTrainingName.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            Me.ViewState("mintCourseMasterunkid") = mintCourseMasterunkid
            Me.ViewState("mblnShowTrainingCostItemPopup") = mblnShowTrainingNamePopup
            Me.ViewState("mintDepartmentListId") = mintDepartmentListId
            Me.ViewState("mintTrainingNeedAllocationID") = mintTrainingNeedAllocationID
            Me.ViewState("mintDepartTrainingNeedId") = mintDepartTrainingNeedId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            Dim intFirstOpen As Integer = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                intFirstOpen = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If

            dsCombo = objTPeriod.getListForCombo("List", True, 1)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen.ToString
            End With

            Dim strIDs As String = "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Trim <> "" Then
                Dim arr() As String = Session("Allocation_Hierarchy").ToString.Split(CChar("|"))
                Dim sIDs As String = String.Join(",", (From p In arr.AsEnumerable Where (CInt(p) >= mintTrainingNeedAllocationID) Select (p.ToString)).ToArray)
                If sIDs.Trim <> "" Then
                    strIDs = "0, 9, 10, " & sIDs
                End If
            End If
            dsCombo = objMaster.GetTrainingTargetedGroup("List", strIDs, False, False)

            With cboTargetedGroup
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                If dsCombo.Tables(0).Select("ID = " & mintTrainingNeedAllocationID & " ").Length > 0 Then
                    .SelectedValue = mintTrainingNeedAllocationID.ToString
                Else
                    .SelectedValue = "0"
                End If
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsCombo = Nothing : objTPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillDepartmentTrainingInfo()
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim objDepartTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objCommon As New clsCommon_Master
        Try
            If mintDepartTrainingNeedId > 0 Then

                objDepartTrainingNeed._Departmentaltrainingneedunkid = mintDepartTrainingNeedId
                'Hemant (03 Dec 2021) -- Start
                'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
                cboPeriod.SelectedValue = CStr(objDepartTrainingNeed._Periodunkid)
                'Hemant (03 Dec 2021) -- End
                mintCourseMasterunkid = CInt(objDepartTrainingNeed._Trainingcourseunkid)
                objCommon._Masterunkid = mintCourseMasterunkid
                txtTrainingName.Text = objCommon._Name

                mintDepartmentListId = CInt(objDepartTrainingNeed._Departmentunkid)
                cboTargetedGroup.SelectedValue = CStr(objDepartTrainingNeed._Targetedgroupunkid)
                txtNoOfStaff.Decimal_ = objDepartTrainingNeed._Noofstaff
                Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDFsource = Nothing
            objDepartTrainingNeed = Nothing
        End Try
    End Sub

    Private Sub FillAllocEmployee()
        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsList As DataSet = Nothing
        Dim dsAllocEmp As DataSet = Nothing
        Dim dsRequestedEmp As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim intColType As Integer = 0
        Dim strFilter As String = ""
        Dim strIDs As String = ""
        Try

            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If


            strIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

            If strIDs.Trim <> "" Then

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case enAllocation.BRANCH
                        strFilter &= " AND T.stationunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT_GROUP
                        strFilter &= " AND T.deptgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT
                        strFilter &= " AND T.departmentunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION_GROUP
                        strFilter &= " AND T.sectiongroupunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION
                        strFilter &= " AND T.sectionunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT_GROUP
                        strFilter &= " AND T.unitgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT
                        strFilter &= " AND T.unitunkid IN (" & strIDs & ") "

                    Case enAllocation.TEAM
                        strFilter &= " AND T.teamunkid IN (" & strIDs & ") "

                    Case enAllocation.JOB_GROUP
                        strFilter &= " AND J.jobgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.JOBS
                        strFilter &= " AND J.jobunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASS_GROUP
                        strFilter &= " AND T.classgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASSES
                        strFilter &= " AND T.classunkid IN (" & strIDs & ") "

                    Case enAllocation.COST_CENTER
                        strFilter &= " AND C.costcenterunkid IN (" & strIDs & ") "

                End Select

            Else
                strFilter &= " AND 1 = 2 "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , 0, strAdvanceFilterQuery:=strFilter)


            dtTable = dsList.Tables(0)
            objEmp = Nothing

            'dtTable = New DataView(dtTable, "", "employeename", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()

                Dim intCellCount As Integer = dgvAllocEmp.Rows(0).Cells.Count

                dgvAllocEmp.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvAllocEmp.Rows(0).Cells(i).Visible = False
                Next
                dgvAllocEmp.Rows(0).Cells(0).Text = "No Records Found"
            Else
                If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                    strFilter &= " AND 1 = 2 "
                End If
                dsAllocEmp = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True, "", mintDepartTrainingNeedId, "")

                Dim strExistIDs As String = String.Join(",", (From p In dsAllocEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strExistIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dtTable Where (strExistIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                    If row.Count > 0 Then
                        'chkChooseEmployee.Checked = True
                        'Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)
                    End If
                End If

                txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text))
                dsRequestedEmp = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                          eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                          True, CBool(Session("IsIncludeInactiveEmp")), "Training", , _
                                                          " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                Dim strRequestedEmpIDs As String = String.Join(",", (From p In dsRequestedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strRequestedEmpIDs.Trim <> "" Then
                    Dim xTable As DataTable
                    Dim xRow() As DataRow = Nothing
                    xRow = dtTable.Select("employeeunkid NOT IN (" & strRequestedEmpIDs & ")")
                    If xRow.Length > 0 Then
                        xTable = xRow.CopyToDataTable()
                        dtTable = xTable
                    Else
                        dtTable = Nothing
                    End If
                    txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text) - CInt(dsRequestedEmp.Tables(0).Rows.Count))
                End If

                If dtTable IsNot Nothing Then
                    dtTable = New DataView(dtTable, "", "IsChecked DESC, employeename", DataViewRowState.CurrentRows).ToTable
                End If

                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()
            End If

            'chkAllocEmpOnlyTicked.Checked = False

            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable = Nothing
            objDTEmp = Nothing
        End Try
    End Sub

    Private Sub FillTargetedGroup()
        Dim objDTAllocation As New clsDepttrainingneed_allocation_Tran
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim dsTargetGroup As DataSet = Nothing
        Dim dsRequestedEmp As DataSet = Nothing
        Dim intColType As Integer = 0
        Try


            Select Case CInt(cboTargetedGroup.SelectedValue)

                Case 0 'Employee Names
                    Dim objEmp As New clsEmployee_Master
                    dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , mintDepartmentListId)

                    intColType = 5

                    dtTable = dsList.Tables(0)
                    objEmp = Nothing

                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " stationunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " deptgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " departmentunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION_GROUP

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " sectiongroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " sectionunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT_GROUP

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " unitgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " unitunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " teamunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOB_GROUP

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " jobgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOBS

                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " jobunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.CLASS_GROUP

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " classgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.CLASSES

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " classesunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable



                Case enAllocation.COST_CENTER

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2

                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " costcenterunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("code").ColumnName = "Code"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobunkid").ColumnName = "Id"
                dtTable.Columns("Code").ColumnName = "Code"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentercode").ColumnName = "Code"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(2).ColumnName = "Code"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_code").ColumnName = "Code"
                dtTable.Columns("country_name").ColumnName = "Name"
            ElseIf intColType = 5 Then
                dtTable.Columns("employeeunkid").ColumnName = "Id"
                dtTable.Columns("employeecode").ColumnName = "Code"
                dtTable.Columns("employeename").ColumnName = "Name"
            End If

            dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()

                Dim intCellCount As Integer = dgvEmployee.Rows(0).Cells.Count

                dgvEmployee.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvEmployee.Rows(0).Cells(i).Visible = False
                Next
                dgvEmployee.Rows(0).Cells(0).Text = "No Records Found"
            Else

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case 0 'Employee Names
                        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
                        dsTargetGroup = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True, "", mintDepartTrainingNeedId)


                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id IN (" & strIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            End If
                            Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                        End If

                        txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text))
                        dsRequestedEmp = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                  eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                                  True, CBool(Session("IsIncludeInactiveEmp")), "Training", , _
                                                                  " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                                  CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                        Dim strRequestedEmpIDs As String = String.Join(",", (From p In dsRequestedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strRequestedEmpIDs.Trim <> "" Then
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id NOT IN (" & strRequestedEmpIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            Else
                                dtTable = Nothing
                            End If
                            txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text) - CInt(dsRequestedEmp.Tables(0).Rows.Count))
                        End If

                    Case Else
                        dsTargetGroup = objDTAllocation.GetList("List", mintDepartTrainingNeedId)

                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("allocationtranunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id  IN (" & strIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            Else
                                dtTable = Nothing
                            End If
                        End If
                End Select

                If dtTable IsNot Nothing Then
                    dtTable = New DataView(dtTable, "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable
                End If
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()
                If dtTable IsNot Nothing AndAlso dtTable.Select("IsChecked = 1 ").Length = dgvEmployee.Rows.Count Then
                    CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
                End If

                Call FillAllocEmployee()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDTAllocation = Nothing
            objTrainingRequest = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            mintTrainingRequestunkid = 0
            mintDepartTrainingNeedId = 0
            txtTrainingName.Text = ""

            cboTargetedGroup.SelectedValue = "0"
            txtNoOfStaff.Decimal_ = 0
            dgvEmployee.DataSource = Nothing
            dgvEmployee.DataBind()

            dgvAllocEmp.DataSource = Nothing
            dgvAllocEmp.DataBind()
            txtNoOfVacantStaff.Decimal_ = 0

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try
            objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue"), Me)
                Return False
            End If

            If CInt(txtTrainingName.Text.Trim.Length) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Training Name is mandatory information. Please select Training Name to continue"), Me)
                Return False
            End If

            If dtpApplicationDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Application date is mandatory information. Please select Application date to continue"), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
        End Try
    End Function

    Private Sub SetValue(ByRef objRequestMaster As clstraining_request_master)
        Dim objDepartTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim objFinancingSources As New clstraining_request_financing_sources_tran
        Dim dsFSource As DataSet = Nothing
        Try
            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            objDepartTrainingNeed._Departmentaltrainingneedunkid = mintDepartTrainingNeedId
            objRequestMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            objRequestMaster._Application_Date = dtpApplicationDate.GetDate
            objRequestMaster._Coursemasterunkid = mintCourseMasterunkid

            objRequestMaster._IsScheduled = True

            objRequestMaster._Start_Date = objDepartTrainingNeed._Startdate
            objRequestMaster._End_Date = objDepartTrainingNeed._Enddate
            objRequestMaster._Trainingproviderunkid = objDepartTrainingNeed._Trainingproviderunkid
            objRequestMaster._Trainingvenueunkid = objDepartTrainingNeed._Trainingvenueunkid
            objRequestMaster._TotalTrainingCost = 0

            objRequestMaster._IsAlignedCurrentRole = False
            objRequestMaster._IsPartofPDP = False
            objRequestMaster._IsForeignTravelling = False

            objRequestMaster._ExpectedReturn = ""
            objRequestMaster._Remarks = ""
            objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
            objRequestMaster._IsSubmitApproval = True
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Userunkid = CInt(Session("UserId"))
                objRequestMaster._LoginEmployeeunkid = -1
            Else
                objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objRequestMaster._Userunkid = -1
            End If
            objRequestMaster._IsWeb = True
            objRequestMaster._Isvoid = False
            objRequestMaster._Voiddatetime = Nothing
            objRequestMaster._Voidreason = ""
            objRequestMaster._Voiduserunkid = -1
            objRequestMaster._ClientIP = CStr(Session("IP_ADD"))
            objRequestMaster._FormName = mstrModuleName
            objRequestMaster._HostName = CStr(Session("HOST_NAME"))
            If mintDepartTrainingNeedId > 0 Then
                objRequestMaster._DepartmentalTrainingNeedunkid = mintDepartTrainingNeedId
            End If
            objRequestMaster._IsEnrollConfirm = True
            objRequestMaster._IsEnrollReject = False
            objRequestMaster._EnrollAmount = 0

            objRequestMaster._CompletedStatusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            objRequestMaster._IsSkipTrainingRequestAndApproval = True

            dsFSource = objDFsource.GetList("List", mintDepartTrainingNeedId)
            Dim lstFinancingSources As New List(Of clstraining_request_financing_sources_tran)

            For Each drRow As DataRow In dsFSource.Tables(0).Rows
                objFinancingSources = New clstraining_request_financing_sources_tran

                With objFinancingSources
                    .pintTrainingRequestfinancingsourcestranunkid = -1
                    .pintTrainingRequestunkid = mintTrainingRequestunkid
                    .pintFinancingsourceunkid = CInt(drRow.Item("financingsourceunkid").ToString())

                    .pblnIsweb = True
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = 0
                    End If
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                lstFinancingSources.Add(objFinancingSources)
            Next
            objRequestMaster._lstFinancingSourceNew = lstFinancingSources

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDepartTrainingNeed = Nothing
            objDFsource = Nothing
            objFinancingSources = Nothing
        End Try
    End Sub

    Private Sub Submit_Click()
        Dim objRequestMaster As New clstraining_request_master
        Dim strEmployeeList As String = String.Empty

        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            Dim allEmp As List(Of String)

            If cboTargetedGroup.SelectedValue = "0" Then 'employee names
                gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee."), Me)
                    Exit Sub
                ElseIf gRow.Count > CInt(txtNoOfVacantStaff.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s)."), Me)
                    Exit Sub
                End If

                allEmp = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("id").ToString()).ToList
            Else
                gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee."), Me)
                    Exit Sub
                ElseIf gRow.Count > CInt(txtNoOfVacantStaff.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s)."), Me)
                    Exit Sub
                End If

                allEmp = gRow.AsEnumerable().Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
            End If

            strEmployeeList = String.Join(",", allEmp.ToArray)

            SetValue(objRequestMaster)

            If objRequestMaster.InsertAllByEmployeeList(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       CStr(Session("UserAccessModeSetting")), _
                                                       Session("EmployeeAsOnDate").ToString, _
                                                       strEmployeeList, Nothing) = False Then
                DisplayMessage.DisplayMessage(objRequestMaster._Message, Me)
            Else

                Call Clear_Controls()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Protected Sub btnTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrainingName.Click

        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAddTrainingName.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Training is compulsory information.Please Check atleast One Training.", Me)
                Exit Sub
            End If
            If gRow Is Nothing OrElse gRow.Count > 1 Then
                DisplayMessage.DisplayMessage("Please Check Only One Training.", Me)
                Exit Sub
            End If
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For Each dgRow As GridViewRow In gRow
                    mintCourseMasterunkid = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("masterunkid").ToString())
                    txtTrainingName.Text = dgvAddTrainingName.DataKeys(dgRow.RowIndex)("name").ToString()
                    mintDepartTrainingNeedId = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("departmentaltrainingneedunkid").ToString())
                    FillDepartmentTrainingInfo()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally


        End Try

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Are you sure you want to submit for Skip Training Request and Approval?")
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Links Event "

    Protected Sub lnkAddTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingName.Click
        Try
            dgvAddTrainingName.DataSource = Nothing
            dgvAddTrainingName.DataBind()

            Dim objCalender As New clsTraining_Calendar_Master
            Dim dsList As DataSet = objCalender.getListForCombo("List", False, enStatusType.OPEN)
            With cboTrainingCalender
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
            End With
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            cboTrainingCalender.SelectedValue = CStr(cboPeriod.SelectedValue)
            lblTrainingCalender.Visible = True
            cboTrainingCalender.Visible = True
            cboTrainingCalender.Enabled = False
            'Hemant (03 Dec 2021) -- End
            dsList.Clear()
            dsList = Nothing
            cboTrainingCalender_SelectedIndexChanged(New Object, New EventArgs)


            mblnShowTrainingNamePopup = True
            popupTrainingName.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Protected Sub cboTrainingCalender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCalender.SelectedIndexChanged
        Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsList As DataSet = Nothing
        Try
            Dim strFilter As String = ""

            strFilter = " AND trdepartmentaltrainingneed_master.periodunkid = '" & CInt(cboTrainingCalender.SelectedValue) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0)  = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0)  = 0 "
            dsList = objDeptTrainingNeed.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), CInt(Session("TrainingNeedAllocationID")), "Tranining" _
                                                        , False, strFilter, 0, 0)


            If dsList.Tables.Count > 0 Then
                Dim xTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid")

                If xTable.Columns.Contains("IsGrp") = False Then
                    xTable.Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
                End If

                For Each r As DataRow In xTable.Rows
                    r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    r("IsGrp") = False
                Next

                xTable.Columns("trainingcourseunkid").ColumnName = "masterunkid"
                xTable.Columns("trainingcoursename").ColumnName = "name"

                Dim dsTCategoryList As DataSet = objTCategory.getListForCombo("List", False)
                Dim dRow As DataRow = Nothing
                For Each drTCategory As DataRow In dsTCategoryList.Tables(0).Rows
                    Dim drRow() As DataRow = xTable.Select("trainingcategoryunkid = " & CInt(drTCategory.Item("categoryunkid")) & " ")
                    If drRow.Length > 0 Then
                        dRow = xTable.NewRow()
                        dRow.Item("departmentaltrainingneedunkid") = -1
                        dRow.Item("trainingcategoryunkid") = CInt(drTCategory.Item("categoryunkid"))
                        dRow.Item("trainingcategoryname") = CStr(drTCategory.Item("categoryname"))
                        dRow.Item("masterunkid") = -1
                        dRow.Item("name") = ""
                        dRow.Item("startdate") = ""
                        dRow.Item("enddate") = ""
                        dRow.Item("allocationtranname") = ""
                        dRow.Item("allocationtranunkid") = -1
                        dRow.Item("IsGrp") = 1
                        xTable.Rows.Add(dRow)
                    End If
                Next

                xTable = New DataView(xTable, "", "trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy

                dgvAddTrainingName.DataSource = xTable
                dgvAddTrainingName.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDeptTrainingNeed = Nothing
            objTCategory = Nothing
        End Try
    End Sub

    Protected Sub cboTargetedGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTargetedGroup.SelectedIndexChanged
        Try
            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvEmployee.Enabled = True
                lblNoOfVacantStaff.Visible = False
                txtNoOfVacantStaff.Visible = False
                pnlAllocEmp.Visible = False
            Else
                dgvEmployee.Enabled = True
                lblNoOfVacantStaff.Visible = True
                txtNoOfVacantStaff.Visible = True
                pnlAllocEmp.Visible = True
            End If
            Call FillTargetedGroup()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " GridView "

    Protected Sub dgvAddTrainingName_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAddTrainingName.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(dgvAddTrainingName.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "trainingcategoryname").ToString
                    e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = 2 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    Dim ChkgvSelect As CheckBox = TryCast(e.Row.FindControl("ChkgvSelect"), CheckBox)
                    ChkgvSelect.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Protected Sub ChkAllTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If IsValidData(False, False) = False Then Exit Try

            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            For Each gvRow As GridViewRow In dgvEmployee.Rows
                CType(gvRow.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = chkSelectAll.Checked
            Next

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkgvSelectTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If IsValidData(False, False) = False Then Exit Try

            Dim chkSelect As CheckBox = CType(sender, CheckBox)

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso dgvEmployee.Rows.Count = gRow.Count Then
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
            Else
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = False
            End If

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnSubmit.ID.ToUpper
                    Call Submit_Click()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, lblDetialHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApplicationDate.ID, lblApplicationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingName.ID, lblTrainingName.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEmployeeDetailsCaption.ID, lblEmployeeDetailsCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTargetedGroup.ID, lblTargetedGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblNoOfStaff.ID, lblNoOfStaff.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblNoOfVacantStaff.ID, lblNoOfVacantStaff.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTTrainingName.ID, lblTTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCalender.ID, lblTrainingCalender.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lnkAddTrainingName.ID, lnkAddTrainingName.ToolTip)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSubmit.ID, Me.btnSubmit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnTrainingName.ID, Me.btnTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCloseTCostItem.ID, Me.btnCloseTCostItem.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, lblDetialHeader.Text)

            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            Me.lblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApplicationDate.ID, lblApplicationDate.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingName.ID, lblTrainingName.Text)

            Me.lblEmployeeDetailsCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEmployeeDetailsCaption.ID, lblEmployeeDetailsCaption.Text)
            Me.lblTargetedGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTargetedGroup.ID, lblTargetedGroup.Text)
            Me.lblNoOfStaff.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblNoOfStaff.ID, lblNoOfStaff.Text)
            Me.lblNoOfVacantStaff.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblNoOfVacantStaff.ID, lblNoOfVacantStaff.Text)
            Me.lblTTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTTrainingName.ID, lblTTrainingName.Text)
            Me.lblTrainingCalender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCalender.ID, lblTrainingCalender.Text)

            Me.lnkAddTrainingName.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lnkAddTrainingName.ID, lnkAddTrainingName.ToolTip)

            Me.btnSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnTrainingName.ID, Me.btnTrainingName.Text).Replace("&", "")
            Me.btnCloseTCostItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCloseTCostItem.ID, Me.btnCloseTCostItem.Text).Replace("&", "")

            dgvEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            dgvEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            dgvAllocEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            dgvAllocEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            dgvAddTrainingName.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, Training Name is mandatory information. Please select Training Name to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, Application date is mandatory information. Please select Application date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Are you sure you want to submit for Skip Training Request and Approval?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
