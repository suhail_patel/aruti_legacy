﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_SkipTrainingRequisitionAndApproval.aspx.vb"
    Inherits="Training_Skip_Training_Requisition_And_Approval_wPg_SkipTrainingRequisitionAndApproval"
    Title="Skip Training Requisition And Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script type="text/javascript">
                        
            $("body").on("click", "[id*=ChkAll]", function() {    
                var chkHeader = $(this);
               var grid = $(this).closest(".body");
                $("[id*=ChkgvSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvSelect]", function() {   
                var grid = $(this).closest(".body");
               var chkHeader = $("[id*=chkAllSelect]", grid);
                debugger;
               if ($("[id*=ChkgvSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                 }
               else {
                    chkHeader.prop("checked", false);
                }
          });

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearchAddTrainingName').val().length > 0) {
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr').hide();
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr:first').show();
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchAddTrainingName').val() + '\')').parent().show();
                }
                else if ($('#txtSearchAddTrainingName').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= dgvAddTrainingName.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearchAddTrainingName').val('');
                $('#<%= dgvAddTrainingName.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchAddTrainingName').focus();
            } 

	  function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();
                
            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }
                
            }
            else if ($('#' + txtID).val().length == 0) {
            resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
            $('.norecords').remove();
            }            
            $('#' + txtID).focus();
        }
    </script>
    
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
            <cnf:confirmation id="cnfConfirm" runat="server" title="Aruti" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Skip Training Requisition And Approval"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Training Request Info"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" Text="Training Period" runat="server" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplicationDate" runat="server" Text="Application Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpApplicationDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtTrainingName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 m-t-30">
                                        <asp:LinkButton runat="server" ID="lnkAddTrainingName"  ToolTip="Add Training Name">
                                                                        <i class="fas fa-plus-circle" ></i>
                                        </asp:LinkButton>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblEmployeeDetailsCaption" runat="server" Text="Employee Details" ></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTargetedGroup" Text="Targeted Group" runat="server" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTargetedGroup" runat="server" AutoPostBack="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNoOfStaff" Text="Number of Staff(s)" runat="server" CssClass="form-label" />
                                        <nut:NumericText ID="txtNoOfStaff" runat="server" cssclass="form-control" Enabled="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNoOfVacantStaff" Text="Number of Vacant Staff(s)" runat="server"
                                            CssClass="form-label" />
                                        <nut:NumericText ID="txtNoOfVacantStaff" runat="server" cssclass="form-control" Enabled="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" id="txtSearchEmp" name="txtSearchEmp" placeholder="Search Targeted Group"
                                                                    maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchEmp','<%= dgvEmployee.ClientID %>');" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 34vh;">
                                                            <asp:GridView ID="dgvEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                Enabled="false" DataKeyNames="id" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-CssClass="align-center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAllTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                AutoPostBack="true" OnCheckedChanged="ChkAllTargetedGroup_CheckedChanged"/>
                                                                            
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvSelectTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                Checked='<%# Eval("IsChecked")%>' AutoPostBack="true" OnCheckedChanged="ChkgvSelectTargetedGroup_CheckedChanged" />
                                                                            
                                                                        </ItemTemplate>
                                                                        <HeaderStyle />
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                    <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                    <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <HeaderTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="lnkDeleteAllEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                    CommandName="RemoveAll"> <i class="fas fa-times text-danger"></i> </asp:LinkButton>
                                                                            </span>
                                                                        </HeaderTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlAllocEmp" runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <input type="text" id="txtSearchAllocEmp" name="txtSearchAllocEmp" placeholder="Search Employee"
                                                                        maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchAllocEmp', '<%= dgvAllocEmp.ClientID %>');" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="table-responsive" style="max-height: 34vh;">
                                                                <asp:GridView ID="dgvAllocEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                    DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                            HeaderStyle-CssClass="align-center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="ChkAllAllocEmp" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAllocEmp');" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="ChkgvSelectAllocEmp" runat="server" Text=" " CssClass="chk-sm"
                                                                                    Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this,'ChkgvSelectAllocEmp', 'ChkAllAllocEmp')" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle />
                                                                            <ItemStyle />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                        <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <HeaderTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="lnkDeleteAllAllocEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                        CommandName="RemoveAll"> <i class="fas fa-times text-danger"></i> </asp:LinkButton>
                                                                                </span>
                                                                            </HeaderTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupTrainingName" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hfTrainingName" runat="server" CancelControlID="hfTrainingName"
                    PopupControlID="pnlTrainingName">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlTrainingName" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTTrainingName" Text="Add Training Name" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblTrainingCalender" runat="server" Text="Training Calendar" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboTrainingCalender" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="txtSearchAddTrainingName" name="txtSearch" placeholder="Type To Search Text"
                                            maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTrainingName', '<%= dgvAddTrainingName.ClientID %>');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="max-height: 415px;">
                                    <asp:GridView ID="dgvAddTrainingName" runat="server" AutoGenerateColumns="False"
                                        AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid,name,trainingcategoryname,allocationtranname,departmentaltrainingneedunkid,IsGrp">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="align-center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                </ItemTemplate>
                                                <HeaderStyle />
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                        </Columns>
                                        <HeaderStyle Font-Bold="False" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <div style="float: left">
                            <asp:Button ID="btnAddTrainingName" runat="server" CssClass="btn btn-primary" Text="Add Training"
                                Visible="false" />
                        </div>
                        <asp:Button ID="btnTrainingName" runat="server" CssClass="btn btn-primary" Text="Add" />
                        <asp:Button ID="btnCloseTCostItem" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="hfTrainingName" runat="Server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
