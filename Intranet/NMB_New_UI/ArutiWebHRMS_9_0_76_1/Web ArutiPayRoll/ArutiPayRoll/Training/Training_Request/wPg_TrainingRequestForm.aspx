﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingRequestForm.aspx.vb"
    Inherits="Training_Training_Request_wPg_TrainingRequestForm" Title="Training Request Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        prm.add_endRequest(endTotalAmountRequestHandler);
        function endTotalAmountRequestHandler(sender, event) {
          
            $('input[id*=txtAmount]').blur(function() {
                CalcTotal();
            });
        }
        
         function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
            //Pinkal (08-Apr-2021)-- Start
            //Enhancement  -  Working on Employee Claim Form Report.
           $("body").on("click", "[id*=ChkAll]", function() {    
                var chkHeader = $(this);
               var grid = $(this).closest(".body");
                $("[id*=ChkgvSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvSelect]", function() {   
                var grid = $(this).closest(".body");
               var chkHeader = $("[id*=chkAllSelect]", grid);
                debugger;
               if ($("[id*=ChkgvSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                 }
               else {
                    chkHeader.prop("checked", false);
                }
          });

//        $("body").on('click', "[id*=ChkgvSelect]", function() {
//            $('input[type="checkbox"]').not(this).prop('checked', false);
//            });
        
       //Pinkal (08-Apr-2021)-- End

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearchAddTrainingName').val().length > 0) {
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr').hide();
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr:first').show();
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchAddTrainingName').val() + '\')').parent().show();
                }
                else if ($('#txtSearchAddTrainingName').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= dgvAddTrainingName.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearchAddTrainingName').val('');
                $('#<%= dgvAddTrainingName.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchAddTrainingName').focus();
            }
            
             $("body").on("click", "[id*=ChkAddAttendedTrainingAll]", function() {    
                var chkHeader = $(this);
                var grid = $(this).closest(".body");
                $("[id*=ChkgvAddAttendedTrainingSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvAddAttendedTrainingSelect]", function() {   
                var grid = $(this).closest(".body");
                var chkHeader = $("[id*=chkAllSelect]", grid);
                debugger;
                if ($("[id*=ChkgvAddAttendedTrainingSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromAddAttendedTrainingSearching() {
                if ($('#txtSearchAddAttendedTrainingName').val().length > 0) {
                    $('#<%= dgvAddAttendedTraining.ClientID %> tbody tr').hide();
                    $('#<%= dgvAddAttendedTraining.ClientID %> tbody tr:first').show();
                    $('#<%= dgvAddAttendedTraining.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchAddAttendedTrainingName').val() + '\')').parent().show();
                }
                else if ($('#txtSearchAddAttendedTrainingName').val().length == 0) {
                    resetFromAddAttendedTrainingSearchValue();
                }
                if ($('#<%= dgvAddAttendedTraining.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromAddAttendedTrainingSearchValue();
                }
            }
            function resetFromAddAttendedTrainingSearchValue() {
                $('#txtSearchAddAttendedTrainingName').val('');
                $('#<%= dgvAddAttendedTraining.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchAddAttendedTrainingName').focus();
            }
            
            
        function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();
                
            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }
                
            }
            else if ($('#' + txtID).val().length == 0) {
            resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
            $('.norecords').remove();
            }            
            $('#' + txtID).focus();
        }
        
         function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboScanDcoumentType.ClientID %>');
            var cboEmp = $('#<%= cboEmployee.ClientID %>');
             var txtTrName = $('#<%= txtTrainingName.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            
            return true;
        }    
    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
            <der:DelReason ID="delReason" runat="server" Title="Aruti" />
            <cnf:Confirmation ID="cnfApprovDisapprove" runat="server" Title="Aruti" />
            <cnf:Confirmation ID="cntSaveConfirm" runat="server" Title="Aruti" />
            <%--<div class="block-header">
                <h2>
                    <asp:Label ID="lblHeader" runat="server" Text="Training Request Form"></asp:Label>
                </h2>
            </div>--%>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="card profile-card">
                        <div class="profile-header">
                            &nbsp;</div>
                        <div class="profile-body">
                            <div class="image-area">
                                <uc8:ZoomImage ID="imgEmp" runat="server" ZoomPercentage="250" />
                            </div>
                            <div class="content-area">
                                <h4>
                                    <asp:Label ID="objlblEmployeeName" runat="server" Text="Employee Name"></asp:Label>
                                </h4>
                                <p>
                                    <asp:Label ID="objlblJob" runat="server" Text="Job Title"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="objlblDepartment" runat="server" Text="Department"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="objlblLineManager" runat="server" Text="Line Manager"></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Training Request Form"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <asp:Panel ID="pnlPart1" runat="server" Width="100%">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblTrainingRequestDetails" runat="server" Text="Training Request Details"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" Text="Training Period" runat="server" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="false">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <asp:Label ID="lblApplyingScheduledTraining" runat="server" Text="Applying for Pre-Scheduled Training?"></asp:Label>
                                                        <asp:RadioButton ID="rdbApplyingScheduledTrainingYes" runat="server" Text="Yes" GroupName="ApplyingScheduledTraining"
                                                            AutoPostBack="true" />
                                                        <asp:RadioButton ID="rdbApplyingScheduledTrainingNo" runat="server" Text="No" GroupName="ApplyingScheduledTraining"
                                                            AutoPostBack="true" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApplicationDate" runat="server" Text="Application Date"></asp:Label>
                                                        <div class="form-group">
                                                            <uc2:DateCtrl ID="dtpApplicationDate" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix" style="display:none">
                                                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">                                                        
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTrainingName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 m-t-30">
                                                        <asp:LinkButton runat="server" ID="lnkAddTrainingName" CssClass="m-r-10" ToolTip="Add Training Name">
                                                         <i class="fas fa-plus-circle" ></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingName" runat="server" Text="Training Name"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingName" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                                        <div class="form-group">
                                                            <uc2:DateCtrl ID="dtpStartDate" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                                        <div class="form-group">
                                                            <uc2:DateCtrl ID="dtpEndDate" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblProviderName" runat="server" Text="Provider Name"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingProvider" runat="server" AutoPostBack="false">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblProviderAddress" runat="server" Text="Provider Address"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingVenue" runat="server" AutoPostBack="false">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingCost" runat="server" Text="Training Cost"></asp:Label>
                                                        <div class="table-responsive" style="max-height: 300px">
                                                            <asp:GridView ID="dgvTrainingCostItem" runat="server" AutoGenerateColumns="False"
                                                                AllowPaging="false" DataKeyNames="trainingrequestcosttranunkid,infounkid,amount"
                                                                CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <%--  <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />--%>
                                                                    <asp:BoundField DataField="info_name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEInfoName" />
                                                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="align-center"
                                                                        ItemStyle-HorizontalAlign="Right" FooterText="colhAmount">
                                                                        <ItemTemplate>
                                                                            <nut:NumericText ID="txtAmount" runat="server" Type="Point" Style="text-align: right"
                                                                                CssClass="form-control" Text='<%# Eval("amount") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--  <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                    <ItemStyle HorizontalAlign="Center" />                                                                 
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="DeleteTCostItem" runat="server" ToolTip="Remove">
                                                                              <i class="fas fa-trash text-danger"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <div>
                                                            <asp:Label ID="lblFinancingSource" runat="server" Text="Financing Source"></asp:Label>
                                                            <div style="float: right; display: none;">
                                                                <asp:LinkButton runat="server" ID="lnkAddFinancingSource" CssClass="m-r-10" ToolTip="Add Financing Source">
                                                                 <i class="fas fa-plus-circle" ></i>
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="table-responsive" style="max-height: 50vh;">
                                                            <asp:GridView ID="dgvFinancingSource" runat="server" AutoGenerateColumns="False"
                                                                AllowPaging="false" DataKeyNames="masterunkid" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-CssClass="align-center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAllFSource" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvSelectFSource" runat="server" Text=" " CssClass="chk-sm" Checked='<%# Eval("IsChecked")%>' />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle />
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                    <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <HeaderTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="lnkDeleteAllFSource" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                    CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                </asp:LinkButton>
                                                                            </span>
                                                                        </HeaderTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTotalTrainingCost" runat="server" Text="Total Training Cost" Class="TotalLabel"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line" style="text-align: right;">
                                                                <asp:TextBox ID="txtTotalTrainingCost" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0" CssClass="form-control" Enabled="false"
                                                                    EnableViewState="False" ViewStateMode="Disabled"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <div>
                                                            <asp:Label ID="lblTrainingAttended" runat="server" Text="List of the Training attended in the past 1 year"></asp:Label>
                                                            <div style="float: right">
                                                                <asp:LinkButton runat="server" ID="lnkAddAttendedTraining" CssClass="m-r-10" ToolTip="Add Attended Training">
                                                                 <i class="fas fa-plus-circle" ></i>
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="table-responsive" style="max-height: 300px">
                                                            <asp:GridView ID="dgvSelectedAttendedTraining" runat="server" AutoGenerateColumns="False"
                                                                AllowPaging="false" DataKeyNames="trainingattendedtranunkid" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <%-- <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />--%>
                                                                    <asp:BoundField DataField="course_name" HeaderText="Course Name" ReadOnly="true"
                                                                        FooterText="dgcolhECourseName" />
                                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <%--<HeaderTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="lnkDeleteAllFSource" runat="server" ToolTip="Remove All">
                                                                               <i class="fas fa-times text-danger"></i>
                                                                            </asp:LinkButton>
                                                                        </span>
                                                                    </HeaderTemplate>--%>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkDeleteSelectedAttendedTraining" runat="server" ToolTip="Remove"
                                                                                OnClick="lnkDeleteSelectedAttendedTraining_Click">
                                                                              <i class="fas fa-trash text-danger"></i>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAlignedCurrentJob" runat="server" Text="Is this training aligned with your current role?"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:RadioButton ID="rdbAlignedCurrentJobYes" runat="server" Text="Yes" GroupName="AlignedCurrentJob" />
                                                        <asp:RadioButton ID="rdbAlignedCurrentJobNo" runat="server" Text="No" GroupName="AlignedCurrentJob" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRecommendedPDP" runat="server" Text="Is this training part of the recommended training in your PDP?"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:RadioButton ID="rdbRecommendedPDPYes" runat="server" Text="Yes" GroupName="RecommendedPDP"
                                                            Enabled="false" />
                                                        <asp:RadioButton ID="rdbRecommendedPDPNo" runat="server" Text="No" GroupName="RecommendedPDP"
                                                            Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblForeignTravelling" runat="server" Text="Does this Training Require Travelling to Foreign Country?"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:RadioButton ID="rdbForeignTravellingYes" runat="server" Text="Yes" GroupName="ForeignTravelling" />
                                                        <asp:RadioButton ID="rdbForeignTravellingNo" runat="server" Text="No" GroupName="ForeignTravelling" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblExpectedReturn" runat="server" Text="Please describe the Expected Return on Investment"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtExpectedReturn" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                    Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Any other Remarks/Comments"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                    Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlComplete" runat="server" Visible="false">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblTrainingCompletedInfo" runat="server" Text="Training Completion Info"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkOtherQualification" runat="server" Text="Other Qualification"
                                                            AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Panel ID="pnlQualificationGroup" runat="server" Visible="false">
                                                            <asp:Label ID="lblQualificationGroup" runat="server" Text="Qualification Group :"
                                                                CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList data-live-search="true" ID="cboQualifGrp" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlOtherQualificationGroup" runat="server" Visible="false">
                                                            <asp:Label ID="lblOtherQualificationGroup" runat="server" Text="Other Qualification Group :"
                                                                CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtOtherQualificationGrp" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Panel ID="pnlQualification" runat="server" Visible="false">
                                                            <asp:Label ID="lblQualification" runat="server" Text="Qualification :" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList data-live-search="true" ID="cboQualifcation" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlOtherQualification" runat="server" Visible="false">
                                                            <asp:Label ID="lblOtherQualification" runat="server" Text="Other Qualification :"
                                                                CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtOtherQualification" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Panel ID="pnlResultCode" runat="server" Visible="false">
                                                            <asp:Label ID="lblResultCode" runat="server" Text="Result Code :" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList data-live-search="true" ID="cboResultCode" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlOtherResultCode" runat="server" Visible="false">
                                                            <asp:Label ID="lblOtherResultCode" runat="server" Text="Other Result Code :" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtOtherResultCode" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblGPAcode" runat="server" Text="GPA/Points :" CssClass="form-label"></asp:Label>
                                                        <nut:NumericText ID="txtGPAcode" runat="server" Type="Point" />
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnlCompletedRemark" runat="server" Visible="false">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblCompletedRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtCompletedRemark" runat="server" TextMode="MultiLine" Rows="2"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlScanAttachment" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAttachmentInfo" runat="server" Text="Attachment Info"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix d--f ai--c">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-b-0 d--f">
                                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                            <div id="fileuploader">
                                                                <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse"
                                                                    onclick="return IsValidAttach();" />
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                            Text="Browse" />
                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-default" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 300px;">
                                                            <asp:GridView ID="dgvAttchment" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                RowStyle-Wrap="false" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%"
                                                                DataKeyNames="scanattachtranunkid, filepath, GUID,form_name">
                                                                <Columns>
                                                                    <asp:TemplateField FooterText="objcohDelete">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                    ToolTip="Delete">
                                                                                    <i class="fas fa-trash text-danger"></i>
                                                                                </asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField FooterText="objcolhDownload">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                    ToolTip="Download"><i class="fas fa-download"></i></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhFileName" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div style="display: none;">
                                <asp:Panel ID="pnlStatus" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card inner-card">
                                                <div class="header">
                                                    <h2>
                                                        <asp:Label ID="lblChangeStatus" runat="server" Text="Change Status"></asp:Label>
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="row clearfix d--f ai--c">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpStatus" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-b-0 d--f">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <asp:Panel ID="pnlPart2" runat="server" Visible="false">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblApprovalData" runat="server" Text="Approval Info" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRole" runat="server" Text="Role" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpRole" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtApproverLevel" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprovedAmount" runat="server" Text="Approved Amount"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line" style="text-align: right;">
                                                                <asp:TextBox ID="txtApprovedAmount" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprRejectRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtApprRejectRemark" runat="server" TextMode="MultiLine" Rows="2"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlEnrollment" runat="server" Visible="false">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblEnrollmentInfo" runat="server" Text="Enrollment Info" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEnrollmentAmount" runat="server" Text="Enrollment Amount"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line" style="text-align: right;">
                                                                <asp:TextBox ID="txtEnrollmentAmount" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEnrollmentRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEnrollmentRemark" runat="server" TextMode="MultiLine" Rows="2"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnEnrollConfirm" runat="server" Text="Confirm" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnEnrollReject" runat="server" Text="Reject" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnDisapprove" runat="server" Text="Reject" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnComplete" runat="server" Text="Complete" class="btn btn-primary"
                                Visible="false" />
                            <asp:Button ID="btnCompletedApprove" runat="server" Text="Approve" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnCompletedDisapprove" runat="server" Text="Reject" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit for Approval" CssClass="btn btn-default" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <cc1:ModalPopupExtender ID="popupTrainingName" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfTrainingName" runat="server" CancelControlID="hfTrainingName"
                PopupControlID="pnlTrainingName">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlTrainingName" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblTTrainingName" Text="Add Training Name" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="LblTrainingCalender" runat="server" Text="Training Calendar" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <asp:DropDownList ID="cboTrainingCalender" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTrainingName" name="txtSearch" placeholder="Type To Search Text"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTrainingName', '<%= dgvAddTrainingName.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTrainingName" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid,name,trainingcategoryname,allocationtranname,departmentaltrainingneedunkid,IsGrp">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <%--'Pinkal (08-Apr-2021)-- Start
                                             'Enhancement  -  Working on Employee Claim Form Report.
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAll" runat="server" Text=" " CssClass="chk-sm" />
                                            </HeaderTemplate>
                                            Pinkal (08-Apr-2021)-- --%>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />--%>
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div style="float: left">
                        <asp:Button ID="btnAddTrainingName" runat="server" CssClass="btn btn-primary" Text="Add Training"
                            Visible="false" />
                    </div>
                    <asp:Button ID="btnTrainingName" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTCostItem" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfTrainingName" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddTrainingName" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTrainingName" runat="server" CancelControlID="hfAddTrainingName"
                PopupControlID="pnlAddTrainingName">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTrainingName" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblTrainingCourseMaster" Text="Training Course Master" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblCode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtCode" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblAlias" runat="server" Text="Alias" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtAlias" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblAddTrainingName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtAddTrainingName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine"
                                        Rows="2"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:RadioButton ID="rdJobCapability" runat="server" Text="Job Capability" GroupName="CourseType" />
                            <asp:RadioButton ID="rdCareerDevelopment" runat="server" Text="Career Development"
                                GroupName="CourseType" />
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnTrainingNameAdd" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnTrainingNameClose" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTrainingName" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAttendedTrainingName" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAttendedTrainingName" runat="server" CancelControlID="hfAttendedTrainingName"
                PopupControlID="pnlAttendedTrainingName">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAttendedTrainingName" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAttendedTrainingName" Text="Add Attended Training Name" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddAttendedTrainingName" name="txtSearch" placeholder="Type To Search Text"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromAddAttendedTrainingSearching();" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddAttendedTraining" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="course_name">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAddAttendedTrainingAll" runat="server" Text=" " CssClass="chk-sm" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvAddAttendedTrainingSelect" runat="server" Text=" " CssClass="chk-sm" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />--%>
                                        <asp:BoundField DataField="course_name" HeaderText="Course Name" ReadOnly="true"
                                            FooterText="colhECourseName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div style="float: left">
                        <asp:Button ID="btnAddAttendedTrainingName" runat="server" CssClass="btn btn-primary"
                            Text="Add Attended Training" Visible="false" />
                    </div>
                    <asp:Button ID="btnSelectAttendedTrainingName" runat="server" CssClass="btn btn-primary"
                        Text="Add" />
                    <asp:Button ID="btnCloseAttendedTrainingName" runat="server" CssClass="btn btn-default"
                        Text="Close" />
                    <asp:HiddenField ID="hfAttendedTrainingName" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddAttendedTrainingName" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddAttendedTrainingName" runat="server" CancelControlID="hfAddAttendedTrainingName"
                PopupControlID="pnlAddAttendedTrainingName">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddAttendedTrainingName" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddAttendedTrainingName" Text="Add Attended Training Name" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtAddAttendedTrainingName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAttendedTrainingNameAdd" runat="server" CssClass="btn btn-primary"
                        Text="Add" />
                    <asp:Button ID="btnAttendedTrainingNameClose" runat="server" CssClass="btn btn-default"
                        Text="Close" />
                    <asp:HiddenField ID="hfAddAttendedTrainingName" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupFinancingSource" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddFSource" runat="server" CancelControlID="hfAddFSource"
                PopupControlID="pnlAddFSource">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddFSource" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddFSource" Text="Add Financing Sources" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddFSource" name="txtSearchAddFSource" placeholder="Search Financing Sources"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddFSource', '<%= dgvAddFinancingSource.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddFinancingSource" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid,name">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddFSource" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddFSource');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddFSource" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddFSource', 'ChkAllAddFSource')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddFSource" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseFSource" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddFSource" runat="Server" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
            <asp:HiddenField ID="hfobjlblTotalAmt" runat="server" Value="0.00" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="dgvAttchment" />
            <asp:PostBackTrigger ControlID="btnDownloadAll" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function() {           
			CalcTotal();
            $('input[id*=txtAmount]').blur(function() {
                CalcTotal();
            });

        });

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }
        
        function CalcTotal() {
            var totalamt = 0;
            $('input[id*=txtAmount]').each(function() {
                totalamt += Number($(this).val());
            });

            if (totalamt.toString().indexOf('.') >= 0)
                $$('txtTotalTrainingCost').val(totalamt);
            else
                $$('txtTotalTrainingCost').val(totalamt.toString().concat('.00'));

            $$('hfobjlblTotalAmt').val($$('txtTotalTrainingCost').text());
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                url: "wPg_TrainingRequestForm.aspx?uploadimage=mSEfU19VPc4=",
                multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                    $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
        //$('input[type=file]').live("click", function() {
        $("body").on("click", 'input[type=file]', function(){
            return IsValidAttach();
        });
    </script>

</asp:Content>
