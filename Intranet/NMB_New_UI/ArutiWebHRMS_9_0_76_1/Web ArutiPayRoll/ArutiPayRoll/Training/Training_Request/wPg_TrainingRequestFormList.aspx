﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingRequestFormList.aspx.vb"
    Inherits="Training_Training_Request_wPg_TrainingRequestFormList" Title="Training Request Form List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <uc4:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Request Form List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                            <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Training Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpTrainingName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Approval Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblScheduled" runat="server" Text="Pre-Scheduled" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpScheduled" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0 ">
                                        <asp:Label ID="lblTotalTrainingCost" runat="server" Text="Total Training Cost" CssClass="form-label"></asp:Label>
                                        <nut:NumericText ID="txtTotalTrainingCost" runat="server" Type="Point" />
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12  col-xs-12  m-t-20">
                                        <div class="form-group ">
                                            <asp:DropDownList ID="cboCondition" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompletionStatus" runat="server" Text="Completion Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCompletionStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblHeader" runat="server" Text="Training Request Form"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown" style="padding-right: 60px;">
                                        <asp:Label ID="lblRequestsGrandList" runat="server" Text="Total Training Cost (Requests): "></asp:Label>
                                        <asp:Label ID="objRequestsGrandList" runat="server" Text="0.00"></asp:Label>
                                    </li>
                                    <li class="dropdown">
                                        <asp:Label ID="lblApprovedGrandList" runat="server" Text="Total Training Cost (Approved): "></asp:Label>
                                        <asp:Label ID="objApprovedGrandList" runat="server" Text="0.00"></asp:Label>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:Panel ID="pnldgTrainingName" runat="server" ScrollBars="Auto" Width="100%">
                                                <asp:GridView ID="dgTrainingName" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" HeaderStyle-Font-Bold="false"
                                                    DataKeyNames="trainingrequestunkid,statusunkid,status,issubmit_approval,isenroll_confirm,IsGrp,employeename,
                                                    iscompleted_submit_approval,completed_statusunkid,completed_approval_date,employeeunkid,
                                                    isdaysafterfeedback_submitted,end_date,isposttrainingfeedback_submitted"
                                                    Width="99%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Select" ToolTip="Edit"
                                                                        OnClick="lnkEdit_Click">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" OnClick="lnkDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnPreTrainingFeedBack">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ImgPreTrainingFeedBack" runat="server" ToolTip="PreTraining FeedBack"
                                                                    OnClick="lnkPreTrainingFeedBack_Click"> 
                                                                       <i class="fas fa-chalkboard-teacher" style="font-size:15px; color:Green"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnEnroll">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ImgEnroll" runat="server" ToolTip="Enroll" OnClick="lnkEnroll_Click">
                                                                       <i class="far fa-address-card" style="font-size:15px; color:Black"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnComplete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ImgComplete" runat="server"  ToolTip="Complete" OnClick="lnkComplete_Click">
                                                                       <i class="fas fa-check-circle" style="font-size:15px; color:Blue"></i >
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnPostTrainingFeedBack">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ImgPostTrainingFeedBack" runat="server" ToolTip="Post Training FeedBack"
                                                                    OnClick="lnkPostTrainingFeedBack_Click"> 
                                                                       <i class="fas fa-user-graduate" style="font-size:15px; color:Green"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnDaysAfterTrainingFeedBack">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ImgDaysAfterTrainingFeedBack" runat="server" ToolTip="Days After Training FeedBack"
                                                                    OnClick="lnkDayAfterTrainingFeedBack_Click"> 
                                                                      <i class="fas fa-file-signature" style="font-size:15px; color:Green"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                            FooterText="colhEmployee" />--%>
                                                        <asp:BoundField DataField="Training" HeaderText="Training Name" ReadOnly="True" FooterText="colhTraining" />
                                                        <asp:BoundField DataField="Start_Date" HeaderText="Start Date" ReadOnly="True" FooterText="colhSDate" />
                                                        <asp:BoundField DataField="End_Date" HeaderText="End Date" ReadOnly="True" FooterText="colhEDate" />
                                                        <asp:BoundField DataField="Scheduled" HeaderText="Scheduled" ReadOnly="True" FooterText="colhScheduled" />
                                                        <asp:BoundField DataField="TotalTrainingCost" HeaderText="Total Training Cost" ReadOnly="True"
                                                            FooterText="colhTotalTrainingCost" />
                                                        <asp:BoundField DataField="ApprovedAmount" HeaderText="Approved Amount" ReadOnly="True"
                                                            FooterText="colhApprovedAmount" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                                        <asp:BoundField DataField="Enrolled" HeaderText="Enrolled" ReadOnly="True" FooterText="colhEnrolled" />
                                                        <asp:BoundField DataField="CompletedStatus" HeaderText="Completion Status" ReadOnly="True"
                                                            FooterText="colhCompletedStatus" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
