﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class Training_Training_Request_wPg_TrainingRequestForm
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private objTrainingRequest As New clstraining_request_master
    Private ReadOnly mstrModuleName As String = "frmTrainingRequestForm"
    Private mintTrainingRequestunkid As Integer
    Private mintCourseMasterunkid As Integer
    Private mblnShowTrainingNamePopup As Boolean = False
    Private mblnShowAddTrainingNamePopup As Boolean = False
    Private mblnShowAttendedTrainingNamePopup As Boolean = False
    Private mblnShowAddAttendedTrainingNamePopup As Boolean = False
    Private mblnShowFinancingSourcePopup As Boolean = False
    Private mintTrainingAttendedTranunkid As Integer
    Private mblnIsAddMode As Boolean
    Private mblnFromApproval As Boolean
    Private mintMappingUnkid As Integer = 0
    Private mintPendingTrainingTranunkid As Integer
    Private mintMaxPriority As Integer = 0
    Private mintScanattachtranunkid As Integer = 0
    Private mstrDeleteAction As String = ""
    Private mintDepartTrainingNeedId As Integer = -1
    Private mblnFromCompleteESS As Boolean
    Private mblnFromCompleteMSS As Boolean
    Private mblnFromEnroll As Boolean
    Private objCONN As SqlConnection
    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Private mintNoOfStaff As Integer
    Private mintInsertFormId As Integer
    'Hemant (09 Feb 2022) -- End
#End Region

    Private Enum colTrainingAttachment
        Delete = 0
        Download = 1
        FileName = 2
    End Enum

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objCostTran As New clstraining_request_cost_tran
        Try
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            'If Session("clsuser") Is Nothing Then
            '    Exit Sub
            'End If
            'Hemant (28 Jul 2021) -- End            

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If
            'Hemant (20 Aug 2021) -- End

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                If Request.QueryString.ToString.Contains("uploadimage") = False Then
                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                    mintTrainingRequestunkid = CInt(arr(0))
                    ViewState("TrainingRequestunkid") = mintTrainingRequestunkid
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(1))
                    'HttpContext.Current.Session("Employeeunkid") = CInt(arr(2))
                    If CBool(arr(4)) Then
                        HttpContext.Current.Session("UserId") = CInt(arr(3))
                    Else
                        HttpContext.Current.Session("Employeeunkid") = CInt(arr(2))
                    End If
                    Session("PendingTrainingTranunkid") = CInt(arr(5))
                    Session("mintMappingUnkid") = CInt(arr(6))
                    If CBool(arr(7)) Then
                        Session("mblnIsAddMode") = True
                        Session("mblnFromApproval") = True
                        'Else
                        '    Session("mblnIsAddMode") = False
                        '    Session("mblnFromApproval") = False
                    End If
                    If CBool(arr(8)) Then
                        Session("mblnIsAddMode") = True
                        Session("mblnFromCompleteMSS") = True
                        'Else
                        '    Session("mblnIsAddMode") = False
                        '    Session("mblnFromCompleteMSS") = False
                    End If
                    If CBool(arr(9)) Then
                        Session("mblnFromEnroll") = True
                        'Else
                        '    Session("mblnFromEnroll") = False
                    End If

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions

                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))


                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    'If CBool(Session("IsArutiDemo")) = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False) Then
                    '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    '    Exit Sub
                    'End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If


                    Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                    Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                    Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                    If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                    End If

                    Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim()


                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    'Dim base As New Basepage
                    'If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(Session("Employeeunkid"))) = False Then
                    '    DisplayMessage.DisplayMessage(strError, Me.Page)
                    '    Exit Try
                    'End If

                    Call GetDatabaseVersion()

                    If CBool(arr(4)) Then
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                    Else
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(Session("Employeeunkid"))

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        HttpContext.Current.Session("UserId") = -1
                        HttpContext.Current.Session("Employeeunkid") = CInt(Session("Employeeunkid"))
                        HttpContext.Current.Session("UserName") = "ID " & " : " & objEmployee._Employeecode & vbCrLf & "Employee : " & objEmployee._Firstname & " " & objEmployee._Surname 'objEmp._Displayname
                        HttpContext.Current.Session("Password") = objEmployee._Password
                        HttpContext.Current.Session("LeaveBalances") = 0
                        HttpContext.Current.Session("MemberName") = "Emp. : (" & objEmployee._Employeecode & ") " & objEmployee._Firstname & " " & objEmployee._Surname
                        HttpContext.Current.Session("RoleID") = 0
                        HttpContext.Current.Session("LangId") = 1
                        HttpContext.Current.Session("Firstname") = objEmployee._Firstname
                        HttpContext.Current.Session("Surname") = objEmployee._Surname
                        HttpContext.Current.Session("DisplayName") = objEmployee._Displayname
                        HttpContext.Current.Session("Theme_id") = objEmployee._Theme_Id
                        HttpContext.Current.Session("Lastview_id") = objEmployee._LastView_Id

                        objEmployee = Nothing
                    End If

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    If CInt(arr(5)) > 0 Then
                        Dim dsList As DataSet = Nothing
                        Dim objApprovaltran As New clstrainingapproval_process_tran
                        dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, _
                                                                     CInt(Session("UserId")), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     CStr(Session("UserAccessModeSetting")), True, _
                                                                     CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                                     -1, mintTrainingRequestunkid)

                        Dim dRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("pendingtrainingtranunkid") = CInt(arr(5)))

                        If CInt(dRow(0).Item("statusunkid")) <> 1 Then
                            If CInt(dRow(0).Item("statusunkid")) = 3 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "You can't Edit this training request detail. Reason: This training request is already rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                                Exit Sub
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "You can't Edit this training request detail. Reason: This training request is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                                Exit Sub
                            End If
                        End If

                        Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= CInt(dRow(0).Item("priority")) AndAlso x.Field(Of Integer)("statusunkid") <> 1)
                        If dtRow.Count > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "You can't Edit this training request detail. Reason: This training request is already approved/reject or assign"), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        objApprovaltran = Nothing
                    End If

                    If CBool(arr(8)) Then
                        Dim objRequest As New clstraining_request_master
                        objRequest._TrainingRequestunkid = mintTrainingRequestunkid
                        If objRequest._CompletedStatusunkid > enTrainingRequestStatus.PENDING Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "You can't Edit this training request detail. Reason: This Completion training is already approved/reject "), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                    End If
                End If

                CType(Me.Master.FindControl("pnlMenuWrapper"), Panel).Visible = False

            Else
                If Not Session("TrainingRequestunkid") Is Nothing Or Not Session("TrainingRequestguid") Is Nothing Then
                    ViewState("TrainingRequestunkid") = CInt(Session("TrainingRequestunkid"))
                    ViewState("TrainingRequestguid") = CStr(Session("TrainingRequestguid"))
                    Session.Remove("TrainingRequestunkid")
                    Session.Remove("TrainingRequestguid")
                Else
                    ViewState("TrainingRequestguid") = Guid.NewGuid.ToString()
                End If
            End If


            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count <= 0 Then
                Exit Sub
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Hemant (20 Aug 2021) -- End

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                If Session("mblnFromApproval") IsNot Nothing Then
                    ViewState("mblnFromApproval") = CBool(Session("mblnFromApproval"))
                    Session.Remove("mblnFromApproval")
                End If

                If Session("mblnIsAddMode") IsNot Nothing Then
                    ViewState("mblnIsAddMode") = CBool(Session("mblnIsAddMode"))
                    Session.Remove("mblnIsAddMode")
                End If

                If Session("mblnFromCompleteESS") IsNot Nothing Then
                    ViewState("mblnFromCompleteESS") = CBool(Session("mblnFromCompleteESS"))
                    Session.Remove("mblnFromCompleteESS")
                End If

                If Session("mblnFromCompleteMSS") IsNot Nothing Then
                    ViewState("mblnFromCompleteMSS") = CBool(Session("mblnFromCompleteMSS"))
                    Session.Remove("mblnFromCompleteMSS")
                End If

                If Session("mblnFromEnroll") IsNot Nothing Then
                    ViewState("mblnFromEnroll") = CBool(Session("mblnFromEnroll"))
                    Session.Remove("mblnFromEnroll")
                End If

                If Not Session("mintDepartTrainingNeedId") Is Nothing Or Not Session("mintDepartTrainingNeedId") Is Nothing Then
                    ViewState("mintDepartTrainingNeedId") = CInt(Session("mintDepartTrainingNeedId"))
                    Session.Remove("mintDepartTrainingNeedId")
                End If

                If Session("PendingTrainingTranunkid") IsNot Nothing Then
                    mintPendingTrainingTranunkid = CInt(Session("PendingTrainingTranunkid"))
                    Session("PendingTrainingTranunkid") = Nothing
                End If

                mblnFromApproval = CBool(ViewState("mblnFromApproval"))
                mblnIsAddMode = CBool(ViewState("mblnIsAddMode"))
                mintTrainingRequestunkid = CInt(ViewState("TrainingRequestunkid"))
                mintDepartTrainingNeedId = CInt(ViewState("mintDepartTrainingNeedId"))
                mblnFromCompleteESS = CBool(ViewState("mblnFromCompleteESS"))
                mblnFromCompleteMSS = CBool(ViewState("mblnFromCompleteMSS"))
                mblnFromEnroll = CBool(ViewState("mblnFromEnroll"))
                ' mstrTrainingRequestguid = CStr(ViewState("TrainingRequestguid"))


                If mblnIsAddMode = False Then

                    FillCombo()
                    FillAttendedTrainingList()

                    rdbApplyingScheduledTrainingYes.Checked = True
                    Call cboEmployee_SelectedIndexChanged(sender, e)
                    'FillTrainingNameList()

                    If Session("TrainingRequestunkid") IsNot Nothing Then
                        mintTrainingRequestunkid = CInt(Session("TrainingRequestunkid"))
                        Session("TrainingRequestunkid") = Nothing
                        Call GetValue(mintTrainingRequestunkid)
                        cboEmployee.Enabled = False
                    End If
                    If mintDepartTrainingNeedId > 0 Then
                        dtpApplicationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                        'Hemant (09 Feb 2022) -- Start            
                        'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                        FillTrainingNameList()
                        'Hemant (09 Feb 2022) -- End
                        FillDepartmentTrainingInfo()
                        'mintDepartTrainingNeedId = 0
                    Else
                        FillTrainingCostItem()
                        FillFinancingSource()
                        Call GetValue(mintTrainingRequestunkid)
                        FillAttachment()
                    End If
                    If mblnFromEnroll OrElse Request.QueryString.Count > 0 Then
                        pnlPart1.Enabled = False
                        pnlScanAttachment.Enabled = False
                        pnlPart2.Visible = False
                        pnlEnrollment.Visible = True
                        'Hemant (25 May 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                        'pnlEnrollment.Enabled = False
                        txtEnrollmentAmount.Enabled = False
                        'Hemant (25 May 2021) -- End
                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        btnApprove.Visible = False
                        btnDisapprove.Visible = False
                        btnEnrollConfirm.Visible = True
                        btnEnrollReject.Visible = True
                        pnlStatus.Visible = False
                    End If

                Else
                    If mblnFromApproval = True Then
                        If Session("mintMappingUnkid") IsNot Nothing Then
                            ViewState("mintMappingUnkid") = Session("mintMappingUnkid")
                            Session.Remove("mintMappingUnkid")
                            mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                        End If

                        FillCombo()
                        FillAttendedTrainingList()
                        'FillTrainingNameList()

                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        btnApprove.Visible = True
                        btnDisapprove.Visible = True
                        btnEnrollConfirm.Visible = False
                        btnEnrollReject.Visible = False

                        pnlPart1.Enabled = False
                        'pnlScanAttachment.Enabled = False
                        cboScanDcoumentType.Enabled = False

                        pnlStatus.Visible = False
                        pnlPart2.Visible = True

                        drpRole.SelectedValue = CStr(mintMappingUnkid)
                        drpRole_SelectedIndexChanged(New Object, New EventArgs)
                        drpRole.Enabled = False
                        txtApproverLevel.Enabled = False
                        Call GetValue(mintTrainingRequestunkid)
                        FillAttachment()
                    End If
                End If

                If mblnFromCompleteESS Then
                    FillCombo()
                    FillAttendedTrainingList()

                    btnSave.Visible = False
                    btnSubmit.Visible = False
                    btnApprove.Visible = False
                    btnDisapprove.Visible = False
                    btnEnrollConfirm.Visible = False
                    btnEnrollReject.Visible = False
                    btnComplete.Visible = True
                    pnlPart1.Enabled = False
                    pnlScanAttachment.Enabled = True
                    pnlStatus.Visible = False
                    pnlPart2.Visible = False
                    pnlComplete.Visible = True
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
                    If CBool(Session("AllowToMarkTrainingAsComplete")) = True Then
                        pnlCompletedRemark.Visible = True
                    End If
                    'Hemant (25 May 2021) -- End

                    Call GetValue(mintTrainingRequestunkid)
                    FillAttachment()
                End If

                If mblnFromCompleteMSS Then
                    FillCombo()
                    FillAttendedTrainingList()

                    btnSave.Visible = False
                    btnSubmit.Visible = False
                    btnApprove.Visible = False
                    btnDisapprove.Visible = False
                    btnEnrollConfirm.Visible = False
                    btnEnrollReject.Visible = False
                    btnComplete.Visible = False
                    pnlPart1.Enabled = False
                    pnlScanAttachment.Enabled = True
                    pnlStatus.Visible = False
                    pnlPart2.Visible = False
                    pnlComplete.Visible = True
                    pnlCompletedRemark.Visible = True
                    btnCompletedApprove.Visible = True
                    btnCompletedDisapprove.Visible = True
                    Call GetValue(mintTrainingRequestunkid)
                    FillAttachment()
                End If

            Else
                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintCourseMasterunkid = CInt(ViewState("mintCourseMasterunkid"))
                mblnShowTrainingNamePopup = CBool(ViewState("mblnShowTrainingNamePopup"))
                mintTrainingAttendedTranunkid = CInt(ViewState("mintTrainingAttendedTranunkid"))
                mblnFromApproval = CBool(ViewState("mblnFromApproval"))
                'mstrTrainingRequestguid = CStr(Me.ViewState("mstrTrainingRequestguid"))
                mintPendingTrainingTranunkid = CInt(Me.ViewState("PendingTrainingTranunkid"))
                mintMaxPriority = CInt(Me.ViewState("mintMaxPriority"))
                mblnShowFinancingSourcePopup = CBool(ViewState("mblnShowFinancingSourcePopup"))
                mintScanattachtranunkid = CInt(ViewState("mintScanattachtranunkid"))
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintDepartTrainingNeedId = CInt(Me.ViewState("mintDepartTrainingNeedId"))
                mblnFromCompleteESS = CBool(ViewState("mblnFromCompleteESS"))
                mblnFromCompleteMSS = CBool(ViewState("mblnFromCompleteMSS"))
                mblnFromEnroll = CBool(ViewState("mblnFromEnroll"))
                'Hemant (09 Feb 2022) -- Start            
                'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
                mintNoOfStaff = CInt(ViewState("mintNoOfStaff"))
                mintInsertFormId = CInt(ViewState("mintInsertFormId"))
                'Hemant (09 Feb 2022) -- End
            End If
            If mblnShowTrainingNamePopup = True Then
                popupTrainingName.Show()
            End If
            If mblnShowAddTrainingNamePopup = True Then
                popupAddTrainingName.Show()
            End If
            If mblnShowAttendedTrainingNamePopup = True Then
                popupAttendedTrainingName.Show()
            End If
            If mblnShowFinancingSourcePopup = True Then
                popupFinancingSource.Show()
            End If
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            Me.ViewState("mintCourseMasterunkid") = mintCourseMasterunkid
            Me.ViewState("mblnShowTrainingCostItemPopup") = mblnShowTrainingNamePopup
            Me.ViewState("mintTrainingAttendedTranunkid") = mintTrainingAttendedTranunkid
            Me.ViewState("mblnFromApproval") = mblnFromApproval
            Me.ViewState("PendingTrainingTranunkid") = mintPendingTrainingTranunkid
            Me.ViewState("mintMaxPriority") = mintMaxPriority
            Me.ViewState("mblnShowFinancingSourcePopup") = mblnShowFinancingSourcePopup
            Me.ViewState.Add("mintScanattachtranunkid", mintScanattachtranunkid)
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintDepartTrainingNeedId") = mintDepartTrainingNeedId
            Me.ViewState("mblnFromCompleteESS") = mblnFromCompleteESS
            Me.ViewState("mblnFromCompleteMSS") = mblnFromCompleteMSS
            Me.ViewState("mblnFromEnroll") = mblnFromEnroll
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            Me.ViewState("mintNoOfStaff") = mintNoOfStaff
            Me.ViewState("mintInsertFormId") = mintInsertFormId
            'Hemant (09 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objEmployee As New clsEmployee_Master
        Dim objApprover As New clstraining_approver_master
        Dim objInstitute As New clsinstitute_master
        Dim objTVenue As New clstrtrainingvenue_master
        Dim objCommon As New clsCommon_Master
        Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objQGMaster As New clsCommon_Master
        Dim objQMaster As New clsqualification_master
        Dim dsCombo As DataSet = Nothing
        Try
            Dim intFirstOpen As Integer = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                intFirstOpen = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If

            dsCombo = objTPeriod.getListForCombo("List", True, 1)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen.ToString
            End With

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombo.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(Session("EmpUnkid"))
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                    .SelectedValue = CStr(Session("Employeeunkid"))
                End With
            End If

            dsCombo = objInstitute.getListForCombo(False, "List", True)
            With cboTrainingProvider
                .DataTextField = "name"
                .DataValueField = "instituteunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            'dsCombo = objTVenue.getListForCombo("List", True)
            'With cboTrainingVenue
            '    .DataTextField = "name"
            '    .DataValueField = "venueunkid"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'Hemant (03 Jun 2021) -- End

            dsCombo = objApprover.GetList("List", True, "", 0, Nothing, True)
            With drpRole
                .DataValueField = "mappingunkid"
                .DataTextField = "role"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objDeptTrainingNeed.getTrainingRequestStatusComboList("List", True)
            With drpStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objQGMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "List")
            With cboQualifGrp
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objQMaster.GetComboList("List", True)
            With cboQualifcation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            cboTrainingName.DataSource = Nothing
            cboTrainingName.DataBind()
            dsCombo.Tables(0).Rows.Clear()
            Dim dRow As DataRow = dsCombo.Tables(0).NewRow
            dRow("Id") = 0
            dRow("Name") = "Select"
            dsCombo.Tables(0).Rows.InsertAt(dRow, 0)

            With cboTrainingName
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With
            'Hemant (09 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
            objEmployee = Nothing
            objApprover = Nothing
            objInstitute = Nothing
            objTVenue = Nothing
            objCommon = Nothing
            objDeptTrainingNeed = Nothing
            objQGMaster = Nothing
            objQMaster = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillTrainingNameList()
        'Hemant (09 Feb 2022) -- Start            
        'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
        'Dim dsList As DataSet
        'Try

        '    dsList = objTrainingRequest.getTrainingComboList("List", rdbApplyingScheduledTrainingYes.Checked, rdbApplyingScheduledTrainingNo.Checked)

        '    dgvAddTrainingName.DataSource = dsList.Tables(0)
        '    dgvAddTrainingName.DataBind()

        'Catch ex As Exception
        '    DisplayMessage.DisplayError(ex, Me)
        'End Try
        
        Try
            cboTrainingName.DataSource = Nothing
            cboTrainingName.DataBind()
            If rdbApplyingScheduledTrainingYes.Checked Then
                Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
                Dim objTCategory As New clsTraining_Category_Master
                Dim dsList As DataSet = Nothing
                Dim strFilter As String = ""
                Dim objEmp As New clsEmployee_Master

                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)

                strFilter = " AND trdepartmentaltrainingneed_master.periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' " & _
                            " AND ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0)  = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                            " AND ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0)  = 0 "
                dsList = objDeptTrainingNeed.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                            , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), CInt(Session("TrainingNeedAllocationID")), "Tranining" _
                                                            , CBool(IIf(CInt(cboEmployee.SelectedValue) > 0, True, False)), strFilter, 0, 0)

                strFilter = ""
                Dim ds As New DataSet
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim iLst As IEnumerable(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("targetedgroupunkid")).Distinct.ToList()
                    If iLst.Count > 0 Then
                        For Each item In iLst
                            Dim xRow() As DataRow = Nothing
                            Dim dt As New DataTable
                            Select Case item
                                Case enAllocation.BRANCH
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Stationunkid & "' "
                                Case enAllocation.DEPARTMENT_GROUP
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Deptgroupunkid & "' "
                                Case enAllocation.DEPARTMENT
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Departmentunkid & "' "
                                Case enAllocation.SECTION_GROUP
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Sectiongroupunkid & "' "
                                Case enAllocation.SECTION
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Sectionunkid & "' "
                                Case enAllocation.UNIT_GROUP
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Unitgroupunkid & "' "
                                Case enAllocation.UNIT
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Unitunkid & "' "
                                Case enAllocation.TEAM
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Teamunkid & "' "
                                Case enAllocation.JOB_GROUP
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Jobgroupunkid & "' "
                                Case enAllocation.JOBS
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Jobunkid & "' "
                                Case enAllocation.CLASS_GROUP
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Classgroupunkid & "' "
                                Case enAllocation.CLASSES
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Classunkid & "' "
                                Case enAllocation.COST_CENTER
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Costcenterunkid & "' "
                                Case 0
                                    strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
                            End Select
                            xRow = dsList.Tables(0).Select(strFilter)
                            If xRow.Length > 0 Then
                                dt = xRow.CopyToDataTable()
                                If ds.Tables.Count <= 0 Then
                                    ds.Tables.Add(dt.Copy)
                                Else
                                    ds.Tables(0).Merge(dt.Copy, True)
                                End If
                            End If
                            dt = Nothing
                        Next
                    End If
                End If
                If CInt(cboEmployee.SelectedValue) <= 0 Then ds = dsList
                If ds.Tables.Count > 0 Then
                    Dim xTable As DataTable = ds.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid")

                    For Each r As DataRow In xTable.Rows
                        r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    Next

                    xTable.Columns("trainingcourseunkid").ColumnName = "masterunkid"
                    xTable.Columns("trainingcoursename").ColumnName = "name"

                    xTable = New DataView(xTable, "", "trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy

                    Dim drRow As DataRow = xTable.NewRow
                    drRow("name") = "Select"
                    drRow("departmentaltrainingneedunkid") = -1
                    drRow("masterunkid") = 0
                    xTable.Rows.InsertAt(drRow, 0)

                    With cboTrainingName
                        .DataTextField = "name"
                        .DataValueField = "departmentaltrainingneedunkid"
                        .DataSource = xTable
                        .DataBind()
                        .SelectedValue = "-1"
                    End With

                Else
                    Dim xTable As New DataTable
                    ds.Tables.Add(xTable)
                    ds.Tables(0).Columns.Add("name", GetType(System.String)).DefaultValue = ""
                    ds.Tables(0).Columns.Add("departmentaltrainingneedunkid", GetType(System.Int32)).DefaultValue = -1
                    ds.Tables(0).Columns.Add("masterunkid", GetType(System.Int32)).DefaultValue = 0

                    Dim drRow As DataRow = ds.Tables(0).NewRow
                    drRow("name") = "Select"
                    drRow("departmentaltrainingneedunkid") = -1
                    drRow("masterunkid") = 0
                    ds.Tables(0).Rows.InsertAt(drRow, 0)

                    With cboTrainingName
                        .DataTextField = "name"
                        .DataValueField = "departmentaltrainingneedunkid"
                        .DataSource = ds.Tables(0)
                        .DataBind()
                        .SelectedValue = "-1"
                    End With
                End If
                objDeptTrainingNeed = Nothing
                objTCategory = Nothing
                dsList = Nothing
            Else
                LblTrainingCalender.Visible = False
                cboTrainingCalender.Visible = False
                Dim dsList As DataSet = objTrainingRequest.getTrainingComboList("List", CInt(cboPeriod.SelectedValue), rdbApplyingScheduledTrainingYes.Checked, rdbApplyingScheduledTrainingNo.Checked, True)
                'Hemant (09 Feb 2022) -- [CInt(cboPeriod.SelectedValue),blnAddSelect:=True]

                dsList.Tables(0).Columns.Add("departmentaltrainingneedunkid", GetType(System.Int32)).DefaultValue = -1
                dsList.Tables(0).Columns.Add("trainingcategoryname", GetType(System.String)).DefaultValue = ""
                dsList.Tables(0).Columns.Add("startdate", GetType(System.String)).DefaultValue = ""
                dsList.Tables(0).Columns.Add("enddate", GetType(System.String)).DefaultValue = ""
                dsList.Tables(0).Columns.Add("allocationtranname", GetType(System.String)).DefaultValue = ""
                dsList.Tables(0).Columns.Add("trainingcategoryunkid", GetType(System.Int32)).DefaultValue = 0
                dsList.Tables(0).Columns.Add("allocationtranunkid", GetType(System.Int32)).DefaultValue = 0
                dsList.Tables(0).Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    drRow.Item("departmentaltrainingneedunkid") = -1
                Next
                With cboTrainingName
                    .DataTextField = "name"
                    .DataValueField = "masterunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (09 Feb 2022) -- End

    End Sub

    Private Sub FillTrainingCostItem()
        Dim objTItem As New clstrainingitemsInfo_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim objTrainingCost As New clstraining_request_cost_tran
        Dim dsTCostItem As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim dsList As DataSet
        Dim mblnblank As Boolean = False
        Try
            'dsList = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)

            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
            '    Dim drRow As DataRow = dsList.Tables(0).NewRow
            '    drRow("infotypeid") = "0"
            '    drRow("info_code") = ""
            '    drRow("info_name") = ""
            '    drRow("description") = ""
            '    drRow("defaultitemtypeid") = "0"
            '    dsList.Tables(0).Rows.Add(drRow)
            '    mblnblank = True
            'End If

            'dgvTrainingCostItem.DataSource = dsList.Tables(0)
            'dgvTrainingCostItem.DataBind()
            dsList = objTItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            Dim dtCol As New DataColumn
            dtCol = New DataColumn
            dtCol.ColumnName = "trainingrequestcosttranunkid"
            dtCol.Caption = "trainingrequestcosttranunkid"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = -1
            dsList.Tables(0).Columns.Add(dtCol)

            dtCol = New DataColumn
            dtCol.ColumnName = "Amount"
            dtCol.Caption = "Amount"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingCostItem.DataSource = dsList
                dgvTrainingCostItem.DataBind()

                Dim intCellCount As Integer = dgvTrainingCostItem.Rows(0).Cells.Count

                dgvTrainingCostItem.Rows(0).Cells(0).Visible = False
                dgvTrainingCostItem.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingCostItem.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingCostItem.Rows(0).Cells(1).Text = "No Records Found"
            Else
                If mintDepartTrainingNeedId > 0 Then
                    dsTCostItem = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)
                    For Each drRow As DataRow In dsTCostItem.Tables(0).Rows
                        drRow("Amount") = 0
                    Next
                    dsTCostItem.AcceptChanges()
                    'Hemant (13 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                    'Dim dsDeptCostItem As DataSet = Nothing
                    'dsDeptCostItem = objTCostItem.GetList("List", mintDepartTrainingNeedId)
                    'dsDeptCostItem.Tables(0).Columns("costitemunkid").ColumnName = "infounkid"
                    'Dim strDeptCostIDs As String = String.Join(",", (From p In dsDeptCostItem.Tables(0) Select (p.Item("infounkid").ToString)).ToArray)
                    'If strDeptCostIDs.Trim <> "" Then
                    '    Dim row As List(Of DataRow) = (From p In dsTCostItem.Tables(0) Where (strDeptCostIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                    '    For Each dtRow As DataRow In row
                    '        Dim r() As DataRow = dsDeptCostItem.Tables(0).Select("infounkid = " & CInt(dtRow.Item("infounkid")) & " ")
                    '        If r.Length > 0 Then
                    '            dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), GUI.fmtCurrency)
                    '        Else
                    '            dtRow.Item("Amount") = 0
                    '        End If
                    '    Next
                    'Else

                    'End If
                    'Hemant (13 Aug 2021) -- End

                Else
                    dsTCostItem = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)
                    ' dsTCostItem.Tables(0).Columns("trainingcostitemunkid").ColumnName = "costitemunkid"
                End If

                Dim strIDs As String = String.Join(",", (From p In dsTCostItem.Tables(0) Select (p.Item("infounkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        Dim r() As DataRow = dsTCostItem.Tables(0).Select("infounkid = " & CInt(dtRow.Item("infounkid")) & " ")
                        If r.Length > 0 Then
                            dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), GUI.fmtCurrency)
                            dtRow.Item("trainingrequestcosttranunkid") = CInt(r(0).Item("trainingrequestcosttranunkid"))
                        Else
                            dtRow.Item("Amount") = 0
                        End If
                    Next
                Else

                End If

                dtTable = New DataView(dsList.Tables(0), "", "Amount DESC, info_name", DataViewRowState.CurrentRows).ToTable

                dgvTrainingCostItem.DataSource = dtTable
                dgvTrainingCostItem.DataBind()

            End If



            'If mblnblank Then dgvTrainingCostItem.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingCost = Nothing
            objTCostItem = Nothing
        End Try
    End Sub

    Private Sub FillFinancingSource()
        Dim objCommon As New clsCommon_Master
        Dim objTFsource As New clstraining_request_financing_sources_tran
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim dsList As DataSet = Nothing
        Dim dsFSource As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, "List", , True)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvFinancingSource.DataSource = dsList
                dgvFinancingSource.DataBind()

                Dim intCellCount As Integer = dgvFinancingSource.Rows(0).Cells.Count

                dgvFinancingSource.Rows(0).Cells(0).Visible = False
                dgvFinancingSource.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvFinancingSource.Rows(0).Cells(i).Visible = False
                Next
                dgvFinancingSource.Rows(0).Cells(1).Text = "No Records Found"
            Else
                If mintDepartTrainingNeedId > 0 Then
                    dsFSource = objDFsource.GetList("List", mintDepartTrainingNeedId)
                    If dsFSource.Tables(0).Columns.Contains("trainingrequestfinancingsourcestranunkid") = False Then
                        dsFSource.Tables(0).Columns.Add("trainingrequestfinancingsourcestranunkid", GetType(System.Int32)).DefaultValue = -1
                    End If
                Else
                    dsFSource = objTFsource.GetList("List", mintTrainingRequestunkid)
                End If

                Dim strIDs As String = String.Join(",", (From p In dsFSource.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvFinancingSource.DataSource = dtTable
                dgvFinancingSource.DataBind()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTFsource = Nothing
            objDFsource = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub FillAttendedTrainingList()
        Dim objAttendedTraining As New clstraining_Attended_Training
        Dim dsList As DataSet
        Dim mblnblank As Boolean = False
        Dim dt As DataTable
        Try
            dsList = objAttendedTraining.getGridList("List")

            dt = dsList.Tables(0).DefaultView.ToTable(True, "Course_Name")
            dsList.Tables.Remove(dsList.Tables(0))
            dsList.Tables.Add(dt)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("course_name") = ""
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            dgvAddAttendedTraining.DataSource = dsList.Tables(0)
            dgvAddAttendedTraining.DataBind()

            If mblnblank Then dgvAddAttendedTraining.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAttendedTraining = Nothing
        End Try
    End Sub

    Private Sub FillEmployeeAttendedTrainingList()
        Dim dtEmployeeAttendedTraining As New DataTable
        Dim objAttendedTraining As New clstraining_Attended_Training
        Dim mblnblank As Boolean = False

        Try
            dtEmployeeAttendedTraining = objAttendedTraining.GetList("List", CInt(cboEmployee.SelectedValue)).Tables(0)

            If dtEmployeeAttendedTraining IsNot Nothing AndAlso dtEmployeeAttendedTraining.Rows.Count <= 0 Then
                Dim drRow As DataRow = dtEmployeeAttendedTraining.NewRow
                drRow("trainingattendedtranunkid") = "-1"
                drRow("employeeunkid") = "-1"
                drRow("course_name") = ""
                dtEmployeeAttendedTraining.Rows.Add(drRow)
                mblnblank = True
            End If

            dgvSelectedAttendedTraining.DataSource = dtEmployeeAttendedTraining
            dgvSelectedAttendedTraining.DataBind()

            If mblnblank Then dgvSelectedAttendedTraining.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAttendedTraining = Nothing
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try
            objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, Calendar is mandatory information. Please select Calendar to continue"), Me)
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue"), Me)
                Return False
            End If

            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'If CInt(txtTrainingName.Text.Trim.Length) <= 0 Then
            If CInt(cboTrainingName.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, Training Name is mandatory information. Please select Training Name to continue"), Me)
                Return False
            End If
            'Hemant (09 Feb 2022) -- End

            If dtpApplicationDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Application date is mandatory information. Please select Application date to continue"), Me)
                Return False
            End If

            If dtpStartDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, the Start date is mandatory information. Please select the start date to continue"), Me)
                Return False
            End If

            If dtpEndDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, the End date is mandatory information. Please select the end date to continue"), Me)
                Return False
            End If

            If dtpStartDate.GetDate.Date < objTPeriod._StartDate OrElse dtpStartDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, Start date should be in between selected period start date and end date"), Me)
                dtpStartDate.Focus()
                Return False
            End If
            If dtpEndDate.GetDate.Date < objTPeriod._StartDate OrElse dtpEndDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry, End date should be in between selected period start date and end date"), Me)
                dtpEndDate.Focus()
                Return False
            End If
            If dtpStartDate.GetDate.Date > dtpEndDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Sorry, Start date should not be greater than end date"), Me)
                dtpEndDate.Focus()
                Return False
            End If

            'If CInt(cboTrainingProvider.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, Provider Name is mandatory information. Please select Provider Name to continue."), Me)
            '    Return False
            'End If

            'If CInt(cboTrainingVenue.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, Provider Address is mandatory information. Please select Provider Address to continue."), Me)
            '    Return False
            'End If

            'Hemant (13 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            'If CDec(txtTotalTrainingCost.Text) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Sorry, Total Training Cost should be greater than Zero"), Me)
            '    Return False
            'End If
            'Hemant (13 Aug 2021) -- End

            If (rdbAlignedCurrentJobYes.Checked Or rdbAlignedCurrentJobNo.Checked) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, ""Is this training aligned with your current role?"" is mandatory information. Please select Yes/No to continue."), Me)
                Return False
            End If

            If (rdbRecommendedPDPYes.Checked Or rdbRecommendedPDPNo.Checked) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, ""Is this training part of the recommended training in your PDP?"" is mandatory information. Please select Yes/No to continue"), Me)
                Return False
            End If

            If (rdbForeignTravellingYes.Checked Or rdbForeignTravellingNo.Checked) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, ""Does this Training Require Travelling to Foreign Country?"" is mandatory information. Please select Yes/No to continue"), Me)
                Return False
            End If

            If 1 = 2 AndAlso rdbForeignTravellingYes.Checked = True Then
                Dim objApproverTran As New clsTraining_Approver_Tran
                Dim objCalendar As New clsTraining_Calendar_Master
                Dim objApproverLevel As New clstraining_approverlevel_master
                Dim dsApproverList As DataSet
                Dim dsApproverLevelList As DataSet
                dsApproverList = objApproverTran.GetApproverData(CInt(cboEmployee.SelectedValue))
                Dim dsCalendarList As DataSet = objCalendar.getListForCombo("List", , StatusType.Open)
                If dsCalendarList IsNot Nothing AndAlso dsCalendarList.Tables(0).Rows.Count > 0 Then
                    dsApproverLevelList = objApproverLevel.GetList("List", True, False, " hrtraining_approverlevel_master.calendarunkid = " & dsCalendarList.Tables(0).Rows(0).Item("calendarunkid").ToString)
                    mintMaxPriority = CInt(dsApproverLevelList.Tables(0).Compute("Max(priority)", ""))
                    Dim drMaxLevel() As DataRow = dsApproverList.Tables(0).Select("levelunkid = " & mintMaxPriority)
                    If drMaxLevel.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sorry, the selected employee is not mapped with the highest approver level defined in the system. Training will not be approved unless Employee is mapped with the highest level"), Me)
                        Return False
                    End If
                End If
            End If

            If mblnFromApproval = True Then
                If CInt(drpRole.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, Role is mandatory information. Please select Role to continue"), Me)
                    Return False
                End If

                If CInt(txtApproverLevel.Text.Length) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Sorry, Approver Level is mandatory information. Please select Approver Level to continue"), Me)
                    Return False
                End If

                If Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper AndAlso CDec(txtApprovedAmount.Text) <= 0 AndAlso CDec(txtTotalTrainingCost.Text) > 0 Then
                    'Hemant (13 Aug 2021) -- [AndAlso CDec(txtTotalTrainingCost.Text) > 0]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, Approved Amount is mandatory information. Please enter the Approved Amount to continue"), Me)
                    Return False
                End If

                If CInt(txtApprRejectRemark.Text.Length) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, Approver Remark is mandatory information. Please select Approver Remark to continue"), Me)
                    Return False
                End If
            End If

            If mblnFromCompleteESS OrElse mblnFromCompleteMSS Then
                If chkOtherQualification.Checked = True Then
                    If CInt(txtOtherQualificationGrp.Text.Trim.Length) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Sorry, Other Qualification Group is mandatory information. Please enter Other Qualification Group to continue"), Me)
                        Return False
                    End If

                    If CInt(txtOtherQualification.Text.Trim.Length) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Sorry, Other Qualification is mandatory information. Please enter Other Qualification to continue"), Me)
                        Return False
                    End If
                Else
                    If CInt(cboQualifGrp.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "Sorry, Qualification Group is mandatory information. Please select Qualification Group to continue"), Me)
                        Return False
                    End If

                    If CInt(cboQualifcation.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "Sorry, Qualification is mandatory information. Please select Qualification to continue"), Me)
                        Return False
                    End If
                End If

            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
        End Try
    End Function

    Private Sub SetValue(ByRef objRequestMaster As clstraining_request_master)
        Try
            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            objRequestMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            objRequestMaster._Application_Date = dtpApplicationDate.GetDate
            objRequestMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objRequestMaster._Coursemasterunkid = mintCourseMasterunkid
            If rdbApplyingScheduledTrainingYes.Checked Then
                objRequestMaster._IsScheduled = True
            Else
                objRequestMaster._IsScheduled = False
            End If
            objRequestMaster._Start_Date = dtpStartDate.GetDate
            objRequestMaster._End_Date = dtpEndDate.GetDate
            objRequestMaster._Trainingproviderunkid = CInt(cboTrainingProvider.SelectedValue)
            objRequestMaster._Trainingvenueunkid = CInt(cboTrainingVenue.SelectedValue)
            objRequestMaster._TotalTrainingCost = CDec(txtTotalTrainingCost.Text)
            If rdbAlignedCurrentJobYes.Checked Then
                objRequestMaster._IsAlignedCurrentRole = True
            Else
                objRequestMaster._IsAlignedCurrentRole = False
            End If

            If rdbRecommendedPDPYes.Checked Then
                objRequestMaster._IsPartofPDP = True
            Else
                objRequestMaster._IsPartofPDP = False
            End If

            If rdbForeignTravellingYes.Checked Then
                objRequestMaster._IsForeignTravelling = True
            Else
                objRequestMaster._IsForeignTravelling = False
            End If

            objRequestMaster._ExpectedReturn = CStr(txtExpectedReturn.Text)
            objRequestMaster._Remarks = CStr(txtRemarks.Text)
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            If CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
                (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                objRequestMaster._IsSubmitApproval = True
                objRequestMaster._IsSkipTrainingRequestAndApproval = True
            Else
                'Hemant (23 Sep 2021) -- End
                objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
                objRequestMaster._IsSubmitApproval = False
            End If 'Hemant (23 Sep 2021)

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Userunkid = CInt(Session("UserId"))
                objRequestMaster._LoginEmployeeunkid = -1
            Else
                objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objRequestMaster._Userunkid = -1
            End If
            objRequestMaster._IsWeb = True
            objRequestMaster._Isvoid = False
            objRequestMaster._Voiddatetime = Nothing
            objRequestMaster._Voidreason = ""
            objRequestMaster._Voiduserunkid = -1
            objRequestMaster._ClientIP = CStr(Session("IP_ADD"))
            objRequestMaster._FormName = mstrModuleName
            objRequestMaster._HostName = CStr(Session("HOST_NAME"))
            If mintDepartTrainingNeedId > 0 Then
                objRequestMaster._DepartmentalTrainingNeedunkid = mintDepartTrainingNeedId
            End If
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            If CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
               (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._IsEnrollConfirm = True
            Else
                'Hemant (23 Sep 2021) -- End
                objRequestMaster._IsEnrollConfirm = False
            End If 'Hemant (23 Sep 2021)
            objRequestMaster._IsEnrollReject = False
            objRequestMaster._EnrollAmount = 0
            objRequestMaster._TrainingStatusunkid = CInt(drpStatus.SelectedValue)
            objRequestMaster._MaxPriority = mintMaxPriority
            objRequestMaster._CompletedStatusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            If chkOtherQualification.Checked Then
                objRequestMaster._Other_QualificationGrp = txtOtherQualificationGrp.Text.Trim
                objRequestMaster._Other_Qualification = txtOtherQualification.Text.Trim
                objRequestMaster._other_ResultCode = txtOtherResultCode.Text.Trim
                objRequestMaster._Qualificationgroupunkid = 0
                objRequestMaster._Qualificationunkid = 0
                objRequestMaster._Resultunkid = 0
            Else
                objRequestMaster._Other_QualificationGrp = ""
                objRequestMaster._Other_Qualification = ""
                objRequestMaster._other_ResultCode = ""
            End If
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objRequestMaster._TrainingCostEmp = CDec(txtTotalTrainingCost.Text)
            If mintTrainingRequestunkid <= 0 Then objRequestMaster._InsertFormId = 1
            'Hemant (09 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValueTrainingApproval(ByRef objTrainingApproval As clstrainingapproval_process_tran)
        Try
            objTrainingApproval._PendingTrainingTranunkid = mintPendingTrainingTranunkid
            objTrainingApproval._ApprovedAmount = CDec(txtApprovedAmount.Text)
            objTrainingApproval._Approvertranunkid = CInt(drpRole.Attributes("mappingunkid"))
            objTrainingApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApproval._Userunkid = CInt(Session("UserId"))
            objTrainingApproval._IsWeb = True
            objTrainingApproval._Isvoid = False
            objTrainingApproval._Voiddatetime = Nothing
            objTrainingApproval._Voidreason = ""
            objTrainingApproval._Voiduserunkid = -1
            objTrainingApproval._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApproval._FormName = mstrModuleName
            objTrainingApproval._HostName = CStr(Session("HOST_NAME"))
            objTrainingApproval._Mapuserunkid = CInt(Session("UserId"))
            objTrainingApproval._Remark = txtApprRejectRemark.Text
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            If mintInsertFormId = 2 Then
                objTrainingApproval._ApprovedAmountEmp = CDec(txtApprovedAmount.Text) / mintNoOfStaff
            Else
                objTrainingApproval._ApprovedAmountEmp = CDec(txtApprovedAmount.Text)
            End If
            'Hemant (09 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            rdbApplyingScheduledTrainingYes.Checked = True
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'txtTrainingName.Text = ""
            cboTrainingName.SelectedIndex = 0
            'Hemant (09 Feb 2022) -- End
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            cboTrainingProvider.SelectedValue = "0"
            cboTrainingVenue.SelectedValue = "0"

            dgvSelectedAttendedTraining.DataSource = Nothing
            dgvSelectedAttendedTraining.DataBind()

            FillTrainingCostItem()

            txtTotalTrainingCost.Text = "0.00"
            FillFinancingSource()
            rdbAlignedCurrentJobYes.Checked = False
            rdbAlignedCurrentJobNo.Checked = True
            rdbRecommendedPDPYes.Checked = False
            rdbRecommendedPDPNo.Checked = True
            rdbForeignTravellingYes.Checked = False
            rdbForeignTravellingNo.Checked = True
            imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
            txtExpectedReturn.Text = ""
            txtRemarks.Text = ""

            objlblEmployeeName.Text = ""
            objlblJob.Text = ""
            objlblDepartment.Text = ""
            objlblLineManager.Text = ""
            drpStatus.SelectedValue = CStr(0)

            chkOtherQualification.Checked = False
            chkOtherQualification_CheckedChanged(Nothing, Nothing)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearAddTrainingControls()
        Try
            txtCode.Text = ""
            txtAlias.Text = ""
            txtAddTrainingName.Text = ""
            txtDescription.Text = ""
            rdJobCapability.Checked = False
            rdCareerDevelopment.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("~/images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Private Sub FillEmployeeInfo(ByVal intEmpId As Integer)
        Dim objEmployee As New clsEmployee_Master
        Dim objJob As New clsJobs
        Dim objDep As New clsDepartment
        Try
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = intEmpId

            Dim strNoimage As String = "data:image/png;base64," & ImageToBase64()

            If objEmployee._blnImgInDb Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    If objEmployee._Photo IsNot Nothing Then
                        imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                    Else
                        imgEmp.ImageUrl = strNoimage
                    End If
                End If
            Else
                imgEmp.ImageUrl = strNoimage
            End If

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, intEmpId, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            If dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0).Rows(0)("Employee Code").ToString().ToString().Length > 0 Then
                Dim drrow As DataRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishierarchy") = True).FirstOrDefault()
                objlblLineManager.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "Reporting to:") & " " & CStr(drrow("Employee"))
            End If

            objlblEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            objJob._Jobunkid = objEmployee._Jobunkid
            objlblJob.Text = objJob._Job_Name

            objDep._Departmentunkid = objEmployee._Departmentunkid
            objlblDepartment.Text = objDep._Name

            FillEmployeeAttendedTrainingList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objJob = Nothing
            objDep = Nothing
        End Try
    End Sub

    Private Sub GetValue(ByVal intTrainingRequestunkid As Integer)
        Dim objRequestMaster As New clstraining_request_master
        Dim objCommonMaster As New clsCommon_Master
        Try
            objRequestMaster._TrainingRequestunkid = intTrainingRequestunkid
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            FillTrainingVenueCombo(objRequestMaster._Trainingvenueunkid)
            'Hemant (03 Jun 2021) -- End
            cboEmployee.SelectedValue = CStr(objRequestMaster._Employeeunkid)
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'cboEmployee_SelectedIndexChanged(Nothing, Nothing)
            Clear_Controls()
            Call FillEmployeeInfo(CInt(cboEmployee.SelectedValue))
            'Hemant (09 Feb 2022) -- End

            'Pinkal (08-Apr-2021)-- Start
            'Enhancement  -  Working on Employee Claim Form Report.
            If intTrainingRequestunkid <= 0 Then
                rdbApplyingScheduledTrainingYes.Checked = True
                dtpApplicationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            Else
                cboPeriod.SelectedValue = CStr(objRequestMaster._Periodunkid)
                If CBool(objRequestMaster._IsScheduled) Then
                    rdbApplyingScheduledTrainingYes.Checked = True
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                    rdbApplyingScheduledTrainingNo.Checked = False
                    'Hemant (09 Feb 2022) -- End
                Else
                    rdbApplyingScheduledTrainingNo.Checked = True
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                    rdbApplyingScheduledTrainingYes.Checked = False
                    'Hemant (09 Feb 2022) -- End
                End If
                dtpApplicationDate.SetDate = CDate(objRequestMaster._Application_Date)
            End If
            'Pinkal (08-Apr-2021) -- End

            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            FillTrainingNameList()
            'Hemant (09 Feb 2022) -- End

            mintCourseMasterunkid = objRequestMaster._Coursemasterunkid
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'objCommonMaster._Masterunkid = mintCourseMasterunkid
            'txtTrainingName.Text = objCommonMaster._Name
            mintInsertFormId = objRequestMaster._InsertFormId
            If objRequestMaster._DepartmentalTrainingNeedunkid > 0 Then
                Dim objDepartmentaltrainingneed As New clsDepartmentaltrainingneed_master
                cboTrainingName.SelectedValue = CStr(objRequestMaster._DepartmentalTrainingNeedunkid)
                objDepartmentaltrainingneed._Departmentaltrainingneedunkid = objRequestMaster._DepartmentalTrainingNeedunkid
                mintNoOfStaff = objDepartmentaltrainingneed._Noofstaff
                objDepartmentaltrainingneed = Nothing
            Else
                cboTrainingName.SelectedValue = CStr(mintCourseMasterunkid)
            End If
            'Hemant (09 Feb 2022) -- End
            dtpStartDate.SetDate = CDate(objRequestMaster._Start_Date)
            dtpEndDate.SetDate = CDate(objRequestMaster._End_Date)
            cboTrainingProvider.SelectedValue = objRequestMaster._Trainingproviderunkid.ToString
            cboTrainingVenue.SelectedValue = objRequestMaster._Trainingvenueunkid.ToString
            txtTotalTrainingCost.Text = Format(CDec(objRequestMaster._TotalTrainingCost), CStr(Session("fmtCurrency")))
            If CBool(objRequestMaster._IsAlignedCurrentRole) Then
                rdbAlignedCurrentJobYes.Checked = True
                rdbAlignedCurrentJobNo.Checked = False
            Else
                rdbAlignedCurrentJobNo.Checked = True
                rdbAlignedCurrentJobYes.Checked = False
            End If

            If CBool(objRequestMaster._IsPartofPDP) Then
                rdbRecommendedPDPYes.Checked = True
                rdbRecommendedPDPNo.Checked = False
            Else
                rdbRecommendedPDPNo.Checked = True
                rdbRecommendedPDPYes.Checked = False
            End If
            If CBool(objRequestMaster._IsForeignTravelling) Then
                rdbForeignTravellingYes.Checked = True
                rdbForeignTravellingNo.Checked = False
            Else
                rdbForeignTravellingNo.Checked = True
                rdbForeignTravellingYes.Checked = False
            End If

            txtExpectedReturn.Text = objRequestMaster._ExpectedReturn
            txtRemarks.Text = objRequestMaster._Remarks
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            'txtEnrollmentAmount.Text = Format(CDec(objRequestMaster._ApprovedCost), CStr(Session("fmtCurrency")))
            txtEnrollmentAmount.Text = Format(CDec(objRequestMaster._ApprovedAmountEmp), CStr(Session("fmtCurrency")))
            'Hemant (09 Feb 2022) -- End
            If (CBool(objRequestMaster._IsEnrollConfirm) = True AndAlso CBool(objRequestMaster._IsEnrollReject) = False) OrElse _
            (CBool(objRequestMaster._IsEnrollConfirm) = False AndAlso CBool(objRequestMaster._IsEnrollReject) = True) Then
                btnEnrollConfirm.Enabled = False
                btnEnrollReject.Enabled = False
                'Hemant (25 May 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                txtEnrollmentRemark.Enabled = False
                'Hemant (25 May 2021) -- End
            End If
            drpStatus.SelectedValue = CStr(objRequestMaster._TrainingStatusunkid)
            If objRequestMaster._TrainingStatusunkid > 0 Then
                btnSubmit.Visible = False
            End If
            If objRequestMaster._Statusunkid > enTrainingRequestStatus.APPROVED Then
                btnApprove.Enabled = False
                btnDisapprove.Enabled = False
            End If
            If mblnFromApproval = True Then
                Dim objTrainingApprovalProcessTran As New clstrainingapproval_process_tran
                Dim dtApprover As DataTable = objTrainingApprovalProcessTran.GetTrainingApprovalData(Session("Database_Name").ToString, _
                                                                                                     eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                                                     eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                                                     False, _
                                                                                                     objRequestMaster._Employeeunkid, _
                                                                                                     mintCourseMasterunkid, _
                                                                                                      "  trtraining_request_master.trainingrequestunkid = " & mintTrainingRequestunkid & _
                                                                                                      " AND hrtraining_approverlevel_master.priority < " & CInt(txtApproverLevel.Attributes("priority")) & _
                                                                                                      " AND trtrainingapproval_process_tran.visibleid = 2 AND trtrainingapproval_process_tran.statusunkid = 2 ")

                'Hemant (16 Nov 2021) -- [Session("Database_Name").ToString, eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))),False]
                If dtApprover.Rows.Count > 0 Then
                    dtApprover = New DataView(dtApprover, "", "priority DESC ", DataViewRowState.CurrentRows).ToTable
                    txtApprovedAmount.Text = Format(CDec(dtApprover.Rows(0).Item("approvedamount")), CStr(Session("fmtCurrency")))
                Else
                    txtApprovedAmount.Text = txtTotalTrainingCost.Text
                End If
                objTrainingApprovalProcessTran = Nothing
            End If
            If mblnFromCompleteESS Then
                chkOtherQualification.Checked = False
            Else
                If CInt(objRequestMaster._Qualificationgroupunkid) <= 0 Then
                    chkOtherQualification.Checked = True
                Else
                    chkOtherQualification.Checked = False
                End If
            End If
            chkOtherQualification_CheckedChanged(Nothing, Nothing)
            cboQualifGrp.SelectedValue = CStr(objRequestMaster._Qualificationgroupunkid)
            cboQualifGrp_SelectedIndexChanged(New Object, New EventArgs)
            cboQualifcation.SelectedValue = CStr(objRequestMaster._Qualificationunkid)
            cboQualifcation_SelectedIndexChanged(New Object, New EventArgs)
            cboResultCode.SelectedValue = CStr(objRequestMaster._Resultunkid)
            txtOtherQualificationGrp.Text = CStr(objRequestMaster._Other_QualificationGrp)
            txtOtherQualification.Text = CStr(objRequestMaster._Other_Qualification)
            txtOtherResultCode.Text = CStr(objRequestMaster._other_ResultCode)
            txtGPAcode.Text = CStr(objRequestMaster._GPAcode)
            'Hemant (25 May 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            txtEnrollmentRemark.Text = CStr(objRequestMaster._EnrollmentRemark)
            txtCompletedRemark.Text = CStr(objRequestMaster._CompletedRemark)
            'Hemant (25 May 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub SetAttendedTrainingValue(ByRef objAttendedTraining As clstraining_Attended_Training)
        Try
            objAttendedTraining._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objAttendedTraining._CourseName = txtAddAttendedTrainingName.Text
            objAttendedTraining._Userunkid = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAttendedTraining._AuditUserId = CInt(Session("UserId"))
            Else
                objAttendedTraining._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAttendedTraining._Isvoid = False
            objAttendedTraining._Voiduserunkid = -1
            objAttendedTraining._Voiddatetime = Nothing
            objAttendedTraining._Voidreason = ""

            objAttendedTraining._IsWeb = True
            objAttendedTraining._ClientIP = CStr(Session("IP_ADD"))
            objAttendedTraining._HostName = CStr(Session("HOST_NAME"))
            objAttendedTraining._FormName = mstrModuleName

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetCommonMasterValue(ByRef objCommonMaster As clsCommon_Master)
        Try
            objCommonMaster._Mastertype = clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER
            objCommonMaster._Code = txtCode.Text
            objCommonMaster._Alias = txtAlias.Text
            objCommonMaster._Name = txtAddTrainingName.Text
            objCommonMaster._Description = txtDescription.Text
            objCommonMaster._Isactive = True

            If rdJobCapability.Checked Then
                objCommonMaster._CouseTypeId = enCourseType.Job_Capability
            ElseIf rdCareerDevelopment.Checked Then
                objCommonMaster._CouseTypeId = enCourseType.Career_Development
            End If

            objCommonMaster._Userunkid = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objCommonMaster._AuditUserId = CInt(Session("UserId"))
            Else
                objCommonMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objCommonMaster._FromWeb = True
            objCommonMaster._ClientIP = CStr(Session("IP_ADD"))
            objCommonMaster._HostName = CStr(Session("HOST_NAME"))
            objCommonMaster._FormName = mstrModuleName

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView, ByVal grpType As Integer)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)


            If grpType = 1 Then
                row.BackColor = ColorTranslator.FromHtml("#E6E6E6")
            Else
                row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
            End If

            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub FillAttachment()
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTable As DataTable
        Try
            If mintTrainingRequestunkid > 0 Then
                dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestunkid, "")
            Else
                dtTable = Nothing
            End If
            dgvAttchment.AutoGenerateColumns = False
            dgvAttchment.DataSource = dtTable
            dgvAttchment.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillDepartmentTrainingInfo()
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim objDepartTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim objCommon As New clsCommon_Master
        Dim objRequestMaster As New clstraining_request_master
        Try
            If mintDepartTrainingNeedId > 0 Then

                objDepartTrainingNeed._Departmentaltrainingneedunkid = mintDepartTrainingNeedId
                'Hemant (07 Jun 2021) -- Start
                'ENHANCEMENT : OLD-406 - Global Training Requests
                Dim dsRequestedEmp As DataSet = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                          eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                          True, CBool(Session("IsIncludeInactiveEmp")), "Training", , _
                                                          " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
                If objDepartTrainingNeed._Noofstaff <= CInt(dsRequestedEmp.Tables(0).Rows.Count) Then
                    mintCourseMasterunkid = 0
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                    'txtTrainingName.Text = ""
                    cboTrainingName.SelectedIndex = 0
                    'Hemant (09 Feb 2022) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 43, "Sorry, No Vacant Seat is available for Selected Training"), Me)
                    Exit Sub
                End If
                'Hemant (07 Jun 2021) -- End
                'Hemant (03 Dec 2021) -- Start
                'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
                cboPeriod.SelectedValue = CStr(objDepartTrainingNeed._Periodunkid)
                'Hemant (03 Dec 2021) -- End
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
                FillTrainingVenueCombo(objDepartTrainingNeed._Trainingvenueunkid)
                'Hemant (03 Jun 2021) -- End
                mintCourseMasterunkid = CInt(objDepartTrainingNeed._Trainingcourseunkid)
                'Hemant (09 Feb 2022) -- Start            
                'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                'objCommon._Masterunkid = mintCourseMasterunkid
                'txtTrainingName.Text = objCommon._Name
                cboTrainingName.SelectedValue = CStr(objDepartTrainingNeed._Departmentaltrainingneedunkid)
                'Hemant (09 Feb 2022) -- End
                dtpStartDate.SetDate = CDate(objDepartTrainingNeed._Startdate)
                dtpEndDate.SetDate = CDate(objDepartTrainingNeed._Enddate)
                cboTrainingProvider.SelectedValue = CStr(objDepartTrainingNeed._Trainingproviderunkid)
                cboTrainingVenue.SelectedValue = CStr(objDepartTrainingNeed._Trainingvenueunkid)

                FillTrainingCostItem()
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                'txtTotalTrainingCost.Text = Format(CDec(objDepartTrainingNeed._Totalcost), CStr(Session("fmtCurrency")))
                txtTotalTrainingCost.Text = Format(CDec(0), CStr(Session("fmtCurrency")))
                'Hemant (13 Aug 2021) -- End 

                Call FillFinancingSource()

                If objDepartTrainingNeed._ModuleId = CInt(enModuleReference.PDP) Then
                    rdbRecommendedPDPYes.Checked = True
                    rdbRecommendedPDPNo.Checked = False
                Else
                    rdbRecommendedPDPYes.Checked = False
                    rdbRecommendedPDPNo.Checked = True
                End If
            Else
                rdbRecommendedPDPYes.Checked = False
                rdbRecommendedPDPNo.Checked = True
            End If

            rdbRecommendedPDPYes.Enabled = False
            rdbRecommendedPDPNo.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDFsource = Nothing
            objDepartTrainingNeed = Nothing
            objTCostItem = Nothing
            objRequestMaster = Nothing
        End Try
    End Sub

    'Hemant (03 Jun 2021) -- Start
    'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
    Private Sub FillTrainingVenueCombo(ByVal intVenueId As Integer)
        Dim objTVenue As New clstrtrainingvenue_master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsCombo = objTVenue.getListForCombo("List", True)
            If intVenueId <= 0 Then
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 OR (islocked = 1 AND venueunkid = " & intVenueId & " ) ", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboTrainingVenue
                .DataTextField = "name"
                .DataValueField = "venueunkid"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTVenue = Nothing
        End Try
    End Sub
    'Hemant (03 Jun 2021) -- End

    Protected Sub Save_Click()
        Dim objRequestMaster As New clstraining_request_master
        Dim objRequestCost As New clstraining_request_cost_tran
        Dim blnFlag As Boolean = False
        Dim dtCostTran As DataTable
        Dim dtOldCostTran As DataTable
        'Dim btn As Button = CType(sender, Button)
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            SetValue(objRequestMaster)

            If Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" Then
                objRequestMaster._IsSubmitApproval = True
            Else
                objRequestMaster._IsSubmitApproval = False
            End If

            Dim lstFinancingSources As New List(Of clstraining_request_financing_sources_tran)
            Dim lstVoidFinancingSources As New List(Of clstraining_request_financing_sources_tran)
            Dim objFinancingSources As New clstraining_request_financing_sources_tran

            Dim lstTrainingCost As New List(Of clstraining_request_cost_tran)
            Dim lstVoidTrainingCost As New List(Of clstraining_request_cost_tran)
            Dim objTrainingCost As New clstraining_request_cost_tran

            '*** Training Cost ***
            dtOldCostTran = objRequestCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid).Tables(0)
            dtCostTran = objRequestCost._TranDataTable
            Dim grow As IEnumerable(Of GridViewRow) = Nothing
            grow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow)()

            For Each dr As GridViewRow In grow
                Dim drRow As DataRow = dtCostTran.NewRow
                drRow("trainingrequestcosttranunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString())
                drRow("trainingcostitemunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("infounkid").ToString())
                Dim txtamount As Controls_NumericTextBox = CType(dr.FindControl("txtamount"), Controls_NumericTextBox)
                drRow("amount") = CDec(txtamount.Text)

                drRow("Isvoid") = False
                drRow("Voiddatetime") = DBNull.Value
                drRow("Voidreason") = ""
                drRow("Voiduserunkid") = -1
                drRow("voidloginemployeeunkid") = -1
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objRequestMaster._Userunkid = CInt(Session("UserId"))
                Else
                    objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                End If
                If CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) <= 0 AndAlso CDec(txtamount.Text) > 0 Then
                    drRow("AUD") = "A"
                    dtCostTran.Rows.Add(drRow)
                ElseIf CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) > 0 AndAlso CDec(txtamount.Text) > 0 Then
                    Dim drOld() As DataRow = dtOldCostTran.Select("trainingrequestcosttranunkid = " & CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) & "")
                    If drOld.Length > 0 Then
                        If CDec(drOld(0).Item("amount")) <> CDec(txtamount.Text) Then
                            drRow("AUD") = "U"
                            dtCostTran.Rows.Add(drRow)
                        End If
                    End If
                ElseIf CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) > 0 AndAlso CDec(txtamount.Text) <= 0 Then
                    Dim drOld() As DataRow = dtOldCostTran.Select("trainingrequestcosttranunkid = " & CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) & "")
                    If drOld.Length > 0 Then
                        drRow("Isvoid") = True
                        drRow("Voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drRow("Voidreason") = ""
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            objRequestMaster._Voiduserunkid = CInt(Session("UserId"))
                        Else
                            objRequestMaster._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                        End If
                        drRow("AUD") = "D"
                        dtCostTran.Rows.Add(drRow)
                    End If
                End If
            Next
            dtCostTran.AcceptChanges()

            Dim gNewRow As List(Of GridViewRow) = Nothing
            Dim strIDs As String

            '*** Financial Sources ***

            grow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).ToList
            strIDs = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString).ToArray)

            If mintTrainingRequestunkid > 0 Then

                Dim dsTFSources As DataSet = objFinancingSources.GetList("Emp", mintTrainingRequestunkid)

                Dim drVoid As List(Of DataRow) = (From p In dsTFSources.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("financingsourceunkid").ToString) = False) Select (p)).ToList
                Dim strExistIDs As String = String.Join(",", (From p In dsTFSources.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                gNewRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString) = False).Select(Function(x) x).ToList

                For Each r As DataRow In drVoid

                    objFinancingSources = New clstraining_request_financing_sources_tran

                    With objFinancingSources
                        .pintTrainingRequestfinancingsourcestranunkid = CInt(r.Item("TrainingRequestfinancingsourcestranunkid"))

                        .pblnIsvoid = True
                        .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        .pstrVoidreason = ""

                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintVoiduserunkid = CInt(Session("UserId"))
                            .pintVoidloginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintVoiduserunkid = 0
                            .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = 0
                        End If
                        .pblnIsweb = True
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName
                    End With

                    lstVoidFinancingSources.Add(objFinancingSources)
                Next

                grow = gNewRow

            End If

            For Each dgRow As GridViewRow In grow
                objFinancingSources = New clstraining_request_financing_sources_tran

                With objFinancingSources
                    .pintTrainingRequestfinancingsourcestranunkid = -1
                    .pintTrainingRequestunkid = mintTrainingRequestunkid
                    .pintFinancingsourceunkid = CInt(dgvFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                    .pblnIsweb = True
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = 0
                    End If
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                lstFinancingSources.Add(objFinancingSources)
            Next

            objRequestMaster._lstFinancingSourceNew = lstFinancingSources
            objRequestMaster._lstFinancingSourceVoid = lstVoidFinancingSources

            objRequestMaster._Datasource_TrainingCostItem = dtCostTran

            If objRequestMaster.Save(Session("Database_Name").ToString, _
                                     CInt(Session("UserId")), _
                                     CInt(Session("Fin_year")), _
                                     CInt(Session("CompanyUnkId")), _
                                     CStr(Session("UserAccessModeSetting")), _
                                     Session("EmployeeAsOnDate").ToString, _
                                     CInt(Session("TrainingApproverAllocationID")), Nothing) = False Then
                'Hemant (09 Feb 2022) -- [CInt(Session("TrainingApproverAllocationID"))]
                DisplayMessage.DisplayMessage(objRequestMaster._Message, Me)
            Else
                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                If objRequestMaster._IsSubmitApproval = True AndAlso _
                  ((CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) OrElse _
                    (CBool(Session("SkipTrainingRequisitionAndApproval")) = False)) Then
                    'Hemant (23 Sep 2021) -- [AndAlso ((CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) OrElse (CBool(Session("SkipTrainingRequisitionAndApproval")) = False) ]

                    objRequestMaster.Send_Notification_Approver(Session("Database_Name").ToString, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(IIf(objRequestMaster._MinApprovedPriority > 0, objRequestMaster._MinApprovedPriority, -1)), _
                                                                clstraining_request_master.enEmailType.Training_Approver, _
                                                                CInt(Session("CompanyUnkId")), cboEmployee.SelectedItem.Text, cboTrainingName.Text, _
                                                                dtpApplicationDate.GetDate.Date, CInt(Session("Fin_year")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("ArutiSelfServiceURL")), _
                                                                dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, _
                                                                enLoginMode, intLoginByEmployeeId, intUserId, , , False)
                    'Hemant (09 Feb 2022) -- [dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date,blnSendEmailReportingTo := False, txtTrainingName.Text --> cboTrainingName.Text]
                    'Hemant (16 Nov 2021) -- [Session("Database_Name").ToString]

                End If
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                If Request.QueryString.Count <= 0 Then
                    'Hemant (13 Aug 2021) -- End
                    Response.Redirect(Session("rootpath").ToString & "Training/Training_Request/wPg_TrainingRequestFormList.aspx", False)
                    'Hemant (13 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                Else
                    Session.Abandon()
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                End If
                'Hemant (13 Aug 2021) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    'Protected Sub btnTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrainingName.Click

    '    Try
    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddTrainingName.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "Training is compulsory information.Please Check atleast One Training."), Me)
    '            Exit Sub
    '        End If
    '        If gRow Is Nothing OrElse gRow.Count > 1 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 46, "Please Check Only One Training."), Me)
    '            Exit Sub
    '        End If
    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            For Each dgRow As GridViewRow In gRow
    '                mintCourseMasterunkid = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("masterunkid").ToString())
    '                txtTrainingName.Text = dgvAddTrainingName.DataKeys(dgRow.RowIndex)("name").ToString()
    '                mintDepartTrainingNeedId = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("departmentaltrainingneedunkid").ToString())
    '                FillDepartmentTrainingInfo()
    '            Next
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally


    '    End Try

    'End Sub
    'Hemant (09 Feb 2022) -- End


    Protected Sub btnAddTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTrainingName.Click
        Try
            mblnShowAddTrainingNamePopup = True
            popupAddTrainingName.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupTrainingName.Show()
        End Try
    End Sub

    Protected Sub btnAddAttendedTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttendedTrainingName.Click
        Try
            mblnShowAddAttendedTrainingNamePopup = True
            popupAddAttendedTrainingName.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAttendedTrainingName.Show()
        End Try
    End Sub

    Protected Sub btnAttendedTrainingNameAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttendedTrainingNameAdd.Click
        Dim objAttendedTraining As New clstraining_Attended_Training
        Dim dsList As DataSet
        Try
            dsList = objAttendedTraining.getGridList("List")
            Dim drAttendTraining() As DataRow = dsList.Tables(0).Select("course_name = '" & txtAddAttendedTrainingName.Text & "'")
            If txtAddAttendedTrainingName.Text.EndsWith(".") Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 41, "Sorry, Invalid Character not allowed in Attended Training Name!!!"), Me)
                Exit Sub
            End If
            If drAttendTraining.Length > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Attended Training is already Exist!!!"), Me)
                Exit Sub
            End If



            SetAttendedTrainingValue(objAttendedTraining)

            If objAttendedTraining.Insert() = False Then
                DisplayMessage.DisplayMessage(objAttendedTraining._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Training Attended saved successfully"), Me)
                mblnShowAddAttendedTrainingNamePopup = False
                popupAddAttendedTrainingName.Hide()
                Call FillAttendedTrainingList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAttendedTraining = Nothing
            popupAttendedTrainingName.Show()
        End Try
    End Sub

    Protected Sub btnCloseAttendedTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseAttendedTrainingName.Click
        Try
            mblnShowAttendedTrainingNamePopup = False
            popupAttendedTrainingName.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSelectAttendedTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectAttendedTrainingName.Click
        Dim objAttendedTraining As New clstraining_Attended_Training
        Dim strMessage As String = ""
        Dim dtOldAttendTraning As New DataTable
        Try
            dtOldAttendTraning = objAttendedTraining.GetList("List", CInt(cboEmployee.SelectedValue)).Tables(0)
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAddAttendedTraining.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvAddAttendedTrainingSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "Training is compulsory information.Please Check atleast One Training."), Me)
                Exit Sub
            End If
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                For Each dgRow As GridViewRow In gRow
                    Dim drRow() As DataRow = dtOldAttendTraning.Select("course_name = '" & dgvAddAttendedTraining.DataKeys(dgRow.RowIndex)("course_name").ToString() & "'")
                    If drRow.Length <= 0 Then

                        objAttendedTraining._Employeeunkid = CInt(cboEmployee.SelectedValue)
                        objAttendedTraining._CourseName = dgvAddAttendedTraining.DataKeys(dgRow.RowIndex)("course_name").ToString()
                        objAttendedTraining._Userunkid = CInt(Session("UserId"))
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            objAttendedTraining._AuditUserId = CInt(Session("UserId"))
                        Else
                            objAttendedTraining._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                        End If

                        objAttendedTraining._Isvoid = False
                        objAttendedTraining._Voiduserunkid = -1
                        objAttendedTraining._Voiddatetime = Nothing
                        objAttendedTraining._Voidreason = ""

                        objAttendedTraining._IsWeb = True
                        objAttendedTraining._ClientIP = CStr(Session("IP_ADD"))
                        objAttendedTraining._HostName = CStr(Session("HOST_NAME"))
                        objAttendedTraining._FormName = mstrModuleName


                        If objAttendedTraining.Insert() = False Then
                            dgRow.Style.Add("background", "red")
                            strMessage &= " * " & objAttendedTraining._Message & "<br />"
                        End If
                    End If
                Next
                If strMessage.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMessage, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Training Attended saved successfully"), Me)
                    Call FillAttendedTrainingList()
                End If
                Call FillEmployeeAttendedTrainingList()
                mblnShowAttendedTrainingNamePopup = False
                popupAttendedTrainingName.Hide()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAttendedTraining = Nothing
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSubmit.Click
        Try
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            If CDec(txtTotalTrainingCost.Text) <= 0 Then
                cntSaveConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 51, "You have not indicated training cost amount, are you sure you want to continue?")
                cntSaveConfirm.Show()
            Else
                Call Save_Click()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Hemant (13 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            If Request.QueryString.Count <= 0 Then
                'Hemant (13 Aug 2021) -- End
                If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                    Response.Redirect(Session("ReturnURL").ToString, False)
                    Session("ReturnURL") = Nothing
                Else

                    Response.Redirect("~\UserHome.aspx", False)
                End If
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            Else
                Session.Abandon()
                Response.Redirect("~/Index.aspx", False)
            End If
            'Hemant (13 Aug 2021) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnTrainingNameAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrainingNameAdd.Click
        Dim objCommonMaster As New clsCommon_Master
        Try
            If rdJobCapability.Checked = False AndAlso rdCareerDevelopment.Checked = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please Select at least one course type"), Me)
                Exit Sub
            End If
            SetCommonMasterValue(objCommonMaster)

            If objCommonMaster.Insert() = False Then
                DisplayMessage.DisplayMessage(objCommonMaster._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Training saved successfully"), Me)
                ClearAddTrainingControls()
                mblnShowAddTrainingNamePopup = False
                popupAddTrainingName.Hide()
                'FillTrainingNameList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCommonMaster = Nothing
            popupTrainingName.Show()
        End Try
    End Sub

    Protected Sub btnTrainingNameClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrainingNameClose.Click
        Try
            mblnShowAddTrainingNamePopup = False
            popupAddTrainingName.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupTrainingName.Show()
        End Try
    End Sub

    Protected Sub btnAttendedTrainingNameClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttendedTrainingNameClose.Click
        Try
            mblnShowAddAttendedTrainingNamePopup = False
            popupAddAttendedTrainingName.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAttendedTrainingName.Show()
        End Try
    End Sub

    Protected Sub btnCloseTCostItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseTCostItem.Click
        Try
            mblnShowTrainingNamePopup = False
            popupTrainingName.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, _
                                                                                                btnDisapprove.Click, _
                                                                                                btnEnrollConfirm.Click, _
                                                                                                btnEnrollReject.Click, _
                                                                                                btnComplete.Click, _
                                                                                                btnCompletedApprove.Click, _
                                                                                                btnCompletedDisapprove.Click
        Try
            Select Case CType(sender, Button).ID.ToUpper
                Case btnDisapprove.ID.ToUpper
                    If txtApprRejectRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "Sorry, Remark is mandatory information. Please add remark to continue."), Me)
                        Exit Sub
                    End If
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 49, "Are you sure you want to reject this Training Request?")
                Case btnApprove.ID.ToUpper
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 50, "Are you sure you want to approve this Training Request?")
                Case btnEnrollConfirm.ID.ToUpper
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Are you sure you want to Confirm for Enrollment this Training Request?")
                Case btnEnrollReject.ID.ToUpper
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                    If txtEnrollmentRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, "Sorry, Enrollment Remark is mandatory information. Please enter Enrollment Remark to continue"), Me)
                        Exit Sub
                    End If
                    'Hemant (25 May 2021) -- End
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 53, "Are you sure you want to Reject for Enrollment this Training Request?")
                Case btnComplete.ID.ToUpper
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 54, "Are you sure you want to Update Completed Status for this Training Request?")
                Case btnCompletedDisapprove.ID.ToUpper
                    If txtCompletedRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "Sorry, Remark is mandatory information. Please add remark to continue."), Me)
                        Exit Sub
                    End If
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 55, "Are you sure you want to reject this Completed Training?")
                Case btnCompletedApprove.ID.ToUpper
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 56, "Are you sure you want to approve this Completed Training?")
            End Select
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            cnfApprovDisapprove.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseFSource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseFSource.Click
        Try
            mblnShowFinancingSourcePopup = False
            popupFinancingSource.Hide()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnAddFSource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFSource.Click
        Dim objRequestMaster As New clstraining_request_master
        Try
            If IsValidate() = False Then Exit Sub

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAddFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddFSource"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please tick at least one financing source from the list"), Me)
                Exit Try
            End If

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                Dim lstFSource As New List(Of clstraining_request_financing_sources_tran)
                Dim objFSource As New clstraining_request_financing_sources_tran

                For Each dgRow As GridViewRow In gRow
                    objFSource = New clstraining_request_financing_sources_tran

                    With objFSource
                        .pintTrainingRequestfinancingsourcestranunkid = -1
                        .pintTrainingRequestunkid = mintTrainingRequestunkid
                        .pintFinancingsourceunkid = CInt(dgvAddFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                        .pblnIsweb = True
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = 0
                        End If
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With

                    lstFSource.Add(objFSource)
                Next


                SetValue(objRequestMaster)
                objFSource._TrainingRequestunkid = mintTrainingRequestunkid
                If objFSource.SaveAll(lstFSource, objRequestMaster) = False Then
                    If objFSource._Message <> "" Then
                        DisplayMessage.DisplayMessage(objFSource._Message, Me)
                    End If
                Else
                    mintTrainingRequestunkid = objFSource._TrainingRequestunkid
                End If

                Call FillFinancingSource()

                mblnShowFinancingSourcePopup = False
                popupFinancingSource.Hide()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If IsValidate() = False Then Exit Sub
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                If mintTrainingRequestunkid <= 0 Then
                    Dim blnFlag As Boolean = False
                    Dim dtOldCostTran As DataTable
                    Dim dtCostTran As DataTable
                    Dim objRequestMaster As New clstraining_request_master
                    Dim objRequestCost As New clstraining_request_cost_tran
                    SetValue(objRequestMaster)
                    objRequestMaster._IsSubmitApproval = False
                    Dim lstFinancingSources As New List(Of clstraining_request_financing_sources_tran)
                    Dim lstVoidFinancingSources As New List(Of clstraining_request_financing_sources_tran)
                    Dim objFinancingSources As New clstraining_request_financing_sources_tran

                    Dim lstTrainingCost As New List(Of clstraining_request_cost_tran)
                    Dim lstVoidTrainingCost As New List(Of clstraining_request_cost_tran)
                    Dim objTrainingCost As New clstraining_request_cost_tran


                    dtOldCostTran = objRequestCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid).Tables(0)
                    dtCostTran = objRequestCost._TranDataTable
                    Dim grow As IEnumerable(Of GridViewRow) = Nothing
                    grow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow)()

                    For Each dr As GridViewRow In grow
                        Dim drRow As DataRow = dtCostTran.NewRow
                        drRow("trainingrequestcosttranunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString())
                        drRow("trainingcostitemunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("infounkid").ToString())
                        Dim txtamount As Controls_NumericTextBox = CType(dr.FindControl("txtamount"), Controls_NumericTextBox)
                        drRow("amount") = CDec(txtamount.Text)

                        drRow("Isvoid") = False
                        drRow("Voiddatetime") = DBNull.Value
                        drRow("Voidreason") = ""
                        drRow("Voiduserunkid") = -1
                        drRow("voidloginemployeeunkid") = -1
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            objRequestMaster._Userunkid = CInt(Session("UserId"))
                        Else
                            objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                        End If
                        If CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) <= 0 AndAlso CDec(txtamount.Text) > 0 Then
                            drRow("AUD") = "A"
                            dtCostTran.Rows.Add(drRow)
                        ElseIf CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) > 0 AndAlso CDec(txtamount.Text) > 0 Then
                            Dim drOld() As DataRow = dtOldCostTran.Select("trainingrequestcosttranunkid = " & CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) & "")
                            If drOld.Length > 0 Then
                                If CDec(drOld(0).Item("amount")) <> CDec(txtamount.Text) Then
                                    drRow("AUD") = "U"
                                    dtCostTran.Rows.Add(drRow)
                                End If
                            End If
                        ElseIf CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) > 0 AndAlso CDec(txtamount.Text) <= 0 Then
                            Dim drOld() As DataRow = dtOldCostTran.Select("trainingrequestcosttranunkid = " & CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) & "")
                            If drOld.Length > 0 Then
                                drRow("Isvoid") = True
                                drRow("Voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                drRow("Voidreason") = ""
                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                    objRequestMaster._Voiduserunkid = CInt(Session("UserId"))
                                Else
                                    objRequestMaster._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                                End If
                                drRow("AUD") = "D"
                                dtCostTran.Rows.Add(drRow)
                            End If
                        End If
                    Next
                    dtCostTran.AcceptChanges()


                    Dim gNewRow As List(Of GridViewRow) = Nothing
                    Dim strIDs As String

                    '*** Financial Sources ***

                    grow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).ToList
                    strIDs = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString).ToArray)

                    If mintTrainingRequestunkid > 0 Then

                        Dim dsTFSources As DataSet = objFinancingSources.GetList("Emp", mintTrainingRequestunkid)

                        Dim drVoid As List(Of DataRow) = (From p In dsTFSources.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("financingsourceunkid").ToString) = False) Select (p)).ToList
                        Dim strExistIDs As String = String.Join(",", (From p In dsTFSources.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                        gNewRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString) = False).Select(Function(x) x).ToList

                        For Each r As DataRow In drVoid

                            objFinancingSources = New clstraining_request_financing_sources_tran

                            With objFinancingSources
                                .pintTrainingRequestfinancingsourcestranunkid = CInt(r.Item("TrainingRequestfinancingsourcestranunkid"))

                                .pblnIsvoid = True
                                .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                .pstrVoidreason = ""

                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                    .pintVoiduserunkid = CInt(Session("UserId"))
                                    .pintVoidloginemployeeunkid = 0
                                    .pintAuditUserId = CInt(Session("UserId"))
                                Else
                                    .pintVoiduserunkid = 0
                                    .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                    .pintAuditUserId = 0
                                End If
                                .pblnIsweb = True
                                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                .pstrClientIp = Session("IP_ADD").ToString()
                                .pstrHostName = Session("HOST_NAME").ToString()
                                .pstrFormName = mstrModuleName
                            End With

                            lstVoidFinancingSources.Add(objFinancingSources)
                        Next

                        grow = gNewRow

                    End If

                    For Each dgRow As GridViewRow In grow
                        objFinancingSources = New clstraining_request_financing_sources_tran

                        With objFinancingSources
                            .pintTrainingRequestfinancingsourcestranunkid = -1
                            .pintTrainingRequestunkid = mintTrainingRequestunkid
                            .pintFinancingsourceunkid = CInt(dgvFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                            .pblnIsweb = True
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintUserunkid = CInt(Session("UserId"))
                                .pintLoginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintUserunkid = 0
                                .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = 0
                            End If
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName

                        End With

                        lstFinancingSources.Add(objFinancingSources)
                    Next

                    objRequestMaster._lstFinancingSourceNew = lstFinancingSources
                    objRequestMaster._lstFinancingSourceVoid = lstVoidFinancingSources

                    objRequestMaster._Datasource_TrainingCostItem = dtCostTran

                    If objRequestMaster.Save(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             CStr(Session("UserAccessModeSetting")), _
                                             Session("EmployeeAsOnDate").ToString, _
                                             CInt(Session("TrainingApproverAllocationID")), Nothing) = False Then
                        'Hemant (09 Feb 2022) -- [CInt(Session("TrainingApproverAllocationID"))]
                        DisplayMessage.DisplayMessage(objRequestMaster._Message, Me)
                    Else
                        mintTrainingRequestunkid = objRequestMaster._TrainingRequestunkid
                    End If
                    objRequestMaster = Nothing
                End If

                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                Dim objScan As New clsScan_Attach_Documents
                Dim mdtFullAttachment As DataTable = objScan._Datatable

                If objScan.IsExist(enImg_Email_RefId.Training_Module, enScanAttactRefId.TRAINING_NEED_FORM, f.Name, -1, -1, mintTrainingRequestunkid) = False Then

                    Dim dRow As DataRow
                    dRow = mdtFullAttachment.NewRow
                    dRow("scanattachtranunkid") = -1
                    dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                    dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                    dRow("filename") = f.Name
                    dRow("modulerefid") = enImg_Email_RefId.Training_Module
                    dRow("scanattachrefid") = enScanAttactRefId.TRAINING_NEED_FORM
                    dRow("transactionunkid") = mintTrainingRequestunkid
                    dRow("orgfilepath") = f.FullName
                    dRow("valuename") = ""
                    dRow("attached_date") = Today.Date
                    dRow("filename") = f.Name
                    dRow("filesize") = f.Length / 1024 'objScan.ConvertFileSize(f.Length)
                    dRow("attached_date") = Today.Date
                    If mblnFromCompleteESS Then
                        dRow("form_name") = "frmCompletedTrainingInfo"
                    Else
                        dRow("form_name") = mstrModuleName
                    End If


                    dRow("AUD") = "A"
                    dRow("GUID") = Guid.NewGuid.ToString

                    dRow("userunkid") = CInt(Session("userid"))

                    Dim xDocumentData As Byte() = IO.File.ReadAllBytes(f.FullName)
                    dRow("file_data") = xDocumentData

                    '*** Save / Upload attachment
                    Dim mdsDoc As DataSet
                    Dim mstrFolderName As String = ""
                    mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                    Dim strFileName As String = ""
                    mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(enScanAttactRefId.TRAINING_NEED_FORM)) Select (p.Item("Name").ToString)).FirstOrDefault
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(f.FullName)

                    If File.Exists(f.FullName) Then
                        Dim strPath As String = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/") & mstrFolderName & "/" & strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(f.FullName, Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))

                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath

                        mdtFullAttachment.Rows.Add(dRow)

                        objScan._Datatable = mdtFullAttachment.Copy
                        If objScan.InsertUpdateDelete_Documents(Nothing) = False Then
                            DisplayMessage.DisplayMessage(objScan._Message, Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "The file selected is already existing"), Me)
                    Exit Sub
                End If
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTable As DataTable
        Dim strMsg As String = String.Empty
        Try
            If mintTrainingRequestunkid > 0 Then
                dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestunkid, "")

                If dtTable.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "No file to download"), Me)
                    Exit Sub
                End If

                strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", dtTable, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "No file to download"), Me)
                Exit Sub
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Links Event "

    Protected Sub lnkAddAttendedTraining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAttendedTraining.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue"), Me)
                Exit Sub
            End If

            Call FillAttendedTrainingList()
            If CBool(Session("AllowToAddAttendedTraining")) = True Then
                btnAddAttendedTrainingName.Visible = True
            Else
                btnAddAttendedTrainingName.Visible = False
            End If

            mblnShowAttendedTrainingNamePopup = True
            popupAttendedTrainingName.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeleteSelectedAttendedTraining_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            mintTrainingAttendedTranunkid = CInt(dgvSelectedAttendedTraining.DataKeys(row.RowIndex)("trainingattendedtranunkid"))
            mstrDeleteAction = "delattendedtraining"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    'Protected Sub lnkAddTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingName.Click
    '    Try
    '        'Pinkal (08-Apr-2021)-- Start
    '        'Enhancement  -  Working on Employee Claim Form Report.
    '        dgvAddTrainingName.DataSource = Nothing
    '        dgvAddTrainingName.DataBind()

    '        If rdbApplyingScheduledTrainingYes.Checked Then

    '            Dim objCalender As New clsTraining_Calendar_Master
    '            Dim dsList As DataSet = objCalender.getListForCombo("List", True, enStatusType.OPEN)
    '            With cboTrainingCalender
    '                .DataValueField = "calendarunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables(0).Copy()
    '                .DataBind()
    '            End With
    '            'Hemant (03 Dec 2021) -- Start
    '            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    '            cboTrainingCalender.SelectedValue = CStr(cboPeriod.SelectedValue)
    '            LblTrainingCalender.Visible = True
    '            cboTrainingCalender.Visible = True
    '            cboTrainingCalender.Enabled = False
    '            'Hemant (03 Dec 2021) -- End
    '            dsList.Clear()
    '            dsList = Nothing

    '            cboTrainingCalender_SelectedIndexChanged(New Object, New EventArgs)

    '        Else
    '            LblTrainingCalender.Visible = False
    '            cboTrainingCalender.Visible = False
    '            Dim dsList As DataSet = objTrainingRequest.getTrainingComboList("List", rdbApplyingScheduledTrainingYes.Checked, rdbApplyingScheduledTrainingNo.Checked)

    '            dsList.Tables(0).Columns.Add("departmentaltrainingneedunkid", GetType(System.Int32)).DefaultValue = -1
    '            dsList.Tables(0).Columns.Add("trainingcategoryname", GetType(System.String)).DefaultValue = ""
    '            dsList.Tables(0).Columns.Add("startdate", GetType(System.String)).DefaultValue = ""
    '            dsList.Tables(0).Columns.Add("enddate", GetType(System.String)).DefaultValue = ""
    '            dsList.Tables(0).Columns.Add("allocationtranname", GetType(System.String)).DefaultValue = ""
    '            dsList.Tables(0).Columns.Add("trainingcategoryunkid", GetType(System.Int32)).DefaultValue = 0
    '            dsList.Tables(0).Columns.Add("allocationtranunkid", GetType(System.Int32)).DefaultValue = 0
    '            dsList.Tables(0).Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
    '            For Each drRow As DataRow In dsList.Tables(0).Rows
    '                drRow.Item("departmentaltrainingneedunkid") = -1
    '            Next
    '            dgvAddTrainingName.DataSource = dsList.Tables(0)
    '            dgvAddTrainingName.DataBind()
    '        End If
    '        'Pinkal (08-Apr-2021) -- End
    '        mblnShowTrainingNamePopup = True
    '        popupTrainingName.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Hemant (09 Feb 2022) -- End


    Protected Sub lnkAddFinancingSource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddFinancingSource.Click
        Dim objCommon As New clsCommon_Master
        Dim dsList As DataSet
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue"), Me)
                Exit Sub
            End If

            Dim strNotFilter As String = ""
            Dim strNotIDs As String = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("financingsourceunkid").ToString).ToArray)
            If strNotIDs.Trim <> "" Then
                strNotFilter &= " AND cfcommon_master.masterunkid NOT IN (" & strNotIDs & ") "
            End If

            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, "List", , True, strNotFilter)

            dgvAddFinancingSource.DataSource = dsList
            dgvAddFinancingSource.DataBind()

            mblnShowFinancingSourcePopup = True
            popupFinancingSource.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCommon = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Clear_Controls()

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Call FillEmployeeInfo(CInt(cboEmployee.SelectedValue))
                    Session("Employeeunkid") = CInt(cboEmployee.SelectedValue)

                Else
                    Call FillEmployeeInfo(CInt(Session("Employeeunkid")))

                End If
                'Hemant (09 Feb 2022) -- Start            
                'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                FillTrainingNameList()
                'Hemant (09 Feb 2022) -- End
            Else
                Clear_Controls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub drpRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpRole.SelectedIndexChanged
        Try
            If CInt(IIf(drpRole.SelectedValue = "", 0, drpRole.SelectedValue)) > 0 Then
                Dim objlevel As New clstraining_approverlevel_master
                Dim objApprover As New clstraining_approver_master
                objApprover._Mappingunkid = CInt(IIf(drpRole.SelectedValue = "", 0, drpRole.SelectedValue))
                objlevel._Levelunkid = objApprover._Levelunkid
                txtApproverLevel.Text = objlevel._Levelname
                txtApproverLevel.Attributes.Add("levelunkid", objlevel._Levelunkid.ToString())
                txtApproverLevel.Attributes.Add("priority", objlevel._Priority.ToString())
                drpRole.Attributes.Add("mappingunkid", objApprover._Mappingunkid.ToString())
                drpRole.Attributes.Add("mapuserunkid", objApprover._Mapuserunkid.ToString())
                objlevel = Nothing : objApprover = Nothing
            Else
                txtApproverLevel.Attributes.Remove("levelunkid")
                txtApproverLevel.Attributes.Remove("priority")
                drpRole.Attributes.Remove("mappingunkid")
                drpRole.Attributes.Remove("mapuserunkid")
                txtApproverLevel.Text = ""
                txtApprRejectRemark.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (08-Apr-2021)-- Start
    'Enhancement  -  Working on Employee Claim Form Report.   
    Protected Sub cboTrainingCalender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCalender.SelectedIndexChanged
        Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsList As DataSet = Nothing
        Try
            Dim strFilter As String = ""
            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)

            strFilter = " AND trdepartmentaltrainingneed_master.periodunkid = '" & CInt(cboTrainingCalender.SelectedValue) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0)  = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0)  = 0 "
            dsList = objDeptTrainingNeed.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), CInt(Session("TrainingNeedAllocationID")), "Tranining" _
                                                        , True, strFilter, 0, 0)

            strFilter = ""
            Dim ds As New DataSet
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim iLst As IEnumerable(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("targetedgroupunkid")).Distinct.ToList()
                If iLst.Count > 0 Then
                    For Each item In iLst
                        Dim xRow() As DataRow = Nothing
                        Dim dt As New DataTable
                        Select Case item
                            Case enAllocation.BRANCH
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Stationunkid & "' "
                            Case enAllocation.DEPARTMENT_GROUP
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Deptgroupunkid & "' "
                            Case enAllocation.DEPARTMENT
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Departmentunkid & "' "
                            Case enAllocation.SECTION_GROUP
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Sectiongroupunkid & "' "
                            Case enAllocation.SECTION
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Sectionunkid & "' "
                            Case enAllocation.UNIT_GROUP
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Unitgroupunkid & "' "
                            Case enAllocation.UNIT
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Unitunkid & "' "
                            Case enAllocation.TEAM
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Teamunkid & "' "
                            Case enAllocation.JOB_GROUP
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Jobgroupunkid & "' "
                            Case enAllocation.JOBS
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Jobunkid & "' "
                            Case enAllocation.CLASS_GROUP
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Classgroupunkid & "' "
                            Case enAllocation.CLASSES
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Classunkid & "' "
                            Case enAllocation.COST_CENTER
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & objEmp._Costcenterunkid & "' "
                            Case 0
                                strFilter = " targetedgroupunkid = '" & item & "' AND allocationtranunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
                        End Select
                        xRow = dsList.Tables(0).Select(strFilter)
                        If xRow.Length > 0 Then
                            dt = xRow.CopyToDataTable()
                            If ds.Tables.Count <= 0 Then
                                ds.Tables.Add(dt.Copy)
                            Else
                                ds.Tables(0).Merge(dt.Copy, True)
                            End If
                        End If
                        dt = Nothing
                    Next
                End If
            End If
            If ds.Tables.Count > 0 Then
                Dim xTable As DataTable = ds.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid")

                If xTable.Columns.Contains("IsGrp") = False Then
                    xTable.Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
                End If

                For Each r As DataRow In xTable.Rows
                    r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    r("IsGrp") = False
                Next

                xTable.Columns("trainingcourseunkid").ColumnName = "masterunkid"
                xTable.Columns("trainingcoursename").ColumnName = "name"

                Dim dsTCategoryList As DataSet = objTCategory.getListForCombo("List", False)
                Dim dRow As DataRow = Nothing
                For Each drTCategory As DataRow In dsTCategoryList.Tables(0).Rows
                    Dim drRow() As DataRow = xTable.Select("trainingcategoryunkid = " & CInt(drTCategory.Item("categoryunkid")) & " ")
                    If drRow.Length > 0 Then
                        dRow = xTable.NewRow()
                        dRow.Item("departmentaltrainingneedunkid") = -1
                        dRow.Item("trainingcategoryunkid") = CInt(drTCategory.Item("categoryunkid"))
                        dRow.Item("trainingcategoryname") = CStr(drTCategory.Item("categoryname"))
                        dRow.Item("masterunkid") = -1
                        dRow.Item("name") = ""
                        dRow.Item("startdate") = ""
                        dRow.Item("enddate") = ""
                        dRow.Item("allocationtranname") = ""
                        dRow.Item("allocationtranunkid") = -1
                        dRow.Item("IsGrp") = 1
                        xTable.Rows.Add(dRow)
                    End If
                Next


                xTable = New DataView(xTable, "", "trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy

                dgvAddTrainingName.DataSource = xTable
                dgvAddTrainingName.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDeptTrainingNeed = Nothing
            objTCategory = Nothing
        End Try
    End Sub
    'Pinkal (08-Apr-2021) -- End

    Protected Sub cboQualifGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQualifGrp.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objQMaster As New clsqualification_master
            dsCombos = objQMaster.GetComboList("List", True, CInt(cboQualifGrp.SelectedValue))
            With cboQualifcation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboQualifcation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQualifcation.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objQMaster As New clsqualification_master
            dsCombos = objQMaster.GetResultCodeFromQualification(cboQualifcation.SelectedValue, True)
            With cboResultCode
                .DataValueField = "resultunkid"
                .DataTextField = "resultname"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            FillTrainingNameList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboTrainingName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingName.SelectedIndexChanged
        Try

            If rdbApplyingScheduledTrainingYes.Checked Then
                mintDepartTrainingNeedId = CInt(cboTrainingName.SelectedValue)
            End If

            If rdbApplyingScheduledTrainingNo.Checked Then
                mintDepartTrainingNeedId = -1
                mintCourseMasterunkid = CInt(cboTrainingName.SelectedValue)
            End If

            FillDepartmentTrainingInfo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (09 Feb 2022) -- End


#End Region

#Region " Radion Button's Event "

    Protected Sub rdbApplyingScheduledTrainingYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbApplyingScheduledTrainingYes.CheckedChanged, rdbApplyingScheduledTrainingNo.CheckedChanged
        'Dim dsList As DataSet
        Try
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'txtTrainingName.Text = ""
            FillTrainingNameList()
            'Hemant (09 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELATTENDEDTRAINING"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Enter Reason to Delete")
                    delReason.Show()

                Case "DELATTACHMENT"
                    If mintTrainingRequestunkid > 0 AndAlso mintScanattachtranunkid > 0 Then
                        Dim objDocument As New clsScan_Attach_Documents
                        If objDocument.Delete(mintScanattachtranunkid.ToString, Nothing) = False Then
                            DisplayMessage.DisplayMessage(objDocument._Message, Me)
                        Else
                            Call FillAttachment()
                        End If

                    End If

            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfApprovDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfApprovDisapprove.buttonYes_Click
        Dim objTrainingApproval As New clstrainingapproval_process_tran
        Dim objRequestMaster As New clstraining_request_master
        Try
            Dim blnFlag As Boolean = False

            If IsValidate() = False Then Exit Sub

            Call SetValueTrainingApproval(objTrainingApproval)
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnApprove.ID.ToUpper, btnDisapprove.ID.ToUpper
                    If Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper Then
                        objTrainingApproval._Statusunkid = enTrainingRequestStatus.APPROVED
                    ElseIf Me.ViewState("Sender").ToString().ToUpper() = btnDisapprove.ID.ToUpper Then
                        objTrainingApproval._Statusunkid = enTrainingRequestStatus.REJECTED
                    End If

                    If mintPendingTrainingTranunkid > 0 Then
                        blnFlag = objTrainingApproval.Update(Session("Database_Name").ToString, _
                                                             eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                             mintCourseMasterunkid)
                        'Hemant (16 Nov 2021) -- [Session("Database_Name").ToString,eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate")))]
                    Else
                        blnFlag = objTrainingApproval.Insert()
                    End If

                    If blnFlag = False And objTrainingApproval._Message <> "" Then

                        DisplayMessage.DisplayMessage(objTrainingApproval._Message, Me)
                    Else
                        objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid

                        If objTrainingApproval._Statusunkid = enTrainingRequestStatus.APPROVED Then
                            objRequestMaster.Send_Notification_Approver(Session("Database_Name").ToString, _
                                                                        CInt(cboEmployee.SelectedValue), _
                                                                        CInt(IIf(CInt(txtApproverLevel.Attributes("priority")) > 0, CInt(txtApproverLevel.Attributes("priority")), -1)), _
                                                                        clstraining_request_master.enEmailType.Training_Approver, _
                                                                        CInt(Session("CompanyUnkId")), cboEmployee.SelectedItem.Text, cboTrainingName.Text, _
                                                                        dtpApplicationDate.GetDate.Date, CInt(Session("Fin_year")), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                        CStr(Session("ArutiSelfServiceURL")), dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, enLogin_Mode.DESKTOP, _
                                                                        0, CInt(Session("UserId")), , , False)
                            'Hemant (09 Feb 2022) -- [dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date,blnSendEmailReportingTo:= False, txtTrainingName.Text-->cboTrainingName.Text]
                            'Hemant (16 Nov 2021) -- [Session("Database_Name").ToString]
                            If objRequestMaster._Statusunkid = enTrainingRequestStatus.APPROVED Then
                                objRequestMaster.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
                                                                       enTrainingRequestStatus.APPROVED, _
                                                                      clstraining_request_master.enEmailType.Training_Approver, _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      CInt(Session("CompanyUnkId")), cboTrainingName.Text, _
                                                                      dtpApplicationDate.GetDate.Date, mintTrainingRequestunkid, _
                                                                      CStr(Session("ArutiSelfServiceURL")), txtApprRejectRemark.Text.Trim, _
                                                                      enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                                'Hemant (09 Feb 2022) -- [txtTrainingName.Text-->cboTrainingName.Text]
                            End If

                        ElseIf objTrainingApproval._Statusunkid = enTrainingRequestStatus.REJECTED Then
                            objRequestMaster.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
                                                                        enTrainingRequestStatus.REJECTED, _
                                                                       clstraining_request_master.enEmailType.Training_Approver, _
                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                       CInt(Session("CompanyUnkId")), cboTrainingName.Text, _
                                                                       dtpApplicationDate.GetDate.Date, mintTrainingRequestunkid, _
                                                                       CStr(Session("ArutiSelfServiceURL")), txtApprRejectRemark.Text.Trim, _
                                                                       enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                            'Hemant (09 Feb 2022) -- [txtTrainingName.Text-->cboTrainingName.Text]
                        End If

                        'Hemant (13 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        If Request.QueryString.Count <= 0 Then
                            'Hemant (13 Aug 2021) -- End
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Information saved successfully."), Me, "../Training_Request_Approval/wPg_TrainingRequestApprovalList.aspx")
                            'Hemant (13 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        Else
                            Session.Abandon()
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        End If
                        'Hemant (13 Aug 2021) -- End
                    End If

                Case btnEnrollConfirm.ID.ToUpper, btnEnrollReject.ID.ToUpper
                    Dim blnIsEnrollConfirm As Boolean = False
                    Dim blnIsEnrollReject As Boolean = False
                    Dim decEnrollAmount As Decimal = 0
                    If Me.ViewState("Sender").ToString().ToUpper() = btnEnrollConfirm.ID.ToUpper Then
                        'Hemant (28 Jul 2021) -- Start             
                        'ENHANCEMENT : OLD-293 - Training Evaluation
                        Dim objEvalQuestion As New clseval_question_master
                        Dim objEvaluation As New clstraining_evaluation_tran
                        Dim intEmployeeunkid As Integer = -1
                        Dim dsSumbitQuestions As DataSet
                        Dim dsQuestions As DataSet = Nothing
                        Dim blnAllowEnroll As Boolean = False

                        objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
                        intEmployeeunkid = objRequestMaster._Employeeunkid

                        dsQuestions = objEvalQuestion.GetList("List", True, , clseval_group_master.enFeedBack.PRETRAINING)
                        If dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                            dsSumbitQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.PRETRAINING)
                            If dsSumbitQuestions IsNot Nothing AndAlso dsSumbitQuestions.Tables(0).Rows.Count > 0 AndAlso objRequestMaster._IsPreTrainingFeedbackSubmitted = True Then
                                blnAllowEnroll = True
                            End If
                        Else
                            blnAllowEnroll = True
                        End If

                        If blnAllowEnroll = True OrElse CBool(Session("PreTrainingEvaluationSubmitted")) = True Then
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 47, "Sorry, You can't Enroll for this training before submitting Pre-Training Evaluation Form for this training."), Me)
                            Exit Sub
                        End If
                        'Hemant (28 Jul 2021) -- End
                        blnIsEnrollConfirm = True
                        blnIsEnrollReject = False
                        decEnrollAmount = CDec(txtEnrollmentAmount.Text)
                        'Hemant (28 Jul 2021) -- Start             
                        'ENHANCEMENT : OLD-293 - Training Evaluation
                        objEvalQuestion = Nothing
                        objEvaluation = Nothing
                        'Hemant (28 Jul 2021) -- End
                    ElseIf Me.ViewState("Sender").ToString().ToUpper() = btnEnrollReject.ID.ToUpper Then
                        blnIsEnrollConfirm = False
                        blnIsEnrollReject = True
                        decEnrollAmount = 0
                    End If

                    Dim objTMaster As New clstraining_request_master

                    With objTMaster

                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginEmployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = 0
                        End If
                        .pblnIsWeb = True
                        .pstrClientIP = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With

                    If objTMaster.UpdateEnrollment(mintTrainingRequestunkid.ToString, _
                                                blnIsEnrollConfirm, _
                                                blnIsEnrollReject, _
                                                decEnrollAmount, _
                                                txtEnrollmentRemark.Text _
                                                ) = False Then

                        If objTMaster._Message <> "" Then
                            DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                            Exit Try
                        End If
                    Else
                        'Hemant (13 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        If Request.QueryString.Count <= 0 Then
                            'Hemant (13 Aug 2021) -- End
                            Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestFormList.aspx", False)
                            'Hemant (13 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        Else
                            Session.Abandon()
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        End If
                        'Hemant (13 Aug 2021) -- End
                    End If

                Case btnComplete.ID.ToUpper

                    If mblnFromCompleteESS Then
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-451 - Training Completion Screen - Forces user to attach Doc even when Cert not required.
                        Dim objTrainingRequest As New clstraining_request_master
                        Dim objDepartmentalTrainingNeed As New clsDepartmentaltrainingneed_master
                        objTrainingRequest._TrainingRequestunkid = mintTrainingRequestunkid
                        objDepartmentalTrainingNeed._Departmentaltrainingneedunkid = objTrainingRequest._DepartmentalTrainingNeedunkid
                        If objTrainingRequest._DepartmentalTrainingNeedunkid > 0 AndAlso objDepartmentalTrainingNeed._Iscertirequired Then
                            'Hemant (20 Aug 2021) -- End
                            Dim objDocument As New clsScan_Attach_Documents
                            Dim dtTable As DataTable
                            dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestunkid, "")
                            Dim drAttachRow() As DataRow = dtTable.Select("form_name = 'frmCompletedTrainingInfo'")
                            If drAttachRow.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, Document Attachment is mandatory information. Please Attach Document to continue"), Me)
                                Exit Sub
                            End If
                            objDocument = Nothing
                        End If
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-451 - Training Completion Screen - Forces user to attach Doc even when Cert not required.
                        objTrainingRequest = Nothing
                        objDepartmentalTrainingNeed = Nothing
                        'Hemant (20 Aug 2021) -- End
                    End If

                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
                    Dim blnFromApproval As Boolean = False
                    Dim intStatusunkid As Integer = enTrainingRequestStatus.PENDING
                    Dim dtScanTable As DataTable = Nothing
                    If CBool(Session("AllowToMarkTrainingAsComplete")) = True Then
                        blnFromApproval = True
                        intStatusunkid = enTrainingRequestStatus.APPROVED

                        Dim objDocument As New clsScan_Attach_Documents

                        dtScanTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestunkid, "")
                        dtScanTable = New DataView(dtScanTable, " form_name = 'frmCompletedTrainingInfo' ", "", DataViewRowState.CurrentRows).ToTable

                        Dim mdsDoc As DataSet
                        Dim mstrTrainingFolderName As String = String.Empty
                        Dim mstrQualificationFolderName As String = String.Empty
                        mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")

                        mstrTrainingFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(enScanAttactRefId.TRAINING_NEED_FORM)) Select (p.Item("Name").ToString)).FirstOrDefault
                        mstrQualificationFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(enScanAttactRefId.QUALIFICATIONS)) Select (p.Item("Name").ToString)).FirstOrDefault
                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrQualificationFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrQualificationFolderName))
                        End If
                        Dim strDestinationPath As String
                        For Each drRow As DataRow In dtScanTable.Rows
                            strDestinationPath = drRow.Item("filepath").ToString.Replace(mstrTrainingFolderName, mstrQualificationFolderName)
                            drRow.Item("scanattachrefid") = enScanAttactRefId.QUALIFICATIONS
                            drRow.Item("modulerefid") = enImg_Email_RefId.Employee_Module
                            If Not File.Exists(strDestinationPath) Then
                                File.Copy(drRow.Item("filepath").ToString, strDestinationPath)
                            End If
                            drRow.Item("filepath") = drRow.Item("filepath").ToString.Replace(mstrTrainingFolderName, mstrQualificationFolderName)
                            drRow.Item("AUD") = "A"
                        Next
                        dtScanTable.AcceptChanges()

                    End If
                    'Hemant (25 May 2021) -- End

                    Dim objTMaster As New clstraining_request_master
                    objTMaster._TrainingRequestunkid = mintTrainingRequestunkid
                    With objTMaster


                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginEmployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                            'Hemant (25 May 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
                            .pintCompletedUserunkid = CInt(Session("UserId"))
                            'Hemant (25 May 2021) -- End
                        Else
                            .pintUserunkid = 0
                            .pintLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = 0
                        End If
                        .pblnIsWeb = True
                        .pstrClientIP = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With


                    If objTMaster.UpdateCompletedTrainingInfo(mintTrainingRequestunkid.ToString, _
                                                CInt(cboQualifGrp.SelectedValue), _
                                                CInt(cboQualifcation.SelectedValue), _
                                                CInt(cboResultCode.SelectedValue), _
                                                CDec(txtGPAcode.Text), _
                                                txtOtherQualificationGrp.Text, _
                                                txtOtherQualification.Text, _
                                                txtOtherResultCode.Text, _
                                                blnFromApproval, intStatusunkid, -1, txtCompletedRemark.Text, dtScanTable) = False Then
                        'Hemant (25 May 2021) --  [False --> blnFromApproval , 1 --> intStatusunkid,Nothing --> dtScanTable]
                        If objTMaster._Message <> "" Then
                            DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                            Exit Try
                        End If
                    Else
                        Dim enLoginMode As New enLogin_Mode
                        Dim intLoginByEmployeeId As Integer = 0
                        Dim intUserId As Integer = 0

                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                            enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                            intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                            intUserId = 0
                        Else
                            enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                            intUserId = CInt(Session("UserId"))
                            intLoginByEmployeeId = 0
                        End If
                        'Hemant (25 May 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
                        If CBool(Session("AllowToMarkTrainingAsComplete")) = True Then
                            objTMaster.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
                                                                           enTrainingRequestStatus.APPROVED, _
                                                                          clstraining_request_master.enEmailType.Completed_Training_Approver, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          CInt(Session("CompanyUnkId")), cboTrainingName.Text, _
                                                                          dtpApplicationDate.GetDate.Date, mintTrainingRequestunkid, _
                                                                          CStr(Session("ArutiSelfServiceURL")), txtCompletedRemark.Text.Trim, _
                                                                          enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                            'Hemant (09 Feb 2022) -- [txtTrainingName.Text --> cboTrainingName.Text]
                        Else
                            'Hemant (25 May 2021) -- End
                            objTMaster.Send_Notification_Approver(Session("Database_Name").ToString, _
                                                                  CInt(cboEmployee.SelectedValue), _
                                                                  CInt(IIf(objTMaster._MinApprovedPriority > 0, objTMaster._MinApprovedPriority, -1)), _
                                                                  clstraining_request_master.enEmailType.Completed_Training_Approver, _
                                                                  CInt(Session("CompanyUnkId")), cboEmployee.SelectedItem.Text, cboTrainingName.Text, _
                                                                  dtpApplicationDate.GetDate.Date, CInt(Session("Fin_year")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                  CStr(Session("ArutiSelfServiceURL")), _
                                                                  dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, _
                                                                  enLoginMode, intLoginByEmployeeId, intUserId, , , False)
                            'Hemant (09 Feb 2022) -- [dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date,blnSendEmailReportingTo:=False,txtTrainingName.Text --> cboTrainingName.Text]
                            'Hemant (16 Nov 2021) -- [Session("Database_Name").ToString]
                        End If 'Hemant (25 May 2021)

                        'Hemant (13 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        If Request.QueryString.Count <= 0 Then
                            'Hemant (13 Aug 2021) -- End
                            Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestFormList.aspx", False)
                            'Hemant (13 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        Else
                            Session.Abandon()
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        End If
                        'Hemant (13 Aug 2021) -- End
                    End If

                Case btnCompletedApprove.ID.ToUpper, btnCompletedDisapprove.ID.ToUpper
                    Dim intStatusunkid As Integer
                    Dim objDocument As New clsScan_Attach_Documents
                    Dim dtTable As DataTable

                    If Me.ViewState("Sender").ToString().ToUpper() = btnCompletedApprove.ID.ToUpper Then
                        intStatusunkid = enTrainingRequestStatus.APPROVED

                        dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestunkid, "")
                        dtTable = New DataView(dtTable, " form_name = 'frmCompletedTrainingInfo' ", "", DataViewRowState.CurrentRows).ToTable

                        Dim mdsDoc As DataSet
                        Dim mstrTrainingFolderName As String = String.Empty
                        Dim mstrQualificationFolderName As String = String.Empty
                        mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")

                        mstrTrainingFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(enScanAttactRefId.TRAINING_NEED_FORM)) Select (p.Item("Name").ToString)).FirstOrDefault
                        mstrQualificationFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(enScanAttactRefId.QUALIFICATIONS)) Select (p.Item("Name").ToString)).FirstOrDefault
                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrQualificationFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrQualificationFolderName))
                        End If
                        Dim strDestinationPath As String
                        For Each drRow As DataRow In dtTable.Rows
                            strDestinationPath = drRow.Item("filepath").ToString.Replace(mstrTrainingFolderName, mstrQualificationFolderName)
                            drRow.Item("scanattachrefid") = enScanAttactRefId.QUALIFICATIONS
                            drRow.Item("modulerefid") = enImg_Email_RefId.Employee_Module
                            If Not File.Exists(strDestinationPath) Then
                                File.Copy(drRow.Item("filepath").ToString, strDestinationPath)
                            End If
                            drRow.Item("filepath") = drRow.Item("filepath").ToString.Replace(mstrTrainingFolderName, mstrQualificationFolderName)
                            drRow.Item("AUD") = "A"
                        Next
                        dtTable.AcceptChanges()

                    ElseIf Me.ViewState("Sender").ToString().ToUpper() = btnCompletedDisapprove.ID.ToUpper Then
                        intStatusunkid = enTrainingRequestStatus.REJECTED
                    End If
                    Dim objTMaster As New clstraining_request_master

                    With objTMaster


                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginEmployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                            .pintCompletedUserunkid = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = 0
                        End If
                        .pblnIsWeb = True
                        .pstrClientIP = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With



                    If objTMaster.UpdateCompletedTrainingInfo(mintTrainingRequestunkid.ToString, _
                                                CInt(cboQualifGrp.SelectedValue), _
                                                CInt(cboQualifcation.SelectedValue), _
                                                CInt(cboResultCode.SelectedValue), _
                                                CDec(txtGPAcode.Text), _
                                                txtOtherQualificationGrp.Text, _
                                                txtOtherQualification.Text, _
                                                txtOtherResultCode.Text, _
                                                True, intStatusunkid, _
                                                mintPendingTrainingTranunkid, _
                                                txtCompletedRemark.Text, _
                                                dtTable) = False Then

                        If objTMaster._Message <> "" Then
                            DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                            Exit Try
                        End If
                    Else
                        If intStatusunkid = enTrainingRequestStatus.APPROVED Then
                            objTMaster.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
                                                                           enTrainingRequestStatus.APPROVED, _
                                                                          clstraining_request_master.enEmailType.Completed_Training_Approver, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          CInt(Session("CompanyUnkId")), cboTrainingName.Text, _
                                                                          dtpApplicationDate.GetDate.Date, mintTrainingRequestunkid, _
                                                                          CStr(Session("ArutiSelfServiceURL")), txtCompletedRemark.Text.Trim, _
                                                                          enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                            'Hemant (09 Feb 2022) -- [txtTrainingName.Text --> cboTrainingName.Text]

                            'Hemant (04 Sep 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-444 - Success Message when Training Completion is Approved.
                            If Request.QueryString.Count <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 58, "Training completion status approved successfully."), Me, "../Training_Completion_Approval/wPg_TrainingCompletionApprovalList.aspx")
                            Else
                                Session.Abandon()
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 58, "Training completion status approved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                            End If
                            'Hemant (04 Sep 2021) -- End
                        ElseIf intStatusunkid = enTrainingRequestStatus.REJECTED Then
                            objTMaster.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
                                                                        enTrainingRequestStatus.REJECTED, _
                                                                       clstraining_request_master.enEmailType.Completed_Training_Approver, _
                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                       CInt(Session("CompanyUnkId")), cboTrainingName.Text, _
                                                                       dtpApplicationDate.GetDate.Date, mintTrainingRequestunkid, _
                                                                       CStr(Session("ArutiSelfServiceURL")), txtCompletedRemark.Text.Trim, _
                                                                       enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                            'Hemant (09 Feb 2022) -- [txtTrainingName.Text --> cboTrainingName.Text]
                            'Hemant (04 Sep 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-444 - Success Message when Training Completion is Approved.
                            If Request.QueryString.Count <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 59, "Training completion status rejected successfully."), Me, "../Training_Completion_Approval/wPg_TrainingCompletionApprovalList.aspx")
                            Else
                                Session.Abandon()
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 59, "Training completion status rejected successfully. "), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                            End If
                            'Hemant (04 Sep 2021) -- End
                        End If
                        'Hemant (18 May 2021) -- Start
                        'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
                        'Response.Redirect(Session("rootpath").ToString & "Training\Training_Request_Approval\wPg_TrainingRequestApprovalList.aspx", False)
                        'Hemant (13 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        'Hemant (04 Sep 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-444 - Success Message when Training Completion is Approved.
                        'If Request.QueryString.Count <= 0 Then
                        '    'Hemant (13 Aug 2021) -- End
                        '    Response.Redirect(Session("rootpath").ToString & "Training\Training_Completion_Approval\wPg_TrainingCompletionApprovalList.aspx", False)
                        '    'Hemant (13 Aug 2021) -- Start
                        '    'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                        'Else
                        '    Session.Abandon()
                        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 44, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        'End If
                        'Hemant (04 Sep 2021) -- End
                        'Hemant (13 Aug 2021) -- End
                        'Hemant (18 May 2021) -- End

                    End If
            End Select


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

    'Hemant (13 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
    Protected Sub cntSaveConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cntSaveConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Call Save_Click()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (13 Aug 2021) -- End

#End Region

#Region "Delete Reason"

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Dim objAttendedTraining As New clstraining_Attended_Training
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELATTENDEDTRAINING"
                    SetAttendedTrainingValue(objAttendedTraining)
                    objAttendedTraining._Isvoid = True
                    objAttendedTraining._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objAttendedTraining._Voiduserunkid = CInt(Session("UserId"))
                    objAttendedTraining._Voidreason = delReason.Reason
                    blnFlag = objAttendedTraining.Delete(mintTrainingAttendedTranunkid)
                    If blnFlag = False AndAlso objAttendedTraining._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objAttendedTraining._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Training Attended deleted successfully"), Me)
                        Call FillEmployeeAttendedTrainingList()
                    End If

            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        Finally
            objAttendedTraining = Nothing
        End Try
    End Sub

#End Region

#Region " GridView "

    Protected Sub dgvAddTrainingName_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAddTrainingName.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            If e.Row.RowType = DataControlRowType.DataRow Then
                If rdbApplyingScheduledTrainingYes.Checked AndAlso CBool(dgvAddTrainingName.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "trainingcategoryname").ToString
                    e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = 2 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    Dim ChkgvSelect As CheckBox = TryCast(e.Row.FindControl("ChkgvSelect"), CheckBox)
                    ChkgvSelect.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Attachment Events "

    Protected Sub dgvAttchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvAttchment.RowCommand
        Try
            Dim SrNo As Integer = CInt(e.CommandArgument)

            mintScanattachtranunkid = CInt(dgvAttchment.DataKeys(SrNo).Item("scanattachtranunkid"))

            If e.CommandName = "Remove" Then
                If mintScanattachtranunkid > 0 Then

                    mstrDeleteAction = "delattachment"
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Are you sure you want to delete this attachment?")
                    cnfConfirm.Show()
                    Exit Sub
                End If

            ElseIf e.CommandName = "Download" Then

                Dim xPath As String = ""

                If mintScanattachtranunkid > 0 Then
                    xPath = dgvAttchment.DataKeys(SrNo).Item("filepath").ToString
                    If xPath.Contains(Session("ArutiSelfServiceURL").ToString) = True Then
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    End If
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 57, "File does not Exist..."), Me)
                        Exit Sub
                    End If
                    fileInfo = Nothing
                    Dim strFile As String = xPath
                    Response.ContentType = "image/jpg/pdf"
                    Response.AddHeader("Content-Disposition", "attachment;filename=""" & dgvAttchment.Rows(SrNo).Cells(colTrainingAttachment.FileName).Text & """")
                    Response.Clear()
                    Response.TransmitFile(strFile)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                mintScanattachtranunkid = 0

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvAttchment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAttchment.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim DeleteImg As LinkButton = TryCast(e.Row.FindControl("DeleteImg"), LinkButton)
                If mblnFromCompleteESS AndAlso dgvAttchment.DataKeys(e.Row.RowIndex)("form_name").ToString = mstrModuleName Then
                    DeleteImg.Visible = False
                End If
                If mblnFromCompleteMSS OrElse mblnFromApproval Then
                    DeleteImg.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"
    Protected Sub chkOtherQualification_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherQualification.CheckedChanged
        Try
            If chkOtherQualification.Checked = False Then
                pnlOtherQualificationGroup.Visible = False
                pnlOtherQualification.Visible = False
                pnlOtherResultCode.Visible = False
                pnlQualificationGroup.Visible = True
                pnlQualification.Visible = True
                pnlResultCode.Visible = True

            Else
                pnlQualificationGroup.Visible = False
                pnlQualification.Visible = False
                pnlResultCode.Visible = False
                pnlOtherQualificationGroup.Visible = True
                pnlOtherQualification.Visible = True
                pnlOtherResultCode.Visible = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingRequestDetails.ID, lblTrainingRequestDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEmployee.ID, lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApplyingScheduledTraining.ID, lblApplyingScheduledTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApplicationDate.ID, lblApplicationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingName.ID, lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStartDate.ID, lblStartDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEndDate.ID, lblEndDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblProviderName.ID, lblProviderName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblProviderAddress.ID, lblProviderAddress.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCost.ID, lblTrainingCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblFinancingSource.ID, lblFinancingSource.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingAttended.ID, lblTrainingAttended.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAlignedCurrentJob.ID, lblAlignedCurrentJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRecommendedPDP.ID, lblRecommendedPDP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblForeignTravelling.ID, lblForeignTravelling.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblExpectedReturn.ID, lblExpectedReturn.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRemarks.ID, lblRemarks.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCompletedInfo.ID, lblTrainingCompletedInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblQualificationGroup.ID, lblQualificationGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblOtherQualificationGroup.ID, lblOtherQualificationGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblQualification.ID, lblQualification.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblOtherQualification.ID, lblOtherQualification.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblResultCode.ID, lblResultCode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblOtherResultCode.ID, lblOtherResultCode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblGPAcode.ID, lblGPAcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCompletedRemark.ID, lblCompletedRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAttachmentInfo.ID, lblAttachmentInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblScanDocumentType.ID, lblScanDocumentType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblChangeStatus.ID, lblChangeStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStatus.ID, lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprovalData.ID, lblApprovalData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRole.ID, lblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLevel.ID, lblLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprovedAmount.ID, lblApprovedAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprRejectRemark.ID, lblApprRejectRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEnrollmentInfo.ID, lblEnrollmentInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEnrollmentAmount.ID, lblEnrollmentAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEnrollmentRemark.ID, lblEnrollmentRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTTrainingName.ID, lblTTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, LblTrainingCalender.ID, LblTrainingCalender.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCourseMaster.ID, lblTrainingCourseMaster.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCode.ID, lblCode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAlias.ID, lblAlias.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAddTrainingName.ID, lblAddTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDescription.ID, lblDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAttendedTrainingName.ID, lblAttendedTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAddAttendedTrainingName.ID, lblAddAttendedTrainingName.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, chkOtherQualification.ID, chkOtherQualification.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdJobCapability.ID, rdJobCapability.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdCareerDevelopment.ID, rdCareerDevelopment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbApplyingScheduledTrainingYes.ID, rdbApplyingScheduledTrainingYes.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbApplyingScheduledTrainingNo.ID, rdbApplyingScheduledTrainingNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbAlignedCurrentJobYes.ID, rdbAlignedCurrentJobYes.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbAlignedCurrentJobNo.ID, rdbAlignedCurrentJobNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbRecommendedPDPYes.ID, rdbRecommendedPDPYes.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbRecommendedPDPNo.ID, rdbRecommendedPDPNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbForeignTravellingYes.ID, rdbForeignTravellingYes.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, rdbForeignTravellingNo.ID, rdbForeignTravellingNo.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnDownloadAll.ID, btnDownloadAll.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnEnrollConfirm.ID, btnEnrollConfirm.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnEnrollReject.ID, btnEnrollReject.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnApprove.ID, btnApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnDisapprove.ID, btnDisapprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnComplete.ID, btnComplete.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCompletedApprove.ID, btnCompletedApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCompletedDisapprove.ID, btnCompletedDisapprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSave.ID, btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSubmit.ID, btnSubmit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnAddTrainingName.ID, btnAddTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnTrainingName.ID, btnTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCloseTCostItem.ID, btnCloseTCostItem.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnTrainingNameAdd.ID, btnTrainingNameAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnTrainingNameClose.ID, btnTrainingNameClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnAddAttendedTrainingName.ID, btnAddAttendedTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSelectAttendedTrainingName.ID, btnSelectAttendedTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCloseAttendedTrainingName.ID, btnCloseAttendedTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnAttendedTrainingNameAdd.ID, btnAttendedTrainingNameAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnAttendedTrainingNameClose.ID, btnAttendedTrainingNameClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnAddFSource.ID, btnAddFSource.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCloseFSource.ID, btnCloseFSource.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingCostItem.Columns(0).FooterText, dgvTrainingCostItem.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvSelectedAttendedTraining.Columns(0).FooterText, dgvSelectedAttendedTraining.Columns(0).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAttchment.Columns(2).FooterText, dgvAttchment.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAddAttendedTraining.Columns(1).FooterText, dgvAddAttendedTraining.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAddFinancingSource.Columns(1).FooterText, dgvAddFinancingSource.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAddFinancingSource.Columns(2).FooterText, dgvAddFinancingSource.Columns(2).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblTrainingRequestDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingRequestDetails.ID, lblTrainingRequestDetails.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEmployee.ID, lblEmployee.Text)
            Me.lblApplyingScheduledTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApplyingScheduledTraining.ID, lblApplyingScheduledTraining.Text)
            Me.lblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApplicationDate.ID, lblApplicationDate.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingName.ID, lblTrainingName.Text)
            Me.lblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStartDate.ID, lblStartDate.Text)
            Me.lblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEndDate.ID, lblEndDate.Text)
            Me.lblProviderName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblProviderName.ID, lblProviderName.Text)
            Me.lblProviderAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblProviderAddress.ID, lblProviderAddress.Text)
            Me.lblTrainingCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCost.ID, lblTrainingCost.Text)
            Me.lblFinancingSource.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblFinancingSource.ID, lblFinancingSource.Text)
            Me.lblTotalTrainingCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Me.lblTrainingAttended.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingAttended.ID, lblTrainingAttended.Text)
            Me.lblAlignedCurrentJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAlignedCurrentJob.ID, lblAlignedCurrentJob.Text)
            Me.lblRecommendedPDP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRecommendedPDP.ID, lblRecommendedPDP.Text)
            Me.lblForeignTravelling.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblForeignTravelling.ID, lblForeignTravelling.Text)
            Me.lblExpectedReturn.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblExpectedReturn.ID, lblExpectedReturn.Text)
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRemarks.ID, lblRemarks.Text)
            Me.lblTrainingCompletedInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCompletedInfo.ID, lblTrainingCompletedInfo.Text)
            Me.lblQualificationGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblQualificationGroup.ID, lblQualificationGroup.Text)
            Me.lblOtherQualificationGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblOtherQualificationGroup.ID, lblOtherQualificationGroup.Text)
            Me.lblQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblQualification.ID, lblQualification.Text)
            Me.lblOtherQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblOtherQualification.ID, lblOtherQualification.Text)
            Me.lblResultCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblResultCode.ID, lblResultCode.Text)
            Me.lblOtherResultCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblOtherResultCode.ID, lblOtherResultCode.Text)
            Me.lblGPAcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblGPAcode.ID, lblGPAcode.Text)
            Me.lblCompletedRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCompletedRemark.ID, lblCompletedRemark.Text)
            Me.lblAttachmentInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAttachmentInfo.ID, lblAttachmentInfo.Text)
            Me.lblScanDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblScanDocumentType.ID, lblScanDocumentType.Text)
            Me.lblChangeStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblChangeStatus.ID, lblChangeStatus.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStatus.ID, lblStatus.Text)
            Me.lblApprovalData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprovalData.ID, lblApprovalData.Text)
            Me.lblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRole.ID, lblRole.Text)
            Me.lblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLevel.ID, lblLevel.Text)
            Me.lblApprovedAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprovedAmount.ID, lblApprovedAmount.Text)
            Me.lblApprRejectRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprRejectRemark.ID, lblApprRejectRemark.Text)
            Me.lblEnrollmentInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEnrollmentInfo.ID, lblEnrollmentInfo.Text)
            Me.lblEnrollmentAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEnrollmentAmount.ID, lblEnrollmentAmount.Text)
            Me.lblEnrollmentRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEnrollmentRemark.ID, lblEnrollmentRemark.Text)
            Me.lblTTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTTrainingName.ID, lblTTrainingName.Text)
            Me.LblTrainingCalender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblTrainingCalender.ID, LblTrainingCalender.Text)
            Me.lblTrainingCourseMaster.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCourseMaster.ID, lblTrainingCourseMaster.Text)
            Me.lblCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCode.ID, lblCode.Text)
            Me.lblAlias.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAlias.ID, lblAlias.Text)
            Me.lblAddTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAddTrainingName.ID, lblAddTrainingName.Text)
            Me.lblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDescription.ID, lblDescription.Text)
            Me.lblAttendedTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAttendedTrainingName.ID, lblAttendedTrainingName.Text)
            Me.lblAddAttendedTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAddAttendedTrainingName.ID, lblAddAttendedTrainingName.Text)

            Me.chkOtherQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), chkOtherQualification.ID, chkOtherQualification.Text)

            Me.rdJobCapability.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdJobCapability.ID, rdJobCapability.Text)
            Me.rdCareerDevelopment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdCareerDevelopment.ID, rdCareerDevelopment.Text)
            Me.rdbApplyingScheduledTrainingYes.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbApplyingScheduledTrainingYes.ID, rdbApplyingScheduledTrainingYes.Text)
            Me.rdbApplyingScheduledTrainingNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbApplyingScheduledTrainingNo.ID, rdbApplyingScheduledTrainingNo.Text)
            Me.rdbAlignedCurrentJobYes.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbAlignedCurrentJobYes.ID, rdbAlignedCurrentJobYes.Text)
            Me.rdbAlignedCurrentJobNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbAlignedCurrentJobNo.ID, rdbAlignedCurrentJobNo.Text)
            Me.rdbRecommendedPDPYes.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbRecommendedPDPYes.ID, rdbRecommendedPDPYes.Text)
            Me.rdbRecommendedPDPNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbRecommendedPDPNo.ID, rdbRecommendedPDPNo.Text)
            Me.rdbForeignTravellingYes.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbForeignTravellingYes.ID, rdbForeignTravellingYes.Text)
            Me.rdbForeignTravellingNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), rdbForeignTravellingNo.ID, rdbForeignTravellingNo.Text)

            Me.btnDownloadAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnDownloadAll.ID, btnDownloadAll.Text).Replace("&", "")
            Me.btnEnrollConfirm.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnEnrollConfirm.ID, btnEnrollConfirm.Text).Replace("&", "")
            Me.btnEnrollReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnEnrollReject.ID, btnEnrollReject.Text).Replace("&", "")
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnApprove.ID, btnApprove.Text).Replace("&", "")
            Me.btnDisapprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnDisapprove.ID, btnDisapprove.Text).Replace("&", "")
            Me.btnComplete.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnComplete.ID, btnComplete.Text).Replace("&", "")
            Me.btnCompletedApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCompletedApprove.ID, btnCompletedApprove.Text).Replace("&", "")
            Me.btnCompletedDisapprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCompletedDisapprove.ID, btnCompletedDisapprove.Text).Replace("&", "")
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSave.ID, btnSave.Text).Replace("&", "")
            Me.btnSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSubmit.ID, btnSubmit.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")
            Me.btnAddTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnAddTrainingName.ID, btnAddTrainingName.Text).Replace("&", "")
            Me.btnTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnTrainingName.ID, btnTrainingName.Text).Replace("&", "")
            Me.btnCloseTCostItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCloseTCostItem.ID, btnCloseTCostItem.Text).Replace("&", "")
            Me.btnTrainingNameAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnTrainingNameAdd.ID, btnTrainingNameAdd.Text).Replace("&", "")
            Me.btnTrainingNameClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnTrainingNameClose.ID, btnTrainingNameClose.Text).Replace("&", "")
            Me.btnAddAttendedTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnAddAttendedTrainingName.ID, btnAddAttendedTrainingName.Text).Replace("&", "")
            Me.btnSelectAttendedTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSelectAttendedTrainingName.ID, btnSelectAttendedTrainingName.Text).Replace("&", "")
            Me.btnCloseAttendedTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCloseAttendedTrainingName.ID, btnCloseAttendedTrainingName.Text).Replace("&", "")
            Me.btnAttendedTrainingNameAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnAttendedTrainingNameAdd.ID, btnAttendedTrainingNameAdd.Text).Replace("&", "")
            Me.btnAttendedTrainingNameClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnAttendedTrainingNameClose.ID, btnAttendedTrainingNameClose.Text).Replace("&", "")
            Me.btnAddFSource.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnAddFSource.ID, btnAddFSource.Text).Replace("&", "")
            Me.btnCloseFSource.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCloseFSource.ID, btnCloseFSource.Text).Replace("&", "")

            dgvTrainingCostItem.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingCostItem.Columns(0).FooterText, dgvTrainingCostItem.Columns(0).HeaderText)
            dgvTrainingCostItem.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)

            dgvFinancingSource.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)

            dgvSelectedAttendedTraining.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvSelectedAttendedTraining.Columns(0).FooterText, dgvSelectedAttendedTraining.Columns(0).HeaderText)

            dgvAttchment.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAttchment.Columns(2).FooterText, dgvAttchment.Columns(2).HeaderText)

            dgvAddTrainingName.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)

            dgvAddAttendedTraining.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAddAttendedTraining.Columns(1).FooterText, dgvAddAttendedTraining.Columns(1).HeaderText)

            dgvAddFinancingSource.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAddFinancingSource.Columns(1).FooterText, dgvAddFinancingSource.Columns(1).HeaderText)
            dgvAddFinancingSource.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAddFinancingSource.Columns(2).FooterText, dgvAddFinancingSource.Columns(2).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Training Attended saved successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Training saved successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, Application date is mandatory information. Please select Application date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, the End date is mandatory information. Please select the end date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Please Select at least one course type")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, Training Name is mandatory information. Please select Training Name to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, the Start date is mandatory information. Please select the start date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Training Attended deleted successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Enter Reason to Delete")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, ""Is this training aligned with your current role?"" is mandatory information. Please select Yes/No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "The file selected is already existing")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "No file to download")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, ""Is this training part of the recommended training in your PDP?"" is mandatory information. Please select Yes/No to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Sorry, ""Does this Training Require Travelling to Foreign Country?"" is mandatory information. Please select Yes/No to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Sorry, the selected employee is not mapped with the highest approver level defined in the system. Training will not be approved unless Employee is mapped with the highest level")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Sorry, Role is mandatory information. Please select Role to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "Sorry, Approver Level is mandatory information. Please select Approver Level to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Please tick at least one financing source from the list")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, Approved Amount is mandatory information. Please enter the Approved Amount to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Sorry, Approver Remark is mandatory information. Please select Approver Remark to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Sorry, Calendar is mandatory information. Please select Calendar to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Sorry, End date should be in between selected period start date and end date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Sorry, Start date should not be greater than end date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Sorry, Document Attachment is mandatory information. Please Attach Document to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Sorry, Start date should be in between selected period start date and end date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Sorry, Other Qualification Group is mandatory information. Please enter Other Qualification Group to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 32, "Sorry, Other Qualification is mandatory information. Please enter Other Qualification to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "Sorry, Qualification Group is mandatory information. Please select Qualification Group to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 34, "Sorry, Qualification is mandatory information. Please select Qualification to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 35, "Reporting to:")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 36, "You can't Edit this training request detail. Reason: This Completion training is already approved/reject")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 37, "You can't Edit this training request detail. Reason: This training request is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 38, "You can't Edit this training request detail. Reason: This training request is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "You can't Edit this training request detail. Reason: This training request is already approved/reject or assign")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "Attended Training is already Exist!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 41, "Sorry, Invalid Character not allowed in Attended Training Name!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 42, "Sorry, Enrollment Remark is mandatory information. Please enter Enrollment Remark to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 43, "Sorry, No Vacant Seat is available for Selected Training")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 44, "Information saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 45, "Training is compulsory information.Please Check atleast One Training.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 46, "Please Check Only One Training.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 47, "Sorry, You can't Enroll for this training before submitting Pre-Training Evaluation Form for this training.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 48, "Sorry, Remark is mandatory information. Please add remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 49, "Are you sure you want to reject this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 50, "Are you sure you want to approve this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 51, "You have not indicated training cost amount, are you sure you want to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 52, "Are you sure you want to Confirm for Enrollment this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 53, "Are you sure you want to Reject for Enrollment this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 54, "Are you sure you want to Update Completed Status for this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 55, "Are you sure you want to reject this Completed Training?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 56, "Are you sure you want to approve this Completed Training?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 57, "File does not Exist...")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 58, "Training completion status approved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 59, "Training completion status rejected successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class