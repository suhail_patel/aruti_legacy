﻿#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region

Partial Class Training_Training_Request_Approval_wPg_TrainingRequestApprovalList
    Inherits Basepage

#Region " Private Variable "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTrainingRequestApprovalList"
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillCombo()
                SetApprovalData()
                FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objApprovalMaster As New clstraining_requisition_approval_master
        Dim dsCombo As New DataSet
        'Hemant (03 Dec 2021) -- Start
        'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
        Dim objTPeriod As New clsTraining_Calendar_Master
        'Hemant (03 Dec 2021) -- End
        Try
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.            
            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            'Hemant (03 Dec 2021) -- End

            dsCombo = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

            With drpTraining
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", True)
            With drpemp
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objApprovalMaster.getStatusComboList("List", True)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

            dsCombo = objApprovalMaster.getCompletionStatusComboList("List", True)
            With cboCompletionStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            objTPeriod = Nothing
            'Hemant (03 Dec 2021) -- End
            objEmp = Nothing
            objApprovalMaster = Nothing
        End Try
    End Sub

    Private Sub SetApprovalData()
        Dim objApprover As New clstraining_approver_master
        Dim dsList As New DataSet
        Try
            dsList = objApprover.GetList("List", True, "", , Nothing, False, CInt(Session("RoleId")))
            If dsList.Tables("List").Rows.Count > 0 Then
                txtRole.Text = dsList.Tables("List").Rows(0).Item("role").ToString()
                txtRole.Attributes.Add("mappingunkid", dsList.Tables("List").Rows(0).Item("mappingunkid").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprover = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub FillList()
        Dim objApprovaltran As New clstrainingapproval_process_tran
        Dim dtApprovalList As New DataTable
        Dim mstrEmployeeIDs As String = ""
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            If CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If
            'Hemant (03 Dec 2021) -- End

            If CInt(drpemp.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.employeeunkid = " & CInt(drpemp.SelectedValue) & " "
            End If

            If CInt(drpTraining.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.coursemasterunkid = " & CInt(drpTraining.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If CInt(cboCompletionStatus.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.completed_statusunkid = " & CInt(cboCompletionStatus.SelectedValue) & " "
            End If

            strSearch &= "AND trtrainingapproval_process_tran.visibleid <> -1 " & " "

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objApprovaltran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                             "List", strSearch, mstrAdvanceFilter)

            Dim mintTrainingRequestunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""
            Dim mstrCompletedStatus As String = ""

            If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each drRow As DataRow In dsList.Tables(0).Rows

                    If CInt(drRow("pendingtrainingtranunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintTrainingRequestunkid <> CInt(drRow("trainingrequestunkid")) Then
                        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND trainingrequestunkid = " & CInt(drRow("trainingrequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintTrainingRequestunkid = CInt(drRow("trainingrequestunkid"))
                        mstrStaus = ""
                        mstrCompletedStatus = ""
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 1 Then

                                    If CInt(dr(i)("statusunkid")) = 2 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                End If

                            Next
                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("completed_statusunkid")) = 2 Then
                                    mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(drRow("completed_statusunkid")) = 1 Then

                                    If CInt(dr(i)("completed_statusunkid")) = 2 Then
                                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("completed_statusunkid")) = 3 Then
                                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("completed_statusunkid")) = 3 Then
                                    mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If
                    If mstrCompletedStatus <> "" Then
                        drRow("completed_status") = mstrCompletedStatus.Trim
                    End If

                Next

            End If

            dtApprovalList = dsList.Tables(0).Clone

            dtApprovalList.Columns("application_date").DataType = GetType(Object)

            Dim dtRow As DataRow

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dtRow = dtApprovalList.NewRow
                For Each dtCol As DataColumn In dtApprovalList.Columns
                    dtRow(dtCol.ColumnName) = dRow(dtCol.ColumnName)
                Next

                'If CBool(dRow("Isgrp")) = True Then
                '    dtRow("application_date") = dRow("application_no")
                'End If
                dtApprovalList.Rows.Add(dtRow)
            Next

            dtApprovalList.AcceptChanges()

            Dim dtTable As New DataTable
            If chkMyApproval.Checked Then
                dtTable = New DataView(dtApprovalList, "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dtApprovalList, "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-451 - Training Completion Screen - Forces user to attach Doc even when Cert not required.
            'Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "trainingrequestunkid", "employeeunkid")

            'Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("trainingrequestunkid") Equals drTemp.Field(Of Integer)("trainingrequestunkid") And _
            '             drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable

            'If dRow1.Count > 0 Then
            '    dRow1.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            'End If

            'gvTrainingApprovalList.Columns(0).Visible = CBool(Session("AllowToChangeLoanStatus"))
            Dim xRow() As DataRow = Nothing
            Dim dt As New DataTable
            Dim xTable As New DataTable
            Dim dtFinalTable As New DataTable
            dt = New DataView(dtTable, "mapuserunkid = " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable
            Dim dtTemp As DataTable = New DataView(dt, "", "", DataViewRowState.CurrentRows).ToTable(True, "trainingrequestunkid", "employeeunkid")
            For Each drRow As DataRow In dtTemp.Rows
                xRow = dtTable.Select("trainingrequestunkid =" & drRow.Item("trainingrequestunkid"))
                If xRow.Length > 0 Then
                    xTable = xRow.CopyToDataTable()
                    dtFinalTable.Merge(xTable, True)
                End If
            Next
            dt = Nothing
            xTable = Nothing
            'Hemant (20 Aug 2021) -- End

            gvTrainingApprovalList.AutoGenerateColumns = False
            gvTrainingApprovalList.DataSource = dtFinalTable
            gvTrainingApprovalList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkChangeStatus_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objApprovaltran As New clstrainingapproval_process_tran
        Dim objTrainingApprover As New clstraining_approver_master
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("uempid")) = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")) AndAlso CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("ecompid")) = CInt(Session("CompanyUnkId")) Then
                DisplayMessage.DisplayMessage("Sorry, you cannot approve/reject your own training requisition. Please contact aruti administrator to make new approver with same level.", Me)
            End If

            If CInt(Session("UserId")) <> CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, this training request is a pending task for a different approver and cannot be edited from your account"), Me)
                Exit Sub
            End If

            objTrainingApprover._Mappingunkid = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("approverunkid"))

            Dim dsList As DataSet
            Dim dtList As DataTable

            Dim objapproverlevel As New clstraining_approverlevel_master
            objapproverlevel._Levelunkid = objTrainingApprover._Levelunkid

            dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                         CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")), _
                                                         CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("trainingrequestunkid")), _
                                                         " trtrainingapproval_process_tran.mapuserunkid <> " & CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                        dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                        If dtList.Rows.Count > 0 Then Continue For

                        If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If

                    ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                        If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.APPROVED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                Exit Sub
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.REJECTED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                Exit Sub
            End If

            Session("mblnIsAddMode") = True
            Session("mblnFromApproval") = True
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            'Session("mintMappingUnkid") = txtRole.Attributes("mappingunkid")
            Session("mintMappingUnkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("approverunkid")
            'Hemant (03 Dec 2021) -- End
            Session("PendingTrainingTranunkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("pendingtrainingtranunkid")
            Session("TrainingRequestunkid") = Convert.ToString(lnk.CommandArgument.ToString())
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
            objTrainingApprover = Nothing
        End Try
    End Sub

    Protected Sub lnkComplete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objApprovaltran As New clstrainingapproval_process_tran
        Dim objTrainingApprover As New clstraining_approver_master
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("uempid")) = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")) AndAlso CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("ecompid")) = CInt(Session("CompanyUnkId")) Then
                DisplayMessage.DisplayMessage("Sorry, you cannot approve/reject your own training requisition. Please contact aruti administrator to make new approver with same level.", Me)
            End If

            If CInt(Session("UserId")) <> CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, this training request is a pending task for a different approver and cannot be edited from your account"), Me)
                Exit Sub
            End If

            objTrainingApprover._Mappingunkid = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("approverunkid"))

            Dim dsList As DataSet
            Dim dtList As DataTable

            Dim objapproverlevel As New clstraining_approverlevel_master
            objapproverlevel._Levelunkid = objTrainingApprover._Levelunkid

            dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                         CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")), _
                                                         CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("trainingrequestunkid")), _
                                                         " trtrainingapproval_process_tran.mapuserunkid <> " & CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                        dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND completed_statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                        If dtList.Rows.Count > 0 Then Continue For

                        If CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 2 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 3 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If

                    ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                        If CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 2 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 3 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                Exit Sub
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("completed_statusunkid")) = enTrainingRequestStatus.REJECTED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                Exit Sub
            End If

            Session("mblnIsAddMode") = True
            Session("mblnFromCompleteMSS") = True
            Session("mintMappingUnkid") = txtRole.Attributes("mappingunkid")
            Session("PendingTrainingTranunkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("pendingtrainingtranunkid")
            Session("TrainingRequestunkid") = Convert.ToString(lnk.CommandArgument.ToString())
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
            objTrainingApprover = Nothing
        End Try
    End Sub

#End Region

#Region "GridView's Event"

    Protected Sub gvTrainingApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingApprovalList.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If CBool(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "employee").ToString
                        e.Row.Cells(2).ColumnSpan = e.Row.Cells.Count - 2
                        e.Row.BackColor = Color.Silver
                        e.Row.ForeColor = Color.Black
                        e.Row.Font.Bold = True

                        For i As Integer = 3 To e.Row.Cells.Count - 2
                            e.Row.Cells(i).Visible = False
                        Next

                        Dim lnkChangeStatus As LinkButton = TryCast(e.Row.FindControl("lnkChangeStatus"), LinkButton)
                        lnkChangeStatus.Visible = False

                        Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                        ImgComplete.Visible = False

                    Else
                        If e.Row.Cells(2).Text.ToString().Trim <> "" AndAlso e.Row.Cells(2).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text).Date.ToShortDateString
                        End If

                        e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                        e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))

                        Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                        If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval AndAlso _
                           CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) > 0 AndAlso _
                           CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_visibleid")) >= 0 Then
                            ImgComplete.Visible = True
                        Else
                            ImgComplete.Visible = False
                        End If

                    End If
                    'If IsDBNull(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item(0)) = False AndAlso _
                    '            CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item(0)) > 0 Then

                    'End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, lblDetialHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRole.ID, lblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllevel.ID, lbllevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblemp.ID, lblemp.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStatus.ID, lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCompletionStatus.ID, lblCompletionStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, chkMyApproval.ID, chkMyApproval.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSearch.ID, btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(2).FooterText, gvTrainingApprovalList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(3).FooterText, gvTrainingApprovalList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(4).FooterText, gvTrainingApprovalList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(5).FooterText, gvTrainingApprovalList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(6).FooterText, gvTrainingApprovalList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(7).FooterText, gvTrainingApprovalList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(8).FooterText, gvTrainingApprovalList.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(9).FooterText, gvTrainingApprovalList.Columns(9).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, lblDetialHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Me.lblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRole.ID, lblRole.Text)
            Me.lbllevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllevel.ID, lbllevel.Text)
            Me.lblemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblemp.ID, lblemp.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStatus.ID, lblStatus.Text)
            Me.lblCompletionStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCompletionStatus.ID, lblCompletionStatus.Text)

            Me.chkMyApproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), chkMyApproval.ID, chkMyApproval.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSearch.ID, btnSearch.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")

            gvTrainingApprovalList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(2).FooterText, gvTrainingApprovalList.Columns(2).HeaderText)
            gvTrainingApprovalList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(3).FooterText, gvTrainingApprovalList.Columns(3).HeaderText)
            gvTrainingApprovalList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(4).FooterText, gvTrainingApprovalList.Columns(4).HeaderText)
            gvTrainingApprovalList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(5).FooterText, gvTrainingApprovalList.Columns(5).HeaderText)
            gvTrainingApprovalList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(6).FooterText, gvTrainingApprovalList.Columns(6).HeaderText)
            gvTrainingApprovalList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(7).FooterText, gvTrainingApprovalList.Columns(7).HeaderText)
            gvTrainingApprovalList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(8).FooterText, gvTrainingApprovalList.Columns(8).HeaderText)
            gvTrainingApprovalList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(9).FooterText, gvTrainingApprovalList.Columns(9).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, this training request is a pending task for a different approver and cannot be edited from your account")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Approved By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Rejected By :-")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
