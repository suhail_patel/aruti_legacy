﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingImpactEvaluation.aspx.vb"
    Inherits="wPg_TrainingImpactEvaluation" Title="Training Impact Evaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <ucDel:DeleteReason ID="popupDelete" runat="server" Title="Are you Sure You Want To delete?:" />
                <ucCfnYesno:Confirmation ID="popupFinalize" runat="server" Message="Are you sure you want to Finally save this Imapct feedback.After this you won't be allowed to EDIT this Impact feedback.Do you want to continue?"
                    Title="Confirmation" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Impact Evaluation" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Training Impact Evaluation Add/Edit"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCourse" runat="server" Text="Course" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpCourse" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtStartDate" runat="server" AutoPostBack="false" Enabled="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLineManager" runat="server" Text="Line Manager / Supervisor" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpLineManager" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtEndDate" runat="server" AutoPostBack="false" Enabled="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <asp:MultiView ID="mvwImpact" runat="server" ActiveViewIndex="0" Visible="false">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:View runat="server" ID="StepA">
                                            <asp:Panel ID="pnlPartA" runat="server" Width="100%">
                                                <div class="card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lbl_PartA" runat="server" Text="Part-A" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblItemA" runat="server" CssClass="form-label"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <h4>
                                                                    <asp:Label ID="LblItemACaption" runat="server" CssClass="form-label"></asp:Label>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="card inner-card">
                                                                    <div class="body">
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="lblItemAKPI" runat="server" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtItemAKPI" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                                    <asp:Panel ID="pnlItemAKPIList" runat="server" ScrollBars="Auto" Width="100%">
                                                                                        <asp:DataGrid ID="GvItemAKPIList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" AllowPaging="false"
                                                                                            HeaderStyle-Font-Bold="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="ImgSelectItemAKPI" runat="server" CommandName="Select" ToolTip="Edit">
                                                                                                    <i class="fas fa-pencil-alt"></i> 
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="ImgDeleteItemAKPI" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                                                    <i class="fas fa-trash text-danger"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn DataField="kra" ReadOnly="True" FooterText="colhItemAKPI" />
                                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="footer">
                                                                        <asp:Button ID="btnAddItemAKPI" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                                        <asp:Button ID="btnEditItemAKPI" runat="server" Text="Edit" CssClass="btn btn-default" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="card inner-card">
                                                                    <div class="body">
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="lblItemAJobCapability" runat="server" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtItemAJobCapability" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                                    <asp:Panel ID="pnlItemAJobCapability" runat="server" ScrollBars="Auto" Width="100%">
                                                                                        <asp:DataGrid ID="gvItemAJobCapability" runat="server" AutoGenerateColumns="False"
                                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%"
                                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="ImgSelectItemAJobCapability" runat="server" CommandName="Select"
                                                                                                                ToolTip="Edit">
                                                                                                    <i class="fas fa-pencil-alt"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="ImgDeleteItemAJobCapability" runat="server" CommandName="Delete"
                                                                                                                ToolTip="Delete">
                                                                                                    <i class="fas fa-trash text-danger"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn DataField="development_area" ReadOnly="True" FooterText="colhItemAJobCapability" />
                                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="footer">
                                                                        <asp:Button ID="btnAddItemAGAP" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                                        <asp:Button ID="btnEditItemAGAP" runat="server" Text="Edit" CssClass="btn btn-default" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblItemB" runat="server" CssClass="form-label"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <h4>
                                                                    <asp:Label ID="LblItemBCaption" runat="server" CssClass="form-label"></asp:Label>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblItemBTrainingObjective" runat="server" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtItemBTrainingObjective" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="card inner-card">
                                                                    <div class="body">
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                                    <asp:Panel ID="pnlItemBKPIList" runat="server" ScrollBars="Auto" Width="100%">
                                                                                        <asp:DataGrid ID="gvItemBKPIList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" AllowPaging="false"
                                                                                            HeaderStyle-Font-Bold="false" ShowFooter="false">
                                                                                            <Columns>
                                                                                                <asp:BoundColumn DataField="kra" ReadOnly="True" FooterText="colhPartAItemBKPI" />
                                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                                    <asp:Panel ID="pnlItemBList" runat="server" ScrollBars="Auto" Width="100%">
                                                                                        <asp:DataGrid ID="gvItemBList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" AllowPaging="false"
                                                                                            HeaderStyle-Font-Bold="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="ImgSelectItemAJobCapability" runat="server" CommandName="Select"
                                                                                                                ToolTip="Edit">
                                                                                                    <i class="fas fa-pencil-alt"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="ImgDeleteItemAJobCapability" runat="server" CommandName="Delete"
                                                                                                                ToolTip="Delete">
                                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn DataField="training_objective" ReadOnly="True" FooterText="colhItemBTrainingObjective" />
                                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                            </Columns>
                                                                                            <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                                                        </asp:DataGrid>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="footer">
                                                                        <asp:Button ID="btnAddItemB" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                                        <asp:Button ID="btnEditItemB" runat="server" Text="Edit" CssClass="btn btn-default" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:View ID="StepB" runat="server">
                                            <asp:Panel ID="pnlPartB" runat="server" Width="100%">
                                                <div class="card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lbl_PartB" runat="server" Text="Part-B" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPartBDesc" runat="server" CssClass="form-label"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <h4>
                                                                    <asp:Label ID="lblPartBJobCapabilities" runat="server" CssClass="form-label"></asp:Label>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNametask" runat="server" CssClass="form-label"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                    <asp:Panel ID="pnlPartBKRA" runat="server" ScrollBars="Auto" Width="100%">
                                                                        <asp:DataGrid ID="GvPartBKRA" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="kra" HeaderText="Key Performance Indicator or Key Results Area for Senior Position"
                                                                                    ReadOnly="True" FooterText="colhPartBKRA" />
                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtPartBNameTask" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:View ID="StepC" runat="server">
                                            <asp:Panel ID="pnlPartC" runat="server" Width="100%">
                                                <div class="card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lbl_PartC" runat="server" Text="Part-C" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPartCDesc" runat="server" CssClass="form-label"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblQuestion" runat="server" Text="Question" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpQuestion" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblAnswer" runat="server" Text="Answer" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpAnswer" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblComments" runat="server" Text="Comment" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="TxtComments" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnAddFeedback" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                            <asp:Button ID="btnEditFeedBack" runat="server" Text="Edit" CssClass="btn btn-default" />
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                    <asp:Panel ID="pnlFeedBackList" runat="server" ScrollBars="Auto" Width="100%">
                                                                        <asp:DataGrid ID="GvFeedBackList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Edit">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="ImgSelectFeedback" runat="server" CommandName="Select" ToolTip="Edit">
                                                                                        <i class="fas fa-pencil-alt"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Delete">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="ImgDeleteFeedback" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="question" HeaderText="Question" ReadOnly="True" FooterText="colhQuestion" />
                                                                                <asp:BoundColumn DataField="answer" HeaderText="Answer" ReadOnly="True" FooterText="colhAnswer" />
                                                                                <asp:BoundColumn DataField="manager_remark" HeaderText="Comment" ReadOnly="True"
                                                                                    FooterText="colhAnswer" />
                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                <asp:BoundColumn DataField="questionunkid" HeaderText="questionunkid" ReadOnly="True"
                                                                                    Visible="false" />
                                                                                <asp:BoundColumn DataField="answerunkid" HeaderText="answerunkid" ReadOnly="True"
                                                                                    Visible="false" />
                                                                            </Columns>
                                                                            <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                    </div>
                                </div>
                            </asp:MultiView>
                            <div class="footer">
                                <asp:Button ID="BtnBack" runat="server" Text="Back" CssClass="btn btn-default" />
                                <asp:Button ID="BtnNext" runat="server" Text="Next" CssClass="btn btn-default" />
                                <asp:Button ID="btnFinalSave" runat="server" Text="Final Save" CssClass="btn btn-default" />
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
