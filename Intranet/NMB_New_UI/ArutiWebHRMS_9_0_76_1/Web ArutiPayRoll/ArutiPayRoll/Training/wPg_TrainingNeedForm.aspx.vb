﻿Option Strict On

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO

Partial Class Training_wPg_TrainingNeedForm
    Inherits Basepage

    Private ReadOnly mstrModuleName As String = "frmTrainingNeedForm"
    Private DisplayMessage As New CommonCodes
    Private mintTrainingneedformunkid As Integer = 0
    Private mintTrainingneedformTranunkid As Integer = 0
    Private mintTrainingneedformcosttranunkid As Integer = 0
    Private mintScanattachtranunkid As Integer = 0
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mdtEmployee As DataTable
    Private mdtOldEmployee As DataTable
    Private TNFSrNo As Integer = 0
    Private TNFCostSrNo As Integer = 0
    Private mstrAdvanceFilter As String = ""
    Private mintBaseCountryId As Integer = 0

    Private Enum colTrainingNeed
        Edit = 0
        Delete = 1
        Cost = 2
        Attachment = 3
        TrainingCourseName = 4
        TotalEmployee = 5
        StartDate = 6
        EndDate = 7
        PriorityName = 8
    End Enum

    Private Enum colTrainingCost
        Edit = 0
        Delete = 1
        Item = 2
        Quantity = 3
        Price = 4
        Amount = 5
    End Enum

    Private Enum colTrainingAttachment
        Delete = 0
        Download = 1
        FileName = 2
    End Enum

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mdtEmployee", mdtEmployee)
            Me.ViewState.Add("mdtOldEmployee", mdtOldEmployee)
            Me.ViewState.Add("mintTrainingneedformunkid", mintTrainingneedformunkid)
            Me.ViewState.Add("mintTrainingneedformTranunkid", mintTrainingneedformTranunkid)
            Me.ViewState.Add("mintTrainingneedformcosttranunkid", mintTrainingneedformcosttranunkid)
            Me.ViewState.Add("mintScanattachtranunkid", mintScanattachtranunkid)
            Me.ViewState.Add("mdtPeriodStart", mdtPeriodStart)
            Me.ViewState.Add("mdtPeriodEnd", mdtPeriodEnd)
            Me.ViewState.Add("TNFSrNo", TNFSrNo)
            Me.ViewState.Add("TNFCostSrNo", TNFCostSrNo)
            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            Me.ViewState.Add("mintBaseCountryId", mintBaseCountryId)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            If CInt(Session("TrainingNeedFormNoType")) = 1 Then
                txtFormNo.Enabled = False
            Else
                txtFormNo.Enabled = True
            End If

            If Not IsPostBack Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                mintTrainingneedformTranunkid = 0
                btnAdd.Enabled = True
                btnUpdate.Enabled = False

                If Session("trainingneedformunkid") IsNot Nothing Then
                    mintTrainingneedformunkid = CInt(Session("trainingneedformunkid"))
                End If

                Call FillCombo()
                Call FillList()

                Call GetValue()

            Else
                txtPopUpCostUnitAmount.Text = Request.Form(hfPopUpCostUnitAmount.UniqueID)

                mdtEmployee = CType(ViewState("mdtEmployee"), DataTable)
                mdtOldEmployee = CType(ViewState("mdtOldEmployee"), DataTable)
                mintTrainingneedformunkid = CInt(ViewState("mintTrainingneedformunkid"))
                mintTrainingneedformTranunkid = CInt(ViewState("mintTrainingneedformTranunkid"))
                mintTrainingneedformcosttranunkid = CInt(ViewState("mintTrainingneedformcosttranunkid"))
                mintScanattachtranunkid = CInt(ViewState("mintScanattachtranunkid"))
                mdtPeriodStart = CDate(ViewState("mdtPeriodStart"))
                mdtPeriodEnd = CDate(ViewState("mdtPeriodEnd"))
                TNFSrNo = CInt(ViewState("TNFSrNo"))
                TNFCostSrNo = CInt(ViewState("TNFCostSrNo"))
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter"))
                mintBaseCountryId = CInt(ViewState("mintBaseCountryId"))

            End If

            If Request.QueryString("uploadimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Dim objExRate As New clsExchangeRate
        Dim dsCombo As DataSet
        Try

            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "List", True, 0)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs)
            End With

            dsCombo = objMaster.GetPriority(True)
            With cboPriority
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_REQUEST_COST_ITEM, True, "List")
            With cboPopUpCostItem
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objExRate.getComboList("Currency", True)
            With cboPopUpCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_name"
                .DataSource = dsCombo.Tables("Currency")
                .DataBind()
                .SelectedValue = "0"
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillTrainingCourse()
        Dim objCommon As New clsCommon_Master
        Dim objCItems As New clscompeteny_customitem_tran
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try
            If chkIncludeTraining.Checked = True Then
                dsCombo = objCItems.getTrainingCourseComboList(CInt(cboPeriod.SelectedValue), True, Nothing)
                dtTable = New DataView(dsCombo.Tables(0)).ToTable
            Else
                dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    dtTable = New DataView(dsCombo.Tables(0), "masterunkid IN (0) ", "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsCombo.Tables(0)).ToTable
                End If
            End If

            With cboDevRequire
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet = Nothing
        Dim strFilter As String = ""
        Try
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                        CInt(Session("UserId")), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        Session("UserAccessModeSetting").ToString, True, _
                                        True, "Emp", False, strAdvanceFilterQuery:=mstrAdvanceFilter)

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If
            If dsList.Tables(0).Columns.Contains("trainingneedformemptranunkid") = False Then
                Dim dtCol As New DataColumn("trainingneedformemptranunkid", System.Type.GetType("System.Int32"))
                dtCol.DefaultValue = 0
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If
            If dsList.Tables(0).Columns.Contains("AUD") = False Then
                Dim dtCol As New DataColumn("AUD", System.Type.GetType("System.String"))
                dtCol.DefaultValue = ""
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            If mintTrainingneedformTranunkid > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim objTNFEmp As New clsTraining_need_form_emp_tran
                Dim ds As DataSet = objTNFEmp.GetList("List", mintTrainingneedformTranunkid)

                Dim dtCol() As DataColumn = {dsList.Tables(0).Columns("trainingneedformemptranunkid"), dsList.Tables(0).Columns("employeeunkid")}
                dsList.Tables(0).PrimaryKey = dtCol

                Dim dt As DataTable = New DataView(ds.Tables(0)).ToTable(True, "trainingneedformemptranunkid", "employeeunkid")

                For Each r As DataRow In dt.Rows
                    Dim dr() As DataRow = dsList.Tables(0).Select("employeeunkid = " & CInt(r.Item("employeeunkid")) & " ")

                    For Each row As DataRow In dr
                        row.Item("trainingneedformemptranunkid") = r.Item("trainingneedformemptranunkid")
                        row.Item("IsChecked") = True
                    Next
                Next

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Dim blnVisible As Boolean = True
            If dsList.Tables(0).Rows.Count <= 0 Then
                Dim r As DataRow = dsList.Tables(0).NewRow
                dsList.Tables(0).Rows.Add(r)
                blnVisible = False
            End If

            mdtEmployee = New DataView(dsList.Tables(0), "", "IsChecked DESC, employeename", DataViewRowState.CurrentRows).ToTable
            dgvEmployeeList.DataSource = mdtEmployee
            dgvEmployeeList.DataBind()
            dgvEmployeeList.Rows(0).Visible = blnVisible
        End Try
    End Sub

    Private Sub FillItemCostList()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet = Nothing
        Dim strFilter As String = ""
        Try
            If mintTrainingneedformTranunkid <= 0 Then
                strFilter &= " AND 1 = 2 "
            End If

            Dim objTNFCost As New clsTraining_need_form_cost_tran
            dsList = objTNFCost.GetList("List", mintTrainingneedformTranunkid, , strFilter)

            If dsList.Tables(0).Columns.Contains("AUD") = False Then
                Dim dtCol As New DataColumn("AUD", System.Type.GetType("System.String"))
                dtCol.DefaultValue = ""
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Dim blnVisible As Boolean = True
            If dsList.Tables(0).Rows.Count <= 0 Then
                Dim r As DataRow = dsList.Tables(0).NewRow
                dsList.Tables(0).Rows.Add(r)
                blnVisible = False
                txtGrandTotal.Text = ""
            Else
                Dim decTotal As Decimal = (From p In dsList.Tables(0) Select (CDec(p.Item("amount")))).Sum
                txtGrandTotal.Text = Format(decTotal, Session("fmtCurrency").ToString)
            End If

            Dim mdtItemCost As DataTable = New DataView(dsList.Tables(0), "", "trainingcostitemname", DataViewRowState.CurrentRows).ToTable
            dgvItemCost.DataSource = mdtItemCost
            dgvItemCost.DataBind()
            dgvItemCost.Rows(0).Visible = blnVisible
        End Try
    End Sub

    Private Sub FillList()
        Dim objTNFTran As New clsTraining_need_form_tran
        Dim dsList As DataSet = Nothing
        Dim strFilter As String = ""
        Try
            If mintTrainingneedformunkid > 0 Then
                strFilter &= " AND hrtraining_need_form_tran.trainingneedformunkid = " & mintTrainingneedformunkid & " "
            Else
                strFilter &= " AND 1 = 2 "
            End If

            dsList = objTNFTran.GetList("List", 0, strFilter)

            If dsList.Tables(0).Rows.Count > 0 OrElse mintTrainingneedformunkid > 0 Then
                txtFormNo.Enabled = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Dim blnVisible As Boolean = True
            If dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = dsList.Tables(0).NewRow
                dsList.Tables(0).Rows.Add(dr)
                blnVisible = False
            End If
            dgvTrainingNeed.DataSource = dsList.Tables(0)
            dgvTrainingNeed.DataBind()
            dgvTrainingNeed.Rows(0).Visible = blnVisible
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTable As DataTable
        Try
            dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingneedformTranunkid, "")

            dgvAttchment.AutoGenerateColumns = False
            dgvAttchment.DataSource = dtTable
            dgvAttchment.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Sub GetValue()
        Dim objTNFMaster As New clsTraining_need_form_master
        Try
            mdtOldEmployee = Nothing

            objTNFMaster._Trainingneedformunkid = mintTrainingneedformunkid

            txtFormNo.Text = objTNFMaster._Formno
            txtSubmission_remark.Text = objTNFMaster._Submission_remark

            If mintTrainingneedformunkid > 0 Then
                txtFormNo.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtEmployee.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In dgvEmployeeList.Rows
                cb = CType(dgvEmployeeList.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                cb.Checked = blnCheckAll

                mdtEmployee.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(Session("TrainingNeedFormNoType")) = 0 AndAlso txtFormNo.Text.Trim = "" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please enter Form No."), Me)
                txtFormNo.Focus()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please select period."), Me)
                cboPeriod.Focus()
                Return False
            ElseIf dtpDatefrom.GetDate = Nothing Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please enter date from."), Me)
                dtpDatefrom.Focus()
                Return False
            ElseIf dtpDateTo.GetDate = Nothing Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please enter date to."), Me)
                dtpDateTo.Focus()
                Return False
            ElseIf dtpDatefrom.GetDate.Date > dtpDateTo.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, From date should not be greater than to date."), Me)
                dtpDatefrom.Focus()
                Return False
            ElseIf dtpDatefrom.GetDate.Date < mdtPeriodStart Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, From date should not be less than selected period start date."), Me)
                dtpDatefrom.Focus()
                Return False
            ElseIf dtpDateTo.GetDate.Date > mdtPeriodEnd Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, To date should not be greater than selected period end date."), Me)
                dtpDateTo.Focus()
                Return False
            ElseIf CInt(cboDevRequire.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please select developement required."), Me)
                cboDevRequire.Focus()
                Return False
            ElseIf CInt(cboPriority.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Please select priority."), Me)
                cboPriority.Focus()
                Return False
            End If

            If mdtEmployee.Select("IsChecked = 1 ").Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Please select atleast one employee from list."), Me)
                dgvEmployeeList.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub ResetControls()
        Try
            cboDevRequire.SelectedValue = "0"
            cboPriority.SelectedValue = "0"
            txtGrievanceDesc.Text = ""

            mintTrainingneedformTranunkid = 0
            btnAdd.Enabled = True
            btnUpdate.Enabled = False
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetMasterValue() As clsTraining_need_form_master
        Dim objTNFMaster As New clsTraining_need_form_master
        Try
            objTNFMaster._Trainingneedformunkid = mintTrainingneedformunkid
            objTNFMaster._Formno = txtFormNo.Text
            objTNFMaster._Submission_remark = txtSubmission_remark.Text
            objTNFMaster._Statusunkid = enApprovalStatus.PENDING

            If (CInt(Session("loginBy")) = Global.User.en_loginby.Employee) Then
                objTNFMaster._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objTNFMaster._Userunkid = -1
                'objTNFMaster._AuditUserid = CInt(Session("Employeeunkid"))
            Else
                objTNFMaster._Userunkid = CInt(Session("UserId"))
                objTNFMaster._Loginemployeeunkid = -1
                objTNFMaster._AuditUserId = CInt(Session("UserId"))
            End If

            objTNFMaster._Isvoid = False
            objTNFMaster._Voiddatetime = Nothing
            objTNFMaster._Voidreason = ""
            objTNFMaster._Voiduserunkid = -1
            objTNFMaster._Voidloginemployeeunkid = -1


            objTNFMaster._AuditDate = DateTime.Now
            objTNFMaster._ClientIP = CStr(Session("IP_ADD"))
            objTNFMaster._HostName = CStr(Session("HOST_NAME"))
            objTNFMaster._FormName = mstrModuleName
            objTNFMaster._Isweb = True
            objTNFMaster._CompanyUnkid = CInt(Session("CompanyUnkId"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return objTNFMaster
    End Function

#Region " Cost Item Events "

    Private Function IsValidateCost() As Boolean
        Try
            Dim decQty, decRate, decAmt As Decimal
            Decimal.TryParse(txtPopUpCostQty.Text, decQty)
            Decimal.TryParse(txtPopUpCostRate.Text, decRate)
            Decimal.TryParse(txtPopUpCostUnitAmount.Text, decAmt)

            If CInt(cboPopUpCostItem.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Please select cost item."), Me)
                cboPopUpCostItem.Focus()
                Return False
            ElseIf decQty <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Please enter quantity."), Me)
                txtPopUpCostQty.Focus()
                Return False
            ElseIf decRate <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Please enter unit price."), Me)
                txtPopUpCostRate.Focus()
                Return False
            ElseIf CInt(cboPopUpCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select currency."), Me)
                cboPopUpCurrency.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtPopUpCostRate.Text.Trim.Length > 0, txtPopUpCostRate.Text, 0)) * CDec(IIf(txtPopUpCostQty.Text.Trim.Length > 0, txtPopUpCostQty.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetCostControls()
        Try
            cboPopUpCostItem.SelectedValue = "0"
            txtPopUpCostQty.Text = ""
            txtPopUpCostRate.Text = ""
            txtPopUpCostUnitAmount.Text = ""
            hfPopUpCostUnitAmount.Value = txtPopUpCostUnitAmount.Text
            txtPopUpCostDescription.Text = ""
            'cboPopUpCurrency.SelectedValue = "0"

            mintTrainingneedformcosttranunkid = 0
            btnAddCost.Enabled = True
            btnUpdateCost.Enabled = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Combobox Events "
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
            End If

            Call FillTrainingCourse()
            Call FillEmployeeList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Checkbox Events "
    Protected Sub chkIncludeTraining_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeTraining.CheckedChanged
        Try
            Call FillTrainingCourse()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("trainingneedformunkid") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Training/wPg_TrainingNeedFormList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click, btnUpdate.Click
        Try
            If IsValidate() = False Then Exit Try

            Dim objTNFTran As New clsTraining_need_form_tran

            If CType(sender, Button).ID = btnUpdate.ID Then 'Edit
                objTNFTran._Trainingneedformtranunkid = mintTrainingneedformTranunkid
            Else
                objTNFTran._Trainingneedformtranunkid = -1
            End If

            objTNFTran._Trainingneedformunkid = mintTrainingneedformunkid
            objTNFTran._Periodunkid = CInt(cboPeriod.SelectedValue)
            objTNFTran._Start_Date = dtpDatefrom.GetDate
            objTNFTran._End_Date = dtpDateTo.GetDate
            objTNFTran._Trainingcourseunkid = CInt(cboDevRequire.SelectedValue)
            objTNFTran._Priority = CInt(cboPriority.SelectedValue)
            objTNFTran._Description = txtGrievanceDesc.Text
            objTNFTran._Isperformance_Training = chkIncludeTraining.Checked

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTNFTran._Userunkid = CInt(Session("UserId"))
                objTNFTran._Loginemployeeunkid = 0
                'objTNFTran._AuditUserId = CInt(Session("UserId"))
            Else
                objTNFTran._Userunkid = 0
                objTNFTran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objTNFTran._AuditUserId = CInt(Session("Employeeunkid"))
            End If
            objTNFTran._ClientIP = Session("IP_ADD").ToString()
            objTNFTran._HostName = Session("HOST_NAME").ToString()
            objTNFTran._FormName = mstrModuleName
            objTNFTran._Isweb = True
            objTNFTran._CompanyUnkid = CInt(Session("CompanyUnkId"))

            Dim objTNFMaster As clsTraining_need_form_master = SetMasterValue()

            Dim mdtEmp As DataTable = mdtEmployee.Select("IsChecked = 1 ").CopyToDataTable
            If CType(sender, Button).ID = btnAdd.ID Then 'Add
                For Each r As DataRow In mdtEmp.Rows
                    r.Item("AUD") = "A"
                Next
                mdtEmp.AcceptChanges()

                mdtEmp = New DataView(mdtEmp, "AUD <> '' ", "", DataViewRowState.CurrentRows).ToTable

                objTNFTran._xDataOp = Nothing
                If objTNFTran.Insert(CInt(Session("TrainingNeedFormNoType")), Session("TrainingNeedFormNoPrefix").ToString, objTNFMaster, mdtEmp) = False Then
                    If objTNFTran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTNFTran._Message, Me)
                    End If
                Else
                    mintTrainingneedformunkid = objTNFTran._Trainingneedformunkid
                    Call GetValue()
                End If

            ElseIf CType(sender, Button).ID = btnUpdate.ID Then 'Edit
                Dim mdtOldEmp As DataTable = mdtOldEmployee.Select("IsChecked = 1 ").CopyToDataTable

                Dim dtCol() As DataColumn = {mdtEmp.Columns("trainingneedformtranunkid"), mdtEmp.Columns("employeeunkid")}
                mdtEmp.PrimaryKey = dtCol

                Dim dr As IEnumerable(Of DataRow) = Nothing
                Dim dtDeleted As DataTable = Nothing
                Dim dtAdded As DataTable = Nothing
                dr = mdtOldEmp.AsEnumerable.Except(mdtEmp.AsEnumerable, DataRowComparer.Default)
                If dr.Count > 0 Then
                    dtDeleted = dr.CopyToDataTable()
                End If
                dr = mdtEmp.AsEnumerable.Except(mdtOldEmp.AsEnumerable, DataRowComparer.Default)
                If dr.Count > 0 Then
                    dtAdded = dr.CopyToDataTable
                End If
                If dtDeleted IsNot Nothing Then
                    For Each r As DataRow In dtDeleted.Rows
                        r.Item("AUD") = "D"
                    Next
                    dtDeleted.AcceptChanges()
                    mdtEmp.Merge(dtDeleted.Copy)
                End If
                If dtAdded IsNot Nothing Then
                    For Each r As DataRow In dtAdded.Rows
                        r.Item("AUD") = "A"
                    Next
                    dtAdded.AcceptChanges()
                    mdtEmp.Merge(dtAdded.Copy)
                End If

                mdtEmp = New DataView(mdtEmp, "AUD <> '' ", "", DataViewRowState.CurrentRows).ToTable

                objTNFTran._xDataOp = Nothing
                If objTNFTran.Update(objTNFMaster, mdtEmp) = False Then
                    If objTNFTran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTNFTran._Message, Me)
                    End If
                Else
                    mintTrainingneedformunkid = objTNFTran._Trainingneedformunkid
                End If
            End If



            Call FillList()
            Call FillEmployeeList()
            Call ResetControls()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mintTrainingneedformTranunkid = 0
            btnAdd.Enabled = True
            btnUpdate.Enabled = False
            mstrAdvanceFilter = ""

            Call FillEmployeeList()
            Call ResetControls()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDeleteTrainNeedForm_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteTrainNeedForm.buttonDelReasonYes_Click
        Dim objTNFTran As New clsTraining_need_form_tran
        Try
            objTNFTran._Trainingneedformtranunkid = mintTrainingneedformTranunkid
            objTNFTran._Isvoid = True
            objTNFTran._Voiduserunkid = CInt(Session("UserId"))
            objTNFTran._Voiddatetime = DateTime.Now
            objTNFTran._Voidreason = popupDeleteTrainNeedForm.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTNFTran._AuditUserId = CInt(Session("UserId"))
            Else
                objTNFTran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objTNFTran._AuditDate = DateTime.Now
            objTNFTran._ClientIP = Session("IP_ADD").ToString()
            objTNFTran._HostName = Session("HOST_NAME").ToString()
            objTNFTran._FormName = mstrModuleName
            objTNFTran._Isweb = True

            If objTNFTran.Delete() = True Then
                Call FillList()
            ElseIf objTNFTran._Message <> "" Then
                DisplayMessage.DisplayMessage(objTNFTran._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            mintTrainingneedformTranunkid = 0
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            'If mdtTrainingNeedFormDocument Is Nothing OrElse mdtTrainingNeedFormDocument.Select("AUD <> 'D'").Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 33, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
            '    Exit Sub
            'End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                Dim objScan As New clsScan_Attach_Documents
                Dim mdtFullAttachment As DataTable = objScan._Datatable

                If objScan.IsExist(enImg_Email_RefId.Training_Module, enScanAttactRefId.TRAINING_NEED_FORM, f.Name, -1, -1, mintTrainingneedformTranunkid) = False Then

                    Dim dRow As DataRow
                    dRow = mdtFullAttachment.NewRow
                    dRow("scanattachtranunkid") = -1
                    dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                    dRow("employeeunkid") = -1
                    dRow("filename") = f.Name
                    dRow("modulerefid") = enImg_Email_RefId.Training_Module
                    dRow("scanattachrefid") = enScanAttactRefId.TRAINING_NEED_FORM
                    dRow("transactionunkid") = mintTrainingneedformTranunkid
                    dRow("orgfilepath") = f.FullName
                    dRow("valuename") = ""
                    dRow("attached_date") = Today.Date
                    dRow("filename") = f.Name
                    dRow("filesize") = f.Length / 1024 'objScan.ConvertFileSize(f.Length)
                    dRow("attached_date") = Today.Date
                    dRow("form_name") = mstrModuleName

                    dRow("AUD") = "A"
                    dRow("GUID") = Guid.NewGuid.ToString

                    dRow("userunkid") = CInt(Session("userid"))

                    Dim xDocumentData As Byte() = IO.File.ReadAllBytes(f.FullName)
                    dRow("file_data") = xDocumentData

                    'Dim blnIsInApproval As Boolean = False : objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                    'If AddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                    '    blnIsInApproval = True
                    'End If
                    'dRow("isinapproval") = blnIsInApproval

                    '*** Save / Upload attachment
                    Dim mdsDoc As DataSet
                    Dim mstrFolderName As String = ""
                    mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                    Dim strFileName As String = ""
                    mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(enScanAttactRefId.TRAINING_NEED_FORM)) Select (p.Item("Name").ToString)).FirstOrDefault
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(f.FullName)

                    If File.Exists(f.FullName) Then
                        Dim strPath As String = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/") & mstrFolderName & "/" & strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(f.FullName, Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))

                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath

                        mdtFullAttachment.Rows.Add(dRow)

                        objScan._Datatable = mdtFullAttachment.Copy
                        If objScan.InsertUpdateDelete_Documents(Nothing) = False Then
                            DisplayMessage.DisplayMessage(objScan._Message, Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Selected file is already exist."), Me)
                    Exit Sub
                End If
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_ScanAttchment.Show()
        End Try
    End Sub


    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTable As DataTable
        Dim strMsg As String = String.Empty
        Try
            dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingneedformTranunkid, "")

            If dtTable.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "No Files to download."), Me)
                Exit Sub
            End If

            If txtFormNo.Text.Trim.Length > 0 Then
                strMsg = DownloadAllDocument("file" & txtFormNo.Text.Replace(" ", "") + ".zip", dtTable, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Cost Item Events "

    Protected Sub btnAddCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCost.Click, btnUpdateCost.Click
        Try
            If IsValidateCost() = False Then Exit Try

            Dim objTNFCost As New clsTraining_need_form_cost_tran

            If CType(sender, Button).ID = btnAddCost.ID Then 'Add
                If objTNFCost.isExistItem(mintTrainingneedformTranunkid, CInt(cboPopUpCostItem.SelectedValue)) = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, Selected item is already exist."), Me)
                    Exit Try
                End If
            ElseIf CType(sender, Button).ID = btnUpdateCost.ID Then 'Update
                If objTNFCost.isExistItem(mintTrainingneedformTranunkid, CInt(cboPopUpCostItem.SelectedValue), mintTrainingneedformcosttranunkid) = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, Selected item is already exist."), Me)
                    Exit Try
                End If
            End If

            Dim decQty, decRate, decAmt As Decimal
            Decimal.TryParse(txtPopUpCostQty.Text, decQty)
            Decimal.TryParse(txtPopUpCostRate.Text, decRate)
            Decimal.TryParse(txtPopUpCostUnitAmount.Text, decAmt)

            If CType(sender, Button).ID = btnUpdateCost.ID Then 'Update
                objTNFCost._Trainingneedformcosttranunkid = mintTrainingneedformcosttranunkid
            End If

            objTNFCost._Trainingneedformtranunkid = mintTrainingneedformTranunkid
            objTNFCost._Trainingcostitemunkid = CInt(cboPopUpCostItem.SelectedValue)
            objTNFCost._Quantity = decQty
            objTNFCost._Unitprice = decRate
            objTNFCost._Amount = decAmt
            objTNFCost._Countryunkid = CInt(cboPopUpCurrency.SelectedValue)
            objTNFCost._Base_Countryunkid = mintBaseCountryId
            objTNFCost._Description = txtPopUpCostDescription.Text

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboPopUpCurrency.SelectedValue), dtpDateTo.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            objTNFCost._Base_Amount = mdecBaseAmount
            objTNFCost._Exchangerateunkid = mintExchangeRateId
            objTNFCost._Exchange_Rate = mdecExchangeRate

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTNFCost._Userunkid = CInt(Session("UserId"))
                objTNFCost._Loginemployeeunkid = 0
                'objTNFCost._AuditUserId = CInt(Session("UserId"))
            Else
                objTNFCost._Userunkid = 0
                objTNFCost._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objTNFCost._AuditUserId = CInt(Session("Employeeunkid"))
            End If
            objTNFCost._ClientIP = Session("IP_ADD").ToString()
            objTNFCost._HostName = Session("HOST_NAME").ToString()
            objTNFCost._FormName = mstrModuleName
            objTNFCost._Isweb = True
            objTNFCost._CompanyUnkid = CInt(Session("CompanyUnkId"))

            If CType(sender, Button).ID = btnAddCost.ID Then 'Add
                objTNFCost._xDataOp = Nothing
                If objTNFCost.Insert() = False Then
                    If objTNFCost._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTNFCost._Message, Me)
                    End If
                End If

            ElseIf CType(sender, Button).ID = btnUpdateCost.ID Then 'Update
                objTNFCost._xDataOp = Nothing
                If objTNFCost.Update() = False Then
                    If objTNFCost._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTNFCost._Message, Me)
                    End If
                End If

            End If

            Call FillItemCostList()
            Call ResetCostControls()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupTrainingCost.Show()
        End Try
    End Sub

    Protected Sub btnCancelCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelCost.Click
        Try
            mintTrainingneedformcosttranunkid = 0
            btnAddCost.Enabled = True
            btnUpdateCost.Enabled = False

            Call FillItemCostList()
            Call ResetCostControls()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupTrainingCost.Show()
        End Try
    End Sub

    Protected Sub popupDeleteTrainNeedCost_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteTrainNeedCost.buttonDelReasonYes_Click
        Dim objTNFCost As New clsTraining_need_form_cost_tran
        Try
            objTNFCost._Trainingneedformcosttranunkid = mintTrainingneedformcosttranunkid
            objTNFCost._Isvoid = True
            objTNFCost._Voiduserunkid = CInt(Session("UserId"))
            objTNFCost._Voiddatetime = DateTime.Now
            objTNFCost._Voidreason = popupDeleteTrainNeedCost.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTNFCost._AuditUserId = CInt(Session("UserId"))
            Else
                objTNFCost._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objTNFCost._AuditDate = DateTime.Now
            objTNFCost._ClientIP = Session("IP_ADD").ToString()
            objTNFCost._HostName = Session("HOST_NAME").ToString()
            objTNFCost._FormName = mstrModuleName
            objTNFCost._Isweb = True

            If objTNFCost.Delete() = True Then
                Call FillItemCostList()
            ElseIf objTNFCost._Message <> "" Then
                DisplayMessage.DisplayMessage(objTNFCost._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Attachment Events "

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            mintTrainingneedformTranunkid = 0
            mintScanattachtranunkid = 0
            popup_ScanAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintTrainingneedformTranunkid > 0 AndAlso mintScanattachtranunkid > 0 Then
                Dim objDocument As New clsScan_Attach_Documents
                If objDocument.Delete(mintScanattachtranunkid.ToString, Nothing) = False Then
                    DisplayMessage.DisplayMessage(objDocument._Message, Me)
                Else
                    Call FillAttachment()
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_ScanAttchment.Show()
        End Try
    End Sub

#End Region

#End Region

#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter._ShowSkill = True
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Datagrid Events "

    Protected Sub dgvTrainingNeed_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvTrainingNeed.RowCommand
        Try
            If e.CommandName = "Change" Then
                TNFSrNo = CInt(e.CommandArgument)

                mintTrainingneedformTranunkid = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingneedformtranunkid").ToString)

                chkIncludeTraining.Checked = CBool(dgvTrainingNeed.DataKeys(TNFSrNo).Item("isperformance_training").ToString)
                cboPeriod.SelectedValue = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("periodunkid").ToString).ToString
                Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs)
                dtpDatefrom.SetDate = CDate(dgvTrainingNeed.Rows(TNFSrNo).Cells(colTrainingNeed.StartDate).Text)
                dtpDateTo.SetDate = CDate(dgvTrainingNeed.Rows(TNFSrNo).Cells(colTrainingNeed.EndDate).Text)
                cboDevRequire.SelectedValue = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingcourseunkid").ToString).ToString
                cboPriority.SelectedValue = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("priority").ToString).ToString
                txtGrievanceDesc.Text = dgvTrainingNeed.DataKeys(TNFSrNo).Item("description").ToString

                btnAdd.Enabled = False
                btnUpdate.Enabled = True

                mdtOldEmployee = mdtEmployee

            ElseIf e.CommandName = "Remove" Then
                TNFSrNo = CInt(e.CommandArgument)

                mintTrainingneedformTranunkid = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingneedformtranunkid").ToString)

                popupDeleteTrainNeedForm.Show()

            ElseIf e.CommandName = "AddCost" Then
                TNFSrNo = CInt(e.CommandArgument)

                mintTrainingneedformTranunkid = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingneedformtranunkid").ToString)

                Call FillItemCostList()
                Call ResetCostControls()

                popupTrainingCost.Show()

            ElseIf e.CommandName = "AddAttachment" Then
                TNFSrNo = CInt(e.CommandArgument)

                mintTrainingneedformTranunkid = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingneedformtranunkid").ToString)

                Call FillAttachment()

                popup_ScanAttchment.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingNeed_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvTrainingNeed.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(CType(e.Row.DataItem, DataRowView).Item(0)) = True Then Exit Sub

                e.Row.Cells(colTrainingNeed.StartDate).Text = CDate(e.Row.Cells(colTrainingNeed.StartDate).Text).ToString("dd-MMM-yyyy")
                e.Row.Cells(colTrainingNeed.EndDate).Text = CDate(e.Row.Cells(colTrainingNeed.EndDate).Text).ToString("dd-MMM-yyyy")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Cost Item Events "

    Protected Sub dgvItemCost_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvItemCost.RowCommand
        Try
            If e.CommandName = "Change" Then
                TNFCostSrNo = CInt(e.CommandArgument)

                mintTrainingneedformcosttranunkid = CInt(dgvItemCost.DataKeys(TNFCostSrNo).Item("trainingneedformcosttranunkid").ToString)

                cboPopUpCostItem.SelectedValue = CInt(dgvItemCost.DataKeys(TNFCostSrNo).Item("trainingcostitemunkid").ToString).ToString
                txtPopUpCostQty.Text = CDec(dgvItemCost.Rows(TNFCostSrNo).Cells(colTrainingCost.Quantity).Text).ToString("############0.00")
                txtPopUpCostRate.Text = CDec(dgvItemCost.Rows(TNFCostSrNo).Cells(colTrainingCost.Price).Text).ToString("############0.00")
                txtPopUpCostUnitAmount.Text = CDec(dgvItemCost.Rows(TNFCostSrNo).Cells(colTrainingCost.Amount).Text).ToString("############0.00")
                hfPopUpCostUnitAmount.Value = txtPopUpCostUnitAmount.Text
                cboPopUpCurrency.SelectedValue = CInt(dgvItemCost.DataKeys(TNFCostSrNo).Item("countryunkid").ToString).ToString
                txtPopUpCostDescription.Text = dgvItemCost.DataKeys(TNFCostSrNo).Item("description").ToString

                btnAddCost.Enabled = False
                btnUpdateCost.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                TNFCostSrNo = CInt(e.CommandArgument)

                mintTrainingneedformcosttranunkid = CInt(dgvItemCost.DataKeys(TNFCostSrNo).Item("trainingneedformcosttranunkid").ToString)

                popupDeleteTrainNeedCost.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupTrainingCost.Show()
        End Try
    End Sub

    Protected Sub dgvItemCost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvItemCost.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(CType(e.Row.DataItem, DataRowView).Item(0)) = True Then Exit Sub

                e.Row.Cells(colTrainingCost.Quantity).Text = CDec(e.Row.Cells(colTrainingCost.Quantity).Text).ToString(Session("fmtCurrency").ToString)
                e.Row.Cells(colTrainingCost.Price).Text = CDec(e.Row.Cells(colTrainingCost.Price).Text).ToString(Session("fmtCurrency").ToString)
                e.Row.Cells(colTrainingCost.Amount).Text = CDec(e.Row.Cells(colTrainingCost.Amount).Text).ToString(Session("fmtCurrency").ToString)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Attachment Events "

    Protected Sub dgvAttchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvAttchment.RowCommand
        Try
            Dim SrNo As Integer = CInt(e.CommandArgument)

            mintScanattachtranunkid = CInt(dgvAttchment.DataKeys(SrNo).Item("scanattachtranunkid"))

            If e.CommandName = "Remove" Then
                If mintScanattachtranunkid > 0 Then

                    popup_AttachementYesNo.Show()

                End If

            ElseIf e.CommandName = "Download" Then

                Dim xPath As String = ""

                If mintScanattachtranunkid > 0 Then
                    xPath = dgvAttchment.DataKeys(SrNo).Item("filepath").ToString
                    If xPath.Contains(Session("ArutiSelfServiceURL").ToString) = True Then
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    End If
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        DisplayMessage.DisplayMessage("File does not Exist...", Me)
                        Exit Sub
                    End If
                    fileInfo = Nothing
                    Dim strFile As String = xPath
                    Response.ContentType = "image/jpg/pdf"
                    Response.AddHeader("Content-Disposition", "attachment;filename=""" & dgvAttchment.Rows(SrNo).Cells(colTrainingAttachment.FileName).Text & """")
                    Response.Clear()
                    Response.TransmitFile(strFile)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                mintScanattachtranunkid = 0
                popup_ScanAttchment.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFormNo.ID, Me.lblFormNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkIncludeTraining.ID, Me.chkIncludeTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPeriod.ID, Me.lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDatefrom.ID, Me.lblDatefrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDateTo.ID, Me.lblDateTo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDevRequire.ID, Me.lblDevRequire.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPriority.ID, Me.lblPriority.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDescription.ID, Me.lblDescription.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployeeList.Columns(2).FooterText, dgvEmployeeList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployeeList.Columns(3).FooterText, dgvEmployeeList.Columns(3).HeaderText)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(0).FooterText, dgvTrainingNeed.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(1).FooterText, dgvTrainingNeed.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(2).FooterText, dgvTrainingNeed.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(3).FooterText, dgvTrainingNeed.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(4).FooterText, dgvTrainingNeed.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(5).FooterText, dgvTrainingNeed.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(6).FooterText, dgvTrainingNeed.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(7).FooterText, dgvTrainingNeed.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingNeed.Columns(8).FooterText, dgvTrainingNeed.Columns(8).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAdd.ID, Me.btnAdd.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnUpdate.ID, Me.btnUpdate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCancel.ID, Me.btnCancel.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTraningNeedRequest.ID, Me.lblTraningNeedRequest.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblSubmission_remark.ID, Me.lblSubmission_remark.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSubmit.ID, Me.btnSubmit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCost.ID, Me.lblPopUpCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCostHeading2.ID, Me.lblPopUpCostHeading2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCostItem.ID, Me.lblPopUpCostItem.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCostQty.ID, Me.lblPopUpCostQty.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCostRate.ID, Me.lblPopUpCostRate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCostUnitAmount.ID, Me.lblPopUpCostUnitAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPopUpCostDescription.ID, Me.lblPopUpCostDescription.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAddCost.ID, Me.btnAddCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnUpdateCost.ID, Me.btnUpdateCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCancelCost.ID, Me.btnCancelCost.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvItemCost.Columns(0).FooterText, dgvItemCost.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvItemCost.Columns(1).FooterText, dgvItemCost.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvItemCost.Columns(2).FooterText, dgvItemCost.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvItemCost.Columns(3).FooterText, dgvItemCost.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvItemCost.Columns(4).FooterText, dgvItemCost.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvItemCost.Columns(5).FooterText, dgvItemCost.Columns(5).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCostClose.ID, Me.btnCostClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblScanHeader.ID, Me.lblScanHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblScanDocumentType.ID, Me.lblScanDocumentType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnDownloadAll.ID, Me.btnDownloadAll.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnScanClose.ID, Me.btnScanClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAddFile.ID, Me.btnAddFile.Value)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.popup_AttachementYesNo.ID, Me.popup_AttachementYesNo.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.popupAdvanceFilter.ID, Me.popupAdvanceFilter.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.popupDeleteTrainNeedForm.ID, Me.popupDeleteTrainNeedForm.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.popupDeleteTrainNeedCost.ID, Me.popupDeleteTrainNeedCost.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAttchment.Columns(2).FooterText, dgvAttchment.Columns(2).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            lblDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            lnkAdvanceFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            lblFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFormNo.ID, Me.lblFormNo.Text)
            chkIncludeTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeTraining.ID, Me.chkIncludeTraining.Text)
            lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            lblDatefrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDatefrom.ID, Me.lblDatefrom.Text)
            lblDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDateTo.ID, Me.lblDateTo.Text)
            lblDevRequire.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDevRequire.ID, Me.lblDevRequire.Text)
            lblPriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPriority.ID, Me.lblPriority.Text)
            lblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDescription.ID, Me.lblDescription.Text)

            dgvEmployeeList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployeeList.Columns(2).FooterText, dgvEmployeeList.Columns(2).HeaderText)
            dgvEmployeeList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployeeList.Columns(3).FooterText, dgvEmployeeList.Columns(3).HeaderText)


            dgvTrainingNeed.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(0).FooterText, dgvTrainingNeed.Columns(0).HeaderText)
            dgvTrainingNeed.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(1).FooterText, dgvTrainingNeed.Columns(1).HeaderText)
            dgvTrainingNeed.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(2).FooterText, dgvTrainingNeed.Columns(2).HeaderText)
            dgvTrainingNeed.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(3).FooterText, dgvTrainingNeed.Columns(3).HeaderText)
            dgvTrainingNeed.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(4).FooterText, dgvTrainingNeed.Columns(4).HeaderText)
            dgvTrainingNeed.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(5).FooterText, dgvTrainingNeed.Columns(5).HeaderText)
            dgvTrainingNeed.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(6).FooterText, dgvTrainingNeed.Columns(6).HeaderText)
            dgvTrainingNeed.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(7).FooterText, dgvTrainingNeed.Columns(7).HeaderText)
            dgvTrainingNeed.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingNeed.Columns(8).FooterText, dgvTrainingNeed.Columns(8).HeaderText)

            btnAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            btnUpdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnUpdate.ID, Me.btnUpdate.Text).Replace("&", "")
            btnCancel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCancel.ID, Me.btnCancel.Text).Replace("&", "")

            lblTraningNeedRequest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTraningNeedRequest.ID, Me.lblTraningNeedRequest.Text)
            lblSubmission_remark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSubmission_remark.ID, Me.lblSubmission_remark.Text)

            btnSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            lblPopUpCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCost.ID, Me.lblPopUpCost.Text)
            lblPopUpCostHeading2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCostHeading2.ID, Me.lblPopUpCostHeading2.Text)
            lblPopUpCostItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCostItem.ID, Me.lblPopUpCostItem.Text)
            lblPopUpCostQty.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCostQty.ID, Me.lblPopUpCostQty.Text)
            lblPopUpCostRate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCostRate.ID, Me.lblPopUpCostRate.Text)
            lblPopUpCostUnitAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCostUnitAmount.ID, Me.lblPopUpCostUnitAmount.Text)
            lblPopUpCostDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPopUpCostDescription.ID, Me.lblPopUpCostDescription.Text)

            btnAddCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddCost.ID, Me.btnAddCost.Text).Replace("&", "")
            btnUpdateCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnUpdateCost.ID, Me.btnUpdateCost.Text).Replace("&", "")
            btnCancelCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCancelCost.ID, Me.btnCancelCost.Text).Replace("&", "")

            dgvItemCost.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvItemCost.Columns(0).FooterText, dgvItemCost.Columns(0).HeaderText)
            dgvItemCost.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvItemCost.Columns(1).FooterText, dgvItemCost.Columns(1).HeaderText)
            dgvItemCost.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvItemCost.Columns(2).FooterText, dgvItemCost.Columns(2).HeaderText)
            dgvItemCost.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvItemCost.Columns(3).FooterText, dgvItemCost.Columns(3).HeaderText)
            dgvItemCost.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvItemCost.Columns(4).FooterText, dgvItemCost.Columns(4).HeaderText)
            dgvItemCost.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvItemCost.Columns(5).FooterText, dgvItemCost.Columns(5).HeaderText)

            lblGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            btnCostClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCostClose.ID, Me.btnCostClose.Text).Replace("&", "")

            lblScanHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblScanHeader.ID, Me.lblScanHeader.Text)
            lblScanDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblScanDocumentType.ID, Me.lblScanDocumentType.Text)
            btnSaveAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text).Replace("&", "")
            btnDownloadAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnDownloadAll.ID, Me.btnDownloadAll.Text).Replace("&", "")
            btnScanClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnScanClose.ID, Me.btnScanClose.Text).Replace("&", "")
            btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddFile.ID, Me.btnAddFile.Value).Replace("&", "")
            popup_AttachementYesNo.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.popup_AttachementYesNo.ID, Me.popup_AttachementYesNo.Title)
            popup_AttachementYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Are you sure you want to delete this attachment?")
            popupAdvanceFilter.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.popupAdvanceFilter.ID, Me.popupAdvanceFilter.Title)
            popupDeleteTrainNeedForm.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.popupDeleteTrainNeedForm.ID, Me.popupDeleteTrainNeedForm.Title)
            popupDeleteTrainNeedCost.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.popupDeleteTrainNeedCost.ID, Me.popupDeleteTrainNeedCost.Title)

            dgvAttchment.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAttchment.Columns(2).FooterText, dgvAttchment.Columns(2).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please enter Form No.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please select period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Please enter date from.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Please enter date to.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, From date should not be greater than to date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, From date should not be less than selected period start date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, To date should not be greater than selected period end date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Please select developement required.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Please select priority.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Please select atleast one employee from list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Please select cost item.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Please enter quantity.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Please enter unit price.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Please select currency.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Selected file is already exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "No Files to download.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, Selected item is already exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Are you sure you want to delete this attachment?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
