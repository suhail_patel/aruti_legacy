﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingScheduleView.aspx.vb"
    Inherits="Training_Training_Schedule_View_wPg_TrainingScheduleView" Title="Training Schedule View" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/NumberOnly.ascx" TagName="NumberOnly" TagPrefix="num" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .table-bordered tbody tr td
        {
            vertical-align: top;
        }
    </style>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveCollapse();
            RetriveTab();
            CalcGrandTotal();

            $('input[id*=txtAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemAmtChanged').val('1');
            });
            $('input[id*=txtApprovedAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemApprovedAmtChanged').val('1');
            });

            $('.filtercontent').on('click', function() {
                $("[id*=hfFilterContentHidden]").val($('#filtercontent').is(':hidden'));
            });

            //            $('a[id*=txtAmount_lnkup]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });

            //            $('a[id*=txtAmount_lnkdown]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });
            $('#columnlist a').on('click', function(event) {
                $(this).parent().toggleClass('open');
            });

            $('body').on('click', function(e) {
                if (!$('#columnlist').is(e.target)
                && $('#columnlist').has(e.target).length === 0
                && $('.open').has(e.target).length === 0
                ) {
                    $('#columnlist').removeClass('open');
                }
            });
            
            if ($$('chkOtherInstructor').prop('checked') == true)
                $('#txtSearchTInstructor').prop('disabled', true);
            else
                $('#txtSearchTInstructor').prop('disabled', false);
        }
    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="padding: 4px 15px">
                            <div class="row clearfix d--f ai--c">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <h2>
                                        <asp:Label ID="lblPageHeader" runat="server" Text="Planned training"></asp:Label>
                                    </h2>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboPeriodList" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="border-right: 1px solid #ddd">
                                    <div class="row clearfix" style="display: none">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Button ID="btnAddList" runat="server" Text="Add New" CssClass="btn btn-primary m-l-0" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPreScheduled" runat="server" Text="Pre-Scheduled?" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="drpPreScheduledList" runat="server"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmployeeList" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboEmployeeList" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDepartmentList" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboDepartmentList" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblStartDateList" Text="Start Date" runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpStartDateList" runat="server" AutoPostBack="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEndDateList" Text="End Date" runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEndDateList" runat="server" AutoPostBack="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblTargetedGroupList" Text="Target Group" runat="server" CssClass="form-label" />
                                                <asp:DropDownList ID="cboTargetedGroupList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="objlblEmployeeNameList" Text="Employee Name" runat="server" CssClass="form-label" />
                                                <asp:DropDownList ID="cboEmployeeNameList" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" style="display: none;">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="LblTrainingPeriod" Text="Training Period" runat="server" CssClass="form-label" />
                                                <asp:DropDownList ID="drpTrainingPeriod" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblTrainingName" Text="Training Needed/Development Required" runat="server"
                                                    CssClass="form-label" />
                                                <asp:DropDownList ID="cboTrainingName" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblTrainingCategoryList" Text="Training Need Category" runat="server"
                                                    CssClass="form-label" />
                                                <asp:DropDownList ID="cboTrainingCategoryList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblPriorityList" Text="Training Need Priority" runat="server" CssClass="form-label" />
                                                <asp:DropDownList ID="cboPriorityList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblTotalCostList" Text="Total Cost" runat="server" CssClass="form-label" />
                                            <div class="row clearfix">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 p-r-0">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboConditionList" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 ">
                                                    <div class="form-group">
                                                        <nut:NumericText ID="txtTotalCostList" runat="server" Type="Point" Style="text-align: right"
                                                            CssClass="form-control" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblStatusList" Text="Status" runat="server" CssClass="form-label" />
                                                <asp:DropDownList ID="cboStatusList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblRefnoList" Text="Ref. No." runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtRefnoList" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="table-responsive">
                                        <asp:GridView ID="dgvScheduledTraning" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-vertical-center table-hover" AllowPaging="false" DataKeyNames="departmentaltrainingneedunkid, targetedgroupunkid, startdate, enddate, statusunkid, request_statusunkid">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblDay" runat="server" CssClass="schedule-day"></asp:Label>
                                                        <asp:HiddenField ID="hfstartdate" runat="server" Value='<%# Eval("startdate") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblMonthDay" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" FooterText="btnTrainingRequest">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                            <asp:LinkButton ID="lnkTrainingRequest" runat="server" ToolTip="Training Request"
                                                                OnClick="lnkTrainingRequest_Click">
                                                                        <i class="fas fa-edit"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Training Name" FooterText="colhTrainingName">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkTrainingCategory" runat="server" ToolTip="View More" CssClass="show-dot"
                                                            Text='<%# Eval("trainingcoursename") %>' CommandArgument="<%# Container.DataItemIndex %>"
                                                            CommandName="Change">LinkButton</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Targeted Group" FooterText="colhTargetedGroup">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblTargetedGroup" runat="server" Text='<%# Eval("allocationtranname") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Learning Method" FooterText="colhLearningMethod">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblLearningMethod" runat="server" Text='<%# Eval("learningmethodname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="25%" FooterText="colhStatus" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblStatus" runat="server" Text='<%# Eval("statusname") %>' CssClass="label label-primary"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnCloseList" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
            <cc1:ModalPopupExtender ID="popAddEdit" BackgroundCssClass="modal-backdrop" TargetControlID="hfAddEdit"
                runat="server" CancelControlID="hfAddEdit" PopupControlID="pnlAddEdit">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddEdit" runat="server" CssClass="card modal-dialog modal-xl" Style="display: none;">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblAddEditDepartmentalTrainingNeed" Text="Add / Edit Departmental Training Need"
                                    runat="server" />
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="body" style="height: 77vh">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#TrainingInfo" data-toggle="tab">
                                        <asp:Label ID="lblTrainingInfo" runat="server" Text="Training Info"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TargetGroup" data-toggle="tab">
                                        <asp:Label ID="lblTargetGroupInfo" runat="server" Text="Target Group Info"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingResources" data-toggle="tab">
                                        <asp:Label ID="lblTrainingResources" runat="server" Text="Training Resources"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#FinancingSource" data-toggle="tab">
                                        <asp:Label ID="lblFinancingSource" runat="server" Text="Financing Source"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingCoordinator" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCoordinator" runat="server" Text="Training Coordinator"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingCostItem" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCostItem" runat="server" Text="Training Cost Item"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingInstructor" data-toggle="tab">
                                        <asp:Label ID="lblTrainingInstructor" runat="server" Text="Training Instructor"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="TrainingInfo">
                                        <asp:Panel ID="pnlTrainingInfo" runat="server">
                                        <div class="row clearfix d--f" style="align-items: flex-end">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPeriod" Text="Training Period" runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="row margin-0 padding-0">
                                                    <div class="col-md-8 margin-0 padding-0">
                                                        <asp:Label ID="lblCompetences" Text="Competences/Skills/Knowledge Need/Area of Development"
                                                            runat="server" CssClass="form-label" />
                                                    </div>
                                                    <div class="col-md-4 margin-0 padding-0" style="text-align: right">
                                                        <asp:CheckBox ID="chkOtherCompetences" runat="server" Text="Others" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row margin-0 padding-0">
                                                    <div class="col-md-12 margin-0 padding-0">
                                                        <asp:Panel ID="pnlCompetence" runat="server">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboCompetences" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlOtherCompetence" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtOtherCompetence" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="row margin-0 padding-0">
                                                    <div class="col-md-8 margin-0 padding-0">
                                                        <asp:Label ID="lblTrainingNeeded" Text="Training Needed / Development Required" runat="server"
                                                            CssClass="form-label" />
                                                    </div>
                                                    <div class="col-md-4 margin-0 padding-0" style="text-align: right">
                                                        <asp:CheckBox ID="chkOtherTrainingCource" runat="server" Text="Others" AutoPostBack="true"
                                                            Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row margin-0 padding-0">
                                                    <div class="col-md-12 margin-0 padding-0">
                                                        <asp:Panel ID="pnlTCourse" runat="server">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboTrainingNeeded" runat="server" AutoPostBack="false">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlOtherTCourse" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtOtherTCourse" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblTrainingCategory" Text="Training Need Category" runat="server"
                                                    CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboTrainingCategory" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPriority" runat="server" Text="Training Need Priority" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPriority" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblLearningMethod" Text="Learning Method" runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboLearningMethod" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblStartDate" Text="Start Date" runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <uc2:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEndDate" Text="End Date" runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <uc2:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblTrainingProvider" Text="Training Provider" runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboTrainingProvider" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblTrainingVenue" Text="Training Venue" runat="server" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboTrainingVenue" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblRequestStatusList" Text="Training Status" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboRequestStatusList" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-35">
                                                <asp:CheckBox ID="chkCertiRequired" runat="server" Text="Training Certificate Required" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblCommentRemark" runat="server" Text="Comments/Remarks" CssClass="form-label" />
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtCommentRemark" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                            Rows="3"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--fe">
                                                <i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TargetGroup');">
                                                </i>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TargetGroup">
                                        <asp:Panel ID="pnlTargetGroup" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card m-b-0">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTargetedGroup" Text="Targeted Group" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTargetedGroup" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNoOfStaff" Text="Number of Staff(s)" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtNoOfStaff" runat="server" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-35">
                                                                <asp:CheckBox ID="chkChooseEmployee" runat="server" Text="Choose Employee(s)" CssClass="chk-sm"
                                                                    AutoPostBack="true" Visible="false" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                        <div class="body p-t-0">
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <div class="form-line">
                                                                                            <input type="text" id="txtSearchEmp" name="txtSearchEmp" placeholder="Search Targeted Group"
                                                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchEmp', '<%= dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'ChkgvSelectTargetedGroup');" />
                                                                                            <%--<asp:TextBox id="txtSearchEmp" runat="server" placeholder="Search" MaxLength="50" CssClass="form-control" onkeyup="FromSearching();" ></asp:TextBox>--%>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                                    <asp:CheckBox ID="chkEmpOnlyTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                                        CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchEmp', '<% dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'ChkgvSelectTargetedGroup');" />
                                                                                        <%--<asp:LinkButton runat="server" ID="lnkAddEmployee" CssClass="m-r-10" ToolTip="Add Targeted Group"
                                                                                        Visible="false">
                                                                                      <i class="fas fa-plus-circle" ></i>
                                                                                        </asp:LinkButton>--%>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 34vh;">
                                                                                        <asp:GridView ID="dgvEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                                            DataKeyNames="id" CssClass="table table-hover table-bordered">
                                                                                            <Columns>
                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-CssClass="align-center">
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:CheckBox ID="ChkAllTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                                            AutoPostBack="true" OnCheckedChanged="ChkAllTargetedGroup_CheckedChanged" />
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="ChkgvSelectTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                                            Checked='<%# Eval("IsChecked")%>' AutoPostBack="true" OnCheckedChanged="ChkgvSelectTargetedGroup_CheckedChanged" />
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle />
                                                                                                    <ItemStyle />
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <HeaderTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="lnkDeleteAllEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                CommandName="RemoveAll">
                                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </HeaderTemplate>
                                                                                                    <%--<ItemTemplate>
                                                                                                        <asp:LinkButton ID="DeleteEmp" runat="server" ToolTip="Remove" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" >
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>--%>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <asp:Panel ID="pnlAllocEmp" runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                        <div class="body p-t-0">
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <div class="form-line">
                                                                                            <input type="text" id="txtSearchAllocEmp" name="txtSearchAllocEmp" placeholder="Search Employee"
                                                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchAllocEmp', '<%= dgvAllocEmp.ClientID %>');" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                                    <asp:CheckBox ID="chkAllocEmpOnlyTicked" runat="server" Text="Only Ticked Items"
                                                                                        ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchAllocEmp', '<% dgvAllocEmp.ClientID %>', 'chkAllocEmpOnlyTicked', 'ChkgvSelectAllocEmp');" />
                                                                                        <%--<asp:LinkButton runat="server" ID="lnkAddAllocEmp" CssClass="m-r-10" ToolTip="Add Employees"
                                                                                        Visible="false">
                                                                                      <i class="fas fa-plus-circle" ></i>
                                                                                        </asp:LinkButton>--%>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 34vh;">
                                                                                        <asp:GridView ID="dgvAllocEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                                            DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                                            <Columns>
                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-CssClass="align-center">
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:CheckBox ID="ChkAllAllocEmp" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAllocEmp');" />
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="ChkgvSelectAllocEmp" runat="server" Text=" " CssClass="chk-sm"
                                                                                                            Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this, 'ChkgvSelectAllocEmp', 'ChkAllAllocEmp')" />
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle />
                                                                                                    <ItemStyle />
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                                <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <HeaderTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="lnkDeleteAllAllocEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                CommandName="RemoveAll">
                                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </HeaderTemplate>
                                                                                                    <%--<ItemTemplate>
                                                                                                        <asp:LinkButton ID="DeleteEmp" runat="server" ToolTip="Remove" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" >
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>--%>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingInfo');">
                                                </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingResources');">
                                                </i>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingResources">
                                        <asp:Panel ID="pnlTrainingResources" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card m-b-0">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="text" id="txtSearchTrainingResources" name="txtSearchTrainingResources"
                                                                                placeholder="Search Training Resources" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTrainingResources', '<%= dgvTrainingResources.ClientID %>', 'chkTResourcesOnlyTicked', 'ChkgvSelectTResources');" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkTResourcesOnlyTicked" runat="server" Text="Only Ticked Items"
                                                                        ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTrainingResources', '<% dgvTrainingResources.ClientID %>', 'chkTResourcesOnlyTicked', 'ChkgvSelectTResources');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddTrainingResources" CssClass="m-r-10" ToolTip="Add Training Resources"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 49vh;">
                                                                    <asp:GridView ID="dgvTrainingResources" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="masterunkid" CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="ChkAllTResources" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTResources');" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="ChkgvSelectTResources" runat="server" Text=" " CssClass="chk-sm"
                                                                                            Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this, 'ChkgvSelectTResources', 'ChkAllAddTResources')" />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle />
                                                                                <ItemStyle />
                                                                            </asp:TemplateField>
                                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="lnkDeleteAllTResource" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </HeaderTemplate>
                                                                                <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteTResource" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TargetGroup');">
                                                </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('FinancingSource');">
                                                </i>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="FinancingSource">
                                        <asp:Panel ID="pnlFinancingSources" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card m-b-0">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="text" id="txtSearchFinancingSource" name="txtSearchFinancingSource"
                                                                                placeholder="Search Financing Source" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchFinancingSource', '<%= dgvFinancingSource.ClientID %>', 'chkFSourceOnlyTicked', 'ChkgvSelectFSource');" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkFSourceOnlyTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                        CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchFinancingSource', '<% dgvFinancingSource.ClientID %>', 'chkFSourceOnlyTicked', 'ChkgvSelectFSource');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddFinancingSource" CssClass="m-r-10" ToolTip="Add Financing Source"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 49vh;">
                                                                    <asp:GridView ID="dgvFinancingSource" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="masterunkid" CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="ChkAllFSource" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectFSource');" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelectFSource" runat="server" Text=" " CssClass="chk-sm" onclick="ChkSelect(this, 'ChkgvSelectFSource', 'ChkAllFSource')"
                                                                                            Checked='<%# Eval("IsChecked")%>' />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle />
                                                                                <ItemStyle />
                                                                            </asp:TemplateField>
                                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="lnkDeleteAllFSource" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </HeaderTemplate>
                                                                                <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteFSource" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingResources');">
                                                </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCoordinator');">
                                                </i>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingCoordinator">
                                        <asp:Panel ID="pnlTrainingCoordinator" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card m-b-0">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="text" id="txtSearchTCoordinator" name="txtSearchTCoordinator" placeholder="Search Training Coordinator"
                                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTCoordinator', '<%= dgvTrainingCoordinator.ClientID %>', 'chkTCoordinatOnlyTicked', 'ChkgvSelectTCoordinator');" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkTCoordinatOnlyTicked" runat="server" Text="Only Ticked Items"
                                                                        ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTCoordinator', '<% dgvTrainingCoordinator.ClientID %>', 'chkTCoordinatOnlyTicked', 'ChkgvSelectTCoordinator');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddTrainingCoordinator" CssClass="m-r-10" ToolTip="Add Training Coordinator"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 49vh;">
                                                                    <asp:GridView ID="dgvTrainingCoordinator" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="ChkAllTCoordinator" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTCoordinator');" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="ChkgvSelectTCoordinator" runat="server" Text=" " CssClass="chk-sm"
                                                                                            onclick="ChkSelect(this, 'ChkgvSelectTCoordinator', 'ChkAllTCoordinator')" Checked='<%# Eval("IsChecked")%>' />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle />
                                                                                <ItemStyle />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                            <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="lnkDeleteAllTCoordinator" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </HeaderTemplate>
                                                                                <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteTCoordinator" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('FinancingSource');">
                                                </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCostItem');">
                                                </i>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingCostItem">
                                        <asp:Panel ID="pnlTrainingCostItem" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card m-b-0">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="text" id="txtSearchTrainingCostItem" name="txtSearchTrainingCostItem"
                                                                                placeholder="Search Training Cost Item" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTrainingCostItem', '<%= dgvTrainingCostItem.ClientID %>', 'chkTCostItemTicked', 'ChkgvSelectTCostItem');" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkTCostItemTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                        CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTrainingCostItem', '<% dgvTrainingCostItem.ClientID %>', 'chkTCostItemTicked', 'ChkgvSelectTCostItem');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddTrainingCostItem" CssClass="m-r-10" ToolTip="Add Training Cost Item"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 44vh;">
                                                                    <asp:GridView ID="dgvTrainingCostItem" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="infounkid" CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="ChkAllTCostItem" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTCostItem');" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="ChkgvSelectTCostItem" runat="server" Text=" " CssClass="chk-sm"
                                                                                            onclick="ChkSelect(this, 'ChkgvSelectTCostItem', 'ChkAllTCostItem')" Checked='<%# Eval("IsChecked")%>' />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle />
                                                                                <ItemStyle />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="info_code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                            <asp:BoundField DataField="info_name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                            <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="align-center"
                                                                                    FooterText="colhamount" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="270px"
                                                                                    ItemStyle-Width="270px">
                                                                                <ItemTemplate>
                                                                                        <%--<nut:NumericText ID="txtAmount" runat="server" Type="Point" Style="text-align: right"
                                                                                        HideUpDown="true" CssClass="form-control" Text='<%# Eval("amount") %>'  />--%>
                                                                                        <num:NumberOnly ID="txtAmount" runat="server" Text='<%# Eval("amount") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Approved Amount" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center" FooterText="colhapproved_amount" ItemStyle-HorizontalAlign="Right"
                                                                                    HeaderStyle-Width="270px" ItemStyle-Width="270px">
                                                                                    <ItemTemplate>
                                                                                        <num:NumberOnly ID="txtApprovedAmount" runat="server" Text='<%# Eval("approved_amount") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="lnkDeleteAllTCostItem" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </HeaderTemplate>
                                                                                <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteTCostItem" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="footer" style="padding-right: 60px; display: flex; justify-content: space-between;">
                                                            <asp:CheckBox ID="chkTrainingCostOptional" runat="server" Text="Save without indicating Training Cost" />
                                                        <asp:Label ID="lblCostItemTotal" runat="server" Text="Total Cost : " Font-Bold="true"
                                                            CssClass="align-right"></asp:Label>
                                                        <asp:Label ID="objlblCostItemTotal" runat="server" Text="0.00" Font-Bold="true" CssClass="align-right"></asp:Label>
                                                            <asp:Label ID="lblCostItemApprovedTotal" runat="server" Text="Total Approved Cost : "
                                                                Font-Bold="true" CssClass="align-right"></asp:Label>
                                                            <asp:Label ID="objlblCostItemApprovedTotal" runat="server" Text="0.00" Font-Bold="true"
                                                                CssClass="align-right"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCoordinator');">
                                                    </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingInstructor');"></i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingInstructor">
                                        <asp:Panel ID="pnlTrainingInstructor" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix m-l-0 m-r-0">
                                                                <asp:Panel ID = "pnlInstructor" runat="server" CssClass="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <input type="text" id="txtSearchTInstructor" name="txtSearchTInstructor" placeholder="Search Training Instructor"
                                                                                        maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTInstructor', '<%= dgvAddTrainingInstructor.ClientID %>', 'chkTInstructorOnlyTicked', 'ChkgvSelectTInstructor');" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-t-15 padding-0">
                                                                            <asp:CheckBox ID="chkTInstructorTicked" runat="server" Text="Only Ticked Items"
                                                                                ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTInstructor', '<% dgvAddTrainingInstructor.ClientID %>', 'chkTInstructorOnlyTicked', 'ChkgvSelectTInstructor');" />
                                                                            <%--<asp:LinkButton runat="server" ID="lnkAddTrainingInstructorr" CssClass="m-r-10" ToolTip="Add Training Instructor"
                                                                                Visible="false">
                                                                                  <i class="fas fa-plus-circle" ></i>
                                                                            </asp:LinkButton>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 49vh;">
                                                                                <asp:GridView ID="dgvAddTrainingInstructor" runat="server" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                            HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="ChkAllAddTInstructor" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddTInstructor');" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="ChkgvSelectAddTInstructor" runat="server" Text=" " CssClass="chk-sm"
                                                                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTInstructor', 'ChkAllAddTInstructor')" Checked='<%# Eval("IsChecked")%>' />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle />
                                                                                            <ItemStyle />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                        <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <HeaderTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="lnkDeleteAllAddTInstructor" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="RemoveAll">
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                    </asp:LinkButton>
                                                                                                </span>
                                                                                            </HeaderTemplate>
                                                                                            <%--<ItemTemplate>
                                                                                            <asp:LinkButton ID="DeleteAddTInstructor" runat="server" ToolTip="Remove">
                                                                                                <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>--%>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-15 p-l-20">
                                                                            <asp:CheckBox ID="chkOtherInstructor" runat="server" Text="Other Instructor" CssClass="chk-sm"
                                                                            AutoPostBack="true" />
                                                                            </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <asp:Panel ID = "pnlOtherInstructor" runat="server" CssClass="card inner-card m-b-0" Enabled="false">
                                                                                <div class="body p-t-0">
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersName" runat="server" Text="Name" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersCompany" runat="server" Text="Company" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersCompany" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersDept" runat="server" Text="Department" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersDept" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersJob" runat="server" Text="Job" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersJob" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                            <asp:Label ID="lblOthersEmail" runat="server" Text="Email" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                    <div class="footer">
                                                                        <asp:Button ID="btnAddInstructor" runat="server" Text="Add & Save" CssClass="btn btn-primary" />
                                                                        <asp:Button ID="btnUpdateInstructor" runat="server" Text="Update" CssClass="btn btn-default" />
                                                                        <asp:Button ID="btnResetInstructor" runat="server" Text="Reset" CssClass="btn btn-default" />
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 15vh;">
                                                                                <asp:GridView ID="dgvTrainingInstructor" runat="server" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" DataKeyNames="depttrainingneedtraininginstructortranunkid, employeeunkid" CssClass="table table-hover table-bordered">
                                                                                    <Columns>
                                                                                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                            HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="ChkAllTInstructor" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTInstructor');" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="ChkgvSelectTInstructor" runat="server" Text=" " CssClass="chk-sm"
                                                                                                    onclick="ChkSelect(this, 'ChkgvSelectTInstructor', 'ChkAllTInstructor')" Checked='<%# Eval("IsChecked")%>' />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle />
                                                                                            <ItemStyle />
                                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnEditInstruct" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnDeleteInstruct" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />--%>
                                                                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhInstructName" />
                                                                                        <asp:BoundField DataField="CompanyName" HeaderText="Company" ReadOnly="true" FooterText="dgcolhInstructCompany" />
                                                                                        <asp:BoundField DataField="DeptName" HeaderText="Department" ReadOnly="true" FooterText="dgcolhInstructDepartment" />
                                                                                        <asp:BoundField DataField="JobName" HeaderText="Job" ReadOnly="true" FooterText="dgcolhInstructJob" />
                                                                                        <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="true" FooterText="dgcolhInstructEmail" />
                                                                                        <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <HeaderTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="lnkDeleteAllTInstructor" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="RemoveAll">
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                    </asp:LinkButton>
                                                                                                </span>
                                                                                            </HeaderTemplate>
                                                                                            <%--<ItemTemplate>
                                                                                            <asp:LinkButton ID="DeleteTInstructor" runat="server" ToolTip="Remove">
                                                                                                <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>--%>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCostItem');">
                                                </i>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                    <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" />
                    <asp:HiddenField ID="hfAddEdit" runat="Server" />
                </div>
            </asp:Panel>
            <%--
            <cc1:ModalPopupExtender ID="popupAddEmployee" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddEmployee" runat="server" CancelControlID="hfAddEmployee"
                PopupControlID="pnlAddEmployee">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddEmployee" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupAddEmployeeheader" Text="Add Targeted Group" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddEmp" name="txtSearchAddEmp" placeholder="Search Targeted Group"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddEmp', '<%= dgvAddEmployee.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="id">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="CheckAll(this, 'ChkgvSelectAddTargetedGroup');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTargetedGroup', 'ChkAllAddTargetedGroup')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSaveAddEmployee" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseEmployee" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddEmployee" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddAllocEmp" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddAllocEmp" runat="server" CancelControlID="hfAddAllocEmp"
                PopupControlID="pnlAddAllocEmp">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddAllocEmp" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupAddAllocEmpheader" Text="Add Employee" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddAllocEmp" name="txtSearchAddAllocEmp" placeholder="Search Employee"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddAllocEmp', '<%= dgvAddAllocEmp.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddAllocEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="employeeunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddAllocEmp" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddAllocEmp');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddAllocEmp" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddAllocEmp', 'ChkAllAddAllocEmp')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSaveAddAllocEmp" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseAllocEmp" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddAllocEmp" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddTResources" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTResources" runat="server" CancelControlID="hfAddTResources"
                PopupControlID="pnlAddTResources">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTResources" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddTResources" Text="Add Training Resources" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTResources" name="txtSearchAddTResources" placeholder="Search Training Resources"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTResources', '<%= dgvAddTResources.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTResources" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTResources" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="CheckAll(this, 'ChkgvSelectAddTResources');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTResources" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTResources', 'ChkAllAddTResources')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddTResources" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTResources" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTResources" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupFinancingSource" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddFSource" runat="server" CancelControlID="hfAddFSource"
                PopupControlID="pnlAddFSource">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddFSource" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddFSource" Text="Add Financing Sources" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddFSource" name="txtSearchAddFSource" placeholder="Search Financing Sources"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddFSource', '<%= dgvAddFinancingSource.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddFinancingSource" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddFSource" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddFSource');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddFSource" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddFSource', 'ChkAllAddFSource')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddFSource" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseFSource" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddFSource" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddTrainingCoordinator" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTCoordinator" runat="server" CancelControlID="hfAddTCoordinator"
                PopupControlID="pnlAddTCoordinator">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTCoordinator" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddTCoordinator" Text="Add Training Coordinator" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTCoordinator" name="txtSearchAddTCoordinator"
                                        placeholder="Search Training Coordinator" maxlength="50" class="form-control"
                                        style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTCoordinator', '<%= dgvAddTCoordinator.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTCoordinator" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="employeeunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTCoordinator" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="CheckAll(this, 'ChkgvSelectAddTCoordinator');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTCoordinator" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTCoordinator', 'ChkAllAddTCoordinator')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddTCoordinator" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTCoordinator" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTCoordinator" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupTrainingCostItem" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTCostItem" runat="server" CancelControlID="hfAddTCostItem"
                PopupControlID="pnlAddTCostItem">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTCostItem" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddTCostItem" Text="Add Training Cost Item" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTCostItem" name="txtSearchAddTCostItem" placeholder="Search Training Cost Item"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTCostItem', '<%= dgvAddTrainingCostItem.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTrainingCostItem" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="infounkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTCostItem" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddTCostItem');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTCostItem" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTCostItem', 'ChkAllAddTCostItem')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="info_code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="info_name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddTCostItem" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTCostItem" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTCostItem" runat="Server" />
                </div>
            </asp:Panel>
            --%>
            <asp:HiddenField ID="TabName" runat="server" />
            <asp:HiddenField ID="hfobjlblTotalAmt" runat="server" Value="0.00" />
            <asp:HiddenField ID="hfobjlblTotalApprovedAmt" runat="server" Value="0.00" />
            <asp:HiddenField ID="hfCostItemAmtChanged" runat="server" Value="0" />
            <asp:HiddenField ID="hfCostItemApprovedAmtChanged" runat="server" Value="0" />
            <asp:HiddenField ID="hfCancelActiveDTNeed" runat="server" />
            <asp:HiddenField ID="hfCancelInactiveDTNeed" runat="server" />
            <asp:HiddenField ID="CollapseBudgetSummaryApprovalDepartmentName" runat="server" />
            <asp:HiddenField ID="hfFilterContentHidden" runat="server" />
            <asp:HiddenField ID="hfCancelDeleteInstruct" runat="server" />
            <%--<uc9:DeleteReason ID="popupDeleteEmployee" runat="server" Title="Are you sure you want to delete selected employees or selected allocations and their employees ?"
                ValidationGroup="DeleteEmployee" />
            <uc9:DeleteReason ID="popupDeleteAllocEmp" runat="server" Title="Are you sure you want to delete selected employees ?"
                ValidationGroup="DeleteAllocEmp" />
            <uc9:DeleteReason ID="popupDeleteTResoources" runat="server" Title="Are you sure you want to delete selected training resources ?"
                ValidationGroup="DeleteTResoources" />
            <uc9:DeleteReason ID="popupDeleteFSource" runat="server" Title="Are you sure you want to delete selected finaning sources ?"
                ValidationGroup="DeleteFSource" />
            <uc9:DeleteReason ID="popupDeleteTCoordinator" runat="server" Title="Are you sure you want to delete selected training co-ordinator ?"
                ValidationGroup="DeleteTCoordinator" />
            <uc9:DeleteReason ID="popupDeleteTCostItem" runat="server" Title="Are you sure you want to delete selected training cost items ?"
                ValidationGroup="DeleteTCostItem" />--%>
            <uc9:DeleteReason ID="popupDeleteDTNeed" runat="server" Title="Are you sure you want to delete selected departmental training need ?"
                ValidationGroup="DeleteDTNeed" />
            <uc9:DeleteReason ID="popupActiveDTNeed" runat="server" Title="Are you sure you want to make departmental training need active ?"
                ValidationGroup="ActiveDTNeed" CancelControlName="hfCancelActiveDTNeed" />
            <uc9:DeleteReason ID="popupInactiveDTNeed" runat="server" Title="Are you sure you want to make departmental training need inactive ?"
                ValidationGroup="InactiveDTNeed" CancelControlName="hfCancelInactiveDTNeed" />
            <uc9:DeleteReason ID="popupDeleteInstruct" runat="server" Title="Are you sure you want to delete selected instructor?"
                ValidationGroup="DeleteInstruct" CancelControlName="hfCancelDeleteInstruct" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function() {
            RetriveCollapse();
            RetriveTab();
            CalcTotal();
            //CalcGrandTotal();

            $('input[id*=txtAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemAmtChanged').val('1');
            });
            $('input[id*=txtApprovedAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemApprovedAmtChanged').val('1');
            });

            $('.filtercontent').on('click', function() {
                $("[id*=hfFilterContentHidden]").val($('#filtercontent').is(':hidden'));
            });

            //            $('a[id*=txtAmount_lnkup]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });

            //            $('a[id*=txtAmount_lnkdown]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });

            $('#columnlist a').on('click', function(event) {
                $(this).parent().toggleClass('open');
            });

            $('body').on('click', function(e) {
                if (!$('#columnlist').is(e.target)
                && $('#columnlist').has(e.target).length === 0
                && $('.open').has(e.target).length === 0
                ) {
                    $('#columnlist').removeClass('open');
                }
            });

        });

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }

        function RetriveTab() {

            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "TrainingInfo";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }
        function ShowTab(id) {
            $('.nav-tabs a[href="#' + id + '"]').tab('show');
            $("[id*=TabName]").val(id);
        }



        function CalcTotal() {
            var totalamt = 0;
            var totalapprovedamt = 0;
            $('input[id*=txtAmount]').each(function() {
                if (Number($(this).val().replace(/,/g, '')) != 0 && $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == false)
                    $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").prop('checked', true);

                if ($(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == true) {
                    //totalamt += Number($(this).val().replaceAll(',', ''));
                    totalamt += Number($(this).val().replace(/,/g, ''));
                }
            });

            $('input[id*=txtApprovedAmount]').each(function() {
                if (Number($(this).val().replace(/,/g, '')) != 0 && $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == false)
                    $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").prop('checked', true);

                if ($(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == true) {
                    totalapprovedamt += Number($(this).val().replace(/,/g, ''));
                }
            });

            if (totalamt.toString().indexOf('.') >= 0)
                $$('objlblCostItemTotal').text(totalamt);
            else
                $$('objlblCostItemTotal').text(totalamt.toString().concat('.00'));

            if (totalapprovedamt.toString().indexOf('.') >= 0)
                $$('objlblCostItemApprovedTotal').text(totalapprovedamt);
            else
                $$('objlblCostItemApprovedTotal').text(totalapprovedamt.toString().concat('.00'));

            $$('hfobjlblTotalAmt').val($$('objlblCostItemTotal').text());
            $$('hfobjlblTotalApprovedAmt').val($$('objlblCostItemApprovedTotal').text());
        }


        function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));

            if (chkbox.id.includes('ChkAllTCostItem')) {
                if ($(chkbox).is(':checked') == false) {
                    $("[id*=" + chkgvSelect + "]", grid).closest('tr').find("[id*='txtAmount']").val(0);
                    $("[id*=" + chkgvSelect + "]", grid).closest('tr').find("[id*='txtApprovedAmount']").val(0);
                }
                CalcTotal();
            }
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }

            if (chkbox.id.includes('ChkgvSelectTCostItem')) {
                if ($(chkbox).is(':checked') == false) {
                    $(chkbox).closest('tr').find("[id*='txtAmount']").val(0);
                    $(chkbox).closest('tr').find("[id*='txtApprovedAmount']").val(0);
                }
                CalcTotal();
            }

        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }

            }
            else if ($('#' + txtID).val().length == 0) {
            resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
            $('.norecords').remove();
            }
            $('#' + txtID).focus();
        }

        function CalcGrandTotal() {
            var totalamt = 0;
            var totalapprovedamt = 0;
            $('span[id*=lbltotaltrainingcost]').each(function() {
                if ($(this).closest('tr').find("[id*='chkActive']").is(':checked') == true)
                    totalamt += Number($(this).text());
            });

            $('span[id*=lbltotalapprovedtrainingcost]').each(function() {
                if ($(this).closest('tr').find("[id*='chkActive']").is(':checked') == true)
                    totalapprovedamt += Number($(this).text());
            });

            //            if (totalamt.toString().indexOf('.') >= 0)
            //                $$('objGrandTotalList').text(totalamt);
            //            else
            //                $$('objGrandTotalList').text(totalamt.toString().concat('.00'));

            //            if (totalapprovedamt.toString().indexOf('.') >= 0)
            //                $$('objApprovedGrandTotalList').text(totalapprovedamt);
            //            else
            //                $$('objApprovedGrandTotalList').text(totalapprovedamt.toString().concat('.00'));

        }

        function SelectAllChildren(chk) {

            if ($(chk).closest("tr[style*='background-color:Cyan;']").length > 0) {
                $(chk).closest("tr").nextUntil(function() { return $(this).find("span[id*='lbltrainingcourse']").text() != '' }).each(function() {
                    $(this).find("input[id*=chkSelect]").prop('checked', chk.checked);
                });
            }
            else if ($(chk).closest("tr").prevAll("tr[style*='background-color:Cyan;']").length > 0) {
                var chkParent = $(chk).closest("tr").prevAll("tr[style*='background-color:Cyan;']")[0];
                $(chkParent).find("input[id*='chkSelect']").prop('checked', chk.checked);
                $(chkParent).closest("tr").nextUntil(function() { return $(this).find("span[id*='lbltrainingcourse']").text() != '' }).each(function() {
                    $(this).find("input[id*=chkSelect]").prop('checked', chk.checked);
                });
            }

            //CalcGrandTotal();
        }

        function RetriveCollapse() {
            var CollapseName = $("[id*=CollapseBudgetSummaryApprovalDepartmentName]").val();
            if (CollapseName != "") {
                $("#" + CollapseName + "").collapse('show');
            }

            var hfFilterContentHidden = $("[id*=hfFilterContentHidden]").val();
            if (hfFilterContentHidden == "true")
                $("#filtercontent").collapse('show');

            $(".BudgetSummaryApprovalDepartmentCollapse").click(function() {
                $("[id*=CollapseBudgetSummaryApprovalDepartmentName]").val($(this).attr("href").replace("#", ""));
            });

        }
    </script>

</asp:Content>
