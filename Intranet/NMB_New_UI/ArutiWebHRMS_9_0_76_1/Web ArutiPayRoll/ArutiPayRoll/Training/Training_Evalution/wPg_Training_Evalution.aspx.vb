﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class Training_Training_Evalution_wPg_Training_Evalution
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName1 As String = "frmTrainingEvalutionCategoryAddEdit"
    Private ReadOnly mstrModuleName2 As String = "frmTrainingEvalutionQuestionnaireAddEdit"
    Private ReadOnly mstrModuleName3 As String = "frmTrainingEvalutionSubQuestionAddEdit"
    'Hemant (04 Sep 2021) -- Start
    'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
    Private ReadOnly mstrModuleName4 As String = "frmTrainingEvalutionOtherSettingsAddEdit"
    'Hemant (04 Sep 2021) -- End

    Private DisplayMessage As New CommonCodes
    Private mintEvalutionCategoryUnkid As Integer = 0
    Private mintQuestionUnkid As Integer = -1
    Private mintSubQuestionUnkid As Integer = -1
    Private mintTrainingEvaluationAnswerunkid As Integer = -1
    Private mstrOptionValue As String = String.Empty
    Private mintAnswerTypeId As Integer = -1

    Private mstrDeleteAction As String = ""
    Private mblnShowQuestionAnswerPopup As Boolean = False
    Private mblnShowSubQuestionAnswerPopup As Boolean = False
    'Hemant (04 Sep 2021) -- Start
    'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
    Private mstrATPreTrainingFeedbackInstruction As String = ""
    Private mstrATPostTrainingFeedbackInstruction As String = ""
    Private mstrATDaysAfterTrainingFeedbackInstruction As String = ""
    'Hemant (04 Sep 2021) -- End

#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillEvalutionCategoryList()

                Call FillQuestionnaireCombo()
                Call FillQuestionList()

                Call FillSubQuestionnaireCombo()
                Call FillSubQuestionList()
                'Hemant (04 Sep 2021) -- Start
                'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
                Call GetOtherSettings()
                'Hemant (04 Sep 2021) -- End
            Else
                mintEvalutionCategoryUnkid = CInt(Me.ViewState("mintEvalutionCategoryUnkid"))
                mintQuestionUnkid = CInt(Me.ViewState("mintQuestionUnkid"))
                mintSubQuestionUnkid = CInt(Me.ViewState("mintSubQuestionUnkid"))
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintTrainingEvaluationAnswerunkid = CInt(ViewState("mintTrainingEvaluationAnswerunkid"))
                mstrOptionValue = CStr(Me.ViewState("mstrOptionValue"))
                mintAnswerTypeId = CInt(Me.ViewState("mintAnswerTypeId"))
                'Hemant (04 Sep 2021) -- Start
                'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
                mstrATPreTrainingFeedbackInstruction = CStr(Me.ViewState("mstrATPreTrainingFeedbackInstruction"))
                mstrATPostTrainingFeedbackInstruction = CStr(Me.ViewState("mstrATPostTrainingFeedbackInstruction"))
                mstrATDaysAfterTrainingFeedbackInstruction = CStr(Me.ViewState("mstrATDaysAfterTrainingFeedbackInstruction"))
                'Hemant (04 Sep 2021) -- End

                If mblnShowQuestionAnswerPopup = True Then
                    popupQuestionAnswer.Show()
                End If

                If mblnShowSubQuestionAnswerPopup = True Then
                    popupSubQuestionAnswer.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintEvalutionCategoryUnkid") = mintEvalutionCategoryUnkid
            Me.ViewState("mintQuestionUnkid") = mintQuestionUnkid
            Me.ViewState("mintSubQuestionUnkid") = mintSubQuestionUnkid
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintTrainingEvaluationAnswerunkid") = mintTrainingEvaluationAnswerunkid
            Me.ViewState("mstrOptionValue") = mstrOptionValue
            Me.ViewState("mintAnswerTypeId") = mintAnswerTypeId
            'Hemant (04 Sep 2021) -- Start
            'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
            Me.ViewState("mstrATPreTrainingFeedbackInstruction") = mstrATPreTrainingFeedbackInstruction
            Me.ViewState("mstrATPostTrainingFeedbackInstruction") = mstrATPostTrainingFeedbackInstruction
            Me.ViewState("mstrATDaysAfterTrainingFeedbackInstruction") = mstrATDaysAfterTrainingFeedbackInstruction
            'Hemant (04 Sep 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Button Event"
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Evalution Category"

#Region " Private Method(s)"

    Private Sub ClearEvalutionCategoriesCtrls()
        Try
            mintEvalutionCategoryUnkid = 0
            rdbECFeedbackModePreTraining.Enabled = True
            rdbECFeedbackModePostTraining.Enabled = True
            rdbECIsFeedbackModeDaysAfterTrainingAttend.Enabled = True
            txtECName.Text = ""
            rdbECFeedbackModePostTraining.Checked = False
            rdbECFeedbackModePreTraining.Checked = False
            rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = False
            pnlECNotifyTrainingDays.Enabled = False
            txtECNotifyTrainingDays.Text = "0"

            chkECFeedbackModeDaysAfterTrainingAttend.Checked = False
            pnlECXXfeedbackdays.Enabled = False
            txtECXXfeedbackdays.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillEvalutionCategoryList()
        Dim objeval_group_master As New clseval_group_master
        Dim dsList As New DataSet
        Try
            dsList = objeval_group_master.GetList("List")
            gvECList.DataSource = dsList.Tables(0)
            gvECList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objeval_group_master = Nothing
        End Try
    End Sub

    Private Function IsValidActionPlanCategories() As Boolean
        Dim objCategory As New clseval_group_master
        Dim dsCategory As New DataSet
        Try
            If txtECName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Please enter category name to continue."), Me)
                Return False
            End If

            If rdbECFeedbackModePostTraining.Checked = False AndAlso rdbECFeedbackModePreTraining.Checked = False AndAlso rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Please select feedback mode to continue."), Me)
                Return False
            End If

            If rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = True AndAlso CInt(txtECXXfeedbackdays.Text) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Please add days after training attended to continue."), Me)
                Return False
            End If

            If rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = True AndAlso CInt(txtECXXfeedbackdays.Text) < 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 9, "Sorry, Days after training attended should be greater than Zero."), Me)
                Return False
            End If

            If chkECFeedbackModeDaysAfterTrainingAttend.Checked = True AndAlso CInt(txtECNotifyTrainingDays.Text) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Please add notify before days for feedback to continue."), Me)
                Return False
            End If

            If chkECFeedbackModeDaysAfterTrainingAttend.Checked = True AndAlso CInt(txtECNotifyTrainingDays.Text) < 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 10, "Sorry, Notify before days for feedback should be greater than Zero."), Me)
                Return False
            End If

            If rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = True Then
                dsCategory = objCategory.GetList("List", , clseval_group_master.enFeedBack.DAYSAFTERTRAINING)
                Dim drDaysAfterTraining() As DataRow = dsCategory.Tables(0).Select("feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & "")
                If drDaysAfterTraining.Length > 0 Then
                    Dim intMinDays As Integer
                    intMinDays = CInt(dsCategory.Tables(0).Compute("MIN(feedbacknodaysaftertrainingattended)", "1=1"))
                    If CInt(txtECXXfeedbackdays.Text) < intMinDays Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, "Days after training attended can not less than defined days for already exist entry of Days after training attended Feedback ."), Me)
                        Return False
                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Function

    Private Sub SetEvalutionCategoriesValue(ByRef objeval_group_master As clseval_group_master)
        Try
            objeval_group_master._Categoryid = mintEvalutionCategoryUnkid
            objeval_group_master._Categoryname = txtECName.Text

            If rdbECFeedbackModePostTraining.Checked Then
                objeval_group_master._Feedbackmode = CInt(clseval_group_master.enFeedBack.POSTTRAINING)
            ElseIf rdbECFeedbackModePreTraining.Checked Then
                objeval_group_master._Feedbackmode = CInt(clseval_group_master.enFeedBack.PRETRAINING)
            ElseIf rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked Then
                objeval_group_master._Feedbackmode = CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING)
                objeval_group_master._FeedbackNodaysAfterTrainingAttended = CInt(txtECXXfeedbackdays.Text)
            End If

            If chkECFeedbackModeDaysAfterTrainingAttend.Checked Then
                objeval_group_master._Isnotifyforfeedback = True
                objeval_group_master._NotifyBeforeNodaysForFeedback = CInt(txtECNotifyTrainingDays.Text)
            Else
                objeval_group_master._Isnotifyforfeedback = False
                objeval_group_master._NotifyBeforeNodaysForFeedback = 0
            End If

            objeval_group_master._AuditUserId = CInt(Session("UserId"))
            objeval_group_master._ClientIP = CStr(Session("IP_ADD"))
            objeval_group_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objeval_group_master._DatabaseName = CStr(Session("Database_Name"))
            objeval_group_master._FormName = mstrModuleName1
            objeval_group_master._FromWeb = True
            objeval_group_master._HostName = CStr(Session("HOST_NAME"))
            objeval_group_master._Isvoid = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetEvalutionCategoriesValue(ByVal intActionPlanCategoryId As Integer)
        Dim objEval_group_master As New clseval_group_master
        Try
            objEval_group_master._Categoryid = intActionPlanCategoryId

            txtECName.Text = objEval_group_master._Categoryname

            If objEval_group_master._Feedbackmode = CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) Then
                rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = True
                rdbECFeedbackModePostTraining.Checked = False
                rdbECFeedbackModePreTraining.Checked = False
                rdbECIsFeedbackModeDaysAfterTrainingAttend_CheckedChanged(rdbECIsFeedbackModeDaysAfterTrainingAttend, Nothing)
                txtECXXfeedbackdays.Text = objEval_group_master._FeedbackNodaysAfterTrainingAttended.ToString()
            ElseIf objEval_group_master._Feedbackmode = CInt(clseval_group_master.enFeedBack.POSTTRAINING) Then
                rdbECFeedbackModePostTraining.Checked = True
                rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = False
                rdbECFeedbackModePreTraining.Checked = False
                rdbECIsFeedbackModeDaysAfterTrainingAttend_CheckedChanged(rdbECFeedbackModePostTraining, Nothing)
            ElseIf objEval_group_master._Feedbackmode = CInt(clseval_group_master.enFeedBack.PRETRAINING) Then
                rdbECFeedbackModePreTraining.Checked = True
                rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked = False
                rdbECFeedbackModePostTraining.Checked = False
                rdbECIsFeedbackModeDaysAfterTrainingAttend_CheckedChanged(rdbECFeedbackModePreTraining, Nothing)
            End If


            If objEval_group_master._Isnotifyforfeedback Then
                chkECFeedbackModeDaysAfterTrainingAttend.Checked = True
                chkECFeedbackModeDaysAfterTrainingAttend_CheckedChanged(chkECFeedbackModeDaysAfterTrainingAttend, Nothing)
                txtECNotifyTrainingDays.Text = objEval_group_master._NotifyBeforeNodaysForFeedback.ToString()
            Else
                chkECFeedbackModeDaysAfterTrainingAttend.Checked = False
                chkECFeedbackModeDaysAfterTrainingAttend_CheckedChanged(chkECFeedbackModeDaysAfterTrainingAttend, Nothing)
            End If


            'txtACategory.Text = CStr(objActionPlanCategory._Category)
            'txtASortOrder.Text = CStr(objActionPlanCategory._SortOrder)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_group_master = Nothing
        End Try
    End Sub
#End Region

#Region " Checkbox and Radio Events "
    Protected Sub chkECFeedbackModeDaysAfterTrainingAttend_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkECFeedbackModeDaysAfterTrainingAttend.CheckedChanged
        Try
            pnlECNotifyTrainingDays.Enabled = chkECFeedbackModeDaysAfterTrainingAttend.Checked

            If chkECFeedbackModeDaysAfterTrainingAttend.Checked = False Then
                txtECNotifyTrainingDays.Text = "0"
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub rdbECIsFeedbackModeDaysAfterTrainingAttend_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbECIsFeedbackModeDaysAfterTrainingAttend.CheckedChanged, _
                                                                                                                                         rdbECFeedbackModePreTraining.CheckedChanged, _
                                                                                                                                         rdbECFeedbackModePostTraining.CheckedChanged
        Try
            Dim rdb As RadioButton = CType(sender, RadioButton)
            If rdb.ID = rdbECIsFeedbackModeDaysAfterTrainingAttend.ID AndAlso rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked Then
                pnlECXXfeedbackdays.Enabled = rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked
                chkECFeedbackModeDaysAfterTrainingAttend.Enabled = True
                pnlECNotifyTrainingDays.Enabled = True
            Else
                pnlECXXfeedbackdays.Enabled = rdbECIsFeedbackModeDaysAfterTrainingAttend.Checked
                txtECXXfeedbackdays.Text = "0"
                chkECFeedbackModeDaysAfterTrainingAttend.Checked = False
                chkECFeedbackModeDaysAfterTrainingAttend.Enabled = False
                pnlECNotifyTrainingDays.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnECSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnECSave.Click
        Dim objeval_group_master As New clseval_group_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidActionPlanCategories() = False Then
                Exit Sub
            End If
            SetEvalutionCategoriesValue(objeval_group_master)
            If mintEvalutionCategoryUnkid > 0 Then
                blnFlag = objeval_group_master.Update()
            Else
                blnFlag = objeval_group_master.Insert()
            End If

            If blnFlag = False AndAlso objeval_group_master._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objeval_group_master._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Evalution category saved successfully."), Me)
                FillEvalutionCategoryList()
                FillQuestionnaireCombo()
                FillSubQuestionnaireCombo()


                ClearEvalutionCategoriesCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objeval_group_master = Nothing
        End Try
    End Sub

    Protected Sub btnECReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnECReset.Click
        Try
            Call ClearEvalutionCategoriesCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "
    Protected Sub lnkECEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintEvalutionCategoryUnkid = CInt(gvECList.DataKeys(row.RowIndex)("categoryid").ToString())
            GetEvalutionCategoriesValue(mintEvalutionCategoryUnkid)
            rdbECFeedbackModePreTraining.Enabled = False
            rdbECFeedbackModePostTraining.Enabled = False
            rdbECIsFeedbackModeDaysAfterTrainingAttend.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkECDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objeval_group_master As New clseval_group_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            mintEvalutionCategoryUnkid = CInt(gvECList.DataKeys(row.RowIndex)("categoryid"))

            If objeval_group_master.isUsed(mintEvalutionCategoryUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 8, "Sorry, this category value is already in used."), Me)
                mintEvalutionCategoryUnkid = 0
                Exit Sub
            End If

            mstrDeleteAction = "delEvalutionCategory"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Are you sure you want to delete this category?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objeval_group_master = Nothing
        End Try
    End Sub

#End Region

#Region "Gridview Events"
    Protected Sub gvECList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvECList.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblIsNotifyFeedbackCheckTrue As Label = CType(e.Row.FindControl("lblIsNotifyFeedbackCheckTrue"), Label)
                Dim lblIsNotifyFeedbackCheckFalse As Label = CType(e.Row.FindControl("lblIsNotifyFeedbackCheckFalse"), Label)

                Dim isnotifyforfeedback As Boolean = CBool(gvECList.DataKeys(e.Row.RowIndex)("isnotifyforfeedback").ToString())

                If isnotifyforfeedback Then
                    lblIsNotifyFeedbackCheckTrue.Visible = True
                Else
                    lblIsNotifyFeedbackCheckFalse.Visible = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
#End Region

#Region "Questionnaire Setup"

#Region " Private Method(s)"
    Private Sub FillQuestionnaireCombo()
        Dim objEval_group_master As New clseval_group_master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As DataSet = Nothing


        Try
            dsCombo = objEval_group_master.GetCategoryComboList("List", True)
            With cboQSCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objMasterData.GetTrainingEvalutionQuestionnaireAnsWerTypeList(True, "AnsTypeList")
            With cboQSAnswerType
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("AnsTypeList")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearQuestionnaireCtrls()
        Try
            mintQuestionUnkid = 0
            cboQSCategory.SelectedIndex = 0
            txtQSCode.Text = ""
            txtQSQuestion.Text = ""
            txtQSDescription.Text = ""
            cboQSAnswerType.SelectedIndex = 0
            cboQSAnswerType_SelectedIndexChanged(Nothing, Nothing)
            chkQSAskForJustification.Checked = False
            chkQSAskForJustification.Visible = False

            dgvQuestionaireAnswer.DataSource = Nothing
            dgvQuestionaireAnswer.DataBind()

            txtOptionValue.Text = ""
            cboQSAnswerType.Enabled = True
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            cboQSCategory_SelectedIndexChanged(Nothing, Nothing)
            chkQSForLineManagerFeedback.Checked = False
            chkQSForLineManagerFeedback.Visible = False
            'Hemant (20 Aug 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillQuestionList()
        Dim objEval_question_master As New clseval_question_master
        Dim dsList As New DataSet
        Try
            dsList = objEval_question_master.GetList("List")
            gvQuestionnaire.DataSource = dsList.Tables(0)
            gvQuestionnaire.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_question_master = Nothing
        End Try
    End Sub

    Private Function IsValidQuestionnaire() As Boolean
        Try
            If cboQSCategory.SelectedValue <= CStr(0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 1, "Please select category to continue."), Me)
                Return False
            End If

            If txtQSCode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 2, "Please enter code to continue."), Me)
                Return False
            End If

            If txtQSQuestion.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 3, "Please enter question to continue."), Me)
                Return False
            End If

            If cboQSAnswerType.SelectedValue <= CStr(0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 4, "Please select answer type to continue."), Me)
                Return False
            End If

            'If txtQSDescription.Text.Trim.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName2, 5, "Please enter description to continue."), Me)
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetQuestionnaireValue(ByRef objEval_question_master As clseval_question_master)
        Try
            objEval_question_master._Questionnaireid = mintQuestionUnkid

            objEval_question_master._Categoryid = CInt(cboQSCategory.SelectedValue)
            objEval_question_master._Code = txtQSCode.Text

            If chkQSAskForJustification.Checked Then
                objEval_question_master._Isaskforjustification = True
            Else
                objEval_question_master._Isaskforjustification = False
            End If
            objEval_question_master._Question = txtQSQuestion.Text
            objEval_question_master._Description = txtQSDescription.Text
            objEval_question_master._Answertype = CInt(cboQSAnswerType.SelectedValue)

            objEval_question_master._AuditUserId = CInt(Session("UserId"))
            objEval_question_master._ClientIP = CStr(Session("IP_ADD"))
            objEval_question_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objEval_question_master._DatabaseName = CStr(Session("Database_Name"))
            objEval_question_master._FormName = mstrModuleName2
            objEval_question_master._FromWeb = True
            objEval_question_master._HostName = CStr(Session("HOST_NAME"))
            objEval_question_master._Isvoid = False
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            If chkQSForLineManagerFeedback.Checked Then
                objEval_question_master._IsForLineManagerFeedback = True
            Else
                objEval_question_master._IsForLineManagerFeedback = False
            End If
            'Hemant (20 Aug 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetQuestionnaireValue(ByVal intQuestionId As Integer)
        Dim objEval_question_master As New clseval_question_master
        Try
            objEval_question_master._Questionnaireid = intQuestionId
            cboQSCategory.SelectedValue = objEval_question_master._Categoryid.ToString()
            txtQSCode.Text = objEval_question_master._Code
            txtQSQuestion.Text = objEval_question_master._Question
            cboQSAnswerType.SelectedValue = objEval_question_master._Answertype.ToString()

            chkQSAskForJustification.Checked = CBool(objEval_question_master._Isaskforjustification)
            cboQSAnswerType_SelectedIndexChanged(Nothing, Nothing)
            txtQSDescription.Text = objEval_question_master._Description
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            chkQSForLineManagerFeedback.Checked = CBool(objEval_question_master._IsForLineManagerFeedback)
            cboQSCategory_SelectedIndexChanged(Nothing, Nothing)
            'Hemant (20 Aug 2021) -- End
            FillQuestionAnswerList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_question_master = Nothing
        End Try
    End Sub

    Private Sub FillQuestionAnswerList()
        Dim objEvalAnswer As New clseval_answer_master
        Dim dsList As DataSet
        Dim mblnblank As Boolean = False
        Try
            dsList = objEvalAnswer.GetList("List", mintQuestionUnkid)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("trainingevaluationanswerunkid") = -1
                drRow("optionvalue") = ""
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            dgvQuestionaireAnswer.DataSource = dsList.Tables(0)
            dgvQuestionaireAnswer.DataBind()

            If mblnblank Then dgvQuestionaireAnswer.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalAnswer = Nothing
        End Try
    End Sub

    Private Sub GetQuestionAnswerValue(ByVal intQuestionAnswerId As Integer)
        Dim objEvalAnswer As New clseval_answer_master
        Try
            objEvalAnswer._TrainingEvaluationAnswerunkid = intQuestionAnswerId
            txtOptionValue.Text = objEvalAnswer._OptionValue
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalAnswer = Nothing
        End Try
    End Sub

    Private Sub SetQuestionAnswerValue(ByRef objEvalAnswer As clseval_answer_master)
        Try
            objEvalAnswer._TrainingEvaluationAnswerunkid = mintTrainingEvaluationAnswerunkid
            objEvalAnswer._OptionValue = mstrOptionValue
            objEvalAnswer._AnswerTypeid = mintAnswerTypeId
            'If mintTrainingEvaluationAnswerunkid <= 0 Then
            '    Dim intOptionId As Integer
            '    objEvalAnswer.GetNextOptionId(intOptionId, mintQuestionUnkid, mintSubQuestionUnkid)
            '    objEvalAnswer._OptionId = intOptionId
            'End If
            objEvalAnswer._Userunkid = CInt(Session("UserId"))
            objEvalAnswer._AuditUserId = CInt(Session("UserId"))

            objEvalAnswer._Isvoid = False
            objEvalAnswer._Voiduserunkid = -1
            objEvalAnswer._Voiddatetime = Nothing
            objEvalAnswer._Voidreason = ""

            objEvalAnswer._IsWeb = True
            objEvalAnswer._ClientIP = CStr(Session("IP_ADD"))
            objEvalAnswer._HostName = CStr(Session("HOST_NAME"))
            objEvalAnswer._FormName = mstrModuleName2

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnQSSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQSSave.Click
        Dim objEval_question_master As New clseval_question_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidQuestionnaire() = False Then
                Exit Sub
            End If

            'If CInt(cboQSAnswerType.SelectedValue) = CInt(clseval_question_master.enAnswerType.SELECTION) OrElse CInt(cboQSAnswerType.SelectedValue) = CInt(clseval_question_master.enAnswerType.RATING) Then
            '    Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            '    gRow = dgvQuestionaireAnswer.Rows.Cast(Of GridViewRow)()

            '    If gRow Is Nothing OrElse gRow.Count <= 1 Then
            '        For Each dgRow As GridViewRow In gRow
            '            Dim intTrainingEvaluationAnswerId As Integer
            '            intTrainingEvaluationAnswerId = CInt(dgvQuestionaireAnswer.DataKeys(dgRow.RowIndex)("trainingevaluationanswerunkid"))
            '            If intTrainingEvaluationAnswerId <= 0 Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName2, 1016, "Option Value is compulsory information.Please add atleast One Option Value."), Me)
            '                Exit Sub
            '            End If
            '        Next
            '    End If

            'End If

            SetQuestionnaireValue(objEval_question_master)
            If mintQuestionUnkid > 0 Then
                blnFlag = objEval_question_master.Update()
            Else
                blnFlag = objEval_question_master.Insert()
            End If

            If blnFlag = False AndAlso objEval_question_master._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objEval_question_master._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 6, "Question saved successfully."), Me)
                FillQuestionList()
                ClearQuestionnaireCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_question_master = Nothing
        End Try
    End Sub

    Protected Sub btnQSReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQSReset.Click
        Try
            Call ClearQuestionnaireCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddQuestionAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddQuestionAnswer.Click
        Dim objEval_question_master As New clseval_question_master
        Dim objEvalAnswer As New clseval_answer_master
        Try
            If txtOptionValue.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 11, "Please enter Option Value to continue."), Me)
                Exit Sub
            End If

            mstrOptionValue = txtOptionValue.Text
            mintAnswerTypeId = CInt(cboQSAnswerType.SelectedValue)
            SetQuestionnaireValue(objEval_question_master)
            SetQuestionAnswerValue(objEvalAnswer)
            objEvalAnswer._QuestionnaireId = mintQuestionUnkid
            If objEvalAnswer.SaveAllQuestionAnswer(objEval_question_master) = False Then
                If objEvalAnswer._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEvalAnswer._Message, Me)
                End If
            Else
                mintQuestionUnkid = objEvalAnswer._QuestionnaireId
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 10, "Question's Answer saved successfully"), Me)
                mintTrainingEvaluationAnswerunkid = -1
                Call FillQuestionAnswerList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseQuestionAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseQuestionAnswer.Click
        Try
            mblnShowQuestionAnswerPopup = False
            popupQuestionAnswer.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkQSEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintQuestionUnkid = CInt(gvQuestionnaire.DataKeys(row.RowIndex)("questionnaireId").ToString())
            GetQuestionnaireValue(mintQuestionUnkid)
            cboQSAnswerType.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkQSDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objEval_question_master As New clseval_question_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            mintQuestionUnkid = CInt(gvQuestionnaire.DataKeys(row.RowIndex)("questionnaireId"))

            If objEval_question_master.isUsed(mintQuestionUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 7, "Sorry, this question is already in used."), Me)
                mintQuestionUnkid = 0
                Exit Sub
            End If

            mstrDeleteAction = "delQuestion"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 8, "Are you sure you want to delete this question?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_question_master = Nothing
        End Try
    End Sub

    Protected Sub lnkQuestionaireAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQuestionaireAnswer.Click
        Try
            If IsValidQuestionnaire() = False Then
                Exit Sub
            End If

            txtOptionValue.Text = ""

            mblnShowQuestionAnswerPopup = True
            popupQuestionAnswer.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEditQuestionaireAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintTrainingEvaluationAnswerunkid = CInt(dgvQuestionaireAnswer.DataKeys(row.RowIndex)("trainingevaluationanswerunkid"))
            GetQuestionAnswerValue(mintTrainingEvaluationAnswerunkid)
            mblnShowQuestionAnswerPopup = True
            popupQuestionAnswer.Show()
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeleteQuestionaireAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objEvalAnswer As New clseval_answer_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            mintTrainingEvaluationAnswerunkid = CInt(dgvQuestionaireAnswer.DataKeys(row.RowIndex)("trainingevaluationanswerunkid"))

            If objEvalAnswer.isUsed(mintTrainingEvaluationAnswerunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 13, "Sorry, this answer is already in used."), Me)
                mintTrainingEvaluationAnswerunkid = 0
                Exit Sub
            End If

            mstrDeleteAction = "delquestionaireanswer"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 14, "Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalAnswer = Nothing
        End Try
    End Sub


#End Region

#Region "Gridview Events"
    Protected Sub gvQuestionnaire_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvQuestionnaire.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblIsAskforJustificationCheckTrue As Label = CType(e.Row.FindControl("lblIsAskforJustificationCheckTrue"), Label)
                Dim lblIsAskforJustificationCheckFalse As Label = CType(e.Row.FindControl("lblIsAskforJustificationCheckFalse"), Label)

                Dim isAskforJustification As Boolean = CBool(gvQuestionnaire.DataKeys(e.Row.RowIndex)("isAskforJustification").ToString())

                If isAskforJustification Then
                    lblIsAskforJustificationCheckTrue.Visible = True
                Else
                    lblIsAskforJustificationCheckFalse.Visible = True
                End If
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                Dim lblIsForLineManagerFeedbackTrue As Label = CType(e.Row.FindControl("lblIsForLineManagerFeedbackTrue"), Label)
                Dim lblIsForLineManagerFeedbackFalse As Label = CType(e.Row.FindControl("lblIsForLineManagerFeedbackFalse"), Label)

                Dim IsForLineManagerFeedback As Boolean = CBool(gvQuestionnaire.DataKeys(e.Row.RowIndex)("isforlinemanager").ToString())

                If IsForLineManagerFeedback Then
                    lblIsForLineManagerFeedbackTrue.Visible = True
                Else
                    lblIsForLineManagerFeedbackFalse.Visible = True
                End If
                'Hemant (20 Aug 2021) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Combobox Event(s)"

    Protected Sub cboQSAnswerType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQSAnswerType.SelectedIndexChanged
        Try
            If CInt(cboQSAnswerType.SelectedValue) = CInt(clseval_question_master.enAnswerType.SELECTION) Then
                chkQSAskForJustification.Visible = True
                pnlQuestionaireAnswer.Visible = True

            ElseIf CInt(cboQSAnswerType.SelectedValue) = CInt(clseval_question_master.enAnswerType.RATING) Then
                chkQSAskForJustification.Visible = True
                pnlQuestionaireAnswer.Visible = True
            Else
                chkQSAskForJustification.Visible = False
                chkQSAskForJustification.Checked = False
                pnlQuestionaireAnswer.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Protected Sub cboQSCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQSCategory.SelectedIndexChanged
        Dim objCategory As New clseval_group_master
        Try
            If CInt(cboQSCategory.SelectedValue) > 0 Then
                objCategory._Categoryid = CInt(cboQSCategory.SelectedValue)
                If CInt(objCategory._Feedbackmode) = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                    chkQSForLineManagerFeedback.Visible = True
                Else
                    chkQSForLineManagerFeedback.Visible = False
                    chkQSForLineManagerFeedback.Checked = False
                End If
            Else
                chkQSForLineManagerFeedback.Visible = False
                chkQSForLineManagerFeedback.Checked = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub
    'Hemant (20 Aug 2021) -- End
#End Region

#End Region

#Region "Sub-Question Setup"

#Region " Private Method(s)"

    Private Sub FillSubQuestionnaireCombo()
        Dim objEval_group_master As New clseval_group_master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As DataSet = Nothing

        Try
            dsCombo = objEval_group_master.GetCategoryComboList("List", True)
            With cboSQCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboSQCategory_SelectedIndexChanged(Nothing, Nothing)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearSubQuestionSetupCtrls()
        Try
            mintSubQuestionUnkid = 0
            txtSQDescription.Text = ""
            txtSQSubQuestion.Text = ""
            cboSQCategory.SelectedIndex = 0
            cboSQQuestions.SelectedIndex = 0
            cboSQQuestions_SelectedIndexChanged(Nothing, Nothing)
            dgvSubQuestionaireAnswer.DataSource = Nothing
            dgvSubQuestionaireAnswer.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillSubQuestionList()
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Dim dsList As New DataSet
        Try
            dsList = objEval_subquestion_master.GetList("List")
            gvSubQuestion.DataSource = dsList.Tables(0)
            gvSubQuestion.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_subquestion_master = Nothing
        End Try
    End Sub

    Private Function IsValidSubQuestion() As Boolean
        Try
            If CInt(cboSQCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 1, "Please select category to continue."), Me)
                Return False
            End If

            If CInt(cboSQQuestions.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 2, "Please select question to continue."), Me)
                Return False
            End If

            If txtSQSubQuestion.Text.Trim().Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 3, "Please enter sub-question to continue."), Me)
                Return False
            End If

            'If txtSQDescription.Text.Trim().Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName3, 4, "Please enter description to continue."), Me)
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetSubQuestionValue(ByRef objEval_subquestion_master As clseval_subquestion_master)
        Try
            objEval_subquestion_master._Subquestionid = mintSubQuestionUnkid
            objEval_subquestion_master._Categoryid = CInt(cboSQCategory.SelectedValue)
            objEval_subquestion_master._Questionid = CInt(cboSQQuestions.SelectedValue)
            objEval_subquestion_master._Subquestion = txtSQSubQuestion.Text
            objEval_subquestion_master._Description = txtSQDescription.Text


            objEval_subquestion_master._AuditUserId = CInt(Session("UserId"))
            objEval_subquestion_master._ClientIP = CStr(Session("IP_ADD"))
            objEval_subquestion_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objEval_subquestion_master._DatabaseName = CStr(Session("Database_Name"))
            objEval_subquestion_master._FormName = mstrModuleName3
            objEval_subquestion_master._FromWeb = True
            objEval_subquestion_master._HostName = CStr(Session("HOST_NAME"))
            objEval_subquestion_master._Isvoid = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetSubQuestionValue(ByVal intSubQuestionId As Integer)
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Try
            objEval_subquestion_master._Subquestionid = intSubQuestionId

            cboSQCategory.SelectedValue = objEval_subquestion_master._Categoryid.ToString()
            cboSQCategory_SelectedIndexChanged(Nothing, Nothing)
            cboSQQuestions.SelectedValue = objEval_subquestion_master._Questionid.ToString()
            cboSQQuestions_SelectedIndexChanged(Nothing, Nothing)
            txtSQDescription.Text = objEval_subquestion_master._Description
            txtSQSubQuestion.Text = objEval_subquestion_master._Subquestion
            FillSubQuestionAnswerList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_subquestion_master = Nothing
        End Try
    End Sub

    Private Sub FillSubQuestionAnswerList()
        Dim objEvalAnswer As New clseval_answer_master
        Dim dsList As DataSet
        Dim mblnblank As Boolean = False
        Try
            dsList = objEvalAnswer.GetList("List", mintQuestionUnkid, mintSubQuestionUnkid)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("optionvalue") = ""
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            dgvSubQuestionaireAnswer.DataSource = dsList.Tables(0)
            dgvSubQuestionaireAnswer.DataBind()

            If mblnblank Then dgvSubQuestionaireAnswer.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalAnswer = Nothing
        End Try
    End Sub

    Private Sub GetSubQuestionAnswerValue(ByVal intQuestionAnswerId As Integer)
        Dim objEvalAnswer As New clseval_answer_master
        Try
            objEvalAnswer._TrainingEvaluationAnswerunkid = intQuestionAnswerId
            txtSubOptionValue.Text = objEvalAnswer._OptionValue
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalAnswer = Nothing
        End Try
    End Sub


#End Region

#Region " Combo's Events "

    Protected Sub cboSQCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSQCategory.SelectedIndexChanged
        Dim objEval_question_master As New clseval_question_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objEval_question_master.GetQuestionComboList("List", True, CInt(cboSQCategory.SelectedValue))
            With cboSQQuestions
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_question_master = Nothing
        End Try
    End Sub

    Protected Sub cboSQQuestions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSQQuestions.SelectedIndexChanged
        Dim objEvalQuestion As New clseval_question_master
        Try
            If CInt(cboSQQuestions.SelectedValue) > 0 Then
                'Hemant (20 Aug 2021) -- Start
                'BUG : OLD-456 - Evaluation Questions with Sub questions are getting deleted.
                'mintQuestionUnkid = CInt(cboSQQuestions.SelectedValue)
                'objEvalQuestion._Questionnaireid = mintQuestionUnkid
                objEvalQuestion._Questionnaireid = CInt(cboSQQuestions.SelectedValue)
                'Hemant (20 Aug 2021) -- End
                mintAnswerTypeId = objEvalQuestion._Answertype
                If mintAnswerTypeId = CInt(clseval_question_master.enAnswerType.SELECTION) Then
                    pnlSubQuestionaireAnswer.Visible = True
                ElseIf mintAnswerTypeId = CInt(clseval_question_master.enAnswerType.RATING) Then
                    pnlSubQuestionaireAnswer.Visible = True
                Else
                    pnlSubQuestionaireAnswer.Visible = False
                End If
            Else
                pnlSubQuestionaireAnswer.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalQuestion = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Protected Sub btnSQSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSQSave.Click
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidSubQuestion() = False Then
                Exit Sub
            End If
            SetSubQuestionValue(objEval_subquestion_master)
            If mintSubQuestionUnkid > 0 Then
                blnFlag = objEval_subquestion_master.Update()
            Else
                blnFlag = objEval_subquestion_master.Insert()
            End If

            If blnFlag = False AndAlso objEval_subquestion_master._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objEval_subquestion_master._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 5, "Sub-Question saved successfully."), Me)
                FillSubQuestionList()
                ClearSubQuestionSetupCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_subquestion_master = Nothing
        End Try
    End Sub

    Protected Sub btnSQReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSQReset.Click
        Try
            Call ClearSubQuestionSetupCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddSubQuestionAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSubQuestionAnswer.Click
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Dim objEvalAnswer As New clseval_answer_master
        Try
            If txtSubOptionValue.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 9, "Please enter Option Value to continue."), Me)
                Exit Sub
            End If

            mstrOptionValue = txtSubOptionValue.Text

            SetSubQuestionValue(objEval_subquestion_master)
            SetQuestionAnswerValue(objEvalAnswer)
            objEvalAnswer._QuestionnaireId = CInt(cboSQQuestions.SelectedValue)
            objEvalAnswer._SubQuestionId = mintSubQuestionUnkid
            If objEvalAnswer.SaveAllSubQuestionAnswer(objEval_subquestion_master) = False Then
                If objEvalAnswer._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEvalAnswer._Message, Me)
                End If
            Else
                mintSubQuestionUnkid = objEvalAnswer._SubQuestionId
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 8, "SubQuestion's Answer saved successfully"), Me)
                mintTrainingEvaluationAnswerunkid = -1
                Call FillSubQuestionAnswerList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseSubQuestionAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseSubQuestionAnswer.Click
        Try
            mblnShowSubQuestionAnswerPopup = False
            popupSubQuestionAnswer.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkSQEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintSubQuestionUnkid = CInt(gvSubQuestion.DataKeys(row.RowIndex)("subquestionid").ToString())
            GetSubQuestionValue(mintSubQuestionUnkid)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSQDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            mintSubQuestionUnkid = CInt(gvSubQuestion.DataKeys(row.RowIndex)("subquestionid"))

            If objEval_subquestion_master.isUsed(mintSubQuestionUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 11, "Sorry, this subquestion is already in used."), Me)
                mintSubQuestionUnkid = 0
                Exit Sub
            End If

            mstrDeleteAction = "delSubQuestion"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 6, "Are you sure you want to delete this sub-question?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEval_subquestion_master = Nothing
        End Try
    End Sub

    Protected Sub lnkSubQuestionaireAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSubQuestionaireAnswer.Click
        Try
            If IsValidSubQuestion() = False Then
                Exit Sub
            End If

            txtSubOptionValue.Text = ""

            mblnShowSubQuestionAnswerPopup = True
            popupSubQuestionAnswer.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEditSubQuestionaireAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintTrainingEvaluationAnswerunkid = CInt(dgvSubQuestionaireAnswer.DataKeys(row.RowIndex)("trainingevaluationanswerunkid"))
            GetSubQuestionAnswerValue(mintTrainingEvaluationAnswerunkid)
            mblnShowSubQuestionAnswerPopup = True
            popupSubQuestionAnswer.Show()
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeleteSubQuestionaireAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objEvalAnswer As New clseval_answer_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            mintTrainingEvaluationAnswerunkid = CInt(dgvSubQuestionaireAnswer.DataKeys(row.RowIndex)("trainingevaluationanswerunkid"))

            If objEvalAnswer.isUsed(mintTrainingEvaluationAnswerunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 12, "Sorry, this answer is already in used."), Me)
                mintTrainingEvaluationAnswerunkid = 0
                Exit Sub
            End If

            mstrDeleteAction = "delsubquestionaireanswer"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 13, "Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvalAnswer = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region "Other Setting"
    'Hemant (04 Sep 2021) -- Start
    'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
#Region " Private Method(s)"
    Private Sub GetOtherSettings()
        Dim objConfig As New clsConfigOptions
        Dim mstrValue As String = ""
        Try

            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "PreTrainingFeedbackInstruction", Nothing)
            mstrATPreTrainingFeedbackInstruction = mstrValue
            txtPreTrainingFeedbackInstruction.Text = mstrATPreTrainingFeedbackInstruction

            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "PostTrainingFeedbackInstruction", Nothing)
            mstrATPostTrainingFeedbackInstruction = mstrValue
            txtPostTrainingFeedbackInstruction.Text = mstrATPostTrainingFeedbackInstruction


            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "DaysAfterTrainingFeedbackInstruction", Nothing)
            mstrATDaysAfterTrainingFeedbackInstruction = mstrValue
            txtDaysAfterTrainingFeedbackInstruction.Text = mstrATDaysAfterTrainingFeedbackInstruction

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnOtherSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOtherSave.Click
        Dim objConfig As New clsConfigOptions
        Try
            objConfig._Companyunkid = CInt(Session("CompanyUnkId"))

            objConfig._PreTrainingFeedbackInstruction = txtPreTrainingFeedbackInstruction.Text
            objConfig._PostTrainingFeedbackInstruction = txtPostTrainingFeedbackInstruction.Text
            objConfig._DaysAfterTrainingFeedbackInstruction = txtDaysAfterTrainingFeedbackInstruction.Text

            Dim mblnFlag As Boolean = objConfig.updateParam()

            If mblnFlag Then
                Dim objatConfigOption As New clsAtconfigoption
                objatConfigOption._Companyunkid = CInt(Session("CompanyUnkId"))
                objatConfigOption._Audituserunkid = CInt(Session("UserId"))

                If txtPreTrainingFeedbackInstruction.Text <> mstrATPreTrainingFeedbackInstruction Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "PreTrainingFeedbackInstruction")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & lblPreTrainingFeedbackInstruction.Text & " --> Old Value :- " & mstrATPreTrainingFeedbackInstruction.ToString() & " | New Value :- " & txtPreTrainingFeedbackInstruction.Text
                    objatConfigOption.Insert()
                    mstrATPreTrainingFeedbackInstruction = txtPreTrainingFeedbackInstruction.Text
                End If

                If txtPostTrainingFeedbackInstruction.Text <> mstrATPostTrainingFeedbackInstruction Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "PostTrainingFeedbackInstruction")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & lblPostTrainingFeedbackInstruction.Text & " --> Old Value :- " & mstrATPostTrainingFeedbackInstruction.ToString() & " | New Value :- " & txtPostTrainingFeedbackInstruction.Text
                    objatConfigOption.Insert()
                    mstrATPostTrainingFeedbackInstruction = txtPostTrainingFeedbackInstruction.Text
                End If

                If txtDaysAfterTrainingFeedbackInstruction.Text <> mstrATDaysAfterTrainingFeedbackInstruction Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "DaysAfterTrainingFeedbackInstruction")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & lblDaysAfterTrainingFeedbackInstruction.Text & " --> Old Value :- " & mstrATDaysAfterTrainingFeedbackInstruction.ToString() & " | New Value :- " & txtDaysAfterTrainingFeedbackInstruction.Text
                    objatConfigOption.Insert()
                    mstrATDaysAfterTrainingFeedbackInstruction = txtDaysAfterTrainingFeedbackInstruction.Text
                End If

                objatConfigOption = Nothing

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 1, "Other Settings saved successfully"), Me)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
        End Try
    End Sub
#End Region
    'Hemant (04 Sep 2021) -- End


#End Region

#Region "Confirmation"
    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()

                Case "DELEVALUTIONCATEGORY"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 12, "Evalution Category delete reason.")
                    delReason.Show()

                Case "DELQUESTION"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 12, "Question delete reason.")
                    delReason.Show()

                Case "DELQUESTIONAIREANSWER"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 15, "Questionaire's answer delete reason.")
                    delReason.Show()

                Case "DELSUBQUESTION"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 10, "SubQuestion delete reason.")
                    delReason.Show()

                Case "DELSUBQUESTIONAIREANSWER"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 14, "SubQuestion's answer delete reason.")
                    delReason.Show()

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Delete Reason"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELEVALUTIONCATEGORY"
                    Dim objEval_group_master As New clseval_group_master
                    SetEvalutionCategoriesValue(objEval_group_master)
                    objEval_group_master._Isvoid = True
                    objEval_group_master._Voidreason = delReason.Reason
                    objEval_group_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEval_group_master._Voiduserunkid = CInt(Session("UserId"))

                    blnFlag = objEval_group_master.Delete(mintEvalutionCategoryUnkid)

                    If blnFlag = False AndAlso objEval_group_master._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objEval_group_master._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Evalution category deleted successfully."), Me)
                        FillEvalutionCategoryList()
                        FillQuestionnaireCombo()
                        FillSubQuestionnaireCombo()
                        ClearEvalutionCategoriesCtrls()
                    End If
                    objEval_group_master = Nothing

                Case "DELQUESTION"
                    Dim objEval_question_master As New clseval_question_master
                    SetQuestionnaireValue(objEval_question_master)
                    objEval_question_master._Isvoid = True
                    objEval_question_master._Voidreason = delReason.Reason
                    objEval_question_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEval_question_master._Voiduserunkid = CInt(Session("UserId"))

                    blnFlag = objEval_question_master.Delete(mintQuestionUnkid)

                    If blnFlag = False AndAlso objEval_question_master._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objEval_question_master._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 9, "Question deleted successfully."), Me)
                        FillQuestionList()
                        ClearQuestionnaireCtrls()
                    End If
                    objEval_question_master = Nothing
                Case "DELSUBQUESTION"
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    objEval_subquestion_master._Subquestionid = mintSubQuestionUnkid
                    objEval_subquestion_master._AuditUserId = CInt(Session("UserId"))
                    objEval_subquestion_master._ClientIP = CStr(Session("IP_ADD"))
                    objEval_subquestion_master._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    objEval_subquestion_master._DatabaseName = CStr(Session("Database_Name"))
                    objEval_subquestion_master._FormName = mstrModuleName3
                    objEval_subquestion_master._FromWeb = True
                    objEval_subquestion_master._HostName = CStr(Session("HOST_NAME"))
                    objEval_subquestion_master._Isvoid = True
                    objEval_subquestion_master._Voidreason = delReason.Reason
                    objEval_subquestion_master._Voiduserunkid = CInt(Session("UserId"))
                    objEval_subquestion_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                    blnFlag = objEval_subquestion_master.Delete(objEval_subquestion_master._Questionid, mintSubQuestionUnkid)

                    If blnFlag = False AndAlso objEval_subquestion_master._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objEval_subquestion_master._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 7, "SubQuestion deleted successfully."), Me)
                        FillSubQuestionList()
                        ClearSubQuestionSetupCtrls()
                    End If
                    objEval_subquestion_master = Nothing

                Case "DELQUESTIONAIREANSWER"
                    Dim objEvalAnswer As New clseval_answer_master
                    SetQuestionAnswerValue(objEvalAnswer)
                    objEvalAnswer._Isvoid = True
                    objEvalAnswer._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEvalAnswer._Voiduserunkid = CInt(Session("UserId"))
                    objEvalAnswer._Voidreason = delReason.Reason
                    blnFlag = objEvalAnswer.Void(mintTrainingEvaluationAnswerunkid)
                    If blnFlag = False AndAlso objEvalAnswer._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objEvalAnswer._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 16, "Questionaire's Answer deleted successfully"), Me)
                        mintTrainingEvaluationAnswerunkid = -1
                        Call FillQuestionAnswerList()
                    End If

                Case "DELSUBQUESTIONAIREANSWER"
                    Dim objEvalAnswer As New clseval_answer_master
                    SetQuestionAnswerValue(objEvalAnswer)
                    objEvalAnswer._Isvoid = True
                    objEvalAnswer._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEvalAnswer._Voiduserunkid = CInt(Session("UserId"))
                    objEvalAnswer._Voidreason = delReason.Reason
                    blnFlag = objEvalAnswer.Void(mintTrainingEvaluationAnswerunkid)
                    If blnFlag = False AndAlso objEvalAnswer._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objEvalAnswer._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 15, "SubQuestion's Answer deleted successfully"), Me)
                        mintTrainingEvaluationAnswerunkid = -1
                        Call FillSubQuestionAnswerList()
                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, mstrModuleName1, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblEvalutionCategory.ID, Me.lblEvalutionCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblEvalutionCategoryHeader.ID, Me.lblEvalutionCategoryHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblECName.ID, Me.lblECName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblECFeedbackMode.ID, Me.lblECFeedbackMode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.rdbECFeedbackModePreTraining.ID, Me.rdbECFeedbackModePreTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.rdbECFeedbackModePostTraining.ID, Me.rdbECFeedbackModePostTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.ID, Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.ID, Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.chkECFeedbackModeDaysAfterTrainingAttend.ID, Me.chkECFeedbackModeDaysAfterTrainingAttend.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnECReset.ID, Me.btnECReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnECSave.ID, Me.btnECSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvECList.Columns(2).FooterText, gvECList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvECList.Columns(3).FooterText, gvECList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvECList.Columns(4).FooterText, gvECList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, gvECList.Columns(5).FooterText, gvECList.Columns(5).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.BtnClose.ID, Me.BtnClose.Text)

            'Question Setup'
            'Language.setLanguage(mstrModuleName2)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblQSHeader.ID, Me.lblQSHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblQSCategory.ID, Me.lblQSCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblQSCode.ID, Me.lblQSCode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblQSQuestion.ID, Me.lblQSQuestion.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblQSAnswerType.ID, Me.lblQSAnswerType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.chkQSAskForJustification.ID, Me.chkQSAskForJustification.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblQSDescription.ID, Me.lblQSDescription.Text)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.chkQSForLineManagerFeedback.ID, Me.chkQSForLineManagerFeedback.Text)
            'Hemant (20 Aug 2021) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnQSSave.ID, Me.btnQSSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnQSReset.ID, Me.btnQSReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvQuestionnaire.Columns(2).FooterText, gvQuestionnaire.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvQuestionnaire.Columns(3).FooterText, gvQuestionnaire.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvQuestionnaire.Columns(4).FooterText, gvQuestionnaire.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvQuestionnaire.Columns(5).FooterText, gvQuestionnaire.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvQuestionnaire.Columns(6).FooterText, gvQuestionnaire.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, gvQuestionnaire.Columns(7).FooterText, gvQuestionnaire.Columns(7).HeaderText)

            'Sub-Question Setup'
            'Language.setLanguage(mstrModuleName3)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblSubQuestionsHeader.ID, Me.lblSubQuestionsHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblSQCategory.ID, Me.lblSQCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblSQQuestions.ID, Me.lblSQQuestions.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblSQSubQuestion.ID, Me.lblSQSubQuestion.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblSQDescription.ID, Me.lblSQDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.btnSQSave.ID, Me.btnSQSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.btnSQReset.ID, Me.btnSQReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, gvSubQuestion.Columns(2).FooterText, gvSubQuestion.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, gvSubQuestion.Columns(3).FooterText, gvSubQuestion.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, gvSubQuestion.Columns(4).FooterText, gvSubQuestion.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, gvSubQuestion.Columns(5).FooterText, gvSubQuestion.Columns(5).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName1)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.Title)

            Me.lblEvalutionCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEvalutionCategory.ID, Me.lblEvalutionCategory.Text)
            Me.lblEvalutionCategoryHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEvalutionCategoryHeader.ID, Me.lblEvalutionCategoryHeader.Text)
            Me.lblECName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblECName.ID, Me.lblECName.Text)
            Me.lblECFeedbackMode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblECFeedbackMode.ID, Me.lblECFeedbackMode.Text)
            Me.rdbECFeedbackModePreTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.rdbECFeedbackModePreTraining.ID, Me.rdbECFeedbackModePreTraining.Text)
            Me.rdbECFeedbackModePostTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.rdbECFeedbackModePostTraining.ID, Me.rdbECFeedbackModePostTraining.Text)
            Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.ID, Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.Text)
            Me.chkECFeedbackModeDaysAfterTrainingAttend.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.chkECFeedbackModeDaysAfterTrainingAttend.ID, Me.chkECFeedbackModeDaysAfterTrainingAttend.Text)
            Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.ID, Me.rdbECIsFeedbackModeDaysAfterTrainingAttend.Text)
            Me.btnECReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnECReset.ID, Me.btnECReset.Text)
            Me.btnECSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnECSave.ID, Me.btnECSave.Text)

            gvECList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvECList.Columns(2).FooterText, gvECList.Columns(2).HeaderText)
            gvECList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvECList.Columns(3).FooterText, gvECList.Columns(3).HeaderText)
            gvECList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvECList.Columns(4).FooterText, gvECList.Columns(4).HeaderText)
            gvECList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), gvECList.Columns(5).FooterText, gvECList.Columns(5).HeaderText)

            'Question Setup'
            'Language.setLanguage(mstrModuleName2)

            Me.lblQSHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblQSHeader.ID, Me.lblQSHeader.Text)
            Me.lblQSCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblQSCode.ID, Me.lblQSCode.Text)
            Me.lblQSCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblQSCategory.ID, Me.lblQSCategory.Text)
            Me.lblQSQuestion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblQSQuestion.ID, Me.lblQSQuestion.Text)
            Me.lblQSAnswerType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblQSAnswerType.ID, Me.lblQSAnswerType.Text)
            Me.chkQSAskForJustification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.chkQSAskForJustification.ID, Me.chkQSAskForJustification.Text)
            Me.lblQSDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblQSDescription.ID, Me.lblQSDescription.Text)
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            Me.chkQSForLineManagerFeedback.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.chkQSForLineManagerFeedback.ID, Me.chkQSForLineManagerFeedback.Text)
            'Hemant (20 Aug 2021) -- End

            Me.btnQSReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnQSReset.ID, Me.btnQSReset.Text).Replace("&", "")
            Me.btnQSSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnQSSave.ID, Me.btnQSSave.Text).Replace("&", "")

            gvQuestionnaire.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvQuestionnaire.Columns(2).FooterText, gvQuestionnaire.Columns(2).HeaderText)
            gvQuestionnaire.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvQuestionnaire.Columns(3).FooterText, gvQuestionnaire.Columns(3).HeaderText)
            gvQuestionnaire.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvQuestionnaire.Columns(4).FooterText, gvQuestionnaire.Columns(4).HeaderText)
            gvQuestionnaire.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvQuestionnaire.Columns(5).FooterText, gvQuestionnaire.Columns(5).HeaderText)
            gvQuestionnaire.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvQuestionnaire.Columns(6).FooterText, gvQuestionnaire.Columns(6).HeaderText)
            gvQuestionnaire.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), gvQuestionnaire.Columns(7).FooterText, gvQuestionnaire.Columns(7).HeaderText)

            'Sub-question Setup'
            'Language.setLanguage(mstrModuleName3)

            Me.lblSubQuestionsHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblSubQuestionsHeader.ID, Me.lblSubQuestionsHeader.Text)
            Me.lblSQCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblSQCategory.ID, Me.lblSQCategory.Text)
            Me.lblSQQuestions.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblSQQuestions.ID, Me.lblSQQuestions.Text)
            Me.lblSQSubQuestion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblSQSubQuestion.ID, Me.lblSQSubQuestion.Text)
            Me.lblSQDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblSQDescription.ID, Me.lblSQDescription.Text)

            Me.btnSQSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.btnSQSave.ID, Me.btnSQSave.Text)
            Me.btnSQReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.btnSQReset.ID, Me.btnSQReset.Text)

            gvSubQuestion.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), gvSubQuestion.Columns(2).FooterText, gvSubQuestion.Columns(2).HeaderText)
            gvSubQuestion.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), gvSubQuestion.Columns(3).FooterText, gvSubQuestion.Columns(3).HeaderText)
            gvSubQuestion.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), gvSubQuestion.Columns(4).FooterText, gvSubQuestion.Columns(4).HeaderText)
            gvSubQuestion.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), gvSubQuestion.Columns(5).FooterText, gvSubQuestion.Columns(5).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Please enter category name to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Please select feedback mode to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Please add days after training attended to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "Please add notify before days for feedback to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 5, "Evalution category saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 6, "Are you sure you want to delete this category?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 7, "Evalution category deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 8, "Sorry, this category value is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 9, "Sorry, Days after training attended should be greater than Zero.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 10, "Sorry, Notify before days for feedback should be greater than Zero.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 11, "Days after training attended can not less than defined days for already exist entry of Days after training attended Feedback .")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 12, "Evalution Category delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 1, "Please select category to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 2, "Please enter code to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 3, "Please enter question to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 4, "Please select answer type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 6, "Question saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 7, "Sorry, this question is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 8, "Are you sure you want to delete this question?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 9, "Question deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 10, "Question's Answer saved successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 11, "Please enter Option Value to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 12, "Question delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 13, "Sorry, this answer is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 14, "Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 15, "Questionaire's answer delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 16, "Questionaire's Answer deleted successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 1, "Please select category to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 2, "Please select question to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 3, "Please enter sub-question to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 5, "Sub-Question saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 6, "Are you sure you want to delete this sub-question?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 7, "SubQuestion deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 8, "SubQuestion's Answer saved successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 9, "Please enter Option Value to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 10, "SubQuestion delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 11, "Sorry, this subquestion is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 12, "Sorry, this answer is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 13, "Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 14, "SubQuestion's answer delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 15, "SubQuestion's Answer deleted successfully")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
