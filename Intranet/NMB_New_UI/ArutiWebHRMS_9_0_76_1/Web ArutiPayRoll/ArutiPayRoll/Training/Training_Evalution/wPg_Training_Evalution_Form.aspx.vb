﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class Training_Training_Evalution_wPg_Training_Evalution_Form
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTrainingEvalutionForm"
    Private DisplayMessage As New CommonCodes
    Private mintTrainingRequestunkid As Integer = -1
    Private mintCourseMasterunkid As Integer = -1
    Private mintFeedBackModeId As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mdtQuestionnaire As DataTable
    Private objCONN As SqlConnection
    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Private mblnIsDaysAfterFeedbackSubmitted As Boolean = False
    'Hemant (20 Aug 2021) -- End
    'Hemant (01 Sep 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
    Private mblnIsPreTrainingFeedbackSubmitted As Boolean = False
    Private mblnIsPostTrainingFeedbackSubmitted As Boolean = False
    Private mblnIsDaysAfterLineManagerFeedbackSubmitted As Boolean = False
    'Hemant (01 Sep 2021) -- End
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objRequestMaster As New clstraining_request_master
        Try

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If
            'Hemant (20 Aug 2021) -- End

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 6 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Me.ViewState.Add("IsDirect", True)
                    mintTrainingRequestunkid = CInt(arr(0))
                    ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(1))
                    mintEmployeeunkid = CInt(arr(2))
                    mintFeedBackModeId = CInt(arr(3))
                    ViewState("mintFeedBackModeId") = mintFeedBackModeId
                    If CBool(arr(5)) Then
                        HttpContext.Current.Session("UserId") = CInt(arr(4))
                    Else
                        HttpContext.Current.Session("Employeeunkid") = CInt(arr(4))
                    End If

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                    Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                    Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                    SetDateFormat()
                    If CBool(arr(5)) Then
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                    Else
                        Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""
                        Dim objEmp As New clsEmployee_Master
                        objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(Session("Employeeunkid"))
                        xUName = objEmp._Displayname : xPasswd = objEmp._Password
                        HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("UserId") = -1
                        HttpContext.Current.Session("Employeeunkid") = CInt(Session("Employeeunkid"))

                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        HttpContext.Current.Session("DisplayName") = xUName

                        HttpContext.Current.Session("UserName") = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
                        HttpContext.Current.Session("Password") = objEmp._Password
                        HttpContext.Current.Session("LeaveBalances") = 0
                        HttpContext.Current.Session("RoleID") = 0

                        HttpContext.Current.Session("Firstname") = objEmp._Firstname
                        HttpContext.Current.Session("Surname") = objEmp._Surname
                        HttpContext.Current.Session("MemberName") = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname
                        HttpContext.Current.Session("LangId") = 1
                        objEmp = Nothing
                    End If


                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("Login") = True

                End If
                CType(Me.Master.FindControl("pnlMenuWrapper"), Panel).Visible = False
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count <= 0 Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Hemant (20 Aug 2021) -- End

            If Not IsPostBack Then
                GC.Collect()
                Call SetMessages()
                'Call Language._Object.SaveValue()

                If Not Session("TrainingRequestunkid") Is Nothing Or Not Session("TrainingRequestunkid") Is Nothing Then
                    ViewState("mintTrainingRequestunkid") = CInt(Session("TrainingRequestunkid"))
                    Session.Remove("TrainingRequestunkid")
                End If

                If Not Session("FeedBackModeId") Is Nothing Or Not Session("FeedBackModeId") Is Nothing Then
                    ViewState("mintFeedBackModeId") = CInt(Session("FeedBackModeId"))
                    Session.Remove("FeedBackModeId")
                End If

                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintFeedBackModeId = CInt(ViewState("mintFeedBackModeId"))

                objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
                mintEmployeeunkid = objRequestMaster._Employeeunkid
                mintCourseMasterunkid = objRequestMaster._Coursemasterunkid
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsDaysAfterFeedbackSubmitted = objRequestMaster._IsDaysAfterFeedbackSubmitted
                'Hemant (20 Aug 2021) -- End
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                mblnIsPreTrainingFeedbackSubmitted = objRequestMaster._IsPreTrainingFeedbackSubmitted
                mblnIsPostTrainingFeedbackSubmitted = objRequestMaster._IsPostTrainingFeedbackSubmitted
                mblnIsDaysAfterLineManagerFeedbackSubmitted = objRequestMaster._IsDaysAfterLineManagerFeedbackSubmitted
                'Hemant (01 Sep 2021) -- End

                If mintFeedBackModeId > 0 Then
                    Call FillEvalutionCategoryList()
                    FillQuestionsList()
                Else
                    btnSubmit.Visible = False
                    btnSave.Visible = False
                End If

                'Hemant (04 Sep 2021) -- Start
                'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
                Select Case mintFeedBackModeId
                    Case clseval_group_master.enFeedBack.PRETRAINING
                        txtInstruction.Text = Session("PreTrainingFeedbackInstruction").ToString
                    Case clseval_group_master.enFeedBack.POSTTRAINING
                        txtInstruction.Text = Session("PostTrainingFeedbackInstruction").ToString
                    Case clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                        txtInstruction.Text = Session("DaysAfterTrainingFeedbackInstruction").ToString
                End Select

                If txtInstruction.Text.Trim.Length > 0 Then pnlInstruction.Visible = True
                'Hemant (04 Sep 2021) -- End

            Else
                'mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintFeedBackModeId = CInt(ViewState("mintFeedBackModeId"))
                mintEmployeeunkid = CInt(ViewState("mintEmployeeunkid"))
                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintCourseMasterunkid = CInt(ViewState("mintCourseMasterunkid"))
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsDaysAfterFeedbackSubmitted = CBool(ViewState("isdaysafterfeedback_submitted"))
                'Hemant (20 Aug 2021) -- End
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                mblnIsPreTrainingFeedbackSubmitted = CBool(ViewState("mblnIsPreTrainingFeedbackSubmitted"))
                mblnIsPostTrainingFeedbackSubmitted = CBool(ViewState("mblnIsPostTrainingFeedbackSubmitted"))
                mblnIsDaysAfterLineManagerFeedbackSubmitted = CBool(ViewState("mblnIsDaysAfterLineManagerFeedbackSubmitted"))
                'Hemant (01 Sep 2021) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintFeedBackModeId") = mintFeedBackModeId
            Me.ViewState("mintEmployeeunkid") = mintEmployeeunkid
            Me.ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            Me.ViewState("mintCourseMasterunkid") = mintCourseMasterunkid
            Me.ViewState("isdaysafterfeedback_submitted") = mblnIsDaysAfterFeedbackSubmitted
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            Me.ViewState("mblnIsPreTrainingFeedbackSubmitted") = mblnIsPreTrainingFeedbackSubmitted
            Me.ViewState("mblnIsPostTrainingFeedbackSubmitted") = mblnIsPostTrainingFeedbackSubmitted
            Me.ViewState("mblnIsDaysAfterLineManagerFeedbackSubmitted") = mblnIsDaysAfterLineManagerFeedbackSubmitted
            'Hemant (01 Sep 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region " Private Method(s)"

    Private Sub FillEvalutionCategoryList()
        Dim objeval_group_master As New clseval_group_master
        Dim dsList As New DataSet
        Try
            dsList = objeval_group_master.GetList("List", , mintFeedBackModeId)
            dgvCategory.DataSource = dsList.Tables(0)
            dgvCategory.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objeval_group_master = Nothing
        End Try
    End Sub

    Private Function SetValue(ByVal objTrainingEvaluationTran As clstraining_evaluation_tran) As Boolean
        Try
            objTrainingEvaluationTran._Employeeunkid = mintEmployeeunkid
            objTrainingEvaluationTran._TrainingRequestunkid = mintTrainingRequestunkid
            objTrainingEvaluationTran._CourseMasterunkId = mintCourseMasterunkid
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingEvaluationTran._Userunkid = CInt(Session("UserId"))
                objTrainingEvaluationTran._LoginEmployeeunkid = -1
            Else
                objTrainingEvaluationTran._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objTrainingEvaluationTran._Userunkid = -1
            End If
            objTrainingEvaluationTran._Isvoid = False
            objTrainingEvaluationTran._Voiddatetime = Nothing
            objTrainingEvaluationTran._Voidreason = ""
            objTrainingEvaluationTran._Voiduserunkid = -1
            objTrainingEvaluationTran._AuditUserId = CInt(Session("UserId"))
            objTrainingEvaluationTran._ClientIP = CStr(Session("IP_ADD"))
            objTrainingEvaluationTran._FormName = mstrModuleName
            objTrainingEvaluationTran._IsWeb = True
            objTrainingEvaluationTran._HostName = CStr(Session("HOST_NAME"))

            mdtQuestionnaire = objTrainingEvaluationTran._DataTable.Copy

            For Each dgvCategoryItem As DataListItem In dgvCategory.Items
                Dim hfcategoryunkid As HiddenField = CType(dgvCategoryItem.FindControl("hfcategoryunkid"), HiddenField)
                Dim categoryid As Integer = CInt(hfcategoryunkid.Value)

                Dim dgvQuestion As DataList = CType(dgvCategoryItem.FindControl("dgvQuestion"), DataList)
                For Each dgvQuestionItem As DataListItem In dgvQuestion.Items
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    Dim dsList As New DataSet

                    Dim hfquestionnaireId As HiddenField = CType(dgvQuestionItem.FindControl("hfquestionnaireId"), HiddenField)
                    Dim questionnaireid As Integer = CInt(hfquestionnaireId.Value)

                    Dim hfAnswerType As HiddenField = CType(dgvQuestionItem.FindControl("hfAnswerType"), HiddenField)
                    Dim hfCategoryId As HiddenField = CType(dgvQuestionItem.FindControl("hfCategoryId"), HiddenField)
                    Dim hfAskForJustification As HiddenField = CType(dgvQuestionItem.FindControl("hfAskForJustification"), HiddenField)
                    'Hemant (20 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                    Dim hfForLineManagerFeedback As HiddenField = CType(dgvQuestionItem.FindControl("hfForLineManagerFeedback"), HiddenField)
                    'Hemant (20 Aug 2021) -- End

                    dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))
                    If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                        Dim dtquestion As DataRow
                        dtquestion = mdtQuestionnaire.NewRow

                        dtquestion.Item("categoryid") = hfCategoryId.Value
                        dtquestion.Item("questionnaireunkid") = questionnaireid
                        dtquestion.Item("feedbackmodeid") = mintFeedBackModeId
                        dtquestion.Item("answertypeid") = hfAnswerType.Value
                        dtquestion.Item("isaskforjustification") = hfAskForJustification.Value
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        dtquestion.Item("isforlinemanager") = hfForLineManagerFeedback.Value
                        'Hemant (20 Aug 2021) -- End

                        Dim strAnswer As String = String.Empty
                        Select Case CInt(hfAnswerType.Value)
                            Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                Dim txtItemNameFreeText As TextBox = CType(dgvQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                strAnswer = txtItemNameFreeText.Text
                            Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                strAnswer = txtItemNameNUM.Text.ToString
                            Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                Dim dtpItemName As Controls_DateCtrl = CType(dgvQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                strAnswer = dtpItemName.GetDate.Date.ToString("yyyyMMdd")
                            Case CInt(clseval_question_master.enAnswerType.RATING)
                                Dim cboItemNameRating As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                strAnswer = cboItemNameRating.SelectedValue
                            Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                Dim cboItemNameSelection As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                strAnswer = cboItemNameSelection.SelectedValue
                            Case Else
                        End Select

                        dtquestion.Item("subquestionid") = -1
                        dtquestion.Item("answer") = strAnswer
                        If CBool(hfAskForJustification.Value) = True Then
                            Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                            dtquestion.Item("justification_answer") = txtJustify.Text
                        Else
                            dtquestion.Item("justification_answer") = ""
                        End If

                        mdtQuestionnaire.Rows.Add(dtquestion)
                    Else
                        Dim dgvSubQuestion As DataList = CType(dgvQuestionItem.FindControl("dgvSubQuestion"), DataList)
                        For Each dgvSubQuestionItem As DataListItem In dgvSubQuestion.Items
                            Dim dtquestion As DataRow
                            dtquestion = mdtQuestionnaire.NewRow

                            dtquestion.Item("categoryid") = hfCategoryId.Value
                            dtquestion.Item("questionnaireunkid") = questionnaireid
                            dtquestion.Item("feedbackmodeid") = mintFeedBackModeId
                            dtquestion.Item("answertypeid") = hfAnswerType.Value
                            dtquestion.Item("isaskforjustification") = hfAskForJustification.Value
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            dtquestion.Item("isforlinemanager") = hfForLineManagerFeedback.Value
                            'Hemant (20 Aug 2021) -- End
                            Dim hfSubQuestionId As HiddenField = CType(dgvSubQuestionItem.FindControl("hfSubQuestionId"), HiddenField)

                            Dim strAnswer As String = String.Empty
                            Select Case CInt(hfAnswerType.Value)
                                Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                    Dim txtItemNameFreeText As TextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                    strAnswer = txtItemNameFreeText.Text
                                Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                    Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                    strAnswer = txtItemNameNUM.Text.ToString
                                Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                    Dim dtpItemName As Controls_DateCtrl = CType(dgvSubQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                    strAnswer = dtpItemName.GetDate.Date.ToString("yyyyMMdd")
                                Case CInt(clseval_question_master.enAnswerType.RATING)
                                    Dim cboItemNameRating As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                    strAnswer = cboItemNameRating.SelectedValue
                                Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                    Dim cboItemNameSelection As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                    strAnswer = cboItemNameSelection.SelectedValue
                                Case Else
                            End Select

                            dtquestion.Item("subquestionid") = CInt(hfSubQuestionId.Value)
                            dtquestion.Item("answer") = strAnswer
                            If CBool(hfAskForJustification.Value) = True Then
                                Dim txtJustify As TextBox = CType(dgvSubQuestionItem.FindControl("txtJustify"), TextBox)
                                dtquestion.Item("justification_answer") = txtJustify.Text
                            Else
                                dtquestion.Item("justification_answer") = ""
                            End If
                            mdtQuestionnaire.Rows.Add(dtquestion)
                        Next
                    End If
                    objEval_subquestion_master = Nothing
                Next

            Next

            Dim dt As New DataTable
            Dim xRow() As DataRow = Nothing
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                xRow = mdtQuestionnaire.Select("isforlinemanager=1")
            Else
                xRow = mdtQuestionnaire.Select("isforlinemanager=0")
            End If
            If xRow.Length > 0 Then
                dt = xRow.CopyToDataTable()
            End If
            objTrainingEvaluationTran._DataTable = dt

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillQuestionsList()
        Dim objEvaluation As New clstraining_evaluation_tran
        Dim objQuestions As New clseval_question_master
        Dim dsQuestions As DataSet
        Try
            dsQuestions = objEvaluation.GetList("List", mintEmployeeunkid, mintTrainingRequestunkid, mintFeedBackModeId)
            For Each dgvCategoryItem As DataListItem In dgvCategory.Items
                Dim hfcategoryunkid As HiddenField = CType(dgvCategoryItem.FindControl("hfcategoryunkid"), HiddenField)
                Dim categoryid As Integer = CInt(hfcategoryunkid.Value)

                Dim dgvQuestion As DataList = CType(dgvCategoryItem.FindControl("dgvQuestion"), DataList)

                For Each dgvQuestionItem As DataListItem In dgvQuestion.Items
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    Dim dsList As New DataSet

                    Dim hfquestionnaireId As HiddenField = CType(dgvQuestionItem.FindControl("hfquestionnaireId"), HiddenField)
                    Dim questionnaireid As Integer = CInt(hfquestionnaireId.Value)

                    Dim hfAnswerType As HiddenField = CType(dgvQuestionItem.FindControl("hfAnswerType"), HiddenField)
                    Dim hfCategoryId As HiddenField = CType(dgvQuestionItem.FindControl("hfCategoryId"), HiddenField)
                    Dim hfAskForJustification As HiddenField = CType(dgvQuestionItem.FindControl("hfAskForJustification"), HiddenField)

                    dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))
                    If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then

                        For Each drRow As DataRow In dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid <= 0 ")

                            Select Case CInt(hfAnswerType.Value)
                                Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                    Dim txtItemNameFreeText As TextBox = CType(dgvQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                    txtItemNameFreeText.Text = drRow.Item("answer").ToString
                                Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                    Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                    txtItemNameNUM.Text = drRow.Item("answer").ToString
                                Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                    Dim dtpItemName As Controls_DateCtrl = CType(dgvQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                    Dim dt As DateTime = DateTime.ParseExact(drRow.Item("answer").ToString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture)
                                    dtpItemName.SetDate = dt.Date
                                Case CInt(clseval_question_master.enAnswerType.RATING)
                                    Dim cboItemNameRating As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                    cboItemNameRating.SelectedValue = drRow.Item("answer").ToString()
                                Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                    Dim cboItemNameSelection As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                    cboItemNameSelection.SelectedValue = drRow.Item("answer").ToString()
                                Case Else
                            End Select


                            If CBool(hfAskForJustification.Value) = True Then
                                Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                                txtJustify.Text = drRow.Item("justification_answer").ToString()
                            End If
                            Exit For
                        Next
                    Else
                        Dim dgvSubQuestion As DataList = CType(dgvQuestionItem.FindControl("dgvSubQuestion"), DataList)
                        For Each dgvSubQuestionItem As DataListItem In dgvSubQuestion.Items

                            Dim hfSubQuestionId As HiddenField = CType(dgvSubQuestionItem.FindControl("hfSubQuestionId"), HiddenField)

                            For Each drRow As DataRow In dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid = " & CInt(hfSubQuestionId.Value) & " ")
                                Dim strAnswer As String = String.Empty
                                Select Case CInt(hfAnswerType.Value)
                                    Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                        Dim txtItemNameFreeText As TextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                        txtItemNameFreeText.Text = drRow.Item("answer").ToString
                                    Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                        Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                        txtItemNameNUM.Text = drRow.Item("answer").ToString
                                    Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                        Dim dtpItemName As Controls_DateCtrl = CType(dgvSubQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                        Dim dt As DateTime = DateTime.ParseExact(drRow.Item("answer").ToString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture)
                                        dtpItemName.SetDate = dt.Date
                                    Case CInt(clseval_question_master.enAnswerType.RATING)
                                        Dim cboItemNameRating As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                        cboItemNameRating.SelectedValue = drRow.Item("answer").ToString()
                                    Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                        Dim cboItemNameSelection As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                        cboItemNameSelection.SelectedValue = drRow.Item("answer").ToString()
                                    Case Else
                                End Select

                                If CBool(hfAskForJustification.Value) = True Then
                                    Dim txtJustify As TextBox = CType(dgvSubQuestionItem.FindControl("txtJustify"), TextBox)
                                    txtJustify.Text = drRow.Item("justification_answer").ToString()
                                End If
                                Exit For
                            Next

                        Next
                    End If
                    objEval_subquestion_master = Nothing
                Next

            Next
            Dim drQuestions() As DataRow = objQuestions.GetList("List", True, , mintFeedBackModeId).Tables(0).Select("1=1")
            If drQuestions.Length > 0 Then
                btnSubmit.Visible = True
                btnSave.Visible = True
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.PRETRAINING _
                   AndAlso mblnIsPreTrainingFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.POSTTRAINING _
                       AndAlso mblnIsPostTrainingFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING _
                    AndAlso mblnIsDaysAfterFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING _
                    AndAlso mblnIsDaysAfterLineManagerFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                End If
                'Hemant (20 Aug 2021) -- End
            Else
                btnSave.Visible = False
                btnSubmit.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestions = Nothing
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            For Each dgvCategoryItem As DataListItem In dgvCategory.Items
                Dim dgvQuestion As DataList = CType(dgvCategoryItem.FindControl("dgvQuestion"), DataList)

                For Each dgvQuestionItem As DataListItem In dgvQuestion.Items
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    Dim dsList As New DataSet

                    Dim hfquestionnaireId As HiddenField = CType(dgvQuestionItem.FindControl("hfquestionnaireId"), HiddenField)
                    Dim hfAnswerType As HiddenField = CType(dgvQuestionItem.FindControl("hfAnswerType"), HiddenField)
                    Dim hfAskForJustification As HiddenField = CType(dgvQuestionItem.FindControl("hfAskForJustification"), HiddenField)
                    dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))
                    If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                        Select Case CInt(hfAnswerType.Value)
                            Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                Dim txtItemNameFreeText As TextBox = CType(dgvQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                If txtItemNameFreeText.Text.Trim.Length <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                If CInt(txtItemNameNUM.Text) < 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Negative Value is not allowed in Numeric Field."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                Dim dtpItemName As Controls_DateCtrl = CType(dgvQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                If dtpItemName.GetDate = Nothing Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.RATING)
                                Dim cboItemNameRating As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                If CInt(cboItemNameRating.SelectedValue) <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                Dim cboItemNameSelection As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                If CInt(cboItemNameSelection.SelectedValue) <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case Else
                        End Select


                        If CBool(hfAskForJustification.Value) = True Then
                            Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                            If txtJustify.Text.Trim.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, all questions justifications are mandatory. please attempt all questions justifications  to continue."), Me.Page)
                                Return False
                            End If
                        End If
                    Else
                        Dim dgvSubQuestion As DataList = CType(dgvQuestionItem.FindControl("dgvSubQuestion"), DataList)
                        For Each dgvSubQuestionItem As DataListItem In dgvSubQuestion.Items
                            Select Case CInt(hfAnswerType.Value)
                                Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                    Dim txtItemNameFreeText As TextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                    If txtItemNameFreeText.Text.Trim.Length <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                    Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                    If CInt(txtItemNameNUM.Text) < 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Negative Value is not allowed in Numeric Field."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                    Dim dtpItemName As Controls_DateCtrl = CType(dgvSubQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                    If dtpItemName.GetDate = Nothing Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.RATING)
                                    Dim cboItemNameRating As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                    If CInt(cboItemNameRating.SelectedValue) <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                    Dim cboItemNameSelection As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                    If CInt(cboItemNameSelection.SelectedValue) <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case Else
                            End Select

                            If CBool(hfAskForJustification.Value) = True Then
                                Dim txtJustify As TextBox = CType(dgvSubQuestionItem.FindControl("txtJustify"), TextBox)
                                If txtJustify.Text.Trim.Length <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, all questions justifications are mandatory. please attempt all questions justifications  to continue."), Me.Page)
                                    Return False
                                End If
                            End If
                        Next
                    End If
                Next
            Next

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click, _
                                                                                               btnSave.Click
        Dim objTrainingEvaluationTran As New clstraining_evaluation_tran
        Dim objTrainingRequest As New clstraining_request_master
        'Hemant (20 Aug 2021) -- Start
        'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
        Dim blnIsSubmit As Boolean = False
        Dim btn As Button = CType(sender, Button)
        'Hemant (20 Aug 2021) -- End
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            If btn.ID = btnSubmit.ID Then
                objTrainingRequest._TrainingRequestunkid = mintTrainingRequestunkid
                Select Case mintFeedBackModeId
                    Case clseval_group_master.enFeedBack.PRETRAINING
                        objTrainingRequest._IsPreTrainingFeedbackSubmitted = True
                        objTrainingRequest._PreTrainingFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                        blnIsSubmit = True
                    Case clseval_group_master.enFeedBack.POSTTRAINING
                        objTrainingRequest._IsPostTrainingFeedbackSubmitted = True
                        objTrainingRequest._PostTrainingFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                        blnIsSubmit = True
                    Case clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                        If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            Dim objReportTo As New clsReportingToEmployee
                            objReportTo._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = mintEmployeeunkid

                            Dim dt As DataTable = objReportTo._RDataTable
                            Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                            If DefaultReportList.Count > 0 Then
                                objTrainingRequest._IsDaysAfterFeedbackSubmitted = True
                                objTrainingRequest._DaysAfterFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                                objTrainingRequest._DaysAfterFeedbackSubmittedRemark = ""
                                blnIsSubmit = True
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, your line manager info is not present. Please contact support to proceed."), Me.Page)
                                btnSubmit.Enabled = False
                            End If
                            objReportTo = Nothing
                        Else
                            objTrainingRequest._IsDaysAfterLineManagerFeedbackSubmitted = True
                            objTrainingRequest._DaysAfterLineManagerFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                            blnIsSubmit = True
                        End If

                End Select
            End If
            'Hemant (20 Aug 2021) -- End

            If SetValue(objTrainingEvaluationTran) = False Then
                Exit Sub
            End If

            If objTrainingEvaluationTran.SaveEvaluationTran(Nothing, objTrainingRequest) = False Then
                DisplayMessage.DisplayMessage(objTrainingEvaluationTran._Message, Me)
                Exit Sub
            Else
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                If Request.QueryString.Count <= 0 Then
                    'Hemant (13 Aug 2021) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Training Evaluation Saved Successfully."), Me, "../Training_Request/wPg_TrainingRequestFormList.aspx")
                    'Hemant (13 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                Else
                    Session.Abandon()
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Training Evaluation Saved Successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                End If
                'Hemant (13 Aug 2021) -- End
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                If blnIsSubmit = True AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING AndAlso (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    objTrainingEvaluationTran.Send_Notification_ReportingTo(mintEmployeeunkid, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                            CInt(Session("CompanyUnkId")), mintCourseMasterunkid, mintTrainingRequestunkid, CStr(Session("ArutiSelfServiceURL")), _
                                                                            enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                End If
                'Hemant (20 Aug 2021) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingEvaluationTran = Nothing
            objTrainingRequest = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Hemant (13 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            If Request.QueryString.Count <= 0 Then
                'Hemant (13 Aug 2021) -- End
                If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                    Response.Redirect(Session("ReturnURL").ToString, False)
                    Session("ReturnURL") = Nothing
                Else
                    Response.Redirect("~\UserHome.aspx", False)
                End If
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            Else
                Session.Abandon()
                Response.Redirect("~/Index.aspx", False)
            End If
            'Hemant (13 Aug 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "
#End Region

#Region "Gridview Events"

    Protected Sub dgvCategory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgvCategory.ItemDataBound
        Dim dsList As New DataSet
        Dim objEval_question_master As New clseval_question_master
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfcategoryunkid As HiddenField = CType(e.Item.FindControl("hfcategoryunkid"), HiddenField)
                Dim dgvQuestion As DataList = CType(e.Item.FindControl("dgvQuestion"), DataList)
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    dsList = objEval_question_master.GetList("CategoryWiseQuestion", True, CInt(hfcategoryunkid.Value), , True)
                Else
                    'Hemant (20 Aug 2021) -- End
                    dsList = objEval_question_master.GetList("CategoryWiseQuestion", True, CInt(hfcategoryunkid.Value))
                End If 'Hemant (20 Aug 2021) 

                AddHandler dgvQuestion.ItemDataBound, AddressOf Me.dgvQuestion_ItemDataBound
                dgvQuestion.DataSource = dsList.Tables("CategoryWiseQuestion")
                dgvQuestion.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvQuestion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Dim dsList As New DataSet
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfquestionnaireId As HiddenField = CType(e.Item.FindControl("hfquestionnaireId"), HiddenField)
                Dim hfAnswerType As HiddenField = CType(e.Item.FindControl("hfAnswerType"), HiddenField)
                Dim hfAskForJustification As HiddenField = CType(e.Item.FindControl("hfAskForJustification"), HiddenField)
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                Dim hfForLineManagerFeedback As HiddenField = CType(e.Item.FindControl("hfForLineManagerFeedback"), HiddenField)
                'Hemant (20 Aug 2021) -- End

                Dim pnlAnswerFreeText As Panel = CType(e.Item.FindControl("pnlAnswerFreeText"), Panel)
                Dim pnlAnswerSelection As Panel = CType(e.Item.FindControl("pnlAnswerSelection"), Panel)
                Dim pnlAnswerDtp As Panel = CType(e.Item.FindControl("pnlAnswerDtp"), Panel)
                Dim pnlAnswerRating As Panel = CType(e.Item.FindControl("pnlAnswerRating"), Panel)
                Dim pnlAnswerNameNum As Panel = CType(e.Item.FindControl("pnlAnswerNameNum"), Panel)
                Dim pnlJustification As Panel = CType(e.Item.FindControl("pnlJustification"), Panel)

                Dim dgvSubQuestion As DataList = CType(e.Item.FindControl("dgvSubQuestion"), DataList)

                Dim pnlSubQuestion As Panel = CType(e.Item.FindControl("pnlSubQuestion"), Panel)
                Dim lblActionPlanGoalDescription As Label = CType(e.Item.FindControl("lblActionPlanGoalDescription"), Label)

                dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))

                If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                    Select Case CInt(hfAnswerType.Value)
                        Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                            pnlAnswerFreeText.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerFreeText.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerFreeText.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                        Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                            pnlAnswerNameNum.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerNameNum.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerNameNum.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                        Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                            pnlAnswerDtp.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerDtp.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerDtp.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                        Case CInt(clseval_question_master.enAnswerType.RATING)
                            pnlAnswerRating.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerRating.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerRating.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                            Dim objEvalAnswer As New clseval_answer_master
                            Dim dsCombo As DataSet = Nothing
                            Dim cboItemNameRating As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameRating"), DropDownList)
                            dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfquestionnaireId.Value))
                            With cboItemNameRating
                                .DataValueField = "id"
                                .DataTextField = "name"
                                .DataSource = dsCombo.Tables("List")
                                .DataBind()
                                .SelectedValue = "0"
                            End With
                            objEvalAnswer = Nothing
                        Case CInt(clseval_question_master.enAnswerType.SELECTION)
                            pnlAnswerSelection.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerSelection.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerSelection.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                            Dim objEvalAnswer As New clseval_answer_master
                            Dim dsCombo As DataSet = Nothing
                            Dim cboItemNameSelection As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameSelection"), DropDownList)
                            dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfquestionnaireId.Value))
                            With cboItemNameSelection
                                .DataValueField = "id"
                                .DataTextField = "name"
                                .DataSource = dsCombo.Tables("List")
                                .DataBind()
                                .SelectedValue = "0"
                            End With
                            objEvalAnswer = Nothing
                        Case Else
                    End Select
                    If CBool(hfAskForJustification.Value) = True Then
                        pnlJustification.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlJustification.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlJustification.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    End If
                    pnlSubQuestion.Visible = False
                Else
                    pnlSubQuestion.Visible = True
                    lblActionPlanGoalDescription.Visible = False
                    AddHandler dgvSubQuestion.ItemDataBound, AddressOf Me.dgvSubQuestion_ItemDataBound
                    dgvSubQuestion.DataSource = dsList.Tables("SubQuestion")
                    dgvSubQuestion.DataBind()
                End If



            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvSubQuestion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Dim dsList As New DataSet
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfSubQuestionId As HiddenField = CType(e.Item.FindControl("hfSubQuestionId"), HiddenField)
                Dim hfQuestionnaireId As HiddenField = CType(e.Item.FindControl("hfQuestionnaireId"), HiddenField)
                Dim hfAnswerType As HiddenField = CType(e.Item.FindControl("hfAnswerType"), HiddenField)
                Dim hfAskForJustification As HiddenField = CType(e.Item.FindControl("hfAskForJustification"), HiddenField)
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                Dim hfForLineManagerFeedback As HiddenField = CType(e.Item.FindControl("hfForLineManagerFeedback"), HiddenField)
                'Hemant (20 Aug 2021) -- End

                Dim pnlAnswerFreeText As Panel = CType(e.Item.FindControl("pnlAnswerFreeText"), Panel)
                Dim pnlAnswerSelection As Panel = CType(e.Item.FindControl("pnlAnswerSelection"), Panel)
                Dim pnlAnswerDtp As Panel = CType(e.Item.FindControl("pnlAnswerDtp"), Panel)
                Dim pnlAnswerRating As Panel = CType(e.Item.FindControl("pnlAnswerRating"), Panel)
                Dim pnlAnswerNameNum As Panel = CType(e.Item.FindControl("pnlAnswerNameNum"), Panel)
                Dim pnlJustification As Panel = CType(e.Item.FindControl("pnlJustification"), Panel)
                Dim dgvSubQuestion As DataList = CType(e.Item.FindControl("dgvSubQuestion"), DataList)


                Select Case CInt(hfAnswerType.Value)
                    Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                        pnlAnswerFreeText.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerFreeText.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerFreeText.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                        pnlAnswerNameNum.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerNameNum.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerNameNum.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                        pnlAnswerDtp.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerDtp.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerDtp.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    Case CInt(clseval_question_master.enAnswerType.RATING)
                        pnlAnswerRating.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerRating.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerRating.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                        Dim objEvalAnswer As New clseval_answer_master
                        Dim dsCombo As DataSet = Nothing
                        Dim cboItemNameRating As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameRating"), DropDownList)
                        dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfQuestionnaireId.Value), CInt(hfSubQuestionId.Value))
                        With cboItemNameRating
                            .DataValueField = "id"
                            .DataTextField = "name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With
                        objEvalAnswer = Nothing
                    Case CInt(clseval_question_master.enAnswerType.SELECTION)
                        pnlAnswerSelection.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerSelection.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerSelection.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                        Dim objEvalAnswer As New clseval_answer_master
                        Dim dsCombo As DataSet = Nothing
                        Dim cboItemNameSelection As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameSelection"), DropDownList)
                        dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfQuestionnaireId.Value), CInt(hfSubQuestionId.Value))
                        With cboItemNameSelection
                            .DataValueField = "id"
                            .DataTextField = "name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With
                        objEvalAnswer = Nothing
                    Case Else
                End Select

                If CBool(hfAskForJustification.Value) = True Then
                    pnlJustification.Visible = True
                    'Hemant (20 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        pnlJustification.Visible = Not CBool(hfForLineManagerFeedback.Value)
                    Else
                        pnlJustification.Enabled = CBool(hfForLineManagerFeedback.Value)
                    End If
                    'Hemant (20 Aug 2021) -- End
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Training Evaluation Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, all questions justifications are mandatory. please attempt all questions justifications  to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, Negative Value is not allowed in Numeric Field.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, your line manager info is not present. Please contact support to proceed.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
