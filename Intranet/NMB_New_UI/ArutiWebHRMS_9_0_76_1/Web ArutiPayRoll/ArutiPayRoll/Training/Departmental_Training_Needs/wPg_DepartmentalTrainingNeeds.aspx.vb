﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.Net.Dns

#End Region

Partial Class Training_Departmental_Training_Needs_wPg_DepartmentalTrainingNeeds
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmDepartmentalTrainingNeeds"
    Private ReadOnly mstrModuleNameList As String = "frmDepartmentalTrainingNeedsList"
    Private ReadOnly mstrModuleSetMaxBudget As String = "frmSetMaxSummaryBudget"


    Private mintDepartmentaltrainingneedunkid As Integer = 0
    Private mintFormId As Integer = 1
    Private mblnShowAddEditPopup As Boolean = False
    'Private mblnShowAddEmployeePopup As Boolean = False
    'Private mblnShowAddAllocEmpPopup As Boolean = False
    'Private mblnShowTResourcesPopup As Boolean = False
    'Private mblnShowFinancingSourcePopup As Boolean = False
    'Private mblnShowTrainingCoordinatorPopup As Boolean = False
    'Private mblnShowTrainingCostItemPopup As Boolean = False
    Private mstrSortOrder As String = ""
    Private mintDelUnkId As Integer = 0
    Private mintActiveInactiveUnkId As Integer = 0
    Private mintActiveInactiveRowIndex As Integer = 0
    Private mblnIsDeptTrainingNeed As Boolean = False
    Private mblnIsDeptTrainingFromBacklog As Boolean = False
    Private mblnIsDeptTrainingBudgetApproval As Boolean = False
    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mintEmailTypeId As Integer = -1
    Private mintTrainingNeedAllocationID As Integer = -1
    Private mstrTrainingNeedAllocationName As String = ""
    'Hemant (26 Mar 2021) -- End
    'Sohail (10 Feb 2022) -- Start
    'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
    Private mintTrainingBudgetAllocationID As Integer = -1
    'Sohail (10 Feb 2022) -- End
    Private mblnIsUnlockSubmitClicked As Boolean = False
    Private mdecPrevTotalCost As Decimal = 0
    Private mdecPrevApprovedTotalCost As Decimal = 0
    Private mstrURLReferer As String = ""
    Private mstrDefaultStatusNOTInIDs As String = String.Empty
    'Sohail (09 Jun 2021) -- Start
    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
    Private mintInstructSrNo As Integer = 0
    'Sohail (09 Jun 2021) -- End

    Private objCONN As SqlConnection
#End Region

#Region " Properties "
    Public Property sortOrder() As String
        Get
            If mstrSortOrder = "desc" Then
                mstrSortOrder = "asc"
            Else
                mstrSortOrder = "desc"
            End If

            Return mstrSortOrder
        End Get
        Set(ByVal value As String)
            ViewState("sortOrder") = value
        End Set
    End Property
#End Region

#Region " Page Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Session("clsuser") Is Nothing Then
            '    Exit Sub
            'End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count >= 2) AndAlso IsPostBack = False Then

                If Request.QueryString.Count > 0 Then
                    KillIdleSQLSessions()

                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString(1).ToString)).Split(CChar("|"))

                    If arr.Length = 2 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False) Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If

                        Dim clsConfig As New clsConfigOptions
                        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        Session("fmtCurrency") = clsConfig._CurrencyFormat
                        Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim
                        Session("SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
                        Session("NotifyPayroll_Users") = clsConfig._Notify_Payroll_Users

                        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Session("ArutiSelfServiceURL") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                        Else
                            Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
                        End If


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try


                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))

                        'HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        CType(Me.Master.FindControl("pnlMenuWrapper"), Panel).Visible = False
                    Else
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
            End If

            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count < 2 Then
                Exit Sub
            End If

            If IsPostBack = False Then

                Dim blnFromNotifLink As Boolean = False

                If CBool(Session("IsArutiDemo")) = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False) Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                mstrSortOrder = ""

                mintFormId = 0
                If Request.QueryString.Count > 0 Then
                    Dim id As Integer = 0
                    Integer.TryParse(Request.QueryString(0), id)

                    Select Case id

                        Case 2
                            mintFormId = 2
                            mblnIsDeptTrainingFromBacklog = True
                            Session("ActiveMenuID") = "menu_411"

                        Case 3
                            mintFormId = 3
                            mblnIsDeptTrainingBudgetApproval = True
                            Session("ActiveMenuID") = "menu_413"
                            blnFromNotifLink = True

                        Case Else
                            mintFormId = 1
                            mblnIsDeptTrainingNeed = True
                            Session("ActiveMenuID") = "menu_410"

                    End Select

                Else

                    If (Session("intFormId") IsNot Nothing AndAlso CInt(Session("intFormId")) = 1) Then
                        mintFormId = 1
                        mblnIsDeptTrainingNeed = True
                    ElseIf (Session("intFormId") IsNot Nothing AndAlso CInt(Session("intFormId")) = 2) Then
                        mintFormId = 2
                        mblnIsDeptTrainingFromBacklog = True
                    ElseIf (Session("intFormId") IsNot Nothing AndAlso CInt(Session("intFormId")) = 3) Then
                        mintFormId = 3
                        mblnIsDeptTrainingBudgetApproval = True
                    Else
                        mintFormId = 1
                        mblnIsDeptTrainingNeed = True
                    End If

                End If


                'Hemant (26 Mar 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                mintTrainingNeedAllocationID = CInt(Session("TrainingNeedAllocationID"))
                'Hemant (26 Mar 2021) -- End
                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                mintTrainingBudgetAllocationID = CInt(Session("TrainingBudgetAllocationID"))
                'Sohail (10 Feb 2022) -- End

                Call FillCombo()
                'Call FillList("", "")

                btnAddList.Enabled = CBool(Session("AllowToAddDepartmentalTrainingNeed"))
                'Sohail (26 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                lnkBudgetSummary.Visible = CBool(Session("AllowToViewBudgetSummaryForDepartmentalTrainingNeed"))
                btnSaveSetMaxBudget.Visible = CBool(Session("AllowToSetMaxBudgetForDepartmentalTrainingNeed"))
                'Sohail (26 Apr 2021) -- End

                If mintFormId = 2 Then
                    lblPageHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleNameList, 68, "Training Backlog")
                    lblHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleNameList, 69, "Training Backlog")
                ElseIf mintFormId = 3 Then
                    lblPageHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleNameList, 70, "Training Budget Approval")
                    lblHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleNameList, 71, "Training Budget Approval")

                    If blnFromNotifLink = False Then
                        FillBudgetCalendar()
                        popupBudgetSummaryApproval.Show()
                    End If
                    btnSaveAndSubmit.Visible = False 'Sohail (04 Aug 2021)
                Else
                    lblPageHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleNameList, 72, "Departmental Training Needs")
                    lblHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleNameList, 73, "Departmental Training Needs")
                End If

                Me.Title = lblPageHeader.Text

                If CStr(Session("DeptTrainingNeedListColumnsIDs")).Trim <> "" Then
                    For Each id As String In CStr(Session("DeptTrainingNeedListColumnsIDs")).Split(CChar(","))
                        chkColumns.Items(CInt(id) - 1).Selected = True
                    Next
                End If

                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPg_EmployeeSkillList.aspx"
                End If

                If Request.QueryString.Count >= 2 Then
                    btnAddList.Visible = False
                End If
            Else
                mintDepartmentaltrainingneedunkid = CInt(ViewState("mintDepartmentaltrainingneedunkid"))

                objlblCostItemTotal.Text = Request.Form(hfobjlblTotalAmt.UniqueID)
                objlblCostItemApprovedTotal.Text = Request.Form(hfobjlblTotalApprovedAmt.UniqueID)
                mblnShowAddEditPopup = CBool(ViewState("mblnShowAddEditPopup"))
                'mblnShowAddEmployeePopup = CBool(ViewState("mblnShowAddEmployeePopup"))
                'mblnShowAddAllocEmpPopup = CBool(ViewState("mblnShowAddAllocEmpPopup"))
                'mblnShowTResourcesPopup = CBool(ViewState("mblnShowTResourcesPopup"))
                'mblnShowFinancingSourcePopup = CBool(ViewState("mblnShowFinancingSourcePopup"))
                'mblnShowTrainingCoordinatorPopup = CBool(ViewState("mblnShowTrainingCoordinatorPopup"))
                'mblnShowTrainingCostItemPopup = CBool(ViewState("mblnShowTrainingCostItemPopup"))
                mstrSortOrder = ViewState("mstrSortOrder").ToString
                mintDelUnkId = CInt(ViewState("mintDelUnkId"))
                mintActiveInactiveUnkId = CInt(ViewState("mintActiveInactiveUnkId"))
                mintActiveInactiveRowIndex = CInt(ViewState("mintActiveInactiveRowIndex"))
                mblnIsDeptTrainingFromBacklog = CBool(ViewState("mblnIsDeptTrainingFromBacklog"))
                'Hemant (26 Mar 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                mblnIsDeptTrainingNeed = CBool(ViewState("mblnIsDeptTrainingNeed"))
                mblnIsDeptTrainingBudgetApproval = CBool(ViewState("mblnIsDeptTrainingBudgetApproval"))
                mintEmailTypeId = CInt(Me.ViewState("mintEmailTypeId"))
                mintTrainingNeedAllocationID = CInt(Me.ViewState("mintTrainingNeedAllocationID"))
                mstrTrainingNeedAllocationName = ViewState("mstrTrainingNeedAllocationName").ToString
                'Hemant (26 Mar 2021) -- End
                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                mintTrainingBudgetAllocationID = CInt(Me.ViewState("mintTrainingBudgetAllocationID"))
                'Sohail (10 Feb 2022) -- End
                mintFormId = CInt(ViewState("mintFormId"))
                mblnIsUnlockSubmitClicked = CBool(ViewState("mblnIsUnlockSubmitClicked"))
                mdecPrevTotalCost = CInt(ViewState("mdecPrevTotalCost"))
                mdecPrevApprovedTotalCost = CInt(ViewState("mdecPrevApprovedTotalCost"))
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                mstrDefaultStatusNOTInIDs = ViewState("mstrDefaultStatusNOTInIDs").ToString
                mintInstructSrNo = CInt(ViewState("mintInstructSrNo")) 'Sohail (09 Jun 2021)
                'Call UpdateCostAmount()
            End If

            If mblnShowAddEditPopup = True Then
                popAddEdit.Show()
            End If

            'If mblnShowAddEmployeePopup = True Then
            '    popupAddEmployee.Show()
            'End If

            'If mblnShowAddAllocEmpPopup = True Then
            '    popupAddAllocEmp.Show()
            'End If

            'If mblnShowTResourcesPopup = True Then
            '    popupAddTResources.Show()
            'End If

            'If mblnShowFinancingSourcePopup = True Then
            '    popupFinancingSource.Show()
            'End If

            'If mblnShowTrainingCoordinatorPopup = True Then
            '    popupAddTrainingCoordinator.Show()
            'End If

            'If mblnShowTrainingCostItemPopup = True Then
            '    popupTrainingCostItem.Show()
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count < 2 Then
            Me.IsLoginRequired = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mintDepartmentaltrainingneedunkid", mintDepartmentaltrainingneedunkid)

            ViewState("mblnShowAddEditPopup") = mblnShowAddEditPopup
            'ViewState("mblnShowAddEmployeePopup") = mblnShowAddEmployeePopup
            'ViewState("mblnShowAddAllocEmpPopup") = mblnShowAddAllocEmpPopup
            'ViewState("mblnShowTResourcesPopup") = mblnShowTResourcesPopup
            'ViewState("mblnShowFinancingSourcePopup") = mblnShowFinancingSourcePopup
            'ViewState("mblnShowTrainingCoordinatorPopup") = mblnShowTrainingCoordinatorPopup
            'ViewState("mblnShowTrainingCostItemPopup") = mblnShowTrainingCostItemPopup
            ViewState("mstrSortOrder") = mstrSortOrder
            ViewState("mintDelUnkId") = mintDelUnkId
            ViewState("mintActiveInactiveUnkId") = mintActiveInactiveUnkId
            ViewState("mintActiveInactiveRowIndex") = mintActiveInactiveRowIndex
            ViewState("mblnIsDeptTrainingFromBacklog") = mblnIsDeptTrainingFromBacklog
            'Hemant (26 Mar 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            ViewState("mblnIsDeptTrainingNeed") = mblnIsDeptTrainingNeed
            ViewState("mblnIsDeptTrainingBudgetApproval") = mblnIsDeptTrainingBudgetApproval
            ViewState("mintEmailTypeId") = mintEmailTypeId
            ViewState("mintTrainingNeedAllocationID") = mintTrainingNeedAllocationID
            ViewState("mstrTrainingNeedAllocationName") = mstrTrainingNeedAllocationName
            'Hemant (26 Mar 2021) -- End
            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            ViewState("mintTrainingBudgetAllocationID") = mintTrainingBudgetAllocationID
            'Sohail (10 Feb 2022) -- End
            ViewState("mintFormId") = mintFormId
            ViewState("mblnIsUnlockSubmitClicked") = mblnIsUnlockSubmitClicked
            ViewState("mdecPrevTotalCost") = mdecPrevTotalCost
            ViewState("mdecPrevApprovedTotalCost") = mdecPrevApprovedTotalCost
            ViewState("mstrURLReferer") = mstrURLReferer
            ViewState("mstrDefaultStatusNOTInIDs") = mstrDefaultStatusNOTInIDs
            ViewState("mintInstructSrNo") = mintInstructSrNo 'Sohail (09 Jun 2021)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objDept As New clsDepartment
        'Hemant (26 Mar 2021) -- Start
        'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
        Dim strName As String = ""
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        'Hemant (26 Mar 2021) -- End
        Dim objTPeriod As New clsTraining_Calendar_Master
        'Dim objPDPGoal As New clsPdpgoals_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim objDeptTNeetMaster As New clsDepartmentaltrainingneed_master
        Dim objTPriority As New clsTraining_Priority_Master
        Dim objTCostItem As New clstrainingitemsInfo_master
        Dim objTVenue As New clstrtrainingvenue_master
        Dim objMaster As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Dim objInstitute As New clsinstitute_master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        
        Try
            'Hemant (26 Mar 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'dsCombo = objDept.getComboList("List", True)
            'Dim dr() As DataRow = dsCombo.Tables(0).Select("departmentunkid = 0 ")
            'If dr.Length > 0 Then
            '    dr(0).Item("Name") = " " & dr(0).Item("Name").ToString
            '    dsCombo.Tables(0).AcceptChanges()
            'End If
            Select Case mintTrainingNeedAllocationID
                Case enAllocation.BRANCH
                    dsCombo = objStation.getComboList("Station", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 430, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombo = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 429, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombo = objDept.getComboList("Department", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 428, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombo = objSectionGrp.getComboList("List", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 427, "Section Group")
                Case enAllocation.SECTION
                    dsCombo = objSection.getComboList("Section", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 426, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombo = objUnitGroup.getComboList("List", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 425, "Unit Group")
                Case enAllocation.UNIT
                    dsCombo = objUnit.getComboList("Unit", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 424, "Unit")
                Case enAllocation.TEAM
                    dsCombo = objTeam.getComboList("List", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 423, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombo = objJobGrp.getComboList("JobGrp", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 422, "Job Group")
                Case enAllocation.JOBS
                    dsCombo = objJob.getComboList("Job", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 421, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombo = objClassGrp.getComboList("ClassGrp", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 420, "Class Group")
                Case enAllocation.CLASSES
                    dsCombo = objClass.getComboList("Class", True)
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 419, "Classes")
                Case Else
                    dsCombo = Nothing
                    strName = "&nbsp;"
            End Select

            If dsCombo IsNot Nothing Then
                Dim dr() As DataRow = dsCombo.Tables(0).Select("id = 0 ")
                If dr.Length > 0 Then
                    dr(0).Item("Name") = "  " & dr(0).Item("Name").ToString
                    dsCombo.Tables(0).AcceptChanges()
                End If
                'Sohail (06 May 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                If mintFormId = 2 OrElse mintFormId = 3 Then
                    Dim r As DataRow = dsCombo.Tables(0).NewRow
                    r.Item("Id") = -99
                    r.Item("name") = " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 82, "Company")
                    dsCombo.Tables(0).Rows.InsertAt(r, 1)
                End If
                'Sohail (06 May 2021) -- End
                'Hemant (26 Mar 2021) -- End            
                If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsCombo.Tables(0), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "Name", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "", "Name", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            'Hemant (26 Mar 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'With cboDepartmentList
            '    .DataTextField = "name"
            '    .DataValueField = "departmentunkid"
            '    .DataSource = dtTable
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            lblDepartmentList.Text = strName
            mstrTrainingNeedAllocationName = strName
            If dsCombo IsNot Nothing Then

                With cboDepartmentList
                    .DataValueField = "Id"
                    .DataTextField = "name"
                    .DataSource = dtTable
                    .DataBind()
                    .SelectedValue = "0"
                End With

            Else
                cboDepartmentList.DataSource = Nothing
                cboDepartmentList.DataBind()
                cboDepartmentList.Items.Clear()
            End If
            'Hemant (26 Mar 2021) -- End
            Call cboDepartmentList_SelectedIndexChanged(cboDepartmentList, New System.EventArgs)

            Dim intFirstOpen As Integer = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                intFirstOpen = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If
            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriodList
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen.ToString
            End With

            dsCombo = objTPeriod.getListForCombo("List", True, 1)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen.ToString
            End With

            'dsCombo = objPDPGoal.GetGoalListForCombo("List", True)
            'With cboCompetences
            '    .DataTextField = "goal_name"
            '    .DataValueField = "pdpgoalsmstunkid"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            dsCombo = objDeptTNeetMaster.getCompetenceListForCombo(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), "List", True)
            With cboCompetences
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboCompetences_SelectedIndexChanged(cboCompetences, New System.EventArgs)

            dsCombo = objTCategory.getListForCombo("List", True)
            With cboTrainingCategoryList
                .DataTextField = "categoryname"
                .DataValueField = "categoryunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Hemant (15 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'With cboTrainingCategory
            '    .DataTextField = "categoryname"
            '    .DataValueField = "categoryunkid"
            '    .DataSource = dsCombo.Tables(0).Copy
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'Hemant (15 Apr 2021) -- End


            dsCombo = objTPriority.getListForCombo("List", True)
            With cboPriorityList
                .DataTextField = "priority"
                .DataValueField = "trpriorityunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboPriority
                .DataTextField = "priority"
                .DataValueField = "trpriorityunkid"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With


            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataTextField = "name"
                .DataValueField = "masterunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'With cboTrainingNeeded
            '    .DataTextField = "name"
            '    .DataValueField = "masterunkid"
            '    .DataSource = dsCombo.Tables(0).Copy
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With

            dsCombo = objInstitute.getListForCombo(False, "List", True)
            With cboTrainingProvider
                .DataTextField = "name"
                .DataValueField = "instituteunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (26 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'dsCombo = objDeptTNeetMaster.getStatusComboList("List", True)
            If mintFormId = 2 Then 'Backlog
                mstrDefaultStatusNOTInIDs = " " & clsDepartmentaltrainingneed_master.enApprovalStatus.Pending & ", " & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & " "
                dsCombo = objDeptTNeetMaster.getStatusComboList("List", True, "", mstrDefaultStatusNOTInIDs)
            ElseIf mintFormId = 3 Then 'Budget Approval
                mstrDefaultStatusNOTInIDs = " " & clsDepartmentaltrainingneed_master.enApprovalStatus.Pending & ", " & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed & " "
                dsCombo = objDeptTNeetMaster.getStatusComboList("List", True, "", mstrDefaultStatusNOTInIDs)
            Else 'Departmental Training Need
                mstrDefaultStatusNOTInIDs = " " & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed & ", " & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & " "
                dsCombo = objDeptTNeetMaster.getStatusComboList("List", True, "", mstrDefaultStatusNOTInIDs)
            End If
            'If mintFormId = 2 Then
            '    dtTable = New DataView(dsCombo.Tables(0), "Id > " & clsDepartmentaltrainingneed_master.enApprovalStatus.Pending & " ", "", DataViewRowState.CurrentRows).ToTable
            'ElseIf mintFormId = 3 Then
            '    dtTable = New DataView(dsCombo.Tables(0), "Id > " & clsDepartmentaltrainingneed_master.enApprovalStatus.TentativeApproved & " ", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsCombo.Tables(0)).ToTable
            'End If
                dtTable = New DataView(dsCombo.Tables(0)).ToTable
            With cboStatusList
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dtTable
                .DataBind()
                If dtTable.Rows.Count >= 2 Then
                    .SelectedIndex = 1 'by default first filter
                End If
            End With
            'Sohail (26 Apr 2021) -- End

            'Sohail (26 Mar 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'dsCombo = objMaster.GetTrainingTargetedGroup("List", "", False, True)
            Dim strIDs As String = "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Trim <> "" Then
                Dim arr() As String = Session("Allocation_Hierarchy").ToString.Split(CChar("|"))
                Dim sIDs As String = String.Join(",", (From p In arr.AsEnumerable Where (CInt(p) >= mintTrainingNeedAllocationID) Select (p.ToString)).ToArray)
                If sIDs.Trim <> "" Then
                    strIDs = "0, 9, 10, " & sIDs
                End If
            End If
            dsCombo = objMaster.GetTrainingTargetedGroup("List", strIDs, False, False)
            'Sohail (26 Mar 2021) -- End

            With cboTargetedGroup
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                If dsCombo.Tables(0).Select("ID = " & mintTrainingNeedAllocationID & " ").Length > 0 Then
                    .SelectedValue = mintTrainingNeedAllocationID.ToString
                Else
                    .SelectedValue = "0"
                End If
            End With
            'Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)

            dsCombo = objMaster.GetTrainingTargetedGroup("List", strIDs, False, True)
            With cboTargetedGroupList
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = "-1"
            End With
            Call cboTargetedGroupList_SelectedIndexChanged(cboTargetedGroupList, New System.EventArgs)

            dsCombo = objTCostItem.getListForCombo(clstrainingitemsInfo_master.enTrainingItem.Learning_Method, "List", True)
            With cboLearningMethod
                .DataTextField = "name"
                .DataValueField = "infounkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            'dsCombo = objTVenue.getListForCombo("List", True)
            'With cboTrainingVenue
            '    .DataTextField = "name"
            '    .DataValueField = "venueunkid"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'Hemant (03 Jun 2021) -- End


            dsCombo = objDeptTNeetMaster.GetIncludeColumnsList("List", False)
            With chkColumns
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objMaster.GetCondition(False, True, True, True, False)
            With cboConditionList
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CInt(enComparison_Operator.EQUAL).ToString
            End With

            'Sohail (26 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'If mintFormId = 2 Then
            '    dtTable = New DataView(dsCombo.Tables(0), "Id > " & clsDepartmentaltrainingneed_master.enApprovalStatus.Pending & " ", "", DataViewRowState.CurrentRows).ToTable
            'ElseIf mintFormId = 3 Then
            '    dtTable = New DataView(dsCombo.Tables(0), "Id > " & clsDepartmentaltrainingneed_master.enApprovalStatus.TentativeApproved & " ", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsCombo.Tables(0)).ToTable
            'End If
            'With cboStatusList
            '    .DataTextField = "name"
            '    .DataValueField = "id"
            '    .DataSource = dtTable
            '    .DataBind()
            'End With
            'Sohail (26 Apr 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDept = Nothing
            objTPeriod = Nothing
            'objPDPGoal = Nothing
            objTCategory = Nothing
            objDeptTNeetMaster = Nothing
            objTPriority = Nothing
            objTCostItem = Nothing
            objTVenue = Nothing
            objMaster = Nothing
            objCommon = Nothing
            objInstitute = Nothing
        End Try
    End Sub

    Private Sub FillList(ByVal strSortExpression As String, ByVal strSortOrder As String, Optional ByVal blnApplyDefaultStatusFilter As Boolean = True)
        Dim objDeptTraining As New clsDepartmentaltrainingneed_master
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Dim blnIncChildData As Boolean = False
        'Sohail (04 Aug 2021) -- Start
        'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
        Dim strFilterTitle As String = ""
        pnlFilterTitle.Controls.Clear()
        'Sohail (04 Aug 2021) -- End
        Try
            If CBool(Session("AllowToViewDepartmentalTrainingNeed")) = False Then Exit Try

            If rdbDetailedList.Checked = True Then blnIncChildData = True
            If cboEmployeeNameList.SelectedValue = Nothing Then Exit Try

            Dim intEmpUnkId As Integer = 0
            Dim intAllocationtranUnkId As Integer = 0
            If CInt(cboEmployeeNameList.SelectedValue) > 0 Then
                If CInt(cboTargetedGroupList.SelectedValue) <= 0 Then 'Employee Names
                    intEmpUnkId = CInt(cboEmployeeNameList.SelectedValue)
                Else
                    intAllocationtranUnkId = CInt(cboEmployeeNameList.SelectedValue)
                End If
            End If

            'Hemant (26 Mar 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            If mintTrainingNeedAllocationID > 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.allocationid = " & CInt(mintTrainingNeedAllocationID) & " "
            End If
            'Hemant (26 Mar 2021) -- End

            If CInt(cboDepartmentList.SelectedValue) <> 0 OrElse mintFormId = 1 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.departmentunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
            End If

            If CInt(cboTargetedGroupList.SelectedValue) >= 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.targetedgroupunkid = " & CInt(cboTargetedGroupList.SelectedValue) & " "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblTargetedGroupList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboTargetedGroupList.SelectedItem.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            If CInt(cboPeriodList.SelectedValue) > 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.periodunkid = " & CInt(cboPeriodList.SelectedValue) & " "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblPeriodList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboPeriodList.SelectedItem.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            If CInt(cboPriorityList.SelectedValue) > 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.trainingpriority = " & CInt(cboPriorityList.SelectedValue) & " "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblPriorityList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboPriorityList.SelectedItem.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            If CInt(cboTrainingName.SelectedValue) > 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.trainingcourseunkid = " & CInt(cboTrainingName.SelectedValue) & " "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblTrainingName.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboTrainingName.SelectedItem.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            If CInt(cboTrainingCategoryList.SelectedValue) > 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.trainingcategoryunkid = " & CInt(cboTrainingCategoryList.SelectedValue) & " "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblTrainingCategoryList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboTrainingCategoryList.SelectedItem.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            If txtTotalCostList.Decimal_ <> 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.totalcost " & cboConditionList.SelectedItem.Text & " " & txtTotalCostList.Decimal_ & " "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblTotalCostList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboConditionList.SelectedItem.Text & " " & txtTotalCostList.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            If cboStatusList.SelectedValue <> Nothing AndAlso CInt(cboStatusList.SelectedValue) >= 0 Then
                strFilter &= " AND trdepartmentaltrainingneed_master.statusunkid = " & CInt(cboStatusList.SelectedValue) & " " & _
                             " AND trdepartmentaltrainingneed_master.isactive = 1 "
            ElseIf cboStatusList.SelectedValue <> Nothing AndAlso CInt(cboStatusList.SelectedValue) < 0 AndAlso blnApplyDefaultStatusFilter = True Then
                strFilter &= " AND trdepartmentaltrainingneed_master.statusunkid NOT IN (" & mstrDefaultStatusNOTInIDs & ") "
            End If

            'Sohail (04 Aug 2021) -- Start
            'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
            If CInt(cboStatusList.SelectedValue) >= 0 Then
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblStatusList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & cboStatusList.SelectedItem.Text
                pnlFilterTitle.Controls.Add(lbl)
            End If
            'Sohail (04 Aug 2021) -- End

            If txtRefnoList.Text.Trim <> "" Then
                strFilter &= " AND trdepartmentaltrainingneed_master.refno = '" & txtRefnoList.Text & "' "
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement :  : Show Filter Title on departmental training need list screen.
                Dim lbl As New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Font.Bold = True
                lbl.Text = lblRefnoList.Text
                pnlFilterTitle.Controls.Add(lbl)

                lbl = New Label
                lbl.ID = "obj" & Guid.NewGuid.ToString
                lbl.Style.Add("padding-right", "10px")
                lbl.Text = " : " & txtRefnoList.Text
                pnlFilterTitle.Controls.Add(lbl)
                'Sohail (04 Aug 2021) -- End
            End If

            dsList = objDeptTraining.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, mintTrainingNeedAllocationID, "List", blnIncChildData, strFilter, intEmpUnkId, intAllocationtranUnkId)


            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dsList.Tables(0).Rows(0).Item("isactive") = False
                gvDeptTrainingeedList.DataSource = dsList
                gvDeptTrainingeedList.DataBind()

                Dim intCellCount As Integer = gvDeptTrainingeedList.Rows(0).Cells.Count

                gvDeptTrainingeedList.Rows(0).Cells(0).Visible = False
                gvDeptTrainingeedList.Rows(0).Cells(1).Visible = False
                gvDeptTrainingeedList.Rows(0).Cells(2).Visible = False
                gvDeptTrainingeedList.Rows(0).Cells(3).Visible = False
                gvDeptTrainingeedList.Rows(0).Cells(4).ColumnSpan = intCellCount
                For i As Integer = 5 To intCellCount - 1
                    gvDeptTrainingeedList.Rows(0).Cells(i).Visible = False
                Next
                gvDeptTrainingeedList.Rows(0).Cells(4).Text = "No Records Found"

                objGrandTotalList.Text = Format(0, CStr(Session("fmtCurrency")))
                objApprovedGrandTotalList.Text = Format(0, CStr(Session("fmtCurrency")))
            Else
                Dim dv As New DataView()
                dv = dsList.Tables(0).DefaultView

                Dim strUp As String = "<i class='fas fa-caret-up'></i>"
                Dim strDown As String = "<i class='fas fa-caret-down'></i>"
                For Each col As DataControlField In gvDeptTrainingeedList.Columns
                    col.HeaderText = col.HeaderText.Replace(strUp, "")
                    col.HeaderText = col.HeaderText.Replace(strDown, "")
                Next

                If strSortExpression <> String.Empty Then
                    dv.Sort = String.Format("{0} {1}", strSortExpression, strSortOrder)

                    Dim icol = CObj(Nothing)
                    icol = gvDeptTrainingeedList.Columns.Cast(Of DataControlField)().Where(Function(x) x.SortExpression = strSortExpression).[Select](Function(x) x).FirstOrDefault()
                    Dim idx As Integer = gvDeptTrainingeedList.Columns.IndexOf(CType(icol, DataControlField))
                    If strSortOrder.Trim.ToLower = "desc" Then
                        'Dim p As New HtmlGenericControl("i")
                        'p.InnerHtml = "class='fas fa-caret-down'"
                        'gvDeptTrainingeedList.HeaderRow.Cells(idx).Controls.Add(p)
                        gvDeptTrainingeedList.Columns(idx).HeaderText += " " + strDown
                    Else
                        'Dim p As New HtmlGenericControl("i")
                        'p.InnerHtml = "class='fas fa-caret-up'"
                        'gvDeptTrainingeedList.HeaderRow.Cells(idx).Controls.Add(p)
                        gvDeptTrainingeedList.Columns(idx).HeaderText += " " + strUp
                    End If
                Else
                    dv.Sort = String.Format("{0} {1}", "trainingcoursename, departmentaltrainingneedunkid", "ASC")
                End If
                gvDeptTrainingeedList.DataSource = dv
                gvDeptTrainingeedList.DataBind()

                Dim decGTotal As Decimal = 0
                If rdbSummary.Checked = True Then
                    decGTotal = (From p In dsList.Tables(0) Where (CBool(p.Item("isactive")) = True AndAlso CInt(p.Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected AndAlso IsDBNull(p.Item("totalcost")) = False) Select (CDec(p.Item("totalcost")))).Sum
                Else
                    decGTotal = (From p In dsList.Tables(0) Where (CBool(p.Item("isactive")) = True AndAlso CInt(p.Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected AndAlso IsDBNull(p.Item("amount")) = False) Select (CDec(p.Item("amount")))).Sum
                End If

                objGrandTotalList.Text = Format(decGTotal, CStr(Session("fmtCurrency")))


                Dim decApprovedGTotal As Decimal = 0
                If rdbSummary.Checked = True Then
                    decApprovedGTotal = (From p In dsList.Tables(0) Where (CBool(p.Item("isactive")) = True AndAlso CInt(p.Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved AndAlso IsDBNull(p.Item("approved_totalcost")) = False) Select (CDec(p.Item("approved_totalcost")))).Sum
                    'decApprovedGTotal = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkActive"), CheckBox).Checked = True AndAlso CType(x.FindControl("lbltotalapprovedtrainingcost"), Label).Text.Trim <> "").Select(Function(x) CDec(CType(x.FindControl("lbltotalapprovedtrainingcost"), Label).Text)).Sum
                Else
                    decApprovedGTotal = (From p In dsList.Tables(0) Where (CBool(p.Item("isactive")) = True AndAlso CInt(p.Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved AndAlso IsDBNull(p.Item("approved_amount")) = False) Select (CDec(p.Item("approved_amount")))).Sum
                End If

                objApprovedGrandTotalList.Text = Format(decApprovedGTotal, CStr(Session("fmtCurrency")))

                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhedit")).Visible = CBool(Session("AllowToEditDepartmentalTrainingNeed"))
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhdelete")).Visible = CBool(Session("AllowToDeleteDepartmentalTrainingNeed"))
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhactive")).Visible = CBool(Session("AllowToDeleteDepartmentalTrainingNeed"))
                If CInt(cboStatusList.SelectedValue) < 0 Then
                    gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhselect")).Visible = False
                Else
                    gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhselect")).Visible = True
                End If

                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhlearningmethodname")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingprovidername")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingvenuename")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingresourcename")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhfinancingsourcename")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhcoordinatorname")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhiscertirequired")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhremark")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhinfo_name")).Visible = False
                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhamount")).Visible = False

                If CStr(Session("DeptTrainingNeedListColumnsIDs")).Trim <> "" Then
                    For Each id As String In CStr(Session("DeptTrainingNeedListColumnsIDs")).Split(CChar(","))

                        Select Case CInt(id)

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.LearningMethod
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhlearningmethodname")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingProvider
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingprovidername")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingVenue
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingvenuename")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingResourcesNeeded
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingresourcename")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.FinancingSource
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhfinancingsourcename")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingCoordinator
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhcoordinatorname")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingCertificateRequired
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhiscertirequired")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.CommentRemark
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhremark")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingCostItem
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhinfo_name")).Visible = True

                            Case clsDepartmentaltrainingneed_master.enIncludeColumnList.TrainingCostAmount
                                gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhamount")).Visible = True

                        End Select

                    Next
                End If

            End If

            'Hemant (26 Mar 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            btnSubmitForApprovalFromDeptTNeed.Visible = False
            'btnTentativeApprove.Visible = False 'Sohail (26 Apr 2021)
            btnSubmitForApprovalFromTBacklog.Visible = False
            btnFinalApprove.Visible = False
            btnReject.Visible = False
            btnAskForReview.Visible = False
            btnUnlockSubmitApproval.Visible = False
            btnUndoApproved.Visible = False
            btnUndoRejected.Visible = False
            If CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                btnSubmitForApprovalFromDeptTNeed.Visible = CBool(Session("AllowToSubmitForApprovalFromDepartmentalTrainingNeed"))
            ElseIf CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed AndAlso mintFormId = 2 Then
                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                'btnTentativeApprove.Visible = CBool(Session("AllowToTentativeApproveForDepartmentalTrainingNeed"))
                'btnReject.Visible = CBool(Session("AllowToTentativeApproveForDepartmentalTrainingNeed"))
                btnSubmitForApprovalFromTBacklog.Visible = CBool(Session("AllowToSubmitForApprovalFromTrainingBacklog"))
                'Hemant (15 Apr 2021) -- End               
                btnUnlockSubmitApproval.Visible = CBool(Session("AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed"))
            ElseIf CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.AskedForReviewAsPerAmountSet AndAlso mintFormId = 2 Then
                btnSubmitForApprovalFromTBacklog.Visible = CBool(Session("AllowToSubmitForApprovalFromTrainingBacklog"))
            ElseIf CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.TentativeApproved AndAlso (mintFormId = 2 Or mintFormId = 3) Then
                btnSubmitForApprovalFromTBacklog.Visible = CBool(Session("AllowToSubmitForApprovalFromTrainingBacklog"))
            ElseIf CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog AndAlso mintFormId = 3 Then
                btnFinalApprove.Visible = CBool(Session("AllowToFinalApproveDepartmentalTrainingNeed"))
                btnReject.Visible = CBool(Session("AllowToRejectDepartmentalTrainingNeed"))
                btnAskForReview.Visible = CBool(Session("AllowToAskForReviewTrainingAsPerAmountSet"))
                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                btnUnlockSubmitApproval.Visible = CBool(Session("AllowToUnlockSubmittedForTrainingBudgetApproval"))
            ElseIf CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved AndAlso mintFormId = 3 Then
                btnUndoApproved.Visible = CBool(Session("AllowToUndoApprovedTrainingBudgetApproval"))
                'Hemant (15 Apr 2021) -- End
                'Sohail (12 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            ElseIf CInt(cboStatusList.SelectedValue) = clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected AndAlso mintFormId = 3 Then
                btnUndoRejected.Visible = CBool(Session("AllowToUndoRejectedTrainingBudgetApproval"))
                'Sohail (12 Apr 2021) -- End
            End If
            'Hemant (26 Mar 2021) -- End

            If (CInt(cboStatusList.SelectedValue) <= clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog) AndAlso mintFormId = 3 Then
                'lblApprovedGrandTotalList.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 163, "Cost to be approved : ")
                'gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).HeaderText = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 162, "Cost to be approved")
                CType(gvDeptTrainingeedList.HeaderRow.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).Controls(0), LinkButton).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 74, "Cost to be approved")
            Else
                'lblApprovedGrandTotalList.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 161, "Total Approved Cost : ")
                'gvDeptTrainingeedList.Columns(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).HeaderText = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 161, "Total Approved Cost")
                CType(gvDeptTrainingeedList.HeaderRow.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).Controls(0), LinkButton).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 75, "Total Approved Cost")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillTargetedGroup()
        Dim objDTAllocation As New clsDepttrainingneed_allocation_Tran
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim dsTargetGroup As DataSet = Nothing
        Dim intColType As Integer = 0
        Try
            
            If dtpStartDate.GetDate = Nothing OrElse dtpEndDate.GetDate = Nothing Then
                dgvEmployee.DataSource = Nothing
                dgvEmployee.DataBind()
                Exit Try
            End If


            Select Case CInt(cboTargetedGroup.SelectedValue)

                Case 0 'Employee Names

                    'Sohail (24 Nov 2021) -- Start
                    'Internal Issue :  : Filtering employee based on training allocation.
                    Dim strEmpFilter As String = ""
                    Select Case mintTrainingNeedAllocationID
                        Case enAllocation.BRANCH
                            strEmpFilter = " T.stationunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.DEPARTMENT_GROUP
                            strEmpFilter = " T.deptgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.DEPARTMENT
                            strEmpFilter = " T.departmentunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.SECTION_GROUP
                            strEmpFilter = " T.sectiongroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.SECTION
                            strEmpFilter = " T.stationunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.UNIT_GROUP
                            strEmpFilter = " T.unitgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.UNIT
                            strEmpFilter = " T.unitunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.TEAM
                            strEmpFilter = " T.teamunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.JOB_GROUP
                            strEmpFilter = " J.jobgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.JOBS
                            strEmpFilter = " J.jobunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.CLASS_GROUP
                            strEmpFilter = " T.classgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                        Case enAllocation.CLASSES
                            strEmpFilter = " T.classunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                    End Select
                    'Sohail (24 Nov 2021) -- End

                    Dim objEmp As New clsEmployee_Master
                    dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    dtpStartDate.GetDate.Date, _
                                                    dtpEndDate.GetDate.Date, _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    False, "Emp", False, , strFilter:=strEmpFilter)
                    'Sohail (24 Nov 2021) - [Removed: CInt(cboDepartmentList.SelectedValue), strEmpFilter]
                    'Hemant (14 Oct 2021) -- Start             
                    'ISSUE : OLD-489(Finca Uganda) - Inactive employees showing on departmental training needs screens
                    'xIncludeIn_ActiveEmployee := True --> xIncludeIn_ActiveEmployee := False
                    'Hemant (14 Oct 2021) -- End

                    intColType = 5

                    dtTable = dsList.Tables(0)
                    objEmp = Nothing

                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                    'If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
                    '    If CInt(Session("UserId")) > 0 Then
                    '        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    '    Else
                    '        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    '    End If
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " stationunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                    'If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " deptgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                    'If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " departmentunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION_GROUP

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                    'If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " sectiongroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                    'If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " sectionunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT_GROUP

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                    'If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " unitgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                    'If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " unitunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                    'If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " teamunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOB_GROUP

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                    'If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " jobgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOBS

                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                    'If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " jobunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.CLASS_GROUP

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                    'If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " classgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.CLASSES

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                    'If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
                    '    Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
                    '    StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                    '    dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " classesunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                    'Case enAllocation.GradeGroup

                    '    Dim objGradeGrp As New clsGradeGroup
                    '    dsList = objGradeGrp.GetList("List", True)
                    '    objGradeGrp = Nothing

                    '    dtTable = dsList.Tables("List")

                    'Case enAllocation.Grade

                    '    Dim objGrade As New clsGrade
                    '    dsList = objGrade.GetList("List", True)

                    '    dtTable = dsList.Tables("List")

                    'Case enAllocation.GradeLevel

                    '    Dim objGradeLvl As New clsGradeLevel
                    '    dsList = objGradeLvl.GetList("List", True)
                    '    objGradeLvl = Nothing

                    '    dtTable = dsList.Tables("List")

                Case enAllocation.COST_CENTER

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2
                    'dtTable = dsList.Tables("List")
                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " costcenterunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("code").ColumnName = "Code"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobunkid").ColumnName = "Id"
                dtTable.Columns("Code").ColumnName = "Code"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentercode").ColumnName = "Code"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(2).ColumnName = "Code"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_code").ColumnName = "Code"
                dtTable.Columns("country_name").ColumnName = "Name"
            ElseIf intColType = 5 Then
                dtTable.Columns("employeeunkid").ColumnName = "Id"
                dtTable.Columns("employeecode").ColumnName = "Code"
                dtTable.Columns("employeename").ColumnName = "Name"
            End If

            dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()

                Dim intCellCount As Integer = dgvEmployee.Rows(0).Cells.Count

                dgvEmployee.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvEmployee.Rows(0).Cells(i).Visible = False
                Next
                dgvEmployee.Rows(0).Cells(0).Text = "No Records Found"
            Else

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case 0 'Employee Names
                        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
                        dsTargetGroup = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                True, "Emp", True, "", mintDepartmentaltrainingneedunkid)


                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                        End If
                    Case Else
                        dsTargetGroup = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)

                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("allocationtranunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                        Else
                            For Each dtRow As DataRow In dtTable.Rows
                                dtRow.Item("IsChecked") = True
                            Next
                        End If
                End Select

                dtTable = New DataView(dtTable, "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()
                If dtTable.Select("IsChecked = 1 ").Length = dgvEmployee.Rows.Count Then
                    CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
                End If
                chkEmpOnlyTicked.Checked = False

                Call FillAllocEmployee()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

#Region " Old FillTargetedGroup() "
    'Private Sub FillTargetedGroup()
    '    Dim objDTAllocation As New clsDepttrainingneed_allocation_Tran
    '    Dim dsList As DataSet = Nothing
    '    Dim dtTable As DataTable = Nothing
    '    Dim intColType As Integer = 0
    '    Try

    '        Select Case CInt(cboTargetedGroup.SelectedValue)

    '            Case 0 'Employee Names

    '                Dim objDTEmp As New clsDepttrainingneed_employee_Tran
    '                dsList = objDTEmp.GetList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
    '                                        eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
    '                                        CStr(Session("UserAccessModeSetting")), True, _
    '                                        True, "Emp", True, "", mintDepartmentaltrainingneedunkid)

    '                intColType = 5

    '                dtTable = dsList.Tables(0)

    '            Case enAllocation.BRANCH
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objBranch As New clsStation
    '                'dsList = objBranch.GetList("List", True)
    '                'objBranch = Nothing

    '                If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
    '                    If CInt(Session("UserId")) > 0 Then
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    End If
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.DEPARTMENT_GROUP
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objDeptGrp As New clsDepartmentGroup
    '                'dsList = objDeptGrp.GetList("List", True)
    '                'objDeptGrp = Nothing

    '                If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.DEPARTMENT
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objDept As New clsDepartment
    '                'dsList = objDept.GetList("List", True)
    '                'objDept = Nothing

    '                If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.SECTION_GROUP
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objSG As New clsSectionGroup
    '                'dsList = objSG.GetList("List", True)
    '                'objSG = Nothing

    '                If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.SECTION
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objSection As New clsSections
    '                'dsList = objSection.GetList("List", True)
    '                'objSection = Nothing

    '                If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.UNIT_GROUP
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objUG As New clsUnitGroup
    '                'dsList = objUG.GetList("List", True)
    '                'objUG = Nothing

    '                If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.UNIT
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objUnit As New clsUnits
    '                'dsList = objUnit.GetList("List", True)
    '                'objUnit = Nothing

    '                If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.TEAM
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objTeam As New clsTeams
    '                'dsList = objTeam.GetList("List", True)
    '                'objTeam = Nothing

    '                If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.JOB_GROUP
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objjobGRP As New clsJobGroup
    '                'dsList = objjobGRP.GetList("List", True)
    '                'objjobGRP = Nothing

    '                If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.JOBS
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objJobs As New clsJobs
    '                'dsList = objJobs.GetList("List", True)
    '                'objJobs = Nothing

    '                'intColType = 1

    '                If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.CLASS_GROUP
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objClassGrp As New clsClassGroup
    '                'dsList = objClassGrp.GetList("List", True)
    '                'objClassGrp = Nothing

    '                If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.CLASSES
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objClass As New clsClass
    '                'dsList = objClass.GetList("List", True)
    '                'objClass = Nothing

    '                If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
    '                    Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
    '                    StrFilter = StrFilter.Replace("classunkid", "classesunkid")
    '                    dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '                'Case enAllocation.GradeGroup
    '                '    dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                '    'Dim objGradeGrp As New clsGradeGroup
    '                '    'dsList = objGradeGrp.GetList("List", True)
    '                '    'objGradeGrp = Nothing

    '                '    dtTable = dsList.Tables("List")

    '                'Case enAllocation.Grade
    '                '    dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                '    'Dim objGrade As New clsGrade
    '                '    'dsList = objGrade.GetList("List", True)

    '                '    dtTable = dsList.Tables("List")

    '                'Case enAllocation.GradeLevel
    '                '    dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                '    'Dim objGradeLvl As New clsGradeLevel
    '                '    'dsList = objGradeLvl.GetList("List", True)
    '                '    'objGradeLvl = Nothing

    '                '    dtTable = dsList.Tables("List")

    '            Case enAllocation.COST_CENTER
    '                dsList = objDTAllocation.GetList("List", mintDepartmentaltrainingneedunkid)
    '                'Dim objConstCenter As New clscostcenter_master
    '                'dsList = objConstCenter.GetList("List", True)
    '                'objConstCenter = Nothing

    '                'intColType = 2
    '                dtTable = dsList.Tables("List")

    '        End Select

    '        If intColType = 0 Then
    '            dtTable.Columns("depttrainingneedallocationtranunkid").ColumnName = "Id"
    '            'dtTable.Columns("allocationtranunkid").ColumnName = "allocationtranunkid"
    '            'dtTable.Columns("code").ColumnName = "Code"
    '            'dtTable.Columns("name").ColumnName = "Name"
    '            dtTable.Columns("AllocationCode").ColumnName = "Code"
    '            dtTable.Columns("AllocationName").ColumnName = "Name"
    '        ElseIf intColType = 1 Then
    '            'dtTable.Columns("jobgroupunkid").ColumnName = "Id"
    '            'dtTable.Columns("Code").ColumnName = "Code"
    '            'dtTable.Columns("jobname").ColumnName = "Name"
    '        ElseIf intColType = 2 Then
    '            'dtTable.Columns(0).ColumnName = "Id"
    '            'dtTable.Columns("costcentercode").ColumnName = "Code"
    '            'dtTable.Columns("costcentername").ColumnName = "Name"
    '        ElseIf intColType = 3 Then
    '            'dtTable.Columns(1).ColumnName = "Id"
    '            'dtTable.Columns(2).ColumnName = "Code"
    '            'dtTable.Columns(0).ColumnName = "Name"
    '        ElseIf intColType = 4 Then
    '            'dtTable.Columns(0).ColumnName = "Id"
    '            'dtTable.Columns("country_code").ColumnName = "Code"
    '            'dtTable.Columns("country_name").ColumnName = "Name"
    '        ElseIf intColType = 5 Then
    '            dtTable.Columns("depttrainingneedemployeetranunkid").ColumnName = "Id"
    '            dtTable.Columns("employeeunkid").ColumnName = "allocationtranunkid"
    '            dtTable.Columns("employeecode").ColumnName = "Code"
    '            dtTable.Columns("employeename").ColumnName = "Name"
    '        End If

    '        dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

    '        Dim dtCol As New DataColumn
    '        dtCol.ColumnName = "IsChecked"
    '        dtCol.Caption = ""
    '        dtCol.DataType = System.Type.GetType("System.Boolean")
    '        dtCol.DefaultValue = False
    '        dtTable.Columns.Add(dtCol)

    '        'If mintDepartmentaltrainingneedunkid > 0 OrElse dtTable.Rows.Count > 0 Then
    '        If dtTable.Rows.Count > 0 Then
    '            cboTargetedGroup.Enabled = False
    '        Else
    '            cboTargetedGroup.Enabled = True
    '        End If

    '        If dtTable.Rows.Count <= 0 Then
    '            dtTable.Rows.Add(dtTable.NewRow)
    '            dgvEmployee.DataSource = dtTable
    '            dgvEmployee.DataBind()

    '            Dim intCellCount As Integer = dgvEmployee.Rows(0).Cells.Count

    '            dgvEmployee.Rows(0).Cells(0).Visible = False
    '            dgvEmployee.Rows(0).Cells(1).ColumnSpan = intCellCount
    '            For i As Integer = 2 To intCellCount - 1
    '                dgvEmployee.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvEmployee.Rows(0).Cells(1).Text = "No Records Found"
    '        Else
    '            dgvEmployee.DataSource = dtTable
    '            dgvEmployee.DataBind()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        dtTable = Nothing
    '    End Try
    'End Sub
#End Region

    Private Sub FillAllocEmployee()
        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
        Dim dsList As DataSet = Nothing
        Dim dsAllocEmp As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim intColType As Integer = 0
        Dim strFilter As String = ""
        Dim strIDs As String = ""
        Try
            chkChooseEmployee.Checked = False
            Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)

            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If

            If dtpStartDate.GetDate.Date = Nothing OrElse dtpEndDate.GetDate.Date = Nothing Then
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If

            'Sohail (24 Nov 2021) -- Start
            'Internal Issue :  : Filtering employee based on training allocation.
            Dim strEmpFilter As String = ""
            Select Case mintTrainingNeedAllocationID
                Case enAllocation.BRANCH
                    strEmpFilter = " T.stationunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.DEPARTMENT_GROUP
                    strEmpFilter = " T.deptgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.DEPARTMENT
                    strEmpFilter = " T.departmentunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.SECTION_GROUP
                    strEmpFilter = " T.sectiongroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.SECTION
                    strEmpFilter = " T.stationunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.UNIT_GROUP
                    strEmpFilter = " T.unitgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.UNIT
                    strEmpFilter = " T.unitunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.TEAM
                    strEmpFilter = " T.teamunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.JOB_GROUP
                    strEmpFilter = " J.jobgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.JOBS
                    strEmpFilter = " J.jobunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.CLASS_GROUP
                    strEmpFilter = " T.classgroupunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

                Case enAllocation.CLASSES
                    strEmpFilter = " T.classunkid = " & CInt(cboDepartmentList.SelectedValue) & " "

            End Select
            'Sohail (24 Nov 2021) -- End

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then Exit Try

            strIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

            If strIDs.Trim <> "" Then

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case enAllocation.BRANCH
                        strFilter &= " AND T.stationunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT_GROUP
                        strFilter &= " AND T.deptgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT
                        strFilter &= " AND T.departmentunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION_GROUP
                        strFilter &= " AND T.sectiongroupunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION
                        strFilter &= " AND T.sectionunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT_GROUP
                        strFilter &= " AND T.unitgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT
                        strFilter &= " AND T.unitunkid IN (" & strIDs & ") "

                    Case enAllocation.TEAM
                        strFilter &= " AND T.teamunkid IN (" & strIDs & ") "

                    Case enAllocation.JOB_GROUP
                        strFilter &= " AND J.jobgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.JOBS
                        strFilter &= " AND J.jobunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASS_GROUP
                        strFilter &= " AND T.classgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASSES
                        strFilter &= " AND T.classunkid IN (" & strIDs & ") "

                    Case enAllocation.COST_CENTER
                        strFilter &= " AND C.costcenterunkid IN (" & strIDs & ") "

                End Select

            Else
                strFilter &= " AND 1 = 2 "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            dtpStartDate.GetDate.Date, _
                                            dtpEndDate.GetDate.Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            False, "Emp", False, , , strAdvanceFilterQuery:=strFilter, strFilter:=strEmpFilter)
            'Sohail (24 Nov 2021) - [Removed: CInt(cboDepartmentList.SelectedValue), strEmpFilter]
            'Hemant (14 Oct 2021) -- Start             
            'ISSUE : OLD-489(Finca Uganda) - Inactive employees showing on departmental training needs screens
            'xIncludeIn_ActiveEmployee := True --> xIncludeIn_ActiveEmployee := False
            'Hemant (14 Oct 2021) -- End

            dtTable = dsList.Tables(0)
            objEmp = Nothing

            'dtTable = New DataView(dtTable, "", "employeename", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()

                Dim intCellCount As Integer = dgvAllocEmp.Rows(0).Cells.Count

                dgvAllocEmp.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvAllocEmp.Rows(0).Cells(i).Visible = False
                Next
                dgvAllocEmp.Rows(0).Cells(0).Text = "No Records Found"
            Else
                If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                    strFilter &= " AND 1 = 2 "
                End If
                dsAllocEmp = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            dtpStartDate.GetDate.Date, _
                                            dtpEndDate.GetDate.Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True, "", mintDepartmentaltrainingneedunkid, "")

                Dim strExistIDs As String = String.Join(",", (From p In dsAllocEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strExistIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dtTable Where (strExistIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                    If row.Count > 0 Then
                        chkChooseEmployee.Checked = True
                        Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)
                    End If
                End If

                dtTable = New DataView(dtTable, "", "IsChecked DESC, employeename", DataViewRowState.CurrentRows).ToTable

                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()
            End If

            chkAllocEmpOnlyTicked.Checked = False

            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable = Nothing
            objDTEmp = Nothing
        End Try
    End Sub

#Region " Old FillAllocEmployee() "
    'Private Sub FillAllocEmployee()
    '    Dim objDTEmp As New clsDepttrainingneed_employee_Tran
    '    Dim dsList As DataSet = Nothing
    '    Dim dtTable As DataTable = Nothing
    '    Dim strFilter As String = ""
    '    Try
    '        If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
    '            strFilter &= " AND 1 = 2 "
    '        End If
    '        dsList = objDTEmp.GetList(CStr(Session("Database_Name")), _
    '                                    CInt(Session("UserId")), _
    '                                    CInt(Session("Fin_year")), _
    '                                    CInt(Session("CompanyUnkId")), _
    '                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
    '                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
    '                                    CStr(Session("UserAccessModeSetting")), True, _
    '                                    True, "Emp", True, "", mintDepartmentaltrainingneedunkid, strFilter)

    '        dtTable = New DataView(dsList.Tables(0), "", "employeename", DataViewRowState.CurrentRows).ToTable

    '        Dim dtCol As New DataColumn
    '        dtCol.ColumnName = "IsChecked"
    '        dtCol.Caption = ""
    '        dtCol.DataType = System.Type.GetType("System.Boolean")
    '        dtCol.DefaultValue = False
    '        dtTable.Columns.Add(dtCol)

    '        If dtTable.Rows.Count <= 0 Then
    '            dtTable.Rows.Add(dtTable.NewRow)
    '            dgvAllocEmp.DataSource = dtTable
    '            dgvAllocEmp.DataBind()

    '            Dim intCellCount As Integer = dgvAllocEmp.Rows(0).Cells.Count

    '            dgvAllocEmp.Rows(0).Cells(0).Visible = False
    '            dgvAllocEmp.Rows(0).Cells(1).ColumnSpan = intCellCount
    '            For i As Integer = 2 To intCellCount - 1
    '                dgvAllocEmp.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvAllocEmp.Rows(0).Cells(1).Text = "No Records Found"
    '        Else
    '            dgvAllocEmp.DataSource = dtTable
    '            dgvAllocEmp.DataBind()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        dtTable = Nothing
    '    End Try
    'End Sub
#End Region

    Private Sub FillTrainingResources()
        Dim objCommon As New clsCommon_Master
        Dim objTResources As New clsDepttrainingneed_resources_Tran
        Dim dsList As DataSet = Nothing
        Dim dsTResources As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try
            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.TRAINING_RESOURCES, "List", , True)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingResources.DataSource = dsList
                dgvTrainingResources.DataBind()

                Dim intCellCount As Integer = dgvTrainingResources.Rows(0).Cells.Count

                dgvTrainingResources.Rows(0).Cells(0).Visible = False
                dgvTrainingResources.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingResources.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingResources.Rows(0).Cells(1).Text = "No Records Found"
            Else
                dsTResources = objTResources.GetList("List", mintDepartmentaltrainingneedunkid)

                Dim strIDs As String = String.Join(",", (From p In dsTResources.Tables(0) Select (p.Item("trainingresourceunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvTrainingResources.DataSource = dtTable
                dgvTrainingResources.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Old FillTrainingReources "
    'Private Sub FillTrainingResources()
    '    Dim objTResources As New clsDepttrainingneed_resources_Tran
    '    Dim dsList As DataSet = Nothing
    '    Try

    '        dsList = objTResources.GetList("List", mintDepartmentaltrainingneedunkid)

    '        If dsList.Tables(0).Rows.Count <= 0 Then
    '            dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
    '            dgvTrainingResources.DataSource = dsList
    '            dgvTrainingResources.DataBind()

    '            Dim intCellCount As Integer = dgvTrainingResources.Rows(0).Cells.Count

    '            dgvTrainingResources.Rows(0).Cells(0).Visible = False
    '            dgvTrainingResources.Rows(0).Cells(1).ColumnSpan = intCellCount
    '            For i As Integer = 2 To intCellCount - 1
    '                dgvTrainingResources.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvTrainingResources.Rows(0).Cells(1).Text = "No Records Found"
    '        Else
    '            dgvTrainingResources.DataSource = dsList
    '            dgvTrainingResources.DataBind()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
#End Region

    Private Sub FillFinancingSource()
        Dim objCommon As New clsCommon_Master
        Dim objTFsource As New clsDepttrainingneed_financingsources_Tran
        Dim dsList As DataSet = Nothing
        Dim dsFSource As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, "List", , True)


            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvFinancingSource.DataSource = dsList
                dgvFinancingSource.DataBind()

                Dim intCellCount As Integer = dgvFinancingSource.Rows(0).Cells.Count

                dgvFinancingSource.Rows(0).Cells(0).Visible = False
                dgvFinancingSource.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvFinancingSource.Rows(0).Cells(i).Visible = False
                Next
                dgvFinancingSource.Rows(0).Cells(1).Text = "No Records Found"
            Else
                dsFSource = objTFsource.GetList("List", mintDepartmentaltrainingneedunkid)

                Dim strIDs As String = String.Join(",", (From p In dsFSource.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvFinancingSource.DataSource = dtTable
                dgvFinancingSource.DataBind()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTFsource = Nothing
        End Try
    End Sub

#Region " Old FillFinancingSource "
    'Private Sub FillFinancingSource()
    '    Dim objTFsource As New clsDepttrainingneed_financingsources_Tran
    '    Dim dsList As DataSet = Nothing
    '    Try

    '        dsList = objTFsource.GetList("List", mintDepartmentaltrainingneedunkid)

    '        If dsList.Tables(0).Rows.Count <= 0 Then
    '            dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
    '            dgvFinancingSource.DataSource = dsList
    '            dgvFinancingSource.DataBind()

    '            Dim intCellCount As Integer = dgvFinancingSource.Rows(0).Cells.Count

    '            dgvFinancingSource.Rows(0).Cells(0).Visible = False
    '            dgvFinancingSource.Rows(0).Cells(1).ColumnSpan = intCellCount
    '            For i As Integer = 2 To intCellCount - 1
    '                dgvFinancingSource.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvFinancingSource.Rows(0).Cells(1).Text = "No Records Found"
    '        Else
    '            dgvFinancingSource.DataSource = dsList
    '            dgvFinancingSource.DataBind()
    '        End If


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTFsource = Nothing
    '    End Try
    'End Sub
#End Region

    Private Sub FillTrainingCoordinator()
        Dim objEmp As New clsEmployee_Master
        Dim objDTCoord As New clsDepttrainingneed_trainingcoordinator_Tran
        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim dsTCoord As DataSet = Nothing
        Try

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            dtpStartDate.GetDate.Date, _
                                            dtpEndDate.GetDate.Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", False)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingCoordinator.DataSource = dsList
                dgvTrainingCoordinator.DataBind()

                Dim intCellCount As Integer = dgvTrainingCoordinator.Rows(0).Cells.Count

                dgvTrainingCoordinator.Rows(0).Cells(0).Visible = False
                dgvTrainingCoordinator.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingCoordinator.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingCoordinator.Rows(0).Cells(1).Text = "No Records Found"
            Else
                dsTCoord = objDTCoord.GetList(CStr(Session("Database_Name")), _
                                         CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                         eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                         CStr(Session("UserAccessModeSetting")), True, _
                                         True, "Emp", True, "", mintDepartmentaltrainingneedunkid)

                Dim strIDs As String = String.Join(",", (From p In dsTCoord.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, employeename", DataViewRowState.CurrentRows).ToTable

                dgvTrainingCoordinator.DataSource = dtTable
                dgvTrainingCoordinator.DataBind()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDTCoord = Nothing
            objEmp = Nothing
        End Try
    End Sub

#Region " Old FillTrainingCoordinator "
    'Private Sub FillTrainingCoordinator()
    '    Dim objDTCoord As New clsDepttrainingneed_trainingcoordinator_Tran
    '    Dim dsList As DataSet = Nothing
    '    Try

    '        dsList = objDTCoord.GetList(CStr(Session("Database_Name")), _
    '                                    CInt(Session("UserId")), _
    '                                    CInt(Session("Fin_year")), _
    '                                    CInt(Session("CompanyUnkId")), _
    '                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
    '                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
    '                                    CStr(Session("UserAccessModeSetting")), True, _
    '                                    True, "Emp", True, "", mintDepartmentaltrainingneedunkid)

    '        If dsList.Tables(0).Rows.Count <= 0 Then
    '            dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
    '            dgvTrainingCoordinator.DataSource = dsList
    '            dgvTrainingCoordinator.DataBind()

    '            Dim intCellCount As Integer = dgvTrainingCoordinator.Rows(0).Cells.Count

    '            dgvTrainingCoordinator.Rows(0).Cells(0).Visible = False
    '            dgvTrainingCoordinator.Rows(0).Cells(1).ColumnSpan = intCellCount
    '            For i As Integer = 2 To intCellCount - 1
    '                dgvTrainingCoordinator.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvTrainingCoordinator.Rows(0).Cells(1).Text = "No Records Found"
    '        Else
    '            dgvTrainingCoordinator.DataSource = dsList
    '            dgvTrainingCoordinator.DataBind()
    '        End If


    '    Catch ex As Exception

    '    End Try
    'End Sub
#End Region
    
    Private Sub FillTrainingCostItem()
        Dim objTItem As New clstrainingitemsInfo_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim dsList As DataSet = Nothing
        Dim dsTCostItem As DataSet = Nothing
        Dim dtTable As DataTable = Nothing

        Try
            dsList = objTItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            dtCol = New DataColumn
            dtCol.ColumnName = "Amount"
            dtCol.Caption = "Amount"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            dtCol = New DataColumn
            dtCol.ColumnName = "Approved_Amount"
            dtCol.Caption = "Approved_Amount"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingCostItem.DataSource = dsList
                dgvTrainingCostItem.DataBind()

                Dim intCellCount As Integer = dgvTrainingCostItem.Rows(0).Cells.Count

                dgvTrainingCostItem.Rows(0).Cells(0).Visible = False
                dgvTrainingCostItem.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingCostItem.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingCostItem.Rows(0).Cells(1).Text = "No Records Found"
            Else
                dsTCostItem = objTCostItem.GetList("List", mintDepartmentaltrainingneedunkid)

                Dim strIDs As String = String.Join(",", (From p In dsTCostItem.Tables(0) Select (p.Item("costitemunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                        Dim r() As DataRow = dsTCostItem.Tables(0).Select("costitemunkid = " & CInt(dtRow.Item("infounkid")) & " ")
                        If r.Length > 0 Then
                            dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), GUI.fmtCurrency)
                            dtRow.Item("Approved_Amount") = Format(CDec(r(0).Item("Approved_Amount")), GUI.fmtCurrency)
                        Else
                            dtRow.Item("Amount") = 0
                            dtRow.Item("Approved_Amount") = 0
                        End If
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    '    dtRow.Item("Amount") = Format(0, GUI.fmtCurrency)
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, info_name", DataViewRowState.CurrentRows).ToTable

                dgvTrainingCostItem.DataSource = dtTable
                dgvTrainingCostItem.DataBind()
                If mintFormId = 3 Then 'Budget Approval users
                    dgvTrainingCostItem.Columns(getColumnID_Griview(dgvTrainingCostItem, "colhapproved_amount")).Visible = True
                    lblCostItemApprovedTotal.Visible = True
                    objlblCostItemApprovedTotal.Visible = True
                Else
                    dgvTrainingCostItem.Columns(getColumnID_Griview(dgvTrainingCostItem, "colhapproved_amount")).Visible = False
                    lblCostItemApprovedTotal.Visible = False
                    objlblCostItemApprovedTotal.Visible = False
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTItem = Nothing
            objTCostItem = Nothing
        End Try
    End Sub

#Region " Old FillTrainingCostItem "
    'Private Sub FillTrainingCostItem()
    '    Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
    '    Dim dsList As DataSet = Nothing
    '    Try
    '        dsList = objTCostItem.GetList("List", mintDepartmentaltrainingneedunkid)

    '        If dsList.Tables(0).Rows.Count <= 0 Then
    '            dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
    '            dgvTrainingCostItem.DataSource = dsList
    '            dgvTrainingCostItem.DataBind()

    '            Dim intCellCount As Integer = dgvTrainingCostItem.Rows(0).Cells.Count

    '            dgvTrainingCostItem.Rows(0).Cells(0).Visible = False
    '            dgvTrainingCostItem.Rows(0).Cells(1).ColumnSpan = intCellCount
    '            For i As Integer = 2 To intCellCount - 1
    '                dgvTrainingCostItem.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvTrainingCostItem.Rows(0).Cells(1).Text = "No Records Found"
    '        Else
    '            dgvTrainingCostItem.DataSource = dsList
    '            dgvTrainingCostItem.DataBind()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTCostItem = Nothing
    '    End Try
    'End Sub
#End Region

    'Sohail (09 Jun 2021) -- Start
    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
    Private Sub FillInstructor()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            dtpStartDate.GetDate.Date, _
                                            dtpEndDate.GetDate.Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", False)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvAddTrainingInstructor.DataSource = dsList
                dgvAddTrainingInstructor.DataBind()

                Dim intCellCount As Integer = dgvAddTrainingInstructor.Rows(0).Cells.Count

                dgvAddTrainingInstructor.Rows(0).Cells(0).Visible = False
                dgvAddTrainingInstructor.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvAddTrainingInstructor.Rows(0).Cells(i).Visible = False
                Next
                dgvAddTrainingInstructor.Rows(0).Cells(1).Text = "No Records Found"
            Else
                dgvAddTrainingInstructor.DataSource = dsList
                dgvAddTrainingInstructor.DataBind()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub FillTrainingInstructor()
        Dim objDTInstruct As New clsDepttrainingneed_traininginstructor_Tran
        Dim dsList As DataSet = Nothing
        Try

            dsList = objDTInstruct.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                          eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          True, CStr(Session("CompName")), "Emp", True, "", mintDepartmentaltrainingneedunkid)
            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingInstructor.DataSource = dsList
                dgvTrainingInstructor.DataBind()

                Dim intCellCount As Integer = dgvTrainingInstructor.Rows(0).Cells.Count

                dgvTrainingInstructor.Rows(0).Cells(0).Visible = False
                dgvTrainingInstructor.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingInstructor.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingInstructor.Rows(0).Cells(1).Text = "No Records Found"
            Else
                dgvTrainingInstructor.DataSource = dsList.Tables(0)
                dgvTrainingInstructor.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDTInstruct = Nothing
        End Try
    End Sub
    'Sohail (09 Jun 2021) -- End


    Private Function IsValidData(Optional ByVal blnCheckNoOfStaff As Boolean = True, Optional ByVal blnCheckCostItemAmt As Boolean = True) As Boolean
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try
            objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)

            If CInt(cboDepartmentList.SelectedValue) <= 0 AndAlso mintFormId = 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select #department#.").Replace("#department#", mstrTrainingNeedAllocationName), Me)
                mblnShowAddEditPopup = False
                popAddEdit.Hide()
                cboDepartmentList.Focus()
                Return False
            ElseIf CInt(cboDepartmentList.SelectedValue) = 0 AndAlso (mintFormId = 2 OrElse mintFormId = 3) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select #department#.").Replace("#department#", mstrTrainingNeedAllocationName), Me)
                mblnShowAddEditPopup = False
                popAddEdit.Hide()
                cboDepartmentList.Focus()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please select Training Period."), Me)
                TabName.Value = "TrainingInfo"
                cboPeriod.Focus()
                Return False
            ElseIf chkOtherCompetences.Checked = False AndAlso CInt(cboCompetences.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please select Area of Development."), Me)
                TabName.Value = "TrainingInfo"
                cboCompetences.Focus()
                Return False
            ElseIf chkOtherTrainingCource.Checked = False AndAlso CInt(cboTrainingNeeded.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please select the Training needed."), Me)
                TabName.Value = "TrainingInfo"
                cboTrainingNeeded.Focus()
                Return False
            ElseIf chkOtherCompetences.Checked = True AndAlso txtOtherCompetence.Text.Trim = "" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Please enter Area of Development.."), Me)
                TabName.Value = "TrainingInfo"
                chkOtherCompetences.Focus()
                Return False
            ElseIf chkOtherTrainingCource.Checked = True AndAlso txtOtherTCourse.Text.Trim = "" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Please enter training needed."), Me)
                TabName.Value = "TrainingInfo"
                txtOtherTCourse.Focus()
                Return False
            ElseIf CInt(cboTrainingCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Please select training need category."), Me)
                TabName.Value = "TrainingInfo"
                cboTrainingCategory.Focus()
                Return False
                'ElseIf CInt(cboLearningMethod.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Please select learning method."), Me)
                '    TabName.Value = "TrainingInfo"
                '    cboLearningMethod.Focus()
                '    Return False
            ElseIf dtpStartDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Please enter start date."), Me)
                TabName.Value = "TrainingInfo"
                dtpStartDate.Focus()
                Return False
            ElseIf dtpEndDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Please enter end date."), Me)
                TabName.Value = "TrainingInfo"
                dtpEndDate.Focus()
                Return False
            ElseIf dtpStartDate.GetDate.Date < objTPeriod._StartDate OrElse dtpStartDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, the Start date should be in between the start date and end date of the selected period."), Me)
                TabName.Value = "TrainingInfo"
                dtpStartDate.Focus()
                Return False
            ElseIf dtpEndDate.GetDate.Date < objTPeriod._StartDate OrElse dtpEndDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, the End date should be in between the start date and end date of the selected period."), Me)
                TabName.Value = "TrainingInfo"
                dtpEndDate.Focus()
                Return False
            ElseIf dtpStartDate.GetDate.Date > dtpEndDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, the Start date should not be later than the End date."), Me)
                TabName.Value = "TrainingInfo"
                dtpEndDate.Focus()
                Return False
            ElseIf CInt(cboPriority.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Please select priority."), Me)
                TabName.Value = "TrainingInfo"
                cboPriority.Focus()
                Return False
                'ElseIf CInt(cboTrainingProvider.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "Please select training provider."), Me)
                '    TabName.Value = "TrainingInfo"
                '    cboTrainingProvider.Focus()
                '    Return False
                'ElseIf CInt(cboTrainingVenue.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Please select training venue."), Me)
                '    TabName.Value = "TrainingInfo"
                '    cboTrainingVenue.Focus()
                '    Return False
            End If

            Dim intNoOfStaff As Integer = 0
            Integer.TryParse(txtNoOfStaff.Text, intNoOfStaff)
            If intNoOfStaff <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 17, "Please enter the Number of Staff."), Me)
                TabName.Value = "TargetGroup"
                txtNoOfStaff.Focus()
                Return False
            End If

            If blnCheckNoOfStaff = True Then
                'If dgvEmployee.Rows.Count <= 0 OrElse dgvEmployee.Rows(0).Cells(1).ColumnSpan > 0 Then
                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 18, "Please select at least one employee or allocation from the targeted group."), Me)
                    TabName.Value = "TargetGroup"
                    dgvEmployee.Focus()
                    Return False
                End If

                If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                    If intNoOfStaff <> gRow.Count Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Sorry, selected employees should be equal to the number of staff indicated above."), Me)
                        TabName.Value = "TargetGroup"
                        dgvEmployee.Focus()
                        Return False
                    End If
                Else
                    'If dgvAllocEmp.Rows.Count <= 0 OrElse dgvAllocEmp.Rows(0).Cells(1).ColumnSpan > 0 Then
                    'gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

                    'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "Please tick atleaset one employee for selected allocations from targeted group."), Me)
                    '    TabName.Value = "TargetGroup"
                    '    dgvAllocEmp.Focus()
                    '    Return False
                    'End If
                    gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)
                    If gRow IsNot Nothing AndAlso intNoOfStaff < gRow.Count Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "Sorry, selected employees should be less or equal to the number of staff indicated above."), Me)
                        TabName.Value = "TargetGroup"
                        dgvEmployee.Focus()
                        Return False
                    End If
                End If
            End If

            If blnCheckCostItemAmt = True Then
                If chkTrainingCostOptional.Checked = False Then
                    Dim decGTotal As Decimal = 0
                    Decimal.TryParse(objlblCostItemTotal.Text, decGTotal)
                    If decGTotal = 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Sorry, Training Cost is mandatory. Please enter the training cost."), Me)
                        TabName.Value = "TrainingCostItem"
                        Return False
                    End If
                End If

                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("txtAmount"), Controls_NumberOnly).Decimal_ = 0)
                gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True AndAlso CType(x.FindControl("txtAmount"), Controls_NumberOnly).Decimal_ = 0)

                If gRow IsNot Nothing AndAlso gRow.Count > 0 AndAlso gRow(0).Cells(1).ColumnSpan = 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 23, "Please add cost amount for all the selected cost items."), Me)
                    TabName.Value = "TrainingCostItem"
                    dgvTrainingCostItem.Focus()
                    Return False
                End If

                If mintFormId = 3 Then
                    gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True AndAlso CType(x.FindControl("txtApprovedAmount"), Controls_NumberOnly).Decimal_ > CType(x.FindControl("txtAmount"), Controls_NumberOnly).Decimal_)

                    If gRow IsNot Nothing AndAlso gRow.Count > 0 AndAlso gRow(0).Cells(1).ColumnSpan = 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "Sorry, approved amount should not exceed the cost amount."), Me)
                        TabName.Value = "TrainingCostItem"
                        dgvTrainingCostItem.Focus()
                        Return False
                    End If

                    gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True AndAlso CType(x.FindControl("txtAmount"), Controls_NumberOnly).Decimal_ > 0 AndAlso CType(x.FindControl("txtApprovedAmount"), Controls_NumberOnly).Decimal_ <= 0)
                    If gRow IsNot Nothing AndAlso gRow.Count > 0 AndAlso gRow(0).Cells(1).ColumnSpan = 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "Please add approved cost amount for all the selected cost items."), Me)
                        TabName.Value = "TrainingCostItem"
                        dgvTrainingCostItem.Focus()
                        Return False
                    End If
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Public Function IsValidForActiveInactive(ByVal intGridViewRowIndex As Integer) As Boolean
        Dim objTAllocEmp As New clsDepttrainingneed_employee_Tran
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Try
            Dim intUnkId As Integer = CInt(gvDeptTrainingeedList.DataKeys(intGridViewRowIndex).Item("departmentaltrainingneedunkid"))

            'If CInt(gvDeptTrainingeedList.DataKeys(intGridViewRowIndex).Item("targetedgroupunkid")) > 0 Then

            '    Dim dsEmp As DataSet = objTAllocEmp.GetList(CStr(Session("Database_Name")), _
            '                                                CInt(Session("UserId")), _
            '                                                CInt(Session("Fin_year")), _
            '                                                CInt(Session("CompanyUnkId")), _
            '                                                eZeeDate.convertDate(gvDeptTrainingeedList.DataKeys(mintActiveInactiveRowIndex).Item("startdate").ToString), _
            '                                                eZeeDate.convertDate(gvDeptTrainingeedList.DataKeys(mintActiveInactiveRowIndex).Item("enddate").ToString), _
            '                                                CStr(Session("UserAccessModeSetting")), True, _
            '                                                True, "Emp", False, "", intUnkId)

            '    If dsEmp.Tables(0).Rows.Count <= 0 Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "Please add atleaset one employee for selected allocations from targeted group."), Me)
            '        Return False
            '    End If

            'End If

            Dim dsCost As DataSet = objTCostItem.GetList("List", intUnkId, " AND trdepttrainingneed_costitem_tran.amount = 0 ")

            If dsCost.Tables(0).Rows.Count > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 26, "Please add cost amount for added cost items."), Me)
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTAllocEmp = Nothing
        End Try
    End Function

    Public Function IsValidForApproval(ByVal strUnkIDs As List(Of String)) As Boolean
        Dim objTAllocEmp As New clsDepttrainingneed_employee_Tran
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Try
            'Dim intUnkId As Integer = CInt(gvDeptTrainingeedList.DataKeys(intGridViewRowIndex).Item("departmentaltrainingneedunkid"))

            For Each sID As String In strUnkIDs
                Dim objDeptTNeed As New clsDepartmentaltrainingneed_master
                objDeptTNeed._Departmentaltrainingneedunkid = CInt(sID)
                Dim ds As DataSet = objDeptTNeed.GetList(CStr(Session("Database_Name")), _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                objDeptTNeed._Startdate, _
                                                                objDeptTNeed._Enddate, _
                                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                                True, mintTrainingNeedAllocationID, "List", True, _
                                                                " AND trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = " & CInt(sID) & " ")

                'If objDeptTNeed._Targetedgroupunkid > 0 Then 'Not Employee Names

                '    Dim dsEmp As DataSet = objTAllocEmp.GetList(CStr(Session("Database_Name")), _
                '                                                CInt(Session("UserId")), _
                '                                                CInt(Session("Fin_year")), _
                '                                                CInt(Session("CompanyUnkId")), _
                '                                                objDeptTNeed._Startdate, _
                '                                                objDeptTNeed._Enddate, _
                '                                                CStr(Session("UserAccessModeSetting")), True, _
                '                                                True, "Emp", False, "", CInt(sID))

                '    If dsEmp.Tables(0).Rows.Count <= 0 Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "Please add atleaset one employee for training #trainingname# for selected allocations from targeted group.").Replace("#trainingname#", ds.Tables(0).Rows(0).Item("trainingcoursename").ToString), Me)
                '        Return False
                '    End If

                'End If
                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                If mblnIsUnlockSubmitClicked = True AndAlso CInt(ds.Tables(0).Rows(0).Item("insertformid")) = mintFormId Then
                    'Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 27, "Sorry, You can not Unlock Submit for Approval for training #trainingname# added from backlog.").Replace("#trainingname#", ds.Tables(0).Rows(0).Item("trainingcoursename").ToString)
                    Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 27, "Sorry, you cannot unlock training(s) submitted for approval if the training(s) were directly added into the company training backlog. Please use the delete option to remove such training(s) from the list.")
                    If mintFormId = 3 Then
                        strMsg = strMsg.Replace("backlog", "budget approval")
                    End If
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    Return False
                End If
                'Hemant (15 Apr 2021) -- End
                If CInt(ds.Tables(0).Rows(0).Item("allocationtranunkid")) <= 0 Then
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 28, "Please tick atleaset one item from target group for training #trainingname# and Save it.").Replace("#trainingname#", ds.Tables(0).Rows(0).Item("trainingcoursename").ToString), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 28, "Please select at least one item from the target group for this training and Save."), Me)
                    Return False
                End If

                Dim dsCost As DataSet = objTCostItem.GetList("List", CInt(sID), " AND trdepttrainingneed_costitem_tran.amount = 0 ")

                If dsCost.Tables(0).Rows.Count > 0 Then
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 29, "Please add cost amount for training #trainingname# for added cost items.").Replace("#trainingname#", ds.Tables(0).Rows(0).Item("trainingcoursename").ToString), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 29, "Please add cost amount for the selected cost items."), Me)
                    Return False
                End If

            Next


            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTAllocEmp = Nothing
        End Try
    End Function

    Public Function IsValidForMaxBudget(ByVal strDpetTrainingUnkIDs As String, ByVal strDistDeptUnkIDs As List(Of String), ByVal strDistPeriodUnkIDs As List(Of String), Optional ByVal blnSaveAndSubmit As Boolean = False) As Boolean
        'Sohail (04 Aug 2021) - [blnSaveAndSubmit]
        Dim objTNeedMaster As New clsDepartmentaltrainingneed_master
        Dim objMaxBudget As New clsDepartmenttrainingneed_maxbudget
        Try
            If strDistDeptUnkIDs Is Nothing OrElse strDistPeriodUnkIDs Is Nothing Then Return True
            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            If mintTrainingNeedAllocationID <> mintTrainingBudgetAllocationID Then Return True
            'Sohail (10 Feb 2022) -- End

            For Each sPID As String In strDistPeriodUnkIDs
                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                'Dim dsMaxBudget As DataSet = objMaxBudget.GetList("List", CInt(sPID), mintTrainingNeedAllocationID, True, False)
                Dim dsMaxBudget As DataSet = objMaxBudget.GetList("List", CInt(sPID), mintTrainingBudgetAllocationID, True, False)
                'Sohail (10 Feb 2022) -- End

                For Each sID As String In strDistDeptUnkIDs

                    Dim dr() As DataRow = dsMaxBudget.Tables(0).Select("allocationtranunkid = " & CInt(sID) & " ")
                    If dr.Length > 0 AndAlso CDec(dr(0).Item("maxbudget_amt")) > 0 Then
                        'Sohail (10 Feb 2022) -- Start
                        'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                        'Dim decApprovedAmt As Decimal = objTNeedMaster.GetTotalApprovedAmtAllocationWise(CInt(sPID), mintTrainingNeedAllocationID, CInt(sID))
                        Dim decApprovedAmt As Decimal = objTNeedMaster.GetTotalApprovedAmtAllocationWise(CInt(sPID), mintTrainingBudgetAllocationID, CInt(sID))
                        'Sohail (10 Feb 2022) -- End

                        'Sohail (04 Aug 2021) -- Start
                        'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                        'Dim decRequestedAmt As Decimal = objTNeedMaster.GetTotalCostAmtAllocationWise(CInt(sPID), mintTrainingNeedAllocationID, CInt(sID), strDpetTrainingUnkIDs)
                        Dim decRequestedAmt As Decimal = 0
                        If blnSaveAndSubmit = False Then
                            'Sohail (10 Feb 2022) -- Start
                            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                            'decRequestedAmt = objTNeedMaster.GetTotalCostAmtAllocationWise(CInt(sPID), mintTrainingNeedAllocationID, CInt(sID), strDpetTrainingUnkIDs)
                            decRequestedAmt = objTNeedMaster.GetTotalCostAmtAllocationWise(CInt(sPID), mintTrainingBudgetAllocationID, CInt(sID), strDpetTrainingUnkIDs)
                            'Sohail (10 Feb 2022) -- End
                        Else
                            Dim decTotAmt As Decimal = 0
                            Decimal.TryParse(hfobjlblTotalAmt.Value, decTotAmt)
                            decRequestedAmt = decTotAmt
                        End If
                        'Sohail (04 Aug 2021) -- End

                        If (decApprovedAmt + decRequestedAmt) > CDec(dr(0).Item("maxbudget_amt")) Then
                            Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 30, "Sorry, Total Requested amount should not exceed the maximum budgetary allocation for the selected department.")
                            strMsg &= "\n"
                            strMsg &= "\n"
                            strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 31, "Department : #deptname#").Replace("#deptname#", dr(0).Item("allocationtranname").ToString)
                            strMsg &= "\n"
                            strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 32, "Approved Total : #approvedtotal#").Replace("#approvedtotal#", Format(decApprovedAmt, CStr(Session("fmtCurrency"))))
                            strMsg &= "\n"
                            strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 33, "Requested Total : #requestedtotal#").Replace("#requestedtotal#", Format(decRequestedAmt, CStr(Session("fmtCurrency"))))
                            strMsg &= "\n"
                            strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 34, "Sub Total : #subtotal#").Replace("#subtotal#", Format(decApprovedAmt + decRequestedAmt, CStr(Session("fmtCurrency"))))
                            strMsg &= "\n"
                            strMsg &= "\n"
                            strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 35, "Maximum Budget : #maxbudget#").Replace("#maxbudget#", Format(CDec(dr(0).Item("maxbudget_amt")), CStr(Session("fmtCurrency"))))

                            DisplayMessage.DisplayMessage(strMsg, Me)
                            Return False
                        End If

                    End If

                Next
            Next

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTNeedMaster = Nothing
            objMaxBudget = Nothing
        End Try
    End Function

    Private Function SetValueMaster() As clsDepartmentaltrainingneed_master
        Dim objTMaster As New clsDepartmentaltrainingneed_master
        Try
            With objTMaster
                'If CInt(cboTargetedGroup.SelectedValue) > 0 Then 'Not Employee Names
                '    txtNoOfStaff.Text = dgvAllocEmp.Rows.Count.ToString
                'End If

                Dim intNoOfStaff As Integer = 0
                Integer.TryParse(txtNoOfStaff.Text, intNoOfStaff)

                ._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                ._Departmentunkid = CInt(cboDepartmentList.SelectedValue)
                ._Periodunkid = CInt(cboPeriod.SelectedValue)
                If chkOtherCompetences.Checked = False Then
                    'Gajanan [15-Apr-2021] -- Start
                   
                    ._Competenceunkid = CInt(cboCompetences.SelectedValue)
                    ._Other_competence = ""

                    'Gajanan [15-Apr-2021] -- End
                Else
                    If ._ModuleId = CInt(enModuleReference.PDP) AndAlso ._Other_competence.Length <= 0 AndAlso ._Competenceunkid <= 0 Then
                        ._Competenceunkid = -1
                        ._Other_competence = ""
                    Else
                    ._Competenceunkid = 0
                    ._Other_competence = txtOtherCompetence.Text
                End If
                End If
                If chkOtherTrainingCource.Checked = False Then
                    ._Trainingcourseunkid = CInt(cboTrainingNeeded.SelectedValue)
                    ._Other_trainingcourse = ""
                Else
                    ._Trainingcourseunkid = 0
                    ._Other_trainingcourse = txtOtherTCourse.Text
                End If
                ._Trainingcategoryunkid = CInt(cboTrainingCategory.SelectedValue)

                ._Learningmethodunkid = CInt(cboLearningMethod.SelectedValue)
                ._Targetedgroupunkid = CInt(cboTargetedGroup.SelectedValue)
                ._Noofstaff = intNoOfStaff
                ._Startdate = dtpStartDate.GetDate
                ._Enddate = dtpEndDate.GetDate
                ._Trainingpriority = CInt(cboPriority.SelectedValue)
                ._Trainingproviderunkid = CInt(cboTrainingProvider.SelectedValue)
                ._Trainingvenueunkid = CInt(cboTrainingVenue.SelectedValue)
                ._Remark = txtCommentRemark.Text
                ._Iscertirequired = chkCertiRequired.Checked
                ._IsTrainingCostOptional = chkTrainingCostOptional.Checked
                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                If mintDepartmentaltrainingneedunkid <= 0 Then 'Add New
                    ._InsertFormId = mintFormId

                    If mintFormId = 3 Then
                        ._RefNo = .getNextRefNo().ToString
                    End If
                    'Sohail (17 Aug 2021) -- Start
                    'NMB Enhancement : OLD-428 : Send alert to the creator of plan only in departmental training need.
                    ._Createuserunkid = CInt(Session("UserId"))
                    'Sohail (17 Aug 2021) -- End
                End If
                If mintFormId = 2 Then
                    ._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed
                ElseIf mintFormId = 3 Then
                    ._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog
                End If
                'Hemant (15 Apr 2021) -- End
                ._Isweb = True
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    ._Userunkid = CInt(Session("UserId"))
                    ._Loginemployeeunkid = 0
                    ._AuditUserId = CInt(Session("UserId"))
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    ._AuditUserId = CInt(Session("Employeeunkid"))
                End If
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FormName = mstrModuleName
                'Hemant (26 Mar 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                ._AllocationId = mintTrainingNeedAllocationID
                'Hemant (26 Mar 2021) -- End

            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return objTMaster
    End Function

    Private Function UpdateCostAmount() As Boolean
        Try
            If mintDepartmentaltrainingneedunkid <= 0 Then Exit Try
            If hfCostItemAmtChanged.Value = "0" Then Exit Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("txtAmount"), Controls_NumberOnly).Decimal_ = 0)

            If gRow IsNot Nothing AndAlso gRow.Count > 0 AndAlso gRow(0).Cells(1).ColumnSpan > 0 Then 'No Record found
                'Do Nothing
            ElseIf gRow.Count <= 0 AndAlso dgvTrainingCostItem.Rows.Count > 0 Then
                Dim lstTCostItem As New List(Of clsDepttrainingneed_costitem_Tran)
                Dim objTCostItem As New clsDepttrainingneed_costitem_Tran

                For Each dgRow As GridViewRow In dgvTrainingCostItem.Rows
                    objTCostItem = New clsDepttrainingneed_costitem_Tran

                    With objTCostItem
                        .pintDepttrainingneedcostitemtranunkid = CInt(dgvTrainingCostItem.DataKeys(dgRow.RowIndex).Item("depttrainingneedcostitemtranunkid"))
                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                        .pintCostitemunkid = CInt(dgvTrainingCostItem.DataKeys(dgRow.RowIndex)("infounkid").ToString())
                        Dim decAmt As Decimal = CType(dgRow.FindControl("txtAmount"), Controls_NumberOnly).Decimal_
                        .pdecAmount = decAmt
                        .pdecApproved_Amount = decAmt

                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = CInt(Session("Employeeunkid"))
                        End If
                        .pblnIsweb = True
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName
                    End With

                    lstTCostItem.Add(objTCostItem)

                Next

                Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
                Dim decTotAmt As Decimal = 0
                Decimal.TryParse(hfobjlblTotalAmt.Value, decTotAmt)
                objTMaster._Totalcost = decTotAmt
                Dim decTotApprovedAmt As Decimal = 0
                Decimal.TryParse(hfobjlblTotalApprovedAmt.Value, decTotAmt)
                objTMaster._Approved_Totalcost = decTotApprovedAmt
                objTCostItem._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTCostItem.SaveAll(lstTCostItem, objTMaster) = False Then
                    If objTCostItem._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTCostItem._Message, Me)
                    End If
                Else
                    mintDepartmentaltrainingneedunkid = objTCostItem._Departmentaltrainingneedunkid
                End If

                hfCostItemAmtChanged.Value = "0"
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Protected Sub chkActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            If CBool(Session("AllowToDeleteDepartmentalTrainingNeed")) = False Then
                chkSelect.Checked = Not chkSelect.Checked
                Exit Try
            End If

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            mintActiveInactiveRowIndex = gvRow.RowIndex

            If chkSelect.Checked = True AndAlso IsValidForActiveInactive(mintActiveInactiveRowIndex) = False Then
                chkSelect.Checked = Not chkSelect.Checked
                Exit Try
            End If

            If CInt(gvDeptTrainingeedList.DataKeys(mintActiveInactiveRowIndex).Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                chkSelect.Checked = Not chkSelect.Checked
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 36, "Sorry, This Departmental Training Need is already Submitted for Approval."), Me)
                Exit Try
            End If

            mintActiveInactiveUnkId = CInt(gvDeptTrainingeedList.DataKeys(mintActiveInactiveRowIndex).Item("departmentaltrainingneedunkid"))

            If chkSelect.Checked = False Then
                popupInactiveDTNeed.Show()
            Else
                popupActiveDTNeed.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private Sub SendEmail(ByVal strDepartmentalTrainingNeedIDs As String)
        Dim objDeptTraining As New clsDepartmentaltrainingneed_master
        Try
            Select Case mintEmailTypeId
                Case clsDepartmentaltrainingneed_master.enEmailType.Submit_For_Approval_From_Departmental_Training_Need, _
                    clsDepartmentaltrainingneed_master.enEmailType.Submit_For_Approval_From_Training_Backlog
                    objDeptTraining.SendEmails(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "List", mintEmailTypeId, strDepartmentalTrainingNeedIDs, mstrModuleName, _
                                                enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), _
                                            CStr(Session("ArutiSelfServiceURL")), mstrTrainingNeedAllocationName, mintTrainingNeedAllocationID)
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDeptTraining = Nothing
        End Try
    End Sub
    'Hemant (26 Mar 2021) -- End

    Protected Sub ChkAllTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If IsValidData(False, False) = False Then Exit Try

            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            For Each gvRow As GridViewRow In dgvEmployee.Rows
                CType(gvRow.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = chkSelectAll.Checked
            Next

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkgvSelectTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If IsValidData(False, False) = False Then Exit Try

            Dim chkSelect As CheckBox = CType(sender, CheckBox)

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso dgvEmployee.Rows.Count = gRow.Count Then
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
            Else
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = False
            End If

            chkEmpOnlyTicked.Checked = False

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private Sub FillTrainingCategoryCombo(ByVal intModouleId As Integer)
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsCombo = objTCategory.getListForCombo("List", True)
            If intModouleId <= 0 Then
                dtTable = New DataView(dsCombo.Tables(0), " categoryunkid NOT IN ( " & clsTraining_Category_Master.enTrainingCategoryDefaultId.Talent_PDP & "," & clsTraining_Category_Master.enTrainingCategoryDefaultId.Successor_PDP & "," & clsTraining_Category_Master.enTrainingCategoryDefaultId.Individual_Development_Plan & " ) ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0)).ToTable

            End If
            With cboTrainingCategory
                .DataTextField = "categoryname"
                .DataValueField = "categoryunkid"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTCategory = Nothing

        End Try
    End Sub
    'Hemant (15 Apr 2021) -- End

    Private Function Save_Click(Optional ByVal blnAddInstructor As Boolean = False, Optional ByVal blnSaveAndSubmit As Boolean = False) As Boolean
        'Sohail (04 Aug 2021) - [blnSaveAndSubmit]
        'Sohail (09 Jun 2021) - [blnAddInstructor]
        Try
            If IsValidData() = False Then Exit Try
            'Sohail (04 Aug 2021) -- Start
            'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
            If blnSaveAndSubmit = True AndAlso mintFormId = 2 Then
                Dim objTPeriod As New clsTraining_Calendar_Master
                objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)

                If eZeeDate.convertDate(DateAndTime.Today.Date) > eZeeDate.convertDate(objTPeriod._EndDate) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 42, "Sorry, you cannot submit the selected training(s) for budget approval. Submission deadline was #calendarenddate#.").Replace("#calendarenddate#", Format(objTPeriod._EndDate, "dd-MMM-yyyy")), Me)
                    Exit Try
                End If
            End If

            If blnSaveAndSubmit = True Then
                Dim lstDept As New List(Of String)
                lstDept.Add(CInt(cboDepartmentList.SelectedValue).ToString)
                Dim lstPeriod As New List(Of String)
                lstPeriod.Add(CInt(cboPeriod.SelectedValue).ToString)
                If IsValidForMaxBudget("", lstDept, lstPeriod, blnSaveAndSubmit) = False Then Exit Try
            End If
            'Sohail (04 Aug 2021) -- End

            Dim gRow As List(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).ToList

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "Please select at least one item from the target group."), Me)
                Exit Try
            End If

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                'Sohail (12 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                Dim lstTargetGroup As New List(Of clsDepttrainingneed_allocation_Tran)
                Dim lstVoidTargetGroup As New List(Of clsDepttrainingneed_allocation_Tran)
                Dim objTargetGroup As New clsDepttrainingneed_allocation_Tran

                Dim lstAllocEmp As New List(Of clsDepttrainingneed_employee_Tran)
                Dim lstVoidAllocEmp As New List(Of clsDepttrainingneed_employee_Tran)
                Dim objAllocEmp As New clsDepttrainingneed_employee_Tran

                Dim lstTResources As New List(Of clsDepttrainingneed_resources_Tran)
                Dim lstVoidTResources As New List(Of clsDepttrainingneed_resources_Tran)
                Dim objTResources As New clsDepttrainingneed_resources_Tran

                Dim lstTFSources As New List(Of clsDepttrainingneed_financingsources_Tran)
                Dim lstVoidTFSources As New List(Of clsDepttrainingneed_financingsources_Tran)
                Dim objTFSources As New clsDepttrainingneed_financingsources_Tran

                Dim lstTCoord As New List(Of clsDepttrainingneed_trainingcoordinator_Tran)
                Dim lstVoidTCoord As New List(Of clsDepttrainingneed_trainingcoordinator_Tran)
                Dim objTCoord As New clsDepttrainingneed_trainingcoordinator_Tran

                Dim lstTCostItem As New List(Of clsDepttrainingneed_costitem_Tran)
                Dim lstVoidTCostItem As New List(Of clsDepttrainingneed_costitem_Tran)
                Dim objTCostItem As New clsDepttrainingneed_costitem_Tran

                Dim gNewRow As List(Of GridViewRow) = Nothing
                Dim gEditRow As List(Of GridViewRow) = Nothing
                'Sohail (12 Apr 2021) -- End

                'Sohail (09 Jun 2021) -- Start
                'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
                Dim lstTInstruct As New List(Of clsDepttrainingneed_traininginstructor_Tran)
                'Dim lstVoidTCostItem As New List(Of clsDepttrainingneed_traininginstructor_Tran)
                Dim objTInstruct As New clsDepttrainingneed_traininginstructor_Tran
                'Sohail (09 Jun 2021) -- End

                Dim strIDs As String = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

                If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                    'Sohail (12 Apr 2021) -- Start
                    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    'Dim lstTargetGroup As New List(Of clsDepttrainingneed_employee_Tran)
                    'Dim lstVoidTargetGroup As New List(Of clsDepttrainingneed_employee_Tran)
                    'Dim objTargetGroup As New clsDepttrainingneed_employee_Tran
                    'Dim gNewRow As List(Of GridViewRow) = Nothing
                    'Sohail (12 Apr 2021) -- End

                    If mintDepartmentaltrainingneedunkid > 0 Then

                        Dim dsTargetGroup As DataSet = objAllocEmp.GetList(CStr(Session("Database_Name")), _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   dtpStartDate.GetDate.Date, _
                                                                   dtpEndDate.GetDate.Date, _
                                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                                   True, "Emp", True, "", mintDepartmentaltrainingneedunkid)

                        'drNew = (From p In dsTargetGroup.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = False) Select (p)).ToList
                        Dim drVoid As List(Of DataRow) = (From p In dsTargetGroup.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = False) Select (p)).ToList
                        Dim strExistIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        gNewRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString) = False).Select(Function(x) x).ToList
                        'Dim gVoidRow As List(Of GridViewRow) = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = False AndAlso strExistIDs.Split(CChar(",")).Contains(dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString) = True).Select(Function(x) x).ToList

                        For Each r As DataRow In drVoid
                            'For Each r As GridViewRow In gVoidRow
                            objAllocEmp = New clsDepttrainingneed_employee_Tran

                            With objAllocEmp
                                .pintDepttrainingneedemployeetranunkid = CInt(r.Item("depttrainingneedemployeetranunkid"))
                                '.pintDepttrainingneedemployeetranunkid = CInt(dgvEmployee.DataKeys(r.RowIndex).Item("depttrainingneedemployeetranunkid"))

                                .pblnIsvoid = True
                                .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                'Sohail (26 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                                .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                                'Sohail (26 Apr 2021) -- End

                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                    .pintVoiduserunkid = CInt(Session("UserId"))
                                    .pintVoidloginemployeeunkid = 0
                                    .pintAuditUserId = CInt(Session("UserId"))
                                Else
                                    .pintVoiduserunkid = 0
                                    .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                    .pintAuditUserId = CInt(Session("Employeeunkid"))
                                End If
                                .pblnIsweb = True
                                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                .pstrClientIp = Session("IP_ADD").ToString()
                                .pstrHostName = Session("HOST_NAME").ToString()
                                .pstrFormName = mstrModuleName
                            End With

                            lstVoidAllocEmp.Add(objAllocEmp)
                        Next

                        gRow = gNewRow

                    End If


                    For Each dgRow As GridViewRow In gRow
                        objAllocEmp = New clsDepttrainingneed_employee_Tran

                        With objAllocEmp
                            .pintDepttrainingneedemployeetranunkid = -1
                            .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                            .pintEmployeeunkid = CInt(dgvEmployee.DataKeys(dgRow.RowIndex)("id").ToString())

                            .pblnIsweb = True
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintUserunkid = CInt(Session("UserId"))
                                .pintLoginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintUserunkid = 0
                                .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName
                        End With

                        lstAllocEmp.Add(objAllocEmp)

                    Next

                    'Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
                    'objTargetGroup._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                    'If objTargetGroup.SaveAll(lstTargetGroup, objTMaster) = False Then
                    '    If objTargetGroup._Message <> "" Then
                    '        DisplayMessage.DisplayMessage(objTargetGroup._Message, Me)
                    '    End If
                    'Else
                    '    mintDepartmentaltrainingneedunkid = objTargetGroup._Departmentaltrainingneedunkid
                    'End If
                    'Dim objTMaster As New clsDepartmentaltrainingneed_master
                    'objTMaster = SetValueMaster()
                    'objTMaster._lstDeptTEmpNew = lstAllocEmp
                    'objTMaster._lstDeptTEmpVoid = lstVoidAllocEmp
                    'If objTMaster.Save(Nothing) = False Then
                    '    If objTMaster._Message <> "" Then
                    '        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                    '    End If
                    'End If

                Else
                    'Sohail (12 Apr 2021) -- Start
                    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    'Dim lstTargetGroup As New List(Of clsDepttrainingneed_allocation_Tran)
                    'Dim lstVoidTargetGroup As New List(Of clsDepttrainingneed_allocation_Tran)
                    'Dim objTargetGroup As New clsDepttrainingneed_allocation_Tran
                    'Dim lstAllocEmp As New List(Of clsDepttrainingneed_employee_Tran)
                    'Dim lstVoidAllocEmp As New List(Of clsDepttrainingneed_employee_Tran)

                    'Dim gNewRow As List(Of GridViewRow) = Nothing
                    'Sohail (12 Apr 2021) -- End


                    '*** Training Allocation ***
                    If mintDepartmentaltrainingneedunkid > 0 Then

                        Dim dsTargetGroup As DataSet = objTargetGroup.GetList("Emp", mintDepartmentaltrainingneedunkid)

                        Dim drVoid As List(Of DataRow) = (From p In dsTargetGroup.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("allocationtranunkid").ToString) = False) Select (p)).ToList
                        Dim strExistIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("allocationtranunkid").ToString)).ToArray)
                        gNewRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString) = False).Select(Function(x) x).ToList
                        'Dim gVoidRow As List(Of GridViewRow) = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = False AndAlso strExistIDs.Split(CChar(",")).Contains(dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString) = True).Select(Function(x) x).ToList

                        For Each r As DataRow In drVoid
                            'For Each r As GridViewRow In gVoidRow
                            objTargetGroup = New clsDepttrainingneed_allocation_Tran

                            With objTargetGroup
                                .pintDepttrainingneedallocationtranunkid = CInt(r.Item("depttrainingneedallocationtranunkid"))
                                '.pintDepttrainingneedallocationtranunkid = CInt(dgvEmployee.DataKeys(r.RowIndex).Item("depttrainingneedallocationtranunkid"))

                                .pblnIsvoid = True
                                .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                'Sohail (26 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                                .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                                'Sohail (26 Apr 2021) -- End

                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                    .pintVoiduserunkid = CInt(Session("UserId"))
                                    .pintVoidloginemployeeunkid = 0
                                    .pintAuditUserId = CInt(Session("UserId"))
                                Else
                                    .pintVoiduserunkid = 0
                                    .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                    .pintAuditUserId = CInt(Session("Employeeunkid"))
                                End If
                                .pblnIsweb = True
                                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                .pstrClientIp = Session("IP_ADD").ToString()
                                .pstrHostName = Session("HOST_NAME").ToString()
                                .pstrFormName = mstrModuleName
                            End With

                            lstVoidTargetGroup.Add(objTargetGroup)
                        Next

                        gRow = gNewRow

                    End If

                    For Each dgRow As GridViewRow In gRow
                        objTargetGroup = New clsDepttrainingneed_allocation_Tran

                        With objTargetGroup
                            .pintDepttrainingneedallocationtranunkid = -1
                            .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                            .pintAllocationtranunkid = CInt(dgvEmployee.DataKeys(dgRow.RowIndex)("id").ToString())

                            .pblnIsweb = True
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintUserunkid = CInt(Session("UserId"))
                                .pintLoginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintUserunkid = 0
                                .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName

                        End With

                        lstTargetGroup.Add(objTargetGroup)

                    Next

                    '*** Training Employee ***
                    gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True).ToList
                    strIDs = String.Join(",", dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True).Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString).ToArray)

                    If (gRow IsNot Nothing AndAlso gRow.Count > 0) OrElse mintDepartmentaltrainingneedunkid > 0 Then
                        'Dim objAllocEmp As New clsDepttrainingneed_employee_Tran 'Sohail (12 Apr 2021)

                        If mintDepartmentaltrainingneedunkid > 0 Then

                            Dim dsAllocEmp As DataSet = objAllocEmp.GetList(CStr(Session("Database_Name")), _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   dtpStartDate.GetDate.Date, _
                                                                   dtpEndDate.GetDate.Date, _
                                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                                   True, "Emp", True, "", mintDepartmentaltrainingneedunkid)

                            Dim drVoid As List(Of DataRow) = (From p In dsAllocEmp.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = False) Select (p)).ToList
                            Dim strExistIDs As String = String.Join(",", (From p In dsAllocEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                            gNewRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString) = False).Select(Function(x) x).ToList
                            'Dim gVoidRow As List(Of GridViewRow) = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = False AndAlso strExistIDs.Split(CChar(",")).Contains(dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString) = True).Select(Function(x) x).ToList

                            For Each r As DataRow In drVoid
                                'For Each r As GridViewRow In gVoidRow
                                objAllocEmp = New clsDepttrainingneed_employee_Tran

                                With objAllocEmp
                                    .pintDepttrainingneedemployeetranunkid = CInt(r.Item("depttrainingneedemployeetranunkid"))
                                    '.pintDepttrainingneedemployeetranunkid = CInt(dgvAllocEmp.DataKeys(r.RowIndex).Item("depttrainingneedemployeetranunkid"))

                                    .pblnIsvoid = True
                                    .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                    'Sohail (26 Apr 2021) -- Start
                                    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                    '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                                    .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                                    'Sohail (26 Apr 2021) -- End

                                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                        .pintVoiduserunkid = CInt(Session("UserId"))
                                        .pintVoidloginemployeeunkid = 0
                                        .pintAuditUserId = CInt(Session("UserId"))
                                    Else
                                        .pintVoiduserunkid = 0
                                        .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                                    End If
                                    .pblnIsweb = True
                                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    .pstrClientIp = Session("IP_ADD").ToString()
                                    .pstrHostName = Session("HOST_NAME").ToString()
                                    .pstrFormName = mstrModuleName
                                End With

                                lstVoidAllocEmp.Add(objAllocEmp)
                            Next

                            gRow = gNewRow

                        End If

                        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                            For Each dgRow As GridViewRow In gRow
                                objAllocEmp = New clsDepttrainingneed_employee_Tran

                                With objAllocEmp
                                    .pintDepttrainingneedemployeetranunkid = -1
                                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                                    .pintEmployeeunkid = CInt(dgvAllocEmp.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())

                                    .pblnIsweb = True
                                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                        .pintUserunkid = CInt(Session("UserId"))
                                        .pintLoginemployeeunkid = 0
                                        .pintAuditUserId = CInt(Session("UserId"))
                                    Else
                                        .pintUserunkid = 0
                                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                                    End If
                                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    .pstrClientIp = Session("IP_ADD").ToString()
                                    .pstrHostName = Session("HOST_NAME").ToString()
                                    .pstrFormName = mstrModuleName
                                End With

                                lstAllocEmp.Add(objAllocEmp)

                            Next
                        End If
                    End If


                    'Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
                    'objTargetGroup._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                    'If objTargetGroup.SaveAll(lstTargetGroup, objTMaster) = False Then
                    '    If objTargetGroup._Message <> "" Then
                    '        DisplayMessage.DisplayMessage(objTargetGroup._Message, Me)
                    '    End If
                    'Else
                    '    mintDepartmentaltrainingneedunkid = objTargetGroup._Departmentaltrainingneedunkid
                    'End If
                    'Dim objTMaster As New clsDepartmentaltrainingneed_master
                    'objTMaster = SetValueMaster()
                    'objTMaster._lstDeptTAllocNew = lstTargetGroup
                    'objTMaster._lstDeptTAllocVoid = lstVoidTargetGroup
                    'objTMaster._lstDeptTEmpNew = lstAllocEmp
                    'objTMaster._lstDeptTEmpVoid = lstVoidAllocEmp
                    'If objTMaster.Save(Nothing) = False Then
                    '    If objTMaster._Message <> "" Then
                    '        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                    '    End If
                    'End If

                End If

                '*** Training Resources ***
                gRow = dgvTrainingResources.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTResources"), CheckBox).Checked = True).ToList
                strIDs = String.Join(",", dgvTrainingResources.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTResources"), CheckBox).Checked = True).Select(Function(x) dgvTrainingResources.DataKeys(x.RowIndex).Item("masterunkid").ToString).ToArray)

                If mintDepartmentaltrainingneedunkid > 0 Then

                    Dim dsTResources As DataSet = objTResources.GetList("Emp", mintDepartmentaltrainingneedunkid)

                    Dim drVoid As List(Of DataRow) = (From p In dsTResources.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("trainingresourceunkid").ToString) = False) Select (p)).ToList
                    Dim strExistIDs As String = String.Join(",", (From p In dsTResources.Tables(0) Select (p.Item("trainingresourceunkid").ToString)).ToArray)
                    gNewRow = dgvTrainingResources.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTResources"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvTrainingResources.DataKeys(x.RowIndex).Item("masterunkid").ToString) = False).Select(Function(x) x).ToList

                    For Each r As DataRow In drVoid

                        objTResources = New clsDepttrainingneed_resources_Tran

                        With objTResources
                            .pintDepttrainingneedresourcestranunkid = CInt(r.Item("depttrainingneedresourcestranunkid"))

                            .pblnIsvoid = True
                            .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            'Sohail (26 Apr 2021) -- Start
                            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                            '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                            .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                            'Sohail (26 Apr 2021) -- End

                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintVoiduserunkid = CInt(Session("UserId"))
                                .pintVoidloginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintVoiduserunkid = 0
                                .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pblnIsweb = True
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName
                        End With

                        lstVoidTResources.Add(objTResources)
                    Next

                    gRow = gNewRow

                End If

                For Each dgRow As GridViewRow In gRow
                    objTResources = New clsDepttrainingneed_resources_Tran

                    With objTResources
                        .pintDepttrainingneedresourcestranunkid = -1
                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                        .pintTrainingresourceunkid = CInt(dgvTrainingResources.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                        .pblnIsweb = True
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = CInt(Session("Employeeunkid"))
                        End If
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With

                    lstTResources.Add(objTResources)
                Next


                '*** Financial Sources ***
                gRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).ToList
                strIDs = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString).ToArray)

                If mintDepartmentaltrainingneedunkid > 0 Then

                    Dim dsTFSources As DataSet = objTFSources.GetList("Emp", mintDepartmentaltrainingneedunkid)

                    Dim drVoid As List(Of DataRow) = (From p In dsTFSources.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("financingsourceunkid").ToString) = False) Select (p)).ToList
                    Dim strExistIDs As String = String.Join(",", (From p In dsTFSources.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                    gNewRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString) = False).Select(Function(x) x).ToList

                    For Each r As DataRow In drVoid

                        objTFSources = New clsDepttrainingneed_financingsources_Tran

                        With objTFSources
                            .pintDepttrainingneedfinancingsourcestranunkid = CInt(r.Item("depttrainingneedfinancingsourcestranunkid"))

                            .pblnIsvoid = True
                            .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            'Sohail (26 Apr 2021) -- Start
                            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                            '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                            .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                            'Sohail (26 Apr 2021) -- End

                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintVoiduserunkid = CInt(Session("UserId"))
                                .pintVoidloginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintVoiduserunkid = 0
                                .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pblnIsweb = True
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName
                        End With

                        lstVoidTFSources.Add(objTFSources)
                    Next

                    gRow = gNewRow

                End If

                For Each dgRow As GridViewRow In gRow
                    objTFSources = New clsDepttrainingneed_financingsources_Tran

                    With objTFSources
                        .pintDepttrainingneedfinancingsourcestranunkid = -1
                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                        .pintFinancingsourceunkid = CInt(dgvFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                        .pblnIsweb = True
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = CInt(Session("Employeeunkid"))
                        End If
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With

                    lstTFSources.Add(objTFSources)
                Next


                '*** Training Coordinator ***
                gRow = dgvTrainingCoordinator.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCoordinator"), CheckBox).Checked = True).ToList
                strIDs = String.Join(",", dgvTrainingCoordinator.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCoordinator"), CheckBox).Checked = True).Select(Function(x) dgvTrainingCoordinator.DataKeys(x.RowIndex).Item("employeeunkid").ToString).ToArray)

                If mintDepartmentaltrainingneedunkid > 0 Then

                    Dim dsTCoord As DataSet = objTCoord.GetList(CStr(Session("Database_Name")), _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               dtpStartDate.GetDate.Date, _
                                                               dtpEndDate.GetDate.Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, _
                                                               True, "Emp", True, "", mintDepartmentaltrainingneedunkid)

                    Dim drVoid As List(Of DataRow) = (From p In dsTCoord.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = False) Select (p)).ToList
                    Dim strExistIDs As String = String.Join(",", (From p In dsTCoord.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                    gNewRow = dgvTrainingCoordinator.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCoordinator"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvTrainingCoordinator.DataKeys(x.RowIndex).Item("employeeunkid").ToString) = False).Select(Function(x) x).ToList

                    For Each r As DataRow In drVoid

                        objTCoord = New clsDepttrainingneed_trainingcoordinator_Tran

                        With objTCoord
                            .pintDepttrainingneedtrainingcoordinatortranunkid = CInt(r.Item("depttrainingneedtrainingcoordinatortranunkid"))

                            .pblnIsvoid = True
                            .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            'Sohail (26 Apr 2021) -- Start
                            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                            '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                            .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                            'Sohail (26 Apr 2021) -- End

                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintVoiduserunkid = CInt(Session("UserId"))
                                .pintVoidloginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintVoiduserunkid = 0
                                .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pblnIsweb = True
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName
                        End With

                        lstVoidTCoord.Add(objTCoord)
                    Next

                    gRow = gNewRow

                End If

                For Each dgRow As GridViewRow In gRow
                    objTCoord = New clsDepttrainingneed_trainingcoordinator_Tran

                    With objTCoord
                        .pintDepttrainingneedtrainingcoordinatortranunkid = -1
                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                        .pintEmployeeunkid = CInt(dgvTrainingCoordinator.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())

                        .pblnIsweb = True
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = CInt(Session("Employeeunkid"))
                        End If
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With

                    lstTCoord.Add(objTCoord)
                Next


                '*** Training Cost Item ***
                gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True).ToList
                strIDs = String.Join(",", dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True).Select(Function(x) dgvTrainingCostItem.DataKeys(x.RowIndex).Item("infounkid").ToString).ToArray)

                If mintDepartmentaltrainingneedunkid > 0 Then

                    Dim dsTCostItem As DataSet = objTCostItem.GetList("Emp", mintDepartmentaltrainingneedunkid)

                    Dim drVoid As List(Of DataRow) = (From p In dsTCostItem.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("costitemunkid").ToString) = False) Select (p)).ToList
                    Dim strExistIDs As String = String.Join(",", (From p In dsTCostItem.Tables(0) Select (p.Item("costitemunkid").ToString)).ToArray)
                    gNewRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvTrainingCostItem.DataKeys(x.RowIndex).Item("infounkid").ToString) = False).Select(Function(x) x).ToList
                    gEditRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvTrainingCostItem.DataKeys(x.RowIndex).Item("infounkid").ToString) = True).Select(Function(x) x).ToList

                    For Each r As DataRow In drVoid

                        objTCostItem = New clsDepttrainingneed_costitem_Tran

                        With objTCostItem
                            .pintDepttrainingneedcostitemtranunkid = CInt(r.Item("depttrainingneedcostitemtranunkid"))

                            .pblnIsvoid = True
                            .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            'Sohail (26 Apr 2021) -- Start
                            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                            '.pstrVoidreason = popupDeleteEmployee.Reason.ToString
                            .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 78, "Wrongly Posted.")
                            'Sohail (26 Apr 2021) -- End

                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintVoiduserunkid = CInt(Session("UserId"))
                                .pintVoidloginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintVoiduserunkid = 0
                                .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pblnIsweb = True
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName
                        End With

                        lstVoidTCostItem.Add(objTCostItem)
                    Next

                    For Each gr As GridViewRow In gEditRow

                        Dim r() As DataRow = dsTCostItem.Tables(0).Select("Costitemunkid = " & CInt(dgvTrainingCostItem.DataKeys(gr.RowIndex).Item("infounkid")) & " ")

                        If r.Length > 0 Then

                            If CType(dgvTrainingCostItem.Rows(gr.RowIndex).FindControl("txtAmount"), Controls_NumberOnly).Decimal_ <> CDec(r(0).Item("Amount")) _
                                OrElse CType(dgvTrainingCostItem.Rows(gr.RowIndex).FindControl("txtApprovedAmount"), Controls_NumberOnly).Decimal_ <> CDec(r(0).Item("Approved_Amount")) _
                                Then

                                objTCostItem = New clsDepttrainingneed_costitem_Tran

                                With objTCostItem
                                    .pintDepttrainingneedcostitemtranunkid = CInt(r(0).Item("depttrainingneedcostitemtranunkid"))
                                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                                    .pintCostitemunkid = CInt(r(0).Item("Costitemunkid"))
                                    Dim decAmt As Decimal = CType(gr.FindControl("txtAmount"), Controls_NumberOnly).Decimal_
                                    .pdecAmount = decAmt
                                    If mintFormId = 3 Then 'Budget Approval users
                                        Dim decApprovedAmt As Decimal = CType(gr.FindControl("txtApprovedAmount"), Controls_NumberOnly).Decimal_
                                        .pdecApproved_Amount = decApprovedAmt
                                    Else
                                        .pdecApproved_Amount = decAmt
                                    End If

                                    .pblnIsweb = True
                                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                        .pintUserunkid = CInt(Session("UserId"))
                                        .pintLoginemployeeunkid = 0
                                        .pintAuditUserId = CInt(Session("UserId"))
                                    Else
                                        .pintUserunkid = 0
                                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                                    End If
                                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    .pstrClientIp = Session("IP_ADD").ToString()
                                    .pstrHostName = Session("HOST_NAME").ToString()
                                    .pstrFormName = mstrModuleName
                                End With

                                lstTCostItem.Add(objTCostItem)

                            End If

                        End If

                    Next

                    gRow = gNewRow

                End If

                For Each dgRow As GridViewRow In gRow
                    objTCostItem = New clsDepttrainingneed_costitem_Tran

                    With objTCostItem
                        .pintDepttrainingneedcostitemtranunkid = -1
                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                        .pintCostitemunkid = CInt(dgvTrainingCostItem.DataKeys(dgRow.RowIndex)("infounkid").ToString())
                        Dim decAmt As Decimal = CType(dgRow.FindControl("txtAmount"), Controls_NumberOnly).Decimal_
                        .pdecAmount = decAmt
                        If mintFormId = 3 Then 'Budget Approval users
                            Dim decApprovedAmt As Decimal = CType(dgRow.FindControl("txtApprovedAmount"), Controls_NumberOnly).Decimal_
                            .pdecApproved_Amount = decApprovedAmt
                        Else
                            .pdecApproved_Amount = decAmt
                        End If

                        .pblnIsweb = True
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = CInt(Session("Employeeunkid"))
                        End If
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName

                    End With

                    lstTCostItem.Add(objTCostItem)
                Next

                '*** Training Instructor ***
                If blnAddInstructor = True Then
                    If chkOtherInstructor.Checked = False Then
                        gRow = Nothing
                        gRow = dgvAddTrainingInstructor.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddTInstructor"), CheckBox).Checked = True).ToList

                        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                            lstTInstruct = New List(Of clsDepttrainingneed_traininginstructor_Tran)
                            objTInstruct = New clsDepttrainingneed_traininginstructor_Tran

                            For Each dgRow As GridViewRow In gRow
                                objTInstruct = New clsDepttrainingneed_traininginstructor_Tran

                                With objTInstruct
                                    .pintDepttrainingneedtraininginstructortranunkid = -1
                                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                                    .pintEmployeeunkid = CInt(dgvAddTrainingInstructor.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())
                                    .pstrOthers_name = ""
                                    .pstrOthers_company = ""
                                    .pstrOthers_department = ""
                                    .pstrOthers_job = ""
                                    .pstrOthers_email = "'"

                                    .pblnIsweb = True
                                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                        .pintUserunkid = CInt(Session("UserId"))
                                        .pintLoginemployeeunkid = 0
                                        .pintAuditUserId = CInt(Session("UserId"))
                                    Else
                                        .pintUserunkid = 0
                                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                                    End If
                                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    .pstrClientIp = Session("IP_ADD").ToString()
                                    .pstrHostName = Session("HOST_NAME").ToString()
                                    .pstrFormName = mstrModuleName

                                End With

                                lstTInstruct.Add(objTInstruct)

                            Next
                        End If
                    Else

                        lstTInstruct = New List(Of clsDepttrainingneed_traininginstructor_Tran)
                        objTInstruct = New clsDepttrainingneed_traininginstructor_Tran

                        With objTInstruct
                            .pintDepttrainingneedtraininginstructortranunkid = -1
                            .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                            .pintEmployeeunkid = 0
                            .pstrOthers_name = txtOthersName.Text
                            .pstrOthers_company = txtOthersCompany.Text
                            .pstrOthers_department = txtOthersDept.Text
                            .pstrOthers_job = txtOthersJob.Text
                            .pstrOthers_email = txtOthersEmail.Text

                            .pblnIsweb = True
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintUserunkid = CInt(Session("UserId"))
                                .pintLoginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintUserunkid = 0
                                .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName

                        End With

                        lstTInstruct.Add(objTInstruct)

                    End If
                End If




                Dim objTMaster As New clsDepartmentaltrainingneed_master
                objTMaster = SetValueMaster()
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                If blnSaveAndSubmit = True AndAlso mintFormId = 1 Then
                    objTMaster._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed
                ElseIf blnSaveAndSubmit = True AndAlso mintFormId = 2 Then
                    objTMaster._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog

                    Dim intRefNo As Integer = objTMaster.getNextRefNo()
                    objTMaster._RefNo = intRefNo.ToString
                End If
                'Sohail (04 Aug 2021) -- End

                Dim decTotAmt As Decimal = 0
                Decimal.TryParse(hfobjlblTotalAmt.Value, decTotAmt)
                objTMaster._Totalcost = decTotAmt

                Dim decTotApprovedAmt As Decimal = 0
                If mintFormId = 3 Then 'Budget Approval users
                    Decimal.TryParse(hfobjlblTotalApprovedAmt.Value, decTotApprovedAmt)
                Else
                    decTotApprovedAmt = decTotAmt
                End If
                objTMaster._Approved_Totalcost = decTotApprovedAmt

                objTMaster._lstDeptTAllocNew = lstTargetGroup
                objTMaster._lstDeptTAllocVoid = lstVoidTargetGroup
                objTMaster._lstDeptTEmpNew = lstAllocEmp
                objTMaster._lstDeptTEmpVoid = lstVoidAllocEmp
                objTMaster._lstDeptTResourceNew = lstTResources
                objTMaster._lstDeptTResourceVoid = lstVoidTResources
                objTMaster._lstDeptTFSourceNew = lstTFSources
                objTMaster._lstDeptTFSourceVoid = lstVoidTFSources
                objTMaster._lstDeptTCoordNew = lstTCoord
                objTMaster._lstDeptTCoordVoid = lstVoidTCoord
                objTMaster._lstDeptTCostItemNew = lstTCostItem
                objTMaster._lstDeptTCostItemVoid = lstVoidTCostItem
                'Sohail (07 Jun 2021) -- Start
                'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                objTMaster._lstDeptTInstructNew = lstTInstruct
                'Sohail (07 JUn 2021) -- End

                If objTMaster.Save(Nothing) = False Then
                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                    End If
                    'Sohail (09 Jun 2021) -- Start
                    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
                Else
                    mintDepartmentaltrainingneedunkid = objTMaster._Departmentaltrainingneedunkid
                    'Sohail (09 Jun 2021) -- End
                End If

                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                If blnSaveAndSubmit = True AndAlso mintFormId = 1 Then
                    mintEmailTypeId = clsDepartmentaltrainingneed_master.enEmailType.Submit_For_Approval_From_Departmental_Training_Need
                    SendEmail(mintDepartmentaltrainingneedunkid.ToString)

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 41, "Selected training(s) Submitted for Approval Successfully!"), Me)

                ElseIf blnSaveAndSubmit = True AndAlso mintFormId = 2 Then
                    mintEmailTypeId = clsDepartmentaltrainingneed_master.enEmailType.Submit_For_Approval_From_Training_Backlog
                    SendEmail(mintDepartmentaltrainingneedunkid.ToString)

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 44, "Selected training(s) Submitted for Budget Approval Successfully!"), Me)
                End If
                'Sohail (04 Aug 2021) -- End

            End If

            If blnAddInstructor = False Then 'Sohail (09 Jun 2021)
            mblnShowAddEditPopup = False
            popAddEdit.Hide()
            mintDepartmentaltrainingneedunkid = 0
            Call FillList("", "")
            End If 'Sohail (09 Jun 2021)

            'Sohail (09 Jun 2021) -- Start
            'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
            Return True
            'Sohail (09 Jun 2021) -- End    

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'Hemant (03 Jun 2021) -- Start
    'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
    Private Sub FillTrainingVenueCombo(ByVal intVenueId As Integer)
        Dim objTVenue As New clstrtrainingvenue_master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsCombo = objTVenue.getListForCombo("List", True)
            If intVenueId <= 0 Then
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 OR (islocked = 1 AND venueunkid = " & intVenueId & " ) ", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboTrainingVenue
                .DataTextField = "name"
                .DataValueField = "venueunkid"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTVenue = Nothing
        End Try
    End Sub
    'Hemant (03 Jun 2021) -- End

    'Sohail (09 Jun 2021) -- Start
    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
    Private Function IsValidInstructor(ByVal RowNo As Integer) As Boolean
        Try
            If chkOtherInstructor.Checked = False Then

                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = dgvAddTrainingInstructor.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddTInstructor"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 84, "Please tick atlease one training instructor from the list."), Me)
                    Return False
                End If
            Else

                If txtOthersName.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 85, "Please enter other instructor name."), Me)
                    txtOthersName.Focus()
                    Return False
                ElseIf txtOthersCompany.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 86, "Please enter other instructor company name."), Me)
                    txtOthersCompany.Focus()
                    Return False
                ElseIf txtOthersDept.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 87, "Please enter other instructor department name."), Me)
                    txtOthersDept.Focus()
                    Return False
                ElseIf txtOthersJob.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 88, "Please enter other instructor job name."), Me)
                    txtOthersJob.Focus()
                    Return False
                ElseIf txtOthersEmail.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 89, "Please enter other instructor email."), Me)
                    txtOthersEmail.Focus()
                    Return False
                End If

                Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                If Expression.IsMatch(txtOthersEmail.Text.Trim) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 90, "Please Enter Valid Email."), Me)
                    txtOthersEmail.Focus()
                    Return False
                End If

                Dim objInstruct As New clsDepttrainingneed_traininginstructor_Tran
                Dim intUnkId As Integer = 0
                If RowNo > 0 Then
                    intUnkId = CInt(dgvTrainingInstructor.DataKeys(RowNo).Item("depttrainingneedtraininginstructortranunkid"))
                End If
                If objInstruct.isExist(mintDepartmentaltrainingneedunkid, 0, txtOthersName.Text, txtOthersEmail.Text, intUnkId) > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 91, "Sorry, Instructor name and email are already exist."), Me)
                    txtOthersName.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub ResetInstructor()
        Try
            txtOthersName.Text = ""
            txtOthersCompany.Text = ""
            txtOthersDept.Text = ""
            txtOthersJob.Text = ""
            txtOthersEmail.Text = ""
            chkOtherInstructor.Checked = False
            Call chkOtherInstructor_CheckedChanged(chkOtherInstructor, New System.EventArgs)
            chkOtherInstructor.Enabled = True

            btnAddInstructor.Enabled = True
            btnUpdateInstructor.Enabled = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Jun 2021) -- End

#End Region

#Region " Combobox Events "
    Protected Sub cboTargetedGroupList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTargetedGroupList.SelectedIndexChanged
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim intColType As Integer = 0
        Try
            'If CInt(cboTargetedGroupList.SelectedValue) <= 0 Then
            '    cboEmployeeNameList.Enabled = True
            'Else
            '    cboEmployeeNameList.SelectedValue = "0"
            '    cboEmployeeNameList.Enabled = False
            'End If
            'If CInt(cboDepartmentList.SelectedValue) <= 0 AndAlso mblnIsDeptTrainingFromBacklog = False Then Exit Try

            objlblEmployeeNameList.Text = cboTargetedGroupList.SelectedItem.Text

            Select Case CInt(cboTargetedGroupList.SelectedValue)

                Case -1
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                    dtTable = New DataView(dsList.Tables("List"), "stationunkid = -999 ", "", DataViewRowState.CurrentRows).ToTable

                Case 0 'Employee Names
                    Dim objEmp As New clsEmployee_Master
                    dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    True, "Emp", True, , CInt(cboDepartmentList.SelectedValue))

                    intColType = 5

                    dtTable = dsList.Tables(0)
                    objEmp = Nothing

                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                    If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
                        If CInt(Session("UserId")) > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                    If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case enAllocation.DEPARTMENT

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                    'If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                    '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    'Else
                    '    dtTable = dsList.Tables("List")
                    'End If
                    Dim sFilter As String = ""
                    If CInt(cboDepartmentList.SelectedValue) > 0 Then
                        sFilter = " departmentunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION_GROUP

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                    If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                    If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case enAllocation.UNIT_GROUP

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                    If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                    If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                    If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case enAllocation.JOB_GROUP

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                    If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case enAllocation.JOBS

                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                    If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If


                Case enAllocation.CLASS_GROUP

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                    If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                Case enAllocation.CLASSES

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                    If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
                        Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
                        StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                        dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = dsList.Tables("List")
                    End If

                    'Case enAllocation.GradeGroup

                    '    Dim objGradeGrp As New clsGradeGroup
                    '    dsList = objGradeGrp.GetList("List", True)
                    '    objGradeGrp = Nothing

                    '    dtTable = dsList.Tables("List")

                    'Case enAllocation.Grade

                    '    Dim objGrade As New clsGrade
                    '    dsList = objGrade.GetList("List", True)

                    '    dtTable = dsList.Tables("List")

                    'Case enAllocation.GradeLevel

                    '    Dim objGradeLvl As New clsGradeLevel
                    '    dsList = objGradeLvl.GetList("List", True)
                    '    objGradeLvl = Nothing

                    '    dtTable = dsList.Tables("List")

                Case enAllocation.COST_CENTER

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2
                    dtTable = dsList.Tables("List")

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("code").ColumnName = "Code"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobunkid").ColumnName = "Id"
                dtTable.Columns("Code").ColumnName = "Code"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentercode").ColumnName = "Code"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(2).ColumnName = "Code"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_code").ColumnName = "Code"
                dtTable.Columns("country_name").ColumnName = "Name"
            ElseIf intColType = 5 Then
                dtTable.Columns("employeeunkid").ColumnName = "Id"
                dtTable.Columns("employeecode").ColumnName = "Code"
                dtTable.Columns("EmpCodeName").ColumnName = "Name"
            End If

            If dtTable.Select("ID = 0 ").Length <= 0 Then
                Dim dr As DataRow = dtTable.NewRow
                dr.Item("Id") = 0
                dr.Item("Code") = ""
                dr.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 76, " Select")
                dtTable.Rows.Add(dr)
            End If

            If intColType <> 5 Then
                dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable
            End If

            With cboEmployeeNameList
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable = Nothing
        End Try
    End Sub

    Protected Sub cboDepartmentList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartmentList.SelectedIndexChanged
        Try
            'Language.setLanguage("frmNewMDI")
            If CInt(cboDepartmentList.SelectedValue) > 0 Then
                'Hemant (26 Mar 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                'lblDepartmentList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmNewMDI",CInt(HttpContext.Current.Session("LangId")),"btnDepartment", "Department")
                lblDepartmentList.Text = mstrTrainingNeedAllocationName
                'Hemant (26 Mar 2021) -- End
                'Call cboTargetedGroupList_SelectedIndexChanged(cboTargetedGroupList, New System.EventArgs)

                Call FillList("", "")
                pnlData.Visible = True
            ElseIf mintFormId = 1 Then
                lblDepartmentList.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 77, "Please select #department# to continue.").Replace("#department#", mstrTrainingNeedAllocationName)

                pnlData.Visible = False
            ElseIf mintFormId = 2 OrElse mintFormId = 3 Then
                Call FillList("", "")
                pnlData.Visible = True
            End If
            'Language.setLanguage(mstrModuleNameList)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboTargetedGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTargetedGroup.SelectedIndexChanged
        Try
            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                'pnlAllocEmp.Visible = False
                chkChooseEmployee.Checked = False
                Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)
                chkChooseEmployee.Visible = False
            Else
                'pnlAllocEmp.Visible = True
                chkChooseEmployee.Visible = True
            End If
            Call FillTargetedGroup()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboCompetences_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompetences.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            If CInt(cboCompetences.SelectedValue) > 0 Then
                'Dim objPdpTraining As New clsPdpgoals_trainingneed_Tran
                'dsCombo = objPdpTraining.GetDistinctTraining("List", CInt(cboCompetences.SelectedValue), True, Nothing)
                'With cboTrainingNeeded
                '    .DataTextField = "name"
                '    .DataValueField = "masterunkid"
                '    .DataSource = dsCombo.Tables(0)
                '    .DataBind()
                '    .SelectedValue = "0"
                'End With
                'objPdpTraining = Nothing
                Dim objDeptTNeetMaster As New clsDepartmentaltrainingneed_master
                dsCombo = objDeptTNeetMaster.getTrainingCourseListForCombo(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(cboCompetences.SelectedValue), "List", True)
                With cboTrainingNeeded
                    .DataTextField = "name"
                    .DataValueField = "id"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
                objDeptTNeetMaster = Nothing
            Else
                Dim objCommon As New clsCommon_Master
                dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

                With cboTrainingNeeded
                    .DataTextField = "name"
                    .DataValueField = "masterunkid"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
                objCommon = Nothing
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboTrainingCategoryList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCategoryList.SelectedIndexChanged
        Try
            If CInt(cboTrainingCategoryList.SelectedValue) > 0 Then
                Dim objTCat As New clsTraining_Category_Master
                objTCat._Categoryunkid = CInt(cboTrainingCategoryList.SelectedValue)
                cboPriorityList.SelectedValue = objTCat._Priorityunkid.ToString
            End If
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboTrainingCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCategory.SelectedIndexChanged
        Try
            If CInt(cboTrainingCategory.SelectedValue) > 0 Then
                Dim objTCat As New clsTraining_Category_Master
                objTCat._Categoryunkid = CInt(cboTrainingCategory.SelectedValue)
                cboPriority.SelectedValue = objTCat._Priorityunkid.ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboEmployeeNameList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployeeNameList.SelectedIndexChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPeriodList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodList.SelectedIndexChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPriorityList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPriorityList.SelectedIndexChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboTrainingName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingName.SelectedIndexChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboConditionList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboConditionList.SelectedIndexChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboStatusList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatusList.SelectedIndexChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtTotalCostList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalCostList.TextChanged
        Try
            If mdecPrevTotalCost <> txtTotalCostList.Decimal_ Then
                mdecPrevTotalCost = txtTotalCostList.Decimal_
                Call FillList("", "")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub cboTargetedGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTargetedGroup.SelectedIndexChanged
    '    Dim dsList As New DataSet
    '    Dim dtTable As DataTable = Nothing
    '    Dim intColType As Integer = 0
    '    Try
    '        Select Case CInt(cboTargetedGroup.SelectedValue)

    '            Case 0 'Employee Names

    '                Dim objEmp As New clsEmployee_Master
    '                dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                        Session("UserAccessModeSetting").ToString, True, _
    '                                        True, "Emp", False)

    '                intColType = 5

    '                dtTable = dsList.Tables(0)

    '            Case enAnalysisReport.Branch
    '                Dim objBranch As New clsStation
    '                dsList = objBranch.GetList("List", True)
    '                objBranch = Nothing

    '                If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
    '                    If CInt(Session("UserId")) > 0 Then
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    End If
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAnalysisReport.DepartmentGroup
    '                Dim objDeptGrp As New clsDepartmentGroup
    '                dsList = objDeptGrp.GetList("List", True)
    '                objDeptGrp = Nothing

    '                If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAnalysisReport.Department

    '                Dim objDept As New clsDepartment
    '                dsList = objDept.GetList("List", True)
    '                objDept = Nothing

    '                If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAnalysisReport.SectionGroup

    '                Dim objSG As New clsSectionGroup
    '                dsList = objSG.GetList("List", True)
    '                objSG = Nothing

    '                If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAnalysisReport.Section

    '                Dim objSection As New clsSections
    '                dsList = objSection.GetList("List", True)
    '                objSection = Nothing

    '                If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAnalysisReport.UnitGroup

    '                Dim objUG As New clsUnitGroup
    '                dsList = objUG.GetList("List", True)
    '                objUG = Nothing

    '                If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAnalysisReport.Unit

    '                Dim objUnit As New clsUnits
    '                dsList = objUnit.GetList("List", True)
    '                objUnit = Nothing

    '                If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAnalysisReport.Team

    '                Dim objTeam As New clsTeams
    '                dsList = objTeam.GetList("List", True)
    '                objTeam = Nothing

    '                If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAnalysisReport.JobGroup

    '                Dim objjobGRP As New clsJobGroup
    '                dsList = objjobGRP.GetList("List", True)
    '                objjobGRP = Nothing

    '                If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAnalysisReport.Job

    '                Dim objJobs As New clsJobs
    '                dsList = objJobs.GetList("List", True)
    '                objJobs = Nothing

    '                intColType = 1

    '                If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAnalysisReport.ClassGroup

    '                Dim objClassGrp As New clsClassGroup
    '                dsList = objClassGrp.GetList("List", True)
    '                objClassGrp = Nothing

    '                If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAnalysisReport.Classs

    '                Dim objClass As New clsClass
    '                dsList = objClass.GetList("List", True)
    '                objClass = Nothing

    '                If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
    '                    Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
    '                    StrFilter = StrFilter.Replace("classunkid", "classesunkid")
    '                    dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAnalysisReport.GradeGroup

    '                Dim objGradeGrp As New clsGradeGroup
    '                dsList = objGradeGrp.GetList("List", True)
    '                objGradeGrp = Nothing

    '                dtTable = dsList.Tables("List")

    '            Case enAnalysisReport.Grade

    '                Dim objGrade As New clsGrade
    '                dsList = objGrade.GetList("List", True)

    '                dtTable = dsList.Tables("List")

    '            Case enAnalysisReport.GradeLevel

    '                Dim objGradeLvl As New clsGradeLevel
    '                dsList = objGradeLvl.GetList("List", True)
    '                objGradeLvl = Nothing

    '                dtTable = dsList.Tables("List")

    '            Case enAnalysisReport.CostCenter

    '                Dim objConstCenter As New clscostcenter_master
    '                dsList = objConstCenter.GetList("List", True)
    '                objConstCenter = Nothing

    '                intColType = 2
    '                dtTable = dsList.Tables("List")

    '        End Select

    '        If intColType = 0 Then
    '            dtTable.Columns(0).ColumnName = "Id"
    '            dtTable.Columns("code").ColumnName = "Code"
    '            dtTable.Columns("name").ColumnName = "Name"
    '        ElseIf intColType = 1 Then
    '            dtTable.Columns("jobgroupunkid").ColumnName = "Id"
    '            dtTable.Columns("Code").ColumnName = "Code"
    '            dtTable.Columns("jobname").ColumnName = "Name"
    '        ElseIf intColType = 2 Then
    '            dtTable.Columns(0).ColumnName = "Id"
    '            dtTable.Columns("costcentercode").ColumnName = "Code"
    '            dtTable.Columns("costcentername").ColumnName = "Name"
    '        ElseIf intColType = 3 Then
    '            dtTable.Columns(1).ColumnName = "Id"
    '            dtTable.Columns(2).ColumnName = "Code"
    '            dtTable.Columns(0).ColumnName = "Name"
    '        ElseIf intColType = 4 Then
    '            dtTable.Columns(0).ColumnName = "Id"
    '            dtTable.Columns("country_code").ColumnName = "Code"
    '            dtTable.Columns("country_name").ColumnName = "Name"
    '        ElseIf intColType = 5 Then
    '            dtTable.Columns("employeeunkid").ColumnName = "Id"
    '            dtTable.Columns("employeecode").ColumnName = "Code"
    '            dtTable.Columns("employeename").ColumnName = "Name"
    '        End If

    '        dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

    '        Dim dtCol As New DataColumn
    '        dtCol.ColumnName = "IsChecked"
    '        dtCol.Caption = ""
    '        dtCol.DataType = System.Type.GetType("System.Boolean")
    '        dtCol.DefaultValue = False
    '        dtTable.Columns.Add(dtCol)

    '        If dtTable.Rows.Count <= 0 Then

    '        End If

    '        dgvAddEmployee.DataSource = dtTable
    '        dgvAddEmployee.DataBind()

    '        dtTable = Nothing

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub txtRefnoList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefnoList.TextChanged
        Try
            'If mdecPrevTotalCost <> txtTotalCostList.Decimal_ Then
            'mdecPrevTotalCost = txtTotalCostList.Decimal_
            Call FillList("", "")
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event "

    Protected Sub btnCloseList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseList.Click
        Try
            Response.Redirect(mstrURLReferer, False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddList.Click
        Try
            If CBool(Session("AllowToAddDepartmentalTrainingNeed")) = False Then Exit Try

            If CInt(cboDepartmentList.SelectedValue) <= 0 AndAlso mintFormId = 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select #department#.").Replace("#department#", mstrTrainingNeedAllocationName), Me)
                cboDepartmentList.Focus()
                Exit Try
            ElseIf CInt(cboDepartmentList.SelectedValue) = 0 AndAlso (mintFormId = 2 OrElse mintFormId = 3) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select #department#.").Replace("#department#", mstrTrainingNeedAllocationName), Me)
                cboDepartmentList.Focus()
                Exit Try
            End If

            mintDepartmentaltrainingneedunkid = 0
            FillTrainingCategoryCombo(0)
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            FillTrainingVenueCombo(0)
            'Hemant (03 Jun 2021) -- End
            'cboPeriod.SelectedValue = "0"
            cboCompetences.SelectedValue = "0"
            Call cboCompetences_SelectedIndexChanged(cboCompetences, New System.EventArgs)
            cboTrainingCategory.SelectedValue = "0"
            cboTrainingNeeded.SelectedValue = "0"
            cboLearningMethod.SelectedValue = "0"
            'cboTargetedGroup.SelectedValue = "0"
            'Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)
            txtNoOfStaff.Decimal_ = 0
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            cboPriority.SelectedValue = "0"
            cboTrainingProvider.SelectedValue = "0"
            cboTrainingVenue.SelectedValue = "0"
            txtCommentRemark.Text = ""
            chkCertiRequired.Checked = False
            txtOtherCompetence.Text = ""
            txtOtherTCourse.Text = ""
            chkOtherCompetences.Checked = False
            Call chkOtherCompetences_CheckedChanged(chkOtherCompetences, New System.EventArgs)
            chkOtherTrainingCource.Checked = False
            Call chkOtherTrainingCource_CheckedChanged(chkOtherTrainingCource, New System.EventArgs)
            chkChooseEmployee.Checked = False
            chkChooseEmployee.Visible = False
            chkTrainingCostOptional.Checked = False
            pnlAllocEmp.Visible = False

            cboTargetedGroup.Enabled = True
            txtNoOfStaff.Enabled = True
            dgvEmployee.Enabled = True
            cboTrainingCategory.Enabled = True
            cboPriority.Enabled = True
            cboPeriod.Enabled = True
            chkOtherCompetences.Enabled = True
            cboCompetences.Enabled = True
            txtOtherCompetence.Enabled = True
            cboTrainingNeeded.Enabled = True

            pnlTrainingInfo.Enabled = True
            pnlTargetGroup.Enabled = True
            pnlTrainingResources.Enabled = True
            pnlFinancingSources.Enabled = True
            pnlTrainingCoordinator.Enabled = True
            'Sohail (09 Jun 2021) -- Start
            'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
            pnlTrainingInstructor.Enabled = True
            Call Reset()
            'Sohail (09 Jun 2021) -- End

            objlblCostItemTotal.Text = Format(0, CStr(Session("fmtCurrency")))
            objlblCostItemApprovedTotal.Text = Format(0, CStr(Session("fmtCurrency")))
            hfobjlblTotalAmt.Value = "0"
            hfobjlblTotalApprovedAmt.Value = "0"

            Call FillTargetedGroup()
            Call FillAllocEmployee()
            Call FillTrainingResources()
            Call FillFinancingSource()
            Call FillTrainingCoordinator()
            Call FillTrainingCostItem()
            'Sohail (09 Jun 2021) -- Start
            'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
            Call FillInstructor()
            Call FillTrainingInstructor()
            'Sohail (09 Jun 2021) -- End

            TabName.Value = "TrainingInfo"
            mblnShowAddEditPopup = True
            popAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearchList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchList.Click
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRestList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRestList.Click
        Try
            cboTargetedGroupList.SelectedIndex = 0
            cboEmployeeNameList.SelectedValue = "0"
            cboEmployeeNameList.Enabled = True
            'cboPeriodList.SelectedValue = "0"
            cboPriorityList.SelectedValue = "0"
            cboTrainingName.SelectedValue = "0"
            cboTrainingCategoryList.SelectedValue = "0"
            txtTotalCostList.Decimal_ = 0
            cboStatusList.SelectedIndex = 0
            txtRefnoList.Text = ""

            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnShowAddEditPopup = False
            popAddEdit.Hide()
            mintDepartmentaltrainingneedunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboTrainingVenue.SelectedValue) > 0 Then
                Dim objVenueMaster As New clstrtrainingvenue_master
                objVenueMaster._Venueunkid = CInt(cboTrainingVenue.SelectedValue)
                If CInt(txtNoOfStaff.Text) > CInt(objVenueMaster._Capacity) Then
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 92, "The targeted numbers exceed the maximum capacity limit of the selected venue, do you wish to continue?")
                    Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
                    cnfConfirm.Show()
                        Else
                    Call Save_Click()
                        End If
                        Else
                Call Save_Click()
                End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
    Protected Sub btnSaveAndSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAndSubmit.Click
        Try
            If CInt(cboTrainingVenue.SelectedValue) > 0 Then
                Dim objVenueMaster As New clstrtrainingvenue_master
                objVenueMaster._Venueunkid = CInt(cboTrainingVenue.SelectedValue)
                If CInt(txtNoOfStaff.Text) > CInt(objVenueMaster._Capacity) Then
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 92, "The targeted numbers exceed the maximum capacity limit of the selected venue, do you wish to continue?")
                    Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
                    cnfConfirm.Show()
                Else
                    Call Save_Click(False, True)
                End If
            Else
                Call Save_Click(False, True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Aug 2021) -- End

    'Protected Sub btnSaveAddEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddEmployee.Click
    '    Try
    '        If IsValidData(False) = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddTargetedGroup"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Please tick atlease one item from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
    '                Dim lstTargetGroup As New List(Of clsDepttrainingneed_employee_Tran)
    '                Dim objTargetGroup As New clsDepttrainingneed_employee_Tran

    '                For Each dgRow As GridViewRow In gRow
    '                    objTargetGroup = New clsDepttrainingneed_employee_Tran

    '                    With objTargetGroup
    '                        .pintDepttrainingneedemployeetranunkid = -1
    '                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                        .pintEmployeeunkid = CInt(dgvAddEmployee.DataKeys(dgRow.RowIndex)("id").ToString())

    '                        .pblnIsweb = True
    '                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                            .pintUserunkid = CInt(Session("UserId"))
    '                            .pintLoginemployeeunkid = 0
    '                            .pintAuditUserId = CInt(Session("UserId"))
    '                        Else
    '                            .pintUserunkid = 0
    '                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                            .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                        End If
    '                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrClientIp = Session("IP_ADD").ToString()
    '                        .pstrHostName = Session("HOST_NAME").ToString()
    '                        .pstrFormName = mstrModuleName
    '                    End With

    '                    lstTargetGroup.Add(objTargetGroup)

    '                Next

    '                Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '                objTargetGroup._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                If objTargetGroup.SaveAll(lstTargetGroup, objTMaster) = False Then
    '                    If objTargetGroup._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objTargetGroup._Message, Me)
    '                    End If
    '                Else
    '                    mintDepartmentaltrainingneedunkid = objTargetGroup._Departmentaltrainingneedunkid
    '                End If

    '            Else
    '                Dim lstTargetGroup As New List(Of clsDepttrainingneed_allocation_Tran)
    '                Dim objTargetGroup As New clsDepttrainingneed_allocation_Tran

    '                For Each dgRow As GridViewRow In gRow
    '                    objTargetGroup = New clsDepttrainingneed_allocation_Tran

    '                    With objTargetGroup
    '                        .pintDepttrainingneedallocationtranunkid = -1
    '                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                        .pintAllocationtranunkid = CInt(dgvAddEmployee.DataKeys(dgRow.RowIndex)("id").ToString())

    '                        .pblnIsweb = True
    '                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                            .pintUserunkid = CInt(Session("UserId"))
    '                            .pintLoginemployeeunkid = 0
    '                            .pintAuditUserId = CInt(Session("UserId"))
    '                        Else
    '                            .pintUserunkid = 0
    '                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                            .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                        End If
    '                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrClientIp = Session("IP_ADD").ToString()
    '                        .pstrHostName = Session("HOST_NAME").ToString()
    '                        .pstrFormName = mstrModuleName

    '                    End With

    '                    lstTargetGroup.Add(objTargetGroup)

    '                Next

    '                Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '                objTargetGroup._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                If objTargetGroup.SaveAll(lstTargetGroup, objTMaster) = False Then
    '                    If objTargetGroup._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objTargetGroup._Message, Me)
    '                    End If
    '                Else
    '                    mintDepartmentaltrainingneedunkid = objTargetGroup._Departmentaltrainingneedunkid
    '                End If

    '            End If

    '            Call FillTargetedGroup()

    '            mblnShowAddEmployeePopup = False
    '            popupAddEmployee.Hide()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnSaveAddAllocEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddAllocEmp.Click
    '    Try
    '        If IsValidData(False) = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddAllocEmp"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Please tick atlease one item from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            If CInt(cboTargetedGroup.SelectedValue) > 0 Then 'Not Employee Names
    '                Dim lstAllocEmp As New List(Of clsDepttrainingneed_employee_Tran)
    '                Dim objAllocEmp As New clsDepttrainingneed_employee_Tran

    '                For Each dgRow As GridViewRow In gRow
    '                    objAllocEmp = New clsDepttrainingneed_employee_Tran

    '                    With objAllocEmp
    '                        .pintDepttrainingneedemployeetranunkid = -1
    '                        .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                        .pintEmployeeunkid = CInt(dgvAddAllocEmp.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())

    '                        .pblnIsweb = True
    '                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                            .pintUserunkid = CInt(Session("UserId"))
    '                            .pintLoginemployeeunkid = 0
    '                            .pintAuditUserId = CInt(Session("UserId"))
    '                        Else
    '                            .pintUserunkid = 0
    '                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                            .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                        End If
    '                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrClientIp = Session("IP_ADD").ToString()
    '                        .pstrHostName = Session("HOST_NAME").ToString()
    '                        .pstrFormName = mstrModuleName
    '                    End With

    '                    lstAllocEmp.Add(objAllocEmp)

    '                Next

    '                Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '                objAllocEmp._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                If objAllocEmp.SaveAll(lstAllocEmp, objTMaster) = False Then
    '                    If objAllocEmp._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objAllocEmp._Message, Me)
    '                    End If
    '                Else
    '                    mintDepartmentaltrainingneedunkid = objAllocEmp._Departmentaltrainingneedunkid
    '                End If

    '            End If

    '            Call FillAllocEmployee()

    '            mblnShowAddAllocEmpPopup = False
    '            popupAddAllocEmp.Hide()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAddTResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTResources.Click
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddTResources.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddTResources"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Please tick atlease one training resource from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstTResources As New List(Of clsDepttrainingneed_resources_Tran)
    '            Dim objTResources As New clsDepttrainingneed_resources_Tran

    '            For Each dgRow As GridViewRow In gRow
    '                objTResources = New clsDepttrainingneed_resources_Tran

    '                With objTResources
    '                    .pintDepttrainingneedresourcestranunkid = -1
    '                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                    .pintTrainingresourceunkid = CInt(dgvAddTResources.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

    '                    .pblnIsweb = True
    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintUserunkid = CInt(Session("UserId"))
    '                        .pintLoginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintUserunkid = 0
    '                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName

    '                End With

    '                lstTResources.Add(objTResources)

    '            Next

    '            Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '            objTResources._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '            If objTResources.SaveAll(lstTResources, objTMaster) = False Then
    '                If objTResources._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTResources._Message, Me)
    '                End If
    '            Else
    '                mintDepartmentaltrainingneedunkid = objTResources._Departmentaltrainingneedunkid
    '            End If

    '            Call FillTrainingResources()

    '            mblnShowTResourcesPopup = False
    '            popupAddTResources.Hide()

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAddFSource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFSource.Click
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddFSource"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Please tick atlease one financing source from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstFSource As New List(Of clsDepttrainingneed_financingsources_Tran)
    '            Dim objFSource As New clsDepttrainingneed_financingsources_Tran

    '            For Each dgRow As GridViewRow In gRow
    '                objFSource = New clsDepttrainingneed_financingsources_Tran

    '                With objFSource
    '                    .pintDepttrainingneedfinancingsourcestranunkid = -1
    '                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                    .pintFinancingsourceunkid = CInt(dgvAddFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

    '                    .pblnIsweb = True
    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintUserunkid = CInt(Session("UserId"))
    '                        .pintLoginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintUserunkid = 0
    '                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName

    '                End With

    '                lstFSource.Add(objFSource)

    '            Next

    '            Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '            objFSource._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '            If objFSource.SaveAll(lstFSource, objTMaster) = False Then
    '                If objFSource._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objFSource._Message, Me)
    '                End If
    '            Else
    '                mintDepartmentaltrainingneedunkid = objFSource._Departmentaltrainingneedunkid
    '            End If

    '            Call FillFinancingSource()

    '            mblnShowFinancingSourcePopup = False
    '            popupFinancingSource.Hide()

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAddTCoordinator_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTCoordinator.Click
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddTCoordinator.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddTCoordinator"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "Please tick atlease one training coordinator from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstTCoord As New List(Of clsDepttrainingneed_trainingcoordinator_Tran)
    '            Dim objTCoord As New clsDepttrainingneed_trainingcoordinator_Tran

    '            For Each dgRow As GridViewRow In gRow
    '                objTCoord = New clsDepttrainingneed_trainingcoordinator_Tran

    '                With objTCoord
    '                    .pintDepttrainingneedtrainingcoordinatortranunkid = -1
    '                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                    .pintEmployeeunkid = CInt(dgvAddTCoordinator.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())

    '                    .pblnIsweb = True
    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintUserunkid = CInt(Session("UserId"))
    '                        .pintLoginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintUserunkid = 0
    '                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName

    '                End With

    '                lstTCoord.Add(objTCoord)

    '            Next

    '            Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '            objTCoord._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '            If objTCoord.SaveAll(lstTCoord, objTMaster) = False Then
    '                If objTCoord._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTCoord._Message, Me)
    '                End If
    '            Else
    '                mintDepartmentaltrainingneedunkid = objTCoord._Departmentaltrainingneedunkid
    '            End If

    '            Call FillTrainingCoordinator()

    '            mblnShowTrainingCoordinatorPopup = False
    '            popupAddTrainingCoordinator.Hide()

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAddTCostItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTCostItem.Click
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAddTCostItem"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "Please tick atlease one training cost item from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstTCostItem As New List(Of clsDepttrainingneed_costitem_Tran)
    '            Dim objTCostItem As New clsDepttrainingneed_costitem_Tran

    '            For Each dgRow As GridViewRow In gRow
    '                objTCostItem = New clsDepttrainingneed_costitem_Tran

    '                With objTCostItem
    '                    .pintDepttrainingneedcostitemtranunkid = -1
    '                    .pintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '                    .pintCostitemunkid = CInt(dgvAddTrainingCostItem.DataKeys(dgRow.RowIndex)("infounkid").ToString())
    '                    'Dim decAmt As Decimal = 0
    '                    'Decimal.TryParse(CType(dgRow.FindControl("txtAmount"), Controls_NumberOnly).Text, decAmt)
    '                    '.pdecAmount = decAmt
    '                    .pdecAmount = 0
    '                    .pdecApproved_Amount = 0

    '                    .pblnIsweb = True
    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintUserunkid = CInt(Session("UserId"))
    '                        .pintLoginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintUserunkid = 0
    '                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName

    '                End With

    '                lstTCostItem.Add(objTCostItem)

    '            Next

    '            Dim objTMaster As clsDepartmentaltrainingneed_master = SetValueMaster()
    '            objTCostItem._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
    '            If objTCostItem.SaveAll(lstTCostItem, objTMaster) = False Then
    '                If objTCostItem._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTCostItem._Message, Me)
    '                End If
    '            Else
    '                mintDepartmentaltrainingneedunkid = objTCostItem._Departmentaltrainingneedunkid
    '            End If

    '            Call FillTrainingCostItem()

    '            mblnShowTrainingCostItemPopup = False
    '            popupTrainingCostItem.Hide()

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub popupDeleteEmployee_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteEmployee.buttonDelReasonYes_Click
    '    Dim objTEmployee As New clsDepttrainingneed_employee_Tran
    '    Dim objTAllocation As New clsDepttrainingneed_allocation_Tran
    '    Try
    '        If IsValidData(False) = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 33, "Please tick atlease one item from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
    '                Dim lstTargetGroup As New List(Of clsDepttrainingneed_employee_Tran)
    '                Dim objTargetGroup As New clsDepttrainingneed_employee_Tran

    '                For Each dgRow As GridViewRow In gRow
    '                    objTargetGroup = New clsDepttrainingneed_employee_Tran

    '                    With objTargetGroup
    '                        .pintDepttrainingneedemployeetranunkid = CInt(dgvEmployee.DataKeys(dgRow.RowIndex).Item("id"))

    '                        .pblnIsvoid = True
    '                        .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                            .pintVoiduserunkid = CInt(Session("UserId"))
    '                            .pintVoidloginemployeeunkid = 0
    '                            .pintAuditUserId = CInt(Session("UserId"))
    '                        Else
    '                            .pintVoiduserunkid = 0
    '                            .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                            .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                        End If
    '                        .pblnIsweb = True
    '                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrClientIp = Session("IP_ADD").ToString()
    '                        .pstrHostName = Session("HOST_NAME").ToString()
    '                        .pstrFormName = mstrModuleName
    '                    End With

    '                    lstTargetGroup.Add(objTargetGroup)

    '                Next

    '                If objTEmployee.VoidAll(lstTargetGroup, Nothing) = True Then
    '                    Call FillTargetedGroup()
    '                ElseIf objTEmployee._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTEmployee._Message, Me)
    '                    Exit Try
    '                End If
    '            Else
    '                Dim lstTargetGroup As New List(Of clsDepttrainingneed_allocation_Tran)
    '                Dim objTargetGroup As New clsDepttrainingneed_allocation_Tran

    '                For Each dgRow As GridViewRow In gRow
    '                    objTargetGroup = New clsDepttrainingneed_allocation_Tran

    '                    With objTargetGroup
    '                        .pintDepttrainingneedallocationtranunkid = CInt(dgvEmployee.DataKeys(dgRow.RowIndex).Item("id"))

    '                        .pblnIsvoid = True
    '                        .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                            .pintVoiduserunkid = CInt(Session("UserId"))
    '                            .pintVoidloginemployeeunkid = 0
    '                            .pintAuditUserId = CInt(Session("UserId"))
    '                        Else
    '                            .pintVoiduserunkid = 0
    '                            .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                            .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                        End If
    '                        .pblnIsweb = True
    '                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrClientIp = Session("IP_ADD").ToString()
    '                        .pstrHostName = Session("HOST_NAME").ToString()
    '                        .pstrFormName = mstrModuleName
    '                    End With

    '                    lstTargetGroup.Add(objTargetGroup)

    '                Next

    '                Dim lstAllocEmp As List(Of clsDepttrainingneed_employee_Tran) = Nothing
    '                Dim strFilter As String = ""
    '                Dim strIDs As String = ""
    '                strIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("allocationtranunkid").ToString).ToArray)

    '                If strIDs.Trim <> "" Then
    '                    Select Case CInt(cboTargetedGroup.SelectedValue)

    '                        Case enAllocation.BRANCH
    '                            strFilter &= " AND T.stationunkid IN (" & strIDs & ") "

    '                        Case enAllocation.DEPARTMENT_GROUP
    '                            strFilter &= " AND T.deptgroupunkid IN (" & strIDs & ") "

    '                        Case enAllocation.DEPARTMENT
    '                            strFilter &= " AND T.departmentunkid IN (" & strIDs & ") "

    '                        Case enAllocation.SECTION_GROUP
    '                            strFilter &= " AND T.sectiongroupunkid IN (" & strIDs & ") "

    '                        Case enAllocation.SECTION
    '                            strFilter &= " AND T.sectionunkid IN (" & strIDs & ") "

    '                        Case enAllocation.UNIT_GROUP
    '                            strFilter &= " AND T.unitgroupunkid IN (" & strIDs & ") "

    '                        Case enAllocation.UNIT
    '                            strFilter &= " AND T.unitunkid IN (" & strIDs & ") "

    '                        Case enAllocation.TEAM
    '                            strFilter &= " AND T.teamunkid IN (" & strIDs & ") "

    '                        Case enAllocation.JOB_GROUP
    '                            strFilter &= " AND J.jobgroupunkid IN (" & strIDs & ") "

    '                        Case enAllocation.JOBS
    '                            strFilter &= " AND J.jobunkid IN (" & strIDs & ") "

    '                        Case enAllocation.CLASS_GROUP
    '                            strFilter &= " AND T.classgroupunkid IN (" & strIDs & ") "

    '                        Case enAllocation.CLASSES
    '                            strFilter &= " AND T.classunkid IN (" & strIDs & ") "

    '                        Case enAllocation.COST_CENTER
    '                            strFilter &= " AND C.costcenterunkid IN (" & strIDs & ") "

    '                    End Select

    '                    Dim strInIDs As String = String.Join(",", dgvAllocEmp.Rows.Cast(Of GridViewRow).Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString).ToArray)
    '                    If strInIDs.Trim <> "" Then
    '                        strFilter &= " AND hremployee_master.employeeunkid IN (" & strInIDs & ") "
    '                    End If

    '                    If strFilter.Trim <> "" Then
    '                        strFilter = strFilter.Substring(4)
    '                    End If

    '                    Dim objEmp As New clsEmployee_Master
    '                    Dim dsList As DataSet = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                                    CInt(Session("UserId")), _
    '                                                    CInt(Session("Fin_year")), _
    '                                                    CInt(Session("CompanyUnkId")), _
    '                                                    dtpStartDate.GetDate.Date, _
    '                                                    dtpEndDate.GetDate.Date, _
    '                                                    CStr(Session("UserAccessModeSetting")), True, _
    '                                                    True, "Emp", False, , CInt(cboDepartmentList.SelectedValue), strFilterQuery:=strFilter)

    '                    Dim arrEmpIDs() As String = (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray
    '                    If arrEmpIDs.Length > 0 Then
    '                        Dim strUnkIDs As String = String.Join(",", dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) arrEmpIDs.Contains(dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString) = True).Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex).Item("depttrainingneedemployeetranunkid").ToString).ToArray)

    '                        lstAllocEmp = New List(Of clsDepttrainingneed_employee_Tran)
    '                        Dim objAllocEmp As New clsDepttrainingneed_employee_Tran
    '                        For Each sID As String In strUnkIDs.Split(CChar(","))
    '                            objAllocEmp = New clsDepttrainingneed_employee_Tran

    '                            With objAllocEmp
    '                                .pintDepttrainingneedemployeetranunkid = CInt(sID)

    '                                .pblnIsvoid = True
    '                                .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                                .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                                    .pintVoiduserunkid = CInt(Session("UserId"))
    '                                    .pintVoidloginemployeeunkid = 0
    '                                    .pintAuditUserId = CInt(Session("UserId"))
    '                                Else
    '                                    .pintVoiduserunkid = 0
    '                                    .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                                    .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                                End If
    '                                .pblnIsweb = True
    '                                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                                .pstrClientIp = Session("IP_ADD").ToString()
    '                                .pstrHostName = Session("HOST_NAME").ToString()
    '                                .pstrFormName = mstrModuleName
    '                            End With

    '                            lstAllocEmp.Add(objAllocEmp)
    '                        Next
    '                    End If


    '                End If

    '                If objTAllocation.VoidAll(lstTargetGroup, lstAllocEmp, Nothing) = True Then
    '                    Call FillTargetedGroup()
    '                    Call FillAllocEmployee()
    '                ElseIf objTAllocation._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTAllocation._Message, Me)
    '                    Exit Try
    '                End If
    '            End If
    '        End If


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTEmployee = Nothing
    '        objTAllocation = Nothing
    '    End Try
    'End Sub

    'Protected Sub popupDeleteAllocEmp_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteAllocEmp.buttonDelReasonYes_Click
    '    Dim objTEmployee As New clsDepttrainingneed_employee_Tran
    '    Try
    '        If IsValidData(False) = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 34, "Please tick atlease one item from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            If CInt(cboTargetedGroup.SelectedValue) > 0 Then 'Not Employee Names
    '                Dim lstAllocEmp As New List(Of clsDepttrainingneed_employee_Tran)
    '                Dim objAllocEmp As New clsDepttrainingneed_employee_Tran

    '                For Each dgRow As GridViewRow In gRow
    '                    objAllocEmp = New clsDepttrainingneed_employee_Tran

    '                    With objAllocEmp
    '                        .pintDepttrainingneedemployeetranunkid = CInt(dgvAllocEmp.DataKeys(dgRow.RowIndex).Item("depttrainingneedemployeetranunkid"))

    '                        .pblnIsvoid = True
    '                        .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrVoidreason = popupDeleteAllocEmp.Reason.ToString

    '                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                            .pintVoiduserunkid = CInt(Session("UserId"))
    '                            .pintVoidloginemployeeunkid = 0
    '                            .pintAuditUserId = CInt(Session("UserId"))
    '                        Else
    '                            .pintVoiduserunkid = 0
    '                            .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                            .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                        End If
    '                        .pblnIsweb = True
    '                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                        .pstrClientIp = Session("IP_ADD").ToString()
    '                        .pstrHostName = Session("HOST_NAME").ToString()
    '                        .pstrFormName = mstrModuleName
    '                    End With

    '                    lstAllocEmp.Add(objAllocEmp)

    '                Next

    '                If objTEmployee.VoidAll(lstAllocEmp, Nothing) = True Then
    '                    Call FillAllocEmployee()
    '                ElseIf objTEmployee._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTEmployee._Message, Me)
    '                    Exit Try
    '                End If

    '            End If
    '        End If


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTEmployee = Nothing
    '    End Try
    'End Sub

    'Protected Sub popupDeleteTResoources_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteTResoources.buttonDelReasonYes_Click
    '    Dim objTResources As New clsDepttrainingneed_resources_Tran
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvTrainingResources.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTResources"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Please tick atlease one training resource from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstTResources As New List(Of clsDepttrainingneed_resources_Tran)

    '            For Each dgRow As GridViewRow In gRow
    '                objTResources = New clsDepttrainingneed_resources_Tran

    '                With objTResources
    '                    .pintDepttrainingneedresourcestranunkid = CInt(dgvTrainingResources.DataKeys(dgRow.RowIndex).Item("depttrainingneedresourcestranunkid"))

    '                    .pblnIsvoid = True
    '                    .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintVoiduserunkid = CInt(Session("UserId"))
    '                        .pintVoidloginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintVoiduserunkid = 0
    '                        .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pblnIsweb = True
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName
    '                End With

    '                lstTResources.Add(objTResources)

    '            Next

    '            If objTResources.VoidAll(lstTResources, Nothing) = True Then
    '                Call FillTrainingResources()
    '            ElseIf objTResources._Message <> "" Then
    '                DisplayMessage.DisplayMessage(objTResources._Message, Me)
    '                Exit Try
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTResources = Nothing
    '    End Try
    'End Sub

    'Protected Sub popupDeleteFSource_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteFSource.buttonDelReasonYes_Click
    '    Dim objFSource As New clsDepttrainingneed_financingsources_Tran
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Please tick atlease one financing resource from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstFSource As New List(Of clsDepttrainingneed_financingsources_Tran)

    '            For Each dgRow As GridViewRow In gRow
    '                objFSource = New clsDepttrainingneed_financingsources_Tran

    '                With objFSource
    '                    .pintDepttrainingneedfinancingsourcestranunkid = CInt(dgvFinancingSource.DataKeys(dgRow.RowIndex).Item("depttrainingneedfinancingsourcestranunkid"))

    '                    .pblnIsvoid = True
    '                    .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintVoiduserunkid = CInt(Session("UserId"))
    '                        .pintVoidloginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintVoiduserunkid = 0
    '                        .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pblnIsweb = True
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName
    '                End With

    '                lstFSource.Add(objFSource)

    '            Next

    '            If objFSource.VoidAll(lstFSource, Nothing) = True Then
    '                Call FillFinancingSource()
    '            ElseIf objFSource._Message <> "" Then
    '                DisplayMessage.DisplayMessage(objFSource._Message, Me)
    '                Exit Try
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objFSource = Nothing
    '    End Try
    'End Sub

    'Protected Sub popupDeleteTCoordinator_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteTCoordinator.buttonDelReasonYes_Click
    '    Dim objTCoord As New clsDepttrainingneed_trainingcoordinator_Tran
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvTrainingCoordinator.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCoordinator"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 37, "Please tick atlease one training coordinator from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstTCoord As New List(Of clsDepttrainingneed_trainingcoordinator_Tran)

    '            For Each dgRow As GridViewRow In gRow
    '                objTCoord = New clsDepttrainingneed_trainingcoordinator_Tran

    '                With objTCoord
    '                    .pintDepttrainingneedtrainingcoordinatortranunkid = CInt(dgvTrainingCoordinator.DataKeys(dgRow.RowIndex).Item("depttrainingneedtrainingcoordinatortranunkid"))

    '                    .pblnIsvoid = True
    '                    .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintVoiduserunkid = CInt(Session("UserId"))
    '                        .pintVoidloginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintVoiduserunkid = 0
    '                        .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pblnIsweb = True
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName
    '                End With

    '                lstTCoord.Add(objTCoord)

    '            Next

    '            If objTCoord.VoidAll(lstTCoord, Nothing) = True Then
    '                Call FillTrainingCoordinator()
    '            ElseIf objTCoord._Message <> "" Then
    '                DisplayMessage.DisplayMessage(objTCoord._Message, Me)
    '                Exit Try
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTCoord = Nothing
    '    End Try
    'End Sub

    'Protected Sub popupDeleteTCostItem_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteTCostItem.buttonDelReasonYes_Click
    '    Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
    '    Try
    '        If IsValidData(True, False) = False Then Exit Try

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "Please tick atlease one cost item from the list."), Me)
    '            Exit Try
    '        End If

    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            Dim lstTCostItem As New List(Of clsDepttrainingneed_costitem_Tran)

    '            For Each dgRow As GridViewRow In gRow
    '                objTCostItem = New clsDepttrainingneed_costitem_Tran

    '                With objTCostItem
    '                    .pintDepttrainingneedcostitemtranunkid = CInt(dgvTrainingCostItem.DataKeys(dgRow.RowIndex).Item("depttrainingneedcostitemtranunkid"))

    '                    .pblnIsvoid = True
    '                    .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrVoidreason = popupDeleteEmployee.Reason.ToString

    '                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                        .pintVoiduserunkid = CInt(Session("UserId"))
    '                        .pintVoidloginemployeeunkid = 0
    '                        .pintAuditUserId = CInt(Session("UserId"))
    '                    Else
    '                        .pintVoiduserunkid = 0
    '                        .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
    '                        .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                    End If
    '                    .pblnIsweb = True
    '                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                    .pstrClientIp = Session("IP_ADD").ToString()
    '                    .pstrHostName = Session("HOST_NAME").ToString()
    '                    .pstrFormName = mstrModuleName
    '                End With

    '                lstTCostItem.Add(objTCostItem)

    '            Next

    '            If objTCostItem.VoidAll(lstTCostItem, Nothing) = True Then
    '                Call FillTrainingCostItem()
    '            ElseIf objTCostItem._Message <> "" Then
    '                DisplayMessage.DisplayMessage(objTCostItem._Message, Me)
    '                Exit Try
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTCostItem = Nothing
    '    End Try
    'End Sub

    Protected Sub popupDeleteDTNeed_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteDTNeed.buttonDelReasonYes_Click
        Dim objTMaster As New clsDepartmentaltrainingneed_master
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try

            With objTMaster
                ._Departmentaltrainingneedunkid = mintDelUnkId
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popupDeleteDTNeed.Reason.ToString

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                    ._Voidloginemployeeunkid = 0
                    ._AuditUserId = CInt(Session("UserId"))
                Else
                    ._Voiduserunkid = 0
                    ._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                    ._AuditUserId = CInt(Session("Employeeunkid"))
                End If
                ._Isweb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FormName = mstrModuleName

                objTPeriod._Calendarunkid = ._Periodunkid
            End With

            If objTMaster.Void(mintDelUnkId) = False Then

                If objTMaster._Message <> "" Then
                    DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                    Exit Try
                End If
            Else
                Call FillList("", "")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupInactiveDTNeed_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupInactiveDTNeed.buttonDelReasonYes_Click
        Dim objTMaster As New clsDepartmentaltrainingneed_master
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try

            With objTMaster
                ._Departmentaltrainingneedunkid = mintActiveInactiveUnkId
                ._Isactive = False

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    ._Userunkid = CInt(Session("UserId"))
                    ._Loginemployeeunkid = 0
                    ._AuditUserId = CInt(Session("UserId"))
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    ._AuditUserId = CInt(Session("Employeeunkid"))
                End If
                ._Isweb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FormName = mstrModuleName

                objTPeriod._Calendarunkid = ._Periodunkid
            End With

            If objTMaster.MakeActiveInactive(mintActiveInactiveUnkId, _
                                             False _
                                             ) = False Then

                If objTMaster._Message <> "" Then
                    DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                    Exit Try
                End If
            Else
                Call FillList("", "")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupInactiveDTNeed_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupInactiveDTNeed.buttonDelReasonNo_Click
        Try
            CType(gvDeptTrainingeedList.Rows(mintActiveInactiveRowIndex).FindControl("chkActive"), CheckBox).Checked = True
            popupInactiveDTNeed.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupActiveDTNeed_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupActiveDTNeed.buttonDelReasonYes_Click
        Dim objTMaster As New clsDepartmentaltrainingneed_master
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try

            With objTMaster
                ._Departmentaltrainingneedunkid = mintActiveInactiveUnkId
                ._Isactive = True

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    ._Userunkid = CInt(Session("UserId"))
                    ._Loginemployeeunkid = 0
                    ._AuditUserId = CInt(Session("UserId"))
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    ._AuditUserId = CInt(Session("Employeeunkid"))
                End If
                ._Isweb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FormName = mstrModuleName

                objTPeriod._Calendarunkid = ._Periodunkid
            End With

            If objTMaster.MakeActiveInactive(mintActiveInactiveUnkId, _
                                             True _
                                             ) = False Then

                If objTMaster._Message <> "" Then
                    DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                    Exit Try
                End If
            Else
                Call FillList("", "")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupActiveDTNeed_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupActiveDTNeed.buttonDelReasonNo_Click
        Try
            CType(gvDeptTrainingeedList.Rows(mintActiveInactiveRowIndex).FindControl("chkActive"), CheckBox).Checked = False
            popupActiveDTNeed.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Protected Sub btnSubmitForApprovalFromDeptTNeed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApprovalFromDeptTNeed.Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True AndAlso (IsDBNull(gvDeptTrainingeedList.DataKeys(x.RowIndex).Item("startdate")) = True OrElse IsDBNull(gvDeptTrainingeedList.DataKeys(x.RowIndex).Item("enddate")) = True))

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 39, "Please set the start date and end date for all the selected trainings."), Me)
                Exit Sub
            End If

            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            'Sohail (26 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            Dim lstDistDeptUnkIDs As List(Of String) = Nothing
            Dim lstDistPeriodUnkIDs As List(Of String) = Nothing
            'Sohail (26 Apr 2021) -- End

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))

                'Sohail (26 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                lstDistDeptUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentunkid").ToString())).ToList().Distinct.ToList
                lstDistPeriodUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("periodunkid").ToString())).ToList().Distinct.ToList
                'Sohail (26 Apr 2021) -- End
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try
                If IsValidForMaxBudget(strDeptTrainingeedIds, lstDistDeptUnkIDs, lstDistPeriodUnkIDs) = False Then Exit Try 'Sohail (26 Apr 2021)

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")
                    mintEmailTypeId = clsDepartmentaltrainingneed_master.enEmailType.Submit_For_Approval_From_Departmental_Training_Need
                    SendEmail(strDeptTrainingeedIds)

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 41, "Selected training(s) Submitted for Approval Successfully!"), Me)
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub btnTentativeApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTentativeApprove.Click
    '    Try
    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 16, "Training is compulsory information.Please Check atleast One Training."), Me)
    '            Exit Sub
    '        End If

    '        Dim lstIDs As List(Of String) = Nothing
    '        Dim strDeptTrainingeedIds As String = String.Empty
    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

    '            lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
    '            strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
    '        End If

    '        If strDeptTrainingeedIds.Trim.Length > 0 Then
    '            If IsValidForApproval(lstIDs) = False Then Exit Try

    '            Dim objTMaster As New clsDepartmentaltrainingneed_master

    '            With objTMaster

    '                .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.TentativeApproved

    '                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                    .pintUserunkid = CInt(Session("UserId"))
    '                    .pintLoginemployeeunkid = 0
    '                    .pintAuditUserId = CInt(Session("UserId"))
    '                Else
    '                    .pintUserunkid = 0
    '                    .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
    '                    .pintAuditUserId = CInt(Session("Employeeunkid"))
    '                End If
    '                .pblnIsweb = True
    '                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
    '                .pstrClientIp = Session("IP_ADD").ToString()
    '                .pstrHostName = Session("HOST_NAME").ToString()
    '                .pstrFormName = mstrModuleName

    '            End With

    '            If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
    '                                        clsDepartmentaltrainingneed_master.enApprovalStatus.TentativeApproved, _
    '                                        ) = False Then

    '                If objTMaster._Message <> "" Then
    '                    DisplayMessage.DisplayMessage(objTMaster._Message, Me)
    '                    Exit Try
    '                End If
    '            Else
    '                Call FillList("", "")
    '            End If


    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub btnSubmitForApprovalFromTBacklog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApprovalFromTBacklog.Click
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try

            objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)

            If eZeeDate.convertDate(DateAndTime.Today.Date) > eZeeDate.convertDate(objTPeriod._EndDate) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 42, "Sorry, you cannot submit the selected training(s) for budget approval. Submission deadline was #calendarenddate#.").Replace("#calendarenddate#", Format(objTPeriod._EndDate, "dd-MMM-yyyy")), Me)
                Exit Try
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Try
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            Dim lstDistDeptUnkIDs As List(Of String) = Nothing
            Dim lstDistPeriodUnkIDs As List(Of String) = Nothing

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))

                lstDistDeptUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentunkid").ToString())).ToList().Distinct.ToList
                lstDistPeriodUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("periodunkid").ToString())).ToList().Distinct.ToList
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try
                If IsValidForMaxBudget(strDeptTrainingeedIds, lstDistDeptUnkIDs, lstDistPeriodUnkIDs) = False Then Exit Try

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                Dim intRefNo As Integer = objTMaster.getNextRefNo()

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog
                    .pstrRefno = intRefNo.ToString

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")
                    mintEmailTypeId = clsDepartmentaltrainingneed_master.enEmailType.Submit_For_Approval_From_Training_Backlog
                    SendEmail(strDeptTrainingeedIds)

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 44, "Selected training(s) Submitted for Budget Approval Successfully!"), Me)
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
        End Try
    End Sub

    Protected Sub btnFinalApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalApprove.Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            Dim lstDistDeptUnkIDs As List(Of String) = Nothing
            Dim lstDistPeriodUnkIDs As List(Of String) = Nothing

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))

                lstDistDeptUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentunkid").ToString())).ToList().Distinct.ToList
                lstDistPeriodUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("periodunkid").ToString())).ToList().Distinct.ToList
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try
                If IsValidForMaxBudget(strDeptTrainingeedIds, lstDistDeptUnkIDs, lstDistPeriodUnkIDs) = False Then Exit Try

                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 430 : Notify employees who are targeted in departmental training needs plans when such plans are final approved.
                'Dim objTMaster As New clsDepartmentaltrainingneed_master

                'With objTMaster

                '    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved

                '    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                '        .pintUserunkid = CInt(Session("UserId"))
                '        .pintLoginemployeeunkid = 0
                '        .pintAuditUserId = CInt(Session("UserId"))
                '    Else
                '        .pintUserunkid = 0
                '        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                '        .pintAuditUserId = CInt(Session("Employeeunkid"))
                '    End If
                '    .pblnIsweb = True
                '    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                '    .pstrClientIp = Session("IP_ADD").ToString()
                '    .pstrHostName = Session("HOST_NAME").ToString()
                '    .pstrFormName = mstrModuleName

                'End With

                'If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                '                            clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved, _
                '                            ) = False Then

                '    If objTMaster._Message <> "" Then
                '        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                '        Exit Try
                '    End If
                'Else
                '    Call FillList("", "")

                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 46, "Selected training(s) Approved Successfully!"), Me)
                'End If
                popupFinalApprove.Show()
                'Sohail (04 Aug 2021) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 430 : Notify employees who are targeted in departmental training needs plans when such plans are final approved.
    Protected Sub popupFinalApprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalApprove.buttonDelReasonYes_Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            Dim lstDistDeptUnkIDs As List(Of String) = Nothing
            Dim lstDistPeriodUnkIDs As List(Of String) = Nothing

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))

                lstDistDeptUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentunkid").ToString())).ToList().Distinct.ToList
                lstDistPeriodUnkIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("periodunkid").ToString())).ToList().Distinct.ToList
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try
                If IsValidForMaxBudget(strDeptTrainingeedIds, lstDistDeptUnkIDs, lstDistPeriodUnkIDs) = False Then Exit Try

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved
                    .pstrFinalapproval_remark = popupFinalApprove.Reason.ToString

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")

                    objTMaster.SendEmails(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "List", clsDepartmentaltrainingneed_master.enEmailType.Final_Approved, strDeptTrainingeedIds, mstrModuleName, _
                                                enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), _
                                            CStr(Session("ArutiSelfServiceURL")), mstrTrainingNeedAllocationName, mintTrainingNeedAllocationID)

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 46, "Selected training(s) Approved Successfully!"), Me)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Aug 2021) -- End

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try

                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 431 : Add Remark field for Budget Approval/Rejection and notify initiator of the departmental plan.
                'Dim objTMaster As New clsDepartmentaltrainingneed_master

                'With objTMaster

                '    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected

                '    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                '        .pintUserunkid = CInt(Session("UserId"))
                '        .pintLoginemployeeunkid = 0
                '        .pintAuditUserId = CInt(Session("UserId"))
                '    Else
                '        .pintUserunkid = 0
                '        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                '        .pintAuditUserId = CInt(Session("Employeeunkid"))
                '    End If
                '    .pblnIsweb = True
                '    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                '    .pstrClientIp = Session("IP_ADD").ToString()
                '    .pstrHostName = Session("HOST_NAME").ToString()
                '    .pstrFormName = mstrModuleName

                'End With

                'If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                '                            clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected, _
                '                            ) = False Then

                '    If objTMaster._Message <> "" Then
                '        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                '        Exit Try
                '    End If
                'Else
                '    Call FillList("", "")
                'End If
                popupFinalReject.Show()
                'Sohail (04 Aug 2021) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
    Protected Sub popupFinalReject_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalReject.buttonDelReasonYes_Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected
                    .pstrFinalapproval_remark = popupFinalReject.Reason.ToString

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")

                    objTMaster.SendEmails(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "List", clsDepartmentaltrainingneed_master.enEmailType.Final_Rejected, strDeptTrainingeedIds, mstrModuleName, _
                                                enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), _
                                            CStr(Session("ArutiSelfServiceURL")), mstrTrainingNeedAllocationName, mintTrainingNeedAllocationID)

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 46, "Selected training(s) Approved Successfully!"), Me)
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Aug 2021) -- End

    Protected Sub btnUnlockSubmitApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlockSubmitApproval.Click
        Try
            mblnIsUnlockSubmitClicked = True
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try

                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 428 : Email alert when plans are unlocked from the training backlog.
                'Dim intStatusId As Integer = 0

                'Dim objTMaster As New clsDepartmentaltrainingneed_master

                'With objTMaster

                '    If mintFormId = 2 Then
                '        intStatusId = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending
                '    ElseIf mintFormId = 3 Then
                '        intStatusId = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed
                '    Else
                '        intStatusId = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending
                '    End If

                '    .pintStatusunkid = intStatusId

                '    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                '        .pintUserunkid = CInt(Session("UserId"))
                '        .pintLoginemployeeunkid = 0
                '        .pintAuditUserId = CInt(Session("UserId"))
                '    Else
                '        .pintUserunkid = 0
                '        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                '        .pintAuditUserId = CInt(Session("Employeeunkid"))
                '    End If
                '    .pblnIsweb = True
                '    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                '    .pstrClientIp = Session("IP_ADD").ToString()
                '    .pstrHostName = Session("HOST_NAME").ToString()
                '    .pstrFormName = mstrModuleName

                'End With


                'If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                '                            intStatusId, _
                '                            ) = False Then

                '    If objTMaster._Message <> "" Then
                '        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                '        Exit Try
                '    End If
                'Else
                '    Call FillList("", "")

                '    If mintFormId = 3 Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 49, "Selected training(s) which had been submitted for budget approval have been Unlocked Successfully to allow for changes!"), Me)
                '    Else
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 50, "Selected training(s) which had been submitted for backlog approval have been Unlocked Successfully!"), Me)
                '    End If
                'End If
                'Sohail (04 Aug 2021) -- End

            End If
            popupUnlockSubmitted.Show() 'Sohail (04 Aug 2021)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            mblnIsUnlockSubmitClicked = False
        End Try
    End Sub
    'Hemant (26 Mar 2021) -- End

    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 428 : Email alert when plans are unlocked from the training backlog.
    Protected Sub popupUnlockSubmitted_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupUnlockSubmitted.buttonDelReasonYes_Click
        Try
            mblnIsUnlockSubmitClicked = True
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try

                Dim intStatusId As Integer = 0

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    If mintFormId = 2 Then
                        intStatusId = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending
                    ElseIf mintFormId = 3 Then
                        intStatusId = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed
                    Else
                        intStatusId = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending
                    End If

                    .pintStatusunkid = intStatusId
                    .pstrUnlock_remark = popupUnlockSubmitted.Reason.ToString

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With


                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            intStatusId, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")

                    objTMaster.SendEmails(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "List", clsDepartmentaltrainingneed_master.enEmailType.Unlock_Submitted_For_Approval, strDeptTrainingeedIds, mstrModuleName, _
                                                enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), _
                                            CStr(Session("ArutiSelfServiceURL")), mstrTrainingNeedAllocationName, mintTrainingNeedAllocationID)

                    If mintFormId = 3 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 49, "Selected training(s) which had been submitted for budget approval have been Unlocked Successfully to allow for changes!"), Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 50, "Selected training(s) which had been submitted for backlog approval have been Unlocked Successfully!"), Me)
                    End If
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            mblnIsUnlockSubmitClicked = False
        End Try
    End Sub
    'Sohail (04 Aug 2021) -- End

    Protected Sub btnAskForReview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAskForReview.Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.AskedForReviewAsPerAmountSet

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.AskedForReviewAsPerAmountSet, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 52, "Selected training(s) which had been submitted for budget approval have been sent back for amount review Successfully!"), Me)
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUndoApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUndoApproved.Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try
                Dim objTRequest As New clstraining_request_master
                If objTRequest.IsTrainingRequestedByEmployee(strDeptTrainingeedIds) = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 54, "Sorry, you cannot change the status of the approved training(s). Training Requests are already submitted for the selected training(s)."), Me)
                    Exit Sub
                End If

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 55, "Status for the selected training(s) has been changed from Approved to Pending!"), Me)
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUndoRejected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUndoRejected.Click
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvDeptTrainingeedList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 40, "Please tick at least one training to continue."), Me)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = Nothing
            Dim strDeptTrainingeedIds As String = String.Empty
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                lstIDs = (From p In gRow Select (gvDeptTrainingeedList.DataKeys(p.DataItemIndex)("departmentaltrainingneedunkid").ToString())).ToList().Distinct.ToList
                strDeptTrainingeedIds = String.Join(",", CType(lstIDs.ToArray(), String()))
            End If

            If strDeptTrainingeedIds.Trim.Length > 0 Then
                If IsValidForApproval(lstIDs) = False Then Exit Try

                Dim objTMaster As New clsDepartmentaltrainingneed_master

                With objTMaster

                    .pintStatusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = CInt(Session("Employeeunkid"))
                    End If
                    .pblnIsweb = True
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                If objTMaster.UpdateStatus(strDeptTrainingeedIds, _
                                            clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog, _
                                            ) = False Then

                    If objTMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTMaster._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList("", "")

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 57, "Status for the selected training(s) has been changed from Rejected to Pending!"), Me)
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (09 Jun 2021) -- Start
    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
    Protected Sub btnAddInstructor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddInstructor.Click
        Try
            If IsValidInstructor(0) = False Then Exit Try

            If Save_Click(True) = True Then
                Call ResetInstructor()
                Call FillTrainingInstructor()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUpdateInstructor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateInstructor.Click
        Dim objInstruct As New clsDepttrainingneed_traininginstructor_Tran
        Try
            If IsValidInstructor(mintInstructSrNo) = False Then Exit Try

            If CInt(dgvTrainingInstructor.DataKeys(mintInstructSrNo).Item("depttrainingneedtraininginstructortranunkid")) > 0 Then
                objInstruct._Depttrainingneedtraininginstructortranunkid = CInt(dgvTrainingInstructor.DataKeys(mintInstructSrNo).Item("depttrainingneedtraininginstructortranunkid"))

                objInstruct._Others_name = txtOthersName.Text
                objInstruct._Others_company = txtOthersCompany.Text
                objInstruct._Others_department = txtOthersDept.Text
                objInstruct._Others_job = txtOthersJob.Text
                objInstruct._Others_email = txtOthersEmail.Text

                objInstruct._Isweb = True
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objInstruct._Userunkid = CInt(Session("UserId"))
                    objInstruct._Loginemployeeunkid = 0
                    objInstruct._AuditUserId = CInt(Session("UserId"))
                Else
                    objInstruct._Userunkid = 0
                    objInstruct._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    objInstruct._AuditUserId = CInt(Session("Employeeunkid"))
                End If
                objInstruct._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                objInstruct._ClientIP = Session("IP_ADD").ToString()
                objInstruct._HostName = Session("HOST_NAME").ToString()
                objInstruct._FormName = mstrModuleName

                If objInstruct.Update(False, Nothing) = True Then
                    Call FillTrainingInstructor()
                    Call ResetInstructor()
                ElseIf objInstruct._Message <> "" Then
                    DisplayMessage.DisplayMessage(objInstruct._Message, Me)
                    Exit Try
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objInstruct = Nothing
        End Try
    End Sub

    Protected Sub btnResetInstructor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetInstructor.Click
        Try
            Call ResetInstructor()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDeleteInstruct_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteInstruct.buttonDelReasonYes_Click
        Dim objInstruct As New clsDepttrainingneed_traininginstructor_Tran
        Try

            objInstruct._Depttrainingneedtraininginstructortranunkid = CInt(dgvTrainingInstructor.DataKeys(mintInstructSrNo).Item("depttrainingneedtraininginstructortranunkid"))
            objInstruct._Isvoid = True
            objInstruct._Voiduserunkid = CInt(Session("UserId"))
            objInstruct._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objInstruct._Voidreason = popupDeleteInstruct.Reason.ToString

            objInstruct._Isweb = True
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objInstruct._Userunkid = CInt(Session("UserId"))
                objInstruct._Loginemployeeunkid = 0
                objInstruct._AuditUserId = CInt(Session("UserId"))
            Else
                objInstruct._Userunkid = 0
                objInstruct._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objInstruct._AuditUserId = CInt(Session("Employeeunkid"))
            End If
            objInstruct._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objInstruct._ClientIP = Session("IP_ADD").ToString()
            objInstruct._HostName = Session("HOST_NAME").ToString()
            objInstruct._FormName = mstrModuleName

            If objInstruct.Void(CInt(dgvTrainingInstructor.DataKeys(mintInstructSrNo).Item("depttrainingneedtraininginstructortranunkid")), Nothing) = True Then
                Call FillTrainingInstructor()
                Call ResetInstructor()
            ElseIf objInstruct._Message <> "" Then
                DisplayMessage.DisplayMessage(objInstruct._Message, Me)
                Exit Try
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Jun 2021) -- End

#End Region

#Region " GridView Event "

    Protected Sub gvDeptTrainingeedList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDeptTrainingeedList.RowCommand
        Try
            If e.CommandName = "Change" Then
                If CBool(Session("AllowToEditDepartmentalTrainingNeed")) = False Then Exit Try

                Dim SrNo As Integer = CInt(e.CommandArgument)

                mintDepartmentaltrainingneedunkid = CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("departmentaltrainingneedunkid"))

                If CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 58, "Sorry, This Departmental Training Need is already rejected."), Me)
                    Exit Try
                End If

                If CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 59, "Sorry, This Departmental Training Need is already approved at the final level."), Me)
                    Exit Try
                End If

                If mintFormId = 1 Then
                    If CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 36, "Sorry, This Departmental Training Need is already Submitted for Approval."), Me)
                        Exit Try
                    End If
                ElseIf mintFormId = 2 Then
                    If CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 61, "Sorry, This Departmental Training Need is still in pending status."), Me)
                        Exit Try
                    End If
                ElseIf mintFormId = 3 Then
                    If CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("insertformid")) <> 3 Then
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 113, "Sorry, You cannot edit training added by department or backlof user."), Me)
                        'Exit Try
                        pnlTrainingInfo.Enabled = False
                        pnlTargetGroup.Enabled = False
                        pnlTrainingResources.Enabled = False
                        pnlFinancingSources.Enabled = False
                        pnlTrainingCoordinator.Enabled = False
                        'Sohail (09 Jun 2021) -- Start
                        'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
                        pnlTrainingInstructor.Enabled = False
                        'Sohail (09 Jun 2021) -- End
                    End If

                Else
                    If CInt(gvDeptTrainingeedList.DataKeys(SrNo).Item("statusunkid")) <= clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 61, "Sorry, This Departmental Training Need is still in pending status."), Me)
                        Exit Try
                    End If
                End If

                'Sohail (09 Jun 2021) -- Start
                'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
                Call ResetInstructor()
                'Sohail (09 Jun 2021) -- End


                Dim objTMaster As New clsDepartmentaltrainingneed_master
                objTMaster._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                FillTrainingCategoryCombo(objTMaster._ModuleId)
                'Hemant (15 Apr 2021) -- End
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
                FillTrainingVenueCombo(objTMaster._Trainingvenueunkid)
                'Hemant (03 Jun 2021) -- End
                With objTMaster
                    cboDepartmentList.SelectedValue = ._Departmentunkid.ToString
                    cboPeriod.SelectedValue = ._Periodunkid.ToString
                    If ._Competenceunkid > 0 Then
                        cboCompetences.SelectedValue = ._Competenceunkid.ToString
                        Call cboCompetences_SelectedIndexChanged(cboCompetences, New System.EventArgs)
                        txtOtherCompetence.Text = ""
                        chkOtherCompetences.Checked = False
                        Call chkOtherCompetences_CheckedChanged(chkOtherCompetences, New System.EventArgs)
                        'Gajanan [15-Apr-2021] -- Start
                    ElseIf ._ModuleId = CInt(enModuleReference.PDP) AndAlso ._Other_competence.Length <= 0 AndAlso ._Competenceunkid <= 0 Then

                        Dim objPdpgoals_master As New clsPdpgoals_master
                        Dim objpdpform_tran As New clspdpform_tran
                        objPdpgoals_master._Pdpgoalsmstunkid = ._ModuleTranUnkId
                        objpdpform_tran._Itemtranunkid = objPdpgoals_master._Itemunkid
                        cboCompetences.SelectedValue = "0"
                        Call cboCompetences_SelectedIndexChanged(cboCompetences, New System.EventArgs)
                        txtOtherCompetence.Text = objpdpform_tran._Fieldvalue

                        chkOtherCompetences.Checked = True
                        Call chkOtherCompetences_CheckedChanged(chkOtherCompetences, New System.EventArgs)
                        objPdpgoals_master = Nothing
                        objpdpform_tran = Nothing
                        'Gajanan [15-Apr-2021] -- End
                    Else
                        cboCompetences.SelectedValue = "0"
                        Call cboCompetences_SelectedIndexChanged(cboCompetences, New System.EventArgs)
                        txtOtherCompetence.Text = ._Other_competence
                        chkOtherCompetences.Checked = True
                        Call chkOtherCompetences_CheckedChanged(chkOtherCompetences, New System.EventArgs)

                    End If
                    If ._Trainingcourseunkid > 0 Then
                        cboTrainingNeeded.SelectedValue = ._Trainingcourseunkid.ToString
                        txtOtherTCourse.Text = ""
                        chkOtherTrainingCource.Checked = False
                        Call chkOtherTrainingCource_CheckedChanged(chkOtherTrainingCource, New System.EventArgs)
                    Else
                        cboTrainingNeeded.SelectedValue = "0"
                        txtOtherTCourse.Text = ._Other_trainingcourse
                        chkOtherTrainingCource.Checked = True
                        Call chkOtherTrainingCource_CheckedChanged(chkOtherTrainingCource, New System.EventArgs)
                    End If
                    cboTrainingCategory.SelectedValue = ._Trainingcategoryunkid.ToString
                    cboLearningMethod.SelectedValue = ._Learningmethodunkid.ToString
                    cboTargetedGroup.SelectedValue = ._Targetedgroupunkid.ToString
                    Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)
                    txtNoOfStaff.Decimal_ = ._Noofstaff
                    dtpStartDate.SetDate = ._Startdate
                    dtpEndDate.SetDate = ._Enddate
                    cboPriority.SelectedValue = ._Trainingpriority.ToString
                    cboTrainingProvider.SelectedValue = ._Trainingproviderunkid.ToString
                    cboTrainingVenue.SelectedValue = ._Trainingvenueunkid.ToString

                    'Gajanan [23-APR-2021] -- Start
                    'txtOtherCompetence.Text = ._Other_competence
                    'Gajanan [23-APR-2021] -- End

                    txtOtherTCourse.Text = ._Other_trainingcourse
                    chkOtherCompetences.Checked = CInt(cboCompetences.SelectedValue) <= 0
                    chkOtherTrainingCource.Checked = CInt(cboTrainingNeeded.SelectedValue) <= 0
                    txtCommentRemark.Text = ._Remark
                    chkCertiRequired.Checked = ._Iscertirequired
                    chkTrainingCostOptional.Checked = ._IsTrainingCostOptional
                    objlblCostItemTotal.Text = Format(._Totalcost, CStr(Session("fmtCurrency")))
                    objlblCostItemApprovedTotal.Text = Format(._Approved_Totalcost, CStr(Session("fmtCurrency")))
                    'Sohail (26 Apr 2021) -- Start
                    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    hfobjlblTotalAmt.Value = Format(._Totalcost, CStr(Session("fmtCurrency")))
                    hfobjlblTotalApprovedAmt.Value = Format(._Approved_Totalcost, CStr(Session("fmtCurrency")))
                    'Sohail (26 Apr 2021) -- End

                    'Gajanan [15-Apr-2021] -- Start
                    If ._ModuleTranUnkId > 0 And ._ModuleId > 0 Then
                        cboTargetedGroup.Enabled = False
                        txtNoOfStaff.Enabled = False
                        dgvEmployee.Enabled = False
                        cboTrainingCategory.Enabled = False
                        cboPriority.Enabled = False

                        cboPeriod.Enabled = False
                        chkOtherCompetences.Enabled = False
                        cboCompetences.Enabled = False
                        txtOtherCompetence.Enabled = False
                        cboTrainingNeeded.Enabled = False
                        'Sohail (26 Apr 2021) -- Start
                        'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    Else
                        'Hemant (10 May 2021) -- Start
                        'cboTargetedGroup.Enabled = True
                        cboTargetedGroup.Enabled = False
                        'Hemant (10 May 2021) -- End
                        txtNoOfStaff.Enabled = True
                        dgvEmployee.Enabled = True
                        cboTrainingCategory.Enabled = True
                        cboPriority.Enabled = True

                        cboPeriod.Enabled = False
                        chkOtherCompetences.Enabled = True
                        cboCompetences.Enabled = True
                        txtOtherCompetence.Enabled = True
                        cboTrainingNeeded.Enabled = True
                        'Sohail (26 Apr 2021) -- End
                    End If
                    'Gajanan [15-Apr-2021] -- End
                End With

                Call FillTargetedGroup()
                'Call FillAllocEmployee()
                Call FillTrainingResources()
                Call FillFinancingSource()
                Call FillTrainingCoordinator()
                Call FillTrainingCostItem()
                'Sohail (09 Jun 2021) -- Start
                'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
                Call FillInstructor()
                Call FillTrainingInstructor()
                'Sohail (09 Jun 2021) -- End

                TabName.Value = "TrainingInfo"
                mblnShowAddEditPopup = True
                popAddEdit.Show()

            ElseIf e.CommandName = "Remove" Then
                If CBool(Session("AllowToDeleteDepartmentalTrainingNeed")) = False Then Exit Try

                If CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("insertformid")) = 1 AndAlso CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 36, "Sorry, This Departmental Training Need is already Submitted for Approval."), Me)
                    Exit Try
                End If

                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                If CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("insertformid")) = 2 AndAlso CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 36, "Sorry, This Departmental Training Need is already Submitted for Approval."), Me)
                    Exit Try
                End If

                If CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("insertformid")) = 3 AndAlso CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("statusunkid")) <> clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 36, "Sorry, This Departmental Training Need is already Submitted for Approval."), Me)
                    Exit Try
                End If
                'Hemant (15 Apr 2021) -- End

                mintDelUnkId = CInt(gvDeptTrainingeedList.DataKeys(CInt(e.CommandArgument)).Item("departmentaltrainingneedunkid"))

                popupDeleteDTNeed.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvDeptTrainingeedList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDeptTrainingeedList.RowDataBound
        Try
            'If e.Row.RowType = DataControlRowType.Header Then
            '    If gvDeptTrainingeedList.SortExpression <> String.Empty Then
            '        Dim strUp As String = " <i class='fas fa-caret-up'></i>"
            '        Dim strDown As String = " <i class='fas fa-caret-down'></i>"

            '        Dim icol = CObj(Nothing)
            '        icol = gvDeptTrainingeedList.Columns.Cast(Of DataControlField)().Where(Function(x) x.SortExpression = gvDeptTrainingeedList.SortExpression).[Select](Function(x) x).FirstOrDefault()
            '        Dim idx As Integer = gvDeptTrainingeedList.Columns.IndexOf(CType(icol, DataControlField))

            '        'Dim lnk As LinkButton = CType(e.Row.Cells(idx).Controls(1), LinkButton)
            '        'If sortOrder.Trim.ToLower = "desc" Then
            '        '    lnk.Text += strDown
            '        'Else
            '        '    lnk.Text += strUp
            '        'End If
            '        Dim lnk As TableCell = CType(e.Row.Cells(idx), TableCell)
            '        If sortOrder.Trim.ToLower = "desc" Then
            '            lnk.Text += strDown
            '        Else
            '            lnk.Text += strUp
            '        End If
            '    End If
            'End If
            If e.Row.RowType = DataControlRowType.Header Then
                If CInt(cboStatusList.SelectedValue) >= 0 Then
                    e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhactive")).Style.Add("display", "none")
                End If
            End If
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(cboStatusList.SelectedValue) >= 0 Then
                    CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhactive")).FindControl("chkActive"), CheckBox).Text = ""
                    e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhactive")).Style.Add("display", "none")
                End If
                If CType(e.Row.Cells(4).FindControl("lbltrainingcourse"), Label).Text = "" AndAlso CType(e.Row.Cells(5).FindControl("lbltrainingcompetence"), Label).Text = "" Then Exit Try 'No Records Found

                'Gajanan [15-Apr-2021] -- Start
                If CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhstartdate")).FindControl("lblstartdate"), Label).Text.Length > 0 Then
                CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhstartdate")).FindControl("lblstartdate"), Label).Text = CDate(eZeeDate.convertDate(CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhstartdate")).FindControl("lblstartdate"), Label).Text)).ToString("dd-MMM-yyyy")
                End If

                If CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhenddate")).FindControl("lblenddate"), Label).Text.Length > 0 Then
                CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhenddate")).FindControl("lblenddate"), Label).Text = CDate(eZeeDate.convertDate(CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhenddate")).FindControl("lblenddate"), Label).Text)).ToString("dd-MMM-yyyy")
                End If



                Dim lnkdelete As LinkButton = CType(e.Row.FindControl("lnkdelete"), LinkButton)
                If CInt(gvDeptTrainingeedList.DataKeys(e.Row.RowIndex)("moduletranunkid").ToString()) > 0 Then
                    lnkdelete.Visible = False
                Else
                    lnkdelete.Visible = True
                End If
                'Gajanan [15-Apr-2021] -- End

                Dim decAmt As Decimal = 0
                Decimal.TryParse(CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhamount")).FindControl("lblamount"), Label).Text, decAmt)
                If decAmt = 0 Then
                    CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhamount")).FindControl("lblamount"), Label).Text = ""
                Else
                    CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhamount")).FindControl("lblamount"), Label).Text = Format(decAmt, CStr(Session("fmtCurrency")))
                End If
                CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotaltrainingcost")).FindControl("lbltotaltrainingcost"), Label).Text = Format(CDec(CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotaltrainingcost")).FindControl("lbltotaltrainingcost"), Label).Text), CStr(Session("fmtCurrency")))
                If CInt(gvDeptTrainingeedList.DataKeys(e.Row.RowIndex).Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved Then
                    CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).FindControl("lbltotalapprovedtrainingcost"), Label).Text = Format(CDec(CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).FindControl("lbltotalapprovedtrainingcost"), Label).Text), CStr(Session("fmtCurrency")))
                ElseIf mintFormId = 3 AndAlso CInt(gvDeptTrainingeedList.DataKeys(e.Row.RowIndex).Item("statusunkid")) = clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog Then
                    CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).FindControl("lbltotalapprovedtrainingcost"), Label).Text = Format(CDec(CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).FindControl("lbltotalapprovedtrainingcost"), Label).Text), CStr(Session("fmtCurrency")))
                Else
                    CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotalapprovedtrainingcost")).FindControl("lbltotalapprovedtrainingcost"), Label).Text = ""
                End If

                If e.Row.RowIndex > 0 Then
                    If CInt(gvDeptTrainingeedList.DataKeys(e.Row.RowIndex).Item("departmentaltrainingneedunkid")) = CInt(gvDeptTrainingeedList.DataKeys(e.Row.RowIndex - 1).Item("departmentaltrainingneedunkid")) Then
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingcoursename")).FindControl("lbltrainingcourse"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhcompetencename")).FindControl("lbltrainingcompetence"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingcategoryname")).FindControl("lbltrainingcategory"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhlearningmethodname")).FindControl("lbllearningmethod"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhnoofstaff")).FindControl("lblnoofstaff"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhstartdate")).FindControl("lblstartdate"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhenddate")).FindControl("lblenddate"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingpriorityname")).FindControl("lblpriority"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingprovidername")).FindControl("lbltrainingprovider"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtrainingvenuename")).FindControl("lbltrainingvenue"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhiscertirequired")).FindControl("lbltrainingcertirequired"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhremark")).FindControl("lblcommentremark"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhtotaltrainingcost")).FindControl("lbltotaltrainingcost"), Label).Text = ""
                        CType(e.Row.Cells(getColumnID_Griview(gvDeptTrainingeedList, "colhstatus")).FindControl("lblstatusname"), Label).Text = ""
                    Else
                        If rdbDetailedList.Checked = True Then e.Row.BackColor = Color.Cyan
                    End If
                Else
                    If rdbDetailedList.Checked = True Then e.Row.BackColor = Color.Cyan
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvDeptTrainingeedList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvDeptTrainingeedList.Sorting
        Try
            FillList(e.SortExpression, sortOrder)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvEmployee_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvEmployee.RowCommand
        Try
            If e.CommandName = "RemoveAll" Then
                'If IsValidData(False) = False Then Exit Try

                'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

                'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Please tick atlease one item from the list."), Me)
                '    Exit Try
                'End If

                'popupDeleteEmployee.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvAllocEmp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvAllocEmp.RowCommand
        Try
            If e.CommandName = "RemoveAll" Then
                'If IsValidData(False) = False Then Exit Try

                'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

                'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Please tick atlease one item from the list."), Me)
                '    Exit Try
                'End If

                'popupDeleteAllocEmp.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingResources_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvTrainingResources.RowCommand
        Try
            If e.CommandName = "RemoveAll" Then
                'If IsValidData() = False Then Exit Try

                'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvTrainingResources.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTResources"), CheckBox).Checked = True)

                'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 41, "Please tick atlease one training resource from the list."), Me)
                '    Exit Try
                'End If

                'popupDeleteTResoources.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvFinancingSource_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvFinancingSource.RowCommand
        Try
            If e.CommandName = "RemoveAll" Then
                'If IsValidData() = False Then Exit Try

                'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True)

                'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 42, "Please tick atlease one financing source from the list."), Me)
                '    Exit Try
                'End If

                'popupDeleteFSource.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingCoordinator_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvTrainingCoordinator.RowCommand
        Try
            If e.CommandName = "RemoveAll" Then
                'If IsValidData() = False Then Exit Try

                'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvTrainingCoordinator.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCoordinator"), CheckBox).Checked = True)

                'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 43, "Please tick atlease one training coordinator from the list."), Me)
                '    Exit Try
                'End If

                'popupDeleteTCoordinator.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingCostItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvTrainingCostItem.RowCommand
        Try
            If e.CommandName = "RemoveAll" Then
                'If IsValidData(True, False) = False Then Exit Try

                'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                'gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTCostItem"), CheckBox).Checked = True)

                'If gRow Is Nothing OrElse gRow.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Please tick atlease one cost item from the list."), Me)
                '    Exit Try
                'End If

                'popupDeleteTCostItem.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingCostItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvTrainingCostItem.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim decAmt As Decimal = 0
                Decimal.TryParse(CType(e.Row.Cells(getColumnID_Griview(dgvTrainingCostItem, "colhamount")).FindControl("txtAmount"), Controls_NumberOnly).Text, decAmt)
                CType(e.Row.Cells(getColumnID_Griview(dgvTrainingCostItem, "colhamount")).FindControl("txtAmount"), Controls_NumberOnly).Text = Format(decAmt, CStr(Session("fmtCurrency")))

                Dim decApprovedAmt As Decimal = 0
                Decimal.TryParse(CType(e.Row.Cells(getColumnID_Griview(dgvTrainingCostItem, "colhapproved_amount")).FindControl("txtApprovedAmount"), Controls_NumberOnly).Text, decApprovedAmt)
                CType(e.Row.Cells(getColumnID_Griview(dgvTrainingCostItem, "colhapproved_amount")).FindControl("txtApprovedAmount"), Controls_NumberOnly).Text = Format(decApprovedAmt, CStr(Session("fmtCurrency")))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (09 Jun 2021) -- Start
    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
    Protected Sub dgvTrainingInstructor_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvTrainingInstructor.RowCommand
        Try
            If e.CommandName = "Change" Then

                mintInstructSrNo = CInt(e.CommandArgument)

                If CInt(dgvTrainingInstructor.DataKeys(mintInstructSrNo).Item("employeeunkid")) > 0 Then

                    chkOtherInstructor.Checked = False
                Else
                    txtOthersName.Text = dgvTrainingInstructor.Rows(mintInstructSrNo).Cells(2).Text
                    txtOthersCompany.Text = dgvTrainingInstructor.Rows(mintInstructSrNo).Cells(3).Text
                    txtOthersDept.Text = dgvTrainingInstructor.Rows(mintInstructSrNo).Cells(4).Text
                    txtOthersJob.Text = dgvTrainingInstructor.Rows(mintInstructSrNo).Cells(5).Text
                    txtOthersEmail.Text = dgvTrainingInstructor.Rows(mintInstructSrNo).Cells(6).Text

                    chkOtherInstructor.Checked = True
                End If
                Call chkOtherInstructor_CheckedChanged(chkOtherInstructor, New System.EventArgs)
                chkOtherInstructor.Enabled = False

                btnAddInstructor.Enabled = False
                btnUpdateInstructor.Enabled = True

            ElseIf e.CommandName = "Remove" Then

                mintInstructSrNo = CInt(e.CommandArgument)
                Me.ViewState("InstructSrNo") = mintInstructSrNo
                popupDeleteInstruct.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingInstructor_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvTrainingInstructor.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(dgvTrainingInstructor.DataKeys(e.Row.RowIndex).Item("employeeunkid")) = False Then
                    If CInt(dgvTrainingInstructor.DataKeys(e.Row.RowIndex).Item("employeeunkid")) > 0 Then
                        e.Row.Cells(0).FindControl("btnEditInstruct").Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Jun 2021) -- End

#End Region

#Region " TextBox Event "

#End Region

#Region " Checkbox Event "
    Protected Sub chkOtherCompetences_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherCompetences.CheckedChanged
        Try
            pnlCompetence.Visible = Not chkOtherCompetences.Checked
            pnlOtherCompetence.Visible = chkOtherCompetences.Checked

            If chkOtherCompetences.Checked = True Then
                cboCompetences.SelectedValue = "0"
                Call cboCompetences_SelectedIndexChanged(cboCompetences, New System.EventArgs)
            Else
                txtOtherCompetence.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOtherTrainingCource_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherTrainingCource.CheckedChanged
        Try
            pnlTCourse.Visible = Not chkOtherTrainingCource.Checked
            pnlOtherTCourse.Visible = chkOtherTrainingCource.Checked

            If chkOtherTrainingCource.Checked = True Then
                cboTrainingNeeded.SelectedValue = "0"
            Else
                txtOtherTCourse.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkChooseEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkChooseEmployee.CheckedChanged
        Try
            pnlAllocEmp.Visible = chkChooseEmployee.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (09 Jun 2021) -- Start
    'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
    Protected Sub chkOtherInstructor_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherInstructor.CheckedChanged
        Try
            pnlInstructor.Enabled = Not chkOtherInstructor.Checked
            pnlOtherInstructor.Enabled = chkOtherInstructor.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Jun 2021) -- End

#End Region

#Region " Links Event "

    'Protected Sub lnkAddEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddEmployee.Click
    '    Dim dsList As New DataSet
    '    Dim dtTable As DataTable = Nothing
    '    Dim intColType As Integer = 0
    '    Try
    '        If IsValidData(False) = False Then Exit Try

    '        Dim strNotFilter As String = ""
    '        Dim strNotIDs As String = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("allocationtranunkid").ToString).ToArray)
    '        If strNotIDs.Trim <> "" Then
    '            strNotFilter &= " ID NOT IN (" & strNotIDs & ") "
    '        End If

    '        Select Case CInt(cboTargetedGroup.SelectedValue)

    '            Case 0 'Employee Names
    '                Dim objEmp As New clsEmployee_Master
    '                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                                CInt(Session("UserId")), _
    '                                                CInt(Session("Fin_year")), _
    '                                                CInt(Session("CompanyUnkId")), _
    '                                                dtpStartDate.GetDate.Date, _
    '                                                dtpEndDate.GetDate.Date, _
    '                                                CStr(Session("UserAccessModeSetting")), True, _
    '                                                True, "Emp", False, , CInt(cboDepartmentList.SelectedValue))

    '                intColType = 5

    '                dtTable = dsList.Tables(0)
    '                objEmp = Nothing

    '            Case enAllocation.BRANCH
    '                Dim objBranch As New clsStation
    '                dsList = objBranch.GetList("List", True)
    '                objBranch = Nothing

    '                If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then
    '                    If CInt(Session("UserId")) > 0 Then
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                    End If
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.DEPARTMENT_GROUP
    '                Dim objDeptGrp As New clsDepartmentGroup
    '                dsList = objDeptGrp.GetList("List", True)
    '                objDeptGrp = Nothing

    '                If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.DEPARTMENT

    '                Dim objDept As New clsDepartment
    '                dsList = objDept.GetList("List", True)
    '                objDept = Nothing

    '                'If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
    '                '    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                'Else
    '                '    dtTable = dsList.Tables("List")
    '                'End If
    '                Dim sFilter As String = ""
    '                If CInt(cboDepartmentList.SelectedValue) > 0 Then
    '                    sFilter = " departmentunkid = " & CInt(cboDepartmentList.SelectedValue) & " "
    '                End If
    '                dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

    '            Case enAllocation.SECTION_GROUP

    '                Dim objSG As New clsSectionGroup
    '                dsList = objSG.GetList("List", True)
    '                objSG = Nothing

    '                If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.SECTION

    '                Dim objSection As New clsSections
    '                dsList = objSection.GetList("List", True)
    '                objSection = Nothing

    '                If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.UNIT_GROUP

    '                Dim objUG As New clsUnitGroup
    '                dsList = objUG.GetList("List", True)
    '                objUG = Nothing

    '                If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.UNIT

    '                Dim objUnit As New clsUnits
    '                dsList = objUnit.GetList("List", True)
    '                objUnit = Nothing

    '                If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.TEAM

    '                Dim objTeam As New clsTeams
    '                dsList = objTeam.GetList("List", True)
    '                objTeam = Nothing

    '                If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.JOB_GROUP

    '                Dim objjobGRP As New clsJobGroup
    '                dsList = objjobGRP.GetList("List", True)
    '                objjobGRP = Nothing

    '                If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.JOBS

    '                Dim objJobs As New clsJobs
    '                dsList = objJobs.GetList("List", True)
    '                objJobs = Nothing

    '                intColType = 1

    '                If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If


    '            Case enAllocation.CLASS_GROUP

    '                Dim objClassGrp As New clsClassGroup
    '                dsList = objClassGrp.GetList("List", True)
    '                objClassGrp = Nothing

    '                If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
    '                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '            Case enAllocation.CLASSES

    '                Dim objClass As New clsClass
    '                dsList = objClass.GetList("List", True)
    '                objClass = Nothing

    '                If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
    '                    Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
    '                    StrFilter = StrFilter.Replace("classunkid", "classesunkid")
    '                    dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dtTable = dsList.Tables("List")
    '                End If

    '                'Case enAllocation.GradeGroup

    '                '    Dim objGradeGrp As New clsGradeGroup
    '                '    dsList = objGradeGrp.GetList("List", True)
    '                '    objGradeGrp = Nothing

    '                '    dtTable = dsList.Tables("List")

    '                'Case enAllocation.Grade

    '                '    Dim objGrade As New clsGrade
    '                '    dsList = objGrade.GetList("List", True)

    '                '    dtTable = dsList.Tables("List")

    '                'Case enAllocation.GradeLevel

    '                '    Dim objGradeLvl As New clsGradeLevel
    '                '    dsList = objGradeLvl.GetList("List", True)
    '                '    objGradeLvl = Nothing

    '                '    dtTable = dsList.Tables("List")

    '            Case enAllocation.COST_CENTER

    '                Dim objConstCenter As New clscostcenter_master
    '                dsList = objConstCenter.GetList("List", True)
    '                objConstCenter = Nothing

    '                intColType = 2
    '                dtTable = dsList.Tables("List")

    '        End Select

    '        If intColType = 0 Then
    '            dtTable.Columns(0).ColumnName = "Id"
    '            dtTable.Columns("code").ColumnName = "Code"
    '            dtTable.Columns("name").ColumnName = "Name"
    '        ElseIf intColType = 1 Then
    '            dtTable.Columns("jobunkid").ColumnName = "Id"
    '            dtTable.Columns("Code").ColumnName = "Code"
    '            dtTable.Columns("jobname").ColumnName = "Name"
    '        ElseIf intColType = 2 Then
    '            dtTable.Columns(0).ColumnName = "Id"
    '            dtTable.Columns("costcentercode").ColumnName = "Code"
    '            dtTable.Columns("costcentername").ColumnName = "Name"
    '        ElseIf intColType = 3 Then
    '            dtTable.Columns(1).ColumnName = "Id"
    '            dtTable.Columns(2).ColumnName = "Code"
    '            dtTable.Columns(0).ColumnName = "Name"
    '        ElseIf intColType = 4 Then
    '            dtTable.Columns(0).ColumnName = "Id"
    '            dtTable.Columns("country_code").ColumnName = "Code"
    '            dtTable.Columns("country_name").ColumnName = "Name"
    '        ElseIf intColType = 5 Then
    '            dtTable.Columns("employeeunkid").ColumnName = "Id"
    '            dtTable.Columns("employeecode").ColumnName = "Code"
    '            dtTable.Columns("employeename").ColumnName = "Name"
    '        End If

    '        dtTable = New DataView(dtTable, strNotFilter, "Name", DataViewRowState.CurrentRows).ToTable

    '        Dim dtCol As New DataColumn
    '        dtCol.ColumnName = "IsChecked"
    '        dtCol.Caption = ""
    '        dtCol.DataType = System.Type.GetType("System.Boolean")
    '        dtCol.DefaultValue = False
    '        dtTable.Columns.Add(dtCol)

    '        If dtTable.Rows.Count <= 0 Then
    '            dtTable.Rows.Add(dtTable.NewRow)
    '            dgvAddEmployee.DataSource = dtTable
    '            dgvAddEmployee.DataBind()

    '            Dim intCellCount As Integer = dgvAddEmployee.Rows(0).Cells.Count

    '            dgvAddEmployee.Rows(0).Cells(0).ColumnSpan = intCellCount
    '            For i As Integer = 1 To intCellCount - 1
    '                dgvAddEmployee.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvAddEmployee.Rows(0).Cells(0).Text = "No Records Found"
    '        Else
    '            dgvAddEmployee.DataSource = dtTable
    '            dgvAddEmployee.DataBind()
    '        End If


    '        mblnShowAddEmployeePopup = True
    '        popupAddEmployee.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        dsList = Nothing
    '        dtTable = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnCloseEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseEmployee.Click
    '    Try
    '        mblnShowAddEmployeePopup = False
    '        popupAddEmployee.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub lnkAddAllocEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllocEmp.Click
    '    Dim dsList As New DataSet
    '    Dim dtTable As DataTable = Nothing
    '    Dim intColType As Integer = 0
    '    Try
    '        If IsValidData(False) = False Then Exit Try

    '        If CInt(cboTargetedGroup.SelectedValue) <= 0 Then Exit Try 'Employee Names

    '        Dim strFilter As String = ""
    '        Dim strIDs As String = ""
    '        strIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("allocationtranunkid").ToString).ToArray)

    '        If strIDs.Trim = "" Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 211, "Please add atlease one item from target group list."), Me)
    '            Exit Try
    '        End If

    '        Select Case CInt(cboTargetedGroup.SelectedValue)

    '            Case enAllocation.BRANCH
    '                strFilter &= " AND T.stationunkid IN (" & strIDs & ") "

    '            Case enAllocation.DEPARTMENT_GROUP
    '                strFilter &= " AND T.deptgroupunkid IN (" & strIDs & ") "

    '            Case enAllocation.DEPARTMENT
    '                strFilter &= " AND T.departmentunkid IN (" & strIDs & ") "

    '            Case enAllocation.SECTION_GROUP
    '                strFilter &= " AND T.sectiongroupunkid IN (" & strIDs & ") "

    '            Case enAllocation.SECTION
    '                strFilter &= " AND T.sectionunkid IN (" & strIDs & ") "

    '            Case enAllocation.UNIT_GROUP
    '                strFilter &= " AND T.unitgroupunkid IN (" & strIDs & ") "

    '            Case enAllocation.UNIT
    '                strFilter &= " AND T.unitunkid IN (" & strIDs & ") "

    '            Case enAllocation.TEAM
    '                strFilter &= " AND T.teamunkid IN (" & strIDs & ") "

    '            Case enAllocation.JOB_GROUP
    '                strFilter &= " AND J.jobgroupunkid IN (" & strIDs & ") "

    '            Case enAllocation.JOBS
    '                strFilter &= " AND J.jobunkid IN (" & strIDs & ") "

    '            Case enAllocation.CLASS_GROUP
    '                strFilter &= " AND T.classgroupunkid IN (" & strIDs & ") "

    '            Case enAllocation.CLASSES
    '                strFilter &= " AND T.classunkid IN (" & strIDs & ") "

    '            Case enAllocation.COST_CENTER
    '                strFilter &= " AND C.costcenterunkid IN (" & strIDs & ") "

    '        End Select


    '        'Dim strNotFilter As String = ""
    '        Dim strNotIDs As String = String.Join(",", dgvAllocEmp.Rows.Cast(Of GridViewRow).Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString).ToArray)
    '        If strNotIDs.Trim <> "" Then
    '            strFilter &= " AND hremployee_master.employeeunkid NOT IN (" & strNotIDs & ") "
    '        End If

    '        If strFilter.Trim <> "" Then
    '            strFilter = strFilter.Substring(4)
    '        End If

    '        Dim objEmp As New clsEmployee_Master
    '        dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        dtpStartDate.GetDate.Date, _
    '                                        dtpEndDate.GetDate.Date, _
    '                                        CStr(Session("UserAccessModeSetting")), True, _
    '                                        True, "Emp", False, , CInt(cboDepartmentList.SelectedValue), strFilterQuery:=strFilter)


    '        dtTable = dsList.Tables(0)
    '        objEmp = Nothing

    '        dtTable = New DataView(dtTable, "", "employeename", DataViewRowState.CurrentRows).ToTable

    '        Dim dtCol As New DataColumn
    '        dtCol.ColumnName = "IsChecked"
    '        dtCol.Caption = ""
    '        dtCol.DataType = System.Type.GetType("System.Boolean")
    '        dtCol.DefaultValue = False
    '        dtTable.Columns.Add(dtCol)

    '        If dtTable.Rows.Count <= 0 Then
    '            dtTable.Rows.Add(dtTable.NewRow)
    '            dgvAddAllocEmp.DataSource = dtTable
    '            dgvAddAllocEmp.DataBind()

    '            Dim intCellCount As Integer = dgvAddAllocEmp.Rows(0).Cells.Count

    '            dgvAddAllocEmp.Rows(0).Cells(0).ColumnSpan = intCellCount
    '            For i As Integer = 1 To intCellCount - 1
    '                dgvAddAllocEmp.Rows(0).Cells(i).Visible = False
    '            Next
    '            dgvAddAllocEmp.Rows(0).Cells(0).Text = "No Records Found"
    '        Else
    '            dgvAddAllocEmp.DataSource = dtTable
    '            dgvAddAllocEmp.DataBind()
    '        End If


    '        mblnShowAddAllocEmpPopup = True
    '        popupAddAllocEmp.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        dsList = Nothing
    '        dtTable = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnCloseAllocEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseAllocEmp.Click
    '    Try
    '        mblnShowAddAllocEmpPopup = False
    '        popupAddAllocEmp.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub lnkAddTrainingResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingResources.Click
    '    Dim objCommon As New clsCommon_Master
    '    Dim dsList As DataSet
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim strNotFilter As String = ""
    '        Dim strNotIDs As String = String.Join(",", dgvTrainingResources.Rows.Cast(Of GridViewRow).Select(Function(x) dgvTrainingResources.DataKeys(x.RowIndex).Item("trainingresourceunkid").ToString).ToArray)
    '        If strNotIDs.Trim <> "" Then
    '            strNotFilter &= " AND cfcommon_master.masterunkid NOT IN (" & strNotIDs & ") "
    '        End If

    '        dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.TRAINING_RESOURCES, "List", , True, strNotFilter)

    '        dgvAddTResources.DataSource = dsList.Tables(0)
    '        dgvAddTResources.DataBind()

    '        mblnShowTResourcesPopup = True
    '        popupAddTResources.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objCommon = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnCloseTResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseTResources.Click
    '    Try
    '        mblnShowTResourcesPopup = False
    '        popupAddTResources.Hide()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub lnkAddFinancingSource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddFinancingSource.Click
    '    Dim objCommon As New clsCommon_Master
    '    Dim dsList As DataSet
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim strNotFilter As String = ""
    '        Dim strNotIDs As String = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("financingsourceunkid").ToString).ToArray)
    '        If strNotIDs.Trim <> "" Then
    '            strNotFilter &= " AND cfcommon_master.masterunkid NOT IN (" & strNotIDs & ") "
    '        End If

    '        dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, "List", , True, strNotFilter)

    '        dgvAddFinancingSource.DataSource = dsList
    '        dgvAddFinancingSource.DataBind()

    '        mblnShowFinancingSourcePopup = True
    '        popupFinancingSource.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objCommon = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnCloseFSource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseFSource.Click
    '    Try
    '        mblnShowFinancingSourcePopup = False
    '        popupFinancingSource.Hide()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub lnkAddTrainingCoordinator_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingCoordinator.Click
    '    Dim objEmp As New clsEmployee_Master
    '    Dim dsList As DataSet = Nothing
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim strNotFilter As String = ""
    '        Dim strNotIDs As String = String.Join(",", dgvTrainingCoordinator.Rows.Cast(Of GridViewRow).Select(Function(x) dgvTrainingCoordinator.DataKeys(x.RowIndex).Item("employeeunkid").ToString).ToArray)
    '        If strNotIDs.Trim <> "" Then
    '            strNotFilter &= " hremployee_master.employeeunkid NOT IN (" & strNotIDs & ") "
    '        End If

    '        dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        dtpStartDate.GetDate.Date, _
    '                                        dtpEndDate.GetDate.Date, _
    '                                        CStr(Session("UserAccessModeSetting")), True, _
    '                                        True, "Emp", False, strFilterQuery:=strNotFilter)

    '        dgvAddTCoordinator.DataSource = dsList.Tables(0)
    '        dgvAddTCoordinator.DataBind()

    '        mblnShowTrainingCoordinatorPopup = True
    '        popupAddTrainingCoordinator.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objEmp = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnCloseTCoordinator_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseTCoordinator.Click
    '    Try
    '        mblnShowTrainingCoordinatorPopup = False
    '        popupAddTrainingCoordinator.Hide()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub lnkAddTrainingCostItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingCostItem.Click
    '    Dim objTCostItem As New clstrainingitemsInfo_master
    '    Dim dsList As DataSet = Nothing
    '    Try
    '        If IsValidData() = False Then Exit Try

    '        Dim strNotFilter As String = ""
    '        Dim strNotIDs As String = String.Join(",", dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Select(Function(x) dgvTrainingCostItem.DataKeys(x.RowIndex).Item("infounkid").ToString).ToArray)
    '        If strNotIDs.Trim <> "" Then
    '            strNotFilter &= " hremployee_master.employeeunkid NOT IN (" & strNotIDs & ") "
    '        End If

    '        dsList = objTCostItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

    '        dgvAddTrainingCostItem.DataSource = dsList
    '        dgvAddTrainingCostItem.DataBind()

    '        mblnShowTrainingCostItemPopup = True
    '        popupTrainingCostItem.Show()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objTCostItem = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnCloseTCostItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseTCostItem.Click
    '    Try
    '        mblnShowTrainingCostItemPopup = False
    '        popupTrainingCostItem.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub btnSaveColumns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveColumns.Click
        Dim objConfig As New clsConfigOptions
        Try

            Dim strIDs As String = String.Join(",", (From p In chkColumns.Items.Cast(Of ListItem)() Where (p.Selected = True) Select (p.Value.ToString)).ToArray)

            objConfig._Companyunkid = CInt(Session("Companyunkid"))

            objConfig._DeptTrainingNeedListColumnsIDs = strIDs

            objConfig.updateParam()

            Session("DeptTrainingNeedListColumnsIDs") = strIDs

            Call FillList("", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
        End Try
    End Sub

#End Region

#Region " Radio Button Events "
    Protected Sub rdbDetailedList_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbDetailedList.CheckedChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub rdbSummary_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbSummary.CheckedChanged
        Try
            Call FillList("", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Control's Events "
    Protected Sub dtpStartDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpStartDate.TextChanged
        Try
            If dtpStartDate.GetDate <> Nothing Then
                Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dtpEndDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEndDate.TextChanged
        Try
            If dtpEndDate.GetDate <> Nothing Then
                Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Budget Summary & Set Max Budget"

#Region "Private Methods"

    Private Sub FillBudgetCalendar()
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As DataSet = Nothing
        Try
            Dim mintFirstOpenPeriodId As Integer = 0
            Dim dsCombo As DataSet = objCalendar.getListForCombo("List", False, enStatusType.OPEN)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                mintFirstOpenPeriodId = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If

            dsList = objCalendar.getListForCombo("List", True, 0)

            With cboSetMaxBudgetCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintFirstOpenPeriodId.ToString()
            End With
            cboSetMaxBudgetCalendar_SelectedIndexChanged(cboSetMaxBudgetCalendar, New EventArgs())

            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "calendarunkid <> 0", "", DataViewRowState.CurrentRows).ToTable()
            With cboBugdetTrainigCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dtTable.Copy()
                .DataBind()
                .SelectedValue = mintFirstOpenPeriodId.ToString()
            End With
            cboBugdetTrainigCalendar_SelectedIndexChanged(cboSetMaxBudgetCalendar, New EventArgs())

            dtTable.Clear()
            dtTable = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Private Function GetBudgetSummary(ByVal xPreviousPeriodId As Integer, Optional ByVal xAllocationTranId As Integer = -1) As DataTable
        Dim dtTable As New DataTable
        Try
            Dim objDpetTrainingNeed As New clsDepartmentaltrainingneed_master
            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            'Dim dtSummaryData As DataTable = objDpetTrainingNeed.GetBudgetSummaryData(CInt(cboBugdetTrainigCalendar.SelectedValue), xPreviousPeriodId, mintTrainingNeedAllocationID)
            Dim dtSummaryData As DataTable
            If Cache(CStr(Session("Database_Name")) & "_DTBudgetSummaryData") Is Nothing Then
                Dim strAllocationName As String = ""
                dtSummaryData = objDpetTrainingNeed.GetBudgetSummaryData(CInt(cboBugdetTrainigCalendar.SelectedValue), xPreviousPeriodId, mintTrainingNeedAllocationID, mintTrainingBudgetAllocationID, CInt(Session("TrainingCostCenterAllocationID")), CInt(Session("TrainingRemainingBalanceBasedOnID")), strAllocationName)
                Cache.Insert(CStr(Session("Database_Name")) & "_DTBudgetSummaryData", dtSummaryData, New CacheDependency(Server.MapPath(CStr(Session("Database_Name")) & "_DTBudgetSummaryData.xml")), DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration, CacheItemPriority.Default, New CacheItemRemovedCallback(AddressOf CacheRemoved))
                LblSummaryHeader.Text = strAllocationName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleSetMaxBudget, 100, "wise Budget Summary")
            Else
                dtSummaryData = CType(Cache(CStr(Session("Database_Name")) & "_DTBudgetSummaryData"), DataTable)
            End If
            'Sohail (10 Feb 2022) -- End

            If xAllocationTranId <> 0 AndAlso xAllocationTranId <> -1 Then
                dtTable = dtSummaryData.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("departmentunkid") = xAllocationTranId).CopyToDataTable()
            Else
                dtTable = New DataView(dtSummaryData, "", "", DataViewRowState.CurrentRows).ToTable()
            End If

            objDpetTrainingNeed = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    'Sohail (10 Feb 2022) -- Start
    'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
    Public Sub CacheRemoved(ByVal key As String, ByVal value As Object, ByVal reason As CacheItemRemovedReason)
        'Cache(CStr(Session("Database_Name")) & "_DTBudgetSummaryData") = Nothing
    End Sub
    'Sohail (10 Feb 2022) -- End

    Private Sub FillBudgetSummaryApprovalDepartment(ByVal mintPreviousPeriodId As Integer)
        Dim dtTable As DataTable = Nothing
        Try
            dtTable = GetBudgetSummary(mintPreviousPeriodId)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                txtLastBudgetGrandTotalRequested.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PreviousRequestedAmt")), Session("fmtcurrency").ToString())
                txtLastBudgetGrandTotalApproved.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PreviousApprovedAmt")), Session("fmtcurrency").ToString())
                txtLastBudgetGrandTotalEnrolled.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PreviousEnrolledAmt")), Session("fmtcurrency").ToString())
                txtThisBudgetGrandTotalRequested.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrentRequestedAmt")), Session("fmtcurrency").ToString())
                txtThisBudgetGrandTotalApproved.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrentApprovedAmt")), Session("fmtcurrency").ToString())
                txtThisBudgetGrandTotalEnrolled.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrentEnrolledAmt")), Session("fmtcurrency").ToString())

                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                'dtTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "departmentunkid", "allocationtranname")
                dtTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "departmentunkid", "allocationtranname", "allocationtrancode", "Prevmaxbudget_amt", "Currmaxbudget_amt", "PrevRemainingBalance", "CurrRemainingBalance")
                txtLastBudgetGrandTotalMaxAmount.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("Prevmaxbudget_amt")), Session("fmtcurrency").ToString())
                txtThisBudgetGrandTotalMaxAmount.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("Currmaxbudget_amt")), Session("fmtcurrency").ToString())
                txtLastBudgetGrandTotalRemainingBalance.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PrevRemainingBalance")), Session("fmtcurrency").ToString())
                txtThisBudgetGrandTotalRemainingBalance.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrRemainingBalance")), Session("fmtcurrency").ToString())
                'Sohail (10 Feb 2022) -- End
                dlBudgetSummaryApprovalDepartment.DataSource = dtTable.Copy()
                dlBudgetSummaryApprovalDepartment.DataBind()
            Else
                txtLastBudgetGrandTotalRequested.Text = "0.00"
                txtLastBudgetGrandTotalApproved.Text = "0.00"
                txtLastBudgetGrandTotalEnrolled.Text = "0.00"
                txtThisBudgetGrandTotalRequested.Text = "0.00"
                txtThisBudgetGrandTotalApproved.Text = "0.00"
                txtThisBudgetGrandTotalEnrolled.Text = "0.00"
                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                txtLastBudgetGrandTotalMaxAmount.Text = "0.00"
                txtThisBudgetGrandTotalMaxAmount.Text = "0.00"
                txtLastBudgetGrandTotalRemainingBalance.Text = "0.00"
                txtThisBudgetGrandTotalRemainingBalance.Text = "0.00"
                'Sohail (10 Feb 2022) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable.Clear()
            dtTable = Nothing
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkBudgetSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBudgetSummary.Click
        Try
            FillBudgetCalendar()
            popupBudgetSummaryApproval.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub lnkCurrentRefNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim gv As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            Dim lnk As LinkButton = CType(gv.FindControl("lnkCurrentRefNo"), LinkButton)

            If lnk IsNot Nothing Then
                'Sohail (26 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                cboTargetedGroupList.SelectedIndex = 0
                cboEmployeeNameList.SelectedValue = "0"
                cboEmployeeNameList.Enabled = True
                'cboPeriodList.SelectedValue = "0"
                cboPriorityList.SelectedValue = "0"
                cboTrainingName.SelectedValue = "0"
                cboTrainingCategoryList.SelectedValue = "0"
                txtTotalCostList.Decimal_ = 0
                cboStatusList.SelectedIndex = 0
                cboDepartmentList.SelectedValue = "0"
                'Sohail (26 Apr 2021) -- End
                txtRefnoList.Text = lnk.Text
                Dim dgvBudgetSummaryApprovalDepartmentItems As GridView = TryCast(gv.NamingContainer, GridView)
                cboDepartmentList.SelectedValue = dgvBudgetSummaryApprovalDepartmentItems.DataKeys(gv.RowIndex)("departmentunkid").ToString()
                FillList("", "", False)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupBudgetSummaryApproval.Hide()
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnSaveSetMaxBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSetMaxBudget.Click
        Try
            Dim gRow As List(Of GridViewRow) = Nothing
            gRow = GvSetMaxBudget.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_ <> 0).ToList()

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleSetMaxBudget, 66, "Please set maximum budget for at least one allocation."), Me)
                popupBudgetSummaryApproval.Show()
                Exit Try
            End If

            Dim gNewRow As List(Of GridViewRow) = Nothing
            Dim gEditRow As List(Of GridViewRow) = Nothing

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                Dim objMaxBudget As New clsDepartmenttrainingneed_maxbudget

                Dim objTrainingDept As New clsDepartmentaltrainingneed_master
                For Each row As GridViewRow In gRow
                    Dim mdecApprovedAmt As Decimal = objTrainingDept.GetTotalApprovedAmtAllocationWise(CInt(cboSetMaxBudgetCalendar.SelectedValue), mintTrainingNeedAllocationID, CInt(GvSetMaxBudget.DataKeys(row.RowIndex)("allocationtranunkid")))
                    If mdecApprovedAmt > CType(row.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_ AndAlso CType(row.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_ > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleSetMaxBudget, 67, "Sorry, Maximum budget cannot be less than the total approved amount."), Me)
                        popupBudgetSummaryApproval.Show()
                        Exit Sub
                    End If
                Next
                objTrainingDept = Nothing


                Dim lstMaxBudget As New List(Of clsDepartmenttrainingneed_maxbudget)
                Dim strIDs As String = String.Join(",", GvSetMaxBudget.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_ <> 0).Select(Function(x) GvSetMaxBudget.DataKeys(x.RowIndex).Item("allocationtranunkid").ToString).ToArray)

                Dim dsList As DataSet = objMaxBudget.GetList("List", CInt(cboSetMaxBudgetCalendar.SelectedValue), mintTrainingNeedAllocationID, True)
                Dim strExistIDs As String = String.Join(",", (From p In dsList.Tables(0) Where p.Field(Of Decimal)("maxbudget_amt") > 0 Select (p.Item("allocationtranunkid").ToString)).ToArray)

                gNewRow = GvSetMaxBudget.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_ <> 0 AndAlso strExistIDs.Split(CChar(",")).Contains(GvSetMaxBudget.DataKeys(x.RowIndex).Item("allocationtranunkid").ToString) = False).Select(Function(x) x).ToList
                gEditRow = GvSetMaxBudget.Rows.Cast(Of GridViewRow).Where(Function(x) strExistIDs.Split(CChar(",")).Contains(GvSetMaxBudget.DataKeys(x.RowIndex).Item("allocationtranunkid").ToString) = True).Select(Function(x) x).ToList


                For Each grRow As GridViewRow In gNewRow
                    Dim objMaxBdget As New clsDepartmenttrainingneed_maxbudget
                    With objMaxBdget
                        .pmintPeriodunkid = CInt(cboSetMaxBudgetCalendar.SelectedValue)
                        .pmintAllocationid = CInt(GvSetMaxBudget.DataKeys(grRow.RowIndex)("allocationid"))
                        .pmintAllocationtranunkid = CInt(GvSetMaxBudget.DataKeys(grRow.RowIndex)("allocationtranunkid"))
                        .pmdecMaxbudget_Amt = CType(grRow.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_
                        .pmintUserunkid = CInt(Session("UserId"))
                        .pmblnIsvoid = False
                        .pmstrFormName = mstrModuleSetMaxBudget
                        .pmstrClientIp = Session("IP_ADD").ToString()
                        .pmstrHostName = Session("HOST_NAME").ToString()
                        .pmblnIsweb = True
                    End With
                    lstMaxBudget.Add(objMaxBdget)
                Next

                For Each grRow As GridViewRow In gEditRow

                    Dim rw() As DataRow = dsList.Tables(0).Select("allocationid = " & CInt(GvSetMaxBudget.DataKeys(grRow.RowIndex).Item("allocationid")) & " AND allocationtranunkid = " & CInt(GvSetMaxBudget.DataKeys(grRow.RowIndex).Item("allocationtranunkid")))
                    If rw.Length > 0 Then

                        If CType(GvSetMaxBudget.Rows(grRow.RowIndex).FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_ <> CDec(rw(0).Item("maxbudget_amt")) Then

                            Dim objMaxBdget As New clsDepartmenttrainingneed_maxbudget
                            With objMaxBdget
                                .pmintPeriodunkid = CInt(cboSetMaxBudgetCalendar.SelectedValue)
                                .pmintAllocationid = CInt(GvSetMaxBudget.DataKeys(grRow.RowIndex)("allocationid"))
                                .pmintAllocationtranunkid = CInt(GvSetMaxBudget.DataKeys(grRow.RowIndex)("allocationtranunkid"))
                                .pmdecMaxbudget_Amt = CType(grRow.FindControl("txtMaxBudget"), Controls_NumberOnly).Decimal_
                                .pmintUserunkid = CInt(Session("UserId"))
                                .pmblnIsvoid = False
                                .pmstrFormName = mstrModuleSetMaxBudget
                                .pmstrClientIp = Session("IP_ADD").ToString()
                                .pmstrHostName = Session("HOST_NAME").ToString()
                                .pmblnIsweb = True
                            End With
                            lstMaxBudget.Add(objMaxBdget)
                        End If
                    End If
                Next


                ' SET VALUE TO THE CLASS OBJECT
                objMaxBudget._lstDeptMaxBudget = lstMaxBudget

                If objMaxBudget.Save() = False Then
                    If objMaxBudget._Message <> "" Then
                        DisplayMessage.DisplayMessage(objMaxBudget._Message, Me)
                        popupBudgetSummaryApproval.Show()
                        Exit Sub
                    End If
                End If

            End If

            popupBudgetSummaryApproval.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Protected Sub cboBugdetTrainigCalendar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBugdetTrainigCalendar.SelectedIndexChanged
        Dim objCalender As New clsTraining_Calendar_Master
        Try
            objCalender._Calendarunkid = CInt(cboBugdetTrainigCalendar.SelectedValue)
            Dim mintPreviousPeriodId As Integer = objCalender.GetLastCalendarId(objCalender._Calendarunkid, objCalender._StartDate.Date)
            dlBudgetSummaryApprovalDepartment.DataSource = Nothing
            dlBudgetSummaryApprovalDepartment.DataBind()
            FillBudgetSummaryApprovalDepartment(mintPreviousPeriodId)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalender = Nothing
            popupBudgetSummaryApproval.Show()
        End Try
    End Sub

    Protected Sub cboSetMaxBudgetCalendar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSetMaxBudgetCalendar.SelectedIndexChanged
        Try
            Dim mblnBlank As Boolean = False
            Dim objBudget As New clsDepartmenttrainingneed_maxbudget
            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            'Dim dsList As DataSet = objBudget.GetList("List", CInt(cboSetMaxBudgetCalendar.SelectedValue), mintTrainingNeedAllocationID, True)
            Dim dsList As DataSet = objBudget.GetList("List", CInt(cboSetMaxBudgetCalendar.SelectedValue), mintTrainingBudgetAllocationID, True)
            'Sohail (10 Feb 2022) -- End
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("allocationtrancode") = ""
                drRow("allocationtranname") = ""
                drRow("maxbudget_amt") = 0.0
                dsList.Tables(0).Rows.Add(drRow)
                mblnBlank = True
            End If
            GvSetMaxBudget.DataSource = dsList.Tables(0)
            GvSetMaxBudget.DataBind()
            If mblnBlank Then GvSetMaxBudget.Rows(0).Visible = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupBudgetSummaryApproval.Show()
        End Try
    End Sub

#End Region

#Region "Gridview Events"

    Protected Sub GvSetMaxBudget_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvSetMaxBudget.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txt As Controls_NumberOnly = CType(e.Row.FindControl("txtMaxBudget"), Controls_NumberOnly)
                If txt.Text.Length <= 0 Then txt.Text = "0.00"
                txt.Text = Format(txt.Decimal_, Session("fmtcurrency").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dlBudgetSummaryApprovalDepartment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlBudgetSummaryApprovalDepartment.ItemDataBound
        Dim dtTable As New DataTable
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim objCalender As New clsTraining_Calendar_Master
                objCalender._Calendarunkid = CInt(cboBugdetTrainigCalendar.SelectedValue)
                Dim mintPreviousPeriodId As Integer = objCalender.GetLastCalendarId(objCalender._Calendarunkid, objCalender._StartDate.Date)
                objCalender = Nothing

                Dim hfDepartmentId As HiddenField = TryCast(e.Item.FindControl("hfcategoryunkid"), HiddenField)
                Dim dgvBudgetSummaryApprovalDepartmentItems As GridView = TryCast(e.Item.FindControl("dgvBudgetSummaryApprovalDepartmentItems"), GridView)

                dtTable = GetBudgetSummary(mintPreviousPeriodId, CInt(hfDepartmentId.Value))

                If dtTable IsNot Nothing Then
                    dgvBudgetSummaryApprovalDepartmentItems.DataSource = dtTable.Copy()
                    dgvBudgetSummaryApprovalDepartmentItems.DataBind()

                    Dim Lbl As Label = Nothing

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtLastBudgetTotalRequested"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PreviousRequestedAmt")), Session("fmtcurrency").ToString())

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtLastBudgetTotalApproved"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PreviousApprovedAmt")), Session("fmtcurrency").ToString())

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtLastBudgetTotalEnrolled"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("PreviousEnrolledAmt")), Session("fmtcurrency").ToString())

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtThisBudgetTotalRequested"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrentRequestedAmt")), Session("fmtcurrency").ToString())

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtThisBudgetTotalApproved"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrentApprovedAmt")), Session("fmtcurrency").ToString())

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtThisBudgetTotalEnrolled"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("CurrentEnrolledAmt")), Session("fmtcurrency").ToString())
                    Lbl = Nothing

                    'Sohail (10 Feb 2022) -- Start
                    'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtLastBudgetMaxAmount"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Max(Function(x) x.Field(Of Decimal)("Prevmaxbudget_amt")), Session("fmtcurrency").ToString())
                    Lbl = Nothing

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtThisBudgetMaxAmount"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Max(Function(x) x.Field(Of Decimal)("Currmaxbudget_amt")), Session("fmtcurrency").ToString())
                    Lbl = Nothing

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtLastBudgetRemainingBalance"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Max(Function(x) x.Field(Of Decimal)("PrevRemainingBalance")), Session("fmtcurrency").ToString())
                    Lbl = Nothing

                    Lbl = Nothing
                    Lbl = TryCast(e.Item.FindControl("txtThisBudgetRemainingBalance"), Label)
                    Lbl.Text = Format(dtTable.AsEnumerable().Max(Function(x) x.Field(Of Decimal)("CurrRemainingBalance")), Session("fmtcurrency").ToString())
                    Lbl = Nothing
                    'Sohail (10 Feb 2022) -- End

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable.Clear()
            dtTable = Nothing
        End Try

    End Sub

    Public Sub dgvBudgetSummaryApprovalDepartmentItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhPreviousRequestedAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhPreviousRequestedAmount", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhPreviousApprovedAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhPreviousApprovedAmount", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhPreviousEnrolledAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhPreviousEnrolledAmount", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhThisRequestedAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhThisRequestedAmount", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhThisApprovedAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhThisApprovedAmount", False, True)).Text), Session("fmtcurrency").ToString())
                e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhThisEnrolledAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objcolhThisEnrolledAmount", False, True)).Text), Session("fmtcurrency").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "TextBox Events"

    Protected Sub txtMaxbudget_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtMaxbudget As TextBox = CType(sender, TextBox)
            If txtMaxbudget.Text.Length <= 0 Then txtMaxbudget.Text = "0.00"
            txtMaxbudget.Text = Format(CDec(txtMaxbudget.Text), Session("fmtcurrency").ToString())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupBudgetSummaryApproval.Show()
        End Try
    End Sub

#End Region

#End Region

#Region "Confirmation"
    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnSave.ID.ToUpper
                    Call Save_Click()
                    'Sohail (04 Aug 2021) -- Start
                    'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                Case btnSaveAndSubmit.ID.ToUpper
                    Call Save_Click(False, True)
                    'Sohail (04 Aug 2021) -- End
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblPageHeader.ID, Me.Title)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblDepartmentList.ID, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblPageHeader1.ID, Me.lblPageHeader1.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTargetedGroupList.ID, Me.lblTargetedGroupList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblPeriodList.ID, Me.lblPeriodList.Text)
             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblPriorityList.ID, Me.lblPriorityList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingName.ID, Me.lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingCategoryList.ID, Me.lblTrainingCategoryList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTotalCostList.ID, Me.lblTotalCostList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblRefnoList.ID, Me.lblRefnoList.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnAddList.ID, Me.btnAddList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnRestList.ID, Me.btnRestList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnCloseList.ID, Me.btnCloseList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnSaveColumns.ID, Me.btnSaveColumns.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnCloseColumns.ID, Me.btnCloseColumns.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnSubmitForApprovalFromDeptTNeed.ID, Me.btnSubmitForApprovalFromDeptTNeed.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnSubmitForApprovalFromTBacklog.ID, Me.btnSubmitForApprovalFromTBacklog.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnFinalApprove.ID, Me.btnFinalApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnReject.ID, Me.btnReject.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnAskForReview.ID, Me.btnAskForReview.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnUnlockSubmitApproval.ID, Me.btnUnlockSubmitApproval.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnUndoApproved.ID, Me.btnUndoApproved.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnUndoRejected.ID, Me.btnUndoRejected.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblBudgetSummary.ID, Me.lblBudgetSummary.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblGrandTotalList.ID, Me.lblGrandTotalList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblApprovedGrandTotalList.ID, Me.lblApprovedGrandTotalList.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(2).FooterText, gvDeptTrainingeedList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(3).FooterText, gvDeptTrainingeedList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(4).FooterText, gvDeptTrainingeedList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(5).FooterText, gvDeptTrainingeedList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(6).FooterText, gvDeptTrainingeedList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(7).FooterText, gvDeptTrainingeedList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(8).FooterText, gvDeptTrainingeedList.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(9).FooterText, gvDeptTrainingeedList.Columns(9).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(10).FooterText, gvDeptTrainingeedList.Columns(10).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(11).FooterText, gvDeptTrainingeedList.Columns(11).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(12).FooterText, gvDeptTrainingeedList.Columns(12).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(13).FooterText, gvDeptTrainingeedList.Columns(13).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(14).FooterText, gvDeptTrainingeedList.Columns(14).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(15).FooterText, gvDeptTrainingeedList.Columns(15).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(16).FooterText, gvDeptTrainingeedList.Columns(16).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(17).FooterText, gvDeptTrainingeedList.Columns(17).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(18).FooterText, gvDeptTrainingeedList.Columns(18).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(19).FooterText, gvDeptTrainingeedList.Columns(19).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(20).FooterText, gvDeptTrainingeedList.Columns(20).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(21).FooterText, gvDeptTrainingeedList.Columns(21).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(22).FooterText, gvDeptTrainingeedList.Columns(22).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(22).FooterText, gvDeptTrainingeedList.Columns(22).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(23).FooterText, gvDeptTrainingeedList.Columns(23).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvDeptTrainingeedList.Columns(24).FooterText, gvDeptTrainingeedList.Columns(24).HeaderText)

            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblAddEditDepartmentalTrainingNeed.ID, Me.lblAddEditDepartmentalTrainingNeed.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingInfo.ID, Me.lblTrainingInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTargetGroupInfo.ID, Me.lblTargetGroupInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingResources.ID, Me.lblTrainingResources.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblFinancingSource.ID, Me.lblFinancingSource.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingCoordinator.ID, Me.lblTrainingCoordinator.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingCostItem.ID, Me.lblTrainingCostItem.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblPeriod.ID, Me.lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblCompetences.ID, Me.lblCompetences.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkOtherCompetences.ID, Me.chkOtherCompetences.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingNeeded.ID, Me.lblTrainingNeeded.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkOtherTrainingCource.ID, Me.chkOtherTrainingCource.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingCategory.ID, Me.lblTrainingCategory.Text)
             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblPriority.ID, Me.lblPriority.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblLearningMethod.ID, Me.lblLearningMethod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblStartDate.ID, Me.lblStartDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblEndDate.ID, Me.lblEndDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingProvider.ID, Me.lblTrainingProvider.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingVenue.ID, Me.lblTrainingVenue.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblCommentRemark.ID, Me.lblCommentRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkCertiRequired.ID, Me.chkCertiRequired.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTargetedGroup.ID, Me.lblTargetedGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblNoOfStaff.ID, Me.lblNoOfStaff.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkChooseEmployee.ID, Me.chkChooseEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkEmpOnlyTicked.ID, Me.chkEmpOnlyTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblTrainingInstructor.ID, Me.lblTrainingInstructor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkTInstructorTicked.ID, Me.chkTInstructorTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkOtherInstructor.ID, Me.chkOtherInstructor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblOthersName.ID, Me.lblOthersName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblOthersCompany.ID, Me.lblOthersCompany.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblOthersDept.ID, Me.lblOthersDept.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblOthersJob.ID, Me.lblOthersJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblOthersEmail.ID, Me.lblOthersEmail.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnAddInstructor.ID, Me.btnAddInstructor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnUpdateInstructor.ID, Me.btnUpdateInstructor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnResetInstructor.ID, Me.btnResetInstructor.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkAllocEmpOnlyTicked.ID, Me.chkAllocEmpOnlyTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkTResourcesOnlyTicked.ID, Me.chkTResourcesOnlyTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkFSourceOnlyTicked.ID, Me.chkFSourceOnlyTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkTCoordinatOnlyTicked.ID, Me.chkTCoordinatOnlyTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkTCostItemTicked.ID, Me.chkTCostItemTicked.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,chkTrainingCostOptional.ID, Me.chkTrainingCostOptional.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblCostItemTotal.ID, Me.lblCostItemTotal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblCostItemApprovedTotal.ID, Me.lblCostItemApprovedTotal.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnSave.ID, Me.btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnSaveAndSubmit.ID, Me.btnSaveAndSubmit.Text)

             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingResources.Columns(1).FooterText, dgvTrainingResources.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingResources.Columns(2).FooterText, dgvTrainingResources.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvFinancingSource.Columns(2).FooterText, dgvFinancingSource.Columns(2).HeaderText)

             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingCoordinator.Columns(1).FooterText, dgvTrainingCoordinator.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingCoordinator.Columns(2).FooterText, dgvTrainingCoordinator.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingCostItem.Columns(2).FooterText, dgvTrainingCostItem.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingCostItem.Columns(3).FooterText, dgvTrainingCostItem.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingCostItem.Columns(4).FooterText, dgvTrainingCostItem.Columns(4).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvAddTrainingInstructor.Columns(1).FooterText, dgvAddTrainingInstructor.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvAddTrainingInstructor.Columns(2).FooterText, dgvAddTrainingInstructor.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingInstructor.Columns(2).FooterText, dgvTrainingInstructor.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingInstructor.Columns(3).FooterText, dgvTrainingInstructor.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingInstructor.Columns(4).FooterText, dgvTrainingInstructor.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingInstructor.Columns(5).FooterText, dgvTrainingInstructor.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,dgvTrainingInstructor.Columns(6).FooterText, dgvTrainingInstructor.Columns(6).HeaderText)


            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, LblSummaryHeader.ID, Me.LblSummaryHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, LblTabBudgetSummary.ID, Me.LblTabBudgetSummary.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,LblTabSetMaxBudget.ID, Me.LblTabSetMaxBudget.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,LblBudgetTrainingCalendar.ID, Me.LblBudgetTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblLastBudgetGrandTotalRequested.ID, Me.lblLastBudgetGrandTotalRequested.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblLastBudgetGrandTotalApproved.ID, Me.lblLastBudgetGrandTotalApproved.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblLastBudgetGrandTotalEnrolled.ID, Me.lblLastBudgetGrandTotalEnrolled.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblThisBudgetGrandTotalRequested.ID, Me.lblThisBudgetGrandTotalRequested.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,lblThisBudgetGrandTotalApproved.ID, Me.lblThisBudgetGrandTotalApproved.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblThisBudgetGrandTotalEnrolled.ID, Me.lblThisBudgetGrandTotalEnrolled.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLastBudgetGrandTotalMaxAmount.ID, Me.lblLastBudgetGrandTotalMaxAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLastBudgetGrandTotalRemainingBalance.ID, Me.lblLastBudgetGrandTotalRemainingBalance.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblThisBudgetGrandTotalMaxAmount.ID, Me.lblThisBudgetGrandTotalMaxAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblThisBudgetGrandTotalRemainingBalance.ID, Me.lblThisBudgetGrandTotalRemainingBalance.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,LblSetMaxTrainingCalender.ID, Me.LblSetMaxTrainingCalender.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnSaveSetMaxBudget.ID, Me.btnSaveSetMaxBudget.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,btnCloseSetMaxBudget.ID, Me.btnCloseSetMaxBudget.Text)

             Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvSetMaxBudget.Columns(0).FooterText, GvSetMaxBudget.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvSetMaxBudget.Columns(1).FooterText, GvSetMaxBudget.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,GvSetMaxBudget.Columns(2).FooterText, GvSetMaxBudget.Columns(2).HeaderText)

            popupDeleteDTNeed.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 79, "Are you sure you want to delete the selected departmental training need(s)?")
            popupActiveDTNeed.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 80, "Are you sure you want to make departmental training need active?")
            popupInactiveDTNeed.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 81, "Are you sure you want to make departmental training need inactive?")
            popupDeleteInstruct.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 83, "Are you sure you want to delete selected instructor?")
            popupUnlockSubmitted.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 94, "Unlock Remark")
            popupFinalApprove.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 95, "Approval Remark")
            popupFinalReject.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 96, "Rejection Remark")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleNameList)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblPageHeader.ID, Me.Title)

            'Me.lblDepartmentList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblDepartmentList.ID, Me.Title)
            Me.lblPageHeader1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblPageHeader1.ID, Me.lblPageHeader1.Text)

            Me.lblTargetedGroupList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblTargetedGroupList.ID, Me.lblTargetedGroupList.Text)
            Me.lblPeriodList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblPeriodList.ID, Me.lblPeriodList.Text)
            Me.lblPriorityList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblPriorityList.ID, Me.lblPriorityList.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lblTrainingCategoryList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblTrainingCategoryList.ID, Me.lblTrainingCategoryList.Text)
            Me.lblTotalCostList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblTotalCostList.ID, Me.lblTotalCostList.Text)
            Me.lblRefnoList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblRefnoList.ID, Me.lblRefnoList.Text)

            Me.btnAddList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnAddList.ID, Me.btnAddList.Text).Replace("&", "")
            Me.btnRestList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnRestList.ID, Me.btnRestList.Text).Replace("&", "")
            Me.btnCloseList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnCloseList.ID, Me.btnCloseList.Text).Replace("&", "")
            Me.btnSaveColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnSaveColumns.ID, Me.btnSaveColumns.Text).Replace("&", "")
            Me.btnCloseColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnCloseColumns.ID, Me.btnCloseColumns.Text).Replace("&", "")
            Me.btnSubmitForApprovalFromDeptTNeed.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnSubmitForApprovalFromDeptTNeed.ID, Me.btnSubmitForApprovalFromDeptTNeed.Text).Replace("&", "")
            Me.btnSubmitForApprovalFromTBacklog.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnSubmitForApprovalFromTBacklog.ID, Me.btnSubmitForApprovalFromTBacklog.Text).Replace("&", "")
            Me.btnFinalApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnFinalApprove.ID, Me.btnFinalApprove.Text).Replace("&", "")
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnAskForReview.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnAskForReview.ID, Me.btnAskForReview.Text).Replace("&", "")
            Me.btnUnlockSubmitApproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnUnlockSubmitApproval.ID, Me.btnUnlockSubmitApproval.Text).Replace("&", "")
            Me.btnUndoApproved.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnUndoApproved.ID, Me.btnUndoApproved.Text).Replace("&", "")
            Me.btnUndoRejected.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),btnUndoRejected.ID, Me.btnUndoRejected.Text).Replace("&", "")

            Me.lblBudgetSummary.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblBudgetSummary.ID, Me.lblBudgetSummary.Text)
            Me.lblGrandTotalList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblGrandTotalList.ID, Me.lblGrandTotalList.Text)
            Me.lblApprovedGrandTotalList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),lblApprovedGrandTotalList.ID, Me.lblApprovedGrandTotalList.Text)

            gvDeptTrainingeedList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(2).FooterText, gvDeptTrainingeedList.Columns(2).HeaderText)
            gvDeptTrainingeedList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(3).FooterText, gvDeptTrainingeedList.Columns(3).HeaderText)
            gvDeptTrainingeedList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(4).FooterText, gvDeptTrainingeedList.Columns(4).HeaderText)
            gvDeptTrainingeedList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(5).FooterText, gvDeptTrainingeedList.Columns(5).HeaderText)
            gvDeptTrainingeedList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(6).FooterText, gvDeptTrainingeedList.Columns(6).HeaderText)
            gvDeptTrainingeedList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(7).FooterText, gvDeptTrainingeedList.Columns(7).HeaderText)
            gvDeptTrainingeedList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(8).FooterText, gvDeptTrainingeedList.Columns(8).HeaderText)
            gvDeptTrainingeedList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(9).FooterText, gvDeptTrainingeedList.Columns(9).HeaderText)
            gvDeptTrainingeedList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(10).FooterText, gvDeptTrainingeedList.Columns(10).HeaderText)
            gvDeptTrainingeedList.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(11).FooterText, gvDeptTrainingeedList.Columns(11).HeaderText)
            gvDeptTrainingeedList.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(12).FooterText, gvDeptTrainingeedList.Columns(12).HeaderText)
            gvDeptTrainingeedList.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(13).FooterText, gvDeptTrainingeedList.Columns(13).HeaderText)
            gvDeptTrainingeedList.Columns(14).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(14).FooterText, gvDeptTrainingeedList.Columns(14).HeaderText)
            gvDeptTrainingeedList.Columns(15).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(15).FooterText, gvDeptTrainingeedList.Columns(15).HeaderText)
            gvDeptTrainingeedList.Columns(16).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(16).FooterText, gvDeptTrainingeedList.Columns(16).HeaderText)
            gvDeptTrainingeedList.Columns(17).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(17).FooterText, gvDeptTrainingeedList.Columns(17).HeaderText)
            gvDeptTrainingeedList.Columns(18).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(18).FooterText, gvDeptTrainingeedList.Columns(18).HeaderText)
            gvDeptTrainingeedList.Columns(19).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(19).FooterText, gvDeptTrainingeedList.Columns(19).HeaderText)
            gvDeptTrainingeedList.Columns(20).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(20).FooterText, gvDeptTrainingeedList.Columns(20).HeaderText)
            gvDeptTrainingeedList.Columns(21).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(21).FooterText, gvDeptTrainingeedList.Columns(21).HeaderText)
            gvDeptTrainingeedList.Columns(22).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(22).FooterText, gvDeptTrainingeedList.Columns(22).HeaderText)
            gvDeptTrainingeedList.Columns(23).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(23).FooterText, gvDeptTrainingeedList.Columns(23).HeaderText)
            'CType(gvDeptTrainingeedList.HeaderRow.Cells(24).Controls(0), LinkButton).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(23).FooterText, CType(gvDeptTrainingeedList.HeaderRow.Cells(23).Controls(0), LinkButton).Text)
            gvDeptTrainingeedList.Columns(25).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList,CInt(HttpContext.Current.Session("LangId")),gvDeptTrainingeedList.Columns(25).FooterText, gvDeptTrainingeedList.Columns(25).HeaderText)

            'Language.setLanguage(mstrModuleName)
            Me.lblAddEditDepartmentalTrainingNeed.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblAddEditDepartmentalTrainingNeed.ID, Me.lblAddEditDepartmentalTrainingNeed.Text)
            Me.lblTrainingInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingInfo.ID, Me.lblTrainingInfo.Text)
            Me.lblTargetGroupInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTargetGroupInfo.ID, Me.lblTargetGroupInfo.Text)
            Me.lblTrainingResources.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingResources.ID, Me.lblTrainingResources.Text)
            Me.lblFinancingSource.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblFinancingSource.ID, Me.lblFinancingSource.Text)
            Me.lblTrainingCoordinator.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingCoordinator.ID, Me.lblTrainingCoordinator.Text)
            Me.lblTrainingCostItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingCostItem.ID, Me.lblTrainingCostItem.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblCompetences.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblCompetences.ID, Me.lblCompetences.Text)
            Me.chkOtherCompetences.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkOtherCompetences.ID, Me.chkOtherCompetences.Text)
            Me.lblTrainingNeeded.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingNeeded.ID, Me.lblTrainingNeeded.Text)
            Me.chkOtherTrainingCource.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkOtherTrainingCource.ID, Me.chkOtherTrainingCource.Text)
            Me.lblTrainingCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingCategory.ID, Me.lblTrainingCategory.Text)
            Me.lblPriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblPriority.ID, Me.lblPriority.Text)
            Me.lblLearningMethod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblLearningMethod.ID, Me.lblLearningMethod.Text)
            Me.lblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblEndDate.ID, Me.lblEndDate.Text)
            Me.lblTrainingProvider.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingProvider.ID, Me.lblTrainingProvider.Text)
            Me.lblTrainingVenue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingVenue.ID, Me.lblTrainingVenue.Text)
            Me.lblCommentRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblCommentRemark.ID, Me.lblCommentRemark.Text)
            Me.chkCertiRequired.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkCertiRequired.ID, Me.chkCertiRequired.Text)
            Me.lblTargetedGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTargetedGroup.ID, Me.lblTargetedGroup.Text)
            Me.lblNoOfStaff.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblNoOfStaff.ID, Me.lblNoOfStaff.Text)
            Me.chkChooseEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkChooseEmployee.ID, Me.chkChooseEmployee.Text)
            Me.chkEmpOnlyTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkEmpOnlyTicked.ID, Me.chkEmpOnlyTicked.Text)
            Me.lblTrainingInstructor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblTrainingInstructor.ID, Me.lblTrainingInstructor.Text)
            Me.chkTInstructorTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkTInstructorTicked.ID, Me.chkTInstructorTicked.Text)
            Me.chkOtherInstructor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkOtherInstructor.ID, Me.chkOtherInstructor.Text)
            Me.lblOthersName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblOthersName.ID, Me.lblOthersName.Text)
            Me.lblOthersCompany.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblOthersCompany.ID, Me.lblOthersCompany.Text)
            Me.lblOthersDept.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblOthersDept.ID, Me.lblOthersDept.Text)
            Me.lblOthersJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblOthersJob.ID, Me.lblOthersJob.Text)
            Me.lblOthersEmail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblOthersEmail.ID, Me.lblOthersEmail.Text)
            Me.btnAddInstructor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnAddInstructor.ID, Me.btnAddInstructor.Text)
            Me.btnUpdateInstructor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnUpdateInstructor.ID, Me.btnUpdateInstructor.Text).Replace("&", "")
            Me.btnResetInstructor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnResetInstructor.ID, Me.btnResetInstructor.Text).Replace("&", "")

            Me.chkAllocEmpOnlyTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkAllocEmpOnlyTicked.ID, Me.chkAllocEmpOnlyTicked.Text)
            Me.chkTResourcesOnlyTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkTResourcesOnlyTicked.ID, Me.chkTResourcesOnlyTicked.Text)
            Me.chkFSourceOnlyTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkFSourceOnlyTicked.ID, Me.chkFSourceOnlyTicked.Text)
            Me.chkTCoordinatOnlyTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkTCoordinatOnlyTicked.ID, Me.chkTCoordinatOnlyTicked.Text)
            Me.chkTCostItemTicked.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkTCostItemTicked.ID, Me.chkTCostItemTicked.Text)
            Me.chkTrainingCostOptional.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),chkTrainingCostOptional.ID, Me.chkTrainingCostOptional.Text)
            Me.lblCostItemTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblCostItemTotal.ID, Me.lblCostItemTotal.Text)
            Me.lblCostItemApprovedTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblCostItemApprovedTotal.ID, Me.lblCostItemApprovedTotal.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnSaveAndSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnSaveAndSubmit.ID, Me.btnSaveAndSubmit.Text)

            dgvEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            dgvEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            dgvAllocEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            dgvAllocEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            dgvTrainingResources.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingResources.Columns(1).FooterText, dgvTrainingResources.Columns(1).HeaderText)
            dgvTrainingResources.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingResources.Columns(2).FooterText, dgvTrainingResources.Columns(2).HeaderText)

            dgvFinancingSource.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)
            dgvFinancingSource.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvFinancingSource.Columns(2).FooterText, dgvFinancingSource.Columns(2).HeaderText)

            dgvTrainingCoordinator.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingCoordinator.Columns(1).FooterText, dgvTrainingCoordinator.Columns(1).HeaderText)
            dgvTrainingCoordinator.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingCoordinator.Columns(2).FooterText, dgvTrainingCoordinator.Columns(2).HeaderText)

            dgvTrainingCostItem.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)
            dgvTrainingCostItem.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingCostItem.Columns(2).FooterText, dgvTrainingCostItem.Columns(2).HeaderText)
            dgvTrainingCostItem.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingCostItem.Columns(3).FooterText, dgvTrainingCostItem.Columns(3).HeaderText)
            dgvTrainingCostItem.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingCostItem.Columns(4).FooterText, dgvTrainingCostItem.Columns(4).HeaderText)

            dgvAddTrainingInstructor.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvAddTrainingInstructor.Columns(1).FooterText, dgvAddTrainingInstructor.Columns(1).HeaderText)
            dgvAddTrainingInstructor.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvAddTrainingInstructor.Columns(2).FooterText, dgvAddTrainingInstructor.Columns(2).HeaderText)

            dgvTrainingInstructor.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingInstructor.Columns(2).FooterText, dgvTrainingInstructor.Columns(2).HeaderText)
            dgvTrainingInstructor.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingInstructor.Columns(3).FooterText, dgvTrainingInstructor.Columns(3).HeaderText)
            dgvTrainingInstructor.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingInstructor.Columns(4).FooterText, dgvTrainingInstructor.Columns(4).HeaderText)
            dgvTrainingInstructor.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingInstructor.Columns(5).FooterText, dgvTrainingInstructor.Columns(5).HeaderText)
            dgvTrainingInstructor.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgvTrainingInstructor.Columns(6).FooterText, dgvTrainingInstructor.Columns(6).HeaderText)


            'Language.setLanguage(mstrModuleName)
            Me.LblSummaryHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),LblSummaryHeader.ID, Me.LblSummaryHeader.Text)
            Me.LblTabBudgetSummary.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),LblTabBudgetSummary.ID, Me.LblTabBudgetSummary.Text)
            Me.LblTabSetMaxBudget.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),LblTabSetMaxBudget.ID, Me.LblTabSetMaxBudget.Text)
            Me.LblBudgetTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),LblBudgetTrainingCalendar.ID, Me.LblBudgetTrainingCalendar.Text)
            Me.lblLastBudgetGrandTotalRequested.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblLastBudgetGrandTotalRequested.ID, Me.lblLastBudgetGrandTotalRequested.Text)
            Me.lblLastBudgetGrandTotalApproved.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblLastBudgetGrandTotalApproved.ID, Me.lblLastBudgetGrandTotalApproved.Text)
            Me.lblLastBudgetGrandTotalEnrolled.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblLastBudgetGrandTotalEnrolled.ID, Me.lblLastBudgetGrandTotalEnrolled.Text)
            Me.lblThisBudgetGrandTotalRequested.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblThisBudgetGrandTotalRequested.ID, Me.lblThisBudgetGrandTotalRequested.Text)
            Me.lblThisBudgetGrandTotalApproved.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblThisBudgetGrandTotalApproved.ID, Me.lblThisBudgetGrandTotalApproved.Text)
            Me.lblThisBudgetGrandTotalEnrolled.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblThisBudgetGrandTotalEnrolled.ID, Me.lblThisBudgetGrandTotalEnrolled.Text)

            Me.LblSetMaxTrainingCalender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),LblSetMaxTrainingCalender.ID, Me.LblSetMaxTrainingCalender.Text)
            Me.btnSaveSetMaxBudget.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnSaveSetMaxBudget.ID, Me.btnSaveSetMaxBudget.Text).Replace("&", "")
            Me.btnCloseSetMaxBudget.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnCloseSetMaxBudget.ID, Me.btnCloseSetMaxBudget.Text).Replace("&", "")

            GvSetMaxBudget.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvSetMaxBudget.Columns(0).FooterText, GvSetMaxBudget.Columns(0).HeaderText)
            GvSetMaxBudget.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvSetMaxBudget.Columns(1).FooterText, GvSetMaxBudget.Columns(1).HeaderText)
            GvSetMaxBudget.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),GvSetMaxBudget.Columns(2).FooterText, GvSetMaxBudget.Columns(2).HeaderText)

            popupDeleteDTNeed.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 79, "Are you sure you want to delete the selected departmental training need(s)?")
            popupActiveDTNeed.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 80, "Are you sure you want to make departmental training need active?")
            popupInactiveDTNeed.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 81, "Are you sure you want to make departmental training need inactive?")
            popupDeleteInstruct.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 83, "Are you sure you want to delete selected instructor?")
            popupUnlockSubmitted.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 94, "Unlock Remark")
            popupFinalApprove.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 95, "Approval Remark")
            popupFinalReject.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 96, "Rejection Remark")
            popupUnlockSubmitted.ErrorMessage = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 97, "Please enter Unlock Remark")
            popupFinalApprove.ErrorMessage = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 98, "Please enter Approval Remark")
            popupFinalReject.ErrorMessage = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameList, 99, "Please enter Rejection Remark")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Please select #department#.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Please select Training Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Please select Area of Development.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Please select the Training needed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "Please enter Area of Development..")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "Please enter training needed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "Please select training need category.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 9, "Please enter start date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 10, "Please enter end date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 11, "Sorry, the Start date should be in between the start date and end date of the selected period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 12, "Sorry, the End date should be in between the start date and end date of the selected period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 13, "Sorry, the Start date should not be later than the End date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 14, "Please select priority.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 17, "Please enter the Number of Staff.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 18, "Please select at least one employee or allocation from the targeted group.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 19, "Sorry, selected employees should be equal to the number of staff indicated above.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 21, "Sorry, selected employees should be less or equal to the number of staff indicated above.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 22, "Sorry, Training Cost is mandatory. Please enter the training cost.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 23, "Please add cost amount for all the selected cost items.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 24, "Sorry, approved amount should not exceed the cost amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 25, "Please add approved cost amount for all the selected cost items.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 26, "Please add cost amount for added cost items.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 27, "Sorry, you cannot unlock training(s) submitted for approval if the training(s) were directly added into the company training backlog. Please use the delete option to remove such training(s) from the list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 28, "Please select at least one item from the target group for this training and Save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 29, "Please add cost amount for the selected cost items.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 30, "Sorry, Total Requested amount should not exceed the maximum budgetary allocation for the selected department.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 31, "Department : #deptname#")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 32, "Approved Total : #approvedtotal#")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 33, "Requested Total : #requestedtotal#")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 34, "Sub Total : #subtotal#")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 35, "Maximum Budget : #maxbudget#")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 36, "Sorry, This Departmental Training Need is already Submitted for Approval.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 38, "Please select at least one item from the target group.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 39, "Please set the start date and end date for all the selected trainings.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 40, "Please tick at least one training to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 41, "Selected training(s) Submitted for Approval Successfully!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 42, "Sorry, you cannot submit the selected training(s) for budget approval. Submission deadline was #calendarenddate#.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 44, "Selected training(s) Submitted for Budget Approval Successfully!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 46, "Selected training(s) Approved Successfully!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 49, "Selected training(s) which had been submitted for budget approval have been Unlocked Successfully to allow for changes!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 50, "Selected training(s) which had been submitted for backlog approval have been Unlocked Successfully!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 52, "Selected training(s) which had been submitted for budget approval have been sent back for amount review Successfully!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 54, "Sorry, you cannot change the status of the approved training(s). Training Requests are already submitted for the selected training(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 55, "Status for the selected training(s) has been changed from Approved to Pending!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 57, "Status for the selected training(s) has been changed from Rejected to Pending!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 58, "Sorry, This Departmental Training Need is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 59, "Sorry, This Departmental Training Need is already approved at the final level.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 61, "Sorry, This Departmental Training Need is still in pending status.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleSetMaxBudget, 66, "Please set maximum budget for at least one allocation.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleSetMaxBudget, 67, "Sorry, Maximum budget cannot be less than the total approved amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 68, "Training Backlog")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 69, "Training Backlog")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 70, "Training Budget Approval")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 71, "Training Budget Approval")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 72, "Departmental Training Needs")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 73, "Departmental Training Needs")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 74, "Cost to be approved")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 75, "Total Approved Cost")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 76, " Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 77, "Please select #department# to continue.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 78, "Wrongly Posted.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 79, "Are you sure you want to delete the selected departmental training need(s)?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 80, "Are you sure you want to make departmental training need active?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 81, "Are you sure you want to make departmental training need inactive?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 82, "Company")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 83, "Are you sure you want to delete selected instructor?")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 84, "Please tick atlease one training instructor from the list.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 85, "Please enter other instructor name.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 86, "Please enter other instructor company name.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 87, "Please enter other instructor department name.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 88, "Please enter other instructor job name.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 89, "Please enter other instructor email.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 90, "Please Enter Valid Email.")
			Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 91, "Sorry, Instructor name and email are already exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 92, "The targeted numbers exceed the maximum capacity limit of the selected venue, do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 93, "Are you sure you want to Final Approve selected trainings?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 94, "Unlock Remark")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 95, "Approval Remark")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 96, "Rejection Remark")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 97, "Please enter Unlock Remark")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameList, 98, "Please enter Approval Remark")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleNameList, 99, "Please enter Rejection Remark")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleSetMaxBudget, 100, "wise Budget Summary")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 419, "Classes")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 420, "Class Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 421, "Jobs")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 422, "Job Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 423, "Team")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 424, "Unit")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 425, "Unit Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 426, "Section")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 427, "Section Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 428, "Department")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 429, "Department Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 430, "Branch")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
