﻿<%@ Page Title="Departmental Training Needs" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_DepartmentalTrainingNeeds.aspx.vb" Inherits="Training_Departmental_Training_Needs_wPg_DepartmentalTrainingNeeds" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/NumberOnly.ascx" TagName="NumberOnly" TagPrefix="num" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .table-bordered tbody tr td
        {
            vertical-align: top;
        }
    </style>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveCollapse();
            RetriveTab();
            CalcGrandTotal();

            $('input[id*=txtAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemAmtChanged').val('1');
            });
            $('input[id*=txtApprovedAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemApprovedAmtChanged').val('1');
            });

            $('.filtercontent').on('click', function() {
                $("[id*=hfFilterContentHidden]").val($('#filtercontent').is(':hidden'));
            });

            //            $('a[id*=txtAmount_lnkup]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });

            //            $('a[id*=txtAmount_lnkdown]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });
            $('#columnlist a').on('click', function(event) {
                $(this).parent().toggleClass('open');
            });

            $('body').on('click', function(e) {
                if (!$('#columnlist').is(e.target)
                && $('#columnlist').has(e.target).length === 0
                && $('.open').has(e.target).length === 0
                ) {
                    $('#columnlist').removeClass('open');
                }
            });
            
            if ($$('chkOtherInstructor').prop('checked') == true)
                $('#txtSearchTInstructor').prop('disabled', true);
            else
                $('#txtSearchTInstructor').prop('disabled', false);
        }
    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
            <div class="block-header">
                <h2>
                </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Departmental Training Need List"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblDepartmentList" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList data-live-search="true" ID="cboDepartmentList" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<asp:Panel ID="pnlNoEmployeeSelected" runat="server" CssClass="footer text-left">
                                <asp:Label ID="lblNoEmployeeSelected" CssClass="label label-info" Text="Please select employee to continue"
                                    runat="server" />
                            </asp:Panel>--%>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlData" runat="server" CssClass="row clearfix" Visible="false">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblPageHeader1" runat="server" Text="Filter" style="float:left;padding-right:10px;"></asp:Label>
                                        <asp:Panel id="pnlFilterTitle" runat="server" style="font-size: small;color:Gray;" ></asp:Panel>                                        
                                    </h2>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown" style="display: none;">
                                            <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                                <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </li>
                                        <a role="button" data-toggle="collapse" class="filtercontent" href="#filtercontent"
                                            aria-expanded="false" aria-controls="filtercontent"><i class="fas fa-angle-down">
                                            </i></a>
                                    </ul>
                                </div>
                                <div class="body panel-collapse collapse" id="filtercontent" role="tabpanel" aria-labelledby="filtercontent">
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblTargetedGroupList" Text="Targeted Group" runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboTargetedGroupList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="objlblEmployeeNameList" Text="Employee Name" runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboEmployeeNameList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPeriodList" Text="Training Period" runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboPeriodList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPriorityList" runat="server" Text="Training Need Priority" CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboPriorityList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblTrainingName" Text="Training Needed / Development Required" runat="server"
                                                CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboTrainingName" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblTrainingCategoryList" Text="Training Need Category" runat="server"
                                                CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboTrainingCategoryList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12  col-xs-12  m-t-20">
                                            <div class="form-group ">
                                                <asp:DropDownList ID="cboConditionList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblTotalCostList" Text="Total Cost" runat="server" CssClass="form-label" />
                                            <nut:NumericText ID="txtTotalCostList" runat="server" Type="Point" Style="text-align: right"
                                                CssClass="form-control" AutoPostBack="true" />
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblStatusList" Text="Status" runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboStatusList" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblRefnoList" Text="Ref. No." runat="server" CssClass="form-label" />
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtRefnoList" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <div class="pull-left">
                                    </div>
                                    <asp:Button ID="btnAddList" CssClass="btn btn-primary" runat="server" Text="Add New" />
                                    <asp:Button ID="btnSearchList" runat="server" Text="Search" CssClass="btn btn-default"
                                        Visible="false" />
                                    <asp:Button ID="btnRestList" runat="server" Text="Reset" CssClass="btn btn-default" />
                                    <asp:Button ID="btnCloseList" CssClass="btn btn-default" runat="server" Text="Close" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card ">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblHeader" runat="server" Text="Departmental Training Needs"></asp:Label>
                                    </h2>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown" style="padding-right: 60px;">
                                            <asp:Label ID="lblViewList" Text="" runat="server" CssClass="form-label pull-left" />
                                            <asp:RadioButton ID="rdbSummary" runat="server" AutoPostBack="true" CssClass="with-gap"
                                                GroupName="View" Text="Summary" Checked="true" Visible="false" />
                                            <asp:RadioButton ID="rdbDetailedList" runat="server" AutoPostBack="true" CssClass="with-gap"
                                                GroupName="View" Text="Detailed" Visible="false" />
                                            <asp:RadioButton ID="rdbGrouped" runat="server" AutoPostBack="true" CssClass="with-gap"
                                                GroupName="View" Text="Grouped" Visible="false" />
                                            <asp:LinkButton ID="lnkBudgetSummary" runat="server"><i class="fa fa-th-list" style="margin-right:5px;"></i><asp:Label ID="lblBudgetSummary"  runat="server" Text="Open Budget Summary"></asp:Label></asp:LinkButton>
                                        </li>
                                        <li class="dropdown" style="padding-right: 60px;">
                                            <asp:Label ID="lblGrandTotalList" runat="server" Font-Bold="true" Text="Grand Total : "></asp:Label>
                                            <asp:Label ID="objGrandTotalList" runat="server" Text="0.00"></asp:Label>
                                            <asp:Label ID="lblApprovedGrandTotalList" runat="server" Font-Bold="true" Text="Approved Grand Total : "></asp:Label>
                                            <asp:Label ID="objApprovedGrandTotalList" runat="server" Text="0.00"></asp:Label>
                                        </li>
                                        <li class="dropdown" id="columnlist">
                                            <%--<asp:LinkButton ID="btnOperation" runat="server" Text="Columns" class="dropdown-toggle"
                                             aria-haspopup="true" aria-expanded="false">
                                            Columns<span class="caret"></span>
                                        </asp:LinkButton>--%>
                                            <a href="javascript:;" class="dropdown-toggle"><i class="fa fa-list-alt" style="margin-right: 5px;">
                                            </i>Columns <span class="fa fa-chevron-down pull-right" style="padding-top: 8px;
                                                padding-left: 5px;"></span></a>
                                            <ul class="dropdown-menu pull-right" style="padding-left: 10px;">
                                                <li style="padding: 10px;"><a href="javascript:;" style="cursor: default;" onclick="return false;">
                                                    <h2>
                                                        <asp:Label ID="lblColumnList" runat="server" Text="Column List" ></asp:Label> 
                                                    </h2>
                                                </a></li>
                                                <li role="separator" class="divider_"></li>
                                                <li>
                                                    <asp:CheckBoxList ID="chkColumns" runat="server">
                                                    </asp:CheckBoxList>
                                                </li>
                                                <li role="separator" class="divider_"></li>
                                                <li>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Button ID="btnSaveColumns" runat="server" CssClass="btn btn-primary" Text="Save" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Button ID="btnCloseColumns" runat="server" CssClass="btn btn-default" Text="Close"
                                                            OnClientClick="$('#columnlist').removeClass('open'); return false;" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 400px">
                                                <asp:GridView ID="gvDeptTrainingeedList" runat="server" AutoGenerateColumns="false"
                                                    CssClass="table table-hover table-bordered" AllowPaging="false" AllowSorting="true"
                                                    DataKeyNames="departmentaltrainingneedunkid, allocationid, departmentunkid, periodunkid, targetedgroupunkid, startdate, enddate, statusunkid, request_statusunkid,  moduleid, moduletranunkid, insertformid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhactive" HeaderText=""
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkActive" runat="server" CssClass="chk-sm" Checked='<%# Eval("isactive")%>'
                                                                    Text=" " AutoPostBack="true" OnCheckedChanged="chkActive_CheckedChanged" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhselect" HeaderText=""
                                                            ItemStyle-Width="2%">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'chkSelect');" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chk-sm" Text=" " onclick="SelectAllChildren(this);" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit" HeaderText="Edit"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkedit" runat="server" ToolTip="Edit" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Change">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" HeaderText="Delete"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkdelete" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Remove">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ref. No." FooterText="colhrefno" SortExpression="trainingcoursename">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblrefno" runat="server" Text='<%# Eval("refno")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Training Name" FooterText="colhtrainingcoursename"
                                                            SortExpression="trainingcoursename">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingcourse" runat="server" Text="Training Name"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingcourse" runat="server" Text='<%# Eval("trainingcoursename")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingcourse" HeaderText="Training Name" ReadOnly="True"
                                                    FooterText="trainingcourse" SortExpression="trainingcourse" />--%>
                                                        <asp:TemplateField HeaderText="Competences / Skills / Knowledge Need" FooterText="colhcompetencename"
                                                            SortExpression="competencename">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingcompetence" runat="server" Text="Competences / Skills / Knowledge Need"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingcompetence" runat="server" Text='<%# Eval("competencename")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingcompetence" HeaderText="Competences / Skills / Knowledge Need"
                                                    ReadOnly="True" FooterText="colhtrainingcompetence" SortExpression="trainingcompetence" />--%>
                                                        <asp:TemplateField HeaderText="Training Need Category" FooterText="colhtrainingcategoryname"
                                                            SortExpression="trainingcategoryname">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingcategory" runat="server" Text="Training Need Category"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingcategory" runat="server" Text='<%# Eval("trainingcategoryname")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingcategory" HeaderText="Training Need Category"
                                                    ReadOnly="True" FooterText="colhtrainingcategory" SortExpression="trainingcategory" />--%>
                                                        <asp:TemplateField HeaderText="Learning Method" FooterText="colhlearningmethodname"
                                                            SortExpression="learningmethodname" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnklearningmethod" runat="server" Text="Learning Method"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbllearningmethod" runat="server" Text='<%# Eval("learningmethodname")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="learningmethod" HeaderText="Learning Method" ReadOnly="True"
                                                    FooterText="colhlearningmethod" SortExpression="learningmethod" />--%>
                                                        <asp:TemplateField HeaderText="No. of Staff" FooterText="colhnoofstaff" SortExpression="noofstaff">
                                                            <%-- <HeaderTemplate>
                                                        <asp:LinkButton ID="lnknoofstaff" runat="server" Text="No. of Staff"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnoofstaff" runat="server" Text='<%# Eval("noofstaff")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="noofstaff" HeaderText="No. of Staff" ReadOnly="True" FooterText="noofstaff"
                                                    SortExpression="noofstaff" />--%>
                                                        <asp:TemplateField HeaderText="Targeted Group" FooterText="colhallocationtranname"
                                                            SortExpression="allocationtranname">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkstartdate" runat="server" Text="Start Date"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltargetedgroup" runat="server" Text='<%# Eval("allocationtranname")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="targetedgroup" HeaderText="Targeted Group" ReadOnly="True"
                                                    FooterText="colhtargetedgroup" SortExpression="targetedgroup" />--%>
                                                        <asp:TemplateField HeaderText="Start Date" FooterText="colhstartdate" SortExpression="startdate">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkstartdate" runat="server" Text="Start Date"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblstartdate" runat="server" Text='<%# Eval("startdate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="startdate" HeaderText="Start Date" ReadOnly="True" FooterText="colhstartdate"
                                                    SortExpression="startdate" />--%>
                                                        <asp:TemplateField HeaderText="End Date" FooterText="colhenddate" SortExpression="enddate">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkenddate" runat="server" Text="End Date"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblenddate" runat="server" Text='<%# Eval("enddate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="enddate" HeaderText="End Date" ReadOnly="True" FooterText="colhenddate"
                                                    SortExpression="enddate" />--%>
                                                        <asp:TemplateField HeaderText="Priority" FooterText="colhtrainingpriorityname" SortExpression="trainingpriorityname">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkpriority" runat="server" Text="Priority"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpriority" runat="server" Text='<%# Eval("trainingpriorityname")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="priority" HeaderText="Priority" ReadOnly="True" FooterText="colhpriority"
                                                    SortExpression="priority" />--%>
                                                        <asp:TemplateField HeaderText="Training Provider" FooterText="colhtrainingprovidername"
                                                            SortExpression="trainingprovidername" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingprovider" runat="server" Text="Training Provider"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingprovider" runat="server" Text='<%# Eval("trainingprovidername")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingprovider" HeaderText="Training Provider" ReadOnly="True"
                                                    FooterText="colhtrainingprovider" Visible="false" SortExpression="trainingprovider" />--%>
                                                        <asp:TemplateField HeaderText="Training Venue" FooterText="colhtrainingvenuename"
                                                            SortExpression="trainingvenuename" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingvenue" runat="server" Text="Training Venue"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingvenue" runat="server" Text='<%# Eval("trainingvenuename")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingvenue" HeaderText="Training Venue" ReadOnly="True"
                                                    FooterText="colhtrainingvenue" Visible="false" SortExpression="trainingvenue" />--%>
                                                        <asp:TemplateField HeaderText="Training Resources Needed" FooterText="colhtrainingresourcename"
                                                            SortExpression="trainingresourcename" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingresourcesneeded" runat="server" Text="Training Resources Needed"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingresourcesneeded" runat="server" Text='<%# Eval("trainingresourcename")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingresourcesneeded" HeaderText="Training Resources Needed"
                                                    ReadOnly="True" FooterText="colhtrainingresourcesneeded" Visible="false" SortExpression="trainingresourcesneeded" />--%>
                                                        <asp:TemplateField HeaderText="Financing Source" FooterText="colhfinancingsourcename"
                                                            SortExpression="financingsourcename" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkfinancingsource" runat="server" Text="Financing Source"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblfinancingsource" runat="server" Text='<%# Eval("financingsourcename")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="financingsource" HeaderText="Financing Source" ReadOnly="True"
                                                    FooterText="colhfinancingsource" Visible="false" SortExpression="financingsource" />--%>
                                                        <asp:TemplateField HeaderText="Training Coordinator" FooterText="colhcoordinatorname"
                                                            SortExpression="coordinatorname" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingcoordinator" runat="server" Text="Training Coordinator"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingcoordinator" runat="server" Text='<%# Eval("coordinatorname")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingcoordinator" HeaderText="Training Coordinator"
                                                    ReadOnly="True" FooterText="colhtrainingcoordinator" Visible="false" SortExpression="trainingcoordinator" />--%>
                                                        <asp:TemplateField HeaderText="Training Certificate Required" FooterText="colhiscertirequired"
                                                            SortExpression="iscertirequired" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingcertirequired" runat="server" Text="Training Certificate Required"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingcertirequired" runat="server" Text='<%# Eval("iscertirequired")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingcertirequired" HeaderText="Training Certificate Required"
                                                    ReadOnly="True" FooterText="colhtrainingcertirequired" Visible="false" SortExpression="trainingcertirequired" />--%>
                                                        <asp:TemplateField HeaderText="Comment / Remark" FooterText="colhremark" SortExpression="remark"
                                                            Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkcommentremark" runat="server" Text="Comment / Remark"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcommentremark" runat="server" Text='<%# Eval("remark")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="commentremark" HeaderText="Comment / Remark" ReadOnly="True"
                                                    FooterText="colhcommentremark" Visible="false" SortExpression="commentremark" />--%>
                                                        <asp:TemplateField HeaderText="Training Cost Item" FooterText="colhinfo_name" SortExpression="info_name"
                                                            Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktrainingcostitem" runat="server" Text="Training Cost Item"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltrainingcostitem" runat="server" Text='<%# Eval("info_name")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="trainingcostitem" HeaderText="Training Cost Item" ReadOnly="True"
                                                    FooterText="colhtrainingcostitem" Visible="false" SortExpression="trainingcostitem" />--%>
                                                        <asp:TemplateField HeaderText="Amount" FooterText="colhamount" SortExpression="amount"
                                                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" Visible="false">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnkamount" runat="server" Text="Amount"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblamount" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" FooterText="colhamount"
                                                    Visible="false" SortExpression="amount" />--%>
                                                        <asp:TemplateField HeaderText="Total Cost" FooterText="colhtotaltrainingcost" SortExpression="totalcost"
                                                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                            <%--<HeaderTemplate>
                                                        <asp:LinkButton ID="lnktotaltrainingcost" runat="server" Text="Total Training Cost"></asp:LinkButton>
                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotaltrainingcost" runat="server" Text='<%# Eval("totalcost") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Total Approved Cost" FooterText="colhtotalapprovedtrainingcost"
                                                            SortExpression="approved_totalcost" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotalapprovedtrainingcost" runat="server" Text='<%# Eval("approved_totalcost") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="totaltrainingcost" HeaderText="Total Training Cost" ReadOnly="True"
                                                    FooterText="colhtotaltrainingcost" SortExpression="totaltrainingcost" />--%>
                                                        <asp:TemplateField HeaderText="Status" FooterText="colhstatus" SortExpression="statusname">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblstatusname" runat="server" Text='<%# Eval("statusname")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <div class="pull-left">
                                    </div>
                                    <asp:Button ID="btnSubmitForApprovalFromDeptTNeed" CssClass="btn btn-primary" runat="server"
                                        Text="Submit For Approval" />
                                    <%--<asp:Button ID="btnTentativeApprove" CssClass="btn btn-primary" runat="server" Text="Approve"
                                        Visible="false" />--%>
                                    <asp:Button ID="btnSubmitForApprovalFromTBacklog" CssClass="btn btn-primary" runat="server"
                                        Text="Submit For Budget Approval" Visible="false" />
                                    <asp:Button ID="btnFinalApprove" CssClass="btn btn-primary" runat="server" Text="Approve"
                                        Visible="false" />
                                    <asp:Button ID="btnReject" CssClass="btn btn-default" runat="server" Text="Reject"
                                        Visible="false" />
                                    <asp:Button ID="btnAskForReview" CssClass="btn btn-default" runat="server" Text="Ask For Review As Per Amount Set"
                                        Visible="false" />
                                    <asp:Button ID="btnUnlockSubmitApproval" CssClass="btn btn-default" runat="server"
                                        Text="Unlock Submitted for Approval" Visible="false" />
                                    <asp:Button ID="btnUndoApproved" CssClass="btn btn-primary" runat="server" Text="Undo Approved Budget"
                                        Visible="false" />
                                    <asp:Button ID="btnUndoRejected" CssClass="btn btn-primary" runat="server" Text="Undo Rejected Budget"
                                        Visible="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popAddEdit" BackgroundCssClass="modal-backdrop" TargetControlID="hfAddEdit"
                runat="server" CancelControlID="hfAddEdit" PopupControlID="pnlAddEdit">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddEdit" runat="server" CssClass="card modal-dialog modal-xl" Style="display: none;">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblAddEditDepartmentalTrainingNeed" Text="Add / Edit Departmental Training Need"
                                    runat="server" />
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="body" style="height: 77vh">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#TrainingInfo" data-toggle="tab">
                                        <asp:Label ID="lblTrainingInfo" runat="server" Text="Training Info"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TargetGroup" data-toggle="tab">
                                        <asp:Label ID="lblTargetGroupInfo" runat="server" Text="Target Group Info"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingResources" data-toggle="tab">
                                        <asp:Label ID="lblTrainingResources" runat="server" Text="Training Resources"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#FinancingSource" data-toggle="tab">
                                        <asp:Label ID="lblFinancingSource" runat="server" Text="Financing Source"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingCoordinator" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCoordinator" runat="server" Text="Training Coordinator"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingCostItem" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCostItem" runat="server" Text="Training Cost Item"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingInstructor" data-toggle="tab">
                                        <asp:Label ID="lblTrainingInstructor" runat="server" Text="Training Instructor"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="TrainingInfo">
                                        <asp:Panel ID="pnlTrainingInfo" runat="server">
                                            <div class="row clearfix d--f" style="align-items: flex-end">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblPeriod" Text="Training Period" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <div class="row margin-0 padding-0">
                                                        <div class="col-md-8 margin-0 padding-0">
                                                            <asp:Label ID="lblCompetences" Text="Competences/Skills/Knowledge Need/Area of Development"
                                                                runat="server" CssClass="form-label" />
                                                        </div>
                                                        <div class="col-md-4 margin-0 padding-0" style="text-align: right">
                                                            <asp:CheckBox ID="chkOtherCompetences" runat="server" Text="Others" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="row margin-0 padding-0">
                                                        <div class="col-md-12 margin-0 padding-0">
                                                            <asp:Panel ID="pnlCompetence" runat="server">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboCompetences" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlOtherCompetence" runat="server" Visible="false">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOtherCompetence" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <div class="row margin-0 padding-0">
                                                        <div class="col-md-8 margin-0 padding-0">
                                                            <asp:Label ID="lblTrainingNeeded" Text="Training Needed / Development Required" runat="server"
                                                                CssClass="form-label" />
                                                        </div>
                                                        <div class="col-md-4 margin-0 padding-0" style="text-align: right">
                                                            <asp:CheckBox ID="chkOtherTrainingCource" runat="server" Text="Others" AutoPostBack="true"
                                                                Visible="false" />
                                                        </div>
                                                    </div>
                                                    <div class="row margin-0 padding-0">
                                                        <div class="col-md-12 margin-0 padding-0">
                                                            <asp:Panel ID="pnlTCourse" runat="server">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrainingNeeded" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlOtherTCourse" runat="server" Visible="false">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOtherTCourse" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblTrainingCategory" Text="Training Need Category" runat="server"
                                                        CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboTrainingCategory" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblPriority" runat="server" Text="Training Need Priority" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboPriority" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLearningMethod" Text="Learning Method" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboLearningMethod" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblStartDate" Text="Start Date" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <uc2:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEndDate" Text="End Date" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <uc2:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblTrainingProvider" Text="Training Provider" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboTrainingProvider" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblTrainingVenue" Text="Training Venue" runat="server" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboTrainingVenue" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-35">
                                                    <asp:CheckBox ID="chkCertiRequired" runat="server" Text="Training Certificate Required" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblCommentRemark" runat="server" Text="Comments/Remarks" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtCommentRemark" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                Rows="3"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--fe">
                                                    <i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TargetGroup');">
                                                    </i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TargetGroup">
                                        <asp:Panel ID="pnlTargetGroup" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblTargetedGroup" Text="Targeted Group" runat="server" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboTargetedGroup" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblNoOfStaff" Text="Number of Staff(s)" runat="server" CssClass="form-label" />
                                                                    <nut:NumericText ID="txtNoOfStaff" runat="server" CssClass="form-control" />
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-35">
                                                                    <asp:CheckBox ID="chkChooseEmployee" runat="server" Text="Choose Employee(s)" CssClass="chk-sm"
                                                                        AutoPostBack="true" Visible="false" />
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="body p-t-0">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <input type="text" id="txtSearchEmp" name="txtSearchEmp" placeholder="Search Targeted Group"
                                                                                                    maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchEmp', '<%= dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'ChkgvSelectTargetedGroup');" />
                                                                                                <%--<asp:TextBox id="txtSearchEmp" runat="server" placeholder="Search" MaxLength="50" CssClass="form-control" onkeyup="FromSearching();" ></asp:TextBox>--%>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                                        <asp:CheckBox ID="chkEmpOnlyTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                                            CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchEmp', '<% dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'ChkgvSelectTargetedGroup');" />
                                                                                        <%--<asp:LinkButton runat="server" ID="lnkAddEmployee" CssClass="m-r-10" ToolTip="Add Targeted Group"
                                                                                            Visible="false">
                                                                                      <i class="fas fa-plus-circle" ></i>
                                                                                        </asp:LinkButton>--%>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 34vh;">
                                                                                            <asp:GridView ID="dgvEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                                                DataKeyNames="id" CssClass="table table-hover table-bordered">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                        HeaderStyle-CssClass="align-center">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="ChkAllTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                                                AutoPostBack="true" OnCheckedChanged="ChkAllTargetedGroup_CheckedChanged" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="ChkgvSelectTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                                                Checked='<%# Eval("IsChecked")%>' AutoPostBack="true" OnCheckedChanged="ChkgvSelectTargetedGroup_CheckedChanged" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle />
                                                                                                        <ItemStyle />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                                    <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                                    <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                                        <HeaderTemplate>
                                                                                                            <span class="gridiconbc">
                                                                                                                <asp:LinkButton ID="lnkDeleteAllEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                    CommandName="RemoveAll">
                                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                                                </asp:LinkButton>
                                                                                                            </span>
                                                                                                        </HeaderTemplate>
                                                                                                        <%--<ItemTemplate>
                                                                                                        <asp:LinkButton ID="DeleteEmp" runat="server" ToolTip="Remove" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" >
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>--%>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="pnlAllocEmp" runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="body p-t-0">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <input type="text" id="txtSearchAllocEmp" name="txtSearchAllocEmp" placeholder="Search Employee"
                                                                                                    maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchAllocEmp', '<%= dgvAllocEmp.ClientID %>');" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                                        <asp:CheckBox ID="chkAllocEmpOnlyTicked" runat="server" Text="Only Ticked Items"
                                                                                            ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchAllocEmp', '<% dgvAllocEmp.ClientID %>', 'chkAllocEmpOnlyTicked', 'ChkgvSelectAllocEmp');" />
                                                                                        <%--<asp:LinkButton runat="server" ID="lnkAddAllocEmp" CssClass="m-r-10" ToolTip="Add Employees"
                                                                                            Visible="false">
                                                                                      <i class="fas fa-plus-circle" ></i>
                                                                                        </asp:LinkButton>--%>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 34vh;">
                                                                                            <asp:GridView ID="dgvAllocEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                                                DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                        HeaderStyle-CssClass="align-center">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="ChkAllAllocEmp" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAllocEmp');" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="ChkgvSelectAllocEmp" runat="server" Text=" " CssClass="chk-sm"
                                                                                                                Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this, 'ChkgvSelectAllocEmp', 'ChkAllAllocEmp')" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle />
                                                                                                        <ItemStyle />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                                    <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                                    <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                                        <HeaderTemplate>
                                                                                                            <span class="gridiconbc">
                                                                                                                <asp:LinkButton ID="lnkDeleteAllAllocEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                    CommandName="RemoveAll">
                                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                                                </asp:LinkButton>
                                                                                                            </span>
                                                                                                        </HeaderTemplate>
                                                                                                        <%--<ItemTemplate>
                                                                                                        <asp:LinkButton ID="DeleteEmp" runat="server" ToolTip="Remove" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" >
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>--%>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingInfo');">
                                                    </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingResources');">
                                                    </i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingResources">
                                        <asp:Panel ID="pnlTrainingResources" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="text" id="txtSearchTrainingResources" name="txtSearchTrainingResources"
                                                                                placeholder="Search Training Resources" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTrainingResources', '<%= dgvTrainingResources.ClientID %>', 'chkTResourcesOnlyTicked', 'ChkgvSelectTResources');" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkTResourcesOnlyTicked" runat="server" Text="Only Ticked Items"
                                                                        ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTrainingResources', '<% dgvTrainingResources.ClientID %>', 'chkTResourcesOnlyTicked', 'ChkgvSelectTResources');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddTrainingResources" CssClass="m-r-10" ToolTip="Add Training Resources"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 49vh;">
                                                                        <asp:GridView ID="dgvTrainingResources" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="masterunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkAllTResources" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTResources');" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelectTResources" runat="server" Text=" " CssClass="chk-sm"
                                                                                            Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this, 'ChkgvSelectTResources', 'ChkAllAddTResources')" />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle />
                                                                                    <ItemStyle />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkDeleteAllTResource" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </HeaderTemplate>
                                                                                    <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteTResource" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TargetGroup');">
                                                    </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('FinancingSource');">
                                                    </i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="FinancingSource">
                                        <asp:Panel ID="pnlFinancingSources" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="text" id="txtSearchFinancingSource" name="txtSearchFinancingSource"
                                                                                placeholder="Search Financing Source" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchFinancingSource', '<%= dgvFinancingSource.ClientID %>', 'chkFSourceOnlyTicked', 'ChkgvSelectFSource');" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkFSourceOnlyTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                        CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchFinancingSource', '<% dgvFinancingSource.ClientID %>', 'chkFSourceOnlyTicked', 'ChkgvSelectFSource');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddFinancingSource" CssClass="m-r-10" ToolTip="Add Financing Source"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 49vh;">
                                                                        <asp:GridView ID="dgvFinancingSource" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="masterunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkAllFSource" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectFSource');" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelectFSource" runat="server" Text=" " CssClass="chk-sm" onclick="ChkSelect(this, 'ChkgvSelectFSource', 'ChkAllFSource')"
                                                                                            Checked='<%# Eval("IsChecked")%>' />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle />
                                                                                    <ItemStyle />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkDeleteAllFSource" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </HeaderTemplate>
                                                                                    <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteFSource" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingResources');">
                                                    </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCoordinator');">
                                                    </i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingCoordinator">
                                        <asp:Panel ID="pnlTrainingCoordinator" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="text" id="txtSearchTCoordinator" name="txtSearchTCoordinator" placeholder="Search Training Coordinator"
                                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTCoordinator', '<%= dgvTrainingCoordinator.ClientID %>', 'chkTCoordinatOnlyTicked', 'ChkgvSelectTCoordinator');" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkTCoordinatOnlyTicked" runat="server" Text="Only Ticked Items"
                                                                        ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTCoordinator', '<% dgvTrainingCoordinator.ClientID %>', 'chkTCoordinatOnlyTicked', 'ChkgvSelectTCoordinator');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddTrainingCoordinator" CssClass="m-r-10" ToolTip="Add Training Coordinator"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 49vh;">
                                                                        <asp:GridView ID="dgvTrainingCoordinator" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkAllTCoordinator" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTCoordinator');" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelectTCoordinator" runat="server" Text=" " CssClass="chk-sm"
                                                                                            onclick="ChkSelect(this, 'ChkgvSelectTCoordinator', 'ChkAllTCoordinator')" Checked='<%# Eval("IsChecked")%>' />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle />
                                                                                    <ItemStyle />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkDeleteAllTCoordinator" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </HeaderTemplate>
                                                                                    <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteTCoordinator" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('FinancingSource');">
                                                    </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCostItem');">
                                                    </i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingCostItem">
                                        <asp:Panel ID="pnlTrainingCostItem" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="text" id="txtSearchTrainingCostItem" name="txtSearchTrainingCostItem"
                                                                                placeholder="Search Training Cost Item" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTrainingCostItem', '<%= dgvTrainingCostItem.ClientID %>', 'chkTCostItemTicked', 'ChkgvSelectTCostItem');" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                    <asp:CheckBox ID="chkTCostItemTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                        CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTrainingCostItem', '<% dgvTrainingCostItem.ClientID %>', 'chkTCostItemTicked', 'ChkgvSelectTCostItem');" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAddTrainingCostItem" CssClass="m-r-10" ToolTip="Add Training Cost Item"
                                                                        Visible="false">
                                                                          <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>--%>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 44vh;">
                                                                        <asp:GridView ID="dgvTrainingCostItem" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="infounkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkAllTCostItem" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTCostItem');" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelectTCostItem" runat="server" Text=" " CssClass="chk-sm"
                                                                                            onclick="ChkSelect(this, 'ChkgvSelectTCostItem', 'ChkAllTCostItem')" Checked='<%# Eval("IsChecked")%>' />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle />
                                                                                    <ItemStyle />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="info_code" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                <asp:BoundField DataField="info_name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="align-center"
                                                                                    FooterText="colhamount" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="270px"
                                                                                    ItemStyle-Width="270px">
                                                                                    <ItemTemplate>
                                                                                        <%--<nut:NumericText ID="txtAmount" runat="server" Type="Point" Style="text-align: right"
                                                                                        HideUpDown="true" CssClass="form-control" Text='<%# Eval("amount") %>'  />--%>
                                                                                        <num:NumberOnly ID="txtAmount" runat="server" Text='<%# Eval("amount") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Approved Amount" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center" FooterText="colhapproved_amount" ItemStyle-HorizontalAlign="Right"
                                                                                    HeaderStyle-Width="270px" ItemStyle-Width="270px">
                                                                                    <ItemTemplate>
                                                                                        <num:NumberOnly ID="txtApprovedAmount" runat="server" Text='<%# Eval("approved_amount") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkDeleteAllTCostItem" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </HeaderTemplate>
                                                                                    <%--<ItemTemplate>
                                                                                    <asp:LinkButton ID="DeleteTCostItem" runat="server" ToolTip="Remove">
                                                                                        <i class="fas fa-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>--%>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer" style="padding-right: 60px; display: flex; justify-content: space-between;">
                                                            <asp:CheckBox ID="chkTrainingCostOptional" runat="server" Text="Save without indicating Training Cost" />
                                                            <asp:Label ID="lblCostItemTotal" runat="server" Text="Total Cost : " Font-Bold="true"
                                                                CssClass="align-right"></asp:Label>
                                                            <asp:Label ID="objlblCostItemTotal" runat="server" Text="0.00" Font-Bold="true" CssClass="align-right"></asp:Label>
                                                            <asp:Label ID="lblCostItemApprovedTotal" runat="server" Text="Total Approved Cost : "
                                                                Font-Bold="true" CssClass="align-right"></asp:Label>
                                                            <asp:Label ID="objlblCostItemApprovedTotal" runat="server" Text="0.00" Font-Bold="true"
                                                                CssClass="align-right"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCoordinator');">
                                                    </i><i class="fas fa-long-arrow-alt-right font-32" style="cursor: pointer;" onclick="ShowTab('TrainingInstructor');"></i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingInstructor">
                                        <asp:Panel ID="pnlTrainingInstructor" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <div class="body">
                                                            <div class="row clearfix m-l-0 m-r-0">
                                                                <asp:Panel ID = "pnlInstructor" runat="server" CssClass="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <input type="text" id="txtSearchTInstructor" name="txtSearchTInstructor" placeholder="Search Training Instructor"
                                                                                        maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchTInstructor', '<%= dgvAddTrainingInstructor.ClientID %>', 'chkTInstructorOnlyTicked', 'ChkgvSelectTInstructor');" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-t-15 padding-0">
                                                                            <asp:CheckBox ID="chkTInstructorTicked" runat="server" Text="Only Ticked Items"
                                                                                ToolTip="Show Only Ticked Items" CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchTInstructor', '<% dgvAddTrainingInstructor.ClientID %>', 'chkTInstructorOnlyTicked', 'ChkgvSelectTInstructor');" />
                                                                            <%--<asp:LinkButton runat="server" ID="lnkAddTrainingInstructorr" CssClass="m-r-10" ToolTip="Add Training Instructor"
                                                                                Visible="false">
                                                                                  <i class="fas fa-plus-circle" ></i>
                                                                            </asp:LinkButton>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 49vh;">
                                                                                <asp:GridView ID="dgvAddTrainingInstructor" runat="server" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                            HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="ChkAllAddTInstructor" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddTInstructor');" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="ChkgvSelectAddTInstructor" runat="server" Text=" " CssClass="chk-sm"
                                                                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTInstructor', 'ChkAllAddTInstructor')" Checked='<%# Eval("IsChecked")%>' />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle />
                                                                                            <ItemStyle />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />
                                                                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                        <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <HeaderTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="lnkDeleteAllAddTInstructor" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="RemoveAll">
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                    </asp:LinkButton>
                                                                                                </span>
                                                                                            </HeaderTemplate>
                                                                                            <%--<ItemTemplate>
                                                                                            <asp:LinkButton ID="DeleteAddTInstructor" runat="server" ToolTip="Remove">
                                                                                                <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>--%>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-15 p-l-20">
                                                                            <asp:CheckBox ID="chkOtherInstructor" runat="server" Text="Other Instructor" CssClass="chk-sm"
                                                                            AutoPostBack="true" />
                                                                            </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <asp:Panel ID = "pnlOtherInstructor" runat="server" CssClass="card inner-card m-b-0" Enabled="false">
                                                                                <div class="body p-t-0">
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersName" runat="server" Text="Name" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersCompany" runat="server" Text="Company" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersCompany" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersDept" runat="server" Text="Department" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersDept" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblOthersJob" runat="server" Text="Job" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersJob" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                            <asp:Label ID="lblOthersEmail" runat="server" Text="Email" ></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                    <asp:TextBox ID="txtOthersEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                    <div class="footer">
                                                                        <asp:Button ID="btnAddInstructor" runat="server" Text="Add & Save" CssClass="btn btn-primary" />
                                                                        <asp:Button ID="btnUpdateInstructor" runat="server" Text="Update" CssClass="btn btn-default" />
                                                                        <asp:Button ID="btnResetInstructor" runat="server" Text="Reset" CssClass="btn btn-default" />
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 15vh;">
                                                                                <asp:GridView ID="dgvTrainingInstructor" runat="server" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" DataKeyNames="depttrainingneedtraininginstructortranunkid, employeeunkid" CssClass="table table-hover table-bordered">
                                                                                    <Columns>
                                                                                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                            HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="ChkAllTInstructor" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectTInstructor');" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="ChkgvSelectTInstructor" runat="server" Text=" " CssClass="chk-sm"
                                                                                                    onclick="ChkSelect(this, 'ChkgvSelectTInstructor', 'ChkAllTInstructor')" Checked='<%# Eval("IsChecked")%>' />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle />
                                                                                            <ItemStyle />
                                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnEditInstruct" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Change"><i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnDeleteInstruct" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="Remove"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgcolhEcode" />--%>
                                                                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhInstructName" />
                                                                                        <asp:BoundField DataField="CompanyName" HeaderText="Company" ReadOnly="true" FooterText="dgcolhInstructCompany" />
                                                                                        <asp:BoundField DataField="DeptName" HeaderText="Department" ReadOnly="true" FooterText="dgcolhInstructDepartment" />
                                                                                        <asp:BoundField DataField="JobName" HeaderText="Job" ReadOnly="true" FooterText="dgcolhInstructJob" />
                                                                                        <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="true" FooterText="dgcolhInstructEmail" />
                                                                                        <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <HeaderTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="lnkDeleteAllTInstructor" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                        CommandName="RemoveAll">
                                                                                                    <i class="fas fa-times text-danger"></i>
                                                                                                    </asp:LinkButton>
                                                                                                </span>
                                                                                            </HeaderTemplate>
                                                                                            <%--<ItemTemplate>
                                                                                            <asp:LinkButton ID="DeleteTInstructor" runat="server" ToolTip="Remove">
                                                                                                <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>--%>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f jc--sb">
                                                    <i class="fas fa-long-arrow-alt-left font-32" style="cursor: pointer;" onclick="ShowTab('TrainingCostItem');">
                                                    </i>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-default" />
                    <asp:Button ID="btnSaveAndSubmit" runat="server" Text="Save & Submit for Approval" CssClass="btn btn-primary" />
                    <asp:Button ID="btnClose" CssClass="btn btn-default" runat="server" Text="Close" />
                    <asp:HiddenField ID="hfAddEdit" runat="Server" />
                </div>
            </asp:Panel>
            <%--
            <cc1:ModalPopupExtender ID="popupAddEmployee" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddEmployee" runat="server" CancelControlID="hfAddEmployee"
                PopupControlID="pnlAddEmployee">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddEmployee" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupAddEmployeeheader" Text="Add Targeted Group" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddEmp" name="txtSearchAddEmp" placeholder="Search Targeted Group"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddEmp', '<%= dgvAddEmployee.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="id">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="CheckAll(this, 'ChkgvSelectAddTargetedGroup');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTargetedGroup', 'ChkAllAddTargetedGroup')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSaveAddEmployee" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseEmployee" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddEmployee" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddAllocEmp" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddAllocEmp" runat="server" CancelControlID="hfAddAllocEmp"
                PopupControlID="pnlAddAllocEmp">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddAllocEmp" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblPopupAddAllocEmpheader" Text="Add Employee" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddAllocEmp" name="txtSearchAddAllocEmp" placeholder="Search Employee"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddAllocEmp', '<%= dgvAddAllocEmp.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddAllocEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="employeeunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddAllocEmp" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddAllocEmp');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddAllocEmp" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddAllocEmp', 'ChkAllAddAllocEmp')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSaveAddAllocEmp" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseAllocEmp" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddAllocEmp" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddTResources" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTResources" runat="server" CancelControlID="hfAddTResources"
                PopupControlID="pnlAddTResources">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTResources" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddTResources" Text="Add Training Resources" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTResources" name="txtSearchAddTResources" placeholder="Search Training Resources"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTResources', '<%= dgvAddTResources.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTResources" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTResources" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="CheckAll(this, 'ChkgvSelectAddTResources');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTResources" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTResources', 'ChkAllAddTResources')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddTResources" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTResources" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTResources" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupFinancingSource" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddFSource" runat="server" CancelControlID="hfAddFSource"
                PopupControlID="pnlAddFSource">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddFSource" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddFSource" Text="Add Financing Sources" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddFSource" name="txtSearchAddFSource" placeholder="Search Financing Sources"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddFSource', '<%= dgvAddFinancingSource.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddFinancingSource" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddFSource" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddFSource');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddFSource" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddFSource', 'ChkAllAddFSource')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddFSource" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseFSource" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddFSource" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupAddTrainingCoordinator" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTCoordinator" runat="server" CancelControlID="hfAddTCoordinator"
                PopupControlID="pnlAddTCoordinator">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTCoordinator" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddTCoordinator" Text="Add Training Coordinator" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTCoordinator" name="txtSearchAddTCoordinator"
                                        placeholder="Search Training Coordinator" maxlength="50" class="form-control"
                                        style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTCoordinator', '<%= dgvAddTCoordinator.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTCoordinator" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="employeeunkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTCoordinator" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="CheckAll(this, 'ChkgvSelectAddTCoordinator');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTCoordinator" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTCoordinator', 'ChkAllAddTCoordinator')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddTCoordinator" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTCoordinator" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTCoordinator" runat="Server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupTrainingCostItem" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfAddTCostItem" runat="server" CancelControlID="hfAddTCostItem"
                PopupControlID="pnlAddTCostItem">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAddTCostItem" runat="server" CssClass="card modal-dialog modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblAddTCostItem" Text="Add Training Cost Item" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="txtSearchAddTCostItem" name="txtSearchAddTCostItem" placeholder="Search Training Cost Item"
                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTCostItem', '<%= dgvAddTrainingCostItem.ClientID %>');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="max-height: 415px;">
                                <asp:GridView ID="dgvAddTrainingCostItem" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="infounkid">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="align-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAllAddTCostItem" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAddTCostItem');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelectAddTCostItem" runat="server" Text=" " CssClass="chk-sm"
                                                    onclick="ChkSelect(this, 'ChkgvSelectAddTCostItem', 'ChkAllAddTCostItem')" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="info_code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                        <asp:BoundField DataField="info_name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnAddTCostItem" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Button ID="btnCloseTCostItem" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfAddTCostItem" runat="Server" />
                </div>
            </asp:Panel>
            --%>
            <cc1:ModalPopupExtender ID="popupBudgetSummaryApproval" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfSetMaxBudget" runat="server" CancelControlID="btnCloseSetMaxBudget"
                PopupControlID="pnlBudgetSummary">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlBudgetSummary" runat="server" CssClass="card modal-dialog modal-xl modal-l2"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="LblSummaryHeader" runat="server" Text="Budget Summary"></asp:Label>
                    </h2>
                </div>
                <div class="body" style="height: 77vh;">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card inner-card m-b-0">
                                <div class="body" style="padding: 0px;">
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                        <li role="presentation" class="active"><a href="#BudgetSummary" data-toggle="tab">
                                            <asp:Label ID="LblTabBudgetSummary" runat="server" Text="Budget Summary"></asp:Label>
                                        </a></li>
                                        <li role="presentation"><a href="#SetMaxBudget" data-toggle="tab">
                                            <asp:Label ID="LblTabSetMaxBudget" runat="server" Text="Set Max Budget"></asp:Label>
                                        </a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content p-b-0">
                                        <div role="tabpanel" class="tab-pane fade in active" id="BudgetSummary">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <%--<div class="header">
                                                            <h2>
                                                                <asp:Label ID="LblHeaderBudgtmmary" runat="server" Text="Budget Summary"></asp:Label>
                                                            </h2>
                                                        </div>--%>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="LblBudgetTrainingCalendar" Text="Training Calendar" runat="server"
                                                                        CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboBugdetTrainigCalendar" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix" style="display: flex;justify-content: space-between;">
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <div class="title">
                                                                        <asp:Label ID="lblLastBudgetGrandTotalMaxAmount" runat="server" CssClass="form-label"
                                                                            Text="Last Budget Grand Total Max. Amount"></asp:Label>
                                                                    </div>
                                                                    <div class="content">
                                                                        <asp:Label ID="txtLastBudgetGrandTotalMaxAmount" runat="server" Text="0.00"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblLastBudgetGrandTotalRequested" runat="server" CssClass="form-label"
                                                                        Text="Last Budget Grand Total Requested"></asp:Label>
                                                                    <asp:Label ID="txtLastBudgetGrandTotalRequested" Text="0.00" runat="server" />
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblLastBudgetGrandTotalApproved" runat="server" CssClass="form-label"
                                                                        Text="Last Budget Grand Total Approved"></asp:Label>
                                                                    <asp:Label ID="txtLastBudgetGrandTotalApproved" runat="server" Text="0.00"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <div class="title">
                                                                        <asp:Label ID="lblLastBudgetGrandTotalEnrolled" runat="server" CssClass="form-label"
                                                                            Text="Last Budget Grand Total Enrolled"></asp:Label>
                                                                    </div>
                                                                    <div class="content">
                                                                        <asp:Label ID="txtLastBudgetGrandTotalEnrolled" runat="server" Text="0.00"></asp:Label>
                                                                    </div>
                                                                </div>                                                                
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <div class="title">
                                                                        <asp:Label ID="lblLastBudgetGrandTotalRemainingBalance" runat="server" CssClass="form-label"
                                                                            Text="Last Budget Grand Total Remaining Balance"></asp:Label>
                                                                    </div>
                                                                    <div class="content">
                                                                        <asp:Label ID="txtLastBudgetGrandTotalRemainingBalance" runat="server" Text="0.00"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix" style="display: flex;justify-content: space-between;">
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblThisBudgetGrandTotalMaxAmount" runat="server" CssClass="form-label"
                                                                        Text="This Budget Grand Total Max. Amount"></asp:Label>
                                                                    <asp:Label ID="txtThisBudgetGrandTotalMaxAmount" runat="server" Text="0.00"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblThisBudgetGrandTotalRequested" runat="server" CssClass="form-label"
                                                                        Text="This Budget Grand Total Requested"></asp:Label>
                                                                    <asp:Label ID="txtThisBudgetGrandTotalRequested" Text="" runat="server" />
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblThisBudgetGrandTotalApproved" runat="server" CssClass="form-label"
                                                                        Text="This Budget Grand Total Approved"></asp:Label>
                                                                    <asp:Label ID="txtThisBudgetGrandTotalApproved" runat="server" Text=""></asp:Label>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblThisBudgetGrandTotalEnrolled" runat="server" CssClass="form-label"
                                                                        Text="This Budget Grand Total Enrolled"></asp:Label>
                                                                    <asp:Label ID="txtThisBudgetGrandTotalEnrolled" runat="server" Text=""></asp:Label>
                                                                </div>    
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblThisBudgetGrandTotalRemainingBalance" runat="server" CssClass="form-label"
                                                                        Text="This Budget Grand Total Remaining Balance"></asp:Label>
                                                                    <asp:Label ID="txtThisBudgetGrandTotalRemainingBalance" runat="server" Text="0.00"></asp:Label>
                                                                </div>                                                            
                                                            </div>
                                                            <div class="divider">
                                                            </div>
                                                            <asp:DataList ID="dlBudgetSummaryApprovalDepartment" runat="server" Width="100%">
                                                                <ItemTemplate>
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Panel ID="pnlBudgetSummaryApprovalDepartmentContrainer" runat="server">
                                                                                <div class="panel-group" id="pnlBudgetSummaryApprovalDepartment" role="tablist" aria-multiselectable="true">
                                                                                    <div class="panel" style="border: 1px solid #ddd">
                                                                                        <div class="panel-heading" role="tab" id="BudgetSummaryApprovalDepartmentHeading_<%# Eval("departmentunkid") %>"
                                                                                            style="background: #F5F5F5; padding: 5px 10px;">
                                                                                            <h4 class="panel-title d--f ai--c jc--sb">
                                                                                                <asp:Label ID="lblCategoryName" Text='<%# Eval("allocationtranname") & "&nbsp;-&nbsp;["& Eval("allocationtrancode") &"]" %>' runat="server" />
                                                                                                <a role="button" data-toggle="collapse" class="BudgetSummaryApprovalDepartmentCollapse"
                                                                                                    href="#BudgetSummaryApprovalDepartment_<%# Eval("departmentunkid") %>" aria-expanded="false"
                                                                                                    aria-controls="BudgetSummaryApprovalDepartment_<%# Eval("departmentunkid") %>"><i
                                                                                                        class="fas fa-angle-down"></i></a>
                                                                                                <asp:HiddenField ID="hfcategoryunkid" runat="server" Value='<%# Eval("departmentunkid") %>' />
                                                                                            </h4>
                                                                                            <div class="divider m-t-0">
                                                                                            </div>
                                                                                            <div class="row clearfix" style="display: flex;justify-content: space-between;">
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <div class="title">
                                                                                                        <asp:Label ID="lblLastBudgetMaxAmount" runat="server" CssClass="form-label" Text="Last Budget Max. Amount"></asp:Label>
                                                                                                    </div>
                                                                                                    <div class="content">
                                                                                                        <asp:Label ID="txtLastBudgetMaxAmount" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <asp:Label ID="lblLastBudgetTotalRequested" runat="server" CssClass="form-label"
                                                                                                        Text="Last Budget Total Requested"></asp:Label>
                                                                                                    <asp:Label ID="txtLastBudgetTotalRequested" Text="" runat="server" CssClass="form-label" />
                                                                                                </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <asp:Label ID="lblLastBudgetTotalApproved" runat="server" CssClass="form-label" Text="Last Budget Total Approved"></asp:Label>
                                                                                                    <asp:Label ID="txtLastBudgetTotalApproved" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <div class="title">
                                                                                                        <asp:Label ID="lblLastBudgetTotalEnrolled" runat="server" CssClass="form-label" Text="Last Budget Total Enrolled"></asp:Label>
                                                                                                    </div>
                                                                                                    <div class="content">
                                                                                                        <asp:Label ID="txtLastBudgetTotalEnrolled" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <div class="title">
                                                                                                        <asp:Label ID="lblLastBudgetRemainingBalance" runat="server" CssClass="form-label" Text="Last Budget Remaining Balance"></asp:Label>
                                                                                                    </div>
                                                                                                    <div class="content">
                                                                                                        <asp:Label ID="txtLastBudgetRemainingBalance" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                    </div>
                                                                                                </div>                                                                                                
                                                                                            </div>
                                                                                            <div class="row clearfix" style="display: flex;justify-content: space-between;">
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <div class="title">
                                                                                                        <asp:Label ID="lblThisBudgetMaxAmount" runat="server" CssClass="form-label" Text="This Budget Max. Amount"></asp:Label>
                                                                                                    </div>
                                                                                                    <div class="content">
                                                                                                        <asp:Label ID="txtThisBudgetMaxAmount" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                    </div>
                                                                                            </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <asp:Label ID="lblThisBudgetTotalRequested" runat="server" CssClass="form-label"
                                                                                                        Text="This Budget Total Requested"></asp:Label>
                                                                                                    <asp:Label ID="txtThisBudgetTotalRequested" Text="" runat="server" CssClass="form-label" />
                                                                                                </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <asp:Label ID="lblThisBudgetTotalApproved" runat="server" CssClass="form-label" Text="This Budget Total Approved"></asp:Label>
                                                                                                    <asp:Label ID="txtThisBudgetTotalApproved" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                </div>
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <asp:Label ID="lblThisBudgetTotalEnrolled" runat="server" CssClass="form-label" Text="This Budget Total Enrolled"></asp:Label>
                                                                                                    <asp:Label ID="txtThisBudgetTotalEnrolled" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                </div>  
                                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                    <div class="title">
                                                                                                        <asp:Label ID="lblThisBudgetRemainingBalance" runat="server" CssClass="form-label" Text="This Budget Remaining Balance"></asp:Label>
                                                                                                </div>
                                                                                                    <div class="content">
                                                                                                        <asp:Label ID="txtThisBudgetRemainingBalance" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                                                    </div>
                                                                                                </div>                                                                                              
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="BudgetSummaryApprovalDepartment_<%# Eval("departmentunkid") %>" class="panel-collapse collapse"
                                                                                            role="tabpanel" aria-labelledby="BudgetSummaryApprovalDepartment_<%# Eval("departmentunkid") %>">
                                                                                            <div class="panel-body table-responsive" style="max-height: 300px">
                                                                                                <asp:GridView ID="dgvBudgetSummaryApprovalDepartmentItems" runat="server" Width="99%"
                                                                                                    AllowPaging="false" CssClass="table table-hover table-bordered m-b-0" AutoGenerateColumns="false"
                                                                                                    OnRowDataBound="dgvBudgetSummaryApprovalDepartmentItems_RowDataBound" DataKeyNames="departmentunkid">
                                                                                                    <Columns>
                                                                                                        <asp:BoundField DataField="PreviousRefNo" HeaderText="Last Ref No" FooterText="objcolhPreviousRefNo" />
                                                                                                        <asp:BoundField DataField="PreviousRequestedAmt" HeaderText="Last Requested Amount"
                                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="objcolhPreviousRequestedAmount" />
                                                                                                        <asp:BoundField DataField="PreviousApprovedAmt" HeaderText="Last Approved Amount"
                                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="objcolhPreviousApprovedAmount" />
                                                                                                        <asp:BoundField DataField="PreviousEnrolledAmt" HeaderText="Last Enrolled Amount"
                                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="objcolhPreviousEnrolledAmount" />                                                                                                        
                                                                                                        <asp:TemplateField HeaderText="This Ref No" FooterText="objcolhThisRefNo">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:LinkButton ID="lnkCurrentRefNo" runat="server" Text='<%# Eval("CurrentRefNo") %>'
                                                                                                                    OnClick="lnkCurrentRefNo_Click"></asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>    
                                                                                                        <asp:BoundField DataField="CurrentRequestedAmt" HeaderText="This Requested Amount"
                                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="objcolhThisRequestedAmount" />
                                                                                                        <asp:BoundField DataField="CurrentApprovedAmt" HeaderText="This Approved Amount"
                                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="objcolhThisApprovedAmount" />
                                                                                                        <asp:BoundField DataField="CurrentEnrolledAmt" HeaderText="This Enrolled Amount"
                                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="objcolhThisEnrolledAmount" />                                                                                                        
                                                                                                    </Columns>
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="SetMaxBudget">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card m-b-0">
                                                        <%--<div class="header">
                                                            <h2>
                                                                <asp:Label ID="LblHeaderSetMaxBudget" runat="server" Text="Set Max Budget"></asp:Label>
                                                            </h2>
                                                        </div>--%>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="LblSetMaxTrainingCalender" Text="Training Calendar" runat="server"
                                                                        CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboSetMaxBudgetCalendar" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 385px;">
                                                                        <asp:GridView ID="GvSetMaxBudget" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                            CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="allocationid,allocationtranunkid">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="allocationtrancode" HeaderText="Code" ReadOnly="true"
                                                                                    FooterText="dgcolhAllocationCode" />
                                                                                <asp:BoundField DataField="allocationtranname" HeaderText="Name" ReadOnly="true"
                                                                                    FooterText="dgcolhAllocationName" />
                                                                                <asp:TemplateField HeaderText="Set Max. Budget">
                                                                                    <ItemTemplate>
                                                                                        <num:NumberOnly ID="txtMaxBudget" runat="server" Text='<%# Eval("maxbudget_amt") %>'
                                                                                            AutoPostBack="false" OnTextChanged="txtMaxbudget_TextChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle Font-Bold="False" />
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnSaveSetMaxBudget" runat="server" CssClass="btn btn-primary" Text="Save" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnCloseSetMaxBudget" runat="server" CssClass="btn btn-default" Text="Close" />
                    <asp:HiddenField ID="hfSetMaxBudget" runat="Server" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="TabName" runat="server" />
            <asp:HiddenField ID="hfobjlblTotalAmt" runat="server" Value="0.00" />
            <asp:HiddenField ID="hfobjlblTotalApprovedAmt" runat="server" Value="0.00" />
            <asp:HiddenField ID="hfCostItemAmtChanged" runat="server" Value="0" />
            <asp:HiddenField ID="hfCostItemApprovedAmtChanged" runat="server" Value="0" />
            <asp:HiddenField ID="hfCancelActiveDTNeed" runat="server" />
            <asp:HiddenField ID="hfCancelInactiveDTNeed" runat="server" />
            <asp:HiddenField ID="CollapseBudgetSummaryApprovalDepartmentName" runat="server" />
            <asp:HiddenField ID="hfFilterContentHidden" runat="server" />
            <asp:HiddenField ID="hfCancelDeleteInstruct" runat="server" />
            <asp:HiddenField ID="hfUnlockSubmitted" runat="server" />
            <asp:HiddenField ID="hfFinalApprove" runat="server" />
            <asp:HiddenField ID="hfFinalReject" runat="server" />
            <%--<uc9:DeleteReason ID="popupDeleteEmployee" runat="server" Title="Are you sure you want to delete selected employees or selected allocations and their employees ?"
                ValidationGroup="DeleteEmployee" />
            <uc9:DeleteReason ID="popupDeleteAllocEmp" runat="server" Title="Are you sure you want to delete selected employees ?"
                ValidationGroup="DeleteAllocEmp" />
            <uc9:DeleteReason ID="popupDeleteTResoources" runat="server" Title="Are you sure you want to delete selected training resources ?"
                ValidationGroup="DeleteTResoources" />
            <uc9:DeleteReason ID="popupDeleteFSource" runat="server" Title="Are you sure you want to delete selected finaning sources ?"
                ValidationGroup="DeleteFSource" />
            <uc9:DeleteReason ID="popupDeleteTCoordinator" runat="server" Title="Are you sure you want to delete selected training co-ordinator ?"
                ValidationGroup="DeleteTCoordinator" />
            <uc9:DeleteReason ID="popupDeleteTCostItem" runat="server" Title="Are you sure you want to delete selected training cost items ?"
                ValidationGroup="DeleteTCostItem" />--%>
            <uc9:DeleteReason ID="popupDeleteDTNeed" runat="server" Title="Are you sure you want to delete selected departmental training need ?"
                ValidationGroup="DeleteDTNeed" />
            <uc9:DeleteReason ID="popupActiveDTNeed" runat="server" Title="Are you sure you want to make departmental training need active ?"
                ValidationGroup="ActiveDTNeed" CancelControlName="hfCancelActiveDTNeed" />
            <uc9:DeleteReason ID="popupInactiveDTNeed" runat="server" Title="Are you sure you want to make departmental training need inactive ?"
                ValidationGroup="InactiveDTNeed" CancelControlName="hfCancelInactiveDTNeed" />
            <uc9:DeleteReason ID="popupDeleteInstruct" runat="server" Title="Are you sure you want to delete selected instructor?"
                ValidationGroup="DeleteInstruct" CancelControlName="hfCancelDeleteInstruct" />
            <uc9:DeleteReason ID="popupUnlockSubmitted" runat="server" Title="Unlock Remark" ErrorMessage="Please enter Unlock Remark"
                ValidationGroup="UnlockSubmitted" CancelControlName="hfUnlockSubmitted" />
            <uc9:DeleteReason ID="popupFinalApprove" runat="server" Title="Approval Remark" ErrorMessage="Please enter Approval Remark"
                ValidationGroup="FinalApprove" CancelControlName="hfFinalApprove" />
            <uc9:DeleteReason ID="popupFinalReject" runat="server" Title="Rejection Remark" ErrorMessage="Please enter Rejection Remark"
                ValidationGroup="FinalReject" CancelControlName="hfFinalReject" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function() {
            RetriveCollapse();
            RetriveTab();
            CalcTotal();
            //CalcGrandTotal();

            $('input[id*=txtAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemAmtChanged').val('1');
            });
            $('input[id*=txtApprovedAmount]').on('blur', function() {
                CalcTotal();
                $$('hfCostItemApprovedAmtChanged').val('1');
            });

            $('.filtercontent').on('click', function() {
                $("[id*=hfFilterContentHidden]").val($('#filtercontent').is(':hidden'));
            });

            //            $('a[id*=txtAmount_lnkup]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });

            //            $('a[id*=txtAmount_lnkdown]').on('mouseup', function() {
            //                debugger;
            //                CalcTotal();
            //            });

            $('#columnlist a').on('click', function(event) {
                $(this).parent().toggleClass('open');
            });

            $('body').on('click', function(e) {
                if (!$('#columnlist').is(e.target)
                && $('#columnlist').has(e.target).length === 0
                && $('.open').has(e.target).length === 0
                ) {
                    $('#columnlist').removeClass('open');
                }
            });

        });

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }

        function RetriveTab() {

            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "TrainingInfo";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }
        function ShowTab(id) {
            $('.nav-tabs a[href="#' + id + '"]').tab('show');
            $("[id*=TabName]").val(id);
        }



        function CalcTotal() {
            var totalamt = 0;
            var totalapprovedamt = 0;
            $('input[id*=txtAmount]').each(function() {
                if (Number($(this).val().replace(/,/g, '')) != 0 && $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == false)
                    $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").prop('checked', true);

                if ($(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == true) {
                    //totalamt += Number($(this).val().replaceAll(',', ''));
                    totalamt += Number($(this).val().replace(/,/g, ''));
                }
            });

            $('input[id*=txtApprovedAmount]').each(function() {
                if (Number($(this).val().replace(/,/g, '')) != 0 && $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == false)
                    $(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").prop('checked', true);

                if ($(this).closest('tr').find("[id*='ChkgvSelectTCostItem']").is(':checked') == true) {
                    totalapprovedamt += Number($(this).val().replace(/,/g, ''));
                }
            });

            if (totalamt.toString().indexOf('.') >= 0)
                $$('objlblCostItemTotal').text(totalamt);
            else
                $$('objlblCostItemTotal').text(totalamt.toString().concat('.00'));

            if (totalapprovedamt.toString().indexOf('.') >= 0)
                $$('objlblCostItemApprovedTotal').text(totalapprovedamt);
            else
                $$('objlblCostItemApprovedTotal').text(totalapprovedamt.toString().concat('.00'));

            $$('hfobjlblTotalAmt').val($$('objlblCostItemTotal').text());
            $$('hfobjlblTotalApprovedAmt').val($$('objlblCostItemApprovedTotal').text());
        }


        function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));

            if (chkbox.id.includes('ChkAllTCostItem')) {
                if ($(chkbox).is(':checked') == false) {
                    $("[id*=" + chkgvSelect + "]", grid).closest('tr').find("[id*='txtAmount']").val(0);
                    $("[id*=" + chkgvSelect + "]", grid).closest('tr').find("[id*='txtApprovedAmount']").val(0);
                }
                CalcTotal();
            }
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }

            if (chkbox.id.includes('ChkgvSelectTCostItem')) {
                if ($(chkbox).is(':checked') == false) {
                    $(chkbox).closest('tr').find("[id*='txtAmount']").val(0);
                    $(chkbox).closest('tr').find("[id*='txtApprovedAmount']").val(0);
                }
                CalcTotal();
            }

        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
                $('.norecords').remove();
            }
            $('#' + txtID).focus();
        }

        function CalcGrandTotal() {
            var totalamt = 0;
            var totalapprovedamt = 0;
            $('span[id*=lbltotaltrainingcost]').each(function() {
                if ($(this).closest('tr').find("[id*='chkActive']").is(':checked') == true)
                    totalamt += Number($(this).text());
            });

            $('span[id*=lbltotalapprovedtrainingcost]').each(function() {
                if ($(this).closest('tr').find("[id*='chkActive']").is(':checked') == true)
                    totalapprovedamt += Number($(this).text());
            });

            //            if (totalamt.toString().indexOf('.') >= 0)
            //                $$('objGrandTotalList').text(totalamt);
            //            else
            //                $$('objGrandTotalList').text(totalamt.toString().concat('.00'));

            //            if (totalapprovedamt.toString().indexOf('.') >= 0)
            //                $$('objApprovedGrandTotalList').text(totalapprovedamt);
            //            else
            //                $$('objApprovedGrandTotalList').text(totalapprovedamt.toString().concat('.00'));

        }

        function SelectAllChildren(chk) {

            if ($(chk).closest("tr[style*='background-color:Cyan;']").length > 0) {
                $(chk).closest("tr").nextUntil(function() { return $(this).find("span[id*='lbltrainingcourse']").text() != '' }).each(function() {
                    $(this).find("input[id*=chkSelect]").prop('checked', chk.checked);
                });
            }
            else if ($(chk).closest("tr").prevAll("tr[style*='background-color:Cyan;']").length > 0) {
                var chkParent = $(chk).closest("tr").prevAll("tr[style*='background-color:Cyan;']")[0];
                $(chkParent).find("input[id*='chkSelect']").prop('checked', chk.checked);
                $(chkParent).closest("tr").nextUntil(function() { return $(this).find("span[id*='lbltrainingcourse']").text() != '' }).each(function() {
                    $(this).find("input[id*=chkSelect]").prop('checked', chk.checked);
                });
            }

            //CalcGrandTotal();
        }

        function RetriveCollapse() {
            var CollapseName = $("[id*=CollapseBudgetSummaryApprovalDepartmentName]").val();
            if (CollapseName != "") {
                $("#" + CollapseName + "").collapse('show');
            }

            var hfFilterContentHidden = $("[id*=hfFilterContentHidden]").val();
            if (hfFilterContentHidden == "true")
                $("#filtercontent").collapse('show');

            $(".BudgetSummaryApprovalDepartmentCollapse").click(function() {
                $("[id*=CollapseBudgetSummaryApprovalDepartmentName]").val($(this).attr("href").replace("#", ""));
            });

        }
    </script>

</asp:Content>
