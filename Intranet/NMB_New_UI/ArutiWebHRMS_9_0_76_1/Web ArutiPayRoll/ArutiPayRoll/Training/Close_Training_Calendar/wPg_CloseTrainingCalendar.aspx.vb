﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region

Partial Class Training_Close_Training_Calendar_wPg_CloseTrainingCalendar
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmCloseTrainingCalendar"
    Private mblnShowPendingTrainingPopup As Boolean = False
#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillCombo()
            End If

            If mblnShowPendingTrainingPopup = True Then
                popupPendingTraining.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objTPeriod.getListForCombo("List", True, StatusType.Open)
            With cboTrainingCalendar
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsCombo = Nothing
            objTPeriod = Nothing
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue"), Me)
                Return False
            End If

            If IsPendingTraining() Then
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetValue(ByRef objCalendar As clsTraining_Calendar_Master)
        Try
            objCalendar._Calendarunkid = CInt(cboTrainingCalendar.SelectedValue)
            objCalendar._Statusunkid = enStatusType.CLOSE
            objCalendar._AuditUserId = CInt(Session("UserId"))
            objCalendar._FormName = mstrModuleName
            objCalendar._ClientIP = CStr(Session("IP_ADD"))
            objCalendar._HostName = CStr(Session("HOST_NAME"))
            objCalendar._FromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsPendingTraining() As Boolean
        Dim objDeptTraining As New clsDepartmentaltrainingneed_master
        Dim objRequestMaster As New clstraining_request_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsPendingApproveTrainingList As New DataSet
        Dim dsLessStaffTrainingList As New DataSet
        Dim dsPendingToCompleteTrainingList As New DataSet
        Dim mblnFlag As Boolean = False
        Dim StrSearching As String = String.Empty
        Try
            pnl_ViewPendingToApproveTraining.Visible = False
            pnl_ViewLessStaffTraining.Visible = False
            pnl_ViewPendingToCompleteTraining.Visible = False

            StrSearching = " AND trdepartmentaltrainingneed_master.periodunkid = " & CInt(cboTrainingCalendar.SelectedValue) & _
                           " AND trdepartmentaltrainingneed_master.statusunkid NOT IN ( " & clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved & " , " & clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected & ")"

            dsPendingApproveTrainingList = objDeptTraining.GetList(CStr(Session("Database_Name")), _
                                                                    CInt(Session("UserId")), _
                                                                    CInt(Session("Fin_year")), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                                    True, 0, "List", False, _
                                                                    StrSearching)

            For Each r As DataRow In dsPendingApproveTrainingList.Tables(0).Rows
                If r("startdate").ToString.Trim <> "" AndAlso r("enddate").ToString.Trim <> "" Then
                    r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                End If
            Next

            dsPendingApproveTrainingList.Tables(0).AcceptChanges()

            If dsPendingApproveTrainingList.Tables(0).Rows.Count > 0 Then
                GvPendingToApproveTraining.DataSource = dsPendingApproveTrainingList.Tables(0)
                GvPendingToApproveTraining.DataBind()
                pnl_ViewPendingToApproveTraining.Visible = True
                If mblnFlag = False Then mblnFlag = True
            End If

            dsLessStaffTrainingList = objDeptTraining.GetTrainingRequestedStaffCount("List", CInt(cboTrainingCalendar.SelectedValue))

            If dsLessStaffTrainingList.Tables(0).Columns.Contains("IsGrp") = False Then
                dsLessStaffTrainingList.Tables(0).Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
            End If

            For Each r As DataRow In dsLessStaffTrainingList.Tables(0).Rows
                r("training") = r("training").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                r("IsGrp") = False
                If CInt(r("NoOfStaff")) = CInt(r("RequestedCount")) Then
                    r.Delete()
                End If
            Next

            dsLessStaffTrainingList.Tables(0).AcceptChanges()

            Dim dsTCategoryList As DataSet = objTCategory.getListForCombo("List", False)
            Dim dRow As DataRow = Nothing
            For Each drTCategory As DataRow In dsTCategoryList.Tables(0).Rows
                Dim drRow() As DataRow = dsLessStaffTrainingList.Tables(0).Select("trainingcategoryunkid = " & CInt(drTCategory.Item("categoryunkid")) & " ")
                If drRow.Length > 0 Then
                    dRow = dsLessStaffTrainingList.Tables(0).NewRow()
                    dRow.Item("departmentaltrainingneedunkid") = -1
                    dRow.Item("trainingcategoryunkid") = CInt(drTCategory.Item("categoryunkid"))
                    dRow.Item("trainingcategory") = CStr(drTCategory.Item("categoryname"))
                    dRow.Item("trainingcourseunkid") = -1
                    dRow.Item("training") = ""
                    dRow.Item("NoOfStaff") = -1
                    dRow.Item("RequestedCount") = -1
                    dRow.Item("IsGrp") = 1
                    dsLessStaffTrainingList.Tables(0).Rows.Add(dRow)
                End If
            Next

            Dim xTable As DataTable = New DataView(dsLessStaffTrainingList.Tables(0), "", "trainingcategoryunkid, trainingcourseunkid ", DataViewRowState.CurrentRows).ToTable.Copy
            If dsLessStaffTrainingList.Tables(0).Rows.Count > 0 Then
                GvLessStaffTraining.DataSource = xTable
                GvLessStaffTraining.DataBind()
                pnl_ViewLessStaffTraining.Visible = True
                If mblnFlag = False Then mblnFlag = True
            End If

            StrSearching = " trtraining_request_master.periodunkid = " & CInt(cboTrainingCalendar.SelectedValue) & " " & _
                           " AND statusunkid <> " & enTrainingRequestStatus.REJECTED & " " & _
                           " AND completed_statusunkid <> " & enTrainingRequestStatus.REJECTED & " " & _
                           " AND isenroll_reject <> 1 " & _
                           " AND ( issubmit_approval = 0 " & _
                           "        OR statusunkid = 1 " & _
                           "        OR (isenroll_confirm = 0 AND isenroll_reject = 0) " & _
                           "        OR iscompleted_submit_approval = 0 " & _
                           "        OR completed_statusunkid = 1 " & _
                           "        OR (  " & _
                           "                isdaysafterfeedback_submitted = 0 " & _
                           "                AND (SELECT " & _
                           "                       COUNT(*) " & _
                           "                     FROM treval_question_master " & _
                           "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                           "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING & ") > 0 " & _
                           "           )  " & _
                           "        OR (  " & _
                           "                ispretrainingfeedback_submitted = 0 " & _
                           "                AND (SELECT " & _
                           "                       COUNT(*) " & _
                           "                     FROM treval_question_master " & _
                           "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                           "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.PRETRAINING & ") > 0 " & _
                           "           )  " & _
                           "        OR (  " & _
                           "                isposttrainingfeedback_submitted = 0 " & _
                           "                AND (SELECT " & _
                           "                       COUNT(*) " & _
                           "                     FROM treval_question_master " & _
                           "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                           "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.POSTTRAINING & ") > 0 " & _
                           "           )  " & _
                           "        OR (  " & _
                           "                isdaysafter_linemanager_submitted = 0 " & _
                           "                AND (SELECT " & _
                           "                       COUNT(*) " & _
                           "                     FROM treval_question_master " & _
                           "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                           "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING & ") > 0 " & _
                           "           )  " & _
                           "     ) "
            dsPendingToCompleteTrainingList = objRequestMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), _
                                                              True, CBool(Session("IsIncludeInactiveEmp")), "Training", , StrSearching, , False)
            If dsPendingToCompleteTrainingList.Tables(0).Rows.Count > 0 Then
                GvPendingToCompleteTraining.DataSource = dsPendingToCompleteTrainingList.Tables(0)
                GvPendingToCompleteTraining.DataBind()
                pnl_ViewPendingToCompleteTraining.Visible = True
                If mblnFlag = False Then mblnFlag = True
            End If

            If mblnFlag = True Then
                mblnShowPendingTrainingPopup = True
                popupPendingTraining.Show()
            End If

        Catch ex As Exception
            mblnFlag = True
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsPendingApproveTrainingList = Nothing
            dsLessStaffTrainingList = Nothing
            dsPendingToCompleteTrainingList = Nothing
            objDeptTraining = Nothing
            objRequestMaster = Nothing
            objTCategory = Nothing
        End Try
        Return mblnFlag
    End Function

#End Region

#Region " Button's Event "

    Protected Sub btnCloseCalendar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseCalendar.Click
        Try
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Are you sure you want to close this Training Calendar ?")
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnPendingTrainingClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPendingTrainingClose.Click
        Try
            mblnShowPendingTrainingPopup = False
            popupPendingTraining.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " GridView's Event "

    Protected Sub GvLessStaffTraining_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvLessStaffTraining.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(GvLessStaffTraining.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(0).Text = DataBinder.Eval(e.Row.DataItem, "TrainingCategory").ToString
                    e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = 1 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim mblnFlag As Boolean = True
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            SetValue(objCalendar)

            mblnFlag = objCalendar.Update()

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objCalendar._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Training Calendar Closed Successfully!!!"), Me)
                FillCombo()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCalendar.ID, lblTrainingCalendar.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCloseCalendar.ID, btnCloseCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)

            'Pending Training popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCourseMaster.ID, lblTrainingCourseMaster.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllnkPendingToApproveTraining.ID, lbllnkPendingToApproveTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPendingToApproveTraining.Columns(0).FooterText, GvPendingToApproveTraining.Columns(0).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblViewLessStaffTraining.ID, lblViewLessStaffTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvLessStaffTraining.Columns(0).FooterText, GvLessStaffTraining.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvLessStaffTraining.Columns(1).FooterText, GvLessStaffTraining.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvLessStaffTraining.Columns(2).FooterText, GvLessStaffTraining.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllnkViewPendingToCompleteTraining.ID, lbllnkViewPendingToCompleteTraining.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPendingToCompleteTraining.Columns(0).FooterText, GvPendingToCompleteTraining.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvPendingToCompleteTraining.Columns(1).FooterText, GvPendingToCompleteTraining.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnPendingTrainingClose.ID, btnPendingTrainingClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCalendar.ID, lblTrainingCalendar.Text)

            Me.btnCloseCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCloseCalendar.ID, btnCloseCalendar.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")

            'Pending Training popup
            Me.lblTrainingCourseMaster.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCourseMaster.ID, lblTrainingCourseMaster.Text)

            Me.lbllnkPendingToApproveTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllnkPendingToApproveTraining.ID, lbllnkPendingToApproveTraining.Text)
            GvPendingToApproveTraining.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPendingToApproveTraining.Columns(0).FooterText, GvPendingToApproveTraining.Columns(0).HeaderText)

            Me.lblViewLessStaffTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblViewLessStaffTraining.ID, lblViewLessStaffTraining.Text)
            GvLessStaffTraining.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvLessStaffTraining.Columns(0).FooterText, GvLessStaffTraining.Columns(0).HeaderText)
            GvLessStaffTraining.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvLessStaffTraining.Columns(1).FooterText, GvLessStaffTraining.Columns(1).HeaderText)
            GvLessStaffTraining.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvLessStaffTraining.Columns(2).FooterText, GvLessStaffTraining.Columns(2).HeaderText)

            Me.lbllnkViewPendingToCompleteTraining.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllnkViewPendingToCompleteTraining.ID, lbllnkViewPendingToCompleteTraining.Text)
            GvPendingToCompleteTraining.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPendingToCompleteTraining.Columns(0).FooterText, GvPendingToCompleteTraining.Columns(0).HeaderText)
            GvPendingToCompleteTraining.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvPendingToCompleteTraining.Columns(1).FooterText, GvPendingToCompleteTraining.Columns(1).HeaderText)

            Me.btnPendingTrainingClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnPendingTrainingClose.ID, btnPendingTrainingClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Training Calendar Closed Successfully!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Are you sure you want to close this Training Calendar ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
