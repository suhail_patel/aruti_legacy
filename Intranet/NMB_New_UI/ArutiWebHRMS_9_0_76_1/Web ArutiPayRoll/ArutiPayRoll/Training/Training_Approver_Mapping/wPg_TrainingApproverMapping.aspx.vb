﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

#End Region
Partial Class Training_Training_Approver_Mapping_wPg_TrainingApproverMapping
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingApproverMapping"
    Private blnpopupTrainingApprover As Boolean = False
    Private mintApproverMatrixUnkid As Integer = -1
    Private mdtEmployee As DataTable = Nothing
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = String.Empty
    Private dtEmployee As DataTable = Nothing
    Private mintapproverunkid As Integer = -1
    Private currentId As String = ""
    Private mintMappingUnkid As Integer
    Private mblActiveDeactiveApprStatus As Boolean
    Private mdtApproverList As DataTable
    Private mstrDeleteAction As String = ""
    Private mintLevelmstid As Integer
    Private objTrainingApprLvlMaster As New clstraining_approverlevel_master
    Private blnpopupApproverLevel As Boolean = False
    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Private mintTrainingApproverAllocationID As Integer = -1
    'Hemant (09 Feb 2022) -- End

#End Region

#Region "Private Method(s)"

    'Hemant (03 Dec 2021) -- Start
    'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    Private Sub drpPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal cboLevel As DropDownList)
        Dim objLevel As New clstraining_approverlevel_master
        Dim dsList As DataSet
        Try
            Dim cboPeriod As DropDownList = CType(sender, DropDownList)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                dsList = objLevel.getListForCombo(CInt(cboPeriod.SelectedValue), "List", True)
                cboLevel.DataTextField = "name"
                cboLevel.DataValueField = "levelunkid"
                cboLevel.DataSource = dsList.Tables(0)
                cboLevel.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing
            objLevel = Nothing
        End Try
    End Sub
    'Hemant (03 Dec 2021) -- End

#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillComboApprovalLevel()
                If CBool(Session("AllowToViewTrainingApproverLevel")) Then
                    FillListApprovalLevel(False)
                Else
                    FillListApprovalLevel(True)

                End If
                SetVisibilityApprovalLevel()
                ListFillCombo()
                FillApprovalMatrix()
                FillTrainingApproverCombo()

                'Hemant (09 Feb 2022) -- Start            
                'OLD-549(NMB) : Give new screen for training approver allocation mapping
                mintTrainingApproverAllocationID = CInt(Session("TrainingApproverAllocationID"))
                drpALAllocation.SelectedValue = CStr(mintTrainingApproverAllocationID)
                drpALAllocation_SelectedIndexChanged(Nothing, Nothing)
                'Hemant (09 Feb 2022) -- End

                If CBool(Session("AllowToViewTrainingApprover")) Then
                    FillTrainingApproverList(False)
                Else
                    FillTrainingApproverList(True)
                End If
            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If

                blnpopupApproverLevel = Convert.ToBoolean(ViewState("blnpopupApproverLevel").ToString())

                mintApproverMatrixUnkid = CInt(Me.ViewState("mintApproverMatrixUnkid"))

                If ViewState("mdtApproverList") IsNot Nothing Then
                    mdtApproverList = CType(ViewState("mdtApproverList"), DataTable)
                    If mdtApproverList.Rows.Count > 0 AndAlso CInt(mdtApproverList.Rows(0)("mappingunkid").ToString) > 0 Then
                        gvApproverList.DataSource = mdtApproverList
                        gvApproverList.DataBind()
                    End If
                End If
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                'Hemant (09 Feb 2022) -- Start            
                'OLD-549(NMB) : Give new screen for training approver allocation mapping
                mintTrainingApproverAllocationID = CInt(Me.ViewState("mintTrainingApproverAllocationID"))
                'Hemant (09 Feb 2022) -- End

                If blnpopupTrainingApprover Then
                    popupTrainingApprover.Show()
                Else
                    popupTrainingApprover.Hide()
                End If

                If blnpopupApproverLevel Then
                    popupApproverLevel.Show()
                Else
                    popupApproverLevel.Hide()
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupApproverLevel") = blnpopupApproverLevel
            ViewState("mintApproverMatrixUnkid") = mintApproverMatrixUnkid
            ViewState("mintMappingUnkid") = mintMappingUnkid
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus
            ViewState("mdtApproverList") = mdtApproverList
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            ViewState("mintTrainingApproverAllocationID") = mintTrainingApproverAllocationID
            'Hemant (09 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Approval Levels"

#Region " Private Methods "

    Private Sub FillComboApprovalLevel()
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As DataSet
        Try
            dsList = objCalendar.getListForCombo("List", , StatusType.Open)
            If dsList.Tables(0).Rows.Count > 0 Then
                With drpTrainingCalendarAppLevel
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Private Sub FillListApprovalLevel(ByVal isblank As Boolean)
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As New DataSet
        Dim dsCalendarList As New DataSet
        Dim strfilter As String = ""
        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If

            dsCalendarList = objCalendar.getListForCombo("List", , StatusType.Open)
            If dsCalendarList.Tables(0).Rows.Count > 0 Then
                'Hemant (03 Dec 2021) -- Start
                'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
                'If strfilter.Trim.Length > 0 Then
                '    strfilter &= " AND hrtraining_approverlevel_master.calendarunkid = " & dsCalendarList.Tables(0).Rows(0).Item("calendarunkid").ToString
                'Else
                '    strfilter &= " hrtraining_approverlevel_master.calendarunkid = " & dsCalendarList.Tables(0).Rows(0).Item("calendarunkid").ToString
                'End If
                If strfilter.Trim.Length > 0 Then
                    strfilter &= " AND  trtraining_calendar_master.statusunkid =  " & enStatusType.OPEN & ""
                Else
                    strfilter &= " trtraining_calendar_master.statusunkid =  " & enStatusType.OPEN & ""
                End If
                'Hemant (03 Dec 2021) -- End
                dsList = objTrainingApprLvlMaster.GetList("List", True, False, strfilter)
                If dsList.Tables(0).Rows.Count <= 0 Then
                    dsList = objTrainingApprLvlMaster.GetList("List", True, True, "1 = 2")
                    isblank = True
                End If
                GvApprLevelList.DataSource = dsList.Tables("List")
                GvApprLevelList.DataBind()
            End If

            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Private Sub SetVisibilityApprovalLevel()
        Try
            btnnew.Visible = CBool(Session("AllowToAddTrainingApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValueApprovalLevel()
        Try
            If mintLevelmstid > 0 Then
                objTrainingApprLvlMaster._Levelunkid = mintLevelmstid
            End If

            objTrainingApprLvlMaster._Levelcode = txtlevelcode.Text.Trim
            objTrainingApprLvlMaster._Levelname = txtlevelname.Text.Trim
            objTrainingApprLvlMaster._Priority = Convert.ToInt32(txtlevelpriority.Text)
            objTrainingApprLvlMaster._Calendarunkid = CInt(drpTrainingCalendarAppLevel.SelectedValue)
            Call SetAtValueApprovalLevel()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValueApprovalLevel()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApprLvlMaster._AuditUserId = CInt(Session("UserId"))
            End If
            objTrainingApprLvlMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApprLvlMaster._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApprLvlMaster._HostName = CStr(Session("HOST_NAME"))
            objTrainingApprLvlMaster._FormName = mstrModuleName
            objTrainingApprLvlMaster._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub GetValueApprovalLevel()
        Try
            If mintLevelmstid > 0 Then
                objTrainingApprLvlMaster._Levelunkid = mintLevelmstid
            End If
            txtlevelcode.Text = objTrainingApprLvlMaster._Levelcode
            txtlevelname.Text = objTrainingApprLvlMaster._Levelname
            txtlevelpriority.Text = objTrainingApprLvlMaster._Priority.ToString()
            If CInt(objTrainingApprLvlMaster._Calendarunkid) > 0 Then
                drpTrainingCalendarAppLevel.SelectedValue = CStr(objTrainingApprLvlMaster._Calendarunkid)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearDataApprovalLevel()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
            drpTrainingCalendarAppLevel.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "
    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveApprover.Click
        Dim blnFlag As Boolean = False
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
                Exit Sub
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Approver Level Name cannot be blank. Approver Level Name is required information"), Me)
                Exit Sub
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Approver Level Priority cannot be less than Zero. Approver Level Priority is required information"), Me)
                Exit Sub
            ElseIf CInt(drpTrainingCalendarAppLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue"), Me)
                Exit Sub
            End If
            Call SetValueApprovalLevel()
            If mintLevelmstid > 0 Then
                blnFlag = objTrainingApprLvlMaster.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objTrainingApprLvlMaster.Insert()
            End If

            If blnFlag = False And objTrainingApprLvlMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApprLvlMaster._Message, Me)
                blnpopupApproverLevel = True
                popupApproverLevel.Show()
            Else
                ClearDataApprovalLevel()
                FillListApprovalLevel(False)
                ListFillCombo()
                FillTrainingApproverCombo()
                mintLevelmstid = 0
                blnpopupApproverLevel = False
                popupApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValueApprovalLevel()
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())

            If objTrainingApprLvlMaster.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use"), Me)
                Exit Sub
            End If
            mstrDeleteAction = "dellevel"
            cnfConfirm.Show()
            cnfConfirm.Title = "Confirmation"
            cnfConfirm.Message = "Are You Sure You Want To Delete This Approver Level ?"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub btnCloseGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClosApprover.Click
        Try
            ClearDataApprovalLevel()
            blnpopupApproverLevel = False
            popupApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("levelunkid").ToString) > 0 Then
                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                        lnkedit.Visible = CBool(Session("AllowToEditTrainingApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteTrainingApproverLevel"))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Approval Matrix"

#Region " Private Methods "

    Private Sub ListFillCombo()
        Dim objLevel As New clstraining_approverlevel_master
        Dim objMaster As New clsMasterData
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Try

            dsList = objCalendar.getListForCombo("List", , StatusType.Open)
            If dsList.Tables(0).Rows.Count > 0 Then
                With drpTrainingCalendar
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With

                With drpALTrainingCalendar
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0).Copy()
                    .DataBind()
                End With

                With drpATrainingCalendar
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0).Copy()
                    .DataBind()
                End With

                dsList = objLevel.getListForCombo(CInt(drpTrainingCalendar.SelectedValue), "List", True)
                drpLevel.DataTextField = "name"
                drpLevel.DataValueField = "levelunkid"
                drpLevel.DataSource = dsList.Tables(0)
                drpLevel.DataBind()

                drpALevel.DataTextField = "name"
                drpALevel.DataValueField = "levelunkid"
                drpALevel.DataSource = dsList.Tables(0).Copy()
                drpALevel.DataBind()

            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevel = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim objApprovalMatrix As New clsTraining_Approval_Matrix
        Dim dsApprovalMatrix As DataSet
        Try
            If CInt(drpTrainingCalendar.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue"), Me)
                Return False
            End If

            If CInt(drpLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Level is mandatory information. Please select Level to continue"), Me)
                Return False
            End If

            'Hemant (25 Oct 2021) -- Start
            If CDbl(txtCostAmountFrom.Text) > 1000000000000000000 OrElse CDbl(txtCostAmountTo.Text) > 1000000000000000000 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Sorry, Defined Cost Amount is too large"), Me)
                Return False
            End If
            'Hemant (25 Oct 2021) -- End

            dsApprovalMatrix = objApprovalMatrix.GetList("List", CInt(drpTrainingCalendar.SelectedValue))
            Dim drApprovalMatrix As DataRow() = dsApprovalMatrix.Tables(0).Select("levelunkid = " & CInt(drpLevel.SelectedValue) & " AND trainingapprovalmatrixunkid <> " & mintApproverMatrixUnkid & " ")
            If drApprovalMatrix.Length > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, approval matrix for the selected level is already defined for this calendar. Please select another Level to continue"), Me)
                Return False
            End If

            For Each drRow As DataRow In dsApprovalMatrix.Tables(0).Select("trainingapprovalmatrixunkid <> " & mintApproverMatrixUnkid)
                If (CDbl(txtCostAmountFrom.Text) >= CDbl(drRow.Item("costamountfrom")) AndAlso CDbl(txtCostAmountFrom.Text) <= CDbl(drRow.Item("costamountto"))) OrElse _
                (CDbl(txtCostAmountTo.Text) >= CDbl(drRow.Item("costamountfrom")) AndAlso CDbl(txtCostAmountTo.Text) <= CDbl(drRow.Item("costamountto"))) Then

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, this cost range is already defined to another approver level"), Me)
                    Return False

                End If
            Next

            Dim drExistRow() As DataRow = dsApprovalMatrix.Tables(0).Select("trainingapprovalmatrixunkid <> " & mintApproverMatrixUnkid & " AND costamountto > " & CDbl(txtCostAmountFrom.Text) & "  AND costamountto < " & CDbl(txtCostAmountTo.Text))
            If drExistRow.Length > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, this cost range is not valid"), Me)
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Function

    Private Sub FillApprovalMatrix()
        Dim objApprovalMatrix As New clsTraining_Approval_Matrix
        Dim dsList As New DataSet
        Try
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            'dsList = objApprovalMatrix.GetList("List", CInt(drpTrainingCalendar.SelectedValue))
            dsList = objApprovalMatrix.GetList("List", , , , StatusType.Open)
            'Hemant (03 Dec 2021) -- End
            gvApprovalMatrix.DataSource = dsList.Tables(0)
            gvApprovalMatrix.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovalMatrix = Nothing
        End Try
    End Sub

    Private Sub ClearCtrls()
        Try
            mintApproverMatrixUnkid = 0
            drpTrainingCalendar.SelectedIndex = 0
            drpLevel.SelectedValue = CStr(0)
            txtCostAmountFrom.Text = "0"
            txtCostAmountTo.Text = "0"
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            drpTrainingCalendar_SelectedIndexChanged(drpTrainingCalendar, Nothing)
            'Hemant (03 Dec 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue(ByVal objApproverMatrix As clsTraining_Approval_Matrix)
        Try
            objApproverMatrix._TrainingApprovalMatrixunkid = mintApproverMatrixUnkid
            objApproverMatrix._Calendarunkid = CInt(drpTrainingCalendar.SelectedValue)
            objApproverMatrix._Levelunkid = CInt(drpLevel.SelectedValue)
            objApproverMatrix._CostAmountFrom = CDec(txtCostAmountFrom.Text)
            objApproverMatrix._CostAmountTo = CDec(txtCostAmountTo.Text)
            objApproverMatrix._Isvoid = False

            objApproverMatrix._AuditUserId = CInt(Session("UserId"))
            objApproverMatrix._ClientIP = CStr(Session("IP_ADD"))
            objApproverMatrix._FormName = mstrModuleName
            objApproverMatrix._FromWeb = True
            objApproverMatrix._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetApprovalMatrixValue(ByVal intApproverMatrixId As Integer)
        Dim objApprovalMatrix As New clsTraining_Approval_Matrix
        Try
            objApprovalMatrix._TrainingApprovalMatrixunkid = intApproverMatrixId
            drpTrainingCalendar.SelectedValue = CStr(objApprovalMatrix._Calendarunkid)
            drpLevel.SelectedValue = CStr(objApprovalMatrix._Levelunkid)
            txtCostAmountFrom.Text = CStr(objApprovalMatrix._CostAmountFrom)
            txtCostAmountTo.Text = CStr(objApprovalMatrix._CostAmountTo)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovalMatrix = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objApproverMatrix As New clsTraining_Approval_Matrix
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then
                Exit Sub
            End If

            SetValue(objApproverMatrix)
            If mintApproverMatrixUnkid > 0 Then
                blnFlag = objApproverMatrix.Update()
            Else
                blnFlag = objApproverMatrix.Insert()
            End If

            If blnFlag = False AndAlso objApproverMatrix._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objApproverMatrix._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Approval Matrix Saved successfully"), Me)
                ClearCtrls()
                FillApprovalMatrix()
                drpTrainingCalendar.Enabled = True
                drpLevel.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMatrix = Nothing
        End Try

    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ClearCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView's Event"

    Protected Sub gvApprovalMatrix_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApprovalMatrix.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(gvApprovalMatrix.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkApprovalMatrixEdit_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintApproverMatrixUnkid = CInt(gvApprovalMatrix.DataKeys(row.RowIndex)("trainingapprovalmatrixunkid"))
            GetApprovalMatrixValue(mintApproverMatrixUnkid)
            drpTrainingCalendar.Enabled = False
            drpLevel.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub lnkApprovalMatrixDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintApproverMatrixUnkid = CInt(gvApprovalMatrix.DataKeys(row.RowIndex)("trainingapprovalmatrixunkid"))
            mstrDeleteAction = "delmatrix"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "You are about to delete this Approval Matrix Entry. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

#End Region

#Region "ComboBox Event(s)"

    'Hemant (03 Dec 2021) -- Start
    'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    Private Sub drpTrainingCalendar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpTrainingCalendar.SelectedIndexChanged
        Try
            drpPeriod_SelectedIndexChanged(sender, drpLevel)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (03 Dec 2021) -- End

#End Region

#End Region

#Region "Training Approver"

#Region "Private Methods"

    Private Function IsTrainingApproverValid() As Boolean
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Dim dsTrainingApprover As New DataSet
        Try
            If CInt(drpARole.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Role cannot be blank. Role is required information"), Me)
                drpARole.Focus()
                Return False

            ElseIf CInt(drpALevel.SelectedValue) = 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Approver Level is compulsory information. Please Select Approver Level"), Me)
                drpLevel.Focus()
                Return False

            End If

            dsTrainingApprover = objTrainingApproverMaster.GetList("List", CInt(drpTrainingCalendar.SelectedValue))
            Dim drTrainingApprover As DataRow() = dsTrainingApprover.Tables(0).Select("roleunkid = " & CInt(drpARole.SelectedValue) & " AND allocationid = " & CInt(mintTrainingApproverAllocationID) & "    AND levelunkid <> " & CInt(drpALevel.SelectedValue) & " ")
            If drTrainingApprover.Length > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, Selected Role is already defined to another Level for this calendar. Please select another Role to continue"), Me)
                Return False
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAllocationList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocation"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Allocation is compulsory information.Please Check atleast One Allocation Item."), Me)
                Return False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            blnpopupTrainingApprover = True
            popupTrainingApprover.Show()
            objTrainingApproverMaster = Nothing
            dsTrainingApprover = Nothing
        End Try
        Return True
    End Function

    Private Sub FillTrainingApproverList(ByVal isblank As Boolean)
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try
            If CInt(drpALTrainingCalendar.SelectedValue) > 0 Then
                strSearching &= "AND TAM.calendarunkid = " & CInt(drpALTrainingCalendar.SelectedValue) & " "
            End If

            If CInt(drpALLevel.SelectedValue) > 0 Then
                strSearching &= "AND TAM.levelunkid = " & CInt(drpALLevel.SelectedValue) & " "
            End If

            If CInt(drpALRole.SelectedValue) > 0 Then
                strSearching &= "AND TAM.roleunkid = " & CInt(drpALRole.SelectedValue) & " "
            End If

            If CInt(drpALAllocation.SelectedValue) > 0 Then
                strSearching &= "AND TAM.allocationid = " & CInt(drpALAllocation.SelectedValue) & " "
            End If

            If CInt(drpALAllocationName.SelectedValue) > 0 Then
                strSearching &= "AND TAM.allocationunkid = " & CInt(drpALAllocationName.SelectedValue) & " "
            End If


            If isblank Or CBool(Session("AllowToViewTrainingApprover")) = False Then
                strSearching = "AND 1 = 2 "
            End If

            If strSearching.Trim.Length > 0 Then strSearching = strSearching.Substring(3)


            dsApproverList = objTrainingApproverMaster.GetList("List", True, strSearching)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objTrainingApproverMaster.GetList("List", True, "1 = 2", 0, Nothing, True)
                isblank = True
            End If


            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "Level", DataViewRowState.CurrentRows).ToTable()
                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("Level")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLeaveName = drow("Level").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                dtTable.AcceptChanges()
                gvApproverList.DataSource = dtTable
                gvApproverList.DataBind()
                mdtApproverList = dtTable
                If isblank = True Then
                    gvApproverList.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try
    End Sub

    Private Sub FillTrainingApproverCombo()
        Dim objLevel As New clstraining_approverlevel_master
        Dim objApprover As New clstraining_approver_master
        Dim objUserRole As New clsUserRole_Master
        Dim dsList As DataSet
        Try

            dsList = objUserRole.getComboList("UserRole", True)
            With drpARole
                .DataSource = dsList.Tables("UserRole")
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataBind()
            End With
            Me.ViewState.Add("UserList", dsList.Tables("UserRole").Copy())

            With drpALRole
                .DataSource = dsList.Tables("UserRole").Copy()
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataBind()
            End With

            If drpALTrainingCalendar.DataSource IsNot Nothing Then
                dsList = objLevel.getListForCombo(CInt(drpALTrainingCalendar.SelectedValue), "List", True)
                With drpALLevel
                    .DataTextField = "name"
                    .DataValueField = "levelunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With
            End If

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            dsList = (New clsMasterData).GetEAllocation_Notification("AList", "", False, False)
            With drpALAllocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = CStr(enAllocation.DEPARTMENT)
            End With
            'Hemant (09 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevel = Nothing
            objApprover = Nothing
            objUserRole = Nothing
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetAtValue(ByVal objTrainingApproverMaster As clstraining_approver_master)

        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApproverMaster._AuditUserId = CInt(Session("UserId"))
            End If
            objTrainingApproverMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApproverMaster._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApproverMaster._HostName = CStr(Session("HOST_NAME"))
            objTrainingApproverMaster._FormName = mstrModuleName
            objTrainingApproverMaster._IsFromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try

    End Sub

    Protected Sub SetValueToPopup(ByVal objTrainingApproverMaster As clstraining_approver_master)
        Dim objTrainingApproverTran As New clsTraining_Approver_Tran
        Dim dtAssignEmployeeList As New DataTable
        Try
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            drpATrainingCalendar.SelectedValue = objTrainingApproverMaster._Calendarunkid
            drpPeriod_SelectedIndexChanged(drpATrainingCalendar, drpALevel)
            'Hemant (03 Dec 2021) -- End
            drpALevel.SelectedValue = objTrainingApproverMaster._Levelunkid
            drpARole.SelectedValue = objTrainingApproverMaster._Roleunkid
            drpARole_SelectedIndexChanged(Nothing, Nothing)
            drpARole.Enabled = False
            drpATrainingCalendar.Enabled = False

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objTrainingApproverTran._TrainingApproverunkid = objTrainingApproverMaster._Mappingunkid
            'objTrainingApproverTran.GetData()

            'dtAssignEmployeeList = objTrainingApproverTran._TranDataTable
            FillTrainingApproverAllocationList()
            'Hemant (09 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverTran = Nothing
        End Try
    End Sub

    'Hemant (03 Dec 2021) -- Start
    'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    Private Sub ClearCtrlsTrainingApprover()
        Try
            mintMappingUnkid = 0
            drpATrainingCalendar.SelectedIndex = 0
            drpALevel.SelectedValue = CStr(0)
            drpARole.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (03 Dec 2021) -- End

    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Private Sub FillTrainingApproverAllocationList()

        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim StrSearch As String = ""
        Dim objTrainingApprover As New clstraining_approver_master

        Try
            
            dsList = GetAllocationList(False)
            If dsList IsNot Nothing Then
                Dim dtCol As New DataColumn
                dtCol.ColumnName = "IsChecked"
                dtCol.Caption = ""
                dtCol.DataType = System.Type.GetType("System.Boolean")
                dtCol.DefaultValue = False
                dsList.Tables(0).Columns.Add(dtCol)

                If CInt(drpATrainingCalendar.SelectedValue) > 0 AndAlso CInt(drpARole.SelectedValue) > 0 AndAlso CInt(drpALevel.SelectedValue) > 0 Then
                    If CInt(drpATrainingCalendar.SelectedValue) > 0 Then
                        StrSearch &= "AND TAM.calendarunkid = " & CInt(drpATrainingCalendar.SelectedValue) & " "
                    End If

                    If CInt(drpARole.SelectedValue) > 0 Then
                        StrSearch &= "AND TAM.roleunkid = " & CInt(drpARole.SelectedValue) & " "
                    End If

                    If CInt(drpALevel.SelectedValue) > 0 Then
                        StrSearch &= "AND TAM.levelunkid = " & CInt(drpALevel.SelectedValue) & " "
                    End If

                    If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

                    Dim dsAllocation As DataSet = objTrainingApprover.GetList("List", , StrSearch)

                    Dim strIDs As String = String.Join(",", (From p In dsAllocation.Tables(0) Select (p.Item("allocationunkid").ToString)).ToArray)
                    If strIDs.Trim <> "" Then
                        Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                        For Each dtRow As DataRow In row
                            dtRow.Item("IsChecked") = True
                        Next

                    End If
                
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvAllocationList.DataSource = dtTable
                dgvAllocationList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
        End Try
    End Sub

    Private Function GetAllocationList(Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim strName As String = ""
        Dim dsList As DataSet = Nothing
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDept As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Try
            Select Case mintTrainingApproverAllocationID
                Case enAllocation.BRANCH
                    dsList = objStation.getComboList("Station", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 430, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsList = objDeptGrp.getComboList("DeptGrp", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 429, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsList = objDept.getComboList("Department", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 428, "Department")
                Case enAllocation.SECTION_GROUP
                    dsList = objSectionGrp.getComboList("List", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 427, "Section Group")
                Case enAllocation.SECTION
                    dsList = objSection.getComboList("Section", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 426, "Section")
                Case enAllocation.UNIT_GROUP
                    dsList = objUnitGroup.getComboList("List", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 425, "Unit Group")
                Case enAllocation.UNIT
                    dsList = objUnit.getComboList("Unit", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 424, "Unit")
                Case enAllocation.TEAM
                    dsList = objTeam.getComboList("List", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 423, "Team")
                Case enAllocation.JOB_GROUP
                    dsList = objJobGrp.getComboList("JobGrp", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 422, "Job Group")
                Case enAllocation.JOBS
                    dsList = objJob.getComboList("Job", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 421, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsList = objClassGrp.getComboList("ClassGrp", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 420, "Class Group")
                Case enAllocation.CLASSES
                    dsList = objClass.getComboList("Class", blnAddSelect)
                    dsList.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage("clsMasterData", 419, "Classes")
                Case Else
                    dsList = Nothing
                    strName = "&nbsp;"
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDept = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
        End Try
        Return dsList
    End Function
    'Hemant (09 Feb 2022) -- End

#End Region

#Region "Button Methods"

    Protected Sub btnALNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnALNew.Click
        Try
            FillTrainingApproverCombo()
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            drpALAllocation.SelectedValue = CStr(mintTrainingApproverAllocationID)
            FillTrainingApproverAllocationList()
            'Hemant (09 Feb 2022) -- End
            mintMappingUnkid = 0
            drpARole.Enabled = True
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            'drpATrainingCalendar.Enabled = False
            drpATrainingCalendar.Enabled = True
            ClearCtrlsTrainingApprover()
            'Hemant (03 Dec 2021) -- End

            blnpopupTrainingApprover = True
            popupTrainingApprover.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnASave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnASave.Click
        Dim objTrainingApprover As New clstraining_approver_master
        Dim strMessage As String = ""

        Dim blnFlag As Boolean = False
        Try
            If IsTrainingApproverValid() = False Then Exit Sub

            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            'objTrainingApprover._Mappingunkid = mintMappingUnkid
            'objTrainingApprover._Calendarunkid = CInt(drpATrainingCalendar.SelectedValue)
            'objTrainingApprover._Levelunkid = CInt(drpALevel.SelectedValue)
            'objTrainingApprover._Roleunkid = CInt(drpARole.SelectedValue)
            'objTrainingApprover._Isvoid() = False
            'objTrainingApprover._Voiddatetime() = Nothing
            'objTrainingApprover._Voiduserunkid() = -1
            'objTrainingApprover._Voidreason() = ""
            'objTrainingApprover._IsFromWeb = True
            'objTrainingApprover._FormName = mstrModuleName
            'objTrainingApprover._AuditUserId = CInt(Session("UserId"))
            'objTrainingApprover._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            'objTrainingApprover._ClientIP = CStr(Session("IP_ADD"))
            'objTrainingApprover._HostName = CStr(Session("HOST_NAME"))


            'If mintMappingUnkid > 0 Then
            '    blnFlag = objTrainingApprover.Update()
            'Else
            '    blnFlag = objTrainingApprover.Insert()
            'End If
            Dim grow As IEnumerable(Of GridViewRow) = Nothing
            Dim gNewRow As List(Of GridViewRow) = Nothing
            Dim strIDs As String
            Dim StrSearch As String = String.Empty

            '*** Allocation Items - Start ***
            Dim lstTrainingApprover As New List(Of clstraining_approver_master)
            Dim lstVoidTrainingApprover As New List(Of clstraining_approver_master)
            grow = dgvAllocationList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocation"), CheckBox).Checked = True).ToList
            strIDs = String.Join(",", dgvAllocationList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocation"), CheckBox).Checked = True).Select(Function(x) dgvAllocationList.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

            
            If CInt(drpATrainingCalendar.SelectedValue) > 0 Then
                StrSearch &= "AND TAM.calendarunkid = " & CInt(drpATrainingCalendar.SelectedValue) & " "
            End If

            If CInt(drpARole.SelectedValue) > 0 Then
                StrSearch &= "AND TAM.roleunkid = " & CInt(drpARole.SelectedValue) & " "
            End If

            If CInt(drpALevel.SelectedValue) > 0 Then
                StrSearch &= "AND TAM.levelunkid = " & CInt(drpALevel.SelectedValue) & " "
            End If

            If CInt(mintTrainingApproverAllocationID) > 0 Then
                StrSearch &= "AND TAM.allocationid = " & CInt(mintTrainingApproverAllocationID) & " "
            End If


            If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

            Dim dsAllocation As DataSet = objTrainingApprover.GetList("List", , StrSearch)

            Dim drVoid As List(Of DataRow) = (From p In dsAllocation.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("allocationunkid").ToString) = False) Select (p)).ToList
            Dim strExistIDs As String = String.Join(",", (From p In dsAllocation.Tables(0) Select (p.Item("allocationunkid").ToString)).ToArray)
            gNewRow = dgvAllocationList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocation"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvAllocationList.DataKeys(x.RowIndex).Item("id").ToString) = False).Select(Function(x) x).ToList

            For Each r As DataRow In drVoid
                objTrainingApprover = New clstraining_approver_master
                With objTrainingApprover
                    .pintMappingunkid = CInt(r.Item("mappingunkid"))

                    .pblnIsvoid = True
                    .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    .pstrVoidreason = ""

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintVoiduserunkid = CInt(Session("UserId"))
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintVoiduserunkid = 0
                        .pintAuditUserId = 0
                    End If
                    .pblnIsFromWeb = True
                    .pdtAuditDatetime = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName
                End With

                lstVoidTrainingApprover.Add(objTrainingApprover)
            Next

            grow = gNewRow

            For Each dgRow As GridViewRow In grow
                objTrainingApprover = New clstraining_approver_master
                With objTrainingApprover
                    .pintMappingunkid = -1
                    .pintCalendarunkid = CInt(drpATrainingCalendar.SelectedValue)
                    .pintRoleunkid = CInt(drpARole.SelectedValue)
                    .pintLevelunkid = CInt(drpALevel.SelectedValue)
                    .pintAllocationId = mintTrainingApproverAllocationID
                    .pintAllocationunkid = CInt(dgvAllocationList.DataKeys(dgRow.RowIndex)("id").ToString())
                    .pblnIsFromWeb = True
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintAuditUserId = 0
                    End If
                    .pdtAuditDatetime = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIP = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                lstTrainingApprover.Add(objTrainingApprover)
            Next

            objTrainingApprover._lstTrainingApproverNew = lstTrainingApprover
            objTrainingApprover._lstTrainingApproverVoid = lstVoidTrainingApprover
            '*** Allocation Items - End ***

            'Hemant (09 Feb 2022) -- End


            If objTrainingApprover.Save() = False And objTrainingApprover._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApprover._Message, Me)
                blnpopupTrainingApprover = True
                popupTrainingApprover.Show()

            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Training Approver Saved successfully"), Me)

                drpARole.SelectedIndex = 0
                drpALevel.SelectedIndex = 0

                mintMappingUnkid = 0

                blnpopupTrainingApprover = False
                popupTrainingApprover.Hide()
                Call FillTrainingApproverList(False)
            End If



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
        End Try

    End Sub

    Protected Sub btnALReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnALReset.Click
        Try
            drpALLevel.SelectedIndex = 0
            drpALRole.SelectedIndex = 0
            Call FillTrainingApproverList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub drpARole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpARole.SelectedIndexChanged
        Try
            'Hemant (09 Feb 2022) -- Start            
            'OLD-549(NMB) : Give new screen for training approver allocation mapping
            If CInt(drpATrainingCalendar.SelectedValue) > 0 AndAlso CInt(drpARole.SelectedValue) > 0 AndAlso CInt(drpALevel.SelectedValue) > 0 Then
                FillTrainingApproverAllocationList()
            End If
            popupTrainingApprover.Show()
            blnpopupTrainingApprover = True
            'Hemant (09 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Hemant (03 Dec 2021) -- Start
    'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    Private Sub drpALTrainingCalendar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpALTrainingCalendar.SelectedIndexChanged
        Try
            drpPeriod_SelectedIndexChanged(sender, drpALLevel)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub drpATrainingCalendar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpATrainingCalendar.SelectedIndexChanged
        Try
            drpPeriod_SelectedIndexChanged(sender, drpALevel)
            popupTrainingApprover.Show()
            blnpopupTrainingApprover = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (03 Dec 2021) -- End

    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Protected Sub drpALevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpALevel.SelectedIndexChanged
        Try
            If CInt(drpATrainingCalendar.SelectedValue) > 0 AndAlso CInt(drpARole.SelectedValue) > 0 AndAlso CInt(drpALevel.SelectedValue) > 0 Then
                FillTrainingApproverAllocationList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupTrainingApprover.Show()
            blnpopupTrainingApprover = True
        End Try
    End Sub

    Protected Sub drpALAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpALAllocation.SelectedIndexChanged
        Dim dsList As DataSet
        Try
            If CInt(drpALAllocation.SelectedValue) > 0 Then

                dsList = GetAllocationList(True)
                With drpALAllocationName
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing
        End Try
    End Sub
    'Hemant (09 Feb 2022) -- End

#End Region

#Region " Gridview Event "

    Protected Sub gvApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvApproverList.DataKeys(e.Row.RowIndex)("mappingunkid").ToString) > 0 Then
                    oldid = dt.Rows(e.Row.RowIndex)("Level").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("Level").ToString(), gvApproverList)
                        currentId = oldid
                    Else

                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkALdelete As LinkButton = TryCast(e.Row.FindControl("lnkALdelete"), LinkButton)

                        If CBool(Session("AllowToDeleteTrainingApprover")) Then
                            lnkALdelete.Visible = True
                        Else
                            lnkALdelete.Visible = False
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

#End Region

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            blnpopupTrainingApprover = False
            popupTrainingApprover.Hide()
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnALSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnALSearch.Click
        Try
            FillTrainingApproverList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkALEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Try
            Dim lnkALEdit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkALEdit).NamingContainer, GridViewRow)

            objTrainingApproverMaster._Mappingunkid = CInt(lnkALEdit.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkALEdit.CommandArgument.ToString())

            objTrainingApproverMaster._Mappingunkid = mintMappingUnkid
            SetValueToPopup(objTrainingApproverMaster)
            popupTrainingApprover.Show()
            blnpopupTrainingApprover = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try
    End Sub

    Protected Sub lnkALdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objTrainingApproverMaster._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkdelete.CommandArgument.ToString())

            mstrDeleteAction = "delapproval"
            cnfConfirm.Title = "Confirmation"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Are You Sure You Want To Delete This Approver?")
            cnfConfirm.Show()

            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try
    End Sub

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELLEVEL"

                    SetAtValueApprovalLevel()
                    blnFlag = objTrainingApprLvlMaster.Delete(mintLevelmstid)

                    If blnFlag = False And objTrainingApprLvlMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTrainingApprLvlMaster._Message, Me)
                    Else
                        FillListApprovalLevel(False)
                        ListFillCombo()
                        FillTrainingApproverCombo()
                        mintLevelmstid = 0
                    End If

                Case "DELMATRIX"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Enter reason for deleting this Approval Matrix")
                    delReason.Show()
                Case "DELAPPROVAL"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Enter reason for deleting this Training Approver")
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Delete Reason"

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()

                Case "DELMATRIX"
                    Dim objApprovalMatrix As New clsTraining_Approval_Matrix
                    objApprovalMatrix.pblnIsvoid = True
                    objApprovalMatrix.pintVoiduserunkid = CInt(Session("UserId"))
                    objApprovalMatrix.pstrVoidreason = delReason.Reason
                    objApprovalMatrix.pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objApprovalMatrix.pstrFormName = mstrModuleName
                    objApprovalMatrix.pstrClientIP = CStr(Session("IP_ADD"))
                    objApprovalMatrix.pstrHostName = CStr(Session("HOST_NAME"))
                    objApprovalMatrix.pintAuditUserId = CInt(Session("UserId"))
                    objApprovalMatrix.pblnIsWeb = True

                    If objApprovalMatrix.Delete(mintApproverMatrixUnkid) = False Then
                        DisplayMessage.DisplayMessage(objApprovalMatrix._Message, Me)
                        Exit Sub
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Approval Matrix deleted Successfully"), Me)
                        FillApprovalMatrix()
                        ClearCtrls()
                    End If
                    objApprovalMatrix = Nothing

                Case "DELAPPROVAL"
                    Dim objTrainingApproverMaster As New clstraining_approver_master
                    SetAtValue(objTrainingApproverMaster)
                    objTrainingApproverMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objTrainingApproverMaster._Voidreason = delReason.Reason
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        objTrainingApproverMaster._Voiduserunkid = CInt(Session("UserId"))
                    End If

                    If objTrainingApproverMaster.Delete(CInt(mintMappingUnkid)) = False Then
                        DisplayMessage.DisplayMessage(objTrainingApproverMaster._Message, Me)
                        Exit Sub
                    End If
                    FillTrainingApproverList(False)

                    objTrainingApproverMaster = Nothing
                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprovalLevels.ID, lblApprovalLevels.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprovalMatrix.ID, lblApprovalMatrix.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingApprover.ID, lblTrainingApprover.Text)

            'Approval Levels            
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApproverLevelsCaption.ID, lblApproverLevelsCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCancelText1.ID, lblCancelText1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCalendarAppLevel.ID, lblTrainingCalendarAppLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllevelcode.ID, lbllevelcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllevelname.ID, lbllevelname.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllevelpriority.ID, lbllevelpriority.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnnew.ID, btnnew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSaveApprover.ID, btnSaveApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClosApprover.ID, btnClosApprover.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, GvApprLevelList.Columns(5).FooterText, GvApprLevelList.Columns(5).HeaderText)

            'Approval Matrix
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAddEditApprovalMatrix.ID, lblAddEditApprovalMatrix.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCalendar.ID, lblTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLevel.ID, lblLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCostAmountFrom.ID, lblCostAmountFrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCostAmountTo.ID, lblCostAmountTo.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSave.ID, btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnReset.ID, btnReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvApprovalMatrix.Columns(2).FooterText, gvApprovalMatrix.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvApprovalMatrix.Columns(3).FooterText, gvApprovalMatrix.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvApprovalMatrix.Columns(4).FooterText, gvApprovalMatrix.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvApprovalMatrix.Columns(5).FooterText, gvApprovalMatrix.Columns(5).HeaderText)

            'TrainingApprover
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblALCaption.ID, lblALCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblALTrainingCalendar.ID, lblALTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblALLevel.ID, lblALLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblALRole.ID, lblALRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Label3.ID, Label3.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblATrainingCalendar.ID, lblATrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblARole.ID, lblARole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblALevel.ID, lblALevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnALNew.ID, btnALNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnALSearch.ID, btnALSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnALReset.ID, btnALReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnASave.ID, btnASave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnApproverUseraccessClose.ID, btnApproverUseraccessClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvApproverList.Columns(2).FooterText, gvApproverList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblApprovalLevels.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprovalLevels.ID, lblApprovalLevels.Text)
            Me.lblApprovalMatrix.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprovalMatrix.ID, lblApprovalMatrix.Text)
            Me.lblTrainingApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingApprover.ID, lblTrainingApprover.Text)

            'Approval Levels            
            Me.lblApproverLevelsCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApproverLevelsCaption.ID, lblApproverLevelsCaption.Text)
            Me.lblCancelText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCancelText1.ID, lblCancelText1.Text)
            Me.lblTrainingCalendarAppLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCalendarAppLevel.ID, lblTrainingCalendarAppLevel.Text)
            Me.lbllevelcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllevelcode.ID, lbllevelcode.Text)
            Me.lbllevelname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllevelname.ID, lbllevelname.Text)
            Me.lbllevelpriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllevelpriority.ID, lbllevelpriority.Text)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnnew.ID, btnnew.Text).Replace("&", "")
            Me.btnSaveApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSaveApprover.ID, btnSaveApprover.Text).Replace("&", "")
            Me.btnClosApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClosApprover.ID, btnClosApprover.Text).Replace("&", "")

            GvApprLevelList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            GvApprLevelList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            GvApprLevelList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)
            GvApprLevelList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), GvApprLevelList.Columns(5).FooterText, GvApprLevelList.Columns(5).HeaderText)

            'Approval Matrix
            Me.lblAddEditApprovalMatrix.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAddEditApprovalMatrix.ID, lblAddEditApprovalMatrix.Text)
            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCalendar.ID, lblTrainingCalendar.Text)
            Me.lblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLevel.ID, lblLevel.Text)
            Me.lblCostAmountFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCostAmountFrom.ID, lblCostAmountFrom.Text)
            Me.lblCostAmountTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCostAmountTo.ID, lblCostAmountTo.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSave.ID, btnSave.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnReset.ID, btnReset.Text).Replace("&", "")

            gvApprovalMatrix.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvApprovalMatrix.Columns(2).FooterText, gvApprovalMatrix.Columns(2).HeaderText)
            gvApprovalMatrix.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvApprovalMatrix.Columns(3).FooterText, gvApprovalMatrix.Columns(3).HeaderText)
            gvApprovalMatrix.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvApprovalMatrix.Columns(4).FooterText, gvApprovalMatrix.Columns(4).HeaderText)
            gvApprovalMatrix.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvApprovalMatrix.Columns(5).FooterText, gvApprovalMatrix.Columns(5).HeaderText)

            'TrainingApprover
            Me.lblALCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblALCaption.ID, lblALCaption.Text)
            Me.lblALTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblALTrainingCalendar.ID, lblALTrainingCalendar.Text)
            Me.lblALLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblALLevel.ID, lblALLevel.Text)
            Me.lblALRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblALRole.ID, lblALRole.Text)
            Me.Label3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Label3.ID, Label3.Text)
            Me.lblATrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblATrainingCalendar.ID, lblATrainingCalendar.Text)
            Me.lblARole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblARole.ID, lblARole.Text)
            Me.lblALevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblALevel.ID, lblALevel.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, lblPageHeader.Text)

            Me.btnALNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnALNew.ID, btnALNew.Text).Replace("&", "")
            Me.btnALSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnALSearch.ID, btnALSearch.Text).Replace("&", "")
            Me.btnALReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnALReset.ID, btnALReset.Text).Replace("&", "")
            Me.btnASave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnASave.ID, btnASave.Text).Replace("&", "")
            Me.btnApproverUseraccessClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnApproverUseraccessClose.ID, btnApproverUseraccessClose.Text).Replace("&", "")

            gvApproverList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvApproverList.Columns(2).FooterText, gvApproverList.Columns(2).HeaderText)
            gvApproverList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)


            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Level is mandatory information. Please select Level to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Approval Matrix Saved successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Approver Level is compulsory information. Please Select Approver Level")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Role cannot be blank. Role is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, this cost range is already defined to another approver level")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, this cost range is not valid")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "You are about to delete this Approval Matrix Entry. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, approval matrix for the selected level is already defined for this calendar. Please select another Level to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Enter reason for deleting this Approval Matrix")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Enter reason for deleting this Training Approver")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Training Approver Saved successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Approval Matrix deleted Successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Approver Level Priority cannot be less than Zero. Approver Level Priority is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Approver Level Name cannot be blank. Approver Level Name is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Are You Sure You Want To Delete This Approver?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "Sorry, Defined Cost Amount is too large")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Allocation is compulsory information.Please Check atleast One Allocation Item.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, Selected Role is already defined to another Level for this calendar. Please select another Role to continue")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
