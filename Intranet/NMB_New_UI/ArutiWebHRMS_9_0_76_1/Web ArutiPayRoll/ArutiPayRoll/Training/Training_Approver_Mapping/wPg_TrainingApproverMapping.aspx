﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingApproverMapping.aspx.vb"
    Inherits="Training_Training_Approver_Mapping_wPg_TrainingApproverMapping" Title="Training Approver Mapping" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }   
        
        function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Approver Mapping" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#ApprovalLevels" data-toggle="tab">
                                        <asp:Label ID="lblApprovalLevels" runat="server" Text="Approval Levels"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#ApprovalMatrix" data-toggle="tab">
                                        <asp:Label ID="lblApprovalMatrix" runat="server" Text="Approval Matrix"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingApprover" data-toggle="tab">
                                        <asp:Label ID="lblTrainingApprover" runat="server" Text="Training Approver"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="ApprovalLevels">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblApproverLevelsCaption" runat="server" Text="Approver Levels" CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 350px;">
                                                                        <asp:Panel ID="pnl_dgView" runat="server" ScrollBars="Auto">
                                                                            <asp:GridView ID="GvApprLevelList" DataKeyNames="levelunkid" runat="server" AutoGenerateColumns="False"
                                                                                CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("levelunkid") %>'>
                                                                                                 <i class="fas fa-pencil-alt"></i>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("levelunkid") %>'> 
                                                                                                 <i class="fas fa-trash text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="levelcode" HeaderText="Level Code" HeaderStyle-Width="20%"
                                                                                        FooterText="colhlevelname"></asp:BoundField>
                                                                                    <asp:BoundField DataField="levelname" HeaderText="Level Name" HeaderStyle-Width="25%"
                                                                                        FooterText="colhlevelname"></asp:BoundField>
                                                                                    <asp:BoundField DataField="priority" HeaderText="Priority Level" HeaderStyle-Width="15%"
                                                                                        FooterText="colhpriority" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="calendar" HeaderText="Calendar" HeaderStyle-Width="25%"
                                                                                        FooterText="colhcalendar"></asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnnew" runat="server" CssClass="btn btn-primary" Text="New" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="ApprovalMatrix">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblAddEditApprovalMatrix" runat="server" Text="Add/Edit Approval Matrix"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar" Width="100%"
                                                                        CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpTrainingCalendar" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpLevel" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblCostAmountFrom" runat="server" Text="Cost Amount From" CssClass="form-label" />
                                                                    <nut:NumericText ID="txtCostAmountFrom" runat="server" Type="Point" />
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblCostAmountTo" runat="server" Text="Cost Amount To" CssClass="form-label" />
                                                                    <nut:NumericText ID="txtCostAmountTo" runat="server" Type="Point" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 350px;">
                                                                        <asp:GridView ID="gvApprovalMatrix" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="table table-hover table-bordered" RowStyle-Wrap="false" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" DataKeyNames="calendarunkid,trainingapprovalmatrixunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkApprovalMatrixEdit" runat="server" ToolTip="Select" OnClick="lnkApprovalMatrixEdit_Click">
                                                                                                <i class="fas fa-pencil-alt text-primary"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkApprovalMatrixDelete" runat="server" ToolTip="Delete" OnClick="lnkApprovalMatrixDelete_Click">
                                                                                            <i class="fas fa-trash text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="calendar_name" HeaderText="Calendar Name" ReadOnly="True"
                                                                                    FooterText="colhCalendarName"></asp:BoundField>
                                                                                <asp:BoundField DataField="approver_level" HeaderText="Approver Level" ReadOnly="True"
                                                                                    FooterText="colhApproverLevel"></asp:BoundField>
                                                                                <asp:BoundField DataField="costamountfrom" HeaderText="Cost Amount From" ReadOnly="True"
                                                                                    FooterText="colhcostamountfrom"></asp:BoundField>
                                                                                <asp:BoundField DataField="costamountto" HeaderText="Cost Amount To" ReadOnly="True"
                                                                                    FooterText="colhcostamountto"></asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingApprover">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblALCaption" runat="server" Text="Approver(s) List" CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblALTrainingCalendar" runat="server" Text="Calendar" Width="100%"
                                                                        CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpALTrainingCalendar" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblALLevel" runat="server" Text="Approver Level" Width="100%" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpALLevel" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblALRole" runat="server" Text="Role" Width="100%" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpALRole" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblALAllocation" runat="server" Text="Allocation" Width="100%"
                                                                        CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpALAllocation" runat="server" Enabled= "false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblALAllocationName" runat="server" Text="Name" Width="100%" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="drpALAllocationName" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer">
                                                            <asp:Button ID="btnALNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                                            <asp:Button ID="btnALSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                                            <asp:Button ID="btnALReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card">
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 400px;">
                                                                        <asp:GridView ID="gvApproverList" DataKeyNames="mappingunkid" runat="server" AutoGenerateColumns="False"
                                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkALEdit" runat="server" ToolTip="Select" OnClick="lnkALEdit_Click"
                                                                                                CommandArgument='<%#Eval("mappingunkid") %>'>
                                                                                                    <i class="fas fa-pencil-alt text-primary" ></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkALdelete" runat="server" CssClass="griddelete" OnClick="lnkALdelete_Click"
                                                                                            CommandArgument='<%#Eval("mappingunkid") %>' ToolTip="Delete">
                                                                                                <i class="fas fa-trash text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField HeaderText="Role" DataField="Role" ReadOnly="true" FooterText="colhRole" />
                                                                                <asp:BoundField HeaderText="Calendar" DataField="Calendar" ReadOnly="true" FooterText="colhCalendar" />
                                                                                <asp:BoundField HeaderText="Allocation" DataField="allocationname" ReadOnly="true" FooterText="colhAllocation" />
                                                                                <asp:BoundField HeaderText="Name" DataField="allocationitem" ReadOnly="true" FooterText="colhAllocationItem" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <cc1:ModalPopupExtender ID="popupApproverLevel" BackgroundCssClass="modal-backdrop"
                    TargetControlID="txtlevelcode" runat="server" PopupControlID="pnlApprover" DropShadow="false"
                    CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlApprover" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCancelText1" Text="Approver Level Add/ Edit" runat="server" CssClass="form-label" />
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingCalendarAppLevel" runat="server" Text="Training Calendar"
                                            Width="100%" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpTrainingCalendarAppLevel" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevelcode" runat="server" Text="Level Code" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtlevelcode" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtlevelname" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevelpriority" runat="server" Text="Priority (Smaller number means lower level approver)"
                                            CssClass="form-label" />
                                        <nut:NumericText ID="txtlevelpriority" runat="server" Type="Numeric" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSaveApprover" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClosApprover" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupTrainingApprover" BackgroundCssClass="modal-backdrop"
                    TargetControlID="Label3" runat="server" PopupControlID="PanelTrainingApprover"
                    CancelControlID="HvApproverCancel" />
                <asp:Panel ID="PanelTrainingApprover" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="Label3" runat="server" Text="Add/ Edit Training Approver" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblATrainingCalendar" runat="server" Text="Training Calendar" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="drpATrainingCalendar" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblARole" runat="server" Text="Role" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="drpARole" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblALevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="drpALevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <%--'Hemant (09 Feb 2022) -- Start--%>
                                    <%--'OLD-549(NMB) : Give new screen for training approver allocation mapping--%>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAAllocation" runat="server" Text="Allocation" Width="100%" CssClass="form-label" />
                                            <div class="table-responsive" style="max-height: 210px;">
                                                <asp:GridView ID="dgvAllocationList" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                    DataKeyNames="id" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-CssClass="align-center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAllAllocation" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAllocation');" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkgvSelectAllocation" runat="server" Text=" " CssClass="chk-sm"
                                                                    Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this, 'ChkgvSelectAllocation', 'ChkAllAllocation')" />
                                                            </ItemTemplate>
                                                            <HeaderStyle />
                                                            <ItemStyle />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <%--'Hemant (09 Feb 2022) -- End--%>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnASave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="HvApproverCancel" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
	    $(document).ready(function()
		{
		    RetriveTab();
            
        });
         function RetriveTab() {
 
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "ApprovalLevels";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                debugger;
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }

    </script>

</asp:Content>
