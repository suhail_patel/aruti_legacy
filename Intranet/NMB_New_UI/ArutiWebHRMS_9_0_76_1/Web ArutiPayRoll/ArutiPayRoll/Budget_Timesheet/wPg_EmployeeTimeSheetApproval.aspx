﻿<%@ Page Title="Timesheet Approval" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeTimeSheetApproval.aspx.vb" Inherits="Budget_Timesheet_wPg_EmployeeTimeSheetApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/MaskedTimeTextBox.ascx" TagName="MaskedTimeTextBox"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Timesheet Approval"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 400px">
                                                            <asp:DataGrid ID="dgvEmpTimesheet" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered">
                                                                <ItemStyle CssClass="griviewitem" />
                                                                <Columns>
                                                                    <asp:TemplateColumn FooterText="objdgcolhShowDetails">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgShowDetails" runat="server" ImageUrl="~/images/Info_icons.png"
                                                                                CommandName="Show Details" ToolTip="Show Project Hour Details" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Particulars" FooterText="dgcolhParticular" HeaderText="Particular"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="approvaldate" FooterText="dgcolhActivityDate" HeaderText="Approval Date" HeaderStyle-Wrap ="false"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="activity_name" FooterText="dgcolhActivity" HeaderText="Activity Code"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Approver" FooterText="dgcolhApprover" HeaderText="Approver"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="fundname" FooterText="dgcolhDonor" HeaderText="Donor/Grant"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="fundprojectname" FooterText="dgcolhProject" HeaderText="Project Code"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="description" FooterText="dgcolhEmpDescription" HeaderText="Description"
                                                                        ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="submission_remark" FooterText="dgcolhSubmissionRemark"
                                                                        HeaderText="Submission Remark" ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TotalActivityHours" FooterText="dgcolhTotalActivityHours"
                                                                        HeaderText="Total Activity Hours" ReadOnly="true"></asp:BoundColumn>
                                                                    <asp:TemplateColumn FooterText="dgcolhEmpHours" HeaderText="Hours : Mins" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="225px"
                                                                        ItemStyle-Width="225px">
                                                                        <ItemTemplate>
                                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtHours" runat="server" AutoPostBack="true" MaxLength="2" onKeypress="return onlyNumbers(this, event);"
                                                                                            OnTextChanged="txtHours_TextChanged" Style="text-align: right" Width="100%" CssClass="form-control" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-15">
                                                                                <asp:Label ID="lblColon" runat="server" Text=":" CssClass="form-label" />
                                                                            </div>
                                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtMinutes" runat="server" AutoPostBack="true" MaxLength="2" onKeypress="return onlyNumbers(this, event);"
                                                                                            OnTextChanged="txtMinutes_TextChanged" Style="text-align: right" Width="100%"
                                                                                            CssClass="form-control" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="activity_hrs" FooterText="dgcolhHours" HeaderText="Activity Hours"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="activity_hrsinMins" FooterText="objdgcolhActivityHrsInMins"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="percentage" FooterText="objdgcolhActivityPercentage"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhPeriodID" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeID" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="fundsourceunkid" FooterText="objdgcolhFundSourceID" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="projectcodeunkid" FooterText="objdgcolhProjectID" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="activityunkid" FooterText="objdgcolhActivityID" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrp" ReadOnly="true" Visible="false">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="mapuserunkid" FooterText="objdgcolhMappedUserId" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ApprovalStatusId" FooterText="objdgcolhStatusID" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Employee" FooterText="objdgcolhEmployeeName" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="approveremployeeunkid" FooterText="objdgcolhApproverEmployeeID"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="activitydate" FooterText="objdgcolhActivityDate" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="priority" FooterText="objdgcolhPriority" ReadOnly="true"
                                                                        Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="emptimesheetunkid" FooterText="objdgcolhEmpTimesheetID"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="timesheetapprovalunkid" FooterText="objdgcolhTimesheetApprovalID"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="isexternalapprover" FooterText="objdgcolhIsExternalApprover"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TotalActivityHoursInMin" FooterText="objdgcolhTotalActivityHoursInMin"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ShiftHoursInSec" FooterText="objdgcolhShiftHoursInSec"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="isholiday" HeaderText="" FooterText="objdgcolhIsHoliday"
                                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupShowDetails" runat="server" BackgroundCssClass="modal-backdrop"
                        TargetControlID="hdf_Details" PopupControlID="pnlShowDetails" CancelControlID="hdf_Details">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlShowDetails" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Project & Activity Hours Details"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="LblShowDetailsEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="objLblEmpVal" runat="server" Text="#Employee" CssClass="form-label" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="LblShowDetailsPeriod" runat="server" Text="Period" CssClass="form-label" />
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="objLblPeriodVal" runat="server" Text="#Period" CssClass="form-label" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="Label1" runat="server" Text="Project" CssClass="form-label" />
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="objLblProjectVal" runat="server" Text="#Project" CssClass="form-label" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="LblDonorGrant" runat="server" Text="Donor/Grant" CssClass="form-label" />
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="objLblDonorGrantVal" runat="server" Text="#Donor/Grant" CssClass="form-label" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="Label2" runat="server" Text="Activity" CssClass="form-label" />
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="objLblActivityVal" runat="server" Text="#Activity" CssClass="form-label" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 300px;">
                                        <asp:DataGrid ID="dgvProjectHrDetails" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" CssClass="table table-hover table-bordered">
                                            <Columns>
                                                <asp:BoundColumn DataField="Particulars" HeaderText="Particulars" ReadOnly="true"
                                                    FooterText="dgcolhParticulars"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Hours" HeaderText="Hours" ReadOnly="true" FooterText="dgcolhHours">
                                                </asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnShowDetailsClose" runat="server" CssClass=" btn btn-primary" Text="Close" />
                            <asp:HiddenField ID="hdf_Details" runat="server" />
                        </div>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
