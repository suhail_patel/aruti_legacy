﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
'Nilay (27 Feb 2017) -- Start
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
'Nilay (27 Feb 2017) -- End
#End Region

Partial Class Budget_Timesheet_wPg_EmployeeTimeSheetApprovalList
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTimesheetApprovalList"
    Private ReadOnly mstrModuleName1 As String = "frmProjectHoursDetails"

    Private DisplayMessage As New CommonCodes
    Private objTimesheetApproval As clstsemptimesheet_approval
    Private objApprover As clstsapprover_master
    Private objMapping As clsapprover_Usermapping
    Private mstrEmployeeIDs As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    'Private mdtApproveList As DataTable = Nothing
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private mdic As Dictionary(Of String, Integer)
    Private mdtTimesheet As DataTable = Nothing
    Private objCONN As SqlConnection
    Private mintPeriodID As Integer = -1
    Private mintApproverID As Integer = -1
    Private mdtPStartDate As Date = Nothing
    Private mdtPEndDate As Date = Nothing

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length > 0 Then
                    mintPeriodID = CInt(arr(0))
                    mintApproverID = CInt(arr(1))

                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(2))
                    HttpContext.Current.Session("UserId") = CInt(arr(3))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    Call SetDateFormat()

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))

                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                End If

                Dim objTimesheetApproval As New clstsemptimesheet_approval
                Dim strFilter As String = "AND tsemptimesheet_approval.periodunkid=" & mintPeriodID & " " & _
                                          "AND tsemptimesheet_approval.tsapproverunkid=" & mintApproverID & " " & _
                                          "AND ltbemployee_timesheet.statusunkid=2 "
                If strFilter.Trim.Length > 0 Then
                    strFilter = strFilter.Substring(3)
                End If

                Dim dsList As DataSet = objTimesheetApproval.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     CStr(Session("DateFormat")), CInt(Session("UserId")), True, CBool(Session("AllowOverTimeToEmpTimesheet")), Nothing, True, -1, False, _
                                                                     strFilter)

                If dsList.Tables("List").Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage("There is no any pending Timesheet for aaproval.", Me.Page, Session("rootpath").ToString & "Index.aspx")
                    Exit Sub
                End If

                GoTo Link
            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If
Link:
            mdic = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheet.Columns.IndexOf(x))

            If IsPostBack = False Then
                Call SetLanguage()

                Call FillCombo()

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then

                    lnkAllocation.Visible = False
                    btnChangeStatus.Visible = False
                    cboApproverStatus.SelectedValue = "0"
                    cboStatus.SelectedValue = "0"
                    dgvEmpTimesheet.Columns(0).Visible = False
                    cboApprover.Enabled = False

                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                    lnkAllocation.Visible = True 'Nilay (10 Jan 2017)
                    btnChangeStatus.Visible = True
                    dgvEmpTimesheet.Columns(0).Visible = True
                    dgvEmpTimesheet.DataSource = New List(Of String)
                    dgvEmpTimesheet.DataBind()
                    If Request.QueryString.Count > 0 Then
                        cboPeriod.SelectedValue = mintPeriodID.ToString
                        cboPeriod.Enabled = False
                        cboApprover.Enabled = False
                        cboApprover.SelectedValue = mintApproverID.ToString
                        cboApproverStatus.Enabled = False
                        cboStatus.Enabled = False
                        Call btnSearch_Click(btnSearch, New EventArgs())
                    End If
                    SetVisibility()
                End If


                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then Call FillList()
            Else
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
                'mdtApproveList = CType(Me.ViewState("ApproveListTable"), DataTable)
                mintApproverID = CInt(Me.ViewState("ApproverId"))
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate")).Date
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate")).Date
                mdtTimesheet = CType(Me.ViewState("TimesheetDataTable"), DataTable)
                mdtPStartDate = CDate(Me.ViewState("mdtPStartDate")).Date
                mdtPEndDate = CDate(Me.ViewState("mdtPEndDate")).Date
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Me.ViewState("ApproveListTable") = mdtApproveList
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("TimesheetDataTable") = mdtTimesheet
            Me.ViewState("mdtPStartDate") = mdtPStartDate
            Me.ViewState("mdtPEndDate") = mdtPEndDate
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objMaster As New clsMasterData

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")).Date, "List", True)

            Dim intFirstPeriodID As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN, , True)

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = intFirstPeriodID.ToString

            End With
            objPeriod = Nothing
            dsList = Nothing

            Dim objtsApprMaster As New clstsapprover_master
            dsList = objtsApprMaster.GetLevelFromUserLogin(CInt(Session("UserId")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim row As DataRow = dsList.Tables(0).NewRow
                row.Item("tsapproverunkid") = -1
                row.Item("Approver") = "Select"
                dsList.Tables(0).Rows.Add(row)
            End If
          
            With cboApprover
                .DataValueField = "tsapproverunkid"
                .DataTextField = "Approver"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            objtsApprMaster = Nothing

            Dim objFundCode As New clsFundSource_Master
            dsList = objFundCode.GetComboList("List", True)
            With cboDonor
                .DataValueField = "fundsourceunkid"
                .DataTextField = "fundname"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objFundCode = Nothing
            dsList = Nothing


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'Dim dtList As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable
            Dim dtList As DataTable = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable
            'Pinkal (03-Jan-2020) -- End


            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtList
                .DataBind()
                .SelectedValue = "2"
            End With

            With cboApproverStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtList.Copy
                .DataBind()
                .SelectedValue = "2"
            End With

            Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
            Call cboDonor_SelectedIndexChanged(cboDonor, New EventArgs())
            Call cboProject_SelectedIndexChanged(cboProject, New EventArgs())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Try

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewEmployeeBudgetTimesheetApprovalList")) = False Then Exit Sub
            End If

            Dim dsList As New DataSet
            Dim strSearching As String = ""
            objTimesheetApproval = New clstsemptimesheet_approval

            If CInt(cboPeriod.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.periodunkid =" & CInt(cboPeriod.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CDate(dtpDate.GetDate).Date <> Nothing Then
                strSearching &= "AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) = '" & eZeeDate.convertDate(dtpDate.GetDate) & "' "
            End If

            If CInt(cboDonor.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.fundsourceunkid = " & CInt(cboDonor.SelectedValue) & " "
            End If

            If CInt(cboProject.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.projectcodeunkid = " & CInt(cboProject.SelectedValue) & " "
            End If

            If CInt(cboActivity.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.activityunkid = " & CInt(cboActivity.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearching &= "AND ltbemployee_timesheet.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If CInt(cboApproverStatus.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.statusunkid = " & CInt(cboApproverStatus.SelectedValue) & " "
            End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.tsapproverunkid = " & CInt(cboApprover.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter & " "
            End If

            strSearching &= "AND tsemptimesheet_approval.visibleunkid <> -1 " & " "

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            dsList = objTimesheetApproval.GetList("List", CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                  CStr(Session("DateFormat")), CInt(Session("UserId")), True, CBool(Session("AllowOverTimeToEmpTimesheet")), Nothing, True, -1, True, strSearching)

            'mdtApproveList = dsList.Tables("List")
            mdtTimesheet = dsList.Tables("List")

            Dim xTimesheetApprovalunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim strStaus As String = ""

            If mdtTimesheet IsNot Nothing AndAlso mdtTimesheet.Rows.Count > 0 Then

                For Each drRow As DataRow In mdtTimesheet.Rows
                    If CInt(drRow("timesheetapprovalunkid")) <= 0 Then Continue For
                    strStaus = ""
                    If xTimesheetApprovalunkid <> CInt(drRow("timesheetapprovalunkid")) Then
                        'dList = New DataView(mdtApproveList, "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND emptimesheetunkid = " & CInt(drRow("emptimesheetunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        dList = New DataView(mdtTimesheet, "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND emptimesheetunkid = " & CInt(drRow("emptimesheetunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        xTimesheetApprovalunkid = CInt(drRow("timesheetapprovalunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))
                        If dr.Length > 0 Then
                            For i As Integer = 0 To dr.Length - 1
                                If CInt(drRow("ApprovalStatusId")) = 1 Then
                                    strStaus = Language.getMessage(mstrModuleName, 7, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(drRow("ApprovalStatusId")) = 2 Then
                                    If CInt(dr(i)("ApprovalStatusId")) = 1 Then
                                        strStaus = Language.getMessage(mstrModuleName, 7, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("ApprovalStatusId")) = 3 Then
                                        strStaus = Language.getMessage(mstrModuleName, 8, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For
                                    End If

                                ElseIf CInt(drRow("ApprovalStatusId")) = 3 Then
                                    strStaus = Language.getMessage(mstrModuleName, 8, "Rejected By :-  ") & drRow("ApproverName").ToString()
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If strStaus <> "" Then
                        drRow("status") = strStaus.Trim
                    End If
                Next
            End If

            Dim dtTable As New DataTable

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                If chkMyApprovals.Checked Then

                    If CInt(cboApproverStatus.SelectedValue) > 0 Then
                        mdtTimesheet = New DataView(mdtTimesheet, "tsapproverunkid = " & CInt(cboApprover.SelectedValue) & " AND visibleunkid = " & CInt(cboApproverStatus.SelectedValue) & "  AND  Mapuserunkid = " & CInt(Session("UserId")) & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        mdtTimesheet = New DataView(mdtTimesheet, "Mapuserunkid = " & CInt(Session("UserId")) & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                    End If

                    Dim dtTemp As DataTable = New DataView(mdtTimesheet, "", "", DataViewRowState.CurrentRows).ToTable(True, "ADate", "employeeunkid")

                    Dim dr = From drTable In mdtTimesheet Group Join drTemp In dtTemp On drTable.Field(Of String)("ADate") Equals drTemp.Field(Of String)("ADate") And _
                                 drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable

                    If dr.Count > 0 Then
                        dr.ToList.ForEach(Function(x) DeleteRow(x, mdtTimesheet))
                    End If

                Else
                    If mstrEmployeeIDs.Trim.Length > 0 Then
                        mdtTimesheet = New DataView(mdtTimesheet, "employeeunkid  in (" & mstrEmployeeIDs & ") OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If
            Else
                mdtTimesheet = New DataView(mdtTimesheet, "", "ADate desc", DataViewRowState.CurrentRows).ToTable
            End If

            dgvEmpTimesheet.DataSource = mdtTimesheet

            Dim strColumnArray() As String = {"dgcolhApprovalDate"}
            Dim dCol = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).Where(Function(x) strColumnArray.Contains(x.FooterText))
            If dCol.Count > 0 Then
                For Each dc As DataGridColumn In dCol
                    CType(dgvEmpTimesheet.Columns(dgvEmpTimesheet.Columns.IndexOf(dc)), BoundColumn).DataFormatString = "{0:" & Session("DateFormat").ToString & "}"
                Next
            End If

            dgvEmpTimesheet.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnChangeStatus.Enabled = CBool(Session("AllowToChangeEmployeeBudgetTimesheetStatus"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("Adate = '" & dr("ADate").ToString() & "' AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "DeleteRow", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function


#End Region

#Region " DatePicker Events "

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged
        Try
            Dim dsList As New DataSet
            Dim objEmployee As New clsEmployee_Master

            'mdtPeriodStartDate = dtpDate.GetDate
            'mdtPeriodEndDate = dtpDate.GetDate

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, _
                                                     CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName" 'Nilay (10 Jan 2017)
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            ElseIf (CInt(Session("LoginBy"))) = Global.User.en_loginby.Employee Then
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dgvEmpTimesheet.DataSource = New List(Of String)
            dgvEmpTimesheet.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran

            dtpDate.SetDate = Nothing

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtPeriodEndDate = CDate(objPeriod._TnA_EndDate).Date
                mdtPStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtPEndDate = CDate(objPeriod._TnA_EndDate).Date
            Else
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPStartDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
                mdtPEndDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
            End If

            dtpDate_TextChanged(dtpDate, New EventArgs())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboDonor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDonor.SelectedIndexChanged
        Try
            Dim objProjectCode As New clsFundProjectCode
            Dim dsList As DataSet = objProjectCode.GetComboList("List", True, CInt(cboDonor.SelectedValue))

            With cboProject
                .DataValueField = "fundprojectcodeunkid"
                .DataTextField = "fundprojectname"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            dsList = Nothing
            objProjectCode = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboProject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProject.SelectedIndexChanged
        Try
            Dim objActivity As New clsfundactivity_Tran
            Dim dsList As DataSet = objActivity.GetComboList("List", True, CInt(cboProject.SelectedValue))

            With cboActivity
                .DataValueField = "fundactivityunkid"
                .DataTextField = "activityname"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            dsList = Nothing
            objActivity = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmpTimesheet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmpTimesheet.ItemDataBound
        Try
            Call SetDateFormat()


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgvEmpTimesheet.Columns(1).Visible = CBool(Session("ShowBgTimesheetActivityHrsDetail"))
            dgvEmpTimesheet.Columns(mdic("dgcolhProject")).Visible = CBool(Session("ShowBgTimesheetActivityProject"))
            dgvEmpTimesheet.Columns(mdic("dgcolhActivity")).Visible = CBool(Session("ShowBgTimesheetActivity"))
            'Pinkal (28-Jul-2018) -- End


            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)

                Dim intVisible = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).Where(Function(x) x.Visible = True).Count

                If e.Item.ItemIndex >= 0 Then
                    If CBool(dr("IsGrp")) = True Then

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        CType(e.Item.Cells(mdic("objdgcolhShowDetails")).FindControl("imgShowDetails"), ImageButton).Visible = False
                        'Pinkal (28-Mar-2018) -- End

                        CType(e.Item.Cells(mdic("dgcolhIsCheck")).FindControl("chkSelect"), CheckBox).Visible = False

                        e.Item.Cells(mdic("dgcolhIsCheck")).ColumnSpan = mdic("dgcolhParticular") - 1
                        e.Item.Cells(mdic("dgcolhParticular")).ColumnSpan = intVisible
                        For i = mdic("dgcolhParticular") + 1 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next



                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(mdic("dgcolhIsCheck")).CssClass = "GroupHeaderStyleBorderLeft"
                        'e.Item.Cells(mdic("objdgcolhShowDetails")).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(mdic("dgcolhIsCheck")).CssClass = "group-header"
                        e.Item.Cells(mdic("objdgcolhShowDetails")).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End


                        e.Item.Cells(mdic("objdgcolhShowDetails")).Style.Add("text-align", "left")

                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(mdic("dgcolhParticular")).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(mdic("dgcolhParticular")).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End


                        e.Item.Cells(mdic("dgcolhParticular")).Style.Add("text-align", "left")
                        e.Item.Cells(mdic("dgcolhParticular")).Text &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & "[ " & Language.getMessage(mstrModuleName, 10, "Shift Hours:") & " " & e.Item.Cells(mdic("objdgcolhShiftHours")).Text & " ]"
                        mdtTimesheet.Rows(e.Item.ItemIndex).Item("Particulars") = mdtTimesheet.Rows(e.Item.ItemIndex).Item("Particulars").ToString & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & "[ " & Language.getMessage(mstrModuleName, 10, "Shift Hours:") & " " & e.Item.Cells(mdic("objdgcolhShiftHours")).Text & " ]"
                    Else
                        'Pinkal (13-Aug-2018) -- Start
                        'Enhancement - Changes For PACT [Ref #249,252]
                        If CBool(dr("isholiday")) Then
                            For i As Integer = 0 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Style.Add("color", "red")
                            Next
                        End If
                        'Pinkal (13-Aug-2018) -- End
                    End If
                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheet_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvEmpTimesheet_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvEmpTimesheet.ItemCommand
        Try

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

            'GetProjectDetails(mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, CInt(e.Item.Cells(mdic("objdgcolhEmployeeID")).Text), e.Item.Cells(mdic("objdgcolhEmployeeName")).Text, e.Item.Cells(mdic("dgcolhProject")).Text _
            '                        , e.Item.Cells(mdic("dgcolhDonor")).Text, CInt(e.Item.Cells(mdic("objdgcolhActivityID")).Text), e.Item.Cells(mdic("dgcolhActivity")).Text _
            '                        , CDbl(e.Item.Cells(mdic("objdgcolhpercentage")).Text))

            GetProjectDetails(mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, CInt(e.Item.Cells(mdic("objdgcolhEmployeeID")).Text), e.Item.Cells(mdic("objdgcolhEmployeeName")).Text, e.Item.Cells(mdic("dgcolhProject")).Text _
                                    , e.Item.Cells(mdic("dgcolhDonor")).Text, CInt(e.Item.Cells(mdic("objdgcolhActivityID")).Text), e.Item.Cells(mdic("dgcolhActivity")).Text _
                                 , CDbl(e.Item.Cells(mdic("objdgcolhpercentage")).Text), mdtPStartDate, mdtPEndDate)


            'Pinkal (16-May-2018) -- End

            popupShowDetails.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheet_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If Request.QueryString.ToString.Length > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            Else
                Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
            'Nilay (27 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            If CDate(dtpDate.GetDate).Date <> Nothing Then
                If CDate(dtpDate.GetDate).Date < mdtPeriodStartDate OrElse CDate(dtpDate.GetDate).Date > mdtPeriodEndDate Then
                    DisplayMessage.DisplayMessage("Date must be in the range from " & mdtPeriodStartDate & " to " & mdtPeriodEndDate, Me)
                    dtpDate.SetDate = Nothing
                    Exit Sub
                End If
            End If

            Call FillList()

            'CType(dgvEmpTimesheet.Controls(mdic("dgcolhIsCheck")).Controls(mdic("dgcolhIsCheck")).FindControl("chkSelectAll"), CheckBox).Checked = False
            CType(dgvEmpTimesheet.Controls(mdic("dgcolhIsCheck")).Controls(mdic("dgcolhIsCheck")).FindControl("chkAllSelect"), CheckBox).Checked = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If Request.QueryString.ToString.Length > 0 Then
                mstrAdvanceFilter = ""
                dtpDate.SetDate = Nothing
                cboEmployee.SelectedIndex = 0
                cboDonor.SelectedIndex = 0
                cboProject.SelectedIndex = 0
                cboActivity.SelectedIndex = 0
                Call FillList()
            Else
                cboPeriod.SelectedIndex = 0
                dtpDate.SetDate = Nothing
                'chkMyApprovals.Checked = False
                cboEmployee.SelectedIndex = 0
                cboDonor.SelectedIndex = 0
                cboProject.SelectedIndex = 0
                cboActivity.SelectedIndex = 0
                cboStatus.SelectedValue = "2"
                cboApproverStatus.SelectedValue = "2"
                mstrAdvanceFilter = ""
                dgvEmpTimesheet.DataSource = New List(Of String)
                dgvEmpTimesheet.DataBind()
            End If
            'Nilay (27 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
    '    Try
    '        If dgvEmpTimesheet Is Nothing OrElse dgvEmpTimesheet.Items.Count <= 0 Then Exit Sub
    '        If mdtTimesheet.Rows.Count <= 0 Then Exit Sub

    '        Dim dRow As DataRow() = mdtTimesheet.Select("ischecked=True")

    '        If dRow.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please check atleast one transaction from the list to perform further operation on it."), Me)
    '            dgvEmpTimesheet.Focus()
    '            Exit Sub
    '        Else

    '            mdtTimesheet = New DataView(mdtTimesheet, "ischecked = True", "", DataViewRowState.CurrentRows).ToTable

    '            For Each dR In dRow
    '                If CBool(dR.Item("IsGrp")) = False Then

    '                    If CInt(dR.Item("ApprovalStatusId")) = 1 Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
    '                        Exit Sub
    '                    ElseIf CInt(dR.Item("ApprovalStatusId")) = 3 Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
    '                        Exit Sub
    '                    End If

    '                    Dim dtList As DataTable = New DataView(mdtApproveList, "employeeunkid = " & CInt(dR.Item("employeeunkid")) _
    '                                                           & " AND ADate = '" & eZeeDate.convertDate(CDate(dR.Item("activitydate")).Date).ToString() & "'" _
    '                                                           & " AND activityunkid = " & CInt(dR.Item("activityunkid")) _
    '                                                           & " AND approveremployeeunkid <> " & CInt(dR.Item("approveremployeeunkid")), "", _
    '                                                           DataViewRowState.CurrentRows).ToTable

    '                    If dtList.Rows.Count > 0 Then
    '                        Dim objLevel As New clstsapproverlevel_master
    '                        objLevel._Tslevelunkid = objApprover._Tslevelunkid

    '                        For i As Integer = 0 To dtList.Rows.Count - 1

    '                            If objLevel._Priority > CInt(dtList.Rows(i)("Priority")) Then

    '                                'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

    '                                Dim dList As DataTable = New DataView(dtList, "tslevelunkid = " & CInt(dtList.Rows(i)("tslevelunkid")) & " AND (Approvalstatusid = 1 or Approvalstatusid = 7)", "", DataViewRowState.CurrentRows).ToTable

    '                                If dList.Rows.Count > 0 Then Continue For

    '                                'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

    '                                If CInt(dtList.Rows(i)("Approvalstatusid")) = 2 Then
    '                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending."), Me)
    '                                    Exit Sub

    '                                ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
    '                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
    '                                    Exit Sub

    '                                End If

    '                            ElseIf objLevel._Priority <= CInt(dtList.Rows(i)("Priority")) Then

    '                                If CInt(dtList.Rows(i)("Approvalstatusid")) = 1 Then
    '                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
    '                                    Exit Sub

    '                                ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
    '                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
    '                                    Exit Sub
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '            Next

    '            objApprover = New clstsapprover_master
    '            Dim objtsLevel As New clstsapproverlevel_master
    '            objApprover._Tsapproverunkid = CInt(cboApprover.SelectedValue)
    '            objtsLevel._Tslevelunkid = objApprover._Tslevelunkid
    '            Me.Session("Priority") = CInt(objtsLevel._Priority)

    '            Me.Session("ApprovalTable") = mdtTimesheet
    '            Me.Session("PeriodID") = CInt(cboPeriod.SelectedValue)
    '            If Request.QueryString.ToString.Length > 0 Then
    '                Dim strQueryString As String = HttpUtility.UrlEncode(clsCrypto.Encrypt(True.ToString & "|" & mintApproverID.ToString))
    '                Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApproval.aspx?" & strQueryString, False)
    '            Else
    '                Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApproval.aspx", False)
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try
            If dgvEmpTimesheet Is Nothing OrElse dgvEmpTimesheet.Items.Count <= 0 Then Exit Sub

            If mdtTimesheet.Rows.Count <= 0 Then Exit Sub

            'Dim dRow As DataRow() = mdtTimesheet.Select("ischecked=True")
            Dim dRow As IEnumerable(Of DataGridItem) = Nothing
            dRow = dgvEmpTimesheet.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If dRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please check atleast one transaction from the list to perform further operation on it."), Me)
                dgvEmpTimesheet.Focus()
                Exit Sub
            Else

                'mdtTimesheet = New DataView(mdtTimesheet, "ischecked = True", "", DataViewRowState.CurrentRows).ToTable

                For Each dR As DataGridItem In dRow

                    mdtTimesheet.Rows(dR.ItemIndex)("ischecked") = True

                    If CBool(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhIsGrp", False, True)).Text) = False Then

                        If CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhStatusID", False, True)).Text) = 1 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
                            Exit Sub
                        ElseIf CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhStatusID", False, True)).Text) = 3 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                            Exit Sub
                        End If

                        'Dim dtList As DataTable = New DataView(mdtApproveList, "employeeunkid = " & CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhEmployeeID", False, True)).Text) _
                        '                                       & " AND ADate = '" & eZeeDate.convertDate(CDate(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhActivityDate", False, True)).Text).Date).ToString() & "'" _
                        '                                       & " AND activityunkid = " & CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhActivityID", False, True)).Text) _
                        '                                       & " AND approveremployeeunkid <> " & CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhApproverEmployeeID", False, True)).Text), "", _
                        '                                       DataViewRowState.CurrentRows).ToTable

                        Dim dtList As DataTable = New DataView(mdtTimesheet, "employeeunkid = " & CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhEmployeeID", False, True)).Text) _
                                                             & " AND ADate = '" & eZeeDate.convertDate(CDate(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhActivityDate", False, True)).Text).Date).ToString() & "'" _
                                                             & " AND activityunkid = " & CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhActivityID", False, True)).Text) _
                                                             & " AND approveremployeeunkid <> " & CInt(dR.Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhApproverEmployeeID", False, True)).Text), "", _
                                                             DataViewRowState.CurrentRows).ToTable

                        If dtList.Rows.Count > 0 Then
                            Dim objLevel As New clstsapproverlevel_master
                            objLevel._Tslevelunkid = objApprover._Tslevelunkid

                            For i As Integer = 0 To dtList.Rows.Count - 1

                                If objLevel._Priority > CInt(dtList.Rows(i)("Priority")) Then

                                    'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                                    Dim dList As DataTable = New DataView(dtList, "tslevelunkid = " & CInt(dtList.Rows(i)("tslevelunkid")) & " AND (Approvalstatusid = 1 or Approvalstatusid = 7)", "", DataViewRowState.CurrentRows).ToTable

                                    If dList.Rows.Count > 0 Then Continue For

                                    'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                                    If CInt(dtList.Rows(i)("Approvalstatusid")) = 2 Then
                                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending."), Me)
                                        Exit Sub

                                    ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
                                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                                        Exit Sub

                                    End If

                                ElseIf objLevel._Priority <= CInt(dtList.Rows(i)("Priority")) Then

                                    If CInt(dtList.Rows(i)("Approvalstatusid")) = 1 Then
                                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), Me)
                                        Exit Sub

                                    ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
                                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), Me)
                                        Exit Sub
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next

                objApprover = New clstsapprover_master

                Dim objtsLevel As New clstsapproverlevel_master
                objApprover._Tsapproverunkid = CInt(cboApprover.SelectedValue)
                objtsLevel._Tslevelunkid = objApprover._Tslevelunkid
                Me.Session("Priority") = CInt(objtsLevel._Priority)

                Dim drRow = mdtTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True)
                If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                    Me.Session("ApprovalTable") = drRow.CopyToDataTable()
                End If

                Me.Session("PeriodID") = CInt(cboPeriod.SelectedValue)
                If Request.QueryString.ToString.Length > 0 Then
                    Dim strQueryString As String = HttpUtility.UrlEncode(clsCrypto.Encrypt(True.ToString & "|" & mintApproverID.ToString))
                    Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApproval.aspx?" & strQueryString, False)
                Else
                    Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApproval.aspx", False)
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString.Replace("ADF", "hremployee_master")
            Call FillList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Button's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Protected Sub chkMyApprovals_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'Call FillList()
            'Nilay (07 Feb 2017) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

    '        If mdtTimesheet.Rows.Count <= 0 Then Exit Sub

    '        For Each item As DataGridItem In dgvEmpTimesheet.Items
    '            CType(item.Cells(mdic("dgcolhIsCheck")).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '            Dim dRow As DataRow() = mdtTimesheet.Select("emptimesheetunkid=" & CInt(item.Cells(mdic("objdgcolhEmpTimesheetID")).Text))
    '            If dRow.Length > 0 Then
    '                For Each dR In dRow

    '                    'Pinkal (13-Oct-2017) -- Start
    '                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
    '                    'dR.Item("ischeck") = chkSelectAll.Checked
    '                    dR.Item("ischecked") = chkSelectAll.Checked
    '                    'Pinkal (13-Oct-2017) -- End
    '                Next
    '            End If
    '        Next
    '        mdtTimesheet.AcceptChanges()

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgvEmpTimesheet.Items.Count <= 0 Then Exit Sub

    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

    '        Dim dsRow As DataRow() = mdtTimesheet.Select("emptimesheetunkid=" & CInt(item.Cells(mdic("objdgcolhEmpTimesheetID")).Text))
    '        If dsRow.Length > 0 Then
    '            For Each dR In dsRow
    '                'Pinkal (13-Oct-2017) -- Start
    '                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
    '                'dR.Item("ischeck") = chkSelect.Checked
    '                dR.Item("ischecked") = chkSelect.Checked
    '                'Pinkal (13-Oct-2017) -- End
    '            Next
    '        End If
    '        mdtTimesheet.AcceptChanges()

    '        'Nilay (07 Feb 2017) -- Start
    '        'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
    '        Dim drRow As DataRow() = mdtTimesheet.Select("IsGrp=0 AND employeeunkid=" & CInt(item.Cells(mdic("objdgcolhEmployeeID")).Text) & " AND ADate=" & eZeeDate.convertDate(CDate(item.Cells(mdic("objdgcolhActivityDate")).Text).Date).ToString())

    '        'Pinkal (13-Oct-2017) -- Start
    '        'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
    '        'Dim dRow As DataRow() = mdtTimesheet.Select("IsGrp=0 AND ischeck=0 AND employeeunkid=" & CInt(item.Cells(mdic("objdgcolhEmployeeID")).Text) & " AND ADate=" & eZeeDate.convertDate(CDate(item.Cells(mdic("objdgcolhActivityDate")).Text).Date).ToString())
    '        Dim dRow As DataRow() = mdtTimesheet.Select("IsGrp=0 AND ischecked=0 AND employeeunkid=" & CInt(item.Cells(mdic("objdgcolhEmployeeID")).Text) & " AND ADate=" & eZeeDate.convertDate(CDate(item.Cells(mdic("objdgcolhActivityDate")).Text).Date).ToString())
    '        'Pinkal (13-Oct-2017) -- End

    '        Dim gRow As DataRow() = mdtTimesheet.Select("IsGrp=1 AND employeeunkid=" & CInt(item.Cells(mdic("objdgcolhEmployeeID")).Text) & " AND ADate=" & eZeeDate.convertDate(CDate(item.Cells(mdic("objdgcolhActivityDate")).Text).Date).ToString())

    '        If gRow.Length <= 0 Then Exit Sub

    '        'Pinkal (13-Oct-2017) -- Start
    '        'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
    '        If drRow.Length = dRow.Length Then
    '            'gRow(0).Item("ischeck") = False
    '            gRow(0).Item("ischecked") = False
    '        Else
    '            'gRow(0).Item("ischeck") = True
    '            gRow(0).Item("ischecked") = True
    '        End If
    '        'Pinkal (13-Oct-2017) -- End
    '        gRow(0).AcceptChanges()
    '        'Nilay (07 Feb 2017) -- End


    '        'Pinkal (13-Oct-2017) -- Start
    '        'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
    '        'Dim xCount As DataRow() = mdtTimesheet.Select("ischeck=True")
    '        Dim xCount As DataRow() = mdtTimesheet.Select("ischecked=True")
    '        'Pinkal (13-Oct-2017) -- End
    '        If xCount.Length = dgvEmpTimesheet.Items.Count Then
    '            CType(dgvEmpTimesheet.Controls(mdic("dgcolhIsCheck")).Controls(mdic("dgcolhIsCheck")).FindControl("chkSelectAll"), CheckBox).Checked = True
    '        Else
    '            CType(dgvEmpTimesheet.Controls(mdic("dgcolhIsCheck")).Controls(mdic("dgcolhIsCheck")).FindControl("chkSelectAll"), CheckBox).Checked = False
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

#End Region

#Region "Project Details"

#Region "Private Methods"

    Private Sub GetProjectDetails(ByVal mdtFromDate As Date, ByVal mdtToDate As Date, ByVal mintEmployeeId As Integer, ByVal mstrEmployee As String, ByVal mstrProject As String, ByVal mstrDonor As String _
                                              , ByVal mintActivityId As Integer, ByVal mstrActivity As String, ByVal mdblAllocatePencentage As Double, ByVal xPeriodStartDate As Date, ByVal xPeriodEndDate As Date)
        '  'Pinkal (16-May-2018) -- 'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.[ByVal xPeriodStartDate As Date, ByVal xPeriodEndDate As Date]
        Try

            objLblEmpVal.Text = mstrEmployee
            objLblPeriodVal.Text = cboPeriod.SelectedItem.Text
            objLblProjectVal.Text = mstrProject
            objLblDonorGrantVal.Text = mstrDonor
            objLblActivityVal.Text = mstrActivity

            Dim dtTable As New DataTable()
            dtTable.Columns.Add("Id", Type.GetType("System.Int32"))
            dtTable.Columns.Add("Particulars", Type.GetType("System.String"))
            dtTable.Columns.Add("Hours", Type.GetType("System.String"))

            Dim drRow As DataRow = dtTable.NewRow()
            drRow("Id") = 1
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 1, "Projected(targeted) total number of hours for the month")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 2
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 2, "Projected running total number of hours as per entries")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 3
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 3, "Projected running total number of hours as of date")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 4
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 4, "Total Actual(running) number of hours")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 5
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 5, "Balance of the remained hrs")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 6
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 6, "Employee % allocation of the projects")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            Dim RowIndex As Integer = 1


            ' START - (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)
            Dim mdblProejectedMonthHrs As Double = 0
            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            'Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(mdtFromDate, mdtToDate, mintEmployeeId)
            Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(xPeriodStartDate, xPeriodEndDate, mintEmployeeId)
            'Pinkal (16-May-2018) -- End
            dtTable.Rows(RowIndex - 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            mdblProejectedMonthHrs = CDbl((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)
            ' END -  (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)



            'START - (II) Projected running total number of hours as per entries.
            mdblTotalWorkingHrs = 0
            If mdtTimesheet IsNot Nothing AndAlso mdtTimesheet.Rows.Count > 0 Then
                Dim dtDate As Date = Nothing
                dtDate = CDate(mdtTimesheet.Compute("Max(activitydate)", "IsGrp = 0 AND employeeunkid = " & mintEmployeeId & " AND   activityunkid = " & mintActivityId))
                mdblTotalWorkingHrs = GetProjectedTotalHRs(mdtFromDate, dtDate, mintEmployeeId)
                dtTable.Rows(RowIndex)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            Else
                dtTable.Rows(RowIndex)("Hours") = "00:00"
            End If
            'END  - (II)  Projected running total number of hours as per entries.


            ' START - (III) Projected running total number of hours as of date.
            mdblTotalWorkingHrs = 0
            If eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date < mdtToDate.Date Then
                mdtToDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
            End If
            mdblTotalWorkingHrs = GetProjectedTotalHRs(mdtFromDate, mdtToDate, mintEmployeeId)
            dtTable.Rows(RowIndex + 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            ' END - (III) Projected running total number of hours as of date. 


            'START - (IV) Total Actual(running) number of hours (only Entries)
            mdblTotalWorkingHrs = 0
            Dim mdblActualHrs As Double = 0
            If mdtTimesheet IsNot Nothing AndAlso mdtTimesheet.Rows.Count > 0 Then
                mdblTotalWorkingHrs = CInt(mdtTimesheet.Compute("SUM(activity_hrsinMins)", "IsGrp = 0 AND employeeunkid = " & mintEmployeeId & " AND activityunkid =" & mintActivityId)) * 60
                mdblActualHrs = mdblTotalWorkingHrs
                dtTable.Rows(RowIndex + 2)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#0.00").Replace(".", ":")
            Else
                dtTable.Rows(RowIndex + 2)("Hours") = "00:00"
            End If

            'END - (IV) Total Actual(running) number of hours (only Entries)

            'START - (V) Balance of the remained hrs.
            mdblTotalWorkingHrs = 0
            If mdblActualHrs > mdblProejectedMonthHrs Then
                mdblTotalWorkingHrs = mdblActualHrs - mdblProejectedMonthHrs
            Else
                mdblTotalWorkingHrs = mdblProejectedMonthHrs - mdblActualHrs
            End If
            dtTable.Rows(RowIndex + 3)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#0.00").Replace(".", ":")
            'End - (V) Balance of the remained hrs.

            'START - (VI)  Show employee % allocation of the projects 
            dtTable.Rows(dtTable.Rows.Count - 1)("Hours") = mdblAllocatePencentage.ToString("#0.00") & " % "
            'END - (VI) Show employee % allocation of the projects 

            mdblActualHrs = 0
            mdblProejectedMonthHrs = 0
            mdblTotalWorkingHrs = 0

            dgvProjectHrDetails.AutoGenerateColumns = False
            dgvProjectHrDetails.DataSource = dtTable
            dgvProjectHrDetails.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetProjectedTotalHRs(ByVal xFromDate As Date, ByVal xToDate As Date, ByVal xEmployeeId As Integer) As Double
        Dim mintTotalWorkingDays As Integer = 0
        Dim mdblTotalWorkingHrs As Double = 0
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objEmpHoliday As New clsemployee_holiday
        Try

            objEmpShift.GetTotalWorkingHours(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                     , Session("UserAccessModeSetting").ToString(), xEmployeeId, xFromDate.Date, xToDate.Date _
                                                                                                     , mintTotalWorkingDays, mdblTotalWorkingHrs, False)


            Dim dsHoliday As DataSet = objEmpHoliday.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                         , xFromDate.Date, xToDate.Date, Session("UserAccessModeSetting").ToString(), True, False, True, xEmployeeId, , _
                                                                                         , " AND convert(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(xFromDate) & "' AND convert(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(xToDate) & "'")

            If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then
                Dim objShiftTran As New clsshift_tran
                For Each dr As DataRow In dsHoliday.Tables(0).Rows
                    Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, xEmployeeId)
                    objShiftTran.GetShiftTran(mintShiftId)
                    Dim dHLRow() As DataRow = objShiftTran._dtShiftday.Select("isweekend = 0 AND dayid = " & Weekday(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, FirstDayOfWeek.Sunday) - 1)
                    If dHLRow.Length > 0 Then
                        mintTotalWorkingDays -= 1
                        mdblTotalWorkingHrs -= CDbl(dHLRow(0)("workinghrsinsec"))
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpShift = Nothing
            objEmpHoliday = Nothing
        End Try
        Return mdblTotalWorkingHrs
    End Function

#End Region

#Region "Button's Event"

    Protected Sub btnShowDetailsClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowDetailsClose.Click
        Try
            popupShowDetails.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.LblDate.Text = Language._Object.getCaption(Me.LblDate.ID, Me.LblDate.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblApprover.Text = Language._Object.getCaption(Me.LblApprover.ID, Me.LblApprover.Text)
            Me.LblApproverStatus.Text = Language._Object.getCaption(Me.LblApproverStatus.ID, Me.LblApproverStatus.Text)
            Me.lblDonor.Text = Language._Object.getCaption(Me.lblDonor.ID, Me.lblDonor.Text)
            Me.LblProject.Text = Language._Object.getCaption(Me.LblProject.ID, Me.LblProject.Text)
            Me.LblActivity.Text = Language._Object.getCaption(Me.LblActivity.ID, Me.LblActivity.Text)
            Me.LblStatus.Text = Language._Object.getCaption(Me.LblStatus.ID, Me.LblStatus.Text)

            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.ID, Me.btnChangeStatus.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvEmpTimesheet.Columns(1).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(1).FooterText, dgvEmpTimesheet.Columns(1).HeaderText)
            Me.dgvEmpTimesheet.Columns(2).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(2).FooterText, dgvEmpTimesheet.Columns(2).HeaderText)
            Me.dgvEmpTimesheet.Columns(3).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(3).FooterText, dgvEmpTimesheet.Columns(3).HeaderText)
            Me.dgvEmpTimesheet.Columns(4).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(4).FooterText, dgvEmpTimesheet.Columns(4).HeaderText)
            Me.dgvEmpTimesheet.Columns(5).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(5).FooterText, dgvEmpTimesheet.Columns(5).HeaderText)
            Me.dgvEmpTimesheet.Columns(6).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(6).FooterText, dgvEmpTimesheet.Columns(6).HeaderText)
            Me.dgvEmpTimesheet.Columns(7).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(7).FooterText, dgvEmpTimesheet.Columns(7).HeaderText)
            Me.dgvEmpTimesheet.Columns(8).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(8).FooterText, dgvEmpTimesheet.Columns(8).HeaderText)
            Me.dgvEmpTimesheet.Columns(9).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(9).FooterText, dgvEmpTimesheet.Columns(9).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
