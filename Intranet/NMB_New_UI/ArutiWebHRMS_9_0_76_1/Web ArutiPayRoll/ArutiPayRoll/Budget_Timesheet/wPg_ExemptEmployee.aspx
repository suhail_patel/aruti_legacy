﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ExemptEmployee.aspx.vb"
    Inherits="Budget_Timesheet_wPg_ExemptEmployee" Title="Exempt Employee List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 <script type="text/javascript">

     $("body").on("click", "[id*=chkAllSelect]", function() {
         var chkHeader = $(this);
         debugger;
         var grid = $(this).closest("table");
         $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
     });

     $("body").on("click", "[id*=chkSelect]", function() {
         var grid = $(this).closest("table");
         var chkHeader = $("[id*=chkAllSelect]", grid);
         debugger;
         if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
             chkHeader.prop("checked", true);
         }
         else {
             chkHeader.prop("checked", false);
         }
     });

  $.expr[":"].containsNoCase = function(el, i, m) {
           var search = m[3];
           if (!search) return false;
           return eval("/" + search + "/i").test($(el).text());
       };

       function Searching() {
           if ($('#txtSearch').val().length > 0) {
               $('#<%= gvExemptEmployee.ClientID %> tbody tr').hide();
               $('#<%= gvExemptEmployee.ClientID %> tbody tr:first').show();
               $('#<%= gvExemptEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
           }
           else if ($('#txtSearch').val().length == 0) {
               resetFromSearchValue();
           }
           if ($('#<%= gvExemptEmployee.ClientID %> tr:visible').length == 1) {
               $('.norecords').remove();
           }

           if (event.keyCode == 27) {
               resetFromSearchValue();
           }
       }
       function resetFromSearchValue() {
           $('#txtSearch').val('');
           $('#<%= gvExemptEmployee.ClientID %> tr').show();
           $('.norecords').remove();
           $('#txtSearch').focus();
       }
</script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Exempt Employee List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Exempt Employee List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="gvExemptEmployeeList" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" DataKeyNames="tsempexemptunkid,employeeunkid"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateField FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                CommandArgument='<%#Eval("tsempexemptunkid")%>' OnClick="lnkdelete_Click">
                                                                              <i class="fas fa-trash text-danger"></i>       
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhempname" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupExemptEmployee" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblDate" runat="server" PopupControlID="pnlExemptEmployee" DropShadow="false"
                    CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlExemptEmployee" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblExemptEmployee" runat="server" Text="Exempt Employee"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6  col-sm-12 col-xs-12">
                                <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                <uc3:DateCtrl ID="dtpDate" runat="server" AutoPostBack="false" Enabled="false" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                         <div class="form-line">
                                                <input type="text" id="txtSearch" name="txtSearch" placeholder="Type search text"  maxlength="50" onkeyup="Searching(this);" class = "form-control" />
                                          </div>
                                   </div>             
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 320px;">
                                    <asp:GridView ID="gvExemptEmployee" runat="server" AutoGenerateColumns="False" DataKeyNames="employeeunkid,ischecked"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAllSelect" runat="server"  CssClass="filled-in" Text=" "  />  <%--AutoPostBack="true"  OnCheckedChanged="chkSelectAll_CheckedChanged"--%>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />   <%--AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"--%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="EmpCodeName" HeaderText="Employee" FooterText="dgcolhemp" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnExemptEmployee" runat="server" CssClass="btn btn-primary" Text="Exempt" />
                        <asp:Button ID="btnExemptClose" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
                <%--<div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div4" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblTitle" runat="server" Text="Exempt Employee"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div5" class="panel-body-default" style="text-align: left">
                                        <div class="row2">
                                            <div class="ib" style="width: 10%">
                                                
                                            </div>
                                            <div class="ib" style="width: 25%">
                                               
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ib" style="width: 95%;margin-left:10px">
                                             
                                            </div>
                                            <div id="Div2" style="width: 100%; height: 350px; overflow: auto;margin-top:10px">
                                               
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                <uc1:DeleteReason ID="popupDeleteReason" runat="server" />
                <uc2:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
