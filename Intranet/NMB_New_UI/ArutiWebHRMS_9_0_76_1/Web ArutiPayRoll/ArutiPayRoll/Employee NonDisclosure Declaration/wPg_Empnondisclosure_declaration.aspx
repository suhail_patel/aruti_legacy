﻿<%@ Page Title="Confidentiality Agreement and Non-Disclosure Declaration" Language="VB"
    MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_Empnondisclosure_declaration.aspx.vb"
    Inherits="Employee_NonDisclosure_Declaration_wPg_Empnondisclosure_declaration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>
    
    <script type="text/javascript">
        function displaysign() {
            var img = document.getElementById('<%=pnlSign.ClientID %>');
            img.style.display = "block";
        }    

        function GetInstruct(str) {
            var dv = document.getElementById("divInstruction");
            if (dv != null && str != null)
                dv.innerHTML = str.toString();
        }
        
        function FileUploadChangeEvent() {
        var cnt = $('.flupload').length;
                for (i = 0; i < cnt; i++) {
                    var fupld = $('.flupload')[i].id;
                    if (fupld != null)
                    fileUpLoadChange($('.flupload')[i].id);
                }
        }

            $(document).ready(function() {
                FileUploadChangeEvent();
            });

            
            var req = Sys.WebForms.PageRequestManager.getInstance();
            req.add_endRequest(function() {
                FileUploadChangeEvent();
            }); 
    </script>
    
    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc8:ConfirmYesNo ID="popupYesNo" runat="server" Title="Are you sure you want to continue to make an acknowledgement?"
                    Message="You are about to make an acknowledgement to the confidentiality clause. Are you sure you want to continue?" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Confidentiality Agreement and Non-Disclosure Declaration"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Confidentiality Agreement and Non-Disclosure Declaration"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="width: 100%; text-align: center;">
                                        <asp:Label ID="lblDeclarationTitle" runat="server" Font-Bold="true" Font-Underline="true"
                                            Font-Size="Medium" Text="Confidentiality Agreement and Non-Disclosure Declaration"
                                            CssClass="form-label"></asp:Label>
                                    </div>
                                        </div>                                        
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNonDisclosureDeclaration" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 p-l-0">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDeclaredBy" runat="server" Text="Declared by:" Font-Bold="true"
                                                CssClass="form-label"></asp:Label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmpDeclareDate" runat="server" Text="Date:"></asp:Label>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="objlblEmpDeclareDate" runat="server" Text="&nbsp;"></asp:Label>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpName" runat="server" Text="Name:"></asp:Label>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="objlblEmpName" runat="server" Text="&nbsp;"></asp:Label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlSign" runat="server" ToolTip="If this is not your signature, upload new signature">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblempsign" runat="server" Text="Employee Signature:" Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Image ID="imgSignature" runat="server" ToolTip="If this is not your signature, upload new signature" />
                                                    <asp:Label ID="lblnosign" runat="server" Text="Signature Not Available" ForeColor="Red"
                                                        Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                    <asp:UpdatePanel ID="UPUploadSig" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <uc9:FileUpload ID="flUploadsig" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                            </div>                         
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkconfirmSign" ToolTip="Confirm Signature" runat="server" Text="I confirm Signatures" />
                                            </div>
                                        </asp:Panel>
                                            </div>                                           
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnConfirmSign" runat="server" Text="Confirm Signature" CssClass="btn btn-primary" />
                                <asp:Button ID="btnAcknowledge" runat="server" Text="I Acknowledge" CssClass="btn btn-primary" />
                                <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                                        </div>                            
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
