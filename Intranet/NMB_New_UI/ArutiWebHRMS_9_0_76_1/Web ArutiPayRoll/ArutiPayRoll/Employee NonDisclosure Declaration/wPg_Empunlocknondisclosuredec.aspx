﻿<%@ Page Title="Unlock Employee for Non-Declaration Disclosure" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_Empunlocknondisclosuredec.aspx.vb" Inherits="Employee_NonDisclosure_Declaration_wPg_Empunlocknondisclosuredec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>

    <script type="text/javascript">
//        function pageLoad(sender, args) {
//            $("select").searchable();
//        }

        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="" />
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%--<asp:Label ID="lblPageHeader2" runat="server" Text="Unlock Employee for Non-Declaration Disclosure"
                                        CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Unlock Employee for Non-Declaration Disclosure"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 350px;">
                                            <asp:Panel ID="pnl_dgView" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="GvLockEmployeeList" runat="server" AutoGenerateColumns="False"
                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="ndlockunkid, employeeunkid, lockuserunkid, unlockuserunkid, islock, nextlockdatetime, loginemployeeunkid"
                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-CssClass="headerstyle" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" onclick="selectall(this);"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeecode" HeaderText="Emp. Code" HeaderStyle-Width="25%"
                                                            FooterText="colhEmployeecode"></asp:BoundField>
                                                        <asp:BoundField DataField="employeename" HeaderText="Employee Name" HeaderStyle-Width="50%"
                                                            FooterText="colhEmployeename"></asp:BoundField>
                                                        <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock DateTime" HeaderStyle-Width="25%"
                                                            FooterText="colhLockDateTime"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUnlockDays" runat="server" Text="Unlock employees for days " CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUnlockDays" runat="server" CssClass="form-control" Style="text-align: right"
                                                    onKeypress="return onlyNumbers(this, event);" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnUnlock" runat="server" CssClass="btn btn-primary" Text="Unlock" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

        function selectall(chk) {
            $("[id$='chkSelect']").attr('checked', chk.checked);
        }
       
           
    </script>

</asp:Content>
