﻿<%@ Page Title="Final Applicant" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_FinalApplicant.aspx.vb" Inherits="wPg_FinalApplicant" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc8:Confirmation ID="popupConfirm" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Final Applicant" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblVacancyType" runat="server" Text="Vacancy Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboVacancyType" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRefNo" runat="server" Text="Reference No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboRefNo" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEnddate" runat="server" Text="Vacancy End date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtClosingDate" runat="server" ReadOnly="true" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVacancy" runat="server" Text="Vacancy" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboVacancy" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartdate" runat="server" Text="Vacancy Start date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOpeningDate" runat="server" ReadOnly="true" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-default" Visible="false"
                                    ValidationGroup="FinalApplicant" />
                                <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btn btn-default"
                                    Visible="false" ValidationGroup="FinalApplicant" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" ValidationGroup="FinalApplicant" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" ValidationGroup="FinalApplicant" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" ValidationGroup="FinalApplicant"
                                    CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 235px;">
                                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%">
                                                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Enabled="true" OnCheckedChanged="chkHeder1_CheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="refno" HeaderText="Applicant" FooterText="colhFinalApplicantName" />
                                                        <asp:BoundField DataField="Gender" HeaderText="Gender" FooterText="colhFinalGender" />
                                                        <asp:BoundField DataField="BDate" HeaderText="BirthDate" FooterText="colhFinalBirthdate" />
                                                        <asp:BoundField DataField="Phone" HeaderText="Phone" FooterText="colhFinalPhone" />
                                                        <asp:BoundField DataField="Email" HeaderText="Email" FooterText="colhFinalEmail" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" FooterText="colhdgStatus" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
