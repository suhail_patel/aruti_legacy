﻿
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class wPg_ApplicantFilterList
    Inherits Basepage

#Region " Private Variable(s) "

    Dim msg As New CommonCodes
    Private clsuser As New User
    Private objCONN As SqlConnection

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmApplicantFilter_List"
    Private ReadOnly mstrModuleName1 As String = "frmApplicantFilter_Approval"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objVacType As New clsVacancy
        Dim objShortMst As New clsshortlist_master
        Try
            dsList = objVacType.getVacancyType()
            With cboVacancyType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            dsList = objShortMst.getComboList("List", True)
            With cboRefNo
                .DataValueField = "shortlistunkid"
                .DataTextField = "refno"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            With cboStatus
                .Items.Clear()
                .Items.Add("Select")
                .Items.Add("(A). Approved")
                .Items.Add("(P). Pending")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Try
            If Session("AllowToViewShortListApplicants") = False Then Exit Sub

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            Dim objShortMst As New clsshortlist_master
            Dim dTab As DataTable = Nothing
            dTab = objShortMst.Get_Table("List", CInt(cboVacancy.SelectedValue))

            If CInt(cboRefNo.SelectedValue) > 0 Then
                strSearching = "AND orefno = '" & cboRefNo.SelectedItem.Text.Trim.ToString() & "' " 'SHANI [ 01 FEB 2015 ] -- strSearching = "AND orefno = '" & cboRefNo.SelectedText.Trim.ToString() & "' " 
            End If

            If dtpStartdate.IsNull = False Then
                strSearching &= "AND openingdate >= '" & eZeeDate.convertDate(dtpStartdate.GetDate) & "' "
            End If

            If dtEndDate.IsNull = False Then
                strSearching &= "AND closingdate <= '" & eZeeDate.convertDate(dtEndDate.GetDate) & "' "
            End If

            If cboStatus.SelectedIndex > 0 Then
                strSearching &= "AND StatusId = '" & cboStatus.SelectedIndex & "' "
            End If
            Dim dtList As DataTable
            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtList = New DataView(dTab, strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtList = dTab
            End If

            If Me.ViewState("dtList") IsNot Nothing Then
                Me.ViewState("dtList") = dtList
            Else
                Me.ViewState.Add("dtList", dtList)
            End If

            Call SetGridDataSource(dtList, dgvApplicant)

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetGridDataSource(ByVal iDataSource As DataTable, ByVal dv As GridView)
        Try
            dv.AutoGenerateColumns = False
            dv.DataSource = iDataSource
            dv.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnApprove.Visible = CBool(Session("AllowToApproveFinalShortListedApplicant"))
            btnReject.Visible = CBool(Session("AllowToDisapproveFinalShortListedApplicant"))
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)1
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End
#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Page.IsPostBack = False) AndAlso Session("iQueryString") IsNot Nothing Then
                btnApprove.Visible = False : btnReject.Visible = False
                Session("clsuser") = Nothing
            End If


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            If (Session("clsuser") Is Nothing AndAlso Request.QueryString.Count > 0) Then
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If Me.ViewState("IsLink") IsNot Nothing Then
                    Me.ViewState("IsLink") = True
                Else
                    Me.ViewState.Add("IsLink", True)
                End If

                If arr.Length = 8 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))

                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmApplicantFilter_List"
                    StrModuleName1 = "mnuHumanResource"
                    StrModuleName2 = "mnuRecruitmentItem"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")
                    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
                    End If

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objCommon As New CommonCodes
                    'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End
                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    Session("IsImgInDataBase") = gobjConfigOptions._IsImgInDataBase
                    Session("CompanyDomain") = gobjConfigOptions._CompanyDomain


                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    Call FillCombo()

                    cboVacancyType.SelectedValue = CInt(arr(2))
                    Call cboVacancyType_SelectedIndexChanged(New Object, New EventArgs)
                    cboVacancyType.Enabled = False

                    cboVacancy.SelectedValue = CInt(arr(3)) : cboVacancy.Enabled = False
                    cboRefNo.SelectedValue = CInt(arr(4)) : cboRefNo.Enabled = False
                    cboStatus.SelectedValue = CInt(arr(5)) : cboStatus.Enabled = False

                    If arr(6).Trim.Length > 0 AndAlso IsDate(arr(6)) Then
                        dtpStartdate.SetDate = CDate(arr(6))
                    End If
                    dtpStartdate.Enabled = False

                    If arr(7).Trim.Length > 0 AndAlso IsDate(arr(7)) Then
                        dtEndDate.SetDate = CDate(arr(7))
                    End If
                    dtEndDate.Enabled = False

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)) = False Then
                        msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If
                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Sub
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Sub
                            End If
                        End If
                    End If
                    Dim clsConfig As New clsConfigOptions
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))

                    'Sohail (21 Mar 2015) -- Start
                    'Enhancement - New UI Notification Link Changes.
                    'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    'Sohail (21 Mar 2015) -- End
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objUserPrivilege As New clsUserPrivilege

                    'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    'Session("AllowToViewShortListApplicants") = objUserPrivilege._AllowToViewShortListApplicants
                    'Session("AllowToApproveApplicantFilter") = objUserPrivilege._AllowToApproveApplicantFilter
                    'Session("AllowToRejectApplicantFilter") = objUserPrivilege._AllowToRejectApplicantFilter
                    'Sohail (30 Mar 2015) -- End

                    btnApprove.Visible = Session("AllowToApproveApplicantFilter")
                    btnReject.Visible = Session("AllowToRejectApplicantFilter")

                    Call btnSearch_Click(New Object, New EventArgs)
                    btnSearch.Visible = False : btnReset.Visible = False

                    If Session("iQueryString") IsNot Nothing Then
                        btnApprove.Visible = False : btnReject.Visible = False
                    End If

                    If Session("iQueryString") Is Nothing Then Session("iQueryString") = Request.QueryString.ToString
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    HttpContext.Current.Session("Login") = True
                    'Sohail (30 Mar 2015) -- End
                    Exit Sub
                End If
            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmApplicantFilter_List"
            StrModuleName1 = "mnuHumanResource"
            StrModuleName2 = "mnuRecruitmentItem"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            End If

            If (Page.IsPostBack = False) Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End


                clsuser = CType(Session("clsuser"), User)
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    btnApprove.Visible = Session("AllowToApproveApplicantFilter")
                    btnReject.Visible = Session("AllowToRejectApplicantFilter")
                End If
                Call FillCombo()
            End If
            'If IsPostBack Then
            '    'If MyApproveDisapprove.SelectedIndex = -1 Then
            '    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "load", "LoadApplicantFilterImage('" & 1 & "',true);", True)
            '    'Else
            '    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "load", "LoadApplicantFilterImage('" & 1 & "',false);", True)
            '    'End If
            'Else
            '    'If Not Page.ClientScript.IsStartupScriptRegistered("load") Then
            '    '    Page.ClientScript.RegisterStartupScript(Me.GetType(), "load", "LoadApplicantFilterImage();", True)
            '    'End If

            'End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnOperation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOperation.Click
        Try
            If Me.ViewState("dtList") Is Nothing Then Exit Sub


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            Dim ndtRow As DataRow() = Nothing
            ndtRow = Me.ViewState("dtList").Select("IsGrp=True AND IsCheck = True")
            If ndtRow.Length <= 0 Then
                msg.DisplayMessage("Please Select the specific Reference No. to do futher opeation on it.", Me)
                Exit Sub
            End If
            Dim strShortListingIds As String = String.Empty
            If ndtRow.Length > 0 Then
                For i As Integer = 0 To ndtRow.Length - 1
                    strShortListingIds &= "," & ndtRow(i).Item("shortlistunkid").ToString
                Next
            End If
            If strShortListingIds.Trim.Length > 0 Then strShortListingIds = Mid(strShortListingIds, 2)
            If Me.ViewState("ShortListingIds") IsNot Nothing Then
                Me.ViewState("ShortListingIds") = strShortListingIds
            Else
                Me.ViewState.Add("ShortListingIds", strShortListingIds)
            End If

            Dim objApplicantFilter As New clsshortlist_filter
            Dim dsList As DataSet = objApplicantFilter.GetList("Filter", True)
            Dim mdtApplicantFilter As DataTable = New DataView(dsList.Tables(0), "shortlistunkid IN (" & Me.ViewState("ShortListingIds") & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim mdtTable As DataTable = mdtApplicantFilter.Clone

            If mdtTable.Columns.Contains("IsGroup") = False Then
                mdtTable.Columns.Add("IsGroup", Type.GetType("System.Boolean"))
            End If

            If mdtTable.Columns.Contains("qualification") = False Then
                mdtTable.Columns.Add("qualification", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("skill_category") = False Then
                mdtTable.Columns.Add("skill_category", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("skill") = False Then
                mdtTable.Columns.Add("skill", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("ResultLvl_Condition") = False Then
                mdtTable.Columns.Add("ResultLvl_Condition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("gpa_condition") = False Then
                mdtTable.Columns.Add("gpa_condition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("agecondition") = False Then
                mdtTable.Columns.Add("agecondition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("year_condition") = False Then
                mdtTable.Columns.Add("year_condition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("logicalcondition") = False Then
                mdtTable.Columns.Add("logicalcondition", Type.GetType("System.String"))
            End If

            Dim mintShortListID As Integer = -1
            Dim drRow As DataRow = Nothing

            Dim objQualification As New clsqualification_master
            Dim objSkill As New clsskill_master
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'Dim dsCondtion As DataSet = objMaster.GetCondition(False)
            Dim dsCondtion As DataSet = objMaster.GetCondition(False, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            For i As Integer = 0 To mdtApplicantFilter.Rows.Count - 1
                If mintShortListID <> CInt(mdtApplicantFilter.Rows(i)("shortlistunkid")) Then
                    drRow = mdtTable.NewRow()
                    mintShortListID = CInt(mdtApplicantFilter.Rows(i)("shortlistunkid"))
                    drRow("shortlistunkid") = CInt(mdtApplicantFilter.Rows(i)("shortlistunkid"))
                    drRow("filterunkid") = CInt(mdtApplicantFilter.Rows(i)("filterunkid"))
                    drRow("refno") = mdtApplicantFilter.Rows(i)("refno").ToString()
                    drRow("vacancytitle") = mdtApplicantFilter.Rows(i)("vacancytitle").ToString()
                    drRow("qlevel") = DBNull.Value
                    drRow("qualificationgroup") = DBNull.Value
                    drRow("qualification") = DBNull.Value
                    drRow("resultname") = DBNull.Value
                    drRow("resultlevel") = DBNull.Value
                    drRow("result_lvl_condition") = DBNull.Value
                    drRow("gpacode") = DBNull.Value
                    drRow("gpa_condition") = DBNull.Value
                    drRow("age") = DBNull.Value
                    drRow("agecondition") = DBNull.Value
                    drRow("award_year") = DBNull.Value
                    drRow("year_condition") = DBNull.Value
                    drRow("gender_name") = DBNull.Value
                    drRow("skill_category") = DBNull.Value
                    drRow("skill") = DBNull.Value
                    drRow("other_qualificationgrp") = DBNull.Value
                    drRow("other_qualification") = DBNull.Value
                    drRow("other_resultcode") = DBNull.Value
                    drRow("other_skillcategory") = DBNull.Value
                    drRow("other_skill") = DBNull.Value
                    drRow("logicalcondition") = DBNull.Value
                    drRow("IsGroup") = True
                    mdtTable.Rows.Add(drRow)
                End If

                drRow = mdtTable.NewRow()
                drRow("shortlistunkid") = CInt(mdtApplicantFilter.Rows(i)("shortlistunkid"))
                drRow("filterunkid") = CInt(mdtApplicantFilter.Rows(i)("filterunkid"))
                drRow("refno") = ""
                drRow("vacancytitle") = ""
                drRow("qlevel") = CInt(mdtApplicantFilter.Rows(i)("qlevel").ToString())
                drRow("qualificationgroup") = mdtApplicantFilter.Rows(i)("qualificationgroup").ToString()
                If mdtApplicantFilter.Rows(i)("qualificationunkid").ToString().Length > 0 Then
                    drRow("qualification") = objQualification.GetQualification(mdtApplicantFilter.Rows(i)("qualificationunkid").ToString())
                Else
                    drRow("qualification") = ""
                End If
                drRow("resultname") = mdtApplicantFilter.Rows(i)("resultname").ToString()
                drRow("resultlevel") = mdtApplicantFilter.Rows(i)("resultlevel").ToString()

                If CInt(mdtApplicantFilter.Rows(i)("result_lvl_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("result_lvl_condition")))
                    If dr.Length > 0 Then
                        drRow("ResultLvl_Condition") = dr(0)("Name").ToString()
                    Else
                        drRow("ResultLvl_Condition") = ""
                    End If
                Else
                    drRow("ResultLvl_Condition") = ""
                End If

                drRow("gpacode") = CDec(mdtApplicantFilter.Rows(i)("gpacode"))

                If CInt(mdtApplicantFilter.Rows(i)("gpacode_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("gpacode_condition")))
                    If dr.Length > 0 Then
                        drRow("gpa_condition") = dr(0)("Name").ToString()
                    Else
                        drRow("gpa_condition") = ""
                    End If
                Else
                    drRow("gpa_condition") = ""
                End If

                drRow("age") = CInt(mdtApplicantFilter.Rows(i)("age"))

                If CInt(mdtApplicantFilter.Rows(i)("age_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("age_condition")))
                    If dr.Length > 0 Then
                        drRow("agecondition") = dr(0)("Name").ToString()
                    Else
                        drRow("agecondition") = ""
                    End If
                Else
                    drRow("agecondition") = ""
                End If

                drRow("award_year") = mdtApplicantFilter.Rows(i)("award_year").ToString()

                If CInt(mdtApplicantFilter.Rows(i)("award_year_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("award_year_condition")))
                    If dr.Length > 0 Then
                        drRow("year_condition") = dr(0)("Name").ToString()
                    Else
                        drRow("year_condition") = ""
                    End If
                Else
                    drRow("year_condition") = ""
                End If

                drRow("gender_name") = mdtApplicantFilter.Rows(i)("gender_name").ToString()

                If mdtApplicantFilter.Rows(i)("skillcategoryunkid").ToString().Length > 0 Then
                    drRow("skill_category") = objSkill.GetSkillCategory(mdtApplicantFilter.Rows(i)("skillcategoryunkid").ToString())
                Else
                    drRow("skill_category") = ""
                End If

                If mdtApplicantFilter.Rows(i)("skillunkid").ToString().Length > 0 Then
                    drRow("skill") = objSkill.GetSkill(mdtApplicantFilter.Rows(i)("skillunkid").ToString())
                Else
                    drRow("skill") = ""
                End If
                drRow("other_qualificationgrp") = mdtApplicantFilter.Rows(i)("other_qualificationgrp").ToString()
                drRow("other_qualification") = mdtApplicantFilter.Rows(i)("other_qualification").ToString()
                drRow("other_resultcode") = mdtApplicantFilter.Rows(i)("other_resultcode").ToString()
                drRow("other_skillcategory") = mdtApplicantFilter.Rows(i)("other_skillcategory").ToString()
                drRow("other_skill") = mdtApplicantFilter.Rows(i)("other_skill").ToString()

                If CInt(mdtApplicantFilter.Rows(i)("logical_condition")) > 0 Then
                    If CInt(mdtApplicantFilter.Rows(i)("logical_condition")) = 1 Then
                        drRow("logicalcondition") = "AND"
                    ElseIf CInt(mdtApplicantFilter.Rows(i)("logical_condition")) = 2 Then
                        drRow("logicalcondition") = "OR"
                    Else
                        drRow("logicalcondition") = ""
                    End If
                Else
                    drRow("logicalcondition") = ""
                End If

                'Sohail (21 Apr 2020) -- Start
                'NMB Enhancement # : On shortlisting criteria approval / rejection screen on MSS, the shortlisting criteria does not show all the parameters used for shortlisting.
                drRow("nationality") = mdtApplicantFilter.Rows(i)("nationality").ToString()
                drRow("experience_days") = mdtApplicantFilter.Rows(i)("experience_days").ToString()
                drRow("experiencecondition") = mdtApplicantFilter.Rows(i)("experiencecondition").ToString()
                'Sohail (21 Apr 2020) -- End

                drRow("IsGroup") = False
                mdtTable.Rows.Add(drRow)

            Next

            objQualification = Nothing
            objSkill = Nothing

            If Me.ViewState("mdtTable") IsNot Nothing Then
                Me.ViewState("mdtTable") = mdtTable
            Else
                Me.ViewState.Add("mdtTable", mdtTable)
            End If

            Call SetGridDataSource(mdtTable, dgvShortListCriteria)

            Dim objShortMst As New clsshortlist_master
            Dim dTab As DataTable = Nothing
            dTab = objShortMst.Get_Table("List", CInt(cboVacancy.SelectedValue), Me.ViewState("ShortListingIds"))
            If Me.ViewState("dtSCApplicant") IsNot Nothing Then
                Me.ViewState("dtSCApplicant") = dTab
            Else
                Me.ViewState.Add("dtSCApplicant", dTab)
            End If

            Call SetGridDataSource(dTab, dgvData)

            popupApproveDisapprove.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            Call SetGridDataSource(Me.ViewState("dtList"), dgvApplicant)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If Me.ViewState("ShortListingIds").ToString.Trim.Length > 0 Then
                If txtRemark.Text.Trim.Length <= 0 Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName1)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Remark is mandatory information. Please enter remark to continue."), Me)
                    'Anjan [04 June 2014] -- End


                    popupApproveDisapprove.Show()
                    Exit Sub
                End If
            End If
            popupApproveDisapprove.Show()
            If Me.ViewState("Operation") IsNot Nothing Then
                Me.ViewState("Operation") = 1
            Else
                Me.ViewState.Add("Operation", 1)
            End If
            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            'Language.setLanguage(mstrModuleName1)
            'popConfirm.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.Title)
            popConfirm.Title = "Approve/Disapprove Shortlisted Criteria"


            popConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "You are about to approve all shorlisting criteria. Due to this all reference no for this vacancy will be approved and therefore user cannot delete them.") & vbCrLf & _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 8, "Do you wish to continue?")

            'Anjan [04 June 2014] -- End

            popConfirm.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            Call SetGridDataSource(Me.ViewState("dtList"), dgvApplicant)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            If Me.ViewState("ShortListingIds").ToString.Trim.Length > 0 Then
                If txtRemark.Text.Trim.Length <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName1)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Remark is mandatory information. Please enter remark to continue."), Me)
                    'Anjan [04 June 2014] -- End

                    popupApproveDisapprove.Show()
                    Exit Sub
                End If
            End If
            Dim objSFApplicant As New clsshortlist_finalapplicant
            If objSFApplicant.Can_RefNo_Disapproved(Me.ViewState("ShortListingIds")) = False Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Sorry. You cannot disapprove Operation. Reason : Some of the applicants are already approved for batch scheduling."), Me)
                'Anjan [04 June 2014] -- End
                objSFApplicant = Nothing
                Exit Sub
            End If
            objSFApplicant = Nothing

            popupApproveDisapprove.Show()
            If Me.ViewState("Operation") IsNot Nothing Then
                Me.ViewState("Operation") = 2
            Else
                Me.ViewState.Add("Operation", 2)
            End If
            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            'Language.setLanguage(mstrModuleName1)
            'popConfirm.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.Title)
            popConfirm.Title = "Approve/Disapprove Shortlisted Criteria"
            popConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "You are about to disapprove all shorlisting criteria. Due to this all reference no for this vacancy will be disapproved and therefore user can delete them.") & vbCrLf & _
                                 Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 8, "Do you wish to continue?")
            popConfirm.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            Call SetGridDataSource(Me.ViewState("dtList"), dgvApplicant)
        End Try
    End Sub

    Protected Sub popConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popConfirm.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            For Each sUnkid As String In Me.ViewState("ShortListingIds").Split(CChar(","))
                Dim objSLMaster As New clsshortlist_master
                objSLMaster._Shortlistunkid = CInt(sUnkid)
                objSLMaster._Approveuserunkid = Session("UserId")
                objSLMaster._Remark = txtRemark.Text
                Select Case CInt(Me.ViewState("Operation"))
                    Case 1  'APPROVE
                        objSLMaster._Statustypid = enShortListing_Status.SC_APPROVED
                    Case 2  'DISAPPROVE/REJECT
                        objSLMaster._Statustypid = enShortListing_Status.SC_REJECT
                End Select
                objSLMaster._Appr_Date = ConfigParameter._Object._CurrentDateAndTime
                objSLMaster._Voiddatetime = Nothing
                objSLMaster._Isvoid = False
                If objSLMaster.Update() Then
                    blnFlag = True
                Else
                    blnFlag = False
                End If
                objSLMaster = Nothing
            Next
            If blnFlag = True Then
                Call FillList()
                If Me.ViewState("IsLink") IsNot Nothing Then
                    Session("clsuser") = Nothing
                    msg.DisplayMessage("Information successfully saved.", Me, "~/../../Index.aspx")
                Else
                    msg.DisplayMessage("Information successfully saved.", Me)
                    popConfirm.Hide()
                    popupApproveDisapprove.Hide()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("popConfirm_buttonYes_Click : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.ViewState("IsLink") IsNot Nothing Then
                Response.Redirect("~\Index.aspx", False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        If Me.ViewState("IsLink") IsNot Nothing Then
    '            Response.Redirect("~\Index.aspx")
    '        Else
    '            Response.Redirect("~\UserHome.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboVacancy.SelectedValue = "", 0, cboVacancy.SelectedValue)) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                ' msg.DisplayMessage("Sorry Vacancy is mandatory information. Please select vacancy.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Vacancy is mandatory information. Please select Vacancy to Fill List."), Me)
                'Anjan [04 June 2014] -- End


                Exit Sub
            End If
            Call FillList()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboRefNo.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            cboVacancy.SelectedValue = 0
            cboVacancyType.SelectedValue = 0
            dtpStartdate.SetDate = Nothing
            dtEndDate.SetDate = Nothing
            dgvApplicant.DataSource = Nothing
            dgvApplicant.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

#Region " Other Events "

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If dgvApplicant.Rows.Count <= 0 Then Exit Sub
            Dim dvData As DataTable = Me.ViewState("dtList")
            For i As Integer = 0 To dgvApplicant.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvApplicant.Rows(i)
                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
                Dim dRow() As DataRow = dvData.Select("refno = '" & gvRow.Cells(1).Text & "' AND IsGrp = True")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked
                End If
                dvData.AcceptChanges()
            Next
            Me.ViewState("dtList") = dvData
            Call SetGridDataSource(dvData, dgvApplicant)
            CType(dgvApplicant.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = cb.Checked
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                Dim dRow() As DataRow = CType(Me.ViewState("dtList"), DataTable).Select("refno = '" & gvr.Cells(1).Text & "' AND IsGrp = True")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked
                End If
                CType(Me.ViewState("dtList"), DataTable).AcceptChanges()
                Dim drRow() As DataRow = CType(Me.ViewState("dtList"), DataTable).Select("ischeck = 1")
                If CType(Me.ViewState("dtList"), DataTable).Rows.Count = drRow.Length Then
                    CType(dgvApplicant.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = True
                Else
                    CType(dgvApplicant.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = False
                End If
            End If
            Call SetGridDataSource(CType(Me.ViewState("dtList"), DataTable), dgvApplicant)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " GridView's Event(s) "

    Protected Sub dgvApplicant_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvApplicant.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CType(Me.ViewState("dtList"), DataTable).Rows(e.Row.RowIndex).Item("IsGrp") = False Then
                    e.Row.Cells(0).Text = ""
                    e.Row.Cells(1).Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Cells(1).Text
                ElseIf CType(Me.ViewState("dtList"), DataTable).Rows(e.Row.RowIndex).Item("IsGrp") = True Then
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'e.Row.BackColor = Drawing.Color.SteelBlue : e.Row.ForeColor = Drawing.Color.White
                    e.Row.CssClass = "GroupHeaderStyle"
                    'SHANI [01 FEB 2015]--END 
                    e.Row.Font.Bold = True
                    If CType(Me.ViewState("dtList"), DataTable).Rows(e.Row.RowIndex).Item("ischeck") = True Then
                        CType(e.Row.FindControl("chkbox1"), CheckBox).Checked = True
                    Else
                        CType(e.Row.FindControl("chkbox1"), CheckBox).Checked = False
                    End If
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvShortListCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvShortListCriteria.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CType(Me.ViewState("mdtTable"), DataTable).Rows(e.Row.RowIndex).Item("IsGroup") = True Then
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'e.Row.BackColor = Drawing.Color.SteelBlue : e.Row.ForeColor = Drawing.Color.White
                    e.Row.CssClass = "GroupHeaderStyle"
                    'SHANI [01 FEB 2015]--END 
                    e.Row.Font.Bold = True
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CType(Me.ViewState("dtList"), DataTable).Rows(e.Row.RowIndex).Item("IsGrp") = False Then
                    e.Row.Cells(0).Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Cells(0).Text
                ElseIf CType(Me.ViewState("dtSCApplicant"), DataTable).Rows(e.Row.RowIndex).Item("IsGrp") = True Then
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'e.Row.BackColor = Drawing.Color.SteelBlue : e.Row.ForeColor = Drawing.Color.White
                    e.Row.CssClass = "GroupHeaderStyle"
                    'SHANI [01 FEB 2015]--END 
                    e.Row.Font.Bold = True
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Contols Event(s) "

    Protected Sub cboVacancyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy : Dim dsVac As New DataSet
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End
            dsVac = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(IIf(cboVacancyType.SelectedValue = "", 0, cboVacancyType.SelectedValue)))
            With cboVacancy
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsVac.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.lblVacancy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVacancy.ID, Me.lblVacancy.Text)
            Me.lblRefNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRefNo.ID, Me.lblRefNo.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblEnddate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEnddate.ID, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStartdate.ID, Me.lblStartdate.Text)
            Me.LblVacancyType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblVacancyType.ID, Me.LblVacancyType.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnOperation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnOperation.ID, Me.btnOperation.Text).Replace("&", "")


            Me.dgvApplicant.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvApplicant.Columns(1).FooterText, Me.dgvApplicant.Columns(1).HeaderText)
            Me.dgvApplicant.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvApplicant.Columns(2).FooterText, Me.dgvApplicant.Columns(2).HeaderText)
            Me.dgvApplicant.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvApplicant.Columns(3).FooterText, Me.dgvApplicant.Columns(3).HeaderText)
            Me.dgvApplicant.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvApplicant.Columns(4).FooterText, Me.dgvApplicant.Columns(4).HeaderText)


            'Language.setLanguage(mstrModuleName1)

            Me.frmApplicantFilter_Approval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END


            Me.gbApplicantInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.gbApplicantInfo.ID, Me.gbApplicantInfo.Text)
            Me.gbRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.gbRemark.ID, Me.gbRemark.Text)
            Me.gbCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.gbCriteria.ID, Me.gbCriteria.Text)




            Me.dgvShortListCriteria.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(0).FooterText, Me.dgvShortListCriteria.Columns(0).HeaderText)
            Me.dgvShortListCriteria.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(1).FooterText, Me.dgvShortListCriteria.Columns(1).HeaderText)
            Me.dgvShortListCriteria.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(2).FooterText, Me.dgvShortListCriteria.Columns(2).HeaderText)
            Me.dgvShortListCriteria.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(3).FooterText, Me.dgvShortListCriteria.Columns(3).HeaderText)
            Me.dgvShortListCriteria.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(4).FooterText, Me.dgvShortListCriteria.Columns(4).HeaderText)
            Me.dgvShortListCriteria.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(5).FooterText, Me.dgvShortListCriteria.Columns(5).HeaderText)
            Me.dgvShortListCriteria.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(6).FooterText, Me.dgvShortListCriteria.Columns(6).HeaderText)
            Me.dgvShortListCriteria.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(7).FooterText, Me.dgvShortListCriteria.Columns(7).HeaderText)
            Me.dgvShortListCriteria.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(8).FooterText, Me.dgvShortListCriteria.Columns(8).HeaderText)
            Me.dgvShortListCriteria.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(9).FooterText, Me.dgvShortListCriteria.Columns(9).HeaderText)
            Me.dgvShortListCriteria.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(10).FooterText, Me.dgvShortListCriteria.Columns(10).HeaderText)
            Me.dgvShortListCriteria.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(11).FooterText, Me.dgvShortListCriteria.Columns(11).HeaderText)
            Me.dgvShortListCriteria.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(12).FooterText, Me.dgvShortListCriteria.Columns(12).HeaderText)
            Me.dgvShortListCriteria.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(13).FooterText, Me.dgvShortListCriteria.Columns(13).HeaderText)
            Me.dgvShortListCriteria.Columns(14).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(14).FooterText, Me.dgvShortListCriteria.Columns(14).HeaderText)
            Me.dgvShortListCriteria.Columns(15).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(15).FooterText, Me.dgvShortListCriteria.Columns(15).HeaderText)
            Me.dgvShortListCriteria.Columns(16).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(16).FooterText, Me.dgvShortListCriteria.Columns(16).HeaderText)
            Me.dgvShortListCriteria.Columns(17).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(17).FooterText, Me.dgvShortListCriteria.Columns(17).HeaderText)
            Me.dgvShortListCriteria.Columns(18).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(18).FooterText, Me.dgvShortListCriteria.Columns(18).HeaderText)
            Me.dgvShortListCriteria.Columns(19).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(19).FooterText, Me.dgvShortListCriteria.Columns(19).HeaderText)
            Me.dgvShortListCriteria.Columns(20).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(20).FooterText, Me.dgvShortListCriteria.Columns(20).HeaderText)
            Me.dgvShortListCriteria.Columns(21).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(21).FooterText, Me.dgvShortListCriteria.Columns(21).HeaderText)
            Me.dgvShortListCriteria.Columns(22).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(22).FooterText, Me.dgvShortListCriteria.Columns(22).HeaderText)
            'Sohail (21 Apr 2020) -- Start
            'NMB Enhancement # : On shortlisting criteria approval / rejection screen on MSS, the shortlisting criteria does not show all the parameters used for shortlisting.
            Me.dgvShortListCriteria.Columns(23).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(23).FooterText, Me.dgvShortListCriteria.Columns(23).HeaderText)
            Me.dgvShortListCriteria.Columns(24).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(24).FooterText, Me.dgvShortListCriteria.Columns(24).HeaderText)
            Me.dgvShortListCriteria.Columns(25).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvShortListCriteria.Columns(25).FooterText, Me.dgvShortListCriteria.Columns(25).HeaderText)
            'Sohail (21 Apr 2020) -- End


            Me.dgvData.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(0).FooterText, Me.dgvData.Columns(0).HeaderText)
            Me.dgvData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(1).FooterText, Me.dgvData.Columns(1).HeaderText)

            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class
