﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns

#End Region

Partial Class wPgInterviewAnalysisList
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objCONN As SqlConnection
    Private objInterviewAnalysisMaster As clsInterviewAnalysis_master
    Private objInterviewAnalysisTran As clsInteviewAnalysis_tran


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmInterviewAnalysisList"
    Private ReadOnly mstrModuleName1 As String = "frmInterviewFinalEvaluation"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant_master
        Dim objInterviewType As New clsCommon_Master
        Dim objVac As New clsVacancy
        Dim dsList As New DataSet
        Try
            dsList = objApplicant.GetApplicantList("List", True)
            With cboApplicant
                .DataValueField = "applicantunkid"
                .DataTextField = "applicantname"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True)
            With cboInterviewType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objVac.getVacancyType
            With cboVacType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtList As New DataTable
        Dim StrSearching As String = String.Empty
        Try
            If Session("AllowtoViewInterviewAnalysisList") = False Then Exit Sub


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            dtList = objInterviewAnalysisMaster.Get_DataTable(CInt(cboVacancy.SelectedValue), "List")

            If CInt(IIf(cboApplicant.SelectedValue = "", 0, cboApplicant.SelectedValue)) > 0 Then
                StrSearching &= "AND applicantunkid = " & CInt(cboApplicant.SelectedValue) & " "
            End If

            If dtFromDate.IsNull = False AndAlso dtToDate.IsNull = False Then
                StrSearching &= "AND analysisdate >= '" & eZeeDate.convertDate(dtFromDate.GetDate) & "' AND analysisdate <= '" & eZeeDate.convertDate(dtToDate.GetDate) & "'" & " "
            End If

            If CInt(IIf(cboInterviewType.SelectedValue = "", 0, cboInterviewType.SelectedValue)) > 0 Then
                StrSearching &= "AND interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
            End If

            If CInt(IIf(cboBatch.SelectedValue = "", 0, cboBatch.SelectedValue)) > 0 Then
                StrSearching &= "AND batchscheduleunkid = '" & CInt(cboBatch.SelectedValue) & "' "
            End If

            If CInt(IIf(cboResult.SelectedValue = "", 0, cboResult.SelectedValue)) > 0 Then
                StrSearching &= "AND resultcodeunkid = '" & CInt(cboResult.SelectedValue) & "' "
            End If

            If chkEligible.Checked = True And chkNotEligible.Checked = True Then
                StrSearching &= ""
            ElseIf chkEligible.Checked = True Then
                StrSearching &= "AND iseligible = 1 "
            ElseIf chkNotEligible.Checked = True Then
                StrSearching &= "AND iseligible = 0 "
            ElseIf chkEligible.Checked = False And chkNotEligible.Checked = False Then
                StrSearching &= ""
            End If

            Dim dtTable As DataTable

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dtList, StrSearching, "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dtList, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
            End If

            If dtTable.Rows.Count > 0 Then
                Dim itmp() As DataRow = dtTable.Select("is_grp = True")
                If itmp.Length <= 0 Then
                    Dim iGrp = From p In dtTable.AsEnumerable() Where p.Field(Of Integer)("grp_id") > 0 Select p
                    Dim sValue As String = String.Empty
                    Dim selectedTags As List(Of Integer) = iGrp.Select(Function(x) x.Field(Of Integer)("grp_id")).Distinct.ToList
                    Dim result() As String = selectedTags.Select(Function(x) x.ToString()).ToArray()
                    sValue = String.Join(", ", result)
                    Dim dtmp() As DataRow = dtList.Select("grp_id IN(" & sValue & ") AND is_grp = True")
                    If dtmp.Length > 0 Then
                        For iRow As Integer = 0 To dtmp.Length - 1
                            dtTable.ImportRow(dtmp(iRow))
                        Next
                        dtTable = New DataView(dtTable, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
                        dtTable.AcceptChanges()
                    End If
                End If
            End If

            If Me.ViewState("dtTable") IsNot Nothing Then
                Me.ViewState("dtTable") = dtTable
            Else
                Me.ViewState.Add("dtTable", dtTable)
            End If
            Call SetGridDataSource(dtTable)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetGridDataSource(ByVal iDataSource As DataTable)
        Try
            dgvData.AutoGenerateColumns = False
            dgvData.DataSource = iDataSource
            dgvData.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_PopUp_Data()
        Dim dtList As New DataTable
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            dtList = objInterviewAnalysisMaster.Get_DataTable(IIf(cboVacancy.SelectedValue = "", 0, cboVacancy.SelectedValue), "List", Me.ViewState("sAnalysisIds"))
            Dim mdtTable As DataTable = New DataView(dtList, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
            If Me.ViewState("mdtTable") IsNot Nothing Then
                Me.ViewState("mdtTable") = mdtTable
            Else
                Me.ViewState.Add("mdtTable", mdtTable)
            End If
            dgvViewData.AutoGenerateColumns = False
            dgvViewData.DataSource = mdtTable
            dgvViewData.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisbility()
        Try
            btnEligible.Visible = Session("AllowToPerformEligibleOperation")
            btnNEligible.Visible = Session("AllowToPerformNotEligibleOperation")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisbility", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " From's Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objInterviewAnalysisMaster = New clsInterviewAnalysis_master
            objInterviewAnalysisTran = New clsInteviewAnalysis_tran

            If (Page.IsPostBack = False) AndAlso Session("iQueryString") IsNot Nothing Then
                btnApprove.Visible = False : btnDisapprove.Visible = False
                Session("clsuser") = Nothing
            End If


      
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisbility()
            'Varsha Rana (17-Oct-2017) -- End

            If (Session("clsuser") Is Nothing AndAlso Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")


                If Me.ViewState("IsLink") IsNot Nothing Then
                    Me.ViewState("IsLink") = True
                Else
                    Me.ViewState.Add("IsLink", True)
                End If

                If arr.Length = 12 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Session("CompanyUnkId") = CInt(arr(0))
                    Session("UserId") = CInt(arr(1))

                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmInterviewFinalEvaluation"
                    StrModuleName1 = "mnuHumanResource"
                    StrModuleName2 = "mnuRecruitmentItem"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")
                    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
                    End If

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objCommon As New CommonCodes
                    'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End
                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    Session("IsImgInDataBase") = gobjConfigOptions._IsImgInDataBase
                    Session("CompanyDomain") = gobjConfigOptions._CompanyDomain


                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)


                    Call FillCombo()

                    cboVacType.SelectedValue = CInt(arr(2))
                    Call cboVacType_SelectedIndexChanged(New Object, New EventArgs)
                    cboVacType.Enabled = False

                    cboVacancy.SelectedValue = CInt(arr(3))
                    Call cboVacancy_SelectedIndexChanged(New Object, New EventArgs)
                    cboVacancy.Enabled = False


                    'S.SANDEEP [ 20 JAN 2014 ] -- START
                    'cboBatch.SelectedValue = CInt(arr(4))
                    'Call cboBatch_SelectedIndexChanged(New Object, New EventArgs)
                    'cboBatch.Enabled = False

                    'cboResult.SelectedValue = CInt(arr(5)) : cboResult.Enabled = False
                    'cboApplicant.SelectedValue = CInt(arr(6)) : cboApplicant.Enabled = False
                    'cboInterviewType.SelectedValue = CInt(arr(7)) : cboInterviewType.Enabled = False


                    cboBatch.SelectedValue = CInt(IIf(arr(4).Trim = "", 0, arr(4)))
                    Call cboBatch_SelectedIndexChanged(New Object, New EventArgs)
                    cboBatch.Enabled = False

                    cboResult.SelectedValue = CInt(IIf(arr(5).Trim = "", 0, arr(5))) : cboResult.Enabled = False
                    cboApplicant.SelectedValue = CInt(IIf(arr(6).Trim = "", 0, arr(6))) : cboApplicant.Enabled = False
                    cboInterviewType.SelectedValue = CInt(IIf(arr(7).Trim = "", 0, arr(7))) : cboInterviewType.Enabled = False
                    'S.SANDEEP [ 20 JAN 2014 ] -- END


                    If arr(8).Trim.Length > 0 AndAlso IsDate(arr(8)) Then
                        dtFromDate.SetDate = CDate(arr(8))
                    End If
                    dtFromDate.Enabled = False

                    If arr(9).Trim.Length > 0 AndAlso IsDate(arr(9)) Then
                        dtToDate.SetDate = CDate(arr(9))
                    End If
                    dtToDate.Enabled = False

                    'S.SANDEEP [ 20 JAN 2014 ] -- START
                    'chkEligible.Checked = CBool(arr(10)) : chkEligible.Enabled = False
                    'chkNotEligible.Checked = CBool(arr(11)) : chkNotEligible.Enabled = False

                    chkEligible.Checked = CBool(IIf(arr(10).Trim = "", 0, arr(10))) : chkEligible.Enabled = False
                    chkNotEligible.Checked = CBool(IIf(arr(11).Trim = "", 0, arr(11))) : chkNotEligible.Enabled = False
                    'S.SANDEEP [ 20 JAN 2014 ] -- END

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)) = False Then
                        msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Sub
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Sub
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If


                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))

                    'Sohail (21 Mar 2015) -- Start
                    'Enhancement - New UI Notification Link Changes.
                    'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    'Sohail (21 Mar 2015) -- End
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objUserPrivilege As New clsUserPrivilege

                    'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    'Session("AllowtoViewInterviewAnalysisList") = objUserPrivilege._AllowtoViewInterviewAnalysisList
                    'Session("AllowtoApproveApplicantEligibility") = objUserPrivilege._AllowtoApproveApplicantEligibility
                    'Session("AllowtoDisapproveApplicantEligibility") = objUserPrivilege._AllowtoDisapproveApplicantEligibility
                    'Sohail (30 Mar 2015) -- End

                    btnApprove.Visible = Session("AllowtoApproveApplicantEligibility")
                    btnDisapprove.Visible = Session("AllowtoDisapproveApplicantEligibility")

                    Call btnSearch_Click(New Object, New EventArgs)
                    btnSearch.Visible = False : btnReset.Visible = False

                    If Session("iQueryString") IsNot Nothing Then
                        btnApprove.Visible = False : btnDisapprove.Visible = False
                    End If

                    If Session("iQueryString") Is Nothing Then Session("iQueryString") = Request.QueryString.ToString
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    HttpContext.Current.Session("Login") = True
                    'Sohail (30 Mar 2015) -- End
                    Exit Sub
                End If
            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmInterviewFinalEvaluation"
            StrModuleName1 = "mnuHumanResource"
            StrModuleName2 = "mnuRecruitmentItem"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            End If

            If (Page.IsPostBack = False) Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014] -- End
                clsuser = CType(Session("clsuser"), User)
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    btnApprove.Visible = Session("AllowtoApproveApplicantEligibility")
                    btnDisapprove.Visible = Session("AllowtoDisapproveApplicantEligibility")
                End If
                Call FillCombo()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboVacancy.SelectedValue = "", 0, cboVacancy.SelectedValue)) <= 0 Then
                msg.DisplayMessage("Sorry, Please select the vacancy. Vacancy is mandatory information.", Me)
                Exit Sub
            End If
            Call Fill_Grid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboVacancy.SelectedValue = 0
            cboApplicant.SelectedValue = 0
            cboBatch.SelectedValue = 0
            cboInterviewType.SelectedValue = 0
            cboResult.SelectedValue = 0
            cboVacType.SelectedValue = 0
            dtApprDate.SetDate = Nothing
            dtToDate.SetDate = Nothing
            chkEligible.Checked = False
            chkNotEligible.Checked = False
            dgvData.DataSource = Nothing
            dgvData.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEligible_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEligible.Click
        Try
            If Me.ViewState("dtTable") Is Nothing Then Exit Sub
            Dim dtTable As DataTable = Me.ViewState("dtTable")
            Dim sAnalysisIds As String = ""
            Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True AND E_StatusId = 1")
            If dRow.Length <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Please tick atleast one Eligible transaction from the list to perform further operation on it."), Me)
                'Anjan [04 June 2014] -- End


                Exit Sub
            End If
            Dim blnDisplay As Boolean = False
            If dRow.Length > 0 Then
                For i As Integer = 0 To dRow.Length - 1
                    If dRow(i).Item("issent") = False Then
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, You cannot do approve/disapprove operation. Reason : Notification is still pending to send for some applicant(s)."), Me)
                        'Anjan [04 June 2014] -- End
                        Exit Sub
                    ElseIf dRow(i).Item("iscomplete") = False Then
                        Dim sMsg As String = ""
                        If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
                            If sMsg.Trim.Length > 0 Then
                                msg.DisplayMessage(sMsg, Me)
                            End If
                            Exit Sub
                        End If
                    ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") >= 0 Then
                        sAnalysisIds &= "," & dRow(i).Item("a_ids").ToString
                    End If
                Next
            End If
            If sAnalysisIds.Trim.Length > 0 Then
                sAnalysisIds = Mid(sAnalysisIds, 2)
                If Me.ViewState("sAnalysisIds") IsNot Nothing Then
                    Me.ViewState.Add("sAnalysisIds", sAnalysisIds)
                Else
                    Me.ViewState("sAnalysisIds") = sAnalysisIds
                End If
            End If
            Call Fill_PopUp_Data()
            dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            popOperation.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            Call SetGridDataSource(Me.ViewState("dtTable"))
        End Try
    End Sub

    Protected Sub btnNEligible_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNEligible.Click
        Try
            If Me.ViewState("dtTable") Is Nothing Then Exit Sub
            Dim dtTable As DataTable = Me.ViewState("dtTable")
            Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True AND E_StatusId = 0")
            If dRow.Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Please tick atleast one Not Eligible transaction from the list to perform further operation on it."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If
            Dim sAnalysisIds As String = ""
            Dim blnDisplay As Boolean = False
            If dRow.Length > 0 Then
                For i As Integer = 0 To dRow.Length - 1
                    If dRow(i).Item("issent") = False Then
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, You cannot do approve/disapprove operation. Reason : Notification is still pending to send for some applicant(s)."), Me)
                        'Anjan [04 June 2014] -- End
                        Exit Sub
                    ElseIf dRow(i).Item("iscomplete") = False Then
                        Dim sMsg As String = ""
                        If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
                            If sMsg.Trim.Length > 0 Then
                                msg.DisplayMessage(sMsg, Me)
                            End If
                            Exit Sub
                        End If
                    ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") >= 0 Then
                        sAnalysisIds &= "," & dRow(i).Item("a_ids").ToString
                    End If
                Next
            End If
            If sAnalysisIds.Trim.Length > 0 Then
                sAnalysisIds = Mid(sAnalysisIds, 2)
                If Me.ViewState("sAnalysisIds") IsNot Nothing Then
                    Me.ViewState.Add("sAnalysisIds", sAnalysisIds)
                Else
                    Me.ViewState("sAnalysisIds") = sAnalysisIds
                End If
            End If
            Call Fill_PopUp_Data()
            dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            popOperation.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            Call SetGridDataSource(Me.ViewState("dtTable"))
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If Me.ViewState("mdtTable") IsNot Nothing Then
                If Me.ViewState("sAnalysisIds") IsNot Nothing AndAlso Me.ViewState("sAnalysisIds").ToString.Trim.Length > 0 Then
                    If txtRemark.Text.Trim.Length <= 0 Then
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName1)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Remark is mandatory information. Please enter remark to continue."), Me)
                        'Anjan [04 June 2014] -- End
                        dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                        popOperation.Show()
                        Exit Sub
                    End If
                    If dtApprDate.IsNull = True Then
                        msg.DisplayMessage("Approval Date is mandatory information. Please set approval date to continue.", Me)
                        dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                        popOperation.Show()
                        Exit Sub
                    End If
                    Dim mdtTable As DataTable = CType(Me.ViewState("mdtTable"), DataTable)
                    Dim dtmp() As DataRow = mdtTable.Select("is_grp = True")
                    If dtmp.Length > 0 Then
                        Dim iTotal_Position, iTotal_Eligible As Integer
                        iTotal_Position = 0 : iTotal_Eligible = 0
                        objInterviewAnalysisMaster.Get_Eligible_Count(iTotal_Position, iTotal_Eligible, CInt(cboVacancy.SelectedValue))
                        If iTotal_Eligible + dtmp.Length > iTotal_Position Then
                            popOperation.Show()
                            popConfirm1.Title = "Approving Eligible Applicants"

                            'Anjan [04 June 2014] -- Start
                            'ENHANCEMENT : Implementing Language,requested by Andrew
                            'Language.setLanguage(mstrModuleName1)
                            popConfirm1.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 8, "Total position set for the selected vacancy is/are [ ") & iTotal_Position & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 9, " ] and total eligible application approved for this vacancy is/are [ ") & iTotal_Eligible & _
                                                                          Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 10, " ] and total remaining positions are [ ") & iTotal_Position - iTotal_Eligible & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, " ].") & vbCrLf & _
                                                                              Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 12, "You are trying to approve applicant more than the postion set for this vacancy.") & vbCrLf & _
                                                                                Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Do you wish to continue?")
                            'Anjan [04 June 2014] -- End


                            popConfirm1.Show()
                            Exit Sub
                        End If
                    End If
                    dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    popOperation.Show()
                    If Me.ViewState("Operation") IsNot Nothing Then
                        Me.ViewState.Add("Operation", "1")
                    Else
                        Me.ViewState("Operation") = 1
                    End If
                    popConfirm2.Title = "Approving Eligible Applicants"

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName1)
                    popConfirm2.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "You are about to approve all eligible applicant. Due to this all approved applicant will be available for import applicant for employeee.") & vbCrLf & _
                                                              Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Do you wish to continue?")
                    'Anjan [04 June 2014] -- End


                    popConfirm2.Show()
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popConfirm1_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popConfirm1.buttonYes_Click
        Try
            dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            popOperation.Show()
            popConfirm2.Title = "Approving Eligible Applicants"

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            'Language.setLanguage(mstrModuleName1)
            popConfirm2.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "You are about to approve all eligible applicant. Due to this all approved applicant will be available for import applicant for employeee.") & vbCrLf & _
                                              Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Do you wish to continue?")
            'Anjan [04 June 2014] -- End


            popConfirm2.Show()
            Exit Sub
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popConfirm2_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popConfirm2.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            If Me.ViewState("sAnalysisIds") IsNot Nothing AndAlso Me.ViewState("sAnalysisIds").ToString.Trim.Length > 0 Then
                For Each sKeyId As String In Me.ViewState("sAnalysisIds").ToString.Replace("'", "").Split(CChar(","))
                    objInterviewAnalysisMaster._Analysisunkid = CInt(sKeyId)
                    objInterviewAnalysisMaster._Appr_Date = dtApprDate.GetDate
                    objInterviewAnalysisMaster._Appr_Remark = txtRemark.Text
                    objInterviewAnalysisMaster._Approveuserunkid = Session("UserId")
                    Select Case CInt(Me.ViewState("Operation"))
                        Case 1  'APPROVED
                            objInterviewAnalysisMaster._Statustypid = enShortListing_Status.SC_APPROVED
                        Case 2  'DISAPPROVED
                            objInterviewAnalysisMaster._Statustypid = enShortListing_Status.SC_REJECT
                            objInterviewAnalysisMaster._Issent = False
                    End Select
                    If objInterviewAnalysisMaster.Update() = True Then
                        blnFlag = True
                    Else
                        blnFlag = False
                    End If
                Next
                If blnFlag = True Then
                    If Me.ViewState("IsLink") IsNot Nothing Then
                        Session("clsuser") = Nothing
                        msg.DisplayMessage("Information successfully saved.", Me, "~/../../Index.aspx")
                    Else
                        msg.DisplayMessage("Information successfully saved.", Me)
                        popOperation.Hide()
                        Call Fill_Grid()
                    End If
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDisapprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisapprove.Click
        Try
            If Me.ViewState("sAnalysisIds") IsNot Nothing AndAlso Me.ViewState("sAnalysisIds").ToString.Trim.Length > 0 Then
                Dim objAppVac As New clsApplicant_Vacancy_Mapping
                For Each sKeyId As String In Me.ViewState("sAnalysisIds").ToString.Replace("'", "").Split(CChar(","))
                    objInterviewAnalysisMaster._Analysisunkid = CInt(sKeyId)
                    If objAppVac.isExist(objInterviewAnalysisMaster._Applicantunkid, CInt(cboVacancy.SelectedValue), , , True) = True Then
                        Dim objApplicant As New clsApplicant_master
                        objApplicant._Applicantunkid = objInterviewAnalysisMaster._Applicantunkid

                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        'Language.setLanguage(mstrModuleName1)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry you cannot disapprove the selected applicant [") & objApplicant._Firstname & " " & objApplicant._Surname & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "]. Reason : Selected applicant is already imported as an employee."), Me)
                        'Anjan [04 June 2014] -- End
                        Exit Sub
                    End If
                Next
                objAppVac = Nothing
                If txtRemark.Text.Trim.Length <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName1)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Remark is mandatory information. Please enter remark to continue."), Me)
                    'Anjan [04 June 2014] -- End
                    dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    popOperation.Show()
                    Exit Sub
                End If
                If dtApprDate.IsNull = True Then
                    msg.DisplayMessage("Disapproved Date is mandatory information. Please set approval date to continue.", Me)
                    dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    popOperation.Show()
                    Exit Sub
                End If
                dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                popOperation.Show()
                If Me.ViewState("Operation") IsNot Nothing Then
                    Me.ViewState.Add("Operation", "2")
                Else
                    Me.ViewState("Operation") = 2
                End If
                popConfirm2.Title = "Disapproving Eligible Applicants"

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName1)
                popConfirm2.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "You are about to disapprove all eligible applicant. Due to this all disapproved applicant will be not available for import applicant for employeee.") & vbCrLf & _
                                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 7, "Do you wish to continue?")
                'Anjan [04 June 2014] -- End

                popConfirm2.Show()
                Exit Sub
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.ViewState("IsLink") IsNot Nothing Then
                Response.Redirect("~\Index.aspx", False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        If Me.ViewState("IsLink") IsNot Nothing Then
    '            Response.Redirect("~\Index.aspx")
    '        Else
    '            Response.Redirect("~\UserHome.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

#Region " Contols Event(s) "

    Protected Sub cboVacType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVacType.SelectedIndexChanged
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            Dim objVacancy As New clsVacancy : Dim dsVac As New DataSet

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsVac = objVacancy.getComboList(True, "List", , CInt(IIf(cboVacType.SelectedValue = "", 0, cboVacType.SelectedValue)))
            dsVac = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(IIf(cboVacType.SelectedValue = "", 0, cboVacType.SelectedValue)))
            'Shani(20-Nov-2015) -- End

            With cboVacancy
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsVac.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboVacancy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            If CInt(IIf(cboVacancy.SelectedValue = "", 0, cboVacancy.SelectedValue)) > 0 Then
                Dim dsBatch As New DataSet
                Dim objBatch As New clsBatchSchedule

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'dsBatch = objBatch.getListForCombo("List", True)
                dsBatch = objBatch.getListForCombo(CDate(Session("fin_startdate")).Date, "List", True)
                'Pinkal (16-Apr-2016) -- End

                Dim dtFilter = New DataView(dsBatch.Tables(0), "vacancyid IN(0," & CInt(cboVacancy.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable
                With cboBatch
                    .DataValueField = "id"
                    .DataTextField = "name"
                    .DataSource = dtFilter
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                cboBatch.DataSource = Nothing
                cboBatch.DataBind()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBatch.SelectedIndexChanged
        Try
            If CInt(IIf(cboBatch.SelectedValue = "", 0, cboBatch.SelectedValue)) > 0 Then
                Dim dsResult As New DataSet
                Dim objBatch As New clsBatchSchedule
                Dim objResult As New clsresult_master
                objBatch._Batchscheduleunkid = CInt(cboBatch.SelectedValue)
                dsResult = objResult.getComboList("List", True, objBatch._Resultgroupunkid)
                objBatch = Nothing
                With cboResult
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsResult.Tables("List")
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                cboResult.DataSource = Nothing
                cboResult.DataBind()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtFromDate.TextChanged
        Try
            dtToDate.SetDate = dtFromDate.GetDate
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If dgvData.Rows.Count <= 0 Then Exit Sub
            Dim dvData As DataTable = Me.ViewState("dtTable")
            For i As Integer = 0 To dgvData.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvData.Rows(i)
                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
                Dim dRow() As DataRow = dvData.Select("interview_type = '" & gvRow.Cells(1).Text & "' AND is_grp = True")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked
                End If
                dvData.AcceptChanges()
            Next
            Me.ViewState("dtTable") = dvData
            Call SetGridDataSource(dvData)
            CType(dgvData.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = cb.Checked
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                Dim dRow() As DataRow = CType(Me.ViewState("dtTable"), DataTable).Select("interview_type = '" & gvr.Cells(1).Text & "' AND is_grp = True")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked
                End If
                CType(Me.ViewState("dtTable"), DataTable).AcceptChanges()
                Dim drRow() As DataRow = CType(Me.ViewState("dtTable"), DataTable).Select("ischeck = 1")
                If CType(Me.ViewState("dtTable"), DataTable).Rows.Count = drRow.Length Then
                    CType(dgvData.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = True
                Else
                    CType(dgvData.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = False
                End If
            End If
            Call SetGridDataSource(CType(Me.ViewState("dtTable"), DataTable))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CType(Me.ViewState("dtTable"), DataTable).Rows(e.Row.RowIndex).Item("is_grp") = False Then
                    e.Row.Cells(0).Text = ""
                ElseIf CType(Me.ViewState("dtTable"), DataTable).Rows(e.Row.RowIndex).Item("is_grp") = True Then

                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'e.Row.BackColor = Drawing.Color.SteelBlue : e.Row.ForeColor = Drawing.Color.White
                    e.Row.CssClass = "GroupHeaderStyle"
                    'SHANI [01 FEB 2015]--END
                    e.Row.Font.Bold = True
                    If CType(Me.ViewState("dtTable"), DataTable).Rows(e.Row.RowIndex).Item("ischeck") = True Then
                        CType(e.Row.FindControl("chkbox1"), CheckBox).Checked = True
                    Else
                        CType(e.Row.FindControl("chkbox1"), CheckBox).Checked = False
                    End If
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvViewData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvViewData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CType(Me.ViewState("mdtTable"), DataTable).Rows(e.Row.RowIndex).Item("is_grp") = True Then
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'e.Row.BackColor = Drawing.Color.SteelBlue : e.Row.ForeColor = Drawing.Color.White
                    e.Row.CssClass = "GroupHeaderStyle"
                    'SHANI [01 FEB 2015]--END
                    e.Row.Font.Bold = True
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dtApprDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtApprDate.TextChanged
        Try
            dtApprDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            popOperation.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lblApplicant.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApplicant.ID, Me.lblApplicant.Text)
            Me.lblInterviewType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterviewType.ID, Me.lblInterviewType.Text)
            Me.lbAnalysisDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lbAnalysisDateTo.ID, Me.lbAnalysisDateTo.Text)
            Me.lblAnalysisDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAnalysisDateFrom.ID, Me.lblAnalysisDateFrom.Text)

            Me.btnEligible.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnEligible.ID, Me.btnEligible.Text)

            Me.lblVacanyType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVacanyType.ID, Me.lblVacanyType.Text)
            Me.lblVacancy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVacancy.ID, Me.lblVacancy.Text)
            Me.lblBatch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBatch.ID, Me.lblBatch.Text)
            Me.lblResult.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblResult.ID, Me.lblResult.Text)

            Me.chkNotEligible.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkNotEligible.ID, Me.chkNotEligible.Text)
            Me.chkEligible.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkEligible.ID, Me.chkEligible.Text)
            Me.btnEligible.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuEligible", Me.btnEligible.Text).Replace("&", "")
            Me.btnNEligible.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuNotEligible", Me.btnNEligible.Text).Replace("&", "")

            Me.dgvData.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(0).FooterText, Me.dgvData.Columns(0).HeaderText)
            Me.dgvData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(1).FooterText, Me.dgvData.Columns(1).HeaderText)
            Me.dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)

            'Language.setLanguage(mstrModuleName1)

            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.lblCaption.Text)
            Me.btnDisapprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnDisapprove.ID, Me.btnDisapprove.Text).Replace("&", "")
            Me.btnpopClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnpopClose.Text).Replace("&", "")
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "eLine1", Me.lblRemark.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)


            Me.dgvViewData.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvViewData.Columns(0).FooterText, Me.dgvViewData.Columns(0).HeaderText)
            Me.dgvViewData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvViewData.Columns(1).FooterText, Me.dgvViewData.Columns(1).HeaderText)
            Me.dgvViewData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvViewData.Columns(2).FooterText, Me.dgvViewData.Columns(2).HeaderText)
            Me.dgvViewData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvViewData.Columns(3).FooterText, Me.dgvViewData.Columns(3).HeaderText)
            Me.dgvViewData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvViewData.Columns(4).FooterText, Me.dgvViewData.Columns(4).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class