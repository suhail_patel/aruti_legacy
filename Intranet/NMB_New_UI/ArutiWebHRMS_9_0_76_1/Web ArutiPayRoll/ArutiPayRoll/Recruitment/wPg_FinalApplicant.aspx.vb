﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class wPg_FinalApplicant
    Inherits Basepage

#Region " Private Variable(s) "

    Dim msg As New CommonCodes
    Private clsuser As New User
    Private objCONN As SqlConnection
    Private objShortList As New clsshortlist_master
    Private objShortListFinal As New clsshortlist_finalapplicant
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmFinalApplicant"

    'Anjan [04 June 2014] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objVacancy As New clsVacancy
        Dim objMaster As New clsMasterData
        Try
            dsCombo = objVacancy.getVacancyType()
            With cboVacancyType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objMaster.GetComboforShortlistingApprovalStatus(True, "List")
            'Dim dtTab As DataTable = New DataView(dsCombo.Tables(0), "Id <> '" & enShortListing_Status.SC_PENDING & "'", "Id", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetGridDataSource(ByVal iDataSource As DataTable)
        Try
            gvList.AutoGenerateColumns = False
            gvList.DataSource = iDataSource
            gvList.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim iSearch As String = String.Empty
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If CBool(Session("AllowToViewFinalApplicantList")) = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End
            iSearch &= "isfinalshortlisted =  1 "

            If CInt(cboStatus.SelectedValue) > 0 Then
                iSearch &= " AND AUD <> 'D' AND app_statusid = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If CInt(cboRefNo.SelectedValue) > 0 Then
                iSearch &= " AND  shortlistunkid = " & CInt(cboRefNo.SelectedValue)
            End If

            If CInt(cboVacancy.SelectedValue) > 0 Then
                iSearch &= " AND app_issent = 1 "
            End If


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            Dim mdtFinalTran As DataTable
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            'mdtFinalTran = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", False, CInt(cboVacancy.SelectedValue), False, True)
            mdtFinalTran = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", False, CInt(cboVacancy.SelectedValue), False, True, dtAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
            'Sohail (13 Mar 2020) -- End
            mdtFinalTran = New DataView(mdtFinalTran, iSearch, "", DataViewRowState.CurrentRows).ToTable(True, _
                            "ischecked", "finalapplicantunkid", "applicantunkid", "isshortlisted", "isfinalshortlisted", "ACode", "Aname", "Gender", "BDate", "Phone", "Email", "AUD", "app_statusid", "status", "app_issent", "refno")

            If Me.ViewState("mdtFinalTran") IsNot Nothing Then
                Me.ViewState("mdtFinalTran") = mdtFinalTran
            Else
                Me.ViewState.Add("mdtFinalTran", mdtFinalTran)
            End If

            If mdtFinalTran.Rows.Count > 0 Then
                If Session("AllowToApproveFinalShortListedApplicant") Then
                    btnApprove.Visible = True
                End If

                If Session("AllowToDisapproveFinalShortListedApplicant") Then
                    btnDisapprove.Visible = True
                End If
            Else
                btnApprove.Visible = False
                btnDisapprove.Visible = False
            End If

            If mdtFinalTran IsNot Nothing Then
                Dim dtFinalApplicant As DataTable = mdtFinalTran.Copy
                Dim drRow() As DataRow = Nothing
                If CInt(cboStatus.SelectedValue) = enShortListing_Status.SC_PENDING Then
                    drRow = dtFinalApplicant.Select("app_issent = 0")
                    If drRow.Length > 0 Then
                        btnApprove.Visible = False
                        btnDisapprove.Visible = False
                    Else
                        btnApprove.Visible = Session("AllowToApproveFinalShortListedApplicant")
                        btnDisapprove.Visible = Session("AllowToDisapproveFinalShortListedApplicant")
                    End If
                ElseIf CInt(cboStatus.SelectedValue) = enShortListing_Status.SC_APPROVED Then
                    btnApprove.Visible = False
                    btnDisapprove.Visible = Session("AllowToDisapproveFinalShortListedApplicant")

                ElseIf CInt(cboStatus.SelectedValue) = enShortListing_Status.SC_REJECT Then
                    btnApprove.Visible = btnApprove.Visible = Session("AllowToApproveFinalShortListedApplicant")
                    btnDisapprove.Visible = False
                Else
                    cboVacancy_SelectedIndexChanged(New Object(), New EventArgs())
                End If
            End If


            Call SetGridDataSource(mdtFinalTran)

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Page.IsPostBack = False) AndAlso Session("iQueryString") IsNot Nothing Then
                btnApprove.Visible = False : btnDisapprove.Visible = False
                Session("clsuser") = Nothing
            End If

      
            If (Session("clsuser") Is Nothing AndAlso Request.QueryString.Count > 0) Then
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If Me.ViewState("IsLink") IsNot Nothing Then
                    Me.ViewState("IsLink") = True
                Else
                    Me.ViewState.Add("IsLink", True)
                End If

                If arr.Length = 6 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))

                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmFinalApplicant"
                    StrModuleName1 = "mnuHumanResource"
                    StrModuleName2 = "mnuRecruitmentItem"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")
                    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
                    End If

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objCommon As New CommonCodes
                    'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End
                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    Session("IsImgInDataBase") = gobjConfigOptions._IsImgInDataBase
                    Session("CompanyDomain") = gobjConfigOptions._CompanyDomain


                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    Call FillCombo()

                    cboVacancyType.SelectedValue = CInt(arr(2))
                    Call cboVacancyType_SelectedIndexChanged(New Object, New EventArgs)
                    cboVacancyType.Enabled = False

                    cboVacancy.SelectedValue = CInt(arr(3))
                    cboVacancy_SelectedIndexChanged(New Object, New EventArgs)
                    cboVacancy.Enabled = False

                    cboRefNo.SelectedValue = CInt(arr(4)) : cboRefNo.Enabled = False
                    cboStatus.SelectedValue = CInt(arr(5)) : cboStatus.Enabled = False

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)) = False Then
                        msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Sub
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Sub
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))

                    'Sohail (21 Mar 2015) -- Start
                    'Enhancement - New UI Notification Link Changes.
                    'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    'Sohail (21 Mar 2015) -- End
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objUserPrivilege As New clsUserPrivilege

                    'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    'Session("AllowToApproveFinalShortListedApplicant") = objUserPrivilege._AllowToApproveFinalShortListedApplicant
                    'Session("AllowToDisapproveFinalShortListedApplicant") = objUserPrivilege._AllowToDisapproveFinalShortListedApplicant
                    'Sohail (30 Mar 2015) -- End

                    btnApprove.Visible = Session("AllowToApproveFinalShortListedApplicant")
                    btnDisapprove.Visible = Session("AllowToDisapproveFinalShortListedApplicant")

                    Call btnSearch_Click(New Object, New EventArgs)
                    btnSearch.Visible = False : btnReset.Visible = False

                    If Session("iQueryString") Is Nothing Then Session("iQueryString") = Request.QueryString.ToString
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    HttpContext.Current.Session("Login") = True
                    'Sohail (30 Mar 2015) -- End
                    Exit Sub
                End If

            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmFinalApplicant"
            StrModuleName1 = "mnuHumanResource"
            StrModuleName2 = "mnuRecruitmentItem"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            End If

            If (Page.IsPostBack = False) Then

                'Hemant (06 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                Call SetLanguage()
                'Hemant (06 Aug 2019) -- End


                clsuser = CType(Session("clsuser"), User)
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    btnApprove.Visible = Session("AllowToApproveFinalShortListedApplicant")
                    btnDisapprove.Visible = Session("AllowToDisapproveFinalShortListedApplicant")
                End If
                Call FillCombo()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboVacancy.SelectedValue = "", 0, cboVacancy.SelectedValue)) <= 0 Then
                msg.DisplayMessage("Vacancy is mandatory information. Please select vacancy to continue.", Me)
                Exit Sub
            End If
            Call Fill_Grid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboRefNo.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            cboVacancyType.SelectedValue = 0
            txtOpeningDate.Text = ""
            txtClosingDate.Text = ""
            gvList.DataSource = Nothing
            gvList.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If Me.ViewState("mdtFinalTran") IsNot Nothing Then
                Dim mdtFinalApplicant As DataTable = Me.ViewState("mdtFinalTran")
                Dim drRow() As DataRow = mdtFinalApplicant.Select("isChecked = true AND AUD <> 'D' ")
                If drRow.Length <= 0 Then
                    msg.DisplayMessage("Please Check atleast one Final Short listed applicant to approve.", Me)
                    Exit Sub
                End If
                drRow = mdtFinalApplicant.Select("ischecked = true AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_APPROVED)
                If drRow.Length > 0 Then
                    msg.DisplayMessage("You cannot approve some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are already in approved status.", Me)
                    Exit Sub
                End If
                If Me.ViewState("Operation") IsNot Nothing Then
                    Me.ViewState("Operation") = 1
                Else
                    Me.ViewState.Add("Operation", 1)
                End If
                popupConfirm.Title = "Approve Final Short Listed Applicant(s)"
                popupConfirm.Message = "Are You sure want to approve final short listed applicant(s) ?"
                popupConfirm.Show()
                Exit Sub
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDisapprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisapprove.Click
        Try
            If Me.ViewState("mdtFinalTran") IsNot Nothing Then
                Dim mdtFinalApplicant As DataTable = Me.ViewState("mdtFinalTran")
                Dim drRow() As DataRow = mdtFinalApplicant.Select("isChecked = true AND AUD <> 'D' ")
                If drRow.Length <= 0 Then
                    msg.DisplayMessage("Please Check atleast one Final Short listed applicant to disapprove.", Me)
                    Exit Sub
                End If

                drRow = mdtFinalApplicant.Select("ischecked = true AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_REJECT)
                If drRow.Length > 0 Then
                    msg.DisplayMessage("You cannot disapprove some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are in disapproved status.", Me)
                    Exit Sub
                End If

                ' START FOR CHECK WHETHER APPLICANT IS EXIST IN BATCH SCHEDULING FOR PARTICULAR VACANCY
                Dim objBatch As New clsBatchSchedule
                Dim dtRow() As DataRow = mdtFinalApplicant.Select("ischecked = true AND AUD <> 'D' AND (app_statusid = " & enShortListing_Status.SC_PENDING & " OR app_statusid = " & enShortListing_Status.SC_APPROVED & ")")
                If dtRow.Length > 0 Then
                    For i As Integer = 0 To dtRow.Length - 1
                        Dim mdtBatchTran As DataTable = objBatch.GetApplicantFromVacancy(CInt(cboVacancy.SelectedValue), CInt(dtRow(i)("applicantunkid")))
                        If mdtBatchTran.Rows.Count > 0 Then
                            msg.DisplayMessage("You cannot disapprove some of the applicant(s).Reason : some of the applicant(s) are already exist in batch scheduling transaction.", Me)
                            Exit Sub
                        End If
                    Next
                End If
                ' END FOR CHECK WHETHER APPLICANT IS EXIST IN BATCH SCHEDULING FOR PARTICULAR VACANCY
                If Me.ViewState("Operation") IsNot Nothing Then
                    Me.ViewState("Operation") = 2
                Else
                    Me.ViewState.Add("Operation", 2)
                End If
                popupConfirm.Title = "Disapprove Final Short Listed Applicant(s)"
                popupConfirm.Message = "Are You sure want to disapprove final short listed applicant(s) ?"
                popupConfirm.Show()
                Exit Sub
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirm.buttonYes_Click
        Try
            Select Case CInt(Me.ViewState("Operation"))
                Case 1  'APPROVED
                    If objShortListFinal.ApproveFinalApplicant(CInt(cboVacancy.SelectedValue), Me.ViewState("mdtFinalTran")) Then
                        Call Fill_Grid()
                        If Me.ViewState("IsLink") IsNot Nothing Then
                            Session("clsuser") = Nothing
                            msg.DisplayMessage("Final shorted Applicant(s) approved successfully.", Me, "~/../../Index.aspx")
                        Else
                            msg.DisplayMessage("Final shorted Applicant(s) approved successfully.", Me)
                        End If
                    Else
                        msg.DisplayMessage("Final shorted Applicant(s) approval process fail .", Me)
                    End If
                Case 2  'DISAPPROVED
                    If objShortListFinal.DisapproveFinalApplicant(CInt(cboVacancy.SelectedValue), Me.ViewState("mdtFinalTran")) Then
                        Call Fill_Grid()
                        If Me.ViewState("IsLink") IsNot Nothing Then
                            Session("clsuser") = Nothing
                            msg.DisplayMessage("Final shorted Applicant(s) disapproved successfully.", Me, "~/../../Index.aspx")
                        Else
                            msg.DisplayMessage("Final shorted Applicant(s) disapproved successfully.", Me)
                        End If
                    Else
                        msg.DisplayMessage("Final shorted Applicant(s) disapproved process fail .", Me)
                    End If
            End Select
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.ViewState("IsLink") IsNot Nothing Then
                Response.Redirect("~\Index.aspx", False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        If Me.ViewState("IsLink") IsNot Nothing Then
    '            Response.Redirect("~\Index.aspx")
    '        Else
    '            Response.Redirect("~\UserHome.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region " Controls Events "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objVacancy.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue), True)
            Dim dsList As DataSet = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue), True)
            'Shani(20-Nov-2015) -- End

            With cboVacancy
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing

            dsList = objShortList.GetList("List", True)

            Dim dtList As DataTable = New DataView(dsList.Tables(0), "vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " AND statustypid = 1", "", DataViewRowState.CurrentRows).ToTable

            If CInt(cboVacancy.SelectedValue) > 0 Then
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                SetDateFormat()
                'Pinkal (16-Apr-2016) -- End

                Dim objvacacny As New clsVacancy
                objvacacny._Vacancyunkid = CInt(cboVacancy.SelectedValue)
                txtOpeningDate.Text = objvacacny._Openingdate.ToShortDateString
                txtClosingDate.Text = objvacacny._Closingdate.ToShortDateString
            Else
                txtOpeningDate.Text = ""
                txtClosingDate.Text = ""
            End If

            Dim drRow As DataRow = dtList.NewRow
            drRow("refno") = "Select"
            drRow("shortlistunkid") = 0
            drRow("vacancyunkid") = 0
            drRow("statustypid") = 1
            dtList.Rows.InsertAt(drRow, 0)

            With cboRefNo
                .DataValueField = "shortlistunkid"
                .DataTextField = "refno"
                .DataSource = dtList
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            For i As Integer = 0 To gvList.Rows.Count - 1
                Dim gvRow As GridViewRow = gvList.Rows(i)
                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'Dim dRow() As DataRow = CType(Me.ViewState("mdtFinalTran"), DataTable).Select("refno = '" & gvRow.Cells(1).Text & "'")
                Dim dRow() As DataRow = CType(Me.ViewState("mdtFinalTran"), DataTable).Select("refno = '" & gvRow.Cells(1).Text.Trim.Replace("'", "''") & "'")
                'Pinkal (16-Apr-2016) -- End
                If dRow.Length > 0 Then
                    dRow(0).Item("ischecked") = cb.Checked
                End If
                CType(Me.ViewState("mdtFinalTran"), DataTable).AcceptChanges()
            Next
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                Dim dRow() As DataRow = CType(Me.ViewState("mdtFinalTran"), DataTable).Select("refno = '" & gvr.Cells(1).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischecked") = cb.Checked
                End If
                CType(Me.ViewState("mdtFinalTran"), DataTable).AcceptChanges()
                Dim drRow() As DataRow = CType(Me.ViewState("mdtFinalTran"), DataTable).Select("ischecked = 1")
                If CType(Me.ViewState("mdtFinalTran"), DataTable).Rows.Count = drRow.Length Then
                    CType(gvList.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = True
                Else
                    CType(gvList.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = False
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.lblRefNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRefNo.ID, Me.lblRefNo.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
            Me.LblVacancyType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblVacancyType.ID, Me.LblVacancyType.Text)
            Me.lblEnddate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEnddate.ID, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStartdate.ID, Me.lblStartdate.Text)
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text)
            Me.btnDisapprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnDisapprove.ID, Me.btnDisapprove.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblStatus.ID, Me.LblStatus.Text)

            Me.gvList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvList.Columns(0).FooterText, Me.gvList.Columns(0).HeaderText)
            Me.gvList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvList.Columns(1).FooterText, Me.gvList.Columns(1).HeaderText)
            Me.gvList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvList.Columns(2).FooterText, Me.gvList.Columns(2).HeaderText)
            Me.gvList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvList.Columns(3).FooterText, Me.gvList.Columns(3).HeaderText)
            Me.gvList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvList.Columns(4).FooterText, Me.gvList.Columns(4).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class