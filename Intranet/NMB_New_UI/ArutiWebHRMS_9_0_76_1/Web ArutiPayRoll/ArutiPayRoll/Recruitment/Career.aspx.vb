﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Data

#End Region

Partial Class Recruitment_Career
    Inherits System.Web.UI.Page

#Region " Private Variable(s) "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmSearchJob"
#End Region

#Region " Method Functions "

    Private Sub FillVacancyList(ByVal strDatabaseName As String _
                                , ByVal strExtInt As String _
                                )
        Try

            'odsVacancy.SelectParameters.Item("strCompCode").DefaultValue = strCompCode
            'odsVacancy.SelectParameters.Item("intComUnkID").DefaultValue = intCompanyunkid
            odsVacancy.SelectParameters.Item("strDatabaseName").DefaultValue = strDatabaseName
            odsVacancy.SelectParameters.Item("intMasterTypeId").DefaultValue = clsCommon_Master.enCommonMaster.VACANCY_MASTER
            odsVacancy.SelectParameters.Item("intEType").DefaultValue = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE
            If strExtInt = "9" Then
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = False
            Else
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = True
            End If
            odsVacancy.SelectParameters.Item("blnAllVacancy").DefaultValue = False
            odsVacancy.SelectParameters.Item("intDateZoneDifference").DefaultValue = 0
            odsVacancy.SelectParameters.Item("strVacancyUnkIdLIs").DefaultValue = ""
            odsVacancy.SelectParameters.Item("intApplicantUnkId").DefaultValue = 0
            odsVacancy.SelectParameters.Item("blnOnlyCurrent").DefaultValue = False
            odsVacancy.SelectParameters.Item("blnOnlyExportToWeb").DefaultValue = False 'Sohail (14 Oct 2020)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Session("LangId") = 1

                Dim strComp As String = ""
                Dim intCompID As Integer = 0
                Dim ExtInt As String = "7"

                If Request.QueryString("code") Is Nothing OrElse Request.QueryString("code") = "" Then
                    Response.Redirect("~/Index.aspx", False)
                    Exit Try
                Else

                    strComp = Request.QueryString("code").ToString

                    If Request.QueryString("ext") IsNot Nothing Then
                        ExtInt = Request.QueryString("ext").ToString
                    End If

                    Dim objMaster As New clsMasterData
                    Dim objBase As New Basepage
                    If Request.QueryString("code") IsNot Nothing Then
                        Dim objCompany As New clsCompany_Master
                        Dim intCompanyID As Integer = objCompany.GetIdByCode(Request.QueryString("code").ToString)

                        If intCompanyID > 0 Then
                            Dim strError As String = ""
                            objBase.GetCompanyYearInfo(strError, intCompanyID)

                            'Hemant (08 Jul 2021) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                            If Request.UrlReferrer Is Nothing Then
                                gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("Database_Name").ToString)
                                gobjLocalization._LangId = HttpContext.Current.Session("LangId")
                            End If
                            'Hemant (08 Jul 2021) -- End

                        Else
                            Response.Redirect("~/Index.aspx", False)
                            Exit Try
                        End If
                    Else
                        Response.Redirect("~/Index.aspx", False)
                        Exit Try
                    End If

                End If

                Call FillVacancyList(Session("Database_Name").ToString, ExtInt)

                'Hemant (08 Jul 2021) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                Call SetControlCaptions()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                'Hemant (08 Jul 2021) -- End
            Else

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SearchJob_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataList Events "

    Private Sub dlVaanciesList_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlVaanciesList.ItemCommand
        Try
            If e.CommandName.ToUpper = "APPLY" Then
                If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Dim Idx As Integer = e.Item.ItemIndex

                Dim ExtInt As String = "7"
                If Request.QueryString("ext") IsNot Nothing Then
                    ExtInt = Request.QueryString("ext").ToString
                End If
                'Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/Login.aspx?ext=" & ExtInt & "&cc=" & CType(dlVaanciesList.Items(Idx).FindControl("objlblAuthCode"), Label).Text & "", False)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub dlVaanciesList_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlVaanciesList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl As Label = CType(e.Item.FindControl("objlblIsApplyed"), Label)
                CType(e.Item.FindControl("objlblAuthCode"), Label).Visible = False

                If CBool(lbl.Text) = True Then
                    CType(e.Item.FindControl("lblApplyText"), Label).Text = "Applied"
                    CType(e.Item.FindControl("btnApply"), LinkButton).Enabled = False
                    CType(e.Item.FindControl("divApplied"), Control).Visible = True
                Else

                End If
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("experience").ToString.Trim.Length <= 0 Then
                CType(e.Item.FindControl("objlblVacancyTitle"), Label).Text = drv.Item("vacancytitle").ToString & " (" & drv.Item("noposition").ToString & " " & lblPositionMsg.Text & ")"
                'If drv.Item("experience").ToString.Trim.Length <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                'If CInt(drv.Item("experience")) <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                If CInt(drv.Item("experience")) <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divExp"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isexpbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isexpitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    'If drv.Item("other_experience").ToString.Trim = "" Then
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    Else
                        'Dim htmlOutput = "Document.html"
                        'Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                        'Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_experience").ToString, contentUriPrefix)
                        'htmlResult.WriteToFile(htmlOutput)
                        'CType(e.Item.FindControl("objlblExp"), Label).Text = htmlResult._HTML
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length > 0 Then
                        If drv.Item("other_experience").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_experience").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_experience").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If
                            Dim strRTF As String = drv.Item("other_experience").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_experience").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblExp"), Label).Text = strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If drv.Item("noposition").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divNoPosition"), Control).Visible = False
                End If

                If drv.Item("experience_comment").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblExpCmt"), Control).Visible = False
                End If

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim arrJobLocation As New ArrayList
                If Session("CompCode").ToString.ToUpper = "NMB" Then
                    If drv.Item("classgroupname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classgroupname").ToString)
                    End If
                    If drv.Item("classname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classname").ToString)
                    End If
                End If
                CType(e.Item.FindControl("objlblJobLocation"), Label).Text = String.Join(", ", TryCast(arrJobLocation.ToArray(GetType(String)), String()))
                If arrJobLocation.Count <= 0 Then
                    CType(e.Item.FindControl("divJobLocation"), Control).Visible = False
                End If
                'Hemant (01 Nov 2021) -- End

                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divSkill"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isskillbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isskillitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Hemant (08 Jul 2021) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'If drv.Item("other_skill").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_skill").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                        'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                        Dim str As String = ""
                        If drv.Item("skill").ToString.Trim.Length > 0 Then
                            str = "<ul class='p-l-17'>"
                            Dim arr() As String = drv.Item("skill").ToString.Split(";")
                            For i As Integer = 0 To arr.Length - 1
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Next
                            str &= "</ul>"
                        End If
                        CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length > 0 Then
                        If drv.Item("other_skill").ToString.Trim.Length > 0 Then
                            Dim str As String = ""
                            If drv.Item("skill").ToString.Trim.Length > 0 Then
                                str = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("skill").ToString.Split(";")
                                For i As Integer = 0 To arr.Length - 1
                                    str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                Next
                                str &= "</ul>"
                            End If
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_skill").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_skill").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                            
                            Dim strRTF As String = drv.Item("other_skill").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_skill").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                    'Hemant (08 Jul 2021) -- End
                End If

                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("Lang").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divLang"), Control).Visible = False
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                Else
                    'If drv.Item("other_language").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_language").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_language").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length > 0 Then
                        If drv.Item("other_language").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_language").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_language").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                          
                            Dim strRTF As String = drv.Item("other_language").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_language").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If CDec(drv.Item("pay_from").ToString) <= 0 AndAlso CDec(drv.Item("pay_to").ToString) <= 0 Then
                    CType(e.Item.FindControl("divScale"), Control).Visible = False
                Else
                    Dim str As String = Format(CDec(drv.Item("pay_from").ToString), "##,##,##,##,##0.00") & " - " & Format(CDec(drv.Item("pay_to").ToString), "##,##,##,##,##0.00")
                    CType(e.Item.FindControl("objlblScale"), Label).Text = str
                End If

                If drv.Item("openingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divOpenningDate"), Control).Visible = False
                End If
                If drv.Item("closingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divClosuingDate"), Control).Visible = False
                End If
                If drv.Item("remark").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divRemark"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("remark").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblJobDiscription"), Label).Text = strResult
                End If
                If drv.Item("duties").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divResponsblity"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("duties").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblResponsblity"), Label).Text = strResult
                End If
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("Qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString).Length <= 0 Then
                If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divQualification"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isqualibold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isqualiitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'If drv.Item("other_qualification").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_qualification").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_qualification").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length > 0 Then
                        If drv.Item("other_qualification").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_qualification").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_qualification").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                           
                            Dim strRTF As String = drv.Item("other_qualification").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_qualification").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Sub odsVacancy_Selected(ByVal sender As Object, ByVal e As ObjectDataSourceStatusEventArgs) Handles odsVacancy.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is DataTable Then
                intC = CType(e.ReturnValue, DataTable).Rows.Count
            End If

            objlblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub odsVacancy_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsVacancy.Selecting
        Try
            If e.InputParameters("strDatabaseName") Is Nothing Then
                e.Cancel = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Hemant (08 Jul 2021) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPositionMsg.ID, lblPositionMsg.Text)

            For Each dli As DataListItem In dlVaanciesList.Items
                Dim lblJobDiscription As Label = CType(dli.FindControl("lblJobDiscription"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblJobDiscription.ID, lblJobDiscription.Text)
                Dim lblResponsblity As Label = CType(dli.FindControl("lblResponsblity"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblResponsblity.ID, lblResponsblity.Text)
                Dim lblSkill As Label = CType(dli.FindControl("lblSkill"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblSkill.ID, lblSkill.Text)
                Dim lblQualification As Label = CType(dli.FindControl("lblQualification"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblQualification.ID, lblQualification.Text)
                Dim lblExp As Label = CType(dli.FindControl("lblExp"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblExp.ID, lblExp.Text)
                Dim lblNoPosition As Label = CType(dli.FindControl("lblNoPosition"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblNoPosition.ID, lblNoPosition.Text)
                Dim lblLang As Label = CType(dli.FindControl("lblLang"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLang.ID, lblLang.Text)
                Dim lblScale As Label = CType(dli.FindControl("lblScale"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblScale.ID, lblScale.Text)
                Dim lblOpeningDate As Label = CType(dli.FindControl("lblOpeningDate"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblOpeningDate.ID, lblOpeningDate.Text)
                Dim lblClosingDate As Label = CType(dli.FindControl("lblClosingDate"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblClosingDate.ID, lblClosingDate.Text)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim lblJobLocation As Label = CType(dli.FindControl("lblJobLocation"), Label)
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblJobLocation.ID, lblJobLocation.Text)
                'Hemant (01 Nov 2021) -- End
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            lblPositionMsg.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPositionMsg.ID, lblPositionMsg.Text)

            For Each dli As DataListItem In dlVaanciesList.Items
                Dim lblJobDiscription As Label = CType(dli.FindControl("lblJobDiscription"), Label)
                lblJobDiscription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblJobDiscription.ID, lblJobDiscription.Text)
                Dim lblResponsblity As Label = CType(dli.FindControl("lblResponsblity"), Label)
                lblResponsblity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblResponsblity.ID, lblResponsblity.Text)
                Dim lblSkill As Label = CType(dli.FindControl("lblSkill"), Label)
                lblSkill.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblSkill.ID, lblSkill.Text)
                Dim lblQualification As Label = CType(dli.FindControl("lblQualification"), Label)
                lblQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblQualification.ID, lblQualification.Text)
                Dim lblExp As Label = CType(dli.FindControl("lblExp"), Label)
                lblExp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblExp.ID, lblExp.Text)
                Dim lblNoPosition As Label = CType(dli.FindControl("lblNoPosition"), Label)
                lblNoPosition.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblNoPosition.ID, lblNoPosition.Text)
                Dim lblLang As Label = CType(dli.FindControl("lblLang"), Label)
                lblLang.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLang.ID, lblLang.Text)
                Dim lblScale As Label = CType(dli.FindControl("lblScale"), Label)
                lblScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblScale.ID, lblScale.Text)
                Dim lblOpeningDate As Label = CType(dli.FindControl("lblOpeningDate"), Label)
                lblOpeningDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblOpeningDate.ID, lblOpeningDate.Text)
                Dim lblClosingDate As Label = CType(dli.FindControl("lblClosingDate"), Label)
                lblClosingDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblClosingDate.ID, lblClosingDate.Text)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim lblJobLocation As Label = CType(dli.FindControl("lblJobLocation"), Label)
                lblJobLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblJobLocation.ID, lblJobLocation.Text)
                'Hemant (01 Nov 2021) -- End
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (08 Jul 2021) -- End

End Class
