﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO
Imports ArutiReports

#End Region

Partial Class Recruitment_Staff_Requisition_wPg_StaffRequisition_AddEdit
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim strSearching As String = ""
    Private mstrModuleName As String = "frmStaffRequisition_AddEdit"

    Private mdicAllocaions As New Dictionary(Of Integer, String)
    Private mdtEmployee As DataTable
    'SHANI (18 Mar 2015) -- Start
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
    Private mdtOldTable As DataTable = Nothing
    Private mdtNewTable As DataTable = Nothing
    Private mdtList As DataTable = Nothing
    Private mdtView As DataView = Nothing
    Private marrOldEmp As New ArrayList
    Private mstrRowFilter As String = ""
    'SHANI (18 Mar 2015) -- End
    Private mintStaffrequisitionbyid As Integer = 0 'Hemant (02 Jul 2019)
    'Hemant (13 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
    Private marrSelectedEmployeeList As New ArrayList
    Private mblnIsSelectedEmployeeListChanged As Boolean = False
    'Hemant (13 Sep 2019) -- End


    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Private mdtStaffrequisitionDocument As DataTable
    Private objDocument As New clsScan_Attach_Documents
    'Gajanan [6-NOV-2019] -- End
#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If



            If CInt(Session("StaffReqFormNoType")) = 1 Then
                txtFormNo.Enabled = False
            Else
                txtFormNo.Enabled = True
            End If

            If Session("staffrequisitiontranunkid") Is Nothing Then
                btnSave.Visible = Session("AllowToAddStaffRequisition")
            Else
                btnSave.Visible = Session("AllowToEditStaffRequisition")
            End If

            If Not IsPostBack Then
                Call SetLanguage()
                Call CreateTable() 'SHANI (18 Mar 2015)
                FillCombo()
                Call GetValue()

                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                If IsNothing(ViewState("mdtStaffrequisitionDocument")) = False Then
                    mdtStaffrequisitionDocument = CType(ViewState("mdtStaffrequisitionDocument"), DataTable).Copy()
                Else
                    mdtStaffrequisitionDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.STAFF_REQUISITION, If(IsNothing(Session("staffrequisitiontranunkid")), -1, CInt(Session("staffrequisitiontranunkid"))), CStr(Session("Document_Path")))
    				'Gajanan [13-NOV-2019] -- Start   
				    'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
                    If IsNothing(Session("staffrequisitiontranunkid")) Then
                        mdtStaffrequisitionDocument.Rows.Clear()
                    End If
    				'Gajanan [13-NOV-2019] -- End
                End If
                Call FillQualificationAttachment()
                'Gajanan [6-NOV-2019] -- End
            Else
                mstrModuleName = ViewState("mstrModuleName")
                mdicAllocaions = ViewState("mdicAllocaions")
                mdtEmployee = ViewState("mdtEmployee")

                'SHANI (18 Mar 2015)-START
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                mdtOldTable = ViewState("mdtOldTable")
                mdtNewTable = ViewState("mdtNewTable")
                mdtList = ViewState("mdtList")
                marrOldEmp = ViewState("marrOldEmp")
                mstrRowFilter = ViewState("mstrRowFilter")
                'SHANI (18 Mar 2015)--END
                mintStaffrequisitionbyid = CInt(ViewState("mintStaffrequisitionbyid")) 'Hemant (02 Jul 2019)
                'Hemant (13 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
                marrSelectedEmployeeList = ViewState("marrSelectedEmployeeList")
                mblnIsSelectedEmployeeListChanged = ViewState("mblnIsSelectedEmployeeListChanged")
                'Hemant (13 Sep 2019) -- End

                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                mdtStaffrequisitionDocument = CType(ViewState("mdtStaffrequisitionDocument"), DataTable)
                'Gajanan [6-NOV-2019] -- End

            End If
            'Hemant (17 Oct 2019) -- Start
            'Hemant (30 Oct 2019) -- Start
            'lnkViewJobDescription.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.JobReport, CInt(Session("Userid")), Company._Object._Companyunkid)
            lnkViewJobDescription.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.JobReport, CInt(Session("Userid")), CInt(Session("Companyunkid")))
            'Hemant (30 Oct 2019) -- End
            'Hemant (17 Oct 2019) -- End


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB  


            If Request.QueryString("uploadimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If
            Exit Sub
            'Gajanan [6-NOV-2019] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrModuleName", mstrModuleName)
            Me.ViewState.Add("mdicAllocaions", mdicAllocaions)
            Me.ViewState.Add("mdtEmployee", mdtEmployee)
            'SHANI (18 Mar 2015)-START
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            Me.ViewState.Add("mdtOldTable", mdtOldTable)
            Me.ViewState.Add("mdtNewTable", mdtNewTable)
            Me.ViewState.Add("mdtList", mdtList)
            Me.ViewState.Add("marrOldEmp", marrOldEmp)
            Me.ViewState.Add("mstrRowFilter", mstrRowFilter)
            'SHANI (18 Mar 2015)--END
            Me.ViewState.Add("mintStaffrequisitionbyid", mintStaffrequisitionbyid) 'Hemant (02 Jul 2019)
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
            Me.ViewState("marrSelectedEmployeeList") = marrSelectedEmployeeList
            Me.ViewState("mblnIsSelectedEmployeeListChanged") = mblnIsSelectedEmployeeListChanged
            'Hemant (13 Sep 2019) -- End


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   

            If Request.QueryString("uploadimage") Is Nothing Then
                If ViewState("mdtStaffrequisitionDocument") Is Nothing Then
                    ViewState.Add("mdtStaffrequisitionDocument", mdtStaffrequisitionDocument)
                Else
                    ViewState("mdtStaffrequisitionDocument") = mdtStaffrequisitionDocument
                End If
            Else
                If ViewState("mdtStaffrequisitionDocument") Is Nothing Then
                    ViewState.Add("mdtStaffrequisitionDocument", mdtStaffrequisitionDocument)
                Else
                    ViewState("mdtStaffrequisitionDocument") = mdtStaffrequisitionDocument
                End If
            End If

            'Gajanan [6-NOV-2019] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)   'Hemant (13 Aug 2020)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objJob As New clsJobs
        Dim objClassGroup As New clsClassGroup
        Dim objReason As New clsAction_Reason
        'Sohail (12 Oct 2018) -- Start
        'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
        Dim objGrade As New clsGrade
        Dim objCommon As New clsCommon_Master
        'Sohail (12 Oct 2018) -- End
        Dim dsList As New DataSet
        Dim strFilter As String = ""
        'Hemant (02 Jul 2019) -- Start
        'ENHANCEMENT :  ZRA minimum Requirements
        Dim objStaffRequisition_approver_mapping As New clsStaffRequisition_approver_mapping
        Dim objStaffReq As New clsStaffrequisition_Tran
        'Hemant (02 Jul 2019) -- End
        Try


            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            'dsList = objMaster.GetEAllocation_Notification("List")
            dsList = objStaffRequisition_approver_mapping.GetOnlyMappedAllocatonList("List")
            'Hemant (29 Jul 2019) -- Start
            'ISSUE :  Error Occurs - "cboAllocation_SelectedIndexChanged:-Conversion from string "" to type Interger is not valid."  
            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = objMaster.GetEAllocation_Notification("List")
            End If
            'Hemant (29 Jul 2019) -- End
            'Hemant (02 Jul 2019) -- End
            With cboAllocation
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            mdicAllocaions = (From p In dsList.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            dsList = objStaffRequisition_approver_mapping.GetLastMappedAllocatonRow("List")
            If (dsList.Tables(0).Rows.Count > 0) Then
                Dim intLastMappedAllocatonId = CInt(dsList.Tables(0).Rows(0).Item("allocationid").ToString)
                cboAllocation.SelectedValue = intLastMappedAllocatonId
                Call cboAllocation_SelectedIndexChanged(cboAllocation, New System.EventArgs)
                objStaffReq._Staffrequisitionbyid = intLastMappedAllocatonId
                mintStaffrequisitionbyid = intLastMappedAllocatonId
            End If
            'Hemant (02 Jul 2019) -- End

            dsList = objMaster.GetListForStaffRequisitionStatus(True, "Status")
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
            End With

            dsList = objReason.getComboList("Reason", True, False)
            With cboLeavingReason
                .DataValueField = "actionreasonunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Reason")
                .DataBind()
            End With


            'SHANI (18 Mar 2015)-START
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            'strFilter = " ( ISNULL(CONVERT(CHAR(8),termination_from_date,112),'" & eZeeDate.convertDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).AddDays(1)) & "') <= '" & Session("EmployeeAsOnDate").ToString & "' " & _
            '            "   OR ISNULL(CONVERT(CHAR(8),termination_to_date,112),'" & eZeeDate.convertDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).AddDays(1)) & "') <= '" & Session("EmployeeAsOnDate").ToString & "' " & _
            '            "   OR ISNULL(CONVERT(CHAR(8), empl_enddate,112), '" & eZeeDate.convertDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).AddDays(1)) & "') <= '" & Session("EmployeeAsOnDate").ToString & "' ) "

            'dsList = objEmp.GetList("Emp", True, False, , , , , , strFilter)
            'Dim dR As DataRow = dsList.Tables("Emp").NewRow
            'dR.Item("employeeunkid") = 0
            ''Language.setLanguage(mstrModuleName)
            'dR.Item("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, " Select")
            'dsList.Tables("Emp").Rows.InsertAt(dR, 0)
            'mdtEmployee = dsList.Tables("Emp")
            'With cboEmployee
            '    .DataValueField = "employeeunkid"
            '    .DataTextField = "name"
            '    .DataSource = mdtEmployee
            '    .DataBind()
            'End With
            'SHANI (18 Mar 2015)--END

            dsList = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Job")
                .DataBind()
            End With

            'SHANI (18 Mar 2015)-START
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            Call cboJob_SelectedIndexChanged(cboJob, New EventArgs)
            'SHANI (18 Mar 2015)--END 

            dsList = objClassGroup.getComboList("ClassGroup", True)
            With cboClassGroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("ClassGroup")
                .DataBind()
                Call cboClassGroup_SelectedIndexChanged(cboClassGroup, New System.EventArgs)
            End With

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            dsList = objGrade.getComboList("Grade", True)
            With cboGrade
                .DataValueField = "gradeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Grade")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmpType")
            With cboEmploymentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("EmpType")
                .DataBind()
                .SelectedValue = 0
            End With
            'Sohail (12 Oct 2018) -- End

            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   


            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            'Gajanan [6-NOV-2019] -- End

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            dsList = objMaster.getJobAdvert("List")
            With cboJobAdvert
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            'Sohail (29 Sep 2021) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsList.Dispose() : objMaster = Nothing : objEmp = Nothing : objJob = Nothing : objClassGroup = Nothing
        End Try

    End Sub

    Private Sub GetValue()
        Dim objStaffReq As New clsStaffrequisition_Tran
        Try
            If Session("staffrequisitiontranunkid") IsNot Nothing Then
                objStaffReq._Staffrequisitiontranunkid = CInt(Session("staffrequisitiontranunkid"))
            End If
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            objlblPlanned.Text = Format(CInt(objStaffReq._Approved_Headcount), "0")
            objlblAvailable.Text = Format(CInt(objStaffReq._Actual_Headcount), "0")
            objlblVariation.Text = Format(CInt(objStaffReq._Approved_Headcount) - CInt(objStaffReq._Actual_Headcount), "0")
            'Sohail (12 Oct 2018) -- End
            txtFormNo.Text = objStaffReq._Formno
            cboStatus.SelectedValue = objStaffReq._Staffrequisitiontypeid
            Call cboStatus_SelectedIndexChanged(cboStatus, New System.EventArgs)
            If objStaffReq._Staffrequisitionbyid > 0 Then
                cboAllocation.SelectedValue = objStaffReq._Staffrequisitionbyid
            Else
                'Hemant (02 Jul 2019) -- Start
                'ENHANCEMENT :  ZRA minimum Requirements
                'cboAllocation.SelectedIndex = -1
                cboAllocation.SelectedValue = mintStaffrequisitionbyid
                objStaffReq._Staffrequisitionbyid = mintStaffrequisitionbyid
                'Hemant (02 Jul 2019) -- End
            End If
            Call cboAllocation_SelectedIndexChanged(cboAllocation, New System.EventArgs)
            cboName.SelectedValue = objStaffReq._Allocationunkid
            cboClassGroup.SelectedValue = objStaffReq._Classgroupunkid
            Call cboClassGroup_SelectedIndexChanged(cboClassGroup, New System.EventArgs)
            cboClass.SelectedValue = objStaffReq._Classunkid
            'SHANI (18 Mar 2015)-START
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            'cboEmployee.SelectedValue = objStaffReq._Employeeunkid
            'Call cboEmployee_SelectedIndexChanged(cboEmployee, New System.EventArgs)
            'SHANI (18 Mar 2015)--END
            cboLeavingReason.SelectedValue = objStaffReq._Actionreasonunkid
            txtAddStaffReason.Text = objStaffReq._Additionalstaffreason
            cboJob.SelectedValue = objStaffReq._Jobunkid
            Call cboJob_SelectedIndexChanged(cboJob, New System.EventArgs) 'Sohail (25 Jul 2014)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            'txtJobDesc.Text = objStaffReq._Jobdescrription
            If objStaffReq._Jobdescrription.StartsWith("{\rtf") Then
                Dim rtf As New System.Windows.Forms.RichTextBox
                rtf.Rtf = objStaffReq._Jobdescrription
                txtJobDesc.Text = rtf.Text
            Else
                txtJobDesc.Text = objStaffReq._Jobdescrription
            End If
            cboGradeLevel.SelectedValue = objStaffReq._Gradelevelunkid
            cboEmploymentType.SelectedValue = objStaffReq._Employmenttypeunkid
            nudContractDuration.Text = objStaffReq._Contract_Duration_Months
            'Sohail (12 Oct 2018) -- End

            If Session("staffrequisitiontranunkid") IsNot Nothing Then
                cboAllocation.SelectedValue = objStaffReq._Staffrequisitionbyid
                txtPosition.Text = objStaffReq._Noofposition
                dtpWorkStartDate.SetDate = objStaffReq._Workstartdate
            Else
                txtPosition.Text = "1"
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            If objStaffReq._Gradeunkid > 0 Then
                cboGrade.SelectedValue = objStaffReq._Gradeunkid
                Call cboGrade_SelectedIndexChanged(cboGrade, New System.EventArgs)
            End If
            If objStaffReq._Gradelevelunkid > 0 Then
                cboGradeLevel.SelectedValue = objStaffReq._Gradelevelunkid
            End If
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            cboJobReportTo.SelectedValue = objStaffReq._Job_report_tounkid
            cboJobAdvert.SelectedValue = objStaffReq._Jobadvertid
            'Sohail (29 Sep 2021) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani (18 Mar 2015) -- Start
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
    Private Sub CreateTable()
        Try
            mdtList = New DataTable

            mdtList.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtList.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            mdtList.Columns.Add("job_name", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (29 Sep 2021) -- End
            mdtList.Columns.Add("grade", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("gradelevel", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("actionreasonunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "CreateTable", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Dim objStaffEmp As New clsStaffrequisition_emp_tran
        Try

            mdtList.Rows.Clear()

            objStaffEmp._Staffrequisitiontranunkid = Session("staffrequisitiontranunkid")
            mdtOldTable = objStaffEmp._Datasource
            marrOldEmp.AddRange((From p In mdtOldTable Select (p.Item("employeeunkid").ToString)).ToArray)
            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Dim strOldEmpIDs As String = String.Join(",", (From p In mdtOldTable Select (p.Item("employeeunkid").ToString)).ToArray())
            'Hemant (06 Aug 2019) -- End

            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
            If mblnIsSelectedEmployeeListChanged = False Then
                marrSelectedEmployeeList.AddRange((From p In mdtOldTable Select (p.Item("employeeunkid").ToString)).ToArray)
            End If
            'Hemant (13 Sep 2019) -- End

            If CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.ADDITIONAL OrElse CInt(cboJob.SelectedValue) <= 0 OrElse CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.ADDITIONAL Then Exit Try

            If CInt(cboJob.SelectedValue) > 0 Then
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
                'If strFilter.Trim = "" Then
                '    'strFilter = " hremployee_master.jobunkid = " & CInt(cboJob.SelectedValue) & " "
                '    strFilter = " ERECAT.jobunkid = " & CInt(cboJob.SelectedValue) & " "
                'Else
                '    'strFilter &= " AND hremployee_master.jobunkid = " & CInt(cboJob.SelectedValue) & " "
                '    strFilter &= " AND ERECAT.jobunkid = " & CInt(cboJob.SelectedValue) & " "
                'End If
                strFilter &= " AND (ERECAT.jobunkid = " & CInt(cboJob.SelectedValue) & " OR PrevRecat.jobunkid = " & CInt(cboJob.SelectedValue) & " ) "
                'Sohail (29 Sep 2021) -- End
            End If

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(4)
            'Sohail (29 Sep 2021) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetList("Emp", True, False, , , , , , strFilter)
            dsList = objEmp.GetList(Session("Database_Name"), _
                                    Session("UserId"), _
                                    Session("Fin_year"), _
                                    Session("CompanyUnkId"), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                    Session("UserAccessModeSetting"), True, _
                                    True, "Emp", _
                                    Session("ShowFirstAppointmentDate"), , , strFilter, blnIncludePreviousJob:=True)
            'Sohail (29 Sep 2021) - [blnIncludePreviousJob]
            'Shani(24-Aug-2015) -- End

            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Dim strFilterRows As String = String.Empty

            If chkShowInActiveEmployee.Checked = True Then
                strFilterRows &= "  ISNULL(termination_from_date,'') <> " & "''"
            Else
                strFilterRows &= "  ISNULL(termination_from_date,'') = " & "''"
            End If

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            If CInt(cboJob.SelectedValue) > 0 AndAlso chkShowPreviousJobEmp.Checked = True Then
                strFilterRows &= " AND Prevjobunkid = " & CInt(cboJob.SelectedValue) & " "
            ElseIf CInt(cboJob.SelectedValue) > 0 AndAlso chkShowPreviousJobEmp.Checked = False Then
                strFilterRows &= " AND Jobunkid = " & CInt(cboJob.SelectedValue) & " "
            End If
            'Sohail (29 Sep 2021) -- End

            If strOldEmpIDs.Trim.Length > 0 Then
                If strFilterRows.Trim.Length > 0 Then
                    strFilterRows = "( " & strFilterRows & " ) " 'Sohail (29 Sep 2021)
                    strFilterRows &= "OR employeeunkid in ( " & strOldEmpIDs & " ) "
                End If
            End If
            'Hemant (06 Aug 2019) -- End


            Dim dRow As DataRow = Nothing
            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'For Each dtRow As DataRow In dsList.Tables("Emp").Rows
            For Each dtRow As DataRow In dsList.Tables("Emp").Select(strFilterRows)
                'Hemant (06 Aug 2019) -- End
                dRow = mdtList.NewRow

                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("name") = dtRow.Item("name").ToString
                dRow.Item("grade") = dtRow.Item("grade").ToString
                dRow.Item("gradelevel") = dtRow.Item("gradelevel").ToString
                dRow.Item("actionreasonunkid") = CInt(dtRow.Item("actionreasonunkid"))
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
                dRow.Item("job_name") = dtRow.Item("job_name").ToString
                'Sohail (29 Sep 2021) -- End

                'Hemant (13 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
                'If marrOldEmp.Contains(dtRow.Item("employeeunkid").ToString) = True Then
                If (marrSelectedEmployeeList.ToArray.Distinct).ToArray.Contains(dtRow.Item("employeeunkid").ToString) = True Then
                    'Hemant (13 Sep 2019) -- End
                    dRow.Item("IsChecked") = True
                Else
                    dRow.Item("IsChecked") = False
                End If

                mdtList.Rows.Add(dRow)

            Next


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            mdtView = mdtList.DefaultView
            dgvEmployeeList.DataSource = mdtView
            dgvEmployeeList.DataBind()

            objEmp = Nothing
        End Try
    End Sub

    'Shani (18 Mar 2015) -- End

    Private Function IsValidate() As Boolean
        Try
            'Language.setLanguage(mstrModuleName)
            If CInt(Session("StaffReqFormNoType")) = 0 AndAlso txtFormNo.Text.Trim = "" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry! Staff Requisition Form No. cannot be blank. Staff Requisition Form No. is mandatory information."), Me.Page)
                txtFormNo.Focus()
                Return False
            ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please select Requisition Type."), Me.Page)
                cboStatus.Focus()
                Return False
            ElseIf CInt(cboAllocation.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please select Requisition By."), Me.Page)
                cboAllocation.Focus()
                Return False
            ElseIf CInt(cboName.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select") & " " & lblName.Text, Me.Page)
                cboName.Focus()
                Return False

                'SHANI (18 Mar 2015)-START
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'ElseIf CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Please select Employee to be Replaced."), Me.Page)
                '    cboEmployee.Focus()
                '    Return False
                'ElseIf CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT AndAlso CInt(cboLeavingReason.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Please select Leaving Reason."), Me.Page)
                '    cboLeavingReason.Focus()
                '    Return False
            ElseIf (CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT OrElse CInt(cboStatus.SelectedValue) = 3) AndAlso mdtList.Select("IsChecked = 1 ").Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please select atleast one Employee to be Replaced."), Me)
                dgvEmployeeList.Focus()
                Return False
            ElseIf (CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT OrElse CInt(cboStatus.SelectedValue) = 3) AndAlso CInt(cboLeavingReason.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please select Leaving Reason."), Me)
                cboLeavingReason.Focus()
                Return False
                'SHANI (18 Mar 2015)--END
            ElseIf CInt(cboJob.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please select Job Title."), Me.Page)
                cboJob.Focus()
                Return False
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            ElseIf CInt(cboJobReportTo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Please select Job Reporting Line."), Me)
                cboJobReportTo.Focus()
                Return False
            ElseIf CInt(cboJobAdvert.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Please select Job Advert."), Me)
                cboJobAdvert.Focus()
                Return False
                'Sohail (29 Sep 2021) -- End
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Please select Grade."), Me)
                cboGrade.Focus()
                Return False

            ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Please select Grade Level."), Me)
                cboGradeLevel.Focus()
                Return False

            ElseIf CInt(cboEmploymentType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Please select Employment Type."), Me)
                cboEmploymentType.Focus()
                Return False
                'Sohail (12 Oct 2018) -- End

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-516 - They want below fields made mandatory on staff requisition form ( Reporting Line, Advertising internal only or internal and external in parallel, Number of positions, Contract period.
            ElseIf nudContractDuration.Enabled = True AndAlso CInt(nudContractDuration.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 41, "Sorry, No. of Contract Duration should be greater than Zero."), Me)
                nudContractDuration.Focus()
                Return False
                'Hemant (01 Nov 2021) -- End

            ElseIf dtpWorkStartDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, Date to Start Work should be greather than current date."), Me.Page)
                dtpWorkStartDate.Focus()
                Return False
                'Shani (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
            ElseIf CInt(txtPosition.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Sorry, No. of Position should be greater than Zero."), Me)
                txtPosition.Focus()
                Return False
                'Shani (18 Mar 2015) -- End
            End If

            'Sohail (16 Dec 2021) -- Start
            'Enhancement : : NMB - Allow to create staff requisition if class is not same.
            If Session("CompanyGroupName").ToString.ToUpper = "NMB PLC" Then
                If CInt(cboClassGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, "Please select Class Group."), enMsgBoxStyle.Information)
                    cboClassGroup.Focus()
                    Return False
                ElseIf CInt(cboClass.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 43, "Please select Class."), enMsgBoxStyle.Information)
                    cboClass.Focus()
                    Return False
                End If
            End If
            'Sohail (16 Dec 2021) -- End

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - System not to allow raising a staff requisition on a position if manpower planning on that position is not planned in 75.1.
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs).
            If CBool(Session("EnforceManpowerPlanning")) = True Then
                'Hemant (03 Sep 2019) -- End

                'Gajanan [16-NOV-2019] -- Start   
                Dim intCheckedCount As Integer = 0
                If mdtView IsNot Nothing Then
                    'Sohail (15 Oct 2021) -- Start
                    'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                    'intCheckedCount = mdtView.ToTable.Select("IsChecked = 1 ").Length
                    intCheckedCount = mdtView.Table.Select("IsChecked = 1 ").Length
                    'Sohail (15 Oct 2021) -- End

                End If



                'If (CInt(objlblVariation.Text) + mdtView.ToTable.Select("IsChecked = 1 ").Length) < CInt(txtPosition.Text) Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry, No. of position should not be greater than Manpower variations.") & " [" & (CInt(objlblVariation.Text) + mdtView.ToTable.Select("IsChecked = 1 ").Length) & "]. " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Please add vacancies on Job master screen."), Me)
                '    Return False
                'End If
                If (CInt(objlblVariation.Text) + intCheckedCount) < CInt(txtPosition.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry, No. of position should not be greater than Manpower variations.") & " [" & (CInt(objlblVariation.Text) + intCheckedCount) & "]. " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Please add vacancies on Job master screen."), Me)
                    Return False
                End If
                'Gajanan [16-NOV-2019] -- End 
            End If
            'Hemant (03 Sep 2019)

            Dim objStaffReq As New clsStaffrequisition_Tran
            If Session("staffrequisitiontranunkid") IsNot Nothing AndAlso CInt(Session("staffrequisitiontranunkid")) > 0 AndAlso objStaffReq.isUsed(CInt(Session("staffrequisitiontranunkid"))) = True Then
                Dim objStaffReqApp As New clsStaffrequisition_approval_Tran

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'Dim ds As DataSet = objStaffReqApp.GetList("List", CInt(Session("UserId")), , CInt(Session("staffrequisitiontranunkid")).ToString(), , , , , True, , True, CInt(enApprovalStatus.PENDING))
                Dim ds As DataSet = objStaffReqApp.GetList("List", CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), , CInt(Session("staffrequisitiontranunkid")).ToString(), , , , , True, , True, CInt(enApprovalStatus.PENDING))
                'Pinkal (16-Nov-2021)-- End

                If ds.Tables(0).Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, Staff Requisition approval process is in progress and it is not approved by previous level user yet."), Me)
                    Return False
                End If
            End If
            'Sohail (12 Oct 2018) -- End

            'If gobjEmailList.Count > 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait for some time."), Me.Page)
            '    Return False
            'End If

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    'Sohail (23 Sep 2021) -- Start
    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
    'Private Sub Send_Notification()
    '    Try
    '        If gobjEmailList.Count > 0 Then
    '            Dim objSendMail As New clsSendMail
    '            For Each obj In gobjEmailList
    '                objSendMail._ToEmail = obj._EmailTo
    '                objSendMail._Subject = obj._Subject
    '                objSendMail._Message = obj._Message
    '                objSendMail._Form_Name = obj._Form_Name
    '                objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
    '                objSendMail._OperationModeId = obj._OperationModeId
    '                objSendMail._UserUnkid = obj._UserUnkid
    '                objSendMail._SenderAddress = obj._SenderAddress
    '                objSendMail._ModuleRefId = obj._ModuleRefId
    '                Try
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'objSendMail.SendMail()
    '                    objSendMail.SendMail(CInt(Session("CompanyUnkId")))
    '                    'Sohail (30 Nov 2017) -- End
    '                Catch ex As Exception

    '                End Try
    '            Next
    '            gobjEmailList.Clear()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("Send_Notification:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '        If gobjEmailList.Count > 0 Then
    '            gobjEmailList.Clear()
    '        End If
    '    End Try
    'End Sub
    Private Sub Send_Notification(ByVal lstWebEmail As List(Of clsEmailCollection))
        Try
            If lstWebEmail.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In lstWebEmail
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                    Catch ex As Exception

                    End Try
                Next
                lstWebEmail.Clear()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If lstWebEmail.Count > 0 Then
                lstWebEmail.Clear()
            End If
        End Try
    End Sub
    'Sohail (23 Sep 2021) -- End


    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtStaffrequisitionDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtStaffrequisitionDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = -1
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Staff_Requisition
                dRow("scanattachrefid") = enScanAttactRefId.STAFF_REQUISITION

                dRow("transactionunkid") = If(IsNothing(Session("staffrequisitiontranunkid")), -1, CInt(Session("staffrequisitiontranunkid")))

                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
            Else

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtStaffrequisitionDocument.Rows.Add(dRow)
            Call FillQualificationAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillQualificationAttachment()
        Dim dtView As DataView
        Try
            'Gajanan [13-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            'If mdtStaffrequisitionDocument Is Nothing Then Exit Sub
            If mdtStaffrequisitionDocument Is Nothing Then
                dgvQualification.DataSource = Nothing
                dgvQualification.DataBind()
                Exit Sub
            End If
            'Gajanan [13-NOV-2019] -- End   

            dtView = New DataView(mdtStaffrequisitionDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvQualification.AutoGenerateColumns = False
            dgvQualification.DataSource = dtView
            dgvQualification.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub DeleteTempFile()
        Try
            If mdtStaffrequisitionDocument Is Nothing Then Exit Sub
            Dim xRow() As DataRow = mdtStaffrequisitionDocument.Select("localpath <> '' ")
            If xRow.Length > 0 Then
                For Each row As DataRow In xRow
                    If System.IO.File.Exists(row("localpath").ToString) Then
                        System.IO.File.Delete(row("localpath").ToString)
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [6-NOV-2019] -- End

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objStaffReq As New clsStaffrequisition_Tran
        Dim blnFlag As Boolean = False
        Try
            'Shani (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            mdtView = mdtList.DefaultView
            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee.
            'mdtView.RowFilter = mstrRowFilter
            mdtView.RowFilter = ""
            'Sohail (15 Oct 2021) -- End
            'Shani (18 Mar 2015) -- End
            If IsValidate() = False Then Exit Try

            Blank_ModuleName()

            StrModuleName2 = "mnuHumanResource"
            StrModuleName3 = "mnuRecruitmentItem"


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   


            'Gajanan [15-NOV-2019] -- Start   
            'Enhancement:Worked On Staff Requisition New Attachment Mandatory Option FOR NMB   
            'If CBool(Session("StaffRequisitionDocsAttachmentMandatory")) Then
            If (CBool(Session("AdditionalStaffRequisitionDocsAttachmentMandatory")) AndAlso CType(cboStatus.SelectedValue, enStaffRequisition_Status) = enStaffRequisition_Status.ADDITIONAL) OrElse _
               (CBool(Session("AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory")) AndAlso CType(cboStatus.SelectedValue, enStaffRequisition_Status) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL) OrElse _
               (CBool(Session("ReplacementStaffRequisitionDocsAttachmentMandatory")) AndAlso CType(cboStatus.SelectedValue, enStaffRequisition_Status) = enStaffRequisition_Status.REPLACEMENT) Then

                If mdtStaffrequisitionDocument Is Nothing OrElse mdtStaffrequisitionDocument.Select("AUD <> 'D'").Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
                    Exit Sub
                End If
            End If
            'Gajanan [15-NOV-2019] -- End

            'Gajanan [6-NOV-2019] -- End

            If Session("staffrequisitiontranunkid") IsNot Nothing Then
                objStaffReq._Staffrequisitiontranunkid = CInt(Session("staffrequisitiontranunkid"))
            End If
            objStaffReq._Formno = txtFormNo.Text.Trim
            objStaffReq._Staffrequisitiontypeid = CInt(cboStatus.SelectedValue)
            objStaffReq._Staffrequisitionbyid = CInt(cboAllocation.SelectedValue)
            objStaffReq._Allocationunkid = CInt(cboName.SelectedValue)
            objStaffReq._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
            objStaffReq._Classunkid = CInt(cboClass.SelectedValue)
            'objStaffReq._Employeeunkid = CInt(cboEmployee.SelectedValue) 'SHANI (18 Mar 2015) -- Start
            objStaffReq._Actionreasonunkid = CInt(cboLeavingReason.SelectedValue)
            objStaffReq._Additionalstaffreason = txtAddStaffReason.Text.Trim
            objStaffReq._Jobunkid = CInt(cboJob.SelectedValue)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'objStaffReq._Jobdescrription = txtJobDesc.Text.Trim
            Dim rtf As New System.Windows.Forms.RichTextBox
            rtf.Text = txtJobDesc.Text
            objStaffReq._Jobdescrription = rtf.Rtf.Trim
            'Sohail (12 Oct 2018) -- End
            objStaffReq._Noofposition = CInt(txtPosition.Text)
            objStaffReq._Workstartdate = dtpWorkStartDate.GetDate
            objStaffReq._Userunkid = Session("UserId")
            objStaffReq._WebIP = Session("IP_ADD")
            objStaffReq._WebHostName = Session("HOST_NAME")
            objStaffReq._WebFormName = "frmStaffRequisition_AddEdit"
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            objStaffReq._Gradeunkid = CInt(cboGrade.SelectedValue)
            objStaffReq._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            objStaffReq._Employmenttypeunkid = CInt(cboEmploymentType.SelectedValue)
            objStaffReq._Contract_Duration_Months = nudContractDuration.Text
            objStaffReq._Approved_Headcount = CInt(objlblPlanned.Text)
            objStaffReq._Actual_Headcount = CInt(objlblAvailable.Text)
            If Session("staffrequisitiontranunkid") Is Nothing OrElse CInt(Session("staffrequisitiontranunkid")) <= 0 Then
                objStaffReq._Requisition_Date = DateAndTime.Today.Date
            End If
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            objStaffReq._Job_report_tounkid = CInt(cboJobReportTo.SelectedValue)
            objStaffReq._Jobadvertid = CInt(cboJobAdvert.SelectedValue)
            'Sohail (29 Sep 2021) -- End
            objStaffReq._CompanyUnkid = Session("CompanyUnkId") 'Hemant (17 Jul 2019)
            'Shani (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            mdtNewTable = mdtOldTable.Clone

            Dim marrNewEmp As New ArrayList
            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'marrNewEmp.AddRange((From p In mdtView.ToTable.Select("IsChecked = 1 ").AsEnumerable Select (p.Item("employeeunkid").ToString)).ToArray)
            marrNewEmp.AddRange((From p In mdtView.Table.Select("IsChecked = 1 ").AsEnumerable Select (p.Item("employeeunkid").ToString)).ToArray)
            'Sohail (15 Oct 2021) -- End

            Dim add() As String = (From p In marrNewEmp Where (marrOldEmp.Contains(p.ToString) = False) Select (p.ToString)).ToArray
            Dim del() As String = (From p In marrOldEmp Where (marrNewEmp.Contains(p.ToString) = False) Select (p.ToString)).ToArray

            Dim dr_Row As List(Of DataRow) = Nothing
            Dim dRow As DataRow

            dr_Row = (From p In mdtOldTable Where (del.Contains(p.Item("employeeunkid").ToString) = True)).ToList

            If dr_Row IsNot Nothing Then
                For Each row As DataRow In dr_Row
                    dRow = mdtNewTable.NewRow

                    dRow.ItemArray = row.ItemArray
                    dRow.Item("AUD") = "D"

                    mdtNewTable.Rows.Add(dRow)
                Next
            End If

            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'Dim dr As List(Of DataRow) = (From p In mdtView.ToTable.Select("IsChecked = 1 ").AsEnumerable Where (add.Contains(p.Item("employeeunkid").ToString) = True)).ToList
            Dim dr As List(Of DataRow) = (From p In mdtView.Table.Select("IsChecked = 1 ").AsEnumerable Where (add.Contains(p.Item("employeeunkid").ToString) = True)).ToList
            'Sohail (15 Oct 2021) -- End

            If dr IsNot Nothing Then
                For Each dtRow In dr
                    dRow = mdtNewTable.NewRow

                    dRow.Item("IsChecked") = True
                    dRow.Item("staffrequisitionemptranunkid") = -1
                    If Session("staffrequisitiontranunkid") IsNot Nothing Then
                        dRow.Item("staffrequisitiontranunkid") = CInt(Session("staffrequisitiontranunkid"))
                    End If
                    dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                    dRow.Item("userunkid") = Session("UserId")
                    dRow.Item("AUD") = "A"

                    mdtNewTable.Rows.Add(dRow)
                Next
            End If
            'Shani (18 Mar 2015) -- End



            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRowAttachment As DataRow In mdtStaffrequisitionDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRowAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRowAttachment("AUD").ToString = "A" AndAlso dRowAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRowAttachment("localpath")))
                    If File.Exists(CStr(dRowAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRowAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRowAttachment("fileuniquename") = strFileName
                        dRowAttachment("filepath") = strPath
                        dRowAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                ElseIf dRowAttachment("AUD").ToString = "D" AndAlso dRowAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRowAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                End If

            Next
            'Gajanan [6-NOV-2019] -- End


            If Session("staffrequisitiontranunkid") IsNot Nothing Then
                'SHANI (18 Mar 2015)-START
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'blnFlag = objStaffReq.Update()

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objStaffReq.Update(, mdtNewTable)


                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                'blnFlag = objStaffReq.Update(ConfigParameter._Object._CurrentDateAndTime, Nothing, mdtNewTable)
                blnFlag = objStaffReq.Update(ConfigParameter._Object._CurrentDateAndTime, Nothing, mdtNewTable, mdtStaffrequisitionDocument)
                'Gajanan [6-NOV-2019] -- End

                'Shani(20-Nov-2015) -- End

                'SHANI (18 Mar 2015)--END
            Else
                'SHANI (18 Mar 2015)-START
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'blnFlag = objStaffReq.Insert()

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objStaffReq.Insert(mdtNewTable)

                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                'blnFlag = objStaffReq.Insert(ConfigParameter._Object._CurrentDateAndTime, Session("StaffReqFormNoType"), Session("StaffReqFormNoPrefix"), mdtNewTable)
                blnFlag = objStaffReq.Insert(ConfigParameter._Object._CurrentDateAndTime, Session("StaffReqFormNoType"), Session("StaffReqFormNoPrefix"), mdtNewTable, mdtStaffrequisitionDocument)
                'Gajanan [6-NOV-2019] -- End

                'Shani(20-Nov-2015) -- End

                'SHANI (18 Mar 2015)--END
            End If

            If blnFlag = False And objStaffReq._Message <> "" Then
                DisplayMessage.DisplayMessage(objStaffReq._Message, Me.Page)
            Else
                If Session("staffrequisitiontranunkid") Is Nothing Then
                    Dim objStaffReqApproval As New clsStaffrequisition_approval_Tran

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, Session("UserId"), CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), cboJob.SelectedItem.Text, 0, objStaffReq._Staffrequisitiontranunkid, txtJobDesc.Text.Trim, , enLogin_Mode.MGR_SELF_SERVICE, 0)
                    'Sohail (13 Jun 2016) -- Start
                    'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
                    'objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, _
                    '                                       Session("UserId"), _
                    '                                       CInt(cboAllocation.SelectedValue), _
                    '                                       CInt(cboName.SelectedValue), _
                    '                                       cboJob.SelectedItem.Text, 0, _
                    '                                       objStaffReq._Staffrequisitiontranunkid, _
                    '                                       txtJobDesc.Text.Trim, _
                    '                                       Session("CompanyUnkId"), _
                    '                                       Session("IsArutiDemo"), _
                    '                                       Session("ArutiSelfServiceURL"), _
                    '                                       enLogin_Mode.MGR_SELF_SERVICE, 0)
                    'Pinkal (16-Nov-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 

                    'objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, _
                    '                                       Session("UserId"), _
                    '                                       CInt(cboAllocation.SelectedValue), _
                    '                                       CInt(cboName.SelectedValue), _
                    '                                       cboJob.SelectedItem.Text, 0, _
                    '                                       objStaffReq._Staffrequisitiontranunkid, _
                    '                                       txtJobDesc.Text.Trim, _
                    '                                       Session("CompanyUnkId"), _
                    '                                       Session("IsArutiDemo"), _
                    '                                       Session("ArutiSelfServiceURL"), _
                    '                                      CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, _
                    '                                       CStr(Session("Database_Name")), enLogin_Mode.MGR_SELF_SERVICE, 0)


                    objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, _
                                                           Session("UserId"), _
                                                           CInt(cboAllocation.SelectedValue), _
                                                           CInt(cboName.SelectedValue), _
                                                           cboJob.SelectedItem.Text, 0, _
                                                           objStaffReq._Staffrequisitiontranunkid, _
                                                           txtJobDesc.Text.Trim, _
                                                           Session("CompanyUnkId"), _
                                                           Session("IsArutiDemo"), _
                                                           Session("ArutiSelfServiceURL"), _
                                                          CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, _
                                                        CStr(Session("Database_Name")), Session("EmployeeAsOnDate").ToString(), enLogin_Mode.MGR_SELF_SERVICE, 0)

                    'Pinkal (16-Nov-2021)-- End
                    'Sohail (11 Aug 2021) - [CStr(Session("Database_Name"))] - NMB Enhancement :  : The staff requisition requests are sent to all level 1 approvers (respective cheifs) despite them not having access to the respective function.
                    'Sohail (13 Jun 2016) -- End
                    'Shani(20-Nov-2015) -- End

                    'Sohail (23 Sep 2021) -- Start
                    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                    'Call Send_Notification()
                    Call Send_Notification(objStaffReqApproval._WebEmailList)
                    'Sohail (23 Sep 2021) -- End
                End If
            End If

            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'Session("staffrequisitiontranunkid") = Nothing
            ''Gajanan [13-NOV-2019] -- Start   
            ''Enhancement:Worked On NMB Bio Data Approval Query Optimization   

            'ViewState("mdtStaffrequisitionDocument") = Nothing
            'mdtStaffrequisitionDocument = Nothing
            ''Gajanan [13-NOV-2019] -- End
            'Sohail (15 Oct 2021) -- End

            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'If blnFlag = True Then Call GetValue()
            If blnFlag = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Staff Requisition Saved Successfully."), Me)

                'Sohail (15 Oct 2021) -- Start
                'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                Session("staffrequisitiontranunkid") = Nothing
                ViewState("mdtStaffrequisitionDocument") = Nothing
                mdtStaffrequisitionDocument = Nothing
                'Sohail (15 Oct 2021) -- End

                Call GetValue()
                'Gajanan [13-NOV-2019] -- Start   
                'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
                Call FillQualificationAttachment()
                'Gajanan [13-NOV-2019] -- End
            End If
            'Hemant (06 Aug 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objStaffReq = Nothing
        End Try
    End Sub
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'SHANI [01 FEB 2015]--END

        'Shani [ 24 DEC 2014 ] -- END
        Try
            Session("staffrequisitiontranunkid") = Nothing
            'Gajanan [13-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            ViewState("mdtStaffrequisitionDocument") = Nothing
            'Gajanan [13-NOV-2019] -- End

            Response.Redirect(Session("rootpath") & "Recruitment/Staff_Requisition/wPg_StaffRequisitionList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
    '    Dim frm As New frmAction_Reason_AddEdit
    '    Dim intRefId As Integer = -1
    '    Try
    '        If frm.displayDialog(intRefId, enAction.ADD_ONE, False) Then
    '            Dim dsList As New DataSet
    '            Dim objReason As New clsAction_Reason
    '            dsList = objReason.getComboList("Reason", True, False)
    '            With cboLeavingReason
    '                .ValueMember = "actionreasonunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsList.Tables("Reason")
    '                .SelectedValue = intRefId
    '            End With
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
    '    End Try
    'End Sub


    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If IsValidate() = False Then Exit Sub

            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                mdtStaffrequisitionDocument = CType(ViewState("mdtStaffrequisitionDocument"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillQualificationAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [6-NOV-2019] -- End


    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvQualification.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2007, "No Files to download."), Me)
                Exit Sub
            End If

            'Dim dtView As DataView = New DataView(mdtStaffrequisitionDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            If txtFormNo.Text.Trim.Length > 0 Then
                strMsg = DownloadAllDocument("file" & txtFormNo.Text.Replace(" ", "") + ".zip", mdtStaffrequisitionDocument, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            Else
                strMsg = DownloadAllDocument("StaffRequisition" + eZeeDate.convertDate(DateTime.Now) + ".zip", mdtStaffrequisitionDocument, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [6-NOV-2019] -- End

#End Region

#Region " Combobox's Events "

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""
        Try
            'Language.setLanguage(mstrModuleName)

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = "Department Group"
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Classes")

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .DataBind()
                End With
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged

        'SHANI (18 Mar 2015)-START
        'Enhancement - Allow more than one employee to be replaced in staff requisition.
        Dim chk As New CheckBox
        If dgvEmployeeList.Items.Count > 0 Then chk = CType(dgvEmployeeList.Controls(0).Controls(0).FindControl("chkAllSelect"), CheckBox)
        'SHANI (18 Mar 2015)--END
        Try
            If CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.ADDITIONAL Then

                'SHANI (18 Mar 2015)-START
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'cboEmployee.SelectedValue = 0
                'Call cboEmployee_SelectedIndexChanged(cboEmployee, New System.EventArgs)
                'cboEmployee.Enabled = False
                ''objbtnSearchEmp.Enabled = False
                'objtlpEmployee.Style("visibility") = "hidden"

                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                'nudPosition.Enabled = True
                txtPosition.Enabled = True
                'Sohail (12 Oct 2018) -- End
                dgvEmployeeList.Enabled = False
                If chk.Checked <> False Then chk.Checked = False
                chk.Enabled = False
                cboLeavingReason.SelectedValue = 0
                cboLeavingReason.Enabled = False
                'SHANI (18 Mar 2015)--END

                txtAddStaffReason.Enabled = True
                'objbtnAddReason.Enabled = True
            Else
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) - Reason for requesting staff. The reason section should be active regardless of the staff requisition type selected in 75.1.
                'txtAddStaffReason.Text = ""
                'txtAddStaffReason.Enabled = False
                txtAddStaffReason.Enabled = True
                'Sohail (12 Oct 2018) -- End
                'SHANI (18 Mar 2015)-START
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'cboEmployee.Enabled = True
                ''objtlpEmployee.Visible = True
                'objtlpEmployee.Style("visibility") = "block"
                ''objbtnSearchEmp.Enabled = True
                ''objbtnAddReason.Enabled = False

                If CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL Then
                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                    'nudPosition.Enabled = True
                    txtPosition.Enabled = True
                    'Sohail (12 Oct 2018) -- End
                Else
                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                    'nudPosition.Enabled = False
                    txtPosition.Enabled = False
                    'Sohail (12 Oct 2018) -- End
                End If
                dgvEmployeeList.Enabled = True
                chk.Enabled = True
                cboLeavingReason.Enabled = True
                'SHANI (18 Mar 2015)--END
            End If
            Call FillList() 'SHANI (18 Mar 2015)--Start
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'SHANI (18 Mar 2015)-START
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
    'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
    '    Try
    '        If CInt(cboEmployee.SelectedValue) > 0 Then
    '            Dim dr() As DataRow = mdtEmployee.Select("employeeunkid = " & CInt(cboEmployee.SelectedValue) & " ")
    '            If dr.Length > 0 Then
    '                objlblGrade.Text = dr(0).Item("grade").ToString()
    '                objlblSalaryBand.Text = dr(0).Item("gradelevel").ToString
    '                cboLeavingReason.SelectedValue = CInt(dr(0).Item("actionreasonunkid"))
    '            Else
    '                objlblGrade.Text = ""
    '                objlblSalaryBand.Text = ""
    '                cboLeavingReason.SelectedValue = 0
    '                cboLeavingReason.Enabled = False
    '            End If

    '            cboLeavingReason.Enabled = True
    '        Else
    '            objlblGrade.Text = ""
    '            objlblSalaryBand.Text = ""
    '            cboLeavingReason.SelectedValue = 0
    '            cboLeavingReason.Enabled = False
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'SHANI (18 Mar 2015)--END

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged, dtpWorkStartDate.TextChanged
        'Sohail (12 Oct 2018) - [dtpWorkStartDate.TextChanged]
        Dim objJob As New clsJobs
        Dim dsList As DataSet
        Try
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'objlblPlanned.Text = Format(0, "0")
            'objlblAvailable.Text = Format(0, "0")
            'objlblVariation.Text = Format(0, "0")
            If Session("staffrequisitiontranunkid") Is Nothing OrElse CInt(Session("staffrequisitiontranunkid")) <= 0 Then 'Add New Mode
                txtJobDesc.Text = ""
            End If
            cboGrade.Enabled = True
            'Sohail (12 Oct 2018) -- End
            If CInt(cboJob.SelectedValue) > 0 Then
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
                'dsList = objJob.getHeadCount("Count", , CInt(cboJob.SelectedValue))
                'Sohail (17 Apr 2020) -- Start
                'NMB Issue # : On Staff requisition screen on self service, the value of occupied head counts does not refresh after user selects the number of positions to request. Value only refreshes after selection of date to start work which is not okay.
                'dsList = objJob.getHeadCount(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), dtpWorkStartDate.GetDate(), dtpWorkStartDate.GetDate(), Session("UserAccessModeSetting").ToString, True, False, "Count", , CInt(cboJob.SelectedValue))
                dsList = objJob.getHeadCount(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), If(dtpWorkStartDate.IsNull = True, DateAndTime.Today, dtpWorkStartDate.GetDate()), If(dtpWorkStartDate.IsNull = True, DateAndTime.Today, dtpWorkStartDate.GetDate()), Session("UserAccessModeSetting").ToString, True, False, "Count", , CInt(cboJob.SelectedValue))
                'Sohail (17 Apr 2020) -- End
                'Sohail (12 Oct 2018) -- End
                If dsList.Tables("Count").Rows.Count > 0 Then
                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
                    'objlblPlanned.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("PLANNED")), "0")
                    'objlblAvailable.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("AVAILABLE")), "0")
                    'objlblVariation.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("VARIATION")), "0")
                    If Session("staffrequisitiontranunkid") Is Nothing OrElse CInt(Session("staffrequisitiontranunkid")) <= 0 Then 'Add New Mode
                        objlblPlanned.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("PLANNED")), "0")
                        objlblAvailable.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("AVAILABLE")), "0")
                        objlblVariation.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("VARIATION")), "0")
                        'Hemant (06 Aug 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                        If txtJobDesc.Text.Trim.Length <= 0 Then
                            'Hemant (06 Aug 2019) -- End
                            If dsList.Tables("Count").Rows(0).Item("desciription").ToString.StartsWith("{\rtf") = True Then
                                Dim rtf As New System.Windows.Forms.RichTextBox
                                rtf.Rtf = dsList.Tables("Count").Rows(0).Item("desciription").ToString()
                                txtJobDesc.Text = rtf.Text
                            Else
                                txtJobDesc.Text = dsList.Tables("Count").Rows(0).Item("desciription").ToString
                            End If
                        End If 'Hemant (06 Aug 2019)

                    End If
                    'Hemant (06 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    If CInt(cboGrade.SelectedValue) <= 0 Then
                        'Hemant (06 Aug 2019) -- End
                        cboGrade.SelectedValue = CInt(dsList.Tables("Count").Rows(0).Item("jobgradeunkid"))
                        Call cboGrade_SelectedIndexChanged(cboGrade, New System.EventArgs)
                    End If 'Hemant (06 Aug 2019)

                    If CInt(cboGrade.SelectedValue) > 0 Then
                        cboGrade.Enabled = False
                    End If
                    'Sohail (12 Oct 2018) -- End
                End If
            End If

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            Dim intDReportTo As Integer = 0
            If CInt(cboJob.SelectedValue) > 0 Then
                objJob._Jobunkid = CInt(cboJob.SelectedValue)
                intDReportTo = objJob._Report_Tounkid
            End If
            dsList = objJob.getComboList("Job", True)
            With cboJobReportTo
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = intDReportTo
            End With
            'Sohail (29 Sep 2021) -- End

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            Call FillList()
            'Sohail (18 Mar 2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Dim objClass As New clsClass
        Dim dsCombo As DataSet
        Try
            dsCombo = objClass.getComboList("Class", True, CInt(cboClassGroup.SelectedValue))
            With cboClass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Class")
                .DataBind()
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim objGradeLevel As New clsGradeLevel
        Dim dsList As DataSet
        Try
            dsList = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            With cboGradeLevel
                .DataValueField = "gradelevelunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("GradeLevel")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objGradeLevel = Nothing
        End Try
    End Sub
    'Sohail (12 Oct 2018) -- End

    'Hemant (15 Nov 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 1 : During staff requisition, when employment type is selected as Permanent. The text box on contact duration should be greyed out completely and should not allow user to increment the contact duration. It should be greyed out completely.)
    Private Sub cboEmploymentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmploymentType.SelectedIndexChanged
        Try
            If cboEmploymentType.SelectedItem.Text.ToString.ToUpper.Contains("PERMANENT") Then
                nudContractDuration.Text = "0"
                nudContractDuration.Enabled = False
                'Hemant (17 Sep 2020) -- Start
                'New UI Change
                'NumericUpDownExtender1.Enabled = False
                'Hemant (17 Sep 2020) -- End
            Else
                nudContractDuration.Enabled = True
                'Hemant (17 Sep 2020) -- Start
                'New UI Change
                'NumericUpDownExtender1.Enabled = True
                'Hemant (17 Sep 2020) -- End
            End If
        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "cboEmploymentType_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End
        End Try
    End Sub
    'Hemant (15 Nov 2019) -- End

#End Region

#Region " CheckBox's Events "
    'Hemant (06 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub chkShowInActiveEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployee.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (06 Aug 2019) -- End

    'Sohail (29 Sep 2021) -- Start
    'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
    Private Sub chkShowPreviousJobEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowPreviousJobEmp.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (29 Sep 2021) -- End

#End Region

    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
#Region "Gridview Event"
    Protected Sub dgvQualification_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvQualification.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(5).Text) > 0 Then
                    xrow = mdtStaffrequisitionDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(5).Text) & "")
                Else
                    xrow = mdtStaffrequisitionDocument.Select("GUID = '" & e.Item.Cells(4).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        Me.ViewState("DeleteAttRowIndex") = mdtStaffrequisitionDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2008, "File does not Exist..."), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(1).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvQualification_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvQualification.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                DirectCast(Master.FindControl("script1"), ScriptManager).RegisterPostBackControl(e.Item.Cells(3).FindControl("DownloadLink"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub
#End Region
    'Gajanan [6-NOV-2019] -- End


    'SHANI (18 Mar 2015)-START
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
#Region "Controls Event(S)"
    Public Sub chkAllSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            For Each dgItem As DataGridItem In dgvEmployeeList.Items
                Dim xrow As DataRow() = mdtList.Select("employeeunkid=" & CInt(dgItem.Cells(1).Text))
                CType(dgItem.FindControl("chkSelect"), CheckBox).Checked = chk.Checked
                xrow(0).Item("IsChecked") = chk.Checked
                xrow(0).EndEdit()
            Next
            mdtList.AcceptChanges()
            txtPosition.Text = mdtList.Select("IsChecked = 1 ").Length
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
            marrSelectedEmployeeList.Clear()
            marrSelectedEmployeeList.AddRange((From p In mdtList.Select("IsChecked = 1") Select (p.Item("employeeunkid").ToString)).ToArray)
            mblnIsSelectedEmployeeListChanged = True
            'Hemant (13 Sep 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "chkAllSelect", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub chkSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim xrow As DataGridItem = CType(chk.NamingContainer, DataGridItem)
            Dim dtRow As DataRow() = mdtList.Select("employeeunkid=" & CInt(xrow.Cells(1).Text))
            If dtRow.Length > 0 Then
                dtRow(0).Item("ischecked") = chk.Checked
            End If
            mdtList.AcceptChanges()
            txtPosition.Text = mdtList.Select("IsChecked = 1").Length

            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
            marrSelectedEmployeeList.Clear()
            marrSelectedEmployeeList.AddRange((From p In mdtList.Select("IsChecked = 1") Select (p.Item("employeeunkid").ToString)).ToArray)
            mblnIsSelectedEmployeeListChanged = True
            'Hemant (13 Sep 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "chkSelect", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtSearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            mstrRowFilter = ""

            If txtSearchEmp.Text.Trim.Length > 0 Then
                mstrRowFilter = "employeecode LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                "name LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                "grade LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                "gradelevel LIKE '%" & txtSearchEmp.Text & "%' "
            End If
            mdtView = mdtList.DefaultView
            mdtView.RowFilter = mstrRowFilter
            dgvEmployeeList.DataSource = mdtList
            dgvEmployeeList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (06 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Protected Sub lnkViewJobDescription_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewJobDescription.Click
        Try
            'Sohail (13 May 2020) -- Start
            'NMB Issue # : On staff requisition page, after filling data and previewing the job report, and clicking on the close button, the system is losing the prefilled data on the page.
            'Session("ReturnURL") = Request.Url.AbsoluteUri
            'Session("intJobunkid") = CInt(cboJob.SelectedValue) 'Hemant (22 Aug 2019)
            'Response.Redirect(Session("rootpath") & "Reports/Rpt_Employee_Job_Report.aspx", False)
            If CInt(cboJob.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "Please Select Job to view this report."), Me)
                Exit Sub
            End If

            Dim objJob As New clsJobReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            objJob.SetDefaultValue()

            objJob._JobId = CInt(cboJob.SelectedValue)
            objJob._JobName = cboJob.SelectedItem.Text

            objJob._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
            objJob._ReportId = CInt(enJob_Report_Template.Job_Listing_Report)

            objJob.generateReportNew(Session("Database_Name"), _
                                                   Session("UserId"), _
                                                   Session("Fin_year"), _
                                                   Session("CompanyUnkId"), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   Session("UserAccessModeSetting"), True, _
                                                   Session("ExportReportPath"), _
                                                   Session("OpenAfterExport"), _
                                                   0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objJob._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Sohail (13 May 2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (06 Aug 2019) -- End



    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtStaffrequisitionDocument.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtStaffrequisitionDocument.AcceptChanges()
                Call FillQualificationAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [6-NOV-2019] -- End

#End Region

    'SHANI (18 Mar 2015)--END 


    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocation.ID, Me.lblAllocation.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lblGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblGrade.ID, Me.lblGrade.Text)
            'Me.lblSalaryBand.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblSalaryBand.ID, Me.lblSalaryBand.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClass.ID, Me.lblClass.Text)
            Me.lblClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClassGroup.ID, Me.lblClassGroup.Text)
            Me.lblJobDesc.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJobDesc.ID, Me.lblJobDesc.Text).Replace("&&", "&")
            Me.lblPlanned.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPlanned.ID, Me.lblPlanned.Text)
            Me.lblVariation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVariation.ID, Me.lblVariation.Text)
            Me.lblAvailable.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAvailable.ID, Me.lblAvailable.Text)
            Me.lblWorkStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblWorkStartDate.ID, Me.lblWorkStartDate.Text)
            Me.lblLeavingReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeavingReason.ID, Me.lblLeavingReason.Text)
            Me.lblAddStaffReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAddStaffReason.ID, Me.lblAddStaffReason.Text)
            Me.lblFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFormNo.ID, Me.lblFormNo.Text)
            Me.lblNoofPosition.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNoofPosition.ID, Me.lblNoofPosition.Text)
            Me.lblJobReportTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJobReportTo.ID, Me.lblJobReportTo.Text)
            Me.lblJobAdvert.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJobAdvert.ID, Me.lblJobAdvert.Text)
        'Shani (18 Mar 2015) -- Start
        'Enhancement - Allow more than one employee to be replaced in staff requisition.
            Me.dgvEmployeeList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmployeeList.Columns(2).FooterText, Me.dgvEmployeeList.Columns(2).HeaderText)
            Me.dgvEmployeeList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmployeeList.Columns(3).FooterText, Me.dgvEmployeeList.Columns(3).HeaderText)
            Me.dgvEmployeeList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmployeeList.Columns(4).FooterText, Me.dgvEmployeeList.Columns(4).HeaderText)
            Me.dgvEmployeeList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmployeeList.Columns(5).FooterText, Me.dgvEmployeeList.Columns(5).HeaderText)
            Me.dgvEmployeeList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmployeeList.Columns(6).FooterText, Me.dgvEmployeeList.Columns(6).HeaderText)
        'Shani (18 Mar 2015) -- End
        'Hemant (06 Aug 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Me.lnkViewJobDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkViewJobDescription.ID, Me.lnkViewJobDescription.Text)
            Me.chkShowInActiveEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowInActiveEmployee.ID, Me.chkShowInActiveEmployee.Text)
        'Hemant (06 Aug 2019) -- End

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry! Staff Requisition Form No. cannot be blank. Staff Requisition Form No. is mandatory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Please select Requisition Type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Please select Requisition By.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Please select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Please select atleast one Employee to be Replaced.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Please select Leaving Reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Please select Job Title.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, Date to Start Work should be greather than current date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait for some time.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Branch")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Department")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Section Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Section")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Unit Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Unit")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Team")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Job Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Jobs")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Class Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "Classes")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Sorry, No. of Position should be greater than Zero.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Please select Grade.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Please select Grade Level.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Please select Employment Type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Sorry, No. of position should not be greater than Manpower variations.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Please add vacancies on Job master screen.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Sorry, Staff Requisition approval process is in progress and it is not approved by previous level user yet.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Staff Requisition Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 32, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 38, "Please Select Job to view this report.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Please select Job Reporting Line.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "Please select Job Advert.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 41, "Sorry, No. of Contract Duration should be greater than Zero.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 42, "Please select Class Group.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 43, "Please select Class.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
