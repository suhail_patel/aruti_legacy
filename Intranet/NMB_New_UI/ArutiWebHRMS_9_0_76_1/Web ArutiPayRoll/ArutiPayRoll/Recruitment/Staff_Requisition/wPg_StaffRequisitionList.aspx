﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_StaffRequisitionList.aspx.vb"
    Inherits="wPg_StaffRequisitionList" MasterPageFile="~/Home1.master" Title="Staff Requisition List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">

  function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }
       
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc4:Confirmation ID="popupYesNo" runat="server" Title="Confirmation" Message="Are you sure you want to delete this Staff Requisition?" />
                <uc5:DeleteReason ID="popupDelReason" runat="server" Title="Void Reason" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAllocation" runat="server" Text="Requisition By" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAllocation" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboName" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkMyApprovals" runat="server" Text="My Approvals" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 235px;">
                                            <asp:GridView ID="dgStaffReqList" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="staffrequisitiontranunkid"
                                                 CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="btnView" runat="server" CausesValidation="False" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Change" ToolTip="Edit">
                                                                     <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="btnGvRemove" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Remove" ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField >
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkViewStaffRequisitionFormReport" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="ViewStaffRequisitionFormReport" ToolTip="View Staff Requisition Form Report">
                                                                    <i class="fas fa-list-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="staffrequisitionby" HeaderText="Staff Req. By" FooterText="colhAllocation" />
                                                    <asp:BoundField DataField="formno" HeaderText="Form No" FooterText="colhFormNo" />
                                                    <asp:BoundField DataField="name" HeaderText="Allocation" FooterText="colhName" />
                                                    <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="colhJob" />
                                                    <asp:BoundField DataField="jobdescrription" HeaderText="Job Description" FooterText="colhJobDesc" />
                                                    <asp:BoundField DataField="workstartdate" HeaderText="Work Start Date" FooterText="colhWorkStart" />
                                                    <asp:BoundField DataField="form_status" HeaderText="Status" FooterText="colhStatus" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
