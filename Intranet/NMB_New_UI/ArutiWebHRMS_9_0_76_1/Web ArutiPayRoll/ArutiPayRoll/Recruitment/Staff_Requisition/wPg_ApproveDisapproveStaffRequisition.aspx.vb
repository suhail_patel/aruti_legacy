﻿

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class Recruitment_Staff_Requisition_wPg_ApproveDisapproveStaffRequisition
    Inherits Basepage


#Region " Private Variable(s) "

    Dim msg As New CommonCodes
    Private clsuser As New User
    Private objCONN As SqlConnection
    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveStaffRequisition"
    Private ReadOnly mstrModuleName1 As String = "frmApproveDisapproveStaffRequisitionList"

    Private objStaffReqApproval As clsStaffrequisition_approval_Tran
    Private objStaffReq As clsStaffrequisition_Tran

    Private mintCurrApprovalStatus As Integer

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

    Private mdicAllocation As New Dictionary(Of Integer, String)
    Private mdicStatus As New Dictionary(Of Integer, String)

    Private mstrAdvanceFilter As String = ""
    Private mdtList As DataTable = Nothing 'Sohail (12 Oct 2018)
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objApproverMap As New clsStaffRequisition_approver_mapping
        Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing


        Try

            mintCurrApprovalStatus = CInt(Session("statusunkid"))

            dsCombo = objMaster.GetEAllocation_Notification("List")
            mdicAllocation = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.GetListForStaffRequisitionStatus(False, "Status")
            mdicStatus = (From p In dsCombo.Tables("Status").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            If mintCurrApprovalStatus = enApprovalStatus.PENDING Then
                dsCombo = objMaster.getApprovalStatus("ApprovalStatus", False, , , True, True)
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus")).ToTable
            ElseIf mintCurrApprovalStatus = enApprovalStatus.APPROVED Then
                dsCombo = objMaster.getApprovalStatus("ApprovalStatus", False, , , , , True)
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus")).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus"), "ID <> " & mintCurrApprovalStatus & " ", "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            objStaffReq._Staffrequisitiontranunkid = CInt(Session("staffrequisitiontranunkid"))
            dsCombo = objApproverMap.GetList("ApproverLevel", CInt(Session("UserId")), objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))

                'mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'mintMinPriority = objApproverLevel.GetMinPriority(objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'mintMaxPriority = objApproverLevel.GetMaxPriority(objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)

                mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, Session("EmployeeAsOnDate").ToString(), objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                mintMinPriority = objApproverLevel.GetMinPriority(Session("EmployeeAsOnDate").ToString(), objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                mintMaxPriority = objApproverLevel.GetMaxPriority(Session("EmployeeAsOnDate").ToString(), objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority, Session("EmployeeAsOnDate").ToString(), objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)

                'Pinkal (16-Nov-2021)-- End


                If mintCurrLevelPriority >= 0 Then
                    mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
                    objApproverName.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver").ToString
                    objApproverLevelVal.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver_level").ToString
                    objLevelPriorityVal.Text = mintCurrLevelPriority.ToString
                End If
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillInfo()
        Dim objJob As New clsJobs
        Dim dsList As DataSet
        Dim decTotAmt As Decimal = 0
        Dim mintBankPayment As Integer = 0
        Dim mintCashPayment As Integer = 0
        Dim strFilter As String = ""
        'Sohail (12 Oct 2018) -- Start
        'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
        'Dim mdtList As New DataTable 'SHANI (18 Mar 2015)
        mdtList = New DataTable
        'Sohail (12 Oct 2018) -- End
        Try
            'SHANI (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            mdtList.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtList.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("grade", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("gradelevel", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("actionreasonunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'SHANI (18 Mar 2015) -- End

            dsList = objStaffReq.GetList("List", "rcstaffrequisition_tran.staffrequisitiontranunkid = " & CInt(Session("staffrequisitiontranunkid")) & " ")
            If dsList.Tables("List").Rows.Count > 0 Then

                With dsList.Tables("List").Rows(0)
                    txtFormNo.Text = .Item("formno").ToString
                    txtStatus.Text = mdicStatus.Item(CInt(.Item("staffrequisitiontypeid")))
                    Me.ViewState.Add("staffrequisitiontypeid", CInt(.Item("staffrequisitiontypeid")))

                    txtAllocation.Text = mdicAllocation.Item(CInt(.Item("staffrequisitionbyid")))
                    Me.ViewState.Add("staffrequisitionbyid", CInt(.Item("staffrequisitionbyid")))

                    Select Case CInt(.Item("staffrequisitionbyid"))

                        Case enAllocation.BRANCH
                            txtName.Text = .Item("Branch").ToString

                        Case enAllocation.DEPARTMENT_GROUP
                            txtName.Text = .Item("DepartmentGroup").ToString

                        Case enAllocation.DEPARTMENT
                            txtName.Text = .Item("Department").ToString

                        Case enAllocation.SECTION_GROUP
                            txtName.Text = .Item("SectionGroup").ToString

                        Case enAllocation.SECTION
                            txtName.Text = .Item("Section").ToString

                        Case enAllocation.UNIT_GROUP
                            txtName.Text = .Item("UnitGroup").ToString

                        Case enAllocation.UNIT
                            txtName.Text = .Item("Unit").ToString

                        Case enAllocation.TEAM
                            txtName.Text = .Item("Team").ToString

                        Case enAllocation.JOB_GROUP
                            txtName.Text = .Item("JobGroup").ToString

                        Case enAllocation.JOBS
                            txtName.Text = .Item("Job").ToString

                        Case enAllocation.CLASS_GROUP
                            txtName.Text = .Item("ClassGroup").ToString

                        Case enAllocation.CLASSES
                            txtName.Text = .Item("Class").ToString

                    End Select

                    Me.ViewState.Add("allocationunkid", CInt(.Item("allocationunkid")))

                    txtClassGroup.Text = .Item("Staff_ClassGroup").ToString
                    Me.ViewState.Add("classgroupunkid", CInt(.Item("classgroupunkid")))

                    txtClass.Text = .Item("Staff_Class").ToString
                    Me.ViewState.Add("classunkid", CInt(.Item("classunkid")))

                    'SHANI (18 Mar 2015)-START
                    'Enhancement - Allow more than one employee to be replaced in staff requisition.
                    'txtEmployee.Text = .Item("employeename").ToString
                    'Me.ViewState.Add("employeeunkid", CInt(.Item("employeeunkid")))
                    'SHANI (18 Mar 2015)--END

                    txtLeavingReason.Text = .Item("actionreason").ToString
                    Me.ViewState.Add("actionreasonunkid", CInt(.Item("actionreasonunkid")))

                    txtAddStaffReason.Text = .Item("additionalstaffreason").ToString

                    txtJob.Text = .Item("JobTitle").ToString
                    Me.ViewState.Add("jobunkid", CInt(.Item("jobunkid")))

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                    'txtJobDesc.Text = .Item("jobdescrription").ToString
                    If .Item("jobdescrription").ToString.StartsWith("{\rtf") = True Then
                        Dim rtf As New System.Windows.Forms.RichTextBox
                        rtf.Rtf = .Item("jobdescrription").ToString
                        txtJobDesc.Text = rtf.Text
                    Else
                        txtJobDesc.Text = .Item("jobdescrription").ToString
                    End If
                    'Sohail (12 Oct 2018) -- End
                    txtNoofPosition.Text = .Item("noofposition").ToString
                    txtWorkStartDate.Text = eZeeDate.convertDate(.Item("workstartdate").ToString).ToShortDateString
                End With
                'Shani (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                mdtList.Rows.Clear()

                Dim objStaffEmp As New clsStaffrequisition_emp_tran
                Dim objEmp As New clsEmployee_Master
                objStaffEmp._Staffrequisitiontranunkid = CInt(Session("staffrequisitiontranunkid"))
                Dim mdtTable As DataTable = objStaffEmp._Datasource
                Dim strEmpIDs As String = String.Join(",", (From p In mdtTable Select (p.Item("employeeunkid").ToString)).ToArray)
                If strEmpIDs.Trim <> "" Then
                    'Sohail (01 Nov 2017) -- Start
                    'Issue - 70.1 - Anbiguous column name employeeunkid.
                    'strFilter = "employeeunkid IN (" & strEmpIDs & ") "
                    strFilter = " hremployee_master.employeeunkid IN (" & strEmpIDs & ") "
                    'Sohail (01 Nov 2017) -- End

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim dsEmp As DataSet = objEmp.GetList("Emp", True, False, , , , , , strFilter)
                    Dim dsEmp As DataSet = objEmp.GetList(Session("Database_Name"), _
                                                          Session("UserId"), _
                                                          Session("Fin_year"), _
                                                          Session("CompanyUnkId"), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          Session("UserAccessModeSetting"), True, _
                                                          True, "Emp", _
                                                          Session("ShowFirstAppointmentDate"), , , strFilter)
                    'Shani(24-Aug-2015) -- End
                    Dim result = (From s In mdtTable.AsEnumerable Join e In dsEmp.Tables("Emp").AsEnumerable On s.Item("employeeunkid").ToString Equals e.Item("employeeunkid").ToString Select New With _
                                  {.employeeunkid = CInt(s.Item("employeeunkid")) _
                                   , .employeecode = e.Item("employeecode").ToString _
                                   , .name = e.Item("name").ToString _
                                   , .grade = e.Item("grade").ToString _
                                   , .gradelevel = e.Item("gradelevel").ToString _
                                   , .actionreasonunkid = CInt(e.Item("actionreasonunkid")) _
                                   }).ToList.OrderBy(Function(x) x.name)

                    Dim dRow As DataRow = Nothing
                    For Each item In result
                        dRow = mdtList.NewRow

                        dRow.Item("IsChecked") = False
                        dRow.Item("employeeunkid") = item.employeeunkid
                        dRow.Item("employeecode") = item.employeecode
                        dRow.Item("name") = item.name
                        dRow.Item("grade") = item.grade
                        dRow.Item("gradelevel") = item.gradelevel
                        dRow.Item("actionreasonunkid") = item.actionreasonunkid
                        dRow.Item("AUD") = ""

                        mdtList.Rows.Add(dRow)
                    Next
                End If
                'Shani (18 Mar 2015) -- End
            Else
                Me.ViewState.Add("jobunkid", 0)
            End If


            objlblPlanned.Text = Format(0, "0")
            objlblAvailable.Text = Format(0, "0")
            objlblVariation.Text = Format(0, "0")
            If CInt(Me.ViewState("jobunkid")) > 0 Then
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
                'dsList = objJob.getHeadCount("Count", , CInt(Me.ViewState("jobunkid")))
                'dsList = objJob.getHeadCount("Count", , CInt(Me.ViewState("jobunkid")))
                dsList = objJob.getHeadCount(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), txtWorkStartDate.Text, txtWorkStartDate.Text, Session("UserAccessModeSetting").ToString, True, False, "Count", , CInt(Me.ViewState("jobunkid")))
                'Sohail (12 Oct 2018) -- End
                If dsList.Tables("Count").Rows.Count > 0 Then
                    objlblPlanned.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("PLANNED")), "0")
                    objlblAvailable.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("AVAILABLE")), "0")
                    objlblVariation.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("VARIATION")), "0")
                End If
            End If

            'Gajanan [21-April-2020] -- Start   
            Dim objDocument As New clsScan_Attach_Documents
            Dim mdtAttacgment As DataTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.STAFF_REQUISITION, CInt(Me.Session("staffrequisitiontranunkid")), CStr(Session("Document_Path"))).Copy
            If mdtAttacgment Is Nothing Then
                lblEmptyAttachment.Visible = True
            Else
                lblEmptyAttachment.Visible = False
                dgvstaffRequisitionAttachment.AutoGenerateColumns = False
                dgvstaffRequisitionAttachment.DataSource = mdtAttacgment
                dgvstaffRequisitionAttachment.DataBind()
            End If
            pnl_staffRequisitionAttachment.Visible = Not lblEmptyAttachment.Visible
            'Gajanan [21-April-2020] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("FillInfo :- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Shani 18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            dgvEmployeeList.DataSource = mdtList
            dgvEmployeeList.DataBind()
            'Shani 18 Mar 2015) -- End
            objJob = Nothing
        End Try
    End Sub

    Private Function IsValidation() As Boolean
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please select Approval Status."), Me)
                cboStatus.Focus()
                Return False

            ElseIf CInt(Me.ViewState("CurrLevelPriority")) < 0 Then
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry! You can not not Approve / Disapprove Payment. Please contact Administrator."), Me)
                Return False
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - System not to allow raising a staff requisition on a position if manpower planning on that position is not planned in 75.1.
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No. 6 : During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs).
            If CBool(Session("EnforceManpowerPlanning")) = True Then
                'Hemant (03 Sep 2019) -- End
                If (CInt(objlblVariation.Text) + mdtList.Rows.Count) < CInt(txtNoofPosition.Text) Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, No. of position should not be greater than Manpower variations.") & " [" & (CInt(objlblVariation.Text) + mdtList.Rows.Count) & "]. " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Please add vacancies on Job master screen."), Me)
                    Return False
                End If
            End If  'Hemant (03 Sep 2019)

            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            'Dim ds As DataSet = objStaffReqApproval.GetList("List", CInt(Session("UserId")), , CInt(Session("staffrequisitiontranunkid")).ToString(), , , , , True, "", True, CInt(enApprovalStatus.PENDING))
            Dim ds As DataSet = objStaffReqApproval.GetList("List", CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), , CInt(Session("staffrequisitiontranunkid")).ToString(), , , , , True, "", True, CInt(enApprovalStatus.PENDING))
            'Pinkal (16-Nov-2021)-- End


            If ds.Tables("List").Rows.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level."), Me)
                Exit Function
            End If
            'Sohail (12 Oct 2018) -- End

            'If gobjEmailList.Count > 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sending Email(s) process is in progress from other module. Please wait for some time."), Me)
            '    Return False
            'End If

            Return True

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetValue()
        Try
            objStaffReqApproval._Staffrequisitiontranunkid = CInt(Session("staffrequisitiontranunkid"))
            objStaffReqApproval._Levelunkid = CInt(Me.ViewState("CurrLevelID"))
            objStaffReqApproval._Priority = CInt(Me.ViewState("CurrLevelPriority"))
            objStaffReqApproval._Approval_Date = ConfigParameter._Object._CurrentDateAndTime
            objStaffReqApproval._Statusunkid = CInt(cboStatus.SelectedValue)
            objStaffReqApproval._Remarks = txtRemarks.Text.Trim
            objStaffReqApproval._Userunkid = CInt(Session("UserId"))
            objStaffReqApproval._Isvoid = False
            objStaffReqApproval._Voiduserunkid = -1
            objStaffReqApproval._Voiddatetime = Nothing
            objStaffReqApproval._Voidreason = ""

            objStaffReqApproval._WebFormName = "frmApproveDisapproveStaffRequisition"
            StrModuleName2 = "mnuHumanResource"
            StrModuleName3 = "mnuRecruitmentItem"
            objStaffReqApproval._WebIP = Session("IP_ADD")
            objStaffReqApproval._WebHostName = Session("HOST_NAME")

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetValue", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SaveData()
        Dim intIsApproved As Integer = 0
        Try
            Call SetValue()

            'Sohail (02 Dec 2021) -- Start
            'Issue : OLD-529 : WARMA Zambia - Approved Staff requisition Still appears with a Pending Status.
            'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
            If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
                'Sohail (02 Dec 2021) -- End
                intIsApproved = enApprovalStatus.APPROVED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                intIsApproved = enApprovalStatus.REJECTED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.CANCELLED Then
                intIsApproved = enApprovalStatus.CANCELLED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.PUBLISHED Then
                intIsApproved = enApprovalStatus.PUBLISHED
            End If

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only ).
            If txtRemarks.Text.Trim = "" Then
                objStaffReqApproval._Remarks = objStaffReqApproval._Disapprovalreason
            End If
            'Hemant (03 Sep 2019) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim blnResult As Boolean = objStaffReqApproval.UpdatestaffrequisitionApproval(CInt(Session("staffrequisitionapprovaltranunkid")), intIsApproved)
            Dim blnResult As Boolean = objStaffReqApproval.UpdatestaffrequisitionApproval(CInt(Session("staffrequisitionapprovaltranunkid")), ConfigParameter._Object._CurrentDateAndTime, intIsApproved)
            'Shani(20-Nov-2015) -- End


            If blnResult = False And objStaffReqApproval._Message <> "" Then
                msg.DisplayMessage(objStaffReqApproval._Message, Me)
            Else

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")), txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), txtRemarks.Text.Trim, , enLogin_Mode.MGR_SELF_SERVICE, 0)
                'Sohail (13 Jun 2016) -- Start
                'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")), txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), txtRemarks.Text.Trim, Session("CompanyUnkId"), Session("IsArutiDemo"), Session("ArutiSelfServiceURL"), enLogin_Mode.MGR_SELF_SERVICE, 0)
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")), txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), txtRemarks.Text.Trim, Session("CompanyUnkId"), Session("IsArutiDemo"), Session("ArutiSelfServiceURL"), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, enLogin_Mode.MGR_SELF_SERVICE, 0)
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only ).
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")), txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), txtRemarks.Text.Trim, Session("CompanyUnkId"), Session("IsArutiDemo"), Session("ArutiSelfServiceURL"), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, enLogin_Mode.MGR_SELF_SERVICE, 0, Session("StaffRequisitionFinalApprovedNotificationUserIds").ToString())
                'Sohail (11 Aug 2021) -- Start
                'NMB Enhancement :  : The staff requisition requests are sent to all level 1 approvers (respective cheifs) despite them not having access to the respective function.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")), txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), objStaffReqApproval._Remarks, Session("CompanyUnkId"), Session("IsArutiDemo"), Session("ArutiSelfServiceURL"), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, enLogin_Mode.MGR_SELF_SERVICE, 0, Session("StaffRequisitionFinalApprovedNotificationUserIds").ToString())

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")), txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), objStaffReqApproval._Remarks, Session("CompanyUnkId"), Session("IsArutiDemo"), Session("ArutiSelfServiceURL"), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, CStr(Session("Database_Name")), enLogin_Mode.MGR_SELF_SERVICE, 0, Session("StaffRequisitionFinalApprovedNotificationUserIds").ToString())
                objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), CInt(Session("UserId")), CInt(Me.ViewState("staffrequisitionbyid")), CInt(Me.ViewState("allocationunkid")) _
                                                                           , txtJob.Text, CInt(Session("staffrequisitionapprovaltranunkid")), CInt(Session("staffrequisitiontranunkid")), objStaffReqApproval._Remarks _
                                                                           , Session("CompanyUnkId"), Session("IsArutiDemo"), Session("ArutiSelfServiceURL"), CInt(Session("Fin_year")) _
                                                                           , Session("UserAccessModeSetting").ToString, CStr(Session("Database_Name")), Session("EmployeeAsOnDate").ToString(), enLogin_Mode.MGR_SELF_SERVICE _
                                                                           , 0, Session("StaffRequisitionFinalApprovedNotificationUserIds").ToString())

                'Pinkal (16-Nov-2021)-- End

                'Sohail (11 Aug 2021) -- End
                'Hemant (03 Sep 2019) -- End
                'Sohail (12 Oct 2018) -- End
                'Sohail (13 Jun 2016) -- End
                'Shani(20-Nov-2015) -- End

                'Sohail (23 Sep 2021) -- Start
                'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
                'Send_Notification()
                Send_Notification(objStaffReqApproval._WebEmailList)
                'Sohail (23 Sep 2021) -- End

                If Request.QueryString.Count > 0 Then
                    'Hemant (15 Nov 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), True, Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    'Session.Abandon()
                    'Hemant (15 Nov 2019) -- End
                    Response.Redirect("../../Index.aspx", False)
                Else
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Staff Requisition Approval saved successfully."), Me)
                    cboStatus.SelectedIndex = 0
                    txtRemarks.Text = ""
                    Session("staffrequisitionapprovaltranunkid") = Nothing
                    Session("staffrequisitiontranunkid") = Nothing
                    Response.Redirect("~/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisitionList.aspx", False)
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("SaveData :- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (23 Sep 2021) -- Start
    'NMB Issue :  : Email sending in progress message keep popping up on staff requesition screen when editing staff requisition.
    'Private Sub Send_Notification()
    '    Try
    '        If gobjEmailList.Count > 0 Then
    '            Dim objSendMail As New clsSendMail
    '            For Each obj In gobjEmailList
    '                objSendMail._ToEmail = obj._EmailTo
    '                objSendMail._Subject = obj._Subject
    '                objSendMail._Message = obj._Message
    '                objSendMail._Form_Name = obj._Form_Name
    '                objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
    '                objSendMail._OperationModeId = obj._OperationModeId
    '                objSendMail._UserUnkid = obj._UserUnkid
    '                objSendMail._SenderAddress = obj._SenderAddress
    '                objSendMail._ModuleRefId = obj._ModuleRefId
    '                Try
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'objSendMail.SendMail()
    '                    objSendMail.SendMail(CInt(Session("CompanyUnkId")))
    '                    'Sohail (30 Nov 2017) -- End
    '                Catch ex As Exception

    '                End Try
    '            Next
    '            gobjEmailList.Clear()
    '        End If
    '    Catch ex As Exception
    '        'Hemant (13 Aug 2020) -- Start
    '        'Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
    '        msg.DisplayError(ex, Me)
    '        'Hemant (13 Aug 2020) -- End
    '    Finally
    '        If gobjEmailList.Count > 0 Then
    '            gobjEmailList.Clear()
    '        End If
    '    End Try
    'End Sub
    Private Sub Send_Notification(ByVal lstWebEmail As List(Of clsEmailCollection))
        Try
            If lstWebEmail.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In lstWebEmail
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                    Catch ex As Exception

                    End Try
                Next
                lstWebEmail.Clear()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            If lstWebEmail.Count > 0 Then
                lstWebEmail.Clear()
            End If
        End Try
    End Sub
    'Sohail (23 Sep 2021) -- End

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            objStaffReq = New clsStaffrequisition_Tran
            objStaffReqApproval = New clsStaffrequisition_approval_Tran

            'SetLanguage()

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")

                    If arr.Length = 5 Then
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))
                        Session("staffrequisitionapprovaltranunkid") = CInt(arr(2))
                        Session("staffrequisitiontranunkid") = CInt(arr(3))
                        Session("statusunkid") = CInt(arr(4))

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objCommon As New CommonCodes
                        'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)


                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False) Then
                            msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If

                        If ConfigParameter._Object._IsExpire Then
                            msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If

                        Dim clsConfig As New clsConfigOptions
                        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))

                        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                        End If


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try

                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        'Sohail (21 Mar 2015) -- Start
                        'Enhancement - New UI Notification Link Changes.
                        'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                        'Sohail (21 Mar 2015) -- End
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        'gobjLocalization._LangId = clsuser.LanguageUnkid
                        'gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        'gobjLocalization._LangId = clsuser.LanguageUnkid
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End


                        Call SetLanguage()

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objUserPrivilege As New clsUserPrivilege
                        'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                        'Sohail (30 Mar 2015) -- End

                        objStaffReq._Staffrequisitiontranunkid = CInt(Session("staffrequisitiontranunkid"))
                        Dim objApproverMap As New clsStaffRequisition_approver_mapping
                        Dim dsApproverMapping As DataSet = objApproverMap.GetList("List", CInt(Session("UserId")), objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid, "", "")

                        If dsApproverMapping IsNot Nothing AndAlso dsApproverMapping.Tables(0).Rows.Count > 0 Then


                            'Anjan [08 September 2015] -- Start
                            'ENHANCEMENT : wrong parameter order was set.
                            'Dim dsList As DataSet = objStaffReqApproval.GetList("List", -1, CInt(Session("staffrequisitiontranunkid")), objStaffReq._Staffrequisitionbyid, _
                            '                                                    objStaffReq._Allocationunkid, CInt(Session("UserId")), CInt(dsApproverMapping.Tables(0).Rows(0)("levelunkid")), CInt(dsApproverMapping.Tables(0).Rows(0)("priority")), True, "")
                            'Sohail (17 Mar 2016) -- Start
                            'Issue - wrong parameter order was set.
                            'Dim dsList As DataSet = objStaffReqApproval.GetList("List", -1, CInt(Session("staffrequisitiontranunkid")), objStaffReq._Staffrequisitionbyid, _
                            '                                                  , objStaffReq._Allocationunkid, CInt(dsApproverMapping.Tables(0).Rows(0)("levelunkid")), CInt(dsApproverMapping.Tables(0).Rows(0)("priority")), True, "")
                            Dim dsList As DataSet = objStaffReqApproval.GetList("List", CInt(Session("UserId")), -1, CInt(Session("staffrequisitiontranunkid")), objStaffReq._Staffrequisitionbyid _
                                                                              , objStaffReq._Allocationunkid, CInt(dsApproverMapping.Tables(0).Rows(0)("levelunkid")), CInt(dsApproverMapping.Tables(0).Rows(0)("priority")), True, "")
                            'Sohail (17 Mar 2016) -- End
                            'Anjan [08 September 2015] -- End

                            'Hemant (15 Nov 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                            Dim dRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") = CInt(dsApproverMapping.Tables(0).Rows(0)("priority")))
                            'Hemant (15 Nov 2019) -- End

                            'Hemant (15 Nov 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                            '    If CInt(Session("statusunkid")) <> CInt(dsList.Tables(0).Rows(0)("statusunkid")) Then

                            '        Select Case CInt(dsList.Tables(0).Rows(0)("form_statusunkid"))
                            If dRow IsNot Nothing AndAlso dRow.Count > 0 Then

                                If CInt(Session("statusunkid")) <> CInt(dRow(0).Item("statusunkid")) Then

                                    Select Case CInt(dRow(0).Item("form_statusunkid"))
                                        'Hemant (15 Nov 2019) -- End

                                        Case enApprovalStatus.APPROVED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 9, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Approved."), Me, "../../Index.aspx")
                                            Exit Sub
                                        Case enApprovalStatus.REJECTED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 5, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Rejected."), Me, "../../Index.aspx")
                                            Exit Sub

                                        Case enApprovalStatus.CANCELLED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 6, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Cancelled."), Me, "../../Index.aspx")
                                            Exit Sub

                                        Case enApprovalStatus.PUBLISHED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 7, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Published."), Me, "../../Index.aspx")
                                            Exit Sub
                                    End Select

                                    'Hemant (15 Nov 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                    'Select Case CInt(dsList.Tables(0).Rows(0)("statusunkid"))
                                    Select Case CInt(dRow(0).Item("statusunkid"))
                                        'Hemant (15 Nov 2019) -- End

                                        Case enApprovalStatus.APPROVED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 9, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Approved."), Me, "../../Index.aspx")
                                            Exit Sub
                                        Case enApprovalStatus.REJECTED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 5, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Rejected."), Me, "../../Index.aspx")
                                            Exit Sub

                                        Case enApprovalStatus.CANCELLED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 6, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Cancelled."), Me, "../../Index.aspx")
                                            Exit Sub

                                        Case enApprovalStatus.PUBLISHED
                                            'Hemant (15 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 2 : The approval links do not land on the actual pages. They are landing on login page.)
                                            'Session.Abandon()
                                            'Hemant (15 Nov 2019) -- End
                                            'Language.setLanguage(mstrModuleName1)
                                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 7, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Published."), Me, "../../Index.aspx")
                                            Exit Sub
                                    End Select

                                End If

                            End If

                        End If

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("Login") = True
                    Else
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If

            End If
            'OrElse (Session("staffrequisitionapprovaltranunkid") Is Nothing AndAlso Session("staffrequisitiontranunkid") Is Nothing AndAlso Session("statusunkid") Is Nothing

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False) Then
            If CBool(Session("IsArutiDemo")) = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False) Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            Call SetLanguage()

FromMain:
            If IsPostBack = False Then
                FillCombo()
                FillInfo()
            Else
                mintCurrLevelPriority = CInt(Me.ViewState("CurrLevelPriority"))
                mintLowerLevelPriority = CInt(Me.ViewState("LowerLevelPriority"))
                mintMinPriority = CInt(Me.ViewState("MinPriority"))
                mintMaxPriority = CInt(Me.ViewState("MaxPriority"))
                mintCurrLevelPriority = CInt(Me.ViewState("CurrLevelPriority"))
                mintCurrLevelPriority = CInt(Me.ViewState("CurrLevelPriority"))
                mintCurrLevelID = CInt(Me.ViewState("CurrLevelID"))
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                mdtList = CType(Me.ViewState("mdtList"), DataTable)
                'Sohail (12 Oct 2018) -- End
            End If
            'Hemant (17 Oct 2019) -- Start
            'Hemant (30 Oct 2019) -- Start
            'lnkViewJobDescription.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.JobReport, CInt(Session("Userid")), Company._Object._Companyunkid)
            lnkViewJobDescription.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.JobReport, CInt(Session("Userid")), CInt(Session("Companyunkid")))
            'Hemant (30 Oct 2019) -- End
            'Hemant (17 Oct 2019) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
        Me.ViewState.Add("CurrLevelPriority", mintCurrLevelPriority)
        Me.ViewState.Add("LowerLevelPriority", mintLowerLevelPriority)
        Me.ViewState.Add("MinPriority", mintMinPriority)
        Me.ViewState.Add("MaxPriority", mintMaxPriority)
        Me.ViewState.Add("CurrLevelPriority", mintCurrLevelPriority)
        Me.ViewState.Add("CurrLevelPriority", mintCurrLevelPriority)
        Me.ViewState.Add("CurrLevelID", mintCurrLevelID)
        'Sohail (12 Oct 2018) -- Start
        'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
        Me.ViewState.Add("mdtList", mdtList)
        'Sohail (12 Oct 2018) -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMsg As String = ""
        Try
            If IsValidation() = False Then Exit Sub

            If CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
                'Sohail (02 Dec 2021) -- Start
                'Issue : OLD-529 : WARMA Zambia - Approved Staff requisition Still appears with a Pending Status.
                'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                    'Sohail (02 Dec 2021) -- End
                    'Language.setLanguage(mstrModuleName)
                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Are you sure you want to Final Approve this Staff Requisition?")
                Else
                    'Language.setLanguage(mstrModuleName)
                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Are you sure you want to Approve this Staff Requisition?")
                End If
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.CANCELLED Then
                'Language.setLanguage(mstrModuleName)
                strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Are you sure you want to Cancel this Staff Requisition?")
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                'Language.setLanguage(mstrModuleName)
                strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Are you sure you want to Reject this Staff Requisition?")
            End If

            ApproveDisapprove.Message = strMsg
            ApproveDisapprove.Show()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisitionList.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ApproveDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ApproveDisapprove.buttonYes_Click
        Try
            objStaffReqApproval._Staffrequisitionapprovaltranunkid = CInt(Session("staffrequisitionapprovaltranunkid"))
            Call SetValue()

            Dim mstrDisApprovalReason As String = String.Empty
            If CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                If txtRemarks.Text.Trim = "" Then 'Hemant (03 Sep 2019)
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only.)
                    'DeleteReason.Show()
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Please Enter Remarks for Rejection."), Me)
                    txtRemarks.Focus()
                    Exit Try
                    'Hemant (07 Oct 2019) -- End
                    'Hemant (03 Sep 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only ).
                Else
                    SaveData()
                End If 'Hemant (03 Sep 2019) -- End
            Else
                SaveData()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("ApproveDisapprove_buttonYes_Click:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteReason.buttonDelReasonYes_Click
        Try
            objStaffReqApproval._Disapprovalreason = DeleteReason.Reason
            SaveData()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("DeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisitionList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region "Controls Event(S)"
    'Hemant (22 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Protected Sub lnkViewJobDescription_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewJobDescription.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Session("intJobunkid") = CInt(Me.ViewState("jobunkid"))
            Response.Redirect(Session("rootpath") & "Reports/Rpt_Employee_Job_Report.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (22 Aug 2019) -- End

#End Region


    'Gajanan [21-April-2020] -- Start   
#Region "Gridview Event"
    Protected Sub lnkDownloadAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try         'Hemant (13 Aug 2020)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim CommandName As String = btn.CommandName
        Dim rowIndex As String = btn.CommandArgument

        Dim xPath As String = ""
        If CInt(dgvstaffRequisitionAttachment.Items(rowIndex).Cells(getColumnId_Datagrid(dgvstaffRequisitionAttachment, "objcolhScanUnkId", False, False)).Text) > 0 Then
            xPath = dgvstaffRequisitionAttachment.Items(rowIndex).Cells(getColumnId_Datagrid(dgvstaffRequisitionAttachment, "objcolhfilepath", False, False)).Text
            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
            If Strings.Left(xPath, 1) <> "/" Then
                xPath = "~/" & xPath
            Else
                xPath = "~" & xPath
            End If
            xPath = Server.MapPath(xPath)

        ElseIf dgvstaffRequisitionAttachment.Items(rowIndex).Cells(getColumnId_Datagrid(dgvstaffRequisitionAttachment, "objcolhGUID", False, False)).Text <> "" Then
            xPath = dgvstaffRequisitionAttachment.Items(rowIndex).Cells(getColumnId_Datagrid(dgvstaffRequisitionAttachment, "objcolhlocalpath", False, False)).Text
        End If
        If xPath.Trim <> "" Then
            Dim fileInfo As New IO.FileInfo(xPath)
            If fileInfo.Exists = False Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2002, "File does not Exist..."), Me)
                Exit Sub
            End If
            fileInfo = Nothing
            Dim strFile As String = xPath
            Response.ContentType = "image/jpg/pdf"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & dgvstaffRequisitionAttachment.Items(rowIndex).Cells(getColumnId_Datagrid(dgvstaffRequisitionAttachment, "File Name", False, False)).Text & """")
            Response.Clear()
            Response.TransmitFile(strFile)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
#End Region
    'Gajanan [21-April-2020] -- End

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        'Language.setLanguage(mstrModuleName)

        Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END

        Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRemarks.ID, Me.lblRemarks.Text)
        Me.gbStaffRequisitionList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gbStaffRequisitionList.ID, Me.gbStaffRequisitionList.Text)
        Me.gbStaffReqApprovalnfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.gbStaffReqApprovalnfo.ID, Me.gbStaffReqApprovalnfo.Text)
        Me.lblLevelPriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLevelPriority.ID, Me.lblLevelPriority.Text)
        Me.lblApproverLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApproverLevel.ID, Me.lblApproverLevel.Text)
        Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprover.ID, Me.lblApprover.Text)
        Me.lblFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFormNo.ID, Me.lblFormNo.Text)
        Me.lblAddStaffReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAddStaffReason.ID, Me.lblAddStaffReason.Text)
        Me.lblLeavingReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLeavingReason.ID, Me.lblLeavingReason.Text)
        Me.lblWorkStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblWorkStartDate.ID, Me.lblWorkStartDate.Text)
        Me.lblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblClass.ID, Me.lblClass.Text)
        Me.lblJobDesc.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblJobDesc.ID, Me.lblJobDesc.Text).Replace("&&", "&")
        Me.lblClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblClassGroup.ID, Me.lblClassGroup.Text)
        Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblName.ID, Me.lblName.Text)
        Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblJob.ID, Me.lblJob.Text)
        Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAllocation.ID, Me.lblAllocation.Text)
        Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
        Me.lblApprovalstatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprovalstatus.ID, Me.lblApprovalstatus.Text)
        Me.lblAvailable.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAvailable.ID, Me.lblAvailable.Text)
        Me.lblVariation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblVariation.ID, Me.lblVariation.Text)
        Me.lblPlanned.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPlanned.ID, Me.lblPlanned.Text)
        Me.lblJobHeadCountInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblJobHeadCountInfo.ID, Me.lblJobHeadCountInfo.Text)
        Me.lblApproverInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)

        Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
        Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        'Shani (18 Mar 2015) -- Start
        'Enhancement - Allow more than one employee to be replaced in staff requisition.
        Me.lblNoofPosition.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblNoofPosition.ID, Me.lblNoofPosition.Text)
        Me.dgvEmployeeList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvEmployeeList.Columns(1).FooterText, Me.dgvEmployeeList.Columns(1).HeaderText)
        Me.dgvEmployeeList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvEmployeeList.Columns(2).FooterText, Me.dgvEmployeeList.Columns(2).HeaderText)
        Me.dgvEmployeeList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvEmployeeList.Columns(3).FooterText, Me.dgvEmployeeList.Columns(3).HeaderText)
        Me.dgvEmployeeList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvEmployeeList.Columns(4).FooterText, Me.dgvEmployeeList.Columns(4).HeaderText)
        'Shani (18 Mar 2015) -- End
        'Hemant (22 Aug 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
        Me.lnkViewJobDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkViewJobDescription.ID, Me.lnkViewJobDescription.Text)
        'Hemant (22 Aug 2019) -- End


        Me.dgvstaffRequisitionAttachment.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvstaffRequisitionAttachment.Columns(0).FooterText, Me.dgvstaffRequisitionAttachment.Columns(0).HeaderText)
        Me.dgvstaffRequisitionAttachment.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvstaffRequisitionAttachment.Columns(1).FooterText, Me.dgvstaffRequisitionAttachment.Columns(1).HeaderText)
        Me.dgvstaffRequisitionAttachment.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvstaffRequisitionAttachment.Columns(2).FooterText, Me.dgvstaffRequisitionAttachment.Columns(2).HeaderText)


        'Language.setLanguage(mstrModuleName1)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub
End Class
