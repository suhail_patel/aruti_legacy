﻿<%@ Page MasterPageFile="~/Home1.master" Language="VB" AutoEventWireup="false" CodeFile="wPg_ApproveDisapproveStaffRequisition.aspx.vb"
    Inherits="Recruitment_Staff_Requisition_wPg_ApproveDisapproveStaffRequisition"
    Title="Approve / Disapprove Staff Requisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/Delete_Reason.ascx" TagName="DeleteReason" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>

    <script type="text/javascript">

  function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }
       
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc4:Confirmation ID="ApproveDisapprove" runat="server" Message="" Title="Confirmation" />
                <uc6:DeleteReason ID="DeleteReason" runat="server" Title="Void Reason" ReasonType="STAFF_REQUISITION" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approve / Disapprove Staff Requisition"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbStaffRequisitionList" runat="server" Text="Staff Requisition Information"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFormNo" runat="server" Text="Form No." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFormNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Requisition Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtStatus" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAllocation" runat="server" Text="Requisition By" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtAllocation" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtName" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtClassGroup" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtClass" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblJob" runat="server" Text="Job Title" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtJob" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblJobDesc" runat="server" Text="Job Description & Indicators" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtJobDesc" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                                    CssClass="form-control" Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <asp:LinkButton ID="lnkViewJobDescription" runat="server" Text="View Job Description"
                                                            Style="color: Blue; vertical-align: top; float: left;"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee to be replaced" CssClass="form-label"></asp:Label>
                                                        <div class="table-responsive" style="max-height: 350px;">
                                                            <asp:Panel ID="dgv_dgvEmployeeList" runat="server" Width="100%" ScrollBars="Auto">
                                                                <asp:DataGrid ID="dgvEmployeeList" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                                    Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="employeeunkid" Visible="false" FooterText="objcolhEmpID">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="colhEmpCode">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="name" HeaderText="name" FooterText="colhEmpName"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="grade" HeaderText="Grade" FooterText="colhGrade"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="gradelevel" HeaderText="Salary Band" FooterText="colhSalaryBand">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="actionreasonunkid" Visible="false" FooterText="objcolhActionReasonId">
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLeavingReason" runat="server" Text="Leaving Reason" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLeavingReason" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAddStaffReason" runat="server" Text="Additional Staff Reason" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtAddStaffReason" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                                    CssClass="form-control" Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblNoofPosition" runat="server" Text="No. of Position" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtNoofPosition" runat="server" Width="90%" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblWorkStartDate" runat="server" Text="Date to Start Work" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtWorkStartDate" runat="server" Width="90%" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbStaffReqApprovalnfo" runat="server" Text="Staff Requisition Approval Information"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblJobHeadCountInfo" runat="server" Text="Job Head Count Information"
                                                            CssClass="form-label"></asp:Label>
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblPlanned" runat="server" Text="Planned Head Counts"></asp:Label><span
                                                                    class="badge bg-info"><asp:Label ID="objlblPlanned" runat="server" Text="0"></asp:Label></span></li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblAvailable" runat="server" Text="Available Head Counts"></asp:Label><span
                                                                    class="badge bg-info"><asp:Label ID="objlblAvailable" runat="server" Text="0"></asp:Label></span></li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblVariation" runat="server" Text="Variations"></asp:Label><span class="badge bg-info"><asp:Label
                                                                    ID="objlblVariation" runat="server" Text="0"></asp:Label></span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="divider divider-with-text-left">
                                                        <span class="ant-divider-inner-text"></span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproverInfo" runat="server" Text="Approver Information" Width="100%"
                                                            CssClass="form-label"></asp:Label>
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label><span class="badge bg-info"><asp:Label
                                                                    ID="objApproverName" runat="server" Text=""></asp:Label></span></li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblApproverLevel" runat="server" Text="Approver Level"></asp:Label><span
                                                                    class="badge bg-info"><asp:Label ID="objApproverLevelVal" runat="server" Text=""></asp:Label></span></li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblLevelPriority" runat="server" Text="Level Priority"></asp:Label><span
                                                                    class="badge bg-info"><asp:Label ID="objLevelPriorityVal" runat="server" Text="0"></asp:Label></span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="divider divider-with-text-left">
                                                        <span class="ant-divider-inner-text"></span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprovalstatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card inner-card">
                                            </h2>
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="Label1" runat="server" Text="Attach Document" CssClass="form-label"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 300px;">
                                                            <asp:Panel ID="pnl_staffRequisitionAttachment" runat="server" ScrollBars="Auto">
                                                                <asp:DataGrid ID="dgvstaffRequisitionAttachment" runat="server" AllowPaging="false"
                                                                    AutoGenerateColumns="False" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                                    RowStyle-Wrap="false">
                                                                    <Columns>
                                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                        <asp:TemplateColumn HeaderText="Download" FooterText="objcohDownload" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" CommandArgument='<%#Container.ItemIndex %>'
                                                                                    ToolTip="Download" OnClick="lnkDownloadAttachment_Click">
                                                                                                            <i class="fas fa-download"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                            Visible="false" />
                                                                        <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                            Visible="false" />
                                                                        <asp:BoundColumn HeaderText="objcolhfilepath" DataField="filepath" FooterText="objcolhfilepath"
                                                                            Visible="false" />
                                                                        <asp:BoundColumn HeaderText="objcolhlocalpath" DataField="localpath" FooterText="objcolhlocalpath"
                                                                            Visible="false" />
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmptyAttachment" runat="server" ForeColor="Red" Text="No Document Attach"
                                                            CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">                               
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgvstaffRequisitionAttachment" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
