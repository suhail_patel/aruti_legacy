﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Home1.master" CodeFile="wPg_StaffRequisition_AddEdit.aspx.vb"
    Inherits="Recruitment_Staff_Requisition_wPg_StaffRequisition_AddEdit" Title="Staff Requisition Add/Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    
    <script src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
        function IsValidAttach() {
        debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
           
            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    

    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 99%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                
                <uc4:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition Add/Edit" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Staff Requisition Add/Edit"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFormNo" runat="server" Text="Form No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtFormNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Requisition Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeavingReason" runat="server" Text="Leaving Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLeavingReason" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAllocation" runat="server" Text="Requisition By" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAllocation" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboName" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmploymentType" runat="server" Text="Employment Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmploymentType" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboClassGroup" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboClass" AutoPostBack="false" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblContractDuration" runat="server" Text="Contract Duration (months) if Employment type is Contract"
                                            CssClass="form-label"></asp:Label>
                                        <nut:NumericText ID="nudContractDuration" runat="server" Max="9999" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div>
                                        <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div>
                                         <asp:Label ID="lblJobDesc" runat="server" Text="Job Description &amp; Indicators"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtJobDesc" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:LinkButton ID="lnkViewJobDescription" runat="server" Text="View Job Description"
                                             Style="color: Blue; vertical-align: top; float: left;"></asp:LinkButton>
                                    </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                        <div>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <asp:Label ID="lblPlanned" runat="server" Text="Planned Head Counts" Font-Bold="true"></asp:Label>
                                                <span class="badge bg-info">
                                                    <asp:Label ID="objlblPlanned" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                                </span></li>
                                            <li class="list-group-item">
                                                <asp:Label ID="lblAvailable" runat="server" Text="Available Head Counts" Font-Bold="true"></asp:Label>
                                                <span class="badge bg-info">
                                                    <asp:Label ID="objlblAvailable" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                                </span></li>
                                            <li class="list-group-item">
                                                <asp:Label ID="lblVariation" runat="server" Text="Variations" Font-Bold="true"></asp:Label>
                                                <span class="badge bg-info">
                                                    <asp:Label ID="objlblVariation" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                                </span></li>
                                        </ul>
                                    </div>
                                        <div>
                                            <asp:Label ID="lblNoofPosition" runat="server" Text="No. of Position" CssClass="form-label"></asp:Label>
                                        <nut:NumericText ID="txtPosition" runat="server" Max="9999" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div>
                                        <asp:Label ID="lblAddStaffReason" runat="server" Text="Additional Staff Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAddStaffReason" runat="server" Rows="3" TextMode="MultiLine"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                        <div>
                                              <asp:Label ID="lblWorkStartDate" runat="server" Text="Date to Start Work" CssClass="form-label"></asp:Label>
                                        <uc2:DateControl ID="dtpWorkStartDate" runat="server" />
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblJobReportTo" runat="server" Text="Job Reporting Line" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboJobReportTo" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblJobAdvert" runat="server" Text="Job Advert" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboJobAdvert" runat="server" AutoPostBack="false" />
                                    </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                      
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrade" runat="server" Text="Grade" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboGrade" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboGradeLevel" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 ">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblSearchEmp" runat="server" Text="Search Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 350px;">
                                                <asp:Panel ID="pnl_dgvEmployeeList" runat="server" Width="100%" ScrollBars="Auto">
                                                    <asp:DataGrid ID="dgvEmployeeList" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                        Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                FooterText="objcolhCheckAll">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkAllSelect"
                                                                        Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("ischecked") %>'
                                                                        OnCheckedChanged="chkSelect" Text=" " />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="employeeunkid" Visible="false" FooterText="objcolhEmpID">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="employeecode" FooterText="colhEmpCode" HeaderText="Code">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" FooterText="colhEmpName" HeaderText="Employee Name">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="job_name" FooterText="colhJob" HeaderText="Job"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="grade" FooterText="colhGrade" HeaderText="Grade"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="gradelevel" FooterText="colhSalaryBand" HeaderText="Salary Band">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="actionreasonunkid" Visible="false" FooterText="objcolhActionReasonId"
                                                                HeaderText=""></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="AUD" Visible="false" FooterText="objcolhAUD" HeaderText="">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee to be replaced" CssClass="form-label"></asp:Label>
                                            <asp:CheckBox ID="chkShowInActiveEmployee" runat="server" Text="Show Inactive Employee"
                                                AutoPostBack="true"></asp:CheckBox>
                                            <asp:CheckBox ID="chkShowPreviousJobEmp" runat="server" Text="Show Previous Job"
                                                AutoPostBack="true"></asp:CheckBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  ">
                                       <%-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--%>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboDocumentType" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 m-t-30">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-20  ">
                                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                        <div id="fileuploader">
                                                            <input type="button" id="btnAddFile" runat="server" onclick="return IsValidAttach()"
                                                                value="Browse" class="btn btn-primary"/>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  p-l-0">
                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                                    <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse" />
                                                </div>
                                            </div>
                                        <%--</div>--%>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 350px;">
                                                <asp:DataGrid ID="dgvQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                    Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                    HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateColumn FooterText="objcohDelete">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                        <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                        <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                            ItemStyle-Font-Size="22px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                            Visible="false" />
                                                        <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                            Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">                                
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
			});
			function ImageLoad(){
			debugger;
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPg_StaffRequisition_AddEdit.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAddAttachment.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			//$('input[type=file]').live("click", function() {
			$("body").on("click", 'input[type=file]', function(){
			    return IsValidAttach();
			});
    </script>

</asp:Content>
