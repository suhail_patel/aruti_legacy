﻿Option Strict On
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
Imports System.IO

#End Region
Partial Class Recruitment_wPgEmployeeUpdate
    Inherits Basepage

#Region " Private Variable(s) "
    Private msg As New CommonCodes
#End Region

#Region " Private Function(s) & Method(s) "

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Nov 2020) -- End


#End Region

#Region " Links Events "

    Protected Sub lnkQuickLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMySkills.Click, _
                                                                                                  lnkAttachment.Click, lnkMyExperiences.Click, _
                                                                                                  lnkMyReferences.Click, lnkMyProfile.Click, _
                                                                                                  lnkMyQualification.Click, lnkAddress.Click, lnkPersonal.Click

        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                Dim btnquickLink As LinkButton = TryCast(sender, LinkButton)
                Session.Add("IsfromRecruitment", True)
                Select Case btnquickLink.ID
                    Case lnkMySkills.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeSkillList.aspx", False)
                    Case lnkMyProfile.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeDetail.aspx", False)
                    Case lnkMyExperiences.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeExperienceList.aspx", False)
                    Case lnkMyReferences.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpRefereeList.aspx", False)
                    Case lnkMyQualification.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx", False)
                    Case lnkAddress.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeAddress.aspx", False)
                    Case lnkPersonal.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeePersonal.aspx", False)
                    Case lnkAttachment.ID
                        Basepage.SetAttachmentSession("Select Employee", 22, 0, "")
                        Response.Redirect(Session("rootpath").ToString & "Others_Forms/wPg_ScanOrAttachmentInfo.aspx", False)
                End Select


            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


#End Region

#Region "Control Event"

#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Recruitment/SearchJob.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
End Class
