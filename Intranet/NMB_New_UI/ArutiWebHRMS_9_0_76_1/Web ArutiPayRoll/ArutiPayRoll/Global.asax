﻿<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    'S.SANDEEP |24-AUG-2020| -- START
    'ISSUE/ENHANCEMENT : Unhandled Exception
    'Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    ' Code that runs when an unhandled error occurs
    'End Sub
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ex As Exception = Server.GetLastError()
            If TypeOf ex Is HttpUnhandledException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.OutOfMemoryException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.StackOverflowException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.Web.HttpException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.ArgumentException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.NullReferenceException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is ViewStateException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.InvalidCastException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.FormatException Then
                CommonCodes.LogErrorOnly(ex)
                HttpContext.Current.ClearError()
            ElseIf TypeOf ex Is System.Net.Sockets.SocketException Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'CommonCodes.LogErrorOnly(ex)
                'Pinkal (25-Jan-2022) -- End
                HttpContext.Current.ClearError()
                
                'Pinkal (01-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	
            ElseIf TypeOf ex Is System.Threading.ThreadAbortException Then
                HttpContext.Current.ClearError()
                'Pinkal (01-Feb-2022) -- End
                
                
            End If
        Catch ex As Exception
            
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Dim errLog As New Diagnostics.EventLog()
            'Dim mstrErrorMsg As String = ""
            'mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : " & ex.Message & "; " & ex.StackTrace.ToString
            'If ex.InnerException IsNot Nothing Then
            '    mstrErrorMsg &= "; " & ex.InnerException.Message
            'End If
            'errLog.WriteEntry(mstrErrorMsg, Diagnostics.EventLogEntryType.Warning)
            CommonCodes.WriteEventLog(ex)
            'Pinkal (20-Aug-2020) -- End

        End Try
    End Sub
    'S.SANDEEP |24-AUG-2020| -- END

    'Sohail (05 Feb 2019) -- Start
    'NMB Enhancement : Security issues in Self Service in 75.1
    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        Response.AddHeader("X-Frame-Options", "sameorigin")
    End Sub
    'Sohail (05 Feb 2019) -- End

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        
        'Sohail (13 Dec 2018) -- Start
        'NMB Enhancement : Security issues in Self Service in 75.1
        If Request.IsSecureConnection() = True Then
            Response.Cookies("ASP.NET_SessionID").Secure = True
        End If        
        'Sohail (13 Dec 2018) -- End
        
        Session("imgAcc") = ""
        'Sohail (24 Nov 2016) -- Start
        'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
        HttpContext.Current.Session("Theme_id") = 1
        HttpContext.Current.Session("Lastview_id") = 0
        'Sohail (24 Nov 2016) -- End
        Session("menuView") = 0
        Session("Theme")= "blue"
        
        'Dim pathname As String = Request.Url.AbsoluteUri
        'Dim arr() As String = pathname.Split("/")
        'Session("ServerPath") = arr(0) + "/" + arr(1) + "/" + arr(2) + "/" + arr(3) + "/"
        
        'Session("TotalMenu") = 8
        'Session("imgAcc1") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc2") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc3") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc4") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc5") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc6") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc7") = Session("ServerPath") + "images/plus.png"
        'Session("imgAcc8") = Session("ServerPath") + "images/plus.png"
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
</script>