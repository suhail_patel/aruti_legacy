﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.IO
Imports System.Drawing

#End Region

Partial Class PDP_wPgEmployeePdp
    Inherits Basepage

#Region "Private Variable"

    'General 
    Private DisplayMessage As New CommonCodes
    Private clsuser As New User
    Private Shared ReadOnly mstrModuleName As String = "frmEmployeePdp"


    Private Enum enDeleteAction
        DeleteActionPlanTrainingItem = 1
        DeleteActionPlan = 2
        DeletePersonlaAnalysisGoal = 3
        DeleteActionPlanComment = 4
        DeleteLastUpdateProgress = 5
        RejectTraining = 6
    End Enum

    Private Enum enConfirmationAction
        ChangeActionPlanGoalFromSelection = 1
        ResetActionPlanSelection = 2
    End Enum

    Private Enum enPDPFormType
        EmployeeDetail = 1
        PersonalAnalysisandGoals = 2
        DevelopmentActionPlan = 3
        SetCommentForDevelopmentActionPlan = 4
        SetUpdateProgressForDevelopmentActionPlan = 5
        SetDeleteTrainingForDevelopmentActionPlan = 6
        AddTrainingForDevelopmentActionPlan = 7
    End Enum

    Private mintFormunkidId As Integer = -1
    Private mintDeleteActionId As Integer = -1
    Private mintConfirmationActionId As Integer = -1

    Private mintFormStatusUnkid As Integer = 0


    'Employee Detail Tab

    'Personal Analysis Tab
    Private mintCategoryId As Integer = -1
    Private mintItemId As Integer = -1
    Private mintItemTranId As Integer = -1
    Private mintItemtypeid As Integer = -1
    Private mblnIsAddItemData As Boolean = False
    Private mstrPersonalAnalysisEditGUID As String = ""

    'Personal Analysis Popup-Add/Edit
    Private mblnPersonalAnalysisandGoalsListPopup As Boolean = False
    Private mblnPersonalAnalysisandGoalsTablePopup As Boolean = False




    'Action Plan Tab
    Private mintActionPlanItemUnkId As Integer = -1
    Private mintTrainingCourseunkid As Integer = -1
    Private mblnActionPlanSelectionPopup As Boolean = False
    Private mintActionPlanCategoryId As Integer = -1
    Private mintActionPlanGoalSelectedId As Integer = -1
    Private mblnDevelopmentActionPlanPopup As Boolean = False
    Private mstrActionPlanTrainingDeleteReason As String = ""
    Private mintActionPlanCoachId As Integer = -1

    'Action Plan Popup-Add/Edit

    'Action Plan-Comment Popup
    Private mblnActionPlanCommentPopup As Boolean = False
    Private mintActionPlanCommentItemunkid As Integer = -1
    Dim mintEvalutorTypeId As Integer = -1
    Dim mintEvalutorUnkid As Integer = -1
    Dim mintReviewerUnkid As Integer = -1

    'Action Plan-Comment-Attachment Popup
    Private objDocument As New clsScan_Attach_Documents
    Private mdtCommentAttachment As DataTable = Nothing
    Private mblnCommentAttachmentPopup As Boolean = False
    Private mblnIsAttachmentDownloadMode As Boolean = False
    Private mintDeleteAttachmentId As Integer = -1
    Dim mintattachempid As Integer = 0

    'Action Plan-Update Progress Popup
    Private mblnUpdateProgressPopup As Boolean = False

    'Action Plan-View Training Popup
    Private mblnViewTrainingPopup As Boolean = False
    Private mintDeleteActionPlanTrainingcourseunkid As Integer = -1

    'Personal Analysis Custom Item Analysis
    Private mblnPersonalAnalysisCompetencySelectionPopup As Boolean = False
    Private mintPersonalAnalysisCompetencySelectedId As Integer = -1
    Private mblnCompetencySelectionFromList As Boolean = False
    Private mintListPersonalAnalysisSelectionDataListItemIndex As Integer = 0

    Dim mintTalentEmployeeUnkid As Integer = -1
    Dim mintSuccessionEmployeeUnkid As Integer = -1
    Private mblnIsAvailableInTalent As Boolean = False
    Private mblnIsAvailableInSuccession As Boolean = False

    Private mblnReviewTrainingPopup As Boolean = False

    Private objCONN As SqlConnection
    Dim mintDirectEmployeeUnkid As Integer = -1
    Dim mblnIsLineManager As Boolean = False

#End Region

#Region " Private Function(s) & Method(s) "

    'PDP Form Master
    Private Sub Generate_TableCItemPopup_Data(ByVal xHeaderUnkid As Integer, ByVal xmstriEditingGUID As String, ByVal xHeaderName As String)
        Dim dtCItems As New DataTable
        Dim objpdpform_master As New clspdpform_master
        Dim objpdpitem_master As New clspdpitem_master
        Dim intPeriodid As Integer = -1
        Try
            intPeriodid = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))
            dtCItems = objpdpform_master.Get_CTableItems_ForAddEdit(intPeriodid, xHeaderUnkid, xmstriEditingGUID)
            If dtCItems IsNot Nothing AndAlso dtCItems.Rows.Count <= 0 Then Exit Sub

            dgv_Citems.DataSource = dtCItems
            dgv_Citems.DataBind()
            mblnPersonalAnalysisandGoalsTablePopup = True
            popup_CItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtCItems.Dispose()
            objpdpform_master = Nothing
        End Try
    End Sub

    Private Function SavePersonalFormMasterList(ByVal xFieldValue As String) As Boolean
        Dim objpdpform_master As New clspdpform_master
        Dim blnFlag As Boolean = False

        Try
            Dim xGuid As String = ""
            If mstrPersonalAnalysisEditGUID.Length > 0 Then
                objpdpform_master._Pdpformunkid = mintFormunkidId
                xGuid = mstrPersonalAnalysisEditGUID
            Else
                xGuid = Guid.NewGuid().ToString()
                objpdpform_master._Pdpformunkid = mintFormunkidId
            End If
            objpdpform_master._Itemgrpguid = xGuid



            objpdpform_master._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objpdpform_master._Nextjobid = CInt(cboNextJob.SelectedValue)

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpdpform_master._AuditUserId = CInt(Session("UserId"))
            Else
                objpdpform_master._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If



            objpdpform_master._ClientIP = CStr(Session("IP_ADD"))
            objpdpform_master._FormName = mstrModuleName
            objpdpform_master._FromWeb = True
            objpdpform_master._HostName = CStr(Session("HOST_NAME"))
            objpdpform_master._DatabaseName = CStr(Session("Database_Name"))
            objpdpform_master._Isvoid = False
            objpdpform_master._IsAddItemData = mblnIsAddItemData

            If mintFormunkidId <= 0 Then
                objpdpform_master._Statusunkid = clspdpform_master.enPDPFormStatus.Unlock
            End If


            If mblnIsAddItemData Then

                Dim objpdpform_tran As New clspdpform_tran
                objpdpform_tran._Fieldvalue = xFieldValue
                If objpdpform_tran.isValueExist(xFieldValue, xGuid, mintCategoryId, mintItemId, mintFormunkidId) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, this data cannot be saved because it's a duplicate of an existing record. Please enter something new to continue."), Me)
                    Exit Function
                End If


                objpdpform_master._Categoryid = mintCategoryId
                objpdpform_master._Itemunkid = mintItemId

                If mintPersonalAnalysisCompetencySelectedId > 0 Then
                    objpdpform_master._Fieldvalue = mintPersonalAnalysisCompetencySelectedId
                    objpdpform_master._IsCompetencySelection = True
                Else
                    objpdpform_master._Fieldvalue = xFieldValue
                    objpdpform_master._IsCompetencySelection = False
                End If

                objpdpform_master._Itemtranunkid = mintItemTranId
            End If

            blnFlag = objpdpform_master.SavePDPEmployeeData()



            If blnFlag = False AndAlso objpdpform_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpdpform_master._Message, Me)
                Return False
            Else
                If mblnIsAddItemData Then
                    FillPersonalAnalysisItems()
                End If
                ClearPersolnalGoalPopupData()

                If mintFormunkidId <= 0 Then
                    FillPDPFormData()
                End If

                Return True
            End If

            Return False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpform_master = Nothing
        End Try
    End Function

    Private Sub FillPersonalAnalysisItems()
        Dim dsData As New DataSet
        Dim objpdpcategory_master As New clspdpcategory_master
        Dim objpdpform_master As New clspdpform_master
        Try
            dsData = objpdpcategory_master.GetList("Category")
            dlPersonalAnalysisGoalsCategory.DataSource = dsData.Tables("Category")
            dlPersonalAnalysisGoalsCategory.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpcategory_master = Nothing
            objpdpform_master = Nothing
        End Try
    End Sub

    Private Sub FillPDPFormData()
        Dim dsData As New DataSet
        Dim objpdpcategory_master As New clspdpcategory_master
        Dim objpdpform_master As New clspdpform_master
        Dim objEmployee As New clsEmployee_Master
        Try
            dsData = objpdpform_master.GetList("EmpNewJob", True, CInt(cboEmployee.SelectedValue))
            btnSubmitForReview.Visible = False
            btnLock.Visible = False
            btnUnlock.Visible = False
            btnReviewTrainings.Visible = False
            cboNextJob.Enabled = True


            If IsNothing(dsData) = False AndAlso dsData.Tables("EmpNewJob").Rows.Count > 0 Then
                cboNextJob.SelectedValue = dsData.Tables("EmpNewJob").Rows(0)("nextjobid")
                mintFormunkidId = dsData.Tables("EmpNewJob").Rows(0)("pdpformunkid")
                objpdpform_master._Pdpformunkid = mintFormunkidId
                mintFormStatusUnkid = objpdpform_master._Statusunkid



                Select Case mintFormStatusUnkid
                    Case clspdpform_master.enPDPFormStatus.SubmitForReview

                        'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        '    btnLock.Visible = Session("AllowToLockPDPForm")
                        '    btnReviewTrainings.Visible = Session("AllowToLockPDPForm")
                        'Else
                        '    btnLock.Visible = False
                        '    btnReviewTrainings.Visible = False
                        'End If

                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            If mblnIsLineManager Then
                                btnLock.Visible = True
                                btnReviewTrainings.Visible = True
                            Else
                                btnLock.Visible = False
                                btnReviewTrainings.Visible = False
                            End If
                        Else
                            btnLock.Visible = False
                            btnReviewTrainings.Visible = False
                        End If

                    Case clspdpform_master.enPDPFormStatus.Lock

                        'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        '    btnUnlock.Visible = Session("AllowToUnlockPDPForm")
                        '    cboNextJob.Enabled = False
                        'Else
                        '    btnUnlock.Visible = False
                        '    cboNextJob.Enabled = False
                        'End If

                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            If mblnIsLineManager Then
                                btnUnlock.Visible = True
                                cboNextJob.Enabled = False
                            Else
                                btnUnlock.Visible = False
                            cboNextJob.Enabled = False
                            End If

                        Else
                            btnUnlock.Visible = False
                            cboNextJob.Enabled = False
                        End If

                    Case clspdpform_master.enPDPFormStatus.Unlock
                        'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        '    btnSubmitForReview.Visible = False
                        '    btnLock.Visible = Session("AllowToLockPDPForm")
                        '    btnReviewTrainings.Visible = Session("AllowToLockPDPForm")
                        'Else
                        '    btnSubmitForReview.Visible = True
                        '    btnLock.Visible = False
                        '    btnReviewTrainings.Visible = False
                        'End If


                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                            If mblnIsLineManager Then
                            btnSubmitForReview.Visible = False
                                btnLock.Visible = True
                                btnReviewTrainings.Visible = True
                            Else
                                btnSubmitForReview.Visible = False
                                btnLock.Visible = False
                                btnReviewTrainings.Visible = False
                            End If

                        Else
                            btnSubmitForReview.Visible = True
                            btnLock.Visible = False
                            btnReviewTrainings.Visible = False
                        End If

                End Select

            Else
                mintFormunkidId = -1
                mintFormStatusUnkid = CInt(clspdpform_master.enPDPFormStatus.Unlock)
                cboNextJob.SelectedIndex = 0
                'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                '    btnSubmitForReview.Visible = False
                '    btnLock.Visible = Session("AllowToLockPDPForm")
                'Else
                '    btnSubmitForReview.Visible = True
                '    btnLock.Visible = False
                'End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpcategory_master = Nothing
            objpdpform_master = Nothing
        End Try
    End Sub

    Private Function IsPDPDataValid(ByVal enTabType As enPDPFormType, Optional ByVal enActionType As enAuditType = -1) As Boolean
        Try

            If mintFormunkidId > 0 Then
                Select Case mintFormStatusUnkid

                    '-------------------------------------------------------------Submit For Review MODE---------------------------------------------------------------------

                    Case clspdpform_master.enPDPFormStatus.SubmitForReview
                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            Select Case enTabType
                                Case enPDPFormType.EmployeeDetail

                                Case enPDPFormType.PersonalAnalysisandGoals

                                Case enPDPFormType.DevelopmentActionPlan
                                    Select Case enActionType
                                        Case enAuditType.ADD
                                           
                                        Case enAuditType.EDIT
                                           
                                        Case enAuditType.DELETE
                                           
                                    End Select
                                Case enPDPFormType.SetCommentForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 92, "Sorry, you cannot comment on this action plan now. This PDP form is submitted for review. Please lock it to continue."), Me)
                                    Return False

                                Case enPDPFormType.SetUpdateProgressForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 96, "Sorry, you cannot update progress on this action plan now. This PDP form is still under review. Please lock it to continue."), Me)
                                    Return False
                                Case enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan

                                Case enPDPFormType.AddTrainingForDevelopmentActionPlan
                                 


                            End Select
                        Else
                            Select Case enTabType

                                Case enPDPFormType.EmployeeDetail

                                Case enPDPFormType.PersonalAnalysisandGoals

                                    Select Case enActionType
                                        Case enAuditType.ADD
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, you cannot add anything to your development action plan. Your PDP form is already submitted for review. Please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.EDIT
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, you cannot edit anything in your development action plan. Your PDP form is already submitted for review. Please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.DELETE
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, you cannot delete anything from your development action plan. Your PDP form is already submitted for review. Please request for unlocking."), Me)
                                            Return False
                                    End Select

                                Case enPDPFormType.DevelopmentActionPlan
                                    Select Case enActionType
                                        Case enAuditType.ADD
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 67, "Sorry, your personal development form is already submitted for review. To add anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.EDIT
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 68, "Sorry, your personal development form is already submitted for review. To edit anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.DELETE
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 69, "Sorry, your personal development form is already submitted for review. To delete anything, please request for unlocking."), Me)
                                            Return False
                                    End Select

                                Case enPDPFormType.SetCommentForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 93, "Sorry, you cannot comment on this action plan now. Your PDP form is still under review. Please wait for this PDP form to be approved and locked."), Me)
                                    Return False

                                Case enPDPFormType.SetUpdateProgressForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 97, "Sorry, you cannot update progress on this action plan now. Your PDP form is still under review. Please wait for your PDP form to be approved and locked."), Me)
                                    Return False
                                Case enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot delete this training. Your PDP form is already submitted for review. Please wait for the PDP form to be reviewed and unlocked."), Me)
                                    Return False

                                Case enPDPFormType.AddTrainingForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, you cannot add training(s) to the action plan. Your PDP form is already submitted for review. Please wait for the review to continue."), Me)
                                    Return False


                            End Select
                        End If

                        '-------------------------------------------------------------LOCK MODE---------------------------------------------------------------------

                    Case clspdpform_master.enPDPFormStatus.Lock

                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            Select Case enTabType
                                Case enPDPFormType.EmployeeDetail

                                Case enPDPFormType.PersonalAnalysisandGoals
                                    Select Case enActionType
                                        Case enAuditType.ADD
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 70, "Sorry, your personal development form is locked. To add anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.EDIT
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 71, "Sorry, your personal development form is locked. To edit anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.DELETE
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 74, "Sorry, your personal development form is locked. To delete anything, please request for unlocking."), Me)
                                            Return False
                                    End Select
                                Case enPDPFormType.DevelopmentActionPlan

                                    Select Case enActionType
                                        Case enAuditType.ADD
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 75, "Sorry, your personal development form is locked. To add anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.EDIT
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 76, "Sorry, your personal development form is locked. To edit anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.DELETE
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 77, "Sorry, your personal development form is locked. To delete anything, please request for unlocking."), Me)
                                            Return False
                                    End Select

                                Case enPDPFormType.SetCommentForDevelopmentActionPlan

                                Case enPDPFormType.SetUpdateProgressForDevelopmentActionPlan

                                Case enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan

                                Case enPDPFormType.AddTrainingForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, you cannot add training(s) to the action plan. This PDP form is already locked. Please unlock it to continue."), Me)
                                    Return False
                            End Select
                        Else
                            Select Case enTabType
                                Case enPDPFormType.EmployeeDetail

                                Case enPDPFormType.PersonalAnalysisandGoals
                                    Select Case enActionType
                                        Case enAuditType.ADD
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 64, "Sorry, your personal development form is locked. To add anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.EDIT
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 65, "Sorry, your personal development form is locked. To edit anything, please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.DELETE
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 66, "Sorry, your personal development form is locked. To delete anything, please request for unlocking."), Me)
                                            Return False
                                    End Select
                                Case enPDPFormType.DevelopmentActionPlan
                                    Select Case enActionType
                                        Case enAuditType.ADD
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, you cannot add development action plan. Your PDP form is locked. Please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.EDIT
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, you cannot edit development action plan. Your PDP form is locked. Please request for unlocking."), Me)
                                            Return False
                                        Case enAuditType.DELETE
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, you cannot delete development action plan. Your PDP form is locked. Please request for unlocking."), Me)
                                            Return False
                                    End Select

                                Case enPDPFormType.SetCommentForDevelopmentActionPlan

                                Case enPDPFormType.SetUpdateProgressForDevelopmentActionPlan

                                Case enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, you cannot delete this training. This PDP form is already locked. Please have unlock it to continue."), Me)
                                    Return False

                                Case enPDPFormType.AddTrainingForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, you cannot add training(s) to the action plan. Your PDP form is already locked. Please request for unlocking."), Me)
                                    Return False
                            End Select
                        End If

                        '-------------------------------------------------------------Unlock MODE---------------------------------------------------------------------


                    Case clspdpform_master.enPDPFormStatus.Unlock
                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            Select Case enTabType
                                Case enPDPFormType.EmployeeDetail

                                Case enPDPFormType.PersonalAnalysisandGoals

                                Case enPDPFormType.DevelopmentActionPlan
                                    

                                Case enPDPFormType.SetCommentForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 94, "Sorry, you cannot comment on this action plan now. This PDP form is unlocked. Please lock it to continue."), Me)
                                    Return False
                                Case enPDPFormType.SetUpdateProgressForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 98, "Sorry, you cannot update progress on this action plan now. This PDP form is unlocked. Please lock it to continue."), Me)
                                    Return False
                                Case enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan

                                Case enPDPFormType.AddTrainingForDevelopmentActionPlan
                            End Select
                        Else
                            Select Case enTabType
                                Case enPDPFormType.EmployeeDetail

                                Case enPDPFormType.PersonalAnalysisandGoals

                                Case enPDPFormType.DevelopmentActionPlan
                                    Select Case enActionType
                                        Case enAuditType.ADD

                                        Case enAuditType.EDIT

                                        Case enAuditType.DELETE

                                    End Select

                                Case enPDPFormType.SetCommentForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 95, "Sorry, you cannot comment on this action plan now. Your PDP form is unlocked. Please submit this form for review and locking."), Me)
                                    Return False
                                Case enPDPFormType.SetUpdateProgressForDevelopmentActionPlan
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 99, "Sorry, you cannot update progress on this action plan now. Your PDP form is unlocked. Please submit this form for review and locking."), Me)
                                    Return False
                                Case enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan

                                Case enPDPFormType.AddTrainingForDevelopmentActionPlan

                            End Select
                        End If
                End Select
            End If

            '-------------------------------------------------------------End---------------------------------------------------------------------


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function


    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objcommonmaster As New clsCommon_Master
        Dim objjob As New clsJobs
        Dim objAction As New clsAction_Reason
        Dim objemployee_transfer_tran As New clsemployee_transfer_tran
        Dim objemployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim objReportingToEmployee As New clsReportingToEmployee

        Dim cboSelectedValue As Integer = 0
        Try

            dsCombos = objjob.getComboList("Job", True)
            With cboNextJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Job")
                .SelectedValue = CStr(0)
                .DataBind()
            End With


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                Dim strFilter As String = " 1=1"
                If IsNothing(Session("E_Employeeunkid")) = False AndAlso CInt(Session("E_Employeeunkid")) > 0 Then
                    strFilter &= " and hremployee_master.employeeunkid <> " & CInt(Session("E_Employeeunkid")) & " "
                End If


                If mintTalentEmployeeUnkid > 0 Then
                    strFilter &= " and hremployee_master.employeeunkid = " & mintTalentEmployeeUnkid & " "
                ElseIf mintSuccessionEmployeeUnkid > 0 Then
                    strFilter &= " and hremployee_master.employeeunkid = " & mintSuccessionEmployeeUnkid & " "
                End If



                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                            True, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strFilter, False, True)


                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Emp")
                    .SelectedValue = CStr(0)
                    .DataBind()
                End With


                If mintTalentEmployeeUnkid > 0 AndAlso dsCombos.Tables("Emp").Rows.Count > 1 Then
                    cboEmployee.SelectedValue = mintTalentEmployeeUnkid
                    Call cboEmployee_SelectedIndexChanged(Nothing, Nothing)
                ElseIf mintSuccessionEmployeeUnkid > 0 AndAlso dsCombos.Tables("Emp").Rows.Count > 1 Then
                    cboEmployee.SelectedValue = mintSuccessionEmployeeUnkid
                    Call cboEmployee_SelectedIndexChanged(Nothing, Nothing)
                End If


                If mintDirectEmployeeUnkid > 0 Then
                    cboEmployee.SelectedValue = mintDirectEmployeeUnkid
                    Call cboEmployee_SelectedIndexChanged(Nothing, Nothing)
                End If


            Else
                Dim dsTransfer As New DataSet
                Dim objpdpsettings_master As New clspdpsettings_master
                'Dim strPeerVal As String
                'strPeerVal = objpdpsettings_master.GetSettingValueFromKey(clspdpsettings_master.enPDPConfiguration.PEERS)

                'Dim intBranch As Integer = 0
                'Dim intDepartment_group As Integer = 0
                'Dim intDepartment As Integer = 0
                'Dim intSection_group As Integer = 0
                'Dim intSection As Integer = 0
                'Dim intUnit_group As Integer = 0
                'Dim intUnit As Integer = 0
                'Dim intClass As Integer = 0
                'Dim intClass_group As Integer = 0
                'Dim intCost_center As Integer = 0
                'Dim intJob As Integer = 0
                'Dim intJob_group As Integer = 0
                'Dim intTeam As Integer = 0


                'If strPeerVal.Trim.Length > 0 Then

                '    Dim strFilter As String = ""
                '    Dim dsCurrentAllocation As DataSet = objemployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Employeeunkid"))
                '    Dim dsCurrentJob As DataSet = objemployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Employeeunkid"))

                '    Select Case CInt(strPeerVal)
                '        Case enAllocation.BRANCH
                '            intBranch = CInt(dsCurrentAllocation.Tables(0).Rows(0)("stationunkid"))
                '        Case enAllocation.DEPARTMENT_GROUP
                '            intDepartment_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("deptgroupunkid"))
                '            strFilter = " T.deptgroupunkid = " & intDepartment_group & " "
                '        Case enAllocation.DEPARTMENT
                '            intDepartment = CInt(dsCurrentAllocation.Tables(0).Rows(0)("departmentunkid"))
                '        Case enAllocation.SECTION_GROUP
                '            intSection_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("sectiongroupunkid"))
                '            strFilter = " T.sectiongroupunkid = " & intSection_group & " "
                '        Case enAllocation.SECTION
                '            intSection = CInt(dsCurrentAllocation.Tables(0).Rows(0)("sectionunkid"))
                '        Case enAllocation.UNIT_GROUP
                '            intUnit_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("unitgroupunkid"))
                '            strFilter = " T.unitgroupunkid = " & intUnit_group & " "
                '        Case enAllocation.UNIT
                '            intUnit = CInt(dsCurrentAllocation.Tables(0).Rows(0)("unitunkid"))
                '        Case enAllocation.TEAM
                '            intTeam = CInt(dsCurrentAllocation.Tables(0).Rows(0)("teamunkid"))
                '            strFilter = " T.teamunkid = " & intTeam & " "
                '        Case enAllocation.JOB_GROUP
                '            intJob_group = CInt(dsCurrentJob.Tables(0).Rows(0)("jobgroupunkid"))
                '            strFilter = "  J.jobgroupunkid = " & intJob_group & " "
                '        Case enAllocation.JOBS
                '            intJob = CInt(dsCurrentJob.Tables(0).Rows(0)("jobunkid"))
                '        Case enAllocation.CLASS_GROUP
                '            intClass_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("classgroupunkid"))
                '            strFilter = " T.classgroupunkid = " & intClass_group & " "
                '        Case enAllocation.CLASSES
                '            intClass = CInt(dsCurrentAllocation.Tables(0).Rows(0)("classunkid"))
                '        Case Else

                '    End Select


                '    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                            -1, _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            CStr(Session("UserAccessModeSetting")), True, _
                '                            False, "Employee", True, 0, intDepartment, intSection, intUnit, , , intClass, intCost_center, , intJob, , intBranch, , , strFilter, , False)

                'Else

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                            -1, _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            False, "Employee", True, Session("Employeeunkid"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", , False)

                'End If



                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Employee")
                    .SelectedValue = CStr(Session("Employeeunkid"))
                    .DataBind()
                End With
                Call cboEmployee_SelectedIndexChanged(Nothing, Nothing)
            End If


           


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objjob = Nothing
            objAction = Nothing
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using

    End Function

    Private Sub Fill_Info(ByVal intEmpId As Integer)
        Dim objCommon As New clsCommon_Master
        Dim objEmployee As New clsEmployee_Master
        Dim objpdpform_master As New clspdpform_master

        Dim objJob As New clsJobs
        Dim objDep As New clsDepartment
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objMaster As New clsMasterData
        Dim objEmpQualification As New clsEmp_Qualification_Tran
        Dim objShift As New clsNewshift_master
        Dim objPayPoint As New clspaypoint_master
        Dim objECategorize As New clsemployee_categorization_Tran
        Dim objpdpcategory_master As New clspdpcategory_master
        Dim objpdpaction_plan_category As New clspdpaction_plan_category

        Dim objpdpsettings_master As New clspdpsettings_master
        Dim objCycle As New clstlcycle_master
        Dim objsetting As New clstlsettings_master
        Dim objSucsetting As New clssucsettings_master
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim objsucpipeline_master As New clssucpipeline_master


        Dim dsData As New DataSet
        Dim dtData As New DataTable



        Try

            dsData = objpdpcategory_master.GetList("PersonalAnalysisCategorySetting")

            If IsNothing(dsData) OrElse dsData.Tables("PersonalAnalysisCategorySetting").Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, personal analysis category is not available. Please add from the settings to continue."), Me)
                Exit Sub
            End If

            dsData = objpdpaction_plan_category.GetList("ActionPlanListSetting")
            If IsNothing(dsData) OrElse dsData.Tables("ActionPlanListSetting").Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, action category is not available. Please add from the settings to continue."), Me)
                Exit Sub
            End If

            Dim pdpSetting As New Dictionary(Of clspdpsettings_master.enPDPConfiguration, String)
            pdpSetting = objpdpsettings_master.GetSetting()

            If IsNothing(pdpSetting) OrElse pdpSetting.Keys.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, PDP settings are not available. Please add from setting for continue."), Me)
                Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                pnlNoEmployeeSelected.Visible = False
                pnlData.Visible = True
                dsData = objECategorize.Get_Current_Job(Now.Date, CInt(cboEmployee.SelectedValue))

                If IsNothing(dsData) = False AndAlso dsData.Tables(0).Rows.Count > 0 Then
                    txtDateintoJobLevel.Text = dsData.Tables(0).Rows(0)("effectivedate")

                    If dsData.Tables(0).Rows(0)("jobgrp").ToString().Length > 0 Then
                        txtJobLevel.Text = dsData.Tables(0).Rows(0)("jobgrp")
                    Else
                        txtJobLevel.Text = "-"
                    End If

                End If

                objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
                objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = intEmpId

                objlblEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                objJob._Jobunkid = objEmployee._Jobunkid
                objlblJob.Text = objJob._Job_Name

                objDep._Departmentunkid = objEmployee._Departmentunkid
                objlblDepartment.Text = objDep._Name



                Dim dsEmpQualificatonList As DataSet = (New clsEmp_Qualification_Tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_qualification_tran.employeeunkid = " & CInt(intEmpId) & " ", False, False)
                Dim dtEmpQualificaton As DataTable
                dsEmpQualificatonList.Tables(0).DefaultView.Sort = "qlevel desc"
                dtEmpQualificaton = dsEmpQualificatonList.Tables(0).DefaultView.ToTable
                If dtEmpQualificaton.Rows.Count > 0 Then
                    txtQualification.Text = dtEmpQualificaton.Rows(0).Item("Qualify").ToString
                End If

                txtWorkingStation.Text = GetWorkStationInfo(CInt(Session("AllocationWorkStation")), objEmployee)

                txtCodeValue.Text = objEmployee._Employeecode
                objCommon._Masterunkid = objEmployee._Employmenttypeunkid
                Select Case objEmployee._Gender
                    Case 1  'MALE
                        txtGenderValue.Text = "Male"
                    Case 2  'FEMALE
                        txtGenderValue.Text = "Female"
                    Case Else
                        txtGenderValue.Text = ""
                End Select



                If objEmployee._blnImgInDb Then
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                        'Shani(20-Nov-2015) -- End

                        If objEmployee._Photo IsNot Nothing Then
                            imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                            'Shani(20-Nov-2015) -- End
                        Else
                            imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
                        End If
                    End If
                End If


                dtData = objEmployee.GetEmpCurrentAndPastExperience(Session("CompanyUnkId"), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")))

                If IsNothing(dtData) = False AndAlso dtData.Rows.Count > 0 Then
                    gvExperience.DataSource = dtData
                    gvExperience.DataBind()
                End If

                dsData = objpdpform_master.GetPastDevepmentTrainingList("PastDevepmentTrainingList", CInt(cboEmployee.SelectedValue))
                If IsNothing(dsData) = False AndAlso dsData.Tables(0).Rows.Count > 0 Then
                    gvEmpPastTrainings.DataSource = dsData.Tables("PastDevepmentTrainingList")
                    gvEmpPastTrainings.DataBind()
                End If



                'Dim objEIdTran As New clsIdentity_tran
                'objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date)
                Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))
                If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0).Rows(0)("employee").ToString().Trim.Length > 0 Then
                    txtLineManager.Text = dsList.Tables(0).Rows(0)("employee")
                    txtActionPlanCoach.Text = dsList.Tables(0).Rows(0)("employee")
                    mintActionPlanCoachId = CInt(dsList.Tables(0).Rows(0)("reporttoemployeeunkid"))


                    If CInt(Session("E_Employeeunkid")) = mintActionPlanCoachId Then
                    mblnIsLineManager = True
                Else
                        mblnIsLineManager = False
                    End If


                Else
                    txtLineManager.Text = "-"
                    txtActionPlanCoach.Text = "-"
                    mintActionPlanCoachId = -1
                    mblnIsLineManager = False
                End If
                FillPDPFormData()
                FillPersonalAnalysisItems()
                FillDevlopmentActionPlanItems()

            Else
                pnlNoEmployeeSelected.Visible = True
                pnlData.Visible = False
            End If

            Dim mintCycleId As Integer = -1
            mintCycleId = objCycle.getCurrentCycleId(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("Fin_year")), enStatusType.OPEN)
            objCycle._Cycleunkid = mintCycleId
            objtlpipeline_master._DateAsOn = CDate(objCycle._Start_Date)

            If mintCycleId > 0 Then
                If objsetting.isAllTalentSettingExist(mintCycleId) Then
                    Dim strFilter As String = String.Empty
                    strFilter = " tlscreening_process_master.isapproved = 1 "
                    dsData = objtlpipeline_master.GetTalentPoolDetailList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), mintCycleId, _
                                                        "", "EmpTalent", False, " hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & "  and " + strFilter, , True)


                    If IsNothing(dsData) = False AndAlso dsData.Tables("EmpTalent").Rows.Count > 0 Then
                        mblnIsAvailableInTalent = True
                        lblIsAvailableInTalent.Visible = True
                    Else
                        mblnIsAvailableInTalent = False
                        lblIsAvailableInTalent.Visible = False
                    End If
                End If
            End If



            If objSucsetting.isAllSuccessionSettingExist() Then
                objsucpipeline_master._DateAsOn = ConfigParameter._Object._CurrentDateAndTime
                Dim strFilter As String = String.Empty
                strFilter = " sucscreening_process_master.isapproved = 1 "

                dsData = objsucpipeline_master.GetSuccessionPoolDetailList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          False, -1, _
                                          "", "EmpSuccession", False, " hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " and " + strFilter, , True)

                If IsNothing(dsData) = False AndAlso dsData.Tables("EmpSuccession").Rows.Count > 0 Then
                    mblnIsAvailableInSuccession = True
                    lblIsAvailableInSuccession.Visible = True
                Else
                    mblnIsAvailableInSuccession = False
                    lblIsAvailableInSuccession.Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objJob = Nothing
            objDep = Nothing
            objCity = Nothing
            objState = Nothing
            objMaster = Nothing
            objEmpQualification = Nothing
            objECategorize = Nothing
            objEmployee = Nothing
            objCycle = Nothing
            objtlpipeline_master = Nothing
        End Try
    End Sub

    Private Function GetWorkStationInfo(ByVal intAllocationId As Integer, ByVal objEmp As clsEmployee_Master) As String
        Dim strWorkStation As String = String.Empty

        Dim objStation As New clsStation
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objDept As New clsDepartment
        Dim objSectionGroup As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGroup As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGroup As New clsClassGroup
        Dim objClass As New clsClass
        Dim objCostCenter As New clscostcenter_master

        Try
            Select Case intAllocationId
                Case enAllocation.BRANCH
                    objStation._Stationunkid = objEmp._Stationunkid
                    strWorkStation = objStation._Name
                Case enAllocation.DEPARTMENT_GROUP
                    objDeptGroup._Deptgroupunkid = objEmp._Deptgroupunkid
                    strWorkStation = objDeptGroup._Name
                Case enAllocation.DEPARTMENT
                    objDept._Departmentunkid = objEmp._Departmentunkid
                    strWorkStation = objDept._Name
                Case enAllocation.SECTION_GROUP
                    objSectionGroup._Sectiongroupunkid = objEmp._Sectiongroupunkid
                    strWorkStation = objSectionGroup._Name
                Case enAllocation.SECTION
                    objSection._Sectionunkid = objEmp._Sectionunkid
                    strWorkStation = objSection._Name
                Case enAllocation.UNIT_GROUP
                    objUnitGroup._Unitgroupunkid = objEmp._Unitgroupunkid
                    strWorkStation = objUnitGroup._Name
                Case enAllocation.UNIT
                    objUnit._Unitunkid = objEmp._Unitunkid
                    strWorkStation = objUnit._Name
                Case enAllocation.TEAM
                    objTeam._Teamunkid = objEmp._Teamunkid
                    strWorkStation = objTeam._Name
                Case enAllocation.JOB_GROUP
                    objJobGroup._Jobgroupunkid = objEmp._Jobgroupunkid
                    strWorkStation = objJobGroup._Name
                Case enAllocation.JOBS
                    objJob._Jobunkid = objEmp._Jobunkid
                    strWorkStation = objJob._Job_Name
                Case enAllocation.CLASS_GROUP
                    objClassGroup._Classgroupunkid = objEmp._Classgroupunkid
                    strWorkStation = objClassGroup._Name
                Case enAllocation.CLASSES
                    objClass._Classesunkid = objEmp._Classunkid
                    strWorkStation = objClass._Name
                Case enAllocation.COST_CENTER
                    objCostCenter._Costcenterunkid = objEmp._Costcenterunkid
                    strWorkStation = objCostCenter._Costcentername
                Case Else
                    objDept._Departmentunkid = objEmp._Departmentunkid
                    strWorkStation = objDept._Name
            End Select
            Return strWorkStation
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStation = Nothing
            objDeptGroup = Nothing
            objDept = Nothing
            objSectionGroup = Nothing
            objSection = Nothing
            objUnitGroup = Nothing
            objUnit = Nothing
            objTeam = Nothing
            objJobGroup = Nothing
            objJob = Nothing
            objClassGroup = Nothing
            objClass = Nothing
            objCostCenter = Nothing
        End Try
    End Function

    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer, ByVal xmstriEditingGUID As String, ByVal xHeaderName As String)
        Dim dtCItems As New DataTable
        Dim objpdpform_master As New clspdpform_master
        Dim objpdpitem_master As New clspdpitem_master
        Dim intPeriodid As Integer = -1
        Try
            intPeriodid = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))
            'dtCItems = objpdpform_master.Get_CItems_ForAddEdit(intPeriodid, xHeaderUnkid, xmstriEditingGUID)
            'If dtCItems IsNot Nothing AndAlso dtCItems.Rows.Count <= 0 Then Exit Sub

            'dgv_Citems.DataSource = dtCItems
            'dgv_Citems.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtCItems.Dispose()
            objpdpform_master = Nothing
        End Try
    End Sub

    Private Sub ClearPersolnalGoalPopupData()
        Try
            mintCategoryId = -1
            mintItemId = -1
            mintItemtypeid = -1
            mintItemTranId = -1

            mstrPersonalAnalysisEditGUID = ""

            pnlItemNameFreeText.Visible = False
            txtItemNameFreeText.Text = ""
            txtItemNameFreeText.Enabled = True

            pnlItemNameDtp.Visible = False
            dtpItemName.SetDate = Nothing

            pnlItemNameNum.Visible = False
            txtItemNameNUM.Text = 0

            pnlItemNameSelection.Visible = False
            cboItemNameSelection.SelectedValue = 0

            mintItemTranId = -1
            mblnIsAddItemData = False

            mblnPersonalAnalysisCompetencySelectionPopup = False
            mintPersonalAnalysisCompetencySelectedId = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetEvalutorReviewer(ByVal intActionPlanItemUnkId As Integer)
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objReportingToEmployee As New clsReportingToEmployee
        Dim objpdpsettings_master As New clspdpsettings_master
        Dim objemployee_transfer_tran As New clsemployee_transfer_tran
        Dim objEmployee As New clsEmployee_Master
        Dim objemployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim objpdpreviewer_master As New clspdpreviewer_master


        Try
            mintEvalutorTypeId = -1
            mintEvalutorUnkid = -1
            mintReviewerUnkid = -1
            objPdpgoals_master._Pdpgoalsmstunkid = intActionPlanItemUnkId
            If objPdpgoals_master._Goal_Evaluators.Length > 0 Then

                For Each Val As String In objPdpgoals_master._Goal_Evaluators.Split(",")
                    If CInt(Val) = CInt(clspdpsettings_master.enPDPEvalutorTypeId.SELF) Then
                        If CInt(Session("Employeeunkid")) = CInt(cboEmployee.SelectedValue) Then
                            mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.SELF)
                            mintEvalutorUnkid = CInt(Session("Employeeunkid"))
                            Exit For
                        End If

                    ElseIf CInt(Val) = CInt(clspdpsettings_master.enPDPEvalutorTypeId.LINE_MANAGER) Then
                        Dim dt As DataTable
                        objReportingToEmployee._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
                        Dim drrow As DataRow = objReportingToEmployee._RDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishierarchy") = True).FirstOrDefault
                        If IsNothing(drrow) = False Then
                            If CInt(Session("E_Employeeunkid")) = CInt(drrow("reporttoemployeeunkid")) Then
                                mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.LINE_MANAGER)
                                mintEvalutorUnkid = CInt(drrow("reporttoemployeeunkid"))
                                Exit For
                            End If
                        End If

                    ElseIf CInt(Val) = CInt(clspdpsettings_master.enPDPEvalutorTypeId.MENTOR) Then
                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            Exit For
                        End If

                        If objPdpgoals_master._MentorEmployeeunkid = CInt(Session("Employeeunkid")) Then
                            mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.MENTOR)
                            mintEvalutorUnkid = CInt(Session("Employeeunkid"))
                            Exit For
                        End If




                    ElseIf CInt(Val) = CInt(clspdpsettings_master.enPDPEvalutorTypeId.PEERS) Then

                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            Exit For
                        End If


                        Dim dsList As DataSet
                        Dim peerVal As String


                        Dim intBranch As Integer = 0
                        Dim intDepartment_group As Integer = 0
                        Dim intDepartment As Integer = 0
                        Dim intSection_group As Integer = 0
                        Dim intSection As Integer = 0
                        Dim intUnit_group As Integer = 0
                        Dim intUnit As Integer = 0
                        Dim intClass As Integer = 0
                        Dim intClass_group As Integer = 0
                        Dim intCost_center As Integer = 0
                        Dim intJob As Integer = 0
                        Dim intJob_group As Integer = 0
                        Dim intTeam As Integer = 0

                        peerVal = objpdpsettings_master.GetSettingValueFromKey(clspdpsettings_master.enPDPConfiguration.PEERS)


                        Dim strFilter As String = ""
                        Dim dsCurrentAllocation As DataSet = objemployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(cboEmployee.SelectedValue))
                        Dim dsCurrentJob As DataSet = objemployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(cboEmployee.SelectedValue))


                        If IsNothing(dsCurrentAllocation) = False AndAlso IsNothing(dsCurrentJob) = False Then


                            Select Case CInt(peerVal)
                                Case enAllocation.BRANCH
                                    intBranch = CInt(dsCurrentAllocation.Tables(0).Rows(0)("stationunkid"))
                                Case enAllocation.DEPARTMENT_GROUP
                                    intDepartment_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("deptgroupunkid"))
                                    strFilter = " AND T.deptgroupunkid = " & intDepartment_group & " "
                                Case enAllocation.DEPARTMENT
                                    intDepartment = CInt(dsCurrentAllocation.Tables(0).Rows(0)("departmentunkid"))
                                Case enAllocation.SECTION_GROUP
                                    intSection_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("sectiongroupunkid"))
                                    strFilter = " AND T.sectiongroupunkid = " & intSection_group & " "
                                Case enAllocation.SECTION
                                    intSection = CInt(dsCurrentAllocation.Tables(0).Rows(0)("sectionunkid"))
                                Case enAllocation.UNIT_GROUP
                                    intUnit_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("unitgroupunkid"))
                                    strFilter = " AND T.unitgroupunkid = " & intUnit_group & " "
                                Case enAllocation.UNIT
                                    intUnit = CInt(dsCurrentAllocation.Tables(0).Rows(0)("unitunkid"))
                                Case enAllocation.TEAM
                                    intTeam = CInt(dsCurrentAllocation.Tables(0).Rows(0)("teamunkid"))
                                    strFilter = " AND T.teamunkid = " & intTeam & " "
                                Case enAllocation.JOB_GROUP
                                    intJob_group = CInt(dsCurrentJob.Tables(0).Rows(0)("jobgroupunkid"))
                                    strFilter = " AND J.jobgroupunkid = " & intJob_group & " "
                                Case enAllocation.JOBS
                                    intJob = CInt(dsCurrentJob.Tables(0).Rows(0)("jobunkid"))
                                Case enAllocation.CLASS_GROUP
                                    intClass_group = CInt(dsCurrentAllocation.Tables(0).Rows(0)("classgroupunkid"))
                                    strFilter = " AND T.classgroupunkid = " & intClass_group & " "
                                Case enAllocation.CLASSES
                                    intClass = CInt(dsCurrentAllocation.Tables(0).Rows(0)("classunkid"))
                                Case Else

                            End Select


                            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    -1, _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    False, "Employee", True, Session("Employeeunkid"), intDepartment, intSection, intUnit, , , intClass, intCost_center, , intJob, , intBranch, , , strFilter, , False)


                            If dsList.Tables("Employee").Rows.Count > 0 Then
                                mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.PEERS)
                                Exit For
                            End If

                        End If
                    End If
                Next
            End If



            'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
            '    Dim dsReviewerList As DataSet = objpdpreviewer_master.GetList("ReviewerList", True, CInt(Session("UserId")))
            '    If IsNothing(dsReviewerList) = False AndAlso dsReviewerList.Tables("ReviewerList").Rows.Count > 0 Then
            '        mintReviewerUnkid = dsReviewerList.Tables("ReviewerList").Rows(0)("reviewermstunkid")
            '    End If
            'End If


            If mintEvalutorUnkid <= 0 Then
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) AndAlso CBool(Session("AllowToAddCommentForDevelopmentActionPlan")) Then
                    mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.OTHER)
                    mintEvalutorUnkid = CInt(Session("UserId"))
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
            objReportingToEmployee = Nothing
            objpdpsettings_master = Nothing
            objemployee_transfer_tran = Nothing
            objEmployee = Nothing
            objemployee_categorization_Tran = Nothing

        End Try
    End Sub

    'Action Plan
    Private Sub FillActionPlanPopupCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objcommonmaster As New clsCommon_Master
        Dim objMasterData As New clsMasterData
        Dim objPdpgoals_master As New clsPdpgoals_master


        Dim strFilter As String = String.Empty

        Try


            If IsNothing(Session("Employeeunkid")) = False AndAlso CInt(Session("Employeeunkid")) > 0 Then
                strFilter = "hremployee_master.employeeunkid <> " & CInt(Session("Employeeunkid")) & " "
            End If


            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, "", True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True, , , , , , , , , , , , , , , strFilter)
            With drpActionPlanMentor
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Employee")
                .DataBind()
                .SelectedValue = CStr(Session("EmpUnkid"))
            End With

            dsCombos = objMasterData.GetPDPActionPlanStatus(True, "ActionPlanStatus")
            With drpActionPlanStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("ActionPlanStatus")
                .DataBind()
                .SelectedValue = CStr("0")
            End With

            dsCombos = objPdpgoals_master.GetParentComboList("ParentGoal", mintFormunkidId, -1, mintActionPlanItemUnkId, mintActionPlanCategoryId, True, CInt(cboEmployee.SelectedValue), True)
            With drpActionPlanParentGoal
                .DataValueField = "pdpgoalsmstunkid"
                .DataTextField = "goal_name"
                .DataSource = dsCombos.Tables("ParentGoal")
                .DataBind()
                .SelectedValue = CStr("0")
            End With


            FillActionPlanTrainingList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillActionPlanTrainingList()
        Dim dsCombos As New DataSet
        Dim objcommonmaster As New clsCommon_Master
        Try


            If mintActionPlanItemUnkId > 0 Then
                dsCombos = objcommonmaster.GetList(CInt(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER), "TraininList", -1, True, " AND masterunkid not in (SELECT trainingcourseunkid from pdpgoals_trainingneed_tran WHERE isvoid= 0 and pdpgoalsmstunkid = " & mintActionPlanItemUnkId & " and employeeunkid = " & CInt(cboEmployee.SelectedValue) & ")")
            Else
                dsCombos = objcommonmaster.GetList(CInt(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER), "TraininList", -1, True)
            End If

            dgvActionPlanTrainingList.DataSource = dsCombos.Tables(0)
            dgvActionPlanTrainingList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub AddActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpdpaction_plan_category As New clspdpaction_plan_category
        Try
            If IsDataValid() = False Then Exit Sub
            If IsPDPDataValid(enPDPFormType.DevelopmentActionPlan, enAuditType.ADD) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = dlActionCategory.Items(item.ItemIndex)
            Dim hfactionplancategoryunkid As HiddenField = TryCast(row.FindControl("hfactionplancategoryunkid"), HiddenField)
            mintActionPlanCategoryId = hfactionplancategoryunkid.Value

            objpdpaction_plan_category._ActionPlanCategoryunkid = mintActionPlanCategoryId
            lblActionPlanAddEditCategory.Text = objpdpaction_plan_category._Category
            mblnIsAddItemData = False
            mblnDevelopmentActionPlanPopup = True
            FillActionPlanPopupCombo()
            popup_ActionPlanAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub FillEditActionPlanPopUpDetail()
        Dim dsListItem As New DataSet
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objpdpform_tran As New clspdpform_tran
        Try


            dsListItem = objPdpgoals_master.GetActionPlanListByCategory("ActionPlanItem", mintFormunkidId, mintActionPlanCategoryId, CInt(cboEmployee.SelectedValue), mintActionPlanItemUnkId)
            If IsPDPDataValid(enPDPFormType.DevelopmentActionPlan, enAuditType.EDIT) = False Then Exit Sub


            If IsNothing(dsListItem) = False AndAlso dsListItem.Tables("ActionPlanItem").Rows.Count > 0 Then

                txtActionPlanGoalDescription.Text = dsListItem.Tables("ActionPlanItem").Rows(0)("goal_description")
                If CInt(dsListItem.Tables("ActionPlanItem").Rows(0)("statusunkid")) > 0 Then
                    drpActionPlanStatus.SelectedValue = dsListItem.Tables("ActionPlanItem").Rows(0)("statusunkid")
                End If
                If CInt(dsListItem.Tables("ActionPlanItem").Rows(0)("mentoremployeeunkid")) > 0 Then
                    drpActionPlanMentor.SelectedValue = dsListItem.Tables("ActionPlanItem").Rows(0)("mentoremployeeunkid")
                End If



                If CInt(dsListItem.Tables("ActionPlanItem").Rows(0)("itemunkid")) > 0 Then
                    objpdpform_tran._Itemtranunkid = CInt(dsListItem.Tables("ActionPlanItem").Rows(0)("itemunkid"))
                    txtActionPlanGoalName.Text = objpdpform_tran._Fieldvalue
                    txtActionPlanGoalName.Enabled = False
                    mintActionPlanGoalSelectedId = CInt(dsListItem.Tables("ActionPlanItem").Rows(0)("itemunkid"))
                Else
                    txtActionPlanGoalName.Text = dsListItem.Tables("ActionPlanItem").Rows(0)("goal_name")
                    txtActionPlanGoalName.Enabled = True
                    mintActionPlanGoalSelectedId = -1
                End If



                If mintFormStatusUnkid = clspdpform_master.enPDPFormStatus.Lock Then
                    lnkExistingGoal.Visible = False
                    lnkExistingGoalReset.Visible = False
                    txtActionPlanGoalName.Enabled = False
                Else
                    lnkExistingGoal.Visible = True
                    lnkExistingGoalReset.Visible = True
                End If



                dtpActionPlanDueDate.SetDate = eZeeDate.convertDate(dsListItem.Tables("ActionPlanItem").Rows(0)("DueDate"))

                If dsListItem.Tables("ActionPlanItem").Rows(0)("goal_evaluators").ToString().Length > 0 Then
                    For Each strVal As Integer In dsListItem.Tables("ActionPlanItem").Rows(0)("goal_evaluators").ToString().Split(CChar(","))
                        If CInt(clspdpsettings_master.enPDPConfiguration.SELF) = strVal Then
                            chkActionPlanSelf.Checked = True
                        ElseIf CInt(clspdpsettings_master.enPDPConfiguration.LINE_MANAGER) = strVal Then
                            chkActionPlanLineManager.Checked = True
                        ElseIf CInt(clspdpsettings_master.enPDPConfiguration.PEERS) = strVal Then
                            chkActionPlanPeers.Checked = True
                        End If
                    Next
                End If
            End If
            Call FillActionPlanAssignedTrainingList()
            mblnDevelopmentActionPlanPopup = True
            popup_ActionPlanAddEdit.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillActionPlanAssignedTrainingList()
        Dim dsListItem As New DataSet
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran

        Try
            dsListItem = objPdpgoals_trainingneed_Tran.GetActionPlanTraining("TrainingList", mintActionPlanItemUnkId, CInt(cboEmployee.SelectedValue))
            If IsNothing(dsListItem) = False AndAlso dsListItem.Tables("TrainingList").Rows.Count > 0 Then
                gvActionPlanAssignedTraining.DataSource = dsListItem.Tables("TrainingList")
                gvActionPlanAssignedTraining.DataBind()
            Else
                gvActionPlanAssignedTraining.DataSource = Nothing
                gvActionPlanAssignedTraining.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_trainingneed_Tran = Nothing
        End Try
    End Sub

    Protected Sub EditActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsDataValid() = False Then Exit Sub
            If IsPDPDataValid(enPDPFormType.DevelopmentActionPlan, enAuditType.EDIT) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvActionPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfactionplancategoryunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfactionplancategoryunkid"), HiddenField)
            Dim hfpdpgoalsmstunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfpdpgoalsmstunkid"), HiddenField)

            mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
            mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)

            FillActionPlanPopupCombo()
            FillEditActionPlanPopUpDetail()
            

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub OnDeleteActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim blnUpdateProgressStarted As Boolean = False
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master
        Dim dsList As New DataSet

        Try
            If IsPDPDataValid(enPDPFormType.DevelopmentActionPlan, enAuditType.DELETE) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvActionPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfactionplancategoryunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfactionplancategoryunkid"), HiddenField)
            Dim hfpdpgoalsmstunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfpdpgoalsmstunkid"), HiddenField)

            mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
            mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)
            blnUpdateProgressStarted = objPdpgoals_master.IsChildUpdateProgressStarted(CInt(hfpdpgoalsmstunkid.Value))


            If blnUpdateProgressStarted Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot remove this action plan, Reason: progress update for the child action plan is already done. Please remove its child action plan first to continue."), Me)
            Else
                delReason.Title = "Please enter delete reason for deleting action plan"
                delReason.Show()
                mintDeleteActionId = CInt(enDeleteAction.DeleteActionPlan)
            End If

            dsList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(mintActionPlanItemUnkId, -1, CInt(enModuleReference.PDP), True, Nothing)
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 86, "Sorry, you cannot remove this action plan. Some training items are already linked to learning and development."), Me)
                Exit Sub
            End If
            dsList = Nothing

            dsList = objPdpgoals_master.GetActionPlanListByCategory("ActionPlaItem", mintFormunkidId, mintActionPlanCategoryId, CInt(cboEmployee.SelectedValue), mintActionPlanItemUnkId)
            If IsNothing(dsList) = False AndAlso dsList.Tables("ActionPlaItem").Rows.Count > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 87, "Sorry, you cannot remove this action plan. Please undo the progress update(s) done on this goal to continue."), Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function IsDataValid() As Boolean
        Try
            If CInt(cboNextJob.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Preferred Next Move is compulsory information. Please select the preferred next move from Employee Details tab."), Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function


    Private Function IsActionPlanPopUpDataValid() As Boolean
        Dim objPdpgoals_master As New clsPdpgoals_master
        Try
            If mintActionPlanGoalSelectedId <= 0 AndAlso txtActionPlanGoalName.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please add Area of Development or choose an option from the list to continue."), Me)
                Return False
            End If

            If dtpActionPlanDueDate.IsNull() Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 62, "Due Date is mandatory information. Please select due date to continue."), Me)
                Return False
            End If

            'If CInt(drpActionPlanMentor.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Coach/Mentor is mandatory information. Please Select Coach/Mentor to continue."), Me)
            '    Return False
            'End If

            'If CInt(drpActionPlanStatus.SelectedValue) <= 0 Then

            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Status is mandatory information. Please status to continue."), Me)
            '    Return False
            'End If

            If chkActionPlanLineManager.Checked = False AndAlso chkActionPlanSelf.Checked = False AndAlso chkActionPlanPeers.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please Select at least one evaluator to continue."), Me)
                Return False
            End If


            If mintActionPlanGoalSelectedId > 0 Then
                If objPdpgoals_master.isGoalExist(mintActionPlanGoalSelectedId, mintActionPlanItemUnkId, "") Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Action Plan for this goal is already defined. Please enter or select new goal to continue."), Me)
                    Return False
                End If
            Else
                If objPdpgoals_master.isGoalExist(-1, mintActionPlanItemUnkId, txtActionPlanGoalName.Text) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Action Plan for this goal is already defined. Please enter or select new goal to continue."), Me)
                    Return False
                End If
            End If


            If drpActionPlanParentGoal.SelectedValue > 0 AndAlso mintActionPlanItemUnkId > 0 Then


                If objPdpgoals_master.IsChildExist(mintActionPlanItemUnkId) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, the selected goal cannot be a parent goal because it has another parent goal. Please select new goal to continue."), Me)
                    drpActionPlanParentGoal.SelectedValue = "0"
                    Return False
                End If


            End If

            If drpActionPlanParentGoal.SelectedValue > 0 Then
                If objPdpgoals_master.IsParentExist(CInt(drpActionPlanParentGoal.SelectedValue)) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, the selected goal cannot be a parent goal because it's already a parent goal for some other goal."), Me)
                    drpActionPlanParentGoal.SelectedValue = "0"
                    Return False
                End If
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillDevlopmentActionPlanItems()
        Dim dsData As New DataSet
        Dim objpdpaction_plan_category As New clspdpaction_plan_category
        Try

            dsData = objpdpaction_plan_category.GetCategoryList("actionplan")
            dlActionCategory.DataSource = dsData.Tables(0)
            dlActionCategory.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpaction_plan_category = Nothing
        End Try
    End Sub

    Private Sub FillActionPlanSelectionList()
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objpdp_categoryitem_mapping As New clspdp_categoryitem_mapping
        Dim dsList As New DataSet
        Dim strMapping As String = ""
        Try

            dsList = objpdp_categoryitem_mapping.GetList("CategoryMapping")

            If dsList.Tables("CategoryMapping").Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you have not mapped Goals or Area of Developments. Please map from PDP Settings."), Me)
                Exit Sub
            End If

            dsList = objPdpgoals_master.GetActionPlanGoalSelectionList("ActionPlanGoalSelectionList", CInt(cboEmployee.SelectedValue))

            Dim dtTable As DataTable
            dtTable = dsList.Tables(0).Copy()
            Dim mintActionPlanCategoryGrpId As Integer = 0
            Dim mintActionPlanItemGrpId As Integer = 0



            Dim blnCategoryAddgroup As Boolean = False
            'Dim blnItemAddgroup As Boolean = False

            For Each drActionPlanList As DataRow In dsList.Tables(0).Rows

                If mintActionPlanCategoryGrpId <= 0 Then
                    mintActionPlanCategoryGrpId = CInt(drActionPlanList("categoryunkid").ToString())
                    blnCategoryAddgroup = True
                Else
                    If mintActionPlanCategoryGrpId = CInt(drActionPlanList("categoryunkid").ToString()) Then
                        blnCategoryAddgroup = False
                    Else
                        blnCategoryAddgroup = True
                        mintActionPlanCategoryGrpId = CInt(drActionPlanList("categoryunkid").ToString())
                    End If
                End If

                'If mintActionPlanItemGrpId <= 0 Then
                '    mintActionPlanItemGrpId = CInt(drActionPlanList("itemunkid").ToString())
                '    'blnItemAddgroup = True
                'Else
                '    If mintActionPlanItemGrpId = CInt(drActionPlanList("itemunkid").ToString()) Then
                '        blnItemAddgroup = False
                '    Else
                '        blnItemAddgroup = True
                '        mintActionPlanItemGrpId = CInt(drActionPlanList("itemunkid").ToString())
                '    End If
                'End If

                If blnCategoryAddgroup Then
                    Dim dRow As DataRow = Nothing
                    dRow = dtTable.NewRow()
                    dRow.Item("itemtranunkid") = -1
                    dRow.Item("fieldvalue") = drActionPlanList.Item("category")
                    dRow.Item("item") = ""
                    dRow.Item("category") = ""
                    dRow.Item("categoryunkid") = drActionPlanList.Item("categoryunkid")
                    dRow.Item("itemunkid") = -1
                    dRow.Item("isgrp") = 1
                    dtTable.Rows.Add(dRow)
                End If

                'If blnItemAddgroup Then
                '    Dim dRow As DataRow = Nothing
                '    dRow = dtTable.NewRow()
                '    dRow.Item("itemtranunkid") = -1
                '    dRow.Item("fieldvalue") = ""
                '    dRow.Item("item") = drActionPlanList.Item("item")
                '    dRow.Item("category") = ""
                '    dRow.Item("categoryunkid") = drActionPlanList.Item("categoryunkid")
                '    dRow.Item("itemunkid") = drActionPlanList.Item("itemunkid")
                '    dRow.Item("isgrp") = 2
                '    dtTable.Rows.Add(dRow)
                'End If
            Next

            dtTable = New DataView(dtTable, "", "categoryunkid,itemunkid,itemtranunkid", DataViewRowState.CurrentRows).ToTable.Copy

            gvActionPlanGoalSelection.DataSource = dtTable
            gvActionPlanGoalSelection.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveActionPlan()
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objemployee_transfer_tran As New clsemployee_transfer_tran
        Dim objemployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim blnFlag As Boolean = False
        Dim dsAllocationList As DataSet = Nothing
        Dim dsCategorizeList As DataSet = Nothing

        Try
            If mintActionPlanItemUnkId > 0 Then
                objPdpgoals_master._Pdpgoalsmstunkid = mintActionPlanItemUnkId
            Else
                objPdpgoals_master._Pdpgoalsmstunkid = -1
            End If

            objPdpgoals_master._Pdpformunkid = mintFormunkidId
            objPdpgoals_master._Actionplancategoryunkid = mintActionPlanCategoryId
            objPdpgoals_master._Goal_Description = txtActionPlanGoalDescription.Text
            objPdpgoals_master._Statusunkid = CInt(drpActionPlanStatus.SelectedValue)
            objPdpgoals_master._Duedate = dtpActionPlanDueDate.GetDateTime()
            objPdpgoals_master._Employeeunkid = CInt(cboEmployee.SelectedValue)

            If CInt(drpActionPlanMentor.SelectedValue) > 0 Then
                objPdpgoals_master._MentorEmployeeunkid = CInt(drpActionPlanMentor.SelectedValue)
            Else
                objPdpgoals_master._MentorEmployeeunkid = -1
            End If


            If mintActionPlanCoachId > 0 Then
                objPdpgoals_master._Coachunkid = mintActionPlanCoachId
            Else
                objPdpgoals_master._Coachunkid = -1
            End If

            objPdpgoals_master._Isvoid = 0

            'Gajanan [13-Apr-2021] -- Start

            objPdpgoals_master._TrainingAllocationid = CInt(Session("TrainingNeedAllocationID"))
            dsAllocationList = objemployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(cboEmployee.SelectedValue))

            dsCategorizeList = objemployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(cboEmployee.SelectedValue))
            Dim intAllocationDepartment As Integer = -1

            If IsNothing(dsAllocationList) = False AndAlso dsAllocationList.Tables(0).Rows.Count > 0 AndAlso _
                IsNothing(dsCategorizeList) = False AndAlso dsCategorizeList.Tables(0).Rows.Count > 0 Then
                Select Case CInt(Session("TrainingNeedAllocationID"))
                    Case enAllocation.BRANCH
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("stationunkid"))
                    Case enAllocation.DEPARTMENT_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("deptgroupunkid"))
                    Case enAllocation.DEPARTMENT
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("departmentunkid"))
                    Case enAllocation.SECTION_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("sectiongroupunkid"))
                    Case enAllocation.SECTION
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("sectionunkid"))
                    Case enAllocation.UNIT_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("unitgroupunkid"))
                    Case enAllocation.UNIT
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("unitunkid"))
                    Case enAllocation.TEAM
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("teamunkid"))
                    Case enAllocation.JOB_GROUP
                        intAllocationDepartment = CInt(dsCategorizeList.Tables(0).Rows(0)("jobgroupunkid"))
                    Case enAllocation.JOBS
                        intAllocationDepartment = CInt(dsCategorizeList.Tables(0).Rows(0)("jobunkid"))
                    Case enAllocation.CLASS_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("classgroupunkid"))
                    Case enAllocation.CLASSES
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("classunkid"))
                    Case Else
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("departmentunkid"))
                End Select
            End If
            objPdpgoals_master._TrainingDepartmentunkid = intAllocationDepartment

            If mblnIsAvailableInSuccession Then
                objPdpgoals_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Successor_PDP)
            ElseIf mblnIsAvailableInTalent Then
                objPdpgoals_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Talent_PDP)
            Else
                objPdpgoals_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Individual_Development_Plan)
            End If
            'Gajanan [13-Apr-2021] -- End

            If mintActionPlanGoalSelectedId > 0 Then
                Dim objpdpform_tran As New clspdpform_tran
                Dim dsPDPItem As DataSet
                dsPDPItem = objpdpform_tran.GetList("PDPItemData", True, mintActionPlanGoalSelectedId)

                'Gajanan [13-Apr-2021] -- Start
                If IsNothing(dsPDPItem) = Nothing AndAlso dsPDPItem.Tables("PDPItemData").Rows.Count > 0 Then
                    If CBool(dsPDPItem.Tables("PDPItemData").Rows(0)("iscompetencyselection")) Then
                        Dim objassess_competencies_master As New clsassess_competencies_master
                        objassess_competencies_master._Competenciesunkid = CInt(dsPDPItem.Tables("PDPItemData").Rows(0)("fieldvalue"))
                        objPdpgoals_master._CompetencyCategoryId = objassess_competencies_master._Competence_Categoryunkid
                    Else
                        objPdpgoals_master._CompetencyCategoryId = -1
                    End If
                    objpdpform_tran = Nothing
                End If
                'Gajanan [13-Apr-2021] -- End
                objPdpgoals_master._Itemunkid = mintActionPlanGoalSelectedId
                objPdpgoals_master._Goal_Name = ""
            Else
                objPdpgoals_master._Itemunkid = -1
                objPdpgoals_master._Goal_Name = txtActionPlanGoalName.Text
            End If

            If drpActionPlanParentGoal.SelectedValue.Trim.Length > 0 AndAlso CInt(drpActionPlanParentGoal.SelectedValue) > 0 Then
                objPdpgoals_master._Parentgoalunkid = CInt(drpActionPlanParentGoal.SelectedValue)
            Else
                objPdpgoals_master._Parentgoalunkid = -1
            End If

            Dim mstrEvalutor As String = ""
            If chkActionPlanSelf.Checked Then
                mstrEvalutor &= CInt(clspdpsettings_master.enPDPConfiguration.SELF)
            End If

            If chkActionPlanLineManager.Checked Then
                If mstrEvalutor.Length > 0 Then
                    mstrEvalutor &= "," & CInt(clspdpsettings_master.enPDPConfiguration.LINE_MANAGER)
                Else
                    mstrEvalutor &= CInt(clspdpsettings_master.enPDPConfiguration.LINE_MANAGER)
                End If
            End If

            If chkActionPlanPeers.Checked Then
                If mstrEvalutor.Length > 0 Then
                    mstrEvalutor &= "," & CInt(clspdpsettings_master.enPDPConfiguration.PEERS)
                Else
                    mstrEvalutor &= CInt(clspdpsettings_master.enPDPConfiguration.PEERS)
                End If
            End If
            objPdpgoals_master._Goal_Evaluators = mstrEvalutor

            Dim mstrSelectedActionPlanTrainingCourses As String = String.Empty
            mstrSelectedActionPlanTrainingCourses = String.Join(",", dgvActionPlanTrainingList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) CType(x.FindControl("hfmasterid"), HiddenField).Value + "|" + CType(x.FindControl("hfIsFromCompetency"), HiddenField).Value).ToArray())
            objPdpgoals_master._TrainingItems = mstrSelectedActionPlanTrainingCourses
            objPdpgoals_master._TrainingVoidReason = mstrActionPlanTrainingDeleteReason

            If mstrSelectedActionPlanTrainingCourses.Length > 0 Then
                If IsPDPDataValid(enPDPFormType.AddTrainingForDevelopmentActionPlan) = False Then Exit Sub
            End If

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objPdpgoals_master._AuditUserId = CInt(Session("UserId"))
                objPdpgoals_master._Userunkid = CInt(Session("UserId"))
            Else
                objPdpgoals_master._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If
            objPdpgoals_master._ClientIP = CStr(Session("IP_ADD"))
            objPdpgoals_master._FormName = mstrModuleName
            objPdpgoals_master._FromWeb = True
            objPdpgoals_master._HostName = CStr(Session("HOST_NAME"))
            objPdpgoals_master._DatabaseName = CStr(Session("Database_Name"))
            objPdpgoals_master._Isvoid = False
            objPdpgoals_master._AuditDate = ConfigParameter._Object._CurrentDateAndTime

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'If CBool(Session("AllowToLockPDPForm")) Then
                '    objPdpgoals_master._IsAddRecommendedTraining = True
                'Else
                '    objPdpgoals_master._IsAddRecommendedTraining = False
                'End If

                If mblnIsLineManager Then
                    objPdpgoals_master._IsAddRecommendedTraining = True
                Else
                    objPdpgoals_master._IsAddRecommendedTraining = False
                End If
            Else
                objPdpgoals_master._IsAddRecommendedTraining = False
            End If

            blnFlag = objPdpgoals_master.SaveEmployeeGoal()
            If blnFlag = False AndAlso objPdpgoals_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objPdpgoals_master._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Saved Successfully."), Me)
                FillDevlopmentActionPlanItems()
                mblnDevelopmentActionPlanPopup = False
                popup_ActionPlanAddEdit.Hide()

                ClearActionPlanPopupControlsAndValues()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
            objemployee_transfer_tran = Nothing
        End Try
    End Sub

    Protected Sub lnkRemoveAssignedTrainingItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsPDPDataValid(enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan) = False Then Exit Sub

            Dim row As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)
            Dim hftrainingneedtranunkid As HiddenField = CType(gvActionPlanAssignedTraining.Rows(row.RowIndex).FindControl("hftrainingneedtranunkid"), HiddenField)
            Dim hfpdpmstgoalunkid As HiddenField = CType(gvActionPlanAssignedTraining.Rows(row.RowIndex).FindControl("hfpdpmstgoalunkid"), HiddenField)
            Dim hftrainingcourseunkid As HiddenField = CType(gvActionPlanAssignedTraining.Rows(row.RowIndex).FindControl("hftrainingcourseunkid"), HiddenField)
            Dim hftrainingstatusunkid As HiddenField = CType(gvActionPlanAssignedTraining.Rows(row.RowIndex).FindControl("hftrainingstatusunkid"), HiddenField)

            If CInt(hftrainingstatusunkid.Value) = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                mintDeleteActionPlanTrainingcourseunkid = CInt(hftrainingneedtranunkid.Value)
                mintActionPlanItemUnkId = CInt(hfpdpmstgoalunkid.Value)
                mintTrainingCourseunkid = CInt(hftrainingcourseunkid.Value)

                delReason.Title = "Enter Reason For Removing This Training Course Item"
                mintDeleteActionId = CInt(enDeleteAction.DeleteActionPlanTrainingItem)
                delReason.Show()
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 72, "Sorry, you cannot remove this training. This training is already linked to Learning and Development."), Me)
                Exit Sub
            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub lnkRemoveTrainingFromViewTrainingListItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsPDPDataValid(enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan) = False Then Exit Sub

            Dim row As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)
            Dim hftrainingneedtranunkid As HiddenField = CType(dgvViewTrainingList.Rows(row.RowIndex).FindControl("hftrainingneedtranunkid"), HiddenField)
            Dim hfpdpmstgoalunkid As HiddenField = CType(dgvViewTrainingList.Rows(row.RowIndex).FindControl("hfpdpmstgoalunkid"), HiddenField)
            Dim hftrainingcourseunkid As HiddenField = CType(dgvViewTrainingList.Rows(row.RowIndex).FindControl("hftrainingcourseunkid"), HiddenField)
            Dim hftrainingstatusunkid As HiddenField = CType(dgvViewTrainingList.Rows(row.RowIndex).FindControl("hftrainingstatusunkid"), HiddenField)

            If CInt(hftrainingstatusunkid.Value) = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                mintDeleteActionPlanTrainingcourseunkid = CInt(hftrainingneedtranunkid.Value)
                mintActionPlanItemUnkId = CInt(hfpdpmstgoalunkid.Value)
                mintTrainingCourseunkid = CInt(hftrainingcourseunkid.Value)

                delReason.Title = "Enter Reason For Removing This Training Course Item"
                mintDeleteActionId = CInt(enDeleteAction.DeleteActionPlanTrainingItem)
                delReason.Show()
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 72, "You can not remove this training.Reason: This training is linked to Learning and Development."), Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Private Sub ClearActionPlanPopupControlsAndValues()
        Try
            ClearActionPlanIds()

            TabActionPlanName.Value = ""
            txtActionPlanGoalName.Text = ""
            txtActionPlanGoalName.Enabled = True
            txtActionPlanGoalDescription.Text = ""
            drpActionPlanParentGoal.SelectedValue = 0

            drpActionPlanParentGoal.SelectedValue = "0"
            drpActionPlanMentor.SelectedValue = "0"
            dtpActionPlanDueDate.SetDate = Nothing
            drpActionPlanStatus.SelectedValue = "0"
            chkActionPlanPeers.Checked = False

            dgvActionPlanTrainingList.DataSource = Nothing
            dgvActionPlanTrainingList.DataBind()

            gvActionPlanAssignedTraining.DataSource = Nothing
            gvActionPlanAssignedTraining.DataBind()

            mintActionPlanGoalSelectedId = -1
            mintDeleteActionPlanTrainingcourseunkid = -1
            lnkExistingGoal.Visible = True
            lnkExistingGoalReset.Visible = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearActionPlanIds()
        Try
            mintActionPlanCategoryId = -1
            mintActionPlanItemUnkId = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetActionplanfromSelection()
        Dim objpdpform_tran As New clspdpform_tran
        Try
            mintActionPlanGoalSelectedId = gvActionPlanGoalSelection.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("rdbActionPlanGoalSelection"), RadioButton).Checked = True).Select(Function(x) CInt(CType(x.FindControl("hfValue"), HiddenField).Value)).FirstOrDefault()
            Dim dsFieldvalue As DataSet = objpdpform_tran.GetActionPlanSelectionItemValue(mintActionPlanGoalSelectedId)
            If IsNothing(dsFieldvalue) = False AndAlso dsFieldvalue.Tables(0).Rows.Count > 0 Then
                txtActionPlanGoalName.Text = dsFieldvalue.Tables(0).Rows(0)("fieldvalue")
                txtActionPlanGoalName.Enabled = False

                If CBool(dsFieldvalue.Tables(0).Rows(0)("iscompetencyselection")) Then
                    Dim objCmptTraining As New clsassess_competencies_trainingcourses_tran
                    Dim mdtCourses As New DataTable
                    objCmptTraining._Competenciesunkid = CInt(dsFieldvalue.Tables(0).Rows(0)("competencyId"))
                    mdtCourses = objCmptTraining._DataTable

                    For Each drow As DataRow In mdtCourses.Rows
                        Dim gvrow As GridViewRow = dgvActionPlanTrainingList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("hfmasterid"), HiddenField).Value = drow("masterunkid").ToString()).FirstOrDefault()
                        Dim chkCompetency As CheckBox = CType(gvrow.FindControl("chkSelect"), CheckBox)
                        Dim hfIsFromCompetency As HiddenField = CType(gvrow.FindControl("hfIsFromCompetency"), HiddenField)
                        hfIsFromCompetency.Value = "1"
                        chkCompetency.Checked = True
                        chkCompetency.Enabled = False
                    Next
                End If
            End If

            mblnActionPlanSelectionPopup = False
            popup_ActionPlanGoalSelection.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpform_tran = Nothing
        End Try
    End Sub

    Private Sub ResetActionplanfromSelection()
        Try
            mintActionPlanGoalSelectedId = -1
            txtActionPlanGoalName.Text = ""
            txtActionPlanGoalName.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function FillActionPlanViewTrainingList() As Boolean
        Dim dsList As New DataSet
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran

        Try
            dsList = objPdpgoals_trainingneed_Tran.GetActionPlanTraining("ViewTrainingList", mintActionPlanItemUnkId, CInt(cboEmployee.SelectedValue), Nothing)
            If IsNothing(dsList) OrElse dsList.Tables("ViewTrainingList").Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 49, "Sorry, No Training or Activity has been defined."), Me)
                Return False
            Else
                dgvViewTrainingList.DataSource = dsList.Tables("ViewTrainingList")
                dgvViewTrainingList.DataBind()
                Return True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_trainingneed_Tran = Nothing
        End Try
    End Function

    'Update Progress
    Protected Sub SetUpdateProgressForActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)

        Try
            If IsPDPDataValid(enPDPFormType.SetUpdateProgressForDevelopmentActionPlan) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvActionPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfactionplancategoryunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfactionplancategoryunkid"), HiddenField)
            Dim hfpdpgoalsmstunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfpdpgoalsmstunkid"), HiddenField)
            Dim hfisCompleted As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfisCompleted"), HiddenField)

            mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
            mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)

            FillUpdateProgressInfo()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub ViewActionPlanTrainingList(ByVal sender As Object, ByVal e As EventArgs)
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
        Dim dsList As DataSet
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvActionPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfactionplancategoryunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfactionplancategoryunkid"), HiddenField)
            Dim hfpdpgoalsmstunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfpdpgoalsmstunkid"), HiddenField)

            mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
            mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)

            If FillActionPlanViewTrainingList() Then
                mblnViewTrainingPopup = True
                popup_ViewTrainingList.Show()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ClearUpdateProgressData()
        Try
            txtUpdateProgressLastUpdateScore.Text = "0"
            txtUpdateProgressLastUpdatedOn.Text = ""
            txtUpdateProgressScore.Text = "0"
            txtUpdateProgressRemarks.Text = ""
            mintActionPlanCategoryId = -1
            mintActionPlanItemUnkId = -1

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillUpdateProgressInfo()
        Dim dsList As DataSet
        Dim dsStatusList As DataSet
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objMasterData As New clsMasterData

        Try


            'If CBool(hfisCompleted.Value) = False Then
            dsList = objPdpgoals_master.GetActionPlanListByCategory("ActionPlaItem", mintFormunkidId, mintActionPlanCategoryId, CInt(cboEmployee.SelectedValue), mintActionPlanItemUnkId)
            dsStatusList = objMasterData.GetPDPActionPlanStatus(True, "ActionPlanStatus")
            With drpUpdateProgressStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsStatusList.Tables("ActionPlanStatus")
                .DataBind()
                .SelectedValue = CStr("0")
            End With



            If IsNothing(dsList) = False AndAlso dsList.Tables("ActionPlaItem").Rows.Count > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(dsList.Tables("ActionPlaItem").Rows(0)("employeeunkid").ToString())
                txtUpdateProgressGoalEmployee.Text = objEmp._Firstname + " " + objEmp._Surname

                txtUpdateProgressGoal.Text = dsList.Tables("ActionPlaItem").Rows(0)("goal_name")
                txtUpdateProgressGoalDescription.Text = dsList.Tables("ActionPlaItem").Rows(0)("goal_description")
                txtUpdateProgressLastUpdateScore.Text = dsList.Tables("ActionPlaItem").Rows(0)("point")
                If dsList.Tables("ActionPlaItem").Rows(0)("updatedatetime").ToString().Length > 0 Then
                    txtUpdateProgressLastUpdatedOn.Text = eZeeDate.convertDate(dsList.Tables("ActionPlaItem").Rows(0)("updatedatetime"))
                Else
                    txtUpdateProgressLastUpdatedOn.Text = "-"
                End If


                hfLastUpdateProgressId.Value = dsList.Tables("ActionPlaItem").Rows(0)("lastUpdatedProgressId").ToString()
                If CInt(dsList.Tables("ActionPlaItem").Rows(0)("lastUpdatedProgressId")) > 0 Then

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        btnRemoveLastUpdateProgress.Visible = CBool(Session("AllowToUndoUpdateProgressForDevelopmentActionPlan"))
                    Else
                        btnRemoveLastUpdateProgress.Visible = True
                    End If

                Else
                    btnRemoveLastUpdateProgress.Visible = False
                End If
                objEmp = Nothing
            End If

            mblnUpdateProgressPopup = True
            popup_UpdateProgress.Show()
            'Else
            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, Goal is already completed."), Me)
            'Exit Sub
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsUpdateProgressDataValid() As Boolean
        Try
            If txtUpdateProgressScore.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 84, "Completion percentage is mandatory information. Please add completion percentage to continue."), Me)
                Return False
            End If

            If CInt(txtUpdateProgressScore.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Completion percentage is mandatory information and should be greater than 0. Please add completion percentage to continue."), Me)
                Return False
            End If


            If CInt(txtUpdateProgressScore.Text) <= CInt(txtUpdateProgressLastUpdateScore.Text) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Completion percentage should be greater than the last progress update."), Me)
                Return False
            End If


            If txtUpdateProgressRemarks.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 85, "Progress Update Remarks is mandatory information. Please add progress remarks to continue."), Me)
                Return False
            End If

            If drpUpdateProgressStatus.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 50, "Please select status to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'Comments
    Private Sub FillCommentList()
        Dim objPdpevaluator_comments As New clsPdpevaluator_comments
        Try
            Dim strNoimage As String = ImageToBase64()
            Dim dsList As New DataSet
            dsList = objPdpevaluator_comments.GetActionPlanCommentData(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    "", True, False, strNoimage, mintActionPlanItemUnkId, "CommentList", , , , False)


            If IsNothing(dsList) OrElse dsList.Tables("CommentList").Rows.Count <= 0 Then
                pnlActionPlanNoComment.Visible = True
            Else
                pnlActionPlanNoComment.Visible = False
            End If

            dlActionPlanComments.DataSource = dsList
            dlActionPlanComments.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnSetCommentForActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim dsList As DataSet
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objMasterData As New clsMasterData
        Try
            If IsPDPDataValid(enPDPFormType.SetCommentForDevelopmentActionPlan) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvActionPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfactionplancategoryunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfactionplancategoryunkid"), HiddenField)
            Dim hfpdpgoalsmstunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfpdpgoalsmstunkid"), HiddenField)
            Dim hfisCompleted As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfisCompleted"), HiddenField)

            'Gajanan [15-Apr-2021] -- Start
            GetEvalutorReviewer(CInt(hfpdpgoalsmstunkid.Value))
            If mintEvalutorTypeId > 0 Then
                'If mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.PEERS) OrElse mintEvalutorTypeId = CInt(clspdpsettings_master.enPDPEvalutorTypeId.SELF) Then
                '    If CBool(hfisCompleted.Value) Then
                '        mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
                '        mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)
                '        FillCommentList()
                '        mblnActionPlanCommentPopup = True
                '        popup_ActionPlanComment.Show()
                '    Else
                '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Sorry, you can not comment on this goal,Reason: Goal is not complete."), Me)
                '        Exit Sub
                '    End If
                'Else
                '    mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
                '    mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)
                '    FillCommentList()
                '    mblnActionPlanCommentPopup = True
                '    popup_ActionPlanComment.Show()
                'End If

                mintActionPlanCategoryId = CInt(hfactionplancategoryunkid.Value)
                mintActionPlanItemUnkId = CInt(hfpdpgoalsmstunkid.Value)
                FillCommentList()
                dsList = objPdpgoals_master.GetActionPlanListByCategory("ActionPlaItem", mintFormunkidId, mintActionPlanCategoryId, CInt(cboEmployee.SelectedValue), mintActionPlanItemUnkId)
                If IsNothing(dsList) = False AndAlso dsList.Tables("ActionPlaItem").Rows.Count > 0 Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(dsList.Tables("ActionPlaItem").Rows(0)("employeeunkid").ToString())
                    txtActionPlanCommentEmployeeName.Text = objEmp._Firstname + " " + objEmp._Surname
                    txtActionPlanCommentGoalName.Text = dsList.Tables("ActionPlaItem").Rows(0)("goal_name")
                    txtActionPlanCommentGoalDescription.Text = dsList.Tables("ActionPlaItem").Rows(0)("goal_description")
                    txtActionPlanCommentStatus.Text = dsList.Tables("ActionPlaItem").Rows(0)("Status")
                    txtActionPlanCommentProgress.Style.Add("width", CDbl(dsList.Tables("ActionPlaItem").Rows(0)("point")).ToString() + "%")
                    txtActionPlanCommentProgressCount.Text = CDbl(dsList.Tables("ActionPlaItem").Rows(0)("point")).ToString() + "%"
                End If
                mblnActionPlanCommentPopup = True
                popup_ActionPlanComment.Show()
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 53, "Sorry, You don’t have a privilege to provide feedback for this employee."), Me)
                Exit Sub
            End If

            'Gajanan [15-Apr-2021] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnlnkEditActionPlanItemComment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            dlActionPlanComments.EditItemIndex = item.ItemIndex

            FillCommentList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnlnkDeleteActionPlanItemComment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hdf_ActionPlanCommenttranunkid As HiddenField = CType(dlActionPlanComments.Items(item.ItemIndex).FindControl("hdf_ActionPlanCommenttranunkid"), HiddenField)

            mintActionPlanCommentItemunkid = hdf_ActionPlanCommenttranunkid.Value

            delReason.Title = "Please enter delete reason for deleting comment"
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeleteActionPlanComment)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnlnkCancleCommentItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            dlActionPlanComments.EditItemIndex = -1
            ClearActionPlanCommentPopupControls()
            FillCommentList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnlnkSaveEditCommentItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objPdpevaluator_comments As New clsPdpevaluator_comments

        Try

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim itxtActionPlanComment As TextBox = CType(dlActionPlanComments.Items(item.ItemIndex).FindControl("txtActionPlanComment"), TextBox)
            Dim hdf_ActionPlanCommenttranunkid As HiddenField = CType(dlActionPlanComments.Items(item.ItemIndex).FindControl("hdf_ActionPlanCommenttranunkid"), HiddenField)

            objPdpevaluator_comments._Commenttranunkid = CInt(hdf_ActionPlanCommenttranunkid.Value)
            objPdpevaluator_comments._Comments = itxtActionPlanComment.Text
            SaveActionPlanCommentItem(objPdpevaluator_comments)
            ClearActionPlanCommentPopupControls()
            dlActionPlanComments.EditItemIndex = -1
            FillCommentList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpevaluator_comments = Nothing
        End Try
    End Sub

    Private Sub ClearActionPlanCommentPopupData()
        Try
            mintActionPlanItemUnkId = -1
            mintEvalutorUnkid = -1
            mintEvalutorTypeId = -1
            mintReviewerUnkid = -1
            mintActionPlanCommentItemunkid = -1
            mblnIsAttachmentDownloadMode = False


            If IsNothing(mdtCommentAttachment) = False Then
            mdtCommentAttachment.Rows.Clear()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearActionPlanCommentPopupControls()
        Try
            txtActionPlanComment.Text = ""
            mintEvalutorUnkid = -1
            mintEvalutorTypeId = -1
            mintReviewerUnkid = -1
            mintActionPlanCommentItemunkid = -1
            mdtCommentAttachment.Rows.Clear()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Attachment
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtCommentAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtCommentAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Discipline_Module
                dRow("scanattachrefid") = enScanAttactRefId.PDP
                dRow("transactionunkid") = mintActionPlanCommentItemunkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = Date.Today()
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                dRow("filesize_kb") = f.Length / 1024
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END
            Else

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Selected information is already present for this particular employee."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Selected information is already present for this particular employee."), Me)
                'Gajanan [10-June-2019] -- End
                Exit Sub
            End If
            mdtCommentAttachment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtCommentAttachment Is Nothing Then Exit Sub

            dtView = New DataView(mdtCommentAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            gvActionPlanCommentAttachment.AutoGenerateColumns = False
            gvActionPlanCommentAttachment.DataSource = dtView
            gvActionPlanCommentAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachmentCombo()
        Dim objCommon As New clsCommon_Master
        Dim dsList As DataSet
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnlnkDownloadActionPlanCommentAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim strMsg As String = String.Empty
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hdf_ActionPlanCommenttranunkid As HiddenField = CType(dlActionPlanComments.Items(item.ItemIndex).FindControl("hdf_ActionPlanCommenttranunkid"), HiddenField)
            mintActionPlanCommentItemunkid = hdf_ActionPlanCommenttranunkid.Value
            mdtCommentAttachment = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.PDP, mintActionPlanCommentItemunkid, CStr(Session("Document_Path")))

            pnl_ImageAdd.Visible = False
            pnlAttachmentType.Visible = False

            mblnIsAttachmentDownloadMode = True
            gvActionPlanCommentAttachment.DataSource = mdtCommentAttachment
            gvActionPlanCommentAttachment.DataBind()

            mblnCommentAttachmentPopup = True
            popupCommentAttachment.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnlnkAddEditActionPlanCommentAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim strMsg As String = String.Empty
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hdf_ActionPlanCommenttranunkid As HiddenField = CType(dlActionPlanComments.Items(item.ItemIndex).FindControl("hdf_ActionPlanCommenttranunkid"), HiddenField)
            mintActionPlanCommentItemunkid = hdf_ActionPlanCommenttranunkid.Value
            mdtCommentAttachment = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.PDP, mintActionPlanCommentItemunkid, CStr(Session("Document_Path")))

            pnlAttachmentType.Visible = True
            pnl_ImageAdd.Visible = True


            FillAttachmentCombo()
            btnSaveActionPlanCommentAttachmentPopup.Visible = True
            mblnIsAttachmentDownloadMode = False
            gvActionPlanCommentAttachment.DataSource = mdtCommentAttachment
            gvActionPlanCommentAttachment.DataBind()

            mblnCommentAttachmentPopup = True
            popupCommentAttachment.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If gvActionPlanCommentAttachment.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "No Files to download."), Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtCommentAttachment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub OnDeleteCommentAttachment(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim grow As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)

            Dim scanattachtranunkid As Integer = CInt(gvActionPlanCommentAttachment.DataKeys(grow.RowIndex)("scanattachtranunkid"))
            mintDeleteAttachmentId = scanattachtranunkid

            cfDelete.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Personal Analysis Custom Item Competency Selection
    Private Sub FillCompetencySelectionList()
        Dim objCompetency As New clsassess_competencies_master
        Dim intPeriod As Integer = -1
        Dim objpdpitem_master As New clspdpitem_master
        Try

            intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))
            Dim dsList As New DataSet
            dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), intPeriod, _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")))


            gvCompetencySelection.DataSource = dsList.Tables(0)
            gvCompetencySelection.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCompetency = Nothing
            objpdpitem_master = Nothing
        End Try
    End Sub


    Private Sub FillReviewTrainingList()
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
        Dim dsList As New DataSet

        Try

            dsList = objPdpgoals_trainingneed_Tran.GetRecommendedTraining("RecommendedTraining", mintFormunkidId)


            Dim dtTable As DataTable
            dtTable = dsList.Tables(0).Copy()
            Dim mintGoalGrpId As Integer = 0
            Dim blAddgroup As Boolean = False


            For Each drReviewTrainingList As DataRow In dsList.Tables(0).Rows
                If mintGoalGrpId <= 0 Then
                    mintGoalGrpId = CInt(drReviewTrainingList("pdpgoalsmstunkid").ToString())
                    blAddgroup = True
                Else
                    If mintGoalGrpId = CInt(drReviewTrainingList("pdpgoalsmstunkid").ToString()) Then
                        blAddgroup = False
                    Else
                        blAddgroup = True
                        mintGoalGrpId = CInt(drReviewTrainingList("pdpgoalsmstunkid").ToString())
                    End If
                End If



                If blAddgroup Then
                    Dim dRow As DataRow = Nothing
                    dRow = dtTable.NewRow()
                    dRow.Item("trainingneedtranunkid") = -1
                    dRow.Item("name") = drReviewTrainingList("goal_name")
                    dRow.Item("isgrp") = 1
                    dRow.Item("pdpgoalsmstunkid") = drReviewTrainingList("pdpgoalsmstunkid")
                    dtTable.Rows.Add(dRow)
                End If

                'If blnItemAddgroup Then
                '    Dim dRow As DataRow = Nothing
                '    dRow = dtTable.NewRow()
                '    dRow.Item("itemtranunkid") = -1
                '    dRow.Item("fieldvalue") = ""
                '    dRow.Item("item") = drActionPlanList.Item("item")
                '    dRow.Item("category") = ""
                '    dRow.Item("categoryunkid") = drActionPlanList.Item("categoryunkid")
                '    dRow.Item("itemunkid") = drActionPlanList.Item("itemunkid")
                '    dRow.Item("isgrp") = 2
                '    dtTable.Rows.Add(dRow)
                'End If
            Next

            dtTable = New DataView(dtTable, "", "pdpgoalsmstunkid,trainingneedtranunkid", DataViewRowState.CurrentRows).ToTable.Copy



            dgvReviewTraining.DataSource = dtTable
            dgvReviewTraining.DataBind()

            If dtTable.Rows.Count <= 0 Then
                pnlNoReviewTraining.Visible = True
            Else
                pnlNoReviewTraining.Visible = False
            End If



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Form's Event(s) "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 4 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Me.ViewState.Add("IsDirect", True)
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    If CBool(arr(2)) Then
                        HttpContext.Current.Session("UserId") = CInt(arr(1))
                    Else
                        HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
                    End If
                    mintDirectEmployeeUnkid = CInt(arr(3))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                    Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                    Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
                    Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                    Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
                    Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
                    Session("CascadingTypeId") = clsConfig._CascadingTypeId
                    Session("ScoringOptionId") = clsConfig._ScoringOptionId
                    Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
                    Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()

                    If CBool(arr(2)) Then
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                    Else
                        Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""
                        Dim objEmp As New clsEmployee_Master
                        objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
                        xUName = objEmp._Displayname : xPasswd = objEmp._Password
                        HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                        objEmp = Nothing

                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        HttpContext.Current.Session("DisplayName") = xUName

                        HttpContext.Current.Session("UserName") = xUName
                        HttpContext.Current.Session("MemberName") = xUName
                        HttpContext.Current.Session("LangId") = 1
                    End If

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    Call FillCombo()


                    'Call SetControlCaptions()
                    'Call SetMessages()
                    'Call Language._Object.SaveValue()
                    'Call SetLanguage()

                    HttpContext.Current.Session("Login") = True
                    GoTo Link
                End If
            End If



            If CInt(Session("Employeeunkid")) > 0 Then
                mintattachempid = CInt(Session("Employeeunkid"))
            ElseIf CInt(Session("UserId")) > 0 Then
                mintattachempid = 0
            End If

            If (Page.IsPostBack = False) Then
                GC.Collect()

                If IsNothing(Session("TalentEmployeeId")) = False Then
                    mintTalentEmployeeUnkid = CInt(Session("TalentEmployeeId"))
                    Session("TalentEmployeeId") = Nothing
                End If

                If IsNothing(Session("SuccessionEmployeeId")) = False Then
                    mintSuccessionEmployeeUnkid = CInt(Session("SuccessionEmployeeId"))
                    Session("SuccessionEmployeeId") = Nothing
                End If


                Call FillCombo()

                imgEmp.Visible = CBool(Session("IsImgInDataBase"))
                'Call Fill_Info(CInt(Session("Employeeunkid")))
                'Call cboEmployee_SelectedIndexChanged(sender, e)

                mdtCommentAttachment = objDocument.GetQulificationAttachment(mintattachempid, enScanAttactRefId.PDP, mintActionPlanCommentItemunkid, CStr(Session("Document_Path")))
                mdtCommentAttachment.Rows.Clear()
                FillAttachment()
            Else

                If Me.ViewState("mblnPersonalAnalysisandGoalsTablePopup") IsNot Nothing Then
                    mblnPersonalAnalysisandGoalsTablePopup = Me.ViewState("mblnPersonalAnalysisandGoalsTablePopup")
                    If mblnPersonalAnalysisandGoalsTablePopup Then
                        popup_CItemAddEdit.Show()
                    End If
                End If

                If Me.ViewState("mblnPersonalAnalysisandGoalsListPopup") IsNot Nothing Then
                    mblnPersonalAnalysisandGoalsListPopup = Me.ViewState("mblnPersonalAnalysisandGoalsListPopup")
                    If mblnPersonalAnalysisandGoalsListPopup Then
                        popup_ListCItemAddEdit.Show()
                    End If
                End If

                If Me.ViewState("mblnDevelopmentActionPlanPopup") IsNot Nothing Then
                    mblnDevelopmentActionPlanPopup = Me.ViewState("mblnDevelopmentActionPlanPopup")
                    If mblnDevelopmentActionPlanPopup Then
                        popup_ActionPlanAddEdit.Show()
                    End If
                End If

                If Me.ViewState("mblnActionPlanSelectionPopup") IsNot Nothing Then
                    mblnActionPlanSelectionPopup = Me.ViewState("mblnActionPlanSelectionPopup")
                    If mblnActionPlanSelectionPopup Then
                        popup_ActionPlanGoalSelection.Show()
                    End If
                End If

                If Me.ViewState("mblnUpdateProgressPopup") IsNot Nothing Then
                    mblnUpdateProgressPopup = CBool(Me.ViewState("mblnUpdateProgressPopup"))
                    If mblnUpdateProgressPopup Then
                        popup_UpdateProgress.Show()
                    End If
                End If

                If Me.ViewState("mblnViewTrainingPopup") IsNot Nothing Then
                    mblnViewTrainingPopup = CBool(Me.ViewState("mblnViewTrainingPopup"))
                    If mblnViewTrainingPopup Then
                        popup_ViewTrainingList.Show()
                    End If
                End If

                If Me.ViewState("mblnActionPlanCommentPopup") IsNot Nothing Then
                    mblnActionPlanCommentPopup = CBool(Me.ViewState("mblnActionPlanCommentPopup"))
                    If mblnActionPlanCommentPopup Then
                        popup_ActionPlanComment.Show()
                    End If
                End If


                If Me.ViewState("mstrPersonalAnalysisEditGUID") IsNot Nothing Then
                    mstrPersonalAnalysisEditGUID = Me.ViewState("mstrPersonalAnalysisEditGUID")
                End If

                If Me.ViewState("mintFormunkidId") IsNot Nothing Then
                    mintFormunkidId = CInt(Me.ViewState("mintFormunkidId"))
                End If

                If Me.ViewState("mintFormStatusUnkid") IsNot Nothing Then
                    mintFormStatusUnkid = CInt(Me.ViewState("mintFormStatusUnkid"))
                End If

                If Me.ViewState("mintCategoryId") IsNot Nothing Then
                    mintCategoryId = CInt(ViewState("mintCategoryId"))
                End If

                If Me.ViewState("mintItemId") IsNot Nothing Then
                    mintItemId = CInt(ViewState("mintItemId"))
                End If

                If Me.ViewState("mintItemTranId") IsNot Nothing Then
                    mintItemTranId = CInt(ViewState("mintItemTranId"))
                End If


                If Me.ViewState("mintActionPlanGoalSelectedId") IsNot Nothing Then
                    mintActionPlanGoalSelectedId = Me.ViewState("mintActionPlanGoalSelectedId")
                End If

                If Me.ViewState("mintActionPlanCategoryId") IsNot Nothing Then
                    mintActionPlanCategoryId = Me.ViewState("mintActionPlanCategoryId")
                End If

                If Me.ViewState("mintActionPlanItemUnkId") IsNot Nothing Then
                    mintActionPlanItemUnkId = CInt(Me.ViewState("mintActionPlanItemUnkId"))
                End If

                If Me.ViewState("mintTrainingCourseunkid") IsNot Nothing Then
                    mintTrainingCourseunkid = CInt(Me.ViewState("mintTrainingCourseunkid"))
                End If


                If Me.ViewState("mstrActionPlanTrainingDeleteReason") IsNot Nothing Then
                    mstrActionPlanTrainingDeleteReason = CStr(Me.ViewState("mstrActionPlanTrainingDeleteReason"))
                End If

                If Me.ViewState("mintDeleteActionId") IsNot Nothing Then
                    mintDeleteActionId = CInt(Me.ViewState("mintDeleteActionId"))
                End If

                If Me.ViewState("mintConfirmationActionId") IsNot Nothing Then
                    mintConfirmationActionId = CInt(Me.ViewState("mintConfirmationActionId"))
                End If

                If Me.ViewState("mintDeleteActionPlanTrainingcourseunkid") IsNot Nothing Then
                    mintDeleteActionPlanTrainingcourseunkid = CInt(Me.ViewState("mintDeleteActionPlanTrainingcourseunkid"))
                End If


                If Me.ViewState("mintActionPlanCommentItemunkid") IsNot Nothing Then
                    mintActionPlanCommentItemunkid = CInt(Me.ViewState("mintActionPlanCommentItemunkid"))
                End If


                If Me.ViewState("mdtCommentAttachment") IsNot Nothing Then
                    mdtCommentAttachment = CType(Me.ViewState("mdtCommentAttachment"), DataTable)
                End If

                If Me.ViewState("mblnCommentAttachmentPopup") IsNot Nothing Then
                    mblnCommentAttachmentPopup = CBool(Me.ViewState("mblnCommentAttachmentPopup"))
                    If mblnCommentAttachmentPopup Then
                        popupCommentAttachment.Show()
                    End If
                End If

                If Me.ViewState("mblnIsAttachmentDownloadMode") IsNot Nothing Then
                    mblnIsAttachmentDownloadMode = CBool(Me.ViewState("mblnIsAttachmentDownloadMode"))
                End If


                If Me.ViewState("mintDeleteAttachmentId") IsNot Nothing Then
                    mintDeleteAttachmentId = CInt(Me.ViewState("mintDeleteAttachmentId"))
                End If

                If Me.ViewState("mblnIsAddItemData") IsNot Nothing Then
                    mblnIsAddItemData = CBool(Me.ViewState("mblnIsAddItemData"))
                End If

                If Me.ViewState("mintItemtypeid") IsNot Nothing Then
                    mintItemtypeid = CInt(Me.ViewState("mintItemtypeid"))
                End If

                If Me.ViewState("mintActionPlanCoachId") IsNot Nothing Then
                    mintActionPlanCoachId = CInt(Me.ViewState("mintActionPlanCoachId"))
                End If

                If Me.ViewState("mintPersonalAnalysisCompetencySelectedId") IsNot Nothing Then
                    mintPersonalAnalysisCompetencySelectedId = CInt(Me.ViewState("mintPersonalAnalysisCompetencySelectedId"))
                End If

                If Me.ViewState("mblnPersonalAnalysisCompetencySelectionPopup") IsNot Nothing Then
                    mblnPersonalAnalysisCompetencySelectionPopup = CBool(Me.ViewState("mblnPersonalAnalysisCompetencySelectionPopup"))
                    If mblnPersonalAnalysisCompetencySelectionPopup Then
                        popupCompetencySelection.Show()
                    End If
                End If

                If Me.ViewState("mblnCompetencySelectionFromList") IsNot Nothing Then
                    mblnCompetencySelectionFromList = CBool(Me.ViewState("mblnCompetencySelectionFromList"))
                End If

                If Me.ViewState("mintListPersonalAnalysisSelectionDataListItemIndex") IsNot Nothing Then
                    mintListPersonalAnalysisSelectionDataListItemIndex = Me.ViewState("mintListPersonalAnalysisSelectionDataListItemIndex")
                End If

                If Me.ViewState("mintTalentEmployeeUnkid") IsNot Nothing Then
                    mintTalentEmployeeUnkid = CInt(ViewState("mintTalentEmployeeUnkid"))
                End If

                If Me.ViewState("mintSuccessionEmployeeUnkid") IsNot Nothing Then
                    mintSuccessionEmployeeUnkid = CInt(ViewState("mintSuccessionEmployeeUnkid"))
                End If

                If Me.ViewState("mblnIsAvailableInTalent") IsNot Nothing Then
                    mblnIsAvailableInTalent = CBool(ViewState("mblnIsAvailableInTalent"))
                End If

                If Me.ViewState("mblnIsAvailableInSuccession") IsNot Nothing Then
                    mblnIsAvailableInSuccession = CBool(ViewState("mblnIsAvailableInSuccession"))
                End If

                If Me.ViewState("mblnReviewTrainingPopup") IsNot Nothing Then
                    mblnReviewTrainingPopup = CBool(Me.ViewState("mblnReviewTrainingPopup"))
                    If mblnReviewTrainingPopup Then
                        popupReviewTraining.Show()
                    End If
                End If

                If Me.ViewState("mblnIsLineManager") IsNot Nothing Then
                    mblnIsLineManager = CBool(ViewState("mblnIsLineManager"))
                End If

                FillPersonalAnalysisItems()
            End If


            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

Link:

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'If IsPostBack = True And Me.ViewState("OldImage") <> "" Then
        '    Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("OldImage")))
        'End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'Me.IsLoginRequired = True
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mblnPersonalAnalysisandGoalsTablePopup") = mblnPersonalAnalysisandGoalsTablePopup
            Me.ViewState("mblnPersonalAnalysisandGoalsListPopup") = mblnPersonalAnalysisandGoalsListPopup

            Me.ViewState("mblnDevelopmentActionPlanPopup") = mblnDevelopmentActionPlanPopup
            Me.ViewState("mblnActionPlanSelectionPopup") = mblnActionPlanSelectionPopup
            Me.ViewState("mstrPersonalAnalysisEditGUID") = mstrPersonalAnalysisEditGUID
            Me.ViewState("mintCategoryId") = mintCategoryId
            Me.ViewState("mintItemId") = mintItemId
            Me.ViewState("mintFormunkidId") = mintFormunkidId
            Me.ViewState("mintActionPlanGoalSelectedId") = mintActionPlanGoalSelectedId
            Me.ViewState("mintActionPlanCategoryId") = mintActionPlanCategoryId
            Me.ViewState("mintActionPlanItemUnkId") = mintActionPlanItemUnkId
            Me.ViewState("mintTrainingCourseunkid") = mintTrainingCourseunkid

            Me.ViewState("mstrActionPlanTrainingDeleteReason") = mstrActionPlanTrainingDeleteReason
            Me.ViewState("mintDeleteActionId") = mintDeleteActionId
            Me.ViewState("mintConfirmationActionId") = mintConfirmationActionId

            Me.ViewState("mblnUpdateProgressPopup") = mblnUpdateProgressPopup
            Me.ViewState("mblnViewTrainingPopup") = mblnViewTrainingPopup
            Me.ViewState("mintDeleteActionPlanTrainingcourseunkid") = mintDeleteActionPlanTrainingcourseunkid
            Me.ViewState("mblnActionPlanCommentPopup") = mblnActionPlanCommentPopup
            Me.ViewState("mintActionPlanCommentItemunkid") = mintActionPlanCommentItemunkid
            Me.ViewState("mdtCommentAttachment") = mdtCommentAttachment

            Me.ViewState("mblnCommentAttachmentPopup") = mblnCommentAttachmentPopup
            Me.ViewState("mblnIsAttachmentDownloadMode") = mblnIsAttachmentDownloadMode
            Me.ViewState("mintDeleteAttachmentId") = mintDeleteAttachmentId
            Me.ViewState("mblnIsAddItemData") = mblnIsAddItemData
            Me.ViewState("mintItemtypeid") = mintItemtypeid
            Me.ViewState("mintItemTranId") = mintItemTranId
            Me.ViewState("mintActionPlanCoachId") = mintActionPlanCoachId
            Me.ViewState("mintPersonalAnalysisCompetencySelectedId") = mintPersonalAnalysisCompetencySelectedId
            Me.ViewState("mblnPersonalAnalysisCompetencySelectionPopup") = mblnPersonalAnalysisCompetencySelectionPopup
            Me.ViewState("mblnCompetencySelectionFromList") = mblnCompetencySelectionFromList
            Me.ViewState("mintListPersonalAnalysisSelectionDataListItemIndex") = mintListPersonalAnalysisSelectionDataListItemIndex

            Me.ViewState("mintTalentEmployeeUnkid") = mintTalentEmployeeUnkid
            Me.ViewState("mintSuccessionEmployeeUnkid") = mintSuccessionEmployeeUnkid
            Me.ViewState("mblnIsAvailableInTalent") = mblnIsAvailableInTalent
            Me.ViewState("mblnIsAvailableInSuccession") = mblnIsAvailableInSuccession
            Me.ViewState("mintFormStatusUnkid") = mintFormStatusUnkid
            Me.ViewState("mblnReviewTrainingPopup") = mblnReviewTrainingPopup
            Me.ViewState("mblnIsLineManager") = mblnIsLineManager

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combo Events "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Call Fill_Info(CInt(cboEmployee.SelectedValue))
                    'Session("Employeeunkid") = CInt(cboEmployee.SelectedValue)
                Else
                    Call Fill_Info(CInt(Session("Employeeunkid")))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Button/Link Events"
    'General
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            If mintTalentEmployeeUnkid > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Talent_Succession/wpg_TalentProfile.aspx", False)
            ElseIf mintSuccessionEmployeeUnkid > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Talent_Succession/wpg_SuccessionProfile.aspx", False)
            Else
                If Request.QueryString.ToString.Length > 0 Then
                    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                Else
                Response.Redirect("~/Userhome.aspx", False)
            End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnLock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLock.Click
        Dim objpdpform_master As New clspdpform_master
        Dim objPdpgoals_master As New clsPdpgoals_master
        Try

            If mintFormunkidId > 0 Then
                If objpdpform_master.UpdatePDPFormStatus(mintFormunkidId, clspdpform_master.enPDPFormStatus.Lock) = False Then
                    DisplayMessage.DisplayMessage(objpdpform_master._Message, Me)
                Else
                    objPdpgoals_master.SendEmails(clsPdpgoals_master.enEmailType.LOCK_PDP, Session("ArutiSelfServiceURL"), CInt(Session("CompanyUnkId")), _
                                              CInt(Session("UserId")), "", mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, _
                                              Session("UserName").ToString(), CInt(cboEmployee.SelectedValue), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("Fin_year")), _
                                              Session("EmployeeAsOnDate").ToString(), CStr(Session("UserAccessModeSetting")), CStr(Session("Database_Name")))

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 60, "PDP Form Locked Successfully."), Me)
                    FillPDPFormData()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpform_master = Nothing
        End Try
    End Sub
    Protected Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Dim objpdpform_master As New clspdpform_master
        Try

            If mintFormunkidId > 0 Then
                If objpdpform_master.UpdatePDPFormStatus(mintFormunkidId, clspdpform_master.enPDPFormStatus.Unlock) = False Then
                    DisplayMessage.DisplayMessage(objpdpform_master._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 61, "PDP Form Unlocked Successfully."), Me)
                    FillPDPFormData()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpform_master = Nothing
        End Try
    End Sub
    Protected Sub btnSubmitForReview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForReview.Click
        Dim objpdpform_master As New clspdpform_master
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objEmployee As New clsEmployee_Master
        Try

            If mintFormunkidId > 0 Then

                Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))
                If IsNothing(dsList) OrElse dsList.Tables(0).Rows.Count <= 0 AndAlso dsList.Tables(0).Rows(0)("employee").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, line manager is not available, please set line manager for continue"), Me)
                    Exit Sub
                End If

                If objpdpform_master.UpdatePDPFormStatus(mintFormunkidId, clspdpform_master.enPDPFormStatus.SubmitForReview) = False Then
                    DisplayMessage.DisplayMessage(objpdpform_master._Message, Me)
                Else
                    objPdpgoals_master.SendEmails(clsPdpgoals_master.enEmailType.SUBMITFORAPPROVAL, Session("ArutiSelfServiceURL"), CInt(Session("CompanyUnkId")), _
                          CInt(cboEmployee.SelectedValue), "", mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, _
                          cboEmployee.SelectedItem.Text, CInt(cboEmployee.SelectedValue), _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("Fin_year")), _
                          Session("EmployeeAsOnDate").ToString(), CStr(Session("UserAccessModeSetting")), CStr(Session("Database_Name")))

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "PDP Form Submitted For Review Successfully."), Me)
                    FillPDPFormData()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpform_master = Nothing
        End Try
    End Sub

    'Instruction
    Protected Sub lnkPdpInstruction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPdpInstruction.Click
        Dim objpdpsettings_master As New clspdpsettings_master

        Dim mdicPdpSetting As New Dictionary(Of clspdpsettings_master.enPDPConfiguration, String)

        Try

            mdicPdpSetting = objpdpsettings_master.GetSetting()
            If IsNothing(mdicPdpSetting) = False AndAlso mdicPdpSetting.ContainsKey(clspdpsettings_master.enPDPConfiguration.INSTRUCTION) Then
                txtPdpInstruction.Text = mdicPdpSetting(clspdpsettings_master.enPDPConfiguration.INSTRUCTION)
            End If

            popupPdpInstruction.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Personal Analysis-Table
    Protected Sub OnGvAddPersonalDevlopmentGoalItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsDataValid() = False Then Exit Sub

            If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.ADD) = False Then Exit Sub


            ClearPersolnalGoalPopupData()

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = dlPersonalAnalysisGoalsCategory.Items(item.ItemIndex)
            Dim hfcategoryunkid As HiddenField = TryCast(row.FindControl("hfcategoryunkid"), HiddenField)

            mintCategoryId = hfcategoryunkid.Value
            mintItemTranId = -1
            mblnIsAddItemData = True

            Generate_TableCItemPopup_Data(hfcategoryunkid.Value, "", "Add Data")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    Protected Sub OnGvEditPersonalDevlopmentGoalItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsDataValid() = False Then Exit Sub
            If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.EDIT) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim row As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)
            Dim Childgrid As GridView = TryCast(row.Parent.Parent, GridView)

            Dim strGuid = Childgrid.DataKeys(row.RowIndex)("GUID")
            Dim intCategoryunkid = Childgrid.DataKeys(row.RowIndex)("Header_Id")
            Dim intFormunkid = Childgrid.DataKeys(row.RowIndex)("formunkid")

            If strGuid.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, there is no data to be edited."), Me)
                Exit Sub
            End If

            mintCategoryId = intCategoryunkid
            mstrPersonalAnalysisEditGUID = strGuid
            mintFormunkidId = intFormunkid

            Generate_TableCItemPopup_Data(intCategoryunkid, strGuid, "Add Data")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Public Sub OnGvDeletePersonalDevlopmentGoalItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.DELETE) = False Then Exit Sub

            Dim row As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)
            Dim Childgrid As GridView = TryCast(row.Parent.Parent, GridView)

            Dim strGuid = Childgrid.DataKeys(row.RowIndex)("GUID")
            Dim intCategoryunkid = Childgrid.DataKeys(row.RowIndex)("Header_Id")
            Dim intFormunkid = Childgrid.DataKeys(row.RowIndex)("formunkid")

            If strGuid.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry, there is no data to be deleted."), Me)
                Exit Sub
            End If

            mintCategoryId = intCategoryunkid
            mstrPersonalAnalysisEditGUID = strGuid
            mintFormunkidId = intFormunkid

            delReason.Title = "Please enter delete reason"
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeletePersonlaAnalysisGoal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Personal Analysis-Table-Popup
    Protected Sub btnPersonalDevlopmentGoalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPersonalDevlopmentGoalSave.Click
        Dim mDtCategoryItems As New DataTable
        Dim objpdpform_master As New clspdpform_master
        Dim objpdpform_tran As New clspdpform_tran

        Try

            Dim strMsg As String = String.Empty
            mDtCategoryItems = objpdpform_master._DtPersonalAnalysisGoalItems.Copy

            For Each dgItem As DataListItem In dgv_Citems.Items
                Dim drCategoryItems As DataRow
                drCategoryItems = mDtCategoryItems.NewRow
                Dim hfcustomitemunkid As HiddenField = CType(dgItem.FindControl("hfcustomitemunkid"), HiddenField)
                Dim hfitemtypeid As HiddenField = CType(dgItem.FindControl("hfitemtypeid"), HiddenField)

                If CInt(hfitemtypeid.Value) = CInt(clsassess_custom_items.enCustomType.FREE_TEXT) Then
                    If dgItem.FindControl("txtFreetext") IsNot Nothing Then
                        Dim xtxt As TextBox = CType(dgItem.FindControl("txtFreetext"), TextBox)
                        Dim hfCompetencyVal As HiddenField = CType(dgItem.FindControl("hfCompetencyVal"), HiddenField)

                        If xtxt.Visible = True Then
                            If xtxt.Text.Trim.Length > 0 Then
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If
                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value

                                If CInt(hfCompetencyVal.Value) > 0 Then
                                    drCategoryItems.Item("fieldvalue") = hfCompetencyVal.Value
                                    drCategoryItems.Item("iscompetencyselection") = True
                                Else
                                    drCategoryItems.Item("fieldvalue") = xtxt.Text
                                End If


                                mDtCategoryItems.Rows.Add(drCategoryItems)

                            ElseIf xtxt.Enabled = True AndAlso xtxt.ReadOnly = False AndAlso xtxt.Text.Trim.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, you must complete all fields to save."), Me)
                                Exit Sub
                            End If
                        End If
                    End If

                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dgItem.FindControl("dtpSelection") IsNot Nothing Then
                        Dim xdtp As Controls_DateCtrl = CType(dgItem.FindControl("dtpSelection"), Controls_DateCtrl)
                        If xdtp.Visible Then
                            If xdtp.Enabled = True AndAlso xdtp.IsNull = False Then

                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If

                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = eZeeDate.convertDate(xdtp.GetDateTime()).ToString()
                                mDtCategoryItems.Rows.Add(drCategoryItems)
                            ElseIf xdtp.Enabled = True AndAlso xdtp.IsNull Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, all field value is mandatory information."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.SELECTION Then
                    If dgItem.FindControl("cboSelection") IsNot Nothing Then
                        Dim xCbo As DropDownList = CType(dgItem.FindControl("cboSelection"), DropDownList)
                        If xCbo.Visible Then
                            If xCbo.Enabled = True AndAlso xCbo.SelectedValue > 0 Then
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If



                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = xCbo.SelectedValue
                                mDtCategoryItems.Rows.Add(drCategoryItems)

                            ElseIf xCbo.Enabled = True AndAlso xCbo.SelectedValue <= 0 Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, all field value is mandatory information."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    If dgItem.FindControl("txtNUM") IsNot Nothing Then
                        Dim xtxt As Controls_NumericTextBox = CType(dgItem.FindControl("txtNUM"), Controls_NumericTextBox)
                        If xtxt.Visible Then
                            If xtxt.Enabled = True AndAlso xtxt.Text.Trim.Length > 0 Then
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If

                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = xtxt.Text
                                mDtCategoryItems.Rows.Add(drCategoryItems)

                            ElseIf xtxt.Enabled = True AndAlso xtxt.Read_Only = False AndAlso xtxt.Text.Trim.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, all field value is mandatory information."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            Next

            SaveTablePersonalFormMaster(mDtCategoryItems)
            ClearPersolnalGoalPopupData()
            mblnPersonalAnalysisandGoalsTablePopup = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnPersonalDevlopmentGoalClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPersonalDevlopmentGoalClose.Click
        Try
            ClearPersolnalGoalPopupData()
            mblnPersonalAnalysisandGoalsTablePopup = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Personal Analysis-List
    Protected Sub OnAddPersonalDevlopmentGoalItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpdpcategory_master As New clspdpcategory_master
        Dim objpdpitem_master As New clspdpitem_master

        Try
            If IsDataValid() = False Then Exit Sub
            If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.ADD) = False Then Exit Sub

            ClearPersolnalGoalPopupData()

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hfcategoryunkid As HiddenField = TryCast(item.FindControl("hfcategoryunkid"), HiddenField)
            Dim hfitemtypeid As HiddenField = TryCast(item.FindControl("hfitemtypeid"), HiddenField)
            Dim hfitemunkid As HiddenField = TryCast(item.FindControl("hfitemunkid"), HiddenField)
            Dim hfselectionmodeid As HiddenField = TryCast(item.FindControl("hfselectionmodeid"), HiddenField)
            Dim hfiscompetencyselectionset As HiddenField = TryCast(item.FindControl("hfiscompetencyselectionset"), HiddenField)

            mintCategoryId = hfcategoryunkid.Value
            mintItemId = hfitemunkid.Value
            mintItemtypeid = hfitemtypeid.Value
            mintItemTranId = -1
            mblnIsAddItemData = True
            'Generate_Popup_Data(hfcategoryunkid.Value, "", "Add Data")


            If CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.FREE_TEXT) Then
                pnlItemNameFreeText.Visible = True

                If CBool(hfiscompetencyselectionset.Value) Then
                    pnlFreeTextListItemComepetencySelection.Visible = True
                Else
                    pnlFreeTextListItemComepetencySelection.Visible = False
                End If

            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.DATE_SELECTION) Then
                pnlItemNameDtp.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.NUMERIC_DATA) Then
                pnlItemNameNum.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.SELECTION) Then
                pnlItemNameSelection.Visible = True

                Select Case CInt(hfselectionmodeid.Value)
                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                        Dim dsList As New DataSet
                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                        objCMaster = Nothing


                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                    Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES
                        Dim dtab As DataTable = Nothing
                        Dim dsList As New DataSet

                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                        objCMaster = Nothing



                        If CInt(hfselectionmodeid.Value) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                        ElseIf CInt(hfselectionmodeid.Value) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                        End If
                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dtab
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                        Dim objCompetency As New clsassess_competencies_master
                        Dim intPeriod As Integer = -1

                        intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))

                        Dim dsList As New DataSet
                        dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), intPeriod, _
                                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
                        With cboItemNameSelection
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                        objCompetency = Nothing

                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                        Dim objEmpField1 As New clsassess_empfield1_master
                        Dim intPeriod As Integer = -1

                        intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))
                        Dim dsList As New DataSet

                        dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), intPeriod, "List", True, True)
                        With cboItemNameSelection
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                        objEmpField1 = Nothing

                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM

                        Dim dsList As New DataSet

                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                        objCMaster = Nothing

                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                End Select


            End If

            objpdpcategory_master._Categoryunkid = CInt(hfcategoryunkid.Value)
            objpdpitem_master._Itemunkid = CInt(hfitemunkid.Value)
            lblCItem.Text = objpdpcategory_master._Category
            lblItemName.Text = objpdpitem_master._Item


            mblnPersonalAnalysisandGoalsListPopup = True
            popup_ListCItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpcategory_master = Nothing
            objpdpitem_master = Nothing

        End Try

    End Sub
    Protected Sub OnEditPersonalDevlopmentGoalItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpdpitem_master As New clspdpitem_master
        Dim objpdpform_master As New clspdpform_master
        Try
            If IsDataValid() = False Then Exit Sub
            If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.EDIT) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim hfitemtranunkid As HiddenField = TryCast(item.FindControl("hfitemtranunkid"), HiddenField)
            Dim hfcustomitemunkid As HiddenField = TryCast(item.FindControl("hfcustomitemunkid"), HiddenField)
            Dim hfitemtypeid As HiddenField = TryCast(item.FindControl("hfitemtypeid"), HiddenField)
            Dim hfselectionmodeid As HiddenField = TryCast(item.FindControl("hfselectionmodeid"), HiddenField)
            Dim hfcategoryunkid As HiddenField = TryCast(item.FindControl("hfcategoryunkid"), HiddenField)
            Dim hfitemgrpguid As HiddenField = TryCast(item.FindControl("hfitemgrpguid"), HiddenField)
            Dim hfiscompetencyselection As HiddenField = TryCast(item.FindControl("hfiscompetencyselection"), HiddenField)
            Dim hfCompetencyValue As HiddenField = TryCast(item.FindControl("hfCompetencyValue"), HiddenField)


            objpdpitem_master._Itemunkid = CInt(hfcustomitemunkid.Value)
            lblCItem.Text = objpdpitem_master._Item
            mstrPersonalAnalysisEditGUID = hfitemgrpguid.Value
            mintCategoryId = hfcategoryunkid.Value
            mintItemId = hfcustomitemunkid.Value

            mintItemTranId = hfitemtranunkid.Value
            mintItemtypeid = hfitemtypeid.Value

            If CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.FREE_TEXT) Then
                pnlItemNameFreeText.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.DATE_SELECTION) Then
                pnlItemNameDtp.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.NUMERIC_DATA) Then
                pnlItemNameNum.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.SELECTION) Then
                pnlItemNameSelection.Visible = True

                Select Case CInt(hfselectionmodeid.Value)
                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                        Dim dsList As New DataSet
                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                        objCMaster = Nothing


                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                    Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES
                        Dim dtab As DataTable = Nothing
                        Dim dsList As New DataSet

                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                        objCMaster = Nothing

                        If CInt(hfselectionmodeid.Value) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                        ElseIf CInt(hfselectionmodeid.Value) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                        End If
                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dtab
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                        Dim objCompetency As New clsassess_competencies_master
                        Dim intPeriod As Integer = -1

                        intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))

                        Dim dsList As New DataSet
                        dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), intPeriod, _
                                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
                        With cboItemNameSelection
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                        objCompetency = Nothing


                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                        Dim objEmpField1 As New clsassess_empfield1_master
                        Dim intPeriod As Integer = -1

                        intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))
                        Dim dsList As New DataSet

                        dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), intPeriod, "List", True, True)
                        With cboItemNameSelection
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                        objEmpField1 = Nothing

                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM

                        Dim dsList As New DataSet

                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                        objCMaster = Nothing

                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dsList.Tables(0)
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                End Select

            End If
            Dim intPeriodid As Integer = -1
            intPeriodid = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))

            Dim dtCustomItemValue As New DataTable
            dtCustomItemValue = objpdpform_master.Get_CListItems_ForAddEdit(intPeriodid, hfitemtranunkid.Value)


            If CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.FREE_TEXT) Then


                If CBool(hfiscompetencyselection.Value) Then
                    Dim objCompetency As New clsassess_competencies_master
                    mintPersonalAnalysisCompetencySelectedId = CInt(hfCompetencyValue.Value)
                    objCompetency._Competenciesunkid = CInt(hfCompetencyValue.Value)
                    txtItemNameFreeText.Text = objCompetency._Name
                    txtItemNameFreeText.Enabled = False


                    pnlFreeTextListItemComepetencySelection.Visible = True

                Else
                    txtItemNameFreeText.Enabled = True
                    txtItemNameFreeText.Text = dtCustomItemValue.Rows(0)("customvalueId").ToString()
                    pnlFreeTextListItemComepetencySelection.Visible = False
                End If




            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.DATE_SELECTION) Then
                dtpItemName.SetDate() = eZeeDate.convertDate(dtCustomItemValue.Rows(0)("customvalueId").ToString).Date

            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.NUMERIC_DATA) Then
                txtItemNameNUM.Text = dtCustomItemValue.Rows(0)("customvalueId").ToString()

            ElseIf CInt(hfitemtypeid.Value) = CInt(clspdpitem_master.enPdpCustomType.SELECTION) Then
                cboItemNameSelection.SelectedValue = dtCustomItemValue.Rows(0)("customvalueId").ToString()

            End If
            mblnIsAddItemData = True
            mblnPersonalAnalysisandGoalsListPopup = True
            popup_ListCItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpitem_master = Nothing
            objpdpform_master = Nothing
        End Try
    End Sub
    Public Sub OnDeletePersonalDevlopmentGoalItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.DELETE) = False Then Exit Sub

            Dim hfitemtranunkid As HiddenField = TryCast(item.FindControl("hfitemtranunkid"), HiddenField)
            Dim hfcustomitemunkid As HiddenField = TryCast(item.FindControl("hfcustomitemunkid"), HiddenField)
            Dim hfitemtypeid As HiddenField = TryCast(item.FindControl("hfitemtypeid"), HiddenField)
            Dim hfselectionmodeid As HiddenField = TryCast(item.FindControl("hfselectionmodeid"), HiddenField)
            Dim hfcategoryunkid As HiddenField = TryCast(item.FindControl("hfcategoryunkid"), HiddenField)
            Dim hfitemgrpguid As HiddenField = TryCast(item.FindControl("hfitemgrpguid"), HiddenField)
            Dim hfpdpformunkid As HiddenField = TryCast(item.FindControl("hfpdpformunkid"), HiddenField)

            mstrPersonalAnalysisEditGUID = hfitemgrpguid.Value
            mintFormunkidId = hfpdpformunkid.Value
            mintCategoryId = hfcategoryunkid.Value
            mintItemTranId = hfitemtranunkid.Value

            If hfitemgrpguid.Value.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, No goal item has been added in order to delete."), Me)
                Exit Sub
            End If

            delReason.Title = "Please enter delete reason"
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeletePersonlaAnalysisGoal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Personal Analysis-List-Popup
    Protected Sub btnListPersonalDevlopmentGoalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListPersonalDevlopmentGoalSave.Click
        Dim mDtCategoryItems As New DataTable
        Dim objpdpform_master As New clspdpform_master

        Try

            Dim strMsg As String = String.Empty
            Dim FieldValue As String = ""

            Select Case mintItemtypeid
                Case CInt(clspdpitem_master.enPdpCustomType.FREE_TEXT)
                    If txtItemNameFreeText.Text.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Please add something in the field to save."), Me)
                        Exit Sub
                    End If


                    If mintPersonalAnalysisCompetencySelectedId > 0 Then
                        FieldValue = mintPersonalAnalysisCompetencySelectedId
                    Else
                        FieldValue = txtItemNameFreeText.Text
                    End If



                Case CInt(clspdpitem_master.enPdpCustomType.DATE_SELECTION)
                    If dtpItemName.IsNull = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Please add something in the field to save."), Me)
                        Exit Sub
                    End If

                    FieldValue = eZeeDate.convertDate(dtpItemName.GetDateTime()).ToString()

                Case CInt(clspdpitem_master.enPdpCustomType.NUMERIC_DATA)
                    If txtItemNameNUM.Text.Length <= 0 AndAlso CInt(txtItemNameNUM.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Please add something in the field to save."), Me)
                        Exit Sub
                    End If

                    If CInt(txtItemNameNUM.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Field value should be greater than 0."), Me)
                        Exit Sub
                    End If
                    FieldValue = txtItemNameNUM.Text
                Case CInt(clspdpitem_master.enPdpCustomType.SELECTION)

                    If CInt(cboItemNameSelection.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Sorry, field value is required information."), Me)
                        Exit Sub
                    End If

                    FieldValue = cboItemNameSelection.SelectedValue
            End Select


            If SavePersonalFormMasterList(FieldValue) Then
                ClearPersolnalGoalPopupData()
                mblnPersonalAnalysisandGoalsListPopup = False
                popup_ListCItemAddEdit.Hide()
                FillPersonalAnalysisItems()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnListPersonalDevlopmentGoalClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListPersonalDevlopmentGoalClose.Click
        Try
            ClearPersolnalGoalPopupData()
            mblnPersonalAnalysisandGoalsListPopup = False
            popup_ListCItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '  Personal Analysis-Table--Popup
    Protected Sub OnCompetencySelection_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            'ctrlListPersonalAnalysisSelectionDataListItem = item

            'ViewState("ctrlListPersonalAnalysisSelectionDataListItem") = item
            mintListPersonalAnalysisSelectionDataListItemIndex = item.ItemIndex
            FillCompetencySelectionList()
            mblnCompetencySelectionFromList = False
            mblnPersonalAnalysisCompetencySelectionPopup = True
            popupCompetencySelection.Show()


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    Protected Sub OnCompetencySelectionReset_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hfCompetencyVal As HiddenField = CType(item.FindControl("hfCompetencyVal"), HiddenField)
            Dim txtFreetext As TextBox = CType(item.FindControl("txtFreetext"), TextBox)
            hfCompetencyVal.Value = 0
            txtFreetext.Text = ""
            txtFreetext.Enabled = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region

#Region " Button Events "
    'Action Plan
    Protected Sub btnSaveActionPlan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveActionPlan.Click
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
        Dim dsListItem As New DataSet

        Try
            'Dim mstrSelectedActionPlanTrainingCourses As String = String.Empty
            'If chkActionPlanTrainingNeeded.Checked Then
            '    mstrSelectedActionPlanTrainingCourses = String.Join(",", dgvActionPlanTrainingList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) CType(x.FindControl("hfmasterid"), HiddenField).Value).ToArray())
            'End If

            'dsListItem = objPdpgoals_trainingneed_Tran.GetActionPlanTraining("TrainingList", mintActionPlanItemUnkId, CInt(cboEmployee.SelectedValue))

            'If IsNothing(dsListItem) = False AndAlso dsListItem.Tables("TrainingList").Rows.Count > 0 Then
            '    If dsListItem.Tables("TrainingList").Rows.Count > 0 AndAlso chkActionPlanTrainingNeeded.Checked = False Then
            '        delReason.Title = "You Remove Some Training Courses, Please Enter Reason For Deleting."
            '        mintDeleteActionId = CInt(enDeleteAction.DeleteActionPlanTraining)
            '        delReason.Show()
            '        Exit Sub
            '    Else
            '        For Each drow As DataRow In dsListItem.Tables("TrainingList").Rows
            '            Dim isExists As Boolean = mstrSelectedActionPlanTrainingCourses.Split(",").Any(Function(x) x = CInt(drow("trainingcourseunkid").ToString()))
            '            If isExists = False Then
            '                delReason.Title = "You Remove Some Training Courses, Please Enter Reason For Deleting."
            '                mintDeleteActionId = CInt(enDeleteAction.DeleteActionPlanTraining)
            '                delReason.Show()
            '                Exit Sub
            '                Exit For
            '            End If
            '        Next

            '    End If

            'End If

            If IsActionPlanPopUpDataValid() = False Then
                Exit Sub
            End If

            If SavePersonalFormMasterList("") Then
                SaveActionPlan()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_trainingneed_Tran = Nothing
            objPdpgoals_master = Nothing
        End Try
    End Sub
    Protected Sub lnkExistingGoal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExistingGoal.Click
        Try
            FillActionPlanSelectionList()
            mblnActionPlanSelectionPopup = True
            popup_ActionPlanGoalSelection.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub lnkExistingGoalReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExistingGoalReset.Click
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master
        Dim dsInprogressTraining As New DataSet
        Try



            If mintActionPlanItemUnkId > 0 Then
                objPdpgoals_master._Pdpgoalsmstunkid = mintActionPlanItemUnkId

                Dim mblnIsGoalChange As Boolean = False
                'Case 1 Change Goal from Text to list
                If objPdpgoals_master._Goal_Name.Length > 0 AndAlso objPdpgoals_master._Itemunkid <= 0 AndAlso mintActionPlanGoalSelectedId > 0 Then
                    mblnIsGoalChange = True
                End If

                'Case 2 Change Goal from List to text 
                If objPdpgoals_master._Itemunkid > 0 AndAlso objPdpgoals_master._Goal_Name.Length <= 0 AndAlso mintActionPlanGoalSelectedId <= 0 Then
                    mblnIsGoalChange = True
                End If


                'Case 3 Change Goal from Text to Text
                If objPdpgoals_master._Goal_Name.Length > 0 AndAlso txtActionPlanGoalName.Text <> objPdpgoals_master._Goal_Name Then
                    mblnIsGoalChange = True
                End If

                'Case 4 Change Goal from List to List 
                If objPdpgoals_master._Itemunkid > 0 AndAlso mintActionPlanGoalSelectedId = objPdpgoals_master._Itemunkid Then
                    mblnIsGoalChange = True
                End If



                dsInprogressTraining = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(mintActionPlanItemUnkId, -1, CInt(enModuleReference.PDP), True, Nothing)


                If IsNothing(dsInprogressTraining) = False AndAlso dsInprogressTraining.Tables(0).Rows.Count > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 73, "Sorry, you cannot remove the training(s) selected. Reason: Some are already linked to Learning and Development."), Me)
                    Exit Sub
                Else
                    If mblnIsGoalChange Then
                        If IsPDPDataValid(enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan) = False Then Exit Sub
                        mintConfirmationActionId = enConfirmationAction.ResetActionPlanSelection
                        cfConfirmation.Message = Language.getMessage(mstrModuleName, 51, "You are about to delete this goal; all the recommended training courses in this action plan will also be removed. Are you sure you want to delete this goal?")
                        cfConfirmation.Show()
                    Else
                        ResetActionplanfromSelection()
                    End If
                End If
            Else
                ResetActionplanfromSelection()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
        End Try
    End Sub
    Protected Sub btnCloseActionPlan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseActionPlan.Click
        Try
            mblnDevelopmentActionPlanPopup = False
            popup_ActionPlanAddEdit.Hide()
            ClearActionPlanPopupControlsAndValues()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnSetActionPlanGoalSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetActionPlanGoalSelection.Click
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master
        Dim dsInprogressTraining As New DataSet

        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvActionPlanGoalSelection.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("rdbActionPlanGoalSelection"), RadioButton).Checked = True)

            If gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry, please select atleast one goal to continue."), Me.Page)
                Exit Sub
            End If

            If mintActionPlanItemUnkId > 0 Then
                objPdpgoals_master._Pdpgoalsmstunkid = mintActionPlanItemUnkId

                Dim mblnIsGoalChange As Boolean = False
                ''Case 1 Change Goal from Text to list
                'If objPdpgoals_master._Goal_Name.Length > 0 AndAlso objPdpgoals_master._Itemunkid AndAlso mintActionPlanGoalSelectedId > 0 Then
                '    mblnIsGoalChange = True
                'End If

                ''Case 2 Change Goal from List to text 
                'If objPdpgoals_master._Itemunkid > 0 AndAlso objPdpgoals_master._Goal_Name.Length <= 0 AndAlso txtActionPlanGoalName.Text.Trim.Length > 0 Then
                '    mblnIsGoalChange = True
                'End If

                'Case 1 Change Goal from Text to list
                If objPdpgoals_master._Goal_Name.Length > 0 AndAlso objPdpgoals_master._Itemunkid <= 0 AndAlso mintActionPlanGoalSelectedId > 0 Then
                    mblnIsGoalChange = True
                End If

                'Case 2 Change Goal from List to text 
                If objPdpgoals_master._Itemunkid > 0 AndAlso objPdpgoals_master._Goal_Name.Length <= 0 AndAlso mintActionPlanGoalSelectedId <= 0 Then
                    mblnIsGoalChange = True
                End If


                'Case 3 Change Goal from Text to Text
                If objPdpgoals_master._Goal_Name.Length > 0 AndAlso txtActionPlanGoalName.Text <> objPdpgoals_master._Goal_Name Then
                    mblnIsGoalChange = True
                End If

                'Case 4 Change Goal from List to List 
                If objPdpgoals_master._Itemunkid > 0 AndAlso mintActionPlanGoalSelectedId = objPdpgoals_master._Itemunkid Then
                    mblnIsGoalChange = True
                End If

                dsInprogressTraining = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(mintActionPlanItemUnkId, -1, CInt(enModuleReference.PDP), True, Nothing)

                If IsNothing(dsInprogressTraining) = False AndAlso dsInprogressTraining.Tables(0).Rows.Count > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 73, "You can not remove training(s).Reason: Some training items are linked to Learning and Development."), Me)
                    Exit Sub
                Else
                    If mblnIsGoalChange Then
                        If IsPDPDataValid(enPDPFormType.SetDeleteTrainingForDevelopmentActionPlan) = False Then Exit Sub

                        mintConfirmationActionId = enConfirmationAction.ChangeActionPlanGoalFromSelection
                        cfConfirmation.Message = Language.getMessage(mstrModuleName, 52, "You change action plan goal, your all training will be removed, Are you sure still you want to change action plan goal?")
                        cfConfirmation.Show()
                    Else
                        SetActionplanfromSelection()
                    End If
                End If


            Else
                SetActionplanfromSelection()
            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDepartmentaltrainingneed_master = Nothing
            objPdpgoals_master = Nothing
        End Try
    End Sub
    Protected Sub btnCloseActionPlanGoalSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseActionPlanGoalSelection.Click
        Try
            mblnActionPlanSelectionPopup = False
            popup_ActionPlanGoalSelection.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Update Progress
    Protected Sub btnUpdateProgressClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateProgressClose.Click
        Try
            ClearUpdateProgressData()
            mblnUpdateProgressPopup = False
            popup_UpdateProgress.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnUpdateProgressSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateProgressSave.Click
        Dim objPdpgoals_update_Tran As New clsPdpgoals_update_Tran
        Dim blnFlag As Boolean = False
        Try

            If IsUpdateProgressDataValid() = False Then
                Exit Sub
            End If


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objPdpgoals_update_Tran._AuditUserId = CInt(Session("UserId"))
                objPdpgoals_update_Tran._Userunkid = CInt(Session("UserId"))
            Else
                objPdpgoals_update_Tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            objPdpgoals_update_Tran._ClientIP = CStr(Session("IP_ADD"))
            objPdpgoals_update_Tran._FormName = mstrModuleName
            objPdpgoals_update_Tran._FromWeb = True
            objPdpgoals_update_Tran._HostName = CStr(Session("HOST_NAME"))
            objPdpgoals_update_Tran._DatabaseName = CStr(Session("Database_Name"))
            objPdpgoals_update_Tran._Isvoid = False


            objPdpgoals_update_Tran._Pdpgoalsmstunkid = mintActionPlanItemUnkId
            objPdpgoals_update_Tran._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objPdpgoals_update_Tran._StatusId = CInt(drpUpdateProgressStatus.SelectedValue)
            objPdpgoals_update_Tran._Updatedatetime = DateTime.Now
            objPdpgoals_update_Tran._Pct_Updated = txtUpdateProgressScore.Text
            objPdpgoals_update_Tran._Last_Pct_Update = txtUpdateProgressLastUpdateScore.Text
            objPdpgoals_update_Tran._Remarks = txtUpdateProgressRemarks.Text
            objPdpgoals_update_Tran._Approvalstatusid = -1
            objPdpgoals_update_Tran._Approvaldatetime = DateTime.Now.Date
            objPdpgoals_update_Tran._Approvalcomments = ""


            blnFlag = objPdpgoals_update_Tran.Insert()

            If blnFlag = False AndAlso objPdpgoals_update_Tran._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objPdpgoals_update_Tran._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Saved Successfully."), Me)
                ClearUpdateProgressData()
                FillDevlopmentActionPlanItems()
                mblnUpdateProgressPopup = False
                popup_UpdateProgress.Hide()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnRemoveLastUpdateProgress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveLastUpdateProgress.Click
        Try
            delReason.Title = "Please enter delete reason for deleting update progress"
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeleteLastUpdateProgress)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Training
    Protected Sub btnCloseViewTrainingList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseViewTrainingList.Click
        Try
            mblnViewTrainingPopup = False
            popup_ViewTrainingList.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Comment
    Protected Sub btnAddActionPlanComment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionPlanComment.Click
        Dim objPdpevaluator_comments As New clsPdpevaluator_comments
        Try
            objPdpevaluator_comments._Comments = txtActionPlanComment.Text
            SaveActionPlanCommentItem(objPdpevaluator_comments)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpevaluator_comments = Nothing
        End Try
    End Sub
    Private Sub SaveActionPlanCommentItem(ByVal objPdpevaluator_comments As clsPdpevaluator_comments)

        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objReportingToEmployee As New clsReportingToEmployee
        Dim objpdpsettings_master As New clspdpsettings_master
        Dim objemployee_transfer_tran As New clsemployee_transfer_tran
        Dim objEmployee As New clsEmployee_Master

        Dim blnFlag As Boolean = False

        Try

            GetEvalutorReviewer(mintActionPlanItemUnkId)
            objPdpevaluator_comments._Pdpgoalsmstunkid = mintActionPlanItemUnkId

            If mintReviewerUnkid > 0 Then
                objPdpevaluator_comments._Reviewermstunkid = mintReviewerUnkid
                objPdpevaluator_comments._Evaluatortypeid = -1
                objPdpevaluator_comments._Evaluatorunkid = -1
            Else
                objPdpevaluator_comments._Evaluatortypeid = mintEvalutorTypeId
                objPdpevaluator_comments._Evaluatorunkid = mintEvalutorUnkid
                objPdpevaluator_comments._Reviewermstunkid = -1
            End If


            If objPdpevaluator_comments._Commenttranunkid <= 0 Then
                objPdpevaluator_comments._Commentdatetime = DateTime.Now
            End If

            objPdpevaluator_comments._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objPdpevaluator_comments._DtAttachment = mdtCommentAttachment
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objPdpevaluator_comments._AuditUserId = CInt(Session("UserId"))
            Else
                objPdpevaluator_comments._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            objPdpevaluator_comments._ClientIP = CStr(Session("IP_ADD"))
            objPdpevaluator_comments._FormName = mstrModuleName
            objPdpevaluator_comments._FromWeb = True
            objPdpevaluator_comments._HostName = CStr(Session("HOST_NAME"))
            objPdpevaluator_comments._DatabaseName = CStr(Session("Database_Name"))
            objPdpevaluator_comments._Isvoid = False

            blnFlag = objPdpevaluator_comments.SaveEvalutorCommentData()

            If blnFlag = False AndAlso objPdpevaluator_comments._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objPdpevaluator_comments._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Saved Successfully."), Me)
                ClearActionPlanCommentPopupControls()
                FillCommentList()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpsettings_master = Nothing
            objPdpgoals_master = Nothing
            objReportingToEmployee = Nothing
            objEmployee = Nothing
            objemployee_transfer_tran = Nothing
        End Try
    End Sub
    Protected Sub btnCloseActionPlanComment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseActionPlanComment.Click
        Try
            ClearActionPlanCommentPopupData()
            mintActionPlanCategoryId = -1
            mintActionPlanItemUnkId = -1
            mblnActionPlanCommentPopup = False
            popup_ActionPlanComment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Attachment
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub popupAttachment_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAttachment_YesNo.buttonYes_Click
    '    Try
    '        If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
    '            mdtCommentAttachment.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
    '            mdtCommentAttachment.AcceptChanges()
    '            Call fillAttachment()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub btnAddActionPlanCommentAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionPlanCommentAttachment.Click
        Try
            FillAttachmentCombo()
            mblnCommentAttachmentPopup = True
            mblnIsAttachmentDownloadMode = False
            popupCommentAttachment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveActionPlanCommentAttachmentPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveActionPlanCommentAttachmentPopup.Click
        Dim objPdpevaluator_comments As New clsPdpevaluator_comments
        Dim blnFlag As Boolean = False
        Try

            objPdpevaluator_comments._Commenttranunkid = mintActionPlanCommentItemunkid

            objPdpevaluator_comments._DtAttachment = mdtCommentAttachment
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objPdpevaluator_comments._AuditUserId = CInt(Session("UserId"))
            Else
                objPdpevaluator_comments._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            objPdpevaluator_comments._ClientIP = CStr(Session("IP_ADD"))
            objPdpevaluator_comments._FormName = mstrModuleName
            objPdpevaluator_comments._FromWeb = True
            objPdpevaluator_comments._HostName = CStr(Session("HOST_NAME"))
            objPdpevaluator_comments._DatabaseName = CStr(Session("Database_Name"))
            objPdpevaluator_comments._Isvoid = False

            blnFlag = objPdpevaluator_comments.UpdateEvalutorCommentAttachmentData()

            If blnFlag = False AndAlso objPdpevaluator_comments._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objPdpevaluator_comments._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 48, "Saved Successfully."), Me)
                btnSaveActionPlanCommentAttachmentPopup.Visible = False
                ClearActionPlanCommentPopupData()
                mblnCommentAttachmentPopup = False
                popupCommentAttachment.Hide()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpevaluator_comments = Nothing
        End Try
    End Sub

    Protected Sub btnCloseActionPlanCommentAttachmentPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseActionPlanCommentAttachmentPopup.Click
        Try
            ClearActionPlanCommentPopupControls()
            ClearActionPlanCommentPopupData()

            mblnCommentAttachmentPopup = False
            popupCommentAttachment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Personal Analysis Competency Item
    Protected Sub lnkCompetencySelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompetencySelection.Click
        Try
            FillCompetencySelectionList()
            mblnCompetencySelectionFromList = True
            mblnPersonalAnalysisCompetencySelectionPopup = True
            popupCompetencySelection.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkCompetencySelectionReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompetencySelectionReset.Click
        Try
            mintPersonalAnalysisCompetencySelectedId = -1
            txtItemNameFreeText.Text = ""
            txtItemNameFreeText.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveCompetencySelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCompetencySelection.Click
        Dim objCompetency As New clsassess_competencies_master
        Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvCompetencySelection.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("rdbCompetencySelection"), RadioButton).Checked = True)

            If gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry, please select atleast one goal to continue."), Me.Page)
                Exit Sub
            End If


            If mblnCompetencySelectionFromList Then
                mintPersonalAnalysisCompetencySelectedId = gvCompetencySelection.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("rdbCompetencySelection"), RadioButton).Checked = True).Select(Function(x) CInt(CType(x.FindControl("hfValue"), HiddenField).Value)).FirstOrDefault()
                objCompetency._Competenciesunkid = mintPersonalAnalysisCompetencySelectedId
                txtItemNameFreeText.Text = objCompetency._Name
                txtItemNameFreeText.Enabled = False
            Else
                Dim dlItem As DataListItem = dgv_Citems.Items(mintListPersonalAnalysisSelectionDataListItemIndex)
                Dim hfCompetencyVal As HiddenField = CType(dlItem.FindControl("hfCompetencyVal"), HiddenField)
                hfCompetencyVal.Value = gvCompetencySelection.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("rdbCompetencySelection"), RadioButton).Checked = True).Select(Function(x) CInt(CType(x.FindControl("hfValue"), HiddenField).Value)).FirstOrDefault()
                objCompetency._Competenciesunkid = CInt(hfCompetencyVal.Value)
                Dim txtFreetext As TextBox = CType(dlItem.FindControl("txtFreetext"), TextBox)
                txtFreetext.Text = objCompetency._Name
                txtFreetext.Enabled = False
            End If




            mblnPersonalAnalysisCompetencySelectionPopup = False
            popupCompetencySelection.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub btnCloseCompetencySelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseCompetencySelection.Click
        Try
            mblnPersonalAnalysisCompetencySelectionPopup = False
            popupCompetencySelection.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReviewTrainings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReviewTrainings.Click
        Try
            FillReviewTrainingList()
            mblnReviewTrainingPopup = True
            popupReviewTraining.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseReviewTraining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseReviewTraining.Click
        Try
            mblnReviewTrainingPopup = False
            popupReviewTraining.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub btnApprovedLock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovedLock.Click
        Dim strMessage As String = ""
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objemployee_transfer_tran As New clsemployee_transfer_tran
        Dim objemployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim dsAllocationList As DataSet = Nothing
        Dim dsCategorizeList As DataSet = Nothing


        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvReviewTraining.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True And CBool(CType(x.FindControl("hfIsgrp"), HiddenField).Value) = False)


            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Please check at least one training to continue."), Me)
                Exit Sub
            End If


            Dim mdtReviewTraining = New DataTable
            mdtReviewTraining.Columns.Add("Trainingcourseunkid")
            mdtReviewTraining.Columns.Add("Trainingneedtranunkid")
            mdtReviewTraining.Columns.Add("IsFromCompetency")
            mdtReviewTraining.Columns.Add("CompetencyCategoryId")
            mdtReviewTraining.Columns.Add("Goal_Name")
            mdtReviewTraining.Columns.Add("Itemunkid")
            mdtReviewTraining.Columns.Add("Pdpgoalsmstunkid")




            objPdpgoals_master._TrainingAllocationid = CInt(Session("TrainingNeedAllocationID"))
            dsAllocationList = objemployee_transfer_tran.Get_Current_Allocation(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(cboEmployee.SelectedValue))

            dsCategorizeList = objemployee_categorization_Tran.Get_Current_Job(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(cboEmployee.SelectedValue))
            Dim intAllocationDepartment As Integer = -1

            If IsNothing(dsAllocationList) = False AndAlso dsAllocationList.Tables(0).Rows.Count > 0 AndAlso _
                IsNothing(dsCategorizeList) = False AndAlso dsCategorizeList.Tables(0).Rows.Count > 0 Then
                Select Case CInt(Session("TrainingNeedAllocationID"))
                    Case enAllocation.BRANCH
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("stationunkid"))
                    Case enAllocation.DEPARTMENT_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("deptgroupunkid"))
                    Case enAllocation.DEPARTMENT
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("departmentunkid"))
                    Case enAllocation.SECTION_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("sectiongroupunkid"))
                    Case enAllocation.SECTION
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("sectionunkid"))
                    Case enAllocation.UNIT_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("unitgroupunkid"))
                    Case enAllocation.UNIT
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("unitunkid"))
                    Case enAllocation.TEAM
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("teamunkid"))
                    Case enAllocation.JOB_GROUP
                        intAllocationDepartment = CInt(dsCategorizeList.Tables(0).Rows(0)("jobgroupunkid"))
                    Case enAllocation.JOBS
                        intAllocationDepartment = CInt(dsCategorizeList.Tables(0).Rows(0)("jobunkid"))
                    Case enAllocation.CLASS_GROUP
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("classgroupunkid"))
                    Case enAllocation.CLASSES
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("classunkid"))
                    Case Else
                        intAllocationDepartment = CInt(dsAllocationList.Tables(0).Rows(0)("departmentunkid"))
                End Select
            End If
            objPdpgoals_master._TrainingDepartmentunkid = intAllocationDepartment


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objPdpgoals_master._AuditUserId = CInt(Session("UserId"))
                objPdpgoals_master._Userunkid = CInt(Session("UserId"))
            Else
                objPdpgoals_master._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If
            objPdpgoals_master._ClientIP = CStr(Session("IP_ADD"))
            objPdpgoals_master._FormName = mstrModuleName
            objPdpgoals_master._FromWeb = True
            objPdpgoals_master._HostName = CStr(Session("HOST_NAME"))
            objPdpgoals_master._DatabaseName = CStr(Session("Database_Name"))
            objPdpgoals_master._Isvoid = False
            objPdpgoals_master._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objPdpgoals_master._Employeeunkid = CInt(cboEmployee.SelectedValue)

            If mblnIsAvailableInSuccession Then
                objPdpgoals_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Successor_PDP)
            ElseIf mblnIsAvailableInTalent Then
                objPdpgoals_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Talent_PDP)
            Else
                objPdpgoals_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Individual_Development_Plan)
            End If

            For Each dgRow As GridViewRow In gRow
                Dim hftrainingcourseunkid As HiddenField = CType(dgRow.FindControl("hftrainingcourseunkid"), HiddenField)
                Dim hfisfromcompetency As HiddenField = CType(dgRow.FindControl("hfisfromcompetency"), HiddenField)
                Dim hfitemunkid As HiddenField = CType(dgRow.FindControl("hfitemunkid"), HiddenField)
                Dim hftrainingneedtranunkid As HiddenField = CType(dgRow.FindControl("hftrainingneedtranunkid"), HiddenField)
                Dim hfgoalname As HiddenField = CType(dgRow.FindControl("hfgoalname"), HiddenField)
                Dim hfpdpgoalsmstunkid As HiddenField = CType(dgRow.FindControl("hfpdpgoalsmstunkid"), HiddenField)

                Dim drRow As DataRow = mdtReviewTraining.NewRow()
                drRow("Trainingcourseunkid") = CInt(hftrainingcourseunkid.Value)
                drRow("Trainingneedtranunkid") = CInt(hftrainingneedtranunkid.Value)
                drRow("IsFromCompetency") = CBool(hfisfromcompetency.Value)


                If CBool(hfisfromcompetency.Value) Then
                    Dim objpdpform_tran As New clspdpform_tran
                    Dim dsPDPItem As DataSet
                    dsPDPItem = objpdpform_tran.GetList("PDPItemData", True, CInt(hfitemunkid.Value))
                    'Gajanan [13-Apr-2021] -- Start
                    If IsNothing(dsPDPItem) = Nothing AndAlso dsPDPItem.Tables("PDPItemData").Rows.Count > 0 Then
                        If CBool(dsPDPItem.Tables("PDPItemData").Rows(0)("iscompetencyselection")) Then
                            Dim objassess_competencies_master As New clsassess_competencies_master
                            objassess_competencies_master._Competenciesunkid = CInt(dsPDPItem.Tables("PDPItemData").Rows(0)("fieldvalue"))
                            drRow("CompetencyCategoryId") = objassess_competencies_master._Competence_Categoryunkid
                        Else
                            drRow("CompetencyCategoryId") = -1
                        End If
                        objpdpform_tran = Nothing
                    End If

                    'Dim objassess_competencies_master As New clsassess_competencies_master
                    'objassess_competencies_master._Competenciesunkid = CInt(hfitemunkid.Value)
                Else
                    drRow("CompetencyCategoryId") = -1
                End If
                drRow("Goal_Name") = hfgoalname.Value
                drRow("Itemunkid") = hfitemunkid.Value
                drRow("Pdpgoalsmstunkid") = hfpdpgoalsmstunkid.Value
                mdtReviewTraining.Rows.Add(drRow)
            Next


            If objPdpgoals_master.ProcessReviewTraining(mdtReviewTraining, mintFormunkidId) = False Then
                DisplayMessage.DisplayMessage(objPdpgoals_master._Message, Me)
            Else

                objPdpgoals_master.SendEmails(clsPdpgoals_master.enEmailType.LOCK_PDP, Session("ArutiSelfServiceURL"), CInt(Session("CompanyUnkId")), _
                                              CInt(Session("UserId")), "", mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, _
                                              Session("UserName").ToString(), CInt(cboEmployee.SelectedValue), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("Fin_year")), _
                                              Session("EmployeeAsOnDate").ToString(), CStr(Session("UserAccessModeSetting")), CStr(Session("Database_Name")))

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 89, "Approved and locked successfully."), Me)
                mblnReviewTrainingPopup = False
                popupReviewTraining.Hide()

                FillPDPFormData()
                FillReviewTrainingList()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
            objemployee_transfer_tran = Nothing
            objemployee_categorization_Tran = Nothing
        End Try

    End Sub

    Protected Sub btnRejectTraining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRejectTraining.Click
        Dim strMessage As String = ""
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim objemployee_transfer_tran As New clsemployee_transfer_tran
        Dim objemployee_categorization_Tran As New clsemployee_categorization_Tran
        Dim dsAllocationList As DataSet = Nothing
        Dim dsCategorizeList As DataSet = Nothing


        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvReviewTraining.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True And CBool(CType(x.FindControl("hfIsgrp"), HiddenField).Value) = False)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 88, "Please check atleast one training to continue."), Me)
                Exit Sub
            End If



            delReason.Title = Language.getMessage(mstrModuleName, 90, "Please enter the Reason For Rejecting Training Course(s).")
            mintDeleteActionId = CInt(enDeleteAction.RejectTraining)
            delReason.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
            objemployee_transfer_tran = Nothing
            objemployee_categorization_Tran = Nothing
        End Try

    End Sub



#End Region

#Region "Griview Event"

    Protected Sub dlPersonalAnalysisGoalsCategory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPersonalAnalysisGoalsCategory.ItemDataBound
        Dim objpdpform_master As New clspdpform_master
        Dim objpdpitem_master As New clspdpitem_master
        Dim dtCustomTabularGrid As New DataTable
        Dim dsList As New DataSet

        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hfcategoryunkid As HiddenField = TryCast(e.Item.FindControl("hfcategoryunkid"), HiddenField)
                Dim hfcategoryViewtypeId As HiddenField = TryCast(e.Item.FindControl("hfcategoryViewtypeId"), HiddenField)
                Dim lnkAddPersonalDevlopmentGoalItem As LinkButton = TryCast(e.Item.FindControl("lnkAddPersonalDevlopmentGoalItem"), LinkButton)

                Dim dgvPersonalDevlopmentGoalItems As GridView = TryCast(e.Item.FindControl("dgvPersonalDevlopmentGoalItems"), GridView)
                Dim dlPersonalAnalysisGoalsItem As DataList = TryCast(e.Item.FindControl("dlPersonalAnalysisGoalsItem"), DataList)


                If hfcategoryViewtypeId.Value = CInt(enPDPCustomItemViewType.List) Then
                    lnkAddPersonalDevlopmentGoalItem.Visible = False
                    dsList = objpdpitem_master.GetList("PersonalDevlopmentGoalItems", CInt(hfcategoryunkid.Value))
                    If IsNothing(dsList) = False Then
                        dlPersonalAnalysisGoalsItem.DataSource = dsList.Tables("PersonalDevlopmentGoalItems")
                        dlPersonalAnalysisGoalsItem.DataBind()
                    End If
                    dgvPersonalDevlopmentGoalItems.Visible = False
                Else
                    dlPersonalAnalysisGoalsItem.Visible = False
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        lnkAddPersonalDevlopmentGoalItem.Visible = CBool(Session("AllowToAddPersonalAnalysisGoal"))
                    Else
                        If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                            lnkAddPersonalDevlopmentGoalItem.Visible = True
                        End If
                    End If

                    'S.SANDEEP |03-MAY-2021| -- START
                    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                    'dtCustomTabularGrid = objpdpform_master.Get_Custom_Items_Table(hfcategoryunkid.Value, CInt(cboEmployee.SelectedValue))
                    'Call Add_GridColumns(dgvPersonalDevlopmentGoalItems)
                    dtCustomTabularGrid = objpdpform_master.Get_Custom_Items_Table(hfcategoryunkid.Value, CInt(cboEmployee.SelectedValue), CStr(Session("Database_Name")))
                    Dim objCategory As New clspdpcategory_master
                    objCategory._Categoryunkid = CInt(hfcategoryunkid.Value)
                    If objCategory._Isincludeinpm = False Then
                    Call Add_GridColumns(dgvPersonalDevlopmentGoalItems)
                    Else
                        lnkAddPersonalDevlopmentGoalItem.Visible = False
                    End If
                    objCategory = Nothing
                    'S.SANDEEP |03-MAY-2021| -- END

                    dgvPersonalDevlopmentGoalItems.AutoGenerateColumns = False
                    Dim iColName As String = String.Empty
                    For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                        iColName = "" : iColName = "obj" & dCol.ColumnName
                        Dim dgvCol As New BoundField()
                        dgvCol.FooterText = iColName
                        dgvCol.ReadOnly = True
                        dgvCol.DataField = dCol.ColumnName
                        dgvCol.HeaderText = dCol.Caption
                        If dgvPersonalDevlopmentGoalItems.Columns.Contains(dgvCol) = True Then Continue For
                        If dCol.Caption.Length <= 0 Then
                            dgvCol.Visible = False
                        End If
                        dgvPersonalDevlopmentGoalItems.Columns.Add(dgvCol)
                        If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
                    Next
                    Call SetDateFormat()

                    dgvPersonalDevlopmentGoalItems.DataSource = dtCustomTabularGrid
                    dgvPersonalDevlopmentGoalItems.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


    Protected Sub gvActionPlanCommentAttachment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvActionPlanCommentAttachment.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If mblnIsAttachmentDownloadMode Then
                    gvActionPlanCommentAttachment.Columns(getColumnID_Griview(gvActionPlanCommentAttachment, "objcolhDownload", False)).Visible = True
                    gvActionPlanCommentAttachment.Columns(getColumnID_Griview(gvActionPlanCommentAttachment, "objcohDelete", False)).Visible = False
                Else
                    gvActionPlanCommentAttachment.Columns(getColumnID_Griview(gvActionPlanCommentAttachment, "objcolhDownload", False)).Visible = False
                    gvActionPlanCommentAttachment.Columns(getColumnID_Griview(gvActionPlanCommentAttachment, "objcohDelete", False)).Visible = True
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub gvExperience_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExperience.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            Dim lblEmpExpDate_from As Label = CType(e.Row.FindControl("lblEmpExpDate_from"), Label)
            Dim lblEmpExpDate_to As Label = CType(e.Row.FindControl("lblEmpExpDate_to"), Label)

            'S.SANDEEP |27-JAN-2022| -- START
            'ISSUE : DATE CONVERSION ISSUE
            'If lblEmpExpDate_from.Text.Length > 0 Then
            '    lblEmpExpDate_from.Text = eZeeDate.convertDate(lblEmpExpDate_from.Text).Date
            'End If

            'If lblEmpExpDate_to.Text.Length > 0 Then
            '    lblEmpExpDate_to.Text = eZeeDate.convertDate(lblEmpExpDate_to.Text).Date
            'End If

            If lblEmpExpDate_from.Text.Trim().Length > 0 Then
                lblEmpExpDate_from.Text = eZeeDate.convertDate(lblEmpExpDate_from.Text).Date
            End If

            If lblEmpExpDate_to.Text.Trim().Length > 0 Then
                lblEmpExpDate_to.Text = eZeeDate.convertDate(lblEmpExpDate_to.Text).Date
            End If
            'S.SANDEEP |24-JAN-2022| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvEmpPastTrainings_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpPastTrainings.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            Dim lblEnrollmentDate As Label = CType(e.Row.FindControl("lblEnrollmentDate"), Label)

            'S.SANDEEP |27-JAN-2022| -- START
            'ISSUE : DATE CONVERSION ISSUE
            If lblEnrollmentDate.Text.Trim().Length > 0 Then
                lblEnrollmentDate.Text = eZeeDate.convertDate(lblEnrollmentDate.Text).Date
            End If
            'S.SANDEEP |27-JAN-2022| -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvActionPlanGoalSelection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvActionPlanGoalSelection.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            Dim hfcategory As HiddenField = CType(e.Row.FindControl("hfcategory"), HiddenField)
            Dim hfitem As HiddenField = CType(e.Row.FindControl("hfitem"), HiddenField)
            Dim hfisgrp As HiddenField = CType(e.Row.FindControl("hfisgrp"), HiddenField)
            Dim rdbActionPlanGoalSelection As RadioButton = CType(e.Row.FindControl("rdbActionPlanGoalSelection"), RadioButton)


            If CInt(hfisgrp.Value) > 0 Then
                rdbActionPlanGoalSelection.Visible = False
                e.Row.BackColor = ColorTranslator.FromHtml("#E6E6E6")
                'If CInt(hfisgrp.Value) = 1 Then
                '    Me.AddGroup(e.Row, hfcategory.Value, gvActionPlanGoalSelection, CInt(hfisgrp.Value))
                'Else
                '    Me.AddGroup(e.Row, hfitem.Value, gvActionPlanGoalSelection, CInt(hfisgrp.Value))
                'End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub dgvViewTrainingList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvViewTrainingList.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            Dim lnkRemoveTrainingFromViewTrainingListItem As LinkButton = CType(e.Row.FindControl("lnkRemoveTrainingFromViewTrainingListItem"), LinkButton)

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                lnkRemoveTrainingFromViewTrainingListItem.Visible = CBool(Session("AllowToDeleteDevelopmentActionPlan"))
            Else
                If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                    lnkRemoveTrainingFromViewTrainingListItem.Visible = True
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


    Protected Sub dgvReviewTraining_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvReviewTraining.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            Dim hfcategory As HiddenField = CType(e.Row.FindControl("hfcategory"), HiddenField)
            Dim hfisgrp As HiddenField = CType(e.Row.FindControl("hfisgrp"), HiddenField)
            Dim ChkgvSelect As CheckBox = CType(e.Row.FindControl("ChkgvSelect"), CheckBox)


            If CInt(hfisgrp.Value) > 0 Then
                ChkgvSelect.Visible = False
                e.Row.BackColor = ColorTranslator.FromHtml("#E6E6E6")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region

#Region "Datalist Event"
    Protected Sub dgvActionPlanList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemIndex > -1 Then
                Dim hfpdpgoalsmstunkid As HiddenField = CType(e.Item.FindControl("hfpdpgoalsmstunkid"), HiddenField)
                Dim lnkActionPlanComment As LinkButton = CType(e.Item.FindControl("lnkActionPlanComment"), LinkButton)
                Dim lnkActionPlanUpdateProgress As LinkButton = CType(e.Item.FindControl("lnkActionPlanUpdateProgress"), LinkButton)
                Dim lnkActionPlanViewTraining As LinkButton = CType(e.Item.FindControl("lnkActionPlanViewTraining"), LinkButton)

                Dim lnkEditActionPlanItem As LinkButton = CType(e.Item.FindControl("lnkEditActionPlanItem"), LinkButton)
                Dim lnkDeleteActionPlanItem As LinkButton = CType(e.Item.FindControl("lnkDeleteActionPlanItem"), LinkButton)

                Dim txtactionPlanDueDate As Label = CType(e.Item.FindControl("txtactionPlanDueDate"), Label)
                Dim hfparentgoalunkid As HiddenField = CType(e.Item.FindControl("hfparentgoalunkid"), HiddenField)
                Dim pnlActionPlanItem As Panel = CType(e.Item.FindControl("pnlActionPlanItem"), Panel)
                Dim pnlLastUpdateDetail As Panel = CType(e.Item.FindControl("pnlLastUpdateDetail"), Panel)
                Dim txtactionPlanLastUpdateRemark As Label = CType(e.Item.FindControl("txtactionPlanLastUpdateRemark"), Label)
                Dim txtactionPlanLastUpdateDate As Label = CType(e.Item.FindControl("txtactionPlanLastUpdateDate"), Label)

                'S.SANDEEP |27-JAN-2022| -- START
                'ISSUE : DATE CONVERSION ISSUE
                If txtactionPlanLastUpdateDate.Text.Trim().Length > 0 Then
                    txtactionPlanLastUpdateDate.Text = eZeeDate.convertDate(txtactionPlanLastUpdateDate.Text)
                End If
                'S.SANDEEP |27-JAN-2022| -- END

                If txtactionPlanLastUpdateRemark.Text.Length > 0 Then
                    pnlLastUpdateDetail.Visible = True
                End If

                Dim hfGoalPoint As HiddenField = CType(e.Item.FindControl("hfGoalPoint"), HiddenField)
                Dim txtActionPlanProgress As Label = CType(e.Item.FindControl("txtActionPlanProgress"), Label)
                txtActionPlanProgress.Style.Add("width", CDbl(hfGoalPoint.Value).ToString() + "%")

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    lnkEditActionPlanItem.Visible = CBool(Session("AllowToEditDevelopmentActionPlan"))
                    lnkDeleteActionPlanItem.Visible = CBool(Session("AllowToDeleteDevelopmentActionPlan"))
                    lnkActionPlanUpdateProgress.Visible = CBool(Session("AllowToAddUpdateProgressForDevelopmentActionPlan"))
                    lnkActionPlanViewTraining.Visible = CBool(Session("AllowToViewActionPlanAssignedTrainingCourses"))
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkEditActionPlanItem.Visible = True
                        lnkDeleteActionPlanItem.Visible = True
                        lnkActionPlanUpdateProgress.Visible = True
                    End If
                End If


                GetEvalutorReviewer(CInt(hfpdpgoalsmstunkid.Value))
                If mintEvalutorTypeId > 0 OrElse mintReviewerUnkid > 0 Then
                    lnkActionPlanComment.Visible = True
                Else
                    lnkActionPlanComment.Visible = False
                End If


                Dim objPdpgoals_master As New clsPdpgoals_master
                If objPdpgoals_master.IsChildExist((hfpdpgoalsmstunkid.Value)) Then
                    lnkActionPlanComment.Visible = False
                    lnkActionPlanUpdateProgress.Visible = False
                End If
                objPdpgoals_master = Nothing

                If CInt(hfparentgoalunkid.Value) > 0 Then
                    pnlActionPlanItem.CssClass += " m-l-20"
                End If
                'S.SANDEEP |27-JAN-2022| -- START
                'ISSUE : DATE CONVERSION ISSUE
                If txtactionPlanDueDate.Text.Trim().Length > 0 Then
                    txtactionPlanDueDate.Text = eZeeDate.convertDate(txtactionPlanDueDate.Text).Date
                End If
                'S.SANDEEP |27-JAN-2022| -- END

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Protected Sub dlActionCategory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlActionCategory.ItemDataBound
        Dim objPdpgoals_master As New clsPdpgoals_master
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Try

            If e.Item.ItemIndex > -1 Then
                Dim hfactionplancategoryunkid As HiddenField = CType(e.Item.FindControl("hfactionplancategoryunkid"), HiddenField)
                Dim dgvActionPlanList As DataList = CType(e.Item.FindControl("dgvActionPlanList"), DataList)
                Dim lnkAddActionPlanItem As LinkButton = CType(e.Item.FindControl("lnkAddActionPlanItem"), LinkButton)


                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    lnkAddActionPlanItem.Visible = CBool(Session("AllowToAddDevelopmentActionPlan"))
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkAddActionPlanItem.Visible = True
                    End If
                End If

                AddHandler lnkAddActionPlanItem.Click, AddressOf Me.AddActionPlanItem
                dsList = objPdpgoals_master.GetActionPlanListByCategory("ActionItemList", mintFormunkidId, CInt(hfactionplancategoryunkid.Value), CInt(cboEmployee.SelectedValue))
                'dtTable = New DataView(dsList.Tables("ActionItemList"), "", "parentgoalunkid ,pdpgoalsmstunkid", DataViewRowState.CurrentRows).ToTable.Copy

                dgvActionPlanList.DataSource = dsList.Tables("ActionItemList")
                dgvActionPlanList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Protected Sub dlActionPlanComments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlActionPlanComments.ItemDataBound
        Dim objPdpevaluator_comments As New clsPdpevaluator_comments
        Try
            If e.Item.ItemIndex > -1 Then
                Dim lnkEditCommentItem As LinkButton = CType(e.Item.FindControl("lnkEditCommentItem"), LinkButton)
                Dim lnkDeleteCommentItem As LinkButton = CType(e.Item.FindControl("lnkDeleteCommentItem"), LinkButton)
                Dim lnkAddEditCommentAttachmentItem As LinkButton = CType(e.Item.FindControl("lnkAddEditCommentAttachmentItem"), LinkButton)
                Dim lnkDownloadCommentAttachmentItem As LinkButton = CType(e.Item.FindControl("lnkDownloadCommentAttachmentItem"), LinkButton)
                Dim pnlActionPlanCommentActionFooter As Panel = CType(e.Item.FindControl("pnlActionPlanCommentActionFooter"), Panel)


                Dim hdf_ActionPlanCommenttranunkid As HiddenField = CType(e.Item.FindControl("hdf_ActionPlanCommenttranunkid"), HiddenField)


                GetEvalutorReviewer(mintActionPlanItemUnkId)

                If e.Item.ItemType <> ListItemType.EditItem Then
                    objPdpevaluator_comments._Commenttranunkid = hdf_ActionPlanCommenttranunkid.Value


                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        If objPdpevaluator_comments._Evaluatorunkid = CInt(Session("E_Employeeunkid")) OrElse objPdpevaluator_comments._Evaluatorunkid = CInt(Session("UserId")) Then
                            lnkEditCommentItem.Visible = True
                            lnkDeleteCommentItem.Visible = True
                            'lnkAddEditCommentAttachmentItem.Visible = True
                            pnlActionPlanCommentActionFooter.Visible = True
                        Else
                            lnkEditCommentItem.Visible = False
                            lnkDeleteCommentItem.Visible = False
                            'lnkAddEditCommentAttachmentItem.Visible = False
                            pnlActionPlanCommentActionFooter.Visible = False
                        End If
                    Else
                        If objPdpevaluator_comments._Evaluatorunkid = CInt(Session("Employeeunkid")) Then
                            lnkEditCommentItem.Visible = True
                            lnkDeleteCommentItem.Visible = True
                            'lnkAddEditCommentAttachmentItem.Visible = True
                            pnlActionPlanCommentActionFooter.Visible = True
                        Else
                            lnkEditCommentItem.Visible = False
                            lnkDeleteCommentItem.Visible = False
                            'lnkAddEditCommentAttachmentItem.Visible = False
                            pnlActionPlanCommentActionFooter.Visible = False
                        End If
                    End If

                    'If mintEvalutorUnkid = CInt(Session("Employeeunkid")) Then
                    '    pnlActionPlanCommentActionFooter.Visible = True
                    '    lnkDownloadCommentAttachmentItem.Visible = True
                    'Else
                    '    lnkDownloadCommentAttachmentItem.Visible = False
                    'End If

                End If




            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpevaluator_comments = Nothing
        End Try
    End Sub

    Protected Sub dlPersonalAnalysisGoalsItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lnkAddPersonalDevlopmentGoalItem As LinkButton = TryCast(e.Item.FindControl("lnkAddPersonalDevlopmentGoalItem"), LinkButton)
                Dim dlPersonalAnalysisGoalsItemsList As DataList = TryCast(e.Item.FindControl("dlPersonalAnalysisGoalsItemsList"), DataList)
                Dim hfcategoryunkid As HiddenField = TryCast(e.Item.FindControl("hfcategoryunkid"), HiddenField)
                Dim hfitemunkid As HiddenField = TryCast(e.Item.FindControl("hfitemunkid"), HiddenField)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    lnkAddPersonalDevlopmentGoalItem.Visible = CBool(Session("AllowToAddPersonalAnalysisGoal"))
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkAddPersonalDevlopmentGoalItem.Visible = True
                    End If
                End If

                Dim objpdpform_master As New clspdpform_master
                Dim dsList As New DataSet
                dsList = objpdpform_master.Get_Custom_Items_List(CInt(hfcategoryunkid.Value), CInt(hfitemunkid.Value), (cboEmployee.SelectedValue))

                dlPersonalAnalysisGoalsItemsList.DataSource = dsList.Tables(0)
                dlPersonalAnalysisGoalsItemsList.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dlPersonalAnalysisGoalsItemsList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lnkPersonalAnalysisGoalsItemsListEdit As LinkButton = TryCast(e.Item.FindControl("lnkPersonalAnalysisGoalsItemsListEdit"), LinkButton)
                Dim lnkPersonalAnalysisGoalsItemsListDelete As LinkButton = TryCast(e.Item.FindControl("lnkPersonalAnalysisGoalsItemsListDelete"), LinkButton)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    lnkPersonalAnalysisGoalsItemsListEdit.Visible = CBool(Session("AllowToAddPersonalAnalysisGoal"))
                    lnkPersonalAnalysisGoalsItemsListDelete.Visible = CBool(Session("AllowToAddPersonalAnalysisGoal"))
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkPersonalAnalysisGoalsItemsListEdit.Visible = True
                        lnkPersonalAnalysisGoalsItemsListDelete.Visible = True
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Controls event"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click

        Dim blnFlag As Boolean = False
        Try
            If (delReason.Reason.Trim = "") Then
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If


            Select Case mintDeleteActionId
                Case CInt(enDeleteAction.DeleteLastUpdateProgress)
                    Dim objPdpgoals_update_Tran As New clsPdpgoals_update_Tran

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objPdpgoals_update_Tran._AuditUserId = CInt(Session("UserId"))
                        objPdpgoals_update_Tran._Userunkid = CInt(Session("UserId"))
                        objPdpgoals_update_Tran._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objPdpgoals_update_Tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                        objPdpgoals_update_Tran._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

                    End If
                    objPdpgoals_update_Tran._ClientIP = CStr(Session("IP_ADD"))
                    objPdpgoals_update_Tran._FormName = mstrModuleName
                    objPdpgoals_update_Tran._FromWeb = True
                    objPdpgoals_update_Tran._HostName = CStr(Session("HOST_NAME"))
                    objPdpgoals_update_Tran._DatabaseName = CStr(Session("Database_Name"))

                    objPdpgoals_update_Tran._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objPdpgoals_update_Tran._Voidreason = delReason.Reason.Trim
                    objPdpgoals_update_Tran._Isvoid = True

                    blnFlag = objPdpgoals_update_Tran.DeleteLastProgress(CInt(hfLastUpdateProgressId.Value), Nothing)

                    If blnFlag = False AndAlso objPdpgoals_update_Tran._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPdpgoals_update_Tran._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 54, "Last update progress data remove successfully."), Me)
                        FillDevlopmentActionPlanItems()
                        FillUpdateProgressInfo()
                    End If
                    objPdpgoals_update_Tran = Nothing

                Case CInt(enDeleteAction.DeleteActionPlanTrainingItem)
                    Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objPdpgoals_trainingneed_Tran._AuditUserId = CInt(Session("UserId"))
                        objPdpgoals_trainingneed_Tran._Userunkid = CInt(Session("UserId"))
                        objPdpgoals_trainingneed_Tran._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                        objPdpgoals_trainingneed_Tran._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

                    End If
                    objPdpgoals_trainingneed_Tran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objPdpgoals_trainingneed_Tran._ClientIP = CStr(Session("IP_ADD"))
                    objPdpgoals_trainingneed_Tran._FormName = mstrModuleName
                    objPdpgoals_trainingneed_Tran._FromWeb = True
                    objPdpgoals_trainingneed_Tran._HostName = CStr(Session("HOST_NAME"))
                    objPdpgoals_trainingneed_Tran._DatabaseName = CStr(Session("Database_Name"))

                    objPdpgoals_trainingneed_Tran._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objPdpgoals_trainingneed_Tran._Voidreason = delReason.Reason.Trim
                    objPdpgoals_trainingneed_Tran._Isvoid = True

                    blnFlag = objPdpgoals_trainingneed_Tran.Delete(mintDeleteActionPlanTrainingcourseunkid, mintTrainingCourseunkid, mintActionPlanItemUnkId, Nothing)

                    If blnFlag = False AndAlso objPdpgoals_trainingneed_Tran._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPdpgoals_trainingneed_Tran._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 42, "Training course removed successfully."), Me)
                        Call FillActionPlanAssignedTrainingList()
                        FillActionPlanTrainingList()
                        If FillActionPlanViewTrainingList() Then
                            mblnViewTrainingPopup = True
                            popup_ViewTrainingList.Show()
                        Else
                            mblnViewTrainingPopup = False
                            popup_ViewTrainingList.Hide()
                        End If
                    End If
                    mintDeleteActionPlanTrainingcourseunkid = -1
                    mintActionPlanItemUnkId = -1
                    mintTrainingCourseunkid = -1
                    objPdpgoals_trainingneed_Tran = Nothing


                Case CInt(enDeleteAction.RejectTraining)
                    Dim objPdpgoals_trainingneed_Tran As clsPdpgoals_trainingneed_Tran

                    Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                    gRow = dgvReviewTraining.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True And CBool(CType(x.FindControl("hfIsgrp"), HiddenField).Value) = False)


                    If gRow.Count > 0 Then

                        For Each dgRow As GridViewRow In gRow
                            Dim hftrainingcourseunkid As HiddenField = CType(dgRow.FindControl("hftrainingcourseunkid"), HiddenField)
                            Dim hftrainingneedtranunkid As HiddenField = CType(dgRow.FindControl("hftrainingneedtranunkid"), HiddenField)
                            Dim hfpdpgoalsmstunkid As HiddenField = CType(dgRow.FindControl("hfpdpgoalsmstunkid"), HiddenField)

                            objPdpgoals_trainingneed_Tran = New clsPdpgoals_trainingneed_Tran
                            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                                objPdpgoals_trainingneed_Tran._AuditUserId = CInt(Session("UserId"))
                                objPdpgoals_trainingneed_Tran._Userunkid = CInt(Session("UserId"))
                                objPdpgoals_trainingneed_Tran._Voiduserunkid = CInt(Session("UserId"))
                            Else
                                objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                                objPdpgoals_trainingneed_Tran._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

                            End If
                            objPdpgoals_trainingneed_Tran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            objPdpgoals_trainingneed_Tran._ClientIP = CStr(Session("IP_ADD"))
                            objPdpgoals_trainingneed_Tran._FormName = mstrModuleName
                            objPdpgoals_trainingneed_Tran._FromWeb = True
                            objPdpgoals_trainingneed_Tran._HostName = CStr(Session("HOST_NAME"))
                            objPdpgoals_trainingneed_Tran._DatabaseName = CStr(Session("Database_Name"))

                            objPdpgoals_trainingneed_Tran._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                            objPdpgoals_trainingneed_Tran._Voidreason = delReason.Reason.Trim
                            objPdpgoals_trainingneed_Tran._Isvoid = True

                            blnFlag = objPdpgoals_trainingneed_Tran.Delete(hftrainingneedtranunkid.Value, hftrainingcourseunkid.Value, hfpdpgoalsmstunkid.Value, Nothing)

                            If blnFlag = False AndAlso objPdpgoals_trainingneed_Tran._Message.Length > 0 Then
                                DisplayMessage.DisplayMessage(objPdpgoals_trainingneed_Tran._Message, Me)
                                Exit Sub
                            End If
                        Next
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 91, "Training rejected successfully."), Me)
                        FillReviewTrainingList()

                    End If


                   

                Case CInt(enDeleteAction.DeleteActionPlan)
                    Dim objPdpgoals_master As New clsPdpgoals_master
                    objPdpgoals_master._Pdpgoalsmstunkid = mintActionPlanItemUnkId

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objPdpgoals_master._AuditUserId = CInt(Session("UserId"))
                        objPdpgoals_master._Userunkid = CInt(Session("UserId"))
                        objPdpgoals_master._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objPdpgoals_master._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                        objPdpgoals_master._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

                    End If
                    objPdpgoals_master._ClientIP = CStr(Session("IP_ADD"))
                    objPdpgoals_master._FormName = mstrModuleName
                    objPdpgoals_master._FromWeb = True
                    objPdpgoals_master._HostName = CStr(Session("HOST_NAME"))
                    objPdpgoals_master._DatabaseName = CStr(Session("Database_Name"))

                    objPdpgoals_master._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objPdpgoals_master._Voidreason = delReason.Reason.Trim
                    objPdpgoals_master._Isvoid = True

                    objPdpgoals_master._Actionplancategoryunkid = mintActionPlanCategoryId
                    objPdpgoals_master._Pdpformunkid = mintFormunkidId
                    objPdpgoals_master._AuditDate = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime

                    If objPdpgoals_master._Parentgoalunkid > 0 Then
                        blnFlag = objPdpgoals_master.Delete()
                    Else
                        blnFlag = objPdpgoals_master.DeleteAllParentChildGoal(mintActionPlanItemUnkId)
                    End If

                    If blnFlag = False AndAlso objPdpgoals_master._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPdpgoals_master._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 43, "Action plan deleted successfully."), Me)
                        delReason.Reason = ""
                        ClearActionPlanIds()
                        FillDevlopmentActionPlanItems()
                    End If


                Case CInt(enDeleteAction.DeleteActionPlanComment)
                    Dim objPdpevaluator_comments As New clsPdpevaluator_comments
                    objPdpevaluator_comments._Commenttranunkid = mintActionPlanCommentItemunkid

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objPdpevaluator_comments._AuditUserId = CInt(Session("UserId"))
                        objPdpevaluator_comments._Userunkid = CInt(Session("UserId"))
                        objPdpevaluator_comments._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objPdpevaluator_comments._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                        objPdpevaluator_comments._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

                    End If
                    objPdpevaluator_comments._ClientIP = CStr(Session("IP_ADD"))
                    objPdpevaluator_comments._FormName = mstrModuleName
                    objPdpevaluator_comments._FromWeb = True
                    objPdpevaluator_comments._HostName = CStr(Session("HOST_NAME"))
                    objPdpevaluator_comments._DatabaseName = CStr(Session("Database_Name"))

                    objPdpevaluator_comments._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objPdpevaluator_comments._Voidreason = delReason.Reason.Trim
                    objPdpevaluator_comments._Isvoid = True

                    objPdpevaluator_comments._Pdpgoalsmstunkid = mintActionPlanItemUnkId

                    blnFlag = objPdpevaluator_comments.Delete()


                    If blnFlag = False AndAlso objPdpevaluator_comments._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPdpevaluator_comments._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 44, "Comment deleted successfully."), Me)
                        delReason.Reason = ""
                        mintActionPlanCommentItemunkid = -1
                        FillCommentList()
                    End If



                Case CInt(enDeleteAction.DeletePersonlaAnalysisGoal)
                    Dim objpdpform_tran As New clspdpform_tran

                    If mstrPersonalAnalysisEditGUID.Length > 0 Then
                        objpdpform_tran._Itemgrpguid = mstrPersonalAnalysisEditGUID
                        objpdpform_tran._Pdpformunkid = mintFormunkidId
                    End If

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objpdpform_tran._AuditUserId = CInt(Session("UserId"))
                        objpdpform_tran._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objpdpform_tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                    End If


                    objpdpform_tran._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objpdpform_tran._Voidreason = delReason.Reason.Trim
                    objpdpform_tran._Isvoid = True

                    objpdpform_tran._ClientIP = CStr(Session("IP_ADD"))
                    objpdpform_tran._FormName = mstrModuleName
                    objpdpform_tran._FromWeb = True
                    objpdpform_tran._HostName = CStr(Session("HOST_NAME"))
                    objpdpform_tran._DatabaseName = CStr(Session("Database_Name"))


                    If mstrPersonalAnalysisEditGUID.Length > 0 Then

                        If objpdpform_tran.isUsed(-1, mstrPersonalAnalysisEditGUID, True) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 55, "Sorry, this item is already in used."), Me)
                            ClearPersolnalGoalPopupData()
                            Exit Sub
                        End If

                    Else

                        If objpdpform_tran.isUsed(mintItemTranId) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 55, "Sorry, this item is already in used."), Me)
                            ClearPersolnalGoalPopupData()
                            Exit Sub
                        End If

                    End If



                    blnFlag = objpdpform_tran.Delete(mintItemTranId, mstrPersonalAnalysisEditGUID, mintCategoryId)

                    If blnFlag = False AndAlso objpdpform_tran._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objpdpform_tran._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 45, "Personal development goal deleted successfully."), Me)
                        ClearPersolnalGoalPopupData()
                        FillPersonalAnalysisItems()
                    End If
                    objpdpform_tran = Nothing

            End Select


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            delReason.Reason = ""
        End Try
    End Sub

    Protected Sub cfDelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cfDelete.buttonYes_Click
        Dim objScan_Attach_Documents As New clsScan_Attach_Documents
        Dim blnFlag As Boolean = False
        Try
            blnFlag = objScan_Attach_Documents.Delete(mintDeleteAttachmentId)

            If blnFlag = False AndAlso objScan_Attach_Documents._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objScan_Attach_Documents._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 46, "Deleted Successfully."), Me)
                mdtCommentAttachment = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.PDP, mintActionPlanCommentItemunkid, CStr(Session("Document_Path")))
                gvActionPlanCommentAttachment.DataSource = mdtCommentAttachment
                gvActionPlanCommentAttachment.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objScan_Attach_Documents = Nothing
        End Try
    End Sub

    Protected Sub cfConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cfConfirmation.buttonYes_Click
        Dim blnFlag As Boolean = False
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
        Try
            Select Case mintConfirmationActionId
                Case CInt(enConfirmationAction.ResetActionPlanSelection)


                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objPdpgoals_trainingneed_Tran._AuditUserId = CInt(Session("UserId"))
                        objPdpgoals_trainingneed_Tran._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                    End If
                    objPdpgoals_trainingneed_Tran._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objPdpgoals_trainingneed_Tran._Voidreason = Language.getMessage(mstrModuleName, 112, "AutoDelete: Action Plan Goal By User")
                    objPdpgoals_trainingneed_Tran._Isvoid = True

                    objPdpgoals_trainingneed_Tran._ClientIP = CStr(Session("IP_ADD"))
                    objPdpgoals_trainingneed_Tran._FormName = mstrModuleName
                    objPdpgoals_trainingneed_Tran._FromWeb = True
                    objPdpgoals_trainingneed_Tran._HostName = CStr(Session("HOST_NAME"))
                    objPdpgoals_trainingneed_Tran._DatabaseName = CStr(Session("Database_Name"))

                    blnFlag = objPdpgoals_trainingneed_Tran.DeleteAll(mintActionPlanItemUnkId)
                    If blnFlag = False AndAlso objPdpgoals_trainingneed_Tran._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPdpgoals_trainingneed_Tran._Message, Me)
                    Else
                        ResetActionplanfromSelection()
                        FillActionPlanTrainingList()
                        FillActionPlanAssignedTrainingList()
                    End If

                Case CInt(enConfirmationAction.ChangeActionPlanGoalFromSelection)

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objPdpgoals_trainingneed_Tran._AuditUserId = CInt(Session("UserId"))
                        objPdpgoals_trainingneed_Tran._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                    End If
                    objPdpgoals_trainingneed_Tran._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objPdpgoals_trainingneed_Tran._Voidreason = Language.getMessage(mstrModuleName, 112, "AutoDelete: Action Plan Goal By User")
                    objPdpgoals_trainingneed_Tran._Isvoid = True

                    objPdpgoals_trainingneed_Tran._ClientIP = CStr(Session("IP_ADD"))
                    objPdpgoals_trainingneed_Tran._FormName = mstrModuleName
                    objPdpgoals_trainingneed_Tran._FromWeb = True
                    objPdpgoals_trainingneed_Tran._HostName = CStr(Session("HOST_NAME"))
                    objPdpgoals_trainingneed_Tran._DatabaseName = CStr(Session("Database_Name"))

                    blnFlag = objPdpgoals_trainingneed_Tran.DeleteAll(mintActionPlanItemUnkId)
                    If blnFlag = False AndAlso objPdpgoals_trainingneed_Tran._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPdpgoals_trainingneed_Tran._Message, Me)
                    Else
                        FillActionPlanTrainingList()
                        FillActionPlanAssignedTrainingList()
                        SetActionplanfromSelection()
                    End If

            End Select


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_trainingneed_Tran = Nothing
        End Try
    End Sub
#End Region

    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgv_Citems.ItemDataBound

        Try

            If e.Item.ItemIndex > -1 Then
                Dim hfitemtypeid As HiddenField = CType(e.Item.FindControl("hfitemtypeid"), HiddenField)
                Dim hfselectedid As HiddenField = CType(e.Item.FindControl("hfselectedid"), HiddenField)
                Dim hfselectionmodeid As HiddenField = CType(e.Item.FindControl("hfselectionmodeid"), HiddenField)
                Dim pnlCompetencySelection As Panel = CType(e.Item.FindControl("pnlCompetencySelection"), Panel)
                Dim hfiscompetencyselectionset As HiddenField = CType(e.Item.FindControl("hfiscompetencyselectionset"), HiddenField)


                If CBool(hfiscompetencyselectionset.Value) Then
                    pnlCompetencySelection.Visible = True
                Else
                    pnlCompetencySelection.Visible = False
                End If

                Dim hfddate As HiddenField = CType(e.Item.FindControl("hfddate"), HiddenField)
                If CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    If e.Item.FindControl("txtFreetext") IsNot Nothing Then
                        Dim txt As TextBox = CType(e.Item.FindControl("txtFreetext"), TextBox)
                        txt.Visible = True
                        'If CBool(e.Row.Cells(3).Text) Or CBool(e.Row.Cells(6).Text) Then txt.ReadOnly = True
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If e.Item.FindControl("dtpSelection") IsNot Nothing Then
                        Dim dtp As Controls_DateCtrl = CType(e.Item.FindControl("dtpSelection"), Controls_DateCtrl)
                        dtp.Visible = True
                        If hfddate.Value.Trim.Length > 0 AndAlso _
                           hfddate.Value.Trim <> "&nbsp;" Then
                            dtp.SetDate = CDate(hfddate.Value)
                        End If
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    If e.Item.FindControl("txtNUM") IsNot Nothing Then
                        Dim txt As Controls_NumericTextBox = CType(e.Item.FindControl("txtNUM"), Controls_NumericTextBox)
                        txt.Visible = True
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.SELECTION Then
                    If e.Item.FindControl("cboSelection") IsNot Nothing Then
                        Dim cbo As DropDownList = CType(e.Item.FindControl("cboSelection"), DropDownList)

                        cbo.Visible = True
                        Select Case CInt(hfselectionmodeid.Value)
                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                Dim dsList As New DataSet
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing

                                'If CBool(IIf(e.Row.Cells(7).Text = "&nbsp;", 0, e.Row.Cells(7).Text)) = False Then
                                '    Dim objCMaster As New clsCommon_Master
                                '    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                '    objCMaster = Nothing
                                'Else
                                '    Dim objEvalCItem As New clsevaluation_analysis_master
                                '    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                '    objEvalCItem = Nothing
                                'End If

                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                            Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES
                                Dim dtab As DataTable = Nothing
                                Dim dsList As New DataSet

                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing

                                'If CBool(IIf(e.Row.Cells(7).Text = "&nbsp;", 0, e.Row.Cells(7).Text)) = False Then
                                '    Dim objCMaster As New clsCommon_Master
                                '    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                '    objCMaster = Nothing
                                'Else
                                '    Dim objEvalCItem As New clsevaluation_analysis_master
                                '    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                '    objEvalCItem = Nothing
                                'End If

                                If CInt(hfselectionmodeid.Value) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                                ElseIf CInt(hfselectionmodeid.Value) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                                End If
                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dtab
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                Dim objCompetency As New clsassess_competencies_master
                                Dim intPeriod As Integer = -1
                                Dim objpdpitem_master As New clspdpitem_master
                                intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))

                                Dim dsList As New DataSet
                                dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), intPeriod, _
                                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
                                With cbo
                                    .DataValueField = "Id"
                                    .DataTextField = "Name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                                objCompetency = Nothing
                                objpdpitem_master = Nothing

                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                Dim objEmpField1 As New clsassess_empfield1_master
                                Dim intPeriod As Integer = -1
                                Dim objpdpitem_master As New clspdpitem_master
                                intPeriod = objpdpitem_master.GetAssesmentPeriod(Session("Database_Name"), CInt(Session("CompanyUnkId")))
                                Dim dsList As New DataSet

                                dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), intPeriod, "List", True, True)
                                With cbo
                                    .DataValueField = "Id"
                                    .DataTextField = "Name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                                objEmpField1 = Nothing
                                objpdpitem_master = Nothing

                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM

                                Dim dsList As New DataSet

                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                                objCMaster = Nothing

                                'If CBool(IIf(e.Row.Cells(7).Text = "&nbsp;", 0, e.Row.Cells(7).Text)) = False Then
                                '    Dim objCMaster As New clsCommon_Master
                                '    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                                '    objCMaster = Nothing
                                'Else
                                '    Dim objEvalCItem As New clsevaluation_analysis_master
                                '    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                '    objEvalCItem = Nothing
                                'End If

                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                        End Select
                        If hfselectedid.Value.Length > 0 AndAlso _
                           hfselectedid.Value.Trim <> "&nbsp;" Then
                            cbo.SelectedValue = CInt(hfselectedid.Value)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvPersonalDevlopmentGoalItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then


                Dim imgEdit As New LinkButton
                imgEdit.ID = "imgEdit"
                imgEdit.CommandName = "objEdit"
                imgEdit.ToolTip = "Edit"
                imgEdit.CssClass = "lnEdit"
                imgEdit.Text = "<i class='fas fa-pencil-alt text-primary'></i>"

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    imgEdit.Visible = CBool(Session("AllowToEditPersonalAnalysisGoal")) '
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        imgEdit.Visible = True
                    End If
                End If


                AddHandler imgEdit.Click, AddressOf Me.OnGvEditPersonalDevlopmentGoalItem
                e.Row.Cells(0).Controls.Add(imgEdit)

                Dim imgDelete As New LinkButton
                imgDelete.ID = "imgDelete"
                imgDelete.CommandName = "objDelete"
                imgDelete.ToolTip = "Delete"
                imgDelete.CssClass = "lnDelt"
                imgDelete.Text = "<i class='fas fa-trash text-danger'></i>"

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    imgDelete.Visible = CBool(Session("AllowToDeletePersonalAnalysisGoal"))
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        imgDelete.Visible = True
                    End If
                End If

                AddHandler imgDelete.Click, AddressOf Me.OnGvDeletePersonalDevlopmentGoalItem

                e.Row.Cells(1).Controls.Add(imgDelete)

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SaveTablePersonalFormMaster(ByVal DtCategoryItems As DataTable)
        Dim objpdpform_master As New clspdpform_master
        Dim blnFlag As Boolean = False

        Try
            Dim xGuid As String = ""
            If mstrPersonalAnalysisEditGUID.Length > 0 Then
                objpdpform_master._Pdpformunkid = mintFormunkidId
                xGuid = mstrPersonalAnalysisEditGUID
            Else
                xGuid = Guid.NewGuid().ToString()
                objpdpform_master._Pdpformunkid = mintFormunkidId
            End If
            objpdpform_master._Itemgrpguid = xGuid


            objpdpform_master._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objpdpform_master._Nextjobid = CInt(cboNextJob.SelectedValue)

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpdpform_master._AuditUserId = CInt(Session("UserId"))
            Else
                objpdpform_master._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            objpdpform_master._ClientIP = CStr(Session("IP_ADD"))
            objpdpform_master._FormName = mstrModuleName
            objpdpform_master._FromWeb = True
            objpdpform_master._HostName = CStr(Session("HOST_NAME"))
            objpdpform_master._DatabaseName = CStr(Session("Database_Name"))
            objpdpform_master._Isvoid = False


            objpdpform_master._Categoryid = mintCategoryId
            objpdpform_master._Itemtranunkid = mintItemTranId
            objpdpform_master._ViewType = enPDPCustomItemViewType.Table
            objpdpform_master._IsAddItemData = True
            objpdpform_master._DtPersonalAnalysisGoalItems = DtCategoryItems



            If mblnIsAddItemData Then

                Dim iCount As Integer = 0
                For Each drrow As DataRow In DtCategoryItems.Rows
                    Dim objpdpform_tran As New clspdpform_tran
                    objpdpform_tran._Fieldvalue = drrow("fieldvalue").ToString()
                    If objpdpform_tran.isValueExist(drrow("fieldvalue").ToString(), xGuid, mintCategoryId, CInt(drrow("itemunkid").ToString()), mintFormunkidId) Then
                        iCount += 1
                    End If
                Next

                If iCount = DtCategoryItems.Rows.Count Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Item value is already existing."), Me)
                    ClearPersolnalGoalPopupData()
                    Exit Sub
                End If


            End If



            blnFlag = objpdpform_master.SavePDPEmployeeData()

            If blnFlag = False AndAlso objpdpform_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpdpform_master._Message, Me)
            Else
                mintFormunkidId = objpdpform_master._Pdpformunkid
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 48, "Saved Successfully."), Me)
                ClearPersolnalGoalPopupData()
                FillPersonalAnalysisItems()

                If mintFormunkidId <= 0 Then
                    mintFormunkidId = objpdpform_master._Pdpformunkid
                End If

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpform_master = Nothing
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView, ByVal grpType As Integer)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)


            If grpType = 1 Then
                row.BackColor = ColorTranslator.FromHtml("#E6E6E6")
            Else
                row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
            End If

            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Add_GridColumns(ByVal dgvPersonalDevlopmentGoalItems As GridView)
        Try
            Dim iTempField As New TemplateField()

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objEdit"
            iTempField.HeaderText = "Edit"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvPersonalDevlopmentGoalItems.Columns.Count > 0 Then
                If dgvPersonalDevlopmentGoalItems.Columns(0).FooterText <> iTempField.FooterText Then
                    dgvPersonalDevlopmentGoalItems.Columns.Add(iTempField)
                End If
            Else
                dgvPersonalDevlopmentGoalItems.Columns.Add(iTempField)
            End If

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objDelete"
            iTempField.HeaderText = "Delete"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvPersonalDevlopmentGoalItems.Columns.Count > 1 Then
                If dgvPersonalDevlopmentGoalItems.Columns(1).FooterText <> iTempField.FooterText Then
                    dgvPersonalDevlopmentGoalItems.Columns.Add(iTempField)
                End If
            Else
                dgvPersonalDevlopmentGoalItems.Columns.Add(iTempField)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
