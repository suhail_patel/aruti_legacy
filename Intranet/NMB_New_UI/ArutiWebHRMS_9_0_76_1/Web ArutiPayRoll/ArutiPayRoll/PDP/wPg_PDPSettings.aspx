﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_PDPSettings.aspx.vb"
    Inherits="PDP_wPg_PDPSettings" Title="Learning And Development Plan Setup" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ColorPickerTextbox.ascx" TagName="ColorPickerTextbox"
    TagPrefix="colt" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Development Plan Settings"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#PDPInstruction" data-toggle="tab">
                                        <asp:Label ID="lblPDPInstruction" runat="server" Text="Instructions"></asp:Label>
                                    </a></li>
                                    <li role="presentation" style="display: none"><a href="#ReviewerLevels" data-toggle="tab">
                                        <asp:Label ID="lblReviewerLevels" runat="server" Text="Reviewer Levels"></asp:Label>
                                    </a></li>
                                    <li role="presentation" ><a href="#Evaluators" data-toggle="tab">
                                        <asp:Label ID="lblEvaluators" runat="server" Text="Feedback"></asp:Label>
                                    </a></li>
                                    <li role="presentation" style="display: none"><a href="#Reviewers" data-toggle="tab">
                                        <asp:Label ID="lblReviewers" runat="server" Text="Reviewers"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PDPCategories" data-toggle="tab">
                                        <asp:Label ID="lblPDPCategories" runat="server" Text="Parameters"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PDPItems" data-toggle="tab">
                                        <asp:Label ID="lblPDPItems" runat="server" Text="Items"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PDPCategoryItemMapping" data-toggle="tab">
                                        <asp:Label ID="Label1" runat="server" Text="Parameter/Item Mapping"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#ActionPlanCategories" data-toggle="tab">
                                        <asp:Label ID="lblActionPlanCategories" runat="server" Text="Action Plan Categories"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="PDPInstruction">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblInstructionHeader" runat="server" Text="Instruction"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtInstruction" runat="server" CssClass="form-control" Rows="3"
                                                                            TextMode="MultiLine" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnInstruction" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="ReviewerLevels">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblLLevel" runat="server" Text="Add/Edit Reviewer Level"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLlevelcode" runat="server" Text="Level Code" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtLlevelcode" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLlevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtLlevelname" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLlevelpriority" runat="server" Text="Priority" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtLlevelpriority" runat="server" Type="Numeric" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnLSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnLReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvReviewerLevel" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="levelunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="LEdit" runat="server" ToolTip="Edit" OnClick="lnkLEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="LDelete" runat="server" ToolTip="Delete" OnClick="lnkLDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="levelcode" HeaderText="Level Code" ReadOnly="True" FooterText="colhlevelcode">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="levelname" HeaderText="Level Name" ReadOnly="True" FooterText="colhLevelName">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="priority" HeaderText="Priority" ReadOnly="True" FooterText="colhpriority">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Evaluators">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblEEvaluators" runat="server" Text="The following can give feedback/comment on employee development progress"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="body">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <asp:CheckBox ID="chkSelf" runat="server" Text="Self" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <asp:CheckBox ID="chkLineManager" runat="server" Text="Line Manager" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix" style="display:none">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="card inner-card">
                                                                                            <div class="header">
                                                                                                <h2>
                                                                                                    <asp:CheckBox ID="chkPeers" runat="server" Text="Peers are employees in the same"
                                                                                                        AutoPostBack="true" />
                                                                                                </h2>
                                                                                            </div>
                                                                                            <asp:Panel ID="pnlPeers" runat="server" CssClass="body" Enabled="false">
                                                                                                <div class="row clearfix">
                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                        <asp:Label ID="lblPeerTitle" runat="server" Text="Who are the Peers?"></asp:Label>
                                                                                                        <div class="form-group">
                                                                                                            <asp:DropDownList ID="drpAllocationBy" runat="server" AutoPostBack="true">
                                                                                                            </asp:DropDownList>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnESave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Reviewers">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblRReviewer" runat="server" Text="Add/Edit Reviewer"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="display: none">
                                                                <asp:Label ID="lblRLevel" Text="Level" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpRLevel" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblUser" Text="User" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpRUser" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="display: none">
                                                                <asp:Label ID="lblAllocation" Text="Allocation" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpRAllocation" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="bntRSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="bntRReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvReviewers" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="reviewermstunkid,isactive">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="REdit" runat="server" ToolTip="Edit" OnClick="lnkREdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="RDelete" runat="server" ToolTip="Delete" OnClick="lnkRDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="RActive" runat="server" ToolTip="Active" OnClick="RActive_Click">
                                                                                        <i class="fas fa-user-check text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="RInActive" runat="server" ToolTip="Inactive" OnClick="RInActive_Click">
                                                                                        <i class="fas fa-user-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="level" HeaderText="Level" ReadOnly="True" Visible="false"
                                                                                FooterText="colhRLevel"></asp:BoundField>
                                                                            <asp:BoundField DataField="name" HeaderText="User" ReadOnly="True" FooterText="colhRUser">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Allocation" HeaderText="Allocation" ReadOnly="True" Visible="false"
                                                                                FooterText="colhAllocation"></asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PDPCategories">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblPDPCategory" runat="server" Text="Add/Edit Parameters"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPCategory" Text="Parameters" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtPCategory" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblParameterViewType" Text="Parameter Type" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpParameterViewType" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPSortOrder" Text="Sort Order" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtPSortOrder" runat="server" Type="Numeric" Max="9999" />
                                                            </div>
                                                        </div>
                                                        <%--'S.SANDEEP |03-MAY-2021| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : PDP_PM_LINKING--%>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIncludeInPM" runat="server" Text="Include this parameter in Custom Header while doing assessment." />
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkMakeMandatory" runat="server" Text="Make this parameter as mandatory while doing assessment." />
                                                            </div>
                                                        </div>
                                                        <%--'S.SANDEEP |03-MAY-2021| -- END--%>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnPSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnPReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="categoryunkid,viewtype,cdel">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkPEdit" runat="server" ToolTip="Edit" OnClick="lnkPEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkPDelete" runat="server" ToolTip="Delete" OnClick="lnkPDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="category" HeaderText="Parameters" ReadOnly="True" FooterText="colhPcategory">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="viewtype" HeaderText="Parameter Type" ReadOnly="True"
                                                                                FooterText="colhPviewtype"></asp:BoundField>
                                                                            <asp:BoundField DataField="SortOrder" HeaderText="Sort Order" ReadOnly="True" FooterText="colhPSortOrder">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PDPItems">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblIPDPItem" runat="server" Text="Add/Edit Item"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblICategory" Text="Parameter" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpICategory" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIItemType" Text="Item Type" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpIItemType" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:CheckBox ID="chkIIsCompetencySelectionset" runat="server" Text="Provide Competency Selection" />
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-20">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpISection" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblISortOrder" Text="Sort Order" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtISortOrder" runat="server" Type="Numeric" Max="9999" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIItemText" Text="Item Text" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtIItemText" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--'S.SANDEEP |03-MAY-2021| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : PDP_PM_LINKING--%>
                                                        <asp:Panel ID="pnlvisibility" runat="server" Width="100%" Visible="false">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblItemVisibilty" Text="Item Applicable to" runat="server" CssClass="form-label" />
                                                                    <asp:RadioButtonList ID="radItemvisiblity" RepeatDirection="Horizontal" runat="server">
                                                                        <asp:ListItem Selected="True" Text="Employee" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Assessor" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="Reviewer" Value="3"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <%--'S.SANDEEP |03-MAY-2021| -- END--%>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnISave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnIReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvPDPItem" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="itemunkid,idel">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkIEdit" runat="server" ToolTip="Edit" OnClick="lnkIEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkIDelete" runat="server" ToolTip="Delete" OnClick="lnkIDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="category" HeaderText="Parameter" ReadOnly="True" FooterText="colhICategory">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="item" HeaderText="Item" ReadOnly="True" FooterText="colhIItem">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="itemtype" HeaderText="Item Type" ReadOnly="True" FooterText="colhIItemtype">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="selectionmode" HeaderText="Selection Mode" ReadOnly="True"
                                                                                FooterText="colhISelectionmode"></asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Provide Competency Selection" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hfiscompetencyselectionset" runat="server" Value='<%# Eval("iscompetencyselectionset") %>' />
                                                                                    <asp:Label ID="lblIscompetencyselectionsetTrue" runat="server" Text="" Visible="false"
                                                                                        CssClass="fas fa-check-circle text-success" />
                                                                                    <asp:Label ID="lblIscompetencyselectionsetFalse" runat="server" Text="" Visible="false"
                                                                                        CssClass="fas fa-times-circle text-danger " />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="SortOrder" HeaderText="Sort Order" ReadOnly="True" FooterText="colhISortOrder">
                                                                            </asp:BoundField>
                                                                            <%--'S.SANDEEP |03-MAY-2021| -- START--%>
                                                                            <%--'ISSUE/ENHANCEMENT : PDP_PM_LINKING--%>
                                                                            <asp:BoundField DataField="ivisibleto" HeaderText="Applicable To" ReadOnly="True" FooterText="colhivisibleto">
                                                                            </asp:BoundField>
                                                                            <%--'S.SANDEEP |03-MAY-2021| -- END--%>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PDPCategoryItemMapping">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="Label2" runat="server" Text="Parameter/Item Mapping"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCategoryItemMappingCategory" Text="Parameter" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpCategoryItemMappingCategory" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCategoryItemMappingItems" Text="Item" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpCategoryItemMappingItems" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnCategoryItemMappingSave" CssClass="btn btn-primary" runat="server"
                                                            Text="Save" />
                                                        <asp:Button ID="btnCategoryItemMappingReset" CssClass="btn btn-default" runat="server"
                                                            Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvPDPCIMapping" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="categoryitemmappingid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkCIEdit" runat="server" ToolTip="Edit" OnClick="lnkCIEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkCIDelete" runat="server" ToolTip="Delete" OnClick="lnkCIDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="category" HeaderText="Parameter" ReadOnly="True" FooterText="colhICategory">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="item" HeaderText="Item" ReadOnly="True" FooterText="colhIItem">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="ActionPlanCategories">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblActionPlanCategory" runat="server" Text="Add/Edit Action Plan Category"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblACategory" Text="Category" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtACategory" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblASortOrder" Text="Sort Order" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtASortOrder" runat="server" Type="Numeric" Max="9999" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnASave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnAReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvActionPlanCategory" runat="server" AutoGenerateColumns="false"
                                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="actionplancategoryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkAEdit" runat="server" ToolTip="Edit" OnClick="lnkAEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkADelete" runat="server" ToolTip="Delete" OnClick="lnkADelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="category" HeaderText="Category" ReadOnly="True" FooterText="colhAcategory">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SortOrder" HeaderText="Sort Order" ReadOnly="True" FooterText="colhASortOrder">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <asp:HiddenField ID="TabName" runat="server" />
                <cnf:Confirmation ID="cnfConfirmationMapping" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
	    $(document).ready(function()
		{
		    RetriveTab();
            
        });
         function RetriveTab() {
 
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "PDPInstruction";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        debugger;
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
}
    </script>

</asp:Content>
