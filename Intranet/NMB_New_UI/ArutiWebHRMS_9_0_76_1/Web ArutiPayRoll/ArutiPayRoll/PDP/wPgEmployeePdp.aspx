﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmployeePdp.aspx.vb"
    Inherits="PDP_wPgEmployeePdp" Title="Learning and Development Form" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirm" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        
        $(document).ready(function() {
           maintainCommentScrollPosition();
        });
    
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
			RetriveCollapse();
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            maintainCommentScrollPosition();
        }
        
        function IsValidAttach() {
            return true;
        }   
        
         function maintainCommentScrollPosition() {
			$("#dvCommentScroll").scrollTop = parseInt($('#<%=hfCommentScrollPosition.ClientID%>').val());
        }
        function setCommentScrollPosition(scrollValue) {
            $('#<%=hfCommentScrollPosition.ClientID%>').val(scrollValue);
        }    
        
        $("body").on("click", "[id*=chkAllSelect]", function() {
           var chkHeader = $(this);
           var grid = $(this).closest(".table");
           $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
       });

       $("body").on("click", "[id*=chkSelect]", function() {
           var grid = $(this).closest(".table");
           var chkHeader = $("[id*=chkAllSelect]", grid);
           if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                   chkHeader.prop("checked", true);
               }
               else {
                   chkHeader.prop("checked", false);
               }
       });
       
	   
        $("body").on("click", "[id*=ChkAll]", function() {
           var chkHeader = $(this);
           var grid = $(this).closest(".table");
           $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
       });

       $("body").on("click", "[id*=ChkgvSelect]", function() {
           var grid = $(this).closest(".table");
           var chkHeader = $("[id*=ChkAll]", grid);
           if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                   chkHeader.prop("checked", true);
               }
               else {
                   chkHeader.prop("checked", false);
               }
       });	   
	   
	   
       
       function RadioCheck(rb) {
           
        var gv = document.getElementById("<%=gvActionPlanGoalSelection.ClientID%>");
        var rbs = gv.getElementsByTagName("input");
        var row = rb.parentNode.parentNode;
        for (var i = 0; i < rbs.length; i++) {
            if (rbs[i].type == "radio") {
                if (rbs[i].checked && rbs[i] != rb) {
                    rbs[i].checked = false;
                    break;
                }
            }
        }
    }    
    
       function RadioCompetencyCheck(rb) {
           
        var gv = document.getElementById("<%=gvCompetencySelection.ClientID%>");
        var rbs = gv.getElementsByTagName("input");
        var row = rb.parentNode.parentNode;
        for (var i = 0; i < rbs.length; i++) {
            if (rbs[i].type == "radio") {
                if (rbs[i].checked && rbs[i] != rb) {
                    rbs[i].checked = false;
                    break;
                }
            }
        }
    }    
    
   function IsValidAttach() {
            return true;
   }    
        
    
    function gridsearching(ctrl) {
            var txtSearch = $(ctrl);
            
            switch (txtSearch[0].id) {
            
                case 'txtTrainingSearch':
                    if ($(txtSearch).val().length > 0) {
                        $('#<%=dgvActionPlanTrainingList.ClientID %> tbody tr').hide();
                        $('#<%=dgvActionPlanTrainingList.ClientID %> tbody tr:first').show();
                        $('#<%=dgvActionPlanTrainingList.ClientID %> tbody tr td:containsnocase(\'' + $(txtSearch).val() + '\')').parent().show();
                    }
                    else if ($(txtSearch).val().length == 0) {
                        $('#txtTrainingSearch').val('');
                        $('#<%=dgvActionPlanTrainingList.ClientID %> tr').show();
                        $('#txtTrainingSearch').focus();
                    }
                    break;     
            }
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblMainHeader" runat="server" Text="Employee Personal Development Form"></asp:Label>
                                </h2>
                                <ul class="header-dropdown">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkPdpInstruction" runat="server" ToolTip="Personal Development Instruction">
                                                <i class="fas fa-info-circle"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlNoEmployeeSelected" runat="server" CssClass="footer text-left">
                                <asp:Label ID="lblNoEmployeeSelected" CssClass="label label-info" Text="Please select employee to continue"
                                    runat="server" />
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlData" runat="server" CssClass="row clearfix" Visible="false">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card profile-detail">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#Employee_Detail" aria-controls="Employee_Detail"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPDPEmployeeDetailTab" runat="server" Text="Employee Detail"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#CustomItem" aria-controls="CustomItem" role="tab"
                                        data-toggle="tab">
                                        <asp:Label ID="lblPDPPersonalAnalysisandGoalsTab" runat="server" Text="Personal Analysis and Goals"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#DevelopmentActionplan" aria-controls="DevelopmentActionplan"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPDPDevelopmentActionplanTab" runat="server" Text="Development Action plan"></asp:Label>
                                    </a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="Employee_Detail">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix d--f ai--c jc--c">
                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                        <div class="image-area m-r-10">
                                                            <asp:Image ID="imgEmp" runat="server" Width="120" Height="120" Style="border-radius: 50%;" />
                                                    </div>
                                                </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblempcode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtCodeValue" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="objlblEmployeeName" runat="server" Text=""></asp:Label>
                                                        </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblJob" runat="server" Text="Current Job Title" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="objlblJob" runat="server" Text=""></asp:Label>
                                                    </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="objlblDepartment" runat="server" Text="-"></asp:Label>
                                                        </div>
                                                            </div>
                                                        <div class="divider">
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblLineManager" runat="server" Text="Line Manager" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtLineManager" runat="server" Text="-"></asp:Label>
                                                    </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <%-- 'Pinkal (09-Aug-2021)-- Start
                                                                     'NMB New UI Enhancements.--%>
                                                                <%--<asp:Label ID="objlblWorkingStation" runat="server" Text="Working Station" CssClass="form-label"></asp:Label>--%>
                                                                <asp:Label ID="objlblWorkingStation" runat="server" Text="WorkStation" CssClass="form-label"></asp:Label>
                                                                  <%--'Pinkal (09-Aug-2021)-- End--%>
                                                                <asp:Label ID="txtWorkingStation" runat="server" Text="-"></asp:Label>
                                                        </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="objlblQualification" runat="server" Text="Qualification" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtQualification" runat="server" Text="-"></asp:Label>
                                                        </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblJobLevel" runat="server" Text="Job Level" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtJobLevel" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="divider">
                                                            </div>
                                                        </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                                                        <asp:Label ID="lblIsAvailableInTalent" runat="server" Text="" Style="margin-right: 10px;
                                                            font-size: 18px" ToolTip="This Employee is Talent" CssClass="af aruti-icon-star text-color-5"
                                                            Visible="false" />
                                                        <asp:Label ID="lblIsAvailableInSuccession" runat="server" Text="" Style="font-size: 18px"
                                                            ToolTip="This Employee is Successor" CssClass="af aruti-icon-slack text-color-2"
                                                            Visible="false" />
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="objlblDateintoJobLevel" runat="server" Text="Date into Job Level"
                                                                CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtDateintoJobLevel" runat="server" Text="-"></asp:Label>
                                                        </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="objlblGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtGenderValue" runat="server" Text="-"></asp:Label>
                                                        </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNextJob" runat="server" Text="Preferred Next Move" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="cboNextJob" runat="server">
                                                                    </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        </div>
                                                    </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="divider">
                                                </div>
                                            </div>
                                        </div>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Ul1">
                                                    <li role="presentation" class="active"><a href="#EmployeeExp" aria-controls="EmployeeExp"
                                                        role="tab" data-toggle="tab">
                                                        <asp:Label ID="lblEmployeeExperience" runat="server" Text="Employee Experience"></asp:Label>
                                                    </a></li>
                                                    <li role="presentation"><a href="#EmployeePastTrainings" aria-controls="EmployeePastTrainings"
                                                        role="tab" data-toggle="tab">
                                                        <asp:Label ID="Label7" runat="server" Text="Past Development Activities and Trainings"></asp:Label>
                                                    </a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="EmployeeExp">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 200px">
                                                            <asp:GridView ID="gvExperience" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                AllowPaging="false" Width="99%">
                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Date From" HeaderStyle-HorizontalAlign="Right">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpExpDate_from" runat="server" Text='<%# Eval("DATE_FROM") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Date To">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpExpDate_to" runat="server" Text='<%# Eval("DATE_TO") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="EMPLOYER" HeaderText="Company Name" ReadOnly="True" FooterText="colhEmployer">
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="POSITION_HELD" HeaderText="Position Held" ReadOnly="True"
                                                                        FooterText="colhPositionHeld"></asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="EmployeePastTrainings">
                                                        <div class="card inner-card">
                                                            <div class="header">
                                                                <h2>
                                                                    <asp:Label ID="Label9" runat="server" Text="Employee Past Trainings"></asp:Label>
                                                                </h2>
                                                            </div>
                                                            <div class="body">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="table-responsive" style="max-height: 200px">
                                                                            <asp:GridView ID="gvEmpPastTrainings" runat="server" AutoGenerateColumns="False"
                                                                                CssClass="table table-hover table-bordered" AllowPaging="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="Name" HeaderText="Training Title" ReadOnly="True" FooterText="colhTrainingTitle">
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText="Enrollment Date">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblEnrollmentDate" runat="server" Text='<%# Eval("EnrollDate") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="CustomItem">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <asp:DataList ID="dlPersonalAnalysisGoalsCategory" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Panel ID="pnlPersonalAnalysisGoalsCategoryContrainer" runat="server">
                                                                    <div class="panel-group" id="pnlPersonalAnalysisGoalsCategory" role="tablist" aria-multiselectable="true">
                                                                        <div class="panel" style="border: 1px solid #ddd">
                                                                            <div class="panel-heading" role="tab" id="PersonalAnalysisGoalsCategoryHeading_<%# Eval("categoryunkid") %>">
                                                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                                                    <a role="button" data-toggle="collapse" class="PersonalDevlopmentGoalCollapseCategory"
                                                                                        href="#PersonalAnalysisGoalsCategory_<%# Eval("categoryunkid") %>" aria-expanded="false"
                                                                                        aria-controls="PersonalAnalysisGoalsCategory_<%# Eval("categoryunkid") %>" style="flex: 1;
                                                                                        background: #F5F5F5">
                                                                                        <asp:Label ID="lblCategoryName" Text='<%# Eval("category") %>' runat="server" />
                                                                                    </a>
                                                                                    <asp:HiddenField ID="hfcategoryunkid" runat="server" Value='<%# Eval("categoryunkid") %>' />
                                                                                    <asp:HiddenField ID="hfcategoryViewtypeId" runat="server" Value='<%# Eval("viewtypeid") %>' />
                                                                                    <asp:LinkButton runat="server" ID="lnkAddPersonalDevlopmentGoalItem" OnClick="OnGvAddPersonalDevlopmentGoalItem"
                                                                                        Style="background: #F5F5F5">
                                                                                        <i class="fas fa-plus-circle text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="PersonalAnalysisGoalsCategory_<%# Eval("categoryunkid") %>" class="panel-collapse collapse"
                                                                                role="tabpanel" aria-labelledby="PersonalAnalysisGoalsCategoryHeading_<%# Eval("categoryunkid") %>">
                                                                                <div class="panel-body table-responsive" style="max-height: 300px">
                                                                                    <asp:GridView ID="dgvPersonalDevlopmentGoalItems" runat="server" runat="server" Width="99%"
                                                                                        AllowPaging="false" CssClass="table table-hover table-bordered" OnRowDataBound="dgvPersonalDevlopmentGoalItems_RowDataBound"
                                                                                        DataKeyNames="itemtypeid,selectionmodeid,GUID,Header_Id,Header_Name,formunkid">
                                                                                        <Columns>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                    <asp:DataList ID="dlPersonalAnalysisGoalsItem" runat="server" Width="100%" OnItemDataBound="dlPersonalAnalysisGoalsItem_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <asp:Panel ID="pnlPersonalAnalysisGoalsItemContrainer" runat="server">
                                                                                                <div class="panel-group" id="pnlPersonalAnalysisGoalsItem" role="tablist" aria-multiselectable="true">
                                                                                                    <div class="panel" style="border: 1px solid #ddd">
                                                                                                        <div class="panel-heading" role="tab" id="PersonalAnalysisGoalsItemHeading_<%# Eval("itemunkid") %>">
                                                                                                            <h4 class="panel-title d--f ai--c jc--sb">
                                                                                                                <a role="button" data-toggle="collapse" class="PersonalDevlopmentGoalCollapse" href="#PersonalAnalysisGoalsItem_<%# Eval("itemunkid") %>"
                                                                                                                    aria-expanded="false" aria-controls="PersonalAnalysisGoalsItem_<%# Eval("itemunkid") %>"
                                                                                                                    style="flex: 1">
                                                                                                                    <asp:Label ID="lblItemName" Text='<%# Eval("item") %>' runat="server" />
                                                                                                                </a>
                                                                                    <asp:LinkButton runat="server" ID="lnkAddPersonalDevlopmentGoalItem" OnClick="OnAddPersonalDevlopmentGoalItem">
                                                                                        <i class="fas fa-plus-circle text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                    <asp:HiddenField ID="hfcategoryunkid" runat="server" Value='<%# Eval("categoryunkid") %>' />
                                                                                                                <asp:HiddenField ID="hfitemunkid" runat="server" Value='<%# Eval("itemunkid") %>' />
                                                                                                                <asp:HiddenField ID="hfselectionmodeid" runat="server" Value='<%# Eval("selectionmodeid") %>' />
                                                                                                                <asp:HiddenField ID="hfiscompetencyselectionset" runat="server" Value='<%# Eval("iscompetencyselectionset") %>' />
                                                                                                                <asp:HiddenField ID="hfitemtypeid" runat="server" Value='<%# Eval("itemtypeid") %>' />
                                                                                </h4>
                                                                            </div>
                                                                                                        <div id="PersonalAnalysisGoalsItem_<%# Eval("itemunkid") %>" class="panel-collapse collapse"
                                                                                                            role="tabpanel" aria-labelledby="PersonalAnalysisGoalsItemHeading_<%# Eval("itemunkid") %>">
                                                                                <div class="panel-body">
                                                                                            <div class="table-responsive" style="max-height: 300px">
                                                                                                                    <asp:DataList ID="dlPersonalAnalysisGoalsItemsList" runat="server" Width="100%" OnItemDataBound="dlPersonalAnalysisGoalsItemsList_ItemDataBound">
                                                                                                                        <ItemTemplate>
                                                                                                                            <div style="border: 1px solid #ddd; padding: 12px; background: #fff; margin-bottom: 10px;
                                                                                                                                display: flex; align-items: center;">
                                                                                                                                <asp:Label ID="lblCategoryItemName" Text='<%# Eval("custom_value") %>' runat="server"
                                                                                                                                    Style="display: inline-block; flex: 1" />
                                                                                                                                <asp:LinkButton runat="server" ID="lnkPersonalAnalysisGoalsItemsListEdit" CssClass="m-l-10"
                                                                                                                                    OnClick="OnEditPersonalDevlopmentGoalItem">
                                                                                                                                        <i class="fas fa-pencil-alt text-success"></i>
                                                                                                                                </asp:LinkButton>
                                                                                                                                <asp:LinkButton runat="server" ID="lnkPersonalAnalysisGoalsItemsListDelete" CssClass="m-l-10"
                                                                                                                                    OnClick="OnDeletePersonalDevlopmentGoalItem">
                                                                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                                                                </asp:LinkButton>
                                                                                                                                <asp:HiddenField ID="hfcustomitemunkid" runat="server" Value='<%# Eval("customitemunkid") %>' />
                                                                                                                                <asp:HiddenField ID="hfitemtranunkid" runat="server" Value='<%# Eval("itemtranunkid") %>' />
                                                                                                                                <asp:HiddenField ID="hfitemtypeid" runat="server" Value='<%# Eval("itemtypeid") %>' />
                                                                                                                                <asp:HiddenField ID="hfselectionmodeid" runat="server" Value='<%# Eval("selectionmodeid") %>' />
                                                                                                                                <asp:HiddenField ID="hfcategoryunkid" runat="server" Value='<%# Eval("categoryunkid") %>' />
                                                                                                                                <asp:HiddenField ID="hfpdpformunkid" runat="server" Value='<%# Eval("pdpformunkid") %>' />
                                                                                                                                <asp:HiddenField ID="hfiscompetencyselection" runat="server" Value='<%# Eval("iscompetencyselection") %>' />
                                                                                                                                <asp:HiddenField ID="hfCompetencyValue" runat="server" Value='<%# Eval("CompetencyValue") %>' />
                                                                                                                                <asp:HiddenField ID="hfitemgrpguid" runat="server" Value='<%# Eval("itemgrpguid") %>' />
                                                                                                                            </div>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:DataList>
                                                                                                                </div>
                                                                                                            </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                            </asp:Panel>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="DevelopmentActionplan">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <asp:DataList ID="dlActionCategory" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Panel ID="pnlActionPlanList" runat="server">
                                                                    <div class="panel-group" id="pnlActionPlan" role="tablist" aria-multiselectable="true">
                                                                        <div class="panel" style="border: 1px solid #ddd">
                                                                            <div class="panel-heading" role="tab" id="ActionCategoryheadingOne_<%# Eval("actionplancategoryunkid") %>">
                                                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                                                    <a role="button" data-toggle="collapse" class="ActionCategoryCollapse" href="#ActionCategorycollapseOne_<%# Eval("actionplancategoryunkid") %>"
                                                                                        aria-expanded="false" aria-controls="ActionCategorycollapseOne_<%# Eval("actionplancategoryunkid") %>"
                                                                                        style="flex: 1">
                                                                                        <asp:Label ID="lblCategoryName" Text='<%# Eval("category") %>' runat="server" />
                                                                                    </a>
                                                                                    <asp:LinkButton runat="server" ID="lnkAddActionPlanItem" OnClick="AddActionPlanItem"
                                                                                        ToolTip="Add New Action Plan">
                                                                                        <i class="fas fa-plus-circle text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                    <asp:HiddenField ID="hfactionplancategoryunkid" runat="server" Value='<%# Eval("actionplancategoryunkid") %>' />
                                                                                </h4>
                                                                            </div>
                                                                            <div id="ActionCategorycollapseOne_<%# Eval("actionplancategoryunkid") %>" class="panel-collapse collapse"
                                                                                role="tabpanel" aria-labelledby="ActionCategoryheadingOne_<%# Eval("actionplancategoryunkid") %>">
                                                                                <div class="panel-body">
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                                            <div class="table-responsive" style="max-height: 500px">
                                                                                                <asp:DataList ID="dgvActionPlanList" runat="server" Width="100%" OnItemDataBound="dgvActionPlanList_ItemDataBound">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="hfemployeeunkid" Value='<%# Eval("employeeunkid") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfactionplancategoryunkid" Value='<%# Eval("actionplancategoryunkid") %>'
                                                                                                            runat="server" />
                                                                                                        <asp:HiddenField ID="hfpdpgoalsmstunkid" Value='<%# Eval("pdpgoalsmstunkid") %>'
                                                                                                            runat="server" />
                                                                                                        <asp:HiddenField ID="hfparentgoalunkid" Value='<%# Eval("parentgoalunkid") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfstatusunkid" Value='<%# Eval("statusunkid") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfisCompleted" Value='<%# Eval("isCompleted") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfGoalPoint" Value='<%# Eval("point") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfitemunkid" Value='<%# Eval("itemunkid") %>' runat="server" />
                                                                                                        <asp:Panel ID="pnlActionPlanItem" runat="server" CssClass="panel detail-box m-b-20">
                                                                                                            <div class="body">
                                                                                                                <div class="row clearfix">
                                                                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblActionPlanGoalName" runat="server" Text="Area of Development/Goal Name"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtActionPlanGoalName" Text='<%# Eval("goal_name") %>' runat="server" />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblActionPlanStatus" runat="server" Text="Status"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtActionPlanStatus" runat="server" CssClass="label label-primary"
                                                                                                                                Text='<%# Eval("status") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblActionPlanProgress" runat="server" Text="Progress"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <div class="progress">
                                                                                                                                <asp:Label ID="txtActionPlanProgress" runat="server" CssClass="progress-bar bg-green"></asp:Label>
                                                                                                                            </div>
                                                                                                                            <asp:Label ID="txtActionPlanListProgressCount" runat="server" Text='<%# Eval("point") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-25 text-right">
                                                                                                                        <asp:LinkButton runat="server" ID="lnkEditActionPlanItem" OnClick="EditActionPlanItem"
                                                                                                                            ToolTip="Edit Development Action Plan" CssClass="m-r-10" Visible="false">
                                                                                        <i class="fas fa-pencil-alt text-success"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                        <asp:LinkButton runat="server" ID="lnkDeleteActionPlanItem" OnClick="OnDeleteActionPlanItem"
                                                                                                                            ToolTip="Delete Development Action Plan" Visible="false">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row clearfix">
                                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblActionPlanGoalDescription" runat="server" Text="Action/Activities"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtActionPlanGoalDescription" Text='<%# Eval("goal_description") %>'
                                                                                                                                runat="server" />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblactionPlanDueDate" runat="server" Text="Due Date"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtactionPlanDueDate" runat="server" Text='<%# Eval("DueDate") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-25 text-right">
                                                                                                                        <asp:LinkButton runat="server" ID="lnkActionPlanComment" CssClass="m-r-10" ToolTip="Add/View Comment(s) For Development Action Plan"
                                                                                                                            OnClick="OnSetCommentForActionPlanItem">
                                                                                                                            <i class="fas fa-comment text-warning"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                        <asp:LinkButton runat="server" ID="lnkActionPlanUpdateProgress" CssClass="m-r-10"
                                                                                                                            OnClick="SetUpdateProgressForActionPlanItem" Visible="false" ToolTip="Add Update Progress For Development Action Plan">
                                                                                                                        <i class="fas fa-chart-line text-info"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                        <asp:LinkButton runat="server" ID="lnkActionPlanViewTraining" OnClick="ViewActionPlanTrainingList"
                                                                                                                            ToolTip="View Assigned Training Course(s)">
                                                                                                                        <i class="fas fa-book-reader text-primary" ></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <asp:Panel ID="pnlLastUpdateDetail" runat="server" CssClass="row clearfix" Visible="false">
                                                                                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblactionPlanLastUpdateRemark" runat="server" Text="Last Progress Update/Remark"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtactionPlanLastUpdateRemark" runat="server" Text='<%# Eval("remarks") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-1 text-right">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblactionPlanLastUpdateDate" runat="server" Text="Updated On"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtactionPlanLastUpdateDate" runat="server" Text='<%# Eval("updatedatetime") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </asp:Panel>
                                                                                                            </div>
                                                                                                        </asp:Panel>
                                                                                                    </ItemTemplate>
                                                                                                </asp:DataList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnSubmitForReview" runat="server" Text="Submit For Review" CssClass="btn btn-primary"
                                        Visible="false" />
                                    <asp:Button ID="btnUnlock" runat="server" Text="Unlock" CssClass="btn btn-primary"
                                        Visible="false" />
                                    <asp:Button ID="btnReviewTrainings" runat="server" Text="Review Trainings Suggested by the Employee"
                                        CssClass="btn btn-primary pull-left" Visible="false" />
                                    <asp:Button ID="btnLock" runat="server" Text="Lock" CssClass="btn btn-primary" Visible="false" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ListCItemAddEdit" runat="server" TargetControlID="hdf_ListcItem"
                    CancelControlID="hdf_ListcItem" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_ListCItemAddEdit"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ListCItemAddEdit" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCItem" runat="server" Text="Personal Analysis and Goals"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 180px">
                        <div class="row clearfix" style="display: none">
                            <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblItemName" runat="server" CssClass="form-label"></asp:Label>
                                <asp:Panel ID="pnlItemNameFreeText" runat="server" Visible="false" CssClass="d--f ai--c">
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 p-l-0">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtItemNameFreeText" runat="server" TextMode="MultiLine" Rows="3"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    </div>
                                    <asp:Panel ID="pnlFreeTextListItemComepetencySelection" runat="server" Visible="false"
                                        CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1 d--f fd--c">
                                        <asp:LinkButton ID="lnkCompetencySelection" runat="server" CssClass="m-b-10">
                                            <i class="fas fa-list text-primary" style="font-size:16px"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lnkCompetencySelectionReset" runat="server">
                                            <i class="fas fa-redo text-danger" style="font-size:16px"></i>
                                        </asp:LinkButton>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="pnlItemNameSelection" runat="server" Visible="false">
                                        <div class="form-group">
                                        <asp:DropDownList ID="cboItemNameSelection" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlItemNameDtp" runat="server" Visible="false">
                                        <div class="form-group">
                                        <uc1:DateCtrl ID="dtpItemName" runat="server" AutoPostBack="false" />
                                        </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlItemNameNum" runat="server" Visible="false">
                                    <uc4:NumericText ID="txtItemNameNUM" runat="server" Width="100%" AutoPostBack="false" />
                                </asp:Panel>
                                    </div>
                                </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ListcItem" runat="server" />
                        <asp:Button ID="btnListPersonalDevlopmentGoalSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnListPersonalDevlopmentGoalClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                    CancelControlID="hdf_cItem" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_CItemAddEdit"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label4" runat="server" Text="Personal Analysis and Goals"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 450px">
                        <div class="row clearfix" style="display: none">
                            <uc1:DateCtrl ID="DateCtrl1" runat="server" AutoPostBack="false" />
                        </div>
                        <asp:DataList ID="dgv_Citems" runat="server" Width="100%">
                            <ItemTemplate>
                                <div class="clearfix d--f ai--c" style="border: 1px solid rgba(204, 204, 204, 0.35);
                                    padding: 10px; margin-bottom: 10px">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("item") %>' CssClass="form-label"></asp:Label>
                                        <asp:HiddenField ID="hfselectedid" runat="server" Value='<%# Eval("selectedid") %>' />
                                        <asp:HiddenField ID="hfselectionmodeid" runat="server" Value='<%# Eval("selectionmodeid") %>' />
                                        <asp:HiddenField ID="hfddate" runat="server" Value='<%# Eval("ddate") %>' />
                                        <asp:HiddenField ID="hfcustomitemunkid" runat="server" Value='<%# Eval("customitemunkid") %>' />
                                        <asp:HiddenField ID="hfitemtypeid" runat="server" Value='<%# Eval("itemtypeid") %>' />
                                        <asp:HiddenField ID="hfiscompetencyselectionset" runat="server" Value='<%# Eval("iscompetencyselectionset") %>' />
                                        <asp:HiddenField ID="hfCompetencyVal" runat="server" Value='<%# Eval("competencyVal") %>' />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 d--f ai--c">
                                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                        <asp:TextBox ID="txtFreetext" runat="server" TextMode="MultiLine" Rows="3" Visible="false"
                                            Text='<%# Eval("custom_value") %>' class="form-control"></asp:TextBox>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboSelection" runat="server" AutoPostBack="false" Visible="false">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group">
                                            <uc1:DateCtrl ID="dtpSelection" runat="server" AutoPostBack="false" Visible="false" />
                                        </div>
                                        <uc4:NumericText ID="txtNUM" runat="server" Width="100%" AutoPostBack="false" Visible="false"
                                            Text='<%# Eval("custom_value") %>' />
                                    </div>
                                        <asp:Panel ID="pnlCompetencySelection" runat="server" CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1 d--f fd--c">
                                            <asp:LinkButton ID="lnkCompetencySelection" runat="server" CssClass="m-b-10" OnClick="OnCompetencySelection_Click">
                                            <i class="fas fa-list text-primary" style="font-size:16px"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lnkCompetencySelectionReset" runat="server">
                                            <i class="fas fa-redo text-danger" style="font-size:16px"></i>
                                            </asp:LinkButton>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_cItem" runat="server" />
                        <asp:Button ID="btnPersonalDevlopmentGoalSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnPersonalDevlopmentGoalClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ActionPlanAddEdit" runat="server" TargetControlID="hdf_ActionPlan"
                    CancelControlID="hdf_ActionPlan" BackgroundCssClass="modal-backdrop bd-l2" PopupControlID="pnlActionPlanAddEdit"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlActionPlanAddEdit" runat="server" CssClass="card modal-dialog modal-lg modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblActionPlanAddEdit" runat="server" Text="Add/Edit Action Plan"></asp:Label>
                            <small>
                                <asp:Label ID="lblActionPlanAddEditCategory" runat="server" Text=""></asp:Label>
                            </small>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 450px">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist" id="ActionPlanTabs">
                            <li role="presentation" class="active"><a href="#ActionPlanDetail" data-toggle="tab">
                                <asp:Label ID="lblActionPlanTab" runat="server" Text="Action Plan"></asp:Label>
                            </a></li>
                            <li role="presentation"><a href="#RecommendTraining" data-toggle="tab">
                                <asp:Label ID="lblRecommendTrainingTab" runat="server" Text="Recommended Training(s)/Activities"></asp:Label>
                            </a></li>
                        </ul>
                        <div class="tab-content p-b-0" style="overflow: visible">
                            <div role="tabpanel" class="tab-pane fade in active" id="ActionPlanDetail">
                                <div class="row clearfix d--f ai--c">
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                        <asp:Label ID="lblActionPlanGoalName" runat="server" Text="Area of Development/Goal Name"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtActionPlanGoalName" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                    Rows="2"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 d--f fd--c">
                                        <asp:LinkButton ID="lnkExistingGoal" runat="server" CssClass="m-b-10">
                                            <i class="fas fa-list text-primary" style="font-size:16px"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lnkExistingGoalReset" runat="server">
                                            <i class="fas fa-redo text-danger" style="font-size:16px"></i>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblActionPlanGoalDescription" runat="server" Text="Action/Activities"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtActionPlanGoalDescription" runat="server" CssClass="form-control"
                                                    TextMode="MultiLine" Rows="2"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblActionPlanCoach" runat="server" Text="Coach" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtActionPlanCoach" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblActionPlanMentor" runat="server" Text="Mentor" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpActionPlanMentor" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display: none">
                                        <asp:Label ID="lblActionPlanParentGoal" runat="server" Text="Parent Goal" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpActionPlanParentGoal" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblActionPlanDueDate" runat="server" Text="Due Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpActionPlanDueDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpActionPlanStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblActionPlanEvalutor" runat="server" Text="Evaluator" CssClass="form-label"></asp:Label>
                                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 p-l-0">
                                            <asp:CheckBox ID="chkActionPlanSelf" runat="server" Text="Self" Checked="true" />
                                        </div>
                                        <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkActionPlanLineManager" runat="server" Checked="true" Text="Line Manager" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkActionPlanPeers" runat="server" Text="Peers" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="RecommendTraining">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="txtTrainingSearch" name="txtTrainingSearch" placeholder="type search text"
                                                    maxlength="50" style="height: 25px; font: 100" onkeyup="gridsearching(this);"
                                                    class="form-control" />
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlTrainingNeeded" runat="server" class="table-responsive" Style="max-height: 250px">
                                            <asp:GridView ID="dgvActionPlanTrainingList" runat="server" AutoGenerateColumns="false"
                                                CellPadding="3" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                Width="99%">
                                                <Columns>
                                                    <asp:TemplateField FooterText="objcolhCheckAll" ItemStyle-Width="30px">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hfIsFromCompetency" Value="0" runat="server" />
                                                            <asp:HiddenField ID="hfmasterid" Value='<%# Eval("masterunkid") %>' runat="server" />
                                                            <asp:CheckBox ID="chkSelect" runat="server" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="name" HeaderText="Training Course" FooterText="objcolhname">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Panel ID="Panel1" runat="server" class="table-responsive" Style="max-height: 300px">
                                            <asp:GridView ID="gvActionPlanAssignedTraining" runat="server" AutoGenerateColumns="false"
                                                CellPadding="3" CssClass="table table-hover table-bordered" Width="99%">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hftrainingneedtranunkid" Value='<%# Eval("trainingneedtranunkid") %>'
                                                                runat="server" />
                                                            <asp:HiddenField ID="hftrainingcourseunkid" Value='<%# Eval("trainingcourseunkid") %>'
                                                                runat="server" />
                                                            <asp:HiddenField ID="hfpdpmstgoalunkid" Value='<%# Eval("pdpgoalsmstunkid") %>' runat="server" />
                                                            <asp:HiddenField ID="hftrainingstatusunkid" Value='<%# Eval("statusunkid") %>' runat="server" />
                                                            <asp:LinkButton runat="server" ID="lnkRemoveAssignedTrainingItem" OnClick="lnkRemoveAssignedTrainingItem_Click">
                                                                   <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="name" HeaderText="Assigned Training Course" FooterText="objcolhname">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ActionPlan" runat="server" />
                        <asp:Button ID="btnSaveActionPlan" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseActionPlan" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ActionPlanGoalSelection" runat="server" TargetControlID="hdf_ActionPlanGoalSelection"
                    CancelControlID="hdf_ActionPlanGoalSelection" BackgroundCssClass="modal-backdrop bd-l3"
                    PopupControlID="pnlActionPlanGoalSelection" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlActionPlanGoalSelection" runat="server" CssClass="card modal-dialog modal-l3"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblActionPlanGoalSelectionTitle" runat="server" Text="Select Area of Development/Goal Name"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 350px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:GridView ID="gvActionPlanGoalSelection" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-bordered" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:RadioButton ID="rdbActionPlanGoalSelection" runat="server" Text=" " onclick="RadioCheck(this);" />
                                                <asp:HiddenField ID="hfcategoryunkid" runat="server" Value='<%#Eval("categoryunkid")%>' />
                                                <asp:HiddenField ID="hfitemunkid" runat="server" Value='<%#Eval("itemunkid")%>' />
                                                <asp:HiddenField ID="hfitem" runat="server" Value='<%#Eval("item")%>' />
                                                <asp:HiddenField ID="hfcategory" runat="server" Value='<%#Eval("category")%>' />
                                                <asp:HiddenField ID="hfIsgrp" runat="server" Value='<%#Eval("isgrp")%>' />
                                                <asp:HiddenField ID="hfValue" runat="server" Value='<%#Eval("itemtranunkid")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="item" HeaderText="Item" />--%>
                                        <asp:BoundField DataField="fieldvalue" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ActionPlanGoalSelection" runat="server" />
                        <asp:Button ID="btnSetActionPlanGoalSelection" runat="server" Text="Ok" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseActionPlanGoalSelection" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_UpdateProgress" runat="server" TargetControlID="hdf_UpdateProgress"
                    CancelControlID="hdf_UpdateProgress" BackgroundCssClass="modal-backdrop bd-l2"
                    PopupControlID="pnlUpdateProgress" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlUpdateProgress" runat="server" CssClass="card modal-dialog modal-lg modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label1" runat="server" Text="Action Plan Update Progress"></asp:Label>
                        </h2>
                    </div>
                    <div class="body panel detail-box" style="max-height: 450px">
                        <div class="row clearfix m-b-10">
                            <div class="col-lg-12 col-md-21 col-sm-12 col-xs-12">
                            <div class="title">
                                <asp:Label ID="lblUpdateProgressGoalEmployee" runat="server" Text="Employee Name"></asp:Label>
                            </div>
                            <div class="content">
                                <asp:Label ID="txtUpdateProgressGoalEmployee" runat="server" />
                            </div>
                        </div>
                        </div>
                        <div class="row clearfix ">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="title">
                                <asp:Label ID="lblUpdateProgressGoal" runat="server" Text="Area of Development/Goal Name"
                                    CssClass="form-label"></asp:Label>
                                </div>
                                <div class="content">
                                <asp:Label ID="txtUpdateProgressGoal" runat="server"></asp:Label>
                            </div>
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="title">
                                <asp:Label ID="lblUpdateProgressGoalDescription" runat="server" Text="Action/Activities"
                                    CssClass="form-label"></asp:Label>
                                </div>
                                <div class="content">
                                <asp:Label ID="txtUpdateProgressGoalDescription" runat="server"></asp:Label>
                            </div>
                        </div>
                        </div>
                        <div class="divider">
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <asp:Label ID="lblUpdateProgressScore" runat="server" Text="Completion Percentage"
                                    CssClass="form-label"></asp:Label>
                                <uc4:NumericText ID="txtUpdateProgressScore" runat="server" AutoPostBack="false"
                                    Max="100" Min="0" Type="Point" Text="0" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <asp:Label ID="lblUpdateProgressLastUpdatedScore" runat="server" Text="Last Completion Percentage"
                                    CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtUpdateProgressLastUpdateScore" runat="server" ReadOnly="true"
                                            Text="0" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                <asp:Label ID="lblUpdateProgressStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="drpUpdateProgressStatus" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                <asp:Label ID="lblUpdateProgressLastUpdatedOn" runat="server" Text="Last Updated On"
                                    CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtUpdateProgressLastUpdatedOn" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblUpdateProgressRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtUpdateProgressRemarks" runat="server" CssClass="form-control"
                                            TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_UpdateProgress" runat="server" />
                        <asp:HiddenField ID="hfLastUpdateProgressId" runat="server" />
                        <asp:Button ID="btnRemoveLastUpdateProgress" runat="server" Text="Undo Last Update Progress"
                            CssClass="btn btn-danger pull-left" />
                        <asp:Button ID="btnUpdateProgressSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnUpdateProgressClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ViewTrainingList" runat="server" TargetControlID="hdf_ViewTrainingList"
                    CancelControlID="hdf_ViewTrainingList" BackgroundCssClass="modal-backdrop bd-l2"
                    PopupControlID="pnlViewTrainingList" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlViewTrainingList" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label2" runat="server" Text="Assigned Training Course(s)"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 300px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Panel ID="pnl_ViewTrainingList" runat="server" class="table-responsive">
                                    <asp:GridView ID="dgvViewTrainingList" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-hover table-bordered no-header" RowStyle-Wrap="false" Width="99%"
                                        ShowHeader="false">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="30px">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hftrainingneedtranunkid" Value='<%# Eval("trainingneedtranunkid") %>'
                                                        runat="server" />
                                                    <asp:HiddenField ID="hftrainingcourseunkid" Value='<%# Eval("trainingcourseunkid") %>'
                                                        runat="server" />
                                                    <asp:HiddenField ID="hftrainingstatusunkid" Value='<%# Eval("statusunkid") %>' runat="server" />
                                                    <asp:HiddenField ID="hfpdpmstgoalunkid" Value='<%# Eval("pdpgoalsmstunkid") %>' runat="server" />
                                                    <asp:LinkButton runat="server" ID="lnkRemoveTrainingFromViewTrainingListItem" Visible="false"
                                                        OnClick="lnkRemoveTrainingFromViewTrainingListItem_Click">
                                                                   <i class="fas fa-trash text-danger"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="name" HeaderText="Assigned Training Course" FooterText="objcolhname">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="StatusName" HeaderText="Status" FooterText="objcolhname">
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ViewTrainingList" runat="server" />
                        <asp:Button ID="btnCloseViewTrainingList" runat="server" Text="Close" CssClass="btn btn-primary" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ActionPlanComment" runat="server" TargetControlID="hdf_ActionPlanComment"
                    CancelControlID="hdf_ActionPlanComment" BackgroundCssClass="modal-backdrop bd-l2"
                    PopupControlID="pnlActionPlanComment" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlActionPlanComment" runat="server" CssClass="card modal-dialog modal-lg modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label3" runat="server" Text="Action Plan Comment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body panel detail-box" style="max-height: 480px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="title">
                                    <asp:Label ID="lblActionPlanCommentEmployeeName" runat="server" Text="Employee Name"></asp:Label>
                                </div>
                                <div class="content">
                                    <asp:Label ID="txtActionPlanCommentEmployeeName" runat="server" />
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="title">
                                    <asp:Label ID="lblActionPlanCommentStatus" runat="server" Text="Status"></asp:Label>
                                </div>
                                <div class="content">
                                    <asp:Label ID="txtActionPlanCommentStatus" runat="server" CssClass="label label-primary"></asp:Label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="title">
                                    <asp:Label ID="lblActionPlanCommentProgress" runat="server" Text="Progress"></asp:Label>
                                </div>
                                <div class="content">
                                    <div class="progress">
                                        <asp:Label ID="txtActionPlanCommentProgress" runat="server" CssClass="progress-bar bg-green"></asp:Label>
                                    </div>
                                    <asp:Label ID="txtActionPlanCommentProgressCount" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="title">
                                    <asp:Label ID="lblActionPlanCommentGoalName" runat="server" Text="Area of Development/Goal Name"></asp:Label>
                                </div>
                                <div class="content">
                                    <asp:Label ID="txtActionPlanCommentGoalName" runat="server" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="title">
                                    <asp:Label ID="lblActionPlanCommentGoalDescription" runat="server" Text="Action/Activities"></asp:Label>
                                </div>
                                <div class="content">
                                    <asp:Label ID="txtActionPlanCommentGoalDescription" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="divider">
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="overflow" id="dvCommentScroll" style="height: 300px" onscroll="setCommentScrollPosition(this.scrollTop);">
                                    <asp:DataList ID="dlActionPlanComments" runat="server" Width="100%">
                                        <EditItemTemplate>
                                            <asp:Panel ID="Panel4" runat="server" CssClass="action-user-comment-box">
                                                <div class="heading">
                                                    <div class="img-box">
                                                        <asp:Image ID="Image1" runat="server" Width="40" Height="40" Style="border-radius: 50%;"
                                                            ImageUrl='<%# Eval("empBaseImage") %>' />
                                                    </div>
                                                    <div class="comment-user-detail">
                                                        <h4 class="user">
                                                            <asp:Label ID="lblCommentPerson" runat="server" Text='<%# Eval("CommentBy") %>'></asp:Label></h4>
                                                        <p class="date">
                                                            <asp:Label ID="lblCommentDateTime" runat="server" Text='<%# Eval("commentdatetime") %>'></asp:Label></p>
                                                    </div>
                                                </div>
                                                <div class="comment">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtActionPlanComment" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                Rows="2" Text='<%# Eval("comments") %>'></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:HiddenField ID="hdf_ActionPlanCommenttranunkid" runat="server" Value='<%# Eval("commenttranunkid") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkSaveEditCommentItem" OnClick="OnlnkSaveEditCommentItem_Click">
                                                                   <i class="fas fa-save"></i>Save
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lnkCancelEditCommentItem" OnClick="OnlnkCancleCommentItem_Click">
                                                                   <i class="fas fa-times"></i>Cancel
                                                    </asp:LinkButton>
                                                </div>
                                            </asp:Panel>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Panel ID="Panel4" runat="server" CssClass="action-user-comment-box">
                                                <div class="heading">
                                                    <div class="img-box">
                                                        <asp:Image ID="Image1" runat="server" Width="40" Height="40" Style="border-radius: 50%;"
                                                            ImageUrl='<%# Eval("empBaseImage") %>' />
                                                    </div>
                                                    <div class="comment-user-detail">
                                                        <h4 class="user">
                                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("CommentBy") %>'></asp:Label></h4>
                                                        <p class="date">
                                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("commentdatetime") %>'></asp:Label></p>
                                                    </div>
                                                </div>
                                                <div class="comment">
                                                    <p>
                                                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("comments") %>'></asp:Label>
                                                    </p>
                                                </div>
                                                <asp:Panel ID="pnlActionPlanCommentActionFooter" runat="server" CssClass="footer">
                                                    <asp:HiddenField ID="hdf_ActionPlanCommenttranunkid" runat="server" Value='<%# Eval("commenttranunkid") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkDownloadCommentAttachmentItem" Visible="false"
                                                        OnClick="OnlnkDownloadActionPlanCommentAttachment_Click">
                                                                   <i class="fas fa-download"></i>Download Attachment(s)
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lnkEditCommentItem" OnClick="OnlnkEditActionPlanItemComment_Click"
                                                        Visible="false">
                                                                   <i class="fas fa-pencil-alt"></i>Edit
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lnkDeleteCommentItem" OnClick="OnlnkDeleteActionPlanItemComment_Click"
                                                        Visible="false">
                                                                   <i class="fas fa-trash"></i>Delete
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lnkAddEditCommentAttachmentItem" Visible="false"
                                                        OnClick="OnlnkAddEditActionPlanCommentAttachment_Click">
                                                                   <i class="fas fa-paperclip"></i>Add/Edit Attachment
                                                    </asp:LinkButton>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:DataList>
                                    <asp:Panel ID="pnlActionPlanNoComment" runat="server" CssClass="no-comments">
                                        <p>
                                            <asp:Label ID="lblActionPlanNoComment" runat="server" Text="No Comments"></asp:Label>
                                        </p>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix d--f ai--c">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <asp:Label ID="lblActionPlanComment" runat="server" Text="Add Comment" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtActionPlanComment" runat="server" CssClass="form-control" TextMode="MultiLine"
                                            Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <asp:Button ID="btnAddActionPlanComment" runat="server" Text="Add" CssClass="btn btn-primary" />
                                <asp:Button ID="btnAddActionPlanCommentAttachment" runat="server" Text="Add Attachment"
                                    CssClass="btn btn-link" Visible="false" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ActionPlanComment" runat="server" />
                        <asp:Button ID="btnCloseActionPlanComment" runat="server" Text="Close" CssClass="btn btn-primary" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupCommentAttachment" BackgroundCssClass="modal-backdrop bd-l3"
                    TargetControlID="lblCommentAttachmentHeader" runat="server" PopupControlID="pnlCommentAttachment"
                    DropShadow="false" CancelControlID="hfCommentAttachment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCommentAttachment" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCommentAttachmentHeader" Text="Attachment" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="row clearfix ">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row clearfix d--f ai--fe">
                                            <asp:Panel ID="pnlAttachmentType" runat="server" CssClass="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboDocumentType" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </asp:Panel>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 d--f m-b-10">
                                                <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                    <div id="fileuploader">
                                                        <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                            value="Browse" />
                                                    </div>
                                                </asp:Panel>
                                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" CssClass="btn btn-default"
                                                    Text="Browse" />
                                                <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="max-height: 200px;">
                                                    <asp:GridView ID="gvActionPlanCommentAttachment" runat="server" AllowPaging="false"
                                                        AutoGenerateColumns="False" CssClass="table table-hover table-bordered" DataKeyNames="GUID,scanattachtranunkid">
                                                        <Columns>
                                                            <asp:TemplateField FooterText="objcohDelete" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" ToolTip="Delete" OnClick="OnDeleteCommentAttachment">
                                                                         <i class="fas fa-trash text-danger"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField FooterText="objcolhDownload" HeaderStyle-Width="23px" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fas fa-download"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                ItemStyle-HorizontalAlign="Right" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <uc6:Confirm ID="cfDelete" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gvActionPlanCommentAttachment" />
                                        <asp:PostBackTrigger ControlID="btnDownloadAll" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveActionPlanCommentAttachmentPopup" runat="server" CssClass="btn btn-default"
                            Text="Save" Visible="false" />
                        <asp:Button ID="btnCloseActionPlanCommentAttachmentPopup" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                        <asp:HiddenField ID="hfCommentAttachment" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupPdpInstruction" runat="server" CancelControlID="btnPdpInstructionClose"
                    PopupControlID="pnlPdpInstruction" TargetControlID="hfPdpInstruction" Drag="true"
                    PopupDragHandleControlID="pnlPdpInstruction" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlPdpInstruction" runat="server" CssClass="modal-dialog card" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPdpInstruction" runat="server" Text="Disclaimer/Instructions" />
                        </h2>
                    </div>
                    <div class="body" style="height: 250px">
                        <div class="form-group">
                            <div class="form-line">
                                <asp:TextBox ID="txtPdpInstruction" runat="server" CssClass="form-control" TextMode="MultiLine"
                                    Rows="7" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnPdpInstructionClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfPdpInstruction" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupCompetencySelection" runat="server" TargetControlID="hdf_CompetencySelection"
                    CancelControlID="hdf_CompetencySelection" BackgroundCssClass="modal-backdrop bd-l5"
                    PopupControlID="pnlCompetencySelection" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCompetencySelection" runat="server" CssClass="card modal-dialog modal-l5"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCompetencySelectionHeader" runat="server" Text="Select Competency"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 350px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:GridView ID="gvCompetencySelection" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-bordered no-header" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfValue" runat="server" Value='<%#Eval("Id")%>' />
                                                <asp:RadioButton ID="rdbCompetencySelection" runat="server" Text=" " onclick="RadioCompetencyCheck(this);" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Name" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_CompetencySelection" runat="server" />
                        <asp:Button ID="btnSaveCompetencySelection" runat="server" Text="Ok" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseCompetencySelection" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupReviewTraining" runat="server" TargetControlID="hdf_ReviewTraining"
                    CancelControlID="hdf_ReviewTraining" BackgroundCssClass="modal-backdrop bd-l3"
                    PopupControlID="pnlReviewTraining">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlReviewTraining" runat="server" CssClass="card modal-dialog modal-l3"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label10" runat="server" Text="Review Trainings Suggested by the Employee"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 350px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:GridView ID="dgvReviewTraining" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="ChkAll" runat="server" Text=" " CssClass="chk-sm" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                <asp:HiddenField ID="hftrainingneedtranunkid" runat="server" Value='<%#Eval("trainingneedtranunkid")%>' />
                                                <asp:HiddenField ID="hfIsgrp" runat="server" Value='<%#Eval("isgrp")%>' />
                                                <asp:HiddenField ID="hftrainingcourseunkid" runat="server" Value='<%#Eval("trainingcourseunkid")%>' />
                                                <asp:HiddenField ID="hfisfromcompetency" runat="server" Value='<%#Eval("isfromcompetency")%>' />
                                                <asp:HiddenField ID="hfgoalname" runat="server" Value='<%#Eval("goal_name")%>' />
                                                <asp:HiddenField ID="hfitemunkid" runat="server" Value='<%#Eval("itemunkid")%>' />
                                                <asp:HiddenField ID="hfpdpgoalsmstunkid" runat="server" Value='<%#Eval("pdpgoalsmstunkid")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Training Name" />
                                    </Columns>
                                </asp:GridView>
                                <asp:Panel ID="pnlNoReviewTraining" runat="server" CssClass="no-comments" Visible="false">
                                    <p>
                                        <asp:Label ID="lblNoReviewTraining" runat="server" Text="No Data"></asp:Label>
                                    </p>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ReviewTraining" runat="server" />
                        <asp:Button ID="btnRejectTraining" runat="server" Text="Reject Training" CssClass="btn btn-danger pull-left" />
                        <asp:Button ID="btnApprovedLock" runat="server" Text="Approved & Lock" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseReviewTraining" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <uc5:DelReason ID="delReason" runat="server" Title="Please enter delete reason?" />
                <asp:HiddenField ID="TabActionPlanName" runat="server" />
                <asp:HiddenField ID="TabName" runat="server" />
                <asp:HiddenField ID="hfCommentScrollPosition" Value="0" runat="server" />
                <asp:HiddenField ID="CollapseActionPlanName" runat="server" />
                <asp:HiddenField ID="CollapsePersonalDevlopmentGoalName" runat="server" />
                <asp:HiddenField ID="CollapsePersonalDevlopmentGoalCategory" runat="server" />
                 <uc6:Confirm ID="cfConfirmation" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>

        $(document).ready(function() {
            RetriveTab();
			RetriveCollapse();
//            $('#Tabs li > a[href="#DevelopmentActionplan"]').parent().removeClass('active').css('display', 'none');
            ImageLoad();
        });
        
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
		    $("#fileuploader").uploadFile({
			    url: "wPgEmployeePdp.aspx?uploadimage=mSEfU19VPc4=",
                method: "POST",
				dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                maxFileSize: 1024 * 1024,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
				    $("#<%= btnSaveAttachment.ClientID %>").click();
                },
                    onError: function(files, status, errMsg) {
	                alert(errMsg);
                }
            });
        }
        }
        $('body').on("click", "input[type=file]", function() {
            if ($(this).attr("class") != "flupload") {
		    return IsValidAttach();
		    }
        });
        
        function RetriveTab() {

            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "Employee_Detail";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
            
            
           var actionPlanTabName = $("[id*=TabActionPlanName]").val() != "" ? $("[id*=TabActionPlanName]").val() : "ActionPlanDetail";
            $('#ActionPlanTabs a[href="#' + actionPlanTabName + '"]').tab('show');
            $("#ActionPlanTabs a").click(function() {
                $("[id*=TabActionPlanName]").val($(this).attr("href").replace("#", ""));
            });
        }
        
        function RetriveCollapse() {
			var actionPlanCollapseName = $("[id*=CollapseActionPlanName]").val();
            if (actionPlanCollapseName != "") {
				$("#" + actionPlanCollapseName + "").collapse('show');
			}
			
			$(".ActionCategoryCollapse").click(function() {
				$("[id*=CollapseActionPlanName]").val($(this).attr("href").replace("#", ""));
			});				
			

			var collapsePersonalDevlopmentGoalName = $("[id*=CollapsePersonalDevlopmentGoalName]").val();
            if (collapsePersonalDevlopmentGoalName != "") {
				$("#" + collapsePersonalDevlopmentGoalName + "").collapse('show');
			}
			
			$(".PersonalDevlopmentGoalCollapse").click(function() {
				$("[id*=CollapsePersonalDevlopmentGoalName]").val($(this).attr("href").replace("#", ""));
			});				

			
            var CollapsePersonalDevlopmentGoalCategory = $("[id*=CollapsePersonalDevlopmentGoalCategory]").val();
            if (CollapsePersonalDevlopmentGoalCategory != "") {
				$("#" + CollapsePersonalDevlopmentGoalCategory + "").collapse('show');
        }
			
			$(".PersonalDevlopmentGoalCollapseCategory").click(function() {
				$("[id*=CollapsePersonalDevlopmentGoalCategory]").val($(this).attr("href").replace("#", ""));
			});				
        }
    </script>

</asp:Content>
