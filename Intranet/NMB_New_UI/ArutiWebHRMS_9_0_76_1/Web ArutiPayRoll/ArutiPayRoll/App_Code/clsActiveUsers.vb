﻿Imports Microsoft.VisualBasic

Public Class clsActiveUsers
    Implements IDisposable

    Private disposedValue As Boolean = False        ' To detect redundant calls

#Region " Members "
    Public mstrUserId As String
    Public mstrUserName As String
    Public menUserType As enUserType
    Public mblnIsActive As Boolean
    Public mdtLastSeen As DateTime
    Public mintLastMessageReceived As Integer

#End Region

#Region " Constructors "
    Public Sub New(ByVal strUserId As String, ByVal strUserName As String, ByVal enUserType As enUserType)
        mstrUserId = strUserId
        mstrUserName = strUserName
        menUserType = enUserType
        mblnIsActive = False
        mdtLastSeen = DateTime.MinValue
        mintLastMessageReceived = 0
    End Sub
#End Region

    Public Enum enUserType
        User = 0
        Employee = 1
    End Enum

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).

                mstrUserId = ""
                mstrUserName = ""
                mblnIsActive = False
                mdtLastSeen = DateTime.MinValue
                mintLastMessageReceived = 0
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
