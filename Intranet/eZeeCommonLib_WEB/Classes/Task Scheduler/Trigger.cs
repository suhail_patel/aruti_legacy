using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using TaskScheduler.Definition;
using System.Runtime.InteropServices;

namespace TaskScheduler
{
	public sealed class Trigger : System.MarshalByRefObject, System.IDisposable
	{

#region  Enums 

		public enum Types: int
		{
			Once = 0,
			Daily,
			Weekly,
			MonthlyDate,
			MonthlyDOW,
			OnIdle,
			SystemStart,
			LogOn
		}

		[Flags()]
		public enum Months: short
		{
			January = 0X1,
			February = 0X2,
			March = 0X4,
			April = 0X8,
			May = 0X10,
			June = 0X20,
			July = 0X40,
			August = 0X80,
			September = 0X100,
			October = 0X200,
			November = 0X400,
			December = 0X800
		}

		[Flags()]
		public enum Days: short
		{
			Sunday = 0X1,
			Monday = 0X2,
			Tuesday = 0X4,
			Wednesday = 0X8,
			Thursday = 0X10,
			Friday = 0X20,
			Saturday = 0X40
		}

		public enum Weeks: short
		{
			First = 1,
			Second = 2,
			Third = 3,
			Fourth = 4,
			Last = 5
		}

		[Flags()]
		public enum TriggerFlags: int
		{
			HasEndDate = 0X1,
			KillAtDurationEnd = 0X2,
			TriggerDisabled = 0X4
		}

#endregion

#region  Fields 

		private Task m_Task;
		private ITaskTrigger m_ITaskTriggerObj;
		private TASK_TRIGGER m_Trigger;
		private short m_Index;

#endregion

#region  Constructors 

		internal Trigger(ITaskTrigger Trigger, Task ParentTask, short Index)
		{

			m_ITaskTriggerObj = Trigger;
			m_Task = ParentTask;
			m_Index = Index;

			m_Trigger.cbTriggerSize = System.Convert.ToInt16(Marshal.SizeOf(m_Trigger));
			m_ITaskTriggerObj.GetTrigger(ref m_Trigger);

			m_Task.SaveChanges += new Task.SaveChangesEventHandler(Update);

		}

#endregion

#region  Protected Methods 

        ~Trigger()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (m_ITaskTriggerObj != null)
            {
                // Release the COM object
                Marshal.ReleaseComObject(m_ITaskTriggerObj);
                m_ITaskTriggerObj = null;
                m_Task = null;

                // Supress GC Finalize call
                GC.SuppressFinalize(this);
            }
        }

#endregion

#region  Public Properties 

		public short Index
		{
			get
			{
				return m_Index;
			}
		}

		public short DaysInterval
		{
			get
			{

				if (m_Trigger.TriggerType != Types.Daily)
				{
					throw new InvalidTriggerException();
				}

				return m_Trigger.Type.Daily.DaysInterval;

			}
			set
			{

				if (m_Trigger.TriggerType != Types.Daily)
				{
					throw new InvalidTriggerException();
				}

				m_Trigger.Type.Daily.DaysInterval = value;

			}
		}

        //public int Day
        //{
        //    get
        //    {

        //        if (m_Trigger.TriggerType != Types.MonthlyDate)
        //        {
        //            throw new InvalidTriggerException();
        //        }

        //        return m_Trigger.Type.MonthlyDate.rgfDays;

        //    }
        //    set
        //    {

        //        if (m_Trigger.TriggerType != Types.MonthlyDate)
        //        {
        //            throw new InvalidTriggerException();
        //        }
        //        m_Trigger.Type.MonthlyDate.rgfDays = value;

        //    }
        //}

        public int Day
        {
            get
            {

                if (m_Trigger.TriggerType != Types.MonthlyDate)
                {
                    throw new InvalidTriggerException();
                }

                return (int)(Math.Log((double)m_Trigger.Type.MonthlyDate.rgfDays) / Math.Log((double)2)) + 1;

            }
            set
            {

                if (m_Trigger.TriggerType != Types.MonthlyDate)
                {
                    throw new InvalidTriggerException();
                }
                m_Trigger.Type.MonthlyDate.rgfDays = (int)Math.Pow(2, value - 1);

            }
        }

		public Months Month
		{
			get
			{

				switch (m_Trigger.TriggerType)
				{
					case Types.MonthlyDate:
						return (Months)m_Trigger.Type.MonthlyDate.rgfMonths;
					case Types.MonthlyDOW:
						return (Months)m_Trigger.Type.MonthlyDOW.rgfMonths;
					default:
						throw new InvalidTriggerException();
				}

			}
			set
			{

				switch (m_Trigger.TriggerType)
				{
					case Types.MonthlyDate:
						m_Trigger.Type.MonthlyDate.rgfMonths = value;
						break;
					case Types.MonthlyDOW:
						m_Trigger.Type.MonthlyDOW.rgfMonths = value;
						break;
					default:
						throw new InvalidTriggerException();
				}

			}
		}

		public Days DayOfTheWeek
		{
			get
			{

				switch (m_Trigger.TriggerType)
				{
					case Types.Weekly:
						return (Days)m_Trigger.Type.MonthlyDOW.rgfDaysOfTheWeek;
					case Types.MonthlyDOW:
						return (Days)m_Trigger.Type.Weekly.rgfDaysOfTheWeek;
					default:
						throw new InvalidTriggerException();
				}

			}
			set
			{
				switch (m_Trigger.TriggerType)
				{
					case Types.Weekly:
						m_Trigger.Type.Weekly.rgfDaysOfTheWeek = value;
						break;
					case Types.MonthlyDOW:
						m_Trigger.Type.MonthlyDOW.rgfDaysOfTheWeek = value;
						break;
					default:
						throw new InvalidTriggerException();
				}
			}
		}

		public Weeks Week
		{
			get
			{

				if (m_Trigger.TriggerType != Types.MonthlyDOW)
				{
					throw new InvalidTriggerException();
				}

				return (Weeks)m_Trigger.Type.MonthlyDOW.wWhichWeek;

			}
			set
			{

				if (m_Trigger.TriggerType != Types.MonthlyDOW)
				{
					throw new InvalidTriggerException();
				}

				m_Trigger.Type.MonthlyDOW.wWhichWeek = value;

			}
		}

		public short WeeksInterval
		{
			get
			{

				if (m_Trigger.TriggerType != Types.Weekly)
				{
					throw new InvalidTriggerException();
				}

				return m_Trigger.Type.Weekly.WeeksInterval;

			}
			set
			{

				if (m_Trigger.TriggerType != Types.Weekly)
				{
					throw new InvalidTriggerException();
				}

				m_Trigger.Type.Weekly.WeeksInterval = value;

			}
		}

		public string Text
		{
			get
			{
				return m_ITaskTriggerObj.GetTriggerString();
			}
		}

		public Types TriggerType
		{
			get
			{
				return m_Trigger.TriggerType;
			}
			set
			{
				m_Trigger.TriggerType = value;
			}
		}

		public int Duration
		{
			get
			{
				return m_Trigger.MinutesDuration;
			}
			set
			{
				m_Trigger.MinutesDuration = value;
			}
		}

		public int Interval
		{
			get
			{
				return m_Trigger.MinutesInterval;
			}
			set
			{
				m_Trigger.MinutesInterval = value;
			}
		}

		public TriggerFlags Flags
		{
			get
			{
				return m_Trigger.rgFlags;
			}
			set
			{
				m_Trigger.rgFlags = value;
			}
		}

		public System.DateTime BeginDay
		{
			get
			{
				return new System.DateTime(m_Trigger.wBeginYear, m_Trigger.wBeginMonth, m_Trigger.wBeginDay);
			}
			set
			{
				m_Trigger.wBeginDay = System.Convert.ToInt16(value.Day);
				m_Trigger.wBeginMonth = System.Convert.ToInt16(value.Month);
				m_Trigger.wBeginYear = System.Convert.ToInt16(value.Year);
			}
		}

		public System.DateTime EndDay
		{
			get
			{
				return new System.DateTime(m_Trigger.wEndYear, m_Trigger.wEndMonth, m_Trigger.wEndDay);
			}
			set
			{
				m_Trigger.wEndDay = System.Convert.ToInt16(value.Day);
				m_Trigger.wEndMonth = System.Convert.ToInt16(value.Month);
				m_Trigger.wEndYear = System.Convert.ToInt16(value.Year);
			}
		}

		public short RandomInterval
		{
			get
			{
				return m_Trigger.wRandomMinutesInterval;
			}
			set
			{
				m_Trigger.wRandomMinutesInterval = value;
			}
		}

		public System.DateTime StartTime
		{
			get
			{
				return new System.DateTime(1, 1, 1, m_Trigger.wStartHour, m_Trigger.wStartMinute, 0);
			}
			set
			{
				m_Trigger.wStartHour = System.Convert.ToInt16(value.Hour);
				m_Trigger.wStartMinute = System.Convert.ToInt16(value.Minute);
            }
		}

#endregion

#region  Public Methods 

		public void Refresh()
		{
			m_ITaskTriggerObj.GetTrigger(ref m_Trigger);
		}

		public void Update()
		{
			m_ITaskTriggerObj.SetTrigger(ref m_Trigger);
			m_ITaskTriggerObj.GetTrigger(ref m_Trigger);
		}


#endregion
	}
}
