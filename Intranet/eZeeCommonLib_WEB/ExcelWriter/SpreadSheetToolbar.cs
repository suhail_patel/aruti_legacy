﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Xml;

    public sealed class SpreadSheetToolbar : IWriter, IReader, ICodeWriter
    {
        private bool _hidden;

        internal SpreadSheetToolbar()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._hidden)
            {
                Util.AddAssignment(method, targetObject, "Hidden", this._hidden);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._hidden = element.GetAttribute("Hidden", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("c", "Toolbar", "urn:schemas-microsoft-com:office:component:spreadsheet");
            if (this._hidden)
            {
                writer.WriteAttributeString("s", "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Toolbar", "urn:schemas-microsoft-com:office:component:spreadsheet");
        }

        public bool Hidden
        {
            get
            {
                return this._hidden;
            }
            set
            {
                this._hidden = value;
            }
        }
    }
}

