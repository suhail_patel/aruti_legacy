﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class ExcelWorkbook : IWriter, IReader, ICodeWriter
    {
        private int _activeSheet = -2147483648;
        private bool _hideWorkbookTabs;
        private ExcelLinksCollection _links;
        private bool _protectStructure;
        private bool _protectWindows;
        private int _windowHeight = -2147483648;
        private int _windowTopX = -2147483648;
        private int _windowTopY = -2147483648;
        private int _windowWidth = -2147483648;

        internal ExcelWorkbook()
        {
            CrnCollection.GlobalCounter = 0;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._links != null)
            {
                ((ICodeWriter) this._links).WriteTo(type, method, targetObject);
            }
            if (this._hideWorkbookTabs)
            {
                Util.AddAssignment(method, targetObject, "HideWorkbookTabs", this._hideWorkbookTabs);
            }
            if (this._windowHeight != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "WindowHeight", this._windowHeight);
            }
            if (this._windowWidth != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "WindowWidth", this._windowWidth);
            }
            if (this._windowTopX != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "WindowTopX", this._windowTopX);
            }
            if (this._windowTopY != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "WindowTopY", this._windowTopY);
            }
            if (this._activeSheet != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "ActiveSheetIndex", this._activeSheet);
            }
            Util.AddAssignment(method, targetObject, "ProtectWindows", this._protectWindows);
            Util.AddAssignment(method, targetObject, "ProtectStructure", this._protectStructure);
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (Util.IsElement(element2, "HideWorkbookTabs", "urn:schemas-microsoft-com:office:excel"))
                    {
                        this._hideWorkbookTabs = true;
                    }
                    else
                    {
                        if (Util.IsElement(element2, "WindowHeight", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._windowHeight = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "WindowTopX", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._windowTopX = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "WindowTopY", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._windowTopY = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "WindowWidth", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._windowWidth = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "ActiveSheet", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._activeSheet = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (SupBook.IsElement(element2))
                        {
                            SupBook link = new SupBook();
                            ((IReader) link).ReadXml(element2);
                            this.Links.Add(link);
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "ExcelWorkbook", "urn:schemas-microsoft-com:office:excel");
            if (this._links != null)
            {
                ((IWriter) this._links).WriteXml(writer);
            }
            if (this._hideWorkbookTabs)
            {
                writer.WriteElementString("HideWorkbookTabs", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._windowHeight != -2147483648)
            {
                writer.WriteElementString("WindowHeight", "urn:schemas-microsoft-com:office:excel", this._windowHeight.ToString());
            }
            if (this._windowTopX != -2147483648)
            {
                writer.WriteElementString("WindowTopX", "urn:schemas-microsoft-com:office:excel", this._windowTopX.ToString());
            }
            if (this._windowTopY != -2147483648)
            {
                writer.WriteElementString("WindowTopY", "urn:schemas-microsoft-com:office:excel", this._windowTopY.ToString());
            }
            if (this._windowWidth != -2147483648)
            {
                writer.WriteElementString("WindowWidth", "urn:schemas-microsoft-com:office:excel", this._windowWidth.ToString());
            }
            if (this._activeSheet != -2147483648)
            {
                writer.WriteElementString("ActiveSheet", "urn:schemas-microsoft-com:office:excel", this._activeSheet.ToString());
            }
            Util.WriteElementString(writer, "ProtectStructure", "urn:schemas-microsoft-com:office:excel", this._protectStructure);
            Util.WriteElementString(writer, "ProtectWindows", "urn:schemas-microsoft-com:office:excel", this._protectWindows);
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "ExcelWorkbook", "urn:schemas-microsoft-com:office:excel");
        }

        public int ActiveSheetIndex
        {
            get
            {
                return this._activeSheet;
            }
            set
            {
                this._activeSheet = value;
            }
        }

        public bool HideWorkbookTabs
        {
            get
            {
                return this._hideWorkbookTabs;
            }
            set
            {
                this._hideWorkbookTabs = value;
            }
        }

        public ExcelLinksCollection Links
        {
            get
            {
                if (this._links == null)
                {
                    this._links = new ExcelLinksCollection();
                }
                return this._links;
            }
        }

        public bool ProtectStructure
        {
            get
            {
                return this._protectStructure;
            }
            set
            {
                this._protectStructure = value;
            }
        }

        public bool ProtectWindows
        {
            get
            {
                return this._protectWindows;
            }
            set
            {
                this._protectWindows = value;
            }
        }

        public int WindowHeight
        {
            get
            {
                return this._windowHeight;
            }
            set
            {
                this._windowHeight = value;
            }
        }

        public int WindowTopX
        {
            get
            {
                return this._windowTopX;
            }
            set
            {
                this._windowTopX = value;
            }
        }

        public int WindowTopY
        {
            get
            {
                return this._windowTopY;
            }
            set
            {
                this._windowTopY = value;
            }
        }

        public int WindowWidth
        {
            get
            {
                return this._windowWidth;
            }
            set
            {
                this._windowWidth = value;
            }
        }
    }
}

