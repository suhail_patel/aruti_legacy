﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetNamedRangeCollection : CollectionBase, IWriter, ICodeWriter, IReader
    {
        internal WorksheetNamedRangeCollection()
        {
        }

        public int Add(WorksheetNamedRange namedRange)
        {
            if (namedRange == null)
            {
                throw new ArgumentNullException("namedRange");
            }
            return base.InnerList.Add(namedRange);
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                WorksheetNamedRange range = this[i];
                method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { new CodeObjectCreateExpression(new CodeTypeReference(typeof(WorksheetNamedRange)), new CodeExpression[] { new CodePrimitiveExpression(range.Name), new CodePrimitiveExpression(range.RefersTo), new CodePrimitiveExpression(range.Hidden) }) }));
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if ((element2 != null) && WorksheetNamedRange.IsElement(element2))
                {
                    WorksheetNamedRange namedRange = new WorksheetNamedRange();
                    ((IReader) namedRange).ReadXml(element2);
                    this.Add(namedRange);
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Names", "urn:schemas-microsoft-com:office:spreadsheet");
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public bool Contains(WorksheetNamedRange item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(WorksheetNamedRange[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(WorksheetNamedRange item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, WorksheetNamedRange item)
        {
            base.InnerList.Insert(index, item);
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Names", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public void Remove(WorksheetNamedRange item)
        {
            base.InnerList.Remove(item);
        }

        public WorksheetNamedRange this[int index]
        {
            get
            {
                return (WorksheetNamedRange) base.InnerList[index];
            }
        }
    }
}

