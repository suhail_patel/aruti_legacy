﻿namespace ExcelWriter
{
    using System;

    public enum Orientation
    {
        NotSet,
        Portrait,
        Landscape
    }
}

