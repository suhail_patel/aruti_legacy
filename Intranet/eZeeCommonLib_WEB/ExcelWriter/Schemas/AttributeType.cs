﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Xml;

    public sealed class AttributeType : SchemaType, IWriter
    {
        private string _dataType;
        private string _name;
        private string _rsname;

        public AttributeType()
        {
        }

        public AttributeType(string name, string rowsetName, string dataType)
        {
            this._name = name;
            this._rsname = rowsetName;
            this._dataType = dataType;
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "AttributeType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
            if (this._name != null)
            {
                writer.WriteAttributeString("s", "name", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._name);
            }
            if (this._rsname != null)
            {
                writer.WriteAttributeString("rs", "name", "urn:schemas-microsoft-com:rowset", this._rsname);
            }
            if (this._dataType != null)
            {
                writer.WriteStartElement("dt", "datatype", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
                writer.WriteAttributeString("dt", "type", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", this._dataType);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        public string DataType
        {
            get
            {
                return this._dataType;
            }
            set
            {
                this._dataType = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public string RowsetName
        {
            get
            {
                return this._rsname;
            }
            set
            {
                this._rsname = value;
            }
        }
    }
}

