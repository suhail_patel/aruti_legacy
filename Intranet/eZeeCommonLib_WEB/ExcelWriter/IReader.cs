﻿namespace ExcelWriter
{
    using System;
    using System.Xml;

    internal interface IReader
    {
        void ReadXml(XmlElement element);
    }
}

