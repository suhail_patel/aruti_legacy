﻿namespace ExcelWriter
{
    using System;

    public enum UnderlineStyle
    {
        None,
        Single,
        Double,
        SingleAccounting,
        DoubleAccounting
    }
}

