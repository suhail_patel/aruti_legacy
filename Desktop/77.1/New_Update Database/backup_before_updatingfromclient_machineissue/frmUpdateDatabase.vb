﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports System.Security

#End Region

Public Class frmUpdateDatabase

#Region " Private Variables "

    Private dtConfigScript As DataTable = Nothing
    Private dtTranScript As DataTable = Nothing
    Private dtImageScript As DataTable = Nothing
    Private dsList As New DataSet
    Private mstrDatabaseName As String = String.Empty
    Private mstrArgs As String = String.Empty '/N -> New Installation, /S -> Service Pack, /T -> Only Transaction Database Create Script 
    Private dtTemp() As DataRow = Nothing
    Private strResource As String = String.Empty
    Private mIntTotalDatabases As Integer = 0
    Private arrDatabase As ArrayList
    Private iDBCnt As Integer = 0
    Private iReportCnt As Integer = 1
    Private mblnArutiImg As Boolean = False
    Private StrDB_Version As String = String.Empty
    Private Org_DbVersion As String = String.Empty
    Private iRet_Value As Integer = 0
    Private Const conREG_NODE As String = "Software\NPK\Aruti"
    Private blnIsSameVersion As Boolean = False

    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
    Private Const CP_NOCLOSE_BUTTON As Integer = &H200
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myCp As CreateParams = MyBase.CreateParams
            myCp.ClassStyle = myCp.ClassStyle Or CP_NOCLOSE_BUTTON
            Return myCp
        End Get
    End Property
    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>

#End Region

#Region " API Functions "

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function GetPrivateProfileString(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As System.Text.StringBuilder, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="WritePrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function WritePrivateProfileString(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileIntA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function GetPrivateProfileInt(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Integer, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="WritePrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function FlushPrivateProfileString(ByVal lpApplicationName As Integer, ByVal lpKeyName As Integer, ByVal lpString As Integer, ByVal lpFileName As String) As Integer
    End Function

#End Region

#Region " Forms Event(s) "

    Private Sub frmUpdateDatabase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim objDatabase As New eZeeDatabase
            'objDatabase.ServerName = "(local)"
            'objDatabase.Connect()

            Dim mstrServerName As String = ""
            Dim xAppPath As String = getValue()

            If xAppPath.Trim.Length > 0 Then
                mstrServerName = GetString("Aruti_Payroll", "Server", xAppPath)
                If mstrServerName = "" Then mstrServerName = "(local)"
            Else
                mstrServerName = "(local)"
            End If

            Dim aCore32_Ver As Integer = 0
            If IO.File.Exists(xAppPath & "acore32.dll") Then
                Dim vr As FileVersionInfo = FileVersionInfo.GetVersionInfo(xAppPath & "acore32.dll")
                aCore32_Ver = CInt(vr.FileVersion.Replace(".", ""))
            End If

            objDatabase.ServerName = mstrServerName
            objDatabase.Connect()

            Create_Aruti_User()

            If My.Application.CommandLineArgs.Count > 0 Then
                mstrArgs = My.Application.CommandLineArgs(0).ToString
                Try
                    mstrDatabaseName = My.Application.CommandLineArgs(1).ToString
                Catch ex As Exception
                    mstrDatabaseName = ""
                End Try
            End If

            Using objD As New clsDataOperation
                If objD.RecordCount("SELECT 1 FROM sys.databases WHERE UPPER(name) = 'ARUTIIMAGES'") > 0 Then mblnArutiImg = True
            End Using

            iDBCnt += 1

            Select Case mstrArgs.ToString.ToUpper

                Case "/N" 'THIS IS FOR NEW INSTALLTION

                    Call Generate_Config_Script()
                    mIntTotalDatabases = 1
                    pbProgress.Maximum = dtConfigScript.Rows.Count
                    lblDataCount.Text = "Creating Database. Please Wait."

                Case "/S" 'THIS IS FOR SERVICE PACK
                    eZeeDatabase.change_database("hrmsConfiguration")
                    objDataOperation = New clsDataOperation
                    StrDB_Version = objDataOperation.getVersion
                    Org_DbVersion = StrDB_Version
                    Dim sOrgVersion As String = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name & " : " & StrDB_Version
                    Dim StrVer() As String = StrDB_Version.Split(CChar("."))
                    StrDB_Version = ""
                    For i As Integer = 0 To StrVer.Length - 1
                        StrDB_Version &= CInt(StrVer(i))
                    Next

                    Call Generate_Config_Script(sOrgVersion)
                    Dim dCView As DataView = dtConfigScript.DefaultView
                    dCView.RowFilter = "CheckType >=" & StrDB_Version
                    dtConfigScript = dCView.ToTable

                    Call Generate_Trans_Script(sOrgVersion)
                    Dim dTView As DataView = dtTranScript.DefaultView
                    dTView.RowFilter = "CheckType >=" & StrDB_Version
                    dtTranScript = dTView.ToTable

                    If mblnArutiImg = True Then
                        Call Generate_Images_Script(sOrgVersion)
                        Dim dIView As DataView = dtImageScript.DefaultView
                        dIView.RowFilter = "CheckType >=" & StrDB_Version
                        dtImageScript = dIView.ToTable
                    End If

                    arrDatabase = New ArrayList

                    If mstrDatabaseName = "" Then

                        arrDatabase = objDatabase.fillTranDatabaseList
                    Else

                        arrDatabase = New ArrayList
                        arrDatabase.Add(mstrDatabaseName)
                    End If

                    If mblnArutiImg = True Then
                        mIntTotalDatabases = arrDatabase.Count + 2
                    Else
                        mIntTotalDatabases = arrDatabase.Count + 1
                    End If
                    pbProgress.Maximum = mIntTotalDatabases
                    objDataOperation = Nothing

                    lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database : " & iDBCnt.ToString

                Case "/T" 'THIS IS FOR NEW COMPANY MADE FROM ARUTI CONFIGURATION
                    Call Generate_Trans_Script()
                    mIntTotalDatabases = 1
                    pbProgress.Maximum = dtTranScript.Rows.Count
                    arrDatabase = New ArrayList
                    arrDatabase.Add(mstrDatabaseName)
                    lblDataCount.Text = "Creating Database. Please Wait."

                Case Else 'RUNNING FULL SCRIPT IN CASE OF ANY SCRIPT LEFT OUT 

                    Using objD As New clsDataOperation
                        If objD.RecordCount("SELECT 1 FROM sys.databases WHERE UPPER(name) = 'HRMSCONFIGURATION'") <= 0 Then Me.Close()
                    End Using

                    Call Generate_Config_Script()
                    Call Generate_Trans_Script()


                    If mblnArutiImg = True Then
                        Call Generate_Images_Script()
                    End If

                    arrDatabase = New ArrayList
                    If mstrDatabaseName = "" Then
                        arrDatabase = objDatabase.fillTranDatabaseList
                    Else
                        arrDatabase = New ArrayList
                        arrDatabase.Add(mstrDatabaseName)
                    End If


                    If arrDatabase.Count > 0 Then
                        If mblnArutiImg = True Then
                            mIntTotalDatabases = arrDatabase.Count + 2
                        Else
                            mIntTotalDatabases = arrDatabase.Count + 1
                        End If
                    Else
                        mIntTotalDatabases = 1
                    End If
                    pbProgress.Maximum = mIntTotalDatabases

                    lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database : " & iDBCnt.ToString
            End Select

            Using ObjDO As New eZeeCommonLib.clsDataOperation
                dsList = New DataSet
                dsList = ObjDO.ExecQuery("SELECT DISTINCT  " & _
                                         " REVERSE(RIGHT(REVERSE(physical_name),(LEN(physical_name)-CHARINDEX('\', REVERSE(physical_name),1))+1)) As PhyPath  " & _
                                         ",sys.databases.name as dbName " & _
                                         "FROM sys.master_files " & _
                                         "JOIN sys.databases ON sys.master_files.database_id = sys.databases.database_id ", "List")
            End Using
            bgWorker.RunWorkerAsync()
        Catch ex As Exception
            Select Case mstrArgs.ToString.ToUpper
                Case "/N"
                    Using objDa As New clsDataOperation
                        objDa.ExecNonQuery("Use [master] ")
                        objDa.ExecNonQuery("DROP DATABASE hrmsConfiguration")
                    End Using                
            End Select
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Me.Close()
        End Try
    End Sub

#End Region

#Region " Private Function(s) "

    Public Function GetString(ByVal Section As String, ByVal Key As String, ByVal AppPath As String) As String
        Return GetString(Section, Key, "", AppPath)
    End Function

    Public Function GetString(ByVal Section As String, ByVal Key As String, ByVal [Default] As String, ByVal mstrAppPath As String) As String
        Dim intCharCount As Integer = 0
        Dim strResult As String = ""
        Dim objResult As New System.Text.StringBuilder(256)
        intCharCount = GetPrivateProfileString(Section, Key, "", objResult, objResult.Capacity, mstrAppPath & "aruti.ini")

        If intCharCount > 0 Then
            strResult = objResult.ToString().Substring(0, intCharCount)
        End If
        Return strResult
    End Function

    Public Function getValue() As String
        Dim Key As Microsoft.Win32.RegistryKey = Nothing
        Dim Value As String = String.Empty
        Try
            Key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(conREG_NODE, True)
            If Key Is Nothing Then
                Value = ""
            End If
            Value = CStr(Key.GetValue("AppPath"))
        Catch e As Exception
            Value = ""
        End Try
        Return Value
    End Function

    Private Sub Generate_Config_Script(Optional ByVal sVersion As String = "")
        Dim sMsg As String = String.Empty
        Try
            dtConfigScript = Nothing
            '/****************Data Type Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.DataType_1
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript = dsList.Tables(0)
            Catch ex As Exception
                sMsg = "File : [DATA TYPE] "
                Throw ex
            End Try
            '/****************Data Type Script********************/ END

            '/****************Configuration Main Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Config_Main_2
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript.Merge(dsList.Tables(0), True)
            Catch ex As Exception
                sMsg = "File : Configuration [MAIN] "
                Throw ex
            End Try
            '/****************Configuration Main Script********************/ END

            '/****************Configuration Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Config_Alter_3
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Configuration [ALTER] "
                    Throw ex
                End Try
            End If
            '/****************Configuration Alter Script********************/ END

            '/****************Configuration UDF Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Config_UDF_4
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Configuration [USER DEFINE]"
                    Throw ex
                End Try
            End If
            '/****************Configuration UDF Script********************/ END
            If dtConfigScript.Rows.Count <= 0 Then
                MsgBox("Sorry, Error in creating database script. Please contact Aruti Support Team.", MsgBoxStyle.Critical)
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("Generate_Config_Script : " & sMsg & " ;" & vbCrLf & ex.Message, MsgBoxStyle.Critical, sVersion)
            Me.Close()
        End Try
    End Sub

    Private Sub Generate_Trans_Script(Optional ByVal sVersion As String = "")
        Dim sMsg As String = String.Empty
        Try
            dtTranScript = Nothing
            '/****************Data Type Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.DataType_1
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript = dsList.Tables(0)
            Catch ex As Exception
                sMsg = "File : [DATA TYPE]"
                Throw ex
            End Try
            '/****************Data Type Script********************/ END

            '/****************Transaction Main Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Tran_Main_5
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript.Merge(dsList.Tables(0), True)
            Catch ex As Exception
                sMsg = "File : Transaction [MAIN]"
                Throw ex
            End Try
            '/****************Transaction Main Script********************/ END

            '/****************Transaction Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Tran_Alter_6
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Transaction [ALTER]"
                    Throw ex
                End Try
            End If
            '/****************Transaction Alter Script********************/ END

            '/****************Transaction UDF Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Tran_UDF_7
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Transaction [USER DEFINE]"
                    Throw ex
                End Try
            End If
            '/****************Transaction UDF Script********************/ END
            If dtTranScript.Rows.Count <= 0 Then
                MsgBox("Sorry, Error in creating database script. Please contact Aruti Support Team.", MsgBoxStyle.Critical)
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("Generate_Trans_Script : " & sMsg & " ;" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
            Me.Close()
        End Try
    End Sub

    Private Sub Generate_Images_Script(Optional ByVal sVersion As String = "")
        Try
            dtImageScript = Nothing
            '/****************Aruti Images Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Img_Alter_8
            If strResource.Trim.Length > 0 Then
                dsList.ReadXml(New IO.StringReader(strResource)) : dtImageScript = dsList.Tables(0)
            End If
            '/****************Aruti Images Alter Script********************/ END
        Catch ex As Exception
            MsgBox("Generate_Images_Script : " & ex.Message, MsgBoxStyle.Critical, sVersion)
        End Try
    End Sub

    Private Function Create_Aruti_User() As Boolean
        Try
            Dim xQry As String = ""
            xQry = "IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = 'aruti_sa') " & _
                       "BEGIN " & _
                           "USE [master] " & _
                           "CREATE LOGIN [aruti_sa] WITH PASSWORD=N'" & lblDataCount.Tag.ToString() & "', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'bulkadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'dbcreator' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'diskadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'processadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'securityadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'serveradmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'setupadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'sysadmin' " & _
                       "END "
            Using objdo As New clsDataOperation

                objdo.ExecNonQuery(xQry)

                If objdo.ErrorMessage <> "" Then
                    Throw New Exception(objdo.ErrorNumber & ":" & objdo.ErrorMessage)
                End If

            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Controls Event(s) "

    Private Sub bgWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        Dim StrQ As String = String.Empty
        Dim sMsg As String = String.Empty
        Try
            iDBCnt = 1
            Using objDataOperation As New clsDataOperation
                '/**************** RUNNING CONFIGURATION SCRIPT ****************/ START
                If dtConfigScript IsNot Nothing Then
                    StrQ = "USE hrmsConfiguration "
                    sMsg = "hrmsConfiguration"
                    objDataOperation.ExecNonQuery(StrQ.Trim)

                    If Not objDataOperation.ErrorMessage = "" Then
                        MsgBox(objDataOperation.ErrorMessage)
                        Exit Sub
                    End If
                    For iCnt As Integer = 0 To dtConfigScript.Rows.Count - 1
                        StrQ = CStr(dtConfigScript.Rows(iCnt).Item("Script"))
                        objDataOperation.ExecNonQuery(StrQ.Trim)
                        If Not objDataOperation.ErrorMessage = "" Then
                            MsgBox(objDataOperation.ErrorMessage)
                            Exit For
                        End If
                        Select Case mstrArgs.ToString.ToUpper
                            Case "/N", "/T"
                                bgWorker.ReportProgress(iCnt)
                        End Select
                        System.Threading.Thread.Sleep(100)
                    Next
                    Select Case mstrArgs.ToString.ToUpper
                        Case "/N", "/T"
                            Case Else
                            iDBCnt += 1
                                bgWorker.ReportProgress(iDBCnt)
                        End Select
                End If
                '/**************** RUNNING CONFIGURATION SCRIPT ****************/ END


                '/**************** RUNNING TRANSACTION SCRIPT ****************/ START
                If dtTranScript IsNot Nothing Then
                    Dim StrPhysicalPath As String = String.Empty
                    For Each objItem As Object In arrDatabase
                        If objItem.ToString.ToUpper().StartsWith("TRAN_") Then
                            If dsList.Tables(0).Rows.Count > 0 Then  ' if database is not present in physical path then it will ignore.
                                dtTemp = dsList.Tables(0).Select("dbName='" & objItem.ToString & "'")
                                If dtTemp.Length > 0 Then
                                    StrPhysicalPath = CStr(dtTemp(0)("PhyPath"))
                                End If
                            End If

                            If StrPhysicalPath.Length > 0 Then
                                If System.IO.File.Exists(StrPhysicalPath & objItem.ToString & ".mdf") = True And System.IO.File.Exists(StrPhysicalPath & objItem.ToString & "_log.LDF") = True Then
                                    objDataOperation.ExecNonQuery("USE " & objItem.ToString)
                                    sMsg = objItem.ToString
                                    For iCnt As Integer = 0 To dtTranScript.Rows.Count - 1

                                        StrQ = CStr(dtTranScript.Rows(iCnt).Item("Script"))
                                        objDataOperation.ExecNonQuery(StrQ.Trim)
                                        If Not objDataOperation.ErrorMessage = "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        End If
                                        Select Case mstrArgs.ToString.ToUpper
                                            Case "/N", "/T"
                                                bgWorker.ReportProgress(iCnt)
                                        End Select
                                        System.Threading.Thread.Sleep(100)
                                    Next
                                    Select Case mstrArgs.ToString.ToUpper
                                        Case "/N", "/T"
                                        Case Else
                                            iDBCnt += 1
                                            bgWorker.ReportProgress(iDBCnt)
                                    End Select
                                End If
                            End If

                        End If
                    Next
                End If
                '/**************** RUNNING TRANSACTION SCRIPT ****************/ END

                '/**************** RUNNING IMAGES DATABASE SCRIPT ****************/ START
                If mblnArutiImg = True Then
                    If dtImageScript IsNot Nothing Then
                        objDataOperation.ExecNonQuery("USE arutiimages")
                        sMsg = "arutiimages"
                        If Not objDataOperation.ErrorMessage = "" Then
                            MsgBox(objDataOperation.ErrorMessage)
                            Exit Sub
                        End If
                        For iCnt As Integer = 0 To dtImageScript.Rows.Count - 1
                            StrQ = CStr(dtConfigScript.Rows(iCnt).Item("Script"))
                            objDataOperation.ExecNonQuery(StrQ.Trim)
                            If Not objDataOperation.ErrorMessage = "" Then
                                MsgBox(objDataOperation.ErrorMessage)
                                Exit For
                            End If
                            Select Case mstrArgs.ToString.ToUpper
                                Case "/N", "/T"
                                    bgWorker.ReportProgress(iCnt)
                            End Select
                            System.Threading.Thread.Sleep(100)
                        Next
                        Select Case mstrArgs.ToString.ToUpper
                            Case "/N", "/T"
                                Case Else
                                iDBCnt += 1
                                    bgWorker.ReportProgress(iDBCnt)
                            End Select
                    End If
                End If
                '/**************** RUNNING IMAGES DATABASE SCRIPT ****************/ END
            End Using
        Catch ex As Exception
            If Org_DbVersion.Trim.Length > 0 Then
                Using objDataOp As New clsDataOperation

                    StrQ = "Use hrmsConfiguration"

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                        iRet_Value = -1
                    End If

                    StrQ = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[version]') AND type in (N'P', N'PC')) " & _
                           "DROP PROCEDURE [dbo].[version] "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                        iRet_Value = -1
                    End If

                    StrQ = "CREATE PROCEDURE [dbo].[version] AS SELECT '" & Org_DbVersion & "' AS version "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                        iRet_Value = -1
                    End If

                    System.Threading.Thread.Sleep(1000)

                End Using
            End If
            MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
            iRet_Value = -1
        End Try
    End Sub

    Private Sub bgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgWorker.ProgressChanged
        Try
            pbProgress.Value = e.ProgressPercentage
            Select Case mstrArgs.ToString.ToUpper
                Case "/N", "/T"
                Case Else
                    lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database : " & iDBCnt.ToString
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted
        Try
            If iRet_Value = -1 Then
                MsgBox("Fail to update database(s).", CType(MsgBoxStyle.OkOnly + MsgBoxStyle.Information, MsgBoxStyle))
            Else
                MsgBox("Database(s) Updated Successfully.", CType(MsgBoxStyle.OkOnly + MsgBoxStyle.Information, MsgBoxStyle))
            End If
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

#End Region

End Class