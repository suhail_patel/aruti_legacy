IF EXISTS(SELECT 1 FROM SYS.OBJECTS WHERE NAME='WEEKDAYOCCURANCE') DROP FUNCTION WEEKDAYOCCURANCE

CREATE FUNCTION weekdayoccurance
    (
      @date AS SMALLDATETIME
    )
RETURNS INT
AS 
    BEGIN
        DECLARE @startdate AS SMALLDATETIME ,
            @dayoccurance AS INT ,
            @i INT
        SELECT  @startdate = ( LTRIM(STR(DATEPART(yyyy, @date))) + RIGHT('0'
                                                              + LTRIM(STR(DATEPART(mm,
                                                              @date))), 2)
                               + '01' )
        SELECT  @i = 0
        SELECT  @dayoccurance = 0
        WHILE @i <= DATEDIFF(D, @startdate, @date) 
            BEGIN
                IF DATENAME(dw, DATEADD(D, @i, @startdate)) = DATENAME(dw,
                                                              @date) 
                    SELECT  @dayoccurance = @dayoccurance + 1
                SELECT  @i = @i + 1
            END
        IF DATEPART(dw, @date) < DATEPART(dw, @startdate) 
            SELECT  @dayoccurance = @dayoccurance + 1
        ELSE 
            SELECT  @dayoccurance = @dayoccurance + 0
        RETURN @dayoccurance
    END