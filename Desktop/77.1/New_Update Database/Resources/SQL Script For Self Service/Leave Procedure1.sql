IF EXISTS(select * from sys.procedures where name='CreateMonthCalendar') 
BEGIN
	DROP PROC CreateMonthCalendar
END

CREATE PROC CreateMonthCalendar
    (
      @tablename AS NVARCHAR(20) ,
      @fdt AS DATETIME ,
      @tdt AS DATETIME
    )
AS 
    BEGIN
        DECLARE @dt AS SMALLDATETIME ,
            @temp VARCHAR(100) ,
            @sql VARCHAR(8000) ,
            @new VARCHAR(8000) ,
            @msql VARCHAR(8000) ,
            @td AS SMALLDATETIME ,
            @day INT ,
            @totday INT ,
            @i AS INT ,
            @j INT ,
            @stwkno AS INT ,
            @sql1 VARCHAR(8000) ,
            @stwend INT ,
            @countday INT ,
            @startday VARCHAR(10) ,
            @CountMonth INT
        IF EXISTS ( SELECT  name
                    FROM    sys.tables
                    WHERE   name = @tablename ) 
            SELECT  @temp = 'drop table ' + @tablename + ''
        PRINT @temp
        EXEC (@temp)
        SELECT  @dt = @fdt
        SELECT  @countday = DATEDIFF(D, @fdt, @tdt)
        SELECT  @stwkno = DATENAME(wk, @fdt)
        SELECT  @stwend = DATENAME(wk, @tdt)
        PRINT @dt
        SELECT  @sql = ' create table ' + @tablename
                + '( CalendarTableID int identity(1,1),MonthYear varchar(30),startday varchar(10),MonthDays int,MonthId int,YearId int,'
        SELECT  @i = 1
        SELECT  @j = -1
        PRINT @totday
        WHILE @i <= 6 
            BEGIN
                SELECT  @j = -1
                WHILE @j < 6 
                    BEGIN
                        SELECT  @sql = @sql + '[' + LEFT(DATENAME(dw, @j), 3)
                                + '_' + CONVERT(VARCHAR(3), +@i, 103)
                                + '] nvarchar(50) default(' + CHAR(39) + ' '
                                + CHAR(39) + ') , '
                        SELECT  @j = @j + 1
                    END
                SELECT  @i = @i + 1
            END
        SELECT  @sql = LEFT(@sql, LEN(@sql) - 1) + ')'
        PRINT @sql
        EXEC (@sql)
        SELECT  @totday = DATEDIFF(mm, @fdt, @tdt)
        SELECT  @day = DATEPART(m, @fdt)
        PRINT @day
        PRINT @totday
        SET @CountMonth = 0
        WHILE @CountMonth <= @totday 
            BEGIN
                SELECT  @startday = LEFT(DATENAME(dw,
                                                  LTRIM(STR(DATEPART(yyyy, @dt)))
                                                  + RIGHT('0'
                                                          + LTRIM(STR(DATEPART(mm,
                                                              @dt))), 2)
                                                  + '01'), 3) + '_1'
                SELECT  @countday = DAY(DATEADD(DAY, -1,
                                                DATEADD(MONTH, 1,
                                                        DATEADD(DAY,
                                                              1 - DAY(@dt),
                                                              @dt))))
                SELECT  @new = 'insert into ' + @tablename
                        + '(MonthYear,startday,MonthDays,MonthId,YearId) values ('
                        + CHAR(39) + ( DATENAME(m, @dt) ) + '  -  '
                        + STR(YEAR(@dt), 4) + CHAR(39) + ',' + CHAR(39)
                        + @startday + CHAR(39) + ',' + STR(@countday) + ','
                        + STR(MONTH(@dt)) + ',' + STR(YEAR(@dt)) + ')'
                SELECT  @dt = DATEADD(m, 1, @dt)
                SELECT  @day = @day + 1
                SET @CountMonth = @CountMonth + 1
                PRINT @day
                EXEC (@new)
            END
    END