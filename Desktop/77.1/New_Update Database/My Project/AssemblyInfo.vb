﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Aruti SQL")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Aruti")> 
<Assembly: AssemblyProduct("Aruti SQL")> 
<Assembly: AssemblyCopyright("Copyright ©  2018")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("34b19349-70ae-49c5-b77e-ff64f58b2a33")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("9.0.77.1")> 
<Assembly: AssemblyFileVersion("9.0.77.1")> 
