﻿Imports System.Data.SqlClient

Public Class clsNewshift_master

    Private Shared ReadOnly mstrModuleName As String = "clsNewshift_master"
    Dim objShiftDay As New clsshift_tran
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintShiftunkid As Integer
    Private mintShifttypeunkid As Integer
    Private mstrShiftcode As String = String.Empty
    Private mstrShiftname As String = String.Empty
    Private mintTotalWeekHrs As Integer = 0
    Private mblnIsactive As Boolean = True
    Private mstrShiftname1 As String = String.Empty
    Private mstrShiftname2 As String = String.Empty
    Private mblnIsOpenShift As Boolean = False
    Private mintMaxHours As Integer = 0
    Private mblnAttOnDeviceInOutStatus As Boolean = False
    Private mblnIsNormalHrsForWeekend As Boolean = False
    Private mblnIsNormalHrsForHoliday As Boolean = False
    Private mblnIsNormalHrsForDayOff As Boolean = False
    Private mblnCountOnlyShiftTime As Boolean = False
    Private mblnCountOTonTotalWorkedHrs As Boolean = True

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Shiftunkid(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String) As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
            Call GetData(SqlCn, SqlTran, xDatabaseName)
        End Set
    End Property

    Public Property _Shifttypeunkid() As Integer
        Get
            Return mintShifttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintShifttypeunkid = value
        End Set
    End Property

    Public Property _Shiftcode() As String
        Get
            Return mstrShiftcode
        End Get
        Set(ByVal value As String)
            mstrShiftcode = value
        End Set
    End Property

    Public Property _Shiftname() As String
        Get
            Return mstrShiftname
        End Get
        Set(ByVal value As String)
            mstrShiftname = value
        End Set
    End Property

    Public Property _TotalWeekHrs() As Integer
        Get
            Return mintTotalWeekHrs
        End Get
        Set(ByVal value As Integer)
            mintTotalWeekHrs = value
        End Set
    End Property

    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _Shiftname1() As String
        Get
            Return mstrShiftname1
        End Get
        Set(ByVal value As String)
            mstrShiftname1 = value
        End Set
    End Property

    Public Property _Shiftname2() As String
        Get
            Return mstrShiftname2
        End Get
        Set(ByVal value As String)
            mstrShiftname2 = value
        End Set
    End Property

    Public Property _IsOpenShift() As Boolean
        Get
            Return mblnIsOpenShift
        End Get
        Set(ByVal value As Boolean)
            mblnIsOpenShift = value
        End Set
    End Property

    Public Property _MaxHrs() As Integer
        Get
            Return mintMaxHours
        End Get
        Set(ByVal value As Integer)
            mintMaxHours = value
        End Set
    End Property

    Public Property _AttOnDeviceInOutStatus() As Boolean
        Get
            Return mblnAttOnDeviceInOutStatus
        End Get
        Set(ByVal value As Boolean)
            mblnAttOnDeviceInOutStatus = value
        End Set
    End Property

    Public Property _IsNormalHrsForWeekend() As Boolean
        Get
            Return mblnIsNormalHrsForWeekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsNormalHrsForWeekend = value
        End Set
    End Property

    Public Property _IsNormalHrsForHoliday() As Boolean
        Get
            Return mblnIsNormalHrsForHoliday
        End Get
        Set(ByVal value As Boolean)
            mblnIsNormalHrsForHoliday = value
        End Set
    End Property

    Public Property _IsNormalHrsForDayOff() As Boolean
        Get
            Return mblnIsNormalHrsForDayOff
        End Get
        Set(ByVal value As Boolean)
            mblnIsNormalHrsForDayOff = value
        End Set
    End Property

    Public Property _CountOnlyShiftTime() As Boolean
        Get
            Return mblnCountOnlyShiftTime
        End Get
        Set(ByVal value As Boolean)
            mblnCountOnlyShiftTime = value
        End Set
    End Property

    Public Property _CountOTonTotalWorkedHrs() As Boolean
        Get
            Return mblnCountOTonTotalWorkedHrs
        End Get
        Set(ByVal value As Boolean)
            mblnCountOTonTotalWorkedHrs = value
        End Set
    End Property

#End Region

    Public Sub GetData(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, Optional ByVal intUnkId As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim sqlCmd As SqlCommand = Nothing

        If intUnkId > 0 Then mintShiftunkid = intUnkId

        Try
            strQ = "SELECT " & _
                      "  shiftunkid " & _
                      ", shifttypeunkid " & _
                      ", shiftcode " & _
                      ", shiftname " & _
                      ", weekhours " & _
                      ", isactive " & _
                      ", shiftname1 " & _
                      ", shiftname2 " & _
                      ", ISNULL(isopenshift,0) AS isopenshift " & _
                      ", ISNULL(maxshifthrs,0) AS maxshifthrs " & _
                      ", ISNULL(attondeviceiostatus,0) AS attondeviceiostatus " & _
                      ", ISNULL(isnormalhrs_forwkend,0) AS isnormalhrs_forwkend " & _
                      ", ISNULL(isnormalhrs_forph,0) AS isnormalhrs_forph " & _
                      ", ISNULL(isnormalhrs_fordayoff,0) AS isnormalhrs_fordayoff " & _
                      ", ISNULL(iscountshifttimeonly,0) AS iscountshifttimeonly " & _
                      ", ISNULL(iscnt_ot_ontotalworkhrs,0) AS iscnt_ot_ontotalworkhrs " & _
                      " FROM " & xDatabaseName & "..tnashift_master " & _
                      " WHERE shiftunkid = @shiftunkid "


            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@shiftunkid", mintShiftunkid.ToString)
            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintShiftunkid = CInt(dtRow.Item("shiftunkid"))
                mintShifttypeunkid = CInt(dtRow.Item("shifttypeunkid"))
                mstrShiftcode = dtRow.Item("shiftcode").ToString
                mstrShiftname = dtRow.Item("shiftname").ToString
                If Not IsDBNull(dtRow.Item("weekhours")) Then
                    mintTotalWeekHrs = CInt(dtRow.Item("weekhours").ToString())
                End If
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrShiftname1 = dtRow.Item("shiftname1").ToString
                mstrShiftname2 = dtRow.Item("shiftname2").ToString
                mblnIsOpenShift = CBool(dtRow.Item("isopenshift"))
                If Not IsDBNull(dtRow.Item("maxshifthrs")) Then
                    mintMaxHours = CInt(dtRow.Item("maxshifthrs"))
                End If
                mblnAttOnDeviceInOutStatus = CBool(dtRow.Item("attondeviceiostatus"))
                mblnIsNormalHrsForWeekend = CBool(dtRow.Item("isnormalhrs_forwkend"))
                mblnIsNormalHrsForHoliday = CBool(dtRow.Item("isnormalhrs_forph"))
                mblnIsNormalHrsForDayOff = CBool(dtRow.Item("isnormalhrs_fordayoff"))
                mblnCountOnlyShiftTime = CBool(dtRow.Item("iscountshifttimeonly"))
                mblnCountOTonTotalWorkedHrs = CBool(dtRow.Item("iscnt_ot_ontotalworkhrs"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

End Class