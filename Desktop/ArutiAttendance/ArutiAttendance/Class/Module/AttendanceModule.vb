﻿Imports System.Globalization

Public Module AttendanceModule

    Private ReadOnly mstrModuleName As String = "AttendanceModule"

#Region "Enum"

    Public Enum enFingerPrintDevice
        None = 0
        DigitalPersona = 1
        SecuGen = 2
        ZKSoftware = 3
        BioStar = 4
        ZKAcessControl = 5
        Anviz = 6
        ACTAtek = 7
        Handpunch = 8
        SAGEM = 9
        BioStar2 = 10
        FingerTec = 11
        NewAnviz = 12
    End Enum

    Public Enum enInOutSource
        LoginPassword = 1
        FingerPrint = 2
        Swipecard = 3
        Import = 4
        Manual = 5
        GroupLogin = 6
        GlobalAdd = 7
        GlobalEdit = 8
        GlobalDelete = 9
    End Enum

    Public Enum enAT_VIEW_TYPE
        ASSESSMENT_MGT = 1
        LEAVE_MGT = 2
        EMPLOYEE_MGT = 3
        PAYROLL_MGT = 4
        RECRUITMENT_MGT = 5
        CLAIMREQUEST_MGT = 6
        DISCIPLINE_MGT = 7
        LOAN_MGT = 8
        GRIEVANCE_MGT = 9
        TRAININGREQUISITION_MGT = 10
        TNA_MGT = 11
    End Enum

    Public Enum enLogin_Mode
        DESKTOP = 1
        EMP_SELF_SERVICE = 2
        MGR_SELF_SERVICE = 3
    End Enum

#End Region

    Public Function convertDate(ByVal [Date] As System.DateTime) As String
        Return String.Format("{0:0000}{1:00}{2:00}", [Date].Year, [Date].Month, [Date].Day)
    End Function

    Private Function convertDate(ByVal DateString As String) As System.DateTime
        Try
            Return New DateTime(Integer.Parse(DateString.Substring(0, 4)), Integer.Parse(DateString.Substring(4, 2)), Integer.Parse(DateString.Substring(6, 2)))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: convertDate; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function GetWeekDayNumber(ByVal weekName As String) As Integer
        Dim weekNames As New Dictionary(Of String, Integer)
        Try
            Dim dayName As String() = CultureInfo.CurrentCulture.DateTimeFormat.DayNames()
            For i As Integer = 0 To 6
                weekNames.Add(dayName(i), i)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetWeekDayNumber; Module Name: " & mstrModuleName)
        End Try
        Return weekNames(weekName)
    End Function

    Public Function getIP() As String
        Dim IPHEntry As System.Net.IPHostEntry
        Dim IPAdd() As System.Net.IPAddress
        Dim localHost As String
        Dim exForce As Exception

        Try
            localHost = System.Net.Dns.GetHostName()
            IPHEntry = System.Net.Dns.GetHostEntry(localHost)
            IPAdd = IPHEntry.AddressList

            Dim IP4 = New List(Of Net.IPAddress)(Net.Dns.GetHostEntry(localHost).AddressList).Find(Function(f) f.AddressFamily = Net.Sockets.AddressFamily.InterNetwork)
            Return IP4.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getIP; Module Name: " & mstrModuleName)
            Return ""
        Finally
            exForce = Nothing
        End Try

    End Function

    Public Function getHostName() As String
        Dim localHost As String
        Try
            localHost = System.Net.Dns.GetHostName()
            Return localHost
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getHostName; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function

End Module
