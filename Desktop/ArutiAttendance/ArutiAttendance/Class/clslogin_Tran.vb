﻿Imports System.Globalization
Imports System.Data.SqlClient

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clslogin_Tran
    Private Shared ReadOnly mstrModuleName As String = "clslogin_Tran"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoginunkid As Integer
    Private mintLoginsummaryunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintShiftunkid As Integer
    Private mdtLogindate As Date
    Private mdtFromdate As Date
    Private mdtTodate As Date
    Private mdtcheckintime As Date
    Private mdtCheckouttime As Date
    Private mintHoldunkid As Integer
    Private mblnIsPost As Boolean = False
    Private mintWorkhour As Integer
    Private mdblDayType As Double
    Private mintBreakhr As Integer
    Private mintTotalhr As Integer
    Private mintTotalOvertimehr As Integer
    Private mintShorthr As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIspaidLeave As Boolean
    Private mblnIsUnpaidLeave As Boolean
    Private mblnIsAbsentProcess As Boolean
    Private mblnIsactive As Boolean = True
    Private mintInOuType As Integer = -1
    Private mintSourceType As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As DateTime
    Private mstrVoidReason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mintTotalNighthrs As Integer
    Private mblnIsWeekend As Boolean = False
    Private mblnIsholiday As Boolean = False
    Private mintPolicyId As Integer = 0
    Private mintOT2 As Integer = 0
    Private mintOT3 As Integer = 0
    Private mintOT4 As Integer = 0
    Private mintearly_coming As Integer = 0
    Private mintlate_coming As Integer = 0
    Private mintearly_going As Integer = 0
    Private mintlate_going As Integer = 0
    Private mintOperationType As Integer = 0
    Private mdtRoundoffInTime As DateTime = Nothing
    Private mdtRoundoffOutTime As DateTime = Nothing
    Private mblnIsDayOffshift As Boolean = False
    Private mdtOriginal_Intime As DateTime
    Private mdtOriginal_Outtime As DateTime
    Dim mdctFinalOTOrder As Dictionary(Of String, Integer)
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mblnIsgraceroundoff As Boolean = False
    Private mintManualroundofftypeid As Integer = 0
    Private mintRoundminutes As Integer = 0
    Private mintRoundlimit As Integer = 0
    Private mintTeaHr As Integer = 0
    Private mdecDayFraction As Decimal = 0
    'Pinkal (30-Jul-2015) -- Start
    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
    Private mintCompanyId As Integer = 0
    'Pinkal (30-Jul-2015) -- End

    Private mintLateComing_WOGrace As Integer = 0
    Private mintLateComing_AfterGrace As Integer = 0


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.
    Private mstrDeviceAtt_DeviceNo As String = ""
    Private mstrDeviceAtt_IPAddress As String = ""
    Private mstrDeviceAtt_EnrollNo As String = ""
    Private mdtDeviceAtt_LoginDate As Date = Nothing
    Private mdtDeviceAtt_LoginTime As DateTime = Nothing
    Private mstrDeviceAtt_VerifyMode As String = ""
    Private mstrDeviceAtt_InOutMode As String = ""
    Private mstrDeviceAtt_IsError As Boolean = False
    'Pinkal (27-Mar-2017) -- End


    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .
    Private mintDayID As Integer = -1
    'Pinkal (18-Jul-2017) -- End


    'Pinkal (25-AUG-2017) -- Start
    'Enhancement - Working on B5 plus TnA Enhancement.
    Private mintLeaveTypeId As Integer = 0
    'Pinkal (25-AUG-2017) -- End


#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Loginunkid(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String) As Integer
        Get
            Return mintLoginunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginunkid = value
            Call GetData(SqlCn, SqlTran, xDatabaseName)
        End Set
    End Property

    Public Property _LoginSummaryunkid() As Integer
        Get
            Return mintLoginsummaryunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginsummaryunkid = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Shiftunkid() As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public Property _Logindate() As Date
        Get
            Return mdtLogindate
        End Get
        Set(ByVal value As Date)
            mdtLogindate = value
        End Set
    End Property

    Public Property _Fromdate() As Date
        Get
            Return mdtFromdate
        End Get
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    Public Property _Todate() As Date
        Get
            Return mdtTodate
        End Get
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

    Public Property _checkintime() As Date
        Get
            Return mdtcheckintime
        End Get
        Set(ByVal value As Date)
            mdtcheckintime = value
        End Set
    End Property

    Public Property _Checkouttime() As Date
        Get
            Return mdtCheckouttime
        End Get
        Set(ByVal value As Date)
            mdtCheckouttime = value
        End Set
    End Property

    Public Property _Holdunkid() As Integer
        Get
            Return mintHoldunkid
        End Get
        Set(ByVal value As Integer)
            mintHoldunkid = value
        End Set
    End Property

    Public Property _IsPost() As Boolean
        Get
            Return mblnIsPost
        End Get
        Set(ByVal value As Boolean)
            mblnIsPost = value
        End Set
    End Property

    Public Property _DayType() As Integer
        Get
            Return mdblDayType
        End Get
        Set(ByVal value As Integer)
            mdblDayType = value
        End Set
    End Property

    Public Property _Workhour() As Integer
        Get
            Return mintWorkhour
        End Get
        Set(ByVal value As Integer)
            mintWorkhour = value
        End Set
    End Property

    Public Property _Breakhr() As Integer
        Get
            Return mintBreakhr
        End Get
        Set(ByVal value As Integer)
            mintBreakhr = value
        End Set
    End Property

    Public Property _Totalhr() As Integer
        Get
            Return mintTotalhr
        End Get
        Set(ByVal value As Integer)
            mintTotalhr = value
        End Set
    End Property

    Public Property _Shorthr() As Integer
        Get
            Return mintShorthr
        End Get
        Set(ByVal value As Integer)
            mintShorthr = value
        End Set
    End Property

    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    Public Property _IsPaidLeave() As Boolean
        Get
            Return mblnIspaidLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIspaidLeave = value
        End Set
    End Property

    Public Property _IsUnPaidLeave() As Boolean
        Get
            Return mblnIsUnpaidLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsUnpaidLeave = value
        End Set
    End Property

    Public Property _IsAbsentProcess() As Boolean
        Get
            Return mblnIsAbsentProcess
        End Get
        Set(ByVal value As Boolean)
            mblnIsAbsentProcess = value
        End Set
    End Property

    Public Property _InOutType() As Integer
        Get
            Return mintInOuType
        End Get
        Set(ByVal value As Integer)
            mintInOuType = value
        End Set
    End Property

    Public Property _SourceType() As Integer
        Get
            Return mintSourceType
        End Get
        Set(ByVal value As Integer)
            mintSourceType = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public Property _TotalNighthr() As Integer
        Get
            Return mintTotalNighthrs
        End Get
        Set(ByVal value As Integer)
            mintTotalNighthrs = value
        End Set
    End Property

    Public Property _IsWeekend() As Boolean
        Get
            Return mblnIsWeekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeekend = value
        End Set
    End Property

    Public Property _IsHoliday() As Boolean
        Get
            Return mblnIsholiday
        End Get
        Set(ByVal value As Boolean)
            mblnIsholiday = value
        End Set
    End Property

    Public Property _PolicyId() As Integer
        Get
            Return mintPolicyId
        End Get
        Set(ByVal value As Integer)
            mintPolicyId = value
        End Set
    End Property

    Public Property _Ot2() As Integer
        Get
            Return mintOT2
        End Get
        Set(ByVal value As Integer)
            mintOT2 = value
        End Set
    End Property

    Public Property _Ot3() As Integer
        Get
            Return mintOT3
        End Get
        Set(ByVal value As Integer)
            mintOT3 = value
        End Set
    End Property

    Public Property _Ot4() As Integer
        Get
            Return mintOT4
        End Get
        Set(ByVal value As Integer)
            mintOT4 = value
        End Set
    End Property

    Public Property _Early_Coming() As Integer
        Get
            Return mintearly_coming
        End Get
        Set(ByVal value As Integer)
            mintearly_coming = value
        End Set
    End Property

    Public Property _Late_Coming() As Integer
        Get
            Return mintlate_coming
        End Get
        Set(ByVal value As Integer)
            mintlate_coming = value
        End Set
    End Property

    Public Property _Early_Going() As Integer
        Get
            Return mintearly_going
        End Get
        Set(ByVal value As Integer)
            mintearly_going = value
        End Set
    End Property

    Public Property _Late_Going() As Integer
        Get
            Return mintlate_going
        End Get
        Set(ByVal value As Integer)
            mintlate_going = value
        End Set
    End Property

    Public Property _OperationType() As Integer
        Get
            Return mintOperationType
        End Get
        Set(ByVal value As Integer)
            mintOperationType = value
        End Set
    End Property

    Public Property _Roundoff_Intime() As Date
        Get
            Return mdtRoundoffInTime
        End Get
        Set(ByVal value As Date)
            mdtRoundoffInTime = value
        End Set
    End Property

    Public Property _Roundoff_Outtime() As Date
        Get
            Return mdtRoundoffOutTime
        End Get
        Set(ByVal value As Date)
            mdtRoundoffOutTime = value
        End Set
    End Property

    Public Property _IsDayOffShift() As Boolean
        Get
            Return mblnIsDayOffshift
        End Get
        Set(ByVal value As Boolean)
            mblnIsDayOffshift = value
        End Set
    End Property

    Public Property _Original_InTime() As DateTime
        Get
            Return mdtOriginal_Intime
        End Get
        Set(ByVal value As DateTime)
            mdtOriginal_Intime = value
        End Set
    End Property

    Public Property _Original_OutTime() As DateTime
        Get
            Return mdtOriginal_Outtime
        End Get
        Set(ByVal value As DateTime)
            mdtOriginal_Outtime = value
        End Set
    End Property

    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    Public Property _Isgraceroundoff() As Boolean
        Get
            Return mblnIsgraceroundoff
        End Get
        Set(ByVal value As Boolean)
            mblnIsgraceroundoff = value
        End Set
    End Property

    Public Property _Manualroundofftypeid() As Integer
        Get
            Return mintManualroundofftypeid
        End Get
        Set(ByVal value As Integer)
            mintManualroundofftypeid = value
        End Set
    End Property

    Public Property _Roundminutes() As Integer
        Get
            Return mintRoundminutes
        End Get
        Set(ByVal value As Integer)
            mintRoundminutes = value
        End Set
    End Property

    Public Property _Roundlimit() As Integer
        Get
            Return mintRoundlimit
        End Get
        Set(ByVal value As Integer)
            mintRoundlimit = value
        End Set
    End Property

    Public Property _TeaHr() As Integer
        Get
            Return mintTeaHr
        End Get
        Set(ByVal value As Integer)
            mintTeaHr = value
        End Set
    End Property

    Public Property _LeaveDayFraction() As Decimal
        Get
            Return mdecDayFraction
        End Get
        Set(ByVal value As Decimal)
            mdecDayFraction = value
        End Set
    End Property

    Public Property _CompanyId() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    Public Property _LateComingWOGrace() As Integer
        Get
            Return mintLateComing_WOGrace
        End Get
        Set(ByVal value As Integer)
            mintLateComing_WOGrace = value
        End Set
    End Property

    Public Property _LateComing_AfterGrace() As Integer
        Get
            Return mintLateComing_AfterGrace
        End Get
        Set(ByVal value As Integer)
            mintLateComing_AfterGrace = value
        End Set
    End Property

    Public Property _DeviceAtt_DeviceNo() As String
        Get
            Return mstrDeviceAtt_DeviceNo
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_DeviceNo = value
        End Set
    End Property

    Public Property _DeviceAtt_IPAddress() As String
        Get
            Return mstrDeviceAtt_IPAddress
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_IPAddress = value
        End Set
    End Property

    Public Property _DeviceAtt_EnrollNo() As String
        Get
            Return mstrDeviceAtt_EnrollNo
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_EnrollNo = value
        End Set
    End Property

    Public Property _DeviceAtt_LoginDate() As Date
        Get
            Return mdtDeviceAtt_LoginDate
        End Get
        Set(ByVal value As Date)
            mdtDeviceAtt_LoginDate = value
        End Set
    End Property

    Public Property _DeviceAtt_LoginTime() As DateTime
        Get
            Return mdtDeviceAtt_LoginTime
        End Get
        Set(ByVal value As DateTime)
            mdtDeviceAtt_LoginTime = value
        End Set
    End Property

    Public Property _DeviceAtt_VerifyMode() As String
        Get
            Return mstrDeviceAtt_VerifyMode
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_VerifyMode = value
        End Set
    End Property

    Public Property _DeviceAtt_InOutMode() As String
        Get
            Return mstrDeviceAtt_InOutMode
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_InOutMode = value
        End Set
    End Property

    Public Property _DeviceAtt_IsError() As Boolean
        Get
            Return mstrDeviceAtt_IsError
        End Get
        Set(ByVal value As Boolean)
            mstrDeviceAtt_IsError = value
        End Set
    End Property

    Public Property _DayId() As Integer
        Get
            Return mintDayID
        End Get
        Set(ByVal value As Integer)
            mintDayID = value
        End Set
    End Property

    Public Property _LeaveTypeId() As Integer
        Get
            Return mintLeaveTypeId
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

#End Region

#Region "For LOGIN TRAN Table"

    Public Sub GetData(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim SqlCmd As SqlCommand = Nothing

        Try
            strQ = "SELECT " & _
                      "  loginunkid " & _
                      ", employeeunkid " & _
                      ", logindate " & _
                      ", checkintime " & _
                      ", checkouttime " & _
                      ", holdunkid " & _
                      ", workhour " & _
                      ", breakhr " & _
                      ", remark " & _
                      ", inouttype " & _
                      ", sourcetype " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ",roundoff_intime " & _
                      ",roundoff_outtime " & _
                      ",operationtype " & _
                      ",original_intime " & _
                      ",original_outtime " & _
                      ", ISNULL(teahr,0) AS teahr " & _
                     " FROM " & xDatabaseName & "..tnalogin_tran " & _
                     " WHERE loginunkid = @loginunkid "



            If SqlTran Is Nothing Then
                SqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@loginunkid", mintLoginunkid)

            Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoginunkid = CInt(dtRow.Item("loginunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtLogindate = CDate(dtRow.Item("logindate"))

                If Not dtRow.Item("checkintime") Is DBNull.Value Then
                    mdtcheckintime = CDate(dtRow.Item("checkintime"))
                Else
                    mdtcheckintime = Nothing
                End If

                If Not dtRow.Item("checkouttime") Is DBNull.Value Then
                    mdtCheckouttime = CDate(dtRow.Item("checkouttime"))
                Else
                    mdtCheckouttime = Nothing
                End If

                mintHoldunkid = CInt(dtRow.Item("holdunkid"))
                mintWorkhour = CInt(dtRow.Item("workhour"))
                mintBreakhr = CInt(dtRow.Item("breakhr"))
                If Not dtRow.Item("userunkid") Is DBNull.Value Then
                    mintUserunkid = CInt(dtRow.Item("userunkid"))
                End If

                If Not dtRow.Item("inouttype") Is DBNull.Value Then
                    mintInOuType = CInt(dtRow.Item("inouttype"))
                End If

                If Not dtRow.Item("sourcetype") Is DBNull.Value Then
                    mintSourceType = CInt(dtRow.Item("sourcetype"))
                End If

                mstrRemark = dtRow.Item("remark").ToString()
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If Not dtRow.Item("voiduserunkid") Is DBNull.Value Then
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                End If
                If Not dtRow.Item("roundoff_intime") Is DBNull.Value Then
                    mdtRoundoffInTime = CDate(dtRow.Item("roundoff_intime"))
                End If
                If Not dtRow.Item("voidreason") Is DBNull.Value Then
                    mstrVoidReason = CStr(dtRow.Item("voidreason"))
                End If

                If Not dtRow.Item("voiddatetime") Is DBNull.Value Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                If Not dtRow.Item("roundoff_outtime") Is DBNull.Value Then
                    mdtRoundoffOutTime = CDate(dtRow.Item("roundoff_outtime"))
                End If
                If Not dtRow.Item("operationtype") Is DBNull.Value Then
                    mintOperationType = CInt(dtRow.Item("operationtype"))
                End If

                If Not dtRow.Item("original_intime") Is DBNull.Value Then
                    mdtOriginal_Intime = CDate(dtRow.Item("original_intime"))
                Else
                    mdtOriginal_Intime = Nothing
                End If

                If Not dtRow.Item("original_outtime") Is DBNull.Value Then
                    mdtOriginal_Outtime = CDate(dtRow.Item("original_outtime"))
                Else
                    mdtOriginal_Outtime = Nothing
                End If


                If Not dtRow.Item("teahr") Is DBNull.Value Then
                    mintTeaHr = CInt(dtRow.Item("teahr"))
                Else
                    mintTeaHr = 0
                End If

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    Public Function GetList(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim SqlCmd As SqlCommand = Nothing

        Try
            strQ = " SELECT " & _
                      "  loginunkid " & _
                      ", employeeunkid " & _
                      ", logindate " & _
                      ", checkintime " & _
                      ", checkouttime " & _
                      ", holdunkid " & _
                      ", workhour " & _
                      ", breakhr " & _
                      ", remark " & _
                      ", inouttype " & _
                      ", sourcetype " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", roundoff_intime " & _
                      ", roundoff_outtime " & _
                      ", original_intime " & _
                      ", original_outtime " & _
                      ", ISNULL(teahr,0) AS teahr " & _
                      " FROM " & xDatabaseName & "..tnalogin_tran " & _
                      " WHERE employeeunkid=@employeeunkid AND logindate = @logindate "

            If blnOnlyActive Then
                strQ &= " AND isnull(isvoid,0) = 0 "
            End If

            strQ &= " order by checkintime"

            SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeunkid)
            SqlCmd.Parameters.AddWithValue("@logindate", mdtLogindate)

            dsList = New DataSet
            Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                sqlDataadp.Fill(dsList)
            End Using


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function Insert(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                     , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                     , ByVal blnPolicyManagementTNA As Boolean _
                                     , ByVal blnDonotAttendanceinSeconds As Boolean _
                                     , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                    , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                    , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                    , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intEmpId As Integer = -1 _
                                    , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                    , Optional ByVal mstrFilter As String = "") As Boolean


        If mdtcheckintime <> Nothing Then
            If IsExistTime(SqlCn, xDatabaseName, mdtcheckintime) Then
                mstrMessage = "Please check Time information, this information already exists for this employee."
                Return False
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim SqlTran As SqlTransaction = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try

            SqlTran = SqlCn.BeginTransaction    'START TRANSACTION

            strQ = " INSERT INTO " & xDatabaseName & "..tnalogin_tran ( " & _
                      "  employeeunkid " & _
                      ", logindate " & _
                      ", checkintime " & _
                      ", checkouttime " & _
                      ", holdunkid " & _
                      ", workhour " & _
                      ", breakhr " & _
                      ", remark " & _
                      ", inouttype " & _
                      ", sourcetype " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ",original_intime " & _
                      ",original_outtime " & _
                      ",loginemployeeunkid " & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @logindate " & _
                      ", @checkintime " & _
                      ", @checkouttime " & _
                      ", @holdunkid " & _
                      ", @workhour " & _
                      ", @breakhr " & _
                      ", @remark " & _
                      ", @inouttype " & _
                      ", @sourcetype " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @original_intime " & _
                      ", @original_outtime " & _
                      ", @loginemployeeunkid " & _
                    "); SELECT @@identity"


            SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@logindate", mdtLogindate)

            If blnDonotAttendanceinSeconds Then
                SqlCmd.Parameters.AddWithValue("@checkintime", IIf(mdtcheckintime <> Nothing, CDate(mdtcheckintime.ToShortDateString & " " & mdtcheckintime.ToString("HH:mm")), DBNull.Value))
                SqlCmd.Parameters.AddWithValue("@checkouttime", IIf(mdtCheckouttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))
            Else
                SqlCmd.Parameters.AddWithValue("@checkintime", IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                SqlCmd.Parameters.AddWithValue("@checkouttime", IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            SqlCmd.Parameters.AddWithValue("@original_intime", IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
            SqlCmd.Parameters.AddWithValue("@original_outtime", IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))
            SqlCmd.Parameters.AddWithValue("@holdunkid", mintHoldunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@workhour", mintWorkhour.ToString)
            SqlCmd.Parameters.AddWithValue("@breakhr", mintBreakhr.ToString)
            SqlCmd.Parameters.AddWithValue("@remark", mstrRemark.ToString)
            SqlCmd.Parameters.AddWithValue("@inouttype", mintInOuType.ToString)
            SqlCmd.Parameters.AddWithValue("@sourcetype", mintSourceType.ToString)
            SqlCmd.Parameters.AddWithValue("@userunkid", mintUserunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@isvoid", mblnIsvoid.ToString)
            SqlCmd.Parameters.AddWithValue("@voiduserunkid", mintVoiduserunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@voiddatetime", IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            SqlCmd.Parameters.AddWithValue("@voidreason", mstrVoidReason.ToString)
            SqlCmd.Parameters.AddWithValue("@loginemployeeunkid", mintLogEmployeeUnkid)

            mintLoginunkid = CInt(SqlCmd.ExecuteScalar())

            InsertLoginSummary(SqlCn, SqlTran, xDatabaseName _
                                         , xUserUnkid, xYearUnkid _
                                         , xCompanyUnkid, mdtStartDate _
                                         , mdtEndDate, xUserModeSetting _
                                         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                         , blnPolicyManagementTNA _
                                         , blnDonotAttendanceinSeconds _
                                         , blnFirstCheckInLastCheckOut _
                                         , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                         , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                         , strUserAccessLevelFilterString, mstrFilter)


            'END FOR INSERT LOGIN SUMMERY


            If blnFirstCheckInLastCheckOut = False Then
                UpdateBreakTime(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate)
            End If

            'AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE

            InsertAuditTrailLogin(SqlCn, SqlTran, xDatabaseName, 1, blnDonotAttendanceinSeconds)

            SqlTran.Commit()   'COMPLETED TRANSACTION
            Return True
        Catch ex As Exception
            SqlTran.Rollback()   'ROLLBACK TRANSACTION
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Update(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String _
                                  , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                  , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnPolicyManagementTNA As Boolean _
                                  , ByVal blnDonotAttendanceinSeconds As Boolean _
                                  , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                  , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                  , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                  , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                  , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intEmpId As Integer = -1 _
                                  , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                  , Optional ByVal mstrFilter As String = "") As Boolean



        If IsExistTime(SqlCn, xDatabaseName, mdtCheckouttime, mintLoginunkid) Then
            mstrMessage = "Please check Time information, this information already exists for this employee."
            Return False
        ElseIf IsExistTime(SqlCn, xDatabaseName, mdtCheckouttime, mintLoginunkid) Then
            mstrMessage = "Please check Time information, this information already exists for this employee."
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim SqlTran As SqlTransaction = Nothing
        Dim SqlCmd As SqlCommand = Nothing

        Try

            SqlTran = SqlCn.BeginTransaction    'START TRANSACTION

            strQ = "UPDATE " & xDatabaseName & "..tnalogin_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", logindate = @logindate" & _
                      ", checkintime = @checkintime" & _
                      ", checkouttime = @checkouttime" & _
                      ", holdunkid = @holdunkid" & _
                      ", workhour = @workhour" & _
                      ", breakhr = @breakhr" & _
                      ", remark = @remark" & _
                      ", inouttype = @inouttype " & _
                      ", sourcetype = @sourcetype " & _
                      ", userunkid = @userunkid " & _
                      ", isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
                      ", loginemployeeunkid = @loginemployeeunkid " & _
                      ", original_intime = @original_intime " & _
                      ", original_outtime = @original_outtime " & _
                      " WHERE loginunkid = @loginunkid "


            SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            SqlCmd.Parameters.Clear()

            SqlCmd.Parameters.AddWithValue("@loginunkid", mintLoginunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@logindate", mdtLogindate)

            If blnDonotAttendanceinSeconds Then
                SqlCmd.Parameters.AddWithValue("@checkintime", IIf(mdtcheckintime <> Nothing, CDate(mdtcheckintime.ToShortDateString & " " & mdtcheckintime.ToString("HH:mm")), DBNull.Value))
                SqlCmd.Parameters.AddWithValue("@checkouttime", IIf(mdtCheckouttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))
            Else
                SqlCmd.Parameters.AddWithValue("@checkintime", IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                SqlCmd.Parameters.AddWithValue("@checkouttime", IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            SqlCmd.Parameters.AddWithValue("@holdunkid", mintHoldunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@workhour", mintWorkhour.ToString)
            SqlCmd.Parameters.AddWithValue("@breakhr", mintBreakhr.ToString)
            SqlCmd.Parameters.AddWithValue("@remark", mstrRemark)
            SqlCmd.Parameters.AddWithValue("@inouttype", mintInOuType.ToString)
            SqlCmd.Parameters.AddWithValue("@sourcetype", mintSourceType.ToString)
            SqlCmd.Parameters.AddWithValue("@userunkid", mintUserunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@isvoid", mblnIsvoid.ToString)
            SqlCmd.Parameters.AddWithValue("@voiduserunkid", mintVoiduserunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@voiddatetime", IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            SqlCmd.Parameters.AddWithValue("@voidreason", mstrVoidReason.ToString)
            SqlCmd.Parameters.AddWithValue("@loginemployeeunkid", mintLogEmployeeUnkid)
            SqlCmd.Parameters.AddWithValue("@original_intime", IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
            SqlCmd.Parameters.AddWithValue("@original_outtime", IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))
            SqlCmd.ExecuteNonQuery()


            If mdtcheckintime <> Nothing Then
                If blnFirstCheckInLastCheckOut = False Then
                    UpdateBreakTime(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtcheckintime)
                End If
            End If

            InsertLoginSummary(SqlCn, SqlTran, xDatabaseName _
                                         , xUserUnkid, xYearUnkid _
                                         , xCompanyUnkid, mdtStartDate _
                                         , mdtEndDate, xUserModeSetting _
                                         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                         , blnPolicyManagementTNA _
                                         , blnDonotAttendanceinSeconds _
                                         , blnFirstCheckInLastCheckOut _
                                          , blnIsHolidayConsiderOnWeekend _
                                          , blnIsDayOffConsiderOnWeekend _
                                          , blnIsHolidayConsiderOnDayoff _
                                         , blnApplyUserAccessFilter, intEmpId _
                                         , strUserAccessLevelFilterString, mstrFilter)


            'END FOR INSERT LOGIN SUMMERY

            'START AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE
            InsertAuditTrailLogin(SqlCn, SqlTran, xDatabaseName, 2, blnDonotAttendanceinSeconds)
            'END AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE

            SqlTran.Commit()
            Return True
        Catch ex As Exception
            SqlTran.Rollback()
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsEmployeeLoginExist(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String _
                                                         , ByVal mdtDate As String, ByVal mstrEmployeeCode As String, Optional ByVal intEmployeeId As Integer = -1 _
                                                         , Optional ByRef intLoginunkid As Integer = -1) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim iCnt As Integer = 0
        Dim dsList As DataSet = Nothing
        Dim sqlCmd As SqlCommand = Nothing
        Try
          

            strQ = "SELECT tnalogin_tran.loginunkid " & _
                      ", hremployee_master.employeeunkid " & _
                      " FROM " & xDatabaseName & "..tnalogin_tran " & _
                      " LEFT JOIN " & xDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
                      " WHERE CONVERT(CHAR(8), logindate, 112) = @dtdate AND tnalogin_tran.isvoid = 0 "

            If intEmployeeId > 0 Then
                strQ &= " AND tnalogin_tran.employeeunkid = @employeeunkid "
            End If

            If mstrEmployeeCode.Trim.Length > 0 Then
                strQ &= " AND hremployee_master.employeecode = @employeecode "
            End If

            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            sqlCmd.Parameters.Clear()

            If intEmployeeId > 0 Then
                sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmployeeId)
            End If

            If mstrEmployeeCode.Trim.Length > 0 Then
                sqlCmd.Parameters.AddWithValue("@employeecode", mstrEmployeeCode)
            End If

            sqlCmd.Parameters.AddWithValue("@dtdate", mdtDate)

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                intLoginunkid = CInt(dsList.Tables(0).Rows(0)("loginunkid"))
                iCnt = dsList.Tables(0).Rows.Count
            End If

            Return (iCnt > 0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeExist; Module Name: " & mstrModuleName)
            Return True
        Finally
            exForce = Nothing
        End Try
    End Function

    Public Function GetEmployeeLoginDate(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String, ByVal strList As String, ByVal mintEmployeeID As Integer, ByVal mdtLoginLogoutTime As Date) As DataSet
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try


            strQ = "SELECT  tnalogin_tran.loginunkid " & _
                          ", tnalogin_tran.employeeunkid " & _
                          ", tnalogin_tran.logindate " & _
                          ", tnalogin_tran.checkintime " & _
                          ", tnalogin_tran.checkouttime " & _
                          ", tnalogin_tran.holdunkid " & _
                          ", tnalogin_tran.workhour " & _
                          ", tnalogin_tran.breakhr " & _
                          ", tnalogin_tran.remark " & _
                          ", tnalogin_tran.isvoid " & _
                          ", tnalogin_tran.voiduserunkid " & _
                          ", tnalogin_tran.voidreason " & _
                          ", tnalogin_tran.inouttype " & _
                          ", tnalogin_tran.userunkid " & _
                          ", tnalogin_tran.voiddatetime " & _
                          ", tnalogin_tran.sourcetype " & _
                          " FROM " & xDatabaseName & "..tnalogin_tran " & _
                          " LEFT JOIN " & xDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
                          " WHERE ISNULL(isvoid, 0) = 0 AND hremployee_master.employeeunkid = @employeeunkid " & _
                          " AND (original_intime = @logintime OR original_outtime = @logintime) "

            SqlCmd = New SqlCommand(strQ, SqlCn)
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeID)
            SqlCmd.Parameters.AddWithValue("@logintime", mdtLoginLogoutTime)

            Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLoginDate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dsList
    End Function

    Public Function IsExistTime(ByVal sqlCn As SqlConnection, ByVal xDataBaseName As String, ByVal dtTime As Date, Optional ByVal intUnkId As Integer = -1) As Boolean
        Dim strQ As String
        Dim strName As String = ""
        Dim exForce As Exception
        Dim intCnt As Integer = 1
        Try

            Dim sqlCmd As New SqlCommand()
            sqlCmd.Connection = sqlCn
            sqlCmd.Parameters.Clear()

            strQ = "SELECT  " & _
                        " loginunkid " & _
                        " FROM " & xDataBaseName & "..tnalogin_tran " & _
                        " WHERE employeeunkid = @empid " & _
                        " AND CONVERT(CHAR(8),logindate,112) = @dtdate " & _
                        " AND @dtintime  BETWEEN checkintime AND checkouttime AND isvoid = 0"


            If intUnkId <> -1 Then
                strQ &= "AND loginunkid <> @loginunkid "
                sqlCmd.Parameters.AddWithValue("@loginunkid", intUnkId)
            End If

            sqlCmd.Parameters.AddWithValue("@empid", mintEmployeeunkid)
            sqlCmd.Parameters.AddWithValue("@dtdate", convertDate(mdtLogindate.Date))
            sqlCmd.Parameters.AddWithValue("@dtintime", dtTime)
            intCnt = CInt(sqlCmd.ExecuteScalar())

            Return (intCnt > 0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExistTime; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    Public Function GetLoginType(ByVal sqlCn As SqlConnection, ByVal xDataBaseName As String, ByVal intEmployeeId As Integer, Optional ByVal dtLoginDate As Date = Nothing) As Integer
        Dim inType As Integer = -1
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim sqlCmd As SqlCommand = Nothing
        Try

            strQ = " SELECT " & _
                      "  inouttype " & _
                      ", checkintime " & _
                      ", loginunkid " & _
                      ", CONVERT(CHAR(8),logindate,112) AS logindate " & _
                      " FROM " & xDataBaseName & "..tnalogin_tran " & _
                      " WHERE employeeunkid = @employeeid AND isvoid = 0 "

            If dtLoginDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),logindate,112) = @logindate"
            End If

            strQ &= " ORDER BY checkintime  "


            sqlCmd = New SqlCommand()
            sqlCmd.Connection = sqlCn
            sqlCmd.Parameters.Clear()

            sqlCmd.Parameters.AddWithValue("@employeeid", intEmployeeId)

            If dtLoginDate <> Nothing Then
                sqlCmd.Parameters.AddWithValue("@logindate", convertDate(dtLoginDate.Date))
            End If

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            For Each dtrow As DataRow In dsList.Tables(0).Rows
                inType = dtrow.Item("inouttype")
                If IsDBNull(dtrow.Item("checkintime")) = False Then
                    mdtcheckintime = dtrow.Item("checkintime")
                End If
                mdtLogindate = convertDate(dtrow.Item("logindate").ToString())
                mintLoginunkid = CInt(dtrow.Item("loginunkid"))
            Next

            If inType = 1 Or inType = -1 Then
                Return 0
            Else
                Return 1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getLoginType; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function UpdateBreakTime(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal intEmpId As Integer, ByVal dtDate As Date) As Boolean
        Dim strQ As String
        Dim dsList As New DataSet
        Dim intBreakhr As Integer
        Dim sqlCmd As SqlCommand = Nothing
        Try

            mintEmployeeunkid = intEmpId
            mdtLogindate = dtDate.Date
            dsList = GetList(SqlCn, SqlTran, xDatabaseName, "List")

            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1

                strQ = "UPDATE " & xDatabaseName & "..tnalogin_tran SET " & _
                       " breakhr = @breakhr " & _
                       "WHERE loginunkid = @loginunkid AND isvoid = 0 "

                If i = 0 Then
                    intBreakhr = 0
                Else
                    If dsList.Tables("List").Rows(i - 1).Item("checkouttime") Is DBNull.Value Or dsList.Tables("List").Rows(i - 1).Item("checkouttime").ToString = "" Then
                        dsList.Tables("List").Rows(i - 1).Item("checkouttime") = CDate(dsList.Tables("List").Rows(i).Item("checkintime"))
                    End If

                    If CDate(dsList.Tables("List").Rows(i - 1).Item("checkouttime")) < CDate(dsList.Tables("List").Rows(i).Item("checkintime")) Then
                        intBreakhr = DateDiff(DateInterval.Second, CDate(dsList.Tables("List").Rows(i - 1).Item("checkouttime")), CDate(dsList.Tables("List").Rows(i).Item("checkintime")))
                    Else
                        intBreakhr = DateDiff(DateInterval.Second, CDate(dsList.Tables("List").Rows(i).Item("checkintime")), CDate(dsList.Tables("List").Rows(i - 1).Item("checkouttime")))
                    End If
                End If

                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.AddWithValue("@breakhr", intBreakhr)
                sqlCmd.Parameters.AddWithValue("@loginunkid", CInt(dsList.Tables("List").Rows(i).Item("loginunkid")))
                sqlCmd.ExecuteNonQuery()

            Next

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateBreakTime; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Public Function GetEmployeeMaxLoginDate(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String, ByVal mdtTodayDate As DateTime, ByVal intEmployeeId As Integer) As DataTable
        Dim strQ As String = ""
        Dim dtTable As DataTable = Nothing
        Dim sqlCmd As SqlCommand = Nothing
        Dim dsList As DataSet = Nothing
        Try
          
            strQ = " SELECT  loginunkid " & _
                      ", logindate " & _
                      " FROM    ( SELECT    loginunkid " & _
                      ", logindate " & _
                      ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY loginunkid DESC ) AS ROWNO " & _
                      " FROM  " & xDatabaseName & "..tnalogin_tran " & _
                      " WHERE  employeeunkid = @employeeunkid " & _
                      " AND isvoid = 0 " & _
                      " AND logindate = ( SELECT MAX(logindate) AS 'MaxLoginDate' " & _
                                                    " FROM  " & xDatabaseName & "..tnalogin_tran " & _
                                                    " WHERE employeeunkid = @employeeunkid " & _
                                                    " AND ( original_intime < @logintime " & _
                                                    " OR original_outtime < @logintime " & _
                                                    " ) " & _
                                                    "AND isvoid = 0 " & _
                                                 ") " & _
                    " ) AS a " & _
                    " WHERE a.ROWNO = 1 "

            sqlCmd = New SqlCommand(strQ, SqlCn)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmployeeId)
            sqlCmd.Parameters.AddWithValue("@logintime", mdtTodayDate)
            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeMaxLoginDate; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    Public Function GetMaxLoginDate(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String, ByVal dtLoginDate As Date, ByVal intEmployeeID As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim sqlCmd As SqlCommand = Nothing
        Dim dsList As DataSet = Nothing
        Try
            StrQ = " SELECT top 1 * FROM " & xDatabaseName & "..tnalogin_tran WHERE convert(char(8),logindate,112) < @date AND checkouttime is NULL AND employeeunkid = @employeeunkid AND isvoid = 0 ORDER by logindate DESC ,checkintime DESC"

            sqlCmd = New SqlCommand(StrQ, SqlCn)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@date", convertDate(dtLoginDate))
            sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmployeeID)
            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            If dsList IsNot Nothing Then
                dtTable = dsList.Tables(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMaxLoginTimeDate; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function


#End Region

#Region "For LOGIN SUMMERY Table"

    Public Function InsertLoginSummary(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction _
                                                      , ByVal xDatabaseName As String _
                                                      , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                      , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                                      , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                                      , ByVal xOnlyApproved As Boolean _
                                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                      , ByVal blnPolicyManagementTNA As Boolean _
                                                      , ByVal blnDonotAttendanceinSeconds As Boolean _
                                                      , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                                      , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                                      , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                                      , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                                      , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                      , Optional ByVal intEmpId As Integer = -1 _
                                                      , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                                      , Optional ByVal mstrFilter As String = "" _
                                                      , Optional ByVal intLoginsummaryunkid As Integer = 0 _
                                                      , Optional ByVal blnUsedForAbsent As Boolean = False) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim count As Integer = 0
        Dim mintltcome_graceinsec As Integer = 0
        Dim mintelrcome_graceinsec As Integer = 0
        Dim mintltgoing_graceinsec As Integer = 0
        Dim mintelrgoing_graceinsec As Integer = 0
        Dim SqlCmd As SqlCommand = Nothing
        Try

            'START CHECK FOR LOGIN TRAN FOR CALCULATE TOTAL WORKING HOURS

            If blnUsedForAbsent = False Then

                strQ = "SELECT isnull(sum(workhour),0) 'workhour' FROM " & xDatabaseName & "..tnalogin_tran WHERE employeeunkid = @Emp_id AND convert(char(8),logindate,112) = @date and isvoid = 0 AND inouttype = 1"

                SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                SqlCmd.Parameters.Clear()
                SqlCmd.Parameters.AddWithValue("@Emp_id", mintEmployeeunkid.ToString)
                SqlCmd.Parameters.AddWithValue("@date", convertDate(mdtLogindate.Date))
                Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                    dsList = New DataSet
                    sqlDataadp.Fill(dsList)
                End Using

                If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("workhour")) = 0 Then
                        DeleteLoginSummary(SqlCn, SqlTran, xDatabaseName, intLoginsummaryunkid)
                        Return True
                    Else
                        mintTotalhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                    End If

                End If
            ElseIf blnUsedForAbsent = True Then
                mintTotalhr = 0
            End If

            'END CHECK FOR LOGIN TRAN FOR CALCULATE TOTAL WORKING HOURS



            ' START CHECK FOR LOGIN SUMMARY IF RECORED EXIST FOR SPECIFIC EMPLOYEE AND SPECIFIC LOGIN DATE

            Dim intShiftID As Integer = 0

            strQ = "SELECT count(*) 'Rcount',isnull(loginsummaryunkid,0) 'loginsummaryunkid',isnull(shiftunkid,0) 'shiftunkid' FROM " & xDatabaseName & "..tnalogin_summary " & _
                      " WHERE employeeunkid = @empid AND convert(char(10),login_date,101) = @logdate group by loginsummaryunkid,shiftunkid"

            SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@empid", mintEmployeeunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@logdate", mdtLogindate)

            Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                dsList = Nothing
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                count = CInt(dsList.Tables(0).Rows(0)("Rcount"))
                mintLoginsummaryunkid = CInt(dsList.Tables(0).Rows(0)("loginsummaryunkid"))
                intShiftID = CInt(dsList.Tables(0).Rows(0)("shiftunkid"))
            End If

            If intShiftID > 0 AndAlso intShiftID <> mintShiftunkid Then

                strQ = "SELECT isnull(workhour,0) 'workhour' FROM  " & xDatabaseName & "..tnalogin_tran WHERE employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1 AND loginunkid =  @loginunkid"
                SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                SqlCmd.Parameters.Clear()
                SqlCmd.Parameters.AddWithValue("@Emp_id", mintEmployeeunkid)
                SqlCmd.Parameters.AddWithValue("@date", mdtLogindate)
                SqlCmd.Parameters.AddWithValue("@loginunkid", mintLoginunkid)
                Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                    dsList = Nothing
                    dsList = New DataSet
                    sqlDataadp.Fill(dsList)
                End Using

                If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                    mintTotalhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                End If

            End If

            ' END CHECK FOR LOGIN SUMMARY IF RECORED EXIST FOR SPECIFIC EMPLOYEE AND SPECIFIC LOGIN DATE


            ' START CHECK FOR HALF DAY AND FULL DAY

            Dim mblnIsNormalHrsForWeekend = False
            Dim mblnIsNormalHrsForHoliday = False
            Dim mblnIsNormalHrsForDayOff = False
            Dim mblnIsCountOnlyShiftTime As Boolean = False
            Dim mintShiftHrsPerDayInSec As Integer = 0
            Dim mblnCountOTonTotalWorkedHrs As Boolean = True
            Dim mintCountOTMinsAfterShiftEndTime As Integer = 0
            Dim mblnIsOpenShift As Boolean = False

            Dim objshift As New clsNewshift_master
            objshift._Shiftunkid(SqlCn, SqlTran, xDatabaseName) = mintShiftunkid


            mblnIsOpenShift = objshift._IsOpenShift
            mblnIsNormalHrsForWeekend = objshift._IsNormalHrsForWeekend
            mblnIsNormalHrsForHoliday = objshift._IsNormalHrsForHoliday
            mblnIsNormalHrsForDayOff = objshift._IsNormalHrsForDayOff
            mblnIsCountOnlyShiftTime = objshift._CountOnlyShiftTime
            mblnCountOTonTotalWorkedHrs = objshift._CountOTonTotalWorkedHrs

            If mblnIsUnpaidLeave = False AndAlso mblnIspaidLeave = False AndAlso mblnIsWeekend = False Then
                mblnIsDayOffshift = IsDayOffExist(SqlCn, SqlTran, xDatabaseName, mdtLogindate, mintEmployeeunkid, -1)
            End If

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(SqlCn, SqlTran, xDatabaseName, objshift._Shiftunkid(SqlCn, SqlTran, xDatabaseName))

            Dim mdtShiftInTime As DateTime = Nothing
            Dim mdtShiftOutTime As DateTime = Nothing

            mdctFinalOTOrder = New Dictionary(Of String, Integer)
            mdctFinalOTOrder.Add("OT1", 0)
            mdctFinalOTOrder.Add("OT2", 0)
            mdctFinalOTOrder.Add("OT3", 0)
            mdctFinalOTOrder.Add("OT4", 0)

            Dim mdctOTOrder As Dictionary(Of Integer, String) = Nothing
            Dim mdctFinalOTPenalty As Dictionary(Of String, Integer) = Nothing


            Dim drDayName() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))
            If drDayName.Length > 0 Then

                Dim mintShiftCountDay As Integer = 0
                If objshift._IsOpenShift = False Then
                    mintShiftCountDay = DateDiff(DateInterval.Day, CDate(drDayName(0)("starttime")).Date, CDate(drDayName(0)("endtime")).Date)
                    mdtShiftInTime = mdtLogindate.Date & " " & CDate(drDayName(0)("starttime")).ToShortTimeString()
                    mdtShiftOutTime = mdtLogindate.Date.AddDays(mintShiftCountDay) & " " & CDate(drDayName(0)("endtime")).ToShortTimeString()
                    mintShiftHrsPerDayInSec = DateDiff(DateInterval.Second, mdtShiftInTime, mdtShiftOutTime)
                    mintCountOTMinsAfterShiftEndTime = CInt(drDayName(0)("countotmins_aftersftendtimeinsec"))
                End If

                Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                If drWeekend.Length > 0 Then
                    mblnIsWeekend = True
                End If

                'FOR HOLIDAY
                Dim dtTable As DataTable = GetEmployeeHolidayList(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate.Date, mdtLogindate.Date)
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mblnIsholiday = True
                End If

            End If

            If mblnIsWeekend AndAlso mblnIsholiday Then
                mblnIsholiday = blnIsHolidayConsiderOnWeekend
                mblnIsWeekend = Not blnIsHolidayConsiderOnWeekend
            End If

            If mblnIsDayOffshift AndAlso mblnIsWeekend Then
                mblnIsDayOffshift = blnIsDayOffConsiderOnWeekend
                mblnIsWeekend = Not blnIsDayOffConsiderOnWeekend
            End If

            If mblnIsDayOffshift AndAlso mblnIsholiday Then
                mblnIsholiday = blnIsHolidayConsiderOnDayoff
                mblnIsDayOffshift = Not blnIsHolidayConsiderOnDayoff
            End If


            If mblnIspaidLeave OrElse mblnIsUnpaidLeave Then
                mblnIsholiday = False
                mblnIsDayOffshift = False
                mblnIsWeekend = False
            End If

            If mintTotalhr > 0 Then

                If objshift._IsOpenShift = False Then
                    CalculateEarlyLate_ComingGoing(SqlCn, SqlTran, xDatabaseName, mdtShiftInTime, mdtShiftOutTime)
                End If
                If drDayName.Length > 0 Then

                    If blnPolicyManagementTNA Then
                        mintPolicyId = GetPolicyTranID(SqlCn, SqlTran, xDatabaseName, mdtLogindate.Date, mintEmployeeunkid)

                        Dim objPolicyTran As New clsPolicy_tran
                        objPolicyTran.GetPolicyTran(SqlCn, SqlTran, xDatabaseName, mintPolicyId, -1)
                        Dim dtPolicyTran As DataTable = objPolicyTran._DataList

                        Dim drPolicy() As DataRow = dtPolicyTran.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))
                        If drPolicy.Length > 0 Then
                            mintelrcome_graceinsec = CInt(drPolicy(0)("elrcome_graceinsec"))
                            mintltcome_graceinsec = CInt(drPolicy(0)("ltcome_graceinsec"))
                            mintelrgoing_graceinsec = CInt(drPolicy(0)("elrgoing_graceinsec"))
                            mintltgoing_graceinsec = CInt(drPolicy(0)("ltgoing_graceinsec"))

                            If blnFirstCheckInLastCheckOut = True Then
                                mintTotalhr = GetEmployeeTotalWorkHrFromPolicy(SqlCn, SqlTran, xDatabaseName, drPolicy, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut)
                            End If

                            Dim mintFinalWorkingHrs As Integer = 0

                            If blnFirstCheckInLastCheckOut Then
                                If CInt(drPolicy(0)("countbreaktimeaftersec")) < mintTotalhr Then
                                    mintFinalWorkingHrs = (CInt(drDayName(0)("workinghrsinsec")) - CInt(drPolicy(0)("breaktimeinsec")))
                                Else
                                    mintFinalWorkingHrs = CInt(drDayName(0)("workinghrsinsec"))
                                End If
                            ElseIf blnFirstCheckInLastCheckOut = False Then
                                mintFinalWorkingHrs = CInt(drDayName(0)("workinghrsinsec"))
                            End If

                            If mblnIsholiday = False AndAlso mblnIsWeekend = False AndAlso mblnIsDayOffshift = False Then   ' OVERTIME GOES TO OT1,OT2 WHEN IT'S NOT WEEKEND OR HOLIDAY.
                                'mdctOTOrder = objconfig.GetOTOrder(True, mblnIsWeekend, mblnIsDayOffshift, mblnIsholiday)
                                CalculatePolicyMgmtWorkedHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)

                            ElseIf mblnIsholiday OrElse mblnIsWeekend OrElse mblnIsDayOffshift Then  ' OVERTIME GOES TO OT1,OT2,OT3,OT4 WHEN IT'S WEEKEND OR HOLIDAY.  
                                mdctOTOrder = Nothing
                                mdctOTOrder = GetOTOrder(SqlCn, xCompanyUnkid, False, mblnIsWeekend, mblnIsDayOffshift, mblnIsholiday)

                                If mblnIsWeekend Then
                                    If mblnIsNormalHrsForWeekend Then
                                        CalculatePolicyMgmtWorkedHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)
                                    Else
                                        CalculateOTOnWkPHDayOffHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)
                                    End If

                                ElseIf mblnIsholiday Then
                                    If mblnIsNormalHrsForHoliday Then
                                        CalculatePolicyMgmtWorkedHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)
                                    Else
                                        CalculateOTOnWkPHDayOffHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)
                                    End If

                                ElseIf mblnIsDayOffshift Then
                                    If mblnIsNormalHrsForDayOff Then
                                        CalculatePolicyMgmtWorkedHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)
                                    Else
                                        CalculateOTOnWkPHDayOffHrs(SqlCn, SqlTran, xDatabaseName, mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, mblnIsOpenShift)
                                    End If

                                End If

                            End If    ' END FOR mblnIsholiday,mblnIsWeekend , mblnIsDayOffshift

                        End If   ' END FOR drPolicy.Length > 0 

                    Else

                        If (CInt(drDayName(0)("halffromhrsinsec")) <= 0 AndAlso CInt(drDayName(0)("halftohrsinsec")) <= 0) AndAlso CInt(drDayName(0)("workinghrsinsec")) > mintTotalhr Then

                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                If mblnIsOpenShift = False Then
                                    mdblDayType = 0
                                ElseIf mblnIsOpenShift AndAlso mintTotalhr > 0 Then
                                    mdblDayType = 1
                                End If
                            End If

                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) < mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                                mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If

                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR HALF DAY
                        ElseIf mintTotalhr >= CInt(drDayName(0)("halffromhrsinsec")) And mintTotalhr <= CInt(drDayName(0)("halftohrsinsec")) Then

                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                mdblDayType = 0.5
                            End If

                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                                mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If

                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR NOT EVEN HALF DAY & SHORT HRS
                        ElseIf (CInt(drDayName(0)("halffromhrsinsec")) > 0 AndAlso CInt(drDayName(0)("halftohrsinsec")) > 0) AndAlso mintTotalhr < CInt(drDayName(0)("halffromhrsinsec")) Then

                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                mdblDayType = 0
                            End If

                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                                mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If

                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR SHORT HOURS
                        ElseIf (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr) > CInt(drDayName(0)("calcshorttimebeforeinsec")) Then

                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                mdblDayType = 1
                            End If

                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                                mintShorthr = (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr)
                            End If

                            mdctFinalOTOrder.Item("OT1") = 0


                            'FOR BEFORE SHORT HOURS TIME 
                        ElseIf (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr) <= CInt(drDayName(0)("calcshorttimebeforeinsec")) And (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr) > 0 Then

                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                mdblDayType = 1
                            End If

                            mintShorthr = 0
                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR EARLY CHECK OUT
                        ElseIf mintTotalhr < CInt(drDayName(0)("halffromhrsinsec")) Then
                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                mdblDayType = 1
                            End If

                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                                mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If

                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR OVERTIME
                        ElseIf mintTotalhr > (CInt(drDayName(0)("workinghrsinsec")) + CInt(drDayName(0)("calcovertimeafterinsec"))) Then
                            If mblnIsCountOnlyShiftTime Then
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)
                            Else
                                mdblDayType = 1
                            End If

                            If mblnCountOTonTotalWorkedHrs Then
                                mdctFinalOTOrder.Item("OT1") = mintTotalhr - (CInt(drDayName(0)("workinghrsinsec")) + CInt(drDayName(0)("calcovertimeafterinsec")))
                            Else

                                If mintCountOTMinsAfterShiftEndTime <= 0 Then
                                    If DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime) >= CInt(drDayName(0)("calcovertimeafterinsec")) Then
                                        mdctFinalOTOrder.Item("OT1") = CInt(DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime) - CInt(drDayName(0)("calcovertimeafterinsec")))
                                    Else
                                        mdctFinalOTOrder.Item("OT1") = 0
                                    End If
                                ElseIf mintCountOTMinsAfterShiftEndTime > 0 Then
                                    If (mintCountOTMinsAfterShiftEndTime / 60) < DateDiff(DateInterval.Minute, mdtShiftOutTime, mdtCheckouttime) Then
                                        mdctFinalOTOrder.Item("OT1") = CInt(DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime))
                                    ElseIf CInt(drDayName(0)("calcovertimeafterinsec")) > 0 Then
                                        If DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime) > CInt(drDayName(0)("calcovertimeafterinsec")) Then
                                            mdctFinalOTOrder.Item("OT1") = CInt(CInt(drDayName(0)("calcovertimeafterinsec")) - DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime))
                                        Else
                                            mdctFinalOTOrder.Item("OT1") = 0
                                        End If
                                    Else
                                        mdctFinalOTOrder.Item("OT1") = 0
                                    End If
                                End If

                            End If

                            mintShorthr = 0

                        ElseIf mintTotalhr <= (CInt(drDayName(0)("workinghrsinsec")) + CInt(drDayName(0)("calcovertimeafterinsec"))) Then

                            If mblnIsCountOnlyShiftTime Then
                                If CInt(drDayName(0)("calcshorttimebeforeinsec")) < DateDiff(DateInterval.Second, mdtCheckouttime, mdtShiftOutTime) Then
                                    mintShorthr = CInt(DateDiff(DateInterval.Second, mdtCheckouttime, mdtShiftOutTime))
                                Else
                                    mintShorthr = 0
                                End If
                                mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift)

                            Else
                                mdblDayType = 1
                                mintShorthr = 0
                            End If

                        End If


                    End If  ' END FOR ConfigParameter._Object._PolicyManagementTNA


                    If blnPolicyManagementTNA = False Then
                        CalculateNightHrs(drDayName)
                    End If  ' END FOR ConfigParameter._Object._PolicyManagementTNA


                End If  ' END FOR ConfigParameter._Object._PolicyManagementTNA

                If objshift._IsOpenShift = False Then
                    If DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime) > 0 Then
                        mintLateComing_WOGrace = CInt(DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime))
                    End If

                    If mdtRoundoffInTime <> Nothing AndAlso mintltcome_graceinsec > 0 Then

                        If mdtShiftInTime < mdtcheckintime AndAlso DateDiff(DateInterval.Minute, mdtShiftInTime, mdtcheckintime) > (mintltcome_graceinsec / 60) _
                                        AndAlso DateDiff(DateInterval.Minute, mdtShiftInTime, mdtcheckintime) > 0 Then
                            mintLateComing_AfterGrace = CInt(DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime) - mintltcome_graceinsec)
                        End If
                    End If
                End If


                'If mintPolicyId > 0 And blnPolicyManagementTNA Then   'START mintPolicyId > 0
                '    mdctFinalOTPenalty = New Dictionary(Of String, Integer)
                '    CalculateOTPenalty(mdctOTOrder, mdctFinalOTPenalty, mintltcome_graceinsec)
                'End If    'END mintPolicyId > 0

                'END FOR SET LATE COMING WITHOUT GRACE.

            End If   ' END FOR mintTotalhr > 0


            ' END CHECK FOR HALF DAY AND FULL DAY

InsertPoint:

            SqlCmd.Connection = SqlCn
            SqlCmd = New SqlCommand
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@empunkid", mintEmployeeunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@shiftunkid", mintShiftunkid.ToString)
            SqlCmd.Parameters.AddWithValue("@login_date", mdtLogindate)
            SqlCmd.Parameters.AddWithValue("@day_type", mdblDayType)
            SqlCmd.Parameters.AddWithValue("@total_hrs", mintTotalhr.ToString)
            SqlCmd.Parameters.AddWithValue("@ispaidleave", mblnIspaidLeave.ToString)
            SqlCmd.Parameters.AddWithValue("@isunpaidleave", mblnIsUnpaidLeave.ToString)
            SqlCmd.Parameters.AddWithValue("@isabsentprocess", mblnIsAbsentProcess.ToString)
            SqlCmd.Parameters.AddWithValue("@total_short_hrs", mintShorthr.ToString)
            SqlCmd.Parameters.AddWithValue("@total_nighthrs", mintTotalNighthrs.ToString)
            SqlCmd.Parameters.AddWithValue("@isweekend", mblnIsWeekend.ToString)
            SqlCmd.Parameters.AddWithValue("@isholiday", mblnIsholiday.ToString)
            SqlCmd.Parameters.AddWithValue("@policyunkid", mintPolicyId.ToString)
            SqlCmd.Parameters.AddWithValue("@total_overtime", CInt(mdctFinalOTOrder.Item("OT1")))
            SqlCmd.Parameters.AddWithValue("@ot2", CInt(mdctFinalOTOrder.Item("OT2")))
            SqlCmd.Parameters.AddWithValue("@ot3", CInt(mdctFinalOTOrder.Item("OT3")))
            SqlCmd.Parameters.AddWithValue("@ot4", CInt(mdctFinalOTOrder.Item("OT4")))
            SqlCmd.Parameters.AddWithValue("@elrcoming_grace", mintearly_coming)
            SqlCmd.Parameters.AddWithValue("@ltcoming_grace", mintlate_coming)
            SqlCmd.Parameters.AddWithValue("@elrgoing_grace", mintearly_going)
            SqlCmd.Parameters.AddWithValue("@ltgoing_grace", mintlate_going)
            SqlCmd.Parameters.AddWithValue("@isdayoff", mblnIsDayOffshift)
            SqlCmd.Parameters.AddWithValue("@lvdayfraction", mdecDayFraction)
            SqlCmd.Parameters.AddWithValue("@LateComing_WOGrace", mintLateComing_WOGrace)
            SqlCmd.Parameters.AddWithValue("@LateComing_AfterGrace", mintLateComing_AfterGrace)
            If mdctFinalOTPenalty IsNot Nothing Then
                SqlCmd.Parameters.AddWithValue("@ot1_penalty", CInt(mdctFinalOTPenalty.Item("OT1_Penalty")))
                SqlCmd.Parameters.AddWithValue("@ot2_penalty", CInt(mdctFinalOTPenalty.Item("OT2_Penalty")))
                SqlCmd.Parameters.AddWithValue("@ot3_penalty", CInt(mdctFinalOTPenalty.Item("OT3_Penalty")))
                SqlCmd.Parameters.AddWithValue("@ot4_penalty", CInt(mdctFinalOTPenalty.Item("OT4_Penalty")))
            Else
                SqlCmd.Parameters.AddWithValue("@ot1_penalty", 0)
                SqlCmd.Parameters.AddWithValue("@ot2_penalty", 0)
                SqlCmd.Parameters.AddWithValue("@ot3_penalty", 0)
                SqlCmd.Parameters.AddWithValue("@ot4_penalty", 0)
            End If
            SqlCmd.Parameters.AddWithValue("@leavetypeunkid", mintLeaveTypeId)

            If count = 0 Then


ForMultiShift:

                strQ = "INSERT INTO " & xDatabaseName & "..tnalogin_summary ( " & _
                          "  employeeunkid " & _
                          ", shiftunkid " & _
                          ", login_date " & _
                          ", day_type " & _
                          ", total_hrs " & _
                          ", ispaidleave " & _
                          ", isunpaidleave " & _
                          ", isabsentprocess " & _
                          ", total_overtime " & _
                          ", total_short_hrs" & _
                          ", total_nighthrs" & _
                          ", isweekend" & _
                          ", isholiday" & _
                          ", ot2 " & _
                          ", ot3 " & _
                          ", ot4 " & _
                          ", elrcoming_grace " & _
                          ", ltcoming_grace " & _
                          ", elrgoing_grace " & _
                          ", ltgoing_grace " & _
                          ", policyunkid " & _
                          ", isdayoff " & _
                          ", lvdayfraction " & _
                          ", ltcoming " & _
                          ", ltcoming_aftrgrace " & _
                          ", ot1_penalty " & _
                          ", ot2_penalty " & _
                          ", ot3_penalty " & _
                          ", ot4_penalty " & _
                          ", dayid " & _
                          ", leavetypeunkid " & _
                          " ) VALUES (" & _
                          "  @empunkid " & _
                          ", @shiftunkid " & _
                          ", @login_date " & _
                          ", @day_type " & _
                          ", @total_hrs " & _
                          ", @ispaidleave " & _
                          ", @isunpaidleave " & _
                          ", @isabsentprocess " & _
                          ", @total_overtime " & _
                          ", @total_short_hrs" & _
                          ", @total_nighthrs" & _
                          ", @isweekend" & _
                          ", @isholiday" & _
                          ", @ot2 " & _
                          ", @ot3 " & _
                          ", @ot4 " & _
                          ", @elrcoming_grace " & _
                          ", @ltcoming_grace " & _
                          ", @elrgoing_grace " & _
                          ", @ltgoing_grace " & _
                          ", @policyunkid " & _
                          ", @isdayoff " & _
                          ", @lvdayfraction " & _
                          ", @LateComing_WOGrace " & _
                          ", @LateComing_AfterGrace " & _
                          ", @ot1_penalty " & _
                          ", @ot2_penalty " & _
                          ", @ot3_penalty " & _
                          ", @ot4_penalty " & _
                          ", (DATEPART(WEEKDAY,@login_date)) -1 " & _
                          ", @leavetypeunkid " & _
                         " ); SELECT @@identity"

                SqlCmd.CommandText = strQ
                SqlCmd.ExecuteScalar()

            ElseIf count > 0 Then

                If blnPolicyManagementTNA = False Then
                    If intShiftID <> mintShiftunkid Then
                        GoTo ForMultiShift
                    End If
                End If

                SqlCmd.Parameters.AddWithValue("@loginsummaryunkid", mintLoginsummaryunkid.ToString)

                strQ = " UPDATE " & xDatabaseName & "..tnalogin_summary SET " & _
                           "  employeeunkid = @empunkid" & _
                           ", shiftunkid = @shiftunkid" & _
                           ", login_date = @login_date" & _
                           ", day_type = @day_type" & _
                           ", total_hrs = @total_hrs" & _
                           ", ispaidleave = @ispaidleave" & _
                           ", isunpaidleave = @isunpaidleave" & _
                           ", isabsentprocess = @isabsentprocess " & _
                           ", total_overtime = @total_overtime" & _
                           ", total_short_hrs = @total_short_hrs " & _
                           " ,total_nighthrs =  @total_nighthrs " & _
                           ", isweekend =  @isweekend " & _
                           ", isholiday =  @isholiday " & _
                           ", ot2 = @ot2 " & _
                           ", ot3 = @ot3 " & _
                           ", ot4 = @ot4 " & _
                           ", elrcoming_grace = @elrcoming_grace " & _
                           ", ltcoming_grace = @ltcoming_grace " & _
                           ", elrgoing_grace = @elrgoing_grace " & _
                           ", ltgoing_grace = @ltgoing_grace " & _
                           ", policyunkid = @policyunkid " & _
                           ", isdayoff = @isdayoff " & _
                           ", lvdayfraction = @lvdayfraction " & _
                           ", ltcoming = @LateComing_WOGrace " & _
                           ", ltcoming_aftrgrace = @LateComing_AfterGrace " & _
                           ", ot1_penalty = @ot1_penalty " & _
                           ", ot2_penalty = @ot2_penalty " & _
                           ", ot3_penalty = @ot3_penalty " & _
                           ", ot4_penalty = @ot4_penalty " & _
                           ", dayid = (DATEPART(WEEKDAY,@login_date)) -1 " & _
                           ", leavetypeunkid = @leavetypeunkid " & _
                           " WHERE loginsummaryunkid = @loginsummaryunkid "

                SqlCmd.CommandText = strQ
                SqlCmd.ExecuteNonQuery()

            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertLoginSummary; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Sub DeleteLoginSummary(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal intLoginsummryid As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim sqlCmd As SqlCommand = Nothing
        Try
            strQ = "Delete From " & xDatabaseName & "..tnalogin_summary where loginsummaryunkid=@loginsummaryunkid"
            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@loginsummaryunkid", intLoginsummryid)
            sqlCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteLoginSummary; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    Public Function GetEmployeeTotalWorkHrFromPolicy(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String _
                                                                             , ByVal drPolicy() As DataRow, ByVal blnDonotAttendanceinSeconds As Boolean, ByVal blnFirstCheckInLastCheckOut As Boolean) As Integer

        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim iFinalBreakTime As Integer = 0
        Dim intWorkhr As Integer = 0
        Dim sqlCmd As SqlCommand = Nothing

        Try

            If CInt(drPolicy(0).Item("countbreaktimeaftersec")) > 0 Then
                If mintTotalhr > CInt(drPolicy(0).Item("countbreaktimeaftersec")) Then

                    iFinalBreakTime = CInt(drPolicy(0).Item("breaktimeinsec"))

                    If blnFirstCheckInLastCheckOut = True Then
                        strQ = "SELECT loginunkid,workhour,breakhr FROM " & xDatabaseName & "..tnalogin_tran WHERE employeeunkid = '" & mintEmployeeunkid & "' AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND workhour > 0 AND isvoid = 0 "
                        sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                        sqlCmd.Parameters.Clear()
                        Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                            dsList = New DataSet
                            sqlDataadp.Fill(dsList)
                        End Using

                        Dim mintMaxHourInSec As Integer = CInt(dsList.Tables(0).Compute("Max(workhour)", ""))
                        Dim mintMaxLoginID As Integer = CInt(dsList.Tables(0).Compute("Max(loginunkid)", "workhour=" & mintMaxHourInSec))

                        If dsList.Tables(0).Rows.Count > 0 AndAlso mintMaxHourInSec > iFinalBreakTime Then
                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                strQ = "UPDATE  " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END), " & _
                                   "breakhr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                   "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND breakhr > 0 AND isvoid = 0 "


                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE  " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime), " & _
                                       "breakhr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND breakhr > 0 AND isvoid = 0 "
                            End If

                            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                            sqlCmd.Parameters.Clear()
                            sqlCmd.ExecuteNonQuery()

                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                strQ = "UPDATE  " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END) - " & iFinalBreakTime & ", " & _
                                       "breakhr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND workhour > 0 AND breakhr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "


                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE  " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime) - " & iFinalBreakTime & ", " & _
                                       "breakhr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND workhour > 0 AND breakhr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "
                            End If


                            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                            sqlCmd.Parameters.Clear()
                            sqlCmd.ExecuteNonQuery()

                            _Loginunkid(SqlCn, SqlTran, xDatabaseName) = mintMaxLoginID

                            InsertAuditTrailLogin(SqlCn, SqlTran, xDatabaseName, 2, blnDonotAttendanceinSeconds)

                            strQ = "SELECT isnull(sum(workhour),0) 'workhour' FROM " & xDatabaseName & "..tnalogin_tran WHERE employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1 "
                            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                            sqlCmd.Parameters.Clear()
                            sqlCmd.Parameters.AddWithValue("@Emp_id", mintEmployeeunkid.ToString)
                            sqlCmd.Parameters.AddWithValue("@date", mdtLogindate)
                            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                                dsList = New DataSet
                                sqlDataadp.Fill(dsList)
                            End Using

                            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                                intWorkhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                            End If

                        End If

                    End If

                End If

            End If

            If intWorkhr <= 0 Then intWorkhr = mintTotalhr

            iFinalBreakTime = 0
            If CInt(drPolicy(0).Item("countteatimeaftersec")) > 0 Then

                If intWorkhr > CInt(drPolicy(0).Item("countteatimeaftersec")) Then

                    iFinalBreakTime = CInt(drPolicy(0).Item("teatimeinsec"))

                    If blnFirstCheckInLastCheckOut = True Then

                        strQ = "SELECT loginunkid,workhour,breakhr FROM " & xDatabaseName & "..tnalogin_tran WHERE employeeunkid = '" & mintEmployeeunkid & "' AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND workhour > 0 AND isvoid = 0 "
                        sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                        sqlCmd.Parameters.Clear()
                        Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                            dsList = New DataSet
                            sqlDataadp.Fill(dsList)
                        End Using


                        Dim mintMaxHourInSec As Integer = CInt(dsList.Tables(0).Compute("Max(workhour)", ""))
                        Dim mintMaxLoginID As Integer = CInt(dsList.Tables(0).Compute("Max(loginunkid)", "workhour=" & mintMaxHourInSec))

                        If dsList.Tables(0).Rows.Count > 0 AndAlso mintMaxHourInSec > iFinalBreakTime Then

                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                strQ = "UPDATE " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END ), " & _
                                   "teahr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                   "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND teahr > 0 AND isvoid = 0 "

                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime), " & _
                                       "teahr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND teahr > 0 AND isvoid = 0 "
                            End If

                            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                            sqlCmd.Parameters.Clear()
                            sqlCmd.ExecuteNonQuery()


                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                strQ = "UPDATE " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN  roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END ) - ISNULL(breakhr,0) - " & _
                                          iFinalBreakTime & ",  teahr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND workhour > 0 AND teahr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "

                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE " & xDatabaseName & "..tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime) - ISNULL(breakhr,0) -  " & iFinalBreakTime & ", " & _
                                       "teahr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & convertDate(mdtLogindate) & "' AND workhour > 0 AND teahr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "
                            End If

                            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                            sqlCmd.Parameters.Clear()
                            sqlCmd.ExecuteNonQuery()

                            _Loginunkid(SqlCn, SqlTran, xDatabaseName) = mintMaxLoginID

                            InsertAuditTrailLogin(SqlCn, SqlTran, xDatabaseName, 2, blnDonotAttendanceinSeconds)

                            strQ = "SELECT isnull(sum(workhour),0) 'workhour' FROM " & xDatabaseName & "..tnalogin_tran where employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1 "
                            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
                            sqlCmd.Parameters.Clear()
                            sqlCmd.Parameters.AddWithValue("@Emp_id", mintEmployeeunkid)
                            sqlCmd.Parameters.AddWithValue("@date", mdtLogindate)

                            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                                dsList = New DataSet
                                sqlDataadp.Fill(dsList)
                            End Using

                            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                                intWorkhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                            End If

                        End If

                    End If

                End If

            End If

            If intWorkhr <= 0 Then intWorkhr = mintTotalhr

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalWorkHrFromPolicy; Module Name: " & mstrModuleName)
        End Try
        Return intWorkhr
    End Function

    Private Sub CalculatePolicyMgmtWorkedHrs(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal mintFinalWorkingHrs As Integer _
                                                                 , ByVal mdctOTOrder As Dictionary(Of Integer, String), ByVal drPolicy() As DataRow, ByVal drDayName() As DataRow, ByVal mblnIsCountOnlyShiftTime As Boolean _
                                                                   , ByVal xShiftOutTime As DateTime, ByVal blnIsOpenShift As Boolean)


        Try
            If mintTotalhr < mintFinalWorkingHrs Then
                If mblnIsCountOnlyShiftTime Then
                    mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                Else
                    mdblDayType = 0
                End If

                If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                    mintShorthr = mintFinalWorkingHrs - mintTotalhr
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) Then

                If mblnIsCountOnlyShiftTime Then
                    mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                Else
                    mdblDayType = 0
                End If

                If mintTotalhr > mintFinalWorkingHrs + CInt(drPolicy(0)("ltgoing_graceinsec")) AndAlso mintFinalWorkingHrs > 0 Then  'CHECK FOR BASE HOUR + LATE GOING GRACE FOR OVERTIME IF EXCEED THEN GIVE OVERTIME OTHERWISE NO OVERTIME
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = mintTotalhr - mintFinalWorkingHrs
                Else
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = 0
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0
                mintShorthr = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
                          mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) Then

                If mblnIsCountOnlyShiftTime Then
                    mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                Else
                    mdblDayType = 1
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If
                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")))
                End If
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0
                mintShorthr = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
                 mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) AndAlso _
                 mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) Then

                If mblnIsCountOnlyShiftTime Then
                    mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                Else
                    mdblDayType = 1
                End If
                mintShorthr = 0

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")))
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
                    mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) AndAlso _
                    mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) AndAlso _
                    mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then  ' FOR TOTALHOURS - BASE HOURS IS GREATER THAN EQUAL TO  OT1 AND OT2. 

                If mblnIsCountOnlyShiftTime Then
                    mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                Else
                    mdblDayType = 1
                End If

                mintShorthr = 0

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                End If

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
               mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) AndAlso _
               mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) AndAlso _
               mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then  ' FOR TOTALHOURS - BASE HOURS IS GREATER THAN EQUAL TO  OT1 AND OT2. 

                If mblnIsCountOnlyShiftTime Then
                    mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                Else
                    mdblDayType = 1
                End If

                mintShorthr = 0

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) += mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                                + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                                + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then


                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) += mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                              + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                              + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))

                ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) += mintTotalhr - (CInt(drDayName(0)("workinghrsinsec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))

                ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) += mintTotalhr - (CInt(drDayName(0)("workinghrsinsec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))


                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculatePolicyMgmtWorkedHrs; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateOTOnWkPHDayOffHrs(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String _
                                                                , ByVal mintFinalWorkingHrs As Integer, ByVal drPolicy() As DataRow, ByVal mdctOTOrder As Dictionary(Of Integer, String) _
                                                                , ByVal mblnIsCountOnlyShiftTime As Boolean, ByVal xShiftOutTime As DateTime, ByVal blnIsOpenShift As Boolean)


        Try
            If mintTotalhr <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) Then

                If mintTotalhr < mintFinalWorkingHrs Then
                    mdblDayType = 0
                Else
                    If mblnIsCountOnlyShiftTime Then
                        mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                    Else
                        mdblDayType = 1
                    End If
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = mintTotalhr
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0
                mintShorthr = 0

            ElseIf mintTotalhr > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) Then
                If mintTotalhr < mintFinalWorkingHrs Then
                    mdblDayType = 0
                Else
                    If mblnIsCountOnlyShiftTime Then
                        mdblDayType = CalculateDayType(SqlCn, SqlTran, xDatabaseName, mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift)
                    Else
                        mdblDayType = 1
                    End If
                End If

                mintShorthr = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

                If mintTotalhr - CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = mintTotalhr - CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

                ElseIf mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) AndAlso _
                         mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0


                ElseIf mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) AndAlso _
                         mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")))


                ElseIf mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec"))


                    If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    End If

                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateOTOnWkPHDayOffHrs; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateNightHrs(ByVal drDayName As DataRow())
        Try

            If mdtcheckintime.Date <> Nothing AndAlso (CDate(drDayName(0)("nightfromhrs")) <> Nothing AndAlso CDate(drDayName(0)("nighttohrs")) <> Nothing) Then

                If DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), CDate(drDayName(0)("nighttohrs")).AddDays(1)) > 0 AndAlso CDate(drDayName(0)("nightfromhrs")) <> CDate(drDayName(0)("nighttohrs")) Then

                    If CDate(drDayName(0)("nightfromhrs")) > CDate(drDayName(0)("nighttohrs")) Then
                        If mdtcheckintime.ToString("HH") < 12 Then
                            drDayName(0)("nightfromhrs") = mdtcheckintime.Date.AddDays(-1) & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString
                            drDayName(0)("nighttohrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                        Else
                            drDayName(0)("nightfromhrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString
                            drDayName(0)("nighttohrs") = mdtcheckintime.Date.AddDays(1) & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                        End If
                    Else
                        drDayName(0)("nightfromhrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString()
                        drDayName(0)("nighttohrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                    End If

                    If mdtcheckintime <> Nothing AndAlso mdtCheckouttime <> Nothing Then
                        If mdtcheckintime <= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), CDate(drDayName(0)("nighttohrs")))

                        ElseIf mdtcheckintime <= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime <= CDate(drDayName(0)("nighttohrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nightfromhrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), mdtCheckouttime)

                        ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime <= CDate(drDayName(0)("nighttohrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, mdtcheckintime, mdtCheckouttime)

                        ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nightfromhrs")).AddDays(1) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")).AddDays(1), mdtCheckouttime)

                        ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, mdtcheckintime, CDate(drDayName(0)("nighttohrs")))

                        End If

                        If mintTotalNighthrs < 0 Then mintTotalNighthrs = 0
                    End If

                    'START FOR WHEN EMPLOYEE DIRECTLY LOGIN TO NIGHT HRS THEN NO NEED TO CALCULATE SHORT HRS OR OVERTIME HR

                    'strQ = "Select Count(*) From tnalogin_tran WHERE Convert(char(8),logindate,112) = @logindate AND employeeunkid = @employeeunkid  "
                    'objDataOperation.ClearParameters()
                    'objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLogindate))
                    'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                    'Dim icnt As Integer = objDataOperation.RecordCount(strQ)
                    'If icnt <= 1 Then
                    '    mintShorthr = 0
                    'End If

                    'END FOR WHEN EMPLOYEE DIRECTLY LOGIN TO NIGHT HRS THEN NO NEED TO CALCULATE SHORT HRS OR OVERTIME HR

                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateNightHrs; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateEarlyLate_ComingGoing(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal xShiftInTime As DateTime, ByVal xShiftOutTime As DateTime)
        Try
            Dim dsTimesheet As DataSet = GetList(SqlCn, SqlTran, xDatabaseName, "List", True)
            Dim drTimesheet() As DataRow = dsTimesheet.Tables(0).Select("loginunkid <> " & mintLoginunkid & " AND loginunkid = MIN(loginunkid)")
            If drTimesheet.Length <= 0 Then

                If mdtRoundoffInTime = Nothing Then

                    If mdtcheckintime <= xShiftInTime Then    'Early Coming   
                        mintearly_coming = DateDiff(DateInterval.Second, mdtcheckintime, xShiftInTime)
                    ElseIf mdtcheckintime >= xShiftInTime Then       'Late Coming  
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, mdtcheckintime)
                    End If
                Else

                    If mdtRoundoffInTime <= xShiftInTime Then    'Early Coming   
                        mintearly_coming = DateDiff(DateInterval.Second, mdtRoundoffInTime, xShiftInTime)
                    ElseIf mdtRoundoffInTime >= xShiftInTime Then       'Late Coming  
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, mdtRoundoffInTime)
                    End If

                End If
            Else

                If IsDBNull(drTimesheet(0)("roundoff_intime")) Then

                    If Not IsDBNull(drTimesheet(0)("checkintime")) AndAlso CDate(drTimesheet(0)("checkintime")) <= xShiftInTime Then    'Early Coming
                        mintearly_coming = DateDiff(DateInterval.Second, CDate(drTimesheet(0)("checkintime")), xShiftInTime)
                    ElseIf Not IsDBNull(drTimesheet(0)("checkintime")) AndAlso CDate(drTimesheet(0)("checkintime")) >= xShiftInTime Then       'Late Coming
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, CDate(drTimesheet(0)("checkintime")))
                    End If


                ElseIf Not IsDBNull(drTimesheet(0)("roundoff_intime")) Then

                    If Not IsDBNull(drTimesheet(0)("roundoff_intime")) AndAlso CDate(drTimesheet(0)("roundoff_intime")) <= xShiftInTime Then    'Early Coming
                        mintearly_coming = DateDiff(DateInterval.Second, CDate(drTimesheet(0)("roundoff_intime")), xShiftInTime)
                    ElseIf Not IsDBNull(drTimesheet(0)("roundoff_intime")) AndAlso CDate(drTimesheet(0)("roundoff_intime")) >= xShiftInTime Then       'Late Coming
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, CDate(drTimesheet(0)("roundoff_intime")))
                    End If

                End If

            End If


            If mdtRoundoffOutTime = Nothing Then

                If mdtCheckouttime <> Nothing AndAlso mdtCheckouttime <= xShiftOutTime Then    'Early Going   --
                    mintearly_going = DateDiff(DateInterval.Second, mdtCheckouttime, xShiftOutTime)
                ElseIf mdtCheckouttime <> Nothing AndAlso mdtCheckouttime >= xShiftOutTime Then       'Late Going  --
                    mintlate_going = DateDiff(DateInterval.Second, xShiftOutTime, mdtCheckouttime)
                End If

            Else
                If mdtRoundoffOutTime <= xShiftOutTime Then    'Early Going   --
                    mintearly_going = DateDiff(DateInterval.Second, mdtRoundoffOutTime, xShiftOutTime)
                ElseIf mdtRoundoffOutTime >= xShiftOutTime Then       'Late Going  --
                    mintlate_going = DateDiff(DateInterval.Second, xShiftOutTime, mdtRoundoffOutTime)
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateEarlyLate_ComingGoing; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function CalculateDayType(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal xEmployeeID As Integer, ByVal xLoginDate As DateTime _
                                                   , ByVal xShiftOutTime As DateTime, ByVal xTotalShiftWorkingHrsInSec As Integer, ByVal xHalffromHrsInSec As Integer, ByVal xHalfToHrsInSec As Integer _
                                                   , ByVal xCalcShorttimeBeforeInSec As Integer, ByVal blnIsOpenShift As Boolean) As Decimal


        Dim mdclDayType As Double = 0
        Try
            Dim dtList As DataSet = GetList(SqlCn, SqlTran, xDatabaseName, "List", True)
            If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(dtList.Tables(0).Compute("MAX(checkouttime)", "1=1")) Then
                    Dim dtLastLogOut As DateTime = dtList.Tables(0).Compute("MAX(checkouttime)", "1=1")

                    If blnIsOpenShift = False Then

                        If dtLastLogOut <> Nothing AndAlso xShiftOutTime <> Nothing Then

                            Dim xTotalHrs As Integer = 0
                            If dtLastLogOut > xShiftOutTime Then
                                xTotalHrs = mintTotalhr - (mintearly_coming + DateDiff(DateInterval.Second, xShiftOutTime, dtLastLogOut))
                            Else
                                If mintearly_coming > mintTotalhr Then
                                    xTotalHrs = mintearly_coming - mintTotalhr
                                Else
                                    xTotalHrs = mintTotalhr - mintearly_coming
                                End If
                            End If

                            If xTotalHrs >= xHalffromHrsInSec AndAlso xTotalHrs <= xHalfToHrsInSec Then
                                mdclDayType = 0.5
                            ElseIf xTotalHrs > xHalffromHrsInSec AndAlso xTotalHrs > xHalfToHrsInSec Then
                                mdclDayType = 1
                            Else
                                mdclDayType = 0
                            End If

                        Else
                            mdclDayType = 0
                        End If

                    ElseIf blnIsOpenShift Then

                        If mintTotalhr >= xHalffromHrsInSec AndAlso mintTotalhr <= xHalfToHrsInSec Then
                            mdclDayType = 0.5
                        ElseIf mintTotalhr > xHalffromHrsInSec AndAlso mintTotalhr > xHalfToHrsInSec Then
                            mdclDayType = 1
                        ElseIf mintTotalhr > 0 AndAlso (xHalffromHrsInSec <= 0 AndAlso xHalfToHrsInSec <= 0) Then
                            mdclDayType = 1
                        ElseIf mintTotalhr <= 0 Then
                            mdclDayType = 0
                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateDayType; Module Name: " & mstrModuleName)
        End Try
        Return mdclDayType
    End Function

#End Region

#Region "Other Methods"

    Public Function GetOTOrder(ByVal sqlCn As SqlConnection, ByVal xCompanyId As Integer, ByVal mblnWorkingDays As Boolean, ByVal mblnweekend As Boolean, ByVal mblnDayoff As Boolean, ByVal mblnHoliday As Boolean) As Dictionary(Of Integer, String)
        Dim arOT1Order() As String = Nothing
        Dim arOT2Order() As String = Nothing
        Dim arOT3Order() As String = Nothing
        Dim arOT4Order() As String = Nothing
        Dim mintOT1No As Integer = 0
        Dim mintOT2No As Integer = 0
        Dim mintOT3No As Integer = 0
        Dim mintOT4No As Integer = 0
        Dim dctOTOrder As New Dictionary(Of Integer, String)
        Dim sqlCmd As SqlCommand = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            Dim StrCols As String = "'OT1_Order','OT2_Order','OT3_Order','OT4_Order'"

            Dim StrQ As String = "SELECT " & _
                                            "     CF.key_name " & _
                                            "    ,CF.key_value " & _
                                            " FROM hrmsConfiguration..cfconfiguration AS CF " & _
                                            " WHERE CF.[key_name] IN (" & StrCols & ") AND CF.companyunkid = '" & xCompanyId & "' " & " AND CF.key_value <> '' "

            sqlCmd = New SqlCommand(StrQ, sqlCn)
            sqlCmd.Parameters.Clear()
            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dtTable = New DataTable
                sqlDataadp.Fill(dtTable)
            End Using

            Dim mstrOT1Order As String = ""
            Dim mstrOT2Order As String = ""
            Dim mstrOT3Order As String = ""
            Dim mstrOT4Order As String = ""
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "OT1_Order").ToList()
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    mstrOT1Order = drRow(0)("OT1_Order").ToString()
                End If

                drRow = Nothing
                drRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "OT2_Order").ToList()
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    mstrOT2Order = drRow(0)("OT2_Order").ToString()
                End If

                drRow = Nothing
                drRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "OT3_Order").ToList()
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    mstrOT3Order = drRow(0)("OT3_Order").ToString()
                End If

                drRow = Nothing
                drRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "OT4_Order").ToList()
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    mstrOT4Order = drRow(0)("OT4_Order").ToString()
                End If

            End If

            arOT1Order = mstrOT1Order.Trim.Split(CChar("|"))
            arOT2Order = mstrOT2Order.Split(CChar("|"))
            arOT3Order = mstrOT3Order.Split(CChar("|"))
            arOT4Order = mstrOT4Order.Split(CChar("|"))

            If mblnWorkingDays Then
                mintOT1No = CInt(arOT1Order(0))
                mintOT2No = CInt(arOT2Order(0))
                mintOT3No = CInt(arOT3Order(0))
                mintOT4No = CInt(arOT4Order(0))

            ElseIf mblnweekend Then
                mintOT1No = CInt(arOT1Order(1))
                mintOT2No = CInt(arOT2Order(1))
                mintOT3No = CInt(arOT3Order(1))
                mintOT4No = CInt(arOT4Order(1))

            ElseIf mblnDayoff Then
                mintOT1No = CInt(arOT1Order(2))
                mintOT2No = CInt(arOT2Order(2))
                mintOT3No = CInt(arOT3Order(2))
                mintOT4No = CInt(arOT4Order(2))

            ElseIf mblnHoliday Then
                mintOT1No = CInt(arOT1Order(3))
                mintOT2No = CInt(arOT2Order(3))
                mintOT3No = CInt(arOT3Order(3))
                mintOT4No = CInt(arOT4Order(3))

            End If

            dctOTOrder.Add(mintOT1No, "1")  '1,2,3,4 ARE OT1,OT2,OT3,OT4
            dctOTOrder.Add(mintOT2No, "2")
            dctOTOrder.Add(mintOT3No, "3")
            dctOTOrder.Add(mintOT4No, "4")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOTOrder; Module Name: " & mstrModuleName)
        Finally
            Array.Clear(arOT1Order, 0, arOT1Order.Length - 1)
            Array.Clear(arOT2Order, 0, arOT2Order.Length - 1)
            Array.Clear(arOT3Order, 0, arOT3Order.Length - 1)
            Array.Clear(arOT4Order, 0, arOT4Order.Length - 1)
            arOT1Order = Nothing
            arOT2Order = Nothing
            arOT3Order = Nothing
            arOT4Order = Nothing
        End Try
        Return dctOTOrder
    End Function

    Public Function InsertDeviceAttendanceData(ByVal sqlCn As SqlConnection, ByVal xDataBaseName As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Try

            Dim sqlCmd As New SqlCommand()
            sqlCmd.Connection = sqlCn


            strQ = " INSERT INTO " & xDataBaseName & "..tnadevice_attendance ( " & _
                      " deviceno " & _
                      ",ipaddress " & _
                      ",enrollno " & _
                      ",logindate " & _
                      ",logintime " & _
                      ",verifymode " & _
                      ",inoutmode " & _
                      ",IsError " & _
                    " ) VALUES (" & _
                      " @deviceno " & _
                      ", @ipaddress " & _
                      ",@enrollno " & _
                      ",@logindate " & _
                      ",@logintime " & _
                      ",@verifymode " & _
                      ",@inoutmode " & _
                      ",@IsError " & _
                      " ); SELECT @@identity"

            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@deviceno", mstrDeviceAtt_DeviceNo.ToString())
            sqlCmd.Parameters.AddWithValue("@ipaddress", mstrDeviceAtt_IPAddress.ToString())
            sqlCmd.Parameters.AddWithValue("@enrollno", mstrDeviceAtt_EnrollNo.ToString())
            sqlCmd.Parameters.AddWithValue("@logindate", IIf(IsDBNull(mdtDeviceAtt_LoginDate) = False, mdtDeviceAtt_LoginDate, DBNull.Value))
            sqlCmd.Parameters.AddWithValue("@logintime", IIf(IsDBNull(mdtDeviceAtt_LoginTime) = False, mdtDeviceAtt_LoginTime, DBNull.Value))
            sqlCmd.Parameters.AddWithValue("@verifymode", mstrDeviceAtt_VerifyMode.ToString())
            sqlCmd.Parameters.AddWithValue("@inoutmode", mstrDeviceAtt_InOutMode.ToString())
            sqlCmd.Parameters.AddWithValue("@IsError", mstrDeviceAtt_IsError.ToString())
            sqlCmd.CommandText = strQ
            sqlCmd.ExecuteScalar()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDeviceAttendanceData; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Public Function GetDeviceAttendanceData(ByVal SqlCn As SqlConnection, ByVal xDataBaseName As String, ByVal mblnMappedUser As Boolean _
                                                              , ByVal mstrFromDate As String, ByVal mstrEndDate As String) As DataTable   ' ByVal mblnIncludeSuspendedEmploye As Boolean
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet = Nothing
        Try

            strQ = " SELECT ISNULL(hremployee_master.employeeunkid,-1) AS EmployeeId " & _
                      ",ISNULL(hremployee_master.employeecode,'') AS EmployeeCode "

            If mblnMappedUser Then
                strQ &= ",  CASE WHEN  ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN ISNULL(hremployee_master.employeecode,'') + CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN  ' - ' ELSE  ' ' END  + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') ELSE  enrollno + @NotMapped END AS EmployeeName "
            Else
                strQ &= ",  CASE WHEN  ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN ISNULL(hremployee_master.employeecode,'') + CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN  ' - ' ELSE  ' ' END  + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') ELSE enrollno + @NotRegistered END AS EmployeeName "
            End If

            strQ &= ", CASE WHEN ISNULL(tnashift_master.shiftunkid,-1) = -1 THEN ISNULL(ts.shiftunkid,-1) ELSE ISNULL(tnashift_master.shiftunkid,-1) END As ShiftId " & _
                        ",CASE WHEN ISNULL(tnashift_master.shiftunkid,-1) = -1 THEN ISNULL(ts.shiftname,'')  ELSE ISNULL(tnashift_master.shiftname,'') END  AS ShiftName " & _
                        ",logindate " & _
                        ",logintime " & _
                        ",enrollno " & _
                        ",verifymode " & _
                        ",inoutmode " & _
                        ",deviceno AS Device " & _
                        ",ipaddress " & _
                        ",CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) <=0 THEN 1 ELSE 0 END iserror " & _
                        ",CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) <=0 THEN '*** NOT REGISTERED ***'  ELSE '' END [Message] " & _
                        " FROM  " & xDataBaseName & "..tnadevice_attendance " & _
                        " LEFT JOIN " & xDataBaseName & "..hrempid_devicemapping ON hrempid_devicemapping.deviceuserid = tnadevice_attendance.enrollno " & _
                        " LEFT JOIN " & xDataBaseName & "..hremployee_master ON hremployee_master.employeeunkid = hrempid_devicemapping.employeeunkid " & _
                        " LEFT JOIN " & _
                        " ( " & _
                                      " SELECT hremployee_shift_tran.shiftunkid " & _
                                      " ,employeeunkid " & _
                                      " ,effectivedate " & _
                                      " , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                      " FROM " & xDataBaseName & "..hremployee_shift_tran " & _
                                      " WHERE isvoid = 0 AND effectivedate IS NOT NULL " & _
                                 " AND CONVERT(CHAR(8), hremployee_shift_tran.effectivedate, 112) <=  @ToDate " & _
                       " ) AS Shf ON CONVERT(CHAR(8),Shf.effectivedate,112) <= CONVERT(char(8),tnadevice_attendance.logindate,112) AND Shf.employeeunkid = hremployee_master.employeeunkid " & _
                       " AND shf.rno = 1 " & _
                       " LEFT JOIN " & xDataBaseName & "..tnashift_master ON tnashift_master.shiftunkid = shf.shiftunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                                     " SELECT hremployee_shift_tran.shiftunkid " & _
                                     " ,employeeunkid " & _
                                     " ,effectivedate " & _
                                     " , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                     " FROM " & xDataBaseName & "..hremployee_shift_tran " & _
                                     " WHERE isvoid = 0 AND effectivedate IS NOT NULL " & _
                                     " AND CONVERT(CHAR(8), hremployee_shift_tran.effectivedate, 112) <=  @FromDate " & _
                      " ) AS Shf1 ON CONVERT(CHAR(8),Shf1.effectivedate,112) <= CONVERT(char(8),tnadevice_attendance.logindate,112) AND Shf1.employeeunkid = hremployee_master.employeeunkid " & _
                      " AND Shf1.rno = 1 " & _
                      " LEFT JOIN " & xDataBaseName & "..tnashift_master AS ts ON ts.shiftunkid = shf1.shiftunkid " & _
                      " WHERE hrempid_devicemapping.isactive = 1 " & _
                      " ORDER BY ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,''),logintime "

            Dim sqlCmd As New SqlCommand(strQ, SqlCn)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@NotMapped", "*** NOT MAPPED ***")
            sqlCmd.Parameters.AddWithValue("@NotRegistered", "*** NOT REGISTERED ***")
            sqlCmd.Parameters.AddWithValue("@FromDate", mstrFromDate)
            sqlCmd.Parameters.AddWithValue("@ToDate", mstrEndDate)

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using


            dtTable = dsList.Tables(0).Copy()

            'If mblnIncludeSuspendedEmploye = False Then
            '    Dim objEmpDates As New clsemployee_dates_tran
            '    Dim dtSuspendedEmp As DataTable = objEmpDates.GetEmpSuspentionProbationDetailDates(0, mdtFromDate, mdtToDate)
            '    objEmpDates = Nothing

            '    If dtSuspendedEmp IsNot Nothing AndAlso dtSuspendedEmp.Rows.Count > 0 Then
            '        Dim drRow As IEnumerable(Of DataRow) = From EmpLoginList In dtTable Join EmpSuspended In dtSuspendedEmp On EmpSuspended.Field(Of Integer)("employeeunkid") Equals EmpLoginList.Field(Of Integer)("EmployeeId") And EmpSuspended.Field(Of DateTime)("Datevalue") Equals EmpLoginList.Field(Of DateTime)("logindate") Select EmpLoginList
            '        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
            '            drRow.ToList.ForEach(Function(x) DeleteSuspendedEmpDate(dtTable, x))
            '            dtTable.AcceptChanges()
            '        End If
            '    End If
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDeviceAttendanceData; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    Public Function DeleteDeviceAttendanceData(ByVal sqlCn As SqlConnection, ByVal xDataBaseName As String, Optional ByVal mdtDate As Date = Nothing) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            Dim sqlCmd As New SqlCommand()
            sqlCmd.Connection = sqlCn
            sqlCmd.Parameters.Clear()

            If mdtDate <> Nothing AndAlso IsDBNull(mdtDate) = False Then
                strQ = "DELETE FROM " & xDataBaseName & "..tnadevice_attendance WHERE CONVERT(CHAR(8),logidate,112) = @logidate "
            Else
                strQ = "TRUNCATE TABLE " & xDataBaseName & "..tnadevice_attendance"
            End If

            If mdtDate <> Nothing AndAlso IsDBNull(mdtDate) = False Then
                sqlCmd.Parameters.AddWithValue("@logidate", convertDate(mdtDate.Date))
            End If

            sqlCmd.CommandText = strQ
            sqlCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteDeviceAttendanceData; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'Public Function DeleteSuspendedEmpDate(ByVal xTable As DataTable, ByVal drRow As DataRow) As Boolean
    '    Try
    '        If xTable IsNot Nothing AndAlso xTable.Rows.Count > 0 Then
    '            xTable.Rows.Remove(drRow)
    '            xTable.AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: DeleteSuspendedEmpDate; Module Name: " & mstrModuleName)
    '    End Try
    'End Function

    Public Function IsDayOffExist(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String _
                                            , ByVal mdtDate As Date, ByVal intEmpId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim sqlCmd As SqlCommand = Nothing

        Try
            strQ = "SELECT " & _
                      "  empdayofftranunkid " & _
                      ", dayoffdate " & _
                      ", employeeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM " & xDatabaseName & "..hremployee_dayoff_tran " & _
                      " WHERE CONVERT(CHAR(8),dayoffdate,112) = @dayoffdate " & _
                      " AND employeeunkid = @employeeunkid AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND empdayofftranunkid <> @empdayofftranunkid"
            End If

            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@dayoffdate", convertDate(mdtDate.Date))
            sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmpId)
            sqlCmd.Parameters.AddWithValue("@empdayofftranunkid", intUnkid)

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDayOffExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function GetEmployeeHolidayList(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String _
                                                            , ByVal xEmployeeID As Integer, ByVal xStartDate As DateTime, ByVal xEndDate As DateTime) As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim sqlCmd As SqlCommand = Nothing
        Dim dsList As DataSet = Nothing
        Try
            strQ = " SELECT " & _
                      " lvemployee_holiday.employeeunkid   " & _
                      ",CONVERT(char(8),lvholiday_master.holidaydate,112) AS Date " & _
                      ",lvholiday_master.holidayname " & _
                      " FROM " & xDatabaseName & "..lvemployee_holiday " & _
                      " JOIN " & xDatabaseName & "..lvholiday_master on lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                      " WHERE lvemployee_holiday.employeeunkid = @employeeunkid AND CONVERT(CHAR(8),lvholiday_master.holidaydate,112) BETWEEN @startDate AND @EndDate"


            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@employeeunkid", xEmployeeID.ToString())
            sqlCmd.Parameters.AddWithValue("@StartDate", convertDate(xStartDate.Date))
            sqlCmd.Parameters.AddWithValue("@EndDate", convertDate(xEndDate.Date))

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            Return dtList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeHolidayList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Function

    Public Function GetPolicyTranID(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String _
                                                , ByVal Effectivedate As DateTime, ByVal intEmployeeID As Integer) As Integer
        Dim mintPolicyTranId As Integer = -1
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim sqlCmd As SqlCommand = Nothing

        Try
            strQ = "SELECT " & _
                       " top 1 " & _
                       "  emppolicytranunkid " & _
                       ", employeeunkid " & _
                       ", policyunkid " & _
                       ", effectivedate " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       " FROM " & xDatabaseName & "..hremployee_policy_tran " & _
                       " WHERE employeeunkid = @employeeunkid " & _
                       " AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                       " AND isvoid = 0 ORder by effectivedate desc "

            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@effectivedate", Effectivedate.ToString("yyyMMdd"))
            sqlCmd.Parameters.AddWithValue("@employeeunkid", intEmployeeID)
            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintPolicyTranId = CInt(dsList.Tables(0).Rows(0)("policyunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPolicyTranID; Module Name: " & mstrModuleName)
        End Try
        Return mintPolicyTranId
    End Function

    Public Function InsertAuditTrailLogin(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, ByVal AuditType As Integer, ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim sqlCmd As SqlCommand = Nothing

        Try

            strQ = "INSERT INTO " & xDatabaseName & "..attnalogin_tran ( " & _
                      "  loginunkid " & _
                      ", employeeunkid " & _
                      ", logindate " & _
                      ", checkintime " & _
                      ", checkouttime " & _
                      ", holdunkid " & _
                      ", workhour " & _
                      ", breakhr " & _
                      ", remark " & _
                      ", inouttype " & _
                      ", sourcetype " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                      ", operationtype " & _
                      ", original_intime " & _
                      ", original_outtime " & _
                      ", isgraceroundoff " & _
                      ", manualroundofftypeid " & _
                      ", roundminutes " & _
                      ", roundlimit " & _
                      ", teahr " & _
                      ", loginemployeeunkid " & _
                      " ) VALUES (" & _
                      "  @loginunkid " & _
                      ", @employeeunkid " & _
                      ", @logindate " & _
                      ", @checkintime " & _
                      ", @checkouttime " & _
                      ", @holdunkid " & _
                      ", @workhour " & _
                      ", @breakhr " & _
                      ", @remark " & _
                      ", @inouttype " & _
                      ", @sourcetype " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                      ", @operationtype " & _
                      ", @original_intime " & _
                      ", @original_outtime " & _
                      ", @isgraceroundoff " & _
                      ", @manualroundofftypeid " & _
                      ", @roundminutes " & _
                      ", @roundlimit " & _
                      ", @teahr " & _
                      ", @loginemployeeunkid " & _
                      " ); SELECT @@identity"


            mstrWebClientIP = getIP()
            mstrWebHostName = getHostName()

            sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@loginunkid", mintLoginunkid.ToString)

            sqlCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeunkid.ToString)
            sqlCmd.Parameters.AddWithValue("@logindate", mdtLogindate)

            If blnDonotAttendanceinSeconds Then
                sqlCmd.Parameters.AddWithValue("@checkintime", IIf(mdtcheckintime <> Nothing, CDate(mdtcheckintime.ToShortDateString & " " & mdtcheckintime.ToString("HH:mm")), DBNull.Value))
                sqlCmd.Parameters.AddWithValue("@checkouttime", IIf(mdtCheckouttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))
            Else
                sqlCmd.Parameters.AddWithValue("@checkintime", IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                sqlCmd.Parameters.AddWithValue("@checkouttime", IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            sqlCmd.Parameters.AddWithValue("@original_intime", IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
            sqlCmd.Parameters.AddWithValue("@original_outtime", IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))
            sqlCmd.Parameters.AddWithValue("@holdunkid", mintHoldunkid.ToString)
            sqlCmd.Parameters.AddWithValue("@workhour", mintWorkhour.ToString)
            sqlCmd.Parameters.AddWithValue("@breakhr", mintBreakhr.ToString)
            sqlCmd.Parameters.AddWithValue("@remark", mstrRemark.ToString)
            sqlCmd.Parameters.AddWithValue("@inouttype", mintInOuType.ToString)
            sqlCmd.Parameters.AddWithValue("@sourcetype", mintSourceType.ToString)
            sqlCmd.Parameters.AddWithValue("@audittype", AuditType.ToString)
            sqlCmd.Parameters.AddWithValue("@audituserunkid", mintUserunkid)
            sqlCmd.Parameters.AddWithValue("@auditdatetime", Now)
            sqlCmd.Parameters.AddWithValue("@isgraceroundoff", mblnIsgraceroundoff)
            sqlCmd.Parameters.AddWithValue("@manualroundofftypeid", mintManualroundofftypeid)
            sqlCmd.Parameters.AddWithValue("@roundminutes", mintRoundminutes)
            sqlCmd.Parameters.AddWithValue("@roundlimit", mintRoundlimit)
            sqlCmd.Parameters.AddWithValue("@ip", mstrWebClientIP)
            sqlCmd.Parameters.AddWithValue("@machine_name", mstrWebHostName)
            sqlCmd.Parameters.AddWithValue("@form_name", "ArutiAttendance")
            sqlCmd.Parameters.AddWithValue("@isweb", False)
            sqlCmd.Parameters.AddWithValue("@module_name1", "")
            sqlCmd.Parameters.AddWithValue("@module_name2", "")
            sqlCmd.Parameters.AddWithValue("@module_name3", "")
            sqlCmd.Parameters.AddWithValue("@module_name4", "")
            sqlCmd.Parameters.AddWithValue("@module_name5", "")
            sqlCmd.Parameters.AddWithValue("@operationtype", mintOperationType)
            sqlCmd.Parameters.AddWithValue("@teahr", mintTeaHr.ToString)
            sqlCmd.Parameters.AddWithValue("@loginemployeeunkid", mintLogEmployeeUnkid.ToString)
            sqlCmd.ExecuteNonQuery()
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailLogin; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

#End Region

End Class