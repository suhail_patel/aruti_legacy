﻿#Region " Import "

Imports System
Imports System.IO
Imports System.Timers
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.ServiceProcess
Imports System.Security.Cryptography
Imports System.Text
Imports System.Collections.Specialized
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Threading
Imports System.Runtime.InteropServices

#End Region


Public Class ArutiAttendance

#Region "Private Variable"
    Private timer As New System.Timers.Timer()
    Dim sqlCn As SqlConnection
    Dim sqlCmd As SqlCommand
    Private mintDatabaseVersion As Integer = 57
    Private mblnIsDatabaseAccessible As Boolean = True
    Dim strIp As String = ""
    Dim intPort As Integer = 0
    Dim mintMachineSrNo As Integer = 0
    Dim mstrDeviceCode As String = ""
    Dim intDeviceType As Integer = -1
    Dim mstrCommunicationKey As String = ""
    Private mstrUserID As String = ""
    Private mstrPassword As String = ""
    Private mstrDeviceModel As String = ""
    Private mstrSendTnAEarlyLateReportsUserIds As String = ""
    Private mstrTnAFailureNotificationUserIds As String = ""
    Dim t As Thread
    Dim mstrError As String = ""

    Dim anviz_handle As IntPtr

    <DllImport("Kernel32.dll")> _
 Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.CLOCKINGRECORD, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.PERSONINFO, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.PERSONINFOEX, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As Integer, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As Byte, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Sub GetLocalTime(ByRef lpSystemTime As clsAnviz.SYSTEMTIME)
    End Sub

    Private ReaderNo As Integer
    Private ReaderIpAddress As String
    Private clocking As New clsAnviz.CLOCKINGRECORD()

    Private Structure SYSTEMTIME
        Dim wYear As Short
        Dim wMonth As Short
        Dim wDayOfWeek As Short
        Dim wDay As Short
        Dim wHour As Short
        Dim wMinute As Short
        Dim wSecond As Short
        Dim wMilliseconds As Short
    End Structure
    Dim IDNumber As Int32

    Private Declare Sub GetLocalTime Lib "kernel32" (ByRef lpSystemTime As SYSTEMTIME)

#End Region

#Region "Service Method"

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            'System.Diagnostics.Debugger.Launch()
            AddHandler timer.Elapsed, AddressOf OnElapsedTime
            timer.Interval = 60000
            timer.Enabled = True
        Catch ex As Exception
            WriteLog("OnStart:- " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            timer.Enabled = False
        Catch ex As Exception
            WriteLog("OnStop:- " & ex.Message)
        End Try
    End Sub

    Private Sub OnElapsedTime(ByVal source As Object, ByVal e As ElapsedEventArgs)
        Dim dtCompany As DataTable = Nothing
        Dim xCompanyId As Integer = 0
        Dim xYearId As Integer = 0
        Dim objLogin As New clslogin_Tran()
        Try
            'System.Diagnostics.Debugger.Launch()
            timer.Enabled = False
            If IsConnect() = False Then Exit Sub
            dtCompany = IsArutiAttendanceConfigured()

            If IsDBNull(dtCompany) = False AndAlso dtCompany.Rows.Count > 0 Then

                For Each dr As DataRow In dtCompany.Rows
                    xCompanyId = Convert.ToInt32(dr("companyunkid"))
                    xYearId = Convert.ToInt32(dr("yearunkid"))
                    mblnIsDatabaseAccessible = False
                    IsDatabaseAccessible(dr("database_name").ToString())
                    If mblnIsDatabaseAccessible = False Then Continue For
                    Dim dtParams As New DataTable()
                    dtParams = GetAttendanceParameters(Convert.ToInt32(dr("companyunkid")))

                    If IsDBNull(dtParams) = False AndAlso dtParams.Rows.Count > 0 Then

                        Dim objSendmail As New clsSendMail
                        Dim dtCompanyDetails As DataTable = GetCompanyDetails(xCompanyId)

                        If dtCompanyDetails IsNot Nothing AndAlso dtCompanyDetails.Rows.Count > 0 Then
                            objSendmail._SenderAddress = dtCompanyDetails.Rows(0)("senderaddress").ToString()
                            objSendmail._MailserverIP = dtCompanyDetails.Rows(0)("mailserverip").ToString()
                            objSendmail._MailserverPort = dtCompanyDetails.Rows(0)("mailserverport").ToString()
                            objSendmail._UserName = dtCompanyDetails.Rows(0)("username").ToString()
                            objSendmail._Password = dtCompanyDetails.Rows(0)("password").ToString()
                            objSendmail._Isloginssl = CBool(dtCompanyDetails.Rows(0)("isloginssl"))
                        End If

                        Dim drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "SendTnAEarlyLateReportsUserIds").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                            mstrSendTnAEarlyLateReportsUserIds = drRow(0)("key_value").ToString()
                        End If

                        drRow = Nothing
                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "TnAFailureNotificationUserIds").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                            mstrTnAFailureNotificationUserIds = drRow(0)("key_value").ToString()
                        End If

                        drRow = Nothing
                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "RunDailyAttendanceServiceTime").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

                            mstrError = ""

                            If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

                                Dim path As String = AppDomain.CurrentDomain.BaseDirectory + "\DeviceSetting.xml"

                                If System.IO.File.Exists(path) Then

                                    Dim dsMachineSetting As New DataSet()
                                    dsMachineSetting.ReadXml(path)

                                    If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then
                                        Dim drMachineDetail = dsMachineSetting.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("companyunkid") = xCompanyId.ToString()).ToList()

                                        If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

                                            'If objLogin.DeleteDeviceAttendanceData(sqlCn, dr("database_name").ToString()) = False Then
                                            '    Dim mstrError As String = objLogin._Message
                                            '    SendFailedNotificationToUser(dr("database_name").ToString(), xCompanyId, False, mstrError, objSendmail)
                                            '    Continue For
                                            'End If

                                            For i As Integer = 0 To drMachineDetail.Count - 1

                                                intDeviceType = CInt(drMachineDetail(i)("commdeviceid").ToString())

                                                mstrDeviceCode = CStr(drMachineDetail(i)("devicecode").ToString()).Trim.Remove(drMachineDetail(i)("devicecode").ToString().IndexOf("||"), drMachineDetail(i)("devicecode").ToString().Trim.Length - drMachineDetail(i)("devicecode").ToString().IndexOf("||"))

                                                strIp = drMachineDetail(i)("ip").ToString()

                                                If IsDBNull(drMachineDetail(i)("port")) = False AndAlso drMachineDetail(i)("port").ToString() <> "" Then
                                                    intPort = CInt(drMachineDetail(i)("port").ToString())
                                                End If

                                                mintMachineSrNo = CInt(drMachineDetail(i)("machinesrno").ToString())

                                                If dsMachineSetting.Tables(0).Columns.Contains("commkey") Then
                                                    If IsDBNull(drMachineDetail(i)("commkey")) = False AndAlso drMachineDetail(i)("commkey").ToString.Trim.Length > 0 Then
                                                        mstrCommunicationKey = drMachineDetail(i)("commkey").ToString()
                                                    End If
                                                End If

                                                If dsMachineSetting.Tables(0).Columns.Contains("userid") Then
                                                    If IsDBNull(drMachineDetail(i)("userid")) = False AndAlso drMachineDetail(i)("userid").ToString.Trim.Length > 0 Then
                                                        mstrUserID = drMachineDetail(i)("userid").ToString()
                                                    End If
                                                End If

                                                If dsMachineSetting.Tables(0).Columns.Contains("password") Then
                                                    If IsDBNull(drMachineDetail(i)("password")) = False AndAlso drMachineDetail(i)("password").ToString.Trim.Length > 0 Then
                                                        mstrPassword = drMachineDetail(i)("password").ToString()
                                                    End If
                                                End If

                                                If dsMachineSetting.Tables(0).Columns.Contains("devicemodel") Then
                                                    If IsDBNull(drMachineDetail(i)("devicemodel")) = False AndAlso drMachineDetail(i)("devicemodel").ToString.Trim.Length > 0 Then
                                                        mstrDeviceModel = drMachineDetail(i)("devicemodel").ToString()
                                                    End If
                                                End If


                                                If intDeviceType <> enFingerPrintDevice.Handpunch AndAlso intDeviceType <> enFingerPrintDevice.SAGEM Then
                                                    Dim Parameters = New Object() {dr("database_name").ToString(), xCompanyId, objSendmail}
                                                    t = New Thread(AddressOf DeviceIntegration)
                                                    t.Start(Parameters)
                                                    t.Join()


                                                ElseIf intDeviceType = enFingerPrintDevice.Handpunch Then
                                                    'DownloadHandpunch_Data()
                                                    Exit For

                                                ElseIf intDeviceType = enFingerPrintDevice.SAGEM Then
                                                    'DownloadSAGEM_Data()
                                                    Exit For

                                                End If

                                                If mstrError.Trim.Length > 0 Then
                                                    objLogin.DeleteDeviceAttendanceData(sqlCn, dr("database_name").ToString(), Date.Now.AddDays(-1).Date)
                                                    mstrError = ""
                                                    Exit For
                                                End If

                                            Next


                                        End If  'If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

                                    End If  'If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                                Else
                                    mstrError = "Device setting file does not exist on application path.Please put device setting file on application folder."
                                    SendFailedNotificationToUser(dr("database_name").ToString(), xCompanyId, False, mstrError, objSendmail)
                                    Continue For
                                End If  'If System.IO.File.Exists(path) Then


                                'Dim mstrUserAccessModeSetting As String = ""
                                'Dim mblnFirstCheckInLastCheckOut As Boolean = False
                                'Dim mblnPolicyManagementTNA As Boolean = False
                                'Dim mblnDonotAttendanceinSeconds As Boolean = False
                                'Dim mblnIsHolidayConsiderOnWeekend As Boolean = False
                                'Dim mblnIsDayOffConsiderOnWeekend As Boolean = False
                                'Dim mblnIsHolidayConsiderOnDayoff As Boolean = False

                                'Dim drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "UserAccessModeSetting").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mstrUserAccessModeSetting = drOption(0)("key_value").ToString()
                                'End If

                                'drOption = Nothing
                                'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "FirstCheckInLastCheckOut").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mblnFirstCheckInLastCheckOut = CBool(drOption(0)("key_value"))
                                'End If

                                'drOption = Nothing
                                'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "PolicyManagementTNA").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mblnPolicyManagementTNA = CBool(drOption(0)("key_value"))
                                'End If

                                'drOption = Nothing
                                'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "DonotAttendanceinSeconds").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mblnDonotAttendanceinSeconds = CBool(drOption(0)("key_value"))
                                'End If

                                'drOption = Nothing
                                'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnWeekend").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mblnIsHolidayConsiderOnWeekend = CBool(drOption(0)("key_value"))
                                'End If

                                'drOption = Nothing
                                'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsDayOffConsiderOnWeekend").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mblnIsDayOffConsiderOnWeekend = CBool(drOption(0)("key_value"))
                                'End If

                                'drOption = Nothing
                                'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnDayoff").ToList()
                                'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
                                '    mblnIsHolidayConsiderOnDayoff = CBool(drOption(0)("key_value"))
                                'End If


                                '' START TO SAVE DATA FROM TEMP TABLE TO MAIN TABLE
                                'Dim dtTable As DataTable = objLogin.GetDeviceAttendanceData(sqlCn, dr("database_name").ToString(), True, convertDate(Now.Date.AddDays(-1).Date), convertDate(Now.Date.AddDays(-1).Date))

                                'If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                                '    SaveAttendanceData(dr("database_name").ToString(), xCompanyId, xYearId, mstrUserAccessModeSetting, mblnFirstCheckInLastCheckOut _
                                '                                  , mblnPolicyManagementTNA, CBool(dr("DonotAttendanceinSeconds")), mblnIsHolidayConsiderOnWeekend _
                                '                                  , mblnIsDayOffConsiderOnWeekend, mblnIsHolidayConsiderOnDayoff, dtTable)
                                'End If
                                '' END TO SAVE DATA FROM TEMP TABLE TO MAIN TABLE

                                Thread.Sleep(60000)

                            End If  ' If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

                        End If  'If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

                    End If ' If IsDBNull(dtParams) = False AndAlso dtParams.Rows.Count > 0 Then

                Next

            End If
        Catch ex As Exception
            WriteLog("OnElapsedTime:- " & ex.Message)
        Finally
            t = Nothing
            objLogin = Nothing
            If sqlCn.State = ConnectionState.Open Or sqlCn.State = ConnectionState.Broken Then
                sqlCn.Close()
            End If
            timer.Start()
        End Try
    End Sub

#End Region

#Region "Private Method"

    Private Function IsConnect() As Boolean
        Try
            sqlCn = New SqlConnection("Data Source=.\Apayroll;Initial Catalog=hrmsConfiguration;User ID=sa;Password=pRofessionalaRuti999")
            sqlCn.Open()
            sqlCmd = New SqlCommand
            sqlCmd.CommandTimeout = 0
            sqlCmd.Connection = sqlCn
            GetDatabaseVersion()
            Return True
        Catch ex As Exception
            Try
                sqlCn = New SqlConnection("Data Source=.\Apayroll;Initial Catalog=hrmsConfiguration;User ID=aruti_sa;Password=pRofessionalaRuti999")
                sqlCn.Open()
                sqlCmd = New SqlCommand
                sqlCmd.CommandTimeout = 0
                sqlCmd.Connection = sqlCn
                GetDatabaseVersion()
                Return True
            Catch ex1 As Exception
                WriteLog("IsConnect:- " & ex1.Message)
            End Try

            WriteLog("IsConnect : " & ex.Message)
        End Try
    End Function

    Private Sub GetDatabaseVersion()
        Dim strQ As String = ""
        Try
            strQ = "DECLARE @tbl_version TABLE (dbversion NVARCHAR(100)) " & _
                   "DECLARE @SP_CHECK_VERSION AS NVARCHAR(MAX) " & _
                   "    INSERT INTO @tbl_version (dbversion) EXEC version " & _
                   "    SET @SP_CHECK_VERSION = (SELECT dbversion FROM @tbl_version) " & _
                   "DECLARE @POS_START INT SET @POS_START = 1 " & _
                   "DECLARE @POS_END INT SET @POS_END = CHARINDEX('.', @SP_CHECK_VERSION, @POS_START) " & _
                   "DECLARE @COLUMN AS INT SET @COLUMN = 3 " & _
                   "   WHILE (@COLUMN > 1 AND @POS_END > 0) " & _
                   "      BEGIN " & _
                   "         SET @POS_START = @POS_END + 1 " & _
                   "         SET @POS_END = CHARINDEX('.', @SP_CHECK_VERSION, @POS_START) " & _
                   "         SET @COLUMN = @COLUMN - 1 " & _
                   "      END " & _
                   "   IF @COLUMN > 1  SET @POS_START = LEN(@SP_CHECK_VERSION) + 1 " & _
                   "   IF @POS_END = 0 SET @POS_END = LEN(@SP_CHECK_VERSION) + 1 " & _
                   "SELECT SUBSTRING (@SP_CHECK_VERSION, @POS_START, @POS_END - @POS_START) "

            Dim Qcmd As New SqlCommand(strQ, sqlCn)
            mintDatabaseVersion = CInt(Qcmd.ExecuteScalar())
        Catch ex As Exception
            WriteLog("GetDatabaseVersion:- " & ex.Message)
        End Try
    End Sub

    Private Function GetCompanyDetails(ByVal xCompanyId As Integer) As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Try

            strQ = " SELECT " & _
                      " cfcompany_master.companyunkid " & _
                      ",senderaddress " & _
                      ",sendername " & _
                      ",mailserverip " & _
                      ",mailserverport " & _
                      ",username " & _
                      ",password  " & _
                      ",isloginssl " & _
                      ",cffinancial_year_tran.yearunkid " & _
                      ",cffinancial_year_tran.database_name " & _
                      ",cffinancial_year_tran.financialyear_name " & _
                      ",cffinancial_year_tran.start_date  " & _
                      ",cffinancial_year_tran.end_date " & _
                      " FROM hrmsconfiguration..cfcompany_master " & _
                      " JOIN hrmsconfiguration..cffinancial_year_tran ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                      " WHERE isactive = 1 AND cfcompany_master.companyunkid = @companyunkid "


            Dim sqlDataadp As New SqlDataAdapter(strQ, sqlCn)
            sqlDataadp.SelectCommand.Parameters.AddWithValue("@companyunkid", xCompanyId)
            dtList = New DataTable
            sqlDataadp.Fill(dtList)
        Catch ex As Exception
            WriteLog("GetCompanyDetails:- " & ex.Message)
            Return Nothing
        End Try
        Return dtList
    End Function

    Private Function IsArutiAttendanceConfigured() As DataTable
        Dim dt As New DataTable()
        Dim StrQ As String = ""
        Try
            StrQ = " SELECT " & _
                       "     CF.companyunkid " & _
                       "    ,FT.database_name " & _
                       "    ,FT.yearunkid " & _
                       " FROM hrmsConfiguration..cfconfiguration AS CF " & _
                       " JOIN hrmsConfiguration..cffinancial_year_tran AS FT ON FT.companyunkid = CF.companyunkid " & _
                       " WHERE UPPER(CF.[key_name]) = 'RUNDAILYATTENDANCESERVICETIME'  AND (CF.key_value <> '00:00:00' AND CF.key_value <> '00:00' AND CF.key_value <> '')  AND FT.isclosed = 0 "

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If
            sqlCmd.Parameters.Clear()
            sqlCmd.CommandText = StrQ

            Dim da As New SqlDataAdapter(sqlCmd)
            da.Fill(dt)
        Catch ex As SqlException
            WriteLog("IsArutiAttendanceConfigured : " & ex.Message)
        Catch ex As Exception
            WriteLog("IsArutiAttendanceConfigured : " & ex.Message)
        End Try
        Return dt
    End Function

    Private Sub IsDatabaseAccessible(ByVal strDBName As String)
        Dim StrQ As String = ""
        Dim intDBId As Integer = 0
        If sqlCmd Is Nothing Then
            sqlCmd = New SqlCommand()
            sqlCmd.Connection = sqlCn
        End If
        Try
            StrQ = "SELECT database_id " & "FROM sys.databases " & "WHERE name = '" & strDBName & "' AND user_access_desc = 'MULTI_USER' "
            sqlCmd.Parameters.Clear()
            sqlCmd.CommandText = StrQ

            intDBId = CInt(sqlCmd.ExecuteScalar())
            If intDBId > 0 Then
                mblnIsDatabaseAccessible = True
            End If

        Catch ex As Exception
            WriteLog("IsDatabaseAccessible " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Private Function GetAttendanceParameters(ByVal xCompanyId As Integer) As DataTable
        Dim dt As New DataTable()
        Dim StrQ As String = ""
        Dim StrCols As String = ""
        Try
            StrCols = "'USERACCESSMODESETTING', 'FIRSTCHECKINLASTCHECKOUT', 'POLICYMANAGEMENTTNA', 'DONOTATTENDANCEINSECONDS', 'ISHOLIDAYCONSIDERONWEEKEND', 'ISDAYOFFCONSIDERONWEEKEND','ISHOLIDAYCONSIDERONDAYOFF', " & _
                           "'RUNDAILYATTENDANCESERVICETIME','SENDTNAEARLYLATEREPORTSUSERIDS','TNAFAILURENOTIFICATIONUSERIDS'"

            StrQ = "SELECT " & _
                       "     CF.key_name " & _
                       "    ,CF.key_value " & _
                       " FROM hrmsConfiguration..cfconfiguration AS CF " & _
                       " WHERE CF.[key_name] IN (" & StrCols & ") AND CF.companyunkid = '" & xCompanyId & "' " & " AND CF.key_value <> '' "

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If
            sqlCmd.Parameters.Clear()
            sqlCmd.CommandText = StrQ
            Dim da As New SqlDataAdapter(sqlCmd)
            da.Fill(dt)
        Catch ex As Exception
            WriteLog("GetAttendanceParameters " & Date.Now & " : " & ex.Message)
        End Try
        Return dt
    End Function

    Public Function GetConfigKeyValue(ByVal intCompanyUnkId As Integer, ByVal strKeyName As String, Optional ByVal sqlcmd As SqlCommand = Nothing) As String
        Dim strQ As String = ""
        Dim strKeyValue As String = ""
        Dim oCmd As SqlCommand = Nothing
        Try

            strQ = " SELECT  " & _
                      " key_value " & _
                      " FROM hrmsConfiguration..cfconfiguration " & _
                      " WHERE key_name = @key_name "

            If sqlcmd Is Nothing Then
                oCmd = New SqlCommand(strQ, sqlCn)
            Else
                oCmd = sqlcmd
            End If

            oCmd.Parameters.Clear()

            If intCompanyUnkId > 0 Then
                strQ &= " AND companyunkid =  @CompanyId "
                oCmd.Parameters.AddWithValue("@CompanyId", intCompanyUnkId)
            Else
                strQ &= " AND companyunkid IS NULL "
            End If

            oCmd.Parameters.AddWithValue("@key_name", strKeyName)
            oCmd.CommandText = strQ

            strKeyValue = oCmd.ExecuteScalar()

        Catch ex As Exception
            WriteLog("GetConfigKeyValue : " & ex.Message)
        End Try
        Return strKeyValue
    End Function

    Public Sub DeviceIntegration(ByVal objParameters As Object())
        Try
            Dim xDatabaseName As String = ""
            Dim xCompanyId As Integer = 0
            Dim objSendmail As clsSendMail = Nothing

            If objParameters IsNot Nothing Then
                xDatabaseName = objParameters(0).ToString()
                xCompanyId = CInt(objParameters(1))
                objSendmail = CType(objParameters(2), clsSendMail)
            End If


            If intDeviceType = Convert.ToInt16(enFingerPrintDevice.ZKSoftware) Then
                'DownloadZkData()    ;

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.BioStar) Then
                'DownloadBioStarData();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.ZKAcessControl) Then
                'DownloadZkAccessControlData()    ;

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.Anviz) Then
                DownloadAnvizData(xDatabaseName.ToString(), CInt(xCompanyId), objSendmail)

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.ACTAtek) Then
                'DownloadACTAtekData();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.BioStar2) Then
                'DownloadBioStar2Data();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.FingerTec) Then
                'DownloadFingerTecData();   

            End If
        Catch ex As Exception
            WriteLog("DeviceIntegration " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Private Sub DownloadAnvizData(ByVal xDatabaseName As String, ByVal xCompanyId As Integer, ByVal objSendmail As clsSendMail)
        Try
            Dim mintErrorNo As Integer = 0

            mintErrorNo = clsAnviz.CKT_RegisterNet(mintMachineSrNo, strIp.Trim())

            If mintErrorNo <> clsAnviz.CKT_RESULT_OK Then
                If mintErrorNo = clsAnviz.CKT_ERROR_INVPARAM Then
                    mstrError = "Invalid Parameter." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_NETDAEMONREADY Then
                    mstrError = "Error in Netdaemonreday." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_CHECKSUMERR Then
                    mstrError = "Checksum Error." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_MEMORYFULL Then
                    mstrError = "Memory Full." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_INVFILENAME Then
                    mstrError = "Invalid File Name." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_FILECANNOTOPEN Then
                    mstrError = "File Cannot Open." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_FILECONTENTBAD Then
                    mstrError = "File Content Bad." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_FILECANNOTCREATED Then
                    mstrError = "File Cannot Create." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_NOTHISPERSON Then
                    mstrError = "Not this Person." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                Else
                    mstrError = "Unable to connect the device." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub
                End If

            End If


            Dim i As Integer = 0
            Dim RecordCount As New Integer()
            Dim RetCount As New Integer()
            Dim pClockings As New Integer()
            Dim pLongRun As New Integer()
            Dim ptemp As Integer = 0
            Dim tempptr As Integer = 0
            Dim ret As Integer = 0
            Dim j As Integer = 0


            If clsAnviz.CKT_GetClockingNewRecordEx(mintMachineSrNo, pLongRun) = 1 Then
                Do
                    ret = clsAnviz.CKT_GetClockingRecordProgress(pLongRun, RecordCount, RetCount, pClockings)

                    If ret <> 0 Then
                        ptemp = Marshal.SizeOf(clocking)
                        tempptr = pClockings
                        For i = 0 To RetCount - 1
                            RtlMoveMemory(clocking, pClockings, ptemp)
                            pClockings = pClockings + ptemp
                            If clocking.PersonID < 0 Then
                                Continue For
                            End If

                            Dim mstrTime As String = Encoding.Default.GetString(clocking.Time).ToString()

                            If Convert.ToInt32(convertDate(Convert.ToDateTime(mstrTime).Date)) >= Convert.ToInt32(convertDate(Date.Now.AddDays(-1).Date)) AndAlso Convert.ToInt32(convertDate(Convert.ToDateTime(mstrTime))) <= Convert.ToInt32(convertDate(Date.Now.AddDays(-1).Date)) Then
                                If InsertDeviceData(xDatabaseName, mstrDeviceCode, strIp, clocking.PersonID.ToString(), Convert.ToDateTime(mstrTime).Date, Convert.ToDateTime(mstrTime), "0", "-1", mstrError) = False Then
                                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, False, mstrError, objSendmail)
                                    Exit Sub
                                End If
                            Else
                                Continue For
                            End If
                        Next

                        If tempptr <> 0 Then
                            clsAnviz.CKT_FreeMemory(tempptr)
                        End If

                        If ret = 1 Then
                            Exit Do
                        End If

                    End If

                Loop

            End If
        Catch ex1 As ThreadAbortException
            WriteLog("DownloadAnvizData ThreadAbort : " & Date.Now & " : " & ex1.Message)
            SendFailedNotificationToUser(xDatabaseName, xCompanyId, False, ex1.Message.ToString(), objSendmail)
        Catch ex As Exception
            WriteLog("DownloadAnvizData " & Date.Now & " : " & ex.Message)
            SendFailedNotificationToUser(xDatabaseName, xCompanyId, False, ex.Message.ToString(), objSendmail)
        Finally
            clsAnviz.CKT_Disconnect()
        End Try
    End Sub

    Private Function InsertDeviceData(ByVal xDatabaseName As String, ByVal mstrDeviceCode As String, ByVal mstrIPAddress As String, ByVal mstrEnrollNo As String, ByVal mdtLoginDate As Date, ByVal mdtLoginTime As Date, ByVal mstrVerifyCode As String, ByVal mstrInOutMode As String, ByRef mstrError As String) As Boolean
        Dim objLogin As New clslogin_Tran()
        Try

            objLogin._DeviceAtt_DeviceNo = mstrDeviceCode

            objLogin._DeviceAtt_EnrollNo = mstrEnrollNo
            objLogin._DeviceAtt_IPAddress = mstrIPAddress
            objLogin._DeviceAtt_LoginDate = mdtLoginDate.Date
            objLogin._DeviceAtt_LoginTime = mdtLoginTime
            objLogin._DeviceAtt_VerifyMode = mstrVerifyCode
            objLogin._DeviceAtt_InOutMode = mstrInOutMode
            objLogin._DeviceAtt_IsError = False

            If objLogin.InsertDeviceAttendanceData(sqlCn, xDatabaseName) = False Then
                mstrError = objLogin._Message
                Return False
            End If
        Catch ex As Exception
            WriteLog("InsertDeviceData " & Date.Now & " : " & ex.Message)
            Return False
        Finally
            objLogin = Nothing
        End Try
        Return True
    End Function

    Private Sub SaveAttendanceData(ByVal xDatabaseName As String, ByVal xCompanyId As Integer, ByVal xYearId As Integer, ByVal xUserModeSetting As String, ByVal xFirstInLastout As Boolean, ByVal xPolicyManagement As Boolean _
                                                  , ByVal xDonotAttendanceinSeconds As Boolean, ByVal IsHolidayConsiderOnWeekend As Boolean, ByVal IsDayOffConsiderOnWeekend As Boolean _
                                                  , ByVal IsHolidayConsiderOnDayoff As Boolean, ByVal dtTable As DataTable)

        Dim dsList As DataSet = Nothing
        Dim intEmployeeId As Integer = -1
        Dim intShiftId As Integer = -1
        Dim dtLogindate As Date = Nothing
        Dim dtCheckIntime As DateTime = Nothing
        Dim dtCheckOuttime As DateTime = Nothing
        Dim objLogin As clslogin_Tran = Nothing
        Dim xUserId As Integer = 1

        Try


            If dtTable IsNot Nothing Then  ' dtTable IsNot Nothing 

                Dim mblnFlag As Boolean = True

                If dtTable.Rows.Count > 0 Then   'dtTable.Rows.Count > 0

                    For Each dr As DataRow In dtTable.Rows
                        Dim mblnAttendanceOnDeviceInOutStatus As Boolean = False

                        If CInt(dr.Item("EmployeeId")) = -1 Then
                            mblnFlag = False
                            Continue For
                        End If

                        Dim objShiftTran As New clsshift_tran

                        objLogin = New clslogin_Tran
                        dsList = objLogin.GetEmployeeLoginDate(sqlCn, xDatabaseName, "List", CInt(dr("EmployeeId")), CDate(dr("Logintime")))

                        If dsList.Tables("List").Rows.Count <= 0 Then   'dsList.Tables("List").Rows.Count <= 0

                            objLogin._Employeeunkid = CInt(dr("EmployeeId"))
                            objLogin._SourceType = enInOutSource.Import
                            objLogin._Userunkid = xUserId

                            If xFirstInLastout = False Then

                                Dim intCurrentShiftID As Integer = 0
                                intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                If intCurrentShiftID <= 0 Then
                                    objLogin._Shiftunkid = CInt(dr("shiftid"))
                                Else
                                    objLogin._Shiftunkid = intCurrentShiftID
                                End If

                                objLogin._Logindate = CDate(dr("Logindate")).Date

                                objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, objLogin._Shiftunkid)

                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logindate")).Date), False, FirstDayOfWeek.Sunday)))
                                If drShiftTran.Length > 0 Then

                                    Dim objShift As New clsNewshift_master
                                    objShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = objLogin._Shiftunkid

                                    mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus

                                    If objShift._IsOpenShift = False Then
                                        Dim mdtDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm:ss tt"))

                                        If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                            Dim mintPreviousShift As Integer = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, CDate(dr("Logintime")).Date.AddDays(-1).Date, intEmployeeId)

                                            Dim objPrevShift As New clsNewshift_master
                                            objPrevShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = mintPreviousShift

                                            If objPrevShift._IsOpenShift = False Then

                                                If mintPreviousShift <> intShiftId Then
                                                    objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, mintPreviousShift)
                                                    Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logintime")).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                                    If drPreviousShiftTran.Length > 0 Then
                                                        Dim mdtPreviousDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                        If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(dr("Logintime"))) >= 24 Then
                                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                                        Else
                                                            objLogin._Employeeunkid = intEmployeeId
                                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(CDate(dr("Logintime")).Date.AddDays(-1).Date).ToString(), "", intEmployeeId, -1) Then
                                                                objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                            Else
                                                                objLogin._Logindate = CDate(dr("Logintime")).Date
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                End If

                                            Else
                                                objLogin._Logindate = CDate(dr("Logintime")).Date
                                            End If
                                            objPrevShift = Nothing

                                        ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) >= 0 Then
                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                        End If
                                    Else
                                        objLogin._Logindate = CDate(dr("Logintime")).Date
                                    End If
                                    objShift = Nothing
                                End If

                                If mblnAttendanceOnDeviceInOutStatus Then

                                    intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                    If intCurrentShiftID <= 0 Then
                                        objLogin._Shiftunkid = CInt(dr("shiftid"))
                                    Else
                                        objLogin._Shiftunkid = intCurrentShiftID
                                    End If

                                    If CInt(dr("InOutMode")) = 0 Then 'IN STATUS

                                        objLogin._checkintime = CDate(dr("Logintime"))
                                        objLogin._Original_InTime = objLogin._checkintime
                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""

                                        If objLogin.Insert(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting, True, False _
                                                                 , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                 , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                            dr.Item("message") = objLogin._Message
                                            dr.Item("IsError") = True
                                            mblnFlag = False
                                            Continue For
                                        Else
                                            dr.Item("message") = ""
                                        End If

                                    ElseIf CInt(dr("InOutMode")) = 1 Then 'OUT STATUS

                                        Dim intLoginId As Integer = -1
                                        If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(objLogin._Logindate.Date), "", CInt(dr("EmployeeId")), intLoginId) Then
                                            objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = intLoginId
                                            objLogin._Checkouttime = CDate(dr("Logintime"))
                                            objLogin._Original_OutTime = objLogin._Checkouttime
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1

                                            objLogin._SourceType = enInOutSource.Import

                                            objLogin._Shiftunkid = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, objLogin._Logindate.Date, CInt(dr("EmployeeId")))


                                            If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                                      , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                      , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else
                                                dr.Item("message") = ""
                                            End If
                                        End If

                                    End If

                                ElseIf mblnAttendanceOnDeviceInOutStatus = False Then

                                    If objLogin.GetLoginType(sqlCn, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date) = 0 Then
                                        objLogin._Logindate = CDate(dr("Logindate")).Date
                                        objLogin._checkintime = CDate(dr("Logintime"))
                                        objLogin._Original_InTime = objLogin._checkintime

                                        intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                        If intCurrentShiftID <= 0 Then
                                            objLogin._Shiftunkid = CInt(dr("shiftid"))
                                        Else
                                            objLogin._Shiftunkid = intCurrentShiftID
                                        End If

                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""

                                        If objLogin.Insert(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting, True, False _
                                                             , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                             , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                            dr.Item("message") = objLogin._Message
                                            dr.Item("IsError") = True
                                            mblnFlag = False
                                            Continue For
                                        Else
                                            dr.Item("message") = ""
                                        End If

                                    Else

                                        objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName)
                                        objLogin._Checkouttime = CDate(dr("Logintime"))
                                        objLogin._Original_OutTime = objLogin._Checkouttime

                                        intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, objLogin._Logindate.Date, CInt(dr("EmployeeId")))

                                        If intCurrentShiftID <= 0 Then
                                            objLogin._Shiftunkid = CInt(dr("shiftid"))
                                        Else
                                            objLogin._Shiftunkid = intCurrentShiftID
                                        End If


                                        If objLogin._checkintime > objLogin._Checkouttime Then
                                            Continue For
                                        End If


                                        If objLogin._checkintime < objLogin._Checkouttime Then
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1
                                            objLogin._SourceType = enInOutSource.Import


                                            If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                              , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                              , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else
                                                dr.Item("message") = ""
                                            End If
                                        End If

                                    End If

                                End If


                            ElseIf xFirstInLastout Then   'ConfigParameter._Object._FirstCheckInLastCheckOut

                                Dim intCurrentShiftID As Integer = 0

                                If IsDBNull(dr("Logindate")) Then Continue For

                                intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                If intCurrentShiftID <= 0 Then
                                    objLogin._Shiftunkid = CInt(dr("shiftid"))
                                Else
                                    objLogin._Shiftunkid = intCurrentShiftID
                                End If

                                objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, objLogin._Shiftunkid)
                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logindate")).Date), False, FirstDayOfWeek.Sunday)))
                                intEmployeeId = CInt(dr("EmployeeId"))
                                intShiftId = objLogin._Shiftunkid
                                dtLogindate = CDate(dr("Logindate")).Date
                                dtCheckIntime = CDate((dr("Logintime")))
                                dtCheckOuttime = CDate((dr("Logintime")))

                                If drShiftTran.Length > 0 Then   'drShiftTran.Length > 0

                                    Dim objShift As New clsNewshift_master
                                    objShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = objLogin._Shiftunkid

                                    mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus

                                    If objShift._IsOpenShift = False Then
                                        If IsDBNull(drShiftTran(0)("daystart_time")) Then Continue For
                                        Dim mdtDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm tt"))

                                        If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM

                                            Dim mintPreviousShift As Integer = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, CDate(dr("Logintime")).Date.AddDays(-1).Date, intEmployeeId)

                                            Dim objPrevShift As New clsNewshift_master
                                            objPrevShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = mintPreviousShift

                                            If objPrevShift._IsOpenShift = False Then

                                                If mintPreviousShift <> intShiftId Then
                                                    objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, mintPreviousShift)
                                                    Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logintime")).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                                    If drPreviousShiftTran.Length > 0 Then
                                                        Dim mdtPreviousDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                        If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(dr("Logintime"))) >= 24 Then
                                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                                        Else
                                                            objLogin._Employeeunkid = intEmployeeId
                                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(CDate(dr("Logintime")).Date.AddDays(-1).Date).ToString(), "", intEmployeeId, -1) Then
                                                                objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                            Else
                                                                objLogin._Logindate = CDate(dr("Logintime")).Date
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                End If
                                            Else
                                                objLogin._Logindate = CDate(dr("Logintime")).Date
                                            End If  'If objPrevShift._IsOpenShift = False Then

                                        ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) >= 0 Then
                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                        End If
                                    Else
                                        objLogin._Logindate = CDate(dr("Logintime")).Date
                                    End If
                                    objShift = Nothing


                                    If mblnAttendanceOnDeviceInOutStatus Then  'ConfigParameter._Object._SetDeviceInOutStatus

                                        If CInt(dr("InOutMode")) = 0 Then 'IN STATUS

                                            Dim mintLoginId As Integer = -1

                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(CDate(dr("Logintime")).Date), "", intEmployeeId, mintLoginId) Then   'objLogin.IsEmployeeLoginExist
                                                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = mintLoginId

                                                If objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing Then  ' objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing
                                                    objLogin._checkintime = CDate(dr("Logintime"))
                                                    objLogin._Original_InTime = objLogin._checkintime

                                                    Dim objShiftInMst As New clsNewshift_master
                                                    objShiftInMst._Shiftunkid(sqlCn, Nothing, xDatabaseName) = intCurrentShiftID
                                                    If objShiftInMst._MaxHrs > 0 Then  'objShiftInMst._MaxHrs > 0 
DeviceInStatus:
                                                        If DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs) Then  'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1
                                                            objLogin._SourceType = enInOutSource.Import

                                                            If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                                                       , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                                       , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else
                                                                dr.Item("message") = ""
                                                            End If  'objLogin.Update() = False

                                                        ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) > CInt(objShiftInMst._MaxHrs) Then  'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)
                                                            GoTo DeviceInStatus1
                                                        End If   'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)

                                                    ElseIf objShiftInMst._MaxHrs <= 0 Then  'objShiftInMst._MaxHrs > 0 
                                                        objLogin._checkintime = CDate(dr("Logintime"))
                                                        objLogin._Original_InTime = objLogin._checkintime
                                                        GoTo DeviceInStatus
                                                    End If  'objShiftInMst._MaxHrs > 0 

                                                End If 'objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing

                                            Else
DeviceInStatus1:
                                                objLogin._Logindate = CDate(dr("Logintime")).Date

                                                objLogin._checkintime = CDate(dr("Logintime"))
                                                objLogin._Original_InTime = objLogin._checkintime
                                                objLogin._Checkouttime = Nothing
                                                objLogin._Original_OutTime = Nothing
                                                objLogin._InOutType = 0
                                                objLogin._Workhour = 0
                                                objLogin._Holdunkid = 0
                                                objLogin._Voiddatetime = Nothing
                                                objLogin._Isvoid = False
                                                objLogin._Voidreason = ""

                                                If objLogin.Insert(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting, True, False _
                                                      , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                      , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                    dr.Item("message") = objLogin._Message
                                                    dr.Item("IsError") = True
                                                    mblnFlag = False
                                                    Continue For
                                                Else
                                                    dr.Item("message") = ""
                                                End If 'objLogin.Insert

                                            End If  'objLogin.IsEmployeeLoginExist


                                        ElseIf CInt(dr("InOutMode")) = 1 Then 'OUT STATUS
                                            Dim intLoginId As Integer = -1

                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(CDate(dr("Logintime")).Date), "", CInt(dr("EmployeeId")), intLoginId) Then
                                                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = intLoginId
                                                objLogin._Checkouttime = CDate(dr("Logintime"))
                                                objLogin._Original_OutTime = objLogin._Checkouttime

                                                If objLogin._checkintime <> Nothing Then
                                                    Dim objShiftMst As New clsNewshift_master
                                                    objShiftMst._Shiftunkid(sqlCn, Nothing, xDatabaseName) = intCurrentShiftID

                                                    If objShiftMst._MaxHrs > 0 Then
                                                        If DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs) Then  'objShiftMst._MaxHrs > 0 
DeviceOutStatus:
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1
                                                            objLogin._SourceType = enInOutSource.Import

                                                            If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                                                  , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                                  , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else
                                                                dr.Item("message") = ""
                                                            End If

                                                        ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) > CInt(objShiftMst._MaxHrs) Then  ''objShiftMst._MaxHrs > 0 
DeviceOutStatus1:
                                                            objLogin._checkintime = Nothing
                                                            objLogin._Original_InTime = Nothing
                                                            objLogin._InOutType = 0
                                                            objLogin._Workhour = 0
                                                            objLogin._Holdunkid = 0
                                                            objLogin._Voiddatetime = Nothing
                                                            objLogin._Isvoid = False
                                                            objLogin._Voidreason = ""

                                                            If objLogin.Insert(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting, True, False _
                                                                                  , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                                  , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else
                                                                dr.Item("message") = ""
                                                            End If

                                                        End If

                                                    ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0 
                                                        If objLogin._checkintime <> Nothing Then   'objShiftMst._MaxHrs <= 0 
DeviceOutStatus2:
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1
                                                            objLogin._SourceType = enInOutSource.Import


                                                            If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                                             , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                             , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else
                                                                dr.Item("message") = ""
                                                            End If

                                                        ElseIf objLogin._checkintime = Nothing Then   'objShift._MaxHrs <= 0 
                                                            Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(sqlCn, xDatabaseName, CDate(dr("Logintime")).Date, objLogin._Employeeunkid)
                                                            If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then
                                                                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                                objLogin._Checkouttime = CDate(dr("Logintime"))
                                                                objLogin._Original_OutTime = objLogin._Checkouttime
                                                                Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                                objLogin._Workhour = CInt(wkmins)
                                                                objLogin._InOutType = 1
                                                                objLogin._SourceType = enInOutSource.Import

                                                                If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                                      , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                      , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                                    dr.Item("message") = objLogin._Message
                                                                    dr.Item("IsError") = True
                                                                    mblnFlag = False
                                                                    Continue For
                                                                Else
                                                                    dr.Item("message") = ""
                                                                End If
                                                            Else
                                                                Continue For
                                                            End If
                                                        End If   'objShiftMst._MaxHrs <= 0 

                                                    End If   'objShiftMst._MaxHrs <= 0 

                                                ElseIf objLogin._checkintime = Nothing Then  'objLogin._checkintime <> Nothing
                                                    GoTo DeviceOutStatus1
                                                End If

                                            Else  'IF employee login is not exist.

                                                Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(sqlCn, xDatabaseName, CDate(dr("Logintime")).Date, objLogin._Employeeunkid)
                                                If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then  'dtDeviceTable.Rows.Count > 0
                                                    objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                    objLogin._Checkouttime = CDate(dr("Logintime"))
                                                    objLogin._Original_OutTime = objLogin._Checkouttime

                                                    Dim objShiftMst As New clsNewshift_master
                                                    objShiftMst._Shiftunkid(sqlCn, Nothing, xDatabaseName) = intCurrentShiftID
                                                    If objShiftMst._MaxHrs > 0 Then   'objShiftMst._MaxHrs > 0
                                                        If objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs) Then
                                                            GoTo DeviceOutStatus
                                                        ElseIf objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) > CInt(objShiftMst._MaxHrs) Then
                                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                                            GoTo DeviceOutStatus1
                                                        End If 'objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs)
                                                    ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0
                                                        GoTo DeviceOutStatus2
                                                    End If  'objShiftMst._MaxHrs > 0
                                                Else  ' 'dtDeviceTable.Rows.Count > 0
                                                    objLogin._Logindate = CDate(dr("Logintime")).Date
                                                    objLogin._Checkouttime = CDate(dr("Logintime"))
                                                    objLogin._Original_OutTime = objLogin._Checkouttime
                                                    GoTo DeviceOutStatus1
                                                End If  'dtDeviceTable.Rows.Count > 0

                                            End If   'IF employee login is not exist.


                                        End If   'CInt(dr("InOutMode")) = 1


                                    ElseIf mblnAttendanceOnDeviceInOutStatus = False Then  'ConfigParameter._Object._SetDeviceInOutStatus = False

                                        Dim intLoginId As Integer = -1
                                        If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(objLogin._Logindate.Date), "", CInt(dr("EmployeeId")), intLoginId) Then
                                            objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = intLoginId
                                            objLogin._Checkouttime = CDate(dr("Logintime"))
                                            objLogin._Original_OutTime = objLogin._Checkouttime
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1
                                            objLogin._SourceType = enInOutSource.Import
                                            objLogin._Shiftunkid = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, objLogin._Logindate.Date, CInt(dr("EmployeeId")))


                                            If objLogin.Update(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting _
                                                                      , True, False, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                      , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else
                                                dr.Item("message") = ""
                                            End If


                                        Else
                                            objLogin._checkintime = CDate(dr("Logintime"))
                                            objLogin._Original_InTime = objLogin._checkintime
                                            objLogin._Checkouttime = Nothing
                                            objLogin._Original_OutTime = Nothing
                                            objLogin._InOutType = 0
                                            objLogin._Workhour = 0
                                            objLogin._Holdunkid = 0
                                            objLogin._Voiddatetime = Nothing
                                            objLogin._Isvoid = False
                                            objLogin._Voidreason = ""


                                            If objLogin.Insert(sqlCn, xDatabaseName, xUserId, xYearId, xCompanyId, Now.AddDays(-1).Date, Now.AddDays(-1).Date, xUserModeSetting, True, False _
                                                                             , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                             , IsHolidayConsiderOnDayoff, False, -1, "", "") = False Then

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else
                                                dr.Item("message") = ""
                                            End If

                                        End If

                                    End If  'ConfigParameter._Object._SetDeviceInOutStatus


                                    Dim dtMaxEmployeeDate As DataTable = objLogin.GetEmployeeMaxLoginDate(sqlCn, xDatabaseName, objLogin._Logindate.Date, CInt(dr("EmployeeId")))
                                    If dtMaxEmployeeDate IsNot Nothing AndAlso dtMaxEmployeeDate.Rows.Count > 0 Then  'dtMaxEmployeeDate.Rows.Count > 0
                                        objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = CInt(dtMaxEmployeeDate.Rows(0)("loginunkid"))

                                        If objLogin._Checkouttime = Nothing Then
                                            Dim drError() As DataRow = dtTable.Select("Logintime = '" & objLogin._Original_InTime & "' AND EmployeeId = " & objLogin._Employeeunkid)
                                            If drError.Length > 0 Then
                                                drError(0)("Message") = "MisMatch Login/Logout."
                                                drError(0)("IsError") = True
                                                mblnFlag = False
                                                dtTable.AcceptChanges()
                                            End If
                                        ElseIf objLogin._checkintime = Nothing Then
                                            Dim drError() As DataRow = dtTable.Select("Logintime = '" & objLogin._Original_OutTime & "' AND EmployeeId = " & objLogin._Employeeunkid)
                                            If drError.Length > 0 Then  'drError.Length > 0
                                                drError(0)("Message") = "MisMatch Login/Logout."
                                                drError(0)("IsError") = True
                                                mblnFlag = False
                                                dtTable.AcceptChanges()
                                            End If  'drError.Length > 0
                                        End If  ' objLogin._Checkouttime = Nothing AND objLogin._checkintime = Nothing

                                    End If   'dtMaxEmployeeDate.Rows.Count > 0

                                End If   'drShiftTran.Length > 0

                            End If  'ConfigParameter._Object._FirstCheckInLastCheckOut

                        End If  'dsList.Tables("List").Rows.Count <= 0

                    Next

                    '    If mblnFlag Then
                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Data successfully import."), enMsgBoxStyle.Information)
                    '    Else
                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Some Data did not import due to error."), enMsgBoxStyle.Information)
                    '        dtTable = New DataView(dtTable, "IsError = 1", "", DataViewRowState.CurrentRows).ToTable()

                    '        If objLogin.DeleteDeviceAttendanceData() = False Then
                    '            eZeeMsgBox.Show(objLogin._Message, enMsgBoxStyle.Information)
                    '        End If
                    '        Exit Sub
                    '    End If


                    'ElseIf dtTable.Rows.Count <= 0 Then   'dtTable.Rows.Count > 0
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Download Device Data First."), enMsgBoxStyle.Information)
                    'Exit Sub

                End If   'dtTable.Rows.Count > 0

            End If   ' dtTable IsNot Nothing 
        Catch ex As Exception
            Dim strMessage As String = "Employee :- " & intEmployeeId & ",Shift :-" & intShiftId & ",LoginDate :- " & dtLogindate & ",CheckIntime :- " & dtCheckIntime & ",CheckOuttime :- " & dtCheckOuttime
            WriteLog("InsertDeviceData " & Date.Now & " : " & strMessage & vbCrLf & ex.Message)
        Finally
        End Try
    End Sub

    Public Function GetEmployee_Current_ShiftId(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String, ByVal iDate As DateTime, ByVal iEmployId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iValue As Integer = 0
        Dim sqlCmd As SqlCommand = Nothing
        Try
            strQ = "SELECT TOP 1 shiftunkid	AS iShift FROM " & xDatabaseName & "..hremployee_shift_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & convertDate(iDate) & "' AND employeeunkid = '" & iEmployId & "' AND effectivedate IS NOT NULL ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "
            sqlCmd = New SqlCommand(strQ, SqlCn)
            sqlCmd.Parameters.Clear()
            iValue = CInt(sqlCmd.ExecuteScalar())
            Return iValue
        Catch ex As Exception
            WriteLog("GetEmployee_Current_ShiftId " & Date.Now & " : " & ex.Message)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Sub SendFailedNotificationToUser(ByVal xDatabaseName As String, ByVal xCompanyunkid As Integer, ByVal mblnIsdisconnect As Boolean, ByVal mstrError As String, ByVal objSendmail As clsSendMail)
        Try
            Dim mstrModuleName As String = "clsEmployee_Master"
            Dim xUserLanguageId As Integer = -1
            Dim strMessage As String = ""

            If mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then

                Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
                NewCulture.DateTimeFormat.ShortDatePattern = "dd-MMM-yyyy"
                NewCulture.DateTimeFormat.DateSeparator = "-"
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture


                Dim mstrQuery As String = " SELECT " & _
                                                        " cfuser_master.userunkid " & _
                                                        ",ISNULL(cfuser_master.username,'') AS username " & _
                                                        ",ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS UName " & _
                                                        ",ISNULL(cfuser_master.languageunkid,1) AS languageunkid " & _
                                                        ",ISNULL(cfuser_master.email,'') AS email " & _
                                                        " FROM hrmsConfiguration..cfuser_master " & _
                                                        " WHERE cfuser_master.isactive = 1 AND cfuser_master.userunkid = @userunkid "


                For Each sId As String In mstrTnAFailureNotificationUserIds.Trim.Split(CChar(","))
                    strMessage = ""
                    Dim mstrUserName As String = ""
                    Dim mstrUserEmail As String = ""
                    Dim dtUser As New DataTable

                    Dim oCmd As New SqlCommand(mstrQuery, sqlCn)
                    oCmd.Parameters.Clear()
                    oCmd.Parameters.Add("@userunkid", SqlDbType.Int).Value = CInt(sId)
                    Dim adp As New SqlDataAdapter(oCmd)
                    adp.Fill(dtUser)


                    If dtUser IsNot Nothing AndAlso dtUser.Rows.Count > 0 Then

                        If dtUser.Rows(0)("UName").ToString().Trim.Length > 0 Then
                            mstrUserName = dtUser.Rows(0)("UName").ToString().Trim.ToLower()
                        Else
                            mstrUserName = dtUser.Rows(0)("username").ToString().Trim.ToLower()
                        End If

                        xUserLanguageId = CInt(dtUser.Rows(0)("languageunkid"))
                        mstrUserEmail = dtUser.Rows(0)("email").ToString()

                        strMessage = "<HTML> <BODY>"
                        strMessage &= "Dear " & CultureInfo.CurrentCulture.TextInfo.ToTitleCase(mstrUserName) & ", <BR></BR>"
                        If mblnIsdisconnect Then
                            strMessage &= "This is to notify you that device code <B>" & mstrDeviceCode & "</B> with IP Adress <B>(" & strIp & ")</B> was not able to connect." & "<BR></BR>"
                        Else
                            strMessage &= "This is to notify you that the process of downloading Attendance Data failed for <B> (" & Now.AddDays(-1).Date & ")</B>  due to below error." & "<BR></BR>"
                        End If

                        strMessage &= "<B>" & "Error Message :" & mstrError & "</B><BR></BR>"
                        strMessage &= "Regards."

                        Dim blnValue As Boolean = True
                        Dim mstrArutisignature As String = ""
                        mstrArutisignature = GetConfigKeyValue(xCompanyunkid, "ShowArutisignature")
                        blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
                        If blnValue Then
                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        End If
                        strMessage &= "</BODY></HTML>"


                        objSendmail._Message = strMessage.ToString()
                        objSendmail._Form_Name = "ArutiReflex"
                        objSendmail._LogEmployeeUnkid = -1
                        objSendmail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendmail._UserUnkid = 1
                        objSendmail._ModuleRefId = enAT_VIEW_TYPE.TNA_MGT

                        Dim mstrEmailError As String = ""
                        If mstrUserEmail.Trim.Length > 0 Then
                            objSendmail._Subject = "Notification for Failure of Attendance Data"
                            objSendmail._ToEmail = mstrUserEmail.Trim
                            mstrEmailError = objSendmail.SendMail()

                            Insert_Email_Tran_Log(enAT_VIEW_TYPE.TNA_MGT, xDatabaseName, objSendmail._SenderAddress, objSendmail._ToEmail, objSendmail._Subject, objSendmail._Message, 1, enLogin_Mode.DESKTOP, mstrEmailError, "")

                        End If

                    End If

                Next
            End If
        Catch ex As Exception
            WriteLog("SendFailedNotificationToUser : " & ex.Message)
        End Try
    End Sub

    Public Sub Insert_Email_Tran_Log(ByVal mintModuleRefId As enAT_VIEW_TYPE, ByVal mstrDatabaseName As String, ByVal strSenderaddress As String _
                                                     , ByVal mstrToEmail As String, ByVal mstrSubject As String, ByVal mstrMessage As String, ByVal xUserUnkid As Integer _
                                                     , ByVal xOperationModeId As Integer, Optional ByVal strReason As String = "", Optional ByVal mstrAttachedFiles As String = "")

        Dim StrQ As String = String.Empty

        Try
            Dim iTableName As String = String.Empty

            Select Case mintModuleRefId
                Case enAT_VIEW_TYPE.TNA_MGT
                    iTableName = mstrDatabaseName & "..attna_email_ntf_tran"
            End Select

            StrQ = "INSERT INTO " & iTableName & " " & _
                   "( " & _
                   "	 row_guid " & _
                   "	,sender_address " & _
                   "	,recipient_address " & _
                   "	,send_datetime " & _
                   "	,sent_data " & _
                   "	,subject " & _
                   "	,ip " & _
                   "	,host " & _
                   "	,form_name " & _
                   "	,module_name1 " & _
                   "	,module_name2 " & _
                   "	,module_name3 " & _
                   "	,module_name4 " & _
                   "	,module_name5 " & _
                   "	,operationmodeid " & _
                   "	,userunkid " & _
                   "	,loginemployeeunkid " & _
                   "	,fail_reason " & _
                   "	,filename " & _
                   "	,gateway " & _
                   ", isweb " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "	 @row_guid " & _
                   "	,@sender_address " & _
                   "	,@recipient_address " & _
                   "	, getdate() " & _
                   "	,@sent_data " & _
                   "	,@subject " & _
                   "	,@ip " & _
                   "	,@host " & _
                   "	,@form_name " & _
                   "	,@module_name1 " & _
                   "	,@module_name2 " & _
                   "	,@module_name3 " & _
                   "	,@module_name4 " & _
                   "	,@module_name5 " & _
                   "	,@operationmodeid " & _
                   "	,@userunkid " & _
                   "	,@loginemployeeunkid " & _
                   "	,@fail_reason "

            If mstrAttachedFiles.Trim.Length > 0 Then
                StrQ &= ",'" & mstrAttachedFiles & "' "
            Else
                StrQ &= ",'' "
            End If
            StrQ &= "	,@gateway " & _
                    ", @isweb " & _
                    ") "

            Dim oCmd As New SqlCommand(StrQ, sqlCn)
            oCmd.Parameters.Clear()

            oCmd.Parameters.AddWithValue("@row_guid", Guid.NewGuid.ToString())
            oCmd.Parameters.AddWithValue("@sender_address", strSenderaddress)
            oCmd.Parameters.AddWithValue("@recipient_address", mstrToEmail)
            oCmd.Parameters.AddWithValue("@sent_data", mstrMessage)
            oCmd.Parameters.AddWithValue("@subject", mstrSubject)
            oCmd.Parameters.AddWithValue("@ip", getIP())
            oCmd.Parameters.AddWithValue("@host", getHostName())
            oCmd.Parameters.AddWithValue("@form_name", "ArutiReflex")
            oCmd.Parameters.AddWithValue("@isweb", False)
            oCmd.Parameters.AddWithValue("@module_name1", "")
            oCmd.Parameters.AddWithValue("@module_name2", "")
            oCmd.Parameters.AddWithValue("@module_name3", "")
            oCmd.Parameters.AddWithValue("@module_name4", "")
            oCmd.Parameters.AddWithValue("@module_name5", "")
            oCmd.Parameters.AddWithValue("@operationmodeid", xOperationModeId)
            oCmd.Parameters.AddWithValue("@userunkid", xUserUnkid)
            oCmd.Parameters.AddWithValue("@loginemployeeunkid", -1)
            oCmd.Parameters.AddWithValue("@fail_reason", strReason)
            oCmd.Parameters.AddWithValue("@gateway", strSenderaddress)

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            WriteLog("Insert_Email_Tran_Log : " & ex.Message)
            Insert_Email_Tran_Log(mintModuleRefId, mstrDatabaseName, strSenderaddress, mstrToEmail, mstrSubject, mstrMessage, xUserUnkid, xOperationModeId, ex.Message.ToString(), "")
        End Try
    End Sub

    Private Sub WriteLog(ByVal strMessage As String)
        Dim m_strLogFile As String = ""
        Dim m_strFileName As String = "ArutiAttendance_LOG_" & Now.ToString("yyyyMMdd")
        Dim file As System.IO.StreamWriter
        m_strLogFile = IO.Path.Combine(My.Application.Info.DirectoryPath, m_strFileName & ".txt")
        file = New System.IO.StreamWriter(m_strLogFile, True)
        file.BaseStream.Seek(0, SeekOrigin.End)
        file.WriteLine(strMessage)
        file.Close()
    End Sub

#End Region

End Class
