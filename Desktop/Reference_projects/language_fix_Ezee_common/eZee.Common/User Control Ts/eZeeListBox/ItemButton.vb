Imports System.Drawing

Public Class ItemButton
    Inherits eZeeButton

    Private m_clrBackColor As Color
    Private m_clrForeColor As Color

    Private m_intMaximumOrderQty As Integer
    Private _Index As Integer = -1

    Public Property Index() As Integer
        Get
            Return _Index
        End Get
        Set(ByVal value As Integer)
            _Index = value
        End Set
    End Property

    Private _UnkId As Integer = -1

    Public Property UnkID() As Integer
        Get
            Return _UnkId
        End Get
        Set(ByVal value As Integer)
            _UnkId = value
        End Set
    End Property

    Private _Caption As String = ""

    Public Property Caption() As String
        Get
            Return _Caption
        End Get
        Set(ByVal value As String)
            _Caption = value
        End Set
    End Property

    Public Sub New()
        Me.Visible = False
    End Sub

    Public Property OriginalBackColor() As Color
        Get
            Return m_clrBackColor
        End Get
        Set(ByVal value As Color)
            m_clrBackColor = value
        End Set
    End Property

    Public Property OriginalForeColor() As Color
        Get
            Return m_clrForeColor
        End Get
        Set(ByVal value As Color)
            m_clrForeColor = value
        End Set
    End Property

    Public Property MaximumOrderQty() As Integer
        Get
            Return m_intMaximumOrderQty
        End Get
        Set(ByVal value As Integer)
            m_intMaximumOrderQty = value
        End Set
    End Property
End Class