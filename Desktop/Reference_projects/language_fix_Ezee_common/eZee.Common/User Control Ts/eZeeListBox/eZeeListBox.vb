Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

<DefaultProperty("Name"), DefaultEvent("SelectedListItem")> _
Public Class eZeeListBox
    Public Enum enOrientation
        None = 0
        Horizontal = 1
        Vertical = 2
    End Enum

    Private m_intSelectedID As Integer = -1
    Private m_intSelectedIndex As Integer = -1
    Private m_intSelectedText As String = ""

    Private m_blnNext As Boolean = False

    Private Sub _HoveringTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _HoveringTimer.Tick
        If m_blnNext Then
            Call setPage(mintCurrentPage + 1, -1)
        Else
            Call setPage(mintCurrentPage - 1, -1)
        End If
    End Sub

#Region " Properties "

    Private colName As String = String.Empty
    Public Property _DisplayMember() As String
        Get
            Return colName
        End Get
        Set(ByVal value As String)
            colName = value
            objcbodata.DisplayMember = value
        End Set
    End Property

    Private colId As String = String.Empty
    Public Property _ValueMember() As String
        Get
            Return colId
        End Get
        Set(ByVal value As String)
            colId = value
            objcbodata.ValueMember = value
        End Set
    End Property

    Private colBackColor As String = String.Empty
    Public Property _BackcolorMember() As String
        Get
            Return colBackColor
        End Get
        Set(ByVal value As String)
            colBackColor = value
        End Set
    End Property

    Private colForecolor As String = String.Empty
    Public Property _ForecolorMember() As String
        Get
            Return colForecolor
        End Get
        Set(ByVal value As String)
            colForecolor = value
        End Set
    End Property

    Private colTextAlign As String = String.Empty
    Public Property _TextAlignMember() As String
        Get
            Return colTextAlign
        End Get
        Set(ByVal value As String)
            colTextAlign = value
        End Set
    End Property

    Private colFontSize As String = String.Empty
    Public Property _TextSizeMember() As String
        Get
            Return colFontSize
        End Get
        Set(ByVal value As String)
            colFontSize = value
        End Set
    End Property

    Private colFontName As String = String.Empty
    Public Property _FontNameMember() As String
        Get
            Return colFontName
        End Get
        Set(ByVal value As String)
            colFontName = value
        End Set
    End Property

    Private colFontStyle As String = String.Empty
    Public Property _FontStyleMember() As String
        Get
            Return colFontStyle
        End Get
        Set(ByVal value As String)
            colFontStyle = value
        End Set
    End Property

    Private colShowCaption As String = String.Empty
    Public Property _ShowCaptionMember() As String
        Get
            Return colShowCaption
        End Get
        Set(ByVal value As String)
            colShowCaption = value
        End Set
    End Property

    Private colImage As String = String.Empty
    Public Property _ImageMember() As String
        Get
            Return colImage
        End Get
        Set(ByVal value As String)
            colImage = value
        End Set
    End Property

    Private colShrinkImage As String = String.Empty
    Public Property _ShrinkImageMember() As String
        Get
            Return colShrinkImage
        End Get
        Set(ByVal value As String)
            colShrinkImage = value
        End Set
    End Property

    Private colMaximumOrderQty As String = String.Empty
    Public Property _MaximumOrderQty() As String
        Get
            Return colMaximumOrderQty
        End Get
        Set(ByVal value As String)
            colMaximumOrderQty = value
        End Set
    End Property

    Public Property _DataSource() As Object
        Get
            Return objcbodata.DataSource
        End Get
        Set(ByVal value As Object)
            objcbodata.DataSource = value
        End Set
    End Property

    Public ReadOnly Property _Items() As ComboBox.ObjectCollection
        Get
            Return objcbodata.Items
        End Get
    End Property

    <Browsable(False)> _
    Public Property _SelectedIndex() As Integer
        Get
            Return m_intSelectedIndex
        End Get
        Set(ByVal value As Integer)
            m_intSelectedIndex = value
            Call FindIndex(m_intSelectedIndex)
        End Set
    End Property

    <Browsable(False)> _
    Public Property _SelectedId() As Integer
        Get
            Return m_intSelectedID
        End Get
        Set(ByVal value As Integer)
            m_intSelectedID = value
            Call FindId(m_intSelectedID)
        End Set
    End Property

    <Browsable(False)> _
    Public Property _SelectedText() As String
        Get
            Return m_intSelectedText
        End Get
        Set(ByVal value As String)
            m_intSelectedText = value
            Call FindName(m_intSelectedText)
        End Set
    End Property

    Private m_intBorderWidth As Integer = 1

    Public Property _BorderWidth() As Integer
        Get
            Return m_intBorderWidth
        End Get
        Set(ByVal value As Integer)
            m_intBorderWidth = value
            setControls()
            Invalidate()
        End Set
    End Property

    Private m_clrBorderColor As Color = Color.White

    Public Property _BorderColor() As Color
        Get
            Return m_clrBorderColor
        End Get
        Set(ByVal value As Color)
            m_clrBorderColor = value
            Invalidate()
        End Set
    End Property

    Private m_Orientation As enOrientation = enOrientation.None

    Public Property _Orientation() As enOrientation
        Get
            Return m_Orientation
        End Get
        Set(ByVal value As enOrientation)
            m_Orientation = value

            setControls()
            Invalidate()
        End Set
    End Property

    Private m_intCotrolButtonSize As Integer = 30

    Public Property _CotrolButtonSize() As Integer
        Get
            Return m_intCotrolButtonSize
        End Get
        Set(ByVal value As Integer)
            m_intCotrolButtonSize = value
            Invalidate()
        End Set
    End Property

    Private m_intColumns As Integer = 2

    Public Property _Columns() As Integer
        Get
            Return m_intColumns
        End Get
        Set(ByVal value As Integer)
            m_intColumns = value
        End Set
    End Property

    Private m_intRows As Integer = 2

    Public Property _Rows() As Integer
        Get
            Return m_intRows
        End Get
        Set(ByVal value As Integer)
            m_intRows = value
        End Set
    End Property

    Public m_blnShowOutline As Boolean = True

    Public Property _ShowOutline() As Boolean
        Get
            Return m_blnShowOutline
        End Get
        Set(ByVal value As Boolean)
            m_blnShowOutline = value
        End Set
    End Property

    Private m_clrControl As Color = Color.Green

    Public Property _ControlColor() As Color
        Get
            Return m_clrControl
        End Get
        Set(ByVal value As Color)
            m_clrControl = value
            btnNext.BackColor = m_clrControl
            btnNext.ShineColor = m_clrControl
            btnPrevious.BackColor = m_clrControl
            btnPrevious.ShineColor = m_clrControl
        End Set
    End Property

    Private m_clrItem As Color = Color.Black

    Public Property _ItemColor() As Color
        Get
            Return m_clrItem
        End Get
        Set(ByVal value As Color)
            m_clrItem = value
        End Set
    End Property

#End Region

#Region " Events "

    ''' Occurs when the user select items.
    <Category("ListItem"), Description("Occurs when the user select items.")> _
    Public Event SelectedListItem As SelectedListItemEventHandler
    Public Delegate Sub SelectedListItemEventHandler(ByVal sender As System.Object, ByVal e As eZeeListBoxEventArgs)

    Private Sub btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        m_intSelectedID = sender.UnkID
        m_intSelectedIndex = sender.Index
        m_intSelectedText = sender.Caption

        For Each btn As ItemButton In pnlMain.Controls
            If m_intSelectedIndex = Val(btn.Index) Then


                btn.InnerBorderColor = Color.LawnGreen
                btn.BackColor = Color.LawnGreen
                btn.ForeColor = Color.White
                'btn.InnerBorderColor = Color.Black
                'btn.OuterBorderColor = Color.Black
            Else

                btn.BackColor = btn.OriginalBackColor
                btn.ForeColor = btn.OriginalForeColor

                If m_blnShowOutline Then
                    btn.InnerBorderColor = Color.Transparent
                    btn.OuterBorderColor = Color.White
                Else
                    btn.InnerBorderColor = btn.BackColor
                    btn.OuterBorderColor = btn.BackColor
                End If
            End If
        Next

        RaiseEvent SelectedListItem(sender, New eZeeListBoxEventArgs(sender.Index, sender.UnkID, sender.Caption, sender))
    End Sub

#End Region

#Region " Scroll Events "

    Private Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Call setPage(mintCurrentPage - 1, -1)
    End Sub

    Private Sub btnPrevious_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnPrevious.MouseDown
        m_blnNext = False
        _HoveringTimer.Enabled = True
    End Sub

    Private Sub btnPrevious_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnPrevious.MouseUp
        _HoveringTimer.Enabled = False
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Call setPage(mintCurrentPage + 1, -1)
    End Sub

    Private Sub btnNext_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnNext.MouseDown
        m_blnNext = True
        _HoveringTimer.Enabled = True
    End Sub

    Private Sub btnNext_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnNext.MouseUp
        _HoveringTimer.Enabled = False
    End Sub

#End Region

#Region " Methods "

    Public Sub FindName(ByVal Text As String)
        Dim intRowIndex As Integer = -1
        Try

            intRowIndex = objcbodata.FindString(Text)

            'Find Page
            If intRowIndex >= 0 Then
                intRowIndex += 1
                Dim intItemPerPage As Integer = m_intRows * m_intColumns
                Dim intPage As Integer = (intRowIndex) \ intItemPerPage

                If (intRowIndex) Mod intItemPerPage > 0 Then
                    intPage += 1
                End If

                Call setPage(intPage, 2)

            End If

        Finally

        End Try
    End Sub

    Public Sub FindId(ByVal ID As Integer)
        Dim intRowIndex As Integer = -1
        Try

            objcbodata.SelectedValue = ID

            If objcbodata.SelectedIndex >= 0 Then
                intRowIndex = objcbodata.SelectedIndex
                End If

            'Find Page
            If intRowIndex >= 0 Then
                intRowIndex += 1
                Dim intItemPerPage As Integer = m_intRows * m_intColumns
                Dim intPage As Integer = (intRowIndex) \ intItemPerPage

                If (intRowIndex) Mod intItemPerPage > 0 Then
                    intPage += 1
                End If

                Call setPage(intPage, 1)

            End If

        Finally

        End Try
    End Sub

    Public Sub FindIndex(ByVal Index As Integer)
        Dim intRowIndex As Integer = -1
        Try

            If objcbodata.Items.Count > Index Then
                intRowIndex = Index
            End If

            'Find Page
            If intRowIndex >= 0 Then
                intRowIndex += 1
                Dim intItemPerPage As Integer = m_intRows * m_intColumns
                Dim intPage As Integer = (intRowIndex) \ intItemPerPage

                If (intRowIndex) Mod intItemPerPage > 0 Then
                    intPage += 1
                End If

                Call setPage(intPage, 0)

            End If

        Finally

        End Try
    End Sub
#End Region

#Region " Private Method and Function "
    Private mintButtonGap As Integer = 4
    Private mintCurrentPage As Integer = 1

    Private Sub setControls()
        Select Case m_Orientation
            Case enOrientation.None
                Me.btnPrevious.Image = My.Resources.ArrowPrevious
                Me.btnNext.Image = My.Resources.ArrowNext

                Me.btnPrevious.Top = Height - m_intCotrolButtonSize - ((m_intBorderWidth + mintButtonGap))
                Me.btnNext.Top = Height - m_intCotrolButtonSize - ((m_intBorderWidth + mintButtonGap))

                Me.btnPrevious.Left = m_intBorderWidth + mintButtonGap
                Me.btnNext.Left = Width - m_intCotrolButtonSize - m_intBorderWidth - mintButtonGap

                Me.btnPrevious.Width = m_intCotrolButtonSize
                Me.btnNext.Width = m_intCotrolButtonSize

                Me.btnPrevious.Height = m_intCotrolButtonSize
                Me.btnNext.Height = m_intCotrolButtonSize

                Me.pnlMain.Top = m_intBorderWidth + mintButtonGap
                Me.pnlMain.Left = m_intBorderWidth + mintButtonGap

                Me.pnlMain.Width = Width - ((m_intBorderWidth + mintButtonGap) * 2)
                Me.pnlMain.Height = Height - m_intCotrolButtonSize - (m_intBorderWidth * 2) - (mintButtonGap * 4)

            Case enOrientation.Horizontal

                Me.btnPrevious.Image = My.Resources.ArrowPrevious
                Me.btnNext.Image = My.Resources.ArrowNext

                Me.btnPrevious.Top = m_intBorderWidth + mintButtonGap
                Me.btnNext.Top = m_intBorderWidth + mintButtonGap

                Me.btnPrevious.Left = m_intBorderWidth + mintButtonGap
                Me.btnNext.Left = Width - m_intCotrolButtonSize - m_intBorderWidth - mintButtonGap

                Me.btnPrevious.Width = m_intCotrolButtonSize
                Me.btnNext.Width = m_intCotrolButtonSize

                Me.btnPrevious.Height = Height - ((m_intBorderWidth + mintButtonGap) * 2)
                Me.btnNext.Height = Height - ((m_intBorderWidth + mintButtonGap) * 2)

                Me.pnlMain.Top = m_intBorderWidth + mintButtonGap
                Me.pnlMain.Left = m_intCotrolButtonSize + m_intBorderWidth + (mintButtonGap * 2)

                Me.pnlMain.Width = Width - (m_intCotrolButtonSize * 2) - (m_intBorderWidth * 2) - (mintButtonGap * 4)
                Me.pnlMain.Height = Height - (m_intBorderWidth * 2) - (mintButtonGap * 2)

            Case enOrientation.Vertical

                Me.btnPrevious.Image = My.Resources.ArrowUp
                Me.btnNext.Image = My.Resources.ArrowDown

                Me.btnPrevious.Top = m_intBorderWidth + mintButtonGap
                Me.btnNext.Top = Height - m_intCotrolButtonSize - ((m_intBorderWidth + mintButtonGap))

                Me.btnPrevious.Left = m_intBorderWidth + mintButtonGap
                Me.btnNext.Left = m_intBorderWidth + mintButtonGap

                Me.btnPrevious.Height = m_intCotrolButtonSize
                Me.btnNext.Height = m_intCotrolButtonSize

                Me.btnPrevious.Width = Width - ((m_intBorderWidth + mintButtonGap) * 2)
                Me.btnNext.Width = Width - ((m_intBorderWidth + mintButtonGap) * 2)

                Me.pnlMain.Top = m_intCotrolButtonSize + m_intBorderWidth + (mintButtonGap * 2)
                Me.pnlMain.Left = m_intBorderWidth + mintButtonGap

                Me.pnlMain.Width = Width - ((m_intBorderWidth + mintButtonGap) * 2)
                Me.pnlMain.Height = Height - (m_intCotrolButtonSize * 2) - (m_intBorderWidth * 2) - (mintButtonGap * 4)
        End Select

        Call setItemButtons()

    End Sub

    Private Sub setItemButtons()
        If MyBase.DesignMode Then
            Exit Sub
        End If

        Dim itemSize As Size

        Dim intButtonGap As Integer = 4

        itemSize.Width = pnlMain.Width / m_intColumns
        itemSize.Height = pnlMain.Height / m_intRows

        For iCnt As Integer = 0 To pnlMain.Controls.Count - 1
            RemoveHandler pnlMain.Controls(0).Click, AddressOf btn_Click
        Next

        pnlMain.Controls.Clear()

        Dim iX As Integer = 0
        Dim iY As Integer = -intButtonGap / 2

        For iRow As Integer = 1 To m_intRows
            iX = -intButtonGap / 2

            For iCol As Integer = 1 To m_intColumns
                Dim xButton As New ItemButton
                xButton.BackColor = m_clrItem
                xButton.ShineColor = m_clrItem

                xButton.Width = itemSize.Width - intButtonGap
                xButton.Height = itemSize.Height - intButtonGap

                xButton.Left = iX + intButtonGap
                xButton.Top = iY + intButtonGap

                AddHandler xButton.Click, AddressOf btn_Click

                pnlMain.Controls.Add(xButton)
                iX += itemSize.Width
            Next
            iY += itemSize.Height
        Next

        pnlMain.Refresh()

        If objcbodata.Items.Count > 0 Then
            Call setPage(1, -1)
        End If

    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="pintPageNo"></param>
    ''' <param name="intFindBy"> 0 = Index , 1 = Id, 2 = Name </param>
    ''' <remarks></remarks>
    Private Sub setPage(ByVal pintPageNo As Integer, ByVal intFindBy As Integer)
        Dim intItemPerPage As Integer = m_intRows * m_intColumns
        Dim intTotalPage As Integer = objcbodata.Items.Count \ intItemPerPage

        If objcbodata.Items.Count Mod intItemPerPage > 0 Then
            intTotalPage += 1
        End If

        If pintPageNo <= 0 Then
            mintCurrentPage = 1
        ElseIf pintPageNo > intTotalPage Then
            mintCurrentPage = intTotalPage
        Else
            mintCurrentPage = pintPageNo
        End If

        Dim intStart As Integer = 0
        Dim intEnd As Integer = 0

        intStart = (mintCurrentPage - 1) * intItemPerPage + 1
        intEnd = (mintCurrentPage) * intItemPerPage

        If intStart > objcbodata.Items.Count Then
            Exit Sub
        ElseIf intStart < 0 Then
            intStart = 1
        End If

        If intEnd > objcbodata.Items.Count Then
            intEnd = objcbodata.Items.Count
        ElseIf intEnd < 0 Then
            intEnd = 1
        End If

        Dim iCnt As Integer = intStart

        For Each btn As ItemButton In pnlMain.Controls
            If iCnt > intEnd Then
                btn.Visible = False
            Else
                btn.Visible = True

                objcbodata.SelectedIndex = iCnt - 1

                Dim ActiveRow As DataRowView = CType(objcbodata.SelectedItem, DataRowView)

                btn.UnkID = Val(ActiveRow.Item(colId))
                btn.Index = iCnt - 1
                btn.Caption = ActiveRow.Item(colName)

                Try
                    If (colShowCaption = "") OrElse _
                                        CBool(ActiveRow.Item(colShowCaption)) Then
                        btn.Text = btn.Caption
                    Else
                        btn.Text = ""
                    End If
                Catch ex As Exception
                    btn.Text = btn.Caption
                End Try

                Try
                    If colBackColor <> "" Then
                        btn.ShineColor = Color.FromArgb(CInt(ActiveRow.Item(colBackColor)))
                        btn.BackColor = btn.ShineColor
                    Else
                        btn.ShineColor = m_clrItem
                        btn.BackColor = btn.ShineColor
                    End If
                Catch ex As Exception
                End Try

                btn.OriginalBackColor = btn.BackColor
                Try
                    If colForecolor <> "" Then
                        btn.ForeColor = Color.FromArgb(CInt(ActiveRow.Item(colForecolor)))
                    End If
                Catch ex As Exception
                End Try

                btn.OriginalForeColor = btn.ForeColor
                Try
                    If colTextAlign <> "" Then
                        btn.TextAlign = DirectCast(ActiveRow.Item(colTextAlign), ContentAlignment)
                    End If
                Catch ex As Exception
                End Try


                Dim intFName As String = btn.Font.FontFamily.Name
                Dim intFSize As Single = btn.Font.Size
                Dim intFStyle As FontStyle = btn.Font.Style
                Try
                    If colFontName <> "" Then
                        intFName = ActiveRow.Item(colFontName).ToString
                    End If

                    If colFontSize <> "" Then
                        intFSize = ActiveRow.Item(colFontSize).ToString
                    End If

                    If colFontStyle <> "" Then
                        intFStyle = DirectCast([Enum].Parse(GetType(FontStyle), _
                                    ActiveRow.Item(colFontStyle).Value), FontStyle)

                    End If
                Catch ex As Exception
                Finally
                    btn.Font = New Font(intFName, intFSize, intFStyle)
                End Try

                Try
                    If colImage <> "" Then
                        btn.Image = data2Image(ActiveRow.Item(colImage))
                    End If
                Catch ex As Exception
                End Try

                Try
                    If colShrinkImage <> "" Then
                        btn.StretchImage = CBool(ActiveRow.Item(colShrinkImage))
                    End If
                Catch ex As Exception

                End Try
                Try
                    If colMaximumOrderQty <> "" Then
                        btn.MaximumOrderQty = CInt(ActiveRow.Item(colMaximumOrderQty))
                    Else
                        btn.MaximumOrderQty = 0
                    End If
                Catch ex As Exception
                End Try

                If (m_intSelectedIndex = btn.Index And intFindBy <= 0) Or _
                   (m_intSelectedID = btn.UnkID And intFindBy = 1) Or _
                   (m_intSelectedText = btn.Caption And intFindBy = 2) Then

                    m_intSelectedID = btn.UnkID
                    m_intSelectedIndex = btn.Index
                    m_intSelectedText = btn.Caption


                    btn.InnerBorderColor = Color.LawnGreen
                    btn.BackColor = Color.LawnGreen
                    btn.ForeColor = Color.White

                    'btn.InnerBorderColor = Color.Black
                    'btn.OuterBorderColor = Color.Black
                Else
                    If m_blnShowOutline Then
                        btn.InnerBorderColor = Color.Transparent
                        btn.OuterBorderColor = Color.White
                    Else
                        btn.InnerBorderColor = btn.BackColor
                        btn.OuterBorderColor = btn.BackColor
                    End If
                End If
            End If
            iCnt += 1
        Next

    End Sub


    ''' <summary>
    ''' Convert database image data field value into image.
    ''' </summary>
    ''' <param name="Expression">A image data field value.</param> 
    ''' <returns>A image which is converted from image data field value.</returns>
    Public Shared Function data2Image(ByVal Expression As Object) As Image
        If IsDBNull(Expression) Then
            Return Nothing
        Else
            Dim imgContent() As Byte = Expression
            Dim stream As New System.IO.MemoryStream(imgContent)
            Dim image As New System.Drawing.Bitmap(stream)
            Return image
        End If
    End Function
#End Region

#Region " Overrides Events "

    Public Overrides Sub Refresh()
        setPage(mintCurrentPage, -1)
        MyBase.Refresh()
    End Sub

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        setControls()
        MyBase.OnResize(e)
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        MyBase.OnPaint(e)

        If m_intBorderWidth > 0 Then
            e.Graphics.DrawRectangle(New Pen(m_clrBorderColor, m_intBorderWidth * 2), New Rectangle(0.5, 0.5, Width - 1, Height - 1))

            Select Case m_Orientation
                Case enOrientation.None
                    e.Graphics.DrawRectangle(New Pen(m_clrBorderColor, m_intBorderWidth), _
                            New Rectangle(0, _
                                            Height - m_intCotrolButtonSize - m_intBorderWidth - (mintButtonGap * 2), _
                                            Width, _
                                            m_intCotrolButtonSize + (mintButtonGap * 2)))

                Case enOrientation.Vertical

                Case enOrientation.Horizontal

            End Select
        End If


    End Sub

#End Region

End Class





