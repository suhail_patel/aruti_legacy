Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Windows.Forms



''' <summary> 
''' Summary description for eZeeStraightLine 
''' </summary> 
Public Class eZeeStraightLine
    Inherits Control

    Private m_thickness As Integer
    Private m_antiAlias As Boolean
    Private pen As Pen

    Public Sub New()
        Me.Name = "eZeeStraightLine"
        SetStyle(ControlStyles.SupportsTransparentBackColor Or ControlStyles.ResizeRedraw, True)
        SetStyle(ControlStyles.Selectable, False)
        Thickness = 1
        m_antiAlias = True
        BackColor = Color.Transparent
        m_lineType = StraightLineTypes.Horizontal
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso pen IsNot Nothing Then
            pen.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region " Property "
    <Category("Line Properties"), DefaultValue(True), Description("Specifies whether the line is drawn with anti-aliasing")> _
    Public Property AntiAlias() As Boolean
        Get
            Return m_antiAlias
        End Get
        Set(ByVal value As Boolean)
            m_antiAlias = value
            pen = New Pen(ForeColor, Thickness)
            Invalidate()
        End Set
    End Property

    <Category("Line Properties"), DefaultValue(1), Description("Specifies the thickness of the line")> _
    Public Property Thickness() As Integer
        Get
            Return m_thickness
        End Get
        Set(ByVal value As Integer)
            m_thickness = value
            pen = New Pen(ForeColor, Thickness)
            Invalidate()
        End Set
    End Property

    <Category("Line Properties"), DefaultValue(GetType(StraightLineTypes), "StraightLineTypes.Horizontal"), Description("Specifies the type of line the control will display")> _
   Public Property LineType() As StraightLineTypes
        Get
            Return m_lineType
        End Get
        Set(ByVal value As StraightLineTypes)
            m_lineType = value
            pen = New Pen(ForeColor, Thickness)
            Invalidate()
        End Set
    End Property
#End Region

#Region " Paint "
    Private m_lineType As StraightLineTypes

    Protected Overloads Sub LineBase_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        If AntiAlias Then
            e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        End If

        Select Case m_lineType
            Case StraightLineTypes.Horizontal
                DrawCenteredHorizontalLine(e.Graphics)
                Exit Select
            Case StraightLineTypes.Vertical
                DrawCenteredVerticalLine(e.Graphics)
                Exit Select
            Case StraightLineTypes.DiagonalAscending
                DrawCenteredDiagonalAscendingLine(e.Graphics)
                Exit Select
            Case StraightLineTypes.DiagonalDescending
                DrawCenteredDiagonalDescendingLine(e.Graphics)
                Exit Select
            Case Else
                Exit Select
        End Select
    End Sub

    Private Sub DrawCenteredHorizontalLine(ByVal g As Graphics)
        g.DrawLine(pen, 0, Height \ 2, Width, Height \ 2)
    End Sub

    Private Sub DrawCenteredVerticalLine(ByVal g As Graphics)
        g.DrawLine(pen, Width \ 2, 0, Width \ 2, Height)
    End Sub

    Private Sub DrawCenteredDiagonalAscendingLine(ByVal g As Graphics)
        g.DrawLine(pen, 0, Height, Width, 0)
    End Sub

    Private Sub DrawCenteredDiagonalDescendingLine(ByVal g As Graphics)
        g.DrawLine(pen, 0, 0, Width, Height)
    End Sub
#End Region

End Class