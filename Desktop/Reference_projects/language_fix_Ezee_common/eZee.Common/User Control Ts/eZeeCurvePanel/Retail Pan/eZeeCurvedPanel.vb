Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

<ComVisible(False), ToolboxItem(True), Localizable(True), ToolboxBitmap(GetType(Panel)), Designer(GetType(eZeeCurvedPanelDesigner))> _
Public Class eZeeCurvedPanel
    Inherits Panel

    ' Methods
    Public Sub New()
        MyBase.SetStyle((ControlStyles.OptimizedDoubleBuffer Or (ControlStyles.AllPaintingInWmPaint Or (ControlStyles.SupportsTransparentBackColor Or (ControlStyles.Selectable Or (ControlStyles.ResizeRedraw Or ControlStyles.UserPaint))))), True)
        Me._insetMargin = New Padding(1)
        Me._borderWidth = 1
        Me._antiAlias = True
        MyBase.BorderStyle = Windows.Forms.BorderStyle.None
        Me._fillColor = SystemColors.InactiveCaption
        Me._highlightColor = SystemColors.InactiveCaptionText
        Me._borderColor = SystemColors.ControlText
        Me._drawBorder = True
        Me._fillTransparency = 0
        Me._cornerSettings = New CornerSettings

        Me._cornerSettings.SettingChanged = DirectCast([Delegate].Combine(Me._cornerSettings.SettingChanged, New EventHandler(AddressOf Me.OnCornerSettingsChanged)), EventHandler)
        Me.BuildBorderPen()
        Me.BuildPaths()
        Me.BuildFillBrush()
    End Sub


    Protected Sub BuildBorderPen()
        Me._borderPen = New Pen(Me._borderColor, CSng(Me._borderWidth))
    End Sub

    Protected Sub BuildFillBrush()
        Dim bounds As RectangleF = Me._fillPath.GetBounds
        If ((bounds.Height > 0.0!) AndAlso (bounds.Width > 0.0!)) Then
            Dim highlightColor As Color
            Dim fillColor As Color
            If (Me.FillTransparency <> 0) Then
                fillColor = Color.FromArgb(CInt((Me.FillTransparency * 255)), Me.FillColor)
                highlightColor = Color.FromArgb(CInt((Me.FillTransparency * 255)), Me._highlightColor)
            Else
                fillColor = Me.FillColor
                highlightColor = Me._highlightColor
            End If
            Me._fillBrush = New SolidBrush(fillColor)
        End If
    End Sub

    Protected Sub BuildPaths()
        Dim rectangle As New Rectangle(Me._insetMargin.Left, Me._insetMargin.Top, ((MyBase.Width - (Me._insetMargin.Left + Me._insetMargin.Right)) - 1), ((MyBase.Height - (Me._insetMargin.Top + Me._insetMargin.Bottom)) - 1))
        Dim path As New GraphicsPath
        If Me._cornerSettings.TopLeft Then
            path.AddArc(rectangle.X, rectangle.Y, Me._cornerSettings.Radius, Me._cornerSettings.Radius, 180.0!, 90.0!)
        Else
            path.AddLine(rectangle.X, rectangle.Y, rectangle.X, rectangle.Y)
        End If
        If Me._cornerSettings.TopRight Then
            path.AddArc((rectangle.Right - Me._cornerSettings.Radius), rectangle.Y, Me._cornerSettings.Radius, Me._cornerSettings.Radius, 270.0!, 90.0!)
        Else
            path.AddLine(rectangle.Right, rectangle.Y, rectangle.Right, rectangle.Y)
            'path.AddString("Test", Me.Font.FontFamily, 1, 12, rectangle, New StringFormat)
        End If
        If Me._cornerSettings.BottomRight Then
            path.AddArc((rectangle.Right - Me._cornerSettings.Radius), (rectangle.Bottom - Me._cornerSettings.Radius), Me._cornerSettings.Radius, Me._cornerSettings.Radius, 0.0!, 90.0!)
        Else
            path.AddLine(rectangle.Right, rectangle.Bottom, rectangle.Right, rectangle.Bottom)
        End If
        If Me._cornerSettings.BottomLeft Then
            path.AddArc(rectangle.X, (rectangle.Bottom - Me._cornerSettings.Radius), Me._cornerSettings.Radius, Me._cornerSettings.Radius, 90.0!, 90.0!)
        Else
            path.AddLine(rectangle.X, rectangle.Bottom, rectangle.X, rectangle.Bottom)
        End If
        path.CloseFigure()
        Me._fillPath = path
        Me._borderPath = TryCast(path.Clone, GraphicsPath)
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            Me._cornerSettings.SettingChanged = DirectCast([Delegate].Remove(Me._cornerSettings.SettingChanged, New EventHandler(AddressOf Me.OnCornerSettingsChanged)), EventHandler)

            If (Not Me._fillBrush Is Nothing) Then
                Me._fillBrush.Dispose()
                Me._fillBrush = Nothing
            End If
            If (Not Me._borderPen Is Nothing) Then
                Me._borderPen.Dispose()
                Me._borderPen = Nothing
            End If
            If (Not Me._fillPath Is Nothing) Then
                Me._fillPath.Dispose()
                Me._fillPath = Nothing
            End If
            If (Not Me._borderPath Is Nothing) Then
                Me._borderPath.Dispose()
                Me._borderPath = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private Sub OnCornerSettingsChanged(ByVal sender As Object, ByVal eventArgs As EventArgs)
        Me.BuildPaths()
    End Sub

    Protected Overrides Sub OnPaint(ByVal pe As PaintEventArgs)
        pe.Graphics.Clip = New Region(Me._borderPath)
        MyBase.OnPaint(pe)
    End Sub

    Protected Overrides Sub OnPaintBackground(ByVal pe As PaintEventArgs)
        MyBase.OnPaintBackground(pe)
        If Me._antiAlias Then
            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias
        End If
        If ((Not Me._fillBrush Is Nothing) AndAlso (Not Me._fillPath Is Nothing)) Then
            pe.Graphics.FillPath(Me._fillBrush, Me._fillPath)
        End If
        If ((Me._drawBorder AndAlso (Not Me._borderPen Is Nothing)) AndAlso (Not Me._borderPath Is Nothing)) Then
            pe.Graphics.DrawPath(Me._borderPen, Me._borderPath)
        End If
        If MyBase.DesignMode Then
            Using pen As Pen = New Pen(SystemColors.GrayText)
                pen.DashStyle = DashStyle.Dot
                pe.Graphics.DrawRectangle(pen, MyBase.ClientRectangle.Left, MyBase.ClientRectangle.Top, (MyBase.ClientRectangle.Width - 1), (MyBase.ClientRectangle.Height - 1))
            End Using
        End If
    End Sub

    Protected Overrides Sub OnParentChanged(ByVal e As EventArgs)
        If (Not MyBase.Parent Is Nothing) Then
            MyBase.RightToLeft = MyBase.Parent.RightToLeft
            MyBase.Font = MyBase.Parent.Font
        End If
        MyBase.OnParentChanged(e)
    End Sub

    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.BuildPaths()
        Me.BuildFillBrush()
        MyBase.OnSizeChanged(e)
    End Sub


    ' Properties
    <Category("Appearance"), Browsable(True), DefaultValue(True), Description("Controls whether a border will be drawn around the fill rectangle."), [ReadOnly](False)> _
    Public Property DrawFillBorder() As Boolean
        Get
            Return Me._drawBorder
        End Get
        Set(ByVal value As Boolean)
            Me._drawBorder = value
            Me.BuildBorderPen()
            Me.BuildPaths()
            MyBase.Invalidate()
        End Set
    End Property

    <Browsable(True), Description("Controls the color of the border drawn around the fill rectangle."), [ReadOnly](False), Category("Appearance")> _
    Public Property FillBorderColor() As Color
        Get
            Return Me._borderColor
        End Get
        Set(ByVal value As Color)
            Me._borderColor = value
            Me.BuildBorderPen()
            MyBase.Invalidate()
        End Set
    End Property

    <Description("The color to fill the panel."), Category("Appearance"), [ReadOnly](False), Browsable(True)> _
    Public Property FillColor() As Color
        Get
            Return Me._fillColor
        End Get
        Set(ByVal value As Color)
            If (Me.FillColor <> value) Then
                Me._fillColor = value
                Me.BuildFillBrush()
                MyBase.Invalidate()
            End If
        End Set
    End Property

    <Description("The starting color in the panel's filled gradient."), Browsable(True), Category("Appearance"), [ReadOnly](False)> _
    Public Property FillHighlightColor() As Color
        Get
            Return Me._highlightColor
        End Get
        Set(ByVal value As Color)
            Me._highlightColor = value
            Me.BuildFillBrush()
            MyBase.Invalidate()
        End Set
    End Property

    <Browsable(True), Description("Controls the margin between the panel border and fill rectangle."), [ReadOnly](False), Category("Appearance"), DefaultValue(GetType(Padding), "{Left=1,Top=1,Right=1,Bottom=1}")> _
    Public Property FillInset() As System.Windows.Forms.Padding
        Get
            Return Me._insetMargin
        End Get
        Set(ByVal value As System.Windows.Forms.Padding)
            Me._insetMargin = value
            Me.BuildPaths()
            Me.BuildFillBrush()
            MyBase.Invalidate()
        End Set
    End Property

    <Category("Appearance"), Browsable(True), DefaultValue(CDbl(0)), Description("Controls the transparency level of the fill color of the panel."), [ReadOnly](False)> _
    Public Property FillTransparency() As Double
        Get
            Return Me._fillTransparency
        End Get
        Set(ByVal value As Double)
            Me._fillTransparency = value
            If (Me._fillTransparency > 1) Then
                Me._fillTransparency = 1
            ElseIf (Me._fillTransparency < 0) Then
                Me._fillTransparency = 0
            End If
            MyBase.Invalidate()
        End Set
    End Property

    <Localizable(True), Browsable(True), [ReadOnly](True)> _
    Public Overrides Property RightToLeft() As RightToLeft
        Get
            Return MyBase.RightToLeft
        End Get
        Set(ByVal value As RightToLeft)
            MyBase.RightToLeft = value
        End Set
    End Property

    <Browsable(True), Category("Appearance"), [ReadOnly](False), Description("Controls how the corners are rounded.")> _
    Public Property RoundedCorners() As CornerSettings
        Get
            Return Me._cornerSettings
        End Get
        Set(ByVal value As CornerSettings)
            Me._cornerSettings = value
        End Set
    End Property

    Public Property BorderWidth() As Integer
        Get
            Return _borderWidth
        End Get
        Set(ByVal value As Integer)
            _borderWidth = value
            Call BuildBorderPen()
            MyBase.Invalidate()
        End Set
    End Property

    ' Fields
    Private _antiAlias As Boolean
    Private _borderColor As Color
    Private _borderPath As GraphicsPath
    Private _borderPen As Pen
    Private _borderWidth As Integer
    Private _cornerSettings As CornerSettings
    Private _drawBorder As Boolean
    Private _fillBrush As Brush
    Private _fillColor As Color
    Private _fillPath As GraphicsPath
    Private _fillTransparency As Double
    Private _highlightColor As Color
    Private _insetMargin As System.Windows.Forms.Padding
End Class
