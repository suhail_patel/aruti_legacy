Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Public Class eZee_ImageButton
    Inherits System.Windows.Forms.Button

    Private _Image As Image
    Private _StatusImage As Image
    Private _IsNetLock As Boolean = False
    Private _MouseStatus As Integer = 0
    Private _Selected As Boolean = False
    Private _Checked As Boolean = False

#Region " Constructor "

    Public Sub New()

        FlatStyle = Windows.Forms.FlatStyle.Flat
        FlatAppearance.BorderSize = 0
        Margin = New Padding(0)

        SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.OptimizedDoubleBuffer Or ControlStyles.ResizeRedraw Or ControlStyles.SupportsTransparentBackColor Or ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.Opaque, False)
    End Sub

    Protected Overridable Sub PostFilterProperties(ByVal properties As IDictionary)
        properties.Remove("Image")
    End Sub


#End Region

#Region " Overload Property "

    '<Browsable(False)> _
    'Public Shadows WriteOnly Property Image() As Image
    '    Set(ByVal value As Image)
    '    End Set
    'End Property

    <Browsable(True)> _
    Public Shadows Property BackImage() As Image
        Get
            Return _Image
        End Get
        Set(ByVal value As Image)
            _Image = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(False)> _
    Public Shadows Property StatusImage() As Image
        Get
            Return _StatusImage
        End Get
        Set(ByVal value As Image)
            _StatusImage = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(False)> _
    Public Shadows Property Netlock() As Boolean
        Get
            Return _IsNetLock
        End Get
        Set(ByVal value As Boolean)
            _IsNetLock = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(False)> _
    Public Property Checked() As Boolean
        Get
            Return _Checked
        End Get
        Set(ByVal value As Boolean)
            If _Checked <> value Then
                _Checked = value
                _Selected = _Checked
                If _Checked = False Then _MouseStatus = 0
                Me.Invalidate()
            End If
        End Set
    End Property

    Private mstrTimerText As String = ""

    Public Property TimerText() As String
        Get
            Return mstrTimerText
        End Get
        Set(ByVal value As String)
            mstrTimerText = value
        End Set
    End Property

    Private mclsBorderColor As Color = Nothing
    Public Property BorderColor() As Color
        Get
            Return mclsBorderColor
        End Get
        Set(ByVal value As Color)
            mclsBorderColor = value
        End Set
    End Property

#End Region

#Region " Overload Events "

    Protected Overrides Sub OnBackColorChanged(ByVal e As System.EventArgs)
        MyBase.OnBackColorChanged(e)
        Me.Invalidate()
    End Sub

    'Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
    '    MyBase.OnClick(e)
    '    _Selected = True
    '    Me.Invalidate()
    'End Sub

    Protected Overrides Sub OnDoubleClick(ByVal e As System.EventArgs)
        MyBase.OnDoubleClick(e)
        _Selected = True
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnGotFocus(ByVal e As System.EventArgs)
        MyBase.OnGotFocus(e)
        _Selected = True
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
        MyBase.OnLostFocus(e)
        _Selected = False
        _MouseStatus = 0
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDown(e)
        _MouseStatus = 2
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)
        _MouseStatus = 1
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        MyBase.OnMouseLeave(e)
        _MouseStatus = 0
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseUp(e)
        _MouseStatus = 1
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        Call DrawForegroundFromButton(e)
        Call CreateBackgroundFrame(e.Graphics)
        Call DrawButtonForeground(e.Graphics)
    End Sub

#End Region

#Region " Drawing Task Function "

    Private Sub DrawButtonForeground(ByVal g As Graphics)
        If Focused AndAlso ShowFocusCues Then
            Dim rect As Rectangle = ClientRectangle
            rect.Inflate(-4, -4)
            ControlPaint.DrawFocusRectangle(g, rect)
        End If
    End Sub

    Public Sub DrawStateImage(ByVal g As Graphics)
        If Not _StatusImage Is Nothing Then
            g.DrawImage(_StatusImage, 1, 1, Width \ 2, Width \ 2)
        End If
    End Sub

    Public Sub DrawNetLockImage(ByVal g As Graphics)
        If _IsNetLock Then
            g.DrawImage(My.Resources.NetLock, (Width * 2 \ 3) - 1, (Width * 2 \ 3) - 1, Width \ 3, Width \ 3)
        End If
    End Sub

    Public Sub CreateBackgroundFrame(ByVal g As Graphics)
        Dim BackRect As New Rectangle(0, 0, Me.Width, Me.Height)
        ' g.Clear(Color.Transparent)

        Select Case _MouseStatus
            Case 1
                Using lgb As New Drawing.SolidBrush(Color.FromArgb(60, 255, 255, 255))
                    g.FillRectangle(lgb, BackRect)
                End Using
            Case 2
                Using lgb As New Drawing.SolidBrush(Color.FromArgb(60, 0, 0, 0))
                    g.FillRectangle(lgb, BackRect)
                End Using
            Case Else
        End Select

        If Not mclsBorderColor = Nothing Then
            g.DrawRectangle(New Pen(mclsBorderColor, 8), BackRect)
        End If

        If _Checked Then
            g.DrawRectangle(New Pen(Color.White, 4), BackRect)
        End If

    End Sub

    Private Sub DrawForegroundFromButton(ByVal pevent As PaintEventArgs)
        Using imageButton As Button = New Button()
            imageButton.FlatAppearance.BorderSize = 0
            imageButton.FlatStyle = Windows.Forms.FlatStyle.Flat
            imageButton.BackColor = BackColor
            imageButton.AutoEllipsis = AutoEllipsis
            If Enabled Then
                imageButton.ForeColor = ForeColor
            Else
                imageButton.ForeColor = Color.FromArgb((3 * ForeColor.R + BackColor.R) >> 2, (3 * ForeColor.G + BackColor.G) >> 2, (3 * ForeColor.B + BackColor.B) >> 2)
            End If

            imageButton.Font = Font
            imageButton.RightToLeft = RightToLeft

            Dim img As New Bitmap(Width + 1, Height + 1)

            Using g As Graphics = Graphics.FromImage(img)
                g.Clear(Color.Transparent)
                If Not _Image Is Nothing Then
                    g.DrawImage(_Image, 0, 0, img.Width, img.Height)
                End If

                DrawStateImage(g)
                DrawNetLockImage(g)

                If mstrTimerText.Trim <> "" Then
                    g.DrawString(mstrTimerText, Font, New SolidBrush(ForeColor), 0, Size.Height - 15)
                End If
            End Using

            imageButton.Image = img
            imageButton.ImageAlign = ImageAlign
            imageButton.ImageIndex = ImageIndex
            imageButton.ImageKey = ImageKey
            imageButton.ImageList = ImageList
            imageButton.Padding = Padding
            imageButton.Size = Size
            imageButton.Text = Text
            imageButton.TextAlign = TextAlign
            imageButton.TextImageRelation = TextImageRelation
            imageButton.UseCompatibleTextRendering = UseCompatibleTextRendering
            imageButton.UseMnemonic = UseMnemonic
            InvokePaint(imageButton, pevent)
        End Using
    End Sub

#End Region

End Class
