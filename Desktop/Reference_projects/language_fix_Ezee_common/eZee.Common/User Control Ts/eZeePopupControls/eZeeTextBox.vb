'************************************************************************************************************************************
'Class Name : eZeeTextBox
'Purpose    : 
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************

Imports System.ComponentModel

Imports System.Drawing
Imports System.Windows.Forms

<DefaultProperty("Text"), DefaultEvent("TextChanged")> _
Public Class eZeeTextBox
    Private Readonly mstrModuleName as  String  = "eZeeTextBox"

    Private m_blnShowButton As Boolean = True
    Private m_strPasswordChar As Char = ""

    Dim m_strCaption As String = "Enter Text"
    Dim m_strOkCaption As String = "Finish"
    Dim m_strCancelCaption As String = "Cancel"

#Region " Properties "
    Public Property Caption() As String
        Get
            Return m_strCaption
        End Get
        Set(ByVal value As String)
            m_strCaption = value
        End Set
    End Property

    Public Property Ok_Caption() As String
        Get
            Return m_strOkCaption
        End Get
        Set(ByVal value As String)
            m_strOkCaption = value
        End Set
    End Property

    Public Property Cancel_Caption() As String
        Get
            Return m_strCancelCaption
        End Get
        Set(ByVal value As String)
            m_strCancelCaption = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Text() As String
        Get
            Return txt1.Text
        End Get
        Set(ByVal value As String)
            txt1.Text = value
        End Set
    End Property

    Public Property Multiline() As Boolean
        Get
            Return txt1.Multiline
        End Get
        Set(ByVal value As Boolean)
            txt1.Multiline = value
        End Set
    End Property

    Public Property PasswordChar() As Char
        Get
            Return m_strPasswordChar
        End Get
        Set(ByVal value As Char)
            m_strPasswordChar = value
            txt1.PasswordChar = m_strPasswordChar
        End Set
    End Property

    Public Shadows Property RightToLeft() As RightToLeft
        Get
            Return txt1.RightToLeft
        End Get
        Set(ByVal value As RightToLeft)
            txt1.RightToLeft = value
        End Set
    End Property

    Public Property BorderWidth() As Integer
        Get
            Return pnlBorder.Padding.All
        End Get
        Set(ByVal value As Integer)
            pnlBorder.Padding = New Padding(value)
        End Set
    End Property

    Public Property BroderColor() As Color
        Get
            Return pnlBorder.BackColor
        End Get
        Set(ByVal value As Color)
            pnlBorder.BackColor = value
        End Set
    End Property

    Public Property BackGroundColor() As Color
        Get
            Return txt1.BackColor
        End Get
        Set(ByVal value As Color)
            If value <> Color.Transparent Then
                pnlText.BackColor = value
                txt1.BackColor = value
            End If
        End Set
    End Property

    Public Shadows Property ForeColor() As Color
        Get
            Return txt1.ForeColor
        End Get
        Set(ByVal value As Color)
            txt1.ForeColor = value
        End Set
    End Property

    Public Property TextAlign() As HorizontalAlignment
        Get
            Return txt1.TextAlign
        End Get
        Set(ByVal value As HorizontalAlignment)
            txt1.TextAlign = value
        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return txt1.MaxLength
        End Get
        Set(ByVal value As Integer)
            txt1.MaxLength = value
        End Set
    End Property

    Public Property [ReadOnly]() As Boolean
        Get
            Return txt1.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txt1.ReadOnly = value
        End Set
    End Property

    Public Property ShowButton() As Boolean
        Get
            Return m_blnShowButton
        End Get
        Set(ByVal value As Boolean)
            m_blnShowButton = value
            pnlBtn.Visible = m_blnShowButton
        End Set
    End Property

    Public Property InvalidChars() As Char()
        Get
            Return txt1.InvalidChars
        End Get
        Set(ByVal value As Char())
            txt1.InvalidChars = value
        End Set
    End Property

    Public Property SelectionMode() As Boolean
        Get
            Return txt1.SelectionMode
        End Get
        Set(ByVal value As Boolean)
            txt1.SelectionMode = value
        End Set
    End Property

    Public Property SelectionStart() As Integer
        Get
            Return txt1.SelectionStart
        End Get
        Set(ByVal value As Integer)
            txt1.SelectionStart = value
        End Set
    End Property

    Public Property SelectionLength() As Integer
        Get
            Return txt1.SelectionLength
        End Get
        Set(ByVal value As Integer)
            txt1.SelectionLength = value
        End Set
    End Property
#End Region

#Region " Buttons "
    Private Sub btnKeyBoard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeyBoard.Click
        Try
            Using OpenDialog_From As New frmKeyBord
                OpenDialog_From._Caption = m_strCaption
                OpenDialog_From._Ok_Caption = m_strOkCaption
                OpenDialog_From._Cancel_Caption = m_strCancelCaption

                If (txt1.PasswordChar = Nothing) Or txt1.PasswordChar.ToString.Trim = "" Then
                    OpenDialog_From.IsPassword = False
                Else
                    OpenDialog_From.IsPassword = True
                End If
                OpenDialog_From._Text = txt1.Text

                If OpenDialog_From.DisplayDialog() Then
                    txt1.Text = OpenDialog_From._Text
                End If

            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Events "
    Public Shadows Event GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    Public Shadows Event KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Public Shadows Event KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

    Private Sub txt1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt1.GotFocus
        RaiseEvent GotFocus(Me, e)
    End Sub

    Private Sub txt1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt1.LostFocus
        RaiseEvent LostFocus(Me, e)
    End Sub

    Private Sub txt1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt1.TextChanged
        RaiseEvent TextChanged(Me, e)
    End Sub

    Private Sub txt1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt1.KeyPress
        RaiseEvent KeyPress(Me, e)
    End Sub

    Private Sub txt1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt1.KeyUp
        If e.KeyCode = Keys.F5 Then
            Call btnKeyBoard_Click(Nothing, Nothing)
        End If
        RaiseEvent KeyUp(Me, e)
    End Sub

    Private Sub txt1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt1.KeyDown
        RaiseEvent KeyDown(Me, e)
    End Sub
#End Region

   
    
End Class
