<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDatePopUp
    Inherits frmBasePopup

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDatePopUp))
        Me.dtpMain = New eZee.Calendar.MonthCalendar
        Me.btnCancel = New eZee.Common.eZeeButton
        Me.btnFinish = New eZee.Common.eZeeButton
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpMain
        '
        Me.dtpMain.ActiveMonth.Month = 6
        Me.dtpMain.ActiveMonth.Year = 2007
        Me.dtpMain.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.dtpMain.Footer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.dtpMain.Header.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.dtpMain.Header.BackColor2 = System.Drawing.SystemColors.GradientInactiveCaption
        Me.dtpMain.Header.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpMain.Header.GradientMode = eZee.Calendar.mcGradientMode.Vertical
        Me.dtpMain.Header.TextColor = System.Drawing.Color.White
        Me.dtpMain.ImageList = Nothing
        Me.dtpMain.Location = New System.Drawing.Point(6, 1)
        Me.dtpMain.MaxDate = New Date(2017, 6, 26, 15, 5, 4, 138)
        Me.dtpMain.MinDate = New Date(1997, 6, 26, 15, 5, 4, 138)
        Me.dtpMain.Month.BackgroundImage = Nothing
        Me.dtpMain.Month.BorderStyles.Normal = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.dtpMain.Month.BorderStyles.Selected = System.Windows.Forms.ButtonBorderStyle.Inset
        Me.dtpMain.Month.Colors.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.dtpMain.Month.Colors.Days.BackColor2 = System.Drawing.Color.Silver
        Me.dtpMain.Month.Colors.Days.Border = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.dtpMain.Month.Colors.Days.GradientMode = eZee.Calendar.mcGradientMode.Vertical
        Me.dtpMain.Month.Colors.Disabled.GradientMode = eZee.Calendar.mcGradientMode.Horizontal
        Me.dtpMain.Month.Colors.Focus.BackColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(189, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.dtpMain.Month.Colors.Focus.Border = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.dtpMain.Month.Colors.Selected.BackColor = System.Drawing.Color.LightCoral
        Me.dtpMain.Month.Colors.Selected.Border = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.dtpMain.Month.Colors.Trailing.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.dtpMain.Month.Colors.Trailing.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.dtpMain.Month.DateFont = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpMain.Month.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.dtpMain.Name = "dtpMain"
        Me.dtpMain.SelectionMode = eZee.Calendar.mcSelectionMode.One
        Me.dtpMain.SelectTrailingDates = False
        Me.dtpMain.ShowFooter = False
        Me.dtpMain.ShowToday = False
        Me.dtpMain.ShowWeekdays = False
        Me.dtpMain.Size = New System.Drawing.Size(350, 322)
        Me.dtpMain.TabIndex = 0
        Me.dtpMain.Weekdays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.dtpMain.Weekdays.TextColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.dtpMain.Weeknumbers.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.dtpMain.Weeknumbers.TextColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(220, Byte), Integer))
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.Red
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnCancel.GlowColor = System.Drawing.Color.MistyRose
        Me.btnCancel.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnCancel.Location = New System.Drawing.Point(243, 334)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.ShineColor = System.Drawing.Color.LightCoral
        Me.btnCancel.Size = New System.Drawing.Size(113, 34)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'btnFinish
        '
        Me.btnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinish.BackColor = System.Drawing.Color.Green
        Me.btnFinish.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnFinish.GlowColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnFinish.Location = New System.Drawing.Point(124, 334)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.ShineColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.Size = New System.Drawing.Size(113, 34)
        Me.btnFinish.TabIndex = 1
        Me.btnFinish.Text = "Finish"
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Transparent
        Me.pnlMain.Controls.Add(Me.btnCancel)
        Me.pnlMain.Controls.Add(Me.btnFinish)
        Me.pnlMain.Controls.Add(Me.dtpMain)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(1, 35)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Padding = New System.Windows.Forms.Padding(2)
        Me.pnlMain.Size = New System.Drawing.Size(363, 375)
        Me.pnlMain.TabIndex = 0
        '
        'frmDatePopUp
        '
        Me.AcceptButton = Me.btnFinish
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(365, 411)
        Me.Controls.Add(Me.pnlMain)
        Me.Name = "frmDatePopUp"
        Me.Text = "Select Date"
        Me.pnlMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtpMain As eZee.Calendar.MonthCalendar
    Friend WithEvents btnCancel As eZeeButton
    Friend WithEvents btnFinish As eZeeButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel

End Class
