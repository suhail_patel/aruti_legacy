Imports System.Drawing

Public Class frmBasePopup

    Private _color1 As Color = Color.Black
    Private _color2 As Color = Color.Gray
    Private _colorFooter As Color = Color.Teal

    Private _BorderWidth As Integer = 2
    Private _image As Image

    Private _TitleHeight As Integer = 34

    Private formMove As Point

    Private _TitleRect As Rectangle
    Private _ClientRect As Rectangle
    Private _BorderRect As Rectangle
    Private _ButtonRect As Rectangle

#Region " Property "
    Private mstrHelpTag As String = ""
    Public Property _HelpTag() As String
        Get
            Return mstrHelpTag
        End Get
        Set(ByVal value As String)
            mstrHelpTag = value
        End Set
    End Property

    Public Overloads Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal value As String)
            MyBase.Text = value
            Call CreateBackgroundPicture()
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Call CreateBackgroundPicture()
    End Sub

    Protected Overrides Sub Finalize()
        _image.Dispose()
        MyBase.Finalize()
    End Sub
#End Region

#Region " Overrides Events "
    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        MyBase.OnResize(e)
        If Me.DesignMode Then
            Call CreateBackgroundPicture()
        End If
    End Sub
#End Region

#Region " Background "
    Protected Sub CreateBackgroundPicture()
        Try
            _ClientRect = New Rectangle(0, 0, Me.Width, Me.Height)
            _TitleRect = New Rectangle(_BorderWidth, _BorderWidth, Me.Width - (_BorderWidth * 2), _TitleHeight - (_BorderWidth * 2))
            _BorderRect = New Rectangle(_BorderWidth, _BorderWidth, Me.Width - (_BorderWidth * 2), Me.Height - (_BorderWidth * 2))
            _ButtonRect = New Rectangle(Me.Width - 26 - (_BorderWidth * 2), _BorderWidth * 2, _TitleHeight - (_BorderWidth * 6), _TitleHeight - (_BorderWidth * 6))

            If Not _image Is Nothing Then _image.Dispose()
            _image = New Bitmap(Me.Width, Me.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb)
        Catch
            Return
        End Try

        Using gfx As Graphics = Graphics.FromImage(_image)
            Using br As New SolidBrush(Me.BackColor)
                gfx.FillRectangle(br, _ClientRect)
            End Using

            Using br As New SolidBrush(Me._color1)
                gfx.FillRectangle(br, _TitleRect)
            End Using

            If Me._color1.Equals(Me._color2) Then
                'check if we need to calc the color slide 
            Else
                For i As Integer = 0 To _TitleRect.Height Step 2
                    ' 
                    ' calculate the new color to use (linear color mix) 
                    ' 
                    Dim colorR As Integer = CInt((Me._color2.R - Me._color1.R)) * i / _TitleRect.Height
                    Dim colorG As Integer = CInt((Me._color2.G - Me._color1.G)) * i / _TitleRect.Height
                    Dim colorB As Integer = CInt((Me._color2.B - Me._color1.B)) * i / _TitleRect.Height
                    Dim color As Color = Drawing.Color.FromArgb(Me._color1.R + colorR, Me._color1.G + colorG, Me._color1.B + colorB)

                    gfx.DrawLine(New Pen(New SolidBrush(color)), 0, i, Me.Width, i)
                Next
            End If

            'Title
            Using fmt As New StringFormat
                fmt.LineAlignment = StringAlignment.Center
                gfx.DrawString(Me.Text, New Font(Me.Font, FontStyle.Bold), New SolidBrush(Me.ForeColor), _TitleRect, fmt)
            End Using
            Using pn As New Pen(_color1, 2)
                gfx.DrawRectangle(pn, _TitleRect)
            End Using

            Using br As New Drawing2D.LinearGradientBrush(_ButtonRect, Color.Red, Color.Maroon, Drawing2D.LinearGradientMode.Vertical)
                gfx.FillRectangle(br, New Rectangle(_ButtonRect.X + 1, _ButtonRect.Y + 1, _ButtonRect.Width - 2, _ButtonRect.Height - 2))
            End Using

            Using pn As New Pen(Color.White, 2)
                gfx.DrawRectangle(pn, _ButtonRect)
            End Using

            'Footer
            Using br As New SolidBrush(Me._colorFooter)
                gfx.FillRectangle(br, _BorderWidth, Me.Height - 46 - _BorderWidth, Me.Width - (_BorderWidth * 2), 46)
            End Using

            Using pn As New Pen(_color1, 2)
                gfx.DrawRectangle(pn, _BorderRect)
            End Using
        End Using

        MyBase.BackgroundImage = _image

    End Sub
#End Region

#Region " Title bar Mouse Events "
    Dim blnMouseDown As Boolean = False
    Dim blnButtonClick As Boolean = False

    Private Sub frmBasePopup_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
        If blnMouseDown Then
            Location = New Point(Left - (formMove.X - e.X), Top - (formMove.Y - e.Y))
        End If
    End Sub

    Private Sub frmBasePopup_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        blnMouseDown = False
        blnButtonClick = False

        If e.Button = Windows.Forms.MouseButtons.Left Then
            If _ButtonRect.Contains(e.X, e.Y) Then
                blnButtonClick = True
                Me.Close()
            ElseIf _TitleRect.Contains(e.X, e.Y) Then
                blnMouseDown = True
                formMove = New Point(e.X, e.Y)
            End If
        End If
    End Sub

    Private Sub frmBasePopup_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        blnMouseDown = False
    End Sub
#End Region

#Region " Help "
    Private Sub ShowHelp()
        If eZeeFrom_Configuration._ShowHelpButton Then
            If mstrHelpTag <> "" Then
                System.Windows.Forms.Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, mstrHelpTag & ".htm")
            Else
                System.Windows.Forms.Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, "Help_Not_Found.htm")
            End If
        End If
    End Sub

    Private Sub TemplateForm_HelpButtonClicked(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.HelpButtonClicked
        e.Cancel = True
        Call Me.ShowHelp()
    End Sub

    Private Sub TemplateForm_HelpRequested(ByVal sender As System.Object, ByVal hlpevent As System.Windows.Forms.HelpEventArgs) Handles MyBase.HelpRequested
        Call Me.ShowHelp()
    End Sub
#End Region

End Class

