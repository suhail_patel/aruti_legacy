'************************************************************************************************************************************
'Class Name : eZeeNumericUpDown
'Purpose    : 
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************

Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

<DefaultProperty("Value"), DefaultEvent("ValueChanged")> _
Public Class eZeeNumericUpDown
    Private Readonly mstrModuleName as  String  = "eZeeNumericUpDown"

    Private mintMinimum As Integer = 0
    Private mintMaximum As Integer = 100

#Region " Properties "

    Public Shadows Property RightToLeft() As RightToLeft
        Get
            Return txt1.RightToLeft
        End Get
        Set(ByVal value As RightToLeft)
            txt1.RightToLeft = value
        End Set
    End Property

    Public Property BorderWidth() As Integer
        Get
            Return pnlBorder.Padding.All
        End Get
        Set(ByVal value As Integer)
            pnlBorder.Padding = New Padding(value)
        End Set
    End Property

    Public Property BroderColor() As Color
        Get
            Return pnlBorder.BackColor
        End Get
        Set(ByVal value As Color)
            pnlBorder.BackColor = value
        End Set
    End Property

    Public Property BackGroundColor() As Color
        Get
            Return txt1.BackColor
        End Get
        Set(ByVal value As Color)
            If value <> Color.Transparent Then
                pnlText.BackColor = value
                txt1.BackColor = value
            End If
        End Set
    End Property

    Public Shadows Property ForeColor() As Color
        Get
            Return txt1.ForeColor
        End Get
        Set(ByVal value As Color)
            txt1.ForeColor = value
        End Set
    End Property

    Public Property TextAlign() As HorizontalAlignment
        Get
            Return txt1.TextAlign
        End Get
        Set(ByVal value As HorizontalAlignment)
            txt1.TextAlign = value
        End Set
    End Property


    Public Property Minimum() As Integer
        Get
            Return mintMinimum
        End Get
        Set(ByVal value As Integer)
            mintMinimum = value
        End Set
    End Property

    Public Property Maximum() As Integer
        Get
            Return mintMaximum
        End Get
        Set(ByVal value As Integer)
            mintMaximum = value
        End Set
    End Property

    Public Property Value() As Integer
        Get
            Return txt1.NumericText
        End Get
        Set(ByVal value As Integer)
            txt1.Text = value
        End Set
    End Property

    Public Property SelectionMode() As Boolean
        Get
            Return txt1.SelectionMode
        End Get
        Set(ByVal value As Boolean)
            txt1.SelectionMode = value
        End Set
    End Property

#End Region

#Region " Buttons "
    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        If txt1.NumericText < mintMaximum Then
            txt1.Text = txt1.NumericText + 1
        End If
    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If txt1.NumericText > mintMinimum Then
            txt1.Text = txt1.NumericText - 1
        End If
    End Sub

#End Region


#Region " Events "

    Public Shadows Event ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    Private Sub txt1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt1.GotFocus
        RaiseEvent GotFocus(Me, e)
    End Sub

    Private Sub txt1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt1.LostFocus
        RaiseEvent LostFocus(Me, e)
    End Sub

    Private Sub txt1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt1.TextChanged
        RaiseEvent ValueChanged(Me, e)
    End Sub

    Private Sub txt1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt1.KeyPress
        RaiseEvent KeyPress(Me, e)
    End Sub

    Private Sub txt1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt1.KeyUp
        If e.KeyCode = Keys.F5 Then
            Call btnUp_Click(Nothing, Nothing)
        ElseIf e.Shift And e.KeyCode = Keys.F5 Then
            Call btnDown_Click(Nothing, Nothing)
        End If
    End Sub
#End Region


End Class
