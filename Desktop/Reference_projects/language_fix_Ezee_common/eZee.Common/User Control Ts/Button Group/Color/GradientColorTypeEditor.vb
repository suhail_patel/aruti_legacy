Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Drawing.Design
Imports System.Drawing.Drawing2D

Friend Class GradientColorTypeEditor
    Inherits UITypeEditor
    Public Overloads Overrides Function GetEditStyle(ByVal context As ITypeDescriptorContext) As UITypeEditorEditStyle
        Return UITypeEditorEditStyle.None
    End Function

    Public Overloads Overrides Function GetPaintValueSupported(ByVal context As ITypeDescriptorContext) As Boolean
        Return True
    End Function

    Public Overloads Overrides Sub PaintValue(ByVal e As PaintValueEventArgs)
        Dim color As GradientColor = CType(IIf(TypeOf e.Value Is GradientColor, e.Value, Nothing), GradientColor)
        If color Is Nothing Then
            MyBase.PaintValue(e)
        Else
            Using brush As New LinearGradientBrush(e.Bounds, color.StartColor, color.EndColor, color.FillDirection)
                e.Graphics.FillRectangle(brush, e.Bounds)
            End Using
        End If
    End Sub
End Class
