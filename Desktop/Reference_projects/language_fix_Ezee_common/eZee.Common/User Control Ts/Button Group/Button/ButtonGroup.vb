Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Design
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Public Delegate Sub KeyPressedEventHandler(ByVal sender As Object, ByVal e As KeyPressedEventArgs)

<Designer(GetType(ButtonGroup_Designer))> _
<DefaultEvent("Click"), DefaultProperty("Buttons")> _
Public Class ButtonGroup
    Inherits UserControl

    Public Shadows Event KeyDown As KeyPressedEventHandler
    Public Shadows Event KeyUp As KeyPressedEventHandler
    Public Shadows Event Click As KeyPressedEventHandler

    Private m_BG_Buttons As BG_ButtonCollection = New BG_ButtonCollection
    Private gxOff As Graphics
    Private m_bmpOffscreen As Bitmap

#Region " Constructor "
    Public Sub New()
        Me.InitializeComponent()
        MyBase.Width = 300
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not Me.m_bmpOffscreen Is Nothing Then
                Me.m_bmpOffscreen.Dispose()
                Me.m_bmpOffscreen = Nothing
            End If
            If Not Me.gxOff Is Nothing Then
                Me.gxOff.Dispose()
                Me.gxOff = Nothing
            End If
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    Private Sub InitializeComponent()
        MyBase.SuspendLayout()
        MyBase.AutoScaleDimensions = New SizeF(96.0F, 96.0F)
        MyBase.Name = "KeyboardControl"
        Me.Size = New System.Drawing.Size(&HFC, &H77)
        MyBase.ResumeLayout(False)
    End Sub
#End Region

#Region " Special Keys Events "
    Private m_blnShiftPressed As Boolean
    Public ReadOnly Property ShiftPressed()
        Get
            Return m_blnShiftPressed
        End Get
    End Property

    Public Sub Shift_Press()
        m_blnShiftPressed = Not m_blnShiftPressed
    End Sub

    Private m_blnCapsLockPressed As Boolean
    Public ReadOnly Property CapsLockPressed() As Boolean
        Get
            Return m_blnCapsLockPressed
        End Get
    End Property

    Public Sub CapsLock_Press()
        m_blnCapsLockPressed = Not m_blnCapsLockPressed
    End Sub
#End Region

#Region " Property "

    Private m_ButtonStyle As BG_ButtonStyleType = BG_ButtonStyleType.Normal
    <DefaultValue(GetType(BG_ButtonStyleType), "Normal")> _
    Public Property ButtonStyle() As BG_ButtonStyleType
        Get
            Return Me.m_ButtonStyle
        End Get
        Set(ByVal value As BG_ButtonStyleType)
            If Me.m_ButtonStyle <> value Then
                Me.m_ButtonStyle = value
                Me.Invalidate()
            End If
        End Set
    End Property

    <Editor(GetType(BG_ButtonCollectionEditor), GetType(UITypeEditor)), Browsable(True), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Description("Gets or sets the list of Key descriptors.")> _
    Public Property Buttons() As BG_ButtonCollection
        Get
            Return m_BG_Buttons
        End Get
        Set(ByVal value As BG_ButtonCollection)
            m_BG_Buttons = value
        End Set
    End Property

#End Region

#Region " Skin "
    Private m_Selected_Skin As Bitmap

    Dim m_StretchImage As Boolean


    <Category("Appearance"), Description("Gets or sets whether to stretch the skin image.")> _
    <DefaultValue(False)> _
    Public Property StretchSkinImage() As Boolean
        Get
            Return Me.m_StretchImage
        End Get
        Set(ByVal value As Boolean)
            If m_StretchImage <> value Then
                m_StretchImage = value
                MyBase.Invalidate()
            End If
        End Set
    End Property

    Private Function StretchImage(ByVal aSrcImage As Bitmap, ByVal aRect As Rectangle) As Bitmap
        Dim bitmap As Bitmap
        Dim graphics As Graphics = Nothing

        bitmap = New Bitmap(aSrcImage)

        If Not graphics Is Nothing Then
            graphics.Dispose()
            graphics = Nothing
        End If
        Return bitmap
    End Function

    Private m_Background_Skin As Bitmap
    <DefaultValue(GetType(Bitmap), "Nothing")> _
    Public Property BackgroundSkin() As Bitmap
        Get
            Return Me.m_Background_Skin
        End Get
        Set(ByVal value As Bitmap)
            Me.m_Background_Skin = value
            Me.Invalidate()
        End Set
    End Property

    Private m_Background_Selected_Skin As Bitmap
    <DefaultValue(GetType(Bitmap), "Nothing")> _
    Public Property BackgroundSelectedSkin() As Bitmap
        Get
            Return Me.m_Background_Selected_Skin
        End Get
        Set(ByVal value As Bitmap)
            Me.m_Background_Selected_Skin = value
            If Not Me.m_Selected_Skin Is Nothing Then
                Me.m_Selected_Skin.Dispose()
                Me.m_Selected_Skin = Nothing
            End If
        End Set
    End Property

    Private m_EnableTextOnSkin As Boolean
    <DefaultValue(False)> _
    Public Property EnableTextOnSkin() As Boolean
        Get
            Return Me.m_EnableTextOnSkin
        End Get
        Set(ByVal value As Boolean)
            Me.m_EnableTextOnSkin = value
            Me.Invalidate()
        End Set
    End Property
#End Region

#Region " Events "
    Protected Overrides Sub OnPaintBackground(ByVal e As PaintEventArgs)
        'MyBase.OnPaintBackground(e)
    End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        Try
            If intLastSelected > -1 Then
                m_BG_Buttons.Item(intLastSelected).Pressed = False
                Dim newe As New KeyPressedEventArgs( _
                            GetKeyValue(m_BG_Buttons.Item(intLastSelected), False), _
                            m_BG_Buttons.Item(intLastSelected))

                RaiseEvent Click(Me, newe)
                intLastSelected = -1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.Refresh()
        MyBase.OnClick(e)
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.m_HandlerFocusedControl = GetFocus()
        For iCnt As Integer = 0 To m_BG_Buttons.Count - 1
            If m_BG_Buttons.Item(iCnt).Bounds.Contains(e.X, e.Y) Then
                intLastSelected = iCnt
                Me.m_BG_Buttons(iCnt).Pressed = True
                Dim newe As New KeyPressedEventArgs( _
                                    GetKeyValue(m_BG_Buttons.Item(iCnt), False), _
                                    m_BG_Buttons.Item(iCnt))
                RaiseEvent KeyDown(Me, newe)

                Exit For
            End If
        Next
        Me.Refresh()
        MyBase.OnMouseDown(e)
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            If intLastSelected > -1 Then
                m_BG_Buttons.Item(intLastSelected).Pressed = False
                Dim newe As New KeyPressedEventArgs( _
                            GetKeyValue(m_BG_Buttons.Item(intLastSelected), False), _
                            m_BG_Buttons.Item(intLastSelected))

                RaiseEvent KeyUp(Me, newe)
                intLastSelected = -1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.Refresh()
        MyBase.OnMouseUp(e)
    End Sub

    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        If (MyBase.ClientSize.Width = 0) OrElse (MyBase.ClientSize.Height = 0) Then
            MyBase.OnResize(e)
        Else
            If Not Me.m_bmpOffscreen Is Nothing Then
                Me.m_bmpOffscreen.Dispose()
                Me.m_bmpOffscreen = Nothing
                Me.gxOff.Dispose()
                Me.gxOff = Nothing
            End If
            Me.m_bmpOffscreen = New Bitmap(MyBase.ClientSize.Width, MyBase.ClientSize.Height)
            Me.gxOff = Graphics.FromImage(Me.m_bmpOffscreen)
            MyBase.OnResize(e)
            AddHandler m_BG_Buttons.RefreshKeyRequired, AddressOf Keyboard_RefreshKeyRequired
        End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        gxOff.Clear(Me.BackColor)

        Select Case m_ButtonStyle
            Case BG_ButtonStyleType.Normal
                Call Draw_Normal_Style(gxOff, MyBase.ClientRectangle)
            Case BG_ButtonStyleType.Gradient
                Call Draw_Gradient_Style(gxOff, MyBase.ClientRectangle)
            Case BG_ButtonStyleType.Skin
                Call Draw_Skin_Style(gxOff, MyBase.ClientRectangle)
            Case Else

        End Select

        e.Graphics.DrawImage(Me.m_bmpOffscreen, 0, 0)

        MyBase.OnPaint(e)
    End Sub
#End Region

#Region " Private Function "
    Dim intLastSelected As Integer = -1
    Private Function GetKeyValue(ByVal Key As BG_Button, ByVal blnDisplay As Boolean) As String
        If Key.Value = String.Empty OrElse blnDisplay Then
            If m_blnCapsLockPressed Then
                If m_blnShiftPressed Then
                    Return Key.Normal_Display
                Else
                    Return Key.ShiftPressed_Display
                End If
            Else
                If m_blnShiftPressed Then
                    Return Key.ShiftPressed_Display
                Else
                    Return Key.Normal_Display
                End If
            End If
        Else
            Return Key.Value
        End If
    End Function

    Private Sub Keyboard_RefreshKeyRequired(ByVal sender As Object, ByVal e As BG_ButtonEventArgs)
        Me.Refresh()
    End Sub
#End Region

#Region " Foucus "
    Private m_HandlerFocusedControl As IntPtr
    Friend Shared Function GetFocus() As IntPtr
        If Environment.OSVersion.Platform = PlatformID.WinCE Then
            Return GetFocusCE()
        End If
        Return GetFocusWin()
    End Function

    <DllImport("coredll.dll", EntryPoint:="GetFocus")> _
        Private Shared Function GetFocusCE() As IntPtr
    End Function
    <DllImport("user32.dll", EntryPoint:="GetFocus")> _
    Private Shared Function GetFocusWin() As IntPtr
    End Function
#End Region

#Region " Painting Function "
    Private Sub Draw_Gradient_Style(ByVal gr As Graphics, ByVal aRect As Rectangle)
        Dim count As Integer = Me.m_BG_Buttons.Count
        For iCnt As Integer = 0 To count - 1
            Dim aBG_Button As BG_Button = Me.m_BG_Buttons(iCnt)
            Me.Draw_Gradient_Button(Me.gxOff, aBG_Button)
        Next
    End Sub

    Private Sub Draw_Normal_Style(ByVal gr As Graphics, ByVal aRect As Rectangle)
        Dim count As Integer = Me.m_BG_Buttons.Count
        For iCnt As Integer = 0 To count - 1
            Dim aBG_Button As BG_Button = Me.m_BG_Buttons(iCnt)
            If Not aBG_Button Is Nothing Then
                Me.Draw_Normal_Button(gr, aBG_Button)
            End If
        Next iCnt
    End Sub

    Private Sub Draw_Skin_Style(ByVal gr As Graphics, ByVal aRect As Rectangle)
        If Not Me.BackgroundSkin Is Nothing Then
            If (Not m_StretchImage) Then
                gr.DrawImage(Me.m_Selected_Skin, 0, 0)
            Else
                Dim imageAttr As New Imaging.ImageAttributes()
                gr.DrawImage(Me.m_Selected_Skin, aRect, 0, 0, Me.m_Selected_Skin.Width, Me.m_Selected_Skin.Height, GraphicsUnit.Pixel, imageAttr)
            End If

            If Not Me.BackgroundSelectedSkin Is Nothing Then
                Dim attributes2 As New Imaging.ImageAttributes()
                Dim count As Integer = Me.m_BG_Buttons.Count
                For i As Integer = 0 To count - 1
                    Dim aBG_Button As BG_Button = Me.m_BG_Buttons(i)
                    If aBG_Button.Pressed Then
                        Dim x As Integer = aBG_Button.Bounds.X
                        Dim y As Integer = aBG_Button.Bounds.Y
                        Dim width As Integer = aBG_Button.Bounds.Width
                        Dim height As Integer = aBG_Button.Bounds.Height

                        If Me.m_Selected_Skin Is Nothing Then
                            Me.m_Selected_Skin = Me.StretchImage(Me.m_Background_Selected_Skin, aRect)
                        End If

                        gr.DrawImage(Me.m_Selected_Skin, aBG_Button.Bounds, x, y, width, height, GraphicsUnit.Pixel, attributes2)

                        If Me.m_EnableTextOnSkin Then
                            Me.DrawText(gxOff, GetKeyValue(aBG_Button, True), aBG_Button.Font, aBG_Button.SelectedForeColor, aBG_Button.Bounds, aBG_Button.TextFormat)
                        End If
                    Else
                        If Me.m_EnableTextOnSkin Then
                            Me.DrawText(gxOff, GetKeyValue(aBG_Button, True), aBG_Button.Font, aBG_Button.ForeColor, aBG_Button.Bounds, aBG_Button.TextFormat)
                        End If
                    End If
                Next i
            End If
        End If
    End Sub

    Private Sub Draw_Normal_Button(ByVal gr As Graphics, ByVal aBG_Button As BG_Button)
        Dim colorBk As Color
        Dim colorFr As Color

        If aBG_Button.Pressed Then
            colorBk = aBG_Button.SelectedBackColor
            colorFr = aBG_Button.SelectedForeColor
        Else
            colorBk = aBG_Button.BackColor
            colorFr = aBG_Button.ForeColor
        End If

        Using brush As New SolidBrush(colorBk)
            gr.FillRectangle(brush, aBG_Button.Bounds)
        End Using

        Me.DrawBorder(gr, aBG_Button.BorderColor, aBG_Button.Bounds, aBG_Button.BorderWidth)
        Me.DrawText(gr, GetKeyValue(aBG_Button, True), aBG_Button.Font, colorFr, New Rectangle(aBG_Button.Bounds.X, aBG_Button.Bounds.Y, aBG_Button.Bounds.Width, aBG_Button.Bounds.Height), aBG_Button.TextFormat)
    End Sub

    Private Sub Draw_Gradient_Button(ByVal gr As Graphics, ByVal aBG_Button As BG_Button)
        Dim colorFr As Color
        Dim color1 As Color
        Dim color2 As Color

        If aBG_Button.Pressed Then
            colorFr = aBG_Button.SelectedForeColor
            color1 = aBG_Button.GradientBackColor.StartColor
            color2 = aBG_Button.GradientBackColor.EndColor
        Else
            colorFr = aBG_Button.ForeColor
            color1 = aBG_Button.GradientBackColor.EndColor
            color2 = aBG_Button.GradientBackColor.StartColor
        End If

        Using gb As New Drawing2D.LinearGradientBrush(aBG_Button.Bounds, color1, color2, aBG_Button.GradientBackColor.FillDirection)
            gr.FillRectangle(gb, aBG_Button.Bounds)
        End Using

        Me.DrawBorder(gr, aBG_Button.BorderColor, aBG_Button.Bounds, aBG_Button.BorderWidth)
        Me.DrawText(gr, GetKeyValue(aBG_Button, True), aBG_Button.Font, colorFr, aBG_Button.Bounds, aBG_Button.TextFormat)
    End Sub

    Private Sub DrawText(ByVal gr As Graphics, ByVal aText As String, ByVal aFont As Font, ByVal aColor As Color, ByVal aRectF As RectangleF, ByVal aStringFormat As StringFormat)
        Using brs As New SolidBrush(aColor)
            gr.DrawString(aText, aFont, brs, aRectF, aStringFormat)
        End Using
    End Sub

    Private Sub DrawBorder(ByVal gr As Graphics, ByVal aColor As Color, ByVal aRectF As Rectangle, ByVal aWidth As Integer)
        If aWidth <= 0 Then Exit Sub
        Using pen As New Pen(aColor, aWidth)
            aRectF.Inflate(-(aWidth \ 2), -(aWidth \ 2))
            gr.DrawRectangle(pen, aRectF)
        End Using
    End Sub
#End Region

End Class