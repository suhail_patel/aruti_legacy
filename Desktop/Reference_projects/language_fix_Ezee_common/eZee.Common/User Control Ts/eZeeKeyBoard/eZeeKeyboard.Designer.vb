<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeKeyboard
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picCapsLockDown = New System.Windows.Forms.PictureBox
        Me.picRightShiftDown = New System.Windows.Forms.PictureBox
        Me.picLeftShiftDown = New System.Windows.Forms.PictureBox
        Me.picKeyboard = New System.Windows.Forms.PictureBox
        CType(Me.picCapsLockDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRightShiftDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLeftShiftDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKeyboard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picCapsLockDown
        '
        Me.picCapsLockDown.Image = Global.eZee.Common.My.Resources.Resources.CapsLock_Down
        Me.picCapsLockDown.Location = New System.Drawing.Point(3, 113)
        Me.picCapsLockDown.Name = "picCapsLockDown"
        Me.picCapsLockDown.Size = New System.Drawing.Size(100, 55)
        Me.picCapsLockDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picCapsLockDown.TabIndex = 5
        Me.picCapsLockDown.TabStop = False
        Me.picCapsLockDown.Visible = False
        '
        'picRightShiftDown
        '
        Me.picRightShiftDown.Image = Global.eZee.Common.My.Resources.Resources.Shift_Right_Down
        Me.picRightShiftDown.Location = New System.Drawing.Point(684, 169)
        Me.picRightShiftDown.Name = "picRightShiftDown"
        Me.picRightShiftDown.Size = New System.Drawing.Size(74, 55)
        Me.picRightShiftDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picRightShiftDown.TabIndex = 6
        Me.picRightShiftDown.TabStop = False
        Me.picRightShiftDown.Visible = False
        '
        'picLeftShiftDown
        '
        Me.picLeftShiftDown.Image = Global.eZee.Common.My.Resources.Resources.Shift_Left_Down
        Me.picLeftShiftDown.Location = New System.Drawing.Point(3, 169)
        Me.picLeftShiftDown.Name = "picLeftShiftDown"
        Me.picLeftShiftDown.Size = New System.Drawing.Size(128, 55)
        Me.picLeftShiftDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLeftShiftDown.TabIndex = 7
        Me.picLeftShiftDown.TabStop = False
        Me.picLeftShiftDown.Visible = False
        '
        'picKeyboard
        '
        Me.picKeyboard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picKeyboard.Image = Global.eZee.Common.My.Resources.Resources.Keyboard_Amount
        Me.picKeyboard.InitialImage = Nothing
        Me.picKeyboard.Location = New System.Drawing.Point(0, 0)
        Me.picKeyboard.Name = "picKeyboard"
        Me.picKeyboard.Size = New System.Drawing.Size(871, 282)
        Me.picKeyboard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picKeyboard.TabIndex = 4
        Me.picKeyboard.TabStop = False
        '
        'eZeeKey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.picCapsLockDown)
        Me.Controls.Add(Me.picRightShiftDown)
        Me.Controls.Add(Me.picLeftShiftDown)
        Me.Controls.Add(Me.picKeyboard)
        Me.Name = "eZeeKey"
        Me.Size = New System.Drawing.Size(871, 282)
        CType(Me.picCapsLockDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRightShiftDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLeftShiftDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKeyboard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents picCapsLockDown As System.Windows.Forms.PictureBox
    Private WithEvents picRightShiftDown As System.Windows.Forms.PictureBox
    Private WithEvents picLeftShiftDown As System.Windows.Forms.PictureBox
    Private WithEvents picKeyboard As System.Windows.Forms.PictureBox

End Class
