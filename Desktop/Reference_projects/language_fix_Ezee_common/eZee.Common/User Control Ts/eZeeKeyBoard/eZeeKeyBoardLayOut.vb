Imports System.ComponentModel

<Category("Keyboard Type"), Description("Type of keyboard to use")> _
Public Enum eZeeKeyBoardLayOut
    None
    KeyBoard_Standard
    KeyBoard_Alphabetical
    NumPad_Decimal
    NumPad_Amount
    NumPad_Number
End Enum