Imports System.Windows.Forms
Imports System.drawing

Public Class eZeeToolStripMenuItem
    Inherits ToolStripMenuItem

    Private colHeader As ColumnHeader = Nothing
    Private mintColumnIndex As Integer = -1
    Private mintColumnWidth As Integer = 0

    Public Property BindedColumn() As ColumnHeader
        Get
            Return colHeader
        End Get
        Set(ByVal value As ColumnHeader)
            colHeader = value
        End Set
    End Property

    Public Property BindedColumnIndex() As Integer
        Get
            Return mintColumnIndex
        End Get
        Set(ByVal value As Integer)
            mintColumnIndex = value
        End Set
    End Property

    Public Property BindedColumnWidth() As Integer
        Get
            Return mintColumnWidth
        End Get
        Set(ByVal value As Integer)
            mintColumnWidth = value
        End Set
    End Property

End Class
