Imports System.Windows.Forms
Imports System.ComponentModel.Component

Partial Class eZeePieChart
    Inherits System.Windows.Forms.Panel

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()
        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)

    End Sub

    ''' <summary> 
    ''' Initializes the <c>PieChartControl</c>. 
    ''' </summary> 
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()
        Me.SetStyle(ControlStyles.UserPaint, True)
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        Me.SetStyle(ControlStyles.ResizeRedraw, True)

        m_toolTip = New ToolTip()

        InitializeComponent()

    End Sub


    'Component overrides dispose to clean up the component list.
    ''' <summary> 
    ''' Clean up any resources being used. 
    ''' </summary> 
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If Not m_disposed Then
            Try
                If disposing Then
                    m_pieChart.Dispose()
                    m_toolTip.Dispose()
                End If
                m_disposed = True
            Finally
                MyBase.Dispose(disposing)
            End Try
        End If
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

End Class
