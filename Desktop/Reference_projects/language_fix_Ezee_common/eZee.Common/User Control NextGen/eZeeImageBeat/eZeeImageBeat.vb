Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.ComponentModel
Imports System.Windows.Forms


<System.Serializable()> _
Public Class eZeeImageBeat
    Inherits PictureBox
    Implements IDisposable

    'Class Variables
    Private mintCount As Integer = 0
    Private mintBlinkAfterClicked As Integer = 0

    Private mblnMakeBig As Boolean = False
    Private mblnIsClicked As Boolean = True
    Private mblnTimerStarted As Boolean = False

    Private mlocOldLocation As Point = Nothing
    Private mszOldSize As Size = New Size(48, 48)

    'Property Variables 
    Private mimgBeatingImage As Image = Nothing
    Private mimgImageOnHover As Image = Nothing
    Private mimgImageOnPress As Image = Nothing
    Private mimgImageOnDisable As Image = Nothing

    Private mintBeatingSpeed As Integer = 16
    Private mintBeatingSpeedOnClick As Integer = 5
    Private mintBeatsAfterClicked As Integer = 2
    Private mszSmallBeatingSize As Size = New Size(0, 0)

    'Control Declaration
    Friend WithEvents tmrTimer As Timer

#Region " Constructors "

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        tmrTimer = New Timer
        Me.SizeMode = PictureBoxSizeMode.StretchImage
        Me.Cursor = Cursors.Hand
    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

        tmrTimer = New Timer
        Me.SizeMode = PictureBoxSizeMode.StretchImage
        Me.Cursor = Cursors.Hand
    End Sub

#End Region

#Region " Properties "

#Region " Hiding PictureBox's Events "

    <Browsable(False)> _
    Public Shadows Property Image() As Image
        Get
            Return MyBase.Image
        End Get
        Set(ByVal value As Image)
            MyBase.Image = value
        End Set
    End Property

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows Property SizeMode() As PictureBoxSizeMode
        Get
            Return MyBase.SizeMode
        End Get
        Set(ByVal value As PictureBoxSizeMode)
            MyBase.SizeMode = value
        End Set
    End Property

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows Property InitialImage() As Image
        Get
            Return MyBase.InitialImage
        End Get
        Set(ByVal value As Image)
            MyBase.InitialImage = value
        End Set
    End Property

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows Property ErrorImage() As Image
        Get
            Return MyBase.ErrorImage
        End Get
        Set(ByVal value As Image)
            MyBase.ErrorImage = value
        End Set
    End Property

#End Region

    Public Shadows Property Enabled() As Boolean
        Get
            Return MyBase.Enabled
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                Me.Image = Me.BeatingImage
            Else
                Me.Image = Me.DisabledImage
            End If
            MyBase.Enabled = value
        End Set
    End Property

    Public Property BeatingImage() As Image
        Get
            Return mimgBeatingImage
        End Get
        Set(ByVal value As Image)
            mimgBeatingImage = value
            Me.Image = value
        End Set
    End Property

    Public Property OnHoverImage() As Image
        Get
            Return mimgImageOnHover
        End Get
        Set(ByVal value As Image)
            mimgImageOnHover = value
        End Set
    End Property

    Public Property OnPressImage() As Image
        Get
            Return mimgImageOnPress
        End Get
        Set(ByVal value As Image)
            mimgImageOnPress = value
        End Set
    End Property

    Public Property DisabledImage() As Image
        Get
            Return mimgImageOnDisable
        End Get
        Set(ByVal value As Image)
            mimgImageOnDisable = value
        End Set
    End Property

    Public Property BeatingSpeed() As Integer
        Get
            Return mintBeatingSpeed
        End Get
        Set(ByVal value As Integer)
            mintBeatingSpeed = value
        End Set
    End Property

    Public Property BeatingSpeedOnClick() As Integer
        Get
            Return mintBeatingSpeedOnClick
        End Get
        Set(ByVal value As Integer)
            mintBeatingSpeedOnClick = value
        End Set
    End Property

    Public Property BeatsAfterClicked() As Integer
        Get
            Return mintBeatsAfterClicked
        End Get
        Set(ByVal value As Integer)
            mintBeatsAfterClicked = value
        End Set
    End Property

    Public Property SmallBeatingSize() As Size
        Get
            Return mszSmallBeatingSize
        End Get
        Set(ByVal value As Size)
            If value.Height > Me.Size.Height Then
                MsgBox("SmallBeatingSize can't be greator than control size.")
                Exit Property
            End If
            mszSmallBeatingSize = value
        End Set
    End Property

#End Region

#Region " Private & Public Methods "

    Public Sub StartBeating()
        mblnIsClicked = False
        tmrTimer.Interval = Me.BeatingSpeed
        tmrTimer.Start()
        mblnTimerStarted = True
    End Sub

    Public Sub StopBeating()
        mblnIsClicked = True
        Call ResetTimer()
    End Sub

    Private Sub ResetTimer()
        tmrTimer.Stop()
        tmrTimer.Interval = Me.BeatingSpeed
        Me.Size = mszOldSize
        Me.Location = mlocOldLocation
        mintCount = Me.Size.Height
        mblnTimerStarted = False
    End Sub

    Public Overloads Sub Dispose()
        MyBase.Dispose()

        tmrTimer.Dispose()
    End Sub

#End Region

#Region " Override Events "

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        Me.Width = Me.Height
        mlocOldLocation = Me.Location
        mszOldSize = Me.Size
        mintCount = Me.Size.Width
    End Sub

    Protected Overrides Sub OnParentVisibleChanged(ByVal e As System.EventArgs)
        MyBase.OnParentVisibleChanged(e)
    End Sub

    Protected Overrides Sub OnLocationChanged(ByVal e As System.EventArgs)
        MyBase.OnLocationChanged(e)

        If DesignMode Then
            mlocOldLocation = Me.Location
        End If
    End Sub

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        MyBase.OnResize(e)

        If DesignMode Then
            Me.Width = Me.Height

            Me.mszOldSize = Me.Size
            'If Me.Height < Me.SmallBeatingSize.Height Then
            '    Me.SmallBeatingSize = Me.Size
            'End If

            'If Me.Height > 24 Then
            '    Me.SmallBeatingSize = New Size(24, 24)
            'End If
        End If
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)

        If mblnIsClicked = False Then
            Call ResetTimer()
        End If
        If Me.OnHoverImage IsNot Nothing Then
            Me.Image = Me.OnHoverImage
        End If
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        MyBase.OnMouseLeave(e)

        If mblnIsClicked = False And mblnTimerStarted = False Then
            tmrTimer.Start()
            mblnTimerStarted = True
        End If
        Me.Image = Me.BeatingImage
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDown(e)

        If Me.OnPressImage IsNot Nothing Then
            Me.Image = Me.OnPressImage
        End If
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseUp(e)

        If Me.OnPressImage IsNot Nothing Then
            Me.Image = Me.OnHoverImage
        End If
    End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        mblnIsClicked = True
        tmrTimer.Interval = Me.BeatingSpeedOnClick
        tmrTimer.Start()
        mblnTimerStarted = True

        MyBase.OnClick(e)
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub tmrTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrTimer.Tick
        If mblnIsClicked = True Then
            mintBlinkAfterClicked += 1
            If mintBlinkAfterClicked = Me.BeatsAfterClicked * Me.SmallBeatingSize.Height Then
                mintBlinkAfterClicked = 0
                Call ResetTimer()
            End If
        End If

        If mintCount = Me.SmallBeatingSize.Height Then
            mblnMakeBig = True
        ElseIf mintCount = mszOldSize.Height Then
            mblnMakeBig = False
        End If

        If mblnMakeBig Then
            Me.Left -= 1
            Me.Top -= 1
            Me.Height += 2
            Me.Width += 2
            mintCount += 2
        Else
            Me.Left += 1
            Me.Top += 1
            Me.Height -= 2
            Me.Width -= 2
            mintCount -= 2
        End If
    End Sub

#End Region

End Class
