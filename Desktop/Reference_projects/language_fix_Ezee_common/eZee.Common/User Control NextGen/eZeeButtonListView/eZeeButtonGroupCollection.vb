
Public Class eZeeButtonGroupCollection
    Implements IList, ICollection, IEnumerable

    Private m_Owner As eZeeButtonListView
    Private list As ArrayList

    Friend Sub New(ByVal owner As eZeeButtonListView)
        Me.m_Owner = owner
        Me.list = New ArrayList()
    End Sub

    Public Function Add(ByVal group As eZeeButtonGroup) As Integer
        If Me.Contains(group) Then
            Return -1
        End If

        group.eZeeButtonListView = Me.m_Owner

        'group.RemoveCheckedChangedHanlder()
        If (group.ButtonType <> eZeeLightButton.enButtonType.NormalButton) Then
            group.Selected = Not Me.m_Owner.CollapseOnFill
        End If
        group.Index = Me.list.Count

        If (Me.m_Owner.VerticalScroll.Visible) Then
            group.Width = Me.m_Owner.Width - (Me.m_Owner.Padding.Left + Me.m_Owner.Padding.Right) - 2 - 18
        Else
            group.Width = Me.m_Owner.Width - (Me.m_Owner.Padding.Left + Me.m_Owner.Padding.Right) - 2
        End If

        Me.m_Owner.Controls.Add(group)
        'group.AddCheckedChangedHanlder()

        'Me.m_Owner.HideHorzScrollBar()

        Dim num As Integer = Me.list.Add(group)
    End Function

    Public Function Add(ByVal key As String, ByVal headerText As String) As eZeeButtonGroup
        Dim group As New eZeeButtonGroup(key, headerText)
        Me.Add(group)
        Return group
    End Function

    Public Sub AddRange(ByVal groups As eZeeButtonGroup())
        Dim i As Integer
        For i = 0 To groups.Length - 1
            Me.Add(groups(i))
        Next i
    End Sub

    Private Function Add(ByVal value As Object) As Integer Implements System.Collections.IList.Add
        If Not TypeOf value Is eZeeButtonGroup Then
            Throw New ArgumentException("value")
        End If
        Return Me.Add(DirectCast(value, eZeeButtonGroup))
    End Function

    Public Sub AddRange(ByVal groups As eZeeButtonGroupCollection)
        Dim i As Integer
        For i = 0 To groups.Count - 1
            Me.Add(groups.Item(i))
        Next i
    End Sub

    Public Sub CopyTo(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
        Me.list.CopyTo(array, index)
    End Sub

    Public ReadOnly Property Count() As Integer Implements System.Collections.ICollection.Count
        Get
            Return Me.list.Count
        End Get
    End Property

    Private ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
        Get
            Return True
        End Get
    End Property

    Private ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
        Get
            Return Me
        End Get
    End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return Me.list.GetEnumerator()
    End Function

    Public Sub Clear() Implements System.Collections.IList.Clear
        Me.list.Clear()
        Me.m_Owner.ClearView()
    End Sub

    Private Function Contains(ByVal value As Object) As Boolean Implements System.Collections.IList.Contains
        Return Me.list.Contains(value)
    End Function

    Public Function Contains(ByVal value As eZeeButtonGroup) As Boolean
        Return Me.list.Contains(value)
    End Function

    Public Function IndexOf(ByVal value As Object) As Integer Implements System.Collections.IList.IndexOf
        Return Me.list.IndexOf(value)
    End Function

    <System.ComponentModel.EditorBrowsable(False)> _
    Public Sub Insert(ByVal index As Integer, ByVal value As Object) Implements System.Collections.IList.Insert

    End Sub

    Private ReadOnly Property IsFixedSize() As Boolean Implements System.Collections.IList.IsFixedSize
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.IList.IsReadOnly
        Get
            Return False
        End Get
    End Property

    Default Public Property Item(ByVal key As String) As eZeeButtonGroup
        Get
            If (Not Me.list Is Nothing) Then
                Dim i As Integer
                For i = 0 To Me.list.Count - 1
                    If (String.Compare(key, Me.Item(i).Name, False) = 0) Then
                        Return Me.Item(i)
                    End If
                Next i
            End If
            Return Nothing
        End Get
        Set(ByVal value As eZeeButtonGroup)
            Dim num As Integer = -1
            If (Not Me.list Is Nothing) Then
                Dim i As Integer
                For i = 0 To Me.list.Count - 1
                    If (String.Compare(key, Me.Item(i).Name, False) = 0) Then
                        num = i
                        Exit For
                    End If
                Next i
                If (num <> -1) Then
                    Me.list.Item(num) = value
                End If
            End If
        End Set
    End Property

    Default Public Property Item(ByVal index As Integer) As Object Implements System.Collections.IList.Item
        Get
            Return DirectCast(Me.list.Item(index), eZeeButtonGroup)
        End Get
        Set(ByVal value As Object)
            If Not Me.list.Contains(value) Then
                Me.list.Item(index) = DirectCast(value, eZeeButtonGroup)
            End If
        End Set
    End Property

    Public Sub Remove(ByVal group As eZeeButtonGroup)
        Me.list.Remove(group)
        Me.m_Owner.Controls.Remove(group)
    End Sub

    Private Sub Remove(ByVal value As Object) Implements System.Collections.IList.Remove
        If TypeOf value Is eZeeButtonGroup Then
            Me.Remove(DirectCast(value, eZeeButtonGroup))
        End If
    End Sub

    Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.IList.RemoveAt
        Me.Remove(Me.Item(index))
    End Sub

    Public Sub Expand(ByVal grp As eZeeButtonGroup, ByVal showHideScrollBar As Boolean)
        Me.Expand(grp.GroupID, showHideScrollBar)
    End Sub

    Public Sub Expand(ByVal grpId As Integer, ByVal showHideScrollBar As Boolean)
        Dim i As Integer = 0
        Dim blnGroupFound As Boolean = False
        'Me.m_Owner.SuspendLayout()
        For Each cntrl As System.Windows.Forms.Control In Me.m_Owner.Controls
            If (TypeOf cntrl Is eZeeButtonGroup) Then
                If (blnGroupFound) Then
                    i += 1
                End If
                If (CType(cntrl, eZeeButtonGroup).GroupID = grpId) Then
                    CType(cntrl, eZeeButtonGroup).Selected = True
                    blnGroupFound = True
                    i += 1
                End If
            End If
            If (i > 1) Then
                Exit For
            End If
            If (blnGroupFound) Then
                If (TypeOf cntrl Is eZeeButtonItem) Then
                    cntrl.Visible = True
                End If
            End If
        Next
        If showHideScrollBar Then
        Me.m_Owner.HideHorzScrollBar()
        End If
        'Me.m_Owner.ResumeLayout()
    End Sub

    Public Sub Expand(ByVal groups As List(Of eZeeButtonGroup), ByVal showHideScrollBar As Boolean)
        For Each group As eZeeButtonGroup In groups
            Me.Expand(group.GroupID, showHideScrollBar)
        Next
    End Sub

    Public Sub Collapse(ByVal grp As eZeeButtonGroup, ByVal showHideScrollBar As Boolean)
        Me.Collapse(grp.GroupID, showHideScrollBar)
    End Sub

    Public Sub Collapse(ByVal grpId As Integer, ByVal showHideScrollBar As Boolean)
        'Me.CollapseExpandGroup(grpId, False)
        Dim i As Integer = 0
        Dim blnGroupFound As Boolean = False
        Me.m_Owner.SuspendLayout()

        For j As Integer = Me.m_Owner.Controls.Count - 1 To 0 Step -1
            If (TypeOf Me.m_Owner.Controls(j) Is eZeeButtonItem) Then
                If (CType(Me.m_Owner.Controls(j), eZeeButtonItem).GroupId = grpId) Then
                    Me.m_Owner.Controls(j).Visible = False
                End If
            End If
        Next
        'With Me.m_Owner
        '    For j As Integer = .Controls.Count - 1 To 0 Step -1
        '        If (TypeOf .Controls(j) Is eZeeButtonGroup) Then
        '            If (blnGroupFound) Then
        '                i += 1
        '            End If
        '            If (CType(.Controls(j), eZeeButtonGroup).GroupID = grpId) Then
        '                CType(.Controls(j), eZeeButtonGroup).Selected = False
        '                blnGroupFound = True
        '                i += 1
        '            End If
        '        End If
        '        If (i > 1) Then
        '            Exit For
        '        End If
        '        If blnGroupFound Then
        '            If (TypeOf .Controls(j) Is eZeeButtonItem) Then
        '                .Controls(j).Visible = False
        '            End If
        '        End If
        '    Next
        'End With
        If showHideScrollBar Then
            Me.m_Owner.HideHorzScrollBar()
        End If
        Me.m_Owner.ResumeLayout()
    End Sub

    Public Sub Collapse(ByVal groups As eZeeButtonGroup(), ByVal showHideScrollBar As Boolean)
        For Each group As eZeeButtonGroup In groups
            Me.Collapse(group.GroupID, showHideScrollBar)
        Next
    End Sub

    Public Sub CollapseExceptThis(ByVal Group As eZeeButtonGroup, ByVal showHideScrollBar As Boolean)
        Me.CollapseExceptThis(Group.GroupID, showHideScrollBar)
    End Sub

    Public Sub CollapseExceptThis(ByVal GroupId As Integer, ByVal showHideScrollBar As Boolean)
        For Each cntrl As System.Windows.Forms.Control In Me.m_Owner.Controls
            If (TypeOf cntrl Is eZeeButtonGroup) Then
                If (CType(cntrl, eZeeButtonGroup).GroupID <> GroupId) Then
                    CType(cntrl, eZeeButtonGroup).Selected = False
                End If
            End If
            If (TypeOf cntrl Is eZeeButtonItem) Then
                If (CType(cntrl, eZeeButtonItem).GroupId <> GroupId) Then
                    cntrl.Visible = False
                End If
            End If
        Next
        If showHideScrollBar Then
            Me.m_Owner.HideHorzScrollBar()
        End If
    End Sub

End Class
