Imports System
Imports System.Windows.Forms.Design
Imports System.ComponentModel.Design
Imports System.Runtime.Serialization
Imports System.ComponentModel
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports System.Drawing

<DesignTimeVisible(True), DefaultEvent("Click")> _
Public Class NavigationButton
    Inherits System.Windows.Forms.UserControl

#Region " Variables "

    Private mintMinHeight As Integer = 35
    Private mintMinWidth As Integer = 150

    Private mblnShowImage As Boolean = True
    Private menImageSize As EnumImageSize = EnumImageSize.Image24x24
    Private mintIndex As Integer = -1

    Private mblnSelected As Boolean = False

    Private mblnFlag As Boolean = False
    Private mblnClicked As Boolean = False

    Private m_Owner As NavigationBar = Nothing

#End Region

#Region " Contructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        MyBase.Height = 35
        MyBase.Width = 150
        MyBase.MinimumSize = New Size(150, 35)
        MyBase.BackgroundImageLayout = ImageLayout.Center
        MyBase.BackColor = Color.White
        MyBase.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    End Sub

#End Region

#Region " Enum "

    Public Enum EnumImageSize
        Image24x24 = 24
        Image32x32 = 32
        Image48x48 = 48
        Image64x64 = 64
    End Enum

#End Region

#Region " Properties "

    Friend Property Owner() As NavigationBar
        Get
            Return m_Owner
        End Get
        Set(ByVal value As NavigationBar)
            m_Owner = value
        End Set
    End Property

    Public Property Selected() As Boolean
        Get
            Return mblnSelected
        End Get
        Set(ByVal value As Boolean)
            mblnSelected = value
            If value Then
                Me.BackgroundImage = GetSelectedImage(Me)
                'Krushna (FD GUI) (10 Aug 2009) -- Start
                If DesignMode = False Then
                For Each navButton As NavigationButton In Me.Parent.Controls
                    If navButton IsNot Me Then
                        navButton.BackgroundImage = Nothing
                        navButton.Selected = False
                    End If
                Next
            End If
                'Krushna (FD GUI) (10 Aug 2009) -- End
            End If
            'If mblnClicked = False Then
            '    Call OnClick(Nothing)
            'End If
        End Set
    End Property

    Public Property Image() As Image
        Get
            Return picImage.Image
        End Get
        Set(ByVal value As Image)
            picImage.Image = value
        End Set
    End Property

    Public Property ImageSize() As EnumImageSize
        Get
            Return menImageSize
        End Get
        Set(ByVal value As EnumImageSize)
            menImageSize = value
            Me.ResizeControls()
        End Set
    End Property

    Public Property ImageSizeMode() As PictureBoxSizeMode
        Get
            Return picImage.SizeMode
        End Get
        Set(ByVal value As PictureBoxSizeMode)
            picImage.SizeMode = value
        End Set
    End Property

    '<Browsable(True)> _
    'Public Property Caption() As String
    '    Get
    '        Return lblText.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblText.Text = value
    '        Me.Invalidate()
    '    End Set
    'End Property

    <Browsable(True), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
    Public Overrides Property Text() As String
        Get
            Return lblText.Text
        End Get
        Set(ByVal value As String)
            lblText.Text = value
            Me.Invalidate()
        End Set
    End Property

    Public Property ShowImage() As Boolean
        Get
            Return mblnShowImage
        End Get
        Set(ByVal value As Boolean)
            mblnShowImage = value
            picImage.Visible = value
            Me.ResizeControls()
        End Set
    End Property

    Public Property TextAlign() As ContentAlignment
        Get
            Return lblText.TextAlign
        End Get
        Set(ByVal value As ContentAlignment)
            lblText.TextAlign = value
        End Set
    End Property

    Public Property SpaceBetweenImageAndText() As Integer
        Get
            Return lblSpace.Width
        End Get
        Set(ByVal value As Integer)
            lblSpace.Width = value
            lblSpace.Text = ""
        End Set
    End Property

#End Region

#Region " Private Methods "

    Friend Sub ResizeControls()
        If picImage.Visible = True Then
            Select Case menImageSize
                Case EnumImageSize.Image24x24
                    picImage.Size = New Size(24 + 5, 24 + 5)
                    Me.MinimumSize = New Size(150, 35)
                Case EnumImageSize.Image32x32
                    picImage.Size = New Size(32 + 5, 32 + 5)
                    Me.MinimumSize = New Size(150, 32 + 5 + 6)
                Case EnumImageSize.Image48x48
                    picImage.Size = New Size(48 + 5, 48 + 5)
                    Me.MinimumSize = New Size(150, 48 + 5 + 6)
                Case EnumImageSize.Image64x64
                    picImage.Size = New Size(64 + 5, 64 + 5)
                    Me.MinimumSize = New Size(150, 64 + 5 + 6)
            End Select
            If Me.Height < Me.MinimumSize.Height Then
                Me.Height = Me.MinimumSize.Height
            End If
        End If
        Me.Invalidate()
    End Sub

    Private Function GetHoverBackImage(ByVal cntrl As Control) As Image
        Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)
        Dim start As New Point
        start.X = cntrl.Width / 0.4
        start.Y = 0
        Dim gb As New LinearGradientBrush(start, New Point(start.X, (cntrl.Height * 0.4)), Color.FromArgb(255, 251, 212), Color.FromArgb(255, 231, 150))
        g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, cntrl.Height * 0.4))
        gb = New LinearGradientBrush(New Point(start.X, cntrl.Height * 0.4), New Point(start.X, cntrl.Height), Color.FromArgb(255, 213, 82), Color.FromArgb(255, 230, 151))
        g.FillRectangle(gb, New Rectangle(0, cntrl.Height * 0.4, cntrl.Width, cntrl.Height))

        g.DrawLine(New Pen(Color.FromArgb(255, 247, 185)), 1, 1, cntrl.Width - 2, 1) 'Up Inner Border
        g.DrawLine(New Pen(Color.FromArgb(219, 197, 123)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(Color.FromArgb(255, 216, 146)), 1, cntrl.Height - 2, cntrl.Width - 2, cntrl.Height - 2) 'Down Inner Border
        g.DrawLine(New Pen(Color.FromArgb(208, 186, 115)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border
        g.DrawLine(New Pen(Color.FromArgb(215, 183, 102)), 0, 0, 0, cntrl.Height) 'Left Vertical Border
        g.DrawLine(New Pen(Color.FromArgb(216, 182, 101)), cntrl.Width - 1, 0, cntrl.Width - 1, cntrl.Height) 'Right Vertical Border

        Return btmap
    End Function

    Private Function GetSelectedImage(ByVal cntrl As Control) As Image
        Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)
        Dim start As New Point
        Dim gb As LinearGradientBrush
        start.X = cntrl.Width * 0.45
        start.Y = 0
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height * 0.45), Color.FromArgb(244, 188, 106), Color.FromArgb(255, 176, 92))
        g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, cntrl.Height * 0.45))
        gb = New LinearGradientBrush(New Point(start.X, cntrl.Height * 0.45 - 1), New Point(start.X, cntrl.Height), Color.FromArgb(254, 160, 63), Color.FromArgb(255, 205, 130))
        g.FillRectangle(gb, New Rectangle(0, cntrl.Height * 0.45, cntrl.Width, cntrl.Height))

        'Left Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 120, 87), Color.FromArgb(200, 186, 164))
        g.FillRectangle(gb, New Rectangle(0, 1, 1, cntrl.Height))

        'Right Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 120, 87), Color.FromArgb(200, 186, 164))
        g.FillRectangle(gb, New Rectangle(cntrl.Width - 1, 1, 1, cntrl.Height))

        g.DrawLine(New Pen(Color.FromArgb(139, 118, 84)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(Color.FromArgb(201, 159, 97)), 1, 1, cntrl.Width - 2, 1) 'Up Inner Border 1
        g.DrawLine(New Pen(Color.FromArgb(226, 177, 103)), 1, 2, cntrl.Width - 2, 2) 'Up Inner Border 2
        g.DrawLine(New Pen(Color.FromArgb(253, 173, 17)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border

        Return btmap
    End Function

    Private Function GetHoverSelectedImage(ByVal cntrl As Control) As Image
        Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)
        Dim start As New Point
        Dim gb As LinearGradientBrush
        start.X = cntrl.Width * 0.4
        start.Y = 0
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height * 0.4), Color.FromArgb(252, 194, 124), Color.FromArgb(251, 179, 97))
        g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, cntrl.Height * 0.4))
        gb = New LinearGradientBrush(New Point(start.X, cntrl.Height * 0.4 - 1), New Point(start.X, cntrl.Height), Color.FromArgb(251, 166, 62), Color.FromArgb(252, 211, 111))
        g.FillRectangle(gb, New Rectangle(0, cntrl.Height * 0.4, cntrl.Width, cntrl.Height))

        'Left Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 129, 101), Color.FromArgb(200, 186, 164))
        g.FillRectangle(gb, New Rectangle(0, 1, 1, cntrl.Height))

        'Right Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 129, 101), Color.FromArgb(204, 190, 165))
        g.FillRectangle(gb, New Rectangle(cntrl.Width - 1, 1, 1, cntrl.Height))

        g.DrawLine(New Pen(Color.FromArgb(142, 129, 101)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(Color.FromArgb(168, 136, 93)), 1, 1, cntrl.Width - 2, 1) 'Up Inner Border 1
        g.DrawLine(New Pen(Color.FromArgb(211, 160, 103)), 1, 2, cntrl.Width - 2, 2) 'Up Inner Border 2
        g.DrawLine(New Pen(Color.FromArgb(212, 197, 173)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border
        g.DrawLine(New Pen(Color.FromArgb(255, 229, 141)), 1, cntrl.Height - 2, cntrl.Width - 2, cntrl.Height - 2) 'Down Inner Border

        Return btmap
    End Function

#End Region

#Region " Protected Events "
    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        If Me.Size.Width < mintMinWidth Then
            Me.Width = mintMinWidth
        ElseIf Me.Size.Height < mintMinHeight Then
            Me.Height = mintMinHeight
        Else
            MyBase.OnResize(e)
        End If
    End Sub

    Protected Overrides Sub OnMouseMove(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        If Me.Selected Then
            Me.BackgroundImage = GetHoverSelectedImage(Me)
        Else
            Me.BackgroundImage = GetHoverBackImage(Me)
        End If

        MyBase.OnMouseMove(mevent)
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDown(e)
        Me.BackgroundImage = GetHoverSelectedImage(Me)
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseUp(e)
        Me.BackgroundImage = GetSelectedImage(Me)
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal eventargs As System.EventArgs)
        If Me.Selected Then
            Me.BackgroundImage = GetSelectedImage(Me)
        Else
            Me.BackgroundImage = Nothing
        End If

        MyBase.OnMouseLeave(eventargs)
        mblnFlag = False
    End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        MyBase.OnClick(e)
        'mblnClicked = True
        Me.Selected = True
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()
        If Me.RightToLeft = Windows.Forms.RightToLeft.Yes Then
            picImage.Dock = DockStyle.Right
            lblSpace.Dock = DockStyle.Right
            lblText.Dock = DockStyle.Fill
        End If
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)

        mblnFlag = True
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lblText_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblText.Click
        Call OnClick(e)
        'If Not Me.Selected Then
        '    Me.Selected = True
        'End If
    End Sub

    Private Sub lblText_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblText.MouseDown
        Call OnMouseDown(e)
    End Sub

    Private Sub lblText_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblText.MouseLeave
        Call OnMouseLeave(e)
        mblnFlag = False
    End Sub

    Private Sub lblText_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblText.MouseMove
        If Not mblnFlag Then
            Call OnMouseMove(e)
            mblnFlag = True
        End If
    End Sub

    Private Sub lblText_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblText.MouseUp
        Call OnMouseUp(e)
    End Sub

    Private Sub picImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picImage.Click
        Call OnClick(e)
        'If Not Me.Selected Then
        '    Me.Selected = True
        'End If
    End Sub

    Private Sub picImage_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picImage.MouseDown
        Call OnMouseDown(e)
    End Sub

    Private Sub picImage_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles picImage.MouseLeave
        Call OnMouseLeave(e)
        mblnFlag = False
    End Sub

    Private Sub picImage_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picImage.MouseMove
        If Not mblnFlag Then
            Call OnMouseMove(e)
            mblnFlag = True
        End If
    End Sub

    Private Sub picImage_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picImage.MouseUp
        Call OnMouseUp(e)
    End Sub

    Private Sub lblSpace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSpace.Click
        Call OnClick(e)
        'If Not Me.Selected Then
        '    Me.Selected = True
        'End If
    End Sub

    Private Sub lblSpace_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblSpace.MouseDown
        Call OnMouseDown(e)
    End Sub

    Private Sub lblSpace_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSpace.MouseLeave
        Call OnMouseLeave(e)
        mblnFlag = False
    End Sub

    Private Sub lblSpace_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblSpace.MouseMove
        If Not mblnFlag Then
            Call OnMouseMove(e)
            mblnFlag = True
        End If
    End Sub

    Private Sub lblSpace_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblSpace.MouseUp
        Call OnMouseUp(e)
    End Sub

#End Region

#Region " Type Converter Friend Class "

    Friend Class NavigationButtonConverter
        Inherits TypeConverter

        Public Overloads Overrides Function CanConvertTo(ByVal context As ITypeDescriptorContext, ByVal destType As Type) As Boolean
            If destType Is GetType(Serialization.InstanceDescriptor) Then
                Return True
            End If

            Return MyBase.CanConvertTo(context, destType)
        End Function

        Public Overloads Overrides Function ConvertTo(ByVal context As ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, ByVal destType As Type) As Object
            If destType Is GetType(Serialization.InstanceDescriptor) Then
                Dim ci As System.Reflection.ConstructorInfo = GetType(NavigationButton).GetConstructor(System.Type.EmptyTypes)

                Return New Serialization.InstanceDescriptor(ci, Nothing, False)
            End If

            Return MyBase.ConvertTo(context, culture, value, destType)
        End Function

    End Class

#End Region

End Class

'#Region " Classes related to designer operation "

'Public Class NavigationButtonDesigner
'    Inherits System.Windows.Forms.Design.ControlDesigner

'    Private Action_Lists As DesignerActionListCollection

'    Public Overrides ReadOnly Property ActionLists() As DesignerActionListCollection
'        Get
'            If Action_Lists Is Nothing Then
'                Action_Lists = New DesignerActionListCollection()
'                Action_Lists.Add(New NavigationButtonActionList(Me.Component))
'            End If
'            Return Action_Lists
'        End Get
'    End Property

'    Public Overrides Sub Initialize(ByVal component As System.ComponentModel.IComponent)
'        MyBase.Initialize(component)

'        Dim m_NavigationButton As New NavigationButton

'        If m_NavigationButton IsNot Nothing Then
'            EnableDesignMode(m_NavigationButton.lblText, "InnerLabel")
'        End If
'    End Sub

'End Class

'Public Class NavigationButtonActionList
'    Inherits System.ComponentModel.Design.DesignerActionList

'    Private m_NavigationButton As NavigationButton
'    Private DesignerActionSvc As DesignerActionUIService = Nothing

'    Public Sub New(ByVal component As IComponent)
'        MyBase.New(component)
'        Me.m_NavigationButton = component

'        Me.DesignerActionSvc = CType(GetService(GetType(DesignerActionUIService)), DesignerActionUIService)
'    End Sub

'    Private Function GetPropertyByName(ByVal PropName As String) As PropertyDescriptor
'        Dim Prop As PropertyDescriptor
'        Prop = TypeDescriptor.GetProperties(m_NavigationButton)(PropName)
'        If Prop Is Nothing Then
'            Throw New ArgumentException("Invalid property.", PropName)
'        Else
'            Return Prop
'        End If
'    End Function

'#Region " Properties "

'    Public Property Image() As Image
'        Get
'            Return m_NavigationButton.Image
'        End Get
'        Set(ByVal value As Image)
'            GetPropertyByName("Image").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'    Public Property ImageSize() As NavigationButton.EnumImageSize
'        Get
'            Return m_NavigationButton.ImageSize
'        End Get
'        Set(ByVal value As NavigationButton.EnumImageSize)
'            GetPropertyByName("ImageSize").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'    Public Property ImageSizeMode() As PictureBoxSizeMode
'        Get
'            Return m_NavigationButton.ImageSizeMode
'        End Get
'        Set(ByVal value As PictureBoxSizeMode)
'            GetPropertyByName("ImageSizeMode").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'    Public Property Text() As String
'        Get
'            Return m_NavigationButton.Text
'        End Get
'        Set(ByVal value As String)
'            GetPropertyByName("Text").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'    Public Property ShowImage() As Boolean
'        Get
'            Return m_NavigationButton.ShowImage
'        End Get
'        Set(ByVal value As Boolean)
'            GetPropertyByName("ShowImage").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'    Public Property CaptionAlign() As ContentAlignment
'        Get
'            Return m_NavigationButton.TextAlign
'        End Get
'        Set(ByVal value As ContentAlignment)
'            GetPropertyByName("TextAlign").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'    Public Property SpaceBetweenImageAndText() As Integer
'        Get
'            Return m_NavigationButton.SpaceBetweenImageAndText
'        End Get
'        Set(ByVal value As Integer)
'            GetPropertyByName("SpaceBetweenImageAndText").SetValue(m_NavigationButton, value)
'        End Set
'    End Property

'#End Region

'    Public Overrides Function GetSortedActionItems() As DesignerActionItemCollection

'        Dim items As New DesignerActionItemCollection()

'        '===Main Category===
'        items.Add(New DesignerActionHeaderItem("Main"))
'        items.Add(New DesignerActionPropertyItem("Text", "Text", "Main", "Set the caption of a control"))
'        items.Add(New DesignerActionPropertyItem("CaptionAlign", "Caption Align", "Main", "Set the alignment of caption to a control"))

'        '===Image Settings Category===
'        items.Add(New DesignerActionHeaderItem("Image Settings"))

'        items.Add(New DesignerActionPropertyItem("Image", "Image", "Image Settings", "Set the image to a control"))
'        items.Add(New DesignerActionPropertyItem("ImageSize", "Image Size", "Image Settings", "Set the size of the image of a control"))
'        items.Add(New DesignerActionPropertyItem("ImageSizeMode", "Size Mode", "Image Settings", "Set the size mode of image of a control"))
'        items.Add(New DesignerActionPropertyItem("ShowImage", "Show Image", "Image Settings", "Indicates whether image should be displayed or not"))

'        '===Miscellaneous Category===
'        items.Add(New DesignerActionHeaderItem("Miscellaneous"))

'        items.Add(New DesignerActionPropertyItem("SpaceBetweenImageAndText", "Space", "Miscellaneous", "Set the space between caption and image."))

'        Return items
'    End Function

'End Class

'#End Region

#Region " Navigation Button Collection "

Public Class NavigationButtonCollection
    Inherits CollectionBase

    Private Control As NavigationBar

    Friend Sub New(ByVal Control As NavigationBar)
        Me.Control = Control
    End Sub

    Default Public ReadOnly Property Item(ByVal Index As Integer) As NavigationButton
        Get
            Return DirectCast(List(Index), NavigationButton)
        End Get
    End Property

    Public Function IndexOf(ByVal value As NavigationButton) As Integer
        Return List.IndexOf(value)
    End Function

    Public Function Contains(ByVal Button As NavigationButton) As Boolean
        Return List.Contains(Button)
    End Function

    Public Function Add(ByVal Button As NavigationButton) As Integer
        Dim i As Integer

        i = List.Add(Button)
        Return i
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As NavigationButton)
        ' insert the item 
        List.Insert(index, value)
    End Sub

    Public Sub Remove(ByVal Button As NavigationButton)
        List.Remove(Button)
    End Sub

    'Protected Overloads Overrides Sub OnInsertComplete(ByVal index As Integer, ByVal value As Object)
    '    ' call base class 
    '    MyBase.OnInsertComplete(index, value)

    '    ' reset current page index 
    '    Me.Control.SelectedIndex = index
    'End Sub

    'Protected Overloads Overrides Sub OnRemoveComplete(ByVal index As Integer, ByVal value As Object)
    '    ' call base class 
    '    MyBase.OnRemoveComplete(index, value)

    '    ' check if removing current page 
    '    If Me.Control.SelectedIndex = index Then
    '        ' check if at the end of the list 
    '        If index < InnerList.Count Then
    '            Me.Control.SelectedIndex = index
    '        Else
    '            Me.Control.SelectedIndex = InnerList.Count - 1
    '        End If
    '    End If
    'End Sub

End Class

#End Region