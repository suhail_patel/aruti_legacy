Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports system.Windows.Forms
Imports System.ComponentModel

Public Class eZeeHeading
    Inherits Panel

    Private strHeading As String = ""

    Private mclrBorderColor As Color = Color.Black

    Private mclrGradientColor As Color = SystemColors.ButtonFace
    Private mstrHeaderText As String = ""
    Private mblnShowDefaultBorderColor As Boolean = True
    Private menBorderStyle As ButtonBorderStyle = ButtonBorderStyle.Solid
    Private menHeadingAlign As ContentAlignment = ContentAlignment.MiddleLeft

    Private WithEvents objlblHeaderText As Label

    <Browsable(True)> _
    Public Overrides Property Text() As String
        Get
            Return mstrHeaderText 'objlblHeaderText.Text
        End Get
        Set(ByVal value As String)
            mstrHeaderText = value
            objlblHeaderText.Text = mstrHeaderText
            Me.Invalidate()
        End Set
    End Property

    Public Property ShowDefaultBorderColor() As Boolean
        Get
            Return mblnShowDefaultBorderColor
        End Get
        Set(ByVal value As Boolean)
            mblnShowDefaultBorderColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Property BorderColor() As Color
        Get
            Return mclrBorderColor
        End Get
        Set(ByVal value As Color)
            mclrBorderColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Property PanelBorderStyle() As ButtonBorderStyle
        Get
            Return menBorderStyle
        End Get
        Set(ByVal value As ButtonBorderStyle)
            menBorderStyle = value
            Me.Invalidate()
        End Set
    End Property

    Public Property GradientColor() As Color
        Get
            Return mclrGradientColor
        End Get
        Set(ByVal value As Color)
            mclrGradientColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        objlblHeaderText = New Label

        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.CacheText, True)
        SetStyle(ControlStyles.ResizeRedraw, True)

        Me.Padding = New Padding(8, 0, 0, 0)
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(e)

        'Dim g As Graphics = e.Graphics
        'Dim start As New Point
        'start.X = Me.Width / 0.5
        'start.Y = 0
        'Dim gb As New LinearGradientBrush(start, New Point(start.X, (Me.Height * 0.6)), Me.GradientColor1, Me.GradientColor2)
        'g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, Me.Height * 0.6))
        'gb = New LinearGradientBrush(New Point(start.X, Me.Height * 0.4), New Point(start.X, Me.Height), Me.GradientColor2, Me.GradientColor1)
        'g.FillRectangle(gb, New Rectangle(0, Me.Height * 0.5, Me.Width, Me.Height))

        'g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), 1, 0, Me.Width - 2, 0) 'Up Outer Border
        'g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
        'g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), 0, 0, 0, Me.Height) 'Left Vertical Border
        'g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border

        Dim g As Graphics = e.Graphics

        Dim start As New Point
        start.X = CInt(Me.Width / 0.5)
        start.Y = 0

        Dim rect As Rectangle

        rect = New Rectangle(start, CType(New Point(start.X, CInt((Me.Height * 0.6))), Drawing.Size))
        Dim gb As New LinearGradientBrush(rect, ControlPaint.Light(mclrGradientColor, 1.5F), mclrGradientColor, LinearGradientMode.Vertical) 'Color.FromArgb(206, 223, 251)
        g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, CInt(Me.Height * 0.6)))

        rect = New Rectangle(New Point(start.X, CInt(Me.Height * 0.4)), CType(New Point(start.X, Me.Height), Drawing.Size))
        gb = New LinearGradientBrush(rect, mclrGradientColor, ControlPaint.Light(mclrGradientColor, 1.5F), LinearGradientMode.Vertical) 'Color.FromArgb(241, 247, 254))
        g.FillRectangle(gb, New Rectangle(0, CInt(Me.Height * 0.5), Me.Width, CInt(Me.Height * 0.5)))

        rect = New Rectangle(start, New Size(Me.Width, Me.Height))

        If Me.ShowDefaultBorderColor Then
            g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), 1, 0, Me.Width - 2, 0) 'Up Outer Border
            g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
            g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), 0, 0, 0, Me.Height) 'Left Vertical Border
            g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border
        Else
            g.DrawLine(New Pen(Me.BorderColor), 1, 0, Me.Width - 2, 0) 'Up Outer Border
            g.DrawLine(New Pen(Me.BorderColor), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
            g.DrawLine(New Pen(Me.BorderColor), 0, 0, 0, Me.Height) 'Left Vertical Border
            g.DrawLine(New Pen(Me.BorderColor), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border
        End If

        If Me.ShowDefaultBorderColor Then
            ControlPaint.DrawBorder(g, Me.ClientRectangle, ControlPaint.Dark(mclrGradientColor, -0.1F), Me.PanelBorderStyle)
        Else
            ControlPaint.DrawBorder(g, Me.ClientRectangle, Me.BorderColor, Me.PanelBorderStyle)
        End If
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        objlblHeaderText.AutoSize = False
        objlblHeaderText.BackColor = Color.Transparent
        objlblHeaderText.Name = "objlblHeaderText"
        objlblHeaderText.Text = Me.Text
        objlblHeaderText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        objlblHeaderText.Dock = DockStyle.Fill

        Me.Controls.Add(objlblHeaderText)
    End Sub

    'Private Function GetHoverBackImage(ByVal cntrl As System.Windows.Forms.Control) As Image
    '    Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

    '    Dim g As Graphics = Graphics.FromImage(btmap)
    '    Dim start As New Point
    '    start.X = cntrl.Width / 0.5
    '    start.Y = 0
    '    Dim gb As New LinearGradientBrush(start, New Point(start.X, (cntrl.Height * 0.6)), Color.FromArgb(241, 247, 254), Color.FromArgb(206, 223, 251))
    '    g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, cntrl.Height * 0.6))
    '    gb = New LinearGradientBrush(New Point(start.X, cntrl.Height * 0.4), New Point(start.X, cntrl.Height), Color.FromArgb(206, 223, 251), Color.FromArgb(241, 247, 254))
    '    g.FillRectangle(gb, New Rectangle(0, cntrl.Height * 0.5, cntrl.Width, cntrl.Height))

    '    g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
    '    g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border
    '    g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), 0, 0, 0, cntrl.Height) 'Left Vertical Border
    '    g.DrawLine(New Pen(Color.FromArgb(133, 174, 243)), cntrl.Width - 1, 0, cntrl.Width - 1, cntrl.Height) 'Right Vertical Border

    '    Return btmap
    'End Function

End Class
