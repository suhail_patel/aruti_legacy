Imports System
Imports System.Collections



''' Represents a collection of wizard pages. 

Public Class WizardPagesCollection
    Inherits CollectionBase

#Region "Fields"
    Private owner As eZeeWizard = Nothing
#End Region

#Region "Constructor"

    ''' Creates a new instance of the <see cref="WizardPagesCollection"/> class. 
    ''' <param name="owner">A Wizard object that owns the collection.</param> 
    Friend Sub New(ByVal owner As eZeeWizard)
        Me.owner = owner
    End Sub
#End Region

#Region "Indexer"
    Default Public Property Item(ByVal index As Integer) As eZeeWizardPage
        Get
            Return DirectCast(MyBase.List(index), eZeeWizardPage)
        End Get
        Set(ByVal value As eZeeWizardPage)
            MyBase.List(index) = value
        End Set
    End Property
#End Region

#Region "Methods"

    ''' Adds an object to the end of the WizardPagesCollection. 
    ''' <param name="value">The WizardPage to be added. 
    ''' The value can be null.</param> 
    ''' <returns>An Integer value representing the index at which the value has been added.</returns> 
    Public Function Add(ByVal value As eZeeWizardPage) As Integer
        Dim result As Integer = List.Add(value)
        Return result
    End Function


    ''' Adds the elements of an array of WizardPage objects to the end of the WizardPagesCollection. 
    ''' <param name="pages">An array on WizardPage objects to be added. 
    ''' The array itself cannot be null, but it can contain elements that are null.</param> 
    Public Sub AddRange(ByVal pages As eZeeWizardPage())
        ' Use external to validate and add each entry 
        For Each page As eZeeWizardPage In pages
            Me.Add(page)
        Next
    End Sub


    ''' Searches for the specified WizardPage and returns the zero-based index 
    ''' of the first occurrence in the entire WizardPagesCollection. 
    ''' <param name="value">A WizardPage object to locate in the WizardPagesCollection. 
    ''' The value can be null.</param> 
    ''' <returns></returns> 
    Public Function IndexOf(ByVal value As eZeeWizardPage) As Integer
        Return List.IndexOf(value)
    End Function


    ''' Inserts an element into the WizardPagesCollection at the specified index. 
    ''' <param name="index">An Integer value representing the zero-based index at which value should be inserted.</param> 
    ''' <param name="value">A WizardPage object to insert. The value can be null.</param> 
    Public Sub Insert(ByVal index As Integer, ByVal value As eZeeWizardPage)
        ' insert the item 
        List.Insert(index, value)
    End Sub


    ''' Removes the first occurrence of a specific object from the WizardPagesCollection. 
    ''' <param name="value">A WizardPage object to remove. The value can be null.</param> 
    Public Sub Remove(ByVal value As eZeeWizardPage)
        ' remove the item 
        List.Remove(value)
    End Sub


    ''' Determines whether an element is in the WizardPagesCollection. 
    ''' <param name="value">The WizardPage object to locate. The value can be null.</param> 
    ''' <returns>true if item is found in the WizardPagesCollection; otherwise, false.</returns> 
    Public Function Contains(ByVal value As eZeeWizardPage) As Boolean
        Return List.Contains(value)
    End Function


    ''' Performs additional custom processes after inserting a new element into the WizardPagesCollection instance. 
    ''' <param name="index">The zero-based index at which to insert value.</param> 
    ''' <param name="value">The new value of the element at index.</param> 
    Protected Overloads Overrides Sub OnInsertComplete(ByVal index As Integer, ByVal value As Object)
        ' call base class 
        MyBase.OnInsertComplete(index, value)

        ' reset current page index 
        Me.owner.SelectedIndex = index
    End Sub


    ''' Performs additional custom processes after removing an element from the System.Collections.CollectionBase instance. 
    ''' <param name="index">The zero-based index at which value can be found.</param> 
    ''' <param name="value">The value of the element to remove from index.</param> 
    Protected Overloads Overrides Sub OnRemoveComplete(ByVal index As Integer, ByVal value As Object)
        ' call base class 
        MyBase.OnRemoveComplete(index, value)

        ' check if removing current page 
        If Me.owner.SelectedIndex = index Then
            ' check if at the end of the list 
            If index < InnerList.Count Then
                Me.owner.SelectedIndex = index
            Else
                Me.owner.SelectedIndex = InnerList.Count - 1
            End If
        End If
    End Sub
#End Region
End Class
