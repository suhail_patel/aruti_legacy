Imports System
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Windows.Forms.VisualStyles
Imports System.Diagnostics

Public Class ExpandableGridViewColumn
    Inherits DataGridViewTextBoxColumn

    Friend _defaultNodeImage As Image

    Public Sub New()
        Me.CellTemplate = New ExpandableGridViewCell()
    End Sub

    ' Need to override Clone for design-time support. 
    Public Overloads Overrides Function Clone() As Object
        Dim c As ExpandableGridViewColumn = DirectCast(MyBase.Clone(), ExpandableGridViewColumn)
        c._defaultNodeImage = Me._defaultNodeImage
        Return c
    End Function

    Public Property DefaultNodeImage() As Image
        Get
            Return _defaultNodeImage
        End Get
        Set(ByVal value As Image)
            _defaultNodeImage = value
        End Set
    End Property
End Class