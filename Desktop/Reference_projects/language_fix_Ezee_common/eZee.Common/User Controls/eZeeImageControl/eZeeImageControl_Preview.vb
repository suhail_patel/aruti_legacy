Imports System.Windows.Forms
Imports System.Drawing

Public Class eZeeImageControl_Preview
#Region " Property "
    Private m_image As Image
    Public Property _Image() As Image
        Get
            Return m_image
        End Get
        Set(ByVal value As Image)
            m_image = value
            Call SetImage()
        End Set
    End Property
#End Region

#Region " Private Function "
    Private Sub SetImage()
        If m_image Is Nothing Then
        Else
            If m_image.Size.Height > picBoxMain.Height Or m_image.Size.Width > picBoxMain.Width Then
                picBoxMain.SizeMode = PictureBoxSizeMode.StretchImage
            Else
                picBoxMain.SizeMode = PictureBoxSizeMode.CenterImage
            End If

            picBoxMain.Image = m_image
        End If
    End Sub
#End Region

End Class