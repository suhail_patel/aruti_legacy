<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeImageControl
    Inherits System.Windows.Forms.UserControl

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ofdImageDialog = New System.Windows.Forms.OpenFileDialog
        Me.objbtnRefresh = New eZee.Common.eZeeGradientButton
        Me.objbtnCapture = New eZee.Common.eZeeGradientButton
        Me.objbtnDelete = New eZee.Common.eZeeGradientButton
        Me.objbtnAdd = New eZee.Common.eZeeGradientButton
        Me.objPicImage = New System.Windows.Forms.PictureBox
        CType(Me.objPicImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objbtnRefresh
        '
        Me.objbtnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnRefresh.BackColor = System.Drawing.Color.Transparent
        Me.objbtnRefresh.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnRefresh.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnRefresh.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnRefresh.BorderSelected = False
        Me.objbtnRefresh.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnRefresh.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnRefresh.Image = Global.eZee.Common.My.Resources.Resources.Picture_Refresh
        Me.objbtnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnRefresh.Location = New System.Drawing.Point(102, 35)
        Me.objbtnRefresh.Name = "objbtnRefresh"
        Me.objbtnRefresh.Size = New System.Drawing.Size(19, 19)
        Me.objbtnRefresh.TabIndex = 3
        '
        'objbtnCapture
        '
        Me.objbtnCapture.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnCapture.BackColor = System.Drawing.Color.Transparent
        Me.objbtnCapture.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnCapture.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnCapture.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnCapture.BorderSelected = False
        Me.objbtnCapture.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnCapture.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnCapture.Image = Global.eZee.Common.My.Resources.Resources.Picture_Capture
        Me.objbtnCapture.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnCapture.Location = New System.Drawing.Point(102, 0)
        Me.objbtnCapture.Name = "objbtnCapture"
        Me.objbtnCapture.Size = New System.Drawing.Size(19, 19)
        Me.objbtnCapture.TabIndex = 4
        Me.objbtnCapture.Visible = False
        '
        'objbtnDelete
        '
        Me.objbtnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnDelete.BackColor = System.Drawing.Color.Transparent
        Me.objbtnDelete.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnDelete.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnDelete.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnDelete.BorderSelected = False
        Me.objbtnDelete.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnDelete.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnDelete.Image = Global.eZee.Common.My.Resources.Resources.Picture_Delete
        Me.objbtnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnDelete.Location = New System.Drawing.Point(102, 81)
        Me.objbtnDelete.Name = "objbtnDelete"
        Me.objbtnDelete.Size = New System.Drawing.Size(19, 19)
        Me.objbtnDelete.TabIndex = 2
        '
        'objbtnAdd
        '
        Me.objbtnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdd.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAdd.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAdd.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAdd.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnAdd.BorderSelected = False
        Me.objbtnAdd.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnAdd.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAdd.Image = Global.eZee.Common.My.Resources.Resources.Picture_Add
        Me.objbtnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAdd.Location = New System.Drawing.Point(102, 58)
        Me.objbtnAdd.Name = "objbtnAdd"
        Me.objbtnAdd.Size = New System.Drawing.Size(19, 19)
        Me.objbtnAdd.TabIndex = 1
        '
        'objPicImage
        '
        Me.objPicImage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objPicImage.BackgroundImage = Global.eZee.Common.My.Resources.Resources.No_Image
        Me.objPicImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objPicImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objPicImage.ImageLocation = ""
        Me.objPicImage.InitialImage = Global.eZee.Common.My.Resources.Resources.No_Image
        Me.objPicImage.Location = New System.Drawing.Point(0, 0)
        Me.objPicImage.Name = "objPicImage"
        Me.objPicImage.Size = New System.Drawing.Size(100, 100)
        Me.objPicImage.TabIndex = 0
        Me.objPicImage.TabStop = False
        '
        'eZeeImageControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.objbtnRefresh)
        Me.Controls.Add(Me.objbtnCapture)
        Me.Controls.Add(Me.objbtnDelete)
        Me.Controls.Add(Me.objbtnAdd)
        Me.Controls.Add(Me.objPicImage)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "eZeeImageControl"
        Me.Size = New System.Drawing.Size(126, 100)
        CType(Me.objPicImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents objPicImage As System.Windows.Forms.PictureBox
    Friend WithEvents ofdImageDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents objbtnAdd As eZeeGradientButton
    Friend WithEvents objbtnDelete As eZeeGradientButton
    Friend WithEvents objbtnCapture As eZeeGradientButton
    Friend WithEvents objbtnRefresh As eZeeGradientButton

End Class
