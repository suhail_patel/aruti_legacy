<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeImageControl_Preview
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pblMain = New System.Windows.Forms.Panel
        Me.picBoxMain = New System.Windows.Forms.PictureBox
        Me.pblMain.SuspendLayout()
        CType(Me.picBoxMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pblMain
        '
        Me.pblMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pblMain.Controls.Add(Me.picBoxMain)
        Me.pblMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pblMain.Location = New System.Drawing.Point(0, 0)
        Me.pblMain.Name = "pblMain"
        Me.pblMain.Size = New System.Drawing.Size(200, 200)
        Me.pblMain.TabIndex = 0
        '
        'picBoxMain
        '
        Me.picBoxMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picBoxMain.Location = New System.Drawing.Point(0, 0)
        Me.picBoxMain.Name = "picBoxMain"
        Me.picBoxMain.Size = New System.Drawing.Size(198, 198)
        Me.picBoxMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBoxMain.TabIndex = 0
        Me.picBoxMain.TabStop = False
        '
        'frmPreview
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(200, 200)
        Me.Controls.Add(Me.pblMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "eZeeImageControl_Preview"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Preview"
        Me.pblMain.ResumeLayout(False)
        CType(Me.picBoxMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pblMain As System.Windows.Forms.Panel
    Friend WithEvents picBoxMain As System.Windows.Forms.PictureBox
End Class
