<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeImageControl_Capture
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstDevices = New System.Windows.Forms.ListBox
        Me.objbtnCancel = New eZeeGradientButton
        Me.objbtnOk = New eZeeGradientButton
        Me.objbtnStart = New eZeeGradientButton
        Me.objbtnStop = New eZeeGradientButton
        Me.objbtnCapture = New eZeeGradientButton
        Me.picCapture = New System.Windows.Forms.PictureBox
        CType(Me.picCapture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstDevices
        '
        Me.lstDevices.FormattingEnabled = True
        Me.lstDevices.Location = New System.Drawing.Point(226, 304)
        Me.lstDevices.Name = "lstDevices"
        Me.lstDevices.Size = New System.Drawing.Size(182, 30)
        Me.lstDevices.TabIndex = 5
        '
        'objbtnCancel
        '
        Me.objbtnCancel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnCancel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnCancel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnCancel.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnCancel.BorderSelected = False
        Me.objbtnCancel.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnCancel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnCancel.Image = My.Resources.Resources.Picture_Capture_Cancel
        Me.objbtnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnCancel.Location = New System.Drawing.Point(217, 254)
        Me.objbtnCancel.Name = "objbtnCancel"
        Me.objbtnCancel.Size = New System.Drawing.Size(32, 32)
        Me.objbtnCancel.TabIndex = 0
        '
        'objbtnOk
        '
        Me.objbtnOk.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOk.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOk.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOk.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnOk.BorderSelected = False
        Me.objbtnOk.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnOk.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOk.Image = My.Resources.Resources.Picture_Capture_Ok
        Me.objbtnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOk.Location = New System.Drawing.Point(179, 254)
        Me.objbtnOk.Name = "objbtnOk"
        Me.objbtnOk.Size = New System.Drawing.Size(32, 32)
        Me.objbtnOk.TabIndex = 3
        '
        'objbtnStart
        '
        Me.objbtnStart.BackColor = System.Drawing.Color.Transparent
        Me.objbtnStart.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnStart.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnStart.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnStart.BorderSelected = False
        Me.objbtnStart.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnStart.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnStart.Enabled = False
        Me.objbtnStart.Image = My.Resources.Resources.Picture_Capture_Start
        Me.objbtnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnStart.ImageSelected = My.Resources.Resources.Picture_Capture_Start
        Me.objbtnStart.Location = New System.Drawing.Point(3, 254)
        Me.objbtnStart.Name = "objbtnStart"
        Me.objbtnStart.Size = New System.Drawing.Size(32, 32)
        Me.objbtnStart.TabIndex = 0
        '
        'objbtnStop
        '
        Me.objbtnStop.BackColor = System.Drawing.Color.Transparent
        Me.objbtnStop.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnStop.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnStop.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnStop.BorderSelected = False
        Me.objbtnStop.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnStop.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnStop.Enabled = False
        Me.objbtnStop.Image = My.Resources.Resources.Picture_Capture_Stop
        Me.objbtnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnStop.ImageSelected = My.Resources.Resources.Picture_Capture_Stop
        Me.objbtnStop.Location = New System.Drawing.Point(41, 254)
        Me.objbtnStop.Name = "objbtnStop"
        Me.objbtnStop.Size = New System.Drawing.Size(32, 32)
        Me.objbtnStop.TabIndex = 1
        '
        'objbtnCapture
        '
        Me.objbtnCapture.BackColor = System.Drawing.Color.Transparent
        Me.objbtnCapture.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnCapture.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnCapture.BorderNormalColor = System.Drawing.Color.Black
        Me.objbtnCapture.BorderSelected = False
        Me.objbtnCapture.BorderSelectedColor = System.Drawing.Color.Black
        Me.objbtnCapture.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnCapture.Enabled = False
        Me.objbtnCapture.Image = My.Resources.Resources.Picture_Capture_Big
        Me.objbtnCapture.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnCapture.ImageSelected = My.Resources.Resources.Picture_Capture_Big
        Me.objbtnCapture.Location = New System.Drawing.Point(79, 254)
        Me.objbtnCapture.Name = "objbtnCapture"
        Me.objbtnCapture.Size = New System.Drawing.Size(32, 32)
        Me.objbtnCapture.TabIndex = 2
        '
        'picCapture
        '
        Me.picCapture.BackColor = System.Drawing.Color.White
        Me.picCapture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.picCapture.Location = New System.Drawing.Point(3, 3)
        Me.picCapture.Name = "picCapture"
        Me.picCapture.Size = New System.Drawing.Size(246, 222)
        Me.picCapture.TabIndex = 5
        Me.picCapture.TabStop = False
        '
        'eZeeImageControl_Capture
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkGray
        Me.ClientSize = New System.Drawing.Size(254, 294)
        Me.Controls.Add(Me.objbtnCancel)
        Me.Controls.Add(Me.objbtnOk)
        Me.Controls.Add(Me.objbtnStart)
        Me.Controls.Add(Me.objbtnStop)
        Me.Controls.Add(Me.objbtnCapture)
        Me.Controls.Add(Me.picCapture)
        Me.Controls.Add(Me.lstDevices)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "eZeeImageControl_Capture"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capture Image"
        CType(Me.picCapture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstDevices As System.Windows.Forms.ListBox
    Friend WithEvents picCapture As System.Windows.Forms.PictureBox
    Friend WithEvents objbtnCancel As eZeeGradientButton
    Friend WithEvents objbtnOk As eZeeGradientButton
    Friend WithEvents objbtnStart As eZeeGradientButton
    Friend WithEvents objbtnStop As eZeeGradientButton
    Friend WithEvents objbtnCapture As eZeeGradientButton
End Class
