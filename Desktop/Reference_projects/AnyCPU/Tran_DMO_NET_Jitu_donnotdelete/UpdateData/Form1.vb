Imports System.Security

Public Class Form1
    Dim ds As New DataSet
    Dim dt() As DataRow
    Dim ObjDO As eZeeCommonLib.clsDataOperation

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim ds As New DataSet("Data")
        'Dim dt As New DataTable("Data")

        'dt.Columns.Add("Id", System.Type.GetType("System.Int32"))
        'dt.Columns.Add("Version", System.Type.GetType("System.String"))
        'dt.Columns.Add("CheckType", System.Type.GetType("System.Int32"))
        'dt.Columns.Add("ConditionValue1", System.Type.GetType("System.String"))
        'dt.Columns.Add("ConditionValue2", System.Type.GetType("System.String"))
        'dt.Columns.Add("Script", System.Type.GetType("System.String"))

        'ds.Tables.Add(dt)
        'ds.WriteXml("c:\Data.XML", XmlWriteMode.WriteSchema)
        Dim str As String = My.Resources.Data
        'str = eZeeCommonLib.clsSecurity.Encrypt(str, "Naimish")
        'str = eZeeCommonLib.clsSecurity.Decrypt(str, "Naimish")

        ds.ReadXml(New System.IO.StringReader(str))
        'ds.ReadXml("c:\Data_Tran.XML")
        'For i As Integer = 0 To 100
        '    Dim dr As DataRow = ds.Tables(0).NewRow
        '    dr.Item("Id") = i
        '    dr.Item("Version") = "6.1.0.1"
        '    dr.Item("CheckType") = 0
        '    dr.Item("ConditionValue1") = ""
        '    dr.Item("ConditionValue2") = ""
        '    dr.Item("Script") = "Select 1"
        '    ds.Tables(0).Rows.Add(dr)
        'Next
        'ds.WriteXml("c:\Data.XML", XmlWriteMode.WriteSchema)

        dt = ds.Tables(0).Select("Version >= '6.1.0.1'")

        pbProgress.Maximum = dt.Length

        'Dim objData As New eZeeCommonLib.eZeeDatabase
        'objData.ServerName = "(Local)"
        'objData.Connect()

        'objData.fillDatabaseList()
        BackgroundWorker1.RunWorkerAsync()

        'End
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim strQ As String = String.Empty
        ' Using ObjDO As New eZeeCommonLib.clsDataOperation
        'ObjDO.BindTransaction()

        'ObjDO.ExecNonQuery(strQ)
        objDataOperation.ExecNonQuery("IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = '[" & gDataBaseName & "]') CREATE DATABASE " & gDataBaseName)

        objDataOperation.ExecNonQuery("USE " & gDataBaseName)
        For iCnt As Integer = 0 To dt.Length - 1
            strQ = dt(iCnt).Item("Script")
            objDataOperation.ExecNonQuery(strQ)
            If Not objDataOperation.ErrorMessage = "" Then
                MsgBox(objDataOperation.ErrorMessage)
                Exit For
            End If
            BackgroundWorker1.ReportProgress(iCnt + 1)
            If iCnt = 4 Then
                System.Threading.Thread.Sleep(1000)
            Else
                System.Threading.Thread.Sleep(100)
            End If
        Next

        'If ObjDO.ErrorMessage <> "" Then
        '    ObjDO.ReleaseTransaction(False)
        'Else
        '    ObjDO.ReleaseTransaction(True)
        'End If

        'End Using '
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        pbProgress.Value = e.ProgressPercentage
        lblProgress.Text = pbProgress.Value & "/" & pbProgress.Maximum
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        'MsgBox("Done").
        'End
        Me.Close()
    End Sub
End Class
