Public Class XMLHelper
    Public Shared ReadOnly ConTable As String = "Table"
    Public Shared ReadOnly ConStoreProcedure As String = "StoreProcedure"
    Public Shared ReadOnly ConView As String = "View"
    Public Shared ReadOnly ConTrigger As String = "Trigger"
    Public Shared ReadOnly ConUpdateScript As String = "UpdateScript"
    Public Shared ReadOnly ConType As String = "Type"
    Public Shared ReadOnly ConIndex As String = "Index"

#Region " Conditions "
    Public Shared ReadOnly Condition_DataType As String = "IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'{0}' AND ss.name = N'dbo')"
#End Region
End Class
