Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms

Public Class TreeViewScroll
    Inherits TreeView

    Private Const SB_HORZ As Integer = 0
    Private Const SB_VERT As Integer = 1

    Private Const WM_VSCROLL As Integer = &H115
    Private Const WM_HSCROLL As Integer = &H114

    <System.Runtime.InteropServices.DllImport("user32.d ll")> _
    Private Shared Function GetScrollPos(ByVal hWnd As Integer, ByVal nBar As Integer) As Integer
    End Function
    Public ReadOnly Property HScrollPos() As Integer
        Get
            Return GetScrollPos(CInt(Fix(Handle)), SB_VERT)
        End Get
    End Property
    Public ReadOnly Property VScrollPos() As Integer
        Get
            Return GetScrollPos(CInt(Fix(Handle)), SB_HORZ)
        End Get
    End Property

    Public Event Scroll As ScrollEventHandler

    Protected Overrides Sub WndProc(ByRef m As Message)
        If ScrollEvent IsNot Nothing Then
            Select Case m.Msg
                Case WM_VSCROLL
                    Dim t As ScrollEventType = CType(System.Enum.Parse(GetType(ScrollEventType), (m.WParam.ToInt32() And 65535).ToString()), ScrollEventType)
                    RaiseEvent Scroll(m.HWnd, New ScrollEventArgs(t, (CInt(Fix(m.WParam.ToInt64() >> 16))) And 255))

            End Select
        End If
        MyBase.WndProc(m)
    End Sub
End Class