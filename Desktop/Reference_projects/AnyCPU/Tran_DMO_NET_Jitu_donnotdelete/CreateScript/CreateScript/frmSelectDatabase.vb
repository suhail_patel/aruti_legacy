Public Class frmSelectDatabase
    Private mstrDatabaseName As String = ""

    Public ReadOnly Property _DatabaseName() As String
        Get
            Return mstrDatabaseName
        End Get
    End Property

    Private mdsv As DatabaseServer
    Public WriteOnly Property _DatabaseServer() As DatabaseServer
        Set(ByVal value As DatabaseServer)
            mdsv = value
        End Set
    End Property

    Public Function GetDatabaseName() As String()
        Dim connString As String = ""
        Dim str As New List(Of String)
        Try
            connString = DatabaseCompare.Domain.Connection.ConnectionString(mdsv.Server, mdsv.User, mdsv.Password, mdsv.Integrated)
            Using conn As New SqlClient.SqlConnection(connString)
                conn.Open()
                Using command As SqlClient.SqlCommand = conn.CreateCommand()
                    command.CommandText = "select * from sys.databases"

                    Using reader As SqlClient.SqlDataReader = command.ExecuteReader()
                        Do While reader.Read()
                            str.Add(reader.GetString(0).Trim())
                        Loop
                    End Using
                End Using
                conn.Close()
                conn.Dispose()
            End Using
            '            catch ( Exception ex )
        Catch ex As Exception
            Throw ex
        End Try
        Return str.ToArray
    End Function

    Private Sub frmSelectDatabase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lstTestDatabase.DataSource = Me.GetDatabaseName()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally

        End Try
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        mstrDatabaseName = lstTestDatabase.Text
    End Sub
End Class