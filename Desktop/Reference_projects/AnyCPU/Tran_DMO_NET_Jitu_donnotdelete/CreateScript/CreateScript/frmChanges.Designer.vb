<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChanges
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlCompare = New System.Windows.Forms.SplitContainer
        Me.txtScriptDatabase = New SqlBuilder.Controls.RichTextBoxParser
        Me.tsScriptDatabase = New System.Windows.Forms.ToolStrip
        Me.lblScriptDatabase = New System.Windows.Forms.ToolStripLabel
        Me.txtTestDatabase = New SqlBuilder.Controls.RichTextBoxParser
        Me.tsTestDatabase = New System.Windows.Forms.ToolStrip
        Me.lblTestDatabase = New System.Windows.Forms.ToolStripLabel
        Me.btnCancel = New System.Windows.Forms.Button
        Me.pnlCompare.Panel1.SuspendLayout()
        Me.pnlCompare.Panel2.SuspendLayout()
        Me.pnlCompare.SuspendLayout()
        Me.tsScriptDatabase.SuspendLayout()
        Me.tsTestDatabase.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCompare
        '
        Me.pnlCompare.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCompare.Location = New System.Drawing.Point(0, 0)
        Me.pnlCompare.Name = "pnlCompare"
        '
        'pnlCompare.Panel1
        '
        Me.pnlCompare.Panel1.Controls.Add(Me.txtScriptDatabase)
        Me.pnlCompare.Panel1.Controls.Add(Me.tsScriptDatabase)
        '
        'pnlCompare.Panel2
        '
        Me.pnlCompare.Panel2.Controls.Add(Me.txtTestDatabase)
        Me.pnlCompare.Panel2.Controls.Add(Me.tsTestDatabase)
        Me.pnlCompare.Panel2.Controls.Add(Me.btnCancel)
        Me.pnlCompare.Size = New System.Drawing.Size(800, 496)
        Me.pnlCompare.SplitterDistance = 406
        Me.pnlCompare.TabIndex = 0
        '
        'txtScriptDatabase
        '
        Me.txtScriptDatabase.BackColor = System.Drawing.Color.White
        Me.txtScriptDatabase.DetectUrls = False
        Me.txtScriptDatabase.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtScriptDatabase.FirstVisibleLine = 1
        Me.txtScriptDatabase.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScriptDatabase.HideSelection = False
        Me.txtScriptDatabase.Location = New System.Drawing.Point(0, 26)
        Me.txtScriptDatabase.Name = "txtScriptDatabase"
        Me.txtScriptDatabase.Size = New System.Drawing.Size(406, 470)
        Me.txtScriptDatabase.StatusBar = Nothing
        Me.txtScriptDatabase.TabIndex = 11
        Me.txtScriptDatabase.Text = ""
        Me.txtScriptDatabase.TextModified = False
        '
        'tsScriptDatabase
        '
        Me.tsScriptDatabase.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsScriptDatabase.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblScriptDatabase})
        Me.tsScriptDatabase.Location = New System.Drawing.Point(0, 0)
        Me.tsScriptDatabase.Name = "tsScriptDatabase"
        Me.tsScriptDatabase.Size = New System.Drawing.Size(406, 26)
        Me.tsScriptDatabase.TabIndex = 12
        Me.tsScriptDatabase.Text = "ToolStrip"
        '
        'lblScriptDatabase
        '
        Me.lblScriptDatabase.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScriptDatabase.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblScriptDatabase.Name = "lblScriptDatabase"
        Me.lblScriptDatabase.Size = New System.Drawing.Size(160, 23)
        Me.lblScriptDatabase.Text = "Script Database"
        '
        'txtTestDatabase
        '
        Me.txtTestDatabase.BackColor = System.Drawing.Color.White
        Me.txtTestDatabase.DetectUrls = False
        Me.txtTestDatabase.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTestDatabase.FirstVisibleLine = 1
        Me.txtTestDatabase.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTestDatabase.HideSelection = False
        Me.txtTestDatabase.Location = New System.Drawing.Point(0, 26)
        Me.txtTestDatabase.Name = "txtTestDatabase"
        Me.txtTestDatabase.Size = New System.Drawing.Size(390, 470)
        Me.txtTestDatabase.StatusBar = Nothing
        Me.txtTestDatabase.TabIndex = 12
        Me.txtTestDatabase.Text = ""
        Me.txtTestDatabase.TextModified = False
        '
        'tsTestDatabase
        '
        Me.tsTestDatabase.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsTestDatabase.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblTestDatabase})
        Me.tsTestDatabase.Location = New System.Drawing.Point(0, 0)
        Me.tsTestDatabase.Name = "tsTestDatabase"
        Me.tsTestDatabase.Size = New System.Drawing.Size(390, 26)
        Me.tsTestDatabase.TabIndex = 13
        Me.tsTestDatabase.Text = "ToolStrip1"
        '
        'lblTestDatabase
        '
        Me.lblTestDatabase.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTestDatabase.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblTestDatabase.Name = "lblTestDatabase"
        Me.lblTestDatabase.Size = New System.Drawing.Size(145, 23)
        Me.lblTestDatabase.Text = "Test Database"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(253, 461)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Button1"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmChanges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(800, 496)
        Me.Controls.Add(Me.pnlCompare)
        Me.Name = "frmChanges"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Compare"
        Me.pnlCompare.Panel1.ResumeLayout(False)
        Me.pnlCompare.Panel1.PerformLayout()
        Me.pnlCompare.Panel2.ResumeLayout(False)
        Me.pnlCompare.Panel2.PerformLayout()
        Me.pnlCompare.ResumeLayout(False)
        Me.tsScriptDatabase.ResumeLayout(False)
        Me.tsScriptDatabase.PerformLayout()
        Me.tsTestDatabase.ResumeLayout(False)
        Me.tsTestDatabase.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCompare As System.Windows.Forms.SplitContainer
    Friend WithEvents txtScriptDatabase As SqlBuilder.Controls.RichTextBoxParser
    Friend WithEvents txtTestDatabase As SqlBuilder.Controls.RichTextBoxParser
    Friend WithEvents tsScriptDatabase As System.Windows.Forms.ToolStrip
    Friend WithEvents lblScriptDatabase As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tsTestDatabase As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTestDatabase As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
