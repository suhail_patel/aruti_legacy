Namespace SQLSMO
    Public Class Wait
#Region " Property "
        Private dataset_renamed As DataSet

        Public Property _DataSet() As DataSet
            Get
                Return dataset_renamed
            End Get
            Set(ByVal value As DataSet)
                dataset_renamed = value
            End Set
        End Property

        Private m_dsv As DatabaseServer
        Public Property _DatabaseServer() As DatabaseServer
            Get
                Return m_dsv
            End Get
            Set(ByVal value As DatabaseServer)
                m_dsv = value
            End Set
        End Property
#End Region

#Region " Public Method "
        Public Function displayDialog() As Boolean
            Me.ShowDialog()
        End Function
#End Region

        Private databases As New ArrayList

#Region " Form "
        Private Sub Wait_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                BackgroundWorker1.RunWorkerAsync()

            Catch ex As Exception
                Throw ex
                Me.Close()
            End Try
        End Sub
#End Region

#Region " Controls "
        Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
            Dim strQ As String = String.Empty
            Try
                Dim str As String = DatabaseCompare.Domain.Connection.ConnectionString(m_dsv.Server, m_dsv.User, m_dsv.Password, m_dsv.Integrated, m_dsv.ScriptDatabase)
                Dim ObjDO As New DatabaseCompare.Domain.Database(str)
                ObjDO.TestConnection()
                'Type
                BackgroundWorker1.ReportProgress(dataset_renamed.Tables(XMLHelper.ConType).Rows.Count, "Type")

                For iCnt As Integer = 0 To dataset_renamed.Tables(XMLHelper.ConType).Rows.Count - 1
                    BackgroundWorker1.ReportProgress(iCnt + 1, "PB")
                    strQ = String.Format(XMLHelper.Condition_DataType, _
                        dataset_renamed.Tables(XMLHelper.ConType).Rows(iCnt)("ConditionValue")) & _
                    "  " & dataset_renamed.Tables(XMLHelper.ConType).Rows(iCnt)("Script") & " "
                    ObjDO.ExecNonQuery(strQ)
                Next

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private strMsgText As String = "Updating {0}: "
        Private strProgressText As String = "Current {0} out of {1}"
        Private mstrObject As String = "Database"

        Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
            Select Case CStr(e.UserState)
                Case "PB"
                    pbProgress.Value = e.ProgressPercentage
                    lblProgress.Text = String.Format(strMsgText, mstrObject) & String.Format(strProgressText, pbProgress.Value, pbProgress.Maximum)
                Case Else
                    mstrObject = e.UserState
                    pbProgress.Value = 0
                    pbProgress.Maximum = e.ProgressPercentage
            End Select

        End Sub

        Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
            Me.Close()
        End Sub
#End Region

    End Class
End Namespace