Imports SQLDMO

Public Class Class1
    Public s_transfer As New TransferClass()

    Friend Enum Db_Types
        Defaults
        Functions
        Rules
        SPs
        Tables
        Triggers
        UDTs
        Views
    End Enum

    Public Sub ScriptDatabase(ByVal db As Database)
        ' TransferClass tc = new TransferClass();
        s_transfer.ScriptType = SQLDMO_SCRIPT_TYPE.SQLDMOScript_DRI_All Or SQLDMO_SCRIPT_TYPE.SQLDMOScript_TransferDefault
        s_transfer.ScriptType = s_transfer.ScriptType And ((Not SQLDMO_SCRIPT_TYPE.SQLDMOScript_IncludeHeaders))
        s_transfer.DropDestObjectsFirst = True
        s_transfer.CopySchema = True
        s_transfer.CopyAllObjects = False ' first stop all objects

        Dim workingFolder As String = "C:\Test\Default\"
        s_transfer.CopyAllUserDefinedDatatypes = True

        GenerateScript(s_transfer, workingFolder, db)
        s_transfer.CopyAllUserDefinedDatatypes = False

        'For dt As Db_Types = Db_Types.Defaults To Db_Types.Views
        '    Select Case dt
        '        Case Db_Types.Defaults
        '            s_transfer.CopyAllDefaults = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllDefaults = False
        '        Case Db_Types.Functions
        '            s_transfer.CopyAllFunctions = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllFunctions = False
        '        Case Db_Types.Rules
        '            s_transfer.CopyAllRules = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllRules = False
        '        Case Db_Types.SPs
        '            s_transfer.CopyAllStoredProcedures = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllStoredProcedures = False
        '        Case Db_Types.Tables
        '            s_transfer.CopyAllTables = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllTables = False
        '        Case Db_Types.Triggers
        '            s_transfer.CopyAllTriggers = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllTriggers = False
        '        Case Db_Types.UDTs
        '            s_transfer.CopyAllUserDefinedDatatypes = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllUserDefinedDatatypes = False
        '        Case Db_Types.Views
        '            s_transfer.CopyAllViews = True
        '            GenerateScript(s_transfer, workingFolder, db)
        '            s_transfer.CopyAllViews = False
        '    End Select
        '    s_transfer.RemoveAllObjects()
        'Next dt


        'If ScriptDone IsNot Nothing Then
        '    ScriptDone()
        'End If
    End Sub

    Private Sub GenerateScript(ByVal tc As TransferClass, ByVal folder As String, ByVal db As Database)
        db.ScriptTransfer(tc, SQLDMO_XFRSCRIPTMODE_TYPE.SQLDMOXfrFile_SingleFilePerObject, folder)
    End Sub
End Class
