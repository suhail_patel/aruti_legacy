Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Data.SqlClient

Namespace DatabaseCompare.Domain
	''' <summary>
	''' Summary description for View.
	''' </summary>
	Public Class View
        Inherits DatabaseObject

        Private textDefinition_Renamed As String

        Public Sub New(ByVal name As String, ByVal id As Integer)
            MyBase.New(name, id)
        End Sub

        Public Overrides Sub GatherData(ByRef conn As SqlConnection)
            MyBase.GatherData(conn)
            Using command As SqlCommand = conn.CreateCommand()
                command.CommandText = "select text from syscomments where id=@id"
                'command.Parameters.Add("@id", Me.Id)
                command.Parameters.Add(New SqlParameter("@id", Me.Id))
                Using reader As SqlDataReader = command.ExecuteReader()
                    Do While reader.Read()
                        textDefinition_Renamed &= reader.GetString(0).Trim()
                    Loop
                End Using
            End Using
        End Sub

        Protected Overrides Function LocalCompare(ByVal obj As DatabaseObject) As Boolean
            If TypeOf obj Is View Then
                Return Me.TextDefinition = (CType(obj, View)).TextDefinition
            End If
            Return False
        End Function

        Public Overrides Property TextDefinition() As String
            Get
                Return textDefinition_Renamed
            End Get
            Set(ByVal value As String)
                textDefinition_Renamed = value
            End Set
        End Property
    End Class
End Namespace
