Imports Microsoft.VisualBasic
Imports System

Namespace DatabaseCompare.Domain
	''' <summary>
	''' Summary description for Column.
	''' </summary>
	Public Class Column
		Private name_Renamed As String
		Private type_Renamed As Integer
		Private length_Renamed As Integer
		Private scale_Renamed As Integer

		Public Sub New(ByVal name As String, ByVal type As Integer, ByVal length As Integer, ByVal scale As Integer)
			Me.name_Renamed = name
			Me.type_Renamed = type
			Me.length_Renamed = length
			Me.scale_Renamed = scale
		End Sub

		Public Property Name() As String
			Get
				Return name_Renamed
			End Get
			Set(ByVal value As String)
				name_Renamed = value
			End Set
		End Property

		Public Property Type() As Integer
			Get
				Return type_Renamed
			End Get
			Set(ByVal value As Integer)
				type_Renamed = value
			End Set
		End Property

		Public Property Length() As Integer
			Get
				Return length_Renamed
			End Get
			Set(ByVal value As Integer)
				length_Renamed = value
			End Set
		End Property

		Public Property Scale() As Integer
			Get
				Return scale_Renamed
			End Get
			Set(ByVal value As Integer)
				scale_Renamed = value
			End Set
		End Property

		Public Function CompareTo(ByVal c As Column) As Boolean
			Return Me.Name = c.Name AndAlso Me.Type = c.Type AndAlso Me.Length = c.Length AndAlso Me.Scale = c.Scale
		End Function
	End Class
End Namespace
