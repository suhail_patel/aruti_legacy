Imports Microsoft.VisualBasic
Imports System
Imports System.Data.SqlClient

Namespace DatabaseCompare.Domain
    ''' <summary>
    ''' Summary description for Function.
    ''' </summary>
    Public Class Index
        Private name_Renamed As String
        Private system_type_id_Renamed As Integer
        Private user_type_id_Renamed As Integer
        Private textDefinition_Renamed As String

        Public Sub New(ByVal name As String, ByVal system_type_id As Integer, ByVal user_type_id As Integer)
            Me.name_Renamed = name
            Me.system_type_id_Renamed = system_type_id
            Me.user_type_id_Renamed = user_type_id
        End Sub

        Public Property Name() As String
            Get
                Return name_Renamed
            End Get
            Set(ByVal value As String)
                name_Renamed = value
            End Set
        End Property

        Public Property System_Type_Id() As Integer
            Get
                Return system_type_id_Renamed
            End Get
            Set(ByVal value As Integer)
                system_type_id_Renamed = value
            End Set
        End Property

        Public Property User_Type_Id() As Integer
            Get
                Return user_type_id_Renamed
            End Get
            Set(ByVal value As Integer)
                user_type_id_Renamed = value
            End Set
        End Property

        Public Function CompareTo(ByVal c As Index) As Boolean
            Return Me.Name = c.Name AndAlso Me.system_type_id_Renamed = c.system_type_id_Renamed
        End Function

        Public Sub GatherData(ByVal conn As SqlConnection)
            Using command As SqlCommand = conn.CreateCommand()
                command.CommandText = "SELECT 'CREATE TYPE [dbo].[' + U1.name + '] FROM [' + S1.name +'] ' + CASE u1.is_nullable WHEN 0 THEN 'NOT NULL' ELSE '' END " & _
                        "FROM sys.types S1, sys.types U1 " & _
                        "WHERE u1.system_type_id = s1.user_type_id " & _
                        "AND U1.user_type_id = @id "

                command.Parameters.Add(New SqlParameter("@id", Me.user_type_id_Renamed))
                Using reader As SqlDataReader = command.ExecuteReader()
                    Do While reader.Read()
                        textDefinition_Renamed &= reader.GetString(0).Trim()
                    Loop
                End Using
            End Using
        End Sub

        Public ReadOnly Property TextDefinition() As String
            Get
                Return textDefinition_Renamed
            End Get
        End Property
    End Class
End Namespace
