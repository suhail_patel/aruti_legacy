Imports Microsoft.VisualBasic
Imports System

Namespace DatabaseCompare.Domain
	''' <summary>
	''' Summary description for UserTable.
	''' </summary>
	Public Class UserTable
        Inherits DatabaseObject


        Public Sub New(ByVal name As String, ByVal id As Integer)
            MyBase.New(name, id)
        End Sub

        Private textDefinition_Renamed As String
        Public Overrides Property TextDefinition() As String
            Get
                Return textDefinition_Renamed
            End Get
            Set(ByVal value As String)
                textDefinition_Renamed = value
            End Set
        End Property
    End Class


End Namespace
