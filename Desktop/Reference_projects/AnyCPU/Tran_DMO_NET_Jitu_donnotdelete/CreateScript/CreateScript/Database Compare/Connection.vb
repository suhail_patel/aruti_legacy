Namespace DatabaseCompare.Domain
    Public Class Connection
        Public Shared Function ConnectionString(ByVal Server As String, ByVal User As String, ByVal Password As String, ByVal Integrated As Boolean) As String
            Dim str As String = ""

            If Integrated Then
                str = String.Format("Data Source={0};Integrated Security=SSPI;", Server)
            Else
                str = String.Format("Data Source={0};UId={1};Pwd={2};", Server, User, Password)
            End If
            Return str
        End Function

        Public Shared Function ConnectionString(ByVal Server As String, ByVal User As String, ByVal Password As String, ByVal Integrated As Boolean, ByVal Database As String) As String
            Dim str As String = ""

            If Integrated Then
                str = String.Format("Data Source={0};Initial Catalog={1};Integrated Security=SSPI;", Server, Database)
            Else
                str = String.Format("Data Source={0};Initial Catalog={1};UId={2};Pwd={3};", Server, Database, User, Password)
            End If
            Return str
        End Function
    End Class
End Namespace
