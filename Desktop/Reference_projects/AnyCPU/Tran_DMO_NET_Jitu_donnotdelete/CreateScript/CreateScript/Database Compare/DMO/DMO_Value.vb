'***** Hello
Imports Microsoft.SqlServer.Management

Public Class DMO_Value
    Private SQLServerConnection As New Common.ServerConnection
    Private SQLServer As Smo.Server
    Private SQLDatabse As Smo.Database

    Public IsActive As Boolean = False

    Public Sub New()
    End Sub

    Public Sub New(ByVal dsv As DatabaseServer, ByVal DatabaseName As String)
        If dsv.Integrated Then
            SQLServerConnection.LoginSecure = True
        Else
            SQLServerConnection.LoginSecure = False
            SQLServerConnection.Login = dsv.User
            SQLServerConnection.Password = dsv.Password
        End If

        SQLServerConnection.ServerInstance = dsv.Server
        SQLServerConnection.Connect()

        SQLServer = New Smo.Server(SQLServerConnection)
        SQLDatabse = SQLServer.Databases(DatabaseName)
        
    End Sub

    Public Function Type(ByVal Name As String) As String
        If Not IsActive Then Return ""
        Return "" 'SQLDatabse.UserDefinedDataTypes.Item(Name).Script(SQLDMO.SQLDMO_SCRIPT_TYPE.SQLDMOScript_Default)

    End Function

    Public Function Table(ByVal Name As String) As String
        If Not IsActive Then Return ""
        Return "" 'SQLDatabse.Tables.Item(Name).Script(SQLDMO.SQLDMO_SCRIPT_TYPE.SQLDMOScript_Default)
    End Function

    Public Function StoreProcedure(ByVal Name As String) As String
        If Not IsActive Then Return ""
        Return "" 'SQLDatabse.StoredProcedures.Item(Name).Script(SQLDMO.SQLDMO_SCRIPT_TYPE.SQLDMOScript_Default)
    End Function

    Public Function View(ByVal Name As String) As String
        If Not IsActive Then Return ""
        Return "" 'SQLDatabse.Views.Item(Name).Script(SQLDMO.SQLDMO_SCRIPT_TYPE.SQLDMOScript_Default)
    End Function

    Public Function [Function](ByVal Name As String) As String
        If Not IsActive Then Return ""
        Return "" 'SQLDatabse.UserDefinedFunctions.Item(Name).Script(SQLDMO.SQLDMO_SCRIPT_TYPE.SQLDMOScript_Default)
    End Function

    Private m_Compare As New List(Of DMO_Object)

    Public Sub LoadObject()
        m_Compare.Clear()
        Dim sTable As New Smo.ScriptingOptions

        sTable.Indexes = True
        sTable.NonClusteredIndexes = True
        sTable.ExtendedProperties = True
        sTable.IncludeIfNotExists = True

        Dim sNormal As New Smo.ScriptingOptions
        sNormal.IncludeIfNotExists = True

        Dim sReplace As New Smo.ScriptingOptions
        sReplace.ScriptDrops = True
        sReplace.IncludeIfNotExists = True


        For Each dtRow As DataRow In SQLDatabse.ExecuteWithResults("select name from sys.types WHERE schema_id = 1").Tables(0).Rows
            m_Compare.Add(New DMO_Object("UserDefinedDataType", dtRow("Name").ToString, SQLDatabse.UserDefinedDataTypes.Item(dtRow("Name").ToString).Script(sNormal)))
        Next

        For Each dtRow As DataRow In SQLDatabse.ExecuteWithResults("SELECT name FROM sys.tables").Tables(0).Rows
            m_Compare.Add(New DMO_Object("Table", dtRow("Name").ToString, SQLDatabse.Tables.Item(dtRow("Name").ToString).Script(sTable)))
        Next

        'For Each dtRow As DataRow In SQLDatabse.ExecuteWithResults("select name from sysobjects where xtype='TR' and category=0").Tables(0).Rows
        '    m_Compare.Add(New DMO_Object("Triggers", dtRow("Name").ToString, SQLDatabse.Triggers.Item(dtRow("Name").ToString).Script()))
        'Next

      
        For Each dtRow As DataRow In SQLDatabse.ExecuteWithResults("select name from sysobjects where xtype='P' and category=0").Tables(0).Rows
            m_Compare.Add(New DMO_Object("StoredProcedure", dtRow("Name").ToString, SQLDatabse.StoredProcedures.Item(dtRow("Name").ToString).Script(sReplace)))
        Next

        For Each dtRow As DataRow In SQLDatabse.ExecuteWithResults("select name from sysobjects where xtype='IF' and category=0").Tables(0).Rows
            m_Compare.Add(New DMO_Object("UserDefinedFunction", dtRow("Name").ToString, SQLDatabse.UserDefinedFunctions.Item(dtRow("Name").ToString).Script(sReplace)))
        Next

        For Each dtRow As DataRow In SQLDatabse.ExecuteWithResults("select name, id from sysobjects where xtype='V' and category=0").Tables(0).Rows
            m_Compare.Add(New DMO_Object("View", dtRow("Name").ToString, SQLDatabse.Views.Item(dtRow("Name").ToString).Script(sReplace)))
        Next


        Dim str As String = ""
        For Each DMO As DMO_Object In m_Compare
            str += DMO.Script & vbCrLf
        Next

        System.IO.File.WriteAllText("c:\Text.txt", str.ToString)
    End Sub
End Class
