Public Class DMO_Object
    Private type_renamed As String
    Private name_renamed As String
    Private script_renamed As String

    Public Sub New()
        type_renamed = ""
        name_renamed = ""
        script_renamed = ""
    End Sub

    Public Sub New(ByVal type As String, ByVal name As String, ByVal script As String)
        type_renamed = type
        name_renamed = name
        script_renamed = script
    End Sub

    Public Sub New(ByVal type As String, ByVal name As String, ByVal script As System.Collections.Specialized.StringCollection)
        type_renamed = type
        name_renamed = name
        Me.SetScript(script)
    End Sub

    Public Property Type() As String
        Get
            Return type_renamed
        End Get
        Set(ByVal value As String)
            type_renamed = value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return name_renamed
        End Get
        Set(ByVal value As String)
            name_renamed = value
        End Set
    End Property

    Public Property Script() As String
        Get
            Return script_renamed
        End Get
        Set(ByVal value As String)
            script_renamed = value
        End Set
    End Property

    Public Sub SetScript(ByVal script As System.Collections.Specialized.StringCollection)
        Dim strings(script.Count) As String
        script.CopyTo(strings, 0)
        script_renamed = String.Join(vbCrLf, strings)
    End Sub

    Public Function CompareTo(ByVal c As DMO_Object) As Boolean
        Return Me.Script = c.Script
    End Function

    Public Overloads Function ToString() As String
        Return script_renamed
    End Function
End Class
