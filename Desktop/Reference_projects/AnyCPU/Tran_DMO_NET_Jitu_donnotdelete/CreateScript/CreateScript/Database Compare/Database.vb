Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Data.SqlClient
Imports CreateScript.SQLSMO

Namespace DatabaseCompare.Domain
	''' <summary>
	''' Summary description for Database.
	''' </summary>
    Public Class Database
        Implements IDisposable

        Private connString As String
        Private userTables_Renamed As Hashtable
        Private views_Renamed As Hashtable
        Private storedProcs_Renamed As Hashtable
        Private functions_Renamed As Hashtable
        Private type_Renamed As Hashtable

        Public Sub New(ByVal connString As String)
            Me.connString = connString
            Me.userTables_Renamed = New Hashtable()
            Me.views_Renamed = New Hashtable()
            Me.storedProcs_Renamed = New Hashtable()
            Me.functions_Renamed = New Hashtable()
            Me.type_Renamed = New Hashtable()
        End Sub

        Public Property UserTables() As Hashtable
            Get
                Return userTables_Renamed
            End Get
            Set(ByVal value As Hashtable)
                userTables_Renamed = value
            End Set
        End Property

        Public Property Views() As Hashtable
            Get
                Return views_Renamed
            End Get
            Set(ByVal value As Hashtable)
                views_Renamed = value
            End Set
        End Property

        Public Property StoredProcs() As Hashtable
            Get
                Return storedProcs_Renamed
            End Get
            Set(ByVal value As Hashtable)
                storedProcs_Renamed = value
            End Set
        End Property

        Public Property Functions() As Hashtable
            Get
                Return functions_Renamed
            End Get
            Set(ByVal value As Hashtable)
                functions_Renamed = value
            End Set
        End Property

        Public Property Types() As Hashtable
            Get
                Return type_Renamed
            End Get
            Set(ByVal value As Hashtable)
                type_Renamed = value
            End Set
        End Property

        Public Function TestConnection() As Boolean
            Try
                Using conn As New SqlConnection(connString)
                    conn.Open()
                    Using command As SqlCommand = conn.CreateCommand()
                        command.CommandText = "select count(*) from sysobjects"
                        Dim o As Object = command.ExecuteScalar()
                        If o Is Nothing OrElse o Is DBNull.Value Then
                            Return False
                        End If
                        Dim i As Integer = CInt(Fix(o))
                        If i <= 0 Then
                            Return False
                        End If
                    End Using
                    conn.Close()
                    conn.Dispose()
                End Using
                '            catch ( Exception ex )
            Catch
                Return False
            End Try
            Return True
        End Function

        Public Function SelectDatabase(ByVal strDatabase As String) As Boolean
            Try
                Using conn As New SqlConnection(connString)
                    conn.Open()
                    Using command As SqlCommand = conn.CreateCommand()
                        command.CommandText = "USE [" & strDatabase & "]"
                        Dim o As Object = command.ExecuteScalar()
                        If o Is Nothing OrElse o Is DBNull.Value Then
                            Return False
                        End If
                        Dim i As Integer = CInt(Fix(o))
                        If i <= 0 Then
                            Return False
                        End If
                    End Using
                    conn.Close()
                    conn.Dispose()
                End Using
            Catch ex As Exception
                Return False
            End Try
            Return True
        End Function

        Public Function ExecNonQuery(ByVal sQry As String) As Boolean
            Try
                Using conn As New SqlConnection(connString)
                    conn.Open()
                    Using command As SqlCommand = conn.CreateCommand()
                        command.CommandText = sQry
                        Dim int As Integer = command.ExecuteNonQuery()
                    End Using
                    conn.Close()
                    conn.Dispose()
                End Using
            Catch ex As Exception
                Throw ex
            End Try
            Return True
        End Function

        Private m_SMO As New DMO_Value

        Public Sub ConnectDMO(ByVal dsv As DatabaseServer, ByVal DatabaseName As String)
            m_SMO = New DMO_Value(dsv, DatabaseName)
        End Sub

        Public Sub LoadObjects()
            Try
                Using conn As New SqlConnection(connString)
                    conn.Open()

                    Using command As SqlCommand = conn.CreateCommand()
                        ' get the tables
                        ' get the views
                        ' get the stored procs
                        ' get the functions
                        ' get the Type
                        command.CommandText = "select name, id from sysobjects where xtype='U'"
                        Using reader As SqlDataReader = command.ExecuteReader()
                            Do While reader.Read()
                                Dim t As New UserTable(reader.GetString(0), reader.GetInt32(1))
                                If m_SMO.IsActive Then
                                    t.TextDefinition = m_SMO.Table(t.Name)
                                End If
                                userTables_Renamed(t.Name) = t
                            Loop
                        End Using
                        command.CommandText = "select name, id from sysobjects where xtype='V' and category=0"
                        Using reader As SqlDataReader = command.ExecuteReader()
                            Do While reader.Read()
                                Dim v As New View(reader.GetString(0), reader.GetInt32(1))
                                If m_SMO.IsActive Then
                                    v.TextDefinition = m_SMO.View(v.Name)
                                End If
                                views_Renamed(v.Name) = v
                            Loop
                        End Using
                        command.CommandText = "select name, id from sysobjects where xtype='P' and category=0"
                        Using reader As SqlDataReader = command.ExecuteReader()
                            Do While reader.Read()
                                Dim sp As New StoredProc(reader.GetString(0), reader.GetInt32(1))
                                If m_SMO.IsActive Then
                                    sp.TextDefinition = m_SMO.StoreProcedure(sp.Name)
                                End If
                                storedProcs_Renamed(sp.Name) = sp
                            Loop
                        End Using
                        command.CommandText = "select name, id from sysobjects where xtype='FN' and category=0"
                        Using reader As SqlDataReader = command.ExecuteReader()
                            Do While reader.Read()
                                Dim f As New [Function](reader.GetString(0), reader.GetInt32(1))
                                If m_SMO.IsActive Then
                                    f.TextDefinition = m_SMO.Function(f.Name)
                                End If
                                functions_Renamed(f.Name) = f
                            Loop
                        End Using
                        command.CommandText = "select name, system_type_id , user_type_id from sys.types WHERE schema_id = 1 "
                        Using reader As SqlDataReader = command.ExecuteReader()
                            Do While reader.Read()
                                Dim t As New Type(reader.GetString(0), reader.GetByte(1), reader.GetInt32(2))
                                If m_SMO.IsActive Then
                                    t.TextDefinition = m_SMO.Type(t.Name)
                                End If
                                type_Renamed(t.Name) = t
                            Loop
                        End Using
                    End Using

                    ' gather the data for the tables, views, procs , functions and type
                    'For Each t As UserTable In userTables_Renamed.Values
                    '    t.GatherData(conn)
                    'Next t
                    'For Each v As View In views_Renamed.Values
                    '    v.GatherData(conn)
                    'Next v
                    'For Each sp As StoredProc In storedProcs_Renamed.Values
                    '    sp.GatherData(conn)
                    'Next sp
                    'For Each f As [Function] In functions_Renamed.Values
                    '    f.GatherData(conn)
                    'Next f
                    'For Each t As Type In type_Renamed.Values
                    '    t.GatherData(conn)
                    'Next t

                    conn.Close()
                    conn.Dispose()
                End Using
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function CompareTo(ByVal db2 As Database, Optional ByVal ObjectName As String = "") As ArrayList
            Dim diffs As New ArrayList()
            Select Case ObjectName
                Case ""
                    CompareObjects(Me.UserTables, db2.UserTables, "Table", diffs)
                    CompareObjects(Me.Views, db2.Views, "View", diffs)
                    CompareObjects(Me.StoredProcs, db2.StoredProcs, "StoredProc", diffs)
                    CompareObjects(Me.Functions, db2.Functions, "Function", diffs)
                Case XMLHelper.ConTable
                    CompareObjects(Me.UserTables, db2.UserTables, "Table", diffs)
                Case XMLHelper.ConStoreProcedure
                    CompareObjects(Me.StoredProcs, db2.StoredProcs, "StoredProc", diffs)
                Case XMLHelper.ConType
                    CompareObjects(Me.Types, db2.Types, "Type", diffs)
                Case XMLHelper.ConIndex

                Case XMLHelper.ConTrigger

                Case XMLHelper.ConView
                    CompareObjects(Me.Views, db2.Views, "View", diffs)
            End Select

            Return diffs
        End Function

        Private Sub CompareObjects(ByVal ht1 As Hashtable, ByVal ht2 As Hashtable, ByVal type As String, ByVal diffs As ArrayList)
            For Each t As DatabaseObject In ht1.Values
                If ht2(t.Name) Is Nothing Then
                    Dim d As New DBDifference(type, t.Name, "Missing in Database 2", 2)
                    d.DB1_TextDefinition = t.TextDefinition
                    d.DB2_TextDefinition = ""
                    diffs.Add(d)
                End If
            Next t
            For Each t As DatabaseObject In ht2.Values
                If ht1(t.Name) Is Nothing Then
                    Dim d As New DBDifference(type, t.Name, "Missing in Database 1", 1)
                    d.DB1_TextDefinition = ""
                    d.DB2_TextDefinition = t.TextDefinition
                    diffs.Add(d)
                End If
            Next t
            For Each t As DatabaseObject In ht1.Values
                Dim o As DatabaseObject = TryCast(ht2(t.Name), DatabaseObject)
                If o IsNot Nothing Then
                    Dim d As DBDifference
                    If (Not t.CompareTo(o)) Then
                        d = New DBDifference(type, t.Name, "Different", 3)
                    Else
                        d = New DBDifference(type, t.Name, "Same", 0)
                    End If
                    d.DB1_TextDefinition = t.TextDefinition
                    d.DB2_TextDefinition = o.TextDefinition
                    diffs.Add(d)
                End If
            Next t
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                    userTables_Renamed.Clear()
                    views_Renamed.Clear()
                    storedProcs_Renamed.Clear()
                    functions_Renamed.Clear()
                    type_Renamed.Clear()
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
