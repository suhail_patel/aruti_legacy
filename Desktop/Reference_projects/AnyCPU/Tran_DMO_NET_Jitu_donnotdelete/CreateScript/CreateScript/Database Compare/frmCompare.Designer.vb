<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbLiveServer = New System.Windows.Forms.GroupBox
        Me.btnLiveServerConnect = New System.Windows.Forms.Button
        Me.lblLiveServer = New System.Windows.Forms.Label
        Me.gbTestServer = New System.Windows.Forms.GroupBox
        Me.lblTestServer = New System.Windows.Forms.Label
        Me.btnTestServerConnect = New System.Windows.Forms.Button
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.tvTestServer = New TreeViewScroll
        Me.tvLiveServer = New TreeViewScroll
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.statusBar = New System.Windows.Forms.StatusBar
        Me.btnCompareDatabases = New System.Windows.Forms.Button
        Me.gbLiveServer.SuspendLayout()
        Me.gbTestServer.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbLiveServer
        '
        Me.gbLiveServer.Controls.Add(Me.btnLiveServerConnect)
        Me.gbLiveServer.Controls.Add(Me.lblLiveServer)
        Me.gbLiveServer.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbLiveServer.Location = New System.Drawing.Point(0, 0)
        Me.gbLiveServer.Name = "gbLiveServer"
        Me.gbLiveServer.Size = New System.Drawing.Size(397, 55)
        Me.gbLiveServer.TabIndex = 19
        Me.gbLiveServer.TabStop = False
        Me.gbLiveServer.Text = "Live Server"
        '
        'btnLiveServerConnect
        '
        Me.btnLiveServerConnect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLiveServerConnect.Location = New System.Drawing.Point(367, 19)
        Me.btnLiveServerConnect.Name = "btnLiveServerConnect"
        Me.btnLiveServerConnect.Size = New System.Drawing.Size(23, 22)
        Me.btnLiveServerConnect.TabIndex = 17
        Me.btnLiveServerConnect.Text = "..."
        Me.btnLiveServerConnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLiveServerConnect.UseVisualStyleBackColor = True
        '
        'lblLiveServer
        '
        Me.lblLiveServer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLiveServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLiveServer.Location = New System.Drawing.Point(6, 21)
        Me.lblLiveServer.Name = "lblLiveServer"
        Me.lblLiveServer.Size = New System.Drawing.Size(356, 19)
        Me.lblLiveServer.TabIndex = 21
        Me.lblLiveServer.Text = "Live Server"
        Me.lblLiveServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbTestServer
        '
        Me.gbTestServer.Controls.Add(Me.lblTestServer)
        Me.gbTestServer.Controls.Add(Me.btnTestServerConnect)
        Me.gbTestServer.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbTestServer.Location = New System.Drawing.Point(0, 0)
        Me.gbTestServer.Name = "gbTestServer"
        Me.gbTestServer.Size = New System.Drawing.Size(392, 55)
        Me.gbTestServer.TabIndex = 18
        Me.gbTestServer.TabStop = False
        Me.gbTestServer.Text = "Test Server"
        '
        'lblTestServer
        '
        Me.lblTestServer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTestServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTestServer.Location = New System.Drawing.Point(6, 21)
        Me.lblTestServer.Name = "lblTestServer"
        Me.lblTestServer.Size = New System.Drawing.Size(351, 19)
        Me.lblTestServer.TabIndex = 20
        Me.lblTestServer.Text = "Test Server"
        Me.lblTestServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnTestServerConnect
        '
        Me.btnTestServerConnect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestServerConnect.Location = New System.Drawing.Point(363, 19)
        Me.btnTestServerConnect.Name = "btnTestServerConnect"
        Me.btnTestServerConnect.Size = New System.Drawing.Size(23, 22)
        Me.btnTestServerConnect.TabIndex = 19
        Me.btnTestServerConnect.Text = "..."
        Me.btnTestServerConnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTestServerConnect.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(6, 6)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvTestServer)
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbTestServer)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.tvLiveServer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbLiveServer)
        Me.SplitContainer1.Size = New System.Drawing.Size(793, 386)
        Me.SplitContainer1.SplitterDistance = 392
        Me.SplitContainer1.TabIndex = 21
        '
        'tvTestServer
        '
        Me.tvTestServer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvTestServer.FullRowSelect = True
        Me.tvTestServer.HideSelection = False
        Me.tvTestServer.Location = New System.Drawing.Point(0, 55)
        Me.tvTestServer.Name = "tvTestServer"
        Me.tvTestServer.Size = New System.Drawing.Size(392, 331)
        Me.tvTestServer.TabIndex = 19
        '
        'tvLiveServer
        '
        Me.tvLiveServer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvLiveServer.FullRowSelect = True
        Me.tvLiveServer.HideSelection = False
        Me.tvLiveServer.Location = New System.Drawing.Point(0, 55)
        Me.tvLiveServer.Name = "tvLiveServer"
        Me.tvLiveServer.Size = New System.Drawing.Size(397, 331)
        Me.tvLiveServer.TabIndex = 20
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.statusBar)
        Me.Panel1.Controls.Add(Me.btnCompareDatabases)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(6, 392)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(793, 84)
        Me.Panel1.TabIndex = 22
        '
        'statusBar
        '
        Me.statusBar.Location = New System.Drawing.Point(0, 62)
        Me.statusBar.Name = "statusBar"
        Me.statusBar.Size = New System.Drawing.Size(793, 22)
        Me.statusBar.TabIndex = 16
        '
        'btnCompareDatabases
        '
        Me.btnCompareDatabases.Location = New System.Drawing.Point(8, 7)
        Me.btnCompareDatabases.Name = "btnCompareDatabases"
        Me.btnCompareDatabases.Size = New System.Drawing.Size(73, 49)
        Me.btnCompareDatabases.TabIndex = 15
        Me.btnCompareDatabases.Text = "Compare Changes"
        Me.btnCompareDatabases.UseVisualStyleBackColor = True
        '
        'frmCompare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(805, 482)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Panel1)
        Me.MinimumSize = New System.Drawing.Size(492, 483)
        Me.Name = "frmCompare"
        Me.Padding = New System.Windows.Forms.Padding(6)
        Me.Text = "frmCompare"
        Me.gbLiveServer.ResumeLayout(False)
        Me.gbTestServer.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbLiveServer As System.Windows.Forms.GroupBox
    Friend WithEvents btnLiveServerConnect As System.Windows.Forms.Button
    Friend WithEvents gbTestServer As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tvTestServer As TreeViewScroll
    Friend WithEvents tvLiveServer As TreeViewScroll
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCompareDatabases As System.Windows.Forms.Button
    Private WithEvents statusBar As System.Windows.Forms.StatusBar
    Friend WithEvents lblLiveServer As System.Windows.Forms.Label
    Friend WithEvents lblTestServer As System.Windows.Forms.Label
    Friend WithEvents btnTestServerConnect As System.Windows.Forms.Button
End Class
