Imports Microsoft.VisualBasic
Imports System

Namespace DatabaseCompare.Domain
	Public Class DBDifference
		Implements IComparable
		Private type_Renamed As String
		Private name_Renamed As String
        Private status_Renamed As String
        Private statusId_Renamed As Integer
        Private DB1_textDefinition_Renamed As String
        Private DB2_textDefinition_Renamed As String

        Public Sub New(ByVal type As String, ByVal name As String, ByVal status As String, ByVal statusId As Integer)
            Me.type_Renamed = type
            Me.name_Renamed = name
            Me.status_Renamed = status
            Me.statusId_Renamed = statusId
            Me.DB1_textDefinition_Renamed = ""
            Me.DB2_textDefinition_Renamed = ""
        End Sub

        Public Property Type() As String
            Get
                Return type_Renamed
            End Get
            Set(ByVal value As String)
                type_Renamed = value
            End Set
        End Property

        Public Property Name() As String
            Get
                Return name_Renamed
            End Get
            Set(ByVal value As String)
                name_Renamed = value
            End Set
        End Property

        Public Property Status() As String
            Get
                Return status_Renamed
            End Get
            Set(ByVal value As String)
                status_Renamed = value
            End Set
        End Property

        Public Property StatusId() As Integer
            Get
                Return statusId_Renamed
            End Get
            Set(ByVal value As Integer)
                statusId_Renamed = value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return type_Renamed & ": " & name_Renamed & " " & status_Renamed
        End Function

        Public Property DB1_TextDefinition() As String
            Get
                Return DB1_textDefinition_Renamed
            End Get
            Set(ByVal value As String)
                DB1_textDefinition_Renamed = value
            End Set
        End Property

        Public Property DB2_TextDefinition() As String
            Get
                Return DB2_textDefinition_Renamed
            End Get
            Set(ByVal value As String)
                DB2_textDefinition_Renamed = value
            End Set
        End Property

#Region "IComparable Members"
        Public Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo
            Dim d As DBDifference = TryCast(obj, DBDifference)
            If d IsNot Nothing Then
                If d.Type <> Me.Type Then
                    Return Me.Type.CompareTo(d.Type)
                End If
                Return Me.Name.CompareTo(d.Name)
            End If
            Return 0
        End Function
#End Region
	End Class
End Namespace
