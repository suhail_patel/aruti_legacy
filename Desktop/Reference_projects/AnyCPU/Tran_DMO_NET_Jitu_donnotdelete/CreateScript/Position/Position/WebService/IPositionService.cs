﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.ServiceModel;

namespace PositionTrackerPC.WebService
{
    //[ServiceContract(Namespace="http://bruins/positionservice")]
    public interface IPositionService
    {
        //[OperationContract]
        bool SendPosition(double latitude, double longitude, string remarks);
    }
}
