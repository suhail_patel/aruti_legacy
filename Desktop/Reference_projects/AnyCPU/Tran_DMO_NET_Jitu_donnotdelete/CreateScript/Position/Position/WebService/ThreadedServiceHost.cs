﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace PositionTrackerPC.WebService
{
    /// <summary>
    /// Class to house the thread for the webservice.
    /// Part of this code is borrowed from CodeProject and slighty modified
    /// to make singleton service classes possible
    /// </summary>
    /// <typeparam name="TService">implementing class of the webservice</typeparam>
    /// <typeparam name="TContract">contract interface of the webservice</typeparam>
    class ThreadedServiceHost<TService, TContract>
    {
        const int SleepTime = 100;

        private ServiceHost _serviceHost = null;
        private Thread _thread;
        private string _serviceAddress;
        private string _endpointAddress;
        private bool _running;
        private object _singletonInstance = null;

        /// <summary>
        /// Constructor without singleton
        /// </summary>
        /// <param name="serviceAddress">the address of the service</param>
        /// <param name="endpointAddress">the address of the endpoint</param>
        public ThreadedServiceHost(string serviceAddress, string endpointAddress)
        {
            _serviceAddress = serviceAddress;
            _endpointAddress = endpointAddress;

            // start a new thread to host the service
            _thread = new Thread(new ThreadStart(ThreadMethod));
            _thread.Start();
        }

        /// <summary>
        /// Constructor with singleton
        /// </summary>
        /// <param name="singletonInstance">singleton class instance</param>
        /// <param name="serviceAddress">the address of the service</param>
        /// <param name="endpointAddress">the address of the endpoint</param>
        public ThreadedServiceHost(object singletonInstance, string serviceAddress, string endpointAddress)
        {
            _singletonInstance = singletonInstance;
            _serviceAddress = serviceAddress;
            _endpointAddress = endpointAddress;

            // start a new thread to house the service
            _thread = new Thread(new ThreadStart(ThreadMethod));
            _thread.Start();
        }

        /// <summary>
        /// The method that implements the thread
        /// </summary>
        void ThreadMethod()
        {
            try
            {
                // Indicate we are running
                _running = true;

                // Construct the host, with either the singleton or the type
                if (_singletonInstance != null)
                {
                    _serviceHost = new ServiceHost(_singletonInstance, new Uri(_serviceAddress));
                }
                else
                {
                    _serviceHost = new ServiceHost(typeof(TService), new Uri(_serviceAddress));
                }

                // Add service endpoint, and publish metadata (easy to consume ...)
                _serviceHost.AddServiceEndpoint(typeof(TContract), new BasicHttpBinding(), _endpointAddress);
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;

                _serviceHost.Description.Behaviors.Add(smb);
                _serviceHost.Open();

                while (_running)
                {
                    // Wait until thread is stopped
                    Thread.Sleep(SleepTime);
                }

                // Stop the host
                _serviceHost.Close();
            }
            catch (Exception)
            {
                if (_serviceHost != null)
                {
                    _serviceHost.Close();
                }
            }
        }

        /// <summary>
        /// Request the end of the thread method.
        /// </summary>
        public void Stop()
        {
            lock (this)
            {
                _running = false;
            }
        }
    }
}
