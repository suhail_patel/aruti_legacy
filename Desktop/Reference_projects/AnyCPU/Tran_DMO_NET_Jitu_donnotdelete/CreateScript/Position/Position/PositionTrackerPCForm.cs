﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using PositionTrackerPC.WebService;
using PositionTrackerPC.Helpers;
using PositionTrackerPC.Configuration;

namespace PositionTrackerPC
{
    public partial class PositionTrackerPCForm : Form
    {
        private AppConfiguration _configuration = null;
        public delegate void UpdateForm(object sender, PositionChangedEventArgs e);
        private int _zoomFactor = 16;

        ThreadedServiceHost<PositionService, IPositionService> _positionService = null;

        public PositionTrackerPCForm()
        {

            InitializeComponent();

            _configuration = AppConfiguration.ApplicationConfiguration();
            InitializeWebService();
        }

        /// <summary>
        /// When the form closes, make sure teh service stops
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PositionTrackerPCForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseWebService();
            _configuration.Save();
        }

        // Central screen updater routine
        public void UpdateFormCoordinates(object sender, PositionChangedEventArgs e)
        {
            ShowLocation showLocation = new ShowLocation();
            showLocation.Location = e.ChangedPosition;
            showLocation.Remarks = e.Remarks;
            locationTimeCombo.Items.Add(showLocation);
            locationTimeCombo.SelectedItem = showLocation;

            ShowLocationOnMap(showLocation);
        }

        #region webservice handling
        /// <summary>
        /// Initialize and startup the webservice
        /// </summary>
        private void InitializeWebService()
        {
            // The singleton class that implements our contract
            PositionService theService = new PositionService();
            // Event handler that will be called when the service is called
            theService.PositionChanged += new EventHandler<PositionChangedEventArgs>(theService_PositionChanged);

            // Start the service in its own thread
            string url = String.Format("http://{0}:{1}/services", _configuration.WebServiceHostName, _configuration.PortNumber);
            _positionService = new ThreadedServiceHost<PositionService, IPositionService>(theService, url, "PositionService");
        }

        /// <summary>
        /// Handler for when the position was sent in
        /// </summary>
        /// <param name="sender">who sent it</param>
        /// <param name="e">what did it send</param>
        void theService_PositionChanged(object sender, PositionChangedEventArgs e)
        {
            // Invoke screen updater on the UI thread
            UpdateForm updater = new UpdateForm(UpdateFormCoordinates);
            Invoke(updater, new object[] {this, e});
        }

        /// <summary>
        /// stop the webservice
        /// </summary>
        private void CloseWebService()
        {
            _positionService.Stop();
        }
        #endregion

        /// <summary>
        /// Load form (set default zoomfactor)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PositionTrackerPCForm_Load(object sender, EventArgs e)
        {
            zoomFactorCombo.SelectedItem = _zoomFactor.ToString();
        }

        /// <summary>
        /// change location map to selected one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void locationTimeCombo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ShowLocation location = (ShowLocation)locationTimeCombo.SelectedItem;

            ShowLocationOnMap(location);
        }

        /// <summary>
        /// Show a mlocation map
        /// </summary>
        /// <param name="location">for this location</param>
        private void ShowLocationOnMap(ShowLocation location)
        {
            if (location == null) return;

            // update screen labels
            timeLabel.Text = location.ToString();
            PositionCoordinatesLabel.Text = location.Location.DegreesMinutes;
            remarksLabel.Text = location.Remarks;

            // Build Google Map API url
            MapUrlBuilder builder = new MapUrlBuilder();
            builder.CenterCoordinate = location.Location;
            builder.MapType = "mobile";
            builder.ZoomLevel = _zoomFactor;
            builder.GoogleMapsAPIKey = "";

            // Create location map
            LocationMap map = new LocationMap(builder.MapUrl);
            // show it
            mapPictureBox.Image = map.Map;
        }

        /// <summary>
        /// Zoom factor changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zoomFactorCombo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _zoomFactor = Int32.Parse(zoomFactorCombo.SelectedItem.ToString());

            ShowLocationOnMap((ShowLocation) locationTimeCombo.SelectedItem);
        }

           }
}
