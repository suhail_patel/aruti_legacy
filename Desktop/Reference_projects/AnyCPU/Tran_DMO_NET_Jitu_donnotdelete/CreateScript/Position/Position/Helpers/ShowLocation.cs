﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using PositionTrackerPC.Helpers;

namespace PositionTrackerPC.Helpers
{
    /// <summary>
    /// Helper class to display and remember locations visited in one session
    /// </summary>
    public class ShowLocation
    {
        private Coordinate _location = null;
        private string _remarks = string.Empty;
        private DateTime _timing = DateTime.Now;

        /// <summary>
        /// Location coordinates
        /// </summary>
        public Coordinate Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }

        /// <summary>
        /// Remarks for this location
        /// </summary>
        public string Remarks
        {
            get
            {
                return _remarks;
            }
            set
            {
                _remarks = value;
            }
        }

        /// <summary>
        /// Time this location was visited
        /// </summary>
        public DateTime Time
        {
            get
            {
                return _timing;
            }
            set
            {
                _timing = value;
            }
        }

        // Override to show time in combobox
        public override string ToString()
        {
            return Time.ToShortTimeString();
        }
    }
}
