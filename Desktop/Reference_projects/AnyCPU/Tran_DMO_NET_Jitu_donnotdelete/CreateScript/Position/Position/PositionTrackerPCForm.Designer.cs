﻿namespace PositionTrackerPC
{
    partial class PositionTrackerPCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PositionCoordinatesLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.remarksLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.locationTimeCombo = new System.Windows.Forms.ComboBox();
            this.zoomFactorCombo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.mapPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mapPictureBox
            // 
            this.mapPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mapPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapPictureBox.Location = new System.Drawing.Point(12, 104);
            this.mapPictureBox.Name = "mapPictureBox";
            this.mapPictureBox.Size = new System.Drawing.Size(512, 512);
            this.mapPictureBox.TabIndex = 0;
            this.mapPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Last Known Coordinates";
            // 
            // PositionCoordinatesLabel
            // 
            this.PositionCoordinatesLabel.AutoSize = true;
            this.PositionCoordinatesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PositionCoordinatesLabel.Location = new System.Drawing.Point(140, 9);
            this.PositionCoordinatesLabel.Name = "PositionCoordinatesLabel";
            this.PositionCoordinatesLabel.Size = new System.Drawing.Size(21, 13);
            this.PositionCoordinatesLabel.TabIndex = 2;
            this.PositionCoordinatesLabel.Text = "<>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Remarks";
            // 
            // remarksLabel
            // 
            this.remarksLabel.AutoSize = true;
            this.remarksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarksLabel.Location = new System.Drawing.Point(140, 54);
            this.remarksLabel.Name = "remarksLabel";
            this.remarksLabel.Size = new System.Drawing.Size(21, 13);
            this.remarksLabel.TabIndex = 4;
            this.remarksLabel.Text = "<>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Last known coordinates map";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.Location = new System.Drawing.Point(140, 32);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(21, 13);
            this.timeLabel.TabIndex = 7;
            this.timeLabel.Text = "<>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Time";
            // 
            // locationTimeCombo
            // 
            this.locationTimeCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.locationTimeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.locationTimeCombo.FormattingEnabled = true;
            this.locationTimeCombo.Location = new System.Drawing.Point(12, 622);
            this.locationTimeCombo.Name = "locationTimeCombo";
            this.locationTimeCombo.Size = new System.Drawing.Size(149, 21);
            this.locationTimeCombo.TabIndex = 9;
            this.locationTimeCombo.SelectionChangeCommitted += new System.EventHandler(this.locationTimeCombo_SelectionChangeCommitted);
            this.locationTimeCombo.SelectedIndexChanged += new System.EventHandler(this.locationTimeCombo_SelectedIndexChanged);
            // 
            // zoomFactorCombo
            // 
            this.zoomFactorCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomFactorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.zoomFactorCombo.FormattingEnabled = true;
            this.zoomFactorCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.zoomFactorCombo.Location = new System.Drawing.Point(375, 622);
            this.zoomFactorCombo.Name = "zoomFactorCombo";
            this.zoomFactorCombo.Size = new System.Drawing.Size(149, 21);
            this.zoomFactorCombo.TabIndex = 10;
            this.zoomFactorCombo.SelectionChangeCommitted += new System.EventHandler(this.zoomFactorCombo_SelectionChangeCommitted);
            // 
            // PositionTrackerPCForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 652);
            this.Controls.Add(this.zoomFactorCombo);
            this.Controls.Add(this.locationTimeCombo);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.remarksLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PositionCoordinatesLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mapPictureBox);
            this.Name = "PositionTrackerPCForm";
            this.Text = "Position Tracker PC";
            this.Load += new System.EventHandler(this.PositionTrackerPCForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PositionTrackerPCForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.mapPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox mapPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label PositionCoordinatesLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label remarksLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox locationTimeCombo;
        private System.Windows.Forms.ComboBox zoomFactorCombo;
    }
}

