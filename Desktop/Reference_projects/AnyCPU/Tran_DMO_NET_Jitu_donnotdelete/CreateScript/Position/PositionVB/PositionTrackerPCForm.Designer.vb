Imports Microsoft.VisualBasic
Imports System
Namespace PositionTrackerPC
	Partial Public Class PositionTrackerPCForm
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.mapPictureBox = New System.Windows.Forms.PictureBox
            Me.label1 = New System.Windows.Forms.Label
            Me.PositionCoordinatesLabel = New System.Windows.Forms.Label
            Me.label2 = New System.Windows.Forms.Label
            Me.remarksLabel = New System.Windows.Forms.Label
            Me.label3 = New System.Windows.Forms.Label
            Me.timeLabel = New System.Windows.Forms.Label
            Me.label5 = New System.Windows.Forms.Label
            Me.locationTimeCombo = New System.Windows.Forms.ComboBox
            Me.zoomFactorCombo = New System.Windows.Forms.ComboBox
            Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
            CType(Me.mapPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'mapPictureBox
            '
            Me.mapPictureBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.mapPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.mapPictureBox.Location = New System.Drawing.Point(12, 104)
            Me.mapPictureBox.Name = "mapPictureBox"
            Me.mapPictureBox.Size = New System.Drawing.Size(512, 512)
            Me.mapPictureBox.TabIndex = 0
            Me.mapPictureBox.TabStop = False
            '
            'label1
            '
            Me.label1.AutoSize = True
            Me.label1.Location = New System.Drawing.Point(12, 9)
            Me.label1.Name = "label1"
            Me.label1.Size = New System.Drawing.Size(122, 13)
            Me.label1.TabIndex = 1
            Me.label1.Text = "Last Known Coordinates"
            '
            'PositionCoordinatesLabel
            '
            Me.PositionCoordinatesLabel.AutoSize = True
            Me.PositionCoordinatesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PositionCoordinatesLabel.Location = New System.Drawing.Point(140, 9)
            Me.PositionCoordinatesLabel.Name = "PositionCoordinatesLabel"
            Me.PositionCoordinatesLabel.Size = New System.Drawing.Size(21, 13)
            Me.PositionCoordinatesLabel.TabIndex = 2
            Me.PositionCoordinatesLabel.Text = "<>"
            '
            'label2
            '
            Me.label2.AutoSize = True
            Me.label2.Location = New System.Drawing.Point(12, 54)
            Me.label2.Name = "label2"
            Me.label2.Size = New System.Drawing.Size(49, 13)
            Me.label2.TabIndex = 3
            Me.label2.Text = "Remarks"
            '
            'remarksLabel
            '
            Me.remarksLabel.AutoSize = True
            Me.remarksLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.remarksLabel.Location = New System.Drawing.Point(140, 54)
            Me.remarksLabel.Name = "remarksLabel"
            Me.remarksLabel.Size = New System.Drawing.Size(21, 13)
            Me.remarksLabel.TabIndex = 4
            Me.remarksLabel.Text = "<>"
            '
            'label3
            '
            Me.label3.AutoSize = True
            Me.label3.Location = New System.Drawing.Point(9, 88)
            Me.label3.Name = "label3"
            Me.label3.Size = New System.Drawing.Size(143, 13)
            Me.label3.TabIndex = 5
            Me.label3.Text = "Last known coordinates map"
            '
            'timeLabel
            '
            Me.timeLabel.AutoSize = True
            Me.timeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.timeLabel.Location = New System.Drawing.Point(140, 32)
            Me.timeLabel.Name = "timeLabel"
            Me.timeLabel.Size = New System.Drawing.Size(21, 13)
            Me.timeLabel.TabIndex = 7
            Me.timeLabel.Text = "<>"
            '
            'label5
            '
            Me.label5.AutoSize = True
            Me.label5.Location = New System.Drawing.Point(12, 32)
            Me.label5.Name = "label5"
            Me.label5.Size = New System.Drawing.Size(30, 13)
            Me.label5.TabIndex = 6
            Me.label5.Text = "Time"
            '
            'locationTimeCombo
            '
            Me.locationTimeCombo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.locationTimeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.locationTimeCombo.FormattingEnabled = True
            Me.locationTimeCombo.Items.AddRange(New Object() {"Surat, Gujarat"})
            Me.locationTimeCombo.Location = New System.Drawing.Point(12, 622)
            Me.locationTimeCombo.Name = "locationTimeCombo"
            Me.locationTimeCombo.Size = New System.Drawing.Size(149, 21)
            Me.locationTimeCombo.TabIndex = 9
            '
            'zoomFactorCombo
            '
            Me.zoomFactorCombo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.zoomFactorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.zoomFactorCombo.FormattingEnabled = True
            Me.zoomFactorCombo.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"})
            Me.zoomFactorCombo.Location = New System.Drawing.Point(375, 622)
            Me.zoomFactorCombo.Name = "zoomFactorCombo"
            Me.zoomFactorCombo.Size = New System.Drawing.Size(149, 21)
            Me.zoomFactorCombo.TabIndex = 10
            '
            'WebBrowser1
            '
            Me.WebBrowser1.Location = New System.Drawing.Point(28, 126)
            Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
            Me.WebBrowser1.Name = "WebBrowser1"
            Me.WebBrowser1.Size = New System.Drawing.Size(478, 340)
            Me.WebBrowser1.TabIndex = 11
            '
            'PositionTrackerPCForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(537, 652)
            Me.Controls.Add(Me.WebBrowser1)
            Me.Controls.Add(Me.zoomFactorCombo)
            Me.Controls.Add(Me.locationTimeCombo)
            Me.Controls.Add(Me.timeLabel)
            Me.Controls.Add(Me.label5)
            Me.Controls.Add(Me.label3)
            Me.Controls.Add(Me.remarksLabel)
            Me.Controls.Add(Me.label2)
            Me.Controls.Add(Me.PositionCoordinatesLabel)
            Me.Controls.Add(Me.label1)
            Me.Controls.Add(Me.mapPictureBox)
            Me.Name = "PositionTrackerPCForm"
            Me.Text = "Position Tracker PC"
            CType(Me.mapPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

		#End Region

		Private mapPictureBox As System.Windows.Forms.PictureBox
		Private label1 As System.Windows.Forms.Label
		Private PositionCoordinatesLabel As System.Windows.Forms.Label
		Private label2 As System.Windows.Forms.Label
		Private remarksLabel As System.Windows.Forms.Label
		Private label3 As System.Windows.Forms.Label
		Private timeLabel As System.Windows.Forms.Label
		Private label5 As System.Windows.Forms.Label
		Private WithEvents locationTimeCombo As System.Windows.Forms.ComboBox
        Private WithEvents zoomFactorCombo As System.Windows.Forms.ComboBox
        Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
	End Class
End Namespace

