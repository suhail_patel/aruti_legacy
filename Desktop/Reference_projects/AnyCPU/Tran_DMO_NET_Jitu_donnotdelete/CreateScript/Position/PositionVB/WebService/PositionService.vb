Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
'using System.Linq;
Imports System.Text
Imports System.ServiceModel
Imports PositionTrackerPC.Helpers

Namespace PositionTrackerPC.WebService
	''' <summary>
	''' Implementing class of the webservice
	''' This one is used as a singleton, to make it possible
	''' to communicate with the "outside" code
	''' </summary>
	'[ServiceBehaviorAttribute(InstanceContextMode=InstanceContextMode.Single)]
	Public Class PositionService
		Implements IPositionService
		''' <summary>
		''' Event which is fired when the service was called (from the outside world)
		''' </summary>
		Public Event PositionChanged As EventHandler(Of PositionChangedEventArgs)

		#Region "IPositionService Members"
		''' <summary>
		''' Implementing member of this webservice
		''' </summary>
		''' <param name="latitude">latitude component of position</param>
		''' <param name="longitude">longitude component of position</param>
		''' <param name="remarks">remarks for this position</param>
		''' <returns>whether it succeeded</returns>
		Public Function SendPosition(ByVal latitude As Double, ByVal longitude As Double, ByVal remarks As String) As Boolean Implements IPositionService.SendPosition
			' Fire the event to signify subscribers
			Dim position As New Coordinate(latitude, longitude)
			OnPositionChanged(position, remarks)
			Return True
		End Function
		#End Region

		''' <summary>
		''' Event fire method
		''' </summary>
		''' <param name="Position">position</param>
		''' <param name="remarks">remarks</param>
		Private Sub OnPositionChanged(ByVal Position As Coordinate, ByVal remarks As String)
			' only fire when there are subscribers
			If PositionChangedEvent IsNot Nothing Then
				' construct arguments
				Dim args As New PositionChangedEventArgs(Position, remarks)
				' fire
				RaiseEvent PositionChanged(Me, args)
			End If
		End Sub
	End Class

	''' <summary>
	''' Class holding arguments for the event
	''' </summary>
	Public Class PositionChangedEventArgs
		Inherits EventArgs
		Private _position As Coordinate = Nothing
		Private _remarks As String = String.Empty

		''' <summary>
		''' constructor
		''' </summary>
		''' <param name="position">position</param>
		''' <param name="remarks">remarks</param>
		Public Sub New(ByVal position As Coordinate, ByVal remarks As String)
			_position = position
			_remarks = remarks
		End Sub

		''' <summary>
		'''  Position
		''' </summary>
		Public Property ChangedPosition() As Coordinate
			Get
				Return _position
			End Get
			Set(ByVal value As Coordinate)
				_position = value
			End Set
		End Property
		''' <summary>
		''' Remarks
		''' </summary>
		Public Property Remarks() As String
			Get
				Return _remarks
			End Get
			Set(ByVal value As String)
				_remarks = value
			End Set
		End Property
	End Class
End Namespace
