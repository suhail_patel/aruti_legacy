Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
'using System.Linq;
Imports System.Text

Namespace PositionTrackerPC.Helpers
	''' <summary>
	''' Class to represent coordinates and perform calculations
	''' </summary>
	<Serializable> _
	Public Class Coordinate
		#Region "Private member variables"
		' Coordinate values
		Private _latitude As Double = 0.0
		Private _longitude As Double = 0.0
		#End Region

		#Region "private helper methods"
		''' <summary>
		''' COnvert degrees to radians
		''' </summary>
		''' <param name="value">degree value</param>
		''' <returns>radian value</returns>
		Private Function ToRadians(ByVal value As Double) As Double
			Return (value * Math.PI / 180.0)
		End Function
		''' <summary>
		''' Returns integral part of a double (CF does not support Math.Truncate())
		''' </summary>
		''' <param name="value">value to truncate</param>
		''' <returns>truncated value</returns>
		Private Function Truncate(ByVal value As Double) As Double
			Dim result As Double = value

			If Math.Sign(value) < 0 Then
				result = Math.Ceiling(value)
			Else
				result = Math.Floor(value)
			End If
			Return result
		End Function
		#End Region

		#Region "constructor(s)"
		''' <summary>
		''' Default constructor (no arguments)
		''' </summary>
		Public Sub New()
		End Sub

		''' <summary>
		''' Constructor with simple latitude / longitude arguments
		''' </summary>
		''' <param name="latitude">latitude</param>
		''' <param name="longitude">longitude</param>
		Public Sub New(ByVal latitude As Double, ByVal longitude As Double)
			_latitude = latitude
			_longitude = longitude
		End Sub

		''' <summary>
		''' COnstructor with degrees/minutes components
		''' </summary>
		''' <param name="latitudeDegrees"></param>
		''' <param name="latitudeMinutes"></param>
		''' <param name="longitudeDegrees"></param>
		''' <param name="longitudeMinutes"></param>
		Public Sub New(ByVal latitudeDegrees As Double, ByVal latitudeMinutes As Double, ByVal longitudeDegrees As Double, ByVal longitudeMinutes As Double)
			_latitude = latitudeDegrees + (latitudeMinutes / 60)
			_longitude = longitudeDegrees + (longitudeMinutes / 60)
		End Sub

		''' <summary>
		''' COnstrutor with all of them
		''' </summary>
		''' <param name="latitudeDegrees"></param>
		''' <param name="latitudeMinutes"></param>
		''' <param name="latitudeSeconds"></param>
		''' <param name="longitudeDegrees"></param>
		''' <param name="longitudeMinutes"></param>
		''' <param name="longitudeSeconds"></param>
		Public Sub New(ByVal latitudeDegrees As Double, ByVal latitudeMinutes As Double, ByVal latitudeSeconds As Double, ByVal longitudeDegrees As Double, ByVal longitudeMinutes As Double, ByVal longitudeSeconds As Double)
			_latitude = latitudeDegrees + ((latitudeMinutes + (latitudeSeconds / 60)) / 60)
			_longitude = longitudeDegrees + ((longitudeMinutes + (longitudeSeconds / 60)) / 60)
		End Sub
		#End Region

		#Region "Properties"
		''' <summary>
		''' Gets / sets the latitude component
		''' </summary>
		Public Property Latitude() As Double
			Get
				Return _latitude
			End Get
			Set(ByVal value As Double)
				_latitude = value
			End Set
		End Property

		''' <summary>
		''' Gets / sets the longitude component
		''' </summary>
		Public Property Longitude() As Double
			Get
				Return _longitude
			End Get
			Set(ByVal value As Double)
				_longitude = value
			End Set
		End Property
		#End Region

		#Region "public coordinate string writers"
		''' <summary>
		''' FOrmat according latitude/longitude
		''' </summary>
		Public ReadOnly Property LatitudeLongitude() As String
			Get
				Dim result As String
				If _latitude >= 0.0 Then
					If _longitude >= 0.0 Then
						result = String.Format("{1} {0:00.000000} {3} {2:000.000000}", _latitude,"N", _longitude,"E")
					Else
						result = String.Format("{1} {0:00.000000} {3} {2:000.000000}", _latitude,"N", _longitude,"W")
					End If
				Else
					If _longitude >= 0.0 Then
						result = String.Format("{1} {0:00.000000} {3} {2:000.000000}", _latitude,"S", _longitude,"E")
					Else
						result = String.Format("{1} {0:00.000000} {3} {2:000.000000}", _latitude,"S", _longitude,"W")
					End If
				End If
				Return result
			End Get
		End Property

		''' <summary>
		''' FOrmat according to DM format
		''' </summary>
		Public ReadOnly Property DegreesMinutes() As String
			Get
				Dim result As String = String.Empty
				Dim latitudeDegrees As Double = 0.0, latitudeMinutes As Double = 0.0
				Dim longitudeDegrees As Double = 0.0, longitudeMinutes As Double = 0.0

				latitudeDegrees = Truncate(_latitude)
				latitudeMinutes = (_latitude - latitudeDegrees) * 60

				longitudeDegrees = Truncate(_longitude)
				longitudeMinutes = (_longitude - longitudeDegrees) * 60

				If _latitude >= 0.0 Then
					If _longitude >= 0.0 Then
						result = String.Format("{2} {0:00} {1:00.000}' {5} {3:000} {4:00.000}'", latitudeDegrees, latitudeMinutes,"N", longitudeDegrees, longitudeMinutes,"E")
					Else
						result = String.Format("{2} {0:00} {1:00.000}' {5} {3:000} {4:00.000}'", latitudeDegrees, latitudeMinutes,"N", longitudeDegrees, longitudeMinutes,"W")
					End If
				Else
					If _longitude >= 0.0 Then
						result = String.Format("{2} {0:00} {1:00.000}' {5} {3:000} {4:00.000}'", latitudeDegrees, latitudeMinutes,"S", longitudeDegrees, longitudeMinutes,"E")
					Else
						result = String.Format("{2} {0:00} {1:00.000}' {5} {3:000} {4:00.000}'", latitudeDegrees, latitudeMinutes,"S", longitudeDegrees, longitudeMinutes,"W")
					End If
				End If
				Return result
			End Get
		End Property

		''' <summary>
		''' FOrmat according to DMS format
		''' </summary>
		Public ReadOnly Property DegreesMinutesSeconds() As String
			Get
				Dim result As String = String.Empty
				Dim latitudeDegrees As Double = 0.0, latitudeMinutes As Double = 0.0, latitudeSeconds As Double = 0.0
				Dim longitudeDegrees As Double = 0.0, longitudeMinutes As Double = 0.0, longitudeSeconds As Double = 0.0

				latitudeDegrees = Truncate(_latitude)
				latitudeMinutes = Truncate((_latitude - latitudeDegrees) * 60)
				latitudeSeconds = (((_latitude - latitudeDegrees) * 60) - latitudeMinutes) * 60

				longitudeDegrees = Truncate(_longitude)
				longitudeMinutes = Truncate((_longitude - longitudeDegrees) * 60)
				longitudeSeconds = (((_longitude - longitudeDegrees) * 60) - longitudeMinutes) * 60

				If _latitude >= 0.0 Then
					If _longitude >= 0.0 Then
						result = String.Format("{3} {0:00} {1:00}' {2:00.0000}"" {7} {4:000} {5:00}' {6:00.0000}""", latitudeDegrees, latitudeMinutes, latitudeSeconds,"N", longitudeDegrees, longitudeMinutes, longitudeSeconds,"E")
					Else
						result = String.Format("{3} {0:00} {1:00}' {2:00.0000}"" {7} {4:000} {5:00}' {6:00.0000}""", latitudeDegrees, latitudeMinutes, latitudeSeconds,"N", longitudeDegrees, longitudeMinutes, longitudeSeconds,"W")
					End If
				Else
					If _longitude >= 0.0 Then
						result = String.Format("{3} {0:00} {1:00}' {2:00.0000}"" {7} {4:000} {5:00}' {6:00.0000}""", latitudeDegrees, latitudeMinutes, latitudeSeconds,"S", longitudeDegrees, longitudeMinutes, longitudeSeconds,"E")
					Else
						result = String.Format("{3} {0:00} {1:00}' {2:00.0000}"" {7} {4:000} {5:00}' {6:00.0000}""", latitudeDegrees, latitudeMinutes, latitudeSeconds,"S", longitudeDegrees, longitudeMinutes, longitudeSeconds,"W")
					End If
				End If
				Return result
			End Get
		End Property
		#End Region

		#Region "public methods"
		''' <summary>
		''' Calculates the distance between this coordinate and the argument
		''' uing law of cosines
		''' </summary>
		''' <param name="coordinate">Point to calculate distance to</param>
		''' <returns>The distance</returns>
		Public Function Distance(ByVal coordinate As Coordinate) As Double
			Dim R As Double = 6371.0 ' earth's radius in km
			Dim lat1 As Double = ToRadians(_latitude)
			Dim lat2 As Double = ToRadians(coordinate.Latitude)
			Dim deltaLongitude As Double = ToRadians(coordinate.Longitude - _longitude)

'INSTANT VB NOTE: The local variable distance was renamed since Visual Basic will not allow local variables with the same name as their enclosing function or property:
			Dim distance_Renamed As Double = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(deltaLongitude)) * R

			Return distance_Renamed * 1000.0 ' distance in meters
		End Function
		#End Region
	End Class
End Namespace
