Imports Microsoft.VisualBasic
Imports System.Drawing
Imports System.Net
Imports System

Namespace PositionTrackerPC.Helpers
	<Serializable> _
	Public Class LocationMap
		Private _map As New Bitmap(512, 512)

		''' <summary>
		''' Default constructor
		''' </summary>
		Public Sub New()
		End Sub

		''' <summary>
		''' Csontructor taking a GoogleMaps API webrequest URI
		''' </summary>
		''' <param name="url">url to use as request</param>
		Public Sub New(ByVal url As String)
			_map = FromUrl(url)
		End Sub

		''' <summary>
		''' Resolves the url and returns the map image
		''' </summary>
		''' <param name="url">orl to resolve</param>
		''' <returns>the resulting bitmap</returns>
		Private Function FromUrl(ByVal url As String) As Bitmap
			Dim request As WebRequest = HttpWebRequest.Create(url)
			Dim response As WebResponse = request.GetResponse()

			Return CType(Bitmap.FromStream(response.GetResponseStream()), Bitmap)
		End Function

		''' <summary>
		''' The bitmap
		''' </summary>
		Public Property Map() As Bitmap
			Get
				Return _map
			End Get
			Set(ByVal value As Bitmap)
				_map = value
			End Set
		End Property
	End Class
End Namespace
