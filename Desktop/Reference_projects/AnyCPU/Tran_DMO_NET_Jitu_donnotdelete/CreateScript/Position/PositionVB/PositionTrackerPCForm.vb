Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.ServiceModel
Imports PositionTrackerPC.WebService
Imports PositionTrackerPC.Helpers
Imports PositionTrackerPC.Configuration

Namespace PositionTrackerPC
	Partial Public Class PositionTrackerPCForm
		Inherits Form
		Private _configuration As AppConfiguration = Nothing
		Public Delegate Sub UpdateForm(ByVal sender As Object, ByVal e As PositionChangedEventArgs)
		Private _zoomFactor As Integer = 16

        'Private _positionService As ThreadedServiceHost(Of PositionService, IPositionService) = Nothing

		Public Sub New()

			InitializeComponent()

			_configuration = AppConfiguration.ApplicationConfiguration()
			InitializeWebService()
		End Sub

		''' <summary>
		''' When the form closes, make sure teh service stops
		''' </summary>
		''' <param name="sender"></param>
		''' <param name="e"></param>
		Private Sub PositionTrackerPCForm_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles MyBase.FormClosing
			CloseWebService()
			_configuration.Save()
		End Sub

		' Central screen updater routine
		Public Sub UpdateFormCoordinates(ByVal sender As Object, ByVal e As PositionChangedEventArgs)
			Dim showLocation As New ShowLocation()
			showLocation.Location = e.ChangedPosition
			showLocation.Remarks = e.Remarks
			locationTimeCombo.Items.Add(showLocation)
			locationTimeCombo.SelectedItem = showLocation

			ShowLocationOnMap(showLocation)
		End Sub

		#Region "webservice handling"
		''' <summary>
		''' Initialize and startup the webservice
		''' </summary>
		Private Sub InitializeWebService()
			' The singleton class that implements our contract
			Dim theService As New PositionService()
			' Event handler that will be called when the service is called
			AddHandler theService.PositionChanged, AddressOf theService_PositionChanged

			' Start the service in its own thread
            Dim url As String = String.Format("http://{0}:{1}/services", _configuration.WebServiceHostName, _configuration.PortNumber)
            'WebBrowser1.Url = New System.Uri(url)
            '_positionService = New ThreadedServiceHost(Of PositionService, IPositionService)(theService, url, "PositionService")
		End Sub

		''' <summary>
		''' Handler for when the position was sent in
		''' </summary>
		''' <param name="sender">who sent it</param>
		''' <param name="e">what did it send</param>
		Private Sub theService_PositionChanged(ByVal sender As Object, ByVal e As PositionChangedEventArgs)
			' Invoke screen updater on the UI thread
			Dim updater As New UpdateForm(AddressOf UpdateFormCoordinates)
			Invoke(updater, New Object() {Me, e})
		End Sub

		''' <summary>
		''' stop the webservice
		''' </summary>
		Private Sub CloseWebService()
            '_positionService.Stop()
		End Sub
		#End Region

		''' <summary>
		''' Load form (set default zoomfactor)
		''' </summary>
		''' <param name="sender"></param>
		''' <param name="e"></param>
        Private Sub PositionTrackerPCForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            Dim showLocation As New ShowLocation()
            showLocation.Location = New PositionTrackerPC.Helpers.Coordinate(10, 100)
            showLocation.Remarks = "Surat, Gujarat"
            locationTimeCombo.Items.Add(showLocation)

            zoomFactorCombo.SelectedItem = _zoomFactor.ToString()
        End Sub

		''' <summary>
		''' change location map to selected one
		''' </summary>
		''' <param name="sender"></param>
		''' <param name="e"></param>
		Private Sub locationTimeCombo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As EventArgs) Handles locationTimeCombo.SelectionChangeCommitted
			Dim location As ShowLocation = CType(locationTimeCombo.SelectedItem, ShowLocation)

			ShowLocationOnMap(location)
		End Sub

		''' <summary>
		''' Show a mlocation map
		''' </summary>
		''' <param name="location">for this location</param>
		Private Sub ShowLocationOnMap(ByVal location As ShowLocation)
			If location Is Nothing Then
				Return
			End If

			' update screen labels
			timeLabel.Text = location.ToString()
			PositionCoordinatesLabel.Text = location.Location.DegreesMinutes
			remarksLabel.Text = location.Remarks

			' Build Google Map API url
			Dim builder As New MapUrlBuilder()
            builder.CenterCoordinate = location.Location
            builder.MapType = "mobile"
			builder.ZoomLevel = _zoomFactor
			builder.GoogleMapsAPIKey = ""

            WebBrowser1.Url = New System.Uri(builder.MapUrl)
			' Create location map
            'Dim map As New LocationMap(builder.MapUrl)
            '' show it
            'mapPictureBox.Image = map.Map
		End Sub

		''' <summary>
		''' Zoom factor changed
		''' </summary>
		''' <param name="sender"></param>
		''' <param name="e"></param>
		Private Sub zoomFactorCombo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As EventArgs) Handles zoomFactorCombo.SelectionChangeCommitted
			_zoomFactor = Int32.Parse(zoomFactorCombo.SelectedItem.ToString())

			ShowLocationOnMap(CType(locationTimeCombo.SelectedItem, ShowLocation))
		End Sub

	End Class
End Namespace
