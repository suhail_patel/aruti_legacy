Imports System
Imports System.ComponentModel

<TypeConverter(GetType(ExpandableObjectConverter))> _
    Public NotInheritable Class CornerSettings
    ' Methods
    Private Sub FireSettingChanged()
        If (Not Me.SettingChanged Is Nothing) Then
            Me.SettingChanged.Invoke(Me, New EventArgs)
        End If
    End Sub


    ' Properties
    <DefaultValue(True)> _
    Public Property BottomLeft() As Boolean
        Get
            Return Me._bottomLeft
        End Get
        Set(ByVal value As Boolean)
            Me._bottomLeft = value
            Me.FireSettingChanged()
        End Set
    End Property

    <DefaultValue(True)> _
    Public Property BottomRight() As Boolean
        Get
            Return Me._bottomRadius
        End Get
        Set(ByVal value As Boolean)
            Me._bottomRadius = value
            Me.FireSettingChanged()
        End Set
    End Property

    <DefaultValue(&H19)> _
    Public Property Radius() As Integer
        Get
            Return Me._radius
        End Get
        Set(ByVal value As Integer)
            Me._radius = value
            Me.FireSettingChanged()
        End Set
    End Property

    <DefaultValue(True)> _
    Public Property TopLeft() As Boolean
        Get
            Return Me._topLeft
        End Get
        Set(ByVal value As Boolean)
            Me._topLeft = value
            Me.FireSettingChanged()
        End Set
    End Property

    <DefaultValue(True)> _
    Public Property TopRight() As Boolean
        Get
            Return Me._topRadius
        End Get
        Set(ByVal value As Boolean)
            Me._topRadius = value
            Me.FireSettingChanged()
        End Set
    End Property


    ' Fields
    Private _bottomLeft As Boolean = True
    Private _bottomRadius As Boolean = True
    Private _radius As Integer = &H19
    Public SettingChanged As EventHandler
    Private _topLeft As Boolean = True
    Private _topRadius As Boolean = True
End Class
