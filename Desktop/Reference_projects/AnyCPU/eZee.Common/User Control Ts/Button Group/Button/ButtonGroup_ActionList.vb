Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports System.Drawing
Imports System.Windows.Forms

<System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")> _
 Public Class ButtonGroup_Designer
    Inherits System.Windows.Forms.Design.ControlDesigner

    Private actionLists_Renamed As DesignerActionListCollection

    ' Use pull model to populate smart tag menu.
    Public Overrides ReadOnly Property ActionLists() As DesignerActionListCollection
        Get
            If Nothing Is actionLists_Renamed Then
                actionLists_Renamed = New DesignerActionListCollection()
                actionLists_Renamed.Add(New ButtonGroup_ActionList(Me.Component))
            End If
            Return actionLists_Renamed
        End Get
    End Property
End Class

Public Class ButtonGroup_ActionList
    Inherits System.ComponentModel.Design.DesignerActionList

    Private m_Control As ButtonGroup

    Private designerActionUISvc As DesignerActionUIService = Nothing

    'The constructor associates the control with the smart tag list.
    Public Sub New(ByVal component As IComponent)
        MyBase.New(component)
        Me.m_Control = TryCast(component, ButtonGroup)

        ' Cache a reference to DesignerActionUIService, so the DesigneractionList can be refreshed.
        Me.designerActionUISvc = TryCast(GetService(GetType(DesignerActionUIService)), DesignerActionUIService)
    End Sub

#Region " Helper Methods "
    'Public Sub RefreshDesigner()
    '    If designerActionUISvc IsNot Nothing Then designerActionUISvc.Refresh(Me.Component)
    'End Sub


    ' Helper method to retrieve control properties. Use of GetProperties enables undo and menu updates to work properly.
    Private Function GetPropertyByName(ByVal propName As String) As PropertyDescriptor
        Dim prop As PropertyDescriptor
        prop = TypeDescriptor.GetProperties(m_Control)(propName)
        If Nothing Is prop Then
            Throw New ArgumentException("Matching ColorLabel property not found!", propName)
        Else
            Return prop
        End If
    End Function

    ''' <summary>
    ''' Sets the specified property of specified control to the given value. 
    ''' Note: by means of Reflection methods, "Undo" and "Menu Updates" in IDE work properly! (donot modify control properties directly through the code, but use this method)
    ''' </summary>
    Public Sub SetPropertyByName(ByVal ComponentObject As Object, ByVal propName As String, ByVal value As Object)
        If ComponentObject Is Nothing Then Exit Sub
        Dim prop As PropertyDescriptor = TypeDescriptor.GetProperties(ComponentObject).Item(propName)
        If prop IsNot Nothing Then
            Try
                prop.SetValue(ComponentObject, value)
            Catch ex As Exception
                Dim sValue As String
                If value IsNot Nothing Then sValue = value.ToString() Else sValue = ""
                MessageBox.Show("Smart_ActionList: Cannot set " & prop.Name & " property to the specified value: " & sValue, "Error")
            End Try
        End If
    End Sub

    ''' <summary>Adds a static Text item to ActionItems. Call it in GetSortedActionItems() function.</summary>
    Public Sub AddActionText(ByRef m_ActionList As DesignerActionItemCollection, ByVal displayName As String, ByVal category As String)
        m_ActionList.Add(New DesignerActionTextItem(displayName, category))
    End Sub

    ''' <summary>Adds a Property item to ActionItems. Call it in GetSortedActionItems() function.</summary>
    ''' <param name="memberName">The property name defined in your DesignerActionList (Not your control property!)</param>
    Public Sub AddActionProperty(ByRef m_ActionList As DesignerActionItemCollection, ByVal memberName As String, ByVal displayName As String, ByVal category As String, ByVal description As String)
        m_ActionList.Add(New DesignerActionPropertyItem(memberName, displayName, category, description))
    End Sub

    ''' <summary>Adds a Method item to ActionItems. Call it in GetSortedActionItems() function.</summary>
    ''' <param name="memberName">The property name defined in your DesignerActionList (Not your control property!)</param>
    Public Sub AddActionMethod(ByRef m_ActionList As DesignerActionItemCollection, ByVal memberName As String, ByVal displayName As String, ByVal category As String, ByVal description As String, ByVal includeAsDesignerVerb As Boolean)
        m_ActionList.Add(New DesignerActionMethodItem(Me, memberName, displayName, category, description, includeAsDesignerVerb))
    End Sub

    ''' <summary>Adds a Header (Category) item to ActionItems. Call it in GetSortedActionItems() function.</summary>
    Public Sub AddActionHeader(ByRef m_ActionList As DesignerActionItemCollection, ByVal displayName As String)
        m_ActionList.Add(New DesignerActionHeaderItem(displayName))
    End Sub
#End Region


    Public Overrides Function GetSortedActionItems() As DesignerActionItemCollection
        Dim m_ActionList As New DesignerActionItemCollection()

        Me.AddActionHeader(m_ActionList, "Button Group")

        Me.AddActionProperty(m_ActionList, "ButtonStyle", "ButtonStyle:", "Button Group", "Style of Buttons")
        Me.AddActionProperty(m_ActionList, "Buttons", "Buttons:", "Button Group", "List of Buttons")
        Me.AddActionMethod(m_ActionList, "SwapColors", "Swap Colors", "Button Group", "Swap ForeColor and BackColor", True)
        Me.AddActionMethod(m_ActionList, "AutoArrange", "Auto Arrange", "Button Group", "Arrange all buttons", True)

        Me.AddActionHeader(m_ActionList, "Button Color")
        Me.AddActionProperty(m_ActionList, "ForeColor", "ForeColor:", "Button Color", "Sets the ForeColor")
        Me.AddActionProperty(m_ActionList, "BackColor", "BackColor:", "Button Color", "Sets the BackColor")

        Me.AddActionHeader(m_ActionList, "Selected Color")
        Me.AddActionProperty(m_ActionList, "SelectedBackColor", "SelectedBackColor:", "Selected Color", "Sets the SelectedBackColor")
        Me.AddActionProperty(m_ActionList, "SelectedForeColor", "SelectedForeColor:", "Selected Color", "Sets the SelectedForeColor")

        Me.AddActionHeader(m_ActionList, "Border")
        Me.AddActionProperty(m_ActionList, "BorderColor", "BorderColor:", "Border", "Sets the BorderColor")
        Me.AddActionProperty(m_ActionList, "BorderWidth", "BorderWidth:", "Border", "Sets the BorderWidth")

        Me.AddActionHeader(m_ActionList, "Gradient Style")
        Me.AddActionProperty(m_ActionList, "StartColor", "StartColor:", "Gradient Style", "Sets the GradientStartColor")
        Me.AddActionProperty(m_ActionList, "EndColor", "EndColor:", "Gradient Style", "Sets the GradientEndColor")
        Me.AddActionProperty(m_ActionList, "FillDirection", "FillDirection:", "Gradient Style", "Sets the GradientFillDirection")

        Me.AddActionHeader(m_ActionList, "Size")
        Me.AddActionProperty(m_ActionList, "Width", "Width:", "Size", "Sets the Width")
        Me.AddActionProperty(m_ActionList, "Height", "Height:", "Size", "Sets the Height")

        Me.AddActionHeader(m_ActionList, "Text Format")
        Me.AddActionProperty(m_ActionList, "Font", "Font:", "Text Format", "")
        Me.AddActionProperty(m_ActionList, "TextAlignment", "TextAlignment:", "Text Format", "")

        'Define static section header entries.
        'm_ActionList.Add(New DesignerActionHeaderItem("Appearance"))

        'm_ActionList.Add(New DesignerActionPropertyItem("BackColor", "Back Color", "Appearance", "Selects the background color."))
        'm_ActionList.Add(New DesignerActionPropertyItem("ForeColor", "Fore Color", "Appearance", "Selects the foreground color."))
        'm_ActionList.Add(New DesignerActionPropertyItem("LabelText", "Label Text", "Appearance", "Type few characters to filter Cities."))

        Me.AddActionMethod(m_ActionList, "AddKeyboard", "Add Keyboard", "Add Keyboard", "Add Keyboard", True)
        Me.AddActionMethod(m_ActionList, "AddAZKeyboard", "Add AZ Keyboard", "Add Keyboard", "Add AZ Keyboard", True)
        Me.AddActionMethod(m_ActionList, "FitToControl", "Fit To Control", "Add Keyboard", "Fit To Control", True)

        Return m_ActionList
    End Function



#Region " Button Group Property "
    Public Property ButtonStyle() As BG_ButtonStyleType
        Get
            Return m_Control.ButtonStyle
        End Get
        Set(ByVal value As BG_ButtonStyleType)
            If m_Control.ButtonStyle <> value Then
                m_Control.ButtonStyle = value
                m_Control.Invalidate()
            End If
        End Set
    End Property

    Public Property Buttons() As BG_ButtonCollection
        Get
            Return m_Control.Buttons
        End Get
        Set(ByVal value As BG_ButtonCollection)
            m_Control.Buttons = value
        End Set
    End Property
#End Region

    Private Class SampleKeys
        Public _X As Integer
        Public _Y As Integer
        Public _Width As Integer
        Public _Height As Integer
        Public _Normal_Display As String
        Public _ShiftPressed_Display As String
        Public _Value As String = ""

        Public Sub New(ByVal X As Integer, _
            ByVal Y As Integer, _
            ByVal Width As Integer, _
            ByVal Height As Integer, _
            ByVal Normal_Display As String, _
            ByVal ShiftPressed_Display As String)
            _X = X
            _Y = Y
            _Width = Width
            _Height = Height
            _Normal_Display = Normal_Display
            _ShiftPressed_Display = ShiftPressed_Display
        End Sub

        Public Sub New(ByVal X As Integer, _
            ByVal Y As Integer, _
            ByVal Width As Integer, _
            ByVal Height As Integer, _
            ByVal Normal_Display As String, _
            ByVal ShiftPressed_Display As String, _
            ByVal Value As String)

            _X = X
            _Y = Y
            _Width = Width
            _Height = Height
            _Normal_Display = Normal_Display
            _ShiftPressed_Display = ShiftPressed_Display
            _Value = Value
        End Sub
    End Class

    Private Sub AddAZKeyboard()
        Dim obj As New List(Of SampleKeys)
        obj.Add(New SampleKeys(0, 0, 30, 30, "A", "A"))
        obj.Add(New SampleKeys(30, 0, 30, 30, "B", "B"))
        obj.Add(New SampleKeys(60, 0, 30, 30, "C", "C"))
        obj.Add(New SampleKeys(90, 0, 30, 30, "D", "D"))
        obj.Add(New SampleKeys(120, 0, 30, 30, "E", "E"))
        obj.Add(New SampleKeys(150, 0, 30, 30, "F", "F"))
        obj.Add(New SampleKeys(180, 0, 30, 30, "G", "G"))
        obj.Add(New SampleKeys(210, 0, 30, 30, "H", "H"))
        obj.Add(New SampleKeys(240, 0, 30, 30, "I", "I"))
        obj.Add(New SampleKeys(270, 0, 30, 30, "J", "J"))
        obj.Add(New SampleKeys(300, 0, 30, 30, "K", "K"))
        obj.Add(New SampleKeys(330, 0, 30, 30, "L", "L"))
        obj.Add(New SampleKeys(360, 0, 30, 30, "M", "M"))

        obj.Add(New SampleKeys(0, 30, 30, 30, "N", "N"))
        obj.Add(New SampleKeys(30, 30, 30, 30, "O", "O"))
        obj.Add(New SampleKeys(60, 30, 30, 30, "P", "P"))
        obj.Add(New SampleKeys(90, 30, 30, 30, "Q", "Q"))
        obj.Add(New SampleKeys(120, 30, 30, 30, "R", "R"))
        obj.Add(New SampleKeys(150, 30, 30, 30, "S", "S"))
        obj.Add(New SampleKeys(180, 30, 30, 30, "T", "T"))
        obj.Add(New SampleKeys(210, 30, 30, 30, "U", "U"))
        obj.Add(New SampleKeys(240, 30, 30, 30, "V", "V"))
        obj.Add(New SampleKeys(270, 30, 30, 30, "W", "W"))
        obj.Add(New SampleKeys(300, 30, 30, 30, "X", "X"))
        obj.Add(New SampleKeys(330, 30, 30, 30, "Y", "Y"))
        obj.Add(New SampleKeys(360, 30, 30, 30, "Z", "Z"))

        obj.Add(New SampleKeys(0, 60, 30, 30, "1", "1"))
        obj.Add(New SampleKeys(30, 60, 30, 30, "2", "2"))
        obj.Add(New SampleKeys(60, 60, 30, 30, "3", "3"))
        obj.Add(New SampleKeys(90, 60, 30, 30, "4", "4"))
        obj.Add(New SampleKeys(120, 60, 30, 30, "5", "5"))
        obj.Add(New SampleKeys(150, 60, 30, 30, "6", "6"))
        obj.Add(New SampleKeys(180, 60, 30, 30, "7", "7"))
        obj.Add(New SampleKeys(210, 60, 30, 30, "8", "8"))
        obj.Add(New SampleKeys(240, 60, 30, 30, "9", "9"))
        obj.Add(New SampleKeys(270, 60, 30, 30, "0", "0"))
        obj.Add(New SampleKeys(300, 60, 30, 30, "C", "C", "{Clear}"))
        obj.Add(New SampleKeys(330, 60, 30, 30, "<", "<", "{BACKSPACE}"))
        obj.Add(New SampleKeys(360, 60, 30, 30, "_", "_", " "))

        Dim h As IDesignerHost = DirectCast(GetService(GetType(IDesignerHost)), IDesignerHost)
        Dim dt As DesignerTransaction
        Dim c As IComponentChangeService = DirectCast(GetService(GetType(IComponentChangeService)), IComponentChangeService)

        'Add a new button to the collection
        dt = h.CreateTransaction("Add Button")

        c.OnComponentChanging(m_Control, Nothing)
        For iCnt As Integer = m_Control.Buttons.Count - 1 To 0 Step -1
            h.DestroyComponent(m_Control.Buttons.Item(iCnt))
            m_Control.Buttons.Remove(iCnt)
        Next

        For Each k As SampleKeys In obj
            Dim objbtn As BG_Button = DirectCast(h.CreateComponent(GetType(BG_Button)), BG_Button)
            objbtn.Bounds = New System.Drawing.Rectangle(k._X, k._Y, k._Width, k._Height)
            objbtn.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            objbtn.Normal_Display = k._Normal_Display
            objbtn.SelectedBackColor = System.Drawing.Color.Teal
            objbtn.ShiftPressed_Display = k._ShiftPressed_Display
            objbtn.Value = k._Value
            m_Control.Buttons.Add(objbtn)
        Next

        c.OnComponentChanged(m_Control, Nothing, Nothing, Nothing)
        dt.Commit()

    End Sub

    Private Sub AddKeyboard()
        Dim obj As New List(Of SampleKeys)
        obj.Add(New SampleKeys(0, 0, 40, 40, "`", "~"))
        obj.Add(New SampleKeys(40, 0, 40, 40, "1", "!"))
        obj.Add(New SampleKeys(80, 0, 40, 40, "2", "@"))
        obj.Add(New SampleKeys(120, 0, 40, 40, "3", "#"))
        obj.Add(New SampleKeys(160, 0, 40, 40, "4", "$"))
        obj.Add(New SampleKeys(200, 0, 40, 40, "5", "%"))
        obj.Add(New SampleKeys(240, 0, 40, 40, "6", "^"))
        obj.Add(New SampleKeys(280, 0, 40, 40, "7", "&"))
        obj.Add(New SampleKeys(320, 0, 40, 40, "8", "*"))
        obj.Add(New SampleKeys(360, 0, 40, 40, "9", "("))
        obj.Add(New SampleKeys(400, 0, 40, 40, "0", ")"))
        obj.Add(New SampleKeys(440, 0, 40, 40, "-", "_"))
        obj.Add(New SampleKeys(480, 0, 40, 40, "=", "+"))
        obj.Add(New SampleKeys(520, 0, 70, 40, "BK", "BK","{BACKSPACE}"))
        obj.Add(New SampleKeys(590, 0, 40, 40, "HOME", "HOME", "{HOME}"))

        obj.Add(New SampleKeys(0, 40, 60, 40, "TAB", "TAB", "^{TAB}"))
        obj.Add(New SampleKeys(60, 40, 40, 40, "q", "Q"))
        obj.Add(New SampleKeys(100, 40, 40, 40, "w", "W"))
        obj.Add(New SampleKeys(140, 40, 40, 40, "e", "E"))
        obj.Add(New SampleKeys(180, 40, 40, 40, "r", "R"))
        obj.Add(New SampleKeys(220, 40, 40, 40, "t", "T"))
        obj.Add(New SampleKeys(260, 40, 40, 40, "y", "Y"))
        obj.Add(New SampleKeys(300, 40, 40, 40, "u", "U"))
        obj.Add(New SampleKeys(340, 40, 40, 40, "i", "I"))
        obj.Add(New SampleKeys(380, 40, 40, 40, "o", "O"))
        obj.Add(New SampleKeys(420, 40, 40, 40, "p", "P"))
        obj.Add(New SampleKeys(460, 40, 40, 40, "[", "{"))
        obj.Add(New SampleKeys(500, 40, 40, 40, "]", "}"))
        obj.Add(New SampleKeys(540, 40, 50, 40, " ", " "))
        obj.Add(New SampleKeys(590, 40, 40, 40, "END", "END", "{END}"))

        obj.Add(New SampleKeys(0, 80, 70, 40, "Cap", "Cap","{CAPSLOCK}"))
        obj.Add(New SampleKeys(70, 80, 40, 40, "a", "A"))
        obj.Add(New SampleKeys(110, 80, 40, 40, "s", "S"))
        obj.Add(New SampleKeys(150, 80, 40, 40, "d", "D"))
        obj.Add(New SampleKeys(190, 80, 40, 40, "f", "F"))
        obj.Add(New SampleKeys(230, 80, 40, 40, "g", "G"))
        obj.Add(New SampleKeys(270, 80, 40, 40, "h", "H"))
        obj.Add(New SampleKeys(310, 80, 40, 40, "j", "J"))
        obj.Add(New SampleKeys(350, 80, 40, 40, "k", "K"))
        obj.Add(New SampleKeys(390, 80, 40, 40, "l", "L"))
        obj.Add(New SampleKeys(430, 80, 40, 40, ";", ":"))
        obj.Add(New SampleKeys(470, 80, 40, 40, "'", """"))
        obj.Add(New SampleKeys(510, 80, 80, 40, "Enter", "Enter", "{ENTER}"))
        obj.Add(New SampleKeys(590, 80, 40, 40, "PgUp", "PgUp", "{PGUP}"))

        obj.Add(New SampleKeys(0, 120, 80, 40, "Shift", "Shift", "{Shift}"))
        obj.Add(New SampleKeys(80, 120, 40, 40, "z", "A"))
        obj.Add(New SampleKeys(120, 120, 40, 40, "x", "S"))
        obj.Add(New SampleKeys(160, 120, 40, 40, "c", "D"))
        obj.Add(New SampleKeys(200, 120, 40, 40, "v", "F"))
        obj.Add(New SampleKeys(240, 120, 40, 40, "b", "G"))
        obj.Add(New SampleKeys(280, 120, 40, 40, "n", "H"))
        obj.Add(New SampleKeys(320, 120, 40, 40, "m", "J"))
        obj.Add(New SampleKeys(360, 120, 40, 40, ",", "K"))
        obj.Add(New SampleKeys(400, 120, 40, 40, ".", "L"))
        obj.Add(New SampleKeys(440, 120, 40, 40, "/", ":"))
        obj.Add(New SampleKeys(480, 120, 70, 40, "Shift", "", "{SHIFT}"))
        obj.Add(New SampleKeys(550, 120, 40, 40, ChrW(8593).ToString, ChrW(8593).ToString, "{UP}"))
        obj.Add(New SampleKeys(590, 120, 40, 40, "PgDn", "PgDn", "{PGDN}"))


        obj.Add(New SampleKeys(120, 160, 360, 40, "", "", " "))
        obj.Add(New SampleKeys(510, 160, 40, 40, ChrW(8592).ToString, ChrW(8592).ToString, "{LEFT}"))
        obj.Add(New SampleKeys(550, 160, 40, 40, ChrW(8595).ToString, ChrW(8595).ToString, "{DOWN}"))
        obj.Add(New SampleKeys(590, 160, 40, 40, ChrW(8594).ToString, ChrW(8594).ToString, "{RIGHT}"))

        Dim h As IDesignerHost = DirectCast(GetService(GetType(IDesignerHost)), IDesignerHost)
        Dim dt As DesignerTransaction
        Dim c As IComponentChangeService = DirectCast(GetService(GetType(IComponentChangeService)), IComponentChangeService)

        'Add a new button to the collection
        dt = h.CreateTransaction("Add Button")

        c.OnComponentChanging(m_Control, Nothing)
        For iCnt As Integer = m_Control.Buttons.Count - 1 To 0 Step -1
            h.DestroyComponent(m_Control.Buttons.Item(iCnt))
            m_Control.Buttons.Remove(iCnt)
        Next

        For Each k As SampleKeys In obj
            Dim objbtn As BG_Button = DirectCast(h.CreateComponent(GetType(BG_Button)), BG_Button)
            objbtn.Bounds = New System.Drawing.Rectangle(k._X, k._Y, k._Width, k._Height)
            'If k._Value = "{UP}" Or k._Value = "{LEFT}" Or k._Value = "{DOWN}" Or k._Value = "{RIGHT}" Then
            '    objbtn.Font = New System.Drawing.Font("Wingdings 3", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            'Else
            objbtn.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            'End If


            objbtn.Normal_Display = k._Normal_Display
            objbtn.SelectedBackColor = System.Drawing.Color.Teal
            objbtn.ShiftPressed_Display = k._ShiftPressed_Display
            objbtn.Value = k._Value
            m_Control.Buttons.Add(objbtn)
        Next
        c.OnComponentChanged(m_Control, Nothing, Nothing, Nothing)
        dt.Commit()
    End Sub

    'Public Class UnicodeCast
    '    Public Shared Sub main(ByVal args() As String)

    '        ' Simple arithmetic on chars - VERY BAD for internationalization!
    '        System.out.println("'a' + 1 = " & CChar("a"c + 1))

    '        ' Truncate characters by casting to byte (16-bit to 8-bit casting)
    '        Dim yen As Char = ChrW(&HA5) ' Japanese Yen
    '        Dim aeAcute As Char = ChrW(&H1FC) ' Roman AE with acute accent
    '        System.out.println("Yen as byte: " & CByte(yen))
    '        System.out.println("AE' as byte: " & CByte(aeAcute))
    '        System.out.println("Yen as byte to char: " & CChar(CByte(yen)))
    '        System.out.println("AE' as byte to char: " & CChar(CByte(aeAcute)))

    '        ' Convert ints to chars
    '        Dim iYen As Integer = &HA5
    '        Dim iaeAcute As Integer = &H1FC
    '        System.out.println("Yen from int = " & ChrW(iYen))
    '        System.out.println("AE' from int = " & ChrW(iaeAcute))

    '    End Sub
    'End Class

    Public Sub FitToControl()

        Dim LastWidth As Integer = 0
        Dim LastHeight As Integer = 0

        Dim NewWidth As Integer = m_Control.Width - 2
        Dim NewHeight As Integer = m_Control.Height - 2

        '***** Find Max Height and Width
        For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
            If m_Control.Buttons(iCnt).Bounds.X + m_Control.Buttons(iCnt).Bounds.Width > LastWidth Then
                LastWidth = m_Control.Buttons(iCnt).Bounds.X + m_Control.Buttons(iCnt).Bounds.Width
            End If

            If m_Control.Buttons(iCnt).Bounds.Y + m_Control.Buttons(iCnt).Bounds.Height > LastHeight Then
                LastHeight = m_Control.Buttons(iCnt).Bounds.Y + m_Control.Buttons(iCnt).Bounds.Height
            End If
        Next

        For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
            Dim rect As New Rectangle( _
            Math.Ceiling((NewWidth * m_Control.Buttons(iCnt).Bounds.X) / LastWidth) _
            , Math.Ceiling((NewHeight * m_Control.Buttons(iCnt).Bounds.Y) / LastHeight) _
            , Math.Ceiling((NewWidth * m_Control.Buttons(iCnt).Bounds.Width) / LastWidth) _
            , Math.Ceiling((NewHeight * m_Control.Buttons(iCnt).Bounds.Height) / LastHeight) _
            )

            Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "Bounds", rect)
            m_Control.Refresh()
        Next
    End Sub

#Region " Appearance "
    Public Property ForeColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.ForeColor
            Else
                Return m_Control.Buttons.Item(0).ForeColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "ForeColor", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property BackColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.BackColor
            Else
                Return m_Control.Buttons.Item(0).BackColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "BackColor", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property BorderColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return Color.Black
            Else
                Return m_Control.Buttons.Item(0).BorderColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "BorderColor", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property BorderWidth() As Single
        Get
            If m_Control.Buttons.Count = 0 Then
                Return 1.0!
            Else
                Return m_Control.Buttons.Item(0).BorderWidth
            End If
        End Get
        Set(ByVal value As Single)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "BorderWidth", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property SelectedBackColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return Color.LightGray
            Else
                Return m_Control.Buttons.Item(0).SelectedBackColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "SelectedBackColor", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property SelectedForeColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return Color.Blue
            Else
                Return m_Control.Buttons.Item(0).SelectedForeColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "SelectedForeColor", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Shadows Property Font() As System.Drawing.Font
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.Font
            Else
                Return m_Control.Buttons.Item(0).Font
            End If
        End Get
        Set(ByVal value As Font)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "Font", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property TextAlignment() As Alignment
        Get
            If m_Control.Buttons.Count = 0 Then
                Return Alignment.MiddleCenter
            Else
                Return m_Control.Buttons.Item(0).TextAlignment
            End If
        End Get
        Set(ByVal value As Alignment)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "TextAlignment", value)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property Height() As Integer
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.Height
            Else
                Return m_Control.Buttons.Item(0).Bounds.Height
            End If
        End Get
        Set(ByVal value As Integer)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "Bounds", _
                    New Rectangle(m_Control.Buttons.Item(iCnt).Bounds.X, _
                    m_Control.Buttons.Item(iCnt).Bounds.Y, _
                    m_Control.Buttons.Item(iCnt).Bounds.Width, value))
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property Width() As Integer
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.Width
            Else
                Return m_Control.Buttons.Item(0).Bounds.Width
            End If
        End Get
        Set(ByVal value As Integer)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "Bounds", _
                    New Rectangle(m_Control.Buttons.Item(iCnt).Bounds.X, _
                    m_Control.Buttons.Item(iCnt).Bounds.Y, _
                    value, _
                    m_Control.Buttons.Item(iCnt).Bounds.Height))
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Sub SwapColors()
        Dim c As Color = Me.BackColor
        Me.BackColor = Me.ForeColor
        Me.ForeColor = c
        'RefreshDesigner()
    End Sub

    Public Sub AutoArrange()
        Dim x As Integer = 0
        Dim y As Integer = 0
        Dim maxHight As Integer = 0

        For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
            If (x + m_Control.Buttons(iCnt).Bounds.Width) > m_Control.Width Then
                y += maxHight
                x = 0
                maxHight = 0
            End If

            If m_Control.Buttons(iCnt).Bounds.Width > maxHight Then
                maxHight = m_Control.Buttons(iCnt).Bounds.Width
            End If

            Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "Bounds", New Rectangle(x, y, m_Control.Buttons.Item(iCnt).Bounds.Width, m_Control.Buttons.Item(iCnt).Bounds.Height))

            x += m_Control.Buttons(iCnt).Bounds.Width
            m_Control.Refresh()
        Next
    End Sub

    Public Property StartColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.BackColor
            Else
                Return m_Control.Buttons.Item(0).GradientBackColor.StartColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Dim color As New GradientColor(value, _
                    m_Control.Buttons.Item(iCnt).GradientBackColor.EndColor, _
                    m_Control.Buttons.Item(iCnt).GradientBackColor.FillDirection)
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "GradientBackColor", color)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property EndColor() As Color
        Get
            If m_Control.Buttons.Count = 0 Then
                Return m_Control.BackColor
            Else
                Return m_Control.Buttons.Item(0).GradientBackColor.EndColor
            End If
        End Get
        Set(ByVal value As Color)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Dim color As New GradientColor(m_Control.Buttons.Item(iCnt).GradientBackColor.StartColor, _
                    value, _
                    m_Control.Buttons.Item(iCnt).GradientBackColor.FillDirection)
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "GradientBackColor", color)
                m_Control.Refresh()
            Next
        End Set
    End Property

    Public Property FillDirection() As Drawing2D.LinearGradientMode
        Get
            If m_Control.Buttons.Count = 0 Then
                Return Drawing2D.LinearGradientMode.Vertical
            Else
                Return m_Control.Buttons.Item(0).GradientBackColor.FillDirection
            End If
        End Get
        Set(ByVal value As Drawing2D.LinearGradientMode)
            For iCnt As Integer = 0 To m_Control.Buttons.Count - 1
                Dim color As New GradientColor(m_Control.Buttons.Item(iCnt).GradientBackColor.StartColor, _
                    m_Control.Buttons.Item(iCnt).GradientBackColor.EndColor, _
                    value)
                Me.SetPropertyByName(m_Control.Buttons.Item(iCnt), "GradientBackColor", color)
                m_Control.Refresh()
            Next
        End Set
    End Property
#End Region
End Class

