<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeScrollBar
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If PageUp IsNot Nothing Then
                PageUp.Stop()
                PageUp.Enabled = False
            End If
            If PageDown IsNot Nothing Then
                PageDown.Stop()
                PageDown.Enabled = False
            End If
            If components IsNot Nothing Then
            components.Dispose()
        End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'eZeeScrollBar
        '
        Me.Name = "eZeeScrollBar"
        Me.Size = New System.Drawing.Size(50, 200)
        Me.ResumeLayout(False)

    End Sub

End Class
