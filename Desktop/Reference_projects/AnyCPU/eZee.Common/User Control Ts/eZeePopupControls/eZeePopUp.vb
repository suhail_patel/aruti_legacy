Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

<DefaultProperty("Text"), DefaultEvent("PopUp")> _
Public Class eZeePopUp

    Dim m_strCaption As String = "Select Value"
    Dim m_strOkCaption As String = "Finish"
    Dim m_strCancelCaption As String = "Cancel"

#Region " Properties "
    Public Property Caption() As String
        Get
            Return m_strCaption
        End Get
        Set(ByVal value As String)
            m_strCaption = value
        End Set
    End Property

    Public Property Ok_Caption() As String
        Get
            Return m_strOkCaption
        End Get
        Set(ByVal value As String)
            m_strOkCaption = value
        End Set
    End Property

    Public Property Cancel_Caption() As String
        Get
            Return m_strCancelCaption
        End Get
        Set(ByVal value As String)
            m_strCancelCaption = value
        End Set
    End Property

    Public Shadows Property Font() As Font
        Get
            Return txt1.Font
        End Get
        Set(ByVal value As Font)
            txt1.Font = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Text() As String
        Get
            Return txt1.Text
        End Get
        Set(ByVal value As String)
            txt1.Text = value
        End Set
    End Property

    Public Property Multiline() As Boolean
        Get
            Return txt1.Multiline
        End Get
        Set(ByVal value As Boolean)
            txt1.Multiline = value
        End Set
    End Property

    Public Property PasswordChar() As Char
        Get
            Return txt1.PasswordChar
        End Get
        Set(ByVal value As Char)
            txt1.PasswordChar = value
        End Set
    End Property

    Public Shadows Property RightToLeft() As RightToLeft
        Get
            Return txt1.RightToLeft
        End Get
        Set(ByVal value As RightToLeft)
            txt1.RightToLeft = value
        End Set
    End Property

    Public Property BorderWidth() As Integer
        Get
            Return pnlBorder.Padding.All
        End Get
        Set(ByVal value As Integer)
            pnlBorder.Padding = New Padding(value)
        End Set
    End Property

    Public Property BroderColor() As Color
        Get
            Return pnlBorder.BackColor
        End Get
        Set(ByVal value As Color)
            pnlBorder.BackColor = value
        End Set
    End Property

    Public Property BackGroundColor() As Color
        Get
            Return txt1.BackColor
        End Get
        Set(ByVal value As Color)
            If value <> Color.Transparent Then
                pnlText.BackColor = value
                txt1.BackColor = value
            End If
        End Set
    End Property

    Public Shadows Property ForeColor() As Color
        Get
            Return txt1.ForeColor
        End Get
        Set(ByVal value As Color)
            txt1.ForeColor = value
        End Set
    End Property

    Public Property TextAlign() As HorizontalAlignment
        Get
            Return txt1.TextAlign
        End Get
        Set(ByVal value As HorizontalAlignment)
            txt1.TextAlign = value
        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return txt1.MaxLength
        End Get
        Set(ByVal value As Integer)
            txt1.MaxLength = value
        End Set
    End Property

    Public Property [ReadOnly]() As Boolean
        Get
            Return txt1.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txt1.ReadOnly = value
        End Set
    End Property

    Public Property InvalidChars() As Char()
        Get
            Return txt1.InvalidChars
        End Get
        Set(ByVal value As Char())
            txt1.InvalidChars = value
        End Set
    End Property

    Public Property SelectionMode() As Boolean
        Get
            Return txt1.SelectionMode
        End Get
        Set(ByVal value As Boolean)
            txt1.SelectionMode = value
        End Set
    End Property
#End Region

#Region " Buttons "
    Private Sub btnKeyBoard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeyBoard.Click
        RaiseEvent PopUp(sender, e)
    End Sub
#End Region

#Region " Events "
    Public Event PopUp(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    Private Sub txt1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt1.GotFocus
        RaiseEvent GotFocus(Me, e)
    End Sub

    Private Sub txt1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt1.LostFocus
        RaiseEvent LostFocus(Me, e)
    End Sub

    Private Sub txt1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt1.TextChanged
        RaiseEvent TextChanged(Me, e)
    End Sub

    Private Sub txt1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt1.KeyPress
        RaiseEvent KeyPress(Me, e)
    End Sub

    Private Sub txt1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt1.KeyUp
        If e.KeyCode = Keys.F5 Then
            Call btnKeyBoard_Click(Nothing, Nothing)
        End If
    End Sub

#End Region

End Class
