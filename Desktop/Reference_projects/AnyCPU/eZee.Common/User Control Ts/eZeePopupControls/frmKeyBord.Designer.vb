<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKeyBord
    Inherits frmBasePopup

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmKeyBord))
        Me.ezKbMain = New eZee.Common.eZeeKeyboard
        Me.btnStandard = New eZee.Common.eZeeButton
        Me.btnAlphabetical = New eZee.Common.eZeeButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.txtKeyBoardText = New eZee.TextBox.AlphanumericTextBox
        Me.btnCancel = New eZee.Common.eZeeButton
        Me.btnFinish = New eZee.Common.eZeeButton
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ezKbMain
        '
        Me.ezKbMain.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ezKbMain.KeyboardType = eZee.Common.eZeeKeyBoardLayOut.KeyBoard_Standard
        Me.ezKbMain.Location = New System.Drawing.Point(6, 88)
        Me.ezKbMain.Name = "ezKbMain"
        Me.ezKbMain.Size = New System.Drawing.Size(744, 226)
        Me.ezKbMain.TabIndex = 1
        '
        'btnStandard
        '
        Me.btnStandard.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStandard.BackColor = System.Drawing.Color.Blue
        Me.btnStandard.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold)
        Me.btnStandard.GlowColor = System.Drawing.Color.Blue
        Me.btnStandard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStandard.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnStandard.Location = New System.Drawing.Point(9, 326)
        Me.btnStandard.Name = "btnStandard"
        Me.btnStandard.ShineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnStandard.Size = New System.Drawing.Size(30, 29)
        Me.btnStandard.TabIndex = 4
        Me.btnStandard.Text = "S"
        '
        'btnAlphabetical
        '
        Me.btnAlphabetical.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAlphabetical.BackColor = System.Drawing.Color.Blue
        Me.btnAlphabetical.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold)
        Me.btnAlphabetical.GlowColor = System.Drawing.Color.Blue
        Me.btnAlphabetical.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAlphabetical.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnAlphabetical.Location = New System.Drawing.Point(45, 326)
        Me.btnAlphabetical.Name = "btnAlphabetical"
        Me.btnAlphabetical.ShineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAlphabetical.Size = New System.Drawing.Size(30, 29)
        Me.btnAlphabetical.TabIndex = 5
        Me.btnAlphabetical.Text = "A"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnAlphabetical)
        Me.Panel1.Controls.Add(Me.btnStandard)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.ezKbMain)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(1, 35)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(757, 366)
        Me.Panel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.txtKeyBoardText)
        Me.Panel2.Location = New System.Drawing.Point(6, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(744, 82)
        Me.Panel2.TabIndex = 0
        '
        'txtKeyBoardText
        '
        Me.txtKeyBoardText.BackColor = System.Drawing.Color.White
        Me.txtKeyBoardText.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtKeyBoardText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtKeyBoardText.Flags = 0
        Me.txtKeyBoardText.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKeyBoardText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtKeyBoardText.InvalidChars = New Char(-1) {}
        Me.txtKeyBoardText.Location = New System.Drawing.Point(0, 0)
        Me.txtKeyBoardText.Multiline = True
        Me.txtKeyBoardText.Name = "txtKeyBoardText"
        Me.txtKeyBoardText.SelectionHighlight = False
        Me.txtKeyBoardText.SelectionMode = False
        Me.txtKeyBoardText.Size = New System.Drawing.Size(744, 82)
        Me.txtKeyBoardText.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.Red
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnCancel.GlowColor = System.Drawing.Color.MistyRose
        Me.btnCancel.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnCancel.Location = New System.Drawing.Point(636, 325)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.ShineColor = System.Drawing.Color.LightCoral
        Me.btnCancel.Size = New System.Drawing.Size(113, 34)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnFinish
        '
        Me.btnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinish.BackColor = System.Drawing.Color.Green
        Me.btnFinish.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnFinish.GlowColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnFinish.Location = New System.Drawing.Point(517, 325)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.ShineColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.Size = New System.Drawing.Size(113, 34)
        Me.btnFinish.TabIndex = 2
        Me.btnFinish.Text = "Finish"
        '
        'frmKeyBord
        '
        Me.AcceptButton = Me.btnFinish
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(759, 402)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmKeyBord"
        Me.ShowIcon = False
        Me.Text = "Enter Text"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ezKbMain As eZeeKeyboard
    Friend WithEvents btnAlphabetical As eZeeButton
    Friend WithEvents btnStandard As eZeeButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As eZeeButton
    Friend WithEvents btnFinish As eZeeButton
    Friend WithEvents txtKeyBoardText As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class
