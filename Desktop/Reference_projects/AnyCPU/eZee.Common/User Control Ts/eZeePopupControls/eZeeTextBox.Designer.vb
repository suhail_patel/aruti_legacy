<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeTextBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(eZeeTextBox))
        Me.txt1 = New eZee.TextBox.AlphanumericTextBox
        Me.pnlBtn = New System.Windows.Forms.Panel
        Me.btnKeyBoard = New eZee.Common.eZeeGradientButton
        Me.pnlBorder = New System.Windows.Forms.Panel
        Me.pnlText = New System.Windows.Forms.Panel
        Me.pnlBtn.SuspendLayout()
        Me.pnlBorder.SuspendLayout()
        Me.pnlText.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt1
        '
        Me.txt1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt1.BackColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.txt1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt1.Flags = 0
        Me.txt1.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1.ForeColor = System.Drawing.Color.White
        Me.txt1.InvalidChars = New Char(-1) {}
        Me.txt1.Location = New System.Drawing.Point(6, 6)
        Me.txt1.Name = "txt1"
        Me.txt1.SelectionHighlight = False
        Me.txt1.SelectionMode = False
        Me.txt1.Size = New System.Drawing.Size(241, 24)
        Me.txt1.TabIndex = 21
        '
        'pnlBtn
        '
        Me.pnlBtn.BackColor = System.Drawing.Color.Transparent
        Me.pnlBtn.Controls.Add(Me.btnKeyBoard)
        Me.pnlBtn.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlBtn.Location = New System.Drawing.Point(257, 0)
        Me.pnlBtn.Name = "pnlBtn"
        Me.pnlBtn.Size = New System.Drawing.Size(42, 43)
        Me.pnlBtn.TabIndex = 23
        '
        'btnKeyBoard
        '
        Me.btnKeyBoard.BackColor1 = System.Drawing.Color.Transparent
        Me.btnKeyBoard.BackColor2 = System.Drawing.Color.Transparent
        Me.btnKeyBoard.BorderHoverColor = System.Drawing.Color.White
        Me.btnKeyBoard.BorderNormal = True
        Me.btnKeyBoard.BorderNormalColor = System.Drawing.Color.White
        Me.btnKeyBoard.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnKeyBoard.Image = CType(resources.GetObject("btnKeyBoard.Image"), System.Drawing.Image)
        Me.btnKeyBoard.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnKeyBoard.Location = New System.Drawing.Point(2, 0)
        Me.btnKeyBoard.Name = "btnKeyBoard"
        Me.btnKeyBoard.Size = New System.Drawing.Size(38, 38)
        Me.btnKeyBoard.TabIndex = 28
        '
        'pnlBorder
        '
        Me.pnlBorder.BackColor = System.Drawing.Color.White
        Me.pnlBorder.Controls.Add(Me.pnlText)
        Me.pnlBorder.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBorder.Location = New System.Drawing.Point(0, 0)
        Me.pnlBorder.Name = "pnlBorder"
        Me.pnlBorder.Padding = New System.Windows.Forms.Padding(2)
        Me.pnlBorder.Size = New System.Drawing.Size(257, 43)
        Me.pnlBorder.TabIndex = 24
        '
        'pnlText
        '
        Me.pnlText.BackColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.pnlText.Controls.Add(Me.txt1)
        Me.pnlText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlText.Location = New System.Drawing.Point(2, 2)
        Me.pnlText.Name = "pnlText"
        Me.pnlText.Size = New System.Drawing.Size(253, 39)
        Me.pnlText.TabIndex = 25
        '
        'eZeeTextBox
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.pnlBorder)
        Me.Controls.Add(Me.pnlBtn)
        Me.Name = "eZeeTextBox"
        Me.Size = New System.Drawing.Size(299, 43)
        Me.pnlBtn.ResumeLayout(False)
        Me.pnlBorder.ResumeLayout(False)
        Me.pnlText.ResumeLayout(False)
        Me.pnlText.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlBtn As System.Windows.Forms.Panel
    Friend WithEvents pnlBorder As System.Windows.Forms.Panel
    Friend WithEvents pnlText As System.Windows.Forms.Panel
    Friend WithEvents btnKeyBoard As eZeeGradientButton

End Class
