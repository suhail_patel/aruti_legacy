Imports System
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Friend Class ListViewMethods
    Private Const LVM_FIRST As Integer = &H1000
    Private Const LVM_SCROLL As Integer = LVM_FIRST + 20
    Private Const LVM_GETHEADER As Integer = LVM_FIRST + 31
    Private Const LVM_GETCOUNTPERPAGE As Integer = LVM_FIRST + 40
    Private Const LVM_SETITEMSTATE As Integer = LVM_FIRST + 43
    Private Const LVM_SETEXTENDEDLISTVIEWSTYLE As Integer = LVM_FIRST + 54
    Private Const LVM_SETITEM As Integer = LVM_FIRST + 76
    Private Const LVM_GETCOLUMN As Integer = LVM_FIRST + 95
    Private Const LVM_SETCOLUMN As Integer = LVM_FIRST + 96

    Private Const LVS_EX_SUBITEMIMAGES As Integer = &H2

    Private Const LVIF_TEXT As Integer = &H1
    Private Const LVIF_IMAGE As Integer = &H2
    Private Const LVIF_PARAM As Integer = &H4
    Private Const LVIF_STATE As Integer = &H8
    Private Const LVIF_INDENT As Integer = &H10
    Private Const LVIF_NORECOMPUTE As Integer = &H800

    Private Const LVCF_FMT As Integer = &H1
    Private Const LVCF_WIDTH As Integer = &H2
    Private Const LVCF_TEXT As Integer = &H4
    Private Const LVCF_SUBITEM As Integer = &H8
    Private Const LVCF_IMAGE As Integer = &H10
    Private Const LVCF_ORDER As Integer = &H20
    Private Const LVCFMT_LEFT As Integer = &H0
    Private Const LVCFMT_RIGHT As Integer = &H1
    Private Const LVCFMT_CENTER As Integer = &H2
    Private Const LVCFMT_JUSTIFYMASK As Integer = &H3

    Private Const LVCFMT_IMAGE As Integer = &H800
    Private Const LVCFMT_BITMAP_ON_RIGHT As Integer = &H1000
    Private Const LVCFMT_COL_HAS_IMAGES As Integer = &H8000

    Private Const HDM_FIRST As Integer = &H1200
    Private Const HDM_HITTEST As Integer = HDM_FIRST + 6
    Private Const HDM_GETITEM As Integer = HDM_FIRST + 11
    Private Const HDM_SETITEM As Integer = HDM_FIRST + 12

    Private Const HDI_WIDTH As Integer = &H1
    Private Const HDI_TEXT As Integer = &H2
    Private Const HDI_FORMAT As Integer = &H4
    Private Const HDI_BITMAP As Integer = &H10
    Private Const HDI_IMAGE As Integer = &H20

    Private Const HDF_LEFT As Integer = &H0
    Private Const HDF_RIGHT As Integer = &H1
    Private Const HDF_CENTER As Integer = &H2
    Private Const HDF_JUSTIFYMASK As Integer = &H3
    Private Const HDF_RTLREADING As Integer = &H4
    Private Const HDF_STRING As Integer = &H4000
    Private Const HDF_BITMAP As Integer = &H2000
    Private Const HDF_BITMAP_ON_RIGHT As Integer = &H1000
    Private Const HDF_IMAGE As Integer = &H800
    Private Const HDF_SORTUP As Integer = &H400
    Private Const HDF_SORTDOWN As Integer = &H200

    Private Const SB_HORZ As Integer = 0
    Private Const SB_VERT As Integer = 1
    Private Const SB_CTL As Integer = 2
    Private Const SB_BOTH As Integer = 3

    Private Const SIF_RANGE As Integer = &H1
    Private Const SIF_PAGE As Integer = &H2
    Private Const SIF_POS As Integer = &H4
    Private Const SIF_DISABLENOSCROLL As Integer = &H8
    Private Const SIF_TRACKPOS As Integer = &H10
    Private Const SIF_ALL As Integer = (SIF_RANGE Or SIF_PAGE Or SIF_POS Or SIF_TRACKPOS)

    Private Const ILD_NORMAL As Integer = &H0
    Private Const ILD_TRANSPARENT As Integer = &H1
    Private Const ILD_MASK As Integer = &H10
    Private Const ILD_IMAGE As Integer = &H20
    Private Const ILD_BLEND25 As Integer = &H2
    Private Const ILD_BLEND50 As Integer = &H4

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure HDITEM
        Public mask As Integer
        Public cxy As Integer
        Public pszText As IntPtr
        Public hbm As IntPtr
        Public cchTextMax As Integer
        Public fmt As Integer
        Public lParam As IntPtr
        Public iImage As Integer
        Public iOrder As Integer
        'if (_WIN32_IE >= 0x0500)
        Public type As Integer
        Public pvFilter As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Class HDHITTESTINFO
        Public pt_x As Integer
        Public pt_y As Integer
        Public flags As Integer
        Public iItem As Integer
    End Class

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Public Structure LVCOLUMN
        Public mask As Integer
        Public fmt As Integer
        Public cx As Integer
        <MarshalAs(UnmanagedType.LPTStr)> _
        Public pszText As String
        Public cchTextMax As Integer
        Public iSubItem As Integer
        ' These are available in Common Controls >= 0x0300
        Public iImage As Integer
        Public iOrder As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Public Structure LVFINDINFO
        Public flags As Integer
        Public psz As String
        Public lParam As IntPtr
        Public ptX As Integer
        Public ptY As Integer
        Public vkDirection As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Public Structure LVHITTESTINFO
        Public pt_x As Integer
        Public pt_y As Integer
        Public flags As Integer
        Public iItem As Integer
        Public iSubItem As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Public Structure LVITEM
        Public mask As Integer
        Public iItem As Integer
        Public iSubItem As Integer
        Public state As Integer
        Public stateMask As Integer
        <MarshalAs(UnmanagedType.LPTStr)> _
        Public pszText As String
        Public cchTextMax As Integer
        Public iImage As Integer
        Public lParam As IntPtr
        ' These are available in Common Controls >= 0x0300
        Public iIndent As Integer
        ' These are available in Common Controls >= 0x056
        Public iGroupId As Integer
        Public cColumns As Integer
        Public puColumns As IntPtr
    End Structure

    ''' <summary>
    ''' Notify m header structure.
    ''' </summary>
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure NMHDR
        Public hwndFrom As IntPtr
        Public idFrom As IntPtr
        Public code As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure NMHEADER
        Public nhdr As NMHDR
        Public iItem As Integer
        Public iButton As Integer
        Public pHDITEM As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure NMLISTVIEW
        Public hdr As ListViewMethods.NMHDR
        Public iItem As Integer
        Public iSubItem As Integer
        Public uNewState As Integer
        Public uOldState As Integer
        Public uChanged As Integer
        Public lParam As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure NMLVFINDITEM
        Public hdr As ListViewMethods.NMHDR
        Public iStart As Integer
        Public lvfi As ListViewMethods.LVFINDINFO
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure RECT
        Public left As Integer
        Public top As Integer
        Public right As Integer
        Public bottom As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Class SCROLLINFO
        Public cbSize As Integer = Marshal.SizeOf(GetType(ListViewMethods.SCROLLINFO))
        Public fMask As Integer
        Public nMin As Integer
        Public nMax As Integer
        Public nPage As Integer
        Public nPos As Integer
        Public nTrackPos As Integer
    End Class

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Public Class TOOLINFO
        Public cbSize As Integer = Marshal.SizeOf(GetType(ListViewMethods.TOOLINFO))
        Public uFlags As Integer
        Public hwnd As IntPtr
        Public uId As IntPtr
        Public rect As ListViewMethods.RECT
        Public hinst As IntPtr = IntPtr.Zero
        Public lpszText As IntPtr
        Public lParam As IntPtr = IntPtr.Zero
    End Class

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Public Structure TOOLTIPTEXT
        Public hdr As ListViewMethods.NMHDR
        Public lpszText As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)> _
        Public szText As String
        Public hinst As IntPtr
        Public uFlags As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure WINDOWPOS
        Public hwnd As IntPtr
        Public hwndInsertAfter As IntPtr
        Public x As Integer
        Public y As Integer
        Public cx As Integer
        Public cy As Integer
        Public flags As Integer
    End Structure

    ' Various flavours of SendMessage: plain vanilla, and passing references to various structures
    <DllImport("user32.dll", CharSet:=CharSet.Auto)> _
    Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As IntPtr
    End Function

    <DllImport("user32.dll", EntryPoint:="SendMessage", CharSet:=CharSet.Auto)> _
    Private Shared Function SendMessageLVItem(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, ByRef lvi As LVITEM) As IntPtr
    End Function

    '[DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
    'private static extern IntPtr SendMessageLVColumn(IntPtr hWnd, int m, int wParam, ref LVCOLUMN lvc);
    <DllImport("user32.dll", EntryPoint:="SendMessage", CharSet:=CharSet.Auto)> _
    Private Shared Function SendMessageHDItem(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, ByRef hdi As HDITEM) As IntPtr
    End Function

    <DllImport("user32.dll", EntryPoint:="SendMessage", CharSet:=CharSet.Auto)> _
    Public Shared Function SendMessageHDHITTESTINFO(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, <[In](), Out()> ByVal lParam As HDHITTESTINFO) As IntPtr
    End Function

    <DllImport("user32.dll", EntryPoint:="SendMessage", CharSet:=CharSet.Auto)> _
    Public Shared Function SendMessageTOOLINFO(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As ListViewMethods.TOOLINFO) As IntPtr
    End Function

    ' Entry points used by this code
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Public Shared Function GetScrollInfo(ByVal hWnd As IntPtr, ByVal fnBar As Integer, ByVal si As SCROLLINFO) As Boolean
    End Function

    <DllImport("user32.dll", EntryPoint:="GetUpdateRect", CharSet:=CharSet.Auto)> _
    Private Shared Function GetUpdateRectInternal(ByVal hWnd As IntPtr, ByRef r As Rectangle, ByVal eraseBackground As Boolean) As Integer
    End Function

    <DllImport("comctl32.dll", CharSet:=CharSet.Auto)> _
    Private Shared Function ImageList_Draw(ByVal himl As IntPtr, ByVal i As Integer, ByVal hdcDst As IntPtr, ByVal x As Integer, ByVal y As Integer, ByVal fStyle As Integer) As Boolean
    End Function

    '[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
    'public static extern bool SetScrollInfo(IntPtr hWnd, int fnBar, SCROLLINFO si, bool fRedraw);

    <DllImport("user32.dll", EntryPoint:="ValidateRect", CharSet:=CharSet.Auto)> _
    Private Shared Function ValidatedRectInternal(ByVal hWnd As IntPtr, ByRef r As Rectangle) As IntPtr
    End Function

    Public Shared Function DrawImageList(ByVal g As Graphics, ByVal il As ImageList, ByVal index As Integer, ByVal x As Integer, ByVal y As Integer, ByVal isSelected As Boolean) As Boolean
        Dim flags As Integer = ILD_TRANSPARENT
        If isSelected Then
            flags = flags Or ILD_BLEND25
        End If
        Dim result As Boolean = ImageList_Draw(il.Handle, index, g.GetHdc(), x, y, flags)
        g.ReleaseHdc()
        Return result
    End Function

    ''' <summary>
    ''' Make sure the ListView has the extended style that says to display subitem images.
    ''' </summary>
    ''' <remarks>This method must be called after any .NET call that update the extended styles
    ''' since they seem to erase this setting.</remarks>
    ''' <param name="list">The listview to send a m to</param>
    Public Shared Sub ForceSubItemImagesExStyle(ByVal list As ListView)
        SendMessage(list.Handle, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_SUBITEMIMAGES, LVS_EX_SUBITEMIMAGES)
    End Sub

    ''' <summary>
    ''' Calculates the number of items that can fit vertically in the visible area of a list-view (which
    ''' must be in details or list view.
    ''' </summary>
    ''' <param name="list">The listView</param>
    ''' <returns>Number of visible items per page</returns>
    Public Shared Function GetCountPerPage(ByVal list As ListView) As Integer
        Return CInt(SendMessage(list.Handle, LVM_GETCOUNTPERPAGE, 0, 0))
    End Function
    ''' <summary>
    ''' For the given item and subitem, make it display the given image
    ''' </summary>
    ''' <param name="list">The listview to send a m to</param>
    ''' <param name="itemIndex">row number (0 based)</param>
    ''' <param name="subItemIndex">subitem (0 is the item itself)</param>
    ''' <param name="imageIndex">index into the image list</param>
    Public Shared Sub SetSubItemImage(ByVal list As ListView, ByVal itemIndex As Integer, ByVal subItemIndex As Integer, ByVal imageIndex As Integer)
        Dim lvItem As New LVITEM()
        lvItem.mask = LVIF_IMAGE
        lvItem.iItem = itemIndex
        lvItem.iSubItem = subItemIndex
        lvItem.iImage = imageIndex
        SendMessageLVItem(list.Handle, LVM_SETITEM, 0, lvItem)
    End Sub

    ''' <summary>
    ''' Setup the given column of the listview to show the given image to the right of the text.
    ''' If the image index is -1, any previous image is cleared
    ''' </summary>
    ''' <param name="list">The listview to send a m to</param>
    ''' <param name="columnIndex">Index of the column to modifiy</param>
    ''' <param name="order"></param>
    ''' <param name="imageIndex">Index into the small image list</param>
    Public Shared Sub SetColumnImage(ByVal list As ListView, ByVal columnIndex As Integer, ByVal order As SortOrder, ByVal imageIndex As Integer)
        Dim hdrCntl As IntPtr = ListViewMethods.GetHeaderControl(list)
        If hdrCntl.ToInt32() = 0 Then
            Exit Sub
        End If

        Dim item As New HDITEM()
        item.mask = HDI_FORMAT
        Dim result As IntPtr = SendMessageHDItem(hdrCntl, HDM_GETITEM, columnIndex, item)

        item.fmt = item.fmt And Not (HDF_SORTUP Or HDF_SORTDOWN Or HDF_IMAGE Or HDF_BITMAP_ON_RIGHT)

        If ListViewMethods.HasBuiltinSortIndicators() Then
            If order = SortOrder.Ascending Then
                item.fmt = item.fmt Or HDF_SORTUP
            End If
            If order = SortOrder.Descending Then
                item.fmt = item.fmt Or HDF_SORTDOWN
            End If
        Else
            item.mask = item.mask Or HDI_IMAGE
            item.fmt = item.fmt Or (HDF_IMAGE Or HDF_BITMAP_ON_RIGHT)
            item.iImage = imageIndex
        End If

        result = SendMessageHDItem(hdrCntl, HDM_SETITEM, columnIndex, item)
    End Sub

    ''' <summary>
    ''' Does this version of the operating system have builtin sort indicators?
    ''' </summary>
    ''' <returns>Are there builtin sort indicators</returns>
    ''' <remarks>XP and later have these</remarks>
    Public Shared Function HasBuiltinSortIndicators() As Boolean
        Return OSFeature.Feature.GetVersionPresent(OSFeature.Themes) IsNot Nothing
    End Function

    ''' <summary>
    ''' Return the bounds of the update region on the given control.
    ''' </summary>
    ''' <remarks>The BeginPaint() system call validates the update region, effectively wiping out this information.
    ''' So this call has to be made before the BeginPaint() call.</remarks>
    ''' <param name="cntl">The control whose update region is be calculated</param>
    ''' <returns>A rectangle</returns>
    Public Shared Function GetUpdateRect(ByVal cntl As Control) As Rectangle
        Dim r As New Rectangle()
        GetUpdateRectInternal(cntl.Handle, r, False)
        Return r
    End Function

    ''' <summary>
    ''' Validate an area of the given control. A validated area will not be repainted at the next redraw.
    ''' </summary>
    ''' <param name="cntl">The control to be validated</param>
    ''' <param name="r">The area of the control to be validated</param>
    Public Shared Sub ValidateRect(ByVal cntl As Control, ByVal r As Rectangle)
        ValidatedRectInternal(cntl.Handle, r)
    End Sub

    ''' <summary>
    ''' Select all rows on the given listview
    ''' </summary>
    ''' <param name="list">The listview whose items are to be selected</param>
    Public Shared Sub SelectAllItems(ByVal list As ListView)
        ListViewMethods.SetItemState(list, -1, 2, 2)
    End Sub

    ''' <summary>
    ''' Deselect all rows on the given listview
    ''' </summary>
    ''' <param name="list">The listview whose items are to be deselected</param>
    Public Shared Sub DeselectAllItems(ByVal list As ListView)
        ListViewMethods.SetItemState(list, -1, 2, 0)
    End Sub

    ''' <summary>
    ''' Set the item state on the given item
    ''' </summary>
    ''' <param name="list">The listview whose item's state is to be changed</param>
    ''' <param name="itemIndex">The index of the item to be changed</param>
    ''' <param name="mask">Which bits of the value are to be set?</param>
    ''' <param name="value">The value to be set</param>
    Public Shared Sub SetItemState(ByVal list As ListView, ByVal itemIndex As Integer, ByVal mask As Integer, ByVal value As Integer)
        Dim lvItem As New LVITEM()
        lvItem.stateMask = mask
        lvItem.state = value
        SendMessageLVItem(list.Handle, LVM_SETITEMSTATE, itemIndex, lvItem)
    End Sub

    ''' <summary>
    ''' Scroll the given listview by the given deltas
    ''' </summary>
    ''' <param name="list"></param>
    ''' <param name="dx"></param>
    ''' <param name="dy"></param>
    ''' <returns>true if the scroll succeeded</returns>
    Public Shared Function Scroll(ByVal list As ListView, ByVal dx As Integer, ByVal dy As Integer) As Boolean
        Return SendMessage(list.Handle, LVM_SCROLL, dx, dy) <> IntPtr.Zero
    End Function

    ''' <summary>
    ''' Return the handle to the header control on the given list
    ''' </summary>
    ''' <param name="list">The listview whose header control is to be returned</param>
    ''' <returns>The handle to the header control</returns>
    Public Shared Function GetHeaderControl(ByVal list As ListView) As IntPtr
        Return SendMessage(list.Handle, LVM_GETHEADER, 0, 0)
    End Function

    ''' <summary>
    ''' Return the index of the divider under the given point. Return -1 if no divider is under the pt
    ''' </summary>
    ''' <param name="handle">The list we are interested in</param>
    ''' <param name="pt">The client co-ords</param>
    ''' <returns>The index of the divider under the point, or -1 if no divider is under that point</returns>
    Public Shared Function GetDividerUnderPoint(ByVal handle As IntPtr, ByVal pt As Point) As Integer
        Const HHT_ONDIVIDER As Integer = 4
        Return ListViewMethods.HeaderControlHitTest(handle, pt, HHT_ONDIVIDER)
    End Function

    ''' <summary>
    ''' Return the index of the column of the header that is under the given point.
    ''' Return -1 if no column is under the pt
    ''' </summary>
    ''' <param name="handle">The list we are interested in</param>
    ''' <param name="pt">The client co-ords</param>
    ''' <returns>The index of the column under the point, or -1 if no column header is under that point</returns>
    Public Shared Function GetColumnUnderPoint(ByVal handle As IntPtr, ByVal pt As Point) As Integer
        Const HHT_ONHEADER As Integer = 2
        Return ListViewMethods.HeaderControlHitTest(handle, pt, HHT_ONHEADER)
    End Function

    Private Shared Function HeaderControlHitTest(ByVal handle As IntPtr, ByVal pt As Point, ByVal flag As Integer) As Integer
        Dim testInfo As New HDHITTESTINFO()
        testInfo.pt_x = pt.X
        testInfo.pt_y = pt.Y
        Dim result As IntPtr = ListViewMethods.SendMessageHDHITTESTINFO(handle, HDM_HITTEST, IntPtr.Zero, testInfo)
        If (testInfo.flags And flag) <> 0 Then
            Return result.ToInt32()
        Else
            Return -1
        End If
    End Function

    ''' <summary>
    ''' Get the scroll position of the given scroll bar
    ''' </summary>
    ''' <param name="handle"></param>
    ''' <param name="horizontalBar"></param>
    ''' <returns></returns>
    Public Shared Function GetScrollPosition(ByVal handle As IntPtr, ByVal horizontalBar As Boolean) As Integer
        Dim fnBar As Integer = CInt((IIf(horizontalBar, SB_HORZ, SB_VERT)))

        Dim si As New SCROLLINFO()
        si.fMask = SIF_POS
        If GetScrollInfo(handle, fnBar, si) Then
            Return si.nPos
        Else
            Return -1
        End If
    End Function

End Class