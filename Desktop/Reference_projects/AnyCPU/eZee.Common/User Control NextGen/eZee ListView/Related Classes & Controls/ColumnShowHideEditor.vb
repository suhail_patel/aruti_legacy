Imports System.Windows.Forms.Design
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing

<ToolboxItem(False), ToolboxItemFilter("Prevent", ToolboxItemFilterType.Prevent)> _
Public Class ColumnShowHideEditor
    Inherits UserControl

#Region " Private Variables "

    Private _editorService As IWindowsFormsEditorService = Nothing
    Private mstrColumnList As String = ""
    Private mNorListView As New eZeeBaseListView
    Private mblnIsVisibleColumnListProperty As Boolean = False

#End Region

#Region " Construtors "

    Public Sub New(ByVal editorService As IWindowsFormsEditorService)
        InitializeComponent()
        _editorService = editorService
    End Sub

#End Region

#Region " Properties "

    Public Property eZeeLView() As eZeeBaseListView
        Get
            Return mNorListView
        End Get
        Set(ByVal value As eZeeBaseListView)
            mNorListView = value
            Me.Invalidate()
        End Set
    End Property

    Public Property IsVisibleColumnListProperty() As Boolean
        Get
            Return mblnIsVisibleColumnListProperty
        End Get
        Set(ByVal value As Boolean)
            mblnIsVisibleColumnListProperty = value
        End Set
    End Property

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnEnter(ByVal e As System.EventArgs)
        MyBase.OnEnter(e)

            For Each col As ColumnHeader In Me.eZeeLView.Columns
                If Me.IsVisibleColumnListProperty = True Then
                    If Me.eZeeLView.CompulsoryColumns.Contains(col.Name) Then
                        Continue For
                    End If
                End If

                Dim lvItem As New ListViewItem
                lvItem.Tag = col.Name
                lvItem.Text = col.Text

                If Me.IsVisibleColumnListProperty Then
                    If Me.eZeeLView.OptionalColumns.Contains(col.Name) Then
                        lvItem.Checked = True
                    End If
                Else
                    If Me.eZeeLView.CompulsoryColumns.Contains(col.Name) Then
                        lvItem.Checked = True
                    End If
                End If
                lvColumnsView.Items.Add(lvItem)

                lvItem = Nothing
            Next

        If lvColumnsView.Items.Count > 7 Then
            colhColumns.Width = 252 - 17
        Else
            colhColumns.Width = 252
        End If
    End Sub

#End Region

#Region " Control's Events "

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim strCheckedColumns As String = ""
        For Each lvItem As ListViewItem In lvColumnsView.CheckedItems
            strCheckedColumns &= lvItem.Tag.ToString + ","
        Next

        'MsgBox(strCheckedColumns)
            If Me.IsVisibleColumnListProperty Then
                Me.eZeeLView.OptionalColumns = strCheckedColumns.Remove(strCheckedColumns.LastIndexOf(","), 1)
            Else
                Me.eZeeLView.CompulsoryColumns = strCheckedColumns.Remove(strCheckedColumns.LastIndexOf(","), 1)
            End If

        _editorService.CloseDropDown()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        _editorService.CloseDropDown()
    End Sub

#End Region

End Class
