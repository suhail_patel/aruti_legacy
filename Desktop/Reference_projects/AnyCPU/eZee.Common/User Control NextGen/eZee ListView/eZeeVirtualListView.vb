Option Strict On

Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.ComponentModel
Imports System.Windows.Forms.Design
Imports System.Runtime.InteropServices
Imports System.Drawing.Design
Imports System.Windows.Forms

', System.Diagnostics.DebuggerStepThrough()
<System.Serializable()> _
Public Class eZeeVirtualListView
    Inherits eZeeBaseListView

#Region " Private Variables "

#Region " Proeperty Variables "

    Private cpHeader As New ContextMenuStrip
    Private chkColumnList As ColumnShowHideEditor
    Private mstrNecessaryColumns As String = ""
    Private mstrVisibleColumnList As String = ""
    'Private mstrCheckedItems As String = ""
    Private m_CheckedItems As New List(Of String)
    Private mintMinColumnWidth As Integer = 50

    Private marrItems As ListViewItem() = Nothing

#End Region

#Region " Control Variables "

    Private _headerRect As Rectangle

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

#End Region

#End Region

#Region " Constructor "

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyBase.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

        'Call CreateHeaderItems()


        Me.View = Windows.Forms.View.Details
        Me.MultiSelect = False
        Me.HideSelection = False
        Me.FullRowSelect = True
        Me.GridLines = True
        Me.VirtualMode = True
        Me.OwnerDraw = True

        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
    End Sub

    Public Sub New()
        Me.View = Windows.Forms.View.Details
        Me.MultiSelect = False
        Me.HideSelection = False
        Me.FullRowSelect = True
        Me.GridLines = True
        Me.VirtualMode = True
        Me.OwnerDraw = True

        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
    End Sub

#End Region

#Region " Properties "

    <Browsable(False)> _
    Public ReadOnly Property CheckedItemIdArray() As String()
        Get
            'Dim strIds As String() = {}
            'If mstrCheckedItems.Length > 0 Then
            '    If mstrCheckedItems.Substring(mstrCheckedItems.Length - 1, 1) = "," Then
            '        mstrCheckedItems = mstrCheckedItems.Remove(mstrCheckedItems.LastIndexOf(","), 1)
            '    End If
            '    strIds = mstrCheckedItems.Split(CChar(","))
            'End If
            'Return strIds
            Return m_CheckedItems.ToArray
        End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property CheckedItemIdsString() As String
        Get
            'If mstrCheckedItems.Length > 0 Then
            '    If mstrCheckedItems.Substring(mstrCheckedItems.Length - 1, 1) = "," Then
            '        mstrCheckedItems = mstrCheckedItems.Remove(mstrCheckedItems.LastIndexOf(","), 1)
            '    End If
            '    Return mstrCheckedItems
            'Else
            '    Return ""
            'End If
            Return String.Join(",", m_CheckedItems.ToArray)
        End Get
    End Property

    Public Property LVItems() As ListViewItem()
        Get
            Return marrItems
        End Get
        Set(ByVal value As ListViewItem())
            marrItems = value
        End Set
    End Property

#End Region

#Region " Private Methods "

#End Region

#Region " Public Methods "

    Public Sub CheckItem(ByVal lvItem As ListViewItem)
        Dim value As String = lvItem.Index.ToString
        If lvItem.Checked Then
            'mstrCheckedItems &= " " + lvItem.Index.ToString + ","
            If Not m_CheckedItems.Contains(value) Then
                m_CheckedItems.Add(value)
            End If
        Else
            'If mstrCheckedItems.Length > 0 Then
            '    mstrCheckedItems = mstrCheckedItems.Replace(" " + lvItem.Index.ToString + ",", "")
            '    mstrCheckedItems = mstrCheckedItems.TrimEnd
            'End If
            If m_CheckedItems.Contains(value) Then
                m_CheckedItems.Remove(value)
            End If
        End If
        Me.Invalidate(lvItem.Bounds)
    End Sub

    Public Sub Reset()
        'mstrCheckedItems = ""
        m_CheckedItems.Clear()
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnItemChecked(ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        MyBase.OnItemChecked(e)
        If Me.Items.Count < 1 Then Exit Sub

        If Not Me.CheckBoxes Then Exit Sub

        Call CheckItem(e.Item)
    End Sub

    Protected Overrides Sub OnMouseClick(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseClick(e)

        If Me.Items.Count < 1 Then Exit Sub

        If Not Me.CheckBoxes Then Exit Sub

        Dim lvi As ListViewItem = Me.GetItemAt(e.X, e.Y)
        If lvi IsNot Nothing Then
            If e.X < (lvi.Bounds.Left + 16) Then
                lvi.Checked = Not lvi.Checked
                Call CheckItem(lvi)
            End If
        End If
    End Sub

    Protected Overrides Sub OnMouseDoubleClick(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDoubleClick(e)

        If Not Me.CheckBoxes Then Exit Sub
        If Not Me.OwnerDraw Then Exit Sub

        Dim lvi As ListViewItem = Me.GetItemAt(e.X, e.Y)
        If lvi IsNot Nothing Then
            Call CheckItem(lvi)
        End If
    End Sub

    Protected Overrides Sub OnItemDrag(ByVal e As System.Windows.Forms.ItemDragEventArgs)
        MyBase.OnItemDrag(e)
        If e.Button = Windows.Forms.MouseButtons.Left Then
            Me.DoDragDrop(e.Item, CType(DragDropEffects.Move, DragDropEffects))
        End If
    End Sub

    Protected Overrides Sub OnDrawItem(ByVal e As System.Windows.Forms.DrawListViewItemEventArgs)
        MyBase.OnDrawItem(e)

        If Not Me.OwnerDraw Then Exit Sub
        If Not Me.CheckBoxes = True Then Exit Sub

        e.DrawDefault = True
        If Not e.Item.Checked Then
            e.Item.Checked = True
            e.Item.Checked = False
        End If
    End Sub

    Protected Overrides Sub OnDrawSubItem(ByVal e As System.Windows.Forms.DrawListViewSubItemEventArgs)
        MyBase.OnDrawSubItem(e)
        If Not Me.OwnerDraw Then Exit Sub
        e.DrawDefault = True
    End Sub

    Protected Overrides Sub OnDrawColumnHeader(ByVal e As System.Windows.Forms.DrawListViewColumnHeaderEventArgs)
        MyBase.OnDrawColumnHeader(e)

        If Not Me.OwnerDraw Then Exit Sub
        e.DrawDefault = True
    End Sub

    Protected Overrides Sub OnRetrieveVirtualItem(ByVal e As System.Windows.Forms.RetrieveVirtualItemEventArgs)
        MyBase.OnRetrieveVirtualItem(e)
        Try
            e.Item = Me.LVItems(e.ItemIndex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Overrides Sub OnKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.OnKeyUp(e)
        If e.KeyCode = Keys.Space Then
            Dim lvi As ListViewItem = Me.LVItems(Me.SelectedIndices.Item(0))
            If lvi IsNot Nothing Then
                lvi.Checked = Not lvi.Checked
                Call CheckItem(lvi)
            End If
        End If
    End Sub

#End Region

End Class



