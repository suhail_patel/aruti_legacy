Imports System
Imports System.Collections
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Drawing2D

''' <summary> 
''' Object representing 3D pie. 
''' </summary> 
Public Class PieSlice

    ''' <summary> 
    ''' Initializes an empty instance of <c>PieSlice</c>. 
    ''' </summary> 
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of flat <c>PieSlice</c> class with given 
    ''' bounds and visual style. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="surfaceColor"> 
    ''' Color used to paint the pie slice. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal startAngle As Single, ByVal sweepAngle As Single, _
    ByVal surfaceColor As Color)
        Me.New(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, 0.0F, startAngle, _
        sweepAngle, surfaceColor, ShadowStyle.NoShadow, EdgeColorType.NoEdge)
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieSlice</c> class with given 
    ''' bounds and visual style. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Height of the pie slice. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="surfaceColor"> 
    ''' Color used to paint the pie slice. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used for slice rendering. 
    ''' </param> 
    ''' <param name="edgeColorType"> 
    ''' Edge color style used for slice rendering. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal sliceHeight As Single, ByVal startAngle As Single, _
    ByVal sweepAngle As Single, ByVal surfaceColor As Color, ByVal shadowStyle As ShadowStyle, ByVal edgeColorType As EdgeColorType)
        Me.New()
        ' set some persistent values 
        m_actualStartAngle = startAngle
        m_actualSweepAngle = sweepAngle
        m_surfaceColor = surfaceColor
        m_shadowStyle = shadowStyle
        m_edgeColorType = edgeColorType
        ' create pens for rendering 
        Dim edgeLineColor As Color = EdgeColor.GetRenderingColor(edgeColorType, surfaceColor)
        m_pen = New Pen(edgeLineColor)
        m_pen.LineJoin = LineJoin.Round
        InitializePieSlice(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, sliceHeight)
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieSlice</c> class with given 
    ''' bounds and visual style. 
    ''' </summary> 
    ''' <param name="boundingRect"> 
    ''' Bounding rectangle used to draw the top surface of the slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Pie slice height. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="surfaceColor"> 
    ''' Color used to render pie slice surface. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used in rendering. 
    ''' </param> 
    ''' <param name="edgeColorType"> 
    ''' Edge color type used for rendering. 
    ''' </param> 
    Public Sub New(ByVal boundingRect As RectangleF, ByVal sliceHeight As Single, ByVal startAngle As Single, ByVal sweepAngle As Single, ByVal surfaceColor As Color, ByVal shadowStyle As ShadowStyle, _
    ByVal edgeColorType As EdgeColorType)
        Me.New(boundingRect.X, boundingRect.Y, boundingRect.Width, boundingRect.Height, sliceHeight, startAngle, _
        sweepAngle, surfaceColor, shadowStyle, edgeColorType)
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieSlice</c> class with given 
    ''' bounds and visual style. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Height of the pie slice. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="surfaceColor"> 
    ''' Color used to render pie slice surface. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used in rendering. 
    ''' </param> 
    ''' <param name="edgeColorType"> 
    ''' Edge color type used for rendering. 
    ''' </param> 
    ''' <param name="edgeLineWidth"> 
    ''' Edge line width. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal sliceHeight As Single, ByVal startAngle As Single, _
    ByVal sweepAngle As Single, ByVal surfaceColor As Color, ByVal shadowStyle As ShadowStyle, ByVal edgeColorType As EdgeColorType, ByVal edgeLineWidth As Single)
        Me.New(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, sliceHeight, startAngle, _
        sweepAngle, surfaceColor, shadowStyle, edgeColorType)
        m_pen.Width = edgeLineWidth
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieSlice</c> class with given 
    ''' bounds and visual style. 
    ''' </summary> 
    ''' <param name="boundingRect"> 
    ''' Bounding rectangle used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Pie slice height. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle (in degrees) of the pie slice. 
    ''' </param> 
    ''' <param name="surfaceColor"> 
    ''' Color used to render pie slice surface. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used in rendering. 
    ''' </param> 
    ''' <param name="edgeColorType"> 
    ''' Edge color type used for rendering. 
    ''' </param> 
    ''' <param name="edgeLineWidth"> 
    ''' Edge line width. 
    ''' </param> 
    Public Sub New(ByVal boundingRect As Rectangle, ByVal sliceHeight As Single, ByVal startAngle As Single, ByVal sweepAngle As Single, ByVal surfaceColor As Color, ByVal shadowStyle As ShadowStyle, _
    ByVal edgeColorType As EdgeColorType, ByVal edgeLineWidth As Single)
        Me.New(boundingRect.X, boundingRect.Y, boundingRect.Width, boundingRect.Height, sliceHeight, startAngle, _
        sweepAngle, surfaceColor, shadowStyle, edgeColorType, edgeLineWidth)
    End Sub

    ''' <summary> 
    ''' <c>Finalize</c> implementation 
    ''' </summary> 
    Protected Overrides Sub Finalize()
        Try
            Dispose(False)
        Finally
            MyBase.Finalize()
        End Try
    End Sub

    ''' <summary> 
    ''' Implementation of <c>IDisposable</c> interface. 
    ''' </summary> 
    Public Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> 
    ''' Disposes of all resources used by <c>PieSlice</c> object. 
    ''' </summary> 
    ''' <param name="disposing"></param> 
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not m_disposed Then
            If disposing Then
                Debug.Assert(m_pen IsNot Nothing)
                m_pen.Dispose()
                DisposeBrushes()
                Debug.Assert(m_startSide IsNot Nothing)
                m_startSide.Dispose()
                Debug.Assert(m_endSide IsNot Nothing)
                m_endSide.Dispose()
            End If
            m_disposed = True
        End If
    End Sub

    ''' <summary> 
    ''' Implementation of ICloneable interface. 
    ''' </summary> 
    ''' <returns> 
    ''' A deep copy of this object. 
    ''' </returns> 
    Public Function Clone() As Object
        Return New PieSlice(BoundingRectangle, SliceHeight, StartAngle, SweepAngle, m_surfaceColor, m_shadowStyle, _
        m_edgeColorType)
    End Function

    ''' <summary> 
    ''' Gets starting angle (in degrees) of the pie slice. 
    ''' </summary> 
    Public ReadOnly Property StartAngle() As Single
        Get
            Return m_startAngle
        End Get
    End Property

    ''' <summary> 
    ''' Gets sweep angle (in degrees) of the pie slice. 
    ''' </summary> 
    Public ReadOnly Property SweepAngle() As Single
        Get
            Return m_sweepAngle
        End Get
    End Property

    ''' <summary> 
    ''' Gets ending angle (in degrees) of the pie slice. 
    ''' </summary> 
    Public ReadOnly Property EndAngle() As Single
        Get
            Return (m_startAngle + m_sweepAngle) Mod 360
        End Get
    End Property

    Public Property Text() As String
        Get
            Return m_text
        End Get
        Set(ByVal value As String)
            m_text = value
        End Set
    End Property

    ''' <summary> 
    ''' Draws the pie slice. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Public Sub Draw(ByVal graphics As Graphics)
        DrawBottom(graphics)
        DrawSides(graphics)
        DrawTop(graphics)
    End Sub

    ''' <summary> 
    ''' Checks if given pie slice contains given point. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if point given is contained within the slice. 
    ''' </returns> 
    Public Function Contains(ByVal point As PointF) As Boolean
        If PieSliceContainsPoint(point) Then
            Return True
        End If
        If PeripheryContainsPoint(point) Then
            Return True
        End If
        If m_startSide.Contains(point) Then
            Return True
        End If
        If m_endSide.Contains(point) Then
            Return True
        End If
        Return False
    End Function

    ''' <summary> 
    ''' Evaluates the point in the middle of the slice. 
    ''' </summary> 
    ''' <returns> 
    ''' <c>PointF</c> in the middle of the pie top. 
    ''' </returns> 
    Public Overridable Function GetTextPosition() As PointF
        If SweepAngle >= 180 Then
            Return PeripheralPoint(m_center.X, m_center.Y, m_boundingRectangle.Width / 3, m_boundingRectangle.Height / 3, GetActualAngle(StartAngle) + SweepAngle / 2)
        End If
        Dim x As Single = (m_pointStart.X + m_pointEnd.X) / 2
        Dim y As Single = (m_pointStart.Y + m_pointEnd.Y) / 2
        Dim angle As Single = CSng((Math.Atan2(y - m_center.Y, x - m_center.X) * 180 / Math.PI))
        Return PeripheralPoint(m_center.X, m_center.Y, m_boundingRectangle.Width / 3, m_boundingRectangle.Height / 3, GetActualAngle(angle))
    End Function

    ''' <summary> 
    ''' Gets or sets the bounding rectangle. 
    ''' </summary> 
    Friend Property BoundingRectangle() As RectangleF
        Get
            Return m_boundingRectangle
        End Get
        Set(ByVal value As RectangleF)
            m_boundingRectangle = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the slice height. 
    ''' </summary> 
    Friend Property SliceHeight() As Single
        Get
            Return m_sliceHeight
        End Get
        Set(ByVal value As Single)
            m_sliceHeight = value
        End Set
    End Property

    ''' <summary> 
    ''' Draws pie slice sides. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawSides(ByVal graphics As Graphics)
        DrawHiddenPeriphery(graphics)
        ' draw wegde sides 
        If StartAngle > 90 AndAlso StartAngle < 270 Then
            DrawEndSide(graphics)
            DrawStartSide(graphics)
        Else
            DrawStartSide(graphics)
            DrawEndSide(graphics)
        End If
        DrawVisiblePeriphery(graphics)
    End Sub

    ''' <summary> 
    ''' Splits a pie slice into two on the split angle. 
    ''' </summary> 
    ''' <param name="splitAngle"> 
    ''' Angle at which splitting is performed. 
    ''' </param> 
    ''' <returns> 
    ''' An array of two pie slices. 
    ''' </returns> 
    Friend Function Split(ByVal splitAngle As Single) As PieSlice()
        ' if split angle equals one of bounding angles, then nothing to split 
        If StartAngle = splitAngle OrElse Me.EndAngle = splitAngle Then
            Return New PieSlice() {DirectCast(Clone(), PieSlice)}
        End If

        Dim transformedSplitAngle As Single = TransformAngle(splitAngle)

        Dim actualStartAngle As Single = GetActualAngle(StartAngle)
        Dim newSweepAngle As Single = (splitAngle - actualStartAngle + 360) Mod 360
        Dim pieSlice1 As New PieSlice(BoundingRectangle, SliceHeight, actualStartAngle, newSweepAngle, m_surfaceColor, m_shadowStyle, _
        m_edgeColorType)
        pieSlice1.InitializeSides(True, False)

        newSweepAngle = GetActualAngle(EndAngle) - splitAngle
        Dim pieSlice2 As New PieSlice(BoundingRectangle, SliceHeight, splitAngle, newSweepAngle, m_surfaceColor, m_shadowStyle, _
        m_edgeColorType)
        pieSlice2.InitializeSides(False, True)
        Return New PieSlice() {pieSlice1, pieSlice2}
    End Function

    ''' <summary> 
    ''' Reajusts the pie slice to fit new bounding rectangle provided. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Height of the pie slice. 
    ''' </param> 
    Friend Sub Readjust(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal sliceHeight As Single)
        InitializePieSlice(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, sliceHeight)
    End Sub

    ''' <summary> 
    ''' Draws visible start side. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawStartSide(ByVal graphics As Graphics)
        If m_startSide IsNot Nothing Then
            ' checks if the side is visible 
            If StartAngle > 90 AndAlso StartAngle < 270 Then
                m_startSide.Draw(graphics, m_pen, m_brushStartSide)
            Else
                m_startSide.Draw(graphics, m_pen, m_brushSurface)
            End If
        End If
    End Sub

    ''' <summary> 
    ''' Draws visible end side. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawEndSide(ByVal graphics As Graphics)
        If m_endSide IsNot Nothing Then
            ' checks if the side is visible 
            If EndAngle > 90 AndAlso EndAngle < 270 Then
                m_endSide.Draw(graphics, m_pen, m_brushSurface)
            Else
                m_endSide.Draw(graphics, m_pen, m_brushEndSide)
            End If
        End If
    End Sub

    ''' <summary> 
    ''' Draws visible outer periphery of the pie slice. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawVisiblePeriphery(ByVal graphics As Graphics)
        Dim peripherySurfaceBounds As PeripherySurfaceBounds() = GetVisiblePeripherySurfaceBounds()
        For Each surfaceBounds As PeripherySurfaceBounds In peripherySurfaceBounds
            DrawCylinderSurfaceSection(graphics, m_pen, m_brushPeripherySurface, surfaceBounds.StartAngle, surfaceBounds.EndAngle, surfaceBounds.StartPoint, _
            surfaceBounds.EndPoint)
        Next
    End Sub

    ''' <summary> 
    ''' Draws hidden outer periphery of the pie slice. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawHiddenPeriphery(ByVal graphics As Graphics)
        Dim peripherySurfaceBounds As PeripherySurfaceBounds() = GetHiddenPeripherySurfaceBounds()
        For Each surfaceBounds As PeripherySurfaceBounds In peripherySurfaceBounds
            DrawCylinderSurfaceSection(graphics, m_pen, m_brushSurface, surfaceBounds.StartAngle, surfaceBounds.EndAngle, surfaceBounds.StartPoint, _
            surfaceBounds.EndPoint)
        Next
    End Sub

    ''' <summary> 
    ''' Draws the bottom of the pie slice. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawBottom(ByVal graphics As Graphics)
        graphics.FillPie(m_brushSurface, m_boundingRectangle.X, m_boundingRectangle.Y + m_sliceHeight, m_boundingRectangle.Width, m_boundingRectangle.Height, m_startAngle, _
        m_sweepAngle)
        graphics.DrawPie(m_pen, m_boundingRectangle.X, m_boundingRectangle.Y + m_sliceHeight, m_boundingRectangle.Width, m_boundingRectangle.Height, m_startAngle, _
        m_sweepAngle)
    End Sub

    ''' <summary> 
    ''' Draws the top of the pie slice. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw the pie slice. 
    ''' </param> 
    Friend Sub DrawTop(ByVal graphics As Graphics)
        graphics.FillPie(m_brushSurface, m_boundingRectangle.X, m_boundingRectangle.Y, m_boundingRectangle.Width, m_boundingRectangle.Height, m_startAngle, _
        m_sweepAngle)
        graphics.DrawPie(m_pen, m_boundingRectangle, m_startAngle, m_sweepAngle)
    End Sub

    ''' <summary> 
    ''' Calculates the smallest rectangle into which this pie slice fits. 
    ''' </summary> 
    ''' <returns> 
    ''' <c>RectangleF</c> into which this pie slice fits exactly. 
    ''' </returns> 
    Friend Function GetFittingRectangle() As RectangleF
        Dim boundingRectangle As New RectangleF(m_pointStart.X, m_pointStart.Y, 0, 0)
        If (m_startAngle = 0.0F) OrElse (m_startAngle + m_sweepAngle >= 360) Then
            GraphicsUtil.IncludePointX(boundingRectangle, m_boundingRectangle.Right)
        End If
        If (m_startAngle <= 90) AndAlso (m_startAngle + m_sweepAngle >= 90) OrElse (m_startAngle + m_sweepAngle >= 450) Then
            GraphicsUtil.IncludePointY(boundingRectangle, m_boundingRectangle.Bottom + SliceHeight)
        End If
        If (m_startAngle <= 180) AndAlso (m_startAngle + m_sweepAngle >= 180) OrElse (m_startAngle + m_sweepAngle >= 540) Then
            GraphicsUtil.IncludePointX(boundingRectangle, m_boundingRectangle.Left)
        End If
        If (m_startAngle <= 270) AndAlso (m_startAngle + m_sweepAngle >= 270) OrElse (m_startAngle + m_sweepAngle >= 630) Then
            GraphicsUtil.IncludePointY(boundingRectangle, m_boundingRectangle.Top)
        End If
        GraphicsUtil.IncludePoint(boundingRectangle, m_center)
        GraphicsUtil.IncludePoint(boundingRectangle, m_centerBelow)
        GraphicsUtil.IncludePoint(boundingRectangle, m_pointStart)
        GraphicsUtil.IncludePoint(boundingRectangle, m_pointStartBelow)
        GraphicsUtil.IncludePoint(boundingRectangle, m_pointEnd)
        GraphicsUtil.IncludePoint(boundingRectangle, m_pointEndBelow)
        Return boundingRectangle
    End Function

    ''' <summary> 
    ''' Checks if given point is contained inside the pie slice. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check for. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if given point is inside the pie slice. 
    ''' </returns> 
    Friend Function PieSliceContainsPoint(ByVal point As PointF) As Boolean
        Return PieSliceContainsPoint(point, m_boundingRectangle.X, m_boundingRectangle.Y, m_boundingRectangle.Width, m_boundingRectangle.Height, m_startAngle, _
        m_sweepAngle)
    End Function

    ''' <summary> 
    ''' Checks if given point is contained by cylinder periphery. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check for. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if given point is inside the cylinder periphery. 
    ''' </returns> 
    Friend Function PeripheryContainsPoint(ByVal point As PointF) As Boolean
        Dim peripherySurfaceBounds As PeripherySurfaceBounds() = GetVisiblePeripherySurfaceBounds()
        For Each surfaceBounds As PeripherySurfaceBounds In peripherySurfaceBounds
            If CylinderSurfaceSectionContainsPoint(point, surfaceBounds.StartAngle, surfaceBounds.EndAngle, surfaceBounds.StartPoint, surfaceBounds.EndPoint) Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary> 
    ''' Checks if point provided is inside pie slice start cut side. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if point is inside the start side. 
    ''' </returns> 
    Friend Function StartSideContainsPoint(ByVal point As PointF) As Boolean
        If m_sliceHeight > 0 Then
            Return (m_startSide.Contains(point))
        End If
        Return False
    End Function

    ''' <summary> 
    ''' Checks if point provided is inside pie slice end cut side. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if point is inside the end side. 
    ''' </returns> 
    Friend Function EndSideContainsPoint(ByVal point As PointF) As Boolean
        If m_sliceHeight > 0 Then
            Return (m_endSide.Contains(point))
        End If
        Return False
    End Function

    ''' <summary> 
    ''' Checks if bottom side of the pie slice contains the point. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if point is inside the bottom of the pie slice. 
    ''' </returns> 
    Friend Function BottomSurfaceSectionContainsPoint(ByVal point As PointF) As Boolean
        If m_sliceHeight > 0 Then
            Return (PieSliceContainsPoint(point, m_boundingRectangle.X, m_boundingRectangle.Y + m_sliceHeight, m_boundingRectangle.Width, m_boundingRectangle.Height, m_startAngle, _
            m_sweepAngle))
        End If
        Return False
    End Function

    ''' <summary> 
    ''' Creates brushes used to render the pie slice. 
    ''' </summary> 
    ''' <param name="surfaceColor"> 
    ''' Color used for rendering. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used for rendering. 
    ''' </param> 
    Protected Overridable Sub CreateSurfaceBrushes(ByVal surfaceColor As Color, ByVal shadowStyle As ShadowStyle)
        m_brushSurface = New SolidBrush(surfaceColor)
        m_brushSurfaceHighlighted = New SolidBrush(ColorUtil.CreateColorWithCorrectedLightness(surfaceColor, ColorUtil.BrightnessEnhancementFactor1))
        Select Case shadowStyle
            Case shadowStyle.NoShadow
                m_brushStartSide = New SolidBrush(surfaceColor)
                m_brushEndSide = New SolidBrush(surfaceColor)
                m_brushPeripherySurface = New SolidBrush(surfaceColor)
                Exit Select
            Case shadowStyle.UniformShadow
                m_brushStartSide = New SolidBrush(ColorUtil.CreateColorWithCorrectedLightness(surfaceColor, -ColorUtil.BrightnessEnhancementFactor1))
                m_brushEndSide = New SolidBrush(ColorUtil.CreateColorWithCorrectedLightness(surfaceColor, -ColorUtil.BrightnessEnhancementFactor1))
                m_brushPeripherySurface = New SolidBrush(ColorUtil.CreateColorWithCorrectedLightness(surfaceColor, -ColorUtil.BrightnessEnhancementFactor1))
                Exit Select
            Case shadowStyle.GradualShadow
                Dim angle As Double = m_startAngle - 180 - s_shadowAngle
                If angle < 0 Then
                    angle += 360
                End If
                m_brushStartSide = CreateBrushForSide(surfaceColor, angle)
                angle = m_startAngle + m_sweepAngle - s_shadowAngle
                If angle < 0 Then
                    angle += 360
                End If
                m_brushEndSide = CreateBrushForSide(surfaceColor, angle)
                m_brushPeripherySurface = CreateBrushForPeriphery(surfaceColor)
                Exit Select
        End Select
    End Sub

    ''' <summary> 
    ''' Disposes brush objects. 
    ''' </summary> 
    Protected Sub DisposeBrushes()
        Debug.Assert(m_brushSurface IsNot Nothing)
        Debug.Assert(m_brushStartSide IsNot Nothing)
        Debug.Assert(m_brushEndSide IsNot Nothing)
        Debug.Assert(m_brushPeripherySurface IsNot Nothing)
        Debug.Assert(m_brushSurfaceHighlighted IsNot Nothing)

        m_brushSurface.Dispose()
        m_brushStartSide.Dispose()
        m_brushEndSide.Dispose()
        m_brushPeripherySurface.Dispose()
        m_brushSurfaceHighlighted.Dispose()
    End Sub

    ''' <summary> 
    ''' Creates a brush for start and end sides of the pie slice for 
    ''' gradual shade. 
    ''' </summary> 
    ''' <param name="color"> 
    ''' Color used for pie slice rendering. 
    ''' </param> 
    ''' <param name="angle"> 
    ''' Angle of the surface. 
    ''' </param> 
    ''' <returns> 
    ''' <c>Brush</c> object. 
    ''' </returns> 
    Protected Overridable Function CreateBrushForSide(ByVal color As Color, ByVal angle As Double) As Brush
        Return New SolidBrush(ColorUtil.CreateColorWithCorrectedLightness(color, -CSng((ColorUtil.BrightnessEnhancementFactor1 * (1 - 0.8 * Math.Cos(angle * Math.PI / 180))))))
    End Function

    ''' <summary> 
    ''' Creates a brush for outer periphery of the pie slice used for 
    ''' gradual shadow. 
    ''' </summary> 
    ''' <param name="color"> 
    ''' Color used for pie slice rendering. 
    ''' </param> 
    ''' <returns> 
    ''' <c>Brush</c> object. 
    ''' </returns> 
    Protected Overridable Function CreateBrushForPeriphery(ByVal color As Color) As Brush
        Dim colorBlend As New ColorBlend()
        colorBlend.Colors = New Color() {ColorUtil.CreateColorWithCorrectedLightness(color, -ColorUtil.BrightnessEnhancementFactor1 / 2), color, ColorUtil.CreateColorWithCorrectedLightness(color, -ColorUtil.BrightnessEnhancementFactor1)}
        colorBlend.Positions = New Single() {0.0F, 0.1F, 1.0F}
        Dim brush As New LinearGradientBrush(m_boundingRectangle, Drawing.Color.Blue, Drawing.Color.White, LinearGradientMode.Horizontal)
        brush.InterpolationColors = colorBlend
        Return brush
    End Function

    ''' <summary> 
    ''' Draws the outer periphery of the pie slice. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> object used to draw the surface. 
    ''' </param> 
    ''' <param name="pen"> 
    ''' <c>Pen</c> used to draw outline. 
    ''' </param> 
    ''' <param name="brush"> 
    ''' <c>Brush</c> used to fill the quadrilateral. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Start angle (in degrees) of the periphery section. 
    ''' </param> 
    ''' <param name="endAngle"> 
    ''' End angle (in degrees) of the periphery section. 
    ''' </param> 
    ''' <param name="pointStart"> 
    ''' Point representing the start of the periphery. 
    ''' </param> 
    ''' <param name="pointEnd"> 
    ''' Point representing the end of the periphery. 
    ''' </param> 
    Protected Sub DrawCylinderSurfaceSection(ByVal graphics As Graphics, ByVal pen As Pen, ByVal brush As Brush, ByVal startAngle As Single, ByVal endAngle As Single, ByVal pointStart As PointF, _
    ByVal pointEnd As PointF)
        Dim path As GraphicsPath = CreatePathForCylinderSurfaceSection(startAngle, endAngle, pointStart, pointEnd)
        graphics.FillPath(brush, path)
        graphics.DrawPath(pen, path)
    End Sub

    ''' <summary> 
    ''' Transforms actual angle to angle used for rendering. They are 
    ''' different because of perspective. 
    ''' </summary> 
    ''' <param name="angle"> 
    ''' Actual angle. 
    ''' </param> 
    ''' <returns> 
    ''' Rendering angle. 
    ''' </returns> 
    Protected Function TransformAngle(ByVal angle As Single) As Single
        Dim x As Double = m_boundingRectangle.Width * Math.Cos(angle * Math.PI / 180)
        Dim y As Double = m_boundingRectangle.Height * Math.Sin(angle * Math.PI / 180)
        Dim result As Single = CSng((Math.Atan2(y, x) * 180 / Math.PI))
        If result < 0 Then
            Return result + 360
        End If
        Return result
    End Function

    ''' <summary> 
    ''' Gets the actual angle from the rendering angle. 
    ''' </summary> 
    ''' <param name="transformedAngle"> 
    ''' Transformed angle for which actual angle has to be evaluated. 
    ''' </param> 
    ''' <returns> 
    ''' Actual angle. 
    ''' </returns> 
    Protected Function GetActualAngle(ByVal transformedAngle As Single) As Single
        Dim x As Double = m_boundingRectangle.Height * Math.Cos(transformedAngle * Math.PI / 180)
        Dim y As Double = m_boundingRectangle.Width * Math.Sin(transformedAngle * Math.PI / 180)
        Dim result As Single = CSng((Math.Atan2(y, x) * 180 / Math.PI))
        If result < 0 Then
            Return result + 360
        End If
        Return result
    End Function

    ''' <summary> 
    ''' Calculates the point on ellipse periphery for angle. 
    ''' </summary> 
    ''' <param name="xCenter"> 
    ''' x-coordinate of the center of the ellipse. 
    ''' </param> 
    ''' <param name="yCenter"> 
    ''' y-coordinate of the center of the ellipse. 
    ''' </param> 
    ''' <param name="semiMajor"> 
    ''' Horizontal semi-axis. 
    ''' </param> 
    ''' <param name="semiMinor"> 
    ''' Vertical semi-axis. 
    ''' </param> 
    ''' <param name="angleDegrees"> 
    ''' Angle (in degrees) for which corresponding periphery point has to 
    ''' be obtained. 
    ''' </param> 
    ''' <returns> 
    ''' <c>PointF</c> on the ellipse. 
    ''' </returns> 
    Protected Function PeripheralPoint(ByVal xCenter As Single, ByVal yCenter As Single, ByVal semiMajor As Single, ByVal semiMinor As Single, ByVal angleDegrees As Single) As PointF
        Dim angleRadians As Double = angleDegrees * Math.PI / 180
        Return New PointF(xCenter + CSng((semiMajor * Math.Cos(angleRadians))), yCenter + CSng((semiMinor * Math.Sin(angleRadians))))
    End Function

    ''' <summary> 
    ''' Initializes pie bounding rectangle, pie height, corners 
    ''' coordinates and brushes used for rendering. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the pie slice. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the pie slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Height of the pie slice. 
    ''' </param> 
    Private Sub InitializePieSlice(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal sliceHeight As Single)
        ' stores bounding rectangle and pie slice height 
        m_boundingRectangle = New RectangleF(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect)
        m_sliceHeight = sliceHeight
        ' recalculates start and sweep angle used for rendering 
        m_startAngle = TransformAngle(m_actualStartAngle)
        m_sweepAngle = m_actualSweepAngle
        If m_sweepAngle Mod 180 <> 0.0F Then
            m_sweepAngle = TransformAngle(m_actualStartAngle + m_actualSweepAngle) - m_startAngle
        End If
        If m_sweepAngle < 0 Then
            m_sweepAngle += 360
        End If
        ' creates brushes 
        CreateSurfaceBrushes(m_surfaceColor, m_shadowStyle)
        ' calculates center and end points on periphery 
        Dim xCenter As Single = xBoundingRect + widthBoundingRect / 2
        Dim yCenter As Single = yBoundingRect + heightBoundingRect / 2
        m_center = New PointF(xCenter, yCenter)
        m_centerBelow = New PointF(xCenter, yCenter + sliceHeight)
        m_pointStart = PeripheralPoint(xCenter, yCenter, widthBoundingRect / 2, heightBoundingRect / 2, m_actualStartAngle)
        m_pointStartBelow = New PointF(m_pointStart.X, m_pointStart.Y + sliceHeight)
        m_pointEnd = PeripheralPoint(xCenter, yCenter, widthBoundingRect / 2, heightBoundingRect / 2, m_actualStartAngle + m_actualSweepAngle)
        m_pointEndBelow = New PointF(m_pointEnd.X, m_pointEnd.Y + sliceHeight)
        InitializeSides()
    End Sub

    ''' <summary> 
    ''' Initializes start and end pie slice sides. 
    ''' </summary> 
    Private Sub InitializeSides()
        InitializeSides(True, True)
    End Sub

    ''' <summary> 
    ''' Initializes start and end pie slice sides. 
    ''' </summary> 
    ''' <param name="startSideExists"> 
    ''' Does start side exists. 
    ''' </param> 
    ''' <param name="endSideExists"> 
    ''' Does end side exists. 
    ''' </param> 
    Private Sub InitializeSides(ByVal startSideExists As Boolean, ByVal endSideExists As Boolean)
        If startSideExists Then
            m_startSide = New Quadrilateral(m_center, m_pointStart, m_pointStartBelow, m_centerBelow, m_sweepAngle <> 180)
        Else
            m_startSide = Quadrilateral.Empty
        End If
        If endSideExists Then
            m_endSide = New Quadrilateral(m_center, m_pointEnd, m_pointEndBelow, m_centerBelow, m_sweepAngle <> 180)
        Else
            m_endSide = Quadrilateral.Empty
        End If
    End Sub

    ''' <summary> 
    ''' Gets an array of visible periphery bounds. 
    ''' </summary> 
    ''' <returns> 
    ''' Array of <c>PeripherySurfaceBounds</c> objects. 
    ''' </returns> 
    Private Function GetVisiblePeripherySurfaceBounds() As PeripherySurfaceBounds()
        Dim peripherySurfaceBounds As New ArrayList()
        ' outer periphery side is visible only when startAngle or endAngle 
        ' is between 0 and 180 degrees 
        If Not (m_sweepAngle = 0 OrElse (m_startAngle >= 180 AndAlso m_startAngle + m_sweepAngle <= 360)) Then
            ' draws the periphery from start angle to the end angle or left 
            ' edge, whichever comes first 
            If StartAngle < 180 Then
                Dim fi1 As Single = m_startAngle
                Dim x1 As New PointF(m_pointStart.X, m_pointStart.Y)
                Dim fi2 As Single = EndAngle
                Dim x2 As New PointF(m_pointEnd.X, m_pointEnd.Y)
                If m_startAngle + m_sweepAngle > 180 Then
                    fi2 = 180
                    x2.X = m_boundingRectangle.X
                    x2.Y = m_center.Y
                End If
                peripherySurfaceBounds.Add(New PeripherySurfaceBounds(fi1, fi2, x1, x2))
            End If
            ' if lateral surface is visible from the right edge 
            If m_startAngle + m_sweepAngle > 360 Then
                Dim fi1 As Single = 0
                Dim x1 As New PointF(m_boundingRectangle.Right, m_center.Y)
                Dim fi2 As Single = EndAngle
                Dim x2 As New PointF(m_pointEnd.X, m_pointEnd.Y)
                If fi2 > 180 Then
                    fi2 = 180
                    x2.X = m_boundingRectangle.Left
                    x2.Y = m_center.Y
                End If
                peripherySurfaceBounds.Add(New PeripherySurfaceBounds(fi1, fi2, x1, x2))
            End If
        End If
        Return DirectCast(peripherySurfaceBounds.ToArray(GetType(PeripherySurfaceBounds)), PeripherySurfaceBounds())
    End Function

    ''' <summary> 
    ''' Gets an array of hidden periphery bounds. 
    ''' </summary> 
    ''' <returns> 
    ''' Array of <c>PeripherySurfaceBounds</c> objects. 
    ''' </returns> 
    Private Function GetHiddenPeripherySurfaceBounds() As PeripherySurfaceBounds()
        Dim peripherySurfaceBounds As New ArrayList()
        ' outer periphery side is not visible when startAngle or endAngle 
        ' is between 180 and 360 degrees 
        If Not (m_sweepAngle = 0 OrElse (m_startAngle >= 0 AndAlso m_startAngle + m_sweepAngle <= 180)) Then
            ' draws the periphery from start angle to the end angle or right 
            ' edge, whichever comes first 
            If m_startAngle + m_sweepAngle > 180 Then
                Dim fi1 As Single = m_startAngle
                Dim x1 As New PointF(m_pointStart.X, m_pointStart.Y)
                Dim fi2 As Single = m_startAngle + m_sweepAngle
                Dim x2 As New PointF(m_pointEnd.X, m_pointEnd.Y)
                If fi1 < 180 Then
                    fi1 = 180
                    x1.X = m_boundingRectangle.Left
                    x1.Y = m_center.Y
                End If
                If fi2 > 360 Then
                    fi2 = 360
                    x2.X = m_boundingRectangle.Right
                    x2.Y = m_center.Y
                End If
                peripherySurfaceBounds.Add(New PeripherySurfaceBounds(fi1, fi2, x1, x2))
                ' if pie is crossing 360 & 180 deg. boundary, we have to 
                ' invisible peripheries 
                If m_startAngle < 360 AndAlso m_startAngle + m_sweepAngle > 540 Then
                    fi1 = 180
                    x1 = New PointF(m_boundingRectangle.Left, m_center.Y)
                    fi2 = EndAngle
                    x2 = New PointF(m_pointEnd.X, m_pointEnd.Y)
                    peripherySurfaceBounds.Add(New PeripherySurfaceBounds(fi1, fi2, x1, x2))
                End If
            End If
        End If
        Return DirectCast(peripherySurfaceBounds.ToArray(GetType(PeripherySurfaceBounds)), PeripherySurfaceBounds())
    End Function

    ''' <summary> 
    ''' Creates <c>GraphicsPath</c> for cylinder surface section. This 
    ''' path consists of two arcs and two vertical lines. 
    ''' </summary> 
    ''' <param name="startAngle"> 
    ''' Starting angle of the surface. 
    ''' </param> 
    ''' <param name="endAngle"> 
    ''' Ending angle of the surface. 
    ''' </param> 
    ''' <param name="pointStart"> 
    ''' Starting point on the cylinder surface. 
    ''' </param> 
    ''' <param name="pointEnd"> 
    ''' Ending point on the cylinder surface. 
    ''' </param> 
    ''' <returns> 
    ''' <c>GraphicsPath</c> object representing the cylinder surface. 
    ''' </returns> 
    Private Function CreatePathForCylinderSurfaceSection(ByVal startAngle As Single, ByVal endAngle As Single, ByVal pointStart As PointF, ByVal pointEnd As PointF) As GraphicsPath
        Dim path As New GraphicsPath()
        path.AddArc(m_boundingRectangle, startAngle, endAngle - startAngle)
        path.AddLine(pointEnd.X, pointEnd.Y, pointEnd.X, pointEnd.Y + m_sliceHeight)
        path.AddArc(m_boundingRectangle.X, m_boundingRectangle.Y + m_sliceHeight, m_boundingRectangle.Width, m_boundingRectangle.Height, endAngle, startAngle - endAngle)
        path.AddLine(pointStart.X, pointStart.Y + m_sliceHeight, pointStart.X, pointStart.Y)
        Return path
    End Function

    ''' <summary> 
    ''' Checks if given point is contained within upper and lower pie 
    ''' slice surfaces or within the outer slice brink. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> structure to check for. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Start angle of the slice. 
    ''' </param> 
    ''' <param name="endAngle"> 
    ''' End angle of the slice. 
    ''' </param> 
    ''' <param name="point1"> 
    ''' Starting point on the periphery. 
    ''' </param> 
    ''' <param name="point2"> 
    ''' Ending point on the periphery. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if point given is contained. 
    ''' </returns> 
    Private Function CylinderSurfaceSectionContainsPoint(ByVal point As PointF, ByVal startAngle As Single, ByVal endAngle As Single, ByVal point1 As PointF, ByVal point2 As PointF) As Boolean
        If m_sliceHeight > 0 Then
            Return Quadrilateral.Contains(point, New PointF() {point1, New PointF(point1.X, point1.Y + m_sliceHeight), New PointF(point2.X, point2.Y + m_sliceHeight), point2})
        End If
        Return False
    End Function

    ''' <summary> 
    ''' Checks if point given is contained within the pie slice. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check for. 
    ''' </param> 
    ''' <param name="xBoundingRectangle"> 
    ''' x-coordinate of the rectangle that bounds the ellipse from which 
    ''' slice is cut. 
    ''' </param> 
    ''' <param name="yBoundingRectangle"> 
    ''' y-coordinate of the rectangle that bounds the ellipse from which 
    ''' slice is cut. 
    ''' </param> 
    ''' <param name="widthBoundingRectangle"> 
    ''' Width of the rectangle that bounds the ellipse from which 
    ''' slice is cut. 
    ''' </param> 
    ''' <param name="heightBoundingRectangle"> 
    ''' Height of the rectangle that bounds the ellipse from which 
    ''' slice is cut. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Start angle of the slice. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle of the slice. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if point is contained within the slice. 
    ''' </returns> 
    Private Function PieSliceContainsPoint(ByVal point As PointF, ByVal xBoundingRectangle As Single, ByVal yBoundingRectangle As Single, ByVal widthBoundingRectangle As Single, ByVal heightBoundingRectangle As Single, ByVal startAngle As Single, _
    ByVal sweepAngle As Single) As Boolean
        Dim x As Double = point.X - xBoundingRectangle - widthBoundingRectangle / 2
        Dim y As Double = point.Y - yBoundingRectangle - heightBoundingRectangle / 2
        Dim angle As Double = Math.Atan2(y, x)
        If angle < 0 Then
            angle += (2 * Math.PI)
        End If
        Dim angleDegrees As Double = angle * 180 / Math.PI
        ' point is inside the pie slice only if between start and end angle 
        If (angleDegrees >= startAngle AndAlso angleDegrees <= (startAngle + sweepAngle)) OrElse (startAngle + sweepAngle > 360) AndAlso ((angleDegrees + 360) <= (startAngle + sweepAngle)) Then
            ' distance of the point from the ellipse centre 
            Dim r As Double = Math.Sqrt(y * y + x * x)
            Return GetEllipseRadius(angle) > r
        End If
        Return False
    End Function

    ''' <summary> 
    ''' Evaluates the distance of an ellipse perimeter point for a 
    ''' given angle. 
    ''' </summary> 
    ''' <param name="angle"> 
    ''' Angle for which distance has to be evaluated. 
    ''' </param> 
    ''' <returns> 
    ''' Distance of the point from the ellipse centre. 
    ''' </returns> 
    Private Function GetEllipseRadius(ByVal angle As Double) As Double
        Dim a As Double = m_boundingRectangle.Width / 2
        Dim b As Double = m_boundingRectangle.Height / 2
        Dim a2 As Double = a * a
        Dim b2 As Double = b * b
        Dim cosFi As Double = Math.Cos(angle)
        Dim sinFi As Double = Math.Sin(angle)
        ' distance of the ellipse perimeter point 
        Return (a * b) / Math.Sqrt(b2 * cosFi * cosFi + a2 * sinFi * sinFi)
    End Function

    ''' <summary> 
    ''' Internal structure used to store periphery bounds data. 
    ''' </summary> 
    Private Structure PeripherySurfaceBounds
        Public Sub New(ByVal startAngle As Single, ByVal endAngle As Single, ByVal startPoint As PointF, ByVal endPoint As PointF)
            m_startAngle = startAngle
            m_endAngle = endAngle
            m_startPoint = startPoint
            m_endPoint = endPoint
        End Sub

        Public ReadOnly Property StartAngle() As Single
            Get
                Return m_startAngle
            End Get
        End Property

        Public ReadOnly Property EndAngle() As Single
            Get
                Return m_endAngle
            End Get
        End Property

        Public ReadOnly Property StartPoint() As PointF
            Get
                Return m_startPoint
            End Get
        End Property

        Public ReadOnly Property EndPoint() As PointF
            Get
                Return m_endPoint
            End Get
        End Property

        Private m_startAngle As Single
        Private m_endAngle As Single
        Private m_startPoint As PointF
        Private m_endPoint As PointF
    End Structure

    ''' <summary> 
    ''' Bounding rectangle that bounds the ellipse from which pie slice 
    ''' is cut. 
    ''' </summary> 
    Protected m_boundingRectangle As RectangleF
    ''' <summary> 
    ''' Pie slice height. 
    ''' </summary> 
    Protected m_sliceHeight As Single
    ''' <summary> 
    ''' Start angle. 
    ''' </summary> 
    Protected m_startAngle As Single
    ''' <summary> 
    ''' Sweep angle. 
    ''' </summary> 
    Protected m_sweepAngle As Single
    ''' <summary> 
    ''' Actual start angle. 
    ''' </summary> 
    Private m_actualStartAngle As Single
    ''' <summary> 
    ''' Actual sweep angle. 
    ''' </summary> 
    Private m_actualSweepAngle As Single
    ''' <summary> 
    ''' Color of the surface. 
    ''' </summary> 
    Private m_surfaceColor As Color = Color.Empty
    ''' <summary> 
    ''' Style used for shadow. 
    ''' </summary> 
    Private m_shadowStyle As ShadowStyle = ShadowStyle.NoShadow
    ''' <summary> 
    ''' <c>EdgeColorType</c> used to draw pie sliece edges. 
    ''' </summary> 
    Private m_edgeColorType As EdgeColorType = EdgeColorType.NoEdge
    ''' <summary> 
    ''' <c>Brush</c> used to render slice top surface. 
    ''' </summary> 
    Protected m_brushSurface As Brush = Nothing
    ''' <summary> 
    ''' <c>Brush</c> used to render slice top surface when highlighted. 
    ''' </summary> 
    Protected m_brushSurfaceHighlighted As Brush = Nothing
    ''' <summary> 
    ''' <c>Brush</c> used to render slice starting cut side. 
    ''' </summary> 
    Protected m_brushStartSide As Brush = Nothing
    ''' <summary> 
    ''' <c>Brush</c> used to render slice ending cut side. 
    ''' </summary> 
    Protected m_brushEndSide As Brush = Nothing
    ''' <summary> 
    ''' <c>Brush</c> used to render pie slice periphery (cylinder outer surface). 
    ''' </summary> 
    Protected m_brushPeripherySurface As Brush = Nothing
    ''' <summary> 
    ''' <c>Pen</c> object used to draw pie slice edges. 
    ''' </summary> 
    Protected m_pen As Pen = Nothing
    ''' <summary> 
    ''' <c>PointF</c> corresponding to pie slice center. 
    ''' </summary> 
    Protected m_center As PointF
    ''' <summary> 
    ''' <c>PointF</c> corresponding to the lower pie slice center. 
    ''' </summary> 
    Protected m_centerBelow As PointF
    ''' <summary> 
    ''' <c>PointF</c> on the periphery corresponding to the start cut 
    ''' side. 
    ''' </summary> 
    Protected m_pointStart As PointF
    ''' <summary> 
    ''' <c>PointF</c> on the periphery corresponding to the start cut 
    ''' side. 
    ''' </summary> 
    Protected m_pointStartBelow As PointF
    ''' <summary> 
    ''' <c>PointF</c> on the periphery corresponding to the end cut 
    ''' side. 
    ''' </summary> 
    Protected m_pointEnd As PointF
    ''' <summary> 
    ''' <c>PointF</c> on the periphery corresponding to the end cut 
    ''' side. 
    ''' </summary> 
    Protected m_pointEndBelow As PointF
    ''' <summary> 
    ''' <c>Quadrilateral</c> representing the start side. 
    ''' </summary> 
    Protected m_startSide As Quadrilateral = Quadrilateral.Empty
    ''' <summary> 
    ''' <c>Quadrilateral</c> representing the end side. 
    ''' </summary> 
    Protected m_endSide As Quadrilateral = Quadrilateral.Empty
    ''' <summary> 
    ''' Text attached to the slice. 
    ''' </summary> 
    Private m_text As String
    ''' <summary> 
    ''' Flag indicating if object has been disposed. 
    ''' </summary> 
    Private m_disposed As Boolean = False
    ''' <summary> 
    ''' Angle offset used to define reference angle for gradual shadow. 
    ''' </summary> 
    Private Shared s_shadowAngle As Single = 20.0F
End Class
