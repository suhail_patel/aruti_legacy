
Public Class eZeeButtonGroup
    Inherits eZee.Common.eZeeLightButton

    Private m_intIndex As Integer = -1
    Private m_Owner As eZeeButtonListView
    Private m_items As eZeeButtonListView.eZeeButtonItemCollection
    Private Shared m_nextHeader As Integer
    Private Shared m_nextID As Integer
    Private m_userData As Object
    Private m_id As Integer
    'Private mblnCallCheckedChanged As Boolean = True

    Private Sub SetDefaultValues()
        'Me.Appearance = System.Windows.Forms.Appearance.Button
        'Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        'Me.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue
        'Me.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightSteelBlue
        'Me.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSlateGray
        'Me.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.Height = 25
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.BorderColor = System.Drawing.Color.Empty
        Me.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.FlatAppearance.BorderSize = 0
        Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GradientBackColor = System.Drawing.Color.Silver
        Me.GradientForeColor = System.Drawing.Color.Black
        Me.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    End Sub

    Public Sub New(ByVal key As String, ByVal headerText As String)
        Me.SetDefaultValues()
        Me.Name = key
        Me.Text = headerText
    End Sub

    Public Sub New()
        Me.SetDefaultValues()
    End Sub

    Public Sub New(ByVal header As String)
        Me.SetDefaultValues()
        Me.Text = header
    End Sub

    Public Property eZeeButtonListView() As eZeeButtonListView
        Get
            Return Me.m_Owner
        End Get
        Set(ByVal value As eZeeButtonListView)
            Me.m_Owner = value
        End Set
    End Property

    Public Property GroupID() As Integer
        Get
            Return Me.m_id
        End Get
        Set(ByVal value As Integer)
            Me.m_id = value
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property Items() As eZeeButtonListView.eZeeButtonItemCollection
        Get
            If (Me.m_items Is Nothing) Then
                Me.m_items = New eZeeButtonListView.eZeeButtonItemCollection(New eZeeButtonGroupItemCollection(Me))
            End If
            Return Me.m_items
        End Get
    End Property

    Public Property Index() As Integer
        Get
            Return m_intIndex
        End Get
        Set(ByVal value As Integer)
            m_intIndex = value
        End Set
    End Property

    Public ReadOnly Property HasItems() As Boolean
        Get
            Dim blnHasItems As Boolean = False
            For Each cntrl As Windows.Forms.Control In Me.m_Owner.Controls
                If TypeOf cntrl Is eZeeButtonItem Then
                    If CType(cntrl, eZeeButtonItem).GroupId = Me.GroupID Then
                        blnHasItems = True
                        Exit For
                    End If
                End If
            Next
            Return blnHasItems
        End Get
    End Property

    'Public Sub RemoveCheckedChangedHanlder()
    '    mblnCallCheckedChanged = False
    'End Sub

    'Public Sub AddCheckedChangedHanlder()
    '    mblnCallCheckedChanged = True
    'End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        MyBase.OnClick(e)
        'If (mblnCallCheckedChanged) Then
        If (Me.ButtonType = enButtonType.CheckButton) Then
            If (Me.Selected) Then
                'For Each cntrl As System.Windows.Forms.Control In Me.m_Owner.Controls
                '    If (TypeOf cntrl Is eZeeButtonItem) Then
                '        If (CType(cntrl, eZeeButtonItem).Group Is Me) Then
                '            cntrl.Visible = True
                '        End If
                '    End If
                'Next
                Me.m_Owner.Groups.Expand(Me.GroupID, True)
            Else
                'With Me.m_Owner
                '    For j As Integer = .Controls.Count - 1 To 0 Step -1
                '        If (TypeOf .Controls(j) Is eZeeButtonItem) Then
                '            If (CType(.Controls(j), eZeeButtonItem).Group Is Me) Then
                '                .Controls(j).Visible = False
                '            End If
                '        End If
                '    Next
                'End With
                Me.m_Owner.Groups.Collapse(Me.GroupID, True)
            End If
            'End If
        End If
        Me.m_Owner.RaiseGroupClickEvent(Me)
    End Sub

End Class
