<Serializable()> _
Public Class eZeeButtonListViewHeader

    Private mstrHeaderText As String = "Add New Item"
    Private mstrAddText As String = "Header Text"
    Private m_Owner As eZeeButtonListView = Nothing

    Public Property HeaderText() As String
        Get
            Return mstrHeaderText
        End Get
        Set(ByVal value As String)
            mstrHeaderText = value
            objlblHeaderText.Text = value
        End Set
    End Property

    Public Property AddText() As String
        Get
            Return mstrAddText
        End Get
        Set(ByVal value As String)
            mstrAddText = value
            objlblAddNew.Text = value
        End Set
    End Property

    Public Property Owner() As eZeeButtonListView
        Get
            Return m_Owner
        End Get
        Set(ByVal value As eZeeButtonListView)
            m_Owner = value
        End Set
    End Property


    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objlblAddNew.Text = "Add New Item"
        objlblHeaderText.Text = "Header Text"

        Me.Margin = New System.Windows.Forms.Padding(0, 0, 0, 3)
    End Sub

    Private Sub eZeeButtonListViewHeader_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        Dim g As System.Drawing.Graphics = e.Graphics

        '.DrawBorder(
        g.DrawLine(New System.Drawing.Pen(System.Windows.Forms.ControlPaint.Dark(Me.BackColor)), 0, Me.Height - 1, Me.Width, Me.Height - 1)
    End Sub

    Private Sub objlblAddNew_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles objlblAddNew.LinkClicked
        Me.m_Owner.RaiseLinkLabelClickEvent(sender, e)
    End Sub
End Class
