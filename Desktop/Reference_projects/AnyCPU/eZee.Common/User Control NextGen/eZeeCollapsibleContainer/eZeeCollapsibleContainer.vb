Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.ComponentModel
Imports System.Windows.Forms

<System.Serializable()> <System.ComponentModel.DefaultEvent("HeaderClick")> _
Public Class eZeeCollapsibleContainer
    Inherits Panel

#Region " Property Variables "

    Private mintHeaderHeight As Integer = 25
    Private mintNonHeaderHeight As Integer = 100
    Private mclrGradientColor As Color = SystemColors.ButtonFace
    Private mstrHeaderText As String = ""
    Private mintLeftHeaderSpace As Integer = 0
    Private mblnDrawBorder As Boolean = True
    Private mblnShowDefaultBorderColor As Boolean = True
    Private mclrBorderColor As Color = Color.Black
    Private menBorderStyle As ButtonBorderStyle = ButtonBorderStyle.Solid
    Private mintLastHeight As Integer = 300
    Private mblnShowCollapseButton As Boolean = False
    Private mblnCollapsed As Boolean = False
    Private mblnShowCollapsibleButton As Boolean = True
    Private mintHeightOnCollapse As Integer = 0
    Private mblnCollapseOnLoad As Boolean = False
    Private mblnShowCheckBox As Boolean = False
    Private mblnChecked As Boolean = False
    Private mblnShowDownButton As Boolean = False

    'Krushna (FD GUI) (14 Jul 2009) -- Start
    Private menTextAlign As ContentAlignment = ContentAlignment.MiddleLeft
    Private mblnShowHeader As Boolean = True
    'Krushna (FD GUI) (14 Jul 2009) -- End

    Private mimgCollapsedNormalImage As Image = Nothing
    Private mimgCollapsedHoverImage As Image = Nothing
    Private mimgCollapsedPressedImage As Image = Nothing
    Private mimgExpandedNormalImage As Image = Nothing
    Private mimgExpandedHoverImage As Image = Nothing
    Private mimgExpandedPressedImage As Image = Nothing

    'HRK - 19 Jul 2010 - Start
    Private mstrHeaderMessage As String = String.Empty
    Private mfntHeaderMessageFont As Font = Nothing
    Private mclrHeaderMessageForeColor As Color = Color.Black
    'HRK - 19 Jul 2010 - End

    Private mblnCollapseAllExceptThis As Boolean = False

#End Region

#Region " Control Declaration "

    Private WithEvents objlblHeaderText As Label
    Private WithEvents objimgCollapseButton As eZeeImageButton
    Private WithEvents objimgExpandButton As eZeeImageButton
    Private WithEvents objchkCheckBox As CheckBox
    Private WithEvents objbtnDownButton As Button

#End Region

#Region " Event Declaration "

    Public Event HeaderClick As HeaderClickEventHandler
    Public Event CheckedChanged As CheckedChangedEventHandler

#End Region

#Region " Contructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objlblHeaderText = New Label

        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.CacheText, True)
        SetStyle(ControlStyles.ResizeRedraw, True)

        'Me.Padding = New Padding(3, 3, 3, 3)

    End Sub

#End Region

#Region " Properties "

#Region " Browsable Properties "

    <Browsable(True)> _
    Public Property HeaderHeight() As Integer
        Get
            Return mintHeaderHeight
        End Get
        Set(ByVal value As Integer)
            If value < 25 Then
                MsgBox("Sorry, You can't set height less than 25.")
                Exit Property
            End If
            mintHeaderHeight = value
            If DesignMode Then
                Call SetControls()
            End If
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property GradientColor() As Color
        Get
            Return mclrGradientColor
        End Get
        Set(ByVal value As Color)
            mclrGradientColor = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property TextAlign() As ContentAlignment
        Get
            Return menTextAlign
        End Get
        Set(ByVal value As ContentAlignment)
            menTextAlign = value
            objlblHeaderText.TextAlign = menTextAlign
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Overrides Property Text() As String
        Get
            Return mstrHeaderText 'objlblHeaderText.Text
        End Get
        Set(ByVal value As String)
            mstrHeaderText = value
            objlblHeaderText.Text = mstrHeaderText
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowHeader() As Boolean
        Get
            Return mblnShowHeader
        End Get
        Set(ByVal value As Boolean)
            mblnShowHeader = value
            If DesignMode Then
                If value = True Then
                    Call OnCreateControl()
                Else
                    If objlblHeaderText IsNot Nothing Then Me.Controls.Remove(objlblHeaderText)
                    If objchkCheckBox IsNot Nothing Then Me.Controls.Remove(objchkCheckBox)
                    If objimgCollapseButton IsNot Nothing Then Me.Controls.Remove(objimgCollapseButton)
                    If objimgExpandButton IsNot Nothing Then Me.Controls.Remove(objimgExpandButton)
                    If objbtnDownButton IsNot Nothing Then Me.Controls.Remove(objbtnDownButton)
                End If
            End If
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property LeftTextSpace() As Integer
        Get
            Return mintLeftHeaderSpace
        End Get
        Set(ByVal value As Integer)
            mintLeftHeaderSpace = value
            If DesignMode = True Then
                Call SetControls()
            End If
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowBorder() As Boolean
        Get
            Return mblnDrawBorder
        End Get
        Set(ByVal value As Boolean)
            mblnDrawBorder = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowDefaultBorderColor() As Boolean
        Get
            Return mblnShowDefaultBorderColor
        End Get
        Set(ByVal value As Boolean)
            mblnShowDefaultBorderColor = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property BorderColor() As Color
        Get
            Return mclrBorderColor
        End Get
        Set(ByVal value As Color)
            mclrBorderColor = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property PanelBorderStyle() As ButtonBorderStyle
        Get
            Return menBorderStyle
        End Get
        Set(ByVal value As ButtonBorderStyle)
            menBorderStyle = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowCollapseButton() As Boolean
        Get
            Return mblnShowCollapseButton
        End Get
        Set(ByVal value As Boolean)
            mblnShowCollapseButton = value
            If DesignMode Then
                Call SetControls()
            End If
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowDownButton() As Boolean
        Get
            Return mblnShowDownButton
        End Get
        Set(ByVal value As Boolean)
            mblnShowDownButton = value
            If DesignMode Then
                Call SetControls()
            End If
        End Set
    End Property

    <Browsable(True)> _
    Public Property HeightOnCollapse() As Integer
        Get
            Return mintHeightOnCollapse
        End Get
        Set(ByVal value As Integer)
            mintHeightOnCollapse = value
        End Set
    End Property

    <Browsable(True), EditorBrowsable(EditorBrowsableState.Always)> _
    Public Property CollapseAllExceptThis() As Boolean
        Get
            Return mblnCollapseAllExceptThis
        End Get
        Set(ByVal value As Boolean)
            mblnCollapseAllExceptThis = value
        End Set
    End Property

    Private mintTemp As Integer
    Public Property Temp() As Integer
        Get
            Return mintTemp
        End Get
        Set(ByVal value As Integer)
            mintTemp = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Size() As Size
        Get
            Return MyBase.Size
        End Get
        Set(ByVal value As Size)
            MyBase.Size = value
            If DesignMode Then
                Call SetControls()
            End If
        End Set
    End Property

    <Browsable(True)> _
    Public Property CollapseOnLoad() As Boolean
        Get
            Return mblnCollapseOnLoad
        End Get
        Set(ByVal value As Boolean)
            mblnCollapseOnLoad = value
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowCheckBox() As Boolean
        Get
            Return mblnShowCheckBox
        End Get
        Set(ByVal value As Boolean)
            mblnShowCheckBox = value
            If DesignMode Then
                Call SetControls()
            End If
        End Set
    End Property

    <Browsable(True)> _
    Public Property Checked() As Boolean
        Get
            Return mblnChecked
        End Get
        Set(ByVal value As Boolean)
            mblnChecked = value
            If objchkCheckBox IsNot Nothing Then
                objchkCheckBox.Checked = value
            End If
        End Set
    End Property

    Public Property OpenHeight() As Integer
        Get
            Return mintLastHeight
        End Get
        Set(ByVal value As Integer)
            mintLastHeight = value
        End Set
    End Property

    'HRK - 19 Jul 2010 - Start
    Public Property HeaderMessage() As String
        Get
            Return mstrHeaderMessage
        End Get
        Set(ByVal value As String)
            mstrHeaderMessage = value
            Me.Refresh()
        End Set
    End Property

    Public Property HeaderMessageFont() As Font
        Get
            If mfntHeaderMessageFont Is Nothing Then
                mfntHeaderMessageFont = New Font(objlblHeaderText.Font.Name, objlblHeaderText.Font.SizeInPoints, FontStyle.Regular)
            End If
            Return mfntHeaderMessageFont
        End Get
        Set(ByVal value As Font)
            mfntHeaderMessageFont = value
        End Set
    End Property

    Public Property HeaderMessageForeColor() As Color
        Get
            Return mclrHeaderMessageForeColor
        End Get
        Set(ByVal value As Color)
            mclrHeaderMessageForeColor = value
        End Set
    End Property
    'HRK - 19 Jul 2010 - End

#End Region

#Region " NonBrowsable Properties "

    <Browsable(False)> _
        Public ReadOnly Property NonHeaderHeight() As Integer
        Get
            Return mintNonHeaderHeight
        End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property Collapsed() As Boolean
        Get
            If Me.Height = Me.HeaderHeight + Me.HeightOnCollapse Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    <Browsable(False)> _
    Public Property CollapsedNormalImage() As Image
        Get
            Return mimgCollapsedNormalImage
        End Get
        Set(ByVal value As Image)
            mimgCollapsedNormalImage = value
            'Call SetControls()
        End Set
    End Property

    <Browsable(False)> _
    Public Property CollapsedHoverImage() As Image
        Get
            Return mimgCollapsedHoverImage
        End Get
        Set(ByVal value As Image)
            mimgCollapsedHoverImage = value
            'Call SetControls()
        End Set
    End Property

    <Browsable(False)> _
    Public Property CollapsedPressedImage() As Image
        Get
            Return mimgCollapsedPressedImage
        End Get
        Set(ByVal value As Image)
            mimgCollapsedPressedImage = value
            'Call SetControls()
        End Set
    End Property

    <Browsable(False)> _
    Public Property ExpandedNormalImage() As Image
        Get
            Return mimgExpandedNormalImage
        End Get
        Set(ByVal value As Image)
            mimgExpandedNormalImage = value
            'Call SetControls()
        End Set
    End Property

    <Browsable(False)> _
    Public Property ExpandedHoverImage() As Image
        Get
            Return mimgExpandedHoverImage
        End Get
        Set(ByVal value As Image)
            mimgExpandedHoverImage = value
            'Call SetControls()
        End Set
    End Property

    <Browsable(False)> _
    Public Property ExpandedPressedImage() As Image
        Get
            Return mimgExpandedPressedImage
        End Get
        Set(ByVal value As Image)
            mimgExpandedPressedImage = value
            'Call SetControls()
        End Set
    End Property

#End Region
    
#End Region

#Region " Public Methods "

    Public Function GetPath(ByVal rc As Rectangle, ByVal r As Integer) As Drawing2D.GraphicsPath
        ' Build the path with the round corners in the rectangle 
        ' r is the radious of rounded corner. 
        Dim x As Integer = rc.X, y As Integer = rc.Y, w As Integer = rc.Width, h As Integer = rc.Height
        r = r << 1
        Dim path As New Drawing2D.GraphicsPath()
        If r > 0 Then
            ' If the radious of rounded corner is greater than one side then 
            ' do the side rounded 
            If r > h Then
                r = h
            End If

            'Rounded 
            If r > w Then
                r = w
            End If

            'Rounded 
            path.AddArc(x, y, r, r, 180, 90)
            'Upper left corner 
            path.AddArc(x + w - r, y, r, r, 270, 90)
            'Upper right corner 
            path.AddArc(x + w - r, y + h - r, r, r, 0, 90)
            'Lower right corner 
            path.AddArc(x, y + h - r, r, r, 90, 90)
            'Lower left corner 
            path.CloseFigure()

        Else
            ' If the radious of rounded corner is zero then the path is a rectangle 
            path.AddRectangle(rc)
        End If

        Return path
    End Function

    Public Sub DrawRoundRect(ByVal g As Graphics, _
                            ByVal p As Pen, _
                            ByVal x As Integer, _
                            ByVal y As Integer, _
                            ByVal width As Integer, _
                            ByVal height As Integer, _
                            ByVal radius As Integer)

        Dim gp As GraphicsPath = New GraphicsPath()
        gp.AddLine((x + radius), y, (x + (width - radius)), y)
        gp.AddArc((x + (width - radius)), y, radius, radius, 270, 90)
        gp.AddLine((x + width), (y + radius), (x + width), (y + (height - radius)))
        gp.AddArc((x + (width - radius)), (y + (height - radius)), radius, radius, 0, 90)
        gp.AddLine((x + (width - radius)), (y + height), (x + radius), (y + height))
        gp.AddArc(x, (y + (height - radius)), radius, radius, 90, 90)
        gp.AddLine(x, (y + (height - radius)), x, (y + radius))
        gp.AddArc(x, y, radius, radius, 180, 90)
        gp.CloseFigure()
        g.DrawPath(p, gp)
        gp.Dispose()
    End Sub

    Public Sub SetRoundedShape(ByVal g As Graphics, _
                                ByVal x As Integer, _
                                ByVal y As Integer, _
                                ByVal width As Integer, _
                                ByVal height As Integer, _
                                ByVal radius As Integer)
        Dim gp As GraphicsPath = New GraphicsPath()

        Try
            g.SmoothingMode = SmoothingMode.HighQuality

            gp.AddLine((x + radius), y, (x + (width - radius)), y)
            gp.AddArc((x + (width - radius)), y, radius, radius, 270, 90)
            gp.AddLine((x + width), (y + radius), (x + width), (y + (height - radius)))
            gp.AddArc((x + (width - radius)), (y + (height - radius)), radius, radius, 0, 90)
            gp.AddLine((x + (width - radius)), (y + height), (x + radius), (y + height))
            gp.AddArc(x, (y + (height - radius)), radius, radius, 90, 90)
            gp.AddLine(x, (y + (height - radius)), x, (y + radius))
            gp.AddArc(x, y, radius, radius, 180, 90)
            gp.CloseFigure()

            Me.Region = New Region(gp)
        Catch ex As Exception
            Throw ex
        Finally
            gp.Dispose()
        End Try
    End Sub

    Public Sub CollapseExceptThis(ByVal pnl As eZeeCollapsibleContainer)
        For Each cntrl As eZeeCollapsibleContainer In pnl.Parent.Controls
            If cntrl Is Me Then Continue For
            If cntrl.Collapsed = False Then
                cntrl.Collapse()
            End If
        Next
    End Sub

#End Region

#Region " Private Methods "

    Public Sub Collapse()
        'mintLastHeight = Me.Height
        Me.Height = Me.HeaderHeight + Me.HeightOnCollapse
        objimgCollapseButton.Hide()
        objimgExpandButton.Show()
    End Sub

    Public Sub Expand()
        Me.Height = mintLastHeight
        objimgCollapseButton.Show()
        objimgExpandButton.Hide()

        If Me.CollapseAllExceptThis Then
            Call CollapseExceptThis(Me)
        End If
    End Sub

    Private Sub CollapseOperation()
        If Me.ShowCollapseButton Then
            If Me.Height = Me.HeaderHeight Then
                'Code to Expand
                Call Expand()
            Else
                'Code to Collapse
                Call Collapse()
            End If
        End If
    End Sub

    Private Sub SetControls()
        objlblHeaderText.Height = Me.HeaderHeight - 2
        If Me.ShowCollapseButton Then
            objlblHeaderText.Cursor = Cursors.Hand
            If objimgCollapseButton IsNot Nothing Then
                objimgCollapseButton.Height = Me.HeaderHeight - 2
                objimgExpandButton.Height = Me.HeaderHeight - 2
            Else
                If objimgCollapseButton Is Nothing Then
                    objimgCollapseButton = New eZeeImageButton
                End If
                If objimgExpandButton Is Nothing Then
                    objimgExpandButton = New eZeeImageButton
                End If

                objimgCollapseButton.Size = New Size(20, 20)
                objimgCollapseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)

                objimgExpandButton.Size = New Size(20, 20)
                objimgExpandButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)

                If Not Me.Controls.Contains(objimgCollapseButton) Then
                    Me.Controls.Add(objimgCollapseButton)
                End If
                If Not Me.Controls.Contains(objimgExpandButton) Then
                    Me.Controls.Add(objimgExpandButton)
                End If

                objimgCollapseButton.Show()
                objimgExpandButton.Hide()
            End If

            Me.ExpandedHoverImage = My.Resources.Expanded_Hover_20
            Me.ExpandedNormalImage = My.Resources.Expanded__Normal_20
            Me.ExpandedPressedImage = My.Resources.Expanded_Pressed_20

            Me.CollapsedHoverImage = My.Resources.Collapsed_Hover_20
            Me.CollapsedNormalImage = My.Resources.Collapsed_Normal_20
            Me.CollapsedPressedImage = My.Resources.Collapsed_Pressed_20

            objlblHeaderText.Width = Me.Width - objimgCollapseButton.Width - 8 - mintLeftHeaderSpace
            objimgCollapseButton.Location = New Point(Me.Size.Width - objimgCollapseButton.Width - 4, 3)
            objimgExpandButton.Location = objimgCollapseButton.Location

            objimgCollapseButton.NormalImage = Me.CollapsedNormalImage
            objimgCollapseButton.MouseHoverImage = Me.CollapsedHoverImage
            objimgCollapseButton.MouseDownImage = Me.CollapsedPressedImage
            objimgCollapseButton.MouseUpImage = Me.CollapsedHoverImage

            objimgExpandButton.NormalImage = Me.ExpandedNormalImage
            objimgExpandButton.MouseHoverImage = Me.ExpandedHoverImage
            objimgExpandButton.MouseDownImage = Me.ExpandedPressedImage
            objimgExpandButton.MouseUpImage = Me.ExpandedHoverImage
        Else
            'If objimgCollapseButton IsNot Nothing And objimgExpandButton IsNot Nothing Then
            '    Me.Controls.Remove(objimgCollapseButton)
            '    Me.Controls.Remove(objimgExpandButton)
            'End If

            If Me.Controls.Contains(objimgCollapseButton) Then
                Me.Controls.Remove(objimgCollapseButton)
                objimgCollapseButton = Nothing
            End If
            If Me.Controls.Contains(objimgExpandButton) Then
                Me.Controls.Remove(objimgExpandButton)
                objimgExpandButton = Nothing
            End If

            Me.ExpandedHoverImage = Nothing
            Me.ExpandedNormalImage = Nothing
            Me.ExpandedPressedImage = Nothing

            Me.CollapsedHoverImage = Nothing
            Me.CollapsedNormalImage = Nothing
            Me.CollapsedPressedImage = Nothing

            objlblHeaderText.Width = Me.Width - 8 - mintLeftHeaderSpace
            objlblHeaderText.Left = 4 + mintLeftHeaderSpace
            objlblHeaderText.Top = 1
        End If

        If Me.ShowCheckBox Then
            If objchkCheckBox Is Nothing Then
                objchkCheckBox = New CheckBox
            End If

            objchkCheckBox.Text = ""

            objchkCheckBox.AutoSize = False
            objchkCheckBox.CheckAlign = ContentAlignment.MiddleLeft
            objchkCheckBox.Height = Me.HeaderHeight
            objchkCheckBox.Name = "objCheckBox"
            objchkCheckBox.Width = 13
            objchkCheckBox.BackColor = Color.Transparent
            objchkCheckBox.Location = New Point(6, 1)
            'objchkCheckBox.Top = 1
            objchkCheckBox.Checked = mblnChecked

            objlblHeaderText.Left = objchkCheckBox.Left + objchkCheckBox.Width + 2 + mintLeftHeaderSpace

            If Me.ShowCollapseButton Then
                objlblHeaderText.Width = Me.Width - objimgCollapseButton.Width - objchkCheckBox.Width - 11 - mintLeftHeaderSpace
            Else
                objlblHeaderText.Width = Me.Width - objchkCheckBox.Width - 3 - mintLeftHeaderSpace
            End If

            If Not Me.Controls.Contains(objchkCheckBox) Then
                Me.Controls.Add(objchkCheckBox)
            End If
        Else
            If objchkCheckBox IsNot Nothing Then
                Me.Controls.Remove(objchkCheckBox)
                objchkCheckBox = Nothing
            End If

            'objlblHeaderText.BringToFront()
            'objlblHeaderText.Location = New Point(4, 1)
            objlblHeaderText.Left = 4 + mintLeftHeaderSpace
            objlblHeaderText.Top = 1
            If Me.ShowCollapseButton Then
                objlblHeaderText.Width = Me.Width - objimgCollapseButton.Width - 8 - mintLeftHeaderSpace
            Else
                objlblHeaderText.Width = Me.Width - 8 - mintLeftHeaderSpace
            End If
        End If

        If Me.ShowDownButton Then
            If objbtnDownButton Is Nothing Then
                objbtnDownButton = New Button
            End If

            Me.objbtnDownButton.BackColor = System.Drawing.Color.White
            Me.objbtnDownButton.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.objbtnDownButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver
            Me.objbtnDownButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGray
            Me.objbtnDownButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
            Me.objbtnDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.objbtnDownButton.Size = New System.Drawing.Size(373, 13)
            Me.objbtnDownButton.TabStop = False
            Me.objbtnDownButton.UseVisualStyleBackColor = False
            Me.objbtnDownButton.Text = ""

            If Not Me.Controls.Contains(objbtnDownButton) Then
                Me.Controls.Add(objbtnDownButton)
            End If

            If Not DesignMode Then
                If Me.CollapseOnLoad Then
                    Me.Height = Me.HeightOnCollapse
                Else
                    Me.Height = mintLastHeight
                End If
            End If
            If Me.HeightOnCollapse = Me.Height Then
                objbtnDownButton.Image = My.Resources.Down_16
            Else
                objbtnDownButton.Image = My.Resources.Up_16
            End If
        Else
            If objbtnDownButton IsNot Nothing Then
                Me.Controls.Remove(objbtnDownButton)
                Me.Height = mintLastHeight
                objbtnDownButton = Nothing
            End If
        End If

        Me.Invalidate()
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(e)

        'Krushna (FD GUI) (14 Jul 2009) -- Start
        Dim g As Graphics = e.Graphics
        If Me.ShowHeader = True Then

            Dim start As New Point
            start.X = CInt(Me.Width / 0.5)
            start.Y = 0

            Dim rect As Rectangle

            rect = New Rectangle(start, CType(New Point(start.X, CInt((Me.HeaderHeight * 0.6))), Drawing.Size))
            Dim gb As New LinearGradientBrush(rect, ControlPaint.Light(mclrGradientColor, 1.5F), mclrGradientColor, LinearGradientMode.Vertical) 'Color.FromArgb(206, 223, 251)
            g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, CInt(Me.HeaderHeight * 0.6)))

            rect = New Rectangle(New Point(start.X, CInt(Me.HeaderHeight * 0.4)), CType(New Point(start.X, Me.HeaderHeight), Drawing.Size))
            gb = New LinearGradientBrush(rect, mclrGradientColor, ControlPaint.Light(mclrGradientColor, 1.5F), LinearGradientMode.Vertical) 'Color.FromArgb(241, 247, 254))
            g.FillRectangle(gb, New Rectangle(0, CInt(Me.HeaderHeight * 0.5), Me.Width, CInt(Me.HeaderHeight * 0.5)))

            rect = New Rectangle(start, New Size(Me.Width, Me.HeaderHeight))

            If Me.ShowBorder Then
                If Me.ShowDefaultBorderColor Then
                    g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), 1, 0, Me.Width - 2, 0) 'Up Outer Border
                    g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), 1, Me.HeaderHeight - 1, Me.Width - 2, Me.HeaderHeight - 1) 'Down Outer Border
                    g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), 0, 0, 0, Me.HeaderHeight) 'Left Vertical Border
                    g.DrawLine(New Pen(ControlPaint.Dark(mclrGradientColor, -0.1F)), Me.Width - 1, 0, Me.Width - 1, Me.HeaderHeight) 'Right Vertical Border
                Else
                    g.DrawLine(New Pen(Me.BorderColor), 1, 0, Me.Width - 2, 0) 'Up Outer Border
                    g.DrawLine(New Pen(Me.BorderColor), 1, Me.HeaderHeight - 1, Me.Width - 2, Me.HeaderHeight - 1) 'Down Outer Border
                    g.DrawLine(New Pen(Me.BorderColor), 0, 0, 0, Me.HeaderHeight) 'Left Vertical Border
                    g.DrawLine(New Pen(Me.BorderColor), Me.Width - 1, 0, Me.Width - 1, Me.HeaderHeight) 'Right Vertical Border
                End If
            End If

        End If
        'Krushna (FD GUI) (14 Jul 2009) -- End

        If Me.ShowBorder Then
            If Me.ShowDefaultBorderColor Then
                ControlPaint.DrawBorder(g, Me.ClientRectangle, ControlPaint.Dark(mclrGradientColor, -0.1F), Me.PanelBorderStyle)
            Else
                ControlPaint.DrawBorder(g, Me.ClientRectangle, Me.BorderColor, Me.PanelBorderStyle)
            End If
        End If

        'HRK - 19 Jul 2010 - Start
        If mstrHeaderMessage <> "" Then
            Dim b As New SolidBrush(HeaderMessageForeColor)
            Dim lblSize As SizeF = g.MeasureString(objlblHeaderText.Text, objlblHeaderText.Font)
            Dim sf As New StringFormat
            Dim strDraw As String = mstrHeaderMessage
            If Me.RightToLeft Then
                strDraw &= "  "
                sf.FormatFlags = StringFormatFlags.DirectionRightToLeft
                sf.Alignment = StringAlignment.Near
            Else
                strDraw = "  " & strDraw
            End If
            sf.LineAlignment = StringAlignment.Center
            g.DrawString(strDraw, HeaderMessageFont, b, New RectangleF(lblSize.Width, objlblHeaderText.Top + 1, Me.Width - lblSize.Width - 100, objlblHeaderText.Height), sf)
        End If
        'HRK - 19 Jul 2010 - End
    End Sub

    Protected Overrides Sub OnResize(ByVal eventargs As System.EventArgs)
        MyBase.OnResize(eventargs)
        If objbtnDownButton IsNot Nothing Then
            objbtnDownButton.BringToFront()
        End If
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        'Krushna (FD GUI) (14 Jul 2009) -- Start
        If Me.ShowHeader = False Then Exit Sub
        'Krushna (FD GUI) (14 Jul 2009) -- End

        objlblHeaderText.AutoSize = False
        objlblHeaderText.BackColor = Color.Transparent
        objlblHeaderText.Name = "objlblHeaderText"
        objlblHeaderText.Text = Me.Text
        'Krushna (FD GUI) (14 Jul 2009) -- Start
        objlblHeaderText.TextAlign = Me.TextAlign
        'Krushna (FD GUI) (14 Jul 2009) -- End
        objlblHeaderText.Height = Me.HeaderHeight - 2
        objlblHeaderText.Width = Me.Width - 4 - mintLeftHeaderSpace
        'objlblHeaderText.Location = New Point(4, 1)
        objlblHeaderText.Left = mintLeftHeaderSpace + 4
        objlblHeaderText.Top = 1
        objlblHeaderText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)

        'mintLastHeight = Me.Height
        mintNonHeaderHeight = Me.Height - Me.HeaderHeight
        objlblHeaderText.SendToBack()

        If Me.ShowCollapseButton Or Me.ShowCheckBox Or Me.ShowDownButton Then
            Call SetControls()
        End If

        Me.Controls.Add(objlblHeaderText)

        If objchkCheckBox IsNot Nothing Then
            objchkCheckBox_CheckedChanged(objchkCheckBox, Nothing)
        End If
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lblHeaderText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objlblHeaderText.Click
        Call CollapseOperation()
        RaiseEvent HeaderClick(Me, e)
    End Sub

    Private Sub imgCollapseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objimgCollapseButton.Click
        Call CollapseOperation()
        RaiseEvent HeaderClick(Me, e)
    End Sub

    Private Sub imgExpandButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objimgExpandButton.Click
        Call CollapseOperation()
        RaiseEvent HeaderClick(Me, e)
    End Sub

    Private Sub objchkCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkCheckBox.CheckedChanged
        Me.Checked = objchkCheckBox.Checked
        'Krushna (FD GUI) (14 Jul 2009) -- Start
        If DesignMode = False Then
            If objchkCheckBox.Checked = True Then
                For Each cntrl As Control In Me.Controls
                    If cntrl Is objchkCheckBox Then Continue For
                    cntrl.Enabled = True
                Next
            Else
                For Each cntrl As Control In Me.Controls
                    If cntrl Is objchkCheckBox Then Continue For
                    cntrl.Enabled = False
                Next
            End If
        End If
        'Krushna (FD GUI) (14 Jul 2009) -- End
        RaiseEvent CheckedChanged(sender, e)
    End Sub

    Private Sub objbtnDownButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnDownButton.Click
        If Me.HeightOnCollapse = Me.Height Then
            objbtnDownButton.Image = My.Resources.Up_16
            Me.Height = mintLastHeight
        Else
            objbtnDownButton.Image = My.Resources.Down_16
            Me.Height = Me.HeightOnCollapse
        End If
        Me.Invalidate()
    End Sub

#End Region

End Class

Public Delegate Sub HeaderClickEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
Public Delegate Sub CheckedChangedEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)

